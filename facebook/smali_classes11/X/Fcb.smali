.class public LX/Fcb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FcI;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FcI",
        "<",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        "Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final b:F

.field public static final c:F

.field private static final d:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final f:LX/0lB;

.field private final g:LX/0wM;

.field private final h:Landroid/content/res/Resources;

.field private final i:Ljava/text/NumberFormat;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x3e8

    const/16 v6, 0x64

    const/16 v5, 0x32

    const/4 v4, 0x0

    .line 2262575
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/3rL;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3rL;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3rL;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x19

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3rL;

    const/16 v2, 0xfa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3rL;

    const/16 v2, 0x1f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3rL;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/3rL;

    const/16 v2, 0x2710

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2262576
    sput-object v0, LX/Fcb;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    sput v0, LX/Fcb;->b:F

    .line 2262577
    const v0, 0x461c4000    # 10000.0f

    sget v1, LX/Fcb;->b:F

    div-float/2addr v0, v1

    sput v0, LX/Fcb;->c:F

    .line 2262578
    new-instance v0, LX/FcY;

    invoke-direct {v0}, LX/FcY;-><init>()V

    sput-object v0, LX/Fcb;->d:LX/FcE;

    .line 2262579
    new-instance v0, LX/FcZ;

    invoke-direct {v0}, LX/FcZ;-><init>()V

    sput-object v0, LX/Fcb;->e:LX/FcE;

    return-void
.end method

.method public constructor <init>(LX/0lB;LX/0wM;Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262569
    iput-object p1, p0, LX/Fcb;->f:LX/0lB;

    .line 2262570
    iput-object p2, p0, LX/Fcb;->g:LX/0wM;

    .line 2262571
    iput-object p3, p0, LX/Fcb;->h:Landroid/content/res/Resources;

    .line 2262572
    invoke-static {}, Ljava/text/NumberFormat;->getCurrencyInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, LX/Fcb;->i:Ljava/text/NumberFormat;

    .line 2262573
    iget-object v0, p0, LX/Fcb;->i:Ljava/text/NumberFormat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 2262574
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;F)F
    .locals 2

    .prologue
    .line 2262561
    if-nez p1, :cond_1

    .line 2262562
    :cond_0
    :goto_0
    return p3

    .line 2262563
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/Fcb;->f:LX/0lB;

    const-class v1, Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 2262564
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2262565
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float p3, v0, v1

    goto :goto_0

    .line 2262566
    :catch_0
    goto :goto_0

    .line 2262567
    :catch_1
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Fcb;
    .locals 6

    .prologue
    .line 2262550
    const-class v1, LX/Fcb;

    monitor-enter v1

    .line 2262551
    :try_start_0
    sget-object v0, LX/Fcb;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2262552
    sput-object v2, LX/Fcb;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2262553
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262554
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2262555
    new-instance p0, LX/Fcb;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lB;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, LX/Fcb;-><init>(LX/0lB;LX/0wM;Landroid/content/res/Resources;)V

    .line 2262556
    move-object v0, p0

    .line 2262557
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2262558
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fcb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262559
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2262560
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(F)I
    .locals 5

    .prologue
    .line 2262542
    sget v0, LX/Fcb;->c:F

    div-float v0, p0, v0

    .line 2262543
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 2262544
    float-to-int v2, v0

    int-to-float v2, v2

    sub-float v2, v0, v2

    .line 2262545
    sget-object v0, LX/Fcb;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    .line 2262546
    sget-object v0, LX/Fcb;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 2262547
    sget-object v3, LX/Fcb;->a:LX/0Px;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3rL;

    iget-object v1, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v3, v1

    .line 2262548
    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-float v1, v3

    mul-float/2addr v2, v1

    iget-object v1, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v2, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int/2addr v0, v1

    add-int/2addr v0, v4

    .line 2262549
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2710

    goto :goto_0
.end method

.method private static c(F)F
    .locals 4

    .prologue
    .line 2262530
    sget v0, LX/Fcb;->b:F

    float-to-int v2, v0

    .line 2262531
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, LX/Fcb;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2262532
    sget-object v0, LX/Fcb;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 2262533
    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_1

    .line 2262534
    add-int/lit8 v0, v1, -0x1

    move v2, v0

    .line 2262535
    :cond_0
    int-to-float v0, v2

    sget v1, LX/Fcb;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 2262536
    const v0, 0x461c4000    # 10000.0f

    .line 2262537
    :goto_1
    return v0

    .line 2262538
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2262539
    :cond_2
    sget-object v0, LX/Fcb;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 2262540
    sget-object v1, LX/Fcb;->a:LX/0Px;

    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3rL;

    iget-object v1, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int v1, v3, v1

    .line 2262541
    sget v3, LX/Fcb;->c:F

    int-to-float v2, v2

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v2

    mul-float/2addr v0, v3

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)LX/CyH;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2262525
    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    move-object v1, v3

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262526
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "default"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2262527
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2262528
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2262529
    :cond_0
    new-instance v0, LX/CyH;

    invoke-direct {v0, p1, v3, v1}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262493
    sget-object v0, LX/Fcb;->d:LX/FcE;

    return-object v0
.end method

.method public final a(LX/CyH;)LX/FcT;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const v4, 0x461c4000    # 10000.0f

    const/4 v3, 0x0

    .line 2262513
    iget-object v0, p1, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262514
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "value"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "price_lower_bound"

    invoke-direct {p0, v0, v1, v3}, LX/Fcb;->a(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v1

    .line 2262515
    iget-object v0, p1, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262516
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v2, "value"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "price_upper_bound"

    invoke-direct {p0, v0, v2, v4}, LX/Fcb;->a(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v0

    .line 2262517
    cmpl-float v2, v1, v3

    if-nez v2, :cond_0

    cmpg-float v2, v0, v4

    if-gtz v2, :cond_0

    .line 2262518
    iget-object v1, p0, LX/Fcb;->h:Landroid/content/res/Resources;

    const v2, 0x7f082089

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, LX/Fcb;->i:Ljava/text/NumberFormat;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2262519
    :goto_0
    new-instance v1, LX/FcT;

    iget-object v2, p0, LX/Fcb;->g:LX/0wM;

    const v3, 0x7f020935

    const v4, -0xa76f01

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2262520
    iget-object v3, p1, LX/CyH;->c:LX/4FP;

    move-object v3, v3

    .line 2262521
    invoke-direct {v1, v0, v2, v3}, LX/FcT;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;LX/4FP;)V

    return-object v1

    .line 2262522
    :cond_0
    cmpl-float v2, v1, v3

    if-ltz v2, :cond_1

    cmpl-float v2, v0, v4

    if-nez v2, :cond_1

    .line 2262523
    iget-object v0, p0, LX/Fcb;->h:Landroid/content/res/Resources;

    const v2, 0x7f08208a

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, LX/Fcb;->i:Ljava/text/NumberFormat;

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-long v6, v1

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2262524
    :cond_1
    iget-object v2, p0, LX/Fcb;->h:Landroid/content/res/Resources;

    const v3, 0x7f08208b

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, LX/Fcb;->i:Ljava/text/NumberFormat;

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-long v6, v1

    invoke-virtual {v5, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    iget-object v1, p0, LX/Fcb;->i:Ljava/text/NumberFormat;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v6, v0

    invoke-virtual {v1, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V
    .locals 3

    .prologue
    .line 2262505
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2262506
    invoke-interface {p1}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262507
    iget-object v0, p3, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262508
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "value"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2262509
    if-eqz v0, :cond_0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, -0x6e685d

    .line 2262510
    :goto_0
    iget-object v1, p0, LX/Fcb;->g:LX/0wM;

    const v2, 0x7f020935

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2262511
    return-void

    .line 2262512
    :cond_1
    const v0, -0xa76f01

    goto :goto_0
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 5

    .prologue
    .line 2262495
    check-cast p2, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;

    const v4, 0x461c4000    # 10000.0f

    const/4 v3, 0x0

    .line 2262496
    iget-object v0, p3, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262497
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "value"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "price_lower_bound"

    invoke-direct {p0, v0, v1, v3}, LX/Fcb;->a(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v1

    .line 2262498
    iget-object v0, p3, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262499
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v2, "value"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "price_upper_bound"

    invoke-direct {p0, v0, v2, v4}, LX/Fcb;->a(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v2

    .line 2262500
    const v0, 0x7f0d2b83

    invoke-virtual {p2, v0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;

    .line 2262501
    invoke-virtual {v0, v3, v4}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e(FF)V

    .line 2262502
    invoke-static {v1}, LX/Fcb;->c(F)F

    move-result v1

    invoke-static {v2}, LX/Fcb;->c(F)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->f(FF)V

    .line 2262503
    new-instance v1, LX/Fca;

    invoke-direct {v1, p0, p2, p5, p3}, LX/Fca;-><init>(LX/Fcb;Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;LX/FdQ;LX/CyH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->setRangeSeekBarChangeListener(LX/ACu;)V

    .line 2262504
    return-void
.end method

.method public final b()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/search/results/filters/ui/SearchResultFilterPriceRangeSeekbar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262494
    sget-object v0, LX/Fcb;->e:LX/FcE;

    return-object v0
.end method
