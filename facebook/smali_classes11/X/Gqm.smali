.class public LX/Gqm;
.super LX/16T;
.source ""


# static fields
.field public static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public a:LX/3kp;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2396925
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSTANT_SHOPPING_SAVE_NUX:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Gqm;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/3kp;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2396926
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 2396927
    iput-object p1, p0, LX/Gqm;->a:LX/3kp;

    .line 2396928
    iget-object v0, p0, LX/Gqm;->a:LX/3kp;

    const/4 v1, 0x3

    .line 2396929
    iput v1, v0, LX/3kp;->b:I

    .line 2396930
    iget-object v0, p0, LX/Gqm;->a:LX/3kp;

    sget-object v1, LX/Gn0;->c:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2396931
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2396932
    const-wide/32 v0, 0xf731400

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 1

    .prologue
    .line 2396933
    iget-object v0, p0, LX/Gqm;->a:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2396934
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 2396935
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2396936
    const-string v0, "3903"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2396937
    sget-object v0, LX/Gqm;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
