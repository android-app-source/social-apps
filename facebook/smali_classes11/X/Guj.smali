.class public final LX/Guj;
.super LX/Gui;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

.field private h:J


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 2403751
    iput-object p1, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403752
    invoke-direct {p0, p1, p2}, LX/Gui;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    .line 2403753
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Guj;->h:J

    .line 2403754
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V
    .locals 2

    .prologue
    .line 2403791
    iget-object v0, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403792
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2403793
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/GuX;->g:LX/BWN;

    if-nez v1, :cond_1

    .line 2403794
    :cond_0
    :goto_0
    return-void

    .line 2403795
    :cond_1
    invoke-super {p0, p1, p2}, LX/Gui;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V

    .line 2403796
    const v1, 0x7f0d1188

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;)V
    .locals 3

    .prologue
    .line 2403797
    iget-object v0, p0, LX/GuX;->g:LX/BWN;

    .line 2403798
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403799
    const-string v2, "callback"

    invoke-interface {v0, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Guj;->a:Ljava/lang/String;

    .line 2403800
    iget-object v0, p0, LX/GuX;->g:LX/BWN;

    .line 2403801
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403802
    const-string v2, "post_id"

    invoke-interface {v0, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Guj;->b:Ljava/lang/String;

    .line 2403803
    iget-object v0, p0, LX/GuX;->g:LX/BWN;

    .line 2403804
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403805
    const-string v2, "allow_empty_comment"

    invoke-interface {v0, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/Guj;->d:Z

    .line 2403806
    return-void
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;Landroid/widget/TextView;)V
    .locals 6

    .prologue
    .line 2403763
    invoke-super {p0, p1, p2}, LX/Gui;->a(Lcom/facebook/webview/FacebookWebView;Landroid/widget/TextView;)V

    .line 2403764
    check-cast p2, Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 2403765
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/8oj;->a(Landroid/text/Editable;Z)Ljava/lang/String;

    move-result-object v0

    .line 2403766
    iget-boolean v1, p0, LX/Gui;->d:Z

    if-nez v1, :cond_0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2403767
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 2403768
    :try_start_0
    const-string v2, "text"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2403769
    const-string v0, "post_id"

    iget-object v2, p0, LX/Guj;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2403770
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2403771
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2403772
    iget-object v1, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/4 v2, 0x3

    .line 2403773
    invoke-virtual {v1, v2}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a(I)Landroid/support/v4/app/DialogFragment;

    move-result-object v3

    .line 2403774
    const-string v4, "Cannot create dialog for %s. Check onCreateDialogFragment(int) method"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2403775
    invoke-static {}, LX/0sL;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2403776
    invoke-static {v4, v3}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2403777
    :cond_1
    iget-object v4, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v4, v4

    .line 2403778
    invoke-virtual {v4}, LX/0gc;->a()LX/0hH;

    move-result-object v5

    .line 2403779
    invoke-static {v2}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->g(I)Ljava/lang/String;

    move-result-object p2

    .line 2403780
    invoke-virtual {v4, p2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 2403781
    if-eqz v4, :cond_2

    .line 2403782
    invoke-virtual {v5, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2403783
    :cond_2
    const/16 v4, 0x1001

    invoke-virtual {v5, v4}, LX/0hH;->a(I)LX/0hH;

    .line 2403784
    const/4 v4, 0x1

    invoke-virtual {v3, v5, p2, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 2403785
    iget-object v1, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/Guj;->h:J

    .line 2403786
    iget-object v1, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hx;->a(Z)V

    .line 2403787
    iget-object v1, p0, LX/Guj;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v0, p0}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;Ljava/util/List;LX/BWL;)V

    .line 2403788
    :cond_3
    return-void

    .line 2403789
    :catch_0
    move-exception v0

    .line 2403790
    iget-object v2, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v2}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v2

    const-string v3, "inconceivable exception"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2403755
    iget-object v0, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e(I)V

    .line 2403756
    iget-wide v0, p0, LX/Guj;->h:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 2403757
    iget-object v0, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    iget-object v1, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/Guj;->h:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 2403758
    iput-wide v6, p0, LX/Guj;->h:J

    .line 2403759
    :cond_0
    if-eqz p3, :cond_1

    .line 2403760
    iget-object v0, p0, LX/Guj;->c:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083151

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    .line 2403761
    :goto_0
    return-void

    .line 2403762
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, LX/Gui;->a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method
