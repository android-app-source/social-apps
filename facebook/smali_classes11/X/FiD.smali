.class public LX/FiD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fi7;


# instance fields
.field public final a:LX/FgS;

.field public b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(LX/FgS;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2274690
    iput-object p1, p0, LX/FiD;->a:LX/FgS;

    .line 2274691
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/FiD;->b:Landroid/os/Bundle;

    .line 2274692
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2274688
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 4

    .prologue
    .line 2274695
    iget-object v0, p0, LX/FiD;->a:LX/FgS;

    .line 2274696
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2274697
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FiD;->a:LX/FgS;

    .line 2274698
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2274699
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2274700
    if-nez v0, :cond_1

    .line 2274701
    :cond_0
    const-string v0, ""

    .line 2274702
    :goto_0
    move-object v0, v0

    .line 2274703
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2274704
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2274705
    sget-object v2, LX/7B4;->MARKETPLACE:LX/7B4;

    iget-object v3, p0, LX/FiD;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2274706
    new-instance v2, LX/CwH;

    invoke-direct {v2}, LX/CwH;-><init>()V

    .line 2274707
    iput-object v0, v2, LX/CwH;->b:Ljava/lang/String;

    .line 2274708
    move-object v2, v2

    .line 2274709
    iput-object v0, v2, LX/CwH;->d:Ljava/lang/String;

    .line 2274710
    move-object v0, v2

    .line 2274711
    const-string v2, "scoped"

    .line 2274712
    iput-object v2, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2274713
    move-object v0, v0

    .line 2274714
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2274715
    iput-object v2, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2274716
    move-object v0, v0

    .line 2274717
    sget-object v2, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    .line 2274718
    iput-object v2, v0, LX/CwH;->l:LX/CwI;

    .line 2274719
    move-object v0, v0

    .line 2274720
    invoke-static {p1}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v2

    .line 2274721
    iput-object v2, v0, LX/CwH;->x:LX/0Px;

    .line 2274722
    move-object v0, v0

    .line 2274723
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 2274724
    iput-object v1, v0, LX/CwH;->y:LX/0P1;

    .line 2274725
    move-object v0, v0

    .line 2274726
    iget-object v1, p1, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v1, v1

    .line 2274727
    iget-object v2, p1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v2, v2

    .line 2274728
    iget-object v3, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v3, v3

    .line 2274729
    invoke-virtual {v0, v1, v2, v3}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/103;)LX/CwH;

    move-result-object v0

    const-string v1, ""

    .line 2274730
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2274731
    move-object v0, v0

    .line 2274732
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, LX/FiD;->a:LX/FgS;

    .line 2274733
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2274734
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2274735
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 2274736
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/Fid;)Z
    .locals 1

    .prologue
    .line 2274694
    const/4 v0, 0x0

    return v0
.end method

.method public final b()LX/CwU;
    .locals 1

    .prologue
    .line 2274693
    const/4 v0, 0x0

    return-object v0
.end method
