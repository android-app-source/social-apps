.class public final LX/H7V;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2430741
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2430742
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2430743
    :goto_0
    return v1

    .line 2430744
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2430745
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2430746
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2430747
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2430748
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2430749
    const-string v4, "application"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2430750
    const/4 v3, 0x0

    .line 2430751
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 2430752
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2430753
    :goto_2
    move v2, v3

    .line 2430754
    goto :goto_1

    .line 2430755
    :cond_2
    const-string v4, "deeplink"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2430756
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2430757
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2430758
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2430759
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2430760
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2430761
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2430762
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2430763
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2430764
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2430765
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 2430766
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2430767
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 2430768
    :cond_7
    const-string v6, "profile_picture"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2430769
    invoke-static {p0, p1}, LX/H7Z;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_3

    .line 2430770
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2430771
    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 2430772
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2430773
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v2, v3

    move v4, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2430774
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2430775
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2430776
    if-eqz v0, :cond_2

    .line 2430777
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430778
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2430779
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2430780
    if-eqz v1, :cond_0

    .line 2430781
    const-string v2, "name"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430782
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430783
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2430784
    if-eqz v1, :cond_1

    .line 2430785
    const-string v2, "profile_picture"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430786
    invoke-static {p0, v1, p2}, LX/H7Z;->a(LX/15i;ILX/0nX;)V

    .line 2430787
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2430788
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2430789
    if-eqz v0, :cond_3

    .line 2430790
    const-string v1, "deeplink"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2430791
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2430792
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2430793
    return-void
.end method
