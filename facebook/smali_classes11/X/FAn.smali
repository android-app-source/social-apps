.class public LX/FAn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/6HF;


# instance fields
.field public final a:LX/73w;

.field public final b:Lcom/facebook/performancelogger/PerformanceLogger;

.field public final c:LX/74I;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:J


# direct methods
.method public constructor <init>(LX/73w;Lcom/facebook/performancelogger/PerformanceLogger;LX/74I;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2208219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208220
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FAn;->e:Z

    .line 2208221
    iput-object p1, p0, LX/FAn;->a:LX/73w;

    .line 2208222
    iput-object p2, p0, LX/FAn;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2208223
    iput-object p3, p0, LX/FAn;->c:LX/74I;

    .line 2208224
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2208218
    const-string v0, "camera"

    return-object v0
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 2208216
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    invoke-virtual {v0, p1, p2}, LX/73w;->a(IZ)V

    .line 2208217
    return-void
.end method

.method public final a(LX/6HG;)V
    .locals 7

    .prologue
    .line 2208201
    iget-object v0, p0, LX/FAn;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x250001

    invoke-virtual {p0}, LX/FAn;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2208202
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    invoke-virtual {p1}, LX/6HG;->l()F

    move-result v1

    .line 2208203
    iget v2, p1, LX/6HG;->a:I

    move v2, v2

    .line 2208204
    iget v3, p1, LX/6HG;->b:I

    move v3, v3

    .line 2208205
    iget v4, p1, LX/6HG;->c:I

    move v4, v4

    .line 2208206
    iget v5, p1, LX/6HG;->d:I

    move v5, v5

    .line 2208207
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v6

    .line 2208208
    const-string p0, "session_time"

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208209
    const-string p0, "camera_ref"

    iget-object p1, v0, LX/73w;->i:Ljava/lang/String;

    invoke-virtual {v6, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208210
    const-string p0, "picture_count"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208211
    const-string p0, "touch_to_focus_count"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208212
    const-string p0, "last_second_autofocus_count"

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208213
    const-string p0, "face_detection_autofocus_count"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208214
    sget-object p0, LX/74R;->CAMERA_STOPPED:LX/74R;

    const/4 p1, 0x0

    invoke-static {v0, p0, v6, p1}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2208215
    return-void
.end method

.method public final a(LX/6HG;I)V
    .locals 2

    .prologue
    .line 2208191
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    .line 2208192
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2208193
    const-string p0, "size"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208194
    const-string p0, "layout_orientation"

    iget-object p1, v0, LX/73w;->l:Ljava/lang/String;

    invoke-virtual {v1, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208195
    const-string p0, "device_orientation"

    iget-object p1, v0, LX/73w;->m:Ljava/lang/String;

    invoke-virtual {v1, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208196
    const-string p0, "camera_index"

    iget p1, v0, LX/73w;->n:I

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208197
    const-string p0, "camera_flash"

    iget-object p1, v0, LX/73w;->o:Ljava/lang/String;

    invoke-virtual {v1, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208198
    const-string p0, "camera_ref"

    iget-object p1, v0, LX/73w;->i:Ljava/lang/String;

    invoke-virtual {v1, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208199
    sget-object p0, LX/74R;->CAMERA_TAKE_PHOTO:LX/74R;

    const/4 p1, 0x0

    invoke-static {v0, p0, v1, p1}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2208200
    return-void
.end method

.method public final a(LX/6IG;)V
    .locals 2

    .prologue
    .line 2208188
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    invoke-virtual {p1}, LX/6IG;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2208189
    iput-object v1, v0, LX/73w;->l:Ljava/lang/String;

    .line 2208190
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2208186
    const-string v0, "camera_session_id"

    iget-object v1, p0, LX/FAn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2208187
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2208183
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    .line 2208184
    const/4 p0, 0x0

    invoke-static {v0, p0, p1}, LX/73w;->a(LX/73w;ZLandroid/net/Uri;)V

    .line 2208185
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2208181
    const-string v0, "camera_session_id"

    iget-object v1, p0, LX/FAn;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208182
    return-void
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2208100
    iget-object v0, p0, LX/FAn;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x250001

    invoke-virtual {p0}, LX/FAn;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2208101
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FAn;->e:Z

    .line 2208102
    if-eqz p1, :cond_0

    .line 2208103
    const-string v0, "camera_session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FAn;->d:Ljava/lang/String;

    .line 2208104
    :cond_0
    iget-object v0, p0, LX/FAn;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2208105
    invoke-static {}, LX/74I;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FAn;->d:Ljava/lang/String;

    .line 2208106
    :cond_1
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    iget-object v1, p0, LX/FAn;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/73w;->a(Ljava/lang/String;)V

    .line 2208107
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    .line 2208108
    iput-object p2, v0, LX/73w;->i:Ljava/lang/String;

    .line 2208109
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2208110
    const-string v2, "camera_ref"

    iget-object p0, v0, LX/73w;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208111
    sget-object v2, LX/74R;->CAMERA_START:LX/74R;

    const/4 p0, 0x0

    invoke-static {v0, v2, v1, p0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2208112
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2208176
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    .line 2208177
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2208178
    const-string v2, "close_reason"

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208179
    sget-object v2, LX/74R;->CAMERA_CLOSING:LX/74R;

    const/4 p0, 0x0

    invoke-static {v0, v2, v1, p0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2208180
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 2208153
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    const/4 p0, 0x0

    .line 2208154
    if-nez p2, :cond_2

    .line 2208155
    const-string v1, "CameraException"

    invoke-static {v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2208156
    :goto_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2208157
    const-string v2, "ex_msg"

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208158
    if-eqz p2, :cond_0

    .line 2208159
    const-string v2, "ex_type"

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208160
    :cond_0
    const-string v2, "layout_orientation"

    iget-object v3, v0, LX/73w;->l:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208161
    const-string v2, "device_orientation"

    iget-object v3, v0, LX/73w;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208162
    const-string v2, "camera_index"

    iget v3, v0, LX/73w;->n:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208163
    const-string v2, "camera_flash"

    iget-object v3, v0, LX/73w;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208164
    const-string v2, "product"

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208165
    const-string v2, "device"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208166
    const-string v2, "board"

    sget-object v3, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208167
    const-string v2, "manufacturer"

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208168
    const-string v2, "brand"

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208169
    const-string v2, "model"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208170
    sget-object v2, LX/74R;->CAMERA_EXCEPTION:LX/74R;

    invoke-static {v0, v2, v1, p0, p0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 2208171
    iget-object v1, v0, LX/73w;->e:LX/03V;

    if-eqz v1, :cond_1

    .line 2208172
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Layout:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, LX/73w;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Device:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, LX/73w;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Camera:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, LX/73w;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Flash:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, LX/73w;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2208173
    iget-object v2, v0, LX/73w;->e:LX/03V;

    const-string v3, "CameraException"

    invoke-virtual {v2, v3, v1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2208174
    :cond_1
    return-void

    .line 2208175
    :cond_2
    const-string v1, "CameraException"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2208146
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    .line 2208147
    iput-object p1, v0, LX/73w;->o:Ljava/lang/String;

    .line 2208148
    if-eqz p2, :cond_0

    .line 2208149
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2208150
    const-string v2, "camera_flash"

    iget-object p0, v0, LX/73w;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208151
    sget-object v2, LX/74R;->CAMERA_FLASH:LX/74R;

    const/4 p0, 0x0

    invoke-static {v0, v2, v1, p0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2208152
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2208140
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    .line 2208141
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2208142
    const-string p0, "video_recording_success"

    if-eqz p1, :cond_0

    const-string v1, "1"

    :goto_0
    invoke-virtual {v2, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208143
    sget-object v1, LX/74R;->CAMERA_VIDEO_RECORDER_STOPPED:LX/74R;

    const/4 p0, 0x0

    invoke-static {v0, v1, v2, p0}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2208144
    return-void

    .line 2208145
    :cond_0
    const-string v1, "0"

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2208136
    iget-boolean v0, p0, LX/FAn;->e:Z

    if-eqz v0, :cond_0

    .line 2208137
    iget-object v0, p0, LX/FAn;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x250001

    invoke-virtual {p0}, LX/FAn;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2208138
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FAn;->e:Z

    .line 2208139
    :cond_0
    return-void
.end method

.method public final b(LX/6IG;)V
    .locals 2

    .prologue
    .line 2208133
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    invoke-virtual {p1}, LX/6IG;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2208134
    iput-object v1, v0, LX/73w;->m:Ljava/lang/String;

    .line 2208135
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2208130
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    .line 2208131
    const/4 p0, 0x1

    invoke-static {v0, p0, p1}, LX/73w;->a(LX/73w;ZLandroid/net/Uri;)V

    .line 2208132
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2208128
    iput-object p1, p0, LX/FAn;->d:Ljava/lang/String;

    .line 2208129
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2208127
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2208122
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    .line 2208123
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2208124
    sget-object v2, LX/74R;->CAMERA_LOADING:LX/74R;

    const/4 v3, 0x0

    invoke-static {v0, v2, v1, v3}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2208125
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/FAn;->f:J

    .line 2208126
    return-void
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 2208115
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LX/FAn;->f:J

    sub-long/2addr v0, v2

    .line 2208116
    iget-object v2, p0, LX/FAn;->a:LX/73w;

    .line 2208117
    long-to-float v4, v0

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    .line 2208118
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 2208119
    const-string v6, "load_time"

    invoke-static {v4}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208120
    sget-object v4, LX/74R;->CAMERA_DONE_LOADING:LX/74R;

    const/4 v6, 0x0

    invoke-static {v2, v4, v5, v6}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2208121
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2208113
    iget-object v0, p0, LX/FAn;->a:LX/73w;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/73w;->a(IZ)V

    .line 2208114
    return-void
.end method
