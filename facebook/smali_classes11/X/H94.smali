.class public LX/H94;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I


# instance fields
.field public final e:LX/H8W;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HA0;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Landroid/content/Context;

.field public k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433883
    const v0, 0x7f02086f

    sput v0, LX/H94;->a:I

    .line 2433884
    const v0, 0x7f020b47

    sput v0, LX/H94;->b:I

    .line 2433885
    const v0, 0x7f08157c

    sput v0, LX/H94;->c:I

    .line 2433886
    const v0, 0x7f081580

    sput v0, LX/H94;->d:I

    return-void
.end method

.method public constructor <init>(LX/H8W;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 0
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H8W;",
            "LX/0Ot",
            "<",
            "LX/HA0;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433936
    iput-object p1, p0, LX/H94;->e:LX/H8W;

    .line 2433937
    iput-object p2, p0, LX/H94;->f:LX/0Ot;

    .line 2433938
    iput-object p3, p0, LX/H94;->g:LX/0Ot;

    .line 2433939
    iput-object p4, p0, LX/H94;->h:LX/0Ot;

    .line 2433940
    iput-object p5, p0, LX/H94;->i:LX/0Ot;

    .line 2433941
    iput-object p6, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433942
    iput-object p7, p0, LX/H94;->j:Landroid/content/Context;

    .line 2433943
    return-void
.end method

.method public static a$redex0(LX/H94;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V
    .locals 2

    .prologue
    .line 2433923
    iget-object v0, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->z()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    if-ne v0, p2, :cond_1

    iget-object v0, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->t()Z

    move-result v0

    if-ne v0, p3, :cond_1

    .line 2433924
    :cond_0
    :goto_0
    return-void

    .line 2433925
    :cond_1
    iget-object v0, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    instance-of v0, v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    if-eqz v0, :cond_0

    .line 2433926
    iget-object v0, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-static {v0}, LX/9YE;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)LX/9YE;

    move-result-object v0

    .line 2433927
    iput-object p1, v0, LX/9YE;->z:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2433928
    move-object v0, v0

    .line 2433929
    iput-object p2, v0, LX/9YE;->w:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2433930
    move-object v0, v0

    .line 2433931
    iput-boolean p3, v0, LX/9YE;->q:Z

    .line 2433932
    move-object v0, v0

    .line 2433933
    invoke-virtual {v0}, LX/9YE;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v0

    iput-object v0, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433934
    iget-object v0, p0, LX/H94;->e:LX/H8W;

    new-instance v1, LX/HDW;

    invoke-direct {v1, p1, p2, p3}, LX/HDW;-><init>(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    invoke-virtual {v0, v1}, LX/H8W;->a(LX/HDS;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 2433907
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v2, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    if-ne v0, v2, :cond_1

    .line 2433908
    sget v2, LX/H94;->d:I

    .line 2433909
    sget v3, LX/H94;->b:I

    move v7, v4

    .line 2433910
    :goto_0
    new-instance v0, LX/HA7;

    iget-object v5, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v5

    .line 2433911
    sget-object v6, LX/H93;->a:[I

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v8

    aget v6, v6, v8

    packed-switch v6, :pswitch_data_0

    .line 2433912
    const/4 v6, 0x1

    :goto_1
    move v5, v6

    .line 2433913
    const/4 v8, 0x0

    move v6, v4

    invoke-direct/range {v0 .. v8}, LX/HA7;-><init>(IIIIZZZLjava/lang/String;)V

    .line 2433914
    const/4 v1, 0x0

    .line 2433915
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v3, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    if-ne v2, v3, :cond_0

    .line 2433916
    const v1, 0x7f020881

    .line 2433917
    :cond_0
    move v1, v1

    .line 2433918
    iput v1, v0, LX/HA7;->j:I

    .line 2433919
    return-object v0

    .line 2433920
    :cond_1
    sget v2, LX/H94;->c:I

    .line 2433921
    sget v3, LX/H94;->a:I

    move v7, v1

    goto :goto_0

    .line 2433922
    :pswitch_0
    const/4 v6, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433906
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H94;->c:I

    sget v3, LX/H94;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 7

    .prologue
    .line 2433892
    iget-object v0, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq v0, v1, :cond_0

    .line 2433893
    iget-object v0, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    .line 2433894
    iget-object v0, p0, LX/H94;->e:LX/H8W;

    sget-object v2, LX/9XI;->EVENT_TAPPED_FOLLOW:LX/9XI;

    iget-object v3, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433895
    iget-object v0, p0, LX/H94;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HA0;

    iget-object v2, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    .line 2433896
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 2433897
    iget-object v4, v0, LX/HA0;->b:LX/2dj;

    const-string v5, "PROFILE"

    const/4 v6, 0x1

    invoke-virtual {v4, v2, v5, v6}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2433898
    iget-object v5, v0, LX/HA0;->a:LX/0Sh;

    new-instance v6, LX/H9z;

    invoke-direct {v6, v0, v2, v3}, LX/H9z;-><init>(LX/HA0;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v5, v4, v6}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2433899
    move-object v2, v3

    .line 2433900
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    const/4 v4, 0x1

    invoke-static {p0, v0, v3, v4}, LX/H94;->a$redex0(LX/H94;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    .line 2433901
    new-instance v3, LX/H92;

    invoke-direct {v3, p0, v1}, LX/H92;-><init>(LX/H94;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    iget-object v0, p0, LX/H94;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2433902
    :goto_0
    return-void

    .line 2433903
    :cond_0
    sget-object v0, LX/8Dq;->N:Ljava/lang/String;

    iget-object v1, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->t()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->z()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v3

    iget-object v4, p0, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2433904
    iget-object v0, p0, LX/H94;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, LX/H94;->j:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2433905
    iget-object v0, p0, LX/H94;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x278b

    iget-object v1, p0, LX/H94;->j:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v2, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433887
    iget-object v0, p0, LX/H94;->l:LX/0Px;

    if-nez v0, :cond_0

    .line 2433888
    new-instance v0, LX/H90;

    invoke-direct {v0, p0}, LX/H90;-><init>(LX/H94;)V

    .line 2433889
    new-instance v1, LX/H91;

    invoke-direct {v1, p0}, LX/H91;-><init>(LX/H94;)V

    .line 2433890
    invoke-static {v1, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/H94;->l:LX/0Px;

    .line 2433891
    :cond_0
    iget-object v0, p0, LX/H94;->l:LX/0Px;

    return-object v0
.end method
