.class public final LX/G3N;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G3Q;


# direct methods
.method public constructor <init>(LX/G3Q;)V
    .locals 0

    .prologue
    .line 2315413
    iput-object p1, p0, LX/G3N;->a:LX/G3Q;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2315414
    iget-object v0, p0, LX/G3N;->a:LX/G3Q;

    .line 2315415
    iget-object p1, v0, LX/G3Q;->c:LX/G3C;

    invoke-interface {p1}, LX/G3C;->m()V

    .line 2315416
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2315417
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2315418
    iget-object v0, p0, LX/G3N;->a:LX/G3Q;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2315419
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2315420
    check-cast v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    const/4 v1, 0x0

    .line 2315421
    :goto_2
    iget-object v2, v0, LX/G3Q;->b:LX/G3c;

    .line 2315422
    iput-object v1, v2, LX/G3c;->d:Ljava/lang/String;

    .line 2315423
    iget-object v2, v0, LX/G3Q;->e:LX/G3G;

    iget-object v1, v0, LX/G3Q;->b:LX/G3c;

    .line 2315424
    iget-boolean v3, v1, LX/G3c;->j:Z

    move v1, v3

    .line 2315425
    if-eqz v1, :cond_5

    const-string v1, "profile_refresher"

    :goto_3
    iget-object v3, v0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->f()LX/0Px;

    move-result-object v3

    iget-object p0, v0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {p0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object p0

    invoke-virtual {v2, v1, v3, p0}, LX/G3G;->g(Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)V

    .line 2315426
    invoke-static {v0}, LX/G3Q;->q(LX/G3Q;)V

    .line 2315427
    return-void

    .line 2315428
    :cond_0
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2315429
    check-cast v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v1

    iget-object p0, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2315430
    invoke-virtual {p0, v1, v3}, LX/15i;->g(II)I

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_0

    .line 2315431
    :cond_2
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2315432
    check-cast v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v1

    iget-object p0, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2315433
    invoke-virtual {p0, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {p0, v1, v3}, LX/15i;->g(II)I

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_1

    .line 2315434
    :cond_4
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2315435
    check-cast v1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    .line 2315436
    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2315437
    :cond_5
    const-string v1, "profile_nux"

    goto :goto_3
.end method
