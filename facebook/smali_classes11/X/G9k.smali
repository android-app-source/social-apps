.class public final LX/G9k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/G96;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/G9j;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:F

.field private final h:[I

.field private final i:LX/G93;


# direct methods
.method public constructor <init>(LX/G96;IIIIFLX/G93;)V
    .locals 2

    .prologue
    .line 2323266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2323267
    iput-object p1, p0, LX/G9k;->a:LX/G96;

    .line 2323268
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/G9k;->b:Ljava/util/List;

    .line 2323269
    iput p2, p0, LX/G9k;->c:I

    .line 2323270
    iput p3, p0, LX/G9k;->d:I

    .line 2323271
    iput p4, p0, LX/G9k;->e:I

    .line 2323272
    iput p5, p0, LX/G9k;->f:I

    .line 2323273
    iput p6, p0, LX/G9k;->g:F

    .line 2323274
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, LX/G9k;->h:[I

    .line 2323275
    iput-object p7, p0, LX/G9k;->i:LX/G93;

    .line 2323276
    return-void
.end method

.method private a(IIII)F
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/high16 v0, 0x7fc00000    # NaNf

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2323304
    iget-object v2, p0, LX/G9k;->a:LX/G96;

    .line 2323305
    iget v1, v2, LX/G96;->b:I

    move v3, v1

    .line 2323306
    iget-object v4, p0, LX/G9k;->h:[I

    .line 2323307
    aput v6, v4, v6

    .line 2323308
    aput v6, v4, v7

    .line 2323309
    aput v6, v4, v8

    move v1, p1

    .line 2323310
    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_0

    aget v5, v4, v7

    if-gt v5, p3, :cond_0

    .line 2323311
    aget v5, v4, v7

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v7

    .line 2323312
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2323313
    :cond_0
    if-ltz v1, :cond_1

    aget v5, v4, v7

    if-le v5, p3, :cond_2

    .line 2323314
    :cond_1
    :goto_1
    return v0

    .line 2323315
    :cond_2
    :goto_2
    if-ltz v1, :cond_3

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-nez v5, :cond_3

    aget v5, v4, v6

    if-gt v5, p3, :cond_3

    .line 2323316
    aget v5, v4, v6

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v6

    .line 2323317
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 2323318
    :cond_3
    aget v1, v4, v6

    if-gt v1, p3, :cond_1

    .line 2323319
    add-int/lit8 v1, p1, 0x1

    .line 2323320
    :goto_3
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-eqz v5, :cond_4

    aget v5, v4, v7

    if-gt v5, p3, :cond_4

    .line 2323321
    aget v5, v4, v7

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v7

    .line 2323322
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2323323
    :cond_4
    if-eq v1, v3, :cond_1

    aget v5, v4, v7

    if-gt v5, p3, :cond_1

    .line 2323324
    :goto_4
    if-ge v1, v3, :cond_5

    invoke-virtual {v2, p2, v1}, LX/G96;->a(II)Z

    move-result v5

    if-nez v5, :cond_5

    aget v5, v4, v8

    if-gt v5, p3, :cond_5

    .line 2323325
    aget v5, v4, v8

    add-int/lit8 v5, v5, 0x1

    aput v5, v4, v8

    .line 2323326
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2323327
    :cond_5
    aget v2, v4, v8

    if-gt v2, p3, :cond_1

    .line 2323328
    aget v2, v4, v6

    aget v3, v4, v7

    add-int/2addr v2, v3

    aget v3, v4, v8

    add-int/2addr v2, v3

    .line 2323329
    sub-int/2addr v2, p4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    mul-int/lit8 v3, p4, 0x2

    if-ge v2, v3, :cond_1

    .line 2323330
    invoke-static {p0, v4}, LX/G9k;->a(LX/G9k;[I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v4, v1}, LX/G9k;->a([II)F

    move-result v0

    goto :goto_1
.end method

.method private static a([II)F
    .locals 3

    .prologue
    .line 2323277
    const/4 v0, 0x2

    aget v0, p0, v0

    sub-int v0, p1, v0

    int-to-float v0, v0

    const/4 v1, 0x1

    aget v1, p0, v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method private static a(LX/G9k;[III)LX/G9j;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2323278
    aget v0, p1, v5

    aget v1, p1, v4

    add-int/2addr v0, v1

    aget v1, p1, v6

    add-int/2addr v0, v1

    .line 2323279
    invoke-static {p1, p3}, LX/G9k;->a([II)F

    move-result v1

    .line 2323280
    float-to-int v2, v1

    aget v3, p1, v4

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {p0, p2, v2, v3, v0}, LX/G9k;->a(IIII)F

    move-result v2

    .line 2323281
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2323282
    aget v0, p1, v5

    aget v3, p1, v4

    add-int/2addr v0, v3

    aget v3, p1, v6

    add-int/2addr v0, v3

    int-to-float v0, v0

    const/high16 v3, 0x40400000    # 3.0f

    div-float v3, v0, v3

    .line 2323283
    iget-object v0, p0, LX/G9k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9j;

    .line 2323284
    const/4 v5, 0x0

    .line 2323285
    iget v6, v0, LX/G92;->b:F

    move v6, v6

    .line 2323286
    sub-float v6, v2, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v3

    if-gtz v6, :cond_2

    .line 2323287
    iget v6, v0, LX/G92;->a:F

    move v6, v6

    .line 2323288
    sub-float v6, v1, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v3

    if-gtz v6, :cond_2

    .line 2323289
    iget v6, v0, LX/G9j;->a:F

    sub-float v6, v3, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 2323290
    const/high16 p1, 0x3f800000    # 1.0f

    cmpg-float p1, v6, p1

    if-lez p1, :cond_1

    iget p1, v0, LX/G9j;->a:F

    cmpg-float v6, v6, p1

    if-gtz v6, :cond_2

    :cond_1
    const/4 v5, 0x1

    .line 2323291
    :cond_2
    move v5, v5

    .line 2323292
    if-eqz v5, :cond_0

    .line 2323293
    const/high16 p0, 0x40000000    # 2.0f

    .line 2323294
    iget v4, v0, LX/G92;->a:F

    move v4, v4

    .line 2323295
    add-float/2addr v4, v1

    div-float/2addr v4, p0

    .line 2323296
    iget v5, v0, LX/G92;->b:F

    move v5, v5

    .line 2323297
    add-float/2addr v5, v2

    div-float/2addr v5, p0

    .line 2323298
    iget v6, v0, LX/G9j;->a:F

    add-float/2addr v6, v3

    div-float/2addr v6, p0

    .line 2323299
    new-instance p0, LX/G9j;

    invoke-direct {p0, v4, v5, v6}, LX/G9j;-><init>(FFF)V

    move-object v0, p0

    .line 2323300
    :goto_0
    return-object v0

    .line 2323301
    :cond_3
    new-instance v0, LX/G9j;

    invoke-direct {v0, v1, v2, v3}, LX/G9j;-><init>(FFF)V

    .line 2323302
    iget-object v1, p0, LX/G9k;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2323303
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/G9k;[I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2323222
    iget v2, p0, LX/G9k;->g:F

    .line 2323223
    const/high16 v1, 0x40000000    # 2.0f

    div-float v3, v2, v1

    move v1, v0

    .line 2323224
    :goto_0
    const/4 v4, 0x3

    if-ge v1, v4, :cond_1

    .line 2323225
    aget v4, p1, v1

    int-to-float v4, v4

    sub-float v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_0

    .line 2323226
    :goto_1
    return v0

    .line 2323227
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2323228
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/G9j;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2323229
    iget v4, p0, LX/G9k;->c:I

    .line 2323230
    iget v6, p0, LX/G9k;->f:I

    .line 2323231
    iget v0, p0, LX/G9k;->e:I

    add-int v7, v4, v0

    .line 2323232
    iget v0, p0, LX/G9k;->d:I

    div-int/lit8 v3, v6, 0x2

    add-int v8, v0, v3

    .line 2323233
    const/4 v0, 0x3

    new-array v9, v0, [I

    move v5, v2

    .line 2323234
    :goto_0
    if-ge v5, v6, :cond_9

    .line 2323235
    and-int/lit8 v0, v5, 0x1

    if-nez v0, :cond_0

    add-int/lit8 v0, v5, 0x1

    div-int/lit8 v0, v0, 0x2

    :goto_1
    add-int v10, v8, v0

    .line 2323236
    aput v2, v9, v2

    .line 2323237
    aput v2, v9, v1

    .line 2323238
    aput v2, v9, v12

    move v0, v4

    .line 2323239
    :goto_2
    if-ge v0, v7, :cond_1

    iget-object v3, p0, LX/G9k;->a:LX/G96;

    invoke-virtual {v3, v0, v10}, LX/G96;->a(II)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2323240
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2323241
    :cond_0
    add-int/lit8 v0, v5, 0x1

    div-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    goto :goto_1

    :cond_1
    move v3, v0

    move v0, v2

    .line 2323242
    :goto_3
    if-ge v3, v7, :cond_7

    .line 2323243
    iget-object v11, p0, LX/G9k;->a:LX/G96;

    invoke-virtual {v11, v3, v10}, LX/G96;->a(II)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2323244
    if-eq v0, v1, :cond_6

    .line 2323245
    if-ne v0, v12, :cond_4

    .line 2323246
    invoke-static {p0, v9}, LX/G9k;->a(LX/G9k;[I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2323247
    invoke-static {p0, v9, v10, v3}, LX/G9k;->a(LX/G9k;[III)LX/G9j;

    move-result-object v0

    .line 2323248
    if-eqz v0, :cond_3

    .line 2323249
    :cond_2
    :goto_4
    return-object v0

    .line 2323250
    :cond_3
    aget v0, v9, v12

    aput v0, v9, v2

    .line 2323251
    aput v1, v9, v1

    .line 2323252
    aput v2, v9, v12

    move v0, v1

    .line 2323253
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 2323254
    :cond_4
    add-int/lit8 v0, v0, 0x1

    aget v11, v9, v0

    add-int/lit8 v11, v11, 0x1

    aput v11, v9, v0

    goto :goto_5

    .line 2323255
    :cond_5
    if-ne v0, v1, :cond_6

    .line 2323256
    add-int/lit8 v0, v0, 0x1

    .line 2323257
    :cond_6
    aget v11, v9, v0

    add-int/lit8 v11, v11, 0x1

    aput v11, v9, v0

    goto :goto_5

    .line 2323258
    :cond_7
    invoke-static {p0, v9}, LX/G9k;->a(LX/G9k;[I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2323259
    invoke-static {p0, v9, v10, v7}, LX/G9k;->a(LX/G9k;[III)LX/G9j;

    move-result-object v0

    .line 2323260
    if-nez v0, :cond_2

    .line 2323261
    :cond_8
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 2323262
    :cond_9
    iget-object v0, p0, LX/G9k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2323263
    iget-object v0, p0, LX/G9k;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9j;

    goto :goto_4

    .line 2323264
    :cond_a
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2323265
    throw v0
.end method
