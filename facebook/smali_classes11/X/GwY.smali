.class public LX/GwY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile g:LX/GwY;


# instance fields
.field public final b:LX/0aG;

.field public final c:LX/03V;

.field public final d:LX/4hb;

.field public final e:LX/4hp;

.field public final f:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2407114
    const-class v0, LX/GwY;

    sput-object v0, LX/GwY;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/03V;LX/4hb;LX/4hp;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2407115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407116
    iput-object p1, p0, LX/GwY;->b:LX/0aG;

    .line 2407117
    iput-object p2, p0, LX/GwY;->c:LX/03V;

    .line 2407118
    iput-object p3, p0, LX/GwY;->d:LX/4hb;

    .line 2407119
    iput-object p4, p0, LX/GwY;->e:LX/4hp;

    .line 2407120
    iput-object p5, p0, LX/GwY;->f:Ljava/util/concurrent/Executor;

    .line 2407121
    return-void
.end method

.method public static a(LX/0QB;)LX/GwY;
    .locals 9

    .prologue
    .line 2407122
    sget-object v0, LX/GwY;->g:LX/GwY;

    if-nez v0, :cond_1

    .line 2407123
    const-class v1, LX/GwY;

    monitor-enter v1

    .line 2407124
    :try_start_0
    sget-object v0, LX/GwY;->g:LX/GwY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2407125
    if-eqz v2, :cond_0

    .line 2407126
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2407127
    new-instance v3, LX/GwY;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    .line 2407128
    new-instance v8, LX/4hb;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/3N2;->b(LX/0QB;)LX/3N2;

    move-result-object v7

    check-cast v7, LX/3N2;

    invoke-direct {v8, v6, v7}, LX/4hb;-><init>(Landroid/content/Context;LX/3N2;)V

    .line 2407129
    move-object v6, v8

    .line 2407130
    check-cast v6, LX/4hb;

    invoke-static {v0}, LX/4hp;->b(LX/0QB;)LX/4hp;

    move-result-object v7

    check-cast v7, LX/4hp;

    invoke-static {v0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-direct/range {v3 .. v8}, LX/GwY;-><init>(LX/0aG;LX/03V;LX/4hb;LX/4hp;Ljava/util/concurrent/Executor;)V

    .line 2407131
    move-object v0, v3

    .line 2407132
    sput-object v0, LX/GwY;->g:LX/GwY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2407133
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2407134
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2407135
    :cond_1
    sget-object v0, LX/GwY;->g:LX/GwY;

    return-object v0

    .line 2407136
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2407137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
