.class public LX/Gdq;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/AnC;
.implements LX/2eZ;


# instance fields
.field public a:LX/0ja;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1LV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/Gdn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Landroid/view/WindowManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/feed/annotations/IsHscrollReliableSwipingEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/widget/CustomViewPager;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/view/View;

.field public k:Landroid/view/View;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field public n:LX/Gdp;

.field public o:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/2eR;

.field public q:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

.field public r:LX/Gdm;

.field public s:I

.field public t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2374551
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2374552
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Gdq;->o:LX/0am;

    .line 2374553
    const/4 v0, -0x1

    iput v0, p0, LX/Gdq;->s:I

    .line 2374554
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, LX/Gdq;

    invoke-static {p1}, LX/0ja;->a(LX/0QB;)LX/0ja;

    move-result-object v3

    check-cast v3, LX/0ja;

    invoke-static {p1}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v4

    check-cast v4, LX/1LV;

    invoke-static {p1}, LX/Gdn;->a(LX/0QB;)LX/Gdn;

    move-result-object v5

    check-cast v5, LX/Gdn;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p1}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    const/16 v0, 0x316

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v3, v2, LX/Gdq;->a:LX/0ja;

    iput-object v4, v2, LX/Gdq;->b:LX/1LV;

    iput-object v5, v2, LX/Gdq;->c:LX/Gdn;

    iput-object v6, v2, LX/Gdq;->d:LX/03V;

    iput-object v7, v2, LX/Gdq;->e:Landroid/view/WindowManager;

    iput-object p1, v2, LX/Gdq;->f:LX/0Or;

    .line 2374555
    iget-object v0, p0, LX/Gdq;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v1

    .line 2374556
    const v0, 0x7f030891

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2374557
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Gdq;->setOrientation(I)V

    .line 2374558
    const v0, 0x7f020aee

    invoke-virtual {p0, v0}, LX/Gdq;->setBackgroundResource(I)V

    .line 2374559
    const v0, 0x7f0d1637

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    .line 2374560
    const v0, 0x7f0d15bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Gdq;->h:Landroid/widget/TextView;

    .line 2374561
    const v0, 0x7f0d163d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Gdq;->i:Landroid/widget/TextView;

    .line 2374562
    const v0, 0x7f0d163a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Gdq;->j:Landroid/view/View;

    .line 2374563
    const v0, 0x7f0d163b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Gdq;->k:Landroid/view/View;

    .line 2374564
    const v0, 0x7f0d163c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Gdq;->l:Landroid/view/View;

    .line 2374565
    const v0, 0x7f0d163e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Gdq;->m:Landroid/view/View;

    .line 2374566
    iget-object v0, p0, LX/Gdq;->h:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2374567
    new-instance v0, LX/Gdp;

    iget-object v2, p0, LX/Gdq;->a:LX/0ja;

    invoke-direct {v0, p0, v2, p0}, LX/Gdp;-><init>(LX/Gdq;LX/0ja;LX/AnC;)V

    iput-object v0, p0, LX/Gdq;->n:LX/Gdp;

    .line 2374568
    iget-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    iget-object v2, p0, LX/Gdq;->n:LX/Gdp;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2374569
    iget-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    new-instance v2, LX/Gdo;

    invoke-direct {v2, p0}, LX/Gdo;-><init>(LX/Gdq;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2374570
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    instance-of v0, v0, Lcom/facebook/widget/ListViewFriendlyViewPager;

    if-eqz v0, :cond_0

    .line 2374571
    iget-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    check-cast v0, Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->g()V

    .line 2374572
    :cond_0
    return-void
.end method

.method public static a(LX/Gdq;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2374588
    iget-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewPager;->setVisibility(I)V

    .line 2374589
    iget-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    iget-object v1, p0, LX/Gdq;->n:LX/Gdp;

    if-ne v0, v1, :cond_0

    .line 2374590
    iget-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2374591
    :cond_0
    iget-object v0, p0, LX/Gdq;->r:LX/Gdm;

    invoke-virtual {v0}, LX/2sw;->a()LX/2eR;

    move-result-object v0

    iget-object v1, p0, LX/Gdq;->p:LX/2eR;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/Gdq;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2374592
    iget-object v1, p0, LX/Gdq;->a:LX/0ja;

    iget-object v0, p0, LX/Gdq;->p:LX/2eR;

    invoke-interface {v0}, LX/2eR;->a()Ljava/lang/Class;

    move-result-object v2

    iget-object v0, p0, LX/Gdq;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1, v2, v0, p0}, LX/0ja;->a(Ljava/lang/Class;Landroid/view/View;LX/0h1;)V

    .line 2374593
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Gdq;->o:LX/0am;

    .line 2374594
    iput-object v4, p0, LX/Gdq;->p:LX/2eR;

    .line 2374595
    :cond_1
    iget-object v0, p0, LX/Gdq;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2374596
    iget-object v0, p0, LX/Gdq;->n:LX/Gdp;

    invoke-virtual {v0, p0, v3}, LX/6ly;->b(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Gdq;->o:LX/0am;

    .line 2374597
    iget-object v0, p0, LX/Gdq;->r:LX/Gdm;

    invoke-virtual {v0}, LX/2sw;->a()LX/2eR;

    move-result-object v0

    iput-object v0, p0, LX/Gdq;->p:LX/2eR;

    .line 2374598
    iget-object v0, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {p0, v0}, LX/Gdq;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 2374599
    iget-object v0, p0, LX/Gdq;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0}, LX/Gdq;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/Gdq;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2374600
    :cond_2
    iget-object v0, p0, LX/Gdq;->o:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 2374601
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2374602
    iget-object v0, p0, LX/Gdq;->r:LX/Gdm;

    sget-object v4, LX/99X;->FIRST:LX/99X;

    move-object v1, p1

    move-object v3, p2

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, LX/Gdm;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;)V

    .line 2374603
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Ljava/lang/Object;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 3

    .prologue
    .line 2374583
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2374584
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2374585
    iget-object v0, p0, LX/Gdq;->r:LX/Gdm;

    iget-object v1, p0, LX/Gdq;->n:LX/Gdp;

    iget-object v2, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    .line 2374586
    invoke-virtual {v0, v1, v2}, LX/Gdm;->a(LX/Gdp;Landroid/support/v4/view/ViewPager;)V

    .line 2374587
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2374604
    iget-boolean v0, p0, LX/Gdq;->t:Z

    return v0
.end method

.method public final lY_()V
    .locals 3

    .prologue
    .line 2374581
    iget-object v0, p0, LX/Gdq;->r:LX/Gdm;

    iget-object v1, p0, LX/Gdq;->n:LX/Gdp;

    iget-object v2, p0, LX/Gdq;->g:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v1, v2}, LX/Gdm;->a(LX/Gdp;Landroid/support/v4/view/ViewPager;)V

    .line 2374582
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x674c5305

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2374577
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2374578
    const/4 v1, 0x1

    .line 2374579
    iput-boolean v1, p0, LX/Gdq;->t:Z

    .line 2374580
    const/16 v1, 0x2d

    const v2, -0x4f7dfdb6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1e075215

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2374573
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2374574
    const/4 v1, 0x0

    .line 2374575
    iput-boolean v1, p0, LX/Gdq;->t:Z

    .line 2374576
    const/16 v1, 0x2d

    const v2, 0x6332280

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
