.class public LX/HCF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2438358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I
    .locals 1
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    .line 2438359
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_0

    .line 2438360
    const v0, 0x7f02073f

    .line 2438361
    :goto_0
    return v0

    .line 2438362
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ACTIVITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_1

    .line 2438363
    const v0, 0x7f0208b2

    goto :goto_0

    .line 2438364
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_BOOK_PREVIEW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_2

    .line 2438365
    const v0, 0x7f02077f

    goto :goto_0

    .line 2438366
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_3

    .line 2438367
    const v0, 0x7f02088f

    goto :goto_0

    .line 2438368
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_CUSTOM:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_4

    .line 2438369
    const v0, 0x7f0208f6

    goto :goto_0

    .line 2438370
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_EVENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_5

    .line 2438371
    const v0, 0x7f02085b

    goto :goto_0

    .line 2438372
    :cond_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_JOBS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_6

    .line 2438373
    const v0, 0x7f02078b

    goto :goto_0

    .line 2438374
    :cond_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_GROUPS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_7

    .line 2438375
    const v0, 0x7f0208be

    goto :goto_0

    .line 2438376
    :cond_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_8

    .line 2438377
    const v0, 0x7f0208d3

    goto :goto_0

    .line 2438378
    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LIKES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_9

    .line 2438379
    const v0, 0x7f0208fa

    goto :goto_0

    .line 2438380
    :cond_9
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_a

    .line 2438381
    const v0, 0x7f020964

    goto :goto_0

    .line 2438382
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_MENU:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_b

    .line 2438383
    const v0, 0x7f020884

    goto :goto_0

    .line 2438384
    :cond_b
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_MUSIC:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_c

    .line 2438385
    const v0, 0x7f020938

    goto :goto_0

    .line 2438386
    :cond_c
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_NOTES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_d

    .line 2438387
    const v0, 0x7f020945

    goto :goto_0

    .line 2438388
    :cond_d
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_OFFERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_e

    .line 2438389
    const v0, 0x7f020816

    goto :goto_0

    .line 2438390
    :cond_e
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_PHOTOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_f

    .line 2438391
    const v0, 0x7f02095e

    goto :goto_0

    .line 2438392
    :cond_f
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_POSTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_10

    .line 2438393
    const v0, 0x7f020984

    goto :goto_0

    .line 2438394
    :cond_10
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_REVIEWS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_11

    .line 2438395
    const v0, 0x7f0209e1

    goto/16 :goto_0

    .line 2438396
    :cond_11
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_12

    .line 2438397
    const v0, 0x7f020902

    goto/16 :goto_0

    .line 2438398
    :cond_12
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_13

    .line 2438399
    const v0, 0x7f0209c9

    goto/16 :goto_0

    .line 2438400
    :cond_13
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, v0, :cond_14

    .line 2438401
    const v0, 0x7f020a20

    goto/16 :goto_0

    .line 2438402
    :cond_14
    const v0, 0x7f02099d

    goto/16 :goto_0
.end method
