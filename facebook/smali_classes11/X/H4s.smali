.class public final LX/H4s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V
    .locals 0

    .prologue
    .line 2423469
    iput-object p1, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2423470
    iget-object v0, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v0, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x35000d

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2423471
    iget-object v0, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v0, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    sget-object v1, LX/2kx;->FAILED:LX/2kx;

    invoke-virtual {v0, v1}, LX/2kw;->a(LX/2kx;)V

    .line 2423472
    iget-object v0, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    const/4 v1, 0x0

    .line 2423473
    iput-boolean v1, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ak:Z

    .line 2423474
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2423475
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2423476
    iget-object v0, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    const/4 v1, 0x0

    .line 2423477
    iput-boolean v1, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ak:Z

    .line 2423478
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    .line 2423479
    const/4 v1, 0x2

    .line 2423480
    if-eqz v0, :cond_3

    .line 2423481
    iget-object v1, v0, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    .line 2423482
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/3T7;->b()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2423483
    iget-object v2, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v2, v2, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    sget-object v3, LX/2kx;->FINISHED:LX/2kx;

    invoke-virtual {v2, v3}, LX/2kw;->a(LX/2kx;)V

    .line 2423484
    :cond_0
    iget-object v2, v0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v2

    .line 2423485
    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    if-ne v0, v2, :cond_2

    .line 2423486
    iget-object v0, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2423487
    iget-object v0, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getLoaderManager()LX/0k5;

    move-result-object v0

    .line 2423488
    if-eqz v0, :cond_1

    .line 2423489
    const/16 v2, 0x64

    invoke-virtual {v0, v2}, LX/0k5;->b(I)LX/0k9;

    move-result-object v0

    .line 2423490
    check-cast v0, Lcom/facebook/notifications/loader/NotificationsLoader;

    invoke-virtual {v1}, LX/3T7;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 2423491
    iput v2, v0, Lcom/facebook/notifications/loader/NotificationsLoader;->x:I

    .line 2423492
    :cond_1
    iget-object v0, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-virtual {v1}, LX/3T7;->a()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->b(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Ljava/util/List;)V

    .line 2423493
    const/16 v0, 0x1a

    .line 2423494
    :goto_0
    iget-object v1, p0, LX/H4s;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v1, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x35000d

    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2423495
    return-void

    .line 2423496
    :cond_2
    const/16 v0, 0x1b

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
