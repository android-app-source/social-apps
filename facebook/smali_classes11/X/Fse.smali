.class public LX/Fse;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/FsI;

.field private final b:LX/0tX;

.field private final c:LX/Fsg;

.field private final d:LX/FsX;

.field private final e:LX/G2W;

.field private final f:LX/2dk;

.field public final g:Ljava/lang/String;

.field private final h:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0tX;LX/FsI;LX/Fsg;LX/FsX;LX/G2W;LX/2dk;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297330
    iput-object p3, p0, LX/Fse;->a:LX/FsI;

    .line 2297331
    iput-object p2, p0, LX/Fse;->b:LX/0tX;

    .line 2297332
    iput-object p4, p0, LX/Fse;->c:LX/Fsg;

    .line 2297333
    iput-object p5, p0, LX/Fse;->d:LX/FsX;

    .line 2297334
    iput-object p6, p0, LX/Fse;->e:LX/G2W;

    .line 2297335
    iput-object p7, p0, LX/Fse;->f:LX/2dk;

    .line 2297336
    const v0, 0x7f0815b9

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fse;->g:Ljava/lang/String;

    .line 2297337
    const v0, 0x7f0b0f69

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Fse;->h:I

    .line 2297338
    return-void
.end method

.method private static a(LX/Fse;LX/0zO;)LX/0zX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 2297269
    iget-object v0, p0, LX/Fse;->b:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/FsA;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0zX;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G12;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297322
    iget-object v0, p0, LX/Fse;->a:LX/FsI;

    invoke-virtual {v0, p1}, LX/FsI;->a(LX/G12;)LX/5xH;

    move-result-object v0

    .line 2297323
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297324
    iput p3, v0, LX/0zO;->B:I

    .line 2297325
    move-object v0, v0

    .line 2297326
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297327
    move-object v0, v0

    .line 2297328
    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0v6;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 6
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "LX/3Fw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297316
    iget-object v0, p0, LX/Fse;->f:LX/2dk;

    const/4 v1, 0x0

    const/16 v2, 0x14

    iget v3, p0, LX/Fse;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, LX/2hC;->SELF_PROFILE:LX/2hC;

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/2dk;->b(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297317
    iput p2, v0, LX/0zO;->B:I

    .line 2297318
    move-object v0, v0

    .line 2297319
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297320
    :goto_0
    invoke-static {v0}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/Fsd;

    invoke-direct {v1, p0}, LX/Fsd;-><init>(LX/Fse;)V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0

    .line 2297321
    :cond_0
    invoke-static {p0, v0}, LX/Fse;->a(LX/Fse;LX/0zO;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0v6;LX/0zO;)LX/0zX;
    .locals 2
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;",
            ">;)",
            "LX/0zX",
            "<",
            "LX/FsM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297313
    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297314
    :goto_0
    invoke-static {v0}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/FsY;

    invoke-direct {v1, p0}, LX/FsY;-><init>(LX/Fse;)V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0

    .line 2297315
    :cond_0
    invoke-static {p0, p2}, LX/Fse;->a(LX/Fse;LX/0zO;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0v6;LX/0zO;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 6
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;",
            ">;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297311
    invoke-static {p2}, LX/Fsq;->a(LX/0zO;)LX/Fso;

    move-result-object v2

    .line 2297312
    iget-object v0, p0, LX/Fse;->d:LX/FsX;

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/FsX;->a(LX/0v6;LX/Fso;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0v6;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 2
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "LX/G1P;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297301
    new-instance v0, LX/5wC;

    invoke-direct {v0}, LX/5wC;-><init>()V

    move-object v0, v0

    .line 2297302
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297303
    iput p3, v0, LX/0zO;->B:I

    .line 2297304
    move-object v0, v0

    .line 2297305
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297306
    move-object v0, v0

    .line 2297307
    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2297308
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297309
    :goto_0
    invoke-static {v0}, LX/FsD;->a(LX/0zX;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/Fsa;

    invoke-direct {v1, p0}, LX/Fsa;-><init>(LX/Fse;)V

    invoke-virtual {v0, v1}, LX/0zX;->b(LX/0QK;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/FsZ;

    invoke-direct {v1, p0}, LX/FsZ;-><init>(LX/Fse;)V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0

    .line 2297310
    :cond_0
    invoke-static {p0, v0}, LX/Fse;->a(LX/Fse;LX/0zO;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0v6;LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 5
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/G12;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "LX/G1P;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297289
    iget-wide v2, p2, LX/G12;->a:J

    move-wide v0, v2

    .line 2297290
    new-instance v2, LX/5x6;

    invoke-direct {v2}, LX/5x6;-><init>()V

    move-object v2, v2

    .line 2297291
    const-string v3, "node_id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/5x6;

    move-object v0, v2

    .line 2297292
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297293
    iput p4, v0, LX/0zO;->B:I

    .line 2297294
    move-object v0, v0

    .line 2297295
    iput-object p5, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297296
    move-object v0, v0

    .line 2297297
    invoke-virtual {v0, p3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2297298
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297299
    :goto_0
    invoke-static {v0}, LX/FsD;->a(LX/0zX;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/Fsb;

    invoke-direct {v1, p0}, LX/Fsb;-><init>(LX/Fse;)V

    invoke-virtual {v0, v1}, LX/0zX;->b(LX/0QK;)LX/0zX;

    move-result-object v0

    sget-object v1, LX/G1P;->a:LX/0QK;

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0

    .line 2297300
    :cond_0
    invoke-static {p0, v0}, LX/Fse;->a(LX/Fse;LX/0zO;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G12;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297284
    iget-object v0, p0, LX/Fse;->c:LX/Fsg;

    new-instance v1, LX/Fsp;

    .line 2297285
    iget-wide v6, p1, LX/G12;->a:J

    move-wide v2, v6

    .line 2297286
    const/4 v4, 0x0

    .line 2297287
    iget-boolean v5, p1, LX/G12;->c:Z

    move v5, v5

    .line 2297288
    invoke-direct {v1, v2, v3, v4, v5}, LX/Fsp;-><init>(JLjava/lang/String;Z)V

    const/4 v5, 0x4

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/Fsg;->a(LX/Fsp;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0v6;LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 6
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/G12;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "LX/FsK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297270
    iget-object v0, p0, LX/Fse;->e:LX/G2W;

    .line 2297271
    iget-wide v4, p2, LX/G12;->a:J

    move-wide v2, v4

    .line 2297272
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2297273
    const-string v2, "FRIENDS"

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2297274
    invoke-virtual {v0, v1, v2}, LX/G2W;->a(Ljava/lang/String;Ljava/util/List;)LX/G1Q;

    move-result-object v2

    move-object v0, v2

    .line 2297275
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297276
    iput p4, v0, LX/0zO;->B:I

    .line 2297277
    move-object v0, v0

    .line 2297278
    iput-object p5, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297279
    move-object v0, v0

    .line 2297280
    invoke-virtual {v0, p3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2297281
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297282
    :goto_0
    invoke-static {v0}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/Fsc;

    invoke-direct {v1, p0}, LX/Fsc;-><init>(LX/Fse;)V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0

    .line 2297283
    :cond_0
    invoke-static {p0, v0}, LX/Fse;->a(LX/Fse;LX/0zO;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method
