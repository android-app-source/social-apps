.class public final LX/Fot;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/Fow;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Foy;


# direct methods
.method public constructor <init>(LX/Foy;)V
    .locals 0

    .prologue
    .line 2290412
    iput-object p1, p0, LX/Fot;->a:LX/Foy;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2290413
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2290414
    check-cast p1, LX/Fow;

    .line 2290415
    if-eqz p1, :cond_0

    .line 2290416
    iget-object v0, p0, LX/Fot;->a:LX/Foy;

    iget-object v0, v0, LX/Foy;->e:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    .line 2290417
    iget-object v1, p1, LX/Fow;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2290418
    if-eqz v1, :cond_1

    .line 2290419
    iget-object v1, p1, LX/Fow;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2290420
    iget-object v2, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v2}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2290421
    :cond_0
    :goto_0
    return-void

    .line 2290422
    :cond_1
    iget-object v1, p1, LX/Fow;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2290423
    iput-object v1, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    .line 2290424
    iget-object v1, p1, LX/Fow;->b:Ljava/util/ArrayList;

    move-object v1, v1

    .line 2290425
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2290426
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2290427
    iget-object v5, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->k:Ljava/util/ArrayList;

    new-instance v6, LX/Fon;

    sget-object p0, LX/Foo;->CHARITY:LX/Foo;

    invoke-direct {v6, p0, v2}, LX/Fon;-><init>(LX/Foo;Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2290428
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2290429
    :cond_3
    iget-object v1, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    iget-object v2, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->k:Ljava/util/ArrayList;

    .line 2290430
    iget-boolean v3, p1, LX/Fow;->c:Z

    move v3, v3

    .line 2290431
    invoke-virtual {v1, v2, v3}, LX/Foq;->a(Ljava/util/ArrayList;Z)V

    goto :goto_0
.end method
