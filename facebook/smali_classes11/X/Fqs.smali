.class public LX/Fqs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Fqs;


# instance fields
.field private final a:LX/Fqv;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/2G3;

.field private final d:LX/1mR;

.field private final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/Fqv;Ljava/util/concurrent/ExecutorService;LX/2G3;LX/1mR;LX/0ad;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294610
    iput-object p1, p0, LX/Fqs;->a:LX/Fqv;

    .line 2294611
    iput-object p2, p0, LX/Fqs;->b:Ljava/util/concurrent/ExecutorService;

    .line 2294612
    iput-object p3, p0, LX/Fqs;->c:LX/2G3;

    .line 2294613
    iput-object p4, p0, LX/Fqs;->d:LX/1mR;

    .line 2294614
    iput-object p5, p0, LX/Fqs;->e:LX/0ad;

    .line 2294615
    return-void
.end method

.method public static a(LX/0QB;)LX/Fqs;
    .locals 9

    .prologue
    .line 2294623
    sget-object v0, LX/Fqs;->f:LX/Fqs;

    if-nez v0, :cond_1

    .line 2294624
    const-class v1, LX/Fqs;

    monitor-enter v1

    .line 2294625
    :try_start_0
    sget-object v0, LX/Fqs;->f:LX/Fqs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2294626
    if-eqz v2, :cond_0

    .line 2294627
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2294628
    new-instance v3, LX/Fqs;

    invoke-static {v0}, LX/Fqv;->a(LX/0QB;)LX/Fqv;

    move-result-object v4

    check-cast v4, LX/Fqv;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/2G3;->a(LX/0QB;)LX/2G3;

    move-result-object v6

    check-cast v6, LX/2G3;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v7

    check-cast v7, LX/1mR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/Fqs;-><init>(LX/Fqv;Ljava/util/concurrent/ExecutorService;LX/2G3;LX/1mR;LX/0ad;)V

    .line 2294629
    move-object v0, v3

    .line 2294630
    sput-object v0, LX/Fqs;->f:LX/Fqs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2294631
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2294632
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2294633
    :cond_1
    sget-object v0, LX/Fqs;->f:LX/Fqs;

    return-object v0

    .line 2294634
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2294635
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2294636
    iget-object v0, p0, LX/Fqs;->a:LX/Fqv;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 2294637
    iget-object v0, p0, LX/Fqs;->d:LX/1mR;

    const-string v1, "timeline_fetch_info_review"

    const-string v2, "timeline_fetch_header"

    const-string v3, "timeline_fetch_first_units_classic"

    const-string v4, "timeline_fetch_first_units_plutonium"

    const-string v5, "timeline_fetch_section"

    invoke-static {v1, v2, v3, v4, v5}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2294638
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2294620
    iget-object v0, p0, LX/Fqs;->c:LX/2G3;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, p1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 2294621
    iget-object v0, p0, LX/Fqs;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/timeline/cache/TimelineUserDataCleaner$1;

    invoke-direct {v1, p0}, Lcom/facebook/timeline/cache/TimelineUserDataCleaner$1;-><init>(LX/Fqs;)V

    const v2, 0x7e3040c2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2294622
    return-void
.end method

.method public final clearUserData()V
    .locals 4

    .prologue
    .line 2294616
    iget-object v0, p0, LX/Fqs;->c:LX/2G3;

    const-string v1, "TimelineDBEvictionLogout"

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/0Vx;->a(Ljava/lang/String;J)V

    .line 2294617
    iget-object v0, p0, LX/Fqs;->e:LX/0ad;

    sget-short v1, LX/0wf;->ae:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2294618
    invoke-virtual {p0}, LX/Fqs;->a()V

    .line 2294619
    :cond_0
    return-void
.end method
