.class public LX/F6t;
.super LX/4ok;
.source ""


# instance fields
.field private final a:LX/0dN;

.field public final b:LX/30Y;

.field private final c:LX/17W;

.field public final d:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

.field public final e:LX/1Ck;

.field public final f:LX/0kL;


# direct methods
.method public constructor <init>(LX/30Y;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17W;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/1Ck;LX/0kL;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200923
    invoke-direct {p0, p2}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2200924
    new-instance v0, LX/F6p;

    invoke-direct {v0, p0}, LX/F6p;-><init>(LX/F6t;)V

    iput-object v0, p0, LX/F6t;->a:LX/0dN;

    .line 2200925
    iput-object p1, p0, LX/F6t;->b:LX/30Y;

    .line 2200926
    iput-object p4, p0, LX/F6t;->c:LX/17W;

    .line 2200927
    iput-object p5, p0, LX/F6t;->d:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    .line 2200928
    iput-object p6, p0, LX/F6t;->e:LX/1Ck;

    .line 2200929
    iput-object p7, p0, LX/F6t;->f:LX/0kL;

    .line 2200930
    iget-object v0, p0, LX/F6t;->d:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    .line 2200931
    iget-object v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->l:LX/0Tn;

    move-object v0, v1

    .line 2200932
    invoke-virtual {p0, v0}, LX/4oi;->a(LX/0Tn;)V

    .line 2200933
    const v1, 0x7f0833b6

    invoke-virtual {p0, v1}, LX/F6t;->setTitle(I)V

    .line 2200934
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/F6t;->setDefaultValue(Ljava/lang/Object;)V

    .line 2200935
    iget-object v1, p0, LX/F6t;->a:LX/0dN;

    invoke-interface {p3, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 2200936
    return-void
.end method

.method public static a(LX/0QB;)LX/F6t;
    .locals 9

    .prologue
    .line 2200937
    new-instance v1, LX/F6t;

    invoke-static {p0}, LX/30Y;->b(LX/0QB;)LX/30Y;

    move-result-object v2

    check-cast v2, LX/30Y;

    const-class v3, Landroid/content/Context;

    invoke-interface {p0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v6

    check-cast v6, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v8

    check-cast v8, LX/0kL;

    invoke-direct/range {v1 .. v8}, LX/F6t;-><init>(LX/30Y;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17W;Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/1Ck;LX/0kL;)V

    .line 2200938
    move-object v0, v1

    .line 2200939
    return-object v0
.end method


# virtual methods
.method public onClick()V
    .locals 6

    .prologue
    .line 2200940
    invoke-virtual {p0}, LX/F6t;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2200941
    iget-object v0, p0, LX/F6t;->c:LX/17W;

    invoke-virtual {p0}, LX/F6t;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->fe:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, LX/89v;->CONTINUOUS_SYNC:LX/89v;

    iget-object v5, v5, LX/89v;->value:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2200942
    :goto_0
    return-void

    .line 2200943
    :cond_0
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, LX/F6t;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e024b

    invoke-direct {v0, v1, v2}, LX/0ju;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0833b9

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f0833ba

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/F6s;

    invoke-direct {v2, p0}, LX/F6s;-><init>(LX/F6t;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0833bd

    new-instance v2, LX/F6r;

    invoke-direct {v2, p0}, LX/F6r;-><init>(LX/F6t;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2200944
    goto :goto_0
.end method
