.class public LX/FIC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;
.implements LX/4B8;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final h:Ljava/lang/Object;


# instance fields
.field public a:Lcom/facebook/ui/media/attachments/MediaResourceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6ef;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FGW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FIB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/FIF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Landroid/net/Uri;",
            "Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2221701
    const-class v0, LX/FIC;

    sput-object v0, LX/FIC;->f:Ljava/lang/Class;

    .line 2221702
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FIC;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221577
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/FIC;->g:LX/0QI;

    .line 2221578
    return-void
.end method

.method public static a(LX/0QB;)LX/FIC;
    .locals 11

    .prologue
    .line 2221579
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2221580
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2221581
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2221582
    if-nez v1, :cond_0

    .line 2221583
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2221584
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2221585
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2221586
    sget-object v1, LX/FIC;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2221587
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2221588
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2221589
    :cond_1
    if-nez v1, :cond_4

    .line 2221590
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2221591
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2221592
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2221593
    new-instance v1, LX/FIC;

    invoke-direct {v1}, LX/FIC;-><init>()V

    .line 2221594
    invoke-static {v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v7

    check-cast v7, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static {v0}, LX/6ef;->a(LX/0QB;)LX/6ef;

    move-result-object v8

    check-cast v8, LX/6ef;

    invoke-static {v0}, LX/FGW;->b(LX/0QB;)LX/FGW;

    move-result-object v9

    check-cast v9, LX/FGW;

    .line 2221595
    new-instance p0, LX/FIB;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-direct {p0, v10}, LX/FIB;-><init>(LX/0Zb;)V

    .line 2221596
    move-object v10, p0

    .line 2221597
    check-cast v10, LX/FIB;

    const-class p0, LX/FIF;

    invoke-interface {v0, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/FIF;

    .line 2221598
    iput-object v7, v1, LX/FIC;->a:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    iput-object v8, v1, LX/FIC;->b:LX/6ef;

    iput-object v9, v1, LX/FIC;->c:LX/FGW;

    iput-object v10, v1, LX/FIC;->d:LX/FIB;

    iput-object p0, v1, LX/FIC;->e:LX/FIF;

    .line 2221599
    move-object v1, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2221600
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2221601
    if-nez v1, :cond_2

    .line 2221602
    sget-object v0, LX/FIC;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIC;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2221603
    :goto_1
    if-eqz v0, :cond_3

    .line 2221604
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2221605
    :goto_3
    check-cast v0, LX/FIC;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2221606
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2221607
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2221608
    :catchall_1
    move-exception v0

    .line 2221609
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2221610
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2221611
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2221612
    :cond_2
    :try_start_8
    sget-object v0, LX/FIC;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIC;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 2221613
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 2221614
    const-string v0, "mediaResource"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2221615
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    .line 2221616
    const-string v1, "media_source"

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v4}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221617
    iget-object v1, p0, LX/FIC;->g:LX/0QI;

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-interface {v1, v4}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    .line 2221618
    if-nez v1, :cond_4

    .line 2221619
    invoke-static {v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2221620
    iget-object v1, p0, LX/FIC;->a:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2221621
    :cond_0
    iget-object v1, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v4, LX/2MK;->VIDEO:LX/2MK;

    if-eq v1, v4, :cond_1

    .line 2221622
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v1, "MediaResource is not a video."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2221623
    :goto_0
    return-object v0

    .line 2221624
    :cond_1
    const-string v1, "isOutOfSpace"

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 2221625
    iget-wide v6, v0, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    iget-object v8, p0, LX/FIC;->c:LX/FGW;

    invoke-virtual {v8}, LX/FGW;->a()I

    move-result v8

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-lez v6, :cond_7

    const/4 v6, 0x1

    :goto_1
    move v2, v6

    .line 2221626
    invoke-static {v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v4

    .line 2221627
    if-nez v4, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    if-eqz v1, :cond_3

    .line 2221628
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v1, "Not enough disk space to create new trimmed video."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2221629
    :cond_3
    iget-object v1, p0, LX/FIC;->e:LX/FIF;

    .line 2221630
    new-instance v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;

    invoke-direct {v2, v0}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2221631
    invoke-static {v1}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {v1}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v5

    check-cast v5, LX/2MM;

    .line 2221632
    new-instance v8, LX/6bH;

    invoke-static {v1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    const/16 v7, 0x2641

    invoke-static {v1, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const-class v7, LX/6b8;

    invoke-interface {v1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/6b8;

    invoke-direct {v8, v6, v9, v7}, LX/6bH;-><init>(LX/0TD;LX/0Or;LX/6b8;)V

    .line 2221633
    move-object v6, v8

    .line 2221634
    check-cast v6, LX/6bH;

    invoke-static {v1}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v1}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-static {v1}, LX/FIA;->a(LX/0QB;)LX/FIA;

    move-result-object v9

    check-cast v9, LX/FIA;

    invoke-static {v1}, LX/FI8;->a(LX/0QB;)LX/FI8;

    move-result-object v10

    check-cast v10, LX/FI8;

    invoke-static {v1}, LX/FI5;->a(LX/0QB;)LX/FI5;

    move-result-object v11

    check-cast v11, LX/FI5;

    invoke-static {v1}, LX/FI3;->a(LX/0QB;)LX/FI3;

    move-result-object v12

    check-cast v12, LX/FI3;

    invoke-static {v1}, LX/2Me;->b(LX/0QB;)LX/2Me;

    move-result-object p1

    check-cast p1, LX/2Me;

    .line 2221635
    iput-object v4, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a:LX/18V;

    iput-object v5, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->b:LX/2MM;

    iput-object v6, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->c:LX/6bH;

    iput-object v7, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->d:Ljava/util/concurrent/ExecutorService;

    iput-object v8, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e:LX/0Xl;

    iput-object v9, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->f:LX/FIA;

    iput-object v10, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->g:LX/FI8;

    iput-object v11, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->h:LX/FI5;

    iput-object v12, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->i:LX/FI3;

    iput-object p1, v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->j:LX/2Me;

    .line 2221636
    move-object v1, v2

    .line 2221637
    iget-object v2, p0, LX/FIC;->g:LX/0QI;

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-interface {v2, v4, v1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_4
    move-object v2, v0

    .line 2221638
    :try_start_0
    invoke-virtual {v1}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a()Ljava/lang/String;

    move-result-object v0

    .line 2221639
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2221640
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v4, "Empty fbid returned"

    invoke-static {v0, v4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2221641
    invoke-static {p0, v1, v3}, LX/FIC;->a(LX/FIC;Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 2221642
    :cond_5
    :try_start_1
    iget-object v4, p0, LX/FIC;->g:LX/0QI;

    iget-object v5, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-interface {v4, v5}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 2221643
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2221644
    invoke-static {p0, v1, v3}, LX/FIC;->a(LX/FIC;Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;Ljava/util/Map;)V

    goto/16 :goto_0

    .line 2221645
    :catch_0
    move-exception v0

    .line 2221646
    :try_start_2
    iget-object v4, p0, LX/FIC;->g:LX/0QI;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-interface {v4, v2}, LX/0QI;->b(Ljava/lang/Object;)V

    .line 2221647
    const-string v2, "failure_exception_message"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221648
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2221649
    iget-object v0, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221650
    iget-boolean v0, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->q:Z

    if-nez v0, :cond_6

    iget-boolean v0, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->p:Z

    if-eqz v0, :cond_8

    .line 2221651
    :cond_6
    :goto_2
    sget-object v0, LX/1nY;->SEGMENTED_TRANSCODE_ERROR:LX/1nY;

    const-string v2, "Segmented transcode upload is failed."

    invoke-static {v0, v2}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2221652
    invoke-static {p0, v1, v3}, LX/FIC;->a(LX/FIC;Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;Ljava/util/Map;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p0, v1, v3}, LX/FIC;->a(LX/FIC;Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;Ljava/util/Map;)V

    throw v0

    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 2221653
    :cond_8
    :try_start_3
    const/4 v0, 0x0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iput-boolean v0, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->p:Z

    .line 2221654
    iget-object v0, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->a:LX/18V;

    iget-object v2, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->i:LX/FI3;

    new-instance v4, LX/FI2;

    iget-object v5, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    iget-object v6, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, LX/FI2;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x0

    sget-object v6, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v4, v5, v6}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2221655
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->q:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 2221656
    :catch_1
    move-exception v0

    .line 2221657
    sget-object v2, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->k:Ljava/lang/Class;

    const-string v4, "Cancel upload failed with sessionId %s and streamId %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->n:Ljava/lang/String;

    aput-object v6, v5, v7

    iget-object v6, v1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-static {v2, v0, v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private static a(LX/FIC;Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2221658
    const-string v0, "media_source"

    .line 2221659
    iget-object v1, p1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->l:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v1, v1

    .line 2221660
    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v1}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221661
    const-string v0, "session_id"

    invoke-virtual {p1}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221662
    const-string v0, "stream _id"

    .line 2221663
    iget-object v1, p1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->o:Ljava/lang/String;

    move-object v1, v1

    .line 2221664
    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221665
    invoke-virtual {p1}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2221666
    const-string v0, "segment_partition_status"

    const-string v1, "1"

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221667
    const-string v0, "segment_count"

    invoke-virtual {p1}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221668
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2221669
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2221670
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2221671
    invoke-virtual {p1}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    .line 2221672
    iget-object v4, v0, LX/FIE;->b:LX/6bE;

    move-object v0, v4

    .line 2221673
    if-eqz v0, :cond_1

    .line 2221674
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2221675
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2221676
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2221677
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIE;

    .line 2221678
    iget-boolean v4, v0, LX/FIE;->c:Z

    move v0, v4

    .line 2221679
    if-eqz v0, :cond_3

    .line 2221680
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_2

    .line 2221681
    const-string v0, ", "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2221682
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2221683
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2221684
    :cond_4
    const-string v0, "segment_transcode_status"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221685
    const-string v0, "segment_upload_status"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221686
    iget-boolean v0, p1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->p:Z

    move v0, v0

    .line 2221687
    if-eqz v0, :cond_5

    .line 2221688
    const-string v0, "upload_success_ratio"

    const-string v1, "1"

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221689
    :cond_5
    iget-boolean v0, p1, Lcom/facebook/messaging/media/upload/segmented/VideoSegmentTranscodeUploadOperation;->q:Z

    move v0, v0

    .line 2221690
    if-eqz v0, :cond_6

    .line 2221691
    const-string v0, "upload_cancelled_ratio"

    const-string v1, "1"

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221692
    :cond_6
    iget-object v0, p0, LX/FIC;->d:LX/FIB;

    .line 2221693
    if-nez p2, :cond_7

    .line 2221694
    :goto_1
    return-void

    .line 2221695
    :cond_7
    iget-object v1, v0, LX/FIB;->a:LX/0Zb;

    const-string v2, "messenger_segmented_transcode_upload"

    invoke-interface {v1, v2, p2}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1
.end method


# virtual methods
.method public final cancelOperation(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2221696
    iget-object v0, p0, LX/FIC;->b:LX/6ef;

    invoke-virtual {v0, p1}, LX/6ef;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2221697
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2221698
    const-string v1, "video_segment_transcode_upload"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2221699
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2221700
    :cond_0
    invoke-direct {p0, p1}, LX/FIC;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
