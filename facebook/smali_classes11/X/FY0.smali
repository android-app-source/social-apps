.class public abstract LX/FY0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/BO1;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/MenuItem$OnMenuItemClickListener;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/BO1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2255643
    const-class v0, LX/FY0;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FY0;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2255644
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract b(LX/BO1;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation
.end method

.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2255645
    iget-object v0, p0, LX/FY0;->b:LX/BO1;

    if-nez v0, :cond_0

    .line 2255646
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DAOItem is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2255647
    :cond_0
    iget-object v0, p0, LX/FY0;->b:LX/BO1;

    invoke-virtual {p0, v0}, LX/FY0;->b(LX/BO1;)Z

    move-result v0

    return v0
.end method
