.class public LX/FGd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0ad;

.field private final c:LX/2Mo;

.field private final d:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0ad;LX/2Mo;LX/0Zb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            ">;",
            "LX/0ad;",
            "LX/2Mo;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2219254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219255
    iput-object p1, p0, LX/FGd;->a:LX/0Ot;

    .line 2219256
    iput-object p2, p0, LX/FGd;->b:LX/0ad;

    .line 2219257
    iput-object p3, p0, LX/FGd;->c:LX/2Mo;

    .line 2219258
    iput-object p4, p0, LX/FGd;->d:LX/0Zb;

    .line 2219259
    return-void
.end method

.method public static a(LX/FGd;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2219219
    iget-object v0, p0, LX/FGd;->d:LX/0Zb;

    invoke-interface {v0, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2219220
    return-void
.end method

.method public static b(LX/0QB;)LX/FGd;
    .locals 5

    .prologue
    .line 2219252
    new-instance v3, LX/FGd;

    const/16 v0, 0xd4e

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/2Mo;->a(LX/0QB;)LX/2Mo;

    move-result-object v1

    check-cast v1, LX/2Mo;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-direct {v3, v4, v0, v1, v2}, LX/FGd;-><init>(LX/0Ot;LX/0ad;LX/2Mo;LX/0Zb;)V

    .line 2219253
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2219239
    iget-object v0, p0, LX/FGd;->b:LX/0ad;

    sget-short v1, LX/FGS;->e:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2219240
    :cond_0
    :goto_0
    return-void

    .line 2219241
    :cond_1
    iget-object v0, p0, LX/FGd;->b:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/FGS;->j:S

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FGd;->c:LX/2Mo;

    invoke-virtual {v0}, LX/2Mo;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2219242
    :cond_2
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-eq v0, v1, :cond_3

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 2219243
    :cond_3
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-nez v0, :cond_0

    .line 2219244
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2219245
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219246
    const-string v1, "media_type"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v2}, LX/2MK;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219247
    const-string v1, "media_source"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v2}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219248
    const-string v1, "messenger_media_pre_upload_started"

    invoke-static {p0, v1, v0}, LX/FGd;->a(LX/FGd;Ljava/lang/String;Ljava/util/Map;)V

    .line 2219249
    iget-object v0, p0, LX/FGd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 2219250
    sget-object v1, LX/FHf;->PRE_UPLOAD:LX/FHf;

    invoke-static {v0, p1, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2219251
    goto :goto_0
.end method

.method public final a(LX/2MK;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2219232
    const/4 v1, 0x0

    .line 2219233
    sget-object v2, LX/2MK;->PHOTO:LX/2MK;

    if-ne p1, v2, :cond_2

    .line 2219234
    iget-object v2, p0, LX/FGd;->b:LX/0ad;

    sget-short v3, LX/FGS;->k:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2219235
    :cond_0
    :goto_0
    move v1, v1

    .line 2219236
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/FGd;->b:LX/0ad;

    sget-short v2, LX/FGS;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    .line 2219237
    :cond_2
    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne p1, v2, :cond_0

    .line 2219238
    iget-object v2, p0, LX/FGd;->b:LX/0ad;

    sget-short v3, LX/FGS;->l:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 3

    .prologue
    .line 2219221
    iget-object v0, p0, LX/FGd;->b:LX/0ad;

    sget-short v1, LX/FGS;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2219222
    :cond_0
    :goto_0
    return-void

    .line 2219223
    :cond_1
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-eq v0, v1, :cond_2

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 2219224
    :cond_2
    iget-object v0, p0, LX/FGd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 2219225
    iget-object v1, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    invoke-virtual {v1, p1}, LX/2MT;->b(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2219226
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2219227
    const-string v1, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219228
    const-string v1, "media_type"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v2}, LX/2MK;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219229
    const-string v1, "media_source"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v2}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219230
    const-string v1, "messenger_media_pre_upload_canceled"

    invoke-static {p0, v1, v0}, LX/FGd;->a(LX/FGd;Ljava/lang/String;Ljava/util/Map;)V

    .line 2219231
    goto :goto_0
.end method
