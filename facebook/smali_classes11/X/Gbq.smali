.class public LX/Gbq;
.super LX/Gbk;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final l:Lcom/facebook/resources/ui/FbTextView;

.field private final m:Lcom/facebook/resources/ui/FbTextView;

.field private final n:Lcom/facebook/resources/ui/FbTextView;

.field private final o:Landroid/content/Context;

.field private final p:Landroid/view/View;

.field public q:LX/Gbi;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2370433
    invoke-direct {p0, p1}, LX/Gbk;-><init>(Landroid/view/View;)V

    .line 2370434
    iput-object p1, p0, LX/Gbq;->p:Landroid/view/View;

    .line 2370435
    iget-object v0, p0, LX/Gbq;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2370436
    const v0, 0x7f0d0c23

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Gbq;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2370437
    const v0, 0x7f0d0c24

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Gbq;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2370438
    const v0, 0x7f0d0c25

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Gbq;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2370439
    iput-object p2, p0, LX/Gbq;->o:Landroid/content/Context;

    .line 2370440
    return-void
.end method

.method private c(I)I
    .locals 3

    .prologue
    .line 2370441
    const/4 v0, 0x1

    int-to-float v1, p1

    iget-object v2, p0, LX/Gbq;->o:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a(LX/Gbo;)V
    .locals 6

    .prologue
    const/16 v5, 0xe

    const/16 v4, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 2370442
    iget-object v0, p0, LX/Gbq;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2370443
    iget-object v1, p1, LX/Gbo;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2370444
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2370445
    iget-object v0, p1, LX/Gbo;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2370446
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2370447
    iget-object v0, p0, LX/Gbq;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2370448
    iget-object v0, p0, LX/Gbq;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2370449
    const/16 v1, 0x11

    invoke-direct {p0, v1}, LX/Gbq;->c(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2370450
    iget-object v1, p0, LX/Gbq;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2370451
    :goto_0
    iget-object v0, p1, LX/Gbo;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2370452
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2370453
    iget-object v0, p0, LX/Gbq;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2370454
    iget-object v0, p0, LX/Gbq;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2370455
    invoke-direct {p0, v5}, LX/Gbq;->c(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2370456
    iget-object v1, p0, LX/Gbq;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2370457
    :goto_1
    iget-boolean v0, p1, LX/Gbo;->d:Z

    move v0, v0

    .line 2370458
    if-eqz v0, :cond_2

    .line 2370459
    iget-object v0, p0, LX/Gbq;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Gbq;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2370460
    :goto_2
    return-void

    .line 2370461
    :cond_0
    iget-object v0, p0, LX/Gbq;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2370462
    iget-object v1, p1, LX/Gbo;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2370463
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2370464
    iget-object v0, p0, LX/Gbq;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2370465
    iget-object v0, p0, LX/Gbq;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2370466
    invoke-direct {p0, v5}, LX/Gbq;->c(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2370467
    invoke-direct {p0, v3}, LX/Gbq;->c(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2370468
    iget-object v1, p0, LX/Gbq;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2370469
    :cond_1
    iget-object v0, p0, LX/Gbq;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2370470
    iget-object v0, p0, LX/Gbq;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2370471
    invoke-direct {p0, v3}, LX/Gbq;->c(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2370472
    iget-object v1, p0, LX/Gbq;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2370473
    iget-object v0, p0, LX/Gbq;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2370474
    iget-object v1, p1, LX/Gbo;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2370475
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2370476
    :cond_2
    iget-object v0, p0, LX/Gbq;->l:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Gbq;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x778bdd2a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2370477
    invoke-virtual {p0}, LX/1a1;->e()I

    move-result v1

    .line 2370478
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2370479
    iget-object v2, p0, LX/Gbq;->q:LX/Gbi;

    if-eqz v2, :cond_0

    .line 2370480
    iget-object v2, p0, LX/Gbq;->q:LX/Gbi;

    .line 2370481
    iget-object v4, v2, LX/Gbi;->a:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Gbo;

    .line 2370482
    iget-object v5, v2, LX/Gbi;->c:LX/GbV;

    .line 2370483
    iget-object v7, v5, LX/GbV;->a:LX/10M;

    iget-object v6, v5, LX/GbV;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    .line 2370484
    iget-object v5, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v5

    .line 2370485
    invoke-virtual {v7, v6}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v6

    move-object v5, v6

    .line 2370486
    sget-object v6, LX/Gbh;->a:[I

    .line 2370487
    iget-object v7, v4, LX/Gbo;->e:LX/Gbn;

    move-object v7, v7

    .line 2370488
    invoke-virtual {v7}, LX/Gbn;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 2370489
    :cond_0
    :goto_0
    const v1, -0x4f593fe0

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2370490
    :pswitch_0
    iget-object v4, v2, LX/Gbi;->c:LX/GbV;

    .line 2370491
    iget-object v6, v4, LX/GbV;->g:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    const-string v7, "logged_in_settings"

    const/4 v1, 0x0

    invoke-virtual {v6, v5, v7, v1}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;Z)V

    .line 2370492
    iget-object v6, v4, LX/GbV;->h:LX/2Di;

    invoke-virtual {v6}, LX/2Di;->c()V

    .line 2370493
    iget-object v4, v2, LX/Gbi;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f08347c

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 2370494
    iget-object v4, v2, LX/Gbi;->b:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 2370495
    :pswitch_1
    iget-object v4, v2, LX/Gbi;->c:LX/GbV;

    .line 2370496
    iget-object v6, v4, LX/GbV;->g:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    const-string v7, "logged_in_settings"

    invoke-virtual {v6, v5, v7}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)V

    .line 2370497
    goto :goto_0

    .line 2370498
    :pswitch_2
    const-string v4, "add_pin"

    invoke-static {v2, v5, v4}, LX/Gbi;->a(LX/Gbi;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 2370499
    :pswitch_3
    const-string v4, "change_pin"

    invoke-static {v2, v5, v4}, LX/Gbi;->a(LX/Gbi;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 2370500
    :pswitch_4
    const-string v4, "remove_pin"

    invoke-static {v2, v5, v4}, LX/Gbi;->a(LX/Gbi;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 2370501
    :pswitch_5
    const-string v4, "switch_to_dbl"

    invoke-static {v2, v5, v4}, LX/Gbi;->a(LX/Gbi;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 2370502
    :pswitch_6
    const-string v6, "switch_to_dbl_with_pin"

    invoke-static {v2, v5, v6}, LX/Gbi;->a(LX/Gbi;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)Landroid/content/Intent;

    .line 2370503
    :pswitch_7
    iget-object v5, v2, LX/Gbi;->c:LX/GbV;

    .line 2370504
    iget-object v6, v4, LX/Gbo;->g:Ljava/lang/String;

    move-object v6, v6

    .line 2370505
    new-instance v7, LX/4E4;

    invoke-direct {v7}, LX/4E4;-><init>()V

    .line 2370506
    const-string p0, "datr"

    invoke-virtual {v7, p0, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370507
    move-object v7, v7

    .line 2370508
    new-instance p0, LX/Gbc;

    invoke-direct {p0}, LX/Gbc;-><init>()V

    move-object p0, p0

    .line 2370509
    const-string p1, "input"

    invoke-virtual {p0, p1, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2370510
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2370511
    iget-object p0, v5, LX/GbV;->e:LX/0tX;

    invoke-virtual {p0, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2370512
    iget-object v5, v2, LX/Gbi;->a:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2370513
    invoke-virtual {v2, v1}, LX/1OM;->d(I)V

    .line 2370514
    invoke-virtual {v2}, LX/1OM;->ij_()I

    move-result v4

    invoke-virtual {v2, v1, v4}, LX/1OM;->a(II)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
