.class public final enum LX/FFq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FFq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FFq;

.field public static final enum HIGH_FIVE:LX/FFq;

.field public static final enum NUDGE:LX/FFq;

.field public static final enum OTHERS:LX/FFq;

.field public static final RECIPROCAL_LIGHTWEIGHT_ACTIONS:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SUPPORTED_LIGHTWEIGHT_ACTIONS:LX/2QP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2QP",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum WAVE:LX/FFq;


# instance fields
.field public final actionNameResId:I

.field public final actionType:Ljava/lang/String;

.field public final deliveredResId:I

.field public final imageResId:I

.field public final initialEmoji:Ljava/lang/String;

.field public final missedEmoji:Ljava/lang/String;

.field public final reciprocatedEmoji:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 2217649
    new-instance v0, LX/FFq;

    const-string v1, "NUDGE"

    const-string v3, "NUDGE"

    const-string v4, "\ud83d\udc46"

    const-string v5, "\ud83d\udc46"

    const-string v6, "\ud83d\udc46"

    const v7, 0x7f0801ce

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const v8, 0x7f021021

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const v9, 0x7f08099a

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/FFq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v0, LX/FFq;->NUDGE:LX/FFq;

    .line 2217650
    new-instance v3, LX/FFq;

    const-string v4, "HIGH_FIVE"

    const-string v6, "HIGH_FIVE"

    const-string v7, "\u270b"

    const-string v8, "\ud83d\ude4f"

    const-string v9, "\u270b"

    const v0, 0x7f0801cd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const v0, 0x7f021020

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const v0, 0x7f080998

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    move v5, v13

    invoke-direct/range {v3 .. v12}, LX/FFq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, LX/FFq;->HIGH_FIVE:LX/FFq;

    .line 2217651
    new-instance v3, LX/FFq;

    const-string v4, "WAVE"

    const-string v6, "WAVE"

    const-string v7, "\ud83d\udc4b"

    const-string v8, "\ud83d\ude4c"

    const-string v9, "\ud83d\udc4b"

    const v0, 0x7f0801cf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const v0, 0x7f021022

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const v0, 0x7f080999

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    move v5, v14

    invoke-direct/range {v3 .. v12}, LX/FFq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, LX/FFq;->WAVE:LX/FFq;

    .line 2217652
    new-instance v3, LX/FFq;

    const-string v4, "OTHERS"

    const-string v6, "OTHERS"

    const-string v7, ""

    const-string v8, ""

    const-string v9, ""

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    move v5, v15

    invoke-direct/range {v3 .. v12}, LX/FFq;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, LX/FFq;->OTHERS:LX/FFq;

    .line 2217653
    const/4 v0, 0x4

    new-array v0, v0, [LX/FFq;

    sget-object v1, LX/FFq;->NUDGE:LX/FFq;

    aput-object v1, v0, v2

    sget-object v1, LX/FFq;->HIGH_FIVE:LX/FFq;

    aput-object v1, v0, v13

    sget-object v1, LX/FFq;->WAVE:LX/FFq;

    aput-object v1, v0, v14

    sget-object v1, LX/FFq;->OTHERS:LX/FFq;

    aput-object v1, v0, v15

    sput-object v0, LX/FFq;->$VALUES:[LX/FFq;

    .line 2217654
    new-array v0, v15, [Ljava/lang/String;

    sget-object v1, LX/FFq;->HIGH_FIVE:LX/FFq;

    invoke-virtual {v1}, LX/FFq;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    sget-object v1, LX/FFq;->WAVE:LX/FFq;

    invoke-virtual {v1}, LX/FFq;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v13

    sget-object v1, LX/FFq;->NUDGE:LX/FFq;

    invoke-virtual {v1}, LX/FFq;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v14

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/FFq;->SUPPORTED_LIGHTWEIGHT_ACTIONS:LX/2QP;

    .line 2217655
    new-array v0, v14, [Ljava/lang/String;

    sget-object v1, LX/FFq;->HIGH_FIVE:LX/FFq;

    invoke-virtual {v1}, LX/FFq;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    sget-object v1, LX/FFq;->WAVE:LX/FFq;

    invoke-virtual {v1}, LX/FFq;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, LX/FFq;->RECIPROCAL_LIGHTWEIGHT_ACTIONS:LX/2QP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2217656
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2217657
    iput-object p3, p0, LX/FFq;->actionType:Ljava/lang/String;

    .line 2217658
    iput-object p5, p0, LX/FFq;->reciprocatedEmoji:Ljava/lang/String;

    .line 2217659
    iput-object p6, p0, LX/FFq;->missedEmoji:Ljava/lang/String;

    .line 2217660
    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/FFq;->actionNameResId:I

    .line 2217661
    iput-object p4, p0, LX/FFq;->initialEmoji:Ljava/lang/String;

    .line 2217662
    invoke-virtual {p8}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/FFq;->imageResId:I

    .line 2217663
    invoke-virtual {p9}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/FFq;->deliveredResId:I

    .line 2217664
    return-void
.end method

.method public static parseType(Ljava/lang/String;)LX/FFq;
    .locals 1

    .prologue
    .line 2217665
    sget-object v0, LX/FFq;->SUPPORTED_LIGHTWEIGHT_ACTIONS:LX/2QP;

    invoke-virtual {v0, p0}, LX/2QP;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2217666
    invoke-static {p0}, LX/FFq;->valueOf(Ljava/lang/String;)LX/FFq;

    move-result-object v0

    .line 2217667
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/FFq;->OTHERS:LX/FFq;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FFq;
    .locals 1

    .prologue
    .line 2217668
    const-class v0, LX/FFq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FFq;

    return-object v0
.end method

.method public static values()[LX/FFq;
    .locals 1

    .prologue
    .line 2217669
    sget-object v0, LX/FFq;->$VALUES:[LX/FFq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FFq;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2217670
    iget-object v0, p0, LX/FFq;->actionType:Ljava/lang/String;

    return-object v0
.end method
