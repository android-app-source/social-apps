.class public LX/G4q;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/FqL;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2318029
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/G4q;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2318030
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2318027
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/G4q;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2318028
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2318017
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2318018
    const v0, 0x7f0314d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2318019
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2318020
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2318021
    iget-object v0, p0, LX/G4q;->a:LX/FqL;

    if-eqz v0, :cond_1

    .line 2318022
    iget-object v0, p0, LX/G4q;->a:LX/FqL;

    .line 2318023
    iget-object v1, v0, LX/FqL;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v1, v1, Lcom/facebook/timeline/TimelineFragment;->aV:LX/FvM;

    if-eqz v1, :cond_0

    .line 2318024
    iget-object v1, v0, LX/FqL;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v1, v1, Lcom/facebook/timeline/TimelineFragment;->aV:LX/FvM;

    iget-object p0, v0, LX/FqL;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object p0, p0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    iget-object p1, v0, LX/FqL;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object p1, p1, Lcom/facebook/timeline/TimelineFragment;->ap:LX/G4q;

    invoke-interface {v1, p0, p1}, LX/FvM;->a(LX/BQ1;LX/G4q;)V

    .line 2318025
    :cond_0
    iget-object v1, v0, LX/FqL;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v1, v1, Lcom/facebook/timeline/TimelineFragment;->bo:LX/Fv9;

    invoke-virtual {v1}, LX/Fv9;->a()V

    .line 2318026
    :cond_1
    return-void
.end method
