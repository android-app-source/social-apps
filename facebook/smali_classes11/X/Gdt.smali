.class public LX/Gdt;
.super LX/AnF;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final e:LX/2eR;

.field private static g:LX/0Xm;


# instance fields
.field private final f:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2374671
    new-instance v0, LX/Gds;

    invoke-direct {v0}, LX/Gds;-><init>()V

    sput-object v0, LX/Gdt;->e:LX/2eR;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0hB;Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;LX/1DR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374672
    invoke-direct {p0, p1, p2, p4}, LX/AnF;-><init>(Landroid/content/Context;LX/0hB;LX/1DR;)V

    .line 2374673
    iput-object p3, p0, LX/Gdt;->f:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    .line 2374674
    return-void
.end method

.method public static a(LX/0QB;)LX/Gdt;
    .locals 7

    .prologue
    .line 2374676
    const-class v1, LX/Gdt;

    monitor-enter v1

    .line 2374677
    :try_start_0
    sget-object v0, LX/Gdt;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2374678
    sput-object v2, LX/Gdt;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2374679
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2374680
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2374681
    new-instance p0, LX/Gdt;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v0}, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->a(LX/0QB;)Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v6

    check-cast v6, LX/1DR;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Gdt;-><init>(Landroid/content/Context;LX/0hB;Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;LX/1DR;)V

    .line 2374682
    move-object v0, p0

    .line 2374683
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2374684
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gdt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2374685
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2374686
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/99X;)F
    .locals 1

    .prologue
    .line 2374675
    iget-object v0, p0, LX/Gdt;->f:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    invoke-virtual {v0, p1}, LX/2sw;->a(LX/99X;)F

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 2374665
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2374666
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 2374667
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v1, :cond_0

    .line 2374668
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    invoke-static {v0}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v0

    .line 2374669
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/2eR;
    .locals 1

    .prologue
    .line 2374670
    sget-object v0, LX/Gdt;->e:LX/2eR;

    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 6

    .prologue
    .line 2374663
    iget-object v0, p0, LX/Gdt;->f:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/Gdm;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;)V

    .line 2374664
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/widget/TextView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2374654
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2374655
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 2374656
    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 2374657
    invoke-interface {v0}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2374658
    invoke-interface {v0}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2374659
    :goto_0
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2374660
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2374661
    return-void

    .line 2374662
    :cond_0
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0066

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/widget/CustomViewPager;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 2374652
    iget-object v0, p0, LX/Gdt;->f:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    invoke-virtual {v0, p1, p2}, LX/2sw;->a(Lcom/facebook/widget/CustomViewPager;Landroid/content/res/Resources;)V

    .line 2374653
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/facebook/widget/CustomViewPager;)V
    .locals 1

    .prologue
    .line 2374650
    iget-object v0, p0, LX/Gdt;->f:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    invoke-virtual {v0, p1, p2}, LX/2sw;->a(Ljava/util/List;Lcom/facebook/widget/CustomViewPager;)V

    .line 2374651
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2374649
    iget-object v0, p0, LX/Gdt;->f:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    invoke-virtual {v0}, LX/2sw;->b()I

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2374648
    const-class v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    return-object v0
.end method
