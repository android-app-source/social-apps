.class public abstract LX/GIa;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

.field public b:Lcom/facebook/widget/CustomLinearLayout;

.field public c:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

.field private d:Landroid/view/View;

.field public e:Landroid/view/View;

.field public f:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

.field private g:Landroid/view/View;

.field public h:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

.field public i:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field public l:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

.field private m:Landroid/view/View;

.field public n:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

.field private o:Landroid/view/View;

.field public p:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2336890
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2336891
    invoke-virtual {p0}, LX/GIa;->a()V

    .line 2336892
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2336887
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2336888
    invoke-virtual {p0}, LX/GIa;->a()V

    .line 2336889
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2336884
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2336885
    invoke-virtual {p0}, LX/GIa;->a()V

    .line 2336886
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 2336864
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/GIa;->setOrientation(I)V

    .line 2336865
    invoke-virtual {p0}, LX/GIa;->b()V

    .line 2336866
    const v0, 0x7f0d048d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iput-object v0, p0, LX/GIa;->c:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    .line 2336867
    const v0, 0x7f0d048b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GIa;->d:Landroid/view/View;

    .line 2336868
    const v0, 0x7f0d048c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GIa;->e:Landroid/view/View;

    .line 2336869
    const v0, 0x7f0d048a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    iput-object v0, p0, LX/GIa;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    .line 2336870
    const v0, 0x7f0d0490

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    iput-object v0, p0, LX/GIa;->i:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    .line 2336871
    const v0, 0x7f0d048e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GIa;->j:Landroid/view/View;

    .line 2336872
    const v0, 0x7f0d048f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GIa;->k:Landroid/view/View;

    .line 2336873
    const v0, 0x7f0d0483

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    iput-object v0, p0, LX/GIa;->l:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    .line 2336874
    const v0, 0x7f0d0482

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GIa;->m:Landroid/view/View;

    .line 2336875
    iget-object v0, p0, LX/GIa;->l:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {p0}, LX/GIa;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080a6f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setEditButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 2336876
    const v0, 0x7f0d0487

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GIa;->g:Landroid/view/View;

    .line 2336877
    const v0, 0x7f0d0488

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    iput-object v0, p0, LX/GIa;->h:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    .line 2336878
    const v0, 0x7f0d0486

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    iput-object v0, p0, LX/GIa;->n:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    .line 2336879
    const v0, 0x7f0d0485

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GIa;->o:Landroid/view/View;

    .line 2336880
    iget-object v0, p0, LX/GIa;->n:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {p0}, LX/GIa;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080a70

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setEditButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 2336881
    const v0, 0x7f0d0489

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    iput-object v0, p0, LX/GIa;->p:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    .line 2336882
    const v0, 0x7f0d0484

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    iput-object v0, p0, LX/GIa;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    .line 2336883
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 2336862
    const v0, 0x7f03008f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2336863
    return-void
.end method

.method public final b(Ljava/lang/Iterable;LX/1jt;)V
    .locals 1

    .prologue
    .line 2336837
    iget-object v0, p0, LX/GIa;->n:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 2336838
    return-void
.end method

.method public final c(Ljava/lang/Iterable;LX/1jt;)V
    .locals 1

    .prologue
    .line 2336860
    iget-object v0, p0, LX/GIa;->h:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 2336861
    return-void
.end method

.method public getAudienceOptionsView()Lcom/facebook/widget/CustomLinearLayout;
    .locals 1

    .prologue
    .line 2336859
    iget-object v0, p0, LX/GIa;->b:Lcom/facebook/widget/CustomLinearLayout;

    return-object v0
.end method

.method public setAgeViewTopDividerVisibility(I)V
    .locals 1

    .prologue
    .line 2336893
    iget-object v0, p0, LX/GIa;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2336894
    return-void
.end method

.method public setDetailedTargetingSelectorVisibility(I)V
    .locals 1

    .prologue
    .line 2336856
    iget-object v0, p0, LX/GIa;->h:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setVisibility(I)V

    .line 2336857
    iget-object v0, p0, LX/GIa;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2336858
    return-void
.end method

.method public setGenderViewVisibility(I)V
    .locals 1

    .prologue
    .line 2336852
    iget-object v0, p0, LX/GIa;->i:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->setVisibility(I)V

    .line 2336853
    iget-object v0, p0, LX/GIa;->k:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2336854
    iget-object v0, p0, LX/GIa;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2336855
    return-void
.end method

.method public setInterestsDividerVisibility(I)V
    .locals 1

    .prologue
    .line 2336850
    iget-object v0, p0, LX/GIa;->o:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2336851
    return-void
.end method

.method public setInterestsSelectorVisibility(I)V
    .locals 1

    .prologue
    .line 2336847
    iget-object v0, p0, LX/GIa;->n:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setVisibility(I)V

    .line 2336848
    iget-object v0, p0, LX/GIa;->o:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2336849
    return-void
.end method

.method public setLocationSelectorDividerVisibility(I)V
    .locals 1

    .prologue
    .line 2336845
    iget-object v0, p0, LX/GIa;->m:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2336846
    return-void
.end method

.method public setLocationsSelectorVisibility(I)V
    .locals 1

    .prologue
    .line 2336842
    iget-object v0, p0, LX/GIa;->l:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setVisibility(I)V

    .line 2336843
    iget-object v0, p0, LX/GIa;->m:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2336844
    return-void
.end method

.method public setRegionSelectorVisibility(I)V
    .locals 1

    .prologue
    .line 2336839
    iget-object v0, p0, LX/GIa;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->setVisibility(I)V

    .line 2336840
    invoke-virtual {p0, p1}, LX/GIa;->setLocationSelectorDividerVisibility(I)V

    .line 2336841
    return-void
.end method
