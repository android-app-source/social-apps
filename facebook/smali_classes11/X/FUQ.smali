.class public final LX/FUQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/qrcode/QRCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/qrcode/QRCodeFragment;)V
    .locals 0

    .prologue
    .line 2248259
    iput-object p1, p0, LX/FUQ;->a:Lcom/facebook/qrcode/QRCodeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x72ecf3b

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2248260
    iget-object v2, p0, LX/FUQ;->a:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v2, v2, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248261
    iget-object v3, v2, LX/FUf;->a:LX/0if;

    sget-object v4, LX/0ig;->H:LX/0ih;

    const-string p1, "SAVE_BUTTON_CLICKED"

    invoke-virtual {v3, v4, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248262
    iget-object v2, p0, LX/FUQ;->a:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v2, v2, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    iget-object v3, p0, LX/FUQ;->a:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v3, v3, Lcom/facebook/qrcode/QRCodeFragment;->M:LX/FUV;

    sget-object v4, LX/FUV;->VANITY:LX/FUV;

    if-ne v3, v4, :cond_0

    .line 2248263
    :goto_0
    const-string v3, "qrcode_save_pressed"

    const-string v4, "vanity"

    invoke-static {v2, v3, v4, v0}, LX/FUW;->a(LX/FUW;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2248264
    iget-object v0, p0, LX/FUQ;->a:Lcom/facebook/qrcode/QRCodeFragment;

    const-string v2, "SAVE_CODE"

    sget-object v3, Lcom/facebook/qrcode/QRCodeFragment;->E:[Ljava/lang/String;

    new-instance v4, Lcom/facebook/qrcode/QRCodeFragment$6$1;

    invoke-direct {v4, p0}, Lcom/facebook/qrcode/QRCodeFragment$6$1;-><init>(LX/FUQ;)V

    invoke-static {v0, v2, v3, v4}, Lcom/facebook/qrcode/QRCodeFragment;->a$redex0(Lcom/facebook/qrcode/QRCodeFragment;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 2248265
    const v0, -0x328ed568    # -2.5288128E8f

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2248266
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
