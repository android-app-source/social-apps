.class public final LX/GtG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400475
    iput-object p1, p0, LX/GtG;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 11

    .prologue
    .line 2400476
    iget-object v0, p0, LX/GtG;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->ai:LX/FmM;

    const-string v1, "Raising money for people!"

    const-string v2, "Fundraiser for a person"

    const-string v3, "https://lh3.googleusercontent.com/-qRmDvvvSw70/V1JQ54e1lyI/AAAAAAAAAXs/INTs7fIUgug9O8UmuKeeUy9xJgE4TnkGw/w426-h409/Potato.png"

    .line 2400477
    sget-object v4, LX/6rp;->PAYMENT_METHOD:LX/6rp;

    sget-object v5, LX/6rp;->AUTHENTICATION:LX/6rp;

    sget-object v6, LX/6rp;->PRICE_SELECTOR:LX/6rp;

    invoke-static {v4, v5, v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    .line 2400478
    sget-object v4, LX/6so;->ENTITY:LX/6so;

    sget-object v5, LX/6so;->PURCHASE_REVIEW_CELL:LX/6so;

    sget-object v6, LX/6so;->PRICE_SELECTOR:LX/6so;

    sget-object v7, LX/6so;->PRIVACY_SELECTOR:LX/6so;

    sget-object v8, LX/6so;->PAYMENT_METHOD:LX/6so;

    sget-object v9, LX/6so;->TERMS_AND_POLICIES:LX/6so;

    invoke-static/range {v4 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 2400479
    new-instance v5, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    new-instance v6, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;

    invoke-direct {v6, v1, v2, v3}, Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;-><init>(Lcom/facebook/payments/checkout/configuration/model/PaymentParticipant;Ljava/lang/String;)V

    .line 2400480
    sget-object v6, LX/6qw;->FUNDRAISER_DONATION:LX/6qw;

    sget-object v7, LX/6xg;->MOR_DONATIONS:LX/6xg;

    sget-object v8, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v8}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v8

    invoke-virtual {v8}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6qU;

    move-result-object v8

    invoke-virtual {v8}, LX/6qU;->a()Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;

    move-result-object v8

    invoke-static {v6, v7, v10, v8}, Lcom/facebook/payments/checkout/CheckoutCommonParams;->a(LX/6qw;LX/6xg;LX/0Rf;Lcom/facebook/payments/checkout/CheckoutAnalyticsParams;)LX/6qZ;

    move-result-object v6

    .line 2400481
    iput-object v4, v6, LX/6qZ;->y:LX/0Px;

    .line 2400482
    move-object v4, v6

    .line 2400483
    iput-object v5, v4, LX/6qZ;->g:Lcom/facebook/payments/checkout/configuration/model/CheckoutEntity;

    .line 2400484
    move-object v4, v4

    .line 2400485
    const v5, 0x7f08334e

    .line 2400486
    iput v5, v4, LX/6qZ;->m:I

    .line 2400487
    move-object v5, v4

    .line 2400488
    iget-object v4, v0, LX/FmM;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f08334f

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2400489
    iput-object v4, v5, LX/6qZ;->z:Ljava/lang/String;

    .line 2400490
    move-object v4, v5

    .line 2400491
    invoke-virtual {v4}, LX/6qZ;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v5

    .line 2400492
    iget-object v4, v0, LX/FmM;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/facebook/payments/checkout/CheckoutActivity;->a(Landroid/content/Context;Lcom/facebook/payments/checkout/CheckoutParams;)Landroid/content/Intent;

    move-result-object v6

    .line 2400493
    iget-object v4, v0, LX/FmM;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    iget-object v5, v0, LX/FmM;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-interface {v4, v6, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2400494
    const/4 v0, 0x1

    return v0
.end method
