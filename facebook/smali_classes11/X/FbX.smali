.class public LX/FbX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2260398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;
    .locals 8
    .param p0    # Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2260399
    if-nez p0, :cond_0

    .line 2260400
    :goto_0
    return-object v1

    .line 2260401
    :cond_0
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2260402
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->e()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 2260403
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel;

    .line 2260404
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel$OrderedSnippetsModel$SentenceModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2260405
    :goto_2
    new-instance v6, LX/8eq;

    invoke-direct {v6}, LX/8eq;-><init>()V

    new-instance v7, LX/8er;

    invoke-direct {v7}, LX/8er;-><init>()V

    .line 2260406
    iput-object v0, v7, LX/8er;->a:Ljava/lang/String;

    .line 2260407
    move-object v0, v7

    .line 2260408
    invoke-virtual {v0}, LX/8er;->a()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    move-result-object v0

    .line 2260409
    iput-object v0, v6, LX/8eq;->a:Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel$SentenceModel;

    .line 2260410
    move-object v0, v6

    .line 2260411
    invoke-virtual {v0}, LX/8eq;->a()Lcom/facebook/search/results/protocol/SearchResultsModuleResultsDecorationModels$SearchResultsModuleResultsDecorationModel$ResultDecorationModel$OrderedSnippetsModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2260412
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 2260413
    goto :goto_2

    .line 2260414
    :cond_2
    new-instance v0, LX/8do;

    invoke-direct {v0}, LX/8do;-><init>()V

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2260415
    iput-object v1, v0, LX/8do;->c:LX/0Px;

    .line 2260416
    move-object v0, v0

    .line 2260417
    invoke-virtual {v0}, LX/8do;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v1

    goto :goto_0
.end method
