.class public final LX/HCq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2440085
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2440086
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440087
    :goto_0
    return v1

    .line 2440088
    :cond_0
    const-string v6, "has_custom_ordering"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2440089
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 2440090
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2440091
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2440092
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2440093
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2440094
    const-string v6, "actions"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2440095
    invoke-static {p0, p1}, LX/9Yh;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2440096
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2440097
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2440098
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2440099
    if-eqz v0, :cond_4

    .line 2440100
    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2440101
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2440102
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2440103
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440104
    if-eqz v0, :cond_0

    .line 2440105
    const-string v1, "actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440106
    invoke-static {p0, v0, p2, p3}, LX/9Yh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2440107
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2440108
    if-eqz v0, :cond_1

    .line 2440109
    const-string v1, "has_custom_ordering"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440110
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2440111
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2440112
    return-void
.end method
