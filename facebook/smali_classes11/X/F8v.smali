.class public LX/F8v;
.super LX/2s5;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/NuxStep;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/F8u;

.field public final d:LX/F8w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2204830
    const-class v0, LX/F8v;

    sput-object v0, LX/F8v;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0iA;LX/F8w;LX/F8u;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2204822
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-direct {p0, v0}, LX/2s5;-><init>(LX/0gc;)V

    .line 2204823
    const-string v0, "1630"

    invoke-virtual {p2, v0}, LX/0iA;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v1

    .line 2204824
    if-eqz v1, :cond_0

    instance-of v0, v1, LX/6Xx;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, LX/6Xx;

    invoke-interface {v0}, LX/6Xx;->e()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2204825
    check-cast v1, LX/6Xx;

    invoke-interface {v1}, LX/6Xx;->e()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/F8w;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/F8v;->b:Ljava/util/List;

    .line 2204826
    :goto_0
    iput-object p4, p0, LX/F8v;->c:LX/F8u;

    .line 2204827
    iput-object p3, p0, LX/F8v;->d:LX/F8w;

    .line 2204828
    return-void

    .line 2204829
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/F8v;->b:Ljava/util/List;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/F8v;
    .locals 9

    .prologue
    .line 2204817
    new-instance v4, LX/F8v;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {p0}, LX/F8w;->a(LX/0QB;)LX/F8w;

    move-result-object v2

    check-cast v2, LX/F8w;

    .line 2204818
    new-instance v7, LX/F8u;

    invoke-static {p0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v3

    check-cast v3, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 v6, 0x320

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {v7, v3, v5, v8, v6}, LX/F8u;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/0Uh;LX/0Or;LX/0ad;)V

    .line 2204819
    move-object v3, v7

    .line 2204820
    check-cast v3, LX/F8u;

    invoke-direct {v4, v0, v1, v2, v3}, LX/F8v;-><init>(Landroid/content/Context;LX/0iA;LX/F8w;LX/F8u;)V

    .line 2204821
    return-object v4
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2204807
    iget-object v1, p0, LX/F8v;->c:LX/F8u;

    iget-object v0, p0, LX/F8v;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/NuxStep;

    .line 2204808
    iget-object p0, v0, Lcom/facebook/ipc/model/NuxStep;->name:Ljava/lang/String;

    move-object v0, p0

    .line 2204809
    invoke-virtual {v1, v0}, LX/F8u;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2204816
    iget-object v0, p0, LX/F8v;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2204810
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2204811
    invoke-virtual {p0}, LX/0gG;->b()I

    .line 2204812
    const-string v0, ""

    .line 2204813
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/F8v;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/NuxStep;

    .line 2204814
    iget-object p0, v0, Lcom/facebook/ipc/model/NuxStep;->name:Ljava/lang/String;

    move-object v0, p0

    .line 2204815
    goto :goto_0
.end method
