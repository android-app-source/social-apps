.class public LX/FzE;
.super Lcom/facebook/fbui/widget/contentview/CheckedContentView;
.source ""


# instance fields
.field public j:Landroid/app/Activity;

.field public k:Lcom/facebook/content/SecureContextHelper;

.field public l:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2307770
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FzE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2307771
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2307772
    const v0, 0x7f0102b5

    invoke-direct {p0, p1, p2, v0}, LX/FzE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2307773
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2307766
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2307767
    const-class v0, LX/FzE;

    invoke-static {v0, p0}, LX/FzE;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2307768
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2307769
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/FzE;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object p0

    check-cast p0, Landroid/app/Activity;

    iput-object v1, p1, LX/FzE;->k:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, LX/FzE;->j:Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;)V
    .locals 2

    .prologue
    .line 2307757
    iput-object p2, p0, LX/FzE;->l:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    .line 2307758
    iget-object v0, p1, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2307759
    iget-object v1, p1, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2307760
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 2307761
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2307762
    const v0, 0x7f0e0144

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2307763
    :goto_0
    return-void

    .line 2307764
    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2307765
    const v0, 0x7f0e0143

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    goto :goto_0
.end method
