.class public LX/Gks;
.super LX/Gkp;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2389662
    invoke-direct {p0, p1}, LX/Gkp;-><init>(Landroid/content/res/Resources;)V

    .line 2389663
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2389664
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkq;

    .line 2389665
    iget-boolean v3, v0, LX/Gkq;->j:Z

    move v3, v3

    .line 2389666
    if-eqz v3, :cond_0

    .line 2389667
    iget-boolean v3, v0, LX/Gkq;->g:Z

    move v3, v3

    .line 2389668
    if-nez v3, :cond_0

    .line 2389669
    iget-boolean v3, v0, LX/Gkq;->i:Z

    move v3, v3

    .line 2389670
    if-nez v3, :cond_1

    .line 2389671
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2389672
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2389673
    :cond_2
    return-object p1
.end method

.method public final b(I)LX/GlC;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2389674
    invoke-super {p0, p1}, LX/Gkp;->b(I)LX/GlC;

    move-result-object v0

    .line 2389675
    const-string v1, "has_favorited"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2389676
    const-string v1, "has_hidden"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2389677
    const-string v1, "recently_added"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2389678
    return-object v0
.end method

.method public final synthetic c()Ljava/lang/Enum;
    .locals 1

    .prologue
    .line 2389679
    invoke-virtual {p0}, LX/Gkp;->i()LX/Gkn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/Gkn;
    .locals 1

    .prologue
    .line 2389680
    sget-object v0, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    return-object v0
.end method
