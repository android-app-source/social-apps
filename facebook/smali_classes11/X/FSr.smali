.class public final LX/FSr;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2242561
    const-class v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCurationBucketQueryModel;

    const v0, 0x776ed1d4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "DiscoveryCurationBucketQuery"

    const-string v6, "b49cc995557bf3d411e395a009d34ad1"

    const-string v7, "profile_discovery_bucket"

    const-string v8, "10155183270686729"

    const-string v9, "10155259086626729"

    .line 2242562
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2242563
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2242564
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2242565
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2242566
    sparse-switch v0, :sswitch_data_0

    .line 2242567
    :goto_0
    return-object p1

    .line 2242568
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2242569
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2242570
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2242571
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2242572
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2242573
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2242574
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2242575
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2242576
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2242577
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2242578
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2242579
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2242580
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2242581
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2242582
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2242583
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2242584
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2242585
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2242586
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 2242587
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 2242588
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 2242589
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 2242590
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7448a3b3 -> :sswitch_2
        -0x70b5e4f5 -> :sswitch_16
        -0x6a24640d -> :sswitch_14
        -0x680de62a -> :sswitch_f
        -0x6326fdb3 -> :sswitch_12
        -0x55d541c4 -> :sswitch_15
        -0x4496acc9 -> :sswitch_e
        -0x41a91745 -> :sswitch_5
        -0x2eb22064 -> :sswitch_4
        -0x1b87b280 -> :sswitch_13
        -0x12efdeb3 -> :sswitch_11
        -0x93a55fc -> :sswitch_9
        0x759c0e8 -> :sswitch_d
        0xa1fa812 -> :sswitch_c
        0x14658929 -> :sswitch_b
        0x155c0cca -> :sswitch_a
        0x214100e0 -> :sswitch_10
        0x291d8de0 -> :sswitch_6
        0x35681893 -> :sswitch_1
        0x638aab4b -> :sswitch_3
        0x6d2645b9 -> :sswitch_7
        0x6d80e790 -> :sswitch_0
        0x73a026b5 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2242591
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2242592
    :goto_1
    return v0

    .line 2242593
    :sswitch_0
    const-string v2, "11"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v2, "10"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "13"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "20"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    .line 2242594
    :pswitch_0
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2242595
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2242596
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2242597
    :pswitch_3
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2242598
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2242599
    :pswitch_5
    const-string v0, "contain-fit"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_2
        0x32 -> :sswitch_3
        0x61f -> :sswitch_1
        0x620 -> :sswitch_0
        0x622 -> :sswitch_4
        0x63e -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
