.class public LX/FJ6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

.field public final b:LX/FJ7;

.field private final c:J

.field public d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Landroid/graphics/PointF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Landroid/graphics/PointF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/FCB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:D

.field public j:D


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/FJ7;J)V
    .locals 1

    .prologue
    .line 2223101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223102
    iput-object p1, p0, LX/FJ6;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    .line 2223103
    iput-object p2, p0, LX/FJ6;->b:LX/FJ7;

    .line 2223104
    iput-wide p3, p0, LX/FJ6;->c:J

    .line 2223105
    return-void
.end method

.method public static d(LX/FJ6;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 2223093
    iget-boolean v0, p0, LX/FJ6;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FJ6;->f:Landroid/graphics/PointF;

    if-eqz v0, :cond_1

    .line 2223094
    iget-object v0, p0, LX/FJ6;->b:LX/FJ7;

    invoke-interface {v0, v11}, LX/FJ7;->a(Landroid/net/Uri;)V

    .line 2223095
    iget-object v0, p0, LX/FJ6;->b:LX/FJ7;

    iget-object v1, p0, LX/FJ6;->f:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget-object v2, p0, LX/FJ6;->f:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    float-to-int v2, v2

    invoke-interface {v0, v1, v2}, LX/FJ7;->a(II)V

    .line 2223096
    :cond_0
    :goto_0
    return-void

    .line 2223097
    :cond_1
    iget-object v0, p0, LX/FJ6;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FJ6;->e:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FJ6;->f:Landroid/graphics/PointF;

    if-eqz v0, :cond_0

    .line 2223098
    iget-object v0, p0, LX/FJ6;->b:LX/FJ7;

    iget-object v1, p0, LX/FJ6;->d:Landroid/net/Uri;

    invoke-interface {v0, v1}, LX/FJ7;->a(Landroid/net/Uri;)V

    .line 2223099
    iget-object v0, p0, LX/FJ6;->b:LX/FJ7;

    iget-object v1, p0, LX/FJ6;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget-object v2, p0, LX/FJ6;->e:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    float-to-int v2, v2

    invoke-interface {v0, v1, v2}, LX/FJ7;->a(II)V

    .line 2223100
    new-instance v1, LX/FCB;

    iget-object v2, p0, LX/FJ6;->e:Landroid/graphics/PointF;

    iget-object v3, p0, LX/FJ6;->f:Landroid/graphics/PointF;

    const-wide/16 v4, 0x1f40

    iget-object v0, p0, LX/FJ6;->e:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, LX/FJ6;->e:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    invoke-static {v0, v6}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    new-instance v7, LX/FJ5;

    invoke-direct {v7, p0}, LX/FJ5;-><init>(LX/FJ6;)V

    new-instance v8, LX/FCC;

    invoke-direct {v8}, LX/FCC;-><init>()V

    const/high16 v9, 0x3fc00000    # 1.5f

    invoke-direct/range {v1 .. v11}, LX/FCB;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;JILX/FJ5;LX/FCC;FZLjava/util/List;)V

    iput-object v1, p0, LX/FJ6;->g:LX/FCB;

    goto :goto_0
.end method


# virtual methods
.method public final a(D)V
    .locals 7

    .prologue
    .line 2223106
    invoke-virtual {p0}, LX/FJ6;->b()LX/FJ8;

    move-result-object v0

    sget-object v1, LX/FJ8;->NOT_READY:LX/FJ8;

    if-ne v0, v1, :cond_1

    .line 2223107
    :cond_0
    :goto_0
    return-void

    .line 2223108
    :cond_1
    iget-wide v0, p0, LX/FJ6;->i:D

    add-double/2addr v0, p1

    iput-wide v0, p0, LX/FJ6;->i:D

    .line 2223109
    iget-boolean v0, p0, LX/FJ6;->h:Z

    if-nez v0, :cond_0

    .line 2223110
    iget-object v0, p0, LX/FJ6;->g:LX/FCB;

    iget-wide v2, p0, LX/FJ6;->c:J

    iget-wide v4, p0, LX/FJ6;->i:D

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/FCB;->a(J)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/PointF;)V
    .locals 0
    .param p1    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223090
    iput-object p1, p0, LX/FJ6;->f:Landroid/graphics/PointF;

    .line 2223091
    invoke-static {p0}, LX/FJ6;->d(LX/FJ6;)V

    .line 2223092
    return-void
.end method

.method public final b()LX/FJ8;
    .locals 8

    .prologue
    const-wide v6, 0x409f400000000000L    # 2000.0

    const-wide/16 v4, 0x0

    .line 2223075
    iget-boolean v0, p0, LX/FJ6;->h:Z

    if-nez v0, :cond_4

    .line 2223076
    iget-object v0, p0, LX/FJ6;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FJ6;->g:LX/FCB;

    if-nez v0, :cond_1

    .line 2223077
    :cond_0
    sget-object v0, LX/FJ8;->NOT_READY:LX/FJ8;

    .line 2223078
    :goto_0
    return-object v0

    .line 2223079
    :cond_1
    iget-wide v0, p0, LX/FJ6;->i:D

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_2

    .line 2223080
    sget-object v0, LX/FJ8;->READY:LX/FJ8;

    goto :goto_0

    .line 2223081
    :cond_2
    iget-wide v0, p0, LX/FJ6;->i:D

    cmpg-double v0, v0, v6

    if-gtz v0, :cond_3

    .line 2223082
    sget-object v0, LX/FJ8;->FADE_IN:LX/FJ8;

    goto :goto_0

    .line 2223083
    :cond_3
    iget-wide v0, p0, LX/FJ6;->i:D

    const-wide v2, 0x40b7700000000000L    # 6000.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_4

    .line 2223084
    sget-object v0, LX/FJ8;->NORMAL:LX/FJ8;

    goto :goto_0

    .line 2223085
    :cond_4
    iget-wide v0, p0, LX/FJ6;->j:D

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_5

    .line 2223086
    sget-object v0, LX/FJ8;->WAIT_FOR_NEXT_READY:LX/FJ8;

    goto :goto_0

    .line 2223087
    :cond_5
    iget-wide v0, p0, LX/FJ6;->i:D

    iget-wide v2, p0, LX/FJ6;->j:D

    add-double/2addr v2, v6

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_6

    .line 2223088
    sget-object v0, LX/FJ8;->FADE_OUT:LX/FJ8;

    goto :goto_0

    .line 2223089
    :cond_6
    sget-object v0, LX/FJ8;->FINISHED:LX/FJ8;

    goto :goto_0
.end method
