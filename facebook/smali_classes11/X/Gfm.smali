.class public LX/Gfm;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mW",
        "<",
        "LX/GfY;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/Gg6;

.field private final d:LX/Gfy;

.field private final e:LX/GgA;

.field private final f:LX/GgD;

.field private final g:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;LX/25M;LX/Gfv;LX/Gfy;LX/GgA;LX/GgD;LX/Gg6;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Gfv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/GfY;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;",
            ">;TE;",
            "LX/25M;",
            "LX/Gfv;",
            "LX/Gfy;",
            "LX/GgA;",
            "LX/GgD;",
            "LX/Gg6;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377939
    move-object v5, p4

    check-cast v5, LX/1Pq;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p6

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 2377940
    iput-object p7, p0, LX/Gfm;->d:LX/Gfy;

    .line 2377941
    move-object/from16 v0, p8

    iput-object v0, p0, LX/Gfm;->e:LX/GgA;

    .line 2377942
    move-object/from16 v0, p9

    iput-object v0, p0, LX/Gfm;->f:LX/GgD;

    .line 2377943
    move-object/from16 v0, p10

    iput-object v0, p0, LX/Gfm;->c:LX/Gg6;

    .line 2377944
    iput-object p4, p0, LX/Gfm;->g:LX/1Pn;

    .line 2377945
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2377861
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 5

    .prologue
    .line 2377862
    check-cast p2, LX/GfY;

    .line 2377863
    iget-object v0, p2, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->p()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;

    move-result-object v0

    .line 2377864
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/Gfl;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;)I

    move-result v1

    const v2, 0x4984e12

    if-ne v1, v2, :cond_2

    .line 2377865
    iget-object v0, p0, LX/Gfm;->e:LX/GgA;

    const/4 v1, 0x0

    .line 2377866
    new-instance v2, LX/Gg9;

    invoke-direct {v2, v0}, LX/Gg9;-><init>(LX/GgA;)V

    .line 2377867
    iget-object v3, v0, LX/GgA;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Gg8;

    .line 2377868
    if-nez v3, :cond_0

    .line 2377869
    new-instance v3, LX/Gg8;

    invoke-direct {v3, v0}, LX/Gg8;-><init>(LX/GgA;)V

    .line 2377870
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Gg8;->a$redex0(LX/Gg8;LX/1De;IILX/Gg9;)V

    .line 2377871
    move-object v2, v3

    .line 2377872
    move-object v1, v2

    .line 2377873
    move-object v0, v1

    .line 2377874
    iget-object v1, v0, LX/Gg8;->a:LX/Gg9;

    iput-object p2, v1, LX/Gg9;->a:LX/GfY;

    .line 2377875
    iget-object v1, v0, LX/Gg8;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2377876
    move-object v1, v0

    .line 2377877
    iget-object v0, p0, LX/Gfm;->g:LX/1Pn;

    check-cast v0, LX/1Pp;

    .line 2377878
    iget-object v2, v1, LX/Gg8;->a:LX/Gg9;

    iput-object v0, v2, LX/Gg9;->b:LX/1Pp;

    .line 2377879
    iget-object v2, v1, LX/Gg8;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377880
    move-object v0, v1

    .line 2377881
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2377882
    :goto_0
    iget-object v1, p0, LX/Gfm;->c:LX/Gg6;

    const/4 v2, 0x0

    .line 2377883
    new-instance v3, LX/Gg5;

    invoke-direct {v3, v1}, LX/Gg5;-><init>(LX/Gg6;)V

    .line 2377884
    iget-object v4, v1, LX/Gg6;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Gg4;

    .line 2377885
    if-nez v4, :cond_1

    .line 2377886
    new-instance v4, LX/Gg4;

    invoke-direct {v4, v1}, LX/Gg4;-><init>(LX/Gg6;)V

    .line 2377887
    :cond_1
    invoke-static {v4, p1, v2, v2, v3}, LX/Gg4;->a$redex0(LX/Gg4;LX/1De;IILX/Gg5;)V

    .line 2377888
    move-object v3, v4

    .line 2377889
    move-object v2, v3

    .line 2377890
    move-object v1, v2

    .line 2377891
    iget-object v2, v1, LX/Gg4;->a:LX/Gg5;

    iput-object p2, v2, LX/Gg5;->a:LX/GfY;

    .line 2377892
    iget-object v2, v1, LX/Gg4;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377893
    move-object v1, v1

    .line 2377894
    iget-object v3, v1, LX/Gg4;->a:LX/Gg5;

    if-nez v0, :cond_6

    const/4 v2, 0x0

    :goto_1
    iput-object v2, v3, LX/Gg5;->b:LX/1X1;

    .line 2377895
    iget-object v2, v1, LX/Gg4;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377896
    move-object v1, v1

    .line 2377897
    iget-object v0, p0, LX/Gfm;->g:LX/1Pn;

    check-cast v0, LX/1Pp;

    .line 2377898
    iget-object v2, v1, LX/Gg4;->a:LX/Gg5;

    iput-object v0, v2, LX/Gg5;->c:LX/1Pp;

    .line 2377899
    iget-object v2, v1, LX/Gg4;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377900
    move-object v0, v1

    .line 2377901
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 2377902
    :cond_2
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/Gfl;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;)I

    move-result v0

    const v1, 0x4c808d5

    if-ne v0, v1, :cond_4

    .line 2377903
    iget-object v0, p0, LX/Gfm;->f:LX/GgD;

    const/4 v1, 0x0

    .line 2377904
    new-instance v2, LX/GgC;

    invoke-direct {v2, v0}, LX/GgC;-><init>(LX/GgD;)V

    .line 2377905
    iget-object v3, v0, LX/GgD;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GgB;

    .line 2377906
    if-nez v3, :cond_3

    .line 2377907
    new-instance v3, LX/GgB;

    invoke-direct {v3, v0}, LX/GgB;-><init>(LX/GgD;)V

    .line 2377908
    :cond_3
    invoke-static {v3, p1, v1, v1, v2}, LX/GgB;->a$redex0(LX/GgB;LX/1De;IILX/GgC;)V

    .line 2377909
    move-object v2, v3

    .line 2377910
    move-object v1, v2

    .line 2377911
    move-object v0, v1

    .line 2377912
    iget-object v1, v0, LX/GgB;->a:LX/GgC;

    iput-object p2, v1, LX/GgC;->a:LX/GfY;

    .line 2377913
    iget-object v1, v0, LX/GgB;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2377914
    move-object v1, v0

    .line 2377915
    iget-object v0, p0, LX/Gfm;->g:LX/1Pn;

    check-cast v0, LX/1Pp;

    .line 2377916
    iget-object v2, v1, LX/GgB;->a:LX/GgC;

    iput-object v0, v2, LX/GgC;->b:LX/1Pp;

    .line 2377917
    iget-object v2, v1, LX/GgB;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377918
    move-object v0, v1

    .line 2377919
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 2377920
    :cond_4
    iget-object v0, p0, LX/Gfm;->d:LX/Gfy;

    const/4 v1, 0x0

    .line 2377921
    new-instance v2, LX/Gfx;

    invoke-direct {v2, v0}, LX/Gfx;-><init>(LX/Gfy;)V

    .line 2377922
    iget-object v3, v0, LX/Gfy;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Gfw;

    .line 2377923
    if-nez v3, :cond_5

    .line 2377924
    new-instance v3, LX/Gfw;

    invoke-direct {v3, v0}, LX/Gfw;-><init>(LX/Gfy;)V

    .line 2377925
    :cond_5
    invoke-static {v3, p1, v1, v1, v2}, LX/Gfw;->a$redex0(LX/Gfw;LX/1De;IILX/Gfx;)V

    .line 2377926
    move-object v2, v3

    .line 2377927
    move-object v1, v2

    .line 2377928
    move-object v0, v1

    .line 2377929
    iget-object v1, v0, LX/Gfw;->a:LX/Gfx;

    iput-object p2, v1, LX/Gfx;->a:LX/GfY;

    .line 2377930
    iget-object v1, v0, LX/Gfw;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2377931
    move-object v1, v0

    .line 2377932
    iget-object v0, p0, LX/Gfm;->g:LX/1Pn;

    check-cast v0, LX/1Pp;

    .line 2377933
    iget-object v2, v1, LX/Gfw;->a:LX/Gfx;

    iput-object v0, v2, LX/Gfx;->b:LX/1Pp;

    .line 2377934
    iget-object v2, v1, LX/Gfw;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377935
    move-object v0, v1

    .line 2377936
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto/16 :goto_0

    .line 2377937
    :cond_6
    invoke-virtual {v0}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    goto/16 :goto_1
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2377938
    const/4 v0, 0x0

    return v0
.end method
