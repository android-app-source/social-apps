.class public LX/GiO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Sh;

.field public final b:Ljava/lang/String;

.field public final c:LX/0o8;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/GiN;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0gX;

.field public final f:LX/1My;

.field public g:Z

.field public h:LX/0gM;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0o8;LX/0Sh;LX/0gX;LX/1My;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0o8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2385727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2385728
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GiO;->g:Z

    .line 2385729
    iput-object p1, p0, LX/GiO;->b:Ljava/lang/String;

    .line 2385730
    iput-object p2, p0, LX/GiO;->c:LX/0o8;

    .line 2385731
    iput-object p3, p0, LX/GiO;->a:LX/0Sh;

    .line 2385732
    iput-object p4, p0, LX/GiO;->e:LX/0gX;

    .line 2385733
    iput-object p5, p0, LX/GiO;->f:LX/1My;

    .line 2385734
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/GiO;->d:Ljava/util/Set;

    .line 2385735
    return-void
.end method
