.class public final LX/FGH;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic d:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Lcom/facebook/common/callercontext/CallerContext;ZLcom/facebook/messaging/model/messages/Message;)V
    .locals 0

    .prologue
    .line 2218655
    iput-object p1, p0, LX/FGH;->d:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iput-object p2, p0, LX/FGH;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-boolean p3, p0, LX/FGH;->b:Z

    iput-object p4, p0, LX/FGH;->c:Lcom/facebook/messaging/model/messages/Message;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2218653
    sget-object v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->o:Ljava/lang/Class;

    const-string v1, "Error prefetching stickers."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2218654
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2218643
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2218644
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/service/FetchStickersResult;

    .line 2218645
    iget-object v1, v0, Lcom/facebook/stickers/service/FetchStickersResult;->a:LX/0Px;

    move-object v2, v1

    .line 2218646
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 2218647
    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    if-eqz v4, :cond_1

    .line 2218648
    iget-object v4, p0, LX/FGH;->d:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->e:Landroid/net/Uri;

    iget-object v5, p0, LX/FGH;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-boolean v6, p0, LX/FGH;->b:Z

    iget-object v7, p0, LX/FGH;->c:Lcom/facebook/messaging/model/messages/Message;

    iget-object v7, v7, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v4, v0, v5, v6, v7}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a$redex0(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ZLjava/lang/String;)V

    .line 2218649
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2218650
    :cond_1
    iget-object v4, v0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    if-eqz v4, :cond_0

    .line 2218651
    iget-object v4, p0, LX/FGH;->d:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    iget-object v5, p0, LX/FGH;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-boolean v6, p0, LX/FGH;->b:Z

    iget-object v7, p0, LX/FGH;->c:Lcom/facebook/messaging/model/messages/Message;

    iget-object v7, v7, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v4, v0, v5, v6, v7}, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->a$redex0(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ZLjava/lang/String;)V

    goto :goto_1

    .line 2218652
    :cond_2
    return-void
.end method
