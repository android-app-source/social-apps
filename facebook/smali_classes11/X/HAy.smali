.class public final LX/HAy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/HAz;


# direct methods
.method public constructor <init>(LX/HAz;)V
    .locals 0

    .prologue
    .line 2436294
    iput-object p1, p0, LX/HAy;->a:LX/HAz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10

    .prologue
    .line 2436295
    iget-object v0, p0, LX/HAy;->a:LX/HAz;

    iget-object v0, v0, LX/HAz;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v1, p0, LX/HAy;->a:LX/HAz;

    iget-object v1, v1, LX/HAz;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    .line 2436296
    iget-object v2, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9XE;

    iget-object v3, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->k:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2436297
    iget-object v3, v2, LX/9XE;->a:LX/0Zb;

    sget-object v6, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_BAN_USER:LX/9X6;

    invoke-static {v6, v4, v5}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "banned_user_id"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v3, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2436298
    new-instance v3, LX/4BY;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v3, v2}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 2436299
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f081660

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2436300
    invoke-virtual {v3}, LX/4BY;->show()V

    .line 2436301
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2436302
    const-string v2, "blockUser"

    new-instance v5, Lcom/facebook/friends/methods/BlockUserMethod$Params;

    iget-object v6, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->k:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/facebook/friends/methods/BlockUserMethod$Params;-><init>(JJ)V

    invoke-virtual {v4, v2, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2436303
    iget-object v2, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "contact_us_inbox_ban_person_with_id_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment$6;

    invoke-direct {v6, v0, v4}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment$6;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;Landroid/os/Bundle;)V

    new-instance v4, LX/HB1;

    invoke-direct {v4, v0, v3}, LX/HB1;-><init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;LX/4BY;)V

    invoke-virtual {v2, v5, v6, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2436304
    return-void
.end method
