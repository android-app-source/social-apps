.class public LX/FfN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/Cwo;

.field public final d:LX/Cwp;

.field public final e:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/2Sc;


# direct methods
.method public constructor <init>(LX/0tX;Landroid/content/res/Resources;LX/Cwo;LX/Cwp;LX/1Ck;LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2268091
    iput-object p1, p0, LX/FfN;->a:LX/0tX;

    .line 2268092
    iput-object p2, p0, LX/FfN;->b:Landroid/content/res/Resources;

    .line 2268093
    iput-object p3, p0, LX/FfN;->c:LX/Cwo;

    .line 2268094
    iput-object p4, p0, LX/FfN;->d:LX/Cwp;

    .line 2268095
    iput-object p5, p0, LX/FfN;->e:LX/1Ck;

    .line 2268096
    iput-object p6, p0, LX/FfN;->f:LX/2Sc;

    .line 2268097
    return-void
.end method

.method public static a(LX/CwL;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2268098
    new-instance v0, LX/0lp;

    invoke-direct {v0}, LX/0lp;-><init>()V

    .line 2268099
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 2268100
    :try_start_0
    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v0

    .line 2268101
    invoke-virtual {v0}, LX/0nX;->f()V

    .line 2268102
    const-string v2, "name"

    .line 2268103
    iget-object v3, p0, LX/CwL;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2268104
    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268105
    const-string v2, "action"

    const-string v3, "add"

    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268106
    const-string v2, "value"

    .line 2268107
    iget-object v3, p0, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-object v3, v3

    .line 2268108
    iget-object p0, v3, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    move-object v3, p0

    .line 2268109
    invoke-virtual {v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268110
    invoke-virtual {v0}, LX/0nX;->g()V

    .line 2268111
    invoke-virtual {v0}, LX/0nX;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2268112
    invoke-virtual {v1}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2268113
    :catch_0
    move-exception v0

    .line 2268114
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to convert filters to json string"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
