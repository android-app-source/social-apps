.class public LX/F5Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2198884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2198885
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2198886
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    .line 2198887
    const-string v1, "route"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2198888
    const-string v1, "route"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2198889
    iput-object v1, v0, LX/98r;->b:Ljava/lang/String;

    .line 2198890
    :cond_0
    const-string v1, "uri"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2198891
    if-eqz v1, :cond_3

    .line 2198892
    :goto_0
    move-object v1, v1

    .line 2198893
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2198894
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 2198895
    :cond_1
    const-string v1, "show_search"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2198896
    const-string v1, "show_search"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2198897
    iput-boolean v1, v0, LX/98r;->e:Z

    .line 2198898
    :cond_2
    invoke-static {}, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->m()LX/F5b;

    move-result-object v1

    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/F5b;->a(Landroid/os/Bundle;)LX/F5b;

    move-result-object v0

    invoke-virtual {v0}, LX/F5b;->a()Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    return-object v0

    .line 2198899
    :cond_3
    const-string v1, "route"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v1, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result p0

    sparse-switch p0, :sswitch_data_0

    :cond_4
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 2198900
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 2198901
    :sswitch_0
    const-string p0, "FBGroupsDiscoveryCategoryRoute"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string p0, "FBGroupsDiscoveryTagRoute"

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v1, 0x1

    goto :goto_1

    .line 2198902
    :pswitch_0
    const-string v1, "category_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2198903
    const-string v1, "/groups_discovery_category?category_id=%s"

    const-string v2, "category_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2198904
    :pswitch_1
    const-string v1, "tag_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2198905
    const-string v1, "/groups_discovery_tag?tag_id=%s"

    const-string v2, "tag_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x20438a15 -> :sswitch_0
        0x442370cf -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
