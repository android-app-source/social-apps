.class public LX/GXK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/7iu;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Z

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Z

.field public final k:Z

.field public final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GXM;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Z

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/GXJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Z

.field public final s:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/GX0;",
            ">;"
        }
    .end annotation
.end field

.field public final t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;


# direct methods
.method private constructor <init>(LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0Px;Ljava/lang/String;LX/0am;LX/0am;ZZLX/0Px;LX/0am;ZLX/0Px;LX/GXJ;Ljava/lang/String;ZLX/0am;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p17    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/7iu;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;ZZ",
            "LX/0Px",
            "<",
            "LX/GXM;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ">;",
            "LX/GXJ;",
            "Ljava/lang/String;",
            "Z",
            "LX/0am",
            "<",
            "LX/GX0;",
            ">;",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2363992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2363993
    iput-object p1, p0, LX/GXK;->a:LX/0am;

    .line 2363994
    iput-object p2, p0, LX/GXK;->b:Ljava/lang/String;

    .line 2363995
    iput-object p3, p0, LX/GXK;->c:Ljava/lang/String;

    .line 2363996
    iput-object p4, p0, LX/GXK;->d:Ljava/lang/String;

    .line 2363997
    iput-boolean p5, p0, LX/GXK;->e:Z

    .line 2363998
    iput-object p6, p0, LX/GXK;->f:LX/0Px;

    .line 2363999
    iput-object p7, p0, LX/GXK;->g:Ljava/lang/String;

    .line 2364000
    iput-object p8, p0, LX/GXK;->h:LX/0am;

    .line 2364001
    iput-object p9, p0, LX/GXK;->i:LX/0am;

    .line 2364002
    iput-boolean p10, p0, LX/GXK;->j:Z

    .line 2364003
    iput-boolean p11, p0, LX/GXK;->k:Z

    .line 2364004
    iput-object p12, p0, LX/GXK;->l:LX/0Px;

    .line 2364005
    iput-object p13, p0, LX/GXK;->m:LX/0am;

    .line 2364006
    iput-boolean p14, p0, LX/GXK;->n:Z

    .line 2364007
    move-object/from16 v0, p15

    iput-object v0, p0, LX/GXK;->o:LX/0Px;

    .line 2364008
    move-object/from16 v0, p16

    iput-object v0, p0, LX/GXK;->p:LX/GXJ;

    .line 2364009
    move-object/from16 v0, p17

    iput-object v0, p0, LX/GXK;->q:Ljava/lang/String;

    .line 2364010
    move/from16 v0, p18

    iput-boolean v0, p0, LX/GXK;->r:Z

    .line 2364011
    move-object/from16 v0, p19

    iput-object v0, p0, LX/GXK;->s:LX/0am;

    .line 2364012
    move-object/from16 v0, p20

    iput-object v0, p0, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2364013
    return-void
.end method

.method public synthetic constructor <init>(LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0Px;Ljava/lang/String;LX/0am;LX/0am;ZZLX/0Px;LX/0am;ZLX/0Px;LX/GXJ;Ljava/lang/String;ZLX/0am;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;B)V
    .locals 0

    .prologue
    .line 2363945
    invoke-direct/range {p0 .. p20}, LX/GXK;-><init>(LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0Px;Ljava/lang/String;LX/0am;LX/0am;ZZLX/0Px;LX/0am;ZLX/0Px;LX/GXJ;Ljava/lang/String;ZLX/0am;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2363947
    if-eqz p1, :cond_0

    instance-of v2, p1, LX/GXK;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2363948
    :cond_1
    :goto_0
    return v0

    .line 2363949
    :cond_2
    if-eq p0, p1, :cond_1

    .line 2363950
    check-cast p1, LX/GXK;

    .line 2363951
    iget-object v2, p0, LX/GXK;->a:LX/0am;

    .line 2363952
    iget-object v3, p1, LX/GXK;->a:LX/0am;

    move-object v3, v3

    .line 2363953
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->b:Ljava/lang/String;

    .line 2363954
    iget-object v3, p1, LX/GXK;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2363955
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->c:Ljava/lang/String;

    .line 2363956
    iget-object v3, p1, LX/GXK;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2363957
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->d:Ljava/lang/String;

    .line 2363958
    iget-object v3, p1, LX/GXK;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2363959
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GXK;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2363960
    iget-boolean v3, p1, LX/GXK;->e:Z

    move v3, v3

    .line 2363961
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->f:LX/0Px;

    .line 2363962
    iget-object v3, p1, LX/GXK;->f:LX/0Px;

    move-object v3, v3

    .line 2363963
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->g:Ljava/lang/String;

    .line 2363964
    iget-object v3, p1, LX/GXK;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2363965
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->h:LX/0am;

    .line 2363966
    iget-object v3, p1, LX/GXK;->h:LX/0am;

    move-object v3, v3

    .line 2363967
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->i:LX/0am;

    .line 2363968
    iget-object v3, p1, LX/GXK;->i:LX/0am;

    move-object v3, v3

    .line 2363969
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GXK;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2363970
    iget-boolean v3, p1, LX/GXK;->j:Z

    move v3, v3

    .line 2363971
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GXK;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2363972
    iget-boolean v3, p1, LX/GXK;->k:Z

    move v3, v3

    .line 2363973
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->l:LX/0Px;

    .line 2363974
    iget-object v3, p1, LX/GXK;->l:LX/0Px;

    move-object v3, v3

    .line 2363975
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->m:LX/0am;

    .line 2363976
    iget-object v3, p1, LX/GXK;->m:LX/0am;

    move-object v3, v3

    .line 2363977
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GXK;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2363978
    iget-boolean v3, p1, LX/GXK;->n:Z

    move v3, v3

    .line 2363979
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->o:LX/0Px;

    .line 2363980
    iget-object v3, p1, LX/GXK;->o:LX/0Px;

    move-object v3, v3

    .line 2363981
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->p:LX/GXJ;

    .line 2363982
    iget-object v3, p1, LX/GXK;->p:LX/GXJ;

    move-object v3, v3

    .line 2363983
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->q:Ljava/lang/String;

    .line 2363984
    iget-object v3, p1, LX/GXK;->q:Ljava/lang/String;

    move-object v3, v3

    .line 2363985
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GXK;->r:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2363986
    iget-boolean v3, p1, LX/GXK;->r:Z

    move v3, v3

    .line 2363987
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->s:LX/0am;

    .line 2363988
    iget-object v3, p1, LX/GXK;->s:LX/0am;

    move-object v3, v3

    .line 2363989
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2363990
    iget-object v3, p1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v3, v3

    .line 2363991
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2363946
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/GXK;->a:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/GXK;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/GXK;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/GXK;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, LX/GXK;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/GXK;->f:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/GXK;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, LX/GXK;->h:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, LX/GXK;->i:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, LX/GXK;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, LX/GXK;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, LX/GXK;->l:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, LX/GXK;->m:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-boolean v2, p0, LX/GXK;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, LX/GXK;->o:LX/0Px;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, LX/GXK;->p:LX/GXJ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, LX/GXK;->q:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-boolean v2, p0, LX/GXK;->r:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p0, LX/GXK;->s:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p0, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
