.class public LX/F9j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/F9i;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;


# direct methods
.method public constructor <init>(LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2205819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205820
    iput-object p1, p0, LX/F9j;->a:LX/0dC;

    .line 2205821
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2205822
    check-cast p1, LX/F9i;

    .line 2205823
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/apache/http/NameValuePair;

    const/4 v1, 0x0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "setting"

    invoke-virtual {p1}, LX/F9i;->name()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "phone_id"

    iget-object v4, p0, LX/F9j;->a:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2205824
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "FriendFinderContinuousSyncSettingPost"

    .line 2205825
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2205826
    move-object v1, v1

    .line 2205827
    const-string v2, "POST"

    .line 2205828
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2205829
    move-object v1, v1

    .line 2205830
    const-string v2, "method/FriendFinderContinuousSyncSettingPost"

    .line 2205831
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2205832
    move-object v1, v1

    .line 2205833
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2205834
    move-object v0, v1

    .line 2205835
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2205836
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2205837
    move-object v0, v0

    .line 2205838
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2205817
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2205818
    const/4 v0, 0x0

    return-object v0
.end method
