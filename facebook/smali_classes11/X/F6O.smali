.class public LX/F6O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/F6J;

.field private final d:LX/9hF;


# direct methods
.method public constructor <init>(LX/5pX;Landroid/app/Activity;LX/0Ot;LX/9hF;LX/F6K;)V
    .locals 1
    .param p1    # LX/5pX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pX;",
            "Landroid/app/Activity;",
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;",
            "LX/9hF;",
            "LX/F6K;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200389
    iput-object p2, p0, LX/F6O;->a:Landroid/app/Activity;

    .line 2200390
    iput-object p3, p0, LX/F6O;->b:LX/0Ot;

    .line 2200391
    iput-object p4, p0, LX/F6O;->d:LX/9hF;

    .line 2200392
    invoke-virtual {p5, p1}, LX/F6K;->a(LX/5pX;)LX/F6J;

    move-result-object v0

    iput-object v0, p0, LX/F6O;->c:LX/F6J;

    .line 2200393
    return-void
.end method
