.class public LX/F9h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;",
        "Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2205787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205788
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2205765
    check-cast p1, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;

    .line 2205766
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2205767
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205768
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "pic_size"

    .line 2205769
    iget v2, p1, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->a:I

    move v2, v2

    .line 2205770
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205771
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "session_ID"

    .line 2205772
    iget-object v2, p1, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2205773
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205774
    iget-object v0, p1, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->c:LX/89v;

    move-object v0, v0

    .line 2205775
    if-eqz v0, :cond_0

    sget-object v1, LX/89v;->UNKNOWN:LX/89v;

    invoke-virtual {v0, v1}, LX/89v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2205776
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "flow"

    iget-object v0, v0, LX/89v;->value:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205777
    :cond_0
    iget-object v0, p1, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;->d:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2205778
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2205779
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "excluded_ids"

    .line 2205780
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 2205781
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2205782
    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 2205783
    :cond_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 2205784
    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2205785
    :cond_2
    new-instance v0, LX/14N;

    const-string v1, "FriendFinderPymk"

    const-string v2, "POST"

    const-string v3, "method/friendfinder.pymk"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2205786
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;

    return-object v0
.end method
