.class public final LX/GuN;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2403284
    iput-object p1, p0, LX/GuN;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403285
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403286
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2403287
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403288
    const-string v1, "callback"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2403289
    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2403290
    :goto_0
    return-void

    .line 2403291
    :cond_0
    iget-object v0, p0, LX/GuN;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2403292
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 2403293
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403294
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2403295
    const-string v0, "UNKNOWN"

    .line 2403296
    :cond_1
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v6, "install_non_market_apps"

    invoke-static {v1, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-ne v1, v2, :cond_2

    move v1, v2

    .line 2403297
    :goto_1
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 2403298
    :try_start_1
    const-string v7, "package_name"

    invoke-virtual {v6, v7, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2403299
    const-string v5, "installer_package"

    invoke-virtual {v6, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2403300
    const-string v0, "unknown_sources_checked"

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2403301
    :goto_2
    new-array v0, v2, [Ljava/lang/Object;

    aput-object v6, v0, v3

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v4, v0, v1}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;Ljava/util/List;LX/BWL;)V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 2403302
    goto :goto_1

    .line 2403303
    :catch_0
    move-exception v1

    .line 2403304
    iget-object v6, p0, LX/GuN;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v6}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Error checking if install from unknown sources is allowed"

    new-array v8, v3, [Ljava/lang/Object;

    invoke-static {v6, v1, v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v3

    goto :goto_1

    .line 2403305
    :catch_1
    move-exception v0

    .line 2403306
    iget-object v1, p0, LX/GuN;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v1

    const-string v5, "unexpected json error"

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v5, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method
