.class public LX/FI5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/FI4;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FI5;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/FI5;
    .locals 3

    .prologue
    .line 2221416
    sget-object v0, LX/FI5;->a:LX/FI5;

    if-nez v0, :cond_1

    .line 2221417
    const-class v1, LX/FI5;

    monitor-enter v1

    .line 2221418
    :try_start_0
    sget-object v0, LX/FI5;->a:LX/FI5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2221419
    if-eqz v2, :cond_0

    .line 2221420
    :try_start_1
    new-instance v0, LX/FI5;

    invoke-direct {v0}, LX/FI5;-><init>()V

    .line 2221421
    move-object v0, v0

    .line 2221422
    sput-object v0, LX/FI5;->a:LX/FI5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221423
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2221424
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2221425
    :cond_1
    sget-object v0, LX/FI5;->a:LX/FI5;

    return-object v0

    .line 2221426
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2221427
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2221428
    check-cast p1, LX/FI4;

    .line 2221429
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "end_stream_upload"

    .line 2221430
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2221431
    move-object v0, v0

    .line 2221432
    const-string v1, "POST"

    .line 2221433
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2221434
    move-object v0, v0

    .line 2221435
    new-instance v1, Ljava/lang/StringBuilder;

    const-string p0, "messenger_videos/"

    invoke-direct {v1, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, p1, LX/FI4;->a:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p0, "/?phase=end"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2221436
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2221437
    move-object v0, v0

    .line 2221438
    const/4 v1, 0x1

    .line 2221439
    iput-boolean v1, v0, LX/14O;->o:Z

    .line 2221440
    move-object v0, v0

    .line 2221441
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2221442
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "Stream-Id"

    iget-object p0, p1, LX/FI4;->b:Ljava/lang/String;

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221443
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2221444
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2221445
    move-object v0, v0

    .line 2221446
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2221447
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2221448
    move-object v0, v0

    .line 2221449
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2221450
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2221451
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 2221452
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2221453
    new-instance v0, Lorg/apache/http/HttpException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Video segment transcoding upload failed. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2221454
    :cond_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2221455
    const-string v1, "media_id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
