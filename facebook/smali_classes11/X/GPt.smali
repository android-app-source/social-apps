.class public abstract enum LX/GPt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GPt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GPt;

.field public static final enum NO_SUFFIX:LX/GPt;

.field public static final enum OPTIONAL:LX/GPt;

.field public static final enum REQUIRED:LX/GPt;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2349613
    new-instance v0, LX/GPu;

    const-string v1, "OPTIONAL"

    invoke-direct {v0, v1, v2}, LX/GPu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPt;->OPTIONAL:LX/GPt;

    .line 2349614
    new-instance v0, LX/GPv;

    const-string v1, "REQUIRED"

    invoke-direct {v0, v1, v3}, LX/GPv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPt;->REQUIRED:LX/GPt;

    .line 2349615
    new-instance v0, LX/GPw;

    const-string v1, "NO_SUFFIX"

    invoke-direct {v0, v1, v4}, LX/GPw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPt;->NO_SUFFIX:LX/GPt;

    .line 2349616
    const/4 v0, 0x3

    new-array v0, v0, [LX/GPt;

    sget-object v1, LX/GPt;->OPTIONAL:LX/GPt;

    aput-object v1, v0, v2

    sget-object v1, LX/GPt;->REQUIRED:LX/GPt;

    aput-object v1, v0, v3

    sget-object v1, LX/GPt;->NO_SUFFIX:LX/GPt;

    aput-object v1, v0, v4

    sput-object v0, LX/GPt;->$VALUES:[LX/GPt;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2349611
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GPt;
    .locals 1

    .prologue
    .line 2349617
    const-class v0, LX/GPt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GPt;

    return-object v0
.end method

.method public static values()[LX/GPt;
    .locals 1

    .prologue
    .line 2349612
    sget-object v0, LX/GPt;->$VALUES:[LX/GPt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GPt;

    return-object v0
.end method


# virtual methods
.method public abstract getHint(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
.end method
