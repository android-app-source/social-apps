.class public LX/Gcg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Jv;


# static fields
.field public static final a:Ljava/lang/String;

.field public static b:LX/6Jt;


# instance fields
.field public A:Ljava/lang/String;

.field public final B:Ljava/util/concurrent/locks/Lock;

.field public C:I

.field public D:I

.field public final E:LX/6Il;

.field public final c:Ljava/lang/String;

.field public d:LX/0Zr;

.field public e:Landroid/content/res/Resources;

.field public f:Ljava/util/concurrent/ExecutorService;

.field public g:Landroid/os/Handler;

.field public h:LX/6KO;

.field public i:LX/6KT;

.field public j:Landroid/content/Context;

.field private k:Ljava/util/concurrent/ExecutorService;

.field public l:Lcom/facebook/facedetection/detector/MacerFaceDetector;

.field public m:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

.field public n:LX/6KN;

.field public o:LX/6Ia;

.field public p:LX/6JF;

.field public q:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

.field public r:LX/6KB;

.field public s:LX/6JB;

.field public volatile t:LX/6JR;

.field public u:LX/6JC;

.field public v:LX/7yL;

.field public w:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

.field public x:LX/1FZ;

.field public y:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

.field public z:Ljava/nio/ByteBuffer;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mFaceDetectorLock"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2372184
    const-class v0, LX/Gcg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Gcg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Zr;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/6KO;LX/6KT;Landroid/content/Context;Lcom/facebook/facedetection/detector/MacerFaceDetector;LX/7yL;LX/10M;LX/10O;LX/2J4;LX/1FZ;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2372185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2372186
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/Gcg;->c:Ljava/lang/String;

    .line 2372187
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    .line 2372188
    const/4 v1, -0x1

    iput v1, p0, LX/Gcg;->C:I

    .line 2372189
    const/4 v1, -0x1

    iput v1, p0, LX/Gcg;->D:I

    .line 2372190
    new-instance v1, LX/Gcb;

    invoke-direct {v1, p0}, LX/Gcb;-><init>(LX/Gcg;)V

    iput-object v1, p0, LX/Gcg;->E:LX/6Il;

    .line 2372191
    iput-object p2, p0, LX/Gcg;->d:LX/0Zr;

    .line 2372192
    iput-object p3, p0, LX/Gcg;->e:Landroid/content/res/Resources;

    .line 2372193
    iput-object p4, p0, LX/Gcg;->f:Ljava/util/concurrent/ExecutorService;

    .line 2372194
    iput-object p5, p0, LX/Gcg;->k:Ljava/util/concurrent/ExecutorService;

    .line 2372195
    iput-object p6, p0, LX/Gcg;->g:Landroid/os/Handler;

    .line 2372196
    iput-object p7, p0, LX/Gcg;->h:LX/6KO;

    .line 2372197
    iput-object p8, p0, LX/Gcg;->i:LX/6KT;

    .line 2372198
    iput-object p9, p0, LX/Gcg;->j:Landroid/content/Context;

    .line 2372199
    iput-object p10, p0, LX/Gcg;->l:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    .line 2372200
    iput-object p11, p0, LX/Gcg;->v:LX/7yL;

    .line 2372201
    move-object/from16 v0, p14

    invoke-virtual {v0, p12, p13}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v1

    iput-object v1, p0, LX/Gcg;->w:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 2372202
    iput-object p1, p0, LX/Gcg;->A:Ljava/lang/String;

    .line 2372203
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Gcg;->x:LX/1FZ;

    .line 2372204
    return-void
.end method

.method private a(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 2372205
    iget-object v0, p0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2372206
    :goto_0
    return-void

    .line 2372207
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 2372208
    iget-object v0, p0, LX/Gcg;->z:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    .line 2372209
    iget-object v0, p0, LX/Gcg;->t:LX/6JR;

    iget v0, v0, LX/6JR;->a:I

    iget-object v1, p0, LX/Gcg;->t:LX/6JR;

    iget v1, v1, LX/6JR;->b:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    iput-object v0, p0, LX/Gcg;->z:Ljava/nio/ByteBuffer;

    .line 2372210
    :cond_1
    iget-object v0, p0, LX/Gcg;->z:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 2372211
    if-eqz p1, :cond_2

    .line 2372212
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 2372213
    iget-object v0, p0, LX/Gcg;->z:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2372214
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/Gcg;->k:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionManager$4;-><init>(LX/Gcg;Ljava/nio/ByteBuffer;)V

    const v2, -0x4ff8ee86

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2372215
    :try_start_2
    iget-object v0, p0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2372216
    iget-object v0, p0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 2372217
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2372218
    :catchall_1
    move-exception v0

    iget-object v1, p0, LX/Gcg;->B:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static e(LX/Gcg;)I
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    .line 2372219
    iget-object v0, p0, LX/Gcg;->j:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2372220
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    return v0
.end method

.method public static g(LX/Gcg;)LX/6J9;
    .locals 1

    .prologue
    .line 2372221
    iget-object v0, p0, LX/Gcg;->q:Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    .line 2372222
    iget-boolean p0, v0, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->d:Z

    move v0, p0

    .line 2372223
    if-eqz v0, :cond_0

    .line 2372224
    sget-object v0, LX/6J9;->CAMERA1:LX/6J9;

    .line 2372225
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/6J7;->a()LX/6J9;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/6Js;)V
    .locals 1

    .prologue
    .line 2372226
    if-eqz p1, :cond_0

    .line 2372227
    sget-object v0, LX/7Sc;->INPUT_PREVIEW:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 2372228
    sget-object v0, LX/7Sc;->INPUT_ROTATION:LX/7Sc;

    invoke-virtual {p1, p0, v0}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 2372229
    :cond_0
    return-void
.end method

.method public final a(LX/7Sb;)V
    .locals 2

    .prologue
    .line 2372230
    sget-object v0, LX/Gcf;->a:[I

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2372231
    :goto_0
    return-void

    .line 2372232
    :pswitch_0
    check-cast p1, LX/7Sk;

    .line 2372233
    iget-object v0, p1, LX/7Sk;->a:[B

    move-object v0, v0

    .line 2372234
    if-eqz v0, :cond_0

    .line 2372235
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 2372236
    :goto_1
    invoke-direct {p0, v0}, LX/Gcg;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 2372237
    :cond_0
    iget-object v0, p1, LX/7Sk;->b:[LX/6JH;

    move-object v0, v0

    .line 2372238
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-interface {v0}, LX/6JH;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1

    .line 2372239
    :pswitch_1
    check-cast p1, LX/7Sm;

    .line 2372240
    iget v0, p1, LX/7Sm;->a:I

    move v0, v0

    .line 2372241
    iput v0, p0, LX/Gcg;->C:I

    .line 2372242
    iget v0, p1, LX/7Sm;->b:I

    move v0, v0

    .line 2372243
    iput v0, p0, LX/Gcg;->D:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2372244
    iget-object v0, p0, LX/Gcg;->m:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    if-eqz v0, :cond_0

    .line 2372245
    sget-object v0, LX/Gcg;->b:LX/6Jt;

    iget-object v1, p0, LX/Gcg;->m:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v0, v1}, LX/6Jt;->a(Landroid/view/View;)V

    .line 2372246
    :cond_0
    iget-object v0, p0, LX/Gcg;->o:LX/6Ia;

    if-eqz v0, :cond_1

    .line 2372247
    iget-object v0, p0, LX/Gcg;->o:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->b()V

    .line 2372248
    :cond_1
    return-void
.end method
