.class public final LX/Fe3;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Fe6;

.field public final synthetic c:LX/Fe5;


# direct methods
.method public constructor <init>(LX/Fe5;Ljava/lang/String;LX/Fe6;)V
    .locals 0

    .prologue
    .line 2265042
    iput-object p1, p0, LX/Fe3;->c:LX/Fe5;

    iput-object p2, p0, LX/Fe3;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Fe3;->b:LX/Fe6;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 4

    .prologue
    .line 2265043
    iget-object v0, p0, LX/Fe3;->c:LX/Fe5;

    iget-object v1, p0, LX/Fe3;->b:LX/Fe6;

    .line 2265044
    iget-object v2, v0, LX/Fe5;->d:LX/Cvq;

    .line 2265045
    sget-object v3, LX/Cvx;->a:LX/Cvv;

    const-string p0, "network_operation"

    const/4 p1, 0x1

    invoke-static {v2, v3, p0, p1}, LX/Cvq;->a(LX/Cvq;LX/0Pq;Ljava/lang/String;Z)V

    .line 2265046
    invoke-static {v2}, LX/Cvq;->g(LX/Cvq;)V

    .line 2265047
    const/4 v2, 0x0

    iput-object v2, v0, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2265048
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/Fe6;->n:Z

    .line 2265049
    invoke-static {v1}, LX/Fe6;->h(LX/Fe6;)V

    .line 2265050
    return-void
.end method

.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2265051
    iget-object v0, p0, LX/Fe3;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2265052
    iget-object v0, p0, LX/Fe3;->c:LX/Fe5;

    sget-object v1, LX/3Ql;->FETCH_GRAPH_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    iget-object v2, p0, LX/Fe3;->b:LX/Fe6;

    .line 2265053
    invoke-static {v0, v1, p1}, LX/Fe5;->a(LX/Fe5;LX/3Ql;Ljava/lang/Exception;)V

    .line 2265054
    const/4 v0, 0x0

    iput-boolean v0, v2, LX/Fe6;->n:Z

    .line 2265055
    sget-object v0, LX/EQG;->ERROR:LX/EQG;

    invoke-static {v2, v0}, LX/Fe6;->a(LX/Fe6;LX/EQG;)V

    .line 2265056
    invoke-static {v2}, LX/Fe6;->h(LX/Fe6;)V

    .line 2265057
    :goto_0
    return-void

    .line 2265058
    :cond_0
    iget-object v0, p0, LX/Fe3;->c:LX/Fe5;

    sget-object v1, LX/3Ql;->FETCH_MORE_GRAPH_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    iget-object v2, p0, LX/Fe3;->b:LX/Fe6;

    .line 2265059
    invoke-static {v0, v1, p1}, LX/Fe5;->a(LX/Fe5;LX/3Ql;Ljava/lang/Exception;)V

    .line 2265060
    const/4 v0, 0x0

    iput-boolean v0, v2, LX/Fe6;->n:Z

    .line 2265061
    sget-object v0, LX/EQG;->ERROR_LOADING_MORE:LX/EQG;

    invoke-static {v2, v0}, LX/Fe6;->a(LX/Fe6;LX/EQG;)V

    .line 2265062
    invoke-static {v2}, LX/Fe6;->h(LX/Fe6;)V

    .line 2265063
    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2265064
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2265065
    iget-object v1, p0, LX/Fe3;->c:LX/Fe5;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->j()Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    move-result-object v0

    iget-object v2, p0, LX/Fe3;->b:LX/Fe6;

    iget-object v3, p0, LX/Fe3;->a:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    .line 2265066
    iget-object v4, v1, LX/Fe5;->d:LX/Cvq;

    .line 2265067
    sget-object p0, LX/Cvx;->a:LX/Cvv;

    const-string p1, "network_operation"

    invoke-static {v4, p0, p1}, LX/Cvq;->b(LX/Cvq;LX/0Pq;Ljava/lang/String;)V

    .line 2265068
    invoke-static {v4}, LX/Cvq;->g(LX/Cvq;)V

    .line 2265069
    const/4 v4, 0x0

    iput-object v4, v1, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2265070
    invoke-virtual {v2, v0, v3}, LX/Fe6;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;Z)V

    .line 2265071
    return-void
.end method
