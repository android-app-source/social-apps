.class public LX/GH3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;


# instance fields
.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/GGX;

.field private k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/GE6;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/1Ck;

.field public m:LX/2U3;


# direct methods
.method public constructor <init>(LX/0Or;LX/1Ck;LX/2U3;LX/GEq;LX/GEr;LX/GEt;LX/GEs;LX/GEy;LX/GJx;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/GE6;",
            ">;",
            "LX/1Ck;",
            "LX/2U3;",
            "LX/GEq;",
            "LX/GEr;",
            "LX/GEt;",
            "LX/GEs;",
            "LX/GEy;",
            "LX/GJx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334676
    new-instance v0, LX/GH1;

    invoke-direct {v0, p0}, LX/GH1;-><init>(LX/GH3;)V

    iput-object v0, p0, LX/GH3;->j:LX/GGX;

    .line 2334677
    iput-object p1, p0, LX/GH3;->k:LX/0Or;

    .line 2334678
    iput-object p2, p0, LX/GH3;->l:LX/1Ck;

    .line 2334679
    iput-object p3, p0, LX/GH3;->m:LX/2U3;

    .line 2334680
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    invoke-virtual {v0, p7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030061

    iget-object v3, p0, LX/GH3;->j:LX/GGX;

    sget-object v4, LX/8wK;->FOOTER:LX/8wK;

    invoke-direct {v1, v2, p9, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GH3;->i:LX/0Px;

    .line 2334681
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334682
    iget-object v0, p0, LX/GH3;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 7

    .prologue
    .line 2334683
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2334684
    if-eqz v0, :cond_1

    .line 2334685
    invoke-interface {p2, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334686
    :cond_0
    :goto_0
    return-void

    .line 2334687
    :cond_1
    const-string v0, "storyId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2334688
    const-string v0, "intent"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 2334689
    if-eqz v2, :cond_0

    .line 2334690
    iget-object v3, p0, LX/GH3;->l:LX/1Ck;

    sget-object v4, LX/GE5;->FETCH_INSIGHTS:LX/GE5;

    iget-object v1, p0, LX/GH3;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GE6;

    .line 2334691
    const-string v5, "no storyId"

    invoke-static {v2, v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334692
    iget-object v5, v1, LX/GE6;->a:LX/0tX;

    .line 2334693
    new-instance v6, LX/ABu;

    invoke-direct {v6}, LX/ABu;-><init>()V

    move-object v6, v6

    .line 2334694
    const-string p1, "story_id"

    invoke-virtual {v6, p1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    move-object v6, v6

    .line 2334695
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    move-object v6, v6

    .line 2334696
    invoke-virtual {v5, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 2334697
    new-instance v6, LX/GE4;

    invoke-direct {v6, v1, v2}, LX/GE4;-><init>(LX/GE6;Ljava/lang/String;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p1

    invoke-static {v5, v6, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v1, v5

    .line 2334698
    new-instance v2, LX/GH2;

    invoke-direct {v2, p0, v0, p2}, LX/GH2;-><init>(LX/GH3;Landroid/content/Intent;LX/GCY;)V

    invoke-virtual {v3, v4, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
