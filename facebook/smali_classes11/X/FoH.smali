.class public final LX/FoH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2289263
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2289264
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2289265
    if-eqz v0, :cond_0

    .line 2289266
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2289267
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2289268
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2289269
    if-eqz v0, :cond_1

    .line 2289270
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2289271
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2289272
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2289273
    if-eqz v0, :cond_7

    .line 2289274
    const-string v1, "suggested_cover_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2289275
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2289276
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 2289277
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result v3

    .line 2289278
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2289279
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2289280
    if-eqz v4, :cond_4

    .line 2289281
    const-string v5, "focus"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2289282
    const-wide/16 v10, 0x0

    .line 2289283
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2289284
    const/4 v6, 0x0

    invoke-virtual {p0, v4, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2289285
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_2

    .line 2289286
    const-string v8, "x"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2289287
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 2289288
    :cond_2
    const/4 v6, 0x1

    invoke-virtual {p0, v4, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2289289
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_3

    .line 2289290
    const-string v8, "y"

    invoke-virtual {p2, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2289291
    invoke-virtual {p2, v6, v7}, LX/0nX;->a(D)V

    .line 2289292
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2289293
    :cond_4
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2289294
    if-eqz v4, :cond_5

    .line 2289295
    const-string v5, "photo"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2289296
    invoke-static {p0, v4, p2, p3}, LX/FoG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2289297
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2289298
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2289299
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2289300
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2289301
    return-void
.end method
