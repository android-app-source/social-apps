.class public final LX/Fzz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;)V
    .locals 0

    .prologue
    .line 2308371
    iput-object p1, p0, LX/Fzz;->a:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2308372
    iget-object v0, p0, LX/Fzz;->a:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->p:LX/Fzo;

    iget-object v1, p0, LX/Fzz;->a:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    iget-object v1, v1, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    sget-object v2, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308373
    iget-object v3, v0, LX/Fzo;->b:LX/Fzy;

    .line 2308374
    invoke-static {}, LX/G3u;->c()LX/G3r;

    move-result-object v6

    .line 2308375
    const-string v7, "profile_id"

    invoke-virtual {v6, v7, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2308376
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    const-string v7, "FETCH_MEMORIAL_COVER_PHOTO_QUERY_TAG"

    invoke-static {v7}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v7

    .line 2308377
    iput-object v7, v6, LX/0zO;->d:Ljava/util/Set;

    .line 2308378
    move-object v6, v6

    .line 2308379
    sget-object v7, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v6, v7}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v6

    sget-object v7, LX/0zS;->a:LX/0zS;

    invoke-virtual {v6, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    .line 2308380
    iput-object v2, v6, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308381
    move-object v6, v6

    .line 2308382
    const-wide/16 v8, 0xe10

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    .line 2308383
    iget-object v7, v3, LX/Fzy;->c:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2308384
    new-instance v7, LX/Fzp;

    invoke-direct {v7, v3}, LX/Fzp;-><init>(LX/Fzy;)V

    iget-object v8, v3, LX/Fzy;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v3, v6

    .line 2308385
    new-instance v4, LX/Fzg;

    invoke-direct {v4, v0}, LX/Fzg;-><init>(LX/Fzo;)V

    iget-object v5, v0, LX/Fzo;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2308386
    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2308387
    invoke-direct {p0}, LX/Fzz;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
