.class public LX/F5k;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/F5j;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Landroid/content/Context;

.field public c:LX/DKO;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F5o;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Sh;

.field public final f:LX/DKP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2199177
    const-class v0, LX/F5k;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/F5k;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/DKO;LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2199180
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2199181
    new-instance v0, LX/F5i;

    invoke-direct {v0, p0}, LX/F5i;-><init>(LX/F5k;)V

    iput-object v0, p0, LX/F5k;->f:LX/DKP;

    .line 2199182
    iput-object p1, p0, LX/F5k;->b:Landroid/content/Context;

    .line 2199183
    iput-object p2, p0, LX/F5k;->c:LX/DKO;

    .line 2199184
    iput-object p3, p0, LX/F5k;->e:LX/0Sh;

    .line 2199185
    return-void
.end method

.method public static b(LX/0QB;)LX/F5k;
    .locals 4

    .prologue
    .line 2199178
    new-instance v3, LX/F5k;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/DKO;->a(LX/0QB;)LX/DKO;

    move-result-object v1

    check-cast v1, LX/DKO;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-direct {v3, v0, v1, v2}, LX/F5k;-><init>(Landroid/content/Context;LX/DKO;LX/0Sh;)V

    .line 2199179
    return-object v3
.end method

.method public static g(LX/F5k;)I
    .locals 2

    .prologue
    .line 2199176
    iget-object v0, p0, LX/F5k;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b208c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public static j(LX/F5k;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2199186
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 2199187
    iget-object v1, p0, LX/F5k;->b:Landroid/content/Context;

    move-object v1, v1

    .line 2199188
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v2, 0x400000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 2199189
    iget-object v1, p0, LX/F5k;->b:Landroid/content/Context;

    move-object v1, v1

    .line 2199190
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v1

    move v1, v1

    .line 2199191
    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2199174
    new-instance v0, LX/F60;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/F60;-><init>(Landroid/content/Context;)V

    .line 2199175
    new-instance v1, LX/F5j;

    invoke-direct {v1, v0}, LX/F5j;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2199148
    check-cast p1, LX/F5j;

    .line 2199149
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    .line 2199150
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    .line 2199151
    const/4 v2, 0x0

    move v2, v2

    .line 2199152
    sub-int/2addr v0, v2

    if-ge p2, v0, :cond_0

    move-object v0, v1

    .line 2199153
    check-cast v0, LX/F60;

    .line 2199154
    iget-object v2, p0, LX/F5k;->d:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/F5o;

    .line 2199155
    invoke-virtual {v0, v2}, LX/F60;->a(LX/F5o;)V

    .line 2199156
    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [I

    .line 2199157
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v2

    if-ge p2, v2, :cond_1

    .line 2199158
    const/4 v2, 0x0

    invoke-static {p0}, LX/F5k;->g(LX/F5k;)I

    move-result v3

    aput v3, v0, v2

    .line 2199159
    const/4 v2, 0x3

    .line 2199160
    iget-object v3, p0, LX/F5k;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b208d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move v3, v3

    .line 2199161
    aput v3, v0, v2

    .line 2199162
    :cond_1
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p2, v2, :cond_2

    invoke-static {p0}, LX/F5k;->j(LX/F5k;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    if-nez p2, :cond_5

    invoke-static {p0}, LX/F5k;->j(LX/F5k;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2199163
    if-eqz v2, :cond_4

    .line 2199164
    const/4 v2, 0x2

    invoke-static {p0}, LX/F5k;->g(LX/F5k;)I

    move-result v3

    aput v3, v0, v2

    .line 2199165
    :cond_4
    move-object v0, v0

    .line 2199166
    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x2

    aget v4, v0, v4

    const/4 v5, 0x3

    aget v0, v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 2199167
    return-void

    :cond_5
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2199173
    sget-object v0, LX/F5l;->GYSC_ROW:LX/F5l;

    invoke-virtual {v0}, LX/F5l;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2199168
    iget-object v0, p0, LX/F5k;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2199169
    const/4 v0, 0x0

    .line 2199170
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/F5k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2199171
    const/4 v1, 0x0

    move v1, v1

    .line 2199172
    add-int/2addr v0, v1

    goto :goto_0
.end method
