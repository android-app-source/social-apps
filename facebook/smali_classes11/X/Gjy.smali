.class public final LX/Gjy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Lcom/facebook/greetingcards/verve/model/VMDeck;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

.field private final b:LX/Gk3;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;LX/Gk3;)V
    .locals 0

    .prologue
    .line 2388643
    iput-object p1, p0, LX/Gjy;->a:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2388644
    iput-object p2, p0, LX/Gjy;->b:LX/Gk3;

    .line 2388645
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2388646
    :try_start_0
    iget-object v0, p0, LX/Gjy;->a:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->c:LX/Gk0;

    iget-object v1, p0, LX/Gjy;->b:LX/Gk3;

    invoke-virtual {v0, v1, p1}, LX/2Vx;->a(LX/2WG;Ljava/io/InputStream;)LX/1gI;

    .line 2388647
    iget-object v0, p0, LX/Gjy;->a:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->c:LX/Gk0;

    iget-object v1, p0, LX/Gjy;->b:LX/Gk3;

    invoke-virtual {v0, v1}, LX/2Vx;->b(LX/2WG;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMDeck;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2388648
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0
.end method
