.class public final LX/Gyd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/location/ui/LocationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 0

    .prologue
    .line 2410236
    iput-object p1, p0, LX/Gyd;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x8ccb995

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2410237
    iget-object v1, p0, LX/Gyd;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v1, v1, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v1, v1, LX/1rv;->a:LX/0yG;

    invoke-static {v1}, LX/1rv;->b(LX/0yG;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2410238
    iget-object v1, p0, LX/Gyd;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v1, v1, Lcom/facebook/location/ui/LocationSettingsFragment;->M:LX/1rv;

    iget-object v1, v1, LX/1rv;->a:LX/0yG;

    sget-object v2, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v1, v2, :cond_0

    .line 2410239
    iget-object v1, p0, LX/Gyd;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    .line 2410240
    iget-object v2, v1, Lcom/facebook/location/ui/LocationSettingsFragment;->t:LX/6Zb;

    new-instance v3, LX/2si;

    invoke-direct {v3}, LX/2si;-><init>()V

    const-string p0, "surface_location_settings"

    const-string p1, "mechanism_turn_on_button"

    invoke-virtual {v2, v3, p0, p1}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2410241
    :goto_0
    const v1, 0x76415c07

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2410242
    :cond_0
    iget-object v1, p0, LX/Gyd;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    .line 2410243
    iget-object v2, v1, Lcom/facebook/location/ui/LocationSettingsFragment;->s:LX/0i5;

    sget-object v3, LX/3KE;->a:[Ljava/lang/String;

    new-instance p0, LX/GyX;

    invoke-direct {p0, v1}, LX/GyX;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {v2, v3, p0}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2410244
    goto :goto_0
.end method
