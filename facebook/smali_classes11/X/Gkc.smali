.class public final LX/Gkc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Gkq;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;LX/Gkq;I)V
    .locals 0

    .prologue
    .line 2389197
    iput-object p1, p0, LX/Gkc;->c:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iput-object p2, p0, LX/Gkc;->a:LX/Gkq;

    iput p3, p0, LX/Gkc;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2389184
    iget-object v0, p0, LX/Gkc;->c:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v2, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    iget-object v0, p0, LX/Gkc;->a:LX/Gkq;

    .line 2389185
    iget-object v3, v0, LX/Gkq;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2389186
    iget-object v0, p0, LX/Gkc;->a:LX/Gkq;

    .line 2389187
    iget-boolean v4, v0, LX/Gkq;->g:Z

    move v0, v4

    .line 2389188
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v4, p0, LX/Gkc;->a:LX/Gkq;

    .line 2389189
    iget-boolean v5, v4, LX/Gkq;->h:Z

    move v4, v5

    .line 2389190
    invoke-interface {v2, v3, v0, v4}, LX/Gkk;->a(Ljava/lang/String;ZZ)V

    .line 2389191
    iget-object v0, p0, LX/Gkc;->c:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v2, v0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->g:LX/Glj;

    iget-object v0, p0, LX/Gkc;->a:LX/Gkq;

    .line 2389192
    iget-boolean v3, v0, LX/Gkq;->g:Z

    move v0, v3

    .line 2389193
    if-eqz v0, :cond_1

    const-string v0, "remove_favorite"

    :goto_1
    iget v3, p0, LX/Gkc;->b:I

    iget-object v4, p0, LX/Gkc;->a:LX/Gkq;

    iget-object v5, p0, LX/Gkc;->c:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v5, v5, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-virtual {v5}, LX/GlX;->d()LX/Gkm;

    move-result-object v5

    invoke-virtual {v2, v0, v3, v4, v5}, LX/Glj;->a(Ljava/lang/String;ILX/Gkq;LX/Gkm;)V

    .line 2389194
    return v1

    .line 2389195
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2389196
    :cond_1
    const-string v0, "add_favorite"

    goto :goto_1
.end method
