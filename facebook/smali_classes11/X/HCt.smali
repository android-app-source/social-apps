.class public final LX/HCt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2440184
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2440185
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440186
    :goto_0
    return v1

    .line 2440187
    :cond_0
    const-string v6, "has_custom_ordering"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2440188
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 2440189
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2440190
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2440191
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2440192
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2440193
    const-string v6, "actions"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2440194
    invoke-static {p0, p1}, LX/9Yh;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2440195
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2440196
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2440197
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2440198
    if-eqz v0, :cond_4

    .line 2440199
    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2440200
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2440173
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2440174
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440175
    if-eqz v0, :cond_0

    .line 2440176
    const-string v1, "actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440177
    invoke-static {p0, v0, p2, p3}, LX/9Yh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2440178
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2440179
    if-eqz v0, :cond_1

    .line 2440180
    const-string v1, "has_custom_ordering"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440181
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2440182
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2440183
    return-void
.end method
