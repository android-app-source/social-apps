.class public final LX/FOR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4yu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/4yu",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/4yu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yu",
            "<TT;>;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method private constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2235129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235130
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/0RZ;->i(Ljava/util/Iterator;)LX/4yu;

    move-result-object v0

    iput-object v0, p0, LX/FOR;->a:LX/4yu;

    .line 2235131
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    iput v0, p0, LX/FOR;->b:I

    .line 2235132
    return-void
.end method

.method public static a(Ljava/util/Collection;)LX/FOR;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;)",
            "LX/FOR",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2235133
    new-instance v0, LX/FOR;

    invoke-direct {v0, p0}, LX/FOR;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 2235134
    iget-object v0, p0, LX/FOR;->a:LX/4yu;

    invoke-interface {v0}, LX/4yu;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2235135
    iget v0, p0, LX/FOR;->b:I

    return v0
.end method

.method public final hasNext()Z
    .locals 1

    .prologue
    .line 2235136
    iget-object v0, p0, LX/FOR;->a:LX/4yu;

    invoke-interface {v0}, LX/4yu;->hasNext()Z

    move-result v0

    return v0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 2235137
    iget-object v0, p0, LX/FOR;->a:LX/4yu;

    invoke-interface {v0}, LX/4yu;->next()Ljava/lang/Object;

    move-result-object v0

    .line 2235138
    iget v1, p0, LX/FOR;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/FOR;->b:I

    .line 2235139
    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 2235140
    iget-object v0, p0, LX/FOR;->a:LX/4yu;

    invoke-interface {v0}, LX/4yu;->remove()V

    .line 2235141
    return-void
.end method
