.class public LX/FBX;
.super LX/398;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile b:LX/FBX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2209512
    sget-object v0, LX/0ax;->ae:Ljava/lang/String;

    sput-object v0, LX/FBX;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2209513
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2209514
    sget-object v0, LX/0ax;->ae:Ljava/lang/String;

    const-class v1, Lcom/facebook/katana/orca/DiodeMessengerActivity;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2209515
    sget-object v0, LX/0ax;->aj:Ljava/lang/String;

    const-class v1, Lcom/facebook/katana/orca/DiodeMessengerActivity;

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "click_through"

    aput-object v3, v2, v4

    sget-object v3, LX/B9u;->CREATE_THREAD:LX/B9u;

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2209516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{%s}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "canonical_thread_user"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/orca/DiodeMessengerActivity;

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "click_through"

    aput-object v3, v2, v4

    sget-object v3, LX/B9u;->CANONICAL_THREAD:LX/B9u;

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2209517
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{%s}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "canonical_thread_user"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/orca/DiodeMessengerActivity;

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "click_through"

    aput-object v3, v2, v4

    sget-object v3, LX/B9u;->CANONICAL_THREAD:LX/B9u;

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2209518
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->ap:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{%s}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "thread_id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/orca/DiodeMessengerActivity;

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "click_through"

    aput-object v3, v2, v4

    sget-object v3, LX/B9u;->LEGACY_THREAD_ID:LX/B9u;

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2209519
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->an:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{%s}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "group_thread_fbid"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/orca/DiodeMessengerActivity;

    new-array v2, v6, [Ljava/lang/Object;

    const-string v3, "click_through"

    aput-object v3, v2, v4

    sget-object v3, LX/B9u;->GROUP_THREAD:LX/B9u;

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2209520
    return-void
.end method

.method public static a(LX/0QB;)LX/FBX;
    .locals 3

    .prologue
    .line 2209521
    sget-object v0, LX/FBX;->b:LX/FBX;

    if-nez v0, :cond_1

    .line 2209522
    const-class v1, LX/FBX;

    monitor-enter v1

    .line 2209523
    :try_start_0
    sget-object v0, LX/FBX;->b:LX/FBX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2209524
    if-eqz v2, :cond_0

    .line 2209525
    :try_start_1
    new-instance v0, LX/FBX;

    invoke-direct {v0}, LX/FBX;-><init>()V

    .line 2209526
    move-object v0, v0

    .line 2209527
    sput-object v0, LX/FBX;->b:LX/FBX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2209528
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2209529
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2209530
    :cond_1
    sget-object v0, LX/FBX;->b:LX/FBX;

    return-object v0

    .line 2209531
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2209532
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
