.class public LX/Gfl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/1nA;

.field public final b:LX/0bH;

.field private final c:LX/17Q;

.field public final d:LX/0Zb;

.field public final e:LX/GeG;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field public final g:LX/1DR;

.field public final h:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/1nA;LX/0bH;LX/17Q;LX/0Zb;LX/GeG;Lcom/facebook/content/SecureContextHelper;LX/1DR;Landroid/content/Context;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2377834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2377835
    iput-object p1, p0, LX/Gfl;->a:LX/1nA;

    .line 2377836
    iput-object p2, p0, LX/Gfl;->b:LX/0bH;

    .line 2377837
    iput-object p3, p0, LX/Gfl;->c:LX/17Q;

    .line 2377838
    iput-object p4, p0, LX/Gfl;->d:LX/0Zb;

    .line 2377839
    iput-object p5, p0, LX/Gfl;->e:LX/GeG;

    .line 2377840
    iput-object p6, p0, LX/Gfl;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2377841
    iput-object p7, p0, LX/Gfl;->g:LX/1DR;

    .line 2377842
    iput-object p8, p0, LX/Gfl;->h:Landroid/content/Context;

    .line 2377843
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2377847
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2377848
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 2377849
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Gfl;
    .locals 12

    .prologue
    .line 2377850
    const-class v1, LX/Gfl;

    monitor-enter v1

    .line 2377851
    :try_start_0
    sget-object v0, LX/Gfl;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377852
    sput-object v2, LX/Gfl;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377853
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377854
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377855
    new-instance v3, LX/Gfl;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v4

    check-cast v4, LX/1nA;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v6

    check-cast v6, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/GeG;->b(LX/0QB;)LX/GeG;

    move-result-object v8

    check-cast v8, LX/GeG;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v10

    check-cast v10, LX/1DR;

    const-class v11, Landroid/content/Context;

    invoke-interface {v0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-direct/range {v3 .. v11}, LX/Gfl;-><init>(LX/1nA;LX/0bH;LX/17Q;LX/0Zb;LX/GeG;Lcom/facebook/content/SecureContextHelper;LX/1DR;Landroid/content/Context;)V

    .line 2377856
    move-object v0, v3

    .line 2377857
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377858
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gfl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377859
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377860
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2377846
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->af()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->af()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->af()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPage;)V
    .locals 3

    .prologue
    .line 2377844
    iget-object v0, p0, LX/Gfl;->a:LX/1nA;

    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2377845
    return-void
.end method
