.class public LX/GEs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GK0;

.field private b:LX/0W3;


# direct methods
.method public constructor <init>(LX/GK0;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332133
    iput-object p1, p0, LX/GEs;->a:LX/GK0;

    .line 2332134
    iput-object p2, p0, LX/GEs;->b:LX/0W3;

    .line 2332135
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332136
    const v0, 0x7f030068

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 4

    .prologue
    .line 2332137
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->b()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/GEs;->b:LX/0W3;

    sget-wide v2, LX/0X5;->k:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2332138
    :cond_0
    const/4 v0, 0x0

    .line 2332139
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332140
    iget-object v0, p0, LX/GEs;->a:LX/GK0;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332141
    sget-object v0, LX/8wK;->INSIGHTS_MESSAGES:LX/8wK;

    return-object v0
.end method
