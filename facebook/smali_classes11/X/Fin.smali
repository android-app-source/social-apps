.class public LX/Fin;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Fil;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2275790
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Fin;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275791
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2275792
    iput-object p1, p0, LX/Fin;->b:LX/0Ot;

    .line 2275793
    return-void
.end method

.method public static a(LX/0QB;)LX/Fin;
    .locals 4

    .prologue
    .line 2275794
    const-class v1, LX/Fin;

    monitor-enter v1

    .line 2275795
    :try_start_0
    sget-object v0, LX/Fin;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275796
    sput-object v2, LX/Fin;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275797
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275798
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275799
    new-instance v3, LX/Fin;

    const/16 p0, 0x34f3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Fin;-><init>(LX/0Ot;)V

    .line 2275800
    move-object v0, v3

    .line 2275801
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275802
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fin;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275803
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2275805
    check-cast p2, LX/Fim;

    .line 2275806
    iget-object v0, p0, LX/Fin;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;

    iget-object v1, p2, LX/Fim;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    const/4 p2, 0x2

    const/4 p0, 0x1

    const/4 v8, 0x0

    .line 2275807
    iget-object v2, v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->b:LX/8i7;

    iget-object v3, v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->d:LX/0Uh;

    invoke-static {v2, v1, v3}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->a(LX/8i7;Lcom/facebook/search/model/EntityTypeaheadUnit;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 2275808
    invoke-static {v1}, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityPartDefinition;->c(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/String;

    move-result-object v4

    .line 2275809
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x6

    const v6, 0x7f0b0083

    invoke-interface {v2, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x7

    const v6, 0x7f0b17b2

    invoke-interface {v2, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    .line 2275810
    iget-object v2, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2275811
    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0e086d

    invoke-static {p1, v8, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0e0877

    invoke-static {p1, v8, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2275812
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v6

    iget-object v2, v0, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    .line 2275813
    iget-object v7, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v7, v7

    .line 2275814
    invoke-virtual {v2, v7}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v2

    sget-object v7, Lcom/facebook/search/typeahead/rows/SearchTypeaheadEntityComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const v6, 0x7f010202

    invoke-interface {v2, v6}, LX/1Di;->h(I)LX/1Di;

    move-result-object v2

    const v6, 0x7f010202

    invoke-interface {v2, v6}, LX/1Di;->p(I)LX/1Di;

    move-result-object v2

    const/4 v6, 0x5

    const v7, 0x7f0b17b1

    invoke-interface {v2, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2275815
    invoke-static {}, LX/1dS;->b()V

    .line 2275816
    const/4 v0, 0x0

    return-object v0
.end method
