.class public final LX/F6N;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F6O;


# direct methods
.method public constructor <init>(LX/F6O;)V
    .locals 0

    .prologue
    .line 2200374
    iput-object p1, p0, LX/F6N;->a:LX/F6O;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2200375
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2200376
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2200377
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2200378
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2200379
    :cond_0
    :goto_0
    return-void

    .line 2200380
    :cond_1
    invoke-static {v0}, LX/9hF;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9hE;

    move-result-object v0

    .line 2200381
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2200382
    sget-object v2, LX/74S;->NEWSFEED:LX/74S;

    invoke-virtual {v0, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/9hD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9hD;

    .line 2200383
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 2200384
    iget-object v0, p0, LX/F6N;->a:LX/F6O;

    iget-object v0, v0, LX/F6O;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    iget-object v2, p0, LX/F6N;->a:LX/F6O;

    iget-object v2, v2, LX/F6O;->a:Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v3}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    goto :goto_0
.end method
