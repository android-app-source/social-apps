.class public final LX/FH5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Landroid/net/Uri;",
        "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Z

.field public final synthetic c:LX/FHf;

.field public final synthetic d:Ljava/lang/Integer;

.field public final synthetic e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;ZLX/FHf;Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 2219836
    iput-object p1, p0, LX/FH5;->e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FH5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-boolean p3, p0, LX/FH5;->b:Z

    iput-object p4, p0, LX/FH5;->c:LX/FHf;

    iput-object p5, p0, LX/FH5;->d:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2219837
    check-cast p1, Landroid/net/Uri;

    .line 2219838
    iget-object v0, p0, LX/FH5;->e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->k(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v2

    .line 2219839
    if-eqz p1, :cond_0

    .line 2219840
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    .line 2219841
    iput-object p1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2219842
    move-object v0, v0

    .line 2219843
    const-string v1, "application/octet-stream"

    .line 2219844
    iput-object v1, v0, LX/5zn;->q:Ljava/lang/String;

    .line 2219845
    move-object v0, v0

    .line 2219846
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    move-object v2, v0

    .line 2219847
    :cond_0
    iget-object v0, p0, LX/FH5;->e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-boolean v3, p0, LX/FH5;->b:Z

    iget-object v4, p0, LX/FH5;->c:LX/FHf;

    iget-object v5, p0, LX/FH5;->d:Ljava/lang/Integer;

    const/4 v6, 0x0

    .line 2219848
    invoke-static/range {v0 .. v6}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;ZLX/FHf;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v0, v7

    .line 2219849
    if-eqz p1, :cond_1

    .line 2219850
    iget-object v1, p0, LX/FH5;->e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v2, p0, LX/FH5;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->q:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2219851
    new-instance v3, LX/FGq;

    invoke-direct {v3, v1, p1, v2}, LX/FGq;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Landroid/net/Uri;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    move-object v1, v3

    .line 2219852
    iget-object v2, p0, LX/FH5;->e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v2, v2, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->t:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2219853
    :cond_1
    return-object v0
.end method
