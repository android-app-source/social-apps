.class public final LX/GTm;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLInterfaces$BackgroundLocationNuxQuery$Actor;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 0

    .prologue
    .line 2355822
    iput-object p1, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;)V
    .locals 11
    .param p1    # Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 2355827
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2355828
    :cond_0
    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    const-string v1, "background_location_traveling_nux_actor_info_abnormal"

    const-string v2, "traveling nux does not have profile pic uri or viewer name"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2355829
    :goto_0
    return-void

    .line 2355830
    :cond_1
    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->R:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;->b()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->C:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2355831
    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->q(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2355832
    :goto_1
    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v6, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->S:Landroid/widget/TextView;

    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->q:LX/GVF;

    const v1, 0x7f08379e

    const v3, 0x7f08379f

    const v4, 0x7f0a010d

    const v5, 0x7f0a00d2

    .line 2355833
    iget-object v8, v0, LX/GVF;->b:Landroid/content/res/Resources;

    const/4 v9, 0x2

    new-array v9, v9, [LX/47s;

    const/4 v10, 0x0

    invoke-static {v0, v2, v4}, LX/GVF;->a(LX/GVF;Ljava/lang/String;I)LX/47s;

    move-result-object p1

    aput-object p1, v9, v10

    const/4 v10, 0x1

    iget-object p1, v0, LX/GVF;->b:Landroid/content/res/Resources;

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1, v5}, LX/GVF;->a(LX/GVF;Ljava/lang/String;I)LX/47s;

    move-result-object p1

    aput-object p1, v9, v10

    invoke-static {v8, v1, v9}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v8

    move-object v0, v8

    .line 2355834
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2355835
    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Q:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2355836
    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->T:Landroid/view/View;

    new-instance v1, LX/GU9;

    iget-object v2, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-virtual {v2}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GU9;-><init>(Landroid/content/res/Resources;)V

    invoke-static {v0, v1}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2355837
    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->T:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2355838
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2355824
    sget-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->B:Ljava/lang/Class;

    const-string v1, "failed to get actor info"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355825
    iget-object v0, p0, LX/GTm;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    const-string v1, "background_location_traveling_nux_actor_info_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355826
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2355823
    check-cast p1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;

    invoke-direct {p0, p1}, LX/GTm;->a(Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$ActorModel;)V

    return-void
.end method
