.class public final LX/Ge3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ge4;


# direct methods
.method public constructor <init>(LX/Ge4;)V
    .locals 0

    .prologue
    .line 2374994
    iput-object p1, p0, LX/Ge3;->a:LX/Ge4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2374995
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2374996
    if-eqz p1, :cond_0

    .line 2374997
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2374998
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2374999
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2375000
    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gf()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v0

    goto :goto_0
.end method
