.class public LX/Gbz;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/10M;

.field public d:I

.field public e:LX/10N;

.field public f:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/10M;LX/10N;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2370891
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2370892
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Gbz;->b:Ljava/util/List;

    .line 2370893
    iput-object p1, p0, LX/Gbz;->a:Landroid/content/Context;

    .line 2370894
    iput-object p2, p0, LX/Gbz;->c:LX/10M;

    .line 2370895
    iput-object p3, p0, LX/Gbz;->e:LX/10N;

    .line 2370896
    iput-object p4, p0, LX/Gbz;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2370897
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 12

    .prologue
    .line 2370836
    iget-object v0, p0, LX/Gbz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2370837
    iget-object v0, p0, LX/Gbz;->c:LX/10M;

    invoke-virtual {v0}, LX/10M;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2370838
    iget-object v0, p0, LX/Gbz;->b:Ljava/util/List;

    iget-object v1, p0, LX/Gbz;->c:LX/10M;

    invoke-virtual {v1}, LX/10M;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2370839
    const/4 v4, 0x0

    .line 2370840
    iget-object v2, p0, LX/Gbz;->e:LX/10N;

    const/4 v3, 0x1

    .line 2370841
    iget-object v5, v2, LX/10N;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v6, LX/26p;->k:LX/0Tn;

    invoke-interface {v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->e(LX/0Tn;)Ljava/util/SortedMap;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/SortedMap;->size()I

    move-result v5

    move v5, v5

    .line 2370842
    invoke-virtual {v2}, LX/10N;->i()Z

    move-result v6

    if-nez v6, :cond_5

    .line 2370843
    iget-object v6, v2, LX/10N;->c:LX/0Uh;

    const/16 v7, 0x10

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    move v6, v6

    .line 2370844
    if-eqz v6, :cond_0

    if-eq v5, v3, :cond_2

    .line 2370845
    :cond_0
    iget-object v6, v2, LX/10N;->c:LX/0Uh;

    const/16 v7, 0x11

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    move v6, v6

    .line 2370846
    if-eqz v6, :cond_1

    const/4 v6, 0x2

    if-ge v5, v6, :cond_2

    .line 2370847
    :cond_1
    iget-object v6, v2, LX/10N;->c:LX/0Uh;

    const/16 v7, 0x12

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    move v6, v6

    .line 2370848
    if-eqz v6, :cond_5

    const/4 v6, 0x3

    if-lt v5, v6, :cond_5

    :cond_2
    :goto_0
    move v2, v3

    .line 2370849
    if-eqz v2, :cond_3

    .line 2370850
    new-instance v2, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    const-string v3, "add_user"

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    const-string v9, ""

    const-string v11, ""

    move v10, v4

    invoke-direct/range {v2 .. v11}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2370851
    iget-object v3, p0, LX/Gbz;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2370852
    :cond_3
    const v0, 0xb767ea4

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2370853
    const/4 v0, 0x1

    .line 2370854
    :goto_1
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2370890
    iget-object v0, p0, LX/Gbz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2370889
    iget-object v0, p0, LX/Gbz;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2370888
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2370855
    if-nez p2, :cond_0

    .line 2370856
    iget-object v0, p0, LX/Gbz;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, LX/Gbz;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2370857
    :cond_0
    invoke-virtual {p0, p1}, LX/Gbz;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2370858
    const v1, 0x7f0d0c10

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    .line 2370859
    const-string v2, "add_user"

    iget-object v3, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2370860
    const v2, 0x7f02007b

    invoke-static {v2}, LX/1bX;->a(I)LX/1bX;

    move-result-object v2

    sget-object v3, LX/1bb;->DEFAULT:LX/1bb;

    .line 2370861
    iput-object v3, v2, LX/1bX;->f:LX/1bb;

    .line 2370862
    move-object v2, v2

    .line 2370863
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    move-object v2, v2

    .line 2370864
    invoke-virtual {v1, v2}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setImageRequest(LX/1bf;)V

    .line 2370865
    :goto_0
    const v1, 0x7f0d0bf2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2370866
    iget-object v2, p0, LX/Gbz;->e:LX/10N;

    invoke-virtual {v2}, LX/10N;->i()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mFullName:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2370867
    iget-object v2, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mFullName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2370868
    :goto_1
    const/4 p3, 0x0

    .line 2370869
    iget-object v1, p0, LX/Gbz;->e:LX/10N;

    .line 2370870
    iget-object v2, v1, LX/10N;->c:LX/0Uh;

    const/16 v3, 0x24

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 2370871
    if-nez v1, :cond_4

    .line 2370872
    :cond_1
    :goto_2
    return-object p2

    .line 2370873
    :cond_2
    iget-object v2, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setImage(Ljava/lang/String;)V

    goto :goto_0

    .line 2370874
    :cond_3
    iget-object v2, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2370875
    :cond_4
    const v1, 0x7f0d0c11

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2370876
    if-eqz v1, :cond_1

    .line 2370877
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2370878
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2370879
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->a()Ljava/lang/String;

    move-result-object v2

    .line 2370880
    :goto_3
    move-object v2, v2

    .line 2370881
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2370882
    iget-object v3, p0, LX/Gbz;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/0hM;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    invoke-interface {v3, v2, p3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 2370883
    if-lez v3, :cond_1

    .line 2370884
    const v2, 0x7f0d0c12

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2370885
    iget-object v4, p0, LX/Gbz;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f016a

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, p3

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2370886
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2370887
    invoke-virtual {v1, p3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3
.end method
