.class public final LX/Fkj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;)V
    .locals 0

    .prologue
    .line 2278878
    iput-object p1, p0, LX/Fkj;->a:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2278879
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2278880
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2278881
    if-eqz p1, :cond_1

    .line 2278882
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278883
    if-eqz v0, :cond_1

    .line 2278884
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278885
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;

    .line 2278886
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->a(Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->a(Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->a(Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2278887
    if-eqz v1, :cond_1

    .line 2278888
    iget-object v1, p0, LX/Fkj;->a:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    .line 2278889
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2278890
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->j()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    sget-object p1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2278891
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2278892
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->k()I

    move-result p0

    iput p0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2278893
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->k()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel$HeartsImageModel;->a()I

    move-result p0

    iput p0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2278894
    iget-object p0, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2278895
    :cond_0
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2278896
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->h:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object p0

    sget-object p1, LX/Fkk;->VIEWER:LX/Fkk;

    invoke-static {v1, v2, p0, p1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->a(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;LX/Fkk;)V

    .line 2278897
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->i:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object p0

    sget-object p1, LX/Fkk;->FRIEND:LX/Fkk;

    invoke-static {v1, v2, p0, p1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->a(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;LX/Fkk;)V

    .line 2278898
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->j:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->l()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;

    move-result-object p0

    sget-object p1, LX/Fkk;->NONPROFIT:LX/Fkk;

    invoke-static {v1, v2, p0, p1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->a(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiaryItemFragmentModel;LX/Fkk;)V

    .line 2278899
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->k:Lcom/facebook/fig/listitem/FigListItem;

    const/4 p0, 0x4

    invoke-virtual {v2, p0}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2278900
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->k:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySelectorQueryModel;->m()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2278901
    iget-object v2, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->k:Lcom/facebook/fig/listitem/FigListItem;

    new-instance p0, LX/Fkh;

    invoke-direct {p0, v1}, LX/Fkh;-><init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;)V

    invoke-virtual {v2, p0}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2278902
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
