.class public final LX/H6T;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile s:LX/H6T;


# instance fields
.field public final a:LX/0SG;

.field private final b:Ljava/text/SimpleDateFormat;

.field public final c:LX/17W;

.field public final d:LX/0wM;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Zb;

.field private final i:LX/H82;

.field public final j:LX/17Y;

.field public final k:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation
.end field

.field public final l:LX/H60;

.field public final m:LX/4nT;

.field private final n:LX/0y2;

.field public final o:LX/0y3;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Zi;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/17d;

.field public final r:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/11T;LX/0SG;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/0wM;LX/0Ot;LX/0Ot;LX/0Zb;LX/H82;LX/17Y;LX/0TD;LX/H60;LX/4nT;LX/0y2;LX/0y3;LX/0Ot;LX/17d;LX/0Or;)V
    .locals 2
    .param p11    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p18    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11T;",
            "LX/0SG;",
            "LX/17W;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0wM;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Zb;",
            "LX/H82;",
            "LX/17Y;",
            "LX/0TD;",
            "LX/H60;",
            "LX/4nT;",
            "LX/0y2;",
            "LX/0y3;",
            "LX/0Ot",
            "<",
            "LX/6Zi;",
            ">;",
            "LX/17d;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2426775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2426776
    invoke-virtual {p1}, LX/11T;->f()Ljava/text/SimpleDateFormat;

    move-result-object v1

    iput-object v1, p0, LX/H6T;->b:Ljava/text/SimpleDateFormat;

    .line 2426777
    iput-object p2, p0, LX/H6T;->a:LX/0SG;

    .line 2426778
    iput-object p3, p0, LX/H6T;->c:LX/17W;

    .line 2426779
    iput-object p5, p0, LX/H6T;->d:LX/0wM;

    .line 2426780
    iput-object p4, p0, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2426781
    iput-object p6, p0, LX/H6T;->f:LX/0Ot;

    .line 2426782
    iput-object p7, p0, LX/H6T;->g:LX/0Ot;

    .line 2426783
    iput-object p8, p0, LX/H6T;->h:LX/0Zb;

    .line 2426784
    iput-object p9, p0, LX/H6T;->i:LX/H82;

    .line 2426785
    iput-object p10, p0, LX/H6T;->j:LX/17Y;

    .line 2426786
    iput-object p11, p0, LX/H6T;->k:LX/0TD;

    .line 2426787
    iput-object p12, p0, LX/H6T;->l:LX/H60;

    .line 2426788
    iput-object p13, p0, LX/H6T;->m:LX/4nT;

    .line 2426789
    move-object/from16 v0, p14

    iput-object v0, p0, LX/H6T;->n:LX/0y2;

    .line 2426790
    move-object/from16 v0, p15

    iput-object v0, p0, LX/H6T;->o:LX/0y3;

    .line 2426791
    move-object/from16 v0, p16

    iput-object v0, p0, LX/H6T;->p:LX/0Ot;

    .line 2426792
    move-object/from16 v0, p17

    iput-object v0, p0, LX/H6T;->q:LX/17d;

    .line 2426793
    move-object/from16 v0, p18

    iput-object v0, p0, LX/H6T;->r:LX/0Or;

    .line 2426794
    return-void
.end method

.method public static a(LX/0QB;)LX/H6T;
    .locals 3

    .prologue
    .line 2426795
    sget-object v0, LX/H6T;->s:LX/H6T;

    if-nez v0, :cond_1

    .line 2426796
    const-class v1, LX/H6T;

    monitor-enter v1

    .line 2426797
    :try_start_0
    sget-object v0, LX/H6T;->s:LX/H6T;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2426798
    if-eqz v2, :cond_0

    .line 2426799
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/H6T;->b(LX/0QB;)LX/H6T;

    move-result-object v0

    sput-object v0, LX/H6T;->s:LX/H6T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2426800
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2426801
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2426802
    :cond_1
    sget-object v0, LX/H6T;->s:LX/H6T;

    return-object v0

    .line 2426803
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2426804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "storyFieldToString"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2426805
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/H6T;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2426806
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 2426807
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2426808
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2426809
    :goto_0
    return-object v0

    .line 2426810
    :catch_0
    move-exception v0

    .line 2426811
    const-string v1, "OfferRenderingUtils"

    const-string v2, "Could not get share_id out of the story field id"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2426812
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/H6T;
    .locals 21

    .prologue
    .line 2426813
    new-instance v2, LX/H6T;

    invoke-static/range {p0 .. p0}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v3

    check-cast v3, LX/11T;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    const/16 v8, 0x3be

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xbc6

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/H82;->a(LX/0QB;)LX/H82;

    move-result-object v11

    check-cast v11, LX/H82;

    invoke-static/range {p0 .. p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v12

    check-cast v12, LX/17Y;

    invoke-static/range {p0 .. p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/H60;->a(LX/0QB;)LX/H60;

    move-result-object v14

    check-cast v14, LX/H60;

    invoke-static/range {p0 .. p0}, LX/4nT;->a(LX/0QB;)LX/4nT;

    move-result-object v15

    check-cast v15, LX/4nT;

    invoke-static/range {p0 .. p0}, LX/0y2;->a(LX/0QB;)LX/0y2;

    move-result-object v16

    check-cast v16, LX/0y2;

    invoke-static/range {p0 .. p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v17

    check-cast v17, LX/0y3;

    const/16 v18, 0x262e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v19

    check-cast v19, LX/17d;

    const/16 v20, 0x15e7

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-direct/range {v2 .. v20}, LX/H6T;-><init>(LX/11T;LX/0SG;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/0wM;LX/0Ot;LX/0Ot;LX/0Zb;LX/H82;LX/17Y;LX/0TD;LX/H60;LX/4nT;LX/0y2;LX/0y3;LX/0Ot;LX/17d;LX/0Or;)V

    .line 2426814
    return-object v2
.end method

.method public static b(LX/H83;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2426815
    invoke-virtual {p0}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2426816
    iget-object v0, p0, LX/H83;->b:LX/H70;

    move-object v0, v0

    .line 2426817
    invoke-interface {v0}, LX/H70;->E()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/H6T;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;)Ljava/lang/String;

    move-result-object v0

    .line 2426818
    :goto_0
    return-object v0

    .line 2426819
    :cond_0
    invoke-virtual {p0}, LX/H83;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2426820
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/H6T;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2426821
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/H83;)Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2426682
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H83;->A()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H83;->B()D

    move-result-wide v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H83;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2426822
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-direct {v1, p1, p2}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    new-instance v2, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    new-instance v3, LX/7N4;

    invoke-direct {v3, p1}, LX/7N4;-><init>(Landroid/content/Context;)V

    new-instance v4, LX/Ctw;

    invoke-direct {v4, p1}, LX/Ctw;-><init>(Landroid/content/Context;)V

    new-instance v5, LX/3Gh;

    invoke-direct {v5, p1}, LX/3Gh;-><init>(Landroid/content/Context;)V

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/H83;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2426770
    iget-object v0, p0, LX/H6T;->h:LX/0Zb;

    iget-object v1, p0, LX/H6T;->i:LX/H82;

    const-string v2, "opened_link"

    invoke-virtual {v1, v2, p1, p2}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2426771
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2426772
    iget-object v0, p0, LX/H6T;->j:LX/17Y;

    sget-object v1, LX/0ax;->hE:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2426773
    iget-object v1, p0, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2426774
    return-void
.end method

.method public final a(Landroid/content/Context;LX/H83;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2426758
    invoke-virtual {p2}, LX/H83;->i()Ljava/lang/String;

    move-result-object v2

    .line 2426759
    iget-object v0, p2, LX/H83;->b:LX/H70;

    move-object v0, v0

    .line 2426760
    if-eqz v0, :cond_4

    .line 2426761
    invoke-virtual {p2}, LX/H83;->w()LX/H72;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2426762
    invoke-virtual {p2}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v0

    .line 2426763
    :goto_0
    invoke-static {p2}, LX/H6T;->b(LX/H83;)Ljava/lang/String;

    move-result-object v7

    move-object v6, v0

    .line 2426764
    :goto_1
    invoke-virtual {p2}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    move-result-object v5

    .line 2426765
    :goto_2
    invoke-virtual {p2}, LX/H83;->G()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2426766
    invoke-virtual {p2}, LX/H83;->M()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/H83;->m()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {p0, p1, v6, v7, v1}, LX/H6T;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2426767
    :goto_3
    return-void

    :cond_1
    move-object v5, v1

    .line 2426768
    goto :goto_2

    .line 2426769
    :cond_2
    invoke-virtual {p2}, LX/H83;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, LX/H83;->f()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, LX/H6T;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    move-object v7, v1

    move-object v6, v1

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;LX/H83;Ljava/lang/String;)V
    .locals 11
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2426683
    invoke-virtual {p2}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v0

    .line 2426684
    if-nez v0, :cond_0

    .line 2426685
    const/4 v1, 0x0

    .line 2426686
    :goto_0
    move-object v0, v1

    .line 2426687
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2426688
    iget-object v0, p0, LX/H6T;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v3, 0x0

    iget-object v1, p0, LX/H6T;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    sget-object v4, LX/21D;->OFFERS_SPACE:LX/21D;

    const-string v5, "offerOrCouponShare"

    invoke-interface {v1, v2, v4, v5}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    .line 2426689
    iget-object v1, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2426690
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    invoke-static {v1}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v1

    invoke-virtual {v1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-interface {v0, v3, v1, p1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2426691
    iget-object v0, p0, LX/H6T;->h:LX/0Zb;

    iget-object v1, p0, LX/H6T;->i:LX/H82;

    const-string v2, "share_offer"

    invoke-virtual {v1, v2, p2, p3}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2426692
    return-void

    .line 2426693
    :cond_0
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    .line 2426694
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2426695
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2426696
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 2426697
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    .line 2426698
    if-nez v1, :cond_3

    .line 2426699
    const/4 v5, 0x0

    .line 2426700
    :goto_2
    move-object v1, v5

    .line 2426701
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2426702
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2426703
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2426704
    iput-object v1, v3, LX/23u;->k:LX/0Px;

    .line 2426705
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2426706
    iput-object v1, v3, LX/23u;->N:Ljava/lang/String;

    .line 2426707
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->c()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;

    move-result-object v1

    .line 2426708
    if-nez v1, :cond_9

    .line 2426709
    const/4 v2, 0x0

    .line 2426710
    :goto_3
    move-object v1, v2

    .line 2426711
    iput-object v1, v3, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 2426712
    invoke-virtual {v3}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    goto/16 :goto_0

    .line 2426713
    :cond_3
    new-instance v7, LX/39x;

    invoke-direct {v7}, LX/39x;-><init>()V

    .line 2426714
    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 2426715
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2426716
    const/4 v5, 0x0

    move v6, v5

    :goto_4
    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_4

    .line 2426717
    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;

    .line 2426718
    if-nez v5, :cond_6

    .line 2426719
    const/4 v9, 0x0

    .line 2426720
    :goto_5
    move-object v5, v9

    .line 2426721
    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2426722
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_4

    .line 2426723
    :cond_4
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2426724
    iput-object v5, v7, LX/39x;->b:LX/0Px;

    .line 2426725
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->c()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;

    move-result-object v5

    .line 2426726
    if-nez v5, :cond_7

    .line 2426727
    const/4 v6, 0x0

    .line 2426728
    :goto_6
    move-object v5, v6

    .line 2426729
    iput-object v5, v7, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2426730
    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2426731
    iput-object v5, v7, LX/39x;->t:Ljava/lang/String;

    .line 2426732
    invoke-virtual {v7}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    goto :goto_2

    .line 2426733
    :cond_6
    new-instance v9, LX/4Ys;

    invoke-direct {v9}, LX/4Ys;-><init>()V

    .line 2426734
    invoke-virtual {v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    .line 2426735
    iput-object v10, v9, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2426736
    invoke-virtual {v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v10

    .line 2426737
    iput-object v10, v9, LX/4Ys;->bD:Ljava/lang/String;

    .line 2426738
    invoke-virtual {v9}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v9

    goto :goto_5

    .line 2426739
    :cond_7
    new-instance v6, LX/4XB;

    invoke-direct {v6}, LX/4XB;-><init>()V

    .line 2426740
    invoke-virtual {v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 2426741
    iput-object v8, v6, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2426742
    invoke-virtual {v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$MediaModel;->a()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2426743
    if-nez v8, :cond_8

    .line 2426744
    const/4 v10, 0x0

    .line 2426745
    :goto_7
    move-object v8, v10

    .line 2426746
    iput-object v8, v6, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2426747
    invoke-virtual {v6}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    goto :goto_6

    .line 2426748
    :cond_8
    new-instance v10, LX/2dc;

    invoke-direct {v10}, LX/2dc;-><init>()V

    .line 2426749
    const/4 v5, 0x0

    invoke-virtual {v9, v8, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2426750
    iput-object v5, v10, LX/2dc;->h:Ljava/lang/String;

    .line 2426751
    invoke-virtual {v10}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    goto :goto_7

    .line 2426752
    :cond_9
    new-instance v2, LX/170;

    invoke-direct {v2}, LX/170;-><init>()V

    .line 2426753
    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    .line 2426754
    iput-object v4, v2, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2426755
    invoke-virtual {v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel$ShareableModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 2426756
    iput-object v4, v2, LX/170;->o:Ljava/lang/String;

    .line 2426757
    invoke-virtual {v2}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    goto/16 :goto_3
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2426666
    iget-object v0, p0, LX/H6T;->q:LX/17d;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2426667
    if-nez v0, :cond_0

    .line 2426668
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2426669
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2426670
    :cond_0
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2426671
    const-string v1, "offer_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2426672
    :cond_1
    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2426673
    const-string v1, "offer_view_id"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2426674
    :cond_2
    invoke-static {p7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2426675
    const-string v1, "share_id"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2426676
    :cond_3
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2426677
    const-string v1, "offer_code"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2426678
    :cond_4
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2426679
    const-string v1, "title"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2426680
    :cond_5
    iget-object v1, p0, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2426681
    return-void
.end method

.method public final a(Landroid/view/View;LX/H83;)V
    .locals 2

    .prologue
    .line 2426663
    invoke-virtual {p2}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2426664
    new-instance v1, LX/H6O;

    invoke-direct {v1, p0, v0, p1}, LX/H6O;-><init>(LX/H6T;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426665
    return-void
.end method

.method public final a(Landroid/view/View;LX/H83;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2426660
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2426661
    new-instance v1, LX/H6P;

    invoke-direct {v1, p0, v0, p2, p3}, LX/H6P;-><init>(LX/H6T;Landroid/content/Context;LX/H83;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426662
    return-void
.end method

.method public final a(LX/H83;)Z
    .locals 4

    .prologue
    .line 2426658
    invoke-virtual {p1}, LX/H83;->h()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 2426659
    iget-object v2, p0, LX/H6T;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2426649
    if-eqz p2, :cond_0

    .line 2426650
    sget-object v0, LX/0ax;->hG:Ljava/lang/String;

    const-string v1, "0"

    invoke-static {v0, p2, p3, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2426651
    :goto_0
    iget-object v1, p0, LX/H6T;->j:LX/17Y;

    invoke-interface {v1, p1, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2426652
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2426653
    iget-object v1, p0, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2426654
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2426655
    :cond_0
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2426656
    sget-object v0, LX/0ax;->hH:Ljava/lang/String;

    invoke-static {v0, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2426657
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d(Landroid/content/Context;LX/H83;)V
    .locals 10

    .prologue
    .line 2426645
    invoke-static {p2}, LX/H6T;->c(LX/H83;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2426646
    iget-object v0, p0, LX/H6T;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Zi;

    const-string v3, "OFFERS"

    invoke-virtual {p2}, LX/H83;->A()D

    move-result-wide v4

    invoke-virtual {p2}, LX/H83;->B()D

    move-result-wide v6

    invoke-virtual {p2}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, LX/H83;->C()Ljava/lang/String;

    move-result-object v9

    move-object v2, p1

    invoke-virtual/range {v1 .. v9}, LX/6Zi;->b(Landroid/content/Context;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V

    .line 2426647
    :goto_0
    return-void

    .line 2426648
    :cond_0
    const-string v0, "OfferRenderingUtils"

    const-string v1, "Offer doesn\'t have sufficient location information to show"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
