.class public final enum LX/FD4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FD4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FD4;

.field public static final enum BOTS:LX/FD4;

.field public static final enum BUSINESSES:LX/FD4;


# instance fields
.field public clickLoggingEventName:Ljava/lang/String;

.field public impressionLoggingEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2210835
    new-instance v0, LX/FD4;

    const-string v1, "BOTS"

    const-string v2, "mm_bots_impressions"

    const-string v3, "mm_bots_clicks"

    invoke-direct {v0, v1, v4, v2, v3}, LX/FD4;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/FD4;->BOTS:LX/FD4;

    .line 2210836
    new-instance v0, LX/FD4;

    const-string v1, "BUSINESSES"

    const-string v2, "mm_bymm_impressions"

    const-string v3, "mm_bymm_clicks"

    invoke-direct {v0, v1, v5, v2, v3}, LX/FD4;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/FD4;->BUSINESSES:LX/FD4;

    .line 2210837
    const/4 v0, 0x2

    new-array v0, v0, [LX/FD4;

    sget-object v1, LX/FD4;->BOTS:LX/FD4;

    aput-object v1, v0, v4

    sget-object v1, LX/FD4;->BUSINESSES:LX/FD4;

    aput-object v1, v0, v5

    sput-object v0, LX/FD4;->$VALUES:[LX/FD4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2210838
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2210839
    iput-object p3, p0, LX/FD4;->impressionLoggingEventName:Ljava/lang/String;

    .line 2210840
    iput-object p4, p0, LX/FD4;->clickLoggingEventName:Ljava/lang/String;

    .line 2210841
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FD4;
    .locals 1

    .prologue
    .line 2210842
    const-class v0, LX/FD4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FD4;

    return-object v0
.end method

.method public static values()[LX/FD4;
    .locals 1

    .prologue
    .line 2210843
    sget-object v0, LX/FD4;->$VALUES:[LX/FD4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FD4;

    return-object v0
.end method
