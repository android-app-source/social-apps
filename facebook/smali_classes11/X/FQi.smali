.class public final enum LX/FQi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQi;

.field public static final enum FEW_HOURS:LX/FQi;

.field public static final enum INSTANT:LX/FQi;

.field public static final enum MINUTES:LX/FQi;

.field public static final enum NONE:LX/FQi;

.field public static final enum ONE_DAY:LX/FQi;

.field public static final enum ONE_HOUR:LX/FQi;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2239614
    new-instance v0, LX/FQi;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/FQi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQi;->NONE:LX/FQi;

    .line 2239615
    new-instance v0, LX/FQi;

    const-string v1, "INSTANT"

    invoke-direct {v0, v1, v4}, LX/FQi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQi;->INSTANT:LX/FQi;

    .line 2239616
    new-instance v0, LX/FQi;

    const-string v1, "MINUTES"

    invoke-direct {v0, v1, v5}, LX/FQi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQi;->MINUTES:LX/FQi;

    .line 2239617
    new-instance v0, LX/FQi;

    const-string v1, "ONE_HOUR"

    invoke-direct {v0, v1, v6}, LX/FQi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQi;->ONE_HOUR:LX/FQi;

    .line 2239618
    new-instance v0, LX/FQi;

    const-string v1, "FEW_HOURS"

    invoke-direct {v0, v1, v7}, LX/FQi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQi;->FEW_HOURS:LX/FQi;

    .line 2239619
    new-instance v0, LX/FQi;

    const-string v1, "ONE_DAY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FQi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQi;->ONE_DAY:LX/FQi;

    .line 2239620
    const/4 v0, 0x6

    new-array v0, v0, [LX/FQi;

    sget-object v1, LX/FQi;->NONE:LX/FQi;

    aput-object v1, v0, v3

    sget-object v1, LX/FQi;->INSTANT:LX/FQi;

    aput-object v1, v0, v4

    sget-object v1, LX/FQi;->MINUTES:LX/FQi;

    aput-object v1, v0, v5

    sget-object v1, LX/FQi;->ONE_HOUR:LX/FQi;

    aput-object v1, v0, v6

    sget-object v1, LX/FQi;->FEW_HOURS:LX/FQi;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FQi;->ONE_DAY:LX/FQi;

    aput-object v2, v0, v1

    sput-object v0, LX/FQi;->$VALUES:[LX/FQi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2239621
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2239622
    return-void
.end method

.method public static fromGraphQL(Lcom/facebook/graphql/enums/GraphQLTimespanCategory;)LX/FQi;
    .locals 2

    .prologue
    .line 2239623
    sget-object v0, LX/FQh;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLTimespanCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2239624
    sget-object v0, LX/FQi;->NONE:LX/FQi;

    :goto_0
    return-object v0

    .line 2239625
    :pswitch_0
    sget-object v0, LX/FQi;->MINUTES:LX/FQi;

    goto :goto_0

    .line 2239626
    :pswitch_1
    sget-object v0, LX/FQi;->ONE_HOUR:LX/FQi;

    goto :goto_0

    .line 2239627
    :pswitch_2
    sget-object v0, LX/FQi;->FEW_HOURS:LX/FQi;

    goto :goto_0

    .line 2239628
    :pswitch_3
    sget-object v0, LX/FQi;->ONE_DAY:LX/FQi;

    goto :goto_0

    .line 2239629
    :pswitch_4
    sget-object v0, LX/FQi;->INSTANT:LX/FQi;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQi;
    .locals 1

    .prologue
    .line 2239630
    const-class v0, LX/FQi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQi;

    return-object v0
.end method

.method public static values()[LX/FQi;
    .locals 1

    .prologue
    .line 2239631
    sget-object v0, LX/FQi;->$VALUES:[LX/FQi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQi;

    return-object v0
.end method
