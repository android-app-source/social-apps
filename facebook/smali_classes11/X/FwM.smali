.class public LX/FwM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2303040
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/view/View;LX/1af;Landroid/graphics/Rect;LX/1bf;)LX/9hN;
    .locals 6

    .prologue
    .line 2303041
    new-instance v0, LX/FwL;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/FwL;-><init>(Ljava/lang/String;LX/1af;Landroid/view/View;Landroid/graphics/Rect;LX/1bf;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;)LX/9hN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/drawee/view/DraweeView",
            "<",
            "LX/1af;",
            ">;",
            "LX/1bf;",
            ")",
            "LX/9hN;"
        }
    .end annotation

    .prologue
    .line 2303039
    new-instance v0, LX/FwK;

    invoke-direct {v0, p0, p1, p2}, LX/FwK;-><init>(Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;LX/1Fb;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;
    .locals 2

    .prologue
    .line 2303031
    new-instance v0, LX/5kN;

    invoke-direct {v0}, LX/5kN;-><init>()V

    .line 2303032
    iput-object p0, v0, LX/5kN;->D:Ljava/lang/String;

    .line 2303033
    move-object v0, v0

    .line 2303034
    invoke-static {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 2303035
    iput-object v1, v0, LX/5kN;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2303036
    move-object v0, v0

    .line 2303037
    invoke-virtual {v0}, LX/5kN;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v0

    .line 2303038
    return-object v0
.end method
