.class public final LX/G0D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;)V
    .locals 0

    .prologue
    .line 2308644
    iput-object p1, p0, LX/G0D;->a:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2308645
    iget-object v0, p0, LX/G0D;->a:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->p:LX/Fzo;

    iget-object v1, p0, LX/G0D;->a:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    iget-object v1, v1, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    sget-object v2, Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308646
    iget-object v3, v0, LX/Fzo;->b:LX/Fzy;

    .line 2308647
    invoke-static {}, LX/5wK;->b()LX/5wG;

    move-result-object v6

    .line 2308648
    const-string v7, "profile_id"

    invoke-virtual {v6, v7, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2308649
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    const-string v7, "FETCH_MEMORIAL_PROFILE_PICTURE_QUERY_TAG"

    invoke-static {v7}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v7

    .line 2308650
    iput-object v7, v6, LX/0zO;->d:Ljava/util/Set;

    .line 2308651
    move-object v6, v6

    .line 2308652
    sget-object v7, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v6, v7}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v6

    sget-object v7, LX/0zS;->a:LX/0zS;

    invoke-virtual {v6, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    .line 2308653
    iput-object v2, v6, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308654
    move-object v6, v6

    .line 2308655
    const-wide/16 v8, 0xe10

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    .line 2308656
    iget-object v7, v3, LX/Fzy;->c:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2308657
    new-instance v7, LX/Fzq;

    invoke-direct {v7, v3}, LX/Fzq;-><init>(LX/Fzy;)V

    iget-object v8, v3, LX/Fzy;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v3, v6

    .line 2308658
    new-instance v4, LX/Fzh;

    invoke-direct {v4, v0}, LX/Fzh;-><init>(LX/Fzo;)V

    iget-object v5, v0, LX/Fzo;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2308659
    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2308660
    invoke-direct {p0}, LX/G0D;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
