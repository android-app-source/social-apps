.class public final enum LX/GMa;
.super LX/GMY;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 2344887
    invoke-direct {p0, p1, p2, p3}, LX/GMY;-><init>(Ljava/lang/String;II)V

    return-void
.end method


# virtual methods
.method public final process(Lcom/facebook/adinterfaces/ui/MapSpinnerView;)V
    .locals 2

    .prologue
    .line 2344888
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->postInvalidate()V

    .line 2344889
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, LX/GMY;->mBackgroundAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2344890
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->clearAnimation()V

    .line 2344891
    return-void
.end method
