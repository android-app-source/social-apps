.class public final enum LX/GAZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GAZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GAZ;

.field public static final enum DELETE:LX/GAZ;

.field public static final enum GET:LX/GAZ;

.field public static final enum POST:LX/GAZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2325806
    new-instance v0, LX/GAZ;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, LX/GAZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAZ;->GET:LX/GAZ;

    .line 2325807
    new-instance v0, LX/GAZ;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, LX/GAZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAZ;->POST:LX/GAZ;

    .line 2325808
    new-instance v0, LX/GAZ;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v4}, LX/GAZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAZ;->DELETE:LX/GAZ;

    .line 2325809
    const/4 v0, 0x3

    new-array v0, v0, [LX/GAZ;

    sget-object v1, LX/GAZ;->GET:LX/GAZ;

    aput-object v1, v0, v2

    sget-object v1, LX/GAZ;->POST:LX/GAZ;

    aput-object v1, v0, v3

    sget-object v1, LX/GAZ;->DELETE:LX/GAZ;

    aput-object v1, v0, v4

    sput-object v0, LX/GAZ;->$VALUES:[LX/GAZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2325810
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GAZ;
    .locals 1

    .prologue
    .line 2325811
    const-class v0, LX/GAZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GAZ;

    return-object v0
.end method

.method public static values()[LX/GAZ;
    .locals 1

    .prologue
    .line 2325812
    sget-object v0, LX/GAZ;->$VALUES:[LX/GAZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GAZ;

    return-object v0
.end method
