.class public LX/GdD;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/Gd5;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/Gd5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2373321
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Gd5;
    .locals 4

    .prologue
    .line 2373309
    sget-object v0, LX/GdD;->a:LX/Gd5;

    if-nez v0, :cond_1

    .line 2373310
    const-class v1, LX/GdD;

    monitor-enter v1

    .line 2373311
    :try_start_0
    sget-object v0, LX/GdD;->a:LX/Gd5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2373312
    if-eqz v2, :cond_0

    .line 2373313
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2373314
    const-class v3, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v3}, LX/27Z;->b(Landroid/content/Context;)LX/Gd5;

    move-result-object v3

    move-object v0, v3

    .line 2373315
    sput-object v0, LX/GdD;->a:LX/Gd5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2373316
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2373317
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2373318
    :cond_1
    sget-object v0, LX/GdD;->a:LX/Gd5;

    return-object v0

    .line 2373319
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2373320
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2373308
    const-class v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LX/27Z;->b(Landroid/content/Context;)LX/Gd5;

    move-result-object v0

    return-object v0
.end method
