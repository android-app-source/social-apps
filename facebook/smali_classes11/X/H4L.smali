.class public LX/H4L;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;)V
    .locals 0

    .prologue
    .line 2422623
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2422624
    iput-object p1, p0, LX/H4L;->a:Landroid/content/Context;

    .line 2422625
    iput-object p2, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    .line 2422626
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 2422631
    iget-object v0, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422632
    iget-object p0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    move-object v0, p0

    .line 2422633
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 2422627
    invoke-static {}, LX/H4K;->values()[LX/H4K;

    move-result-object v0

    invoke-virtual {p0, p1}, LX/H4L;->getItemViewType(I)I

    move-result v1

    aget-object v0, v0, v1

    .line 2422628
    sget-object v1, LX/H4J;->a:[I

    invoke-virtual {v0}, LX/H4K;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2422629
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "there are no object associate with location"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2422630
    :pswitch_0
    invoke-direct {p0}, LX/H4L;->a()I

    move-result v0

    sub-int/2addr p1, v0

    :pswitch_1
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2422616
    iget-object v0, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    if-nez v0, :cond_0

    .line 2422617
    const/4 v0, 0x0

    .line 2422618
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422619
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    move-object v0, v1

    .line 2422620
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422621
    iget-object p0, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->b:LX/0Px;

    move-object v1, p0

    .line 2422622
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2422634
    invoke-static {}, LX/H4K;->values()[LX/H4K;

    move-result-object v1

    invoke-virtual {p0, p1}, LX/H4L;->getItemViewType(I)I

    move-result v2

    aget-object v1, v1, v2

    .line 2422635
    sget-object v2, LX/H4J;->a:[I

    invoke-virtual {v1}, LX/H4K;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2422636
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "there are no object associate with location"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2422637
    :pswitch_0
    iget-object v1, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422638
    iget-object v2, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    move-object v1, v2

    .line 2422639
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    iget-object v0, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422640
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    move-object v0, v1

    .line 2422641
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2422642
    :cond_0
    :goto_0
    return-object v0

    .line 2422643
    :pswitch_1
    invoke-direct {p0}, LX/H4L;->a()I

    move-result v1

    sub-int v1, p1, v1

    .line 2422644
    iget-object v2, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422645
    iget-object p1, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->b:LX/0Px;

    move-object v2, p1

    .line 2422646
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    iget-object v0, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422647
    iget-object v2, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->b:LX/0Px;

    move-object v0, v2

    .line 2422648
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2422610
    iget-object v0, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422611
    iget-object v1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    move-object v0, v1

    .line 2422612
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    move-wide v0, v2

    .line 2422613
    :goto_0
    return-wide v0

    .line 2422614
    :cond_0
    invoke-virtual {p0, p1}, LX/H4L;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;

    .line 2422615
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move-wide v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2422579
    iget-object v0, p0, LX/H4L;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->b:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    .line 2422580
    iget-object p0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;->c:LX/0Px;

    move-object v0, p0

    .line 2422581
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2422582
    sget-object v0, LX/H4K;->QUERY_SUGGESTION_CELL:LX/H4K;

    invoke-virtual {v0}, LX/H4K;->ordinal()I

    move-result v0

    .line 2422583
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/H4K;->PLACE_CELL:LX/H4K;

    invoke-virtual {v0}, LX/H4K;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2422585
    invoke-static {}, LX/H4K;->values()[LX/H4K;

    move-result-object v0

    invoke-virtual {p0, p1}, LX/H4L;->getItemViewType(I)I

    move-result v1

    aget-object v0, v0, v1

    .line 2422586
    sget-object v1, LX/H4J;->a:[I

    invoke-virtual {v0}, LX/H4K;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2422587
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2422588
    :pswitch_0
    invoke-virtual {p0, p1}, LX/H4L;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2422589
    if-nez p2, :cond_1

    .line 2422590
    iget-object v1, p0, LX/H4L;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2422591
    const v2, 0x7f03152b

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/contentview/ContentView;

    move-object p2, v1

    .line 2422592
    :goto_0
    const v1, 0x7f0d2fc1

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2422593
    iget-object v2, p0, LX/H4L;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02091e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2422594
    iget-object v2, p0, LX/H4L;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020d59

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2422595
    const v1, 0x7f0d2fc2

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    .line 2422596
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2422597
    if-eqz v0, :cond_0

    .line 2422598
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2422599
    :cond_0
    :goto_1
    return-object p2

    .line 2422600
    :cond_1
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    goto :goto_0

    .line 2422601
    :pswitch_1
    invoke-virtual {p0, p1}, LX/H4L;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;

    .line 2422602
    if-nez p2, :cond_2

    .line 2422603
    iget-object v1, p0, LX/H4L;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2422604
    const v2, 0x7f03152d

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/contentview/ContentView;

    move-object p2, v1

    .line 2422605
    :goto_2
    if-eqz v0, :cond_0

    .line 2422606
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2422607
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2422608
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2422609
    :cond_2
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2422584
    sget-object v0, LX/H4K;->COUNT:LX/H4K;

    invoke-virtual {v0}, LX/H4K;->ordinal()I

    move-result v0

    return v0
.end method
