.class public LX/FKz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/model/messages/Message;",
        "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/FKy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2225871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/0QB;)LX/FKz;
    .locals 2

    .prologue
    .line 2225874
    new-instance v1, LX/FKz;

    invoke-direct {v1}, LX/FKz;-><init>()V

    .line 2225875
    invoke-static {p0}, LX/FKy;->b(LX/0QB;)LX/FKy;

    move-result-object v0

    check-cast v0, LX/FKy;

    .line 2225876
    iput-object v0, v1, LX/FKz;->a:LX/FKy;

    .line 2225877
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 2225878
    check-cast p1, Lcom/facebook/messaging/model/messages/Message;

    .line 2225879
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2225880
    invoke-static {v0, p1}, LX/FKy;->a(Ljava/util/List;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2225881
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "sendMessage"

    .line 2225882
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2225883
    move-object v1, v1

    .line 2225884
    const-string v2, "POST"

    .line 2225885
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2225886
    move-object v1, v1

    .line 2225887
    const-string v2, "me/montage_thread_messages"

    .line 2225888
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2225889
    move-object v1, v1

    .line 2225890
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2225891
    move-object v0, v1

    .line 2225892
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2225893
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225894
    move-object v0, v0

    .line 2225895
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2225872
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "thread_fbid"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2225873
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0
.end method
