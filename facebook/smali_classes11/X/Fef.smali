.class public final LX/Fef;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fee;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V
    .locals 2

    .prologue
    .line 2266027
    iput-object p1, p0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2266028
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CENTRAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_CONTEXT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Fef;->b:LX/0Px;

    return-void
.end method

.method private a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 2266029
    move v1, v0

    move v2, v0

    .line 2266030
    :goto_0
    iget-object v0, p0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2266031
    iget-object v0, p0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0, v1}, LX/CzE;->b(I)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 2266032
    instance-of v4, v0, LX/Cz6;

    if-eqz v4, :cond_1

    .line 2266033
    check-cast v0, LX/Cz6;

    invoke-interface {v0}, LX/Cz6;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2266034
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v4, :cond_0

    move v0, v1

    .line 2266035
    :goto_1
    if-eq v0, v3, :cond_2

    .line 2266036
    iget-object v1, p0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1, v0, p1}, LX/CzE;->b(ILcom/facebook/graphql/model/FeedUnit;)Z

    move v2, v0

    .line 2266037
    :goto_2
    return v2

    .line 2266038
    :cond_0
    iget-object v4, p0, LX/Fef;->b:LX/0Px;

    invoke-virtual {v4, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2266039
    add-int/lit8 v2, v1, 0x1

    .line 2266040
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2266041
    :cond_2
    iget-object v0, p0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    .line 2266042
    if-ltz v2, :cond_3

    iget-object v1, v0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v2, v1, :cond_5

    .line 2266043
    :cond_3
    :goto_3
    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_1

    .line 2266044
    :cond_5
    iget-object v1, v0, LX/CzE;->f:Ljava/util/List;

    invoke-interface {v1, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2266045
    goto :goto_3
.end method


# virtual methods
.method public final a(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0am",
            "<",
            "LX/0us;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2266046
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    invoke-virtual {v1}, LX/Fje;->getSwipeLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2266047
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d(Ljava/lang/String;)V

    .line 2266048
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c(Ljava/lang/String;)V

    .line 2266049
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e(Ljava/lang/String;)V

    .line 2266050
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v4

    const/4 v5, 0x0

    sget-object v6, LX/8cf;->HEAD:LX/8cf;

    invoke-virtual/range {v1 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IILX/8cg;LX/8cf;)V

    .line 2266051
    invoke-virtual/range {p1 .. p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2266052
    :goto_0
    return-void

    .line 2266053
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    .line 2266054
    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2266055
    const-string v4, ""

    invoke-virtual {v5, v1, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2266056
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2266057
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-virtual/range {p2 .. p2}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual/range {p2 .. p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0us;

    invoke-interface {v1}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-static {v2, v1}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 2266058
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2266059
    invoke-virtual/range {p1 .. p1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2266060
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2266061
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2266062
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 2266063
    :cond_3
    new-instance v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f082336

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v15

    invoke-static {}, LX/0Rh;->a()LX/0Rh;

    move-result-object v16

    invoke-direct/range {v1 .. v16}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;LX/0P1;LX/0Px;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;Ljava/lang/Integer;Ljava/lang/String;LX/D0L;LX/0Px;LX/0P1;)V

    .line 2266064
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, LX/Fef;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)I

    move-result v4

    .line 2266065
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v8, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-static {v2}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-static {v3}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->r(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    const/4 v6, 0x0

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static/range {v3 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-virtual {v8, v9, v4, v1, v2}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2266066
    move-object/from16 v0, p0

    iget-object v1, v0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method

.method public final a(LX/7C4;)V
    .locals 3

    .prologue
    .line 2266067
    iget-object v0, p0, LX/Fef;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    .line 2266068
    iget-object v1, p1, LX/7C4;->mError:LX/3Ql;

    move-object v1, v1

    .line 2266069
    const-string v2, "Failed to load latest posts"

    invoke-virtual {v0, v1, v2, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2266070
    return-void
.end method

.method public final b(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0am",
            "<",
            "LX/0us;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2266071
    return-void
.end method
