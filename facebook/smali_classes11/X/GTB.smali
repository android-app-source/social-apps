.class public LX/GTB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2FT;

.field public final c:LX/2FX;

.field public final d:LX/2FU;

.field public final e:LX/GRa;

.field private final f:LX/GRE;

.field public final g:Lcom/facebook/content/SecureContextHelper;

.field public final h:LX/GRv;

.field public final i:LX/0iA;

.field public final j:LX/GRy;

.field public final k:LX/0Uh;


# direct methods
.method public constructor <init>(LX/2FX;LX/0Or;LX/2FT;LX/2FU;LX/GRa;LX/GRE;Lcom/facebook/content/SecureContextHelper;LX/GRv;LX/0iA;LX/GRy;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2FX;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/2FT;",
            "LX/2FU;",
            "LX/GRa;",
            "LX/GRE;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/GRv;",
            "LX/0iA;",
            "LX/GRy;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2355302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2355303
    iput-object p1, p0, LX/GTB;->c:LX/2FX;

    .line 2355304
    iput-object p2, p0, LX/GTB;->a:LX/0Or;

    .line 2355305
    iput-object p3, p0, LX/GTB;->b:LX/2FT;

    .line 2355306
    iput-object p4, p0, LX/GTB;->d:LX/2FU;

    .line 2355307
    iput-object p5, p0, LX/GTB;->e:LX/GRa;

    .line 2355308
    iput-object p6, p0, LX/GTB;->f:LX/GRE;

    .line 2355309
    iput-object p7, p0, LX/GTB;->g:Lcom/facebook/content/SecureContextHelper;

    .line 2355310
    iput-object p8, p0, LX/GTB;->h:LX/GRv;

    .line 2355311
    iput-object p9, p0, LX/GTB;->i:LX/0iA;

    .line 2355312
    iput-object p10, p0, LX/GTB;->j:LX/GRy;

    .line 2355313
    iput-object p11, p0, LX/GTB;->k:LX/0Uh;

    .line 2355314
    return-void
.end method

.method private a(LX/GRZ;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2355204
    new-instance v0, LX/GT4;

    invoke-direct {v0, p0, p2, p1}, LX/GT4;-><init>(LX/GTB;Ljava/lang/String;LX/GRZ;)V

    return-object v0
.end method

.method public static a(LX/GTB;Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;LX/GRZ;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2355288
    invoke-virtual {p2, v2}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->mx_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->setAppName(Ljava/lang/String;)V

    .line 2355289
    invoke-virtual {p2, v2}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->j()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2355290
    invoke-virtual {p2, v2}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->setAppPictureURI(Landroid/net/Uri;)V

    .line 2355291
    :cond_0
    invoke-virtual {p2, v2}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->my_()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;

    move-result-object v0

    .line 2355292
    if-eqz v0, :cond_1

    .line 2355293
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$OverallStarRatingModel;->a()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->setAppRating(F)V

    .line 2355294
    :goto_0
    iget-object v0, p0, LX/GTB;->d:LX/2FU;

    invoke-virtual {p2, v2}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->b()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2FU;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2355295
    invoke-virtual {p2, v2}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2355296
    const v0, 0x7f08376c

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->setInstallLabel(I)V

    .line 2355297
    :goto_1
    const-string v0, "app_invite_install_button_tapped"

    invoke-direct {p0, p2, v0}, LX/GTB;->a(LX/GRZ;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->setInstallOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2355298
    return-void

    .line 2355299
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->setAppRating(F)V

    goto :goto_0

    .line 2355300
    :cond_2
    const v0, 0x7f08376d

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->setInstallLabel(I)V

    goto :goto_1

    .line 2355301
    :cond_3
    const v0, 0x7f08376b

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;->setInstallLabel(I)V

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/GTB;
    .locals 12

    .prologue
    .line 2355286
    new-instance v0, LX/GTB;

    invoke-static {p0}, LX/2FX;->a(LX/0QB;)LX/2FX;

    move-result-object v1

    check-cast v1, LX/2FX;

    const/16 v2, 0x2eb

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/2FT;->b(LX/0QB;)LX/2FT;

    move-result-object v3

    check-cast v3, LX/2FT;

    invoke-static {p0}, LX/2FU;->a(LX/0QB;)LX/2FU;

    move-result-object v4

    check-cast v4, LX/2FU;

    invoke-static {p0}, LX/GRa;->a(LX/0QB;)LX/GRa;

    move-result-object v5

    check-cast v5, LX/GRa;

    const-class v6, LX/GRE;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/GRE;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/GRv;->b(LX/0QB;)LX/GRv;

    move-result-object v8

    check-cast v8, LX/GRv;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v9

    check-cast v9, LX/0iA;

    invoke-static {p0}, LX/GRy;->a(LX/0QB;)LX/GRy;

    move-result-object v10

    check-cast v10, LX/GRy;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-direct/range {v0 .. v11}, LX/GTB;-><init>(LX/2FX;LX/0Or;LX/2FT;LX/2FU;LX/GRa;LX/GRE;Lcom/facebook/content/SecureContextHelper;LX/GRv;LX/0iA;LX/GRy;LX/0Uh;)V

    .line 2355287
    return-object v0
.end method


# virtual methods
.method public final a(LX/GRZ;LX/GSz;)V
    .locals 8

    .prologue
    .line 2355237
    invoke-virtual {p1}, LX/GRZ;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 2355238
    iget-object v0, p2, LX/GSz;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2355239
    iget-object v0, p2, LX/GSz;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2355240
    :cond_0
    const/4 v6, 0x1

    .line 2355241
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 2355242
    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 2355243
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v2, v4, v5

    aput v6, v4, v6

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 2355244
    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2355245
    new-instance v3, LX/GT9;

    invoke-direct {v3, p0, p2}, LX/GT9;-><init>(LX/GTB;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2355246
    new-instance v3, LX/GTA;

    invoke-direct {v3, p0, p2}, LX/GTA;-><init>(LX/GTB;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2355247
    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2355248
    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 2355249
    :goto_0
    return-void

    .line 2355250
    :cond_1
    iget-object v0, p0, LX/GTB;->h:LX/GRv;

    const-string v1, "app_invite_cell_did_show"

    const/4 v7, 0x0

    .line 2355251
    invoke-virtual {p1}, LX/GRZ;->b()I

    move-result v2

    if-nez v2, :cond_3

    .line 2355252
    :goto_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->c()Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/GSz;->setSocialContext(Lcom/facebook/appinvites/protocol/AppInvitesTextWithEntitiesModels$AppInvitesTextWithEntitiesModel;)V

    .line 2355253
    new-instance v0, LX/GT1;

    invoke-direct {v0, p0, p1}, LX/GT1;-><init>(LX/GTB;LX/GRZ;)V

    .line 2355254
    iget-object v1, p2, LX/GSz;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2355255
    iget-object v0, p0, LX/GTB;->f:LX/GRE;

    .line 2355256
    new-instance v3, LX/GRD;

    invoke-static {v0}, LX/0ja;->a(LX/0QB;)LX/0ja;

    move-result-object v1

    check-cast v1, LX/0ja;

    invoke-static {v0}, LX/GTB;->b(LX/0QB;)LX/GTB;

    move-result-object v2

    check-cast v2, LX/GTB;

    invoke-direct {v3, v1, v2, p1}, LX/GRD;-><init>(LX/0ja;LX/GTB;LX/GRZ;)V

    .line 2355257
    move-object v0, v3

    .line 2355258
    iget-object v1, p2, LX/GSz;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2355259
    new-instance v0, LX/GT2;

    invoke-direct {v0, p0, p1}, LX/GT2;-><init>(LX/GTB;LX/GRZ;)V

    .line 2355260
    iget-object v1, p2, LX/GSz;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2355261
    iget v0, p1, LX/GRZ;->a:I

    move v0, v0

    .line 2355262
    iget-object v1, p2, LX/GSz;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2355263
    iget-object v0, p2, LX/GSz;->b:Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;

    move-object v0, v0

    .line 2355264
    invoke-static {p0, v0, p1}, LX/GTB;->a(LX/GTB;Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;LX/GRZ;)V

    .line 2355265
    iget-object v0, p0, LX/GTB;->j:LX/GRy;

    .line 2355266
    iget-boolean v1, v0, LX/GRy;->a:Z

    move v0, v1

    .line 2355267
    if-eqz v0, :cond_5

    .line 2355268
    :cond_2
    :goto_2
    goto :goto_0

    .line 2355269
    :cond_3
    invoke-static {v1}, LX/GRv;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2355270
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2355271
    invoke-virtual {p1}, LX/GRZ;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2355272
    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->mv_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2355273
    :cond_4
    const-string v2, "object_ids"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v7, v5}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2355274
    const-string v2, "app_id"

    invoke-virtual {p1, v7}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2355275
    iget-object v2, v0, LX/GRv;->a:LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_1

    .line 2355276
    :cond_5
    iget-object v0, p0, LX/GTB;->i:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_INVITE_CARET:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/3kX;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kX;

    .line 2355277
    const v1, 0x7f0d056c

    invoke-virtual {p2, v1}, LX/GSz;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2355278
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 2355279
    if-nez v1, :cond_6

    .line 2355280
    :goto_4
    iget-object v1, p0, LX/GTB;->i:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/3kX;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 2355281
    :cond_6
    new-instance v2, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 p2, 0x2

    invoke-direct {v2, v3, p2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2355282
    const/4 v3, -0x1

    .line 2355283
    iput v3, v2, LX/0hs;->t:I

    .line 2355284
    const v3, 0x7f083780

    invoke-virtual {v2, v3}, LX/0hs;->a(I)V

    .line 2355285
    invoke-virtual {v2, v1}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_4
.end method

.method public final a(Lcom/facebook/appinvites/ui/AppInviteContentView;LX/GRZ;I)V
    .locals 4

    .prologue
    .line 2355205
    invoke-virtual {p2, p3}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    .line 2355206
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setSenderName(Ljava/lang/String;)V

    .line 2355207
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->d()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setTimestamp(J)V

    .line 2355208
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->k()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setMessage(Ljava/lang/String;)V

    .line 2355209
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;->d()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setSenderImageURI(Landroid/net/Uri;)V

    .line 2355210
    new-instance v1, LX/GT3;

    invoke-direct {v1, p0, p2, p3, p1}, LX/GT3;-><init>(LX/GTB;LX/GRZ;ILcom/facebook/appinvites/ui/AppInviteContentView;)V

    invoke-virtual {p1, v1}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setSenderImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2355211
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->mw_()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2355212
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->mw_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setPreviewImageUri(Landroid/net/Uri;)V

    .line 2355213
    :goto_0
    const-string v0, "app_invite_canvas_tapped"

    invoke-direct {p0, p2, v0}, LX/GTB;->a(LX/GRZ;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setPreviewImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2355214
    iget-object v0, p1, Lcom/facebook/appinvites/ui/AppInviteContentView;->i:Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;

    move-object v0, v0

    .line 2355215
    const/4 v3, 0x0

    .line 2355216
    iget-object v1, p0, LX/GTB;->k:LX/0Uh;

    const/16 v2, 0x2e5

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2355217
    invoke-virtual {p2, v3}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 2355218
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 2355219
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2355220
    const-string v1, "promo_code"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2355221
    const-string v1, "promo_text"

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2355222
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 2355223
    invoke-virtual {v0, v1}, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->setPromotionText(Ljava/lang/String;)V

    .line 2355224
    invoke-virtual {v0}, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083797

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2355225
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 2355226
    invoke-virtual {v0}, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083798

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v3, v2, p1

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2355227
    :cond_0
    invoke-virtual {v0, v1}, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->setPromotionCodeLine(Ljava/lang/String;)V

    .line 2355228
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/appinvites/ui/AppInvitePromotionDetailsView;->setVisibility(I)V

    .line 2355229
    iget-object v1, p0, LX/GTB;->h:LX/GRv;

    const-string v2, "app_invite_promotion_displayed"

    .line 2355230
    iget v3, p2, LX/GRZ;->a:I

    move v3, v3

    .line 2355231
    invoke-virtual {p2, v3}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/GRv;->a(Ljava/lang/String;Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2355232
    :cond_1
    :goto_1
    return-void

    .line 2355233
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setPreviewImageUri(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2355234
    :catch_0
    iget-object v1, p0, LX/GTB;->h:LX/GRv;

    const-string v2, "app_invite_promotion_parsing_failed"

    .line 2355235
    iget v3, p2, LX/GRZ;->a:I

    move v3, v3

    .line 2355236
    invoke-virtual {p2, v3}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/GRv;->a(Ljava/lang/String;Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;)V

    goto :goto_1
.end method
