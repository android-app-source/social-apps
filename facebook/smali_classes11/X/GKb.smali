.class public LX/GKb;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field private b:LX/GKT;

.field private c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

.field private d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

.field private f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

.field private g:J

.field private h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

.field private i:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;

.field private j:LX/GKX;

.field private k:LX/GN0;


# direct methods
.method public constructor <init>(LX/GN0;LX/GKT;LX/GKX;)V
    .locals 0
    .param p1    # LX/GN0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/GKT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/GKX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2341072
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2341073
    iput-object p1, p0, LX/GKb;->k:LX/GN0;

    .line 2341074
    iput-object p2, p0, LX/GKb;->b:LX/GKT;

    .line 2341075
    iput-object p3, p0, LX/GKb;->j:LX/GKX;

    .line 2341076
    return-void
.end method

.method private a(LX/8wK;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2341071
    new-instance v0, LX/GKZ;

    invoke-direct {v0, p0, p1}, LX/GKZ;-><init>(LX/GKb;LX/8wK;)V

    return-object v0
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3

    .prologue
    .line 2341182
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2341183
    iput-object p1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    .line 2341184
    iget-object v0, p0, LX/GKb;->b:LX/GKT;

    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    .line 2341185
    iget-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    move-object v1, v2

    .line 2341186
    invoke-virtual {v0, v1, p2}, LX/GKT;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2341187
    const v0, 0x7f0d045e

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;

    iput-object v0, p0, LX/GKb;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;

    .line 2341188
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-direct {p0}, LX/GKb;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setAmountSpentValue(Ljava/lang/String;)V

    .line 2341189
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-direct {p0}, LX/GKb;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setAudienceValue(Ljava/lang/String;)V

    .line 2341190
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-direct {p0}, LX/GKb;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setEndDateValue(Ljava/lang/String;)V

    .line 2341191
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-direct {p0}, LX/GKb;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setPaymentMethodValue(Ljava/lang/String;)V

    .line 2341192
    invoke-direct {p0}, LX/GKb;->b()V

    .line 2341193
    invoke-direct {p0}, LX/GKb;->c()V

    .line 2341194
    invoke-direct {p0}, LX/GKb;->i()V

    .line 2341195
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2341176
    sget-object v0, LX/GKa;->a:[I

    iget-object v1, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2341177
    :goto_0
    return-void

    .line 2341178
    :pswitch_0
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b67

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0358

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f02005d

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a(Ljava/lang/String;IIZ)V

    goto :goto_0

    .line 2341179
    :pswitch_1
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b68

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0358

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f02005b

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a(Ljava/lang/String;IIZ)V

    goto :goto_0

    .line 2341180
    :pswitch_2
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b69

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a035c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f02005e

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a(Ljava/lang/String;IIZ)V

    goto :goto_0

    .line 2341181
    :pswitch_3
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b6a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f02005c

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->a(Ljava/lang/String;IIZ)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2341167
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    sget-object v1, LX/8wK;->TARGETING_DESCRIPTION:LX/8wK;

    invoke-direct {p0, v1}, LX/GKb;->a(LX/8wK;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setAudienceRowListener(Landroid/view/View$OnClickListener;)V

    .line 2341168
    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-direct {p0}, LX/GKb;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/8wK;->DURATION_BUDGET:LX/8wK;

    :goto_0
    invoke-direct {p0, v0}, LX/GKb;->a(LX/8wK;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setEndDateRowListener(Landroid/view/View$OnClickListener;)V

    .line 2341169
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    sget-object v1, LX/8wK;->ACCOUNT:LX/8wK;

    invoke-direct {p0, v1}, LX/GKb;->a(LX/8wK;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setPaymentMethodRowListener(Landroid/view/View$OnClickListener;)V

    .line 2341170
    sget-object v0, LX/GKa;->b:[I

    iget-object v1, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2341171
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    sget-object v1, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    invoke-direct {p0, v1}, LX/GKb;->a(LX/8wK;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setAmountSpentRowListener(Landroid/view/View$OnClickListener;)V

    .line 2341172
    :goto_1
    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-direct {p0}, LX/GKb;->d()Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setAdStatusToggleListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2341173
    return-void

    .line 2341174
    :cond_0
    sget-object v0, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    goto :goto_0

    .line 2341175
    :pswitch_0
    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-direct {p0}, LX/GKb;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/8wK;->DURATION_BUDGET:LX/8wK;

    :goto_2
    invoke-direct {p0, v0}, LX/GKb;->a(LX/8wK;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->setAmountSpentRowListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_1
    sget-object v0, LX/8wK;->BUDGET:LX/8wK;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private d()Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .locals 4

    .prologue
    .line 2341164
    iget-object v0, p0, LX/GKb;->k:LX/GN0;

    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2341165
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2341166
    iget-object v3, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v2, v3}, LX/GN0;->a(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v0

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2341163
    iget-wide v0, p0, LX/GKb;->g:J

    sget-object v2, LX/GG5;->CONTINUOUS:LX/GG5;

    invoke-virtual {v2}, LX/GG5;->getDuration()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080b0a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, LX/GKb;->g:J

    iget-object v2, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/GG6;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private f()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2341130
    iget-object v0, p0, LX/GKb;->h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2341131
    iget v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    move v0, v1

    .line 2341132
    const/16 v1, 0x41

    if-ne v0, v1, :cond_6

    const v0, 0x7f080b6c

    .line 2341133
    :goto_0
    iget-object v1, p0, LX/GKb;->h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2341134
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v1, v2

    .line 2341135
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    .line 2341136
    iget-object v1, p0, LX/GKb;->h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2341137
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    move-object v1, v3

    .line 2341138
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    .line 2341139
    const v1, 0x7f080b6b

    .line 2341140
    iget-object v4, p0, LX/GKb;->h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2341141
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move-object v4, v5

    .line 2341142
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->MALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    if-ne v4, v5, :cond_7

    .line 2341143
    const v1, 0x7f080a8c

    .line 2341144
    :cond_0
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, LX/GKb;->h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2341145
    iget v9, v6, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    move v6, v9

    .line 2341146
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget-object v6, p0, LX/GKb;->h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2341147
    iget v9, v6, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    move v6, v9

    .line 2341148
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v4, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2341149
    if-gtz v2, :cond_1

    if-lez v3, :cond_2

    .line 2341150
    :cond_1
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2341151
    :cond_2
    if-lez v2, :cond_3

    .line 2341152
    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0f0048

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v1, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2341153
    :cond_3
    if-lez v2, :cond_4

    if-lez v3, :cond_4

    .line 2341154
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2341155
    :cond_4
    if-lez v3, :cond_5

    .line 2341156
    iget-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0047

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2341157
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2341158
    :cond_6
    const v0, 0x7f080b6d

    goto/16 :goto_0

    .line 2341159
    :cond_7
    iget-object v4, p0, LX/GKb;->h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2341160
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move-object v4, v5

    .line 2341161
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    if-ne v4, v5, :cond_0

    .line 2341162
    const v1, 0x7f080a8b

    goto/16 :goto_1
.end method

.method private g()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2341125
    iget-object v0, p0, LX/GKb;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    iget-object v1, p0, LX/GKb;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    iget-object v1, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 2341126
    iget-object v1, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v1, v2, :cond_0

    .line 2341127
    :goto_0
    return-object v0

    .line 2341128
    :cond_0
    iget-object v1, p0, LX/GKb;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    iget-object v2, p0, LX/GKb;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    iget-object v4, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v4}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v1

    .line 2341129
    iget-object v2, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080b6e

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2341124
    iget-object v0, p0, LX/GKb;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->x()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 10

    .prologue
    .line 2341099
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2341100
    invoke-virtual {v0}, LX/GCE;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2341101
    :goto_0
    return-void

    .line 2341102
    :cond_0
    iget-object v0, p0, LX/GKb;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->setVisibility(I)V

    .line 2341103
    iget-object v0, p0, LX/GKb;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;

    iget-object v1, p0, LX/GKb;->j:LX/GKX;

    iget-object v2, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2341104
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2341105
    new-instance v4, LX/47x;

    invoke-direct {v4, v3}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2341106
    const v5, 0x7f080b63

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2341107
    const-string v5, "[[learn_more]]"

    const v6, 0x7f080a77

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2341108
    new-instance v6, LX/GKV;

    invoke-direct {v6, v1, v2}, LX/GKV;-><init>(LX/GKX;Landroid/content/Context;)V

    move-object v6, v6

    .line 2341109
    const/16 v7, 0x21

    invoke-virtual {v4, v5, v3, v6, v7}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2341110
    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v3

    move-object v1, v3

    .line 2341111
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->setLearnMoreContent(Landroid/text/Spanned;)V

    .line 2341112
    iget-object v0, p0, LX/GKb;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;

    iget-object v1, p0, LX/GKb;->j:LX/GKX;

    iget-object v2, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2341113
    iget-object v3, p0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2341114
    iget-object v4, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2341115
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2341116
    new-instance v6, LX/47x;

    invoke-direct {v6, v5}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2341117
    const v7, 0x7f080b61

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2341118
    const-string v7, "[[cancel_order]]"

    const v8, 0x7f080b62

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2341119
    new-instance v8, LX/GKW;

    invoke-direct {v8, v1, v3, v4, v2}, LX/GKW;-><init>(LX/GKX;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/content/Context;)V

    move-object v8, v8

    .line 2341120
    const/16 v9, 0x21

    invoke-virtual {v6, v7, v5, v8, v9}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2341121
    invoke-virtual {v6}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v5

    move-object v1, v5

    .line 2341122
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->setCancelOrderContent(Landroid/text/Spanned;)V

    .line 2341123
    iget-object v0, p0, LX/GKb;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto/16 :goto_0
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 2341098
    iget-object v0, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2341093
    invoke-super {p0}, LX/GHg;->a()V

    .line 2341094
    iget-object v0, p0, LX/GKb;->b:LX/GKT;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2341095
    iput-object v1, p0, LX/GKb;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    .line 2341096
    iput-object v1, p0, LX/GKb;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewPendingView;

    .line 2341097
    return-void
.end method

.method public final a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2341090
    invoke-super {p0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2341091
    iget-object v0, p0, LX/GKb;->b:LX/GKT;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2341092
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2341089
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;

    invoke-direct {p0, p1, p2}, LX/GKb;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 4

    .prologue
    .line 2341077
    iput-object p1, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2341078
    iget-object v0, p0, LX/GKb;->b:LX/GKT;

    invoke-virtual {v0, p1}, LX/GKT;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2341079
    iget-object v0, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    iput-object v0, p0, LX/GKb;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    .line 2341080
    iget-object v0, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    instance-of v0, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    .line 2341081
    iget-object v0, p0, LX/GKb;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341082
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2341083
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    iput-object v1, p0, LX/GKb;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2341084
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->D()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    iput-object v1, p0, LX/GKb;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2341085
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v2

    iput-wide v2, p0, LX/GKb;->g:J

    .line 2341086
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v1

    .line 2341087
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->G()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v1

    invoke-static {p1, v2, v3, v0, v1}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;LX/0Px;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    iput-object v0, p0, LX/GKb;->h:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2341088
    :cond_0
    return-void
.end method
