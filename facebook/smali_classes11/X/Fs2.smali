.class public LX/Fs2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/9lP;

.field public final c:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/1dy;

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/concurrent/Executor;

.field public i:LX/Fs1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/9lP;Lcom/facebook/common/callercontext/CallerContext;LX/0Or;LX/0Ot;LX/1dy;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9lP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/9lP;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/1dy;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2296517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296518
    iput-object p1, p0, LX/Fs2;->a:Landroid/content/Context;

    .line 2296519
    iput-object p2, p0, LX/Fs2;->b:LX/9lP;

    .line 2296520
    iput-object p3, p0, LX/Fs2;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2296521
    iput-object p4, p0, LX/Fs2;->d:LX/0Or;

    .line 2296522
    iput-object p5, p0, LX/Fs2;->e:LX/0Ot;

    .line 2296523
    iput-object p6, p0, LX/Fs2;->f:LX/1dy;

    .line 2296524
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Fs2;->g:Ljava/util/Set;

    .line 2296525
    sget-object v0, LX/Fs1;->VISIBLE:LX/Fs1;

    iput-object v0, p0, LX/Fs2;->i:LX/Fs1;

    .line 2296526
    iput-object p7, p0, LX/Fs2;->h:Ljava/util/concurrent/Executor;

    .line 2296527
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2296529
    iget-object v0, p0, LX/Fs2;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2296530
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2296531
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2296532
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2296533
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 2296534
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2296535
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2296536
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_1

    .line 2296537
    :cond_1
    return-void
.end method

.method public final a(LX/1Zp;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1Zp",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2296538
    iget-object v0, p0, LX/Fs2;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sg;

    invoke-virtual {v0, p1}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2296539
    iget-object v0, p0, LX/Fs2;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2296540
    new-instance v0, LX/Fs0;

    invoke-direct {v0, p0, p1}, LX/Fs0;-><init>(LX/Fs2;LX/1Zp;)V

    iget-object v1, p0, LX/Fs2;->h:Ljava/util/concurrent/Executor;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2296541
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2296528
    iget-object v0, p0, LX/Fs2;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
