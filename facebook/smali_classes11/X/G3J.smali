.class public final LX/G3J;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:Landroid/animation/AnimatorListenerAdapter;

.field public final synthetic b:LX/G3K;


# direct methods
.method public constructor <init>(LX/G3K;Landroid/animation/AnimatorListenerAdapter;)V
    .locals 0

    .prologue
    .line 2315156
    iput-object p1, p0, LX/G3J;->b:LX/G3K;

    iput-object p2, p0, LX/G3J;->a:Landroid/animation/AnimatorListenerAdapter;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 2315157
    iget-object v0, p0, LX/G3J;->b:LX/G3K;

    iget-object v0, v0, LX/G3K;->e:LX/G46;

    iget-object v1, p0, LX/G3J;->b:LX/G3K;

    iget-object v1, v1, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315158
    iget-object v2, v1, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    move-object v1, v2

    .line 2315159
    iget-object v2, p0, LX/G3J;->b:LX/G3K;

    iget-object v2, v2, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315160
    iget-object v3, v2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    move-object v2, v3

    .line 2315161
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, LX/G3J;->a:Landroid/animation/AnimatorListenerAdapter;

    .line 2315162
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, v2}, Landroid/view/ViewPropertyAnimator;->translationXBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    iget-object p1, v0, LX/G46;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2315163
    return-void
.end method
