.class public final LX/Guq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:Lcom/facebook/webview/FacebookWebView;

.field public final synthetic b:LX/Gui;


# direct methods
.method public constructor <init>(LX/Gui;Lcom/facebook/webview/FacebookWebView;)V
    .locals 0

    .prologue
    .line 2403981
    iput-object p1, p0, LX/Guq;->b:LX/Gui;

    iput-object p2, p0, LX/Guq;->a:Lcom/facebook/webview/FacebookWebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2403974
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 2403975
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2403976
    iget-object v1, p0, LX/Guq;->b:LX/Gui;

    iget-boolean v1, v1, LX/Gui;->d:Z

    if-nez v1, :cond_0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2403977
    :cond_0
    iget-object v0, p0, LX/Guq;->b:LX/Gui;

    iget-object v1, p0, LX/Guq;->a:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, v1, p1}, LX/Gui;->a(Lcom/facebook/webview/FacebookWebView;Landroid/widget/TextView;)V

    .line 2403978
    new-array v0, v2, [C

    invoke-virtual {p1, v0, v2, v2}, Landroid/widget/TextView;->setText([CII)V

    .line 2403979
    iget-object v0, p0, LX/Guq;->b:LX/Gui;

    iget-object v0, v0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2403980
    :cond_1
    return v2
.end method
