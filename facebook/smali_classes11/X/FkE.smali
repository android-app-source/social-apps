.class public LX/FkE;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2278348
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "sideloading/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2278349
    sput-object v0, LX/FkE;->a:LX/0Tn;

    const-string v1, "download"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/FkE;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2278341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)LX/0Tn;
    .locals 1

    .prologue
    .line 2278347
    sget-object v0, LX/FkE;->b:LX/0Tn;

    invoke-virtual {v0, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 2278346
    invoke-static {p0}, LX/FkE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    const-string v1, "download_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static d(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 2278345
    invoke-static {p0}, LX/FkE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    const-string v1, "local_file"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static e(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 2278344
    invoke-static {p0}, LX/FkE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    const-string v1, "file_size"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static f(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 2278343
    invoke-static {p0}, LX/FkE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static g(Ljava/lang/String;)LX/0Tn;
    .locals 2

    .prologue
    .line 2278342
    invoke-static {p0}, LX/FkE;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    const-string v1, "download_status"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
