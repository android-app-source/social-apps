.class public LX/GVB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2358695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/fbui/facepile/FacepileView;LX/0Px;)V
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setFaces"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v1, 0xb

    const/4 v2, 0x0

    .line 2358696
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 2358697
    invoke-virtual {p1, v2, v1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object p1

    .line 2358698
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2358699
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2358700
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    new-instance v6, LX/6UY;

    invoke-virtual {v5, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v6, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2358701
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2358702
    :cond_1
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 2358703
    return-void
.end method
