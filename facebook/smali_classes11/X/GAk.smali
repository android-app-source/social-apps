.class public LX/GAk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/GAU;

.field private final b:Landroid/os/Handler;

.field private final c:J

.field private d:J

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;LX/GAU;)V
    .locals 2

    .prologue
    .line 2326130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326131
    iput-object p2, p0, LX/GAk;->a:LX/GAU;

    .line 2326132
    iput-object p1, p0, LX/GAk;->b:Landroid/os/Handler;

    .line 2326133
    invoke-static {}, LX/GAK;->h()J

    move-result-wide v0

    iput-wide v0, p0, LX/GAk;->c:J

    .line 2326134
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2326135
    iget-wide v0, p0, LX/GAk;->d:J

    iget-wide v2, p0, LX/GAk;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2326136
    iget-object v0, p0, LX/GAk;->a:LX/GAU;

    .line 2326137
    iget-object v1, v0, LX/GAU;->l:LX/GA2;

    move-object v3, v1

    .line 2326138
    iget-wide v0, p0, LX/GAk;->f:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    instance-of v0, v3, LX/GAR;

    if-eqz v0, :cond_1

    .line 2326139
    iget-wide v4, p0, LX/GAk;->d:J

    .line 2326140
    iget-wide v6, p0, LX/GAk;->f:J

    .line 2326141
    check-cast v3, LX/GAR;

    .line 2326142
    iget-object v0, p0, LX/GAk;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 2326143
    iget-object v0, p0, LX/GAk;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/RequestProgress$1;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/facebook/RequestProgress$1;-><init>(LX/GAk;LX/GAR;JJ)V

    const v2, 0x3d09b2be

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2326144
    :cond_0
    iget-wide v0, p0, LX/GAk;->d:J

    iput-wide v0, p0, LX/GAk;->e:J

    .line 2326145
    :cond_1
    return-void
.end method

.method public final a(J)V
    .locals 7

    .prologue
    .line 2326146
    iget-wide v0, p0, LX/GAk;->d:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/GAk;->d:J

    .line 2326147
    iget-wide v0, p0, LX/GAk;->d:J

    iget-wide v2, p0, LX/GAk;->e:J

    iget-wide v4, p0, LX/GAk;->c:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, LX/GAk;->d:J

    iget-wide v2, p0, LX/GAk;->f:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 2326148
    :cond_0
    invoke-virtual {p0}, LX/GAk;->a()V

    .line 2326149
    :cond_1
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 2326150
    iget-wide v0, p0, LX/GAk;->f:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/GAk;->f:J

    .line 2326151
    return-void
.end method
