.class public LX/FAg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field private b:Z

.field public c:Landroid/widget/LinearLayout;

.field public d:LX/Gpd;

.field private e:Landroid/widget/FrameLayout;

.field public final f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 2207956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2207957
    iput-object p1, p0, LX/FAg;->a:Landroid/view/View;

    .line 2207958
    iput-boolean p2, p0, LX/FAg;->b:Z

    .line 2207959
    const v0, 0x7f0d1881

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/FAg;->e:Landroid/widget/FrameLayout;

    .line 2207960
    const v0, 0x7f0d1882

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/FAg;->f:Landroid/view/View;

    .line 2207961
    return-void
.end method

.method public static a(LX/FAg;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2207962
    iget-object v0, p0, LX/FAg;->a:Landroid/view/View;

    const v1, 0x7f0d181f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2207963
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2207964
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2207965
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2207966
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2207967
    return-void
.end method

.method public static a(LX/FAg;LX/GoP;)V
    .locals 2

    .prologue
    .line 2207968
    iget-object v0, p0, LX/FAg;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1821

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2207969
    new-instance v1, LX/GqK;

    invoke-direct {v1, v0}, LX/GqK;-><init>(Landroid/view/View;)V

    move-object v0, v1

    .line 2207970
    new-instance v1, LX/Gpd;

    invoke-direct {v1, v0, p1}, LX/Gpd;-><init>(LX/GqK;LX/GoP;)V

    iput-object v1, p0, LX/FAg;->d:LX/Gpd;

    .line 2207971
    iget-object v1, p0, LX/FAg;->d:LX/Gpd;

    invoke-interface {v0, v1}, LX/CnG;->a(LX/CnT;)V

    .line 2207972
    iget-object v0, p0, LX/FAg;->d:LX/Gpd;

    invoke-static {}, LX/GoK;->a()LX/Gp5;

    invoke-virtual {v0}, LX/Gpd;->b()V

    .line 2207973
    return-void
.end method

.method public static b(LX/FAg;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2207974
    iget-object v0, p0, LX/FAg;->a:Landroid/view/View;

    const v1, 0x7f0d1820

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2207975
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2207976
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2207977
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2207978
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2207979
    return-void
.end method

.method public static b(LX/FAg;Z)V
    .locals 3

    .prologue
    .line 2207980
    iget-boolean v0, p0, LX/FAg;->b:Z

    if-nez v0, :cond_0

    .line 2207981
    iget-object v0, p0, LX/FAg;->e:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2207982
    :goto_0
    return-void

    .line 2207983
    :cond_0
    iget-object v0, p0, LX/FAg;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2207984
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1c0f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 2207985
    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v0, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 2207986
    if-eqz p1, :cond_2

    add-int/2addr v0, v1

    move v1, v0

    .line 2207987
    :goto_1
    iget-object v0, p0, LX/FAg;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2207988
    if-eqz v0, :cond_1

    .line 2207989
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 2207990
    :cond_1
    iget-object v1, p0, LX/FAg;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_2
    move v1, v0

    .line 2207991
    goto :goto_1
.end method
