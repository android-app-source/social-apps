.class public final LX/GNM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;)V
    .locals 0

    .prologue
    .line 2345837
    iput-object p1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1e5c7c1a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2345838
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    .line 2345839
    iget v2, v1, LX/GNP;->l:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v1, LX/GNP;->l:I

    .line 2345840
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget v1, v1, LX/GNP;->l:I

    iget-object v2, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v2, v2, LX/GNP;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 2345841
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->q:LX/GG3;

    iget-object v2, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v2, v2, LX/GNP;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/GG3;->b(Ljava/lang/String;)V

    .line 2345842
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->q:LX/GG3;

    iget-object v2, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v2, v2, LX/GNP;->o:Ljava/lang/String;

    .line 2345843
    iget-object v6, v1, LX/GG3;->h:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const-string v7, "submit_flow_click"

    const-string v8, "walkthrough"

    const/4 v10, 0x0

    move-object v5, v1

    move-object v9, v2

    invoke-static/range {v5 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2345844
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->h:Lcom/facebook/adinterfaces/walkthrough/HoleView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a()V

    .line 2345845
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->d:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v2, v2, LX/GNP;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0d3184

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2345846
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->p:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 2345847
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->p:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2345848
    :cond_0
    const v1, -0x66da9a53

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2345849
    :goto_0
    return-void

    .line 2345850
    :cond_1
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->j:Landroid/widget/TextView;

    const v2, 0x7f080b4a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2345851
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v2, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v2, v2, LX/GNP;->i:Landroid/widget/TextView;

    invoke-static {v1, v2}, LX/GNP;->a$redex0(LX/GNP;Landroid/view/View;)V

    .line 2345852
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget v1, v1, LX/GNP;->l:I

    iget-object v2, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v2, v2, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v2, v2, LX/GNP;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2

    .line 2345853
    iget-object v1, p0, LX/GNM;->a:Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    iget-object v1, v1, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;->a:LX/GNP;

    iget-object v1, v1, LX/GNP;->i:Landroid/widget/TextView;

    const v2, 0x7f080b4b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2345854
    :cond_2
    const v1, -0x6cad8cbd

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
