.class public LX/GWl;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/7ix;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7iw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2362717
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2362718
    const-class v0, LX/GWl;

    invoke-static {v0, p0}, LX/GWl;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2362719
    const v0, 0x7f031035

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2362720
    const v0, 0x7f0a0048

    invoke-virtual {p0, v0}, LX/GWl;->setBackgroundResource(I)V

    .line 2362721
    const v0, 0x7f0d26dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2362722
    iget-object p1, p0, LX/GWl;->a:LX/7ix;

    invoke-virtual {p1, v0}, LX/7ix;->a(Lcom/facebook/fbui/widget/contentview/ContentView;)LX/7iw;

    move-result-object v0

    iput-object v0, p0, LX/GWl;->b:LX/7iw;

    .line 2362723
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/GWl;

    const-class p0, LX/7ix;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/7ix;

    iput-object v1, p1, LX/GWl;->a:LX/7ix;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2362724
    check-cast p1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2362725
    iget-object v0, p0, LX/GWl;->b:LX/7iw;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2362726
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v4

    .line 2362727
    if-nez v4, :cond_0

    .line 2362728
    :goto_0
    return-void

    .line 2362729
    :cond_0
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2362730
    iput-object v1, v0, LX/7iw;->c:Ljava/lang/String;

    .line 2362731
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7iw;->a(Ljava/lang/String;)V

    .line 2362732
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->o()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_3

    .line 2362733
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->o()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2362734
    invoke-virtual {v5, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    .line 2362735
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->o()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v5, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7iw;->a(Landroid/net/Uri;)V

    .line 2362736
    :goto_2
    const-string v1, ""

    .line 2362737
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->j()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->n()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_5

    :goto_3
    if-eqz v2, :cond_1

    .line 2362738
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2362739
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->n()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v4, v2, v3}, LX/15i;->j(II)I

    move-result v3

    .line 2362740
    :cond_1
    invoke-virtual {v0, v1, v3}, LX/7iw;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 2362741
    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_1

    .line 2362742
    :cond_4
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/7iw;->a(Landroid/net/Uri;)V

    goto :goto_2

    :cond_5
    move v2, v3

    .line 2362743
    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_3
.end method
