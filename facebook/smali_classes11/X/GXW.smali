.class public final LX/GXW;
.super LX/3x6;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 2364183
    iput-object p1, p0, LX/GXW;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 2364184
    const v0, 0x7f0b1d36

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/GXW;->b:I

    .line 2364185
    const v0, 0x7f0b1d37

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/GXW;->c:I

    .line 2364186
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2364187
    invoke-super {p0, p1, p2, p3, p4}, LX/3x6;->a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 2364188
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 2364189
    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v0

    .line 2364190
    iget-object v2, p0, LX/GXW;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v2, v2, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->j:LX/GXX;

    invoke-virtual {v2, v0}, LX/GXX;->a(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 2364191
    iget-object v2, p0, LX/GXW;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    invoke-virtual {v2, v0}, LX/1OM;->getItemViewType(I)I

    move-result v0

    sget v2, LX/GXg;->d:I

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 2364192
    :goto_0
    invoke-virtual {p1, v0, v1, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 2364193
    :goto_1
    return-void

    .line 2364194
    :cond_0
    iget v0, p0, LX/GXW;->b:I

    goto :goto_0

    .line 2364195
    :cond_1
    iget-object v2, p0, LX/GXW;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v2, v2, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->j:LX/GXX;

    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/3wr;->a(II)I

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 2364196
    if-eqz v0, :cond_2

    .line 2364197
    iget v0, p0, LX/GXW;->b:I

    iget v2, p0, LX/GXW;->c:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p1, v0, v1, v2, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    .line 2364198
    :cond_2
    iget v0, p0, LX/GXW;->c:I

    div-int/lit8 v0, v0, 0x2

    iget v2, p0, LX/GXW;->b:I

    invoke-virtual {p1, v0, v1, v2, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method
