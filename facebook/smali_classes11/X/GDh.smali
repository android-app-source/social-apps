.class public final LX/GDh;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GDi;

.field public final synthetic b:LX/GDk;


# direct methods
.method public constructor <init>(LX/GDk;LX/GDi;)V
    .locals 0

    .prologue
    .line 2330248
    iput-object p1, p0, LX/GDh;->b:LX/GDk;

    iput-object p2, p0, LX/GDh;->a:LX/GDi;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2330249
    iget-object v0, p0, LX/GDh;->b:LX/GDk;

    .line 2330250
    iget-object v1, v0, LX/GDk;->a:LX/4At;

    invoke-virtual {v1}, LX/4At;->stopShowingProgress()V

    .line 2330251
    instance-of v1, p1, LX/4Ua;

    if-eqz v1, :cond_0

    .line 2330252
    check-cast p1, LX/4Ua;

    .line 2330253
    iget-object v1, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget-object v1, v1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    const-string v2, "&#039;"

    const-string p0, "\'"

    invoke-virtual {v1, v2, p0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2330254
    iget-object v2, v0, LX/GDk;->d:LX/GF4;

    new-instance p0, LX/GFP;

    invoke-direct {p0, v1}, LX/GFP;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b7;)V

    .line 2330255
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2330256
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2330257
    iget-object v0, p0, LX/GDh;->b:LX/GDk;

    iget-object v0, v0, LX/GDk;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2330258
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330259
    check-cast v0, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/CreateSavedAudienceMutationModels$CreateSavedAudienceMutationModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2330260
    const/4 v2, 0x0

    const-class v3, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    .line 2330261
    new-instance v1, LX/A94;

    invoke-direct {v1}, LX/A94;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2330262
    iput-object v2, v1, LX/A94;->c:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2330263
    move-object v1, v1

    .line 2330264
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2330265
    iput-object v2, v1, LX/A94;->d:Ljava/lang/String;

    .line 2330266
    move-object v1, v1

    .line 2330267
    const/4 v2, 0x1

    .line 2330268
    iput-boolean v2, v1, LX/A94;->e:Z

    .line 2330269
    move-object v1, v1

    .line 2330270
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2330271
    iput-object v2, v1, LX/A94;->g:Ljava/lang/String;

    .line 2330272
    move-object v1, v1

    .line 2330273
    sget-object v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->b:LX/0Px;

    .line 2330274
    iput-object v2, v1, LX/A94;->h:LX/0Px;

    .line 2330275
    move-object v1, v1

    .line 2330276
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;->m()LX/2uF;

    move-result-object v2

    .line 2330277
    iput-object v2, v1, LX/A94;->j:LX/2uF;

    .line 2330278
    move-object v1, v1

    .line 2330279
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    .line 2330280
    iput-object v0, v1, LX/A94;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 2330281
    move-object v0, v1

    .line 2330282
    invoke-virtual {v0}, LX/A94;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v0

    .line 2330283
    iget-object v1, p0, LX/GDh;->a:LX/GDi;

    invoke-interface {v1, v0}, LX/GDi;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    .line 2330284
    return-void
.end method
