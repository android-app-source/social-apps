.class public LX/F08;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0fz;

.field public final c:LX/1Ck;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

.field private final g:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:LX/F0B;

.field public j:LX/F1Y;

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2188004
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "friendversary"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/F08;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0fz;LX/0Ot;)V
    .locals 1
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/0fz;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2187956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187957
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/F08;->g:Ljava/util/HashSet;

    .line 2187958
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/F08;->h:Z

    .line 2187959
    iput-object p1, p0, LX/F08;->c:LX/1Ck;

    .line 2187960
    iput-object p2, p0, LX/F08;->b:LX/0fz;

    .line 2187961
    iput-object p3, p0, LX/F08;->d:LX/0Ot;

    .line 2187962
    return-void
.end method

.method public static a(LX/0QB;)LX/F08;
    .locals 4

    .prologue
    .line 2188010
    new-instance v2, LX/F08;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/0fz;->b(LX/0QB;)LX/0fz;

    move-result-object v1

    check-cast v1, LX/0fz;

    const/16 v3, 0x1430

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/F08;-><init>(LX/1Ck;LX/0fz;LX/0Ot;)V

    .line 2188011
    move-object v0, v2

    .line 2188012
    return-object v0
.end method

.method public static a(LX/F08;Z)V
    .locals 2

    .prologue
    .line 2188005
    iget-object v0, p0, LX/F08;->j:LX/F1Y;

    .line 2188006
    iget-object v1, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2188007
    iget-object p0, v0, LX/F1Y;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object p0, p0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->z:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2188008
    invoke-static {p1, p0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a(ZLcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    .line 2188009
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/F08;ZLjava/util/Map;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2187963
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/F08;->a(LX/F08;Z)V

    .line 2187964
    iget-object v1, p0, LX/F08;->i:LX/F0B;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 2187965
    :goto_0
    iget-boolean v2, p0, LX/F08;->m:Z

    if-eqz v2, :cond_1

    .line 2187966
    sget-object v2, LX/0gf;->PULL_TO_REFRESH:LX/0gf;

    .line 2187967
    :goto_1
    move-object v2, v2

    .line 2187968
    if-nez v0, :cond_3

    const/4 v3, 0x1

    .line 2187969
    :goto_2
    if-eqz v3, :cond_4

    .line 2187970
    invoke-static {v1, v2, p2}, LX/F0B;->a(LX/F0B;LX/0gf;Ljava/util/Map;)LX/0zO;

    move-result-object v3

    .line 2187971
    iget-object p0, v1, LX/F0B;->d:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2187972
    iget-object p0, v1, LX/F0B;->a:LX/0QK;

    iget-object p1, v1, LX/F0B;->c:Ljava/util/concurrent/Executor;

    invoke-static {v3, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2187973
    :goto_3
    move-object v0, v3

    .line 2187974
    return-object v0

    :cond_0
    iget-object v0, p0, LX/F08;->b:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->s()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2187975
    :cond_1
    iget-boolean v2, p0, LX/F08;->l:Z

    if-eqz v2, :cond_2

    .line 2187976
    sget-object v2, LX/0gf;->INITIALIZATION:LX/0gf;

    goto :goto_1

    .line 2187977
    :cond_2
    sget-object v2, LX/0gf;->SCROLLING:LX/0gf;

    goto :goto_1

    .line 2187978
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 2187979
    :cond_4
    new-instance v3, LX/0rT;

    invoke-direct {v3}, LX/0rT;-><init>()V

    iget p0, v1, LX/F0B;->i:I

    .line 2187980
    iput p0, v3, LX/0rT;->c:I

    .line 2187981
    move-object v3, v3

    .line 2187982
    iget-object p0, v1, LX/F0B;->j:Lcom/facebook/api/feedtype/FeedType;

    .line 2187983
    iput-object p0, v3, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 2187984
    move-object v3, v3

    .line 2187985
    sget-object p0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2187986
    iput-object p0, v3, LX/0rT;->a:LX/0rS;

    .line 2187987
    move-object v3, v3

    .line 2187988
    sget-object p0, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2187989
    iput-object p0, v3, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2187990
    move-object v3, v3

    .line 2187991
    invoke-virtual {v3, v2}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v3

    .line 2187992
    iput-object v0, v3, LX/0rT;->f:Ljava/lang/String;

    .line 2187993
    move-object v3, v3

    .line 2187994
    invoke-virtual {v3}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    .line 2187995
    new-instance p0, LX/F0M;

    invoke-direct {p0}, LX/F0M;-><init>()V

    move-object p0, p0

    .line 2187996
    invoke-static {v1, p0, v3}, LX/F0B;->a(LX/F0B;LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 2187997
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object p0, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, p0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2187998
    move-object v3, v3

    .line 2187999
    iget-object p0, v1, LX/F0B;->d:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2188000
    iget-object p0, v1, LX/F0B;->b:LX/0QK;

    iget-object p1, v1, LX/F0B;->c:Ljava/util/concurrent/Executor;

    invoke-static {v3, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_3
.end method

.method private b(Z)V
    .locals 5

    .prologue
    .line 2188001
    iput-boolean p1, p0, LX/F08;->m:Z

    .line 2188002
    iget-object v0, p0, LX/F08;->c:LX/1Ck;

    sget-object v1, LX/F07;->REFRESH_FEED:LX/F07;

    new-instance v2, LX/F05;

    invoke-direct {v2, p0}, LX/F05;-><init>(LX/F08;)V

    new-instance v3, LX/F06;

    invoke-direct {v3, p0}, LX/F06;-><init>(LX/F08;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2188003
    return-void
.end method

.method public static k(LX/F08;)V
    .locals 3

    .prologue
    .line 2187944
    iget-object v0, p0, LX/F08;->c:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2187945
    iget-object v0, p0, LX/F08;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/goodwill/feed/data/ThrowbackFeedPager$3;

    invoke-direct {v1, p0}, Lcom/facebook/goodwill/feed/data/ThrowbackFeedPager$3;-><init>(LX/F08;)V

    const v2, 0x6f8c9357

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2187946
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/F08;->k:Z

    .line 2187947
    return-void
.end method


# virtual methods
.method public final clearUserData()V
    .locals 0

    .prologue
    .line 2187948
    invoke-static {p0}, LX/F08;->k(LX/F08;)V

    .line 2187949
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2187950
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/F08;->b(Z)V

    .line 2187951
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2187952
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/F08;->n:Z

    .line 2187953
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/F08;->b(Z)V

    .line 2187954
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2187955
    const-class v0, LX/F08;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "totalStories"

    iget-object v2, p0, LX/F08;->b:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "freshStories"

    iget-object v2, p0, LX/F08;->b:LX/0fz;

    invoke-virtual {v2}, LX/0fz;->w()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "mHasReachedEndOfFeed"

    iget-boolean v2, p0, LX/F08;->k:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
