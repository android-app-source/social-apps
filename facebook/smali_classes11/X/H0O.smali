.class public final enum LX/H0O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H0O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H0O;

.field public static final enum SECTION_GAP:LX/H0O;


# instance fields
.field private final viewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2413794
    new-instance v0, LX/H0O;

    const-string v1, "SECTION_GAP"

    const v2, 0x7f0d00e8

    invoke-direct {v0, v1, v3, v2}, LX/H0O;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0O;->SECTION_GAP:LX/H0O;

    .line 2413795
    const/4 v0, 0x1

    new-array v0, v0, [LX/H0O;

    sget-object v1, LX/H0O;->SECTION_GAP:LX/H0O;

    aput-object v1, v0, v3

    sput-object v0, LX/H0O;->$VALUES:[LX/H0O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2413796
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2413797
    iput p3, p0, LX/H0O;->viewType:I

    .line 2413798
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H0O;
    .locals 1

    .prologue
    .line 2413799
    const-class v0, LX/H0O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H0O;

    return-object v0
.end method

.method public static values()[LX/H0O;
    .locals 1

    .prologue
    .line 2413800
    sget-object v0, LX/H0O;->$VALUES:[LX/H0O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H0O;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2413801
    iget v0, p0, LX/H0O;->viewType:I

    return v0
.end method
