.class public LX/GnI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/15i;

.field private final b:I

.field public final synthetic c:LX/GnL;


# direct methods
.method public constructor <init>(LX/GnL;LX/15i;I)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2393662
    iput-object p1, p0, LX/GnI;->c:LX/GnL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2393663
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p2, p0, LX/GnI;->a:LX/15i;

    iput p3, p0, LX/GnI;->b:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2393664
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/GnI;->a:LX/15i;

    iget v2, p0, LX/GnI;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;
    .locals 4

    .prologue
    .line 2393665
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/GnI;->a:LX/15i;

    iget v2, p0, LX/GnI;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    const-class v3, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;

    invoke-virtual {v0, v2, v1, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 2393666
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/GnI;->a:LX/15i;

    iget v2, p0, LX/GnI;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, LX/15i;->h(II)Z

    move-result v0

    return v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
