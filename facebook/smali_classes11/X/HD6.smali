.class public final LX/HD6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2440814
    const/4 v10, 0x0

    .line 2440815
    const/4 v9, 0x0

    .line 2440816
    const/4 v8, 0x0

    .line 2440817
    const/4 v7, 0x0

    .line 2440818
    const/4 v6, 0x0

    .line 2440819
    const/4 v5, 0x0

    .line 2440820
    const/4 v4, 0x0

    .line 2440821
    const/4 v3, 0x0

    .line 2440822
    const/4 v2, 0x0

    .line 2440823
    const/4 v1, 0x0

    .line 2440824
    const/4 v0, 0x0

    .line 2440825
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 2440826
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440827
    const/4 v0, 0x0

    .line 2440828
    :goto_0
    return v0

    .line 2440829
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440830
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 2440831
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2440832
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2440833
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2440834
    const-string v12, "action_bar_actions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2440835
    invoke-static {p0, p1}, LX/9YX;->b(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2440836
    :cond_2
    const-string v12, "description"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2440837
    invoke-static {p0, p1}, LX/HD0;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2440838
    :cond_3
    const-string v12, "image"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2440839
    invoke-static {p0, p1}, LX/HD1;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2440840
    :cond_4
    const-string v12, "is_current"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2440841
    const/4 v1, 0x1

    .line 2440842
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v7

    goto :goto_1

    .line 2440843
    :cond_5
    const-string v12, "is_recommended"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2440844
    const/4 v0, 0x1

    .line 2440845
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    goto :goto_1

    .line 2440846
    :cond_6
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2440847
    invoke-static {p0, p1}, LX/HD2;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2440848
    :cond_7
    const-string v12, "primary_buttons_actions"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2440849
    invoke-static {p0, p1}, LX/9YX;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2440850
    :cond_8
    const-string v12, "tabs"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 2440851
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2440852
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, v12, :cond_9

    .line 2440853
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, v12, :cond_9

    .line 2440854
    invoke-static {p0, p1}, LX/HD5;->b(LX/15w;LX/186;)I

    move-result v11

    .line 2440855
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2440856
    :cond_9
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2440857
    goto/16 :goto_1

    .line 2440858
    :cond_a
    const-string v12, "template_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2440859
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto/16 :goto_1

    .line 2440860
    :cond_b
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2440861
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 2440862
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2440863
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 2440864
    if-eqz v1, :cond_c

    .line 2440865
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 2440866
    :cond_c
    if-eqz v0, :cond_d

    .line 2440867
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 2440868
    :cond_d
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2440869
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2440870
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2440871
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2440872
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    .line 2440873
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2440874
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440875
    if-eqz v0, :cond_0

    .line 2440876
    const-string v1, "action_bar_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440877
    invoke-static {p0, v0, p2, p3}, LX/9YX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2440878
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440879
    if-eqz v0, :cond_1

    .line 2440880
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440881
    invoke-static {p0, v0, p2}, LX/HD0;->a(LX/15i;ILX/0nX;)V

    .line 2440882
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440883
    if-eqz v0, :cond_2

    .line 2440884
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440885
    invoke-static {p0, v0, p2}, LX/HD1;->a(LX/15i;ILX/0nX;)V

    .line 2440886
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2440887
    if-eqz v0, :cond_3

    .line 2440888
    const-string v1, "is_current"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440889
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2440890
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2440891
    if-eqz v0, :cond_4

    .line 2440892
    const-string v1, "is_recommended"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440893
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2440894
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440895
    if-eqz v0, :cond_5

    .line 2440896
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440897
    invoke-static {p0, v0, p2}, LX/HD2;->a(LX/15i;ILX/0nX;)V

    .line 2440898
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440899
    if-eqz v0, :cond_6

    .line 2440900
    const-string v1, "primary_buttons_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440901
    invoke-static {p0, v0, p2, p3}, LX/9YX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2440902
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440903
    if-eqz v0, :cond_8

    .line 2440904
    const-string v1, "tabs"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440905
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2440906
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 2440907
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/HD5;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2440908
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2440909
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2440910
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2440911
    if-eqz v0, :cond_9

    .line 2440912
    const-string v0, "template_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440913
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2440914
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2440915
    return-void
.end method
