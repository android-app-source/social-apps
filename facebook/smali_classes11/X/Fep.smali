.class public LX/Fep;
.super LX/1Yy;
.source ""


# instance fields
.field private final a:LX/CzE;

.field private final b:LX/1K9;


# direct methods
.method public constructor <init>(LX/CzE;LX/1K9;)V
    .locals 0
    .param p1    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2266778
    invoke-direct {p0}, LX/1Yy;-><init>()V

    .line 2266779
    iput-object p1, p0, LX/Fep;->a:LX/CzE;

    .line 2266780
    iput-object p2, p0, LX/Fep;->b:LX/1K9;

    .line 2266781
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2266773
    check-cast p1, LX/1Zj;

    .line 2266774
    iget-object v0, p0, LX/Fep;->a:LX/CzE;

    iget-object v1, p1, LX/1Zj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CzE;->a(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2266775
    if-nez v0, :cond_0

    .line 2266776
    :goto_0
    return-void

    .line 2266777
    :cond_0
    iget-object v1, p0, LX/Fep;->b:LX/1K9;

    new-instance v2, LX/EJ2;

    iget-boolean v3, p1, LX/1Zj;->e:Z

    invoke-direct {v2, v0, v3}, LX/EJ2;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_0
.end method
