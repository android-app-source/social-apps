.class public final LX/GXE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/GXH;


# direct methods
.method public constructor <init>(LX/GXH;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2363561
    iput-object p1, p0, LX/GXE;->b:LX/GXH;

    iput-object p2, p0, LX/GXE;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2363562
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2363563
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2363564
    if-nez p1, :cond_0

    .line 2363565
    :goto_0
    return-void

    .line 2363566
    :cond_0
    iget-object v0, p0, LX/GXE;->b:LX/GXH;

    iget-object v0, v0, LX/GXH;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v1, p0, LX/GXE;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->do:Ljava/lang/String;

    .line 2363567
    iget-object v3, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v3, v3

    .line 2363568
    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2363569
    iget-object v0, p0, LX/GXE;->b:LX/GXH;

    iget-object v0, v0, LX/GXH;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GXE;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
