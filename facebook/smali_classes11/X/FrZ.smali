.class public final LX/FrZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineContextItemsGraphQLModels$TimelineContextItemsQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Ve;

.field public final synthetic b:LX/Fra;


# direct methods
.method public constructor <init>(LX/Fra;LX/0Ve;)V
    .locals 0

    .prologue
    .line 2295791
    iput-object p1, p0, LX/FrZ;->b:LX/Fra;

    iput-object p2, p0, LX/FrZ;->a:LX/0Ve;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2295792
    iget-object v0, p0, LX/FrZ;->b:LX/Fra;

    iget-object v0, v0, LX/Fra;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a002a

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2295793
    iget-object v0, p0, LX/FrZ;->a:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2295794
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2295795
    check-cast p1, Lcom/facebook/timeline/protocol/FetchTimelineContextItemsGraphQLModels$TimelineContextItemsQueryModel;

    .line 2295796
    if-nez p1, :cond_0

    .line 2295797
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context items result was null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/FrZ;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2295798
    :goto_0
    return-void

    .line 2295799
    :cond_0
    iget-object v0, p0, LX/FrZ;->b:LX/Fra;

    iget-object v0, v0, LX/Fra;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a002a

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2295800
    iget-object v0, p0, LX/FrZ;->a:LX/0Ve;

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineContextItemsGraphQLModels$TimelineContextItemsQueryModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineContextItemsGraphQLModels$TimelineContextItemsModel;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
