.class public LX/GeZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GeX;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gea;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2375785
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/GeZ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Gea;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375805
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2375806
    iput-object p1, p0, LX/GeZ;->b:LX/0Ot;

    .line 2375807
    return-void
.end method

.method public static a(LX/0QB;)LX/GeZ;
    .locals 4

    .prologue
    .line 2375794
    const-class v1, LX/GeZ;

    monitor-enter v1

    .line 2375795
    :try_start_0
    sget-object v0, LX/GeZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2375796
    sput-object v2, LX/GeZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2375797
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2375798
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2375799
    new-instance v3, LX/GeZ;

    const/16 p0, 0x2100

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GeZ;-><init>(LX/0Ot;)V

    .line 2375800
    move-object v0, v3

    .line 2375801
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2375802
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GeZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375803
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2375804
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2375788
    check-cast p2, LX/GeY;

    .line 2375789
    iget-object v0, p0, LX/GeZ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gea;

    iget-object v1, p2, LX/GeY;->a:Lcom/facebook/java2js/JSValue;

    iget-object v2, p2, LX/GeY;->b:LX/5KI;

    const/4 p2, 0x1

    .line 2375790
    const-string v3, "profilePicture"

    invoke-virtual {v1, v3}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v3

    .line 2375791
    const-string v4, "title"

    invoke-virtual {v1, v4}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v4

    .line 2375792
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/Gea;->a:LX/CE0;

    invoke-virtual {v6, p1}, LX/CE0;->c(LX/1De;)LX/CDy;

    move-result-object v6

    const-string p0, "string"

    invoke-virtual {v4, p0}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/CDy;->a(Ljava/lang/CharSequence;)LX/CDy;

    move-result-object v4

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/CDy;->a(LX/1X1;)LX/CDy;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/CDy;->h(I)LX/CDy;

    move-result-object v3

    const-string v4, "subtitle"

    invoke-virtual {v1, v4}, Lcom/facebook/java2js/JSValue;->getStringProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/CDy;->b(Ljava/lang/CharSequence;)LX/CDy;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/CDy;->a(Z)LX/CDy;

    move-result-object v3

    const-string v4, "sponsored"

    invoke-virtual {v1, v4}, Lcom/facebook/java2js/JSValue;->getBooleanProperty(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, v4}, LX/CDy;->b(Z)LX/CDy;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x8

    invoke-interface {v3, p2, v4}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2375793
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2375786
    invoke-static {}, LX/1dS;->b()V

    .line 2375787
    const/4 v0, 0x0

    return-object v0
.end method
