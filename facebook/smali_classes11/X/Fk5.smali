.class public LX/Fk5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278147
    iput-object p1, p0, LX/Fk5;->a:LX/0Zb;

    .line 2278148
    return-void
.end method

.method public static b(LX/0QB;)LX/Fk5;
    .locals 2

    .prologue
    .line 2278149
    new-instance v1, LX/Fk5;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/Fk5;-><init>(LX/0Zb;)V

    .line 2278150
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2278151
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Fk5;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2278152
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2278153
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "sideloading"

    .line 2278154
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2278155
    move-object v2, v0

    .line 2278156
    if-eqz p2, :cond_0

    .line 2278157
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2278158
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2278159
    :cond_0
    iget-object v0, p0, LX/Fk5;->a:LX/0Zb;

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2278160
    return-void
.end method
