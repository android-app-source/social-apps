.class public final LX/H3Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<",
        "Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/search/NearbySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/search/NearbySearchFragment;)V
    .locals 0

    .prologue
    .line 2421117
    iput-object p1, p0, LX/H3Y;->a:Lcom/facebook/nearby/search/NearbySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;)Z
    .locals 3
    .param p0    # Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2421118
    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    iget-object v0, v0, Lcom/facebook/nearby/model/TypeaheadPlace;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2421119
    :goto_0
    if-nez v0, :cond_0

    .line 2421120
    sget-object v1, Lcom/facebook/nearby/search/NearbySearchFragment;->a:Ljava/lang/Class;

    const-string v2, "filtering out invalid TypeaheadPlace result"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2421121
    :cond_0
    return v0

    .line 2421122
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2421123
    check-cast p1, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    invoke-static {p1}, LX/H3Y;->a(Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;)Z

    move-result v0

    return v0
.end method
