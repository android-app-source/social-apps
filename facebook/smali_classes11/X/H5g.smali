.class public LX/H5g;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/2kk;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H5h;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/H5g",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H5h;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424685
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2424686
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/H5g;->b:LX/0Zi;

    .line 2424687
    iput-object p1, p0, LX/H5g;->a:LX/0Ot;

    .line 2424688
    return-void
.end method

.method public static a(LX/0QB;)LX/H5g;
    .locals 4

    .prologue
    .line 2424689
    const-class v1, LX/H5g;

    monitor-enter v1

    .line 2424690
    :try_start_0
    sget-object v0, LX/H5g;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424691
    sput-object v2, LX/H5g;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424692
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424693
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424694
    new-instance v3, LX/H5g;

    const/16 p0, 0x2ae2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/H5g;-><init>(LX/0Ot;)V

    .line 2424695
    move-object v0, v3

    .line 2424696
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424697
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424698
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424699
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2424700
    check-cast p2, LX/H5f;

    .line 2424701
    iget-object v0, p0, LX/H5g;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5h;

    iget-object v1, p2, LX/H5f;->a:LX/2nq;

    iget-object v2, p2, LX/H5f;->b:LX/1Pn;

    .line 2424702
    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2424703
    iget-object p0, v0, LX/H5h;->b:LX/2jO;

    .line 2424704
    iget-object p2, p0, LX/2jO;->i:LX/DqC;

    move-object p0, p2

    .line 2424705
    if-eqz p0, :cond_1

    .line 2424706
    iget-object p2, p0, LX/DqC;->a:Ljava/lang/String;

    move-object p0, p2

    .line 2424707
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2424708
    iget-object v3, v0, LX/H5h;->c:LX/H5b;

    const/4 p0, 0x0

    .line 2424709
    new-instance p2, LX/H5a;

    invoke-direct {p2, v3}, LX/H5a;-><init>(LX/H5b;)V

    .line 2424710
    iget-object v0, v3, LX/H5b;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5Z;

    .line 2424711
    if-nez v0, :cond_0

    .line 2424712
    new-instance v0, LX/H5Z;

    invoke-direct {v0, v3}, LX/H5Z;-><init>(LX/H5b;)V

    .line 2424713
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/H5Z;->a$redex0(LX/H5Z;LX/1De;IILX/H5a;)V

    .line 2424714
    move-object p2, v0

    .line 2424715
    move-object p0, p2

    .line 2424716
    move-object v3, p0

    .line 2424717
    iget-object p0, v3, LX/H5Z;->a:LX/H5a;

    iput-object v1, p0, LX/H5a;->a:LX/2nq;

    .line 2424718
    iget-object p0, v3, LX/H5Z;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2424719
    move-object v3, v3

    .line 2424720
    iget-object p0, v3, LX/H5Z;->a:LX/H5a;

    iput-object v2, p0, LX/H5a;->b:LX/1Pn;

    .line 2424721
    iget-object p0, v3, LX/H5Z;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2424722
    move-object v3, v3

    .line 2424723
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    .line 2424724
    :goto_0
    move-object v0, v3

    .line 2424725
    return-object v0

    :cond_1
    iget-object v3, v0, LX/H5h;->a:LX/H5L;

    const/4 p0, 0x0

    .line 2424726
    new-instance p2, LX/H5K;

    invoke-direct {p2, v3}, LX/H5K;-><init>(LX/H5L;)V

    .line 2424727
    iget-object v0, v3, LX/H5L;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5J;

    .line 2424728
    if-nez v0, :cond_2

    .line 2424729
    new-instance v0, LX/H5J;

    invoke-direct {v0, v3}, LX/H5J;-><init>(LX/H5L;)V

    .line 2424730
    :cond_2
    invoke-static {v0, p1, p0, p0, p2}, LX/H5J;->a$redex0(LX/H5J;LX/1De;IILX/H5K;)V

    .line 2424731
    move-object p2, v0

    .line 2424732
    move-object p0, p2

    .line 2424733
    move-object v3, p0

    .line 2424734
    iget-object p0, v3, LX/H5J;->a:LX/H5K;

    iput-object v1, p0, LX/H5K;->a:LX/2nq;

    .line 2424735
    iget-object p0, v3, LX/H5J;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2424736
    move-object v3, v3

    .line 2424737
    iget-object p0, v3, LX/H5J;->a:LX/H5K;

    iput-object v2, p0, LX/H5K;->b:LX/1Pn;

    .line 2424738
    iget-object p0, v3, LX/H5J;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2424739
    move-object v3, v3

    .line 2424740
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2424741
    invoke-static {}, LX/1dS;->b()V

    .line 2424742
    const/4 v0, 0x0

    return-object v0
.end method
