.class public final LX/Gcc;
.super LX/6Il;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:LX/Gcg;


# direct methods
.method public constructor <init>(LX/Gcg;II)V
    .locals 0

    .prologue
    .line 2372117
    iput-object p1, p0, LX/Gcc;->c:LX/Gcg;

    iput p2, p0, LX/Gcc;->a:I

    iput p3, p0, LX/Gcc;->b:I

    invoke-direct {p0}, LX/6Il;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2372118
    iget-object v0, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->o:LX/6Ia;

    invoke-interface {v0, p0}, LX/6Ia;->b(LX/6Ik;)V

    .line 2372119
    return-void
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 2372120
    :try_start_0
    iget-object v0, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->o:LX/6Ia;

    invoke-interface {v0}, LX/6Ia;->a()LX/6IP;

    move-result-object v0

    .line 2372121
    iget-object v1, p0, LX/Gcc;->c:LX/Gcg;

    invoke-interface {v0}, LX/6IP;->c()Ljava/util/List;

    move-result-object v2

    iget v3, p0, LX/Gcc;->a:I

    iget v4, p0, LX/Gcc;->b:I

    invoke-static {v2, v3, v4}, LX/6JL;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v2

    .line 2372122
    iput-object v2, v1, LX/Gcg;->t:LX/6JR;

    .line 2372123
    iget-object v1, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v1, v1, LX/Gcg;->t:LX/6JR;

    if-nez v1, :cond_0

    .line 2372124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid preview size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2372125
    :catch_0
    :goto_0
    return-void

    .line 2372126
    :cond_0
    :try_start_1
    invoke-interface {v0}, LX/6IP;->d()Ljava/util/List;

    move-result-object v1

    iget v2, p0, LX/Gcc;->a:I

    iget v3, p0, LX/Gcc;->b:I

    invoke-static {v1, v2, v3}, LX/6JL;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v2

    .line 2372127
    if-nez v2, :cond_1

    .line 2372128
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No valid photo size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2372129
    :cond_1
    invoke-interface {v0}, LX/6IP;->e()Ljava/util/List;

    move-result-object v0

    .line 2372130
    iget v1, p0, LX/Gcc;->a:I

    iget v3, p0, LX/Gcc;->b:I

    invoke-static {v0, v1, v3}, LX/6JL;->a(Ljava/util/List;II)LX/6JR;

    move-result-object v0

    .line 2372131
    if-nez v0, :cond_2

    .line 2372132
    iget-object v0, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->t:LX/6JR;

    move-object v4, v0

    .line 2372133
    :goto_1
    iget-object v7, p0, LX/Gcc;->c:LX/Gcg;

    new-instance v0, LX/6JB;

    iget v1, v2, LX/6JR;->a:I

    iget v2, v2, LX/6JR;->b:I

    iget v3, v4, LX/6JR;->a:I

    iget v4, v4, LX/6JR;->b:I

    iget-object v5, p0, LX/Gcc;->c:LX/Gcg;

    invoke-static {v5}, LX/Gcg;->e(LX/Gcg;)I

    move-result v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/6JB;-><init>(IIIIILjava/util/List;)V

    .line 2372134
    iput-object v0, v7, LX/Gcg;->s:LX/6JB;

    .line 2372135
    sget-object v0, LX/Gcg;->b:LX/6Jt;

    iget-object v1, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v1, v1, LX/Gcg;->o:LX/6Ia;

    iget-object v2, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v2, v2, LX/Gcg;->s:LX/6JB;

    iget-object v3, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v3, v3, LX/Gcg;->t:LX/6JR;

    invoke-virtual {v0, v1, v2, v3}, LX/6Jt;->a(LX/6Ia;LX/6JB;LX/6JR;)V

    .line 2372136
    sget-object v0, LX/Gcg;->b:LX/6Jt;

    iget-object v1, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v1, v1, LX/Gcg;->E:LX/6Il;

    iget-object v2, p0, LX/Gcc;->c:LX/Gcg;

    iget-object v2, v2, LX/Gcg;->u:LX/6JC;

    invoke-virtual {v0, v1, v2}, LX/6Jt;->a(LX/6Ik;LX/6JC;)V

    .line 2372137
    iget-object v0, p0, LX/Gcc;->c:LX/Gcg;

    sget-object v1, LX/Gcg;->b:LX/6Jt;

    .line 2372138
    iget-object v2, v1, LX/6Jt;->e:LX/6Js;

    move-object v1, v2

    .line 2372139
    invoke-virtual {v0, v1}, LX/Gcg;->a(LX/6Js;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_2
    move-object v4, v0

    goto :goto_1
.end method
