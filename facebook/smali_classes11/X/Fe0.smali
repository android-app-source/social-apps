.class public final LX/Fe0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fu;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/SearchResultsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V
    .locals 0

    .prologue
    .line 2264689
    iput-object p1, p0, LX/Fe0;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final ko_()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2264690
    iget-object v0, p0, LX/Fe0;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264691
    iget-object v2, v0, LX/Cyn;->f:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2264692
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    .line 2264693
    :goto_0
    move-object v2, v2

    .line 2264694
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2264695
    iget-object v0, p0, LX/Fe0;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C:LX/Cve;

    iget-object v0, p0, LX/Fe0;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 2264696
    iget-object v4, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v4, v4

    .line 2264697
    iget-object v0, p0, LX/Fe0;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v4, v2, v0}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/util/Map;Z)Z

    .line 2264698
    :cond_0
    iget-object v0, p0, LX/Fe0;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->J:LX/CvM;

    invoke-virtual {v0}, LX/CvM;->c()V

    .line 2264699
    return v1

    :cond_1
    move v0, v1

    .line 2264700
    goto :goto_1

    .line 2264701
    :cond_2
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    .line 2264702
    iget-object v3, v0, LX/Cyn;->f:Ljava/util/Map;

    invoke-virtual {v2, v3}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2264703
    iget-object v3, v0, LX/Cyn;->f:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 2264704
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    goto :goto_0
.end method
