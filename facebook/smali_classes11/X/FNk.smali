.class public LX/FNk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0tX;

.field private final c:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/0Or;LX/0tX;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2233318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2233319
    iput-object p1, p0, LX/FNk;->a:LX/0Or;

    .line 2233320
    iput-object p2, p0, LX/FNk;->b:LX/0tX;

    .line 2233321
    iput-object p3, p0, LX/FNk;->c:Ljava/util/concurrent/ExecutorService;

    .line 2233322
    return-void
.end method
