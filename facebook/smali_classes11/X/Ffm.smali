.class public final LX/Ffm;
.super LX/3sJ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;)V
    .locals 0

    .prologue
    .line 2268890
    iput-object p1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    invoke-direct {p0}, LX/3sJ;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 9

    .prologue
    .line 2268891
    iget-object v0, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->l:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyI;

    .line 2268892
    iget-object v1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->r:LX/CyI;

    invoke-virtual {v1}, LX/CyI;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2268893
    invoke-virtual {v0}, LX/CyI;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2268894
    iget-object v1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->d:LX/2Sd;

    sget-object v4, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->k:Ljava/lang/String;

    sget-object v5, LX/7CQ;->SERP_TAB_CLICKED:LX/7CQ;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Current tab: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", Clicked Tab: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v6}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 2268895
    iget-object v1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iget-object v4, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->b:LX/CvY;

    iget-object v1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iget-object v5, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget-object v1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iget-boolean v1, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->w:Z

    if-eqz v1, :cond_1

    sget-object v1, LX/7CK;->BACK_BUTTON:LX/7CK;

    .line 2268896
    :goto_0
    sget-object v6, LX/CvJ;->CLICK:LX/CvJ;

    invoke-static {v6, v5}, LX/CvY;->b(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "action"

    const-string v8, "tab_impression"

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "destination_filter_type"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "source_filter_type"

    invoke-virtual {v6, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "action_source"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2268897
    invoke-static {v4, v5, v6}, LX/CvY;->a(LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2268898
    iget-object v1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->o:LX/Ffp;

    .line 2268899
    invoke-virtual {v1, p1}, LX/2s5;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2268900
    instance-of v3, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    if-eqz v3, :cond_0

    .line 2268901
    check-cast v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    sget-object v3, LX/8ci;->j:LX/8ci;

    .line 2268902
    invoke-virtual {v2}, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->e()Ljava/lang/Class;

    .line 2268903
    iget-object v1, v2, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2268904
    if-eqz v3, :cond_0

    .line 2268905
    iput-object v3, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    .line 2268906
    :cond_0
    iget-object v1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    const/4 v2, 0x0

    .line 2268907
    iput-boolean v2, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->w:Z

    .line 2268908
    iget-object v1, p0, LX/Ffm;->a:Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    .line 2268909
    iput-object v0, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->r:LX/CyI;

    .line 2268910
    return-void

    .line 2268911
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
