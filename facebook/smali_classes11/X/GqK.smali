.class public LX/GqK;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Gpd;",
        ">;",
        "Lcom/facebook/instantshopping/view/block/FooterBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1mR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/CIe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CIb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/CIh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Gqm;

.field public final l:Landroid/widget/FrameLayout;

.field public final m:Landroid/widget/FrameLayout;

.field public final n:LX/CtG;

.field public final o:LX/CtG;

.field public p:LX/Gpd;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 11

    .prologue
    .line 2396312
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396313
    const v0, 0x7f0d1822

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/GqK;->l:Landroid/widget/FrameLayout;

    .line 2396314
    const v0, 0x7f0d1824

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/GqK;->m:Landroid/widget/FrameLayout;

    .line 2396315
    const v0, 0x7f0d1823

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396316
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2396317
    iput-object v0, p0, LX/GqK;->n:LX/CtG;

    .line 2396318
    const v0, 0x7f0d1825

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396319
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2396320
    iput-object v0, p0, LX/GqK;->o:LX/CtG;

    .line 2396321
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/GqK;

    invoke-static {v0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v3

    check-cast v3, LX/GnF;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v5

    check-cast v5, LX/1mR;

    invoke-static {v0}, LX/CIe;->a(LX/0QB;)LX/CIe;

    move-result-object v6

    check-cast v6, LX/CIe;

    invoke-static {v0}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v7

    check-cast v7, LX/CIb;

    invoke-static {v0}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(LX/0QB;)Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    move-result-object v8

    check-cast v8, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v10

    check-cast v10, LX/Go0;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object p1

    check-cast p1, LX/0iA;

    invoke-static {v0}, LX/CIh;->b(LX/0QB;)LX/CIh;

    move-result-object v0

    check-cast v0, LX/CIh;

    iput-object v3, v2, LX/GqK;->a:LX/GnF;

    iput-object v4, v2, LX/GqK;->b:LX/Chv;

    iput-object v5, v2, LX/GqK;->c:LX/1mR;

    iput-object v6, v2, LX/GqK;->d:LX/CIe;

    iput-object v7, v2, LX/GqK;->e:LX/CIb;

    iput-object v8, v2, LX/GqK;->f:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iput-object v9, v2, LX/GqK;->g:LX/0ad;

    iput-object v10, v2, LX/GqK;->h:LX/Go0;

    iput-object p1, v2, LX/GqK;->i:LX/0iA;

    iput-object v0, v2, LX/GqK;->j:LX/CIh;

    .line 2396322
    iget-object v0, p0, LX/GqK;->i:LX/0iA;

    sget-object v1, LX/Gqm;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/Gqm;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/Gqm;

    iput-object v0, p0, LX/GqK;->k:LX/Gqm;

    .line 2396323
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 2396310
    iget-object v0, p0, LX/GqK;->o:LX/CtG;

    invoke-virtual {v0, p1}, LX/CtG;->setTextColor(I)V

    .line 2396311
    return-void
.end method

.method public final a(LX/Clf;)V
    .locals 1

    .prologue
    .line 2396305
    if-eqz p1, :cond_0

    .line 2396306
    iget-object v0, p0, LX/GqK;->o:LX/CtG;

    invoke-virtual {v0, p1}, LX/CtG;->setText(LX/Clf;)V

    .line 2396307
    :goto_0
    return-void

    .line 2396308
    :cond_0
    iget-object v0, p0, LX/GqK;->m:Landroid/widget/FrameLayout;

    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2396309
    goto :goto_0
.end method

.method public final a(LX/CnT;)V
    .locals 0

    .prologue
    .line 2396295
    check-cast p1, LX/Gpd;

    .line 2396296
    iput-object p1, p0, LX/GqK;->p:LX/Gpd;

    .line 2396297
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2396300
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 2396301
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2396302
    iget-object v0, p0, LX/GqK;->n:LX/CtG;

    invoke-virtual {v0}, LX/CtG;->a()V

    .line 2396303
    iget-object v0, p0, LX/GqK;->o:LX/CtG;

    invoke-virtual {v0}, LX/CtG;->a()V

    .line 2396304
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2396298
    iget-object v0, p0, LX/GqK;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 2396299
    return-void
.end method
