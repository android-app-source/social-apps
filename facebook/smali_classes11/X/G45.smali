.class public final LX/G45;
.super Landroid/animation/AnimatorListenerAdapter;
.source ""


# instance fields
.field public final synthetic a:LX/G3c;

.field public final synthetic b:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

.field public final synthetic c:Z

.field public final synthetic d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

.field public final synthetic e:LX/G46;


# direct methods
.method public constructor <init>(LX/G46;LX/G3c;Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;ZLcom/facebook/timeline/refresher/ProfileRefresherView;)V
    .locals 0

    .prologue
    .line 2317055
    iput-object p1, p0, LX/G45;->e:LX/G46;

    iput-object p2, p0, LX/G45;->a:LX/G3c;

    iput-object p3, p0, LX/G45;->b:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    iput-boolean p4, p0, LX/G45;->c:Z

    iput-object p5, p0, LX/G45;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 2317056
    iget-object v0, p0, LX/G45;->a:LX/G3c;

    if-eqz v0, :cond_0

    .line 2317057
    iget-object v0, p0, LX/G45;->a:LX/G3c;

    iget-object v1, p0, LX/G45;->a:LX/G3c;

    invoke-virtual {v1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    .line 2317058
    iput-object v1, v0, LX/G3c;->o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    .line 2317059
    :cond_0
    iget-object v0, p0, LX/G45;->b:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/G45;->b:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->d()Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 2317060
    iget-object v0, p0, LX/G45;->b:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->d()Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2317061
    :cond_1
    iget-object v1, p0, LX/G45;->e:LX/G46;

    iget-boolean v0, p0, LX/G45;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/G45;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2317062
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->n:Landroid/widget/FrameLayout;

    move-object v0, v2

    .line 2317063
    :goto_0
    const/high16 v2, 0x42c80000    # 100.0f

    const/4 p1, 0x0

    .line 2317064
    if-nez v0, :cond_3

    .line 2317065
    :goto_1
    return-void

    .line 2317066
    :cond_2
    iget-object v0, p0, LX/G45;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2317067
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v2

    .line 2317068
    iget-object v2, p0, LX/G45;->a:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2317069
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v2

    .line 2317070
    goto :goto_0

    .line 2317071
    :cond_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 2317072
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 2317073
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    iget-object p1, v1, LX/G46;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method
