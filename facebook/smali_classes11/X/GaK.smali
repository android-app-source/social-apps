.class public final LX/GaK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;)V
    .locals 0

    .prologue
    .line 2368142
    iput-object p1, p0, LX/GaK;->a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2368143
    sget-object v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->r:Ljava/lang/Class;

    const-string v1, "Failed to fetch configuration"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2368144
    iget-object v0, p0, LX/GaK;->a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    iget-object v0, v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->q:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Failed to fetch configuration"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2368145
    iget-object v0, p0, LX/GaK;->a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    const/4 v1, 0x0

    .line 2368146
    iput-object v1, v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2368147
    iget-object v0, p0, LX/GaK;->a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2368148
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2368149
    iget-object v0, p0, LX/GaK;->a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    const/4 v1, 0x0

    .line 2368150
    iput-object v1, v0, Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2368151
    iget-object v0, p0, LX/GaK;->a:Lcom/facebook/common/internalprefhelpers/ConfigurationRefreshUpdaterDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2368152
    return-void
.end method
