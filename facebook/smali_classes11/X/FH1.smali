.class public final LX/FH1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        "LX/FGc;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:LX/FHf;

.field public final synthetic c:Z

.field public final synthetic d:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

.field public final synthetic e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZLcom/facebook/messaging/media/photoquality/PhotoQuality;)V
    .locals 0

    .prologue
    .line 2219721
    iput-object p1, p0, LX/FH1;->e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FH1;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p3, p0, LX/FH1;->b:LX/FHf;

    iput-boolean p4, p0, LX/FH1;->c:Z

    iput-object p5, p0, LX/FH1;->d:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2219722
    check-cast p1, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219723
    if-eqz p1, :cond_0

    .line 2219724
    :goto_0
    iget-object v0, p0, LX/FH1;->e:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FH1;->b:LX/FHf;

    iget-boolean v2, p0, LX/FH1;->c:Z

    iget-object v3, p0, LX/FH1;->d:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;ZLcom/facebook/messaging/media/photoquality/PhotoQuality;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2219725
    :cond_0
    iget-object p1, p0, LX/FH1;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    goto :goto_0
.end method
