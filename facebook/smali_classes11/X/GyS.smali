.class public LX/GyS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GyR;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0yH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0yH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2410156
    iput-object p1, p0, LX/GyS;->a:Landroid/content/Context;

    .line 2410157
    iput-object p2, p0, LX/GyS;->b:LX/0yH;

    .line 2410158
    return-void
.end method

.method public static a(LX/0QB;)LX/GyS;
    .locals 3

    .prologue
    .line 2410152
    new-instance v2, LX/GyS;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v1

    check-cast v1, LX/0yH;

    invoke-direct {v2, v0, v1}, LX/GyS;-><init>(Landroid/content/Context;LX/0yH;)V

    .line 2410153
    move-object v0, v2

    .line 2410154
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2410159
    iget-object v1, p0, LX/GyS;->b:LX/0yH;

    sget-object v2, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    invoke-virtual {v1, v2}, LX/0yH;->a(LX/0yY;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2410160
    :cond_0
    :goto_0
    return v0

    .line 2410161
    :cond_1
    iget-object v1, p0, LX/GyS;->a:Landroid/content/Context;

    invoke-static {v1}, LX/1oW;->a(Landroid/content/Context;)I

    move-result v1

    .line 2410162
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/1oW;->d(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/GyS;->a:Landroid/content/Context;

    invoke-static {v1, v2, v0}, LX/1oW;->a(ILandroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2410151
    iget-object v0, p0, LX/GyS;->a:Landroid/content/Context;

    const v1, 0x7f0837a9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2410150
    iget-object v0, p0, LX/GyS;->a:Landroid/content/Context;

    const v1, 0x7f08002f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2410149
    const-string v0, "google_play_services"

    return-object v0
.end method

.method public final e()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 2410148
    iget-object v0, p0, LX/GyS;->a:Landroid/content/Context;

    invoke-static {v0}, LX/1oW;->a(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, LX/GyS;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/1oW;->a(ILandroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
