.class public final LX/GxV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/GxZ;


# direct methods
.method public constructor <init>(LX/GxZ;Ljava/util/List;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2408136
    iput-object p1, p0, LX/GxV;->d:LX/GxZ;

    iput-object p2, p0, LX/GxV;->a:Ljava/util/List;

    iput-object p3, p0, LX/GxV;->b:Landroid/content/Context;

    iput-object p4, p0, LX/GxV;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2408137
    iget-object v0, p0, LX/GxV;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2408138
    const v1, 0x7f08355f

    if-ne v0, v1, :cond_1

    .line 2408139
    :try_start_0
    iget-object v0, p0, LX/GxV;->b:Landroid/content/Context;

    iget-object v1, p0, LX/GxV;->c:Ljava/lang/String;

    .line 2408140
    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/text/ClipboardManager;

    .line 2408141
    invoke-virtual {v2, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2408142
    :cond_0
    :goto_0
    iget-object v0, p0, LX/GxV;->d:LX/GxZ;

    const/4 v1, 0x0

    iput-object v1, v0, LX/GxZ;->c:LX/2EJ;

    .line 2408143
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2408144
    return-void

    .line 2408145
    :catch_0
    move-exception v0

    .line 2408146
    iget-object v1, p0, LX/GxV;->d:LX/GxZ;

    iget-object v1, v1, LX/GxZ;->g:LX/03V;

    const-string v2, "RefreshableFacewebWebViewContainer"

    const-string v3, "copy link failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2408147
    :cond_1
    const v1, 0x7f083560

    if-ne v0, v1, :cond_0

    .line 2408148
    iget-object v0, p0, LX/GxV;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, LX/GxV;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
