.class public LX/GMo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2345340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345341
    return-void
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 2

    .prologue
    .line 2345342
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 2345343
    invoke-virtual {p0, p1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    .line 2345344
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2345327
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/GMo;->a(Landroid/view/View;LX/GJR;)V

    .line 2345328
    return-void
.end method

.method public static a(Landroid/view/View;LX/GJR;)V
    .locals 6

    .prologue
    .line 2345329
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2345330
    const/4 v2, 0x0

    .line 2345331
    :goto_0
    if-eqz v1, :cond_1

    instance-of v0, v1, Landroid/widget/ScrollView;

    if-nez v0, :cond_1

    .line 2345332
    instance-of v0, v1, Landroid/view/View;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2345333
    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    add-int/2addr v2, v0

    .line 2345334
    :cond_0
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    .line 2345335
    :cond_1
    instance-of v0, v1, Landroid/widget/ScrollView;

    if-eqz v0, :cond_2

    .line 2345336
    check-cast v1, Landroid/widget/ScrollView;

    .line 2345337
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b042e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v4, v0

    .line 2345338
    new-instance v0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;

    move-object v3, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$1;-><init>(Landroid/widget/ScrollView;ILandroid/view/View;ILX/GJR;)V

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 2345339
    :cond_2
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2345325
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/GMo;->a(Landroid/view/ViewGroup;Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 2345326
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2345318
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 2345319
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 2345320
    invoke-virtual {v0, p1}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 2345321
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 2345322
    return-void

    .line 2345323
    :cond_0
    invoke-virtual {v0, p1, v4, v5}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 2345324
    invoke-virtual {v0, p1, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    goto :goto_0
.end method

.method public static a(Landroid/view/ViewGroup;Landroid/animation/LayoutTransition$TransitionListener;)V
    .locals 1

    .prologue
    .line 2345313
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 2345314
    if-eqz p1, :cond_0

    .line 2345315
    invoke-virtual {v0, p1}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 2345316
    :cond_0
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 2345317
    return-void
.end method

.method public static b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2345311
    new-instance v0, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$2;

    invoke-direct {v0, p0}, Lcom/facebook/adinterfaces/util/AdInterfacesUiUtil$2;-><init>(Landroid/view/View;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2345312
    return-void
.end method
