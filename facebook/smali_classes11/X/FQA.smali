.class public LX/FQA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FQA;


# instance fields
.field private final a:LX/0y2;

.field private final b:LX/0Uh;

.field private final c:LX/6aG;


# direct methods
.method public constructor <init>(LX/0y2;LX/0Uh;LX/6aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2238856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2238857
    iput-object p1, p0, LX/FQA;->a:LX/0y2;

    .line 2238858
    iput-object p2, p0, LX/FQA;->b:LX/0Uh;

    .line 2238859
    iput-object p3, p0, LX/FQA;->c:LX/6aG;

    .line 2238860
    return-void
.end method

.method public static a(LX/0QB;)LX/FQA;
    .locals 6

    .prologue
    .line 2238763
    sget-object v0, LX/FQA;->d:LX/FQA;

    if-nez v0, :cond_1

    .line 2238764
    const-class v1, LX/FQA;

    monitor-enter v1

    .line 2238765
    :try_start_0
    sget-object v0, LX/FQA;->d:LX/FQA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2238766
    if-eqz v2, :cond_0

    .line 2238767
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2238768
    new-instance p0, LX/FQA;

    invoke-static {v0}, LX/0y2;->b(LX/0QB;)LX/0y2;

    move-result-object v3

    check-cast v3, LX/0y2;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/6aG;->b(LX/0QB;)LX/6aG;

    move-result-object v5

    check-cast v5, LX/6aG;

    invoke-direct {p0, v3, v4, v5}, LX/FQA;-><init>(LX/0y2;LX/0Uh;LX/6aG;)V

    .line 2238769
    move-object v0, p0

    .line 2238770
    sput-object v0, LX/FQA;->d:LX/FQA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2238771
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2238772
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2238773
    :cond_1
    sget-object v0, LX/FQA;->d:LX/FQA;

    return-object v0

    .line 2238774
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2238775
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V
    .locals 2

    .prologue
    .line 2238850
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2238851
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2238852
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->A:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 p1, 0x0

    invoke-static {v1, p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238853
    :goto_0
    return-void

    .line 2238854
    :cond_0
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->A:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2238855
    iget-object v1, p0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->A:Lcom/facebook/resources/ui/EllipsizingTextView;

    const/4 p1, 0x1

    invoke-static {v1, p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V
    .locals 10

    .prologue
    .line 2238776
    if-nez p1, :cond_1

    .line 2238777
    :cond_0
    :goto_0
    return-void

    .line 2238778
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2238779
    iput-object v0, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->L:Ljava/lang/String;

    .line 2238780
    invoke-static {p1, p2}, LX/FQA;->b(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    .line 2238781
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    if-nez v0, :cond_a

    .line 2238782
    const/4 v0, 0x0

    .line 2238783
    :goto_1
    invoke-virtual {p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->setThumbnailUrl(Ljava/lang/String;)V

    .line 2238784
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2238785
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->n()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v6

    .line 2238786
    if-nez v6, :cond_b

    .line 2238787
    const-wide/16 v4, 0x0

    move v6, v2

    .line 2238788
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->k()LX/0Px;

    move-result-object v7

    .line 2238789
    invoke-static {v7}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v8

    if-eqz v8, :cond_c

    move-object v7, v3

    .line 2238790
    :goto_3
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->w()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 2238791
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->m()LX/0Px;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/FPq;->a(LX/0Px;Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 2238792
    :goto_4
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->h()Ljava/lang/String;

    move-result-object v8

    move-object v3, p1

    invoke-virtual/range {v3 .. v9}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(DILjava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2238793
    iget-object v0, p0, LX/FQA;->c:LX/6aG;

    iget-object v1, p0, LX/FQA;->a:LX/0y2;

    invoke-virtual {v1}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v1

    const/4 v8, 0x0

    .line 2238794
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->c()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;

    move-result-object v9

    .line 2238795
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->f()Landroid/location/Location;

    move-result-object v2

    .line 2238796
    if-eqz v1, :cond_2

    if-nez v2, :cond_e

    :cond_2
    move-object v2, v8

    .line 2238797
    :goto_5
    if-nez v9, :cond_f

    .line 2238798
    :goto_6
    const-string v3, ""

    invoke-virtual {p1, v8, v3, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2238799
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2238800
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->l()LX/0Px;

    move-result-object v4

    .line 2238801
    invoke-static {v4}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2238802
    :goto_7
    invoke-virtual {p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->setPhotoUrls(Ljava/util/List;)V

    .line 2238803
    const/4 v0, 0x3

    const/4 v2, 0x0

    .line 2238804
    if-gez v0, :cond_11

    .line 2238805
    :cond_3
    :goto_8
    move-object v4, v2

    .line 2238806
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->e()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    .line 2238807
    if-eqz v0, :cond_10

    .line 2238808
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;->c()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$SocialContextModel;

    move-result-object v0

    .line 2238809
    if-eqz v0, :cond_10

    .line 2238810
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$SocialContextModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2238811
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2238812
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$SocialContextModel;->a()LX/0Px;

    move-result-object v6

    .line 2238813
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v3

    :goto_9
    if-ge v1, v7, :cond_8

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$SocialContextModel$RangesModel;

    .line 2238814
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$SocialContextModel$RangesModel;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel$SocialContextModel$RangesModel;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v0}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2238815
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 2238816
    :cond_4
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2238817
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_a
    if-ge v2, v6, :cond_7

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    .line 2238818
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;->jM_()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    .line 2238819
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;->d()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_6

    :cond_5
    const-string v0, ""

    :goto_b
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2238820
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    .line 2238821
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_b

    .line 2238822
    :cond_7
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_7

    .line 2238823
    :cond_8
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v2

    .line 2238824
    :goto_c
    invoke-virtual {p1, v4, v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    .line 2238825
    iget-object v0, p0, LX/FQA;->b:LX/0Uh;

    const/16 v1, 0x23f

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    .line 2238826
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 2238827
    sget-object v0, LX/FQ9;->BOOKMARK:LX/FQ9;

    invoke-virtual {p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(LX/FQ9;)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    .line 2238828
    iget-object v0, p2, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->L()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    move-object v0, v0

    .line 2238829
    const/4 p2, 0x1

    const/4 v3, 0x0

    .line 2238830
    iput-object v0, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238831
    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    if-eqz v1, :cond_9

    sget-object v1, LX/FQ9;->BOOKMARK:LX/FQ9;

    iget-object v2, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->H:LX/FQ9;

    invoke-virtual {v1, v2}, LX/FQ9;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2238832
    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-nez v1, :cond_12

    .line 2238833
    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    invoke-static {v1, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    .line 2238834
    :cond_9
    :goto_d
    goto/16 :goto_0

    .line 2238835
    :cond_a
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$BrowseNearbyPlacesFullImageFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2238836
    :cond_b
    invoke-virtual {v6}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;->c()D

    move-result-wide v4

    .line 2238837
    invoke-virtual {v6}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;->a()I

    move-result v6

    goto/16 :goto_2

    .line 2238838
    :cond_c
    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v7, v2

    goto/16 :goto_3

    :cond_d
    move-object v9, v3

    .line 2238839
    goto/16 :goto_4

    .line 2238840
    :cond_e
    invoke-static {v2}, Lcom/facebook/location/ImmutableLocation;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v5

    const-wide v6, 0x40f3a53340000000L    # 80467.203125

    move-object v3, v0

    move-object v4, v1

    invoke-virtual/range {v3 .. v8}, LX/6aG;->a(Lcom/facebook/location/ImmutableLocation;Lcom/facebook/location/ImmutableLocation;DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 2238841
    :cond_f
    invoke-virtual {v9}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_6

    :cond_10
    move-object v0, v1

    goto :goto_c

    .line 2238842
    :cond_11
    invoke-virtual {p2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->i()LX/0Px;

    move-result-object v4

    .line 2238843
    if-eqz v4, :cond_3

    const/4 v2, 0x0

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v4, v2, v5}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    goto/16 :goto_8

    .line 2238844
    :cond_12
    sget-object v1, LX/FQ8;->a:[I

    iget-object v2, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2238845
    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getUnsavedDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2238846
    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    invoke-static {v1, p2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_d

    .line 2238847
    :pswitch_0
    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    invoke-static {v1, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_d

    .line 2238848
    :pswitch_1
    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getSavedDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2238849
    iget-object v1, p1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    invoke-static {v1, p2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Landroid/view/View;Z)V

    goto :goto_d

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
