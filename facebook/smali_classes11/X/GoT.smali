.class public final synthetic LX/GoT;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2394502
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->values()[Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GoT;->b:[I

    :try_start_0
    sget-object v0, LX/GoT;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->LINK:Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    .line 2394503
    :goto_0
    invoke-static {}, LX/Clb;->values()[LX/Clb;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GoT;->a:[I

    :try_start_1
    sget-object v0, LX/GoT;->a:[I

    sget-object v1, LX/Clb;->BODY:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    sget-object v0, LX/GoT;->a:[I

    sget-object v1, LX/Clb;->HEADER_ONE:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v0, LX/GoT;->a:[I

    sget-object v1, LX/Clb;->HEADER_TWO:LX/Clb;

    invoke-virtual {v1}, LX/Clb;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    goto :goto_3

    :catch_1
    goto :goto_2

    :catch_2
    goto :goto_1

    :catch_3
    goto :goto_0
.end method
