.class public LX/FYD;
.super LX/FXy;
.source ""


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field public final b:LX/14x;

.field private final c:LX/B9y;

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;LX/14x;LX/B9y;LX/0Uh;)V
    .locals 0
    .param p1    # Landroid/support/v4/app/FragmentActivity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255860
    invoke-direct {p0}, LX/FXy;-><init>()V

    .line 2255861
    iput-object p1, p0, LX/FYD;->a:Landroid/support/v4/app/FragmentActivity;

    .line 2255862
    iput-object p2, p0, LX/FYD;->b:LX/14x;

    .line 2255863
    iput-object p3, p0, LX/FYD;->c:LX/B9y;

    .line 2255864
    iput-object p4, p0, LX/FYD;->d:LX/0Uh;

    .line 2255865
    return-void
.end method

.method public static a$redex0(LX/FYD;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2255878
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2255879
    iget-object v0, p0, LX/FYD;->c:LX/B9y;

    iget-object v1, p0, LX/FYD;->a:Landroid/support/v4/app/FragmentActivity;

    const-string v5, "saved"

    move-object v2, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, LX/B9y;->a(Landroid/content/Context;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 2255880
    :cond_0
    return-void
.end method

.method public static d(LX/BO1;)Z
    .locals 1

    .prologue
    .line 2255877
    invoke-interface {p0}, LX/BO1;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/BO1;->J()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2255876
    invoke-static {}, LX/10A;->a()I

    move-result v0

    return v0
.end method

.method public final a(LX/BO1;)LX/FXy;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2255881
    iget-object v1, p0, LX/FYD;->d:LX/0Uh;

    const/16 v2, 0x4a1

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2255882
    iget-object v1, p0, LX/FYD;->b:LX/14x;

    invoke-virtual {v1}, LX/14x;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/FYD;->b:LX/14x;

    invoke-virtual {v1}, LX/14x;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2255883
    if-eqz v1, :cond_1

    .line 2255884
    invoke-interface {p1}, LX/BO1;->L()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p1}, LX/BO1;->v()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, LX/FYD;->d(LX/BO1;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2255885
    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 2255886
    :cond_1
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255887
    return-object p0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2255875
    const v0, 0x7f083427

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LX/5MF;->b(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2255874
    const-string v0, "send_as_message_button"

    return-object v0
.end method

.method public final b(LX/BO1;)Z
    .locals 14

    .prologue
    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 2255866
    invoke-static {p1}, LX/FYD;->d(LX/BO1;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/BO1;->q()Ljava/lang/String;

    move-result-object v0

    .line 2255867
    :goto_0
    invoke-interface {p1}, LX/BO1;->L()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2255868
    iget-object v0, p0, LX/FYD;->c:LX/B9y;

    iget-object v1, p0, LX/FYD;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-interface {p1}, LX/BO1;->M()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/BO1;->k()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/BO1;->j()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/BO1;->l()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/BO1;->m()Ljava/lang/String;

    move-result-object v6

    const-string v10, "saved"

    move-object v8, v7

    move-object v9, v7

    invoke-virtual/range {v0 .. v11}, LX/B9y;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2255869
    :goto_1
    return v11

    :cond_0
    move-object v0, v7

    .line 2255870
    goto :goto_0

    .line 2255871
    :cond_1
    invoke-interface {p1}, LX/BO1;->v()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2255872
    iget-object v8, p0, LX/FYD;->c:LX/B9y;

    invoke-interface {p1}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, LX/FYD;->a:Landroid/support/v4/app/FragmentActivity;

    new-instance v12, LX/FYC;

    invoke-direct {v12, p0, v0}, LX/FYC;-><init>(LX/FYD;Ljava/lang/String;)V

    const-string v13, "saved"

    invoke-virtual/range {v8 .. v13}, LX/B9y;->a(Ljava/lang/String;Landroid/content/Context;ZLX/FYC;Ljava/lang/String;)V

    goto :goto_1

    .line 2255873
    :cond_2
    invoke-static {p0, v0}, LX/FYD;->a$redex0(LX/FYD;Ljava/lang/String;)V

    goto :goto_1
.end method
