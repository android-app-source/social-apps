.class public LX/GzF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/Gz6;

.field private c:LX/Gyu;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2411489
    new-instance v0, LX/GzE;

    invoke-direct {v0}, LX/GzE;-><init>()V

    .line 2411490
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    move-object v0, v0

    .line 2411491
    sput-object v0, LX/GzF;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2411492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2411493
    sget-object v0, LX/Gz6;->NATIVE_WITH_FALLBACK:LX/Gz6;

    iput-object v0, p0, LX/GzF;->b:LX/Gz6;

    .line 2411494
    sget-object v0, LX/Gyu;->FRIENDS:LX/Gyu;

    iput-object v0, p0, LX/GzF;->c:LX/Gyu;

    .line 2411495
    invoke-static {}, LX/Gsd;->a()V

    .line 2411496
    return-void
.end method
