.class public LX/GXt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GXt;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2364640
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2364641
    iput-object p1, p0, LX/GXt;->a:LX/0Zb;

    .line 2364642
    return-void
.end method

.method public static a(LX/0QB;)LX/GXt;
    .locals 4

    .prologue
    .line 2364627
    sget-object v0, LX/GXt;->b:LX/GXt;

    if-nez v0, :cond_1

    .line 2364628
    const-class v1, LX/GXt;

    monitor-enter v1

    .line 2364629
    :try_start_0
    sget-object v0, LX/GXt;->b:LX/GXt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2364630
    if-eqz v2, :cond_0

    .line 2364631
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2364632
    new-instance p0, LX/GXt;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/GXt;-><init>(LX/0Zb;)V

    .line 2364633
    move-object v0, p0

    .line 2364634
    sput-object v0, LX/GXt;->b:LX/GXt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2364635
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2364636
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2364637
    :cond_1
    sget-object v0, LX/GXt;->b:LX/GXt;

    return-object v0

    .line 2364638
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2364639
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/GXq;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2364624
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p0, LX/GXq;->value:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2364625
    sget-object v1, LX/GXr;->SELLER_PROFILE_ID:LX/GXr;

    iget-object v1, v1, LX/GXr;->value:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2364626
    return-object v0
.end method


# virtual methods
.method public final a(LX/GXq;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2364622
    iget-object v0, p0, LX/GXt;->a:LX/0Zb;

    invoke-static {p1, p2}, LX/GXt;->b(LX/GXq;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2364623
    return-void
.end method
