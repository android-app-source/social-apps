.class public final LX/H0H;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gzi;

.field public final synthetic b:LX/H0I;


# direct methods
.method public constructor <init>(LX/H0I;LX/Gzi;)V
    .locals 0

    .prologue
    .line 2413755
    iput-object p1, p0, LX/H0H;->b:LX/H0I;

    iput-object p2, p0, LX/H0H;->a:LX/Gzi;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2413756
    iget-object v0, p0, LX/H0H;->a:LX/Gzi;

    if-eqz v0, :cond_0

    .line 2413757
    iget-object v0, p0, LX/H0H;->a:LX/Gzi;

    invoke-interface {v0}, LX/Gzi;->b()V

    .line 2413758
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2413759
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2413760
    iget-object v0, p0, LX/H0H;->a:LX/Gzi;

    if-nez v0, :cond_0

    .line 2413761
    :goto_0
    return-void

    .line 2413762
    :cond_0
    if-eqz p1, :cond_1

    .line 2413763
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2413764
    if-nez v0, :cond_2

    .line 2413765
    :cond_1
    iget-object v0, p0, LX/H0H;->a:LX/Gzi;

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "result is null"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, LX/Gzi;->b()V

    goto :goto_0

    .line 2413766
    :cond_2
    iget-object v0, p0, LX/H0H;->a:LX/Gzi;

    .line 2413767
    invoke-interface {v0}, LX/Gzi;->a()V

    goto :goto_0
.end method
