.class public LX/F5T;
.super LX/F5S;
.source ""

# interfaces
.implements LX/5on;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKTreehouseManager"
.end annotation


# instance fields
.field private final a:Z

.field public final b:LX/33u;

.field private final c:LX/0Sh;

.field public final d:Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;

.field private final e:LX/F5X;

.field private final f:LX/F6E;

.field private final g:LX/F6P;

.field private final h:LX/F6R;

.field private final i:LX/F68;

.field private final j:LX/F63;

.field private final k:LX/0bH;

.field private final l:LX/1nF;

.field private final m:LX/DKO;

.field private final n:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field private final o:LX/DYa;

.field private final p:LX/DYZ;

.field private final q:LX/F2N;

.field private final r:Lcom/facebook/content/SecureContextHelper;

.field private final s:LX/0ad;

.field private t:LX/F6Q;

.field private u:LX/F67;

.field private v:LX/F62;


# direct methods
.method public constructor <init>(LX/5pY;LX/33u;LX/0Sh;Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;LX/F5X;LX/0Uh;LX/F6E;LX/F6P;LX/F6R;LX/F68;LX/F63;LX/0bH;LX/1nF;LX/DKO;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Lcom/facebook/content/SecureContextHelper;LX/F2N;LX/DYZ;LX/0ad;)V
    .locals 3
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2198699
    invoke-direct {p0, p1}, LX/F5S;-><init>(LX/5pY;)V

    .line 2198700
    invoke-virtual {p1, p0}, LX/5pX;->a(LX/5on;)V

    .line 2198701
    iput-object p2, p0, LX/F5T;->b:LX/33u;

    .line 2198702
    iput-object p3, p0, LX/F5T;->c:LX/0Sh;

    .line 2198703
    iput-object p4, p0, LX/F5T;->d:Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;

    .line 2198704
    iput-object p5, p0, LX/F5T;->e:LX/F5X;

    .line 2198705
    iput-object p7, p0, LX/F5T;->f:LX/F6E;

    .line 2198706
    iput-object p8, p0, LX/F5T;->g:LX/F6P;

    .line 2198707
    iput-object p9, p0, LX/F5T;->h:LX/F6R;

    .line 2198708
    iput-object p10, p0, LX/F5T;->i:LX/F68;

    .line 2198709
    iput-object p12, p0, LX/F5T;->k:LX/0bH;

    .line 2198710
    move-object/from16 v0, p13

    iput-object v0, p0, LX/F5T;->l:LX/1nF;

    .line 2198711
    iput-object p11, p0, LX/F5T;->j:LX/F63;

    .line 2198712
    move-object/from16 v0, p14

    iput-object v0, p0, LX/F5T;->m:LX/DKO;

    .line 2198713
    move-object/from16 v0, p15

    iput-object v0, p0, LX/F5T;->n:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 2198714
    move-object/from16 v0, p18

    iput-object v0, p0, LX/F5T;->p:LX/DYZ;

    .line 2198715
    move-object/from16 v0, p17

    iput-object v0, p0, LX/F5T;->q:LX/F2N;

    .line 2198716
    move-object/from16 v0, p16

    iput-object v0, p0, LX/F5T;->r:Lcom/facebook/content/SecureContextHelper;

    .line 2198717
    const/16 v1, 0x4ec

    const/4 v2, 0x0

    invoke-virtual {p6, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/F5T;->a:Z

    .line 2198718
    move-object/from16 v0, p19

    iput-object v0, p0, LX/F5T;->s:LX/0ad;

    .line 2198719
    new-instance v1, LX/F5O;

    invoke-direct {v1, p0}, LX/F5O;-><init>(LX/F5T;)V

    iput-object v1, p0, LX/F5T;->o:LX/DYa;

    .line 2198720
    iget-object v1, p0, LX/F5T;->p:LX/DYZ;

    iget-object v2, p0, LX/F5T;->o:LX/DYa;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2198721
    return-void
.end method

.method public static synthetic a(LX/F5T;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2198722
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/21D;)V
    .locals 3

    .prologue
    .line 2198723
    iget-object v0, p0, LX/F5T;->i:LX/F68;

    .line 2198724
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2198725
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F68;->a(LX/5pX;Landroid/app/Activity;)LX/F67;

    move-result-object v0

    iput-object v0, p0, LX/F5T;->u:LX/F67;

    .line 2198726
    iget-object v0, p0, LX/F5T;->u:LX/F67;

    invoke-virtual {v0, p1, p2}, LX/F67;->a(Ljava/lang/String;LX/21D;)V

    .line 2198727
    return-void
.end method

.method public static a$redex0(LX/F5T;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2198728
    invoke-static {}, LX/5pe;->b()V

    .line 2198729
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2198730
    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5pX;

    .line 2198731
    invoke-virtual {v0}, LX/5pX;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2198732
    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-interface {v0, p1, p2}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2198733
    :goto_0
    return-void

    .line 2198734
    :cond_0
    const-string v0, "RKTreehouseManager"

    const-string v1, "Skipping event due to catalyst instance not ready : %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static synthetic e(LX/F5T;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2198735
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic f(LX/F5T;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2198736
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic g(LX/F5T;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2198737
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/F5T;)LX/1ZF;
    .locals 2

    .prologue
    .line 2198738
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, LX/0f0;

    if-nez v0, :cond_0

    .line 2198739
    const/4 v0, 0x0

    .line 2198740
    :goto_0
    return-object v0

    .line 2198741
    :cond_0
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/0f0;

    const-class v1, LX/1ZF;

    invoke-interface {v0, v1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    goto :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    .line 2198742
    const/16 v0, 0x3ba

    if-ne p1, v0, :cond_1

    iget-object v0, p0, LX/F5T;->t:LX/F6Q;

    if-eqz v0, :cond_1

    .line 2198743
    iget-object v0, p0, LX/F5T;->t:LX/F6Q;

    .line 2198744
    iget-object v1, v0, LX/F6Q;->d:Lcom/facebook/react/bridge/Callback;

    if-nez v1, :cond_5

    .line 2198745
    sget-object v1, LX/F6Q;->a:Ljava/lang/String;

    const-string v2, "onPhotoPicked() invoked without pending callback"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198746
    :cond_0
    :goto_0
    return-void

    .line 2198747
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_2

    .line 2198748
    iget-object v0, p0, LX/F5T;->f:LX/F6E;

    .line 2198749
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2198750
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F6E;->a(LX/5pX;Landroid/app/Activity;)LX/F6D;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LX/F6D;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 2198751
    :cond_2
    const/16 v0, 0x6de

    if-ne p1, v0, :cond_3

    iget-object v0, p0, LX/F5T;->u:LX/F67;

    if-eqz v0, :cond_3

    .line 2198752
    iget-object v0, p0, LX/F5T;->u:LX/F67;

    invoke-virtual {v0, p2, p3}, LX/F67;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 2198753
    :cond_3
    const/16 v0, 0x3f2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/F5T;->v:LX/F62;

    if-eqz v0, :cond_0

    .line 2198754
    iget-object v0, p0, LX/F5T;->v:LX/F62;

    .line 2198755
    const-string v1, ""

    .line 2198756
    const/4 v2, -0x1

    if-ne p2, v2, :cond_4

    .line 2198757
    iget-object v1, v0, LX/F62;->h:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2198758
    :cond_4
    new-instance v2, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v2}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2198759
    const-string v3, "uri"

    invoke-interface {v2, v3, v1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198760
    iget-object v1, v0, LX/F62;->g:Lcom/facebook/react/bridge/Callback;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2198761
    const/4 v1, 0x0

    iput-object v1, v0, LX/F62;->g:Lcom/facebook/react/bridge/Callback;

    .line 2198762
    goto :goto_0

    .line 2198763
    :cond_5
    iget-object v1, v0, LX/F6Q;->d:Lcom/facebook/react/bridge/Callback;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2198764
    const/16 v5, 0x3ba

    invoke-static {v5, p2, p3}, LX/9FO;->a(IILandroid/content/Intent;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    .line 2198765
    new-instance v6, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v6}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2198766
    const/4 p0, -0x1

    if-ne p2, p0, :cond_7

    .line 2198767
    const-string p0, "uri"

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_6

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_6
    invoke-interface {v6, p0, v4}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198768
    :goto_1
    move-object v4, v6

    .line 2198769
    aput-object v4, v2, v3

    invoke-interface {v1, v2}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2198770
    const/4 v1, 0x0

    iput-object v1, v0, LX/F6Q;->d:Lcom/facebook/react/bridge/Callback;

    goto/16 :goto_0

    .line 2198771
    :cond_7
    const-string v5, "uri"

    invoke-interface {v6, v5, v4}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2198772
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2198773
    const-string v1, "appContext"

    const-string v2, "fb4a"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2198774
    const-string v1, "enableDiscoveryVpvLogging"

    iget-boolean v2, p0, LX/F5T;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2198775
    const-string v1, "enableFlaggedPost"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2198776
    return-object v0
.end method

.method public closeModalWindow()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198777
    iget-object v0, p0, LX/F5T;->c:LX/0Sh;

    new-instance v1, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$3;

    invoke-direct {v1, p0}, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$3;-><init>(LX/F5T;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2198778
    return-void
.end method

.method public createGroupChat(Ljava/lang/String;Ljava/lang/String;LX/5pC;I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198691
    iget-object v0, p0, LX/F5T;->e:LX/F5X;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F5X;->a(Landroid/app/Activity;)LX/F5W;

    move-result-object v0

    .line 2198692
    invoke-static {v0}, LX/F5W;->a(LX/F5W;)Landroid/content/Intent;

    move-result-object v1

    .line 2198693
    const-string p0, "target_fragment"

    sget-object p2, LX/0cQ;->GROUP_CREATE_SIDE_CONVERSATION_FRAGMENT:LX/0cQ;

    invoke-virtual {p2}, LX/0cQ;->ordinal()I

    move-result p2

    invoke-virtual {v1, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2198694
    const-string p0, "group_feed_id"

    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2198695
    iget-object p0, v0, LX/F5W;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object p2, v0, LX/F5W;->a:Landroid/app/Activity;

    invoke-interface {p0, v1, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2198696
    return-void
.end method

.method public createHomescreenShortcut(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198779
    const-string v0, "RKTreehouseManager"

    const-string v1, "createHomescreenShortcut() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198780
    return-void
.end method

.method public deleteGYSCFeedItem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198781
    iget-object v0, p0, LX/F5T;->n:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198782
    return-void
.end method

.method public didNavigate(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198783
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198784
    return-void
.end method

.method public didRemoveMember(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198811
    const-string v0, "RKTreehouseManager"

    const-string v1, "didRemoveMember() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198812
    return-void
.end method

.method public editPost(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198836
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    invoke-direct {p0, p1, v0}, LX/F5T;->a(Ljava/lang/String;LX/21D;)V

    .line 2198837
    return-void
.end method

.method public emitNavigationEvent(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198834
    const-string v0, "RKTreehouseManager"

    const-string v1, "emitNavigationEvent() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198835
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2198831
    invoke-super {p0}, LX/F5S;->f()V

    .line 2198832
    iget-object v0, p0, LX/F5T;->p:LX/DYZ;

    iget-object v1, p0, LX/F5T;->o:LX/DYa;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2198833
    return-void
.end method

.method public groupCommerceGetDisplayPrice(LX/5pG;Lcom/facebook/react/bridge/Callback;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198829
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupCommerceGetDisplayPrice() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198830
    return-void
.end method

.method public groupHeaderInfoDidUpdate(Ljava/lang/String;I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198827
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198828
    return-void
.end method

.method public leaveGroup(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198616
    const-string v0, "RKTreehouseManager"

    const-string v1, "leaveGroup() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198617
    return-void
.end method

.method public logEventEnded(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198825
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198826
    return-void
.end method

.method public logEventWithDuration(Ljava/lang/String;I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198823
    const-string v0, "RKTreehouseManager"

    const-string v1, "logEventWithDuration() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198824
    return-void
.end method

.method public logQEExposureLogging(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198821
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198822
    return-void
.end method

.method public navigateToGeneralReactFragment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198815
    iget-object v0, p0, LX/F5T;->l:LX/1nF;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    .line 2198816
    invoke-static {v0, p1}, LX/1nF;->a(LX/1nF;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2198817
    iget-object v2, v0, LX/1nF;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2198818
    const/4 p0, 0x0

    invoke-virtual {v0, p2, p1, p0}, LX/1nF;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object p0

    move-object p0, p0

    .line 2198819
    invoke-interface {v2, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2198820
    :cond_0
    return-void
.end method

.method public onModalClose()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198813
    const-string v0, "RKTreehouseManager"

    const-string v1, "onModalClose() not supported"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198814
    return-void
.end method

.method public openAddCoverPhoto(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198785
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198786
    return-void
.end method

.method public openAddMember(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198803
    iget-object v0, p0, LX/F5T;->e:LX/F5X;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F5X;->a(Landroid/app/Activity;)LX/F5W;

    move-result-object v0

    .line 2198804
    invoke-static {v0}, LX/F5W;->a(LX/F5W;)Landroid/content/Intent;

    move-result-object v1

    .line 2198805
    const-string v2, "group_feed_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2198806
    const-string v2, "target_fragment"

    sget-object p0, LX/0cQ;->GROUP_MEMBER_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {p0}, LX/0cQ;->ordinal()I

    move-result p0

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2198807
    const-string v2, "group_visibility"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2198808
    const-string v2, "group_url"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2198809
    iget-object v2, v0, LX/F5W;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object p0, v0, LX/F5W;->a:Landroid/app/Activity;

    invoke-interface {v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2198810
    return-void
.end method

.method public openAllMembers(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198801
    iget-object v0, p0, LX/F5T;->e:LX/F5X;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F5X;->a(Landroid/app/Activity;)LX/F5W;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/F5W;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 2198802
    return-void
.end method

.method public openCamera(Lcom/facebook/react/bridge/Callback;)V
    .locals 8
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198792
    iget-object v0, p0, LX/F5T;->j:LX/F63;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    .line 2198793
    new-instance v2, LX/F62;

    invoke-static {v0}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v4

    check-cast v4, LX/2Ib;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    const-class v3, LX/0i4;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/0i4;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    move-object v3, v1

    invoke-direct/range {v2 .. v7}, LX/F62;-><init>(Landroid/app/Activity;LX/2Ib;Lcom/facebook/content/SecureContextHelper;LX/0i4;LX/0kL;)V

    .line 2198794
    move-object v0, v2

    .line 2198795
    iput-object v0, p0, LX/F5T;->v:LX/F62;

    .line 2198796
    iget-object v0, p0, LX/F5T;->v:LX/F62;

    .line 2198797
    iget-object v1, v0, LX/F62;->c:LX/2Ib;

    invoke-virtual {v1}, LX/2Ib;->c()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, LX/F62;->h:Landroid/net/Uri;

    .line 2198798
    iput-object p1, v0, LX/F62;->g:Lcom/facebook/react/bridge/Callback;

    .line 2198799
    iget-object v1, v0, LX/F62;->d:LX/0i5;

    sget-object v2, LX/F62;->a:[Ljava/lang/String;

    new-instance p1, LX/F61;

    invoke-direct {p1, v0}, LX/F61;-><init>(LX/F62;)V

    invoke-virtual {v1, v2, p1}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2198800
    return-void
.end method

.method public openComposer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198787
    iget-object v0, p0, LX/F5T;->f:LX/F6E;

    .line 2198788
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2198789
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F6E;->a(LX/5pX;Landroid/app/Activity;)LX/F6D;

    move-result-object v0

    .line 2198790
    iget-object v1, v0, LX/F6D;->c:LX/1Kf;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p1, p2, v3}, LX/F6D;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    const/16 p0, 0x6dc

    iget-object p3, v0, LX/F6D;->b:Landroid/app/Activity;

    invoke-interface {v1, v2, v3, p0, p3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2198791
    return-void
.end method

.method public openDiscoverTab()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198697
    const-string v0, "RKTreehouseManager"

    const-string v1, "openDiscoverTab() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198698
    return-void
.end method

.method public openEditGroupDescription(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198580
    iget-object v0, p0, LX/F5T;->q:LX/F2N;

    invoke-virtual {v0}, LX/F2N;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2198581
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2198582
    iget-object v1, p0, LX/F5T;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2198583
    return-void
.end method

.method public openEvent(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198642
    iget-object v0, p0, LX/F5T;->e:LX/F5X;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F5X;->a(Landroid/app/Activity;)LX/F5W;

    move-result-object v0

    .line 2198643
    invoke-static {p1}, LX/5QR;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2198644
    iget-object v2, v0, LX/F5W;->e:LX/17Y;

    iget-object p0, v0, LX/F5W;->a:Landroid/app/Activity;

    invoke-interface {v2, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2198645
    iget-object v2, v0, LX/F5W;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object p0, v0, LX/F5W;->a:Landroid/app/Activity;

    invoke-interface {v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2198646
    return-void
.end method

.method public openGroupCreateFlow()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198640
    const-string v0, "RKTreehouseManager"

    const-string v1, "openGroupCreateFlow() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198641
    return-void
.end method

.method public openGroupProfile(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198638
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198639
    return-void
.end method

.method public openGroupSearch(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198636
    iget-object v0, p0, LX/F5T;->c:LX/0Sh;

    new-instance v1, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$2;-><init>(LX/F5T;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2198637
    return-void
.end method

.method public openGroupsEdit()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198634
    const-string v0, "RKTreehouseManager"

    const-string v1, "openGroupEditPage() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198635
    return-void
.end method

.method public openMessengerComposer(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198631
    iget-object v0, p0, LX/F5T;->e:LX/F5X;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F5X;->a(Landroid/app/Activity;)LX/F5W;

    move-result-object v0

    .line 2198632
    iget-object v1, v0, LX/F5W;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, v0, LX/F5W;->a:Landroid/app/Activity;

    iget-object v3, v0, LX/F5W;->b:Landroid/content/res/Resources;

    iget-object p0, v0, LX/F5W;->a:Landroid/app/Activity;

    invoke-static {p1, v1, v2, v3, p0}, LX/DTo;->a(Ljava/lang/String;Lcom/facebook/content/SecureContextHelper;Landroid/app/Activity;Landroid/content/res/Resources;Landroid/content/Context;)Z

    .line 2198633
    return-void
.end method

.method public openNotificationSettingsPage(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198629
    const-string v0, "RKTreehouseManager"

    const-string v1, "openNotificationSettingsPage() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198630
    return-void
.end method

.method public openPage(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198627
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198628
    return-void
.end method

.method public openPhotoComposer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198623
    iget-object v0, p0, LX/F5T;->f:LX/F6E;

    .line 2198624
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2198625
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F6E;->a(LX/5pX;Landroid/app/Activity;)LX/F6D;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LX/F6D;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198626
    return-void
.end method

.method public openPhotoGallery(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198618
    iget-object v0, p0, LX/F5T;->g:LX/F6P;

    .line 2198619
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2198620
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F6P;->a(LX/5pX;Landroid/app/Activity;)LX/F6O;

    move-result-object v0

    .line 2198621
    iget-object v1, v0, LX/F6O;->c:LX/F6J;

    sget-object v2, LX/5Go;->GRAPHQL_PHOTO_CREATION_STORY:LX/5Go;

    new-instance p0, LX/F6N;

    invoke-direct {p0, v0}, LX/F6N;-><init>(LX/F6O;)V

    invoke-virtual {v1, p1, v2, p0}, LX/F6J;->a(Ljava/lang/String;LX/5Go;LX/0TF;)V

    .line 2198622
    return-void
.end method

.method public openPhotoGalleryWithMultiplePhoto(LX/5pC;I)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198602
    iget-object v0, p0, LX/F5T;->g:LX/F6P;

    .line 2198603
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2198604
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F6P;->a(LX/5pX;Landroid/app/Activity;)LX/F6O;

    move-result-object v0

    .line 2198605
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2198606
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2198607
    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2198608
    invoke-interface {p1, v1}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2198609
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2198610
    :cond_0
    new-instance v1, LX/9hD;

    const-class v3, LX/23W;

    new-instance p0, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/photos/mediafetcher/query/param/MultiIdQueryParam;-><init>(LX/0Px;)V

    invoke-static {v3, p0}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v2

    invoke-direct {v1, v2}, LX/9hD;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    .line 2198611
    if-ltz p2, :cond_1

    invoke-interface {p1}, LX/5pC;->size()I

    move-result v2

    if-ge p2, v2, :cond_1

    .line 2198612
    invoke-interface {p1, p2}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    .line 2198613
    :cond_1
    sget-object v2, LX/74S;->REACTION_FEED_STORY_PHOTO_ALBUM:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    .line 2198614
    iget-object v2, v0, LX/F6O;->a:Landroid/app/Activity;

    new-instance v3, Lcom/facebook/groups/react/PhotoGalleryLauncher$2;

    invoke-direct {v3, v0, v1}, Lcom/facebook/groups/react/PhotoGalleryLauncher$2;-><init>(LX/F6O;LX/9hD;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2198615
    return-void
.end method

.method public openPhotoPicker(Lcom/facebook/react/bridge/Callback;)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198592
    iget-object v0, p0, LX/F5T;->h:LX/F6R;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    .line 2198593
    new-instance v3, LX/F6Q;

    invoke-static {v0}, LX/9FO;->a(LX/0QB;)LX/9FO;

    move-result-object v2

    check-cast v2, LX/9FO;

    invoke-direct {v3, v1, v2}, LX/F6Q;-><init>(Landroid/app/Activity;LX/9FO;)V

    .line 2198594
    move-object v0, v3

    .line 2198595
    iput-object v0, p0, LX/F5T;->t:LX/F6Q;

    .line 2198596
    iget-object v0, p0, LX/F5T;->t:LX/F6Q;

    .line 2198597
    iget-object v1, v0, LX/F6Q;->c:LX/9FO;

    iget-object v2, v0, LX/F6Q;->b:Landroid/app/Activity;

    const/4 v3, 0x0

    .line 2198598
    invoke-static {v2, v3, v3}, LX/8si;->a(Landroid/content/Context;ZZ)Landroid/content/Intent;

    move-result-object v3

    .line 2198599
    iget-object v4, v1, LX/9FO;->a:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x3ba

    invoke-interface {v4, v3, p0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2198600
    iput-object p1, v0, LX/F6Q;->d:Lcom/facebook/react/bridge/Callback;

    .line 2198601
    return-void
.end method

.method public openPollComposer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198584
    iget-object v0, p0, LX/F5T;->f:LX/F6E;

    .line 2198585
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2198586
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/F6E;->a(LX/5pX;Landroid/app/Activity;)LX/F6D;

    move-result-object v0

    .line 2198587
    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, LX/F6D;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 2198588
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2198589
    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    const-string v2, "GroupsPollComposerPluginConfig"

    invoke-static {v2}, LX/89L;->a(Ljava/lang/String;)LX/89L;

    move-result-object v2

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2198590
    iget-object v2, v0, LX/F6D;->c:LX/1Kf;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    const/16 v4, 0x6dc

    iget-object p0, v0, LX/F6D;->b:Landroid/app/Activity;

    invoke-interface {v2, v3, v1, v4, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2198591
    return-void
.end method

.method public openStoryPermalink(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198578
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198579
    return-void
.end method

.method public openURI(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198669
    const-string v0, "RKTreehouseManager"

    const-string v1, "openURI() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198670
    return-void
.end method

.method public openURL(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198688
    iget-object v0, p0, LX/F5T;->e:LX/F5X;

    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F5X;->a(Landroid/app/Activity;)LX/F5W;

    move-result-object v0

    .line 2198689
    iget-object v1, v0, LX/F5W;->g:LX/17W;

    iget-object p0, v0, LX/F5W;->a:Landroid/app/Activity;

    invoke-virtual {v1, p0, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2198690
    return-void
.end method

.method public openVideo(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198686
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198687
    return-void
.end method

.method public postGroupCreatedEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198684
    iget-object v0, p0, LX/F5T;->m:LX/DKO;

    new-instance v1, LX/DKN;

    invoke-direct {v1, p1, p2, p3}, LX/DKN;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2198685
    return-void
.end method

.method public reloadGroups()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198682
    const-string v0, "RKTreehouseManager"

    const-string v1, "reloadGroups() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198683
    return-void
.end method

.method public reportStoryURL(LX/5pG;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2198677
    sget-object v0, LX/0ax;->du:Ljava/lang/String;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "hideableToken"

    invoke-interface {p1, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "storyGraphQLID"

    invoke-interface {p1, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string v3, "actionType"

    invoke-interface {p1, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "storyRenderLocation"

    invoke-interface {p1, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "trackingCodes"

    invoke-interface {p1, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2198678
    if-eqz v0, :cond_0

    .line 2198679
    new-array v1, v5, [Ljava/lang/Object;

    aput-object v0, v1, v4

    invoke-interface {p2, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2198680
    :goto_0
    return-void

    .line 2198681
    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    invoke-interface {p3, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setPagerSwipingEnabled(Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198675
    const-string v0, "RKTreehouseManager"

    const-string v1, "setPagerSwipingEnabled() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198676
    return-void
.end method

.method public setTitleBarConfig(LX/5pG;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198671
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2198672
    const-string v0, "RKTreehouseManager"

    const-string v1, "Cannot set titlebar. Current activity is null"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198673
    :goto_0
    return-void

    .line 2198674
    :cond_0
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;

    invoke-direct {v1, p0, p1}, Lcom/facebook/groups/fb4a/react/FB4AGroupsManagerJavaModule$4;-><init>(LX/F5T;LX/5pG;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public switchFeed(Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198647
    const-string v0, "RKTreehouseManager"

    const-string v1, "switchFeed() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198648
    return-void
.end method

.method public tappedActorProfile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198667
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198668
    return-void
.end method

.method public tappedActorProfileInGroup(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198665
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198666
    return-void
.end method

.method public tappedPageProfile(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198663
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198664
    return-void
.end method

.method public updateGroupInfoProperty(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198657
    const-string v0, "pending"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2198658
    iget-object v0, p0, LX/F5T;->k:LX/0bH;

    new-instance v1, LX/DNu;

    invoke-direct {v1, p3}, LX/DNu;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2198659
    :goto_0
    return-void

    .line 2198660
    :cond_0
    const-string v0, "reported"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2198661
    iget-object v0, p0, LX/F5T;->k:LX/0bH;

    new-instance v1, LX/DNv;

    invoke-direct {v1, p3}, LX/DNv;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 2198662
    :cond_1
    new-instance v0, LX/5pA;

    const-string v1, "Attempted to update unsupported group info property"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public updateInboxTabs(IIII)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198655
    const-string v0, "RKTreehouseManager"

    const-string v1, "updateInboxTabs() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198656
    return-void
.end method

.method public viewerDidComment(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198653
    const-string v0, "RKTreehouseManager"

    const-string v1, "viewerDidComment() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198654
    return-void
.end method

.method public viewerDidLike(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198651
    const-string v0, "RKTreehouseManager"

    const-string v1, "viewerDidLike() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198652
    return-void
.end method

.method public willNavigate(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2198649
    const-string v0, "RKTreehouseManager"

    const-string v1, "groupHeaderInfoDidUpdate() not supported yet"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198650
    return-void
.end method
