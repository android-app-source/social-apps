.class public final LX/H1A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V
    .locals 0

    .prologue
    .line 2414855
    iput-object p1, p0, LX/H1A;->a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 2414856
    if-eqz p2, :cond_0

    .line 2414857
    new-instance v0, Lcom/facebook/mobileconfig/ui/SearchFragment;

    invoke-direct {v0}, Lcom/facebook/mobileconfig/ui/SearchFragment;-><init>()V

    .line 2414858
    check-cast p1, Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {p1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 2414859
    iput-object v1, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;->h:Ljava/lang/CharSequence;

    .line 2414860
    iget-object v1, p0, LX/H1A;->a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    iget-object v1, v1, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->v:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d1c0e

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const-string v1, "mobileconfig_prefs"

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2414861
    :cond_0
    return-void
.end method
