.class public LX/G8a;
.super LX/2wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2wH",
        "<",
        "LX/G8d;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;)V
    .locals 7

    const/16 v3, 0x4a

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.auth.api.accountactivationstate.internal.IAccountActivationStateService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p0, v0, LX/G8d;

    if-eqz p0, :cond_1

    check-cast v0, LX/G8d;

    goto :goto_0

    :cond_1
    new-instance v0, LX/G8e;

    invoke-direct {v0, p1}, LX/G8e;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const-string v0, "com.google.android.gms.auth.api.accountactivationstate.START"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    const-string v0, "com.google.android.gms.auth.api.accountactivationstate.internal.IAccountActivationStateService"

    return-object v0
.end method
