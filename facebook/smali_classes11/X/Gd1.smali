.class public LX/Gd1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2372947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/View;J)V
    .locals 12

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 2372948
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2372949
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2372950
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 2372951
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2372952
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2372953
    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2372954
    invoke-virtual {p1, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 2372955
    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2372956
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    .line 2372957
    iget v4, v2, Landroid/graphics/Rect;->top:I

    sub-int v3, v4, v3

    .line 2372958
    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v4

    .line 2372959
    iget v4, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v1

    .line 2372960
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    add-int/2addr v5, v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v3

    invoke-direct {v1, v4, v3, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2372961
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 2372962
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v0, v3

    .line 2372963
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    .line 2372964
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    .line 2372965
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v0, v4

    div-float/2addr v0, v7

    .line 2372966
    iget v4, v1, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    sub-float/2addr v4, v0

    float-to-int v4, v4

    iput v4, v1, Landroid/graphics/Rect;->left:I

    .line 2372967
    iget v4, v1, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 2372968
    :goto_0
    invoke-virtual {p1, v6}, Landroid/view/View;->setPivotX(F)V

    .line 2372969
    invoke-virtual {p1, v6}, Landroid/view/View;->setPivotY(F)V

    move-object v0, p1

    move-wide v4, p2

    .line 2372970
    const/high16 p2, 0x3f800000    # 1.0f

    const/4 p1, 0x2

    const/4 p0, 0x1

    const/4 v11, 0x0

    .line 2372971
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2372972
    const-string v7, "x"

    new-array v8, p1, [F

    iget v9, v1, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    aput v9, v8, v11

    iget v9, v2, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    aput v9, v8, p0

    invoke-static {v0, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v7

    const-string v8, "y"

    new-array v9, p1, [F

    iget v10, v1, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    aput v10, v9, v11

    iget v10, v2, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    aput v10, v9, p0

    invoke-static {v0, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v7

    const-string v8, "scaleX"

    new-array v9, p1, [F

    aput v3, v9, v11

    aput p2, v9, p0

    invoke-static {v0, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v7

    const-string v8, "scaleY"

    new-array v9, p1, [F

    aput v3, v9, v11

    aput p2, v9, p0

    invoke-static {v0, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 2372973
    invoke-virtual {v6, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2372974
    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v6, v7}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2372975
    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    .line 2372976
    return-void

    .line 2372977
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v0, v3

    .line 2372978
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    .line 2372979
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v0, v4

    div-float/2addr v0, v7

    .line 2372980
    iget v4, v1, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    sub-float/2addr v4, v0

    float-to-int v4, v4

    iput v4, v1, Landroid/graphics/Rect;->top:I

    .line 2372981
    iget v4, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_0
.end method
