.class public final LX/G0I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2308743
    iput-object p1, p0, LX/G0I;->b:Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;

    iput-object p2, p0, LX/G0I;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3e415636

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2308744
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/G0I;->a:Landroid/content/Context;

    const-class v3, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2308745
    const-string v2, "id"

    iget-object v3, p0, LX/G0I;->b:Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;

    iget-object v3, v3, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2308746
    iget-object v2, p0, LX/G0I;->b:Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;

    iget-object v2, v2, Lcom/facebook/timeline/legacycontact/RememberMemorializedActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/G0I;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2308747
    const v1, -0x20904cbd

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
