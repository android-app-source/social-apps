.class public final LX/HCp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 2440012
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2440013
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2440014
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2440015
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2440016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 2440017
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440018
    :goto_1
    move v1, v2

    .line 2440019
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2440020
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 2440021
    :cond_1
    const-string v9, "is_current"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2440022
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    .line 2440023
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 2440024
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2440025
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2440026
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_2

    if-eqz v8, :cond_2

    .line 2440027
    const-string v9, "image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2440028
    invoke-static {p0, p1}, LX/HCo;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_2

    .line 2440029
    :cond_3
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2440030
    const/4 v8, 0x0

    .line 2440031
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_c

    .line 2440032
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440033
    :goto_3
    move v5, v8

    .line 2440034
    goto :goto_2

    .line 2440035
    :cond_4
    const-string v9, "template_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2440036
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_2

    .line 2440037
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 2440038
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2440039
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 2440040
    if-eqz v1, :cond_7

    .line 2440041
    invoke-virtual {p1, v3, v6}, LX/186;->a(IZ)V

    .line 2440042
    :cond_7
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2440043
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2440044
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_2

    .line 2440045
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440046
    :cond_a
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 2440047
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2440048
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2440049
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_a

    if-eqz v9, :cond_a

    .line 2440050
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 2440051
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 2440052
    :cond_b
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2440053
    invoke-virtual {p1, v8, v5}, LX/186;->b(II)V

    .line 2440054
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_3

    :cond_c
    move v5, v8

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2440079
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2440080
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2440081
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/HCp;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2440082
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2440083
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2440084
    return-void
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 2440055
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2440056
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440057
    if-eqz v0, :cond_0

    .line 2440058
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440059
    invoke-static {p0, v0, p2}, LX/HCo;->a(LX/15i;ILX/0nX;)V

    .line 2440060
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2440061
    if-eqz v0, :cond_1

    .line 2440062
    const-string v1, "is_current"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440063
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2440064
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440065
    if-eqz v0, :cond_3

    .line 2440066
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440067
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2440068
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2440069
    if-eqz v1, :cond_2

    .line 2440070
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440071
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2440072
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2440073
    :cond_3
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2440074
    if-eqz v0, :cond_4

    .line 2440075
    const-string v0, "template_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440076
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2440077
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2440078
    return-void
.end method
