.class public LX/FA3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/FA3;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile j:LX/FA3;


# instance fields
.field private final b:LX/F9x;

.field private final c:LX/0Xl;

.field private final d:Ljava/lang/String;

.field private final e:LX/0Yb;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2206652
    const-class v0, LX/FA3;

    sput-object v0, LX/FA3;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Xl;LX/F9x;)V
    .locals 2
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/GlobalFbBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206654
    const-string v0, "127.0.0.1"

    iput-object v0, p0, LX/FA3;->f:Ljava/lang/String;

    .line 2206655
    const/16 v0, 0x1fb6

    iput v0, p0, LX/FA3;->g:I

    .line 2206656
    const-string v0, "127.0.0.1"

    iput-object v0, p0, LX/FA3;->h:Ljava/lang/String;

    .line 2206657
    const/16 v0, 0x235a

    iput v0, p0, LX/FA3;->i:I

    .line 2206658
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FA3;->d:Ljava/lang/String;

    .line 2206659
    iput-object p3, p0, LX/FA3;->b:LX/F9x;

    .line 2206660
    iput-object p2, p0, LX/FA3;->c:LX/0Xl;

    .line 2206661
    iget-object v0, p0, LX/FA3;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "org.torproject.android.intent.action.STATUS"

    invoke-interface {v0, v1, p0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, LX/FA3;->e:LX/0Yb;

    .line 2206662
    return-void
.end method

.method public static a(LX/0QB;)LX/FA3;
    .locals 6

    .prologue
    .line 2206639
    sget-object v0, LX/FA3;->j:LX/FA3;

    if-nez v0, :cond_1

    .line 2206640
    const-class v1, LX/FA3;

    monitor-enter v1

    .line 2206641
    :try_start_0
    sget-object v0, LX/FA3;->j:LX/FA3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2206642
    if-eqz v2, :cond_0

    .line 2206643
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2206644
    new-instance p0, LX/FA3;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0aW;->a(LX/0QB;)LX/0aW;

    move-result-object v4

    check-cast v4, LX/0Xl;

    invoke-static {v0}, LX/F9x;->a(LX/0QB;)LX/F9x;

    move-result-object v5

    check-cast v5, LX/F9x;

    invoke-direct {p0, v3, v4, v5}, LX/FA3;-><init>(Landroid/content/Context;LX/0Xl;LX/F9x;)V

    .line 2206645
    move-object v0, p0

    .line 2206646
    sput-object v0, LX/FA3;->j:LX/FA3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206647
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2206648
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2206649
    :cond_1
    sget-object v0, LX/FA3;->j:LX/FA3;

    return-object v0

    .line 2206650
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2206651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2206631
    iget-object v0, p0, LX/FA3;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2206632
    iget-object v0, p0, LX/FA3;->b:LX/F9x;

    .line 2206633
    sget-object v1, LX/4ce;->REQUESTED:LX/4ce;

    invoke-static {v0, v1}, LX/F9x;->a(LX/F9x;LX/4ce;)V

    .line 2206634
    new-instance v0, Landroid/content/Intent;

    const-string v1, "org.torproject.android.intent.action.START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2206635
    const-string v1, "org.torproject.android"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2206636
    const-string v1, "org.torproject.android.intent.extra.PACKAGE_NAME"

    iget-object v2, p0, LX/FA3;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2206637
    iget-object v1, p0, LX/FA3;->c:LX/0Xl;

    invoke-interface {v1, v0}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2206638
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2206602
    iget-object v0, p0, LX/FA3;->e:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2206603
    iget-object v0, p0, LX/FA3;->b:LX/F9x;

    invoke-virtual {v0}, LX/F9x;->j()V

    .line 2206604
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 12

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    const/16 v0, 0x26

    const v2, -0x198320f6

    invoke-static {v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2206605
    const-string v0, "org.torproject.android.intent.extra.STATUS"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2206606
    const-string v2, "org.torproject.android.intent.extra.HTTP_PROXY_HOST"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2206607
    const-string v8, "org.torproject.android.intent.extra.HTTP_PROXY_PORT"

    const/16 v9, 0x1fb6

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 2206608
    const-string v9, "org.torproject.android.intent.extra.SOCKS_PROXY_HOST"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2206609
    const-string v10, "org.torproject.android.intent.extra.SOCKS_PROXY_PORT"

    const/16 v11, 0x235a

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 2206610
    if-eqz v2, :cond_0

    .line 2206611
    iput-object v2, p0, LX/FA3;->f:Ljava/lang/String;

    .line 2206612
    iput v8, p0, LX/FA3;->g:I

    .line 2206613
    :cond_0
    if-eqz v9, :cond_1

    .line 2206614
    iput-object v9, p0, LX/FA3;->h:Ljava/lang/String;

    .line 2206615
    iput v10, p0, LX/FA3;->i:I

    .line 2206616
    :cond_1
    const/4 v11, 0x5

    new-array v11, v11, [Ljava/lang/Object;

    aput-object v0, v11, v1

    aput-object v2, v11, v3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v11, v4

    aput-object v9, v11, v5

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v11, v6

    .line 2206617
    if-nez v0, :cond_2

    .line 2206618
    const-string v0, "OFF"

    .line 2206619
    :cond_2
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_3
    move v0, v2

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2206620
    :goto_1
    const v0, 0x5e03c95f

    invoke-static {v0, v7}, LX/02F;->e(II)V

    return-void

    .line 2206621
    :sswitch_0
    const-string v3, "OFF"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v1, "STOPPING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    goto :goto_0

    :sswitch_2
    const-string v1, "STARTING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v4

    goto :goto_0

    :sswitch_3
    const-string v1, "ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v5

    goto :goto_0

    :sswitch_4
    const-string v1, "STARTS_DISABLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v6

    goto :goto_0

    .line 2206622
    :pswitch_0
    iget-object v0, p0, LX/FA3;->b:LX/F9x;

    .line 2206623
    sget-object v1, LX/4cd;->PROXY_ERROR:LX/4cd;

    invoke-static {v0, v1}, LX/F9x;->a$redex0(LX/F9x;LX/4cd;)V

    .line 2206624
    goto :goto_1

    .line 2206625
    :pswitch_1
    iget-object v0, p0, LX/FA3;->b:LX/F9x;

    invoke-virtual {v0}, LX/F9x;->k()V

    goto :goto_1

    .line 2206626
    :pswitch_2
    iget-object v0, p0, LX/FA3;->b:LX/F9x;

    invoke-virtual {v0}, LX/F9x;->i()V

    goto :goto_1

    .line 2206627
    :pswitch_3
    iget-object v0, p0, LX/FA3;->b:LX/F9x;

    iget-object v1, p0, LX/FA3;->f:Ljava/lang/String;

    iget v2, p0, LX/FA3;->g:I

    iget-object v3, p0, LX/FA3;->h:Ljava/lang/String;

    iget v4, p0, LX/FA3;->i:I

    invoke-virtual {v0, v1, v2, v3, v4}, LX/F9x;->a(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_1

    .line 2206628
    :pswitch_4
    iget-object v0, p0, LX/FA3;->b:LX/F9x;

    .line 2206629
    sget-object v1, LX/4ce;->REQUESTED:LX/4ce;

    invoke-static {v0, v1}, LX/F9x;->a(LX/F9x;LX/4ce;)V

    .line 2206630
    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x6b17578c -> :sswitch_1
        -0x67df1556 -> :sswitch_4
        0x9df -> :sswitch_3
        0x1314f -> :sswitch_0
        0x7d22d040 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
