.class public final enum LX/GBB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GBB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GBB;

.field public static final enum CANDIDATE:LX/GBB;

.field public static final enum HEADER:LX/GBB;

.field public static final enum NOT_IN_LIST:LX/GBB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2326617
    new-instance v0, LX/GBB;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/GBB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GBB;->HEADER:LX/GBB;

    .line 2326618
    new-instance v0, LX/GBB;

    const-string v1, "CANDIDATE"

    invoke-direct {v0, v1, v3}, LX/GBB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GBB;->CANDIDATE:LX/GBB;

    .line 2326619
    new-instance v0, LX/GBB;

    const-string v1, "NOT_IN_LIST"

    invoke-direct {v0, v1, v4}, LX/GBB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GBB;->NOT_IN_LIST:LX/GBB;

    .line 2326620
    const/4 v0, 0x3

    new-array v0, v0, [LX/GBB;

    sget-object v1, LX/GBB;->HEADER:LX/GBB;

    aput-object v1, v0, v2

    sget-object v1, LX/GBB;->CANDIDATE:LX/GBB;

    aput-object v1, v0, v3

    sget-object v1, LX/GBB;->NOT_IN_LIST:LX/GBB;

    aput-object v1, v0, v4

    sput-object v0, LX/GBB;->$VALUES:[LX/GBB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2326616
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GBB;
    .locals 1

    .prologue
    .line 2326614
    const-class v0, LX/GBB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GBB;

    return-object v0
.end method

.method public static values()[LX/GBB;
    .locals 1

    .prologue
    .line 2326615
    sget-object v0, LX/GBB;->$VALUES:[LX/GBB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GBB;

    return-object v0
.end method
