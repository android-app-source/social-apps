.class public LX/FKp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225573
    return-void
.end method

.method public static a(Ljava/util/List;)LX/0lF;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)",
            "LX/0lF;"
        }
    .end annotation

    .prologue
    .line 2225574
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2225575
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2225576
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "t_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 2225577
    :cond_0
    return-object v1
.end method

.method public static a(LX/0QB;)LX/FKp;
    .locals 1

    .prologue
    .line 2225578
    new-instance v0, LX/FKp;

    invoke-direct {v0}, LX/FKp;-><init>()V

    .line 2225579
    move-object v0, v0

    .line 2225580
    return-object v0
.end method
