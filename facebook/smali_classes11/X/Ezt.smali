.class public final enum LX/Ezt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ezt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ezt;

.field public static final enum PYMK_ADD_EVENT:LX/Ezt;

.field public static final enum PYMK_IMP_EVENT:LX/Ezt;

.field public static final enum PYMK_PROFILE_EVENT:LX/Ezt;

.field public static final enum PYMK_XOUT_EVENT:LX/Ezt;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2187705
    new-instance v0, LX/Ezt;

    const-string v1, "PYMK_ADD_EVENT"

    const-string v2, "regular_pymk_add"

    invoke-direct {v0, v1, v3, v2}, LX/Ezt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ezt;->PYMK_ADD_EVENT:LX/Ezt;

    .line 2187706
    new-instance v0, LX/Ezt;

    const-string v1, "PYMK_IMP_EVENT"

    const-string v2, "regular_pymk_imp"

    invoke-direct {v0, v1, v4, v2}, LX/Ezt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ezt;->PYMK_IMP_EVENT:LX/Ezt;

    .line 2187707
    new-instance v0, LX/Ezt;

    const-string v1, "PYMK_PROFILE_EVENT"

    const-string v2, "regular_pymk_profile"

    invoke-direct {v0, v1, v5, v2}, LX/Ezt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ezt;->PYMK_PROFILE_EVENT:LX/Ezt;

    .line 2187708
    new-instance v0, LX/Ezt;

    const-string v1, "PYMK_XOUT_EVENT"

    const-string v2, "regular_pymk_xout"

    invoke-direct {v0, v1, v6, v2}, LX/Ezt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Ezt;->PYMK_XOUT_EVENT:LX/Ezt;

    .line 2187709
    const/4 v0, 0x4

    new-array v0, v0, [LX/Ezt;

    sget-object v1, LX/Ezt;->PYMK_ADD_EVENT:LX/Ezt;

    aput-object v1, v0, v3

    sget-object v1, LX/Ezt;->PYMK_IMP_EVENT:LX/Ezt;

    aput-object v1, v0, v4

    sget-object v1, LX/Ezt;->PYMK_PROFILE_EVENT:LX/Ezt;

    aput-object v1, v0, v5

    sget-object v1, LX/Ezt;->PYMK_XOUT_EVENT:LX/Ezt;

    aput-object v1, v0, v6

    sput-object v0, LX/Ezt;->$VALUES:[LX/Ezt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2187710
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2187711
    iput-object p3, p0, LX/Ezt;->value:Ljava/lang/String;

    .line 2187712
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ezt;
    .locals 1

    .prologue
    .line 2187713
    const-class v0, LX/Ezt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ezt;

    return-object v0
.end method

.method public static values()[LX/Ezt;
    .locals 1

    .prologue
    .line 2187714
    sget-object v0, LX/Ezt;->$VALUES:[LX/Ezt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ezt;

    return-object v0
.end method
