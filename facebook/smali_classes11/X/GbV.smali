.class public LX/GbV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/GbV;


# instance fields
.field public final a:LX/10M;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field private final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/0tX;

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

.field public final h:LX/2Di;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Gbo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/2J4;LX/10M;LX/10O;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Di;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/2J4;",
            "LX/10M;",
            "LX/10O;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2Di;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369836
    iput-object p6, p0, LX/GbV;->a:LX/10M;

    .line 2369837
    iput-object p1, p0, LX/GbV;->b:LX/0Or;

    .line 2369838
    iput-object p2, p0, LX/GbV;->c:Landroid/content/Context;

    .line 2369839
    iput-object p3, p0, LX/GbV;->d:Ljava/util/concurrent/ExecutorService;

    .line 2369840
    iput-object p4, p0, LX/GbV;->e:LX/0tX;

    .line 2369841
    iget-object v0, p0, LX/GbV;->a:LX/10M;

    invoke-virtual {p5, v0, p7}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v0

    iput-object v0, p0, LX/GbV;->g:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 2369842
    iput-object p8, p0, LX/GbV;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2369843
    iput-object p9, p0, LX/GbV;->h:LX/2Di;

    .line 2369844
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GbV;->i:Ljava/util/List;

    .line 2369845
    return-void
.end method

.method public static a(LX/0QB;)LX/GbV;
    .locals 13

    .prologue
    .line 2369864
    sget-object v0, LX/GbV;->j:LX/GbV;

    if-nez v0, :cond_1

    .line 2369865
    const-class v1, LX/GbV;

    monitor-enter v1

    .line 2369866
    :try_start_0
    sget-object v0, LX/GbV;->j:LX/GbV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2369867
    if-eqz v2, :cond_0

    .line 2369868
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2369869
    new-instance v3, LX/GbV;

    const/16 v4, 0x12cb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    const-class v8, LX/2J4;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/2J4;

    invoke-static {v0}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v9

    check-cast v9, LX/10M;

    invoke-static {v0}, LX/10O;->b(LX/0QB;)LX/10O;

    move-result-object v10

    check-cast v10, LX/10O;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Di;->b(LX/0QB;)LX/2Di;

    move-result-object v12

    check-cast v12, LX/2Di;

    invoke-direct/range {v3 .. v12}, LX/GbV;-><init>(LX/0Or;Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/2J4;LX/10M;LX/10O;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Di;)V

    .line 2369870
    move-object v0, v3

    .line 2369871
    sput-object v0, LX/GbV;->j:LX/GbV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2369872
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2369873
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2369874
    :cond_1
    sget-object v0, LX/GbV;->j:LX/GbV;

    return-object v0

    .line 2369875
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2369876
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/GbV;LX/15i;I)V
    .locals 12

    .prologue
    .line 2369849
    iget-object v0, p0, LX/GbV;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2369850
    const/4 v0, 0x0

    const v1, -0x4f997970

    invoke-static {p1, p2, v0, v1}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :cond_0
    :goto_1
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v10, v0, LX/1vs;->b:I

    .line 2369851
    const/4 v0, 0x3

    invoke-virtual {v7, v10, v0}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2369852
    const/4 v0, 0x0

    invoke-virtual {v7, v10, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2369853
    const/4 v0, 0x1

    invoke-virtual {v7, v10, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2369854
    iget-object v1, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0834a0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v7, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2369855
    new-instance v0, Ljava/util/Date;

    const/4 v2, 0x4

    invoke-virtual {v7, v10, v2}, LX/15i;->k(II)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 2369856
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "MMMM dd, yyyy"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2369857
    iget-object v3, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0834a1

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2369858
    iget-object v0, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0834a2

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v0, 0x2

    invoke-virtual {v7, v10, v0}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v11, 0x7f0834a3

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2369859
    new-instance v0, LX/Gbo;

    const/4 v4, 0x0

    sget-object v5, LX/Gbn;->OTHER_SESSION:LX/Gbn;

    sget-object v6, LX/Gbm;->PREFERENCE:LX/Gbm;

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    .line 2369860
    iget-object v1, p0, LX/GbV;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2369861
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_0

    .line 2369862
    :cond_2
    iget-object v0, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v11, 0x7f0834a4

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2369863
    :cond_3
    return-void
.end method

.method public static e(LX/GbV;)Z
    .locals 2

    .prologue
    .line 2369846
    iget-object v1, p0, LX/GbV;->a:LX/10M;

    iget-object v0, p0, LX/GbV;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2369847
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2369848
    invoke-virtual {v1, v0}, LX/10M;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private f()LX/Gbo;
    .locals 8

    .prologue
    .line 2369783
    new-instance v0, LX/Gbo;

    iget-object v1, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08346b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083482

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const/4 v4, 0x0

    sget-object v5, LX/Gbn;->ADD_PASSCODE:LX/Gbn;

    sget-object v6, LX/Gbm;->PREFERENCE:LX/Gbm;

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    return-object v0
.end method

.method private g()LX/Gbo;
    .locals 8

    .prologue
    .line 2369834
    new-instance v0, LX/Gbo;

    iget-object v1, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083478

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    const-string v3, ""

    const/4 v4, 0x0

    sget-object v5, LX/Gbn;->CHANGE_PASSCODE:LX/Gbn;

    sget-object v6, LX/Gbm;->PREFERENCE:LX/Gbm;

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    return-object v0
.end method

.method private h()LX/Gbo;
    .locals 8

    .prologue
    .line 2369877
    new-instance v0, LX/Gbo;

    iget-object v1, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08347a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    const-string v3, ""

    const/4 v4, 0x0

    sget-object v5, LX/Gbn;->REMOVE_PASSCODE:LX/Gbn;

    sget-object v6, LX/Gbm;->PREFERENCE:LX/Gbm;

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    return-object v0
.end method

.method private i()LX/Gbo;
    .locals 8

    .prologue
    .line 2369833
    new-instance v0, LX/Gbo;

    iget-object v1, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0834a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0834a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const/4 v4, 0x1

    sget-object v5, LX/Gbn;->REMOVE_ACCOUNT:LX/Gbn;

    sget-object v6, LX/Gbm;->PREFERENCE:LX/Gbm;

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    return-object v0
.end method

.method private l()LX/Gbo;
    .locals 8

    .prologue
    .line 2369832
    new-instance v0, LX/Gbo;

    iget-object v1, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08347f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083483

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const/4 v4, 0x0

    sget-object v5, LX/Gbn;->USE_PASSWORD:LX/Gbn;

    sget-object v6, LX/Gbm;->PREFERENCE:LX/Gbm;

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    return-object v0
.end method

.method private m()LX/Gbo;
    .locals 8

    .prologue
    .line 2369831
    new-instance v0, LX/Gbo;

    iget-object v1, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0834a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0834a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const/4 v4, 0x1

    sget-object v5, LX/Gbn;->RESET_SETTINGS:LX/Gbn;

    sget-object v6, LX/Gbm;->PREFERENCE:LX/Gbm;

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2369825
    iget-object v0, p0, LX/GbV;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2369826
    iget-object v0, p0, LX/GbV;->e:LX/0tX;

    .line 2369827
    new-instance v1, LX/Gbs;

    invoke-direct {v1}, LX/Gbs;-><init>()V

    move-object v1, v1

    .line 2369828
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2369829
    new-instance v1, LX/GbS;

    invoke-direct {v1, p0, p2, p1}, LX/GbS;-><init>(LX/GbV;Landroid/content/Context;Ljava/lang/Runnable;)V

    iget-object v2, p0, LX/GbV;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2369830
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/Gbo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2369784
    iget-object v1, p0, LX/GbV;->a:LX/10M;

    iget-object v0, p0, LX/GbV;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2369785
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2369786
    invoke-virtual {v1, v0}, LX/10M;->b(Ljava/lang/String;)Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    move-result-object v0

    .line 2369787
    if-nez v0, :cond_1

    .line 2369788
    sget-object v0, LX/GbU;->NO_DBL:LX/GbU;

    .line 2369789
    :goto_0
    move-object v0, v0

    .line 2369790
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2369791
    new-instance v3, LX/Gbo;

    iget-object v4, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08349e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, LX/Gbm;->CATEGORY:LX/Gbm;

    const-string v10, ""

    invoke-direct/range {v3 .. v10}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    move-object v2, v3

    .line 2369792
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369793
    sget-object v2, LX/GbT;->a:[I

    invoke-virtual {v0}, LX/GbU;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2369794
    :goto_1
    iget-object v0, p0, LX/GbV;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2369795
    new-instance v3, LX/Gbo;

    iget-object v4, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08349f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, ""

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, LX/Gbm;->CATEGORY:LX/Gbm;

    const-string v10, ""

    invoke-direct/range {v3 .. v10}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    move-object v0, v3

    .line 2369796
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369797
    iget-object v0, p0, LX/GbV;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2369798
    :cond_0
    return-object v1

    .line 2369799
    :pswitch_0
    invoke-direct {p0}, LX/GbV;->f()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369800
    invoke-direct {p0}, LX/GbV;->i()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2369801
    :pswitch_1
    invoke-direct {p0}, LX/GbV;->g()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369802
    invoke-direct {p0}, LX/GbV;->h()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369803
    invoke-direct {p0}, LX/GbV;->i()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2369804
    :pswitch_2
    new-instance v3, LX/Gbo;

    iget-object v4, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08347e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f083480

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const/4 v7, 0x0

    sget-object v8, LX/Gbn;->REMEMBER_PASSWORD:LX/Gbn;

    sget-object v9, LX/Gbm;->PREFERENCE:LX/Gbm;

    const-string v10, ""

    invoke-direct/range {v3 .. v10}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    move-object v0, v3

    .line 2369805
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369806
    new-instance v3, LX/Gbo;

    iget-object v4, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f083481

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/GbV;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f083482

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const/4 v7, 0x0

    sget-object v8, LX/Gbn;->USE_PASSCODE:LX/Gbn;

    sget-object v9, LX/Gbm;->PREFERENCE:LX/Gbm;

    const-string v10, ""

    invoke-direct/range {v3 .. v10}, LX/Gbo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/Gbn;LX/Gbm;Ljava/lang/String;)V

    move-object v0, v3

    .line 2369807
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369808
    invoke-direct {p0}, LX/GbV;->i()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2369809
    :pswitch_3
    invoke-direct {p0}, LX/GbV;->g()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369810
    invoke-direct {p0}, LX/GbV;->h()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369811
    invoke-direct {p0}, LX/GbV;->l()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369812
    invoke-direct {p0}, LX/GbV;->m()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2369813
    :pswitch_4
    invoke-direct {p0}, LX/GbV;->f()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369814
    invoke-direct {p0}, LX/GbV;->l()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369815
    invoke-direct {p0}, LX/GbV;->m()LX/Gbo;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2369816
    :cond_1
    iget-object v1, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    const-string v2, "password_account"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2369817
    sget-object v0, LX/GbU;->SAVED_ID:LX/GbU;

    goto/16 :goto_0

    .line 2369818
    :cond_2
    iget-object v0, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2369819
    invoke-static {p0}, LX/GbV;->e(LX/GbV;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2369820
    sget-object v0, LX/GbU;->DBL_WITH_PIN_CAN_SAVE_ID:LX/GbU;

    goto/16 :goto_0

    .line 2369821
    :cond_3
    sget-object v0, LX/GbU;->DBL_WITH_PIN:LX/GbU;

    goto/16 :goto_0

    .line 2369822
    :cond_4
    invoke-static {p0}, LX/GbV;->e(LX/GbV;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2369823
    sget-object v0, LX/GbU;->DBL_WITHOUT_PIN_CAN_SAVE_ID:LX/GbU;

    goto/16 :goto_0

    .line 2369824
    :cond_5
    sget-object v0, LX/GbU;->DBL_WITHOUT_PIN:LX/GbU;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
