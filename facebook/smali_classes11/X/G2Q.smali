.class public final LX/G2Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

.field public final synthetic b:LX/1Pc;

.field public final synthetic c:Lcom/facebook/timeline/protiles/model/ProtileModel;

.field public final synthetic d:LX/G2S;


# direct methods
.method public constructor <init>(LX/G2S;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;LX/1Pc;Lcom/facebook/timeline/protiles/model/ProtileModel;)V
    .locals 0

    .prologue
    .line 2314015
    iput-object p1, p0, LX/G2Q;->d:LX/G2S;

    iput-object p2, p0, LX/G2Q;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    iput-object p3, p0, LX/G2Q;->b:LX/1Pc;

    iput-object p4, p0, LX/G2Q;->c:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const v0, -0x32f04a18

    invoke-static {v7, v8, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2314016
    iget-object v0, p0, LX/G2Q;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 2314017
    iget-object v0, p0, LX/G2Q;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2314018
    iget-object v0, p0, LX/G2Q;->b:LX/1Pc;

    iget-object v2, p0, LX/G2Q;->c:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/model/ProtileModel;->m()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/2h7;->PROTILES:LX/2h7;

    new-instance v5, LX/G2P;

    invoke-direct {v5, p0, v1, v4}, LX/G2P;-><init>(LX/G2Q;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-interface/range {v0 .. v5}, LX/1Pc;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v0

    .line 2314019
    iget-object v2, p0, LX/G2Q;->c:Lcom/facebook/timeline/protiles/model/ProtileModel;

    iget-object v3, v0, LX/5Oh;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v4, p0, LX/G2Q;->d:LX/G2S;

    iget-object v4, v4, LX/G2S;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/facebook/timeline/protiles/model/ProtileModel;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;J)V

    .line 2314020
    iget-object v2, p0, LX/G2Q;->d:LX/G2S;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v0, v0, LX/5Oh;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v2, v4, v5, v0, v8}, LX/G2S;->a$redex0(LX/G2S;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2314021
    const v0, 0x23c1772b

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
