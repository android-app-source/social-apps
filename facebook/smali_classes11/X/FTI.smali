.class public final LX/FTI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 44

    .prologue
    .line 2245156
    const/16 v40, 0x0

    .line 2245157
    const/16 v39, 0x0

    .line 2245158
    const/16 v38, 0x0

    .line 2245159
    const/16 v37, 0x0

    .line 2245160
    const/16 v36, 0x0

    .line 2245161
    const/16 v35, 0x0

    .line 2245162
    const/16 v34, 0x0

    .line 2245163
    const/16 v33, 0x0

    .line 2245164
    const/16 v32, 0x0

    .line 2245165
    const/16 v31, 0x0

    .line 2245166
    const/16 v30, 0x0

    .line 2245167
    const/16 v29, 0x0

    .line 2245168
    const/16 v28, 0x0

    .line 2245169
    const/16 v27, 0x0

    .line 2245170
    const/16 v26, 0x0

    .line 2245171
    const/16 v25, 0x0

    .line 2245172
    const/16 v24, 0x0

    .line 2245173
    const/16 v23, 0x0

    .line 2245174
    const/16 v22, 0x0

    .line 2245175
    const/16 v21, 0x0

    .line 2245176
    const/16 v20, 0x0

    .line 2245177
    const/16 v19, 0x0

    .line 2245178
    const/16 v18, 0x0

    .line 2245179
    const/16 v17, 0x0

    .line 2245180
    const/16 v16, 0x0

    .line 2245181
    const/4 v15, 0x0

    .line 2245182
    const/4 v14, 0x0

    .line 2245183
    const/4 v13, 0x0

    .line 2245184
    const/4 v12, 0x0

    .line 2245185
    const/4 v11, 0x0

    .line 2245186
    const/4 v10, 0x0

    .line 2245187
    const/4 v9, 0x0

    .line 2245188
    const/4 v8, 0x0

    .line 2245189
    const/4 v7, 0x0

    .line 2245190
    const/4 v6, 0x0

    .line 2245191
    const/4 v5, 0x0

    .line 2245192
    const/4 v4, 0x0

    .line 2245193
    const/4 v3, 0x0

    .line 2245194
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1

    .line 2245195
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2245196
    const/4 v3, 0x0

    .line 2245197
    :goto_0
    return v3

    .line 2245198
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2245199
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v41

    sget-object v42, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    if-eq v0, v1, :cond_1d

    .line 2245200
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v41

    .line 2245201
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2245202
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v42

    sget-object v43, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    if-eq v0, v1, :cond_1

    if-eqz v41, :cond_1

    .line 2245203
    const-string v42, "alternate_name"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_2

    .line 2245204
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    goto :goto_1

    .line 2245205
    :cond_2
    const-string v42, "can_viewer_act_as_memorial_contact"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_3

    .line 2245206
    const/4 v12, 0x1

    .line 2245207
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v39

    goto :goto_1

    .line 2245208
    :cond_3
    const-string v42, "can_viewer_block"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_4

    .line 2245209
    const/4 v11, 0x1

    .line 2245210
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v38

    goto :goto_1

    .line 2245211
    :cond_4
    const-string v42, "can_viewer_message"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_5

    .line 2245212
    const/4 v10, 0x1

    .line 2245213
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v37

    goto :goto_1

    .line 2245214
    :cond_5
    const-string v42, "can_viewer_poke"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_6

    .line 2245215
    const/4 v9, 0x1

    .line 2245216
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v36

    goto :goto_1

    .line 2245217
    :cond_6
    const-string v42, "can_viewer_post"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_7

    .line 2245218
    const/4 v8, 0x1

    .line 2245219
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v35

    goto :goto_1

    .line 2245220
    :cond_7
    const-string v42, "can_viewer_report"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_8

    .line 2245221
    const/4 v7, 0x1

    .line 2245222
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v34

    goto/16 :goto_1

    .line 2245223
    :cond_8
    const-string v42, "cover_photo"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_9

    .line 2245224
    invoke-static/range {p0 .. p1}, LX/FTF;->a(LX/15w;LX/186;)I

    move-result v33

    goto/16 :goto_1

    .line 2245225
    :cond_9
    const-string v42, "friends"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_a

    .line 2245226
    invoke-static/range {p0 .. p1}, LX/5zW;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 2245227
    :cond_a
    const-string v42, "friendship_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_b

    .line 2245228
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v31

    goto/16 :goto_1

    .line 2245229
    :cond_b
    const-string v42, "id"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_c

    .line 2245230
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v30

    goto/16 :goto_1

    .line 2245231
    :cond_c
    const-string v42, "is_followed_by_everyone"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_d

    .line 2245232
    const/4 v6, 0x1

    .line 2245233
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v29

    goto/16 :goto_1

    .line 2245234
    :cond_d
    const-string v42, "is_verified"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_e

    .line 2245235
    const/4 v5, 0x1

    .line 2245236
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v28

    goto/16 :goto_1

    .line 2245237
    :cond_e
    const-string v42, "is_work_user"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_f

    .line 2245238
    const/4 v4, 0x1

    .line 2245239
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v27

    goto/16 :goto_1

    .line 2245240
    :cond_f
    const-string v42, "name"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_10

    .line 2245241
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto/16 :goto_1

    .line 2245242
    :cond_10
    const-string v42, "posted_item_privacy_scope"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_11

    .line 2245243
    invoke-static/range {p0 .. p1}, LX/5R2;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 2245244
    :cond_11
    const-string v42, "preliminaryProfilePicture"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_12

    .line 2245245
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 2245246
    :cond_12
    const-string v42, "profile_intro_card"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_13

    .line 2245247
    invoke-static/range {p0 .. p1}, LX/FTG;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 2245248
    :cond_13
    const-string v42, "profile_photo"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_14

    .line 2245249
    invoke-static/range {p0 .. p1}, LX/FTS;->a(LX/15w;LX/186;)I

    move-result v22

    goto/16 :goto_1

    .line 2245250
    :cond_14
    const-string v42, "profile_picture"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_15

    .line 2245251
    invoke-static/range {p0 .. p1}, LX/FTH;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 2245252
    :cond_15
    const-string v42, "profile_picture_is_silhouette"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_16

    .line 2245253
    const/4 v3, 0x1

    .line 2245254
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto/16 :goto_1

    .line 2245255
    :cond_16
    const-string v42, "profile_video"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_17

    .line 2245256
    invoke-static/range {p0 .. p1}, LX/FTP;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 2245257
    :cond_17
    const-string v42, "secondary_subscribe_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_18

    .line 2245258
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto/16 :goto_1

    .line 2245259
    :cond_18
    const-string v42, "structured_name"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_19

    .line 2245260
    invoke-static/range {p0 .. p1}, LX/3EF;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 2245261
    :cond_19
    const-string v42, "subscribe_status"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1a

    .line 2245262
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    goto/16 :goto_1

    .line 2245263
    :cond_1a
    const-string v42, "timeline_context_items"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1b

    .line 2245264
    invoke-static/range {p0 .. p1}, LX/FTZ;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 2245265
    :cond_1b
    const-string v42, "url"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v42

    if-eqz v42, :cond_1c

    .line 2245266
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto/16 :goto_1

    .line 2245267
    :cond_1c
    const-string v42, "viewer_saved_state"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v41

    if-eqz v41, :cond_0

    .line 2245268
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    goto/16 :goto_1

    .line 2245269
    :cond_1d
    const/16 v41, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2245270
    const/16 v41, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2245271
    if-eqz v12, :cond_1e

    .line 2245272
    const/4 v12, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v12, v1}, LX/186;->a(IZ)V

    .line 2245273
    :cond_1e
    if-eqz v11, :cond_1f

    .line 2245274
    const/4 v11, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v11, v1}, LX/186;->a(IZ)V

    .line 2245275
    :cond_1f
    if-eqz v10, :cond_20

    .line 2245276
    const/4 v10, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v10, v1}, LX/186;->a(IZ)V

    .line 2245277
    :cond_20
    if-eqz v9, :cond_21

    .line 2245278
    const/4 v9, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v9, v1}, LX/186;->a(IZ)V

    .line 2245279
    :cond_21
    if-eqz v8, :cond_22

    .line 2245280
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 2245281
    :cond_22
    if-eqz v7, :cond_23

    .line 2245282
    const/4 v7, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 2245283
    :cond_23
    const/4 v7, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 2245284
    const/16 v7, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 2245285
    const/16 v7, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 2245286
    const/16 v7, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 2245287
    if-eqz v6, :cond_24

    .line 2245288
    const/16 v6, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 2245289
    :cond_24
    if-eqz v5, :cond_25

    .line 2245290
    const/16 v5, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 2245291
    :cond_25
    if-eqz v4, :cond_26

    .line 2245292
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 2245293
    :cond_26
    const/16 v4, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2245294
    const/16 v4, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2245295
    const/16 v4, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2245296
    const/16 v4, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2245297
    const/16 v4, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2245298
    const/16 v4, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2245299
    if-eqz v3, :cond_27

    .line 2245300
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 2245301
    :cond_27
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2245302
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2245303
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2245304
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2245305
    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 2245306
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 2245307
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 2245308
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/16 v5, 0x1b

    const/16 v4, 0x18

    const/16 v3, 0x16

    const/16 v2, 0x9

    .line 2245309
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2245310
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2245311
    if-eqz v0, :cond_0

    .line 2245312
    const-string v1, "alternate_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245313
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245314
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245315
    if-eqz v0, :cond_1

    .line 2245316
    const-string v1, "can_viewer_act_as_memorial_contact"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245317
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245318
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245319
    if-eqz v0, :cond_2

    .line 2245320
    const-string v1, "can_viewer_block"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245321
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245322
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245323
    if-eqz v0, :cond_3

    .line 2245324
    const-string v1, "can_viewer_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245325
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245326
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245327
    if-eqz v0, :cond_4

    .line 2245328
    const-string v1, "can_viewer_poke"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245329
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245330
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245331
    if-eqz v0, :cond_5

    .line 2245332
    const-string v1, "can_viewer_post"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245333
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245334
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245335
    if-eqz v0, :cond_6

    .line 2245336
    const-string v1, "can_viewer_report"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245337
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245338
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245339
    if-eqz v0, :cond_7

    .line 2245340
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245341
    invoke-static {p0, v0, p2, p3}, LX/FTF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245342
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245343
    if-eqz v0, :cond_8

    .line 2245344
    const-string v1, "friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245345
    invoke-static {p0, v0, p2}, LX/5zW;->a(LX/15i;ILX/0nX;)V

    .line 2245346
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2245347
    if-eqz v0, :cond_9

    .line 2245348
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245349
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245350
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2245351
    if-eqz v0, :cond_a

    .line 2245352
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245353
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245354
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245355
    if-eqz v0, :cond_b

    .line 2245356
    const-string v1, "is_followed_by_everyone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245357
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245358
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245359
    if-eqz v0, :cond_c

    .line 2245360
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245361
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245362
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245363
    if-eqz v0, :cond_d

    .line 2245364
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245365
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245366
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2245367
    if-eqz v0, :cond_e

    .line 2245368
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245369
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245370
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245371
    if-eqz v0, :cond_f

    .line 2245372
    const-string v1, "posted_item_privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245373
    invoke-static {p0, v0, p2, p3}, LX/5R2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245374
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245375
    if-eqz v0, :cond_10

    .line 2245376
    const-string v1, "preliminaryProfilePicture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245377
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2245378
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245379
    if-eqz v0, :cond_11

    .line 2245380
    const-string v1, "profile_intro_card"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245381
    invoke-static {p0, v0, p2, p3}, LX/FTG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245382
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245383
    if-eqz v0, :cond_12

    .line 2245384
    const-string v1, "profile_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245385
    invoke-static {p0, v0, p2, p3}, LX/FTS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245386
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245387
    if-eqz v0, :cond_13

    .line 2245388
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245389
    invoke-static {p0, v0, p2}, LX/FTH;->a(LX/15i;ILX/0nX;)V

    .line 2245390
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2245391
    if-eqz v0, :cond_14

    .line 2245392
    const-string v1, "profile_picture_is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245393
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2245394
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245395
    if-eqz v0, :cond_15

    .line 2245396
    const-string v1, "profile_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245397
    invoke-static {p0, v0, p2, p3}, LX/FTP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245398
    :cond_15
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2245399
    if-eqz v0, :cond_16

    .line 2245400
    const-string v0, "secondary_subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245401
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245402
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245403
    if-eqz v0, :cond_17

    .line 2245404
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245405
    invoke-static {p0, v0, p2, p3}, LX/3EF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245406
    :cond_17
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2245407
    if-eqz v0, :cond_18

    .line 2245408
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245409
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245410
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245411
    if-eqz v0, :cond_19

    .line 2245412
    const-string v1, "timeline_context_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245413
    invoke-static {p0, v0, p2, p3}, LX/FTZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245414
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2245415
    if-eqz v0, :cond_1a

    .line 2245416
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245417
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245418
    :cond_1a
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 2245419
    if-eqz v0, :cond_1b

    .line 2245420
    const-string v0, "viewer_saved_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245421
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245422
    :cond_1b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2245423
    return-void
.end method
