.class public LX/Fbz;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261561
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2261562
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbz;
    .locals 3

    .prologue
    .line 2261489
    const-class v1, LX/Fbz;

    monitor-enter v1

    .line 2261490
    :try_start_0
    sget-object v0, LX/Fbz;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261491
    sput-object v2, LX/Fbz;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261492
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261493
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2261494
    new-instance v0, LX/Fbz;

    invoke-direct {v0}, LX/Fbz;-><init>()V

    .line 2261495
    move-object v0, v0

    .line 2261496
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261497
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fbz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261498
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261499
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261500
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261501
    if-nez v0, :cond_0

    .line 2261502
    const/4 v0, 0x0

    .line 2261503
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v2

    invoke-static {v2}, LX/FbX;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v2

    .line 2261504
    iput-object v2, v1, LX/8dV;->d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    .line 2261505
    move-object v1, v1

    .line 2261506
    new-instance v2, LX/8dX;

    invoke-direct {v2}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    .line 2261507
    iput-object v3, v2, LX/8dX;->L:Ljava/lang/String;

    .line 2261508
    move-object v2, v2

    .line 2261509
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v4, 0x581d640b

    invoke-direct {v3, v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2261510
    iput-object v3, v2, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2261511
    move-object v2, v2

    .line 2261512
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v3

    .line 2261513
    iput-object v3, v2, LX/8dX;->ad:Ljava/lang/String;

    .line 2261514
    move-object v2, v2

    .line 2261515
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v3

    .line 2261516
    iput-boolean v3, v2, LX/8dX;->W:Z

    .line 2261517
    move-object v2, v2

    .line 2261518
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 2261519
    if-nez v3, :cond_1

    .line 2261520
    const/4 v4, 0x0

    .line 2261521
    :goto_1
    move-object v3, v4

    .line 2261522
    iput-object v3, v2, LX/8dX;->aw:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2261523
    move-object v2, v2

    .line 2261524
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ap()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 2261525
    if-nez v3, :cond_2

    .line 2261526
    const/4 v4, 0x0

    .line 2261527
    :goto_2
    move-object v3, v4

    .line 2261528
    iput-object v3, v2, LX/8dX;->h:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    .line 2261529
    move-object v2, v2

    .line 2261530
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fI()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v3

    .line 2261531
    if-nez v3, :cond_3

    .line 2261532
    const/4 v4, 0x0

    .line 2261533
    :goto_3
    move-object v3, v4

    .line 2261534
    iput-object v3, v2, LX/8dX;->ac:Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    .line 2261535
    move-object v2, v2

    .line 2261536
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2261537
    iput-object v0, v2, LX/8dX;->F:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2261538
    move-object v0, v2

    .line 2261539
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2261540
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2261541
    move-object v0, v1

    .line 2261542
    goto :goto_0

    :cond_1
    new-instance v4, LX/4aO;

    invoke-direct {v4}, LX/4aO;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p0

    .line 2261543
    iput p0, v4, LX/4aO;->b:I

    .line 2261544
    move-object v4, v4

    .line 2261545
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p0

    .line 2261546
    iput p0, v4, LX/4aO;->d:I

    .line 2261547
    move-object v4, v4

    .line 2261548
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    .line 2261549
    iput-object p0, v4, LX/4aO;->c:Ljava/lang/String;

    .line 2261550
    move-object v4, v4

    .line 2261551
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->l()Ljava/lang/String;

    move-result-object p0

    .line 2261552
    iput-object p0, v4, LX/4aO;->a:Ljava/lang/String;

    .line 2261553
    move-object v4, v4

    .line 2261554
    invoke-virtual {v4}, LX/4aO;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v4

    goto :goto_1

    :cond_2
    new-instance v4, LX/8hU;

    invoke-direct {v4}, LX/8hU;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    .line 2261555
    iput-object p0, v4, LX/8hU;->a:Ljava/lang/String;

    .line 2261556
    move-object v4, v4

    .line 2261557
    invoke-virtual {v4}, LX/8hU;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$BioTextModel;

    move-result-object v4

    goto :goto_2

    :cond_3
    new-instance v4, LX/8hW;

    invoke-direct {v4}, LX/8hW;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;->a()I

    move-result p0

    .line 2261558
    iput p0, v4, LX/8hW;->a:I

    .line 2261559
    move-object v4, v4

    .line 2261560
    invoke-virtual {v4}, LX/8hW;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsUserModels$SearchResultsUserModel$MutualFriendsModel;

    move-result-object v4

    goto :goto_3
.end method
