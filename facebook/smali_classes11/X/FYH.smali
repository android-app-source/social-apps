.class public final LX/FYH;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;


# direct methods
.method public constructor <init>(Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;)V
    .locals 0

    .prologue
    .line 2255909
    iput-object p1, p0, LX/FYH;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 12

    .prologue
    .line 2255910
    iget-object v0, p0, LX/FYH;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    iget-object v0, v0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;

    iget-object v0, v0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->d:LX/FYI;

    iget-object v0, v0, LX/FYI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081abc

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2255911
    iget-object v0, p0, LX/FYH;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    iget-object v0, v0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;

    iget-object v0, v0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->d:LX/FYI;

    iget-object v1, p0, LX/FYH;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    iget-object v1, v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;

    iget-wide v2, v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->a:J

    iget-object v1, p0, LX/FYH;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    iget-object v1, v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;

    iget-object v1, v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->b:Ljava/lang/String;

    .line 2255912
    const-string v4, "ARCHIVED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2255913
    iget-object v4, v0, LX/FYI;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Sc;

    invoke-virtual {v4, v2, v3}, LX/1Sc;->a(J)V

    .line 2255914
    :goto_0
    return-void

    .line 2255915
    :cond_0
    iget-object v4, v0, LX/FYI;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Sc;

    const/4 v7, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2255916
    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "is_unarchived_client"

    aput-object v6, v5, v8

    const-string v6, "saved_state"

    aput-object v6, v5, v9

    const-string v6, "time_saved_ms"

    aput-object v6, v5, v10

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, v4, LX/1Sc;->c:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v4, v2, v3, v5, v6}, LX/1Sc;->a(LX/1Sc;J[Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2255917
    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2255918
    iget-object v0, p0, LX/FYH;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    iget-object v0, v0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;

    iget-object v0, v0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->d:LX/FYI;

    iget-object v1, p0, LX/FYH;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    iget-object v1, v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;

    iget-wide v2, v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->a:J

    iget-object v1, p0, LX/FYH;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;

    iget-object v1, v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1$1;->a:Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;

    iget-object v1, v1, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;->b:Ljava/lang/String;

    .line 2255919
    const-string p0, "ARCHIVED"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2255920
    iget-object p0, v0, LX/FYI;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1Sc;

    .line 2255921
    iget-object p1, p0, LX/1Sc;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$4;

    invoke-direct {v0, p0, v2, v3}, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$4;-><init>(LX/1Sc;J)V

    const v1, -0x265e7a9c

    invoke-static {p1, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2255922
    :cond_0
    return-void
.end method
