.class public final LX/GLI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/GLK;


# direct methods
.method public constructor <init>(LX/GLK;)V
    .locals 0

    .prologue
    .line 2342584
    iput-object p1, p0, LX/GLI;->a:LX/GLK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2342585
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2342586
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2342587
    iget-object v0, p0, LX/GLI;->a:LX/GLK;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2342588
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    .line 2342589
    iget-object v0, p0, LX/GLI;->a:LX/GLK;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/GIr;->f(Z)V

    .line 2342590
    return-void
.end method
