.class public LX/GJy;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

.field public b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

.field private c:LX/GG6;

.field public d:I

.field public e:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GG6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340010
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2340011
    iput-object p1, p0, LX/GJy;->c:LX/GG6;

    .line 2340012
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2340007
    invoke-super {p0}, LX/GHg;->a()V

    .line 2340008
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340009
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2

    .prologue
    .line 2340013
    check-cast p1, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340014
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340015
    const v0, 0x7f080aa9

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2340016
    iput-object p1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340017
    iget-object v0, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2340018
    iget-object v0, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->it_()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->k()I

    move-result v0

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x3

    :goto_0
    move v0, v0

    .line 2340019
    iput v0, p0, LX/GJy;->d:I

    .line 2340020
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/GJy;->e:LX/0Pz;

    .line 2340021
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/GJy;->f:LX/0Pz;

    .line 2340022
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/GJy;->g:LX/0Pz;

    .line 2340023
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/GJy;->h:LX/0Pz;

    .line 2340024
    iget-object v0, p0, LX/GJy;->e:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a035b

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340025
    iget-object v0, p0, LX/GJy;->f:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0f003c

    iget-object p2, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->a()I

    move-result p2

    invoke-virtual {v1, p1, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340026
    iget-object v0, p0, LX/GJy;->g:LX/0Pz;

    iget-object v1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->a()I

    move-result v1

    iget-object p1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v1, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340027
    iget-object v0, p0, LX/GJy;->h:LX/0Pz;

    iget-object v1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->a()I

    move-result v1

    int-to-float v1, v1

    iget-object p1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->iu_()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340028
    iget-object v0, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->it_()I

    move-result v0

    if-lez v0, :cond_3

    .line 2340029
    iget-object v0, p0, LX/GJy;->e:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a035c

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340030
    iget-object v0, p0, LX/GJy;->f:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0f003e

    iget-object p2, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->it_()I

    move-result p2

    invoke-virtual {v1, p1, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340031
    iget-object v0, p0, LX/GJy;->g:LX/0Pz;

    iget-object v1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->it_()I

    move-result v1

    iget-object p1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v1, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340032
    iget-object v0, p0, LX/GJy;->h:LX/0Pz;

    iget-object v1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->it_()I

    move-result v1

    int-to-float v1, v1

    iget-object p1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->iu_()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340033
    :cond_1
    :goto_1
    iget v0, p0, LX/GJy;->d:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 2340034
    iget-object v0, p0, LX/GJy;->e:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a035d

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340035
    :goto_2
    iget-object v0, p0, LX/GJy;->f:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0f003d

    iget-object p2, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->d()I

    move-result p2

    invoke-virtual {v1, p1, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340036
    iget-object v0, p0, LX/GJy;->g:LX/0Pz;

    iget-object v1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->d()I

    move-result v1

    iget-object p1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v1, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340037
    iget-object v0, p0, LX/GJy;->h:LX/0Pz;

    iget-object v1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->d()I

    move-result v1

    int-to-float v1, v1

    iget-object p1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->iu_()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340038
    iget-object v0, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    new-instance v1, LX/GG9;

    invoke-direct {v1}, LX/GG9;-><init>()V

    iget p1, p0, LX/GJy;->d:I

    .line 2340039
    iput p1, v1, LX/GG9;->g:I

    .line 2340040
    move-object v1, v1

    .line 2340041
    iget-object p1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->iu_()I

    move-result p1

    iget-object p2, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p1, p2}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 2340042
    iput-object p1, v1, LX/GG9;->a:Ljava/lang/String;

    .line 2340043
    move-object v1, v1

    .line 2340044
    iget-object p1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f080ab0

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2340045
    iput-object p1, v1, LX/GG9;->b:Ljava/lang/String;

    .line 2340046
    move-object v1, v1

    .line 2340047
    iget-object p1, p0, LX/GJy;->e:LX/0Pz;

    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    .line 2340048
    iput-object p1, v1, LX/GG9;->f:LX/0Px;

    .line 2340049
    move-object v1, v1

    .line 2340050
    iget-object p1, p0, LX/GJy;->f:LX/0Pz;

    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    .line 2340051
    iput-object p1, v1, LX/GG9;->d:LX/0Px;

    .line 2340052
    move-object v1, v1

    .line 2340053
    iget-object p1, p0, LX/GJy;->g:LX/0Pz;

    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    .line 2340054
    iput-object p1, v1, LX/GG9;->c:LX/0Px;

    .line 2340055
    move-object v1, v1

    .line 2340056
    iget-object p1, p0, LX/GJy;->h:LX/0Pz;

    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    .line 2340057
    iput-object p1, v1, LX/GG9;->e:LX/0Px;

    .line 2340058
    move-object v1, v1

    .line 2340059
    invoke-virtual {v1}, LX/GG9;->a()LX/GGA;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->setViewModel(LX/GGA;)V

    .line 2340060
    const/4 v0, 0x0

    .line 2340061
    iput-object v0, p0, LX/GJy;->e:LX/0Pz;

    .line 2340062
    iput-object v0, p0, LX/GJy;->f:LX/0Pz;

    .line 2340063
    iput-object v0, p0, LX/GJy;->g:LX/0Pz;

    .line 2340064
    iput-object v0, p0, LX/GJy;->h:LX/0Pz;

    .line 2340065
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2340066
    return-void

    :cond_2
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 2340067
    :cond_3
    iget-object v0, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->k()I

    move-result v0

    if-lez v0, :cond_1

    .line 2340068
    iget-object v0, p0, LX/GJy;->e:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a035c

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340069
    iget-object v0, p0, LX/GJy;->f:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0f003f

    iget-object p2, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->k()I

    move-result p2

    invoke-virtual {v1, p1, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340070
    iget-object v0, p0, LX/GJy;->g:LX/0Pz;

    iget-object v1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->k()I

    move-result v1

    iget-object p1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v1, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340071
    iget-object v0, p0, LX/GJy;->h:LX/0Pz;

    iget-object v1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->k()I

    move-result v1

    int-to-float v1, v1

    iget-object p1, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->iu_()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2340072
    goto/16 :goto_1

    .line 2340073
    :cond_4
    iget-object v0, p0, LX/GJy;->e:LX/0Pz;

    iget-object v1, p0, LX/GJy;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0a035c

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2340005
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    iput-object v0, p0, LX/GJy;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2340006
    return-void
.end method
