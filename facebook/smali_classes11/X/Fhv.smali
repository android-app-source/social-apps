.class public final LX/Fhv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Cw5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Sl;


# direct methods
.method public constructor <init>(LX/2Sl;)V
    .locals 0

    .prologue
    .line 2273898
    iput-object p1, p0, LX/Fhv;->a:LX/2Sl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2273899
    iget-object v0, p0, LX/Fhv;->a:LX/2Sl;

    invoke-static {v0}, LX/2Sl;->k(LX/2Sl;)V

    .line 2273900
    iget-object v0, p0, LX/Fhv;->a:LX/2Sl;

    invoke-virtual {v0}, LX/2SP;->g()V

    .line 2273901
    iget-object v0, p0, LX/Fhv;->a:LX/2Sl;

    iget-object v0, v0, LX/2Sl;->h:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_PYMK_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2273902
    monitor-enter p0

    .line 2273903
    :try_start_0
    iget-object v0, p0, LX/Fhv;->a:LX/2Sl;

    const/4 v1, 0x0

    .line 2273904
    iput-object v1, v0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2273905
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2273906
    check-cast p1, LX/Cw5;

    .line 2273907
    iget-object v0, p0, LX/Fhv;->a:LX/2Sl;

    invoke-static {v0}, LX/2Sl;->k(LX/2Sl;)V

    .line 2273908
    iget-object v0, p0, LX/Fhv;->a:LX/2Sl;

    invoke-virtual {v0, p1}, LX/2SP;->a(LX/Cw5;)V

    .line 2273909
    iget-object v0, p0, LX/Fhv;->a:LX/2Sl;

    .line 2273910
    iget-object v1, p1, LX/Cw5;->a:LX/0Px;

    move-object v1, v1

    .line 2273911
    invoke-static {v0, v1}, LX/2Sl;->b(LX/2Sl;LX/0Px;)V

    .line 2273912
    monitor-enter p0

    .line 2273913
    :try_start_0
    iget-object v0, p0, LX/Fhv;->a:LX/2Sl;

    const/4 v1, 0x0

    .line 2273914
    iput-object v1, v0, LX/2Sl;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2273915
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
