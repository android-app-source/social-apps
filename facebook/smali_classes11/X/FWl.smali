.class public LX/FWl;
.super LX/5OG;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2252585
    invoke-direct {p0, p1}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 2252586
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2252587
    if-nez p2, :cond_0

    .line 2252588
    new-instance p2, LX/FWm;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, LX/FWm;-><init>(Landroid/content/Context;)V

    .line 2252589
    :goto_0
    invoke-virtual {p0, p1}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/16 p3, 0x8

    const/4 p1, 0x0

    .line 2252590
    if-nez v0, :cond_1

    .line 2252591
    :goto_1
    return-object p2

    .line 2252592
    :cond_0
    check-cast p2, LX/FWm;

    goto :goto_0

    .line 2252593
    :cond_1
    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2252594
    iget-object v1, p2, LX/FWm;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v1, p3}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2252595
    :goto_2
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2252596
    if-nez v1, :cond_3

    .line 2252597
    iget-object v1, p2, LX/FWm;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2252598
    :goto_3
    invoke-interface {v0}, Landroid/view/MenuItem;->isCheckable()Z

    move-result v1

    iput-boolean v1, p2, LX/FWm;->n:Z

    .line 2252599
    invoke-interface {v0}, Landroid/view/MenuItem;->isChecked()Z

    move-result v1

    invoke-virtual {p2, v1}, LX/FWm;->setChecked(Z)V

    .line 2252600
    invoke-interface {v0}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    invoke-virtual {p2, v1}, LX/FWm;->setEnabled(Z)V

    goto :goto_1

    .line 2252601
    :cond_2
    iget-object v1, p2, LX/FWm;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setVisibility(I)V

    .line 2252602
    iget-object v1, p2, LX/FWm;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2252603
    :cond_3
    iget-object p0, p2, LX/FWm;->l:Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2252604
    iget-object p0, p2, LX/FWm;->l:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3
.end method
