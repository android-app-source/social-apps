.class public LX/FeS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:LX/CzE;

.field public final b:LX/0bH;

.field public final c:LX/Feq;

.field public final d:LX/Fes;

.field public final e:LX/1B1;


# direct methods
.method public constructor <init>(LX/CzE;LX/0bH;LX/Feq;LX/Fes;)V
    .locals 3
    .param p1    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2265849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2265850
    iput-object p1, p0, LX/FeS;->a:LX/CzE;

    .line 2265851
    iput-object p2, p0, LX/FeS;->b:LX/0bH;

    .line 2265852
    iput-object p3, p0, LX/FeS;->c:LX/Feq;

    .line 2265853
    iput-object p4, p0, LX/FeS;->d:LX/Fes;

    .line 2265854
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, LX/FeS;->e:LX/1B1;

    .line 2265855
    iget-object v0, p0, LX/FeS;->e:LX/1B1;

    const/4 v1, 0x2

    new-array v1, v1, [LX/0b2;

    const/4 v2, 0x0

    iget-object p1, p0, LX/FeS;->c:LX/Feq;

    iget-object p2, p0, LX/FeS;->a:LX/CzE;

    .line 2265856
    new-instance p4, LX/Fep;

    invoke-static {p1}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object p3

    check-cast p3, LX/1K9;

    invoke-direct {p4, p2, p3}, LX/Fep;-><init>(LX/CzE;LX/1K9;)V

    .line 2265857
    move-object p1, p4

    .line 2265858
    aput-object p1, v1, v2

    const/4 v2, 0x1

    iget-object p1, p0, LX/FeS;->d:LX/Fes;

    iget-object p2, p0, LX/FeS;->a:LX/CzE;

    .line 2265859
    new-instance p4, LX/Fer;

    invoke-static {p1}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object p3

    check-cast p3, LX/1K9;

    invoke-direct {p4, p2, p3}, LX/Fer;-><init>(LX/CzE;LX/1K9;)V

    .line 2265860
    move-object p1, p4

    .line 2265861
    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, LX/1B1;->a([LX/0b2;)V

    .line 2265862
    return-void
.end method
