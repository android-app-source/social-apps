.class public abstract LX/H0j;
.super LX/1a1;
.source ""


# instance fields
.field public l:I

.field public final m:Lcom/facebook/widget/text/BetterEditTextView;

.field public final n:Landroid/widget/TextView;

.field public final o:Landroid/widget/TextView;

.field public p:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2414216
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2414217
    const v0, 0x7f0d1461

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, LX/H0j;->m:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2414218
    const v0, 0x7f0d1462

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/H0j;->n:Landroid/widget/TextView;

    .line 2414219
    const v0, 0x7f0d1463

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/H0j;->o:Landroid/widget/TextView;

    .line 2414220
    iget-object v0, p0, LX/H0j;->m:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, LX/H0o;

    invoke-direct {v1, p0}, LX/H0o;-><init>(LX/H0j;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2414221
    iget-object v0, p0, LX/H0j;->m:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, LX/H0p;

    invoke-direct {v1, p0}, LX/H0p;-><init>(LX/H0j;)V

    .line 2414222
    iput-object v1, v0, Lcom/facebook/widget/text/BetterEditTextView;->k:LX/62y;

    .line 2414223
    return-void
.end method


# virtual methods
.method public abstract c(I)Z
.end method

.method public final d(I)V
    .locals 6

    .prologue
    const/16 v5, 0x96

    .line 2414224
    if-le p1, v5, :cond_0

    .line 2414225
    iget-object v0, p0, LX/H0j;->o:Landroid/widget/TextView;

    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0094

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2414226
    :goto_0
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08152b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414227
    iget-object v1, p0, LX/H0j;->o:Landroid/widget/TextView;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2414228
    return-void

    .line 2414229
    :cond_0
    iget-object v0, p0, LX/H0j;->o:Landroid/widget/TextView;

    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
