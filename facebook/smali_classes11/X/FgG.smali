.class public LX/FgG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field public final b:LX/0Uh;

.field public final c:LX/Fg7;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EIz;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Uh;LX/Fg7;LX/0Ot;LX/0Ot;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/Fg7;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EIz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270044
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270045
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/FgG;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2270046
    iput-object p1, p0, LX/FgG;->a:Ljava/util/concurrent/ExecutorService;

    .line 2270047
    iput-object p2, p0, LX/FgG;->b:LX/0Uh;

    .line 2270048
    iput-object p3, p0, LX/FgG;->c:LX/Fg7;

    .line 2270049
    iput-object p4, p0, LX/FgG;->d:LX/0Ot;

    .line 2270050
    iput-object p5, p0, LX/FgG;->e:LX/0Ot;

    .line 2270051
    return-void
.end method
