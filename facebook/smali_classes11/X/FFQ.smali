.class public LX/FFQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2217430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2217431
    iput-object p1, p0, LX/FFQ;->a:LX/0tX;

    .line 2217432
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;
    .locals 7

    .prologue
    .line 2217433
    new-instance v3, LX/5d0;

    invoke-direct {v3}, LX/5d0;-><init>()V

    move-object v3, v3

    .line 2217434
    const-string v4, "id"

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2217435
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2217436
    iget-object v4, p0, LX/FFQ;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2217437
    const v1, -0x61f07314

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel;

    .line 2217438
    if-nez v0, :cond_0

    .line 2217439
    new-instance v0, Ljava/util/concurrent/ExecutionException;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unable to process GroupThreadQueueEnabledQueryModel."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 2217440
    :cond_0
    new-instance v1, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;

    invoke-virtual {v0}, Lcom/facebook/messaging/groups/graphql/GroupThreadQueueEnabledQueryModels$GroupThreadQueueEnabledQueryModel;->a()Z

    move-result v0

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;-><init>(Z)V

    return-object v1
.end method
