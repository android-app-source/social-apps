.class public final LX/FzV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;)V
    .locals 0

    .prologue
    .line 2308112
    iput-object p1, p0, LX/FzV;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2308097
    iget-object v0, p0, LX/FzV;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->p:LX/Fzo;

    iget-object v1, p0, LX/FzV;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    iget-object v1, v1, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    iget-object v2, p0, LX/FzV;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    invoke-virtual {v2}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->a()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 2308098
    iget-object v3, v0, LX/Fzo;->b:LX/Fzy;

    .line 2308099
    new-instance v4, LX/G0i;

    invoke-direct {v4}, LX/G0i;-><init>()V

    move-object v4, v4

    .line 2308100
    const-string v5, "id"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2308101
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    const-string v5, "FETCH_MEMORIAL_CONTACT_QUERY_TAG"

    invoke-static {v5}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v5

    .line 2308102
    iput-object v5, v4, LX/0zO;->d:Ljava/util/Set;

    .line 2308103
    move-object v4, v4

    .line 2308104
    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, v5}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v4

    sget-object v5, LX/0zS;->a:LX/0zS;

    invoke-virtual {v4, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v4

    .line 2308105
    iput-object v2, v4, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308106
    move-object v4, v4

    .line 2308107
    const-wide/16 v6, 0xe10

    invoke-virtual {v4, v6, v7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v4

    .line 2308108
    iget-object v5, v3, LX/Fzy;->c:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2308109
    new-instance v5, LX/Fzr;

    invoke-direct {v5, v3}, LX/Fzr;-><init>(LX/Fzy;)V

    iget-object v6, v3, LX/Fzy;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v3, v4

    .line 2308110
    move-object v0, v3

    .line 2308111
    return-object v0
.end method
