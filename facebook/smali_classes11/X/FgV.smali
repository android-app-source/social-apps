.class public LX/FgV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Qc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Qn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8cC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FZS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FhP;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:LX/Cvp;

.field public g:Z

.field public final h:Z

.field public final i:Z


# direct methods
.method public constructor <init>(LX/0ad;LX/Cvp;)V
    .locals 2
    .param p2    # LX/Cvp;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2270414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270415
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270416
    iput-object v0, p0, LX/FgV;->a:LX/0Ot;

    .line 2270417
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270418
    iput-object v0, p0, LX/FgV;->b:LX/0Ot;

    .line 2270419
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270420
    iput-object v0, p0, LX/FgV;->c:LX/0Ot;

    .line 2270421
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270422
    iput-object v0, p0, LX/FgV;->d:LX/0Ot;

    .line 2270423
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270424
    iput-object v0, p0, LX/FgV;->e:LX/0Ot;

    .line 2270425
    sget-short v0, LX/100;->aY:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/FgV;->h:Z

    .line 2270426
    sget-short v0, LX/100;->aZ:S

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/FgV;->i:Z

    .line 2270427
    iput-object p2, p0, LX/FgV;->f:LX/Cvp;

    .line 2270428
    return-void
.end method
