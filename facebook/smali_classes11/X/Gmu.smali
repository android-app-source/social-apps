.class public final LX/Gmu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ct5;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/1OS;

.field public final synthetic c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;ZLX/1OS;)V
    .locals 0

    .prologue
    .line 2392867
    iput-object p1, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    iput-boolean p2, p0, LX/Gmu;->a:Z

    iput-object p3, p0, LX/Gmu;->b:LX/1OS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2392868
    iget-boolean v0, p0, LX/Gmu;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gmu;->b:LX/1OS;

    invoke-interface {v0}, LX/1OS;->l()I

    move-result v0

    if-nez v0, :cond_1

    .line 2392869
    :cond_0
    :goto_0
    return-void

    .line 2392870
    :cond_1
    new-instance v2, LX/CsZ;

    iget-object v0, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, LX/CsZ;-><init>(Landroid/content/Context;)V

    .line 2392871
    iget-object v0, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ae:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v1, 0x7f0d011e

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    .line 2392872
    iget-object v1, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v1}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f020ce1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2392873
    invoke-virtual {v2, v1}, LX/CsZ;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2392874
    const v1, 0x7f02167d

    invoke-virtual {v2, v1}, LX/CsZ;->setImageResource(I)V

    .line 2392875
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v1}, LX/CsZ;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2392876
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    mul-int/lit8 v3, v0, 0x2

    invoke-direct {v1, v3, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2392877
    iget-object v0, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ae:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cju;

    const v3, 0x7f0d0123

    invoke-interface {v0, v3}, LX/Cju;->c(I)I

    move-result v3

    .line 2392878
    iget-object v0, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, LX/GmZ;->P()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b1288

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2392879
    :goto_1
    add-int/2addr v0, v3

    invoke-virtual {v1, v5, v0, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2392880
    const/4 v0, 0x1

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2392881
    iget-object v0, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    .line 2392882
    iget-object v3, v0, LX/Chc;->D:Landroid/view/View;

    move-object v3, v3

    .line 2392883
    move-object v0, v3

    .line 2392884
    const v3, 0x7f0d1679

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2392885
    invoke-virtual {v0, v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2392886
    new-instance v3, LX/IXA;

    iget-object v0, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ad:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    iget-object v1, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    .line 2392887
    iget-object v4, v1, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v4, v4

    .line 2392888
    move-object v1, v4

    .line 2392889
    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    check-cast v1, LX/1P1;

    invoke-direct {v3, v2, v0, v1}, LX/IXA;-><init>(LX/CsZ;LX/Chv;LX/1P1;)V

    .line 2392890
    new-instance v0, LX/Gmt;

    invoke-direct {v0, p0, v3}, LX/Gmt;-><init>(LX/Gmu;LX/IXA;)V

    invoke-virtual {v2, v0}, LX/CsZ;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2392891
    :cond_2
    iget-object v0, p0, LX/Gmu;->c:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b1287

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_1
.end method
