.class public final LX/GBj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/growth/model/DeviceOwnerData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GBk;


# direct methods
.method public constructor <init>(LX/GBk;)V
    .locals 0

    .prologue
    .line 2327475
    iput-object p1, p0, LX/GBj;->a:LX/GBk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2327476
    iget-object v0, p0, LX/GBj;->a:LX/GBk;

    iget-object v0, v0, LX/GBk;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327477
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2327478
    check-cast p1, Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2327479
    iget-object v0, p0, LX/GBj;->a:LX/GBk;

    iget-object v0, v0, LX/GBk;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    .line 2327480
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2327481
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2327482
    const-string v1, "email"

    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->c()LX/0Px;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2327483
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2327484
    const-string v1, "phone"

    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->d()LX/0Px;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2327485
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->b()LX/0Px;

    move-result-object v1

    .line 2327486
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2327487
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/model/FullName;

    .line 2327488
    invoke-virtual {v1}, Lcom/facebook/growth/model/FullName;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2327489
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2327490
    const-string v1, "name"

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2327491
    :cond_3
    const/4 v1, 0x0

    .line 2327492
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2327493
    :goto_1
    move-object v1, v1

    .line 2327494
    move-object v1, v1

    .line 2327495
    iput-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->B:Ljava/lang/String;

    .line 2327496
    iget-object v0, p0, LX/GBj;->a:LX/GBk;

    iget-object v0, v0, LX/GBk;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->v(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    .line 2327497
    return-void

    .line 2327498
    :catch_0
    move-exception v3

    .line 2327499
    sget-object v4, Lcom/facebook/account/recovery/common/protocol/AccountRecoverySearchAccountMethodParams;->a:Ljava/lang/Class;

    const-string p1, "jsonEncode search assisted data failed"

    invoke-static {v4, p1, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
