.class public LX/Foq;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Landroid/view/LayoutInflater;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Fon;",
            ">;"
        }
    .end annotation
.end field

.field public d:Landroid/view/View$OnClickListener;

.field public e:Landroid/view/View$OnClickListener;

.field private f:LX/Fon;

.field private final g:LX/Fon;

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2290385
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2290386
    const/4 v0, 0x0

    iput-object v0, p0, LX/Foq;->f:LX/Fon;

    .line 2290387
    new-instance v0, LX/Fon;

    sget-object v1, LX/Foo;->LOADER:LX/Foo;

    invoke-direct {v0, v1}, LX/Fon;-><init>(LX/Foo;)V

    iput-object v0, p0, LX/Foq;->g:LX/Fon;

    .line 2290388
    iput-boolean v2, p0, LX/Foq;->h:Z

    .line 2290389
    iput-boolean v2, p0, LX/Foq;->i:Z

    .line 2290390
    return-void
.end method

.method private a(I)LX/Fon;
    .locals 1

    .prologue
    .line 2290382
    iget-object v0, p0, LX/Foq;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2290383
    iget-object v0, p0, LX/Foq;->g:LX/Fon;

    .line 2290384
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Foq;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fon;

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/facebook/fig/listitem/FigListItem;
    .locals 3

    .prologue
    .line 2290379
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/fig/listitem/FigListItem;

    if-nez v0, :cond_1

    .line 2290380
    :cond_0
    iget-object v0, p0, LX/Foq;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030761

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2290381
    :goto_0
    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2290374
    iget-object v0, p0, LX/Foq;->f:LX/Fon;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Foq;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Foq;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Foq;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fon;

    .line 2290375
    iget-object v1, v0, LX/Fon;->a:LX/Foo;

    move-object v0, v1

    .line 2290376
    sget-object v1, LX/Foo;->CHARITY:LX/Foo;

    if-ne v0, v1, :cond_0

    .line 2290377
    iget-object v0, p0, LX/Foq;->c:Ljava/util/ArrayList;

    iget-object v1, p0, LX/Foq;->f:LX/Fon;

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2290378
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;)V
    .locals 2

    .prologue
    .line 2290370
    new-instance v0, LX/Fon;

    sget-object v1, LX/Foo;->DAF_DISCLOSURE:LX/Foo;

    invoke-direct {v0, v1, p1}, LX/Fon;-><init>(LX/Foo;Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;)V

    iput-object v0, p0, LX/Foq;->f:LX/Fon;

    .line 2290371
    invoke-direct {p0}, LX/Foq;->c()V

    .line 2290372
    const v0, -0x1959299a

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2290373
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/Fon;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2290289
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Foq;->a(Ljava/util/ArrayList;Z)V

    .line 2290290
    return-void
.end method

.method public final a(Ljava/util/ArrayList;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/Fon;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2290365
    iput-object p1, p0, LX/Foq;->c:Ljava/util/ArrayList;

    .line 2290366
    iput-boolean p2, p0, LX/Foq;->h:Z

    .line 2290367
    invoke-direct {p0}, LX/Foq;->c()V

    .line 2290368
    const v0, 0x61aa1662

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2290369
    return-void
.end method

.method public final getCount()I
    .locals 3

    .prologue
    .line 2290358
    iget-boolean v0, p0, LX/Foq;->h:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 2290359
    iget-object v1, p0, LX/Foq;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Foq;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2290360
    sget-object v1, LX/Foo;->CHARITY:LX/Foo;

    iget-object v2, p0, LX/Foq;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fon;

    .line 2290361
    iget-object v2, v0, LX/Fon;->a:LX/Foo;

    move-object v0, v2

    .line 2290362
    invoke-virtual {v1, v0}, LX/Foo;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2290363
    :cond_0
    move v0, v0

    .line 2290364
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Foq;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/Foq;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2290357
    invoke-direct {p0, p1}, LX/Foq;->a(I)LX/Fon;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2290356
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2290291
    invoke-direct {p0, p1}, LX/Foq;->a(I)LX/Fon;

    move-result-object v1

    .line 2290292
    sget-object v0, LX/Foo;->LOADER:LX/Foo;

    .line 2290293
    iget-object v2, v1, LX/Fon;->a:LX/Foo;

    move-object v2, v2

    .line 2290294
    invoke-virtual {v0, v2}, LX/Foo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2290295
    instance-of v0, p2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_4

    check-cast p2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2290296
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2290297
    move-object v0, p2

    .line 2290298
    :goto_1
    return-object v0

    .line 2290299
    :cond_0
    sget-object v0, LX/Fop;->a:[I

    .line 2290300
    iget-object v2, v1, LX/Fon;->a:LX/Foo;

    move-object v2, v2

    .line 2290301
    invoke-virtual {v2}, LX/Foo;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2290302
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized row type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2290303
    iget-object v3, v1, LX/Fon;->a:LX/Foo;

    move-object v1, v3

    .line 2290304
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2290305
    :pswitch_0
    invoke-direct {p0, p2, p3}, LX/Foq;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/facebook/fig/listitem/FigListItem;

    move-result-object v0

    .line 2290306
    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    const/4 v3, 0x0

    .line 2290307
    invoke-virtual {v1}, LX/Fon;->b()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, LX/Fon;->b()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2290308
    invoke-virtual {v1}, LX/Fon;->b()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2290309
    :cond_1
    iget-object v2, p0, LX/Foq;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2290310
    invoke-virtual {v1}, LX/Fon;->b()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTag(Ljava/lang/Object;)V

    .line 2290311
    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2290312
    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2290313
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2290314
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2290315
    move-object v0, v0

    .line 2290316
    goto :goto_1

    .line 2290317
    :pswitch_1
    invoke-direct {p0, p2, p3}, LX/Foq;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/facebook/fig/listitem/FigListItem;

    move-result-object v0

    .line 2290318
    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2290319
    iget-object v2, v1, LX/Fon;->a:LX/Foo;

    sget-object v5, LX/Foo;->CHARITY:LX/Foo;

    if-ne v2, v5, :cond_a

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2290320
    iget-object v2, v1, LX/Fon;->c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    move-object v5, v2

    .line 2290321
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v2

    if-nez v2, :cond_5

    .line 2290322
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2290323
    :goto_3
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2290324
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_7

    .line 2290325
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v2

    iget-object p1, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2290326
    invoke-virtual {p1, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    move v2, v3

    :goto_4
    if-eqz v2, :cond_8

    .line 2290327
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v2

    iget-object p1, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {p1, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2290328
    :goto_5
    iget-boolean v2, p0, LX/Foq;->i:Z

    if-eqz v2, :cond_2

    .line 2290329
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2290330
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 2290331
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2290332
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2290333
    :goto_6
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2290334
    invoke-virtual {v0, v5}, Lcom/facebook/fig/listitem/FigListItem;->setTag(Ljava/lang/Object;)V

    .line 2290335
    iget-object v2, p0, LX/Foq;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2290336
    move-object v0, v0

    .line 2290337
    goto/16 :goto_1

    .line 2290338
    :pswitch_2
    iget-object v0, p0, LX/Foq;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f030742

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2290339
    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v4, 0x8

    .line 2290340
    iget-object v2, v1, LX/Fon;->a:LX/Foo;

    sget-object v3, LX/Foo;->DAF_DISCLOSURE:LX/Foo;

    if-ne v2, v3, :cond_c

    const/4 v2, 0x1

    :goto_7
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2290341
    iget-object v2, v1, LX/Fon;->d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    move-object v2, v2

    .line 2290342
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2290343
    :cond_3
    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 2290344
    :goto_8
    move-object v0, v0

    .line 2290345
    goto/16 :goto_1

    .line 2290346
    :cond_4
    new-instance p2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v0, p0, LX/Foq;->b:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2290347
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->a()Z

    move-result p1

    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->j()Z

    move-result p2

    iget-object p3, p0, LX/Foq;->b:Landroid/content/Context;

    invoke-static {v2, p1, p2, p3}, LX/FkG;->a(Ljava/lang/CharSequence;ZZLandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_6
    move v2, v4

    .line 2290348
    goto/16 :goto_4

    :cond_7
    move v2, v4

    goto/16 :goto_4

    .line 2290349
    :cond_8
    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 2290350
    :cond_9
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_6

    .line 2290351
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2290352
    :cond_b
    :try_start_0
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V

    .line 2290353
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_8

    .line 2290354
    :catch_0
    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_8

    .line 2290355
    :cond_c
    const/4 v2, 0x0

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
