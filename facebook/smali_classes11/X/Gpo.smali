.class public LX/Gpo;
.super LX/Cne;
.source ""


# instance fields
.field public g:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/GoE;

.field private j:LX/Gqh;


# direct methods
.method public constructor <init>(LX/Cow;)V
    .locals 2

    .prologue
    .line 2395842
    invoke-direct {p0, p1}, LX/Cne;-><init>(LX/Cow;)V

    .line 2395843
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Gpo;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object p1

    check-cast p1, LX/Go7;

    invoke-static {v0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v0

    check-cast v0, LX/Go4;

    iput-object p1, p0, LX/Gpo;->g:LX/Go7;

    iput-object v0, p0, LX/Gpo;->h:LX/Go4;

    .line 2395844
    return-void
.end method


# virtual methods
.method public bridge synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2395845
    check-cast p1, LX/Clw;

    invoke-virtual {p0, p1}, LX/Cne;->a(LX/Clw;)V

    return-void
.end method

.method public a(LX/Clw;)V
    .locals 12

    .prologue
    .line 2395846
    check-cast p1, LX/Goo;

    .line 2395847
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395848
    check-cast v0, LX/Gqh;

    iput-object v0, p0, LX/Gpo;->j:LX/Gqh;

    .line 2395849
    invoke-interface {p1}, LX/Goc;->p()Z

    move-result v0

    .line 2395850
    iget-object v1, p0, LX/Gpo;->j:LX/Gqh;

    .line 2395851
    iput-boolean v0, v1, LX/Gqh;->w:Z

    .line 2395852
    invoke-super {p0, p1}, LX/Cne;->a(LX/Clw;)V

    .line 2395853
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395854
    instance-of v0, v0, LX/Gqh;

    if-eqz v0, :cond_0

    instance-of v0, p1, LX/Gog;

    if-nez v0, :cond_1

    .line 2395855
    :cond_0
    :goto_0
    return-void

    .line 2395856
    :cond_1
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v0

    iput-object v0, p0, LX/Gpo;->i:LX/GoE;

    .line 2395857
    iget-object v0, p0, LX/Gpo;->j:LX/Gqh;

    invoke-interface {p1}, LX/GoL;->getAction()LX/CHX;

    move-result-object v1

    invoke-interface {p1}, LX/Gog;->ly_()Z

    move-result v2

    iget-object v3, p0, LX/Gpo;->i:LX/GoE;

    .line 2395858
    invoke-virtual {v0, v3}, LX/Gqh;->a(LX/GoE;)V

    .line 2395859
    if-eqz v1, :cond_2

    .line 2395860
    iget-object v4, v0, LX/Gqh;->r:LX/Grc;

    sget-object v5, LX/Grb;->LINK:LX/Grb;

    invoke-virtual {v4, v5}, LX/Grc;->a(LX/Grb;)V

    .line 2395861
    invoke-interface {v1}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, LX/Gqh;->v:Ljava/lang/String;

    .line 2395862
    :cond_2
    if-eqz v2, :cond_3

    .line 2395863
    iget-object v4, v0, LX/Gqh;->r:LX/Grc;

    sget-object v5, LX/Grb;->EXPANDABLE:LX/Grb;

    invoke-virtual {v4, v5}, LX/Grc;->a(LX/Grb;)V

    .line 2395864
    :cond_3
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v4

    check-cast v4, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    new-instance v5, LX/Gqg;

    invoke-direct {v5, v0, v2, v3, v1}, LX/Gqg;-><init>(LX/Gqh;ZLX/GoE;LX/CHX;)V

    invoke-virtual {v4, v5}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395865
    instance-of v0, p1, LX/Goi;

    if-eqz v0, :cond_4

    .line 2395866
    iget-object v0, p0, LX/Gpo;->j:LX/Gqh;

    invoke-interface {p1}, LX/Goi;->lz_()Z

    move-result v1

    .line 2395867
    if-eqz v1, :cond_4

    .line 2395868
    iget-object v2, v0, LX/Gqh;->t:LX/GrH;

    .line 2395869
    iget-object v0, v2, LX/GrH;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2395870
    :cond_4
    instance-of v0, p1, LX/Goj;

    if-eqz v0, :cond_7

    .line 2395871
    iget-object v0, p0, LX/Gpo;->j:LX/Gqh;

    invoke-interface {p1}, LX/Gon;->K()Ljava/util/List;

    move-result-object v1

    .line 2395872
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;

    .line 2395873
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2395874
    iget-object v3, v0, LX/Gqh;->u:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-virtual {v3, v2}, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;)V

    .line 2395875
    :cond_6
    iget-object v0, p0, LX/Gpo;->j:LX/Gqh;

    invoke-interface {p1}, LX/Goj;->lA_()Z

    move-result v1

    .line 2395876
    if-eqz v1, :cond_7

    .line 2395877
    iget-object v2, v0, LX/Gqh;->u:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->a()V

    .line 2395878
    :cond_7
    invoke-interface {p1}, LX/Goa;->C()I

    move-result v0

    .line 2395879
    iget-object v1, p0, LX/Gpo;->j:LX/Gqh;

    .line 2395880
    iget-object v2, v1, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 2395881
    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    instance-of v2, v2, LX/Gqu;

    if-eqz v2, :cond_8

    .line 2395882
    iget-object v2, v1, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 2395883
    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    check-cast v2, LX/Gqu;

    .line 2395884
    const/4 v3, 0x4

    move v3, v3

    .line 2395885
    invoke-static {v0}, LX/CIY;->a(I)I

    move-result v1

    div-int/2addr v3, v1

    iput v3, v2, LX/Gqu;->a:I

    .line 2395886
    :cond_8
    iget-object v0, p0, LX/Gpo;->j:LX/Gqh;

    invoke-interface {p1}, LX/Gom;->N()Z

    move-result v1

    .line 2395887
    iget-object v2, v0, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 2395888
    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    instance-of v2, v2, LX/Gqv;

    if-eqz v2, :cond_9

    .line 2395889
    iget-object v2, v0, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 2395890
    invoke-interface {v2}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v2

    check-cast v2, LX/Gqv;

    .line 2395891
    iput-boolean v1, v2, LX/Gqv;->a:Z

    .line 2395892
    :cond_9
    iget-object v0, p0, LX/Gpo;->j:LX/Gqh;

    iget-object v1, p0, LX/Gpo;->i:LX/GoE;

    invoke-virtual {v0, v1}, LX/Gqh;->a(LX/GoE;)V

    .line 2395893
    invoke-interface {p1}, LX/God;->lB_()Z

    move-result v0

    .line 2395894
    iget-object v1, p0, LX/Gpo;->j:LX/Gqh;

    .line 2395895
    if-nez v0, :cond_a

    .line 2395896
    iget-object v2, v1, LX/Gqh;->r:LX/Grc;

    sget-object v3, LX/Grb;->HIDDEN:LX/Grb;

    invoke-virtual {v2, v3}, LX/Grc;->a(LX/Grb;)V

    .line 2395897
    :cond_a
    invoke-interface {p1}, LX/Goe;->w()Ljava/lang/String;

    move-result-object v0

    .line 2395898
    iget-object v1, p0, LX/Cne;->d:LX/1Fb;

    if-eqz v1, :cond_0

    .line 2395899
    iget-object v1, p0, LX/Gpo;->j:LX/Gqh;

    iget-object v2, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v2}, LX/1Fb;->c()I

    move-result v2

    iget-object v3, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v3}, LX/1Fb;->a()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LX/Cow;->a(Ljava/lang/String;II)V

    .line 2395900
    iget-object v0, p0, LX/Gpo;->j:LX/Gqh;

    invoke-interface {p1}, LX/Gok;->lF_()LX/2uF;

    move-result-object v1

    iget-object v2, p0, LX/Gpo;->i:LX/GoE;

    .line 2395901
    iget-object v3, v0, LX/Gqh;->o:LX/0ad;

    sget-short v4, LX/CHN;->v:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2395902
    iget-object v3, v0, LX/Cos;->a:LX/Ctg;

    move-object v3, v3

    .line 2395903
    const/4 p1, 0x0

    .line 2395904
    if-eqz v1, :cond_b

    invoke-virtual {v1}, LX/39O;->a()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2395905
    :cond_b
    :goto_1
    goto/16 :goto_0

    .line 2395906
    :cond_c
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 2395907
    iget-object v4, v0, LX/Gqh;->q:Ljava/util/List;

    if-eqz v4, :cond_d

    iget-object v4, v0, LX/Gqh;->q:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2395908
    :cond_d
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, LX/Gqh;->q:Ljava/util/List;

    .line 2395909
    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v8, v4, LX/1vs;->b:I

    .line 2395910
    const v4, 0x7f03097b

    .line 2395911
    iget-object v9, v0, LX/Cos;->a:LX/Ctg;

    move-object v9, v9

    .line 2395912
    invoke-interface {v9}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v9

    invoke-virtual {v6, v4, v9, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    .line 2395913
    invoke-interface {v3}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2395914
    iget-object v9, v0, LX/Gqh;->q:Ljava/util/List;

    new-instance v10, LX/GrY;

    const/4 v11, 0x1

    invoke-virtual {v5, v8, v11}, LX/15i;->g(II)I

    move-result v11

    invoke-static {v5, v11}, LX/Gqh;->a(LX/15i;I)Landroid/graphics/RectF;

    move-result-object v11

    const-class p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-virtual {v5, v8, p1, p0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, LX/CHX;

    invoke-direct {v10, v4, v11, v5, v2}, LX/GrY;-><init>(Lcom/facebook/instantshopping/view/widget/TouchTargetView;Landroid/graphics/RectF;LX/CHX;LX/GoE;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2395915
    :cond_e
    new-instance v4, LX/GrZ;

    iget-object v5, v0, LX/Gqh;->q:Ljava/util/List;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, v0, LX/Gqh;->m:LX/GnF;

    invoke-direct {v4, v3, v5, v6, v7}, LX/GrZ;-><init>(LX/Ctg;Ljava/util/List;Landroid/content/Context;LX/GnF;)V

    iput-object v4, v0, LX/Gqh;->y:LX/GrZ;

    .line 2395916
    iget-object v4, v0, LX/Gqh;->y:LX/GrZ;

    invoke-virtual {v0, v4}, LX/Cos;->a(LX/Ctr;)V

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395825
    iget-object v0, p0, LX/Gpo;->j:LX/Gqh;

    const-string v1, "isTiltToPanGlyphShown"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2395826
    invoke-static {v0}, LX/Gqh;->p(LX/Gqh;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    if-eqz v2, :cond_0

    .line 2395827
    iget-object v2, v0, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    .line 2395828
    iput-boolean v1, v2, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->f:Z

    .line 2395829
    :cond_0
    invoke-super {p0, p1}, LX/Cne;->a(Landroid/os/Bundle;)V

    .line 2395830
    iget-object v0, p0, LX/Gpo;->g:LX/Go7;

    const-string v1, "image_element_start"

    iget-object v2, p0, LX/Gpo;->i:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395831
    iget-object v0, p0, LX/Gpo;->h:LX/Go4;

    iget-object v1, p0, LX/Gpo;->i:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2395832
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395833
    invoke-super {p0, p1}, LX/Cne;->b(Landroid/os/Bundle;)V

    .line 2395834
    iget-object v0, p0, LX/Gpo;->g:LX/Go7;

    const-string v1, "image_element_end"

    iget-object v2, p0, LX/Gpo;->i:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395835
    iget-object v0, p0, LX/Gpo;->h:LX/Go4;

    iget-object v1, p0, LX/Gpo;->i:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2395836
    const-string v0, "isTiltToPanGlyphShown"

    iget-object v1, p0, LX/Gpo;->j:LX/Gqh;

    .line 2395837
    iget-object v2, v1, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    .line 2395838
    iget-boolean v1, v2, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->f:Z

    move v2, v1

    .line 2395839
    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2395840
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2395841
    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
