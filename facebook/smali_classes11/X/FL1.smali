.class public LX/FL1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3Ed;


# direct methods
.method public constructor <init>(LX/3Ed;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225936
    iput-object p1, p0, LX/FL1;->a:LX/3Ed;

    .line 2225937
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2225938
    check-cast p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2225939
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->n:Z

    move v0, v0

    .line 2225940
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2225941
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2225942
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "offline_threading_id"

    iget-object v3, p0, LX/FL1;->a:LX/3Ed;

    invoke-virtual {v3}, LX/3Ed;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225943
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "thread_key"

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/ModifyThreadParams;->u()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225944
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "new_thread_ttl"

    .line 2225945
    iget v3, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->o:I

    move v3, v3

    .line 2225946
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225947
    iget v1, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->o:I

    move v1, v1

    .line 2225948
    if-nez v1, :cond_0

    .line 2225949
    iget v1, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->p:I

    move v1, v1

    .line 2225950
    :goto_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "xmat_ttl"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 2225951
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225952
    const-string v1, "%s/ephemeral_thread_modes"

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/ModifyThreadParams;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/DoA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2225953
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "setThreadEphemeralMode"

    .line 2225954
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2225955
    move-object v2, v2

    .line 2225956
    const-string v3, "POST"

    .line 2225957
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2225958
    move-object v2, v2

    .line 2225959
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 2225960
    move-object v1, v2

    .line 2225961
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2225962
    move-object v0, v1

    .line 2225963
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2225964
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225965
    move-object v0, v0

    .line 2225966
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2225967
    :cond_0
    iget v1, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->o:I

    move v1, v1

    .line 2225968
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225969
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225970
    const/4 v0, 0x0

    return-object v0
.end method
