.class public abstract LX/GNw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/0Vd",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V
    .locals 0

    .prologue
    .line 2346661
    iput-object p1, p0, LX/GNw;->d:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 2346662
    invoke-super {p0, p1}, LX/0Vd;->onCancel(Ljava/util/concurrent/CancellationException;)V

    .line 2346663
    iget-object v0, p0, LX/GNw;->d:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2346664
    return-void
.end method

.method public onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2346665
    iget-object v0, p0, LX/GNw;->d:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2346666
    iget-object v0, p0, LX/GNw;->d:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

    .line 2346667
    const v1, 0x7f080015

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08003e

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    .line 2346668
    iget-object v0, p0, LX/GNw;->d:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

    invoke-virtual {v0, p1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Ljava/lang/Throwable;)V

    .line 2346669
    return-void
.end method
