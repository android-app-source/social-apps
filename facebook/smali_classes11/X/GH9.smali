.class public final LX/GH9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/4BY;

.field public final synthetic c:LX/GHB;


# direct methods
.method public constructor <init>(LX/GHB;Landroid/content/Context;LX/4BY;)V
    .locals 0

    .prologue
    .line 2334795
    iput-object p1, p0, LX/GH9;->c:LX/GHB;

    iput-object p2, p0, LX/GH9;->a:Landroid/content/Context;

    iput-object p3, p0, LX/GH9;->b:LX/4BY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2334809
    iget-object v0, p0, LX/GH9;->c:LX/GHB;

    iget-object v1, p0, LX/GH9;->a:Landroid/content/Context;

    invoke-static {v0, v1}, LX/GHB;->a(LX/GHB;Landroid/content/Context;)V

    .line 2334810
    iget-object v0, p0, LX/GH9;->b:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2334811
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2334796
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2334797
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2334798
    check-cast v0, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;

    .line 2334799
    if-eqz v0, :cond_0

    .line 2334800
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel;->a()Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;

    move-result-object v0

    .line 2334801
    if-eqz v0, :cond_0

    .line 2334802
    iget-object v1, p0, LX/GH9;->c:LX/GHB;

    iget-object v1, v1, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334803
    iget-object p1, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, p1

    .line 2334804
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferCreateMutationModels$LWIOfferCreateMutationModel$OfferModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2334805
    iput-object v0, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->j:Ljava/lang/String;

    .line 2334806
    :cond_0
    iget-object v0, p0, LX/GH9;->c:LX/GHB;

    iget-object v1, p0, LX/GH9;->a:Landroid/content/Context;

    invoke-static {v0, v1}, LX/GHB;->a(LX/GHB;Landroid/content/Context;)V

    .line 2334807
    iget-object v0, p0, LX/GH9;->b:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2334808
    return-void
.end method
