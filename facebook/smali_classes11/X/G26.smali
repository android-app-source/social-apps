.class public final LX/G26;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

.field public final synthetic b:Lcom/facebook/timeline/protiles/model/ProtileModel;

.field public final synthetic c:LX/1Fb;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Fb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2313491
    iput-object p1, p0, LX/G26;->f:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    iput-object p2, p0, LX/G26;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    iput-object p3, p0, LX/G26;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    iput-object p4, p0, LX/G26;->c:LX/1Fb;

    iput-object p5, p0, LX/G26;->d:Ljava/lang/String;

    iput-object p6, p0, LX/G26;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x2

    const v1, -0x1c4af64a

    invoke-static {v0, v6, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2313492
    iget-object v0, p0, LX/G26;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 2313493
    sget-object v0, LX/G27;->a:[I

    iget-object v1, p0, LX/G26;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2313494
    :goto_0
    iget-object v0, p0, LX/G26;->f:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->b:LX/1K9;

    iget-object v1, p0, LX/G26;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    iget-object v2, p0, LX/G26;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-static {v1, v2}, LX/G1D;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)LX/G1D;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 2313495
    const v0, 0x6bc52b1c

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    .line 2313496
    :pswitch_0
    instance-of v8, p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v8}, LX/0PB;->checkArgument(Z)V

    .line 2313497
    iget-object v8, p0, LX/G26;->f:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    iget-object v8, v8, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->c:LX/G2N;

    move-object v9, p1

    check-cast v9, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v11, p0, LX/G26;->c:LX/1Fb;

    iget-object v12, p0, LX/G26;->e:Ljava/lang/String;

    iget-object v13, p0, LX/G26;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    move-object v10, v2

    invoke-virtual/range {v8 .. v13}, LX/G2N;->a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;LX/1Fb;Ljava/lang/String;Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    .line 2313498
    goto :goto_0

    .line 2313499
    :pswitch_1
    iget-object v0, p0, LX/G26;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G26;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->q()I

    move-result v0

    if-lez v0, :cond_0

    .line 2313500
    :goto_1
    iget-object v0, p0, LX/G26;->f:Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/protiles/rows/ProtilesGridPhotoHeaderPartDefinition;->c:LX/G2N;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, LX/G26;->c:LX/1Fb;

    iget-object v4, p0, LX/G26;->d:Ljava/lang/String;

    iget-object v5, p0, LX/G26;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v5}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    .line 2313501
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2313502
    new-instance v9, LX/5vi;

    invoke-direct {v9}, LX/5vi;-><init>()V

    .line 2313503
    iput-object v2, v9, LX/5vi;->c:Ljava/lang/String;

    .line 2313504
    move-object v9, v9

    .line 2313505
    iput-object v4, v9, LX/5vi;->d:Ljava/lang/String;

    .line 2313506
    move-object v9, v9

    .line 2313507
    invoke-static {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v10

    .line 2313508
    iput-object v10, v9, LX/5vi;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2313509
    move-object v9, v9

    .line 2313510
    iput-object v5, v9, LX/5vi;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2313511
    move-object v9, v9

    .line 2313512
    invoke-virtual {v9}, LX/5vi;->a()Lcom/facebook/timeline/intent/ModelBundleGraphQLModels$ModelBundleProfileGraphQLModel;

    move-result-object v9

    .line 2313513
    invoke-static {v8, v9}, LX/5ve;->a(Landroid/os/Bundle;LX/36O;)V

    .line 2313514
    const-string v9, "timeline_has_unseen_section"

    invoke-virtual {v8, v9, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2313515
    sget-object v9, LX/0ax;->bE:Ljava/lang/String;

    invoke-static {v9, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 2313516
    iget-object v10, v0, LX/G2N;->a:LX/17W;

    invoke-virtual {v10, v1, v9, v8}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2313517
    goto/16 :goto_0

    .line 2313518
    :cond_0
    const/4 v6, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
