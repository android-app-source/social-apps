.class public final LX/FuY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fue;


# direct methods
.method public constructor <init>(LX/Fue;)V
    .locals 0

    .prologue
    .line 2300123
    iput-object p1, p0, LX/FuY;->a:LX/Fue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x4ac25835

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2300124
    iget-object v0, p0, LX/FuY;->a:LX/Fue;

    iget-object v0, v0, LX/Fue;->k:LX/Fva;

    if-eqz v0, :cond_0

    .line 2300125
    iget-object v0, p0, LX/FuY;->a:LX/Fue;

    iget-object v0, v0, LX/Fue;->k:LX/Fva;

    .line 2300126
    iget-object v2, v0, LX/Fva;->a:LX/Fvi;

    .line 2300127
    iget-object v3, v2, LX/Fvi;->k:LX/Fw7;

    .line 2300128
    sget-object v0, LX/Fve;->EXPANDED:LX/Fve;

    iput-object v0, v3, LX/Fw7;->b:LX/Fve;

    .line 2300129
    iget-object v3, v2, LX/Fvi;->t:LX/Fvw;

    invoke-virtual {v3}, LX/Fvw;->b()V

    .line 2300130
    const v3, -0x2beb8818

    invoke-static {v2, v3}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2300131
    :cond_0
    iget-object v0, p0, LX/FuY;->a:LX/Fue;

    iget-object v0, v0, LX/Fue;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    iget-object v2, p0, LX/FuY;->a:LX/Fue;

    iget-object v2, v2, LX/Fue;->o:LX/5SB;

    .line 2300132
    iget-wide v8, v2, LX/5SB;->b:J

    move-wide v2, v8

    .line 2300133
    iget-object v4, p0, LX/FuY;->a:LX/Fue;

    iget-object v4, v4, LX/Fue;->o:LX/5SB;

    invoke-virtual {v4}, LX/5SB;->i()Z

    move-result v4

    iget-object v5, p0, LX/FuY;->a:LX/Fue;

    iget-object v5, v5, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    iget-object v6, p0, LX/FuY;->a:LX/Fue;

    iget-object v6, v6, LX/Fue;->m:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LX/BQ9;->c(JLX/9lQ;)V

    .line 2300134
    const v0, 0x7d85e450

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
