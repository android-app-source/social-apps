.class public final LX/Gc5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field public final synthetic c:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;Landroid/view/View;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 0

    .prologue
    .line 2371027
    iput-object p1, p0, LX/Gc5;->c:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iput-object p2, p0, LX/Gc5;->a:Landroid/view/View;

    iput-object p3, p0, LX/Gc5;->b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2371028
    iget-object v0, p0, LX/Gc5;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2371029
    iget-object v0, p0, LX/Gc5;->a:Landroid/view/View;

    invoke-static {v0, p0}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_0
    move v1, v2

    .line 2371030
    :goto_0
    iget-object v0, p0, LX/Gc5;->c:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v0}, LX/Gbz;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2371031
    iget-object v0, p0, LX/Gc5;->c:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v0, v1}, LX/Gbz;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2371032
    iget-object v3, p0, LX/Gc5;->c:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v3, v3, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2371033
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget-object v3, p0, LX/Gc5;->b:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v3, v3, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2371034
    iget-object v0, p0, LX/Gc5;->c:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2371035
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2371036
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 2371037
    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2371038
    iget-object v4, p0, LX/Gc5;->c:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    invoke-static {v4, v0, v1, v3}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;->a$redex0(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;Landroid/view/View;I[I)V

    .line 2371039
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2371040
    :cond_2
    return-void
.end method
