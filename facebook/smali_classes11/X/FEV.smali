.class public final LX/FEV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GamesListQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FEc;

.field public final synthetic b:LX/FEW;


# direct methods
.method public constructor <init>(LX/FEW;LX/FEc;)V
    .locals 0

    .prologue
    .line 2216368
    iput-object p1, p0, LX/FEV;->b:LX/FEW;

    iput-object p2, p0, LX/FEV;->a:LX/FEc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2216391
    iget-object v0, p0, LX/FEV;->a:LX/FEc;

    invoke-virtual {v0}, LX/FEc;->a()V

    .line 2216392
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2216369
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2216370
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2216371
    if-eqz v0, :cond_1

    .line 2216372
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2216373
    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GamesListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GamesListQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2216374
    iget-object v1, p0, LX/FEV;->a:LX/FEc;

    .line 2216375
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2216376
    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GamesListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$GamesListQueryModel;->a()LX/0Px;

    move-result-object v0

    .line 2216377
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    .line 2216378
    iget-object p0, v1, LX/FEc;->a:Ljava/lang/String;

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    .line 2216379
    if-eqz v2, :cond_0

    if-eqz p0, :cond_0

    .line 2216380
    iget-object p1, v1, LX/FEc;->b:LX/FEe;

    iget-object p1, p1, LX/FEe;->d:LX/FEX;

    invoke-virtual {p1}, LX/FEX;->a()V

    .line 2216381
    :cond_0
    if-eqz v2, :cond_2

    if-nez p0, :cond_2

    .line 2216382
    iget-object v2, v1, LX/FEc;->b:LX/FEe;

    iget-object v2, v2, LX/FEe;->d:LX/FEX;

    invoke-virtual {v2}, LX/FEX;->c()V

    .line 2216383
    :goto_0
    iget-object v2, v1, LX/FEc;->b:LX/FEe;

    iget-object v2, v2, LX/FEe;->a:Lcom/facebook/messaging/games/GamesSelectionAdapter;

    iget-object p0, v1, LX/FEc;->a:Ljava/lang/String;

    .line 2216384
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object p1

    iput-object p1, v2, Lcom/facebook/messaging/games/GamesSelectionAdapter;->b:Ljava/util/List;

    .line 2216385
    iput-object p0, v2, Lcom/facebook/messaging/games/GamesSelectionAdapter;->d:Ljava/lang/String;

    .line 2216386
    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2216387
    iget-object v2, v1, LX/FEc;->b:LX/FEe;

    iget-object v2, v2, LX/FEe;->d:LX/FEX;

    invoke-virtual {v2}, LX/FEX;->b()V

    .line 2216388
    :goto_1
    return-void

    .line 2216389
    :cond_1
    iget-object v0, p0, LX/FEV;->a:LX/FEc;

    invoke-virtual {v0}, LX/FEc;->a()V

    goto :goto_1

    .line 2216390
    :cond_2
    iget-object v2, v1, LX/FEc;->b:LX/FEe;

    iget-object v2, v2, LX/FEe;->d:LX/FEX;

    invoke-virtual {v2}, LX/FEX;->d()V

    goto :goto_0
.end method
