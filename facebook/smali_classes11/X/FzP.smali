.class public LX/FzP;
.super LX/FzJ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FzJ",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$ProfileInfoTypeaheadSearchQueryModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0zT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2307869
    const-string v0, "typeahead_session_id"

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, LX/FzP;->b:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(LX/9kE;LX/0tX;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2307870
    invoke-direct {p0, p1, p2}, LX/FzJ;-><init>(LX/9kE;LX/0tX;)V

    .line 2307871
    new-instance v0, LX/FzN;

    invoke-direct {v0, p0}, LX/FzN;-><init>(LX/FzP;)V

    iput-object v0, p0, LX/FzP;->a:LX/0zT;

    .line 2307872
    return-void
.end method
