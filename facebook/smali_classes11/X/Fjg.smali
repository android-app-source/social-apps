.class public LX/Fjg;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final A:LX/0Tn;

.field public static final B:LX/0Tn;

.field public static final C:LX/0Tn;

.field public static final D:LX/0Tn;

.field public static final E:LX/0Tn;

.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;

.field public static final w:LX/0Tn;

.field public static final x:LX/0Tn;

.field public static final y:LX/0Tn;

.field public static final z:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2277425
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "selfupdate/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2277426
    sput-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "scheduled_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->b:LX/0Tn;

    .line 2277427
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "schedule_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->c:LX/0Tn;

    .line 2277428
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "new_build"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->d:LX/0Tn;

    .line 2277429
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "new_build_url"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->e:LX/0Tn;

    .line 2277430
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "new_version_notes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->f:LX/0Tn;

    .line 2277431
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "download_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->g:LX/0Tn;

    .line 2277432
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "local_file"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->h:LX/0Tn;

    .line 2277433
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "hard_nag"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->i:LX/0Tn;

    .line 2277434
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "app_name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->j:LX/0Tn;

    .line 2277435
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "app_version"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->k:LX/0Tn;

    .line 2277436
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "postponed_build"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->l:LX/0Tn;

    .line 2277437
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "postponed_until"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->m:LX/0Tn;

    .line 2277438
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "download_status"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->n:LX/0Tn;

    .line 2277439
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "megaphone"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->o:LX/0Tn;

    .line 2277440
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->p:LX/0Tn;

    .line 2277441
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "file_size"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->q:LX/0Tn;

    .line 2277442
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "apk_source"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->r:LX/0Tn;

    .line 2277443
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "use_oxygen_for_internal"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->s:LX/0Tn;

    .line 2277444
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "show_debug_info"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->t:LX/0Tn;

    .line 2277445
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_last_fetched"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->u:LX/0Tn;

    .line 2277446
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_fetch_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->v:LX/0Tn;

    .line 2277447
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_package_name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->w:LX/0Tn;

    .line 2277448
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_url"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->x:LX/0Tn;

    .line 2277449
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_version_code"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->y:LX/0Tn;

    .line 2277450
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_version_name"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->z:LX/0Tn;

    .line 2277451
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_size"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->A:LX/0Tn;

    .line 2277452
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_release_notes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->B:LX/0Tn;

    .line 2277453
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "foreground_source"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->C:LX/0Tn;

    .line 2277454
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "allow_on_local_builds"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->D:LX/0Tn;

    .line 2277455
    sget-object v0, LX/Fjg;->a:LX/0Tn;

    const-string v1, "launch_interval"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Fjg;->E:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2277456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
