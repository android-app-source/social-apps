.class public final LX/Fri;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/FsM;",
        "LX/FsM;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Frv;


# direct methods
.method public constructor <init>(LX/Frv;)V
    .locals 0

    .prologue
    .line 2296023
    iput-object p1, p0, LX/Fri;->a:LX/Frv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2296024
    check-cast p1, LX/FsM;

    .line 2296025
    iget-object v1, p1, LX/FsM;->b:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 2296026
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSection;

    .line 2296027
    iget-object v2, p0, LX/Fri;->a:LX/Frv;

    iget-object v2, v2, LX/Frv;->c:LX/G4s;

    invoke-virtual {v2, v0}, LX/G4s;->a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)Lcom/facebook/graphql/model/GraphQLTimelineSection;

    move-result-object v0

    .line 2296028
    new-instance v2, LX/4ZF;

    invoke-direct {v2}, LX/4ZF;-><init>()V

    .line 2296029
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2296030
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;->a()LX/0Px;

    move-result-object v3

    iput-object v3, v2, LX/4ZF;->b:LX/0Px;

    .line 2296031
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    iput-object v3, v2, LX/4ZF;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2296032
    invoke-static {v2, v1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2296033
    move-object v1, v2

    .line 2296034
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2296035
    iput-object v0, v1, LX/4ZF;->b:LX/0Px;

    .line 2296036
    move-object v0, v1

    .line 2296037
    invoke-virtual {v0}, LX/4ZF;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    .line 2296038
    new-instance v1, LX/FsM;

    iget-object v2, p1, LX/FsM;->a:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    iget-object v3, p1, LX/FsM;->c:LX/0ta;

    invoke-direct {v1, v2, v0, v3}, LX/FsM;-><init>(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;LX/0ta;)V

    return-object v1
.end method
