.class public LX/Fw8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Fw8;


# instance fields
.field public final a:LX/0iA;

.field private final b:LX/FwD;

.field public c:Z


# direct methods
.method public constructor <init>(LX/0iA;LX/FwD;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2302822
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fw8;->c:Z

    .line 2302823
    iput-object p1, p0, LX/Fw8;->a:LX/0iA;

    .line 2302824
    iput-object p2, p0, LX/Fw8;->b:LX/FwD;

    .line 2302825
    return-void
.end method

.method public static a(LX/0QB;)LX/Fw8;
    .locals 6

    .prologue
    .line 2302837
    sget-object v0, LX/Fw8;->d:LX/Fw8;

    if-nez v0, :cond_1

    .line 2302838
    const-class v1, LX/Fw8;

    monitor-enter v1

    .line 2302839
    :try_start_0
    sget-object v0, LX/Fw8;->d:LX/Fw8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2302840
    if-eqz v2, :cond_0

    .line 2302841
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2302842
    new-instance p0, LX/Fw8;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-static {v0}, LX/FwD;->a(LX/0QB;)LX/FwD;

    move-result-object v4

    check-cast v4, LX/FwD;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, LX/Fw8;-><init>(LX/0iA;LX/FwD;Landroid/content/res/Resources;)V

    .line 2302843
    move-object v0, p0

    .line 2302844
    sput-object v0, LX/Fw8;->d:LX/Fw8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2302845
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2302846
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2302847
    :cond_1
    sget-object v0, LX/Fw8;->d:LX/Fw8;

    return-object v0

    .line 2302848
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2302849
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/5SB;LX/BQ1;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2302826
    iget-boolean v2, p2, LX/BQ1;->c:Z

    move v2, v2

    .line 2302827
    if-eqz v2, :cond_2

    .line 2302828
    invoke-virtual {p2}, LX/BQ1;->aa()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p2}, LX/BQ1;->ad()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2302829
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2302830
    goto :goto_0

    .line 2302831
    :cond_2
    iget-boolean v2, p0, LX/Fw8;->c:Z

    if-nez v2, :cond_3

    iget-object v2, p0, LX/Fw8;->b:LX/FwD;

    invoke-virtual {v2, p1}, LX/FwD;->a(LX/5SB;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p2}, LX/BPy;->j()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p2}, LX/BQ1;->aa()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    move v0, v1

    .line 2302832
    goto :goto_0

    .line 2302833
    :cond_4
    iget-object v2, p0, LX/Fw8;->a:LX/0iA;

    const-string v3, "3621"

    const-class v4, LX/3kc;

    invoke-virtual {v2, v3, v4}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/3kc;

    move-object v2, v2

    .line 2302834
    if-eqz v2, :cond_5

    .line 2302835
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/3kc;->a:Z

    .line 2302836
    :cond_5
    iget-object v2, p0, LX/Fw8;->a:LX/0iA;

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v4, LX/3kc;

    invoke-virtual {v2, v3, v4}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
