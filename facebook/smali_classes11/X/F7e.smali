.class public LX/F7e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2202336
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2202337
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2202338
    const-string v0, "ci_flow"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2202339
    const-string v0, "ci_flow"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/89v;->fromString(Ljava/lang/String;)LX/89v;

    move-result-object v0

    .line 2202340
    iget-object v1, v0, LX/89v;->value:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;Z)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    return-object v0
.end method
