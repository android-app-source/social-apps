.class public final LX/FTU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2245800
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2245801
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2245802
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2245803
    invoke-static {p0, p1}, LX/FTU;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2245804
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2245805
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2245868
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2245869
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2245870
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/FTU;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245871
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2245872
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2245873
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2245837
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 2245838
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2245839
    :goto_0
    return v1

    .line 2245840
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2245841
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 2245842
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2245843
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2245844
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 2245845
    const-string v9, "discovery_bucket"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2245846
    invoke-static {p0, p1}, LX/FTT;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2245847
    :cond_2
    const-string v9, "icon"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2245848
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2245849
    :cond_3
    const-string v9, "node"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2245850
    invoke-static {p0, p1}, LX/5va;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2245851
    :cond_4
    const-string v9, "target_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2245852
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2245853
    :cond_5
    const-string v9, "timeline_context_list_item_type"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2245854
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLTimelineContextListItemType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 2245855
    :cond_6
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2245856
    invoke-static {p0, p1}, LX/5vb;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2245857
    :cond_7
    const-string v9, "url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2245858
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    .line 2245859
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2245860
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2245861
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2245862
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2245863
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2245864
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2245865
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2245866
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2245867
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 2245806
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2245807
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245808
    if-eqz v0, :cond_0

    .line 2245809
    const-string v1, "discovery_bucket"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245810
    invoke-static {p0, v0, p2}, LX/FTT;->a(LX/15i;ILX/0nX;)V

    .line 2245811
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245812
    if-eqz v0, :cond_1

    .line 2245813
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245814
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2245815
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245816
    if-eqz v0, :cond_2

    .line 2245817
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245818
    invoke-static {p0, v0, p2, p3}, LX/5va;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245819
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2245820
    if-eqz v0, :cond_3

    .line 2245821
    const-string v0, "target_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245822
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245823
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2245824
    if-eqz v0, :cond_4

    .line 2245825
    const-string v0, "timeline_context_list_item_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245826
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245827
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245828
    if-eqz v0, :cond_5

    .line 2245829
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245830
    invoke-static {p0, v0, p2}, LX/5vb;->a(LX/15i;ILX/0nX;)V

    .line 2245831
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2245832
    if-eqz v0, :cond_6

    .line 2245833
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245834
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2245835
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2245836
    return-void
.end method
