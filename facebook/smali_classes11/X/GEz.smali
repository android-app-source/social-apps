.class public LX/GEz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/BudgetOptionsView;",
        "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GJJ;

.field private b:LX/0ad;


# direct methods
.method public constructor <init>(LX/GJJ;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332222
    iput-object p1, p0, LX/GEz;->a:LX/GJJ;

    .line 2332223
    iput-object p2, p0, LX/GEz;->b:LX/0ad;

    .line 2332224
    return-void
.end method

.method public static a(LX/0QB;)LX/GEz;
    .locals 1

    .prologue
    .line 2332204
    invoke-static {p0}, LX/GEz;->b(LX/0QB;)LX/GEz;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GEz;
    .locals 15

    .prologue
    .line 2332216
    new-instance v2, LX/GEz;

    .line 2332217
    new-instance v3, LX/GJJ;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v4

    check-cast v4, LX/GG6;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v5

    check-cast v5, LX/2U3;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {p0}, LX/GMU;->a(LX/0QB;)LX/GMU;

    move-result-object v7

    check-cast v7, LX/GMU;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {p0}, LX/GE2;->b(LX/0QB;)LX/GE2;

    move-result-object v9

    check-cast v9, LX/GE2;

    const-class v10, LX/GLN;

    invoke-interface {p0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/GLN;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v11

    check-cast v11, LX/0Sh;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v13

    check-cast v13, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-direct/range {v3 .. v14}, LX/GJJ;-><init>(LX/GG6;LX/2U3;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2332218
    move-object v0, v3

    .line 2332219
    check-cast v0, LX/GJJ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/GEz;-><init>(LX/GJJ;LX/0ad;)V

    .line 2332220
    return-object v2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332215
    const v0, 0x7f03004e

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 4

    .prologue
    .line 2332207
    check-cast p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2332208
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2332209
    :cond_0
    :goto_0
    return v0

    .line 2332210
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v2

    .line 2332211
    sget-object v3, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v2, v3, :cond_2

    sget-object v3, LX/GGB;->PENDING:LX/GGB;

    if-ne v2, v3, :cond_3

    .line 2332212
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v2, v3, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2332213
    goto :goto_0

    .line 2332214
    :cond_3
    sget-object v3, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v2, v3, :cond_4

    sget-object v3, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v2, v3, :cond_4

    sget-object v3, LX/GGB;->EXTENDABLE:LX/GGB;

    if-ne v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/BudgetOptionsView;",
            "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332206
    iget-object v0, p0, LX/GEz;->a:LX/GJJ;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332205
    sget-object v0, LX/8wK;->BUDGET:LX/8wK;

    return-object v0
.end method
