.class public final LX/FqH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "LX/Fw0;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/timeline/TimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/TimelineFragment;J)V
    .locals 0

    .prologue
    .line 2292657
    iput-object p1, p0, LX/FqH;->b:Lcom/facebook/timeline/TimelineFragment;

    iput-wide p2, p0, LX/FqH;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 2292658
    const-string v0, "TimelineFragment.onFragmentCreate.getViewportListener"

    const v1, 0x3935e32

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2292659
    :try_start_0
    iget-object v0, p0, LX/FqH;->b:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->T:LX/Fw1;

    iget-wide v2, p0, LX/FqH;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, LX/FqH;->b:Lcom/facebook/timeline/TimelineFragment;

    iget-object v2, v2, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    iget-object v3, p0, LX/FqH;->b:Lcom/facebook/timeline/TimelineFragment;

    iget-object v3, v3, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    .line 2292660
    new-instance v4, LX/Fw0;

    invoke-static {v0}, LX/BQ9;->a(LX/0QB;)LX/BQ9;

    move-result-object v8

    check-cast v8, LX/BQ9;

    invoke-static {v0}, LX/1dr;->a(LX/0QB;)LX/1dr;

    move-result-object v9

    check-cast v9, LX/1dr;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-static {v0}, LX/1BX;->a(LX/0QB;)LX/1BX;

    move-result-object v11

    check-cast v11, LX/1BX;

    invoke-static {v0}, LX/1BK;->a(LX/0QB;)LX/1BK;

    move-result-object v12

    check-cast v12, LX/1BK;

    invoke-static {v0}, LX/G2X;->a(LX/0QB;)LX/G2X;

    move-result-object v13

    check-cast v13, LX/G2X;

    move-object v5, v1

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v13}, LX/Fw0;-><init>(Ljava/lang/Long;LX/G0n;LX/G4x;LX/BQ9;LX/1dr;LX/0SG;LX/1BX;LX/1BK;LX/G2X;)V

    .line 2292661
    move-object v0, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2292662
    const v1, 0x6f456143

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x3b6f65c1

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
