.class public LX/GIs;
.super LX/GIr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "LX/GIr",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;",
        "TD;>;"
    }
.end annotation


# instance fields
.field public q:LX/GIN;


# direct methods
.method public constructor <init>(LX/GIN;LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337348
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/GIr;-><init>(LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V

    .line 2337349
    iput-object p1, p0, LX/GIs;->q:LX/GIN;

    .line 2337350
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 2337345
    invoke-super {p0}, LX/GIr;->a()V

    .line 2337346
    iget-object v0, p0, LX/GIs;->q:LX/GIN;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2337347
    return-void
.end method

.method public a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2337311
    invoke-super {p0, p1}, LX/GIr;->a(LX/GCE;)V

    .line 2337312
    iget-object v0, p0, LX/GIs;->q:LX/GIN;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2337313
    return-void
.end method

.method public bridge synthetic a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337344
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2337341
    invoke-super {p0, p1}, LX/GIr;->a(Landroid/os/Bundle;)V

    .line 2337342
    iget-object v0, p0, LX/GIs;->q:LX/GIN;

    invoke-virtual {v0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2337343
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337351
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 2337338
    invoke-super {p0, p1}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2337339
    iget-object v0, p0, LX/GIs;->q:LX/GIN;

    invoke-virtual {v0, p1}, LX/GIN;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2337340
    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2337322
    invoke-super {p0, p1, p2}, LX/GIr;->a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337323
    iget-object v0, p0, LX/GIs;->q:LX/GIN;

    .line 2337324
    iget-object v2, p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    move-object v2, v2

    .line 2337325
    invoke-virtual {v0, v2, p2}, LX/GIN;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337326
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2337327
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    iget-object v2, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2337328
    iget-object p1, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v2, p1

    .line 2337329
    if-ne v0, v2, :cond_0

    move v0, v1

    .line 2337330
    :goto_0
    invoke-virtual {p0, v0}, LX/GIr;->a(Z)V

    .line 2337331
    invoke-virtual {p0, v1}, LX/GIr;->b(Z)V

    .line 2337332
    invoke-virtual {p0, v0}, LX/GIr;->c(Z)V

    .line 2337333
    invoke-virtual {p0, v0}, LX/GIr;->e(Z)V

    .line 2337334
    invoke-virtual {p0}, LX/GIs;->d()V

    .line 2337335
    invoke-static {p2}, LX/GIr;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337336
    return-void

    .line 2337337
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2337318
    if-nez p1, :cond_0

    .line 2337319
    :goto_0
    return-void

    .line 2337320
    :cond_0
    invoke-super {p0, p1}, LX/GIr;->b(Landroid/os/Bundle;)V

    .line 2337321
    iget-object v0, p0, LX/GIs;->q:LX/GIN;

    invoke-virtual {v0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2337314
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337315
    new-instance v1, LX/GLP;

    invoke-direct {v1, p0}, LX/GLP;-><init>(LX/GIs;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2337316
    invoke-super {p0}, LX/GIr;->d()V

    .line 2337317
    return-void
.end method
