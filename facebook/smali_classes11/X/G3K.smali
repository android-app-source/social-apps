.class public LX/G3K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/63c;

.field private b:LX/63b;

.field private c:LX/63b;

.field public d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

.field public e:LX/G46;

.field public f:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

.field public g:Landroid/content/res/Resources;

.field private final h:Landroid/view/animation/Interpolator;

.field private final i:Landroid/animation/AnimatorListenerAdapter;


# direct methods
.method public constructor <init>(LX/63c;Landroid/content/res/Resources;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v2, 0x3e800000    # 0.25f

    .line 2315237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2315238
    const v0, 0x3dcccccd    # 0.1f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v2, v0, v2, v1}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, LX/G3K;->h:Landroid/view/animation/Interpolator;

    .line 2315239
    new-instance v0, LX/G3H;

    invoke-direct {v0, p0}, LX/G3H;-><init>(LX/G3K;)V

    iput-object v0, p0, LX/G3K;->i:Landroid/animation/AnimatorListenerAdapter;

    .line 2315240
    new-instance v0, LX/G46;

    invoke-direct {v0}, LX/G46;-><init>()V

    iput-object v0, p0, LX/G3K;->e:LX/G46;

    .line 2315241
    iget-object v0, p0, LX/G3K;->e:LX/G46;

    iget-object v1, p0, LX/G3K;->h:Landroid/view/animation/Interpolator;

    .line 2315242
    iput-object v1, v0, LX/G46;->a:Landroid/view/animation/Interpolator;

    .line 2315243
    iput-object p1, p0, LX/G3K;->a:LX/63c;

    .line 2315244
    iput-object p2, p0, LX/G3K;->g:Landroid/content/res/Resources;

    .line 2315245
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z
    .locals 1

    .prologue
    .line 2315251
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/timeline/refresher/ProfileRefresherView;)Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2315246
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v0

    .line 2315247
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->c(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315248
    iget-object v0, p0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v0

    .line 2315249
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    .line 2315250
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/G3c;)V
    .locals 1

    .prologue
    .line 2315229
    invoke-virtual {p1}, LX/G3c;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315230
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315231
    iget-object p0, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    move-object v0, p0

    .line 2315232
    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2315233
    :goto_0
    return-void

    .line 2315234
    :cond_0
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315235
    iget-object p0, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->r:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    move-object v0, p0

    .line 2315236
    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_0
.end method

.method public static f(LX/G3K;LX/G3c;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2315252
    if-eqz p1, :cond_9

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315253
    invoke-direct {p0, p1}, LX/G3K;->p(LX/G3c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315254
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315255
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v2

    .line 2315256
    iget-object v2, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315257
    iget-object v3, v2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->n:Landroid/widget/FrameLayout;

    move-object v2, v3

    .line 2315258
    iget-object v3, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315259
    iget-object v4, v3, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v3, v4

    .line 2315260
    iget-object v4, p1, LX/G3c;->o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-object v4, v4

    .line 2315261
    invoke-virtual {v3, v4}, LX/G3d;->a(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I

    move-result v3

    iget-object v4, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315262
    iget-object v5, v4, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v4, v5

    .line 2315263
    iget-object v5, p1, LX/G3c;->o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-object v5, v5

    .line 2315264
    invoke-virtual {v4, v5}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2315265
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315266
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v2

    .line 2315267
    iget-object v2, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315268
    iget-object v3, v2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->n:Landroid/widget/FrameLayout;

    move-object v2, v3

    .line 2315269
    invoke-virtual {v0, v2}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 2315270
    :cond_0
    invoke-static {p1}, LX/G3K;->o(LX/G3c;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2315271
    const/4 v5, 0x0

    .line 2315272
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315273
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v2

    .line 2315274
    iget-object v2, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315275
    iget-object v3, v2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->n:Landroid/widget/FrameLayout;

    move-object v2, v3

    .line 2315276
    iget-object v3, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315277
    iget-object v4, v3, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v3, v4

    .line 2315278
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/G3d;->a(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0gG;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    .line 2315279
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315280
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v2

    .line 2315281
    iget-object v2, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315282
    iget-object v3, v2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->n:Landroid/widget/FrameLayout;

    move-object v2, v3

    .line 2315283
    invoke-virtual {v0, v2}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 2315284
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315285
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v2

    .line 2315286
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2315287
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v2

    .line 2315288
    if-eqz v0, :cond_1

    .line 2315289
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 2315290
    :cond_1
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    const v2, 0x7f0d277d

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5, v5}, Landroid/view/View;->scrollTo(II)V

    .line 2315291
    :cond_2
    invoke-direct {p0, p1}, LX/G3K;->g(LX/G3c;)V

    .line 2315292
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v2, :cond_a

    .line 2315293
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iget-object v2, p0, LX/G3K;->g:Landroid/content/res/Resources;

    const v3, 0x7f081610

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setBottomPhotoBarText(Ljava/lang/String;)V

    .line 2315294
    :cond_3
    :goto_1
    invoke-virtual {p1}, LX/G3c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, LX/G3c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2315295
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315296
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    move-object v0, v2

    .line 2315297
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2315298
    :goto_2
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315299
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->f:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v2

    .line 2315300
    invoke-virtual {p1}, LX/G3c;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2315301
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315302
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->g:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v2

    .line 2315303
    invoke-virtual {p1}, LX/G3c;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2315304
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq v0, v2, :cond_d

    .line 2315305
    :cond_4
    :goto_3
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-static {v0}, LX/G3K;->b(Lcom/facebook/timeline/refresher/ProfileRefresherView;)Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    move-result-object v3

    .line 2315306
    if-eqz v3, :cond_5

    .line 2315307
    iget-object v0, v3, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v0, v0

    .line 2315308
    if-eqz v0, :cond_5

    .line 2315309
    iget-object v0, v3, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v0, v0

    .line 2315310
    iget-object v2, v0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    move-object v0, v2

    .line 2315311
    if-nez v0, :cond_e

    .line 2315312
    :cond_5
    :goto_4
    iget-object v0, p0, LX/G3K;->f:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    if-eqz v0, :cond_6

    .line 2315313
    iget-object v0, p0, LX/G3K;->f:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    .line 2315314
    iget v2, p1, LX/G3c;->l:I

    move v2, v2

    .line 2315315
    iget v3, p1, LX/G3c;->m:I

    move v3, v3

    .line 2315316
    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->a(II)V

    .line 2315317
    :cond_6
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-static {v0}, LX/G3K;->b(Lcom/facebook/timeline/refresher/ProfileRefresherView;)Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    move-result-object v0

    .line 2315318
    if-eqz v0, :cond_7

    .line 2315319
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v2, v2

    .line 2315320
    if-eqz v2, :cond_7

    .line 2315321
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v2, v2

    .line 2315322
    invoke-virtual {v2}, LX/Ban;->getCoverPhotoView()Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    move-result-object v2

    if-nez v2, :cond_12

    .line 2315323
    :cond_7
    :goto_5
    invoke-static {p1}, LX/G3K;->o(LX/G3c;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2315324
    iget-object v0, p0, LX/G3K;->e:LX/G46;

    iget-object v2, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v2, v3, v1}, LX/G46;->a(LX/G3c;Lcom/facebook/timeline/refresher/ProfileRefresherView;Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;Z)V

    .line 2315325
    :cond_8
    return-void

    :cond_9
    move v0, v1

    .line 2315326
    goto/16 :goto_0

    .line 2315327
    :cond_a
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v2, :cond_b

    .line 2315328
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->c()V

    .line 2315329
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iget-object v2, p0, LX/G3K;->g:Landroid/content/res/Resources;

    const v3, 0x7f081611

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setBottomBioBar(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2315330
    :cond_b
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v2, :cond_3

    .line 2315331
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->c()V

    .line 2315332
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iget-object v2, p0, LX/G3K;->g:Landroid/content/res/Resources;

    const v3, 0x7f0815f1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setBottomFeaturedPhotosBar(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2315333
    :cond_c
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315334
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    move-object v0, v2

    .line 2315335
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 2315336
    :cond_d
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-static {v0}, LX/G3K;->b(Lcom/facebook/timeline/refresher/ProfileRefresherView;)Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    move-result-object v0

    .line 2315337
    if-eqz v0, :cond_4

    .line 2315338
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v2, v2

    .line 2315339
    if-eqz v2, :cond_4

    .line 2315340
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v0, v2

    .line 2315341
    iget-object v2, v0, LX/Ban;->e:Lcom/facebook/widget/CustomLinearLayout;

    move-object v0, v2

    .line 2315342
    iget-object v2, p0, LX/G3K;->g:Landroid/content/res/Resources;

    const v3, 0x7f021513

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 2315343
    :cond_e
    iget-object v0, v3, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v0, v0

    .line 2315344
    iget-object v2, v0, LX/Ban;->l:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    move-object v4, v2

    .line 2315345
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v2, :cond_f

    .line 2315346
    invoke-virtual {v4}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 2315347
    iget-object v0, p0, LX/G3K;->g:Landroid/content/res/Resources;

    const v5, 0x7f0b0deb

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2315348
    iget-object v0, p0, LX/G3K;->g:Landroid/content/res/Resources;

    const v5, 0x7f0b0deb

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    move-object v0, v2

    .line 2315349
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2315350
    iget-object v5, p0, LX/G3K;->g:Landroid/content/res/Resources;

    const v6, 0x7f0b0dec

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2315351
    invoke-virtual {v4, v2}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2315352
    :cond_f
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq v0, v2, :cond_10

    .line 2315353
    const v0, 0x3ecccccd    # 0.4f

    invoke-virtual {v4, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->setAlpha(F)V

    .line 2315354
    :goto_6
    iget-object v0, v3, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v0, v0

    .line 2315355
    iget-object v2, p1, LX/G3c;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2315356
    if-nez v2, :cond_11

    .line 2315357
    const-string v2, ""

    move-object v2, v2

    .line 2315358
    :goto_7
    move-object v2, v2

    .line 2315359
    invoke-virtual {v0, v2}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->setUpProfilePicture(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2315360
    :cond_10
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v4, v0}, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->setAlpha(F)V

    goto :goto_6

    .line 2315361
    :cond_11
    iget-object v2, p1, LX/G3c;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2315362
    goto :goto_7

    .line 2315363
    :cond_12
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v2, v3, :cond_13

    .line 2315364
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v2, v2

    .line 2315365
    sget-object v3, LX/Bam;->CUSTOM:LX/Bam;

    iput-object v3, v2, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->d:LX/Bam;

    .line 2315366
    invoke-virtual {v2}, LX/Ban;->f()V

    .line 2315367
    :cond_13
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v2, v2

    .line 2315368
    invoke-virtual {v2}, LX/Ban;->getCoverPhotoView()Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    move-result-object v2

    .line 2315369
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq v3, v4, :cond_14

    .line 2315370
    const v3, 0x3ecccccd    # 0.4f

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setAlpha(F)V

    .line 2315371
    :goto_8
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    move-object v2, v2

    .line 2315372
    iget-object v0, p1, LX/G3c;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2315373
    if-eqz v0, :cond_16

    .line 2315374
    iget-object v0, p1, LX/G3c;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2315375
    :goto_9
    move-object v3, v0

    .line 2315376
    iget-object v0, p1, LX/G3c;->a:Ljava/lang/String;

    move-object v4, v0

    .line 2315377
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v5, :cond_15

    const/4 v0, 0x1

    :goto_a
    invoke-virtual {v2, v3, v4, v0}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_5

    .line 2315378
    :cond_14
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setAlpha(F)V

    goto :goto_8

    .line 2315379
    :cond_15
    const/4 v0, 0x0

    goto :goto_a

    :cond_16
    const/4 v3, 0x0

    .line 2315380
    iget-object v0, p1, LX/G3c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    :goto_b
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315381
    iget-boolean v0, p1, LX/G3c;->j:Z

    if-eqz v0, :cond_18

    .line 2315382
    iget-object v0, p1, LX/G3c;->p:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->a()Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2315383
    :goto_c
    move-object v0, v0

    .line 2315384
    goto :goto_9

    :cond_17
    move v0, v3

    .line 2315385
    goto :goto_b

    .line 2315386
    :cond_18
    iget-object v0, p1, LX/G3c;->p:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v3}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v4, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_c
.end method

.method private g(LX/G3c;)V
    .locals 2

    .prologue
    .line 2315217
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2315218
    iget-object v0, p0, LX/G3K;->b:LX/63b;

    if-nez v0, :cond_0

    .line 2315219
    iget-object v0, p0, LX/G3K;->a:LX/63c;

    const v1, 0x7f080031

    invoke-virtual {v0, v1}, LX/63c;->a(I)LX/63b;

    move-result-object v0

    iput-object v0, p0, LX/G3K;->b:LX/63b;

    .line 2315220
    :cond_0
    iget-object v0, p0, LX/G3K;->b:LX/63b;

    invoke-virtual {v0}, LX/63b;->a()LX/0Px;

    move-result-object v0

    .line 2315221
    :goto_0
    iget-object v1, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v1, v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setTitleBarButtonSpecs(Ljava/util/List;)V

    .line 2315222
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    iget-object v1, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315223
    iget-object p0, v1, Lcom/facebook/timeline/refresher/ProfileRefresherView;->v:LX/63W;

    move-object v1, p0

    .line 2315224
    invoke-virtual {v0, v1}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->setTitleBarButtonListener(LX/63W;)V

    .line 2315225
    return-void

    .line 2315226
    :cond_1
    iget-object v0, p0, LX/G3K;->c:LX/63b;

    if-nez v0, :cond_2

    .line 2315227
    iget-object v0, p0, LX/G3K;->a:LX/63c;

    const v1, 0x7f080030

    invoke-virtual {v0, v1}, LX/63c;->a(I)LX/63b;

    move-result-object v0

    iput-object v0, p0, LX/G3K;->c:LX/63b;

    .line 2315228
    :cond_2
    iget-object v0, p0, LX/G3K;->c:LX/63b;

    invoke-virtual {v0}, LX/63b;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static o(LX/G3c;)Z
    .locals 2

    .prologue
    .line 2315215
    iget-object v0, p0, LX/G3c;->o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-object v0, v0

    .line 2315216
    invoke-virtual {p0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p(LX/G3c;)Z
    .locals 2

    .prologue
    .line 2315164
    invoke-static {p1}, LX/G3K;->o(LX/G3c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315165
    iget-object v0, p1, LX/G3c;->o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-object v0, v0

    .line 2315166
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315167
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315168
    iget-object v1, p1, LX/G3c;->o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-object v1, v1

    .line 2315169
    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2315170
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2315171
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(LX/G3c;)V
    .locals 4

    .prologue
    .line 2315188
    invoke-direct {p0, p1}, LX/G3K;->d(LX/G3c;)V

    .line 2315189
    invoke-virtual {p1}, LX/G3c;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2315190
    invoke-virtual {p0, p1}, LX/G3K;->c(LX/G3c;)V

    .line 2315191
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    invoke-static {v0}, LX/G3K;->a(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315192
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    move-object v0, v1

    .line 2315193
    if-eqz v0, :cond_0

    .line 2315194
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315195
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->p:Landroid/view/ViewStub;

    move-object v0, v1

    .line 2315196
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2315197
    :cond_0
    invoke-direct {p0, p1}, LX/G3K;->p(LX/G3c;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2315198
    new-instance v0, LX/G3I;

    invoke-direct {v0, p0, p1}, LX/G3I;-><init>(LX/G3K;LX/G3c;)V

    .line 2315199
    new-instance v1, LX/G3J;

    invoke-direct {v1, p0, v0}, LX/G3J;-><init>(LX/G3K;Landroid/animation/AnimatorListenerAdapter;)V

    .line 2315200
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    invoke-static {v0}, LX/G3K;->a(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315201
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    move-object v0, v2

    .line 2315202
    if-eqz v0, :cond_1

    .line 2315203
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315204
    iget-object v2, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    move-object v0, v2

    .line 2315205
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2315206
    :cond_1
    iget-object v0, p0, LX/G3K;->e:LX/G46;

    iget-object v2, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315207
    iget-object v3, v2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v2, v3

    .line 2315208
    iget-object v3, p1, LX/G3c;->o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-object v3, v3

    .line 2315209
    invoke-virtual {v2, v3}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2315210
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v3

    .line 2315211
    const/high16 v3, 0x42c80000    # 100.0f

    .line 2315212
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    iget-object p1, v0, LX/G46;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, p1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p0

    invoke-virtual {p0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2315213
    :cond_2
    :goto_0
    return-void

    .line 2315214
    :cond_3
    invoke-static {p0, p1}, LX/G3K;->f(LX/G3K;LX/G3c;)V

    goto :goto_0
.end method

.method public final c(LX/G3c;)V
    .locals 2

    .prologue
    .line 2315172
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315173
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    move-object v0, v1

    .line 2315174
    invoke-static {v0}, LX/G46;->a(Landroid/view/View;)V

    .line 2315175
    invoke-virtual {p1}, LX/G3c;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315176
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315177
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315178
    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2315179
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2315180
    invoke-static {v0}, LX/G46;->a(Landroid/view/View;)V

    .line 2315181
    iget-object v0, p0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315182
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315183
    iget-object v1, p1, LX/G3c;->o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-object v1, v1

    .line 2315184
    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2315185
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2315186
    invoke-static {v0}, LX/G46;->a(Landroid/view/View;)V

    .line 2315187
    :cond_0
    return-void
.end method
