.class public final LX/FmG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Qm;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;)V
    .locals 0

    .prologue
    .line 2282260
    iput-object p1, p0, LX/FmG;->a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;B)V
    .locals 0

    .prologue
    .line 2282261
    invoke-direct {p0, p1}, LX/FmG;-><init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 3

    .prologue
    .line 2282262
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2282263
    const-string v1, "privacy_option"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2282264
    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->MUTATION:LX/73S;

    invoke-direct {v1, v2, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 2282265
    iget-object v0, p0, LX/FmG;->a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    iget-object v0, v0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->d:LX/6qh;

    invoke-virtual {v0, v1}, LX/6qh;->a(LX/73T;)V

    .line 2282266
    return-void
.end method
