.class public final LX/FrP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/FrQ;


# direct methods
.method public constructor <init>(LX/FrQ;)V
    .locals 0

    .prologue
    .line 2295505
    iput-object p1, p0, LX/FrP;->a:LX/FrQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 2295506
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    iget-object v0, v0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    if-nez v0, :cond_0

    .line 2295507
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    iget-object v1, p0, LX/FrP;->a:LX/FrQ;

    iget-object v1, v1, LX/FrQ;->g:LX/FvG;

    invoke-virtual {v1}, LX/FvG;->c()Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    move-result-object v1

    .line 2295508
    iput-object v1, v0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    .line 2295509
    :cond_0
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    iget-object v0, v0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    if-eqz v0, :cond_3

    .line 2295510
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    invoke-virtual {v0}, LX/FrQ;->a()V

    .line 2295511
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    iget-object v0, v0, LX/FrQ;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_1

    .line 2295512
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    iget-object v1, p0, LX/FrP;->a:LX/FrQ;

    iget-object v1, v1, LX/FrQ;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, LX/FrQ;->removeView(Landroid/view/View;)V

    .line 2295513
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    const/4 v1, 0x0

    .line 2295514
    iput-object v1, v0, LX/FrQ;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2295515
    :cond_1
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    iget-object v0, v0, LX/FrQ;->j:Landroid/view/View;

    if-nez v0, :cond_2

    .line 2295516
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    const/4 v3, -0x1

    .line 2295517
    new-instance v1, Landroid/view/View;

    invoke-virtual {v0}, LX/FrQ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, LX/FrQ;->j:Landroid/view/View;

    .line 2295518
    iget-object v1, v0, LX/FrQ;->j:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 2295519
    iget-object v1, v0, LX/FrQ;->j:Landroid/view/View;

    const v2, 0x7f0a055a

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2295520
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2295521
    iget-object v2, v0, LX/FrQ;->j:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2295522
    invoke-static {v0}, LX/FrQ;->c(LX/FrQ;)V

    .line 2295523
    iget-object v1, v0, LX/FrQ;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/FrQ;->addView(Landroid/view/View;)V

    .line 2295524
    :cond_2
    iget-object v0, p0, LX/FrP;->a:LX/FrQ;

    invoke-static {v0}, LX/FrQ;->c(LX/FrQ;)V

    .line 2295525
    :cond_3
    return-void
.end method
