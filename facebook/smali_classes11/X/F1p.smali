.class public final LX/F1p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:LX/F1r;


# direct methods
.method public constructor <init>(LX/F1r;)V
    .locals 0

    .prologue
    .line 2192564
    iput-object p1, p0, LX/F1p;->a:LX/F1r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    .line 2192565
    iget-object v0, p0, LX/F1p;->a:LX/F1r;

    iget-object v1, v0, LX/F1r;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget-object v0, p0, LX/F1p;->a:LX/F1r;

    iget-boolean v0, v0, LX/F1r;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F1p;->a:LX/F1r;

    iget-object v0, v0, LX/F1r;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getWidth()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a(I)V

    .line 2192566
    return-void

    .line 2192567
    :cond_0
    iget-object v0, p0, LX/F1p;->a:LX/F1r;

    iget-object v0, v0, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    iget-object v2, p0, LX/F1p;->a:LX/F1r;

    iget-object v2, v2, LX/F1r;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2192568
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2192569
    iget-object v0, p0, LX/F1p;->a:LX/F1r;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/F1r;->setVisibility(I)V

    .line 2192570
    return-void
.end method
