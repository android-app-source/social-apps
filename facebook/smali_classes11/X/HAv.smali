.class public final LX/HAv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V
    .locals 0

    .prologue
    .line 2436270
    iput-object p1, p0, LX/HAv;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, -0x7e26ebd8

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2436271
    iget-object v0, p0, LX/HAv;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-object v2, p0, LX/HAv;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->k:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2436272
    sget-object v4, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_PHONE_NUMBER:LX/9X6;

    invoke-virtual {v0, v4, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2436273
    iget-object v0, p0, LX/HAv;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tel:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2436274
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.DIAL"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 2436275
    iget-object v0, p0, LX/HAv;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HAv;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2436276
    const v0, -0x30932f1f

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
