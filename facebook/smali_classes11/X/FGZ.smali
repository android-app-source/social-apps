.class public LX/FGZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Xl;

.field private final b:LX/2Ml;

.field private final c:LX/2Mn;

.field private final d:LX/2Hu;

.field private final e:LX/2ML;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2219078
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FGZ;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/2Ml;LX/2Mn;LX/2Hu;LX/2ML;)V
    .locals 0
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2219079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219080
    iput-object p1, p0, LX/FGZ;->a:LX/0Xl;

    .line 2219081
    iput-object p2, p0, LX/FGZ;->b:LX/2Ml;

    .line 2219082
    iput-object p3, p0, LX/FGZ;->c:LX/2Mn;

    .line 2219083
    iput-object p4, p0, LX/FGZ;->d:LX/2Hu;

    .line 2219084
    iput-object p5, p0, LX/FGZ;->e:LX/2ML;

    .line 2219085
    return-void
.end method

.method public static a(LX/0QB;)LX/FGZ;
    .locals 13

    .prologue
    .line 2219086
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2219087
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2219088
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2219089
    if-nez v1, :cond_0

    .line 2219090
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2219091
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2219092
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2219093
    sget-object v1, LX/FGZ;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2219094
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2219095
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2219096
    :cond_1
    if-nez v1, :cond_4

    .line 2219097
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2219098
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2219099
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2219100
    new-instance v7, LX/FGZ;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v8

    check-cast v8, LX/0Xl;

    invoke-static {v0}, LX/2Ml;->a(LX/0QB;)LX/2Ml;

    move-result-object v9

    check-cast v9, LX/2Ml;

    invoke-static {v0}, LX/2Mn;->a(LX/0QB;)LX/2Mn;

    move-result-object v10

    check-cast v10, LX/2Mn;

    invoke-static {v0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v11

    check-cast v11, LX/2Hu;

    invoke-static {v0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v12

    check-cast v12, LX/2ML;

    invoke-direct/range {v7 .. v12}, LX/FGZ;-><init>(LX/0Xl;LX/2Ml;LX/2Mn;LX/2Hu;LX/2ML;)V

    .line 2219101
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2219102
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2219103
    if-nez v1, :cond_2

    .line 2219104
    sget-object v0, LX/FGZ;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGZ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2219105
    :goto_1
    if-eqz v0, :cond_3

    .line 2219106
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2219107
    :goto_3
    check-cast v0, LX/FGZ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2219108
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2219109
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2219110
    :catchall_1
    move-exception v0

    .line 2219111
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2219112
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2219113
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2219114
    :cond_2
    :try_start_8
    sget-object v0, LX/FGZ;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGZ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 14

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 2219115
    invoke-static {}, LX/0SW;->createStarted()LX/0SW;

    move-result-object v10

    .line 2219116
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2219117
    const-string v1, "mediaResource"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219118
    iget-object v0, p0, LX/FGZ;->e:LX/2ML;

    invoke-virtual {v0, v2}, LX/2ML;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2219119
    iget-object v1, p0, LX/FGZ;->b:LX/2Ml;

    sget-object v0, LX/FGY;->NO_HASH_AVAILABLE:LX/FGY;

    invoke-virtual {v0}, LX/FGY;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v0}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, LX/2Ml;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JLjava/lang/Throwable;)V

    .line 2219120
    iget-object v0, p0, LX/FGZ;->c:LX/2Mn;

    sget-object v1, LX/FGY;->NO_HASH_AVAILABLE:LX/FGY;

    invoke-virtual {v1}, LX/FGY;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1, v6}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2219121
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v1, "Failed to get hash for media resource"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2219122
    :goto_0
    return-object v0

    .line 2219123
    :cond_0
    iget-object v0, p0, LX/FGZ;->b:LX/2Ml;

    .line 2219124
    const-string v1, "messenger_get_media_fbid_started"

    invoke-static {v0, v2}, LX/2Ml;->b(LX/2Ml;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v3

    invoke-static {v0, v1, v3}, LX/2Ml;->a(LX/2Ml;Ljava/lang/String;Ljava/util/Map;)V

    .line 2219125
    iget-object v0, p0, LX/FGZ;->c:LX/2Mn;

    iget-object v1, p0, LX/FGZ;->e:LX/2ML;

    invoke-virtual {v1, v2}, LX/2ML;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v1

    .line 2219126
    invoke-static {v2}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v3

    .line 2219127
    iget-object v4, v0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v4, v3}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FHF;

    .line 2219128
    if-nez v3, :cond_5

    .line 2219129
    :goto_1
    sget-object v1, LX/FGY;->UNKNOWN:LX/FGY;

    .line 2219130
    iget-object v0, p0, LX/FGZ;->d:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v11

    .line 2219131
    :try_start_0
    new-instance v0, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2219132
    const-string v3, "mhash"

    iget-object v4, p0, LX/FGZ;->e:LX/2ML;

    invoke-virtual {v4, v2}, LX/2ML;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2219133
    new-instance v3, LX/FGX;

    invoke-direct {v3, p0}, LX/FGX;-><init>(LX/FGZ;)V

    .line 2219134
    const-string v4, "/get_media"

    const-string v5, "/get_media_resp"

    invoke-virtual {v11, v4, v0, v5, v3}, LX/2gV;->a(Ljava/lang/String;LX/0lF;Ljava/lang/String;LX/FGX;)LX/76M;

    move-result-object v7

    .line 2219135
    iget-boolean v0, v7, LX/76M;->a:Z

    if-nez v0, :cond_1

    .line 2219136
    sget-object v0, LX/FGY;->MQTT_FAILED:LX/FGY;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2219137
    :try_start_1
    iget-object v1, p0, LX/FGZ;->b:LX/2Ml;

    invoke-virtual {v0}, LX/FGY;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v4}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    iget-object v6, v7, LX/76M;->d:Ljava/lang/Exception;

    invoke-virtual/range {v1 .. v6}, LX/2Ml;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JLjava/lang/Throwable;)V

    .line 2219138
    iget-object v1, p0, LX/FGZ;->c:LX/2Mn;

    invoke-virtual {v0}, LX/FGY;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v7, LX/76M;->d:Ljava/lang/Exception;

    invoke-virtual {v1, v2, v3, v4}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2219139
    invoke-virtual {v7}, LX/76M;->a()Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2219140
    invoke-virtual {v11}, LX/2gV;->f()V

    goto :goto_0

    .line 2219141
    :cond_1
    :try_start_2
    iget-object v0, v7, LX/76M;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    .line 2219142
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 2219143
    sget-object v1, LX/FGY;->SERVER_SIDE_FAILED:LX/FGY;

    .line 2219144
    sget-object v3, LX/1nY;->MQTT_SEND_FAILURE:LX/1nY;

    const-string v4, "MQTT failed to get response from server"

    invoke-static {v3, v4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    move-object v9, v1

    .line 2219145
    :goto_2
    :try_start_3
    iget-object v1, p0, LX/FGZ;->b:LX/2Ml;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v9}, LX/FGY;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v6}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    .line 2219146
    invoke-static {v1, v2}, LX/2Ml;->b(LX/2Ml;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/util/Map;

    move-result-object v12

    .line 2219147
    const-string v13, "media_fbid"

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v12, v13, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219148
    const-string v13, "get_fbid_status"

    invoke-interface {v12, v13, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219149
    const-string v13, "elapsed_time"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v12, v13, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219150
    const-string v13, "messenger_get_media_fbid_finished"

    invoke-static {v1, v13, v12}, LX/2Ml;->a(LX/2Ml;Ljava/lang/String;Ljava/util/Map;)V

    .line 2219151
    iget-object v1, p0, LX/FGZ;->c:LX/2Mn;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v9}, LX/FGY;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v4, v5, v0}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;JLjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2219152
    invoke-virtual {v11}, LX/2gV;->f()V

    move-object v0, v8

    goto/16 :goto_0

    .line 2219153
    :cond_2
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v3, v4, v8

    if-nez v3, :cond_3

    .line 2219154
    sget-object v1, LX/FGY;->NO_EXISTING_MEDIA_FOUND:LX/FGY;

    .line 2219155
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v8

    move-object v9, v1

    goto :goto_2

    .line 2219156
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v3, v4, v8

    if-lez v3, :cond_4

    .line 2219157
    sget-object v1, LX/FGY;->VALID_FBID_RETURNED:LX/FGY;

    .line 2219158
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v8

    .line 2219159
    iget-object v3, p0, LX/FGZ;->a:LX/0Xl;

    invoke-static {v2}, LX/FGn;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Landroid/content/Intent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Xl;->a(Landroid/content/Intent;)V

    move-object v9, v1

    goto :goto_2

    .line 2219160
    :cond_4
    sget-object v1, LX/FGY;->INVALID_RESULT_RETURNED:LX/FGY;

    .line 2219161
    sget-object v3, LX/1nY;->NO_ERROR:LX/1nY;

    const-string v4, "Invalid result returned from MQTT get_media"

    invoke-static {v3, v4}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v8

    move-object v9, v1

    goto :goto_2

    .line 2219162
    :catch_0
    move-exception v6

    move-object v0, v1

    .line 2219163
    :goto_3
    :try_start_5
    iget-object v1, p0, LX/FGZ;->b:LX/2Ml;

    invoke-virtual {v0}, LX/FGY;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v4}, LX/0SW;->elapsed(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, LX/2Ml;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;JLjava/lang/Throwable;)V

    .line 2219164
    iget-object v1, p0, LX/FGZ;->c:LX/2Mn;

    invoke-virtual {v0}, LX/FGY;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0, v6}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2219165
    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2219166
    :catchall_0
    move-exception v0

    invoke-virtual {v11}, LX/2gV;->f()V

    throw v0

    .line 2219167
    :catch_1
    move-exception v6

    goto :goto_3

    :catch_2
    move-exception v6

    move-object v0, v9

    goto :goto_3

    .line 2219168
    :cond_5
    iget-object v4, v3, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2219169
    iget-object v3, v3, LX/FHF;->b:LX/0SW;

    .line 2219170
    const-string v5, "dedup_query_start"

    invoke-static {v4, v5, v3}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    .line 2219171
    const-string v3, "original_sha256"

    invoke-virtual {v4, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_1
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2219172
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2219173
    const-string v1, "media_get_fbid"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2219174
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2219175
    :cond_0
    invoke-direct {p0, p1}, LX/FGZ;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method
