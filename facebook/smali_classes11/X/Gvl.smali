.class public LX/Gvl;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2406041
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2406042
    iput-object p1, p0, LX/Gvl;->a:Landroid/content/Context;

    .line 2406043
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2406044
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2406045
    const-string v0, "Diode experiment - internal"

    invoke-virtual {p0, v0}, LX/Gvl;->setTitle(Ljava/lang/CharSequence;)V

    .line 2406046
    new-instance v0, LX/4or;

    iget-object v1, p0, LX/Gvl;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2406047
    const-string v1, "Set New User"

    invoke-virtual {v0, v1}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2406048
    const-string v1, "New User"

    invoke-virtual {v0, v1}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2406049
    const-string v1, "New User"

    invoke-virtual {v0, v1}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2406050
    sget-object v1, LX/3CC;->b:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4or;->a(LX/0Tn;)V

    .line 2406051
    sget-object v1, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 2406052
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "Unset (Use GK value)"

    aput-object v2, v1, v3

    const-string v2, "Yes"

    aput-object v2, v1, v4

    const-string v2, "No"

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2406053
    new-array v1, v6, [Ljava/lang/String;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    sget-object v2, LX/03R;->YES:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    sget-object v2, LX/03R;->NO:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2406054
    invoke-virtual {p0, v0}, LX/Gvl;->addPreference(Landroid/preference/Preference;)Z

    .line 2406055
    new-instance v0, LX/4or;

    iget-object v1, p0, LX/Gvl;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 2406056
    const-string v1, "Set Is User in Diode"

    invoke-virtual {v0, v1}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 2406057
    const-string v1, "Determines whether you are in the Diode Experiments"

    invoke-virtual {v0, v1}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2406058
    const-string v1, "Is User in Diode?"

    invoke-virtual {v0, v1}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 2406059
    sget-object v1, LX/3CC;->c:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4or;->a(LX/0Tn;)V

    .line 2406060
    sget-object v1, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 2406061
    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "Unset (Use GK value)"

    aput-object v2, v1, v3

    const-string v2, "Yes"

    aput-object v2, v1, v4

    const-string v2, "No"

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 2406062
    new-array v1, v6, [Ljava/lang/String;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    sget-object v2, LX/03R;->YES:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    sget-object v2, LX/03R;->NO:LX/03R;

    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2406063
    new-instance v1, LX/Gvk;

    invoke-direct {v1, p0}, LX/Gvk;-><init>(LX/Gvl;)V

    invoke-virtual {v0, v1}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2406064
    invoke-virtual {p0, v0}, LX/Gvl;->addPreference(Landroid/preference/Preference;)Z

    .line 2406065
    return-void
.end method
