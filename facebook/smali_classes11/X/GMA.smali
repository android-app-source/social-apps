.class public final enum LX/GMA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GMA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GMA;

.field public static final enum DETAIL:LX/GMA;

.field public static final enum GENERIC:LX/GMA;

.field public static final enum NONE:LX/GMA;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2344057
    new-instance v0, LX/GMA;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/GMA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMA;->NONE:LX/GMA;

    .line 2344058
    new-instance v0, LX/GMA;

    const-string v1, "GENERIC"

    invoke-direct {v0, v1, v3}, LX/GMA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMA;->GENERIC:LX/GMA;

    .line 2344059
    new-instance v0, LX/GMA;

    const-string v1, "DETAIL"

    invoke-direct {v0, v1, v4}, LX/GMA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMA;->DETAIL:LX/GMA;

    .line 2344060
    const/4 v0, 0x3

    new-array v0, v0, [LX/GMA;

    sget-object v1, LX/GMA;->NONE:LX/GMA;

    aput-object v1, v0, v2

    sget-object v1, LX/GMA;->GENERIC:LX/GMA;

    aput-object v1, v0, v3

    sget-object v1, LX/GMA;->DETAIL:LX/GMA;

    aput-object v1, v0, v4

    sput-object v0, LX/GMA;->$VALUES:[LX/GMA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2344061
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GMA;
    .locals 1

    .prologue
    .line 2344062
    const-class v0, LX/GMA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GMA;

    return-object v0
.end method

.method public static values()[LX/GMA;
    .locals 1

    .prologue
    .line 2344063
    sget-object v0, LX/GMA;->$VALUES:[LX/GMA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GMA;

    return-object v0
.end method
