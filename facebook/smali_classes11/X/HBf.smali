.class public LX/HBf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HBf;


# instance fields
.field public final a:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2437764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2437765
    iput-object p1, p0, LX/HBf;->a:LX/0if;

    .line 2437766
    return-void
.end method

.method public static a(LX/0QB;)LX/HBf;
    .locals 4

    .prologue
    .line 2437767
    sget-object v0, LX/HBf;->b:LX/HBf;

    if-nez v0, :cond_1

    .line 2437768
    const-class v1, LX/HBf;

    monitor-enter v1

    .line 2437769
    :try_start_0
    sget-object v0, LX/HBf;->b:LX/HBf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2437770
    if-eqz v2, :cond_0

    .line 2437771
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2437772
    new-instance p0, LX/HBf;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/HBf;-><init>(LX/0if;)V

    .line 2437773
    move-object v0, p0

    .line 2437774
    sput-object v0, LX/HBf;->b:LX/HBf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2437775
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2437776
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2437777
    :cond_1
    sget-object v0, LX/HBf;->b:LX/HBf;

    return-object v0

    .line 2437778
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2437779
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/HBf;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2437760
    iget-object v0, p0, LX/HBf;->a:LX/0if;

    sget-object v1, LX/0ig;->ap:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2437761
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 2437762
    iget-object v0, p0, LX/HBf;->a:LX/0if;

    sget-object v1, LX/0ig;->ap:LX/0ih;

    const-string v2, "redirect_fetch_complete"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2437763
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2437755
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "error_type"

    invoke-virtual {v0, v1, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v1, "error_message"

    invoke-virtual {v0, v1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2437756
    iget-object v1, p0, LX/HBf;->a:LX/0if;

    sget-object v2, LX/0ig;->ap:LX/0ih;

    const-string v3, "redirect_fetch_complete"

    const-string v4, "error"

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2437757
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2437758
    iget-object v0, p0, LX/HBf;->a:LX/0if;

    sget-object v1, LX/0ig;->ap:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2437759
    return-void
.end method
