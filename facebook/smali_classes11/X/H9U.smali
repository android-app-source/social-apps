.class public final LX/H9U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8E8;


# instance fields
.field public final synthetic a:LX/0Rl;

.field public final synthetic b:LX/4At;

.field public final synthetic c:LX/H9V;


# direct methods
.method public constructor <init>(LX/H9V;LX/0Rl;LX/4At;)V
    .locals 0

    .prologue
    .line 2434377
    iput-object p1, p0, LX/H9U;->c:LX/H9V;

    iput-object p2, p0, LX/H9U;->a:LX/0Rl;

    iput-object p3, p0, LX/H9U;->b:LX/4At;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2434378
    iget-object v0, p0, LX/H9U;->c:LX/H9V;

    iget-object v0, v0, LX/H9V;->j:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2434379
    iget-object v0, p0, LX/H9U;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2434380
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1

    .prologue
    .line 2434381
    iget-object v0, p0, LX/H9U;->a:LX/0Rl;

    invoke-interface {v0, p1}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    .line 2434382
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2434383
    iget-object v0, p0, LX/H9U;->c:LX/H9V;

    iget-object v0, v0, LX/H9V;->j:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2434384
    iget-object v0, p0, LX/H9U;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2434385
    iget-object v0, p0, LX/H9U;->c:LX/H9V;

    iget-object v0, v0, LX/H9V;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/H9U;->c:LX/H9V;

    iget-object v2, v2, LX/H9V;->j:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081829

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2434386
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1

    .prologue
    .line 2434387
    iget-object v0, p0, LX/H9U;->c:LX/H9V;

    iget-object v0, v0, LX/H9V;->j:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2434388
    iget-object v0, p0, LX/H9U;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2434389
    iget-object v0, p0, LX/H9U;->a:LX/0Rl;

    invoke-interface {v0, p1}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    .line 2434390
    :cond_0
    return-void
.end method
