.class public LX/GLK;
.super LX/GIr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GIr",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private q:LX/GL7;


# direct methods
.method public constructor <init>(LX/GG6;LX/GL2;LX/GL7;LX/GKy;LX/2U3;LX/0W9;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2342598
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/GIr;-><init>(LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V

    .line 2342599
    iput-object p3, p0, LX/GLK;->q:LX/GL7;

    .line 2342600
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 4

    .prologue
    .line 2342601
    invoke-super {p0, p1, p2}, LX/GIr;->a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2342602
    iget-object v1, p0, LX/GLK;->q:LX/GL7;

    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    .line 2342603
    iget-object p1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    move-object v0, p1

    .line 2342604
    invoke-virtual {v1, v0, p2}, LX/GL7;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2342605
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2342606
    invoke-virtual {p0}, LX/GIr;->d()V

    .line 2342607
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2342608
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2342609
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v0, v3

    .line 2342610
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {p0, v3}, LX/GIr;->e(Z)V

    .line 2342611
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {p0, v3}, LX/GIr;->a(Z)V

    .line 2342612
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {p0, v3}, LX/GIr;->c(Z)V

    .line 2342613
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/GIr;->b(Z)V

    .line 2342614
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2342615
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v0, v3

    .line 2342616
    if-nez v0, :cond_0

    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2342617
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v0, v3

    .line 2342618
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v3, :cond_1

    invoke-static {p0}, LX/GLK;->g(LX/GLK;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    const/16 p2, 0x8

    const/4 p1, 0x0

    .line 2342619
    if-nez v0, :cond_3

    .line 2342620
    iget-object v3, p0, LX/GIr;->f:LX/GIa;

    check-cast v3, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {v3, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->setAudienceNameEditVisibility(I)V

    .line 2342621
    iget-object v3, p0, LX/GIr;->f:LX/GIa;

    check-cast v3, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {v3, p2}, LX/GIa;->setLocationSelectorDividerVisibility(I)V

    .line 2342622
    :goto_1
    iget-object v0, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2342623
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v0, v3

    .line 2342624
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v3, :cond_2

    invoke-static {p0}, LX/GLK;->g(LX/GLK;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_2
    const/4 v1, 0x0

    .line 2342625
    if-nez v2, :cond_4

    .line 2342626
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->setSaveAudienceRowVisibility(I)V

    .line 2342627
    :goto_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/GIr;->f(Z)V

    .line 2342628
    return-void

    :cond_1
    move v0, v1

    .line 2342629
    goto :goto_0

    :cond_2
    move v2, v1

    .line 2342630
    goto :goto_2

    .line 2342631
    :cond_3
    iget-object v3, p0, LX/GIr;->f:LX/GIa;

    check-cast v3, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {v3, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->setAudienceNameEditVisibility(I)V

    .line 2342632
    iget-object v3, p0, LX/GIr;->f:LX/GIa;

    check-cast v3, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {v3, p1}, LX/GIa;->setLocationSelectorDividerVisibility(I)V

    .line 2342633
    iget-object v3, p0, LX/GIr;->f:LX/GIa;

    check-cast v3, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    iget-object p1, p0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object p1

    .line 2342634
    iget-object p2, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object p1, p2

    .line 2342635
    invoke-virtual {v3, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->setAudienceNameText(Ljava/lang/String;)V

    .line 2342636
    iget-object v3, p0, LX/GIr;->f:LX/GIa;

    check-cast v3, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    new-instance p1, LX/GLI;

    invoke-direct {p1, p0}, LX/GLI;-><init>(LX/GLK;)V

    invoke-virtual {v3, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->setAudienceEditTextViewListener(Landroid/text/TextWatcher;)V

    goto :goto_1

    .line 2342637
    :cond_4
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->setSaveAudienceRowVisibility(I)V

    .line 2342638
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    .line 2342639
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    move-object v0, v2

    .line 2342640
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->setSaveButtonVisibility(Z)V

    .line 2342641
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    .line 2342642
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    move-object v0, v1

    .line 2342643
    new-instance v1, LX/GLJ;

    invoke-direct {v1, p0}, LX/GLJ;-><init>(LX/GLK;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->setSaveAudienceCheckBoxListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_3
.end method

.method public static c(LX/0QB;)LX/GLK;
    .locals 7

    .prologue
    .line 2342644
    new-instance v0, LX/GLK;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v1

    check-cast v1, LX/GG6;

    invoke-static {p0}, LX/GL2;->b(LX/0QB;)LX/GL2;

    move-result-object v2

    check-cast v2, LX/GL2;

    invoke-static {p0}, LX/GL7;->b(LX/0QB;)LX/GL7;

    move-result-object v3

    check-cast v3, LX/GL7;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v4

    check-cast v4, LX/GKy;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v5

    check-cast v5, LX/2U3;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    invoke-direct/range {v0 .. v6}, LX/GLK;-><init>(LX/GG6;LX/GL2;LX/GL7;LX/GKy;LX/2U3;LX/0W9;)V

    .line 2342645
    return-object v0
.end method

.method public static g(LX/GLK;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2342646
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2342647
    iget-object v3, v2, LX/GCE;->e:LX/0ad;

    move-object v2, v3

    .line 2342648
    sget v3, LX/GDK;->k:I

    invoke-interface {v2, v3, v1}, LX/0ad;->a(II)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2342649
    invoke-super {p0}, LX/GIr;->a()V

    .line 2342650
    iget-object v0, p0, LX/GLK;->q:LX/GL7;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2342651
    const/4 v0, 0x0

    iput-object v0, p0, LX/GLK;->f:LX/GIa;

    .line 2342652
    return-void
.end method

.method public final a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2342653
    invoke-super {p0, p1}, LX/GIr;->a(LX/GCE;)V

    .line 2342654
    iget-object v0, p0, LX/GLK;->q:LX/GL7;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2342655
    return-void
.end method

.method public final bridge synthetic a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2342656
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-direct {p0, p1, p2}, LX/GLK;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2342657
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-direct {p0, p1, p2}, LX/GLK;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2342658
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342659
    invoke-super {p0, p1}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2342660
    iget-object v0, p0, LX/GLK;->q:LX/GL7;

    .line 2342661
    iput-object p1, v0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342662
    return-void
.end method
