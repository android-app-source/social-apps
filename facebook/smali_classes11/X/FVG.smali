.class public final LX/FVG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/FV3;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/FV3;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 2250500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250501
    iput-object p1, p0, LX/FVG;->a:LX/0QB;

    .line 2250502
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2250503
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/FVG;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2250504
    packed-switch p2, :pswitch_data_0

    .line 2250505
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2250506
    :pswitch_0
    invoke-static {p1}, LX/FV4;->a(LX/0QB;)LX/FV4;

    move-result-object v0

    .line 2250507
    :goto_0
    return-object v0

    .line 2250508
    :pswitch_1
    invoke-static {p1}, LX/FV8;->a(LX/0QB;)LX/FV8;

    move-result-object v0

    goto :goto_0

    .line 2250509
    :pswitch_2
    new-instance p0, LX/FV9;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p1}, LX/FWX;->b(LX/0QB;)LX/FWX;

    move-result-object v1

    check-cast v1, LX/FWX;

    invoke-direct {p0, v0, v1}, LX/FV9;-><init>(Landroid/content/res/Resources;LX/FWX;)V

    .line 2250510
    move-object v0, p0

    .line 2250511
    goto :goto_0

    .line 2250512
    :pswitch_3
    new-instance p2, LX/FVA;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p1}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {p1}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object p0

    check-cast p0, LX/1nC;

    invoke-direct {p2, v0, v1, p0}, LX/FVA;-><init>(Landroid/content/res/Resources;LX/1Kf;LX/1nC;)V

    .line 2250513
    move-object v0, p2

    .line 2250514
    goto :goto_0

    .line 2250515
    :pswitch_4
    invoke-static {p1}, LX/FVC;->a(LX/0QB;)LX/FVC;

    move-result-object v0

    goto :goto_0

    .line 2250516
    :pswitch_5
    new-instance p2, LX/FVD;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p1}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v1

    check-cast v1, LX/0hy;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p2, v0, v1, p0}, LX/FVD;-><init>(Landroid/content/res/Resources;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V

    .line 2250517
    move-object v0, p2

    .line 2250518
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2250519
    const/4 v0, 0x6

    return v0
.end method
