.class public final LX/FX6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2253920
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2253921
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2253922
    :goto_0
    return v1

    .line 2253923
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2253924
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2253925
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2253926
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2253927
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2253928
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2253929
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2253930
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 2253931
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2253932
    invoke-static {p0, p1}, LX/FXK;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2253933
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2253934
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2253935
    goto :goto_1

    .line 2253936
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2253937
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2253938
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_a

    .line 2253939
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2253940
    :goto_3
    move v0, v3

    .line 2253941
    goto :goto_1

    .line 2253942
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2253943
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2253944
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2253945
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2253946
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 2253947
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2253948
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2253949
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_6

    if-eqz v6, :cond_6

    .line 2253950
    const-string v7, "has_next_page"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2253951
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    goto :goto_4

    .line 2253952
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2253953
    :cond_8
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2253954
    if-eqz v0, :cond_9

    .line 2253955
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 2253956
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_a
    move v0, v3

    move v5, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2253957
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2253958
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2253959
    if-eqz v0, :cond_1

    .line 2253960
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2253961
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2253962
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2253963
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/FXK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2253964
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2253965
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2253966
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2253967
    if-eqz v0, :cond_3

    .line 2253968
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2253969
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2253970
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2253971
    if-eqz v1, :cond_2

    .line 2253972
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2253973
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2253974
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2253975
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2253976
    return-void
.end method
