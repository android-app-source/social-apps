.class public abstract LX/GmZ;
.super LX/Chc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<REQUEST:",
        "Ljava/lang/Object;",
        "RESU",
        "LT:Ljava/lang/Object;",
        ">",
        "LX/Chc",
        "<TREQUEST;TRESU",
        "LT;",
        ">;",
        "Lcom/facebook/richdocument/view/util/RichDocumentBlocksFetchProgressUpdater$BlocksFetchProgressListener;"
    }
.end annotation


# instance fields
.field public B:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/CjL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cl5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ClD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cta;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cl1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IXQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IXX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bM;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chj;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public V:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ClO;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public W:Landroid/os/Handler;

.field private X:Lcom/facebook/instantarticles/view/ShareBar;

.field private Y:LX/CoC;

.field public Z:LX/Csl;

.field public aa:Landroid/view/ViewStub;

.field public ab:Landroid/widget/ProgressBar;

.field public ac:LX/Csi;

.field public ad:LX/Cry;

.field public ae:Ljava/lang/Runnable;

.field private af:Landroid/widget/FrameLayout;

.field private ag:Z

.field private ah:Z

.field private ai:Z

.field private aj:Z

.field private ak:Z

.field private al:LX/Clo;

.field private final am:LX/Ci1;

.field private final an:LX/Ci0;

.field private final ao:LX/Chz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2392420
    invoke-direct {p0}, LX/Chc;-><init>()V

    .line 2392421
    iput-boolean v1, p0, LX/GmZ;->ag:Z

    .line 2392422
    iput-boolean v1, p0, LX/GmZ;->ah:Z

    .line 2392423
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GmZ;->ai:Z

    .line 2392424
    iput-boolean v1, p0, LX/GmZ;->aj:Z

    .line 2392425
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/GmZ;->W:Landroid/os/Handler;

    .line 2392426
    new-instance v0, LX/GmU;

    invoke-direct {v0, p0}, LX/GmU;-><init>(LX/GmZ;)V

    iput-object v0, p0, LX/GmZ;->am:LX/Ci1;

    .line 2392427
    new-instance v0, LX/GmV;

    invoke-direct {v0, p0}, LX/GmV;-><init>(LX/GmZ;)V

    iput-object v0, p0, LX/GmZ;->an:LX/Ci0;

    .line 2392428
    new-instance v0, LX/GmW;

    invoke-direct {v0, p0}, LX/GmW;-><init>(LX/GmZ;)V

    iput-object v0, p0, LX/GmZ;->ao:LX/Chz;

    .line 2392429
    return-void
.end method

.method private Y()V
    .locals 1

    .prologue
    .line 2392374
    iget-boolean v0, p0, LX/GmZ;->ah:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/GmZ;->ai:Z

    if-eqz v0, :cond_0

    .line 2392375
    invoke-virtual {p0}, LX/Chc;->p()V

    .line 2392376
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GmZ;->ai:Z

    .line 2392377
    :cond_0
    return-void
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 2392378
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_2

    .line 2392379
    iget-boolean v0, p0, LX/GmZ;->aj:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/GmZ;->ai:Z

    if-nez v0, :cond_1

    .line 2392380
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/Chc;->r()V

    .line 2392381
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GmZ;->ai:Z

    .line 2392382
    :cond_1
    return-void

    .line 2392383
    :cond_2
    iget-object v0, p0, LX/GmZ;->S:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bM;

    .line 2392384
    iget v1, v0, LX/8bM;->b:I

    move v0, v1

    .line 2392385
    if-nez v0, :cond_3

    iget-boolean v0, p0, LX/GmZ;->ah:Z

    if-nez v0, :cond_0

    :cond_3
    iget-boolean v0, p0, LX/GmZ;->ah:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LX/GmZ;->ai:Z

    if-nez v0, :cond_1

    goto :goto_0
.end method

.method private static a(LX/GmZ;LX/193;LX/CjL;LX/0Ot;LX/0Ot;LX/03V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/Chi;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GmZ;",
            "LX/193;",
            "LX/CjL;",
            "LX/0Ot",
            "<",
            "LX/8bK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/Cl5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ClD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cta;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cl1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/IXQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/Chi;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/IXX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8bM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Chj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ckt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ClO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2392386
    iput-object p1, p0, LX/GmZ;->B:LX/193;

    iput-object p2, p0, LX/GmZ;->C:LX/CjL;

    iput-object p3, p0, LX/GmZ;->D:LX/0Ot;

    iput-object p4, p0, LX/GmZ;->E:LX/0Ot;

    iput-object p5, p0, LX/GmZ;->F:LX/03V;

    iput-object p6, p0, LX/GmZ;->G:LX/0Ot;

    iput-object p7, p0, LX/GmZ;->H:LX/0Ot;

    iput-object p8, p0, LX/GmZ;->I:LX/0Ot;

    iput-object p9, p0, LX/GmZ;->J:LX/0Ot;

    iput-object p10, p0, LX/GmZ;->K:LX/0Ot;

    iput-object p11, p0, LX/GmZ;->L:LX/0Ot;

    iput-object p12, p0, LX/GmZ;->M:LX/Chi;

    iput-object p13, p0, LX/GmZ;->N:LX/0Ot;

    iput-object p14, p0, LX/GmZ;->O:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/GmZ;->P:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/GmZ;->Q:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/GmZ;->R:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/GmZ;->S:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/GmZ;->T:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/GmZ;->U:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/GmZ;->V:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 25

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v23

    move-object/from16 v2, p0

    check-cast v2, LX/GmZ;

    const-class v3, LX/193;

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/193;

    const-class v4, LX/CjL;

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/CjL;

    const/16 v5, 0x3230

    move-object/from16 v0, v23

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x11e9

    move-object/from16 v0, v23

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v23 .. v23}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    const/16 v8, 0x321a

    move-object/from16 v0, v23

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x321c

    move-object/from16 v0, v23

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x324a

    move-object/from16 v0, v23

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3219

    move-object/from16 v0, v23

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2523

    move-object/from16 v0, v23

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xac0

    move-object/from16 v0, v23

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v23 .. v23}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v14

    check-cast v14, LX/Chi;

    const/16 v15, 0x1032

    move-object/from16 v0, v23

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0xac0

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x3231

    move-object/from16 v0, v23

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x3216

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x2529

    move-object/from16 v0, v23

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x3232

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x31dd

    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x3214

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v24, 0x321f

    invoke-static/range {v23 .. v24}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    invoke-static/range {v2 .. v23}, LX/GmZ;->a(LX/GmZ;LX/193;LX/CjL;LX/0Ot;LX/0Ot;LX/03V;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/Chi;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private ab()V
    .locals 1

    .prologue
    .line 2392387
    iget-boolean v0, p0, LX/GmZ;->ah:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/GmZ;->ag:Z

    if-nez v0, :cond_1

    .line 2392388
    :cond_0
    :goto_0
    return-void

    .line 2392389
    :cond_1
    invoke-virtual {p0}, LX/GmZ;->R()V

    goto :goto_0
.end method


# virtual methods
.method public A()LX/3x6;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2392390
    iget-object v0, p0, LX/GmZ;->Y:LX/CoC;

    return-object v0
.end method

.method public final I()V
    .locals 9

    .prologue
    .line 2392391
    invoke-super {p0}, LX/Chc;->I()V

    .line 2392392
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392393
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392394
    const-string v2, "is_called_by_sample_app"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move v1, v1

    .line 2392395
    iput-boolean v1, v0, LX/Chi;->m:Z

    .line 2392396
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2392397
    const-string v1, "extra_instant_articles_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2392398
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2392399
    const-string v2, "extra_instant_articles_referrer"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2392400
    iget-object v0, p0, LX/GmZ;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    .line 2392401
    iput-object v1, v0, LX/Ckw;->h:Ljava/lang/String;

    .line 2392402
    iput-object v2, v0, LX/Ckw;->i:Ljava/lang/String;

    .line 2392403
    iget-object v0, p0, LX/GmZ;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const-string v3, "native_article_prelaunch"

    invoke-virtual {v0, v3}, LX/Ckw;->b(Ljava/lang/String;)V

    .line 2392404
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_0

    .line 2392405
    iget-object v0, p0, LX/GmZ;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cl1;

    const/4 v6, 0x0

    .line 2392406
    const/4 v5, 0x0

    invoke-static {v1, v5, v6, v6}, LX/Cl1;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2392407
    iget-object v6, v0, LX/Cl1;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v6

    sget-object v7, LX/2fv;->b:LX/0Tn;

    invoke-interface {v6, v7, v5}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v5

    sget-object v6, LX/2fv;->c:LX/0Tn;

    iget-object v7, v0, LX/Cl1;->b:LX/0So;

    invoke-interface {v7}, LX/0So;->now()J

    move-result-wide v7

    invoke-interface {v5, v6, v7, v8}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 2392408
    :cond_0
    iget-object v0, p0, LX/GmZ;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cl5;

    .line 2392409
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 2392410
    const-string v4, "extra_instant_articles_click_url"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2392411
    iput-object v3, v0, LX/Cl5;->p:Ljava/lang/String;

    .line 2392412
    iput-object v1, v0, LX/Cl5;->q:Ljava/lang/String;

    .line 2392413
    iput-object v2, v0, LX/Cl5;->r:Ljava/lang/String;

    .line 2392414
    return-void
.end method

.method public final P()Z
    .locals 3

    .prologue
    .line 2392415
    iget-object v0, p0, LX/GmZ;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bL;

    const/4 p0, 0x0

    .line 2392416
    iget-object v1, v0, LX/8bL;->b:LX/0ad;

    sget-short v2, LX/2yD;->p:S

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2392417
    const/4 v1, 0x1

    .line 2392418
    :goto_0
    move v0, v1

    .line 2392419
    return v0

    :cond_0
    iget-object v1, v0, LX/8bL;->a:LX/0Uh;

    const/16 v2, 0xb1

    invoke-virtual {v1, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    goto :goto_0
.end method

.method public abstract Q()Ljava/lang/String;
.end method

.method public R()V
    .locals 7

    .prologue
    .line 2392436
    iget-object v0, p0, LX/GmZ;->al:LX/Clo;

    if-eqz v0, :cond_0

    .line 2392437
    iget-object v0, p0, LX/GmZ;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cl1;

    iget-object v1, p0, LX/GmZ;->al:LX/Clo;

    .line 2392438
    iget-object v2, v1, LX/Clo;->b:Landroid/os/Bundle;

    move-object v2, v2

    .line 2392439
    iget-object v3, v1, LX/Clo;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2392440
    const-string v4, "version"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "title"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "url"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v5, v2}, LX/Cl1;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2392441
    iget-object v3, v0, LX/Cl1;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/2fv;->b:LX/0Tn;

    invoke-interface {v3, v4, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v3, LX/2fv;->c:LX/0Tn;

    iget-object v4, v0, LX/Cl1;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2392442
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2392443
    const v0, 0x7f0d1679

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2392433
    invoke-super {p0, p1}, LX/Chc;->a(Landroid/content/Context;)V

    .line 2392434
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, LX/GmZ;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2392435
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2392430
    invoke-super {p0, p1}, LX/Chc;->a(Landroid/os/Bundle;)V

    .line 2392431
    iget-object v0, p0, LX/GmZ;->P:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bL;

    invoke-virtual {v0}, LX/8bL;->h()Z

    move-result v0

    iput-boolean v0, p0, LX/GmZ;->ak:Z

    .line 2392432
    return-void
.end method

.method public final b(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/1P1;
    .locals 2

    .prologue
    .line 2392309
    new-instance v0, LX/CqR;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/CqR;-><init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;)V

    return-object v0
.end method

.method public b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2392310
    iget-object v0, p0, LX/GmZ;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11i;

    invoke-virtual {p0}, LX/GmZ;->O()LX/0Pq;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    .line 2392311
    if-eqz v2, :cond_0

    .line 2392312
    const-string v0, "instant_articles_base_view_creation"

    const v1, -0x5959800f

    invoke-static {v2, v0, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2392313
    :cond_0
    invoke-virtual {p0}, LX/GmZ;->Q()Ljava/lang/String;

    move-result-object v0

    .line 2392314
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392315
    const-string v3, "tracking_codes"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2392316
    iget-object v3, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v3, v3

    .line 2392317
    const-string v4, "extra_instant_articles_click_url"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2392318
    iget-object v4, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v4, v4

    .line 2392319
    const-string v5, "extra_instant_articles_canonical_url"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2392320
    iget-object v5, p0, LX/GmZ;->M:LX/Chi;

    .line 2392321
    iput-object v0, v5, LX/Chi;->c:Ljava/lang/String;

    .line 2392322
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392323
    const/4 v5, 0x0

    iput-object v5, v0, LX/Chi;->g:LX/0lF;

    .line 2392324
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2392325
    :try_start_0
    iget-object v5, v0, LX/Chi;->a:LX/0lC;

    invoke-virtual {v5, v1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    iput-object v5, v0, LX/Chi;->g:LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2392326
    :cond_1
    :goto_0
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392327
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392328
    const-string v5, "extra_instant_articles_saved"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 2392329
    iput-boolean v1, v0, LX/Chi;->h:Z

    .line 2392330
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392331
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392332
    const-string v5, "richdocument_fragment_tag"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2392333
    iput-object v1, v0, LX/Chi;->i:Ljava/lang/String;

    .line 2392334
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392335
    iput-object v4, v0, LX/Chi;->d:Ljava/lang/String;

    .line 2392336
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392337
    iput-object v3, v0, LX/Chi;->e:Ljava/lang/String;

    .line 2392338
    new-instance v0, LX/CoC;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CoC;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/GmZ;->Y:LX/CoC;

    .line 2392339
    if-eqz v2, :cond_2

    .line 2392340
    const-string v0, "instant_articles_base_view_init"

    const v1, 0x70d3eef0

    invoke-static {v2, v0, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2392341
    :cond_2
    invoke-super {p0, p1, p2, p3}, LX/Chc;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 2392342
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_3

    .line 2392343
    iget-object v0, p0, LX/GmZ;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chj;

    invoke-virtual {v0, p0}, LX/Chj;->a(LX/ChL;)V

    .line 2392344
    const v0, 0x7f0d167d

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/ShareBar;

    iput-object v0, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    .line 2392345
    iget-object v0, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    if-eqz v0, :cond_3

    .line 2392346
    iget-object v0, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, p0, LX/GmZ;->M:LX/Chi;

    .line 2392347
    iput-object v1, v0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2392348
    iget-object v0, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    new-instance v1, LX/GmX;

    invoke-direct {v1, p0}, LX/GmX;-><init>(LX/GmZ;)V

    .line 2392349
    iput-object v1, v0, Lcom/facebook/instantarticles/view/ShareBar;->A:LX/IYL;

    .line 2392350
    :cond_3
    iget-object v0, p0, LX/GmZ;->aa:Landroid/view/ViewStub;

    if-eqz v0, :cond_7

    .line 2392351
    iget-object v0, p0, LX/GmZ;->aa:Landroid/view/ViewStub;

    .line 2392352
    :goto_1
    move-object v0, v0

    .line 2392353
    iput-object v0, p0, LX/GmZ;->aa:Landroid/view/ViewStub;

    .line 2392354
    const v0, 0x7f0d1680

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/GmZ;->ab:Landroid/widget/ProgressBar;

    .line 2392355
    const v0, 0x7f0d167c

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/Csi;

    move-object v0, v0

    .line 2392356
    iput-object v0, p0, LX/GmZ;->ac:LX/Csi;

    .line 2392357
    iget-object v0, p0, LX/GmZ;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bK;

    invoke-virtual {v0}, LX/8bK;->a()LX/0p3;

    move-result-object v0

    .line 2392358
    sget-object v1, LX/0p3;->GOOD:LX/0p3;

    invoke-virtual {v0, v1}, LX/0p3;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    if-gez v0, :cond_6

    sget-wide v0, LX/CoL;->N:J

    .line 2392359
    :goto_2
    new-instance v4, LX/Cry;

    sget-wide v6, LX/CoL;->P:J

    sub-long/2addr v0, v6

    invoke-direct {v4, p0, v0, v1}, LX/Cry;-><init>(LX/GmZ;J)V

    iput-object v4, p0, LX/GmZ;->ad:LX/Cry;

    .line 2392360
    new-instance v0, Lcom/facebook/instantarticles/BaseInstantArticlesDelegateImpl$StartFakeProgressUpdaterRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/instantarticles/BaseInstantArticlesDelegateImpl$StartFakeProgressUpdaterRunnable;-><init>(LX/GmZ;)V

    iput-object v0, p0, LX/GmZ;->ae:Ljava/lang/Runnable;

    .line 2392361
    const v0, 0x7f0d167b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/GmZ;->af:Landroid/widget/FrameLayout;

    .line 2392362
    if-eqz v2, :cond_4

    .line 2392363
    const-string v0, "instant_articles_base_view_init"

    const v1, 0x3c12d5d0

    invoke-static {v2, v0, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2392364
    :cond_4
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    move-object v0, v0

    .line 2392365
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    iget-object v1, p0, LX/GmZ;->am:LX/Ci1;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2392366
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    move-object v0, v0

    .line 2392367
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    iget-object v1, p0, LX/GmZ;->an:LX/Ci0;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2392368
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    move-object v0, v0

    .line 2392369
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    iget-object v1, p0, LX/GmZ;->ao:LX/Chz;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2392370
    if-eqz v2, :cond_5

    .line 2392371
    const-string v0, "instant_articles_base_view_creation"

    const v1, 0x51a48779

    invoke-static {v2, v0, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2392372
    :cond_5
    return-object v3

    .line 2392373
    :cond_6
    sget-wide v0, LX/CoL;->O:J

    goto :goto_2

    :catch_0
    goto/16 :goto_0

    :cond_7
    const v0, 0x7f0d1681

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    goto/16 :goto_1
.end method

.method public b(LX/Clo;)V
    .locals 3

    .prologue
    .line 2392110
    iput-object p1, p0, LX/GmZ;->al:LX/Clo;

    .line 2392111
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2392112
    iget-object v1, p1, LX/Clo;->b:Landroid/os/Bundle;

    move-object v1, v1

    .line 2392113
    const-string v2, "background_color"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, LX/8ba;->a(Landroid/view/View;I)V

    .line 2392114
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392115
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 2392116
    if-eqz v0, :cond_1

    .line 2392117
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392118
    iget-object v1, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v1

    .line 2392119
    iget-object v1, p0, LX/Chc;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Yh;

    iget-object v2, p0, LX/Chc;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8Yg;

    invoke-virtual {v2, v0}, LX/8Yg;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)Ljava/util/Map;

    move-result-object v2

    .line 2392120
    iput-object v2, v1, LX/8Yh;->a:Ljava/util/Map;

    .line 2392121
    iget-object v1, p0, LX/Chc;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Yh;

    iget-object v2, p0, LX/Chc;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8Yg;

    .line 2392122
    new-instance v2, LX/8Yk;

    invoke-direct {v2, v0}, LX/8Yk;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;)V

    .line 2392123
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 2392124
    iget-object p0, v2, LX/8Yk;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    if-eqz p0, :cond_0

    iget-object p0, v2, LX/8Yk;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;

    move-result-object p0

    if-nez p0, :cond_2

    .line 2392125
    :cond_0
    :goto_0
    move-object v2, p1

    .line 2392126
    const/4 p1, 0x0

    invoke-static {v2, p1}, LX/8Yg;->a(Ljava/util/Set;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    move-object v2, v2

    .line 2392127
    iput-object v2, v1, LX/8Yh;->b:Ljava/util/Map;

    .line 2392128
    :cond_1
    return-void

    .line 2392129
    :cond_2
    iget-object p0, v2, LX/8Yk;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;

    move-result-object p0

    .line 2392130
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392131
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->x()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392132
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->w()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392133
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392134
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->b()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392135
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->p()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392136
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392137
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->u()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392138
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->t()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392139
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->v()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392140
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392141
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->m()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392142
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392143
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->k()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392144
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->j()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392145
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->er_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392146
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->es_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392147
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392148
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392149
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 2392150
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel$FallbackArticleStyleModel;->s()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;

    move-result-object p0

    invoke-static {p1, p0}, LX/8Yk;->a(Ljava/util/Set;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;)V

    goto/16 :goto_0
.end method

.method public final c(LX/Clo;)LX/1OM;
    .locals 7

    .prologue
    .line 2392151
    new-instance v0, LX/CoJ;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/GmZ;->C:LX/CjL;

    invoke-virtual {p0}, LX/GmZ;->O()LX/0Pq;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/CjL;->a(LX/0Pq;)LX/CjK;

    move-result-object v3

    .line 2392152
    iget-object v2, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v2, v2

    .line 2392153
    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v4

    check-cast v4, LX/CqR;

    .line 2392154
    iget-object v2, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v5, v2

    .line 2392155
    invoke-virtual {p0}, LX/GmZ;->O()LX/0Pq;

    move-result-object v6

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, LX/CoJ;-><init>(Landroid/content/Context;LX/Clo;LX/CjK;LX/1P1;Landroid/support/v7/widget/RecyclerView;LX/0Pq;)V

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2392156
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-eqz v0, :cond_1

    .line 2392157
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GmZ;->aj:Z

    .line 2392158
    invoke-direct {p0}, LX/GmZ;->Y()V

    .line 2392159
    :cond_0
    :goto_0
    return-void

    .line 2392160
    :cond_1
    iget-boolean v0, p0, LX/GmZ;->ak:Z

    if-eqz v0, :cond_3

    .line 2392161
    iget-object v0, p0, LX/GmZ;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chj;

    invoke-virtual {v0}, LX/Chj;->c()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/GmZ;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chj;

    invoke-virtual {v0}, LX/Chj;->a()LX/ChL;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 2392162
    :cond_2
    invoke-super {p0}, LX/Chc;->e()V

    goto :goto_0

    .line 2392163
    :cond_3
    invoke-super {p0}, LX/Chc;->e()V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2392164
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-eqz v0, :cond_1

    .line 2392165
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GmZ;->aj:Z

    .line 2392166
    invoke-direct {p0}, LX/GmZ;->Z()V

    .line 2392167
    :cond_0
    :goto_0
    return-void

    .line 2392168
    :cond_1
    iget-boolean v0, p0, LX/GmZ;->ak:Z

    if-eqz v0, :cond_3

    .line 2392169
    iget-object v0, p0, LX/GmZ;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chj;

    invoke-virtual {v0}, LX/Chj;->c()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/GmZ;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chj;

    invoke-virtual {v0}, LX/Chj;->a()LX/ChL;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 2392170
    :cond_2
    invoke-super {p0}, LX/Chc;->f()V

    goto :goto_0

    .line 2392171
    :cond_3
    invoke-super {p0}, LX/Chc;->f()V

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2392172
    invoke-super {p0}, LX/Chc;->g()V

    .line 2392173
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GmZ;->ag:Z

    .line 2392174
    iget-boolean v0, p0, LX/Chc;->A:Z

    if-nez v0, :cond_0

    .line 2392175
    iget-object v0, p0, LX/GmZ;->T:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chj;

    invoke-virtual {v0, p0}, LX/Chj;->b(LX/ChL;)V

    .line 2392176
    :cond_0
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXX;

    const-wide/16 v1, 0x0

    .line 2392177
    iput-wide v1, v0, LX/IXX;->n:J

    .line 2392178
    iput-wide v1, v0, LX/IXX;->o:J

    .line 2392179
    const/4 v0, 0x0

    iput-object v0, p0, LX/GmZ;->al:LX/Clo;

    .line 2392180
    return-void
.end method

.method public final j()Landroid/app/Dialog;
    .locals 10

    .prologue
    const/high16 v5, 0x4000000

    const/high16 v4, -0x80000000

    .line 2392181
    iget-object v0, p0, LX/GmZ;->N:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/2yD;->M:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2392182
    iget-object v0, p0, LX/GmZ;->N:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v2, LX/2yD;->N:C

    const-string v3, "app_global"

    invoke-interface {v0, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2392183
    if-nez v1, :cond_0

    .line 2392184
    new-instance v0, LX/Cha;

    const v1, 0x1030011

    invoke-direct {v0, p0, v1}, LX/Cha;-><init>(LX/Chc;I)V

    .line 2392185
    :goto_0
    return-object v0

    .line 2392186
    :cond_0
    new-instance v1, LX/Cha;

    const v2, 0x1030010

    invoke-direct {v1, p0, v2}, LX/Cha;-><init>(LX/Chc;I)V

    .line 2392187
    if-eqz v0, :cond_3

    const-string v2, "app_global"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2392188
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 2392189
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 2392190
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    move-object v0, v1

    goto :goto_0

    .line 2392191
    :cond_1
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Landroid/view/Window;->setFlags(II)V

    .line 2392192
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2392193
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    .line 2392194
    const/4 v5, 0x0

    .line 2392195
    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const-string v7, "status_bar_height"

    const-string v8, "dimen"

    const-string v9, "android"

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 2392196
    if-lez v6, :cond_2

    .line 2392197
    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2392198
    :cond_2
    move v5, v5

    .line 2392199
    invoke-direct {v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2392200
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v4, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2392201
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2392202
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public m()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2392203
    invoke-super {p0}, LX/Chc;->m()V

    .line 2392204
    iget-object v0, p0, LX/GmZ;->L:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x3d8

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "instant_article_improved_scroll_perf"

    .line 2392205
    :goto_0
    iget-object v1, p0, LX/GmZ;->B:LX/193;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "native_article_story"

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    .line 2392206
    new-instance v1, LX/GmY;

    invoke-direct {v1, p0}, LX/GmY;-><init>(LX/GmZ;)V

    .line 2392207
    iput-object v1, v0, LX/195;->k:LX/1KG;

    .line 2392208
    iget-object v1, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v1, v1

    .line 2392209
    invoke-virtual {p0, v1, v0}, LX/Chc;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/195;)V

    .line 2392210
    return-void

    .line 2392211
    :cond_0
    const-string v0, "instant_article_scroll_perf"

    goto :goto_0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 2392212
    invoke-super {p0}, LX/Chc;->n()V

    .line 2392213
    iget-object v0, p0, LX/GmZ;->I:LX/0Ot;

    if-eqz v0, :cond_0

    .line 2392214
    iget-object v0, p0, LX/GmZ;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cta;

    .line 2392215
    iget-object v1, v0, LX/Cta;->d:LX/CtZ;

    sget-object p0, LX/CtZ;->PAUSED:LX/CtZ;

    if-ne v1, p0, :cond_0

    .line 2392216
    sget-object v1, LX/CtZ;->RESUMED:LX/CtZ;

    iput-object v1, v0, LX/Cta;->d:LX/CtZ;

    .line 2392217
    invoke-static {v0}, LX/Cta;->d(LX/Cta;)V

    .line 2392218
    :cond_0
    return-void
.end method

.method public final o()V
    .locals 8

    .prologue
    .line 2392271
    invoke-super {p0}, LX/Chc;->o()V

    .line 2392272
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GmZ;->ah:Z

    .line 2392273
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2392274
    const-string v1, "extra_instant_articles_referrer"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2392275
    const-string v2, "open_action"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2392276
    const-string v3, "external_click_time"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2392277
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXX;

    .line 2392278
    iput-object v1, v0, LX/IXX;->A:Ljava/lang/String;

    .line 2392279
    iput-wide v4, v0, LX/IXX;->k:J

    .line 2392280
    iget-object v6, v0, LX/IXX;->b:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    iput-wide v6, v0, LX/IXX;->p:J

    .line 2392281
    iget-object v0, p0, LX/GmZ;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    .line 2392282
    iput-object v1, v0, LX/Ckw;->i:Ljava/lang/String;

    .line 2392283
    iput-object v2, v0, LX/Ckw;->j:Ljava/lang/String;

    .line 2392284
    iget-object v0, p0, LX/GmZ;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cl5;

    .line 2392285
    iput-object v1, v0, LX/Cl5;->r:Ljava/lang/String;

    .line 2392286
    iget-object v0, p0, LX/GmZ;->M:LX/Chi;

    .line 2392287
    iput-object v1, v0, LX/Chi;->j:Ljava/lang/String;

    .line 2392288
    invoke-direct {p0}, LX/GmZ;->Y()V

    .line 2392289
    const/4 v2, 0x1

    .line 2392290
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2392291
    if-nez v0, :cond_2

    .line 2392292
    :cond_0
    :goto_0
    invoke-direct {p0}, LX/GmZ;->ab()V

    .line 2392293
    iget-object v0, p0, LX/GmZ;->K:LX/0Ot;

    if-eqz v0, :cond_1

    .line 2392294
    iget-object v0, p0, LX/GmZ;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXQ;

    iget-object v1, p0, LX/GmZ;->af:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/GmZ;->M:LX/Chi;

    .line 2392295
    iget-object v3, v2, LX/Chi;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2392296
    iget-object v3, p0, LX/GmZ;->M:LX/Chi;

    .line 2392297
    iget-object v4, v3, LX/Chi;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2392298
    iput-object v1, v0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    .line 2392299
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/IXQ;->v:Z

    .line 2392300
    iput-object v2, v0, LX/IXQ;->s:Ljava/lang/String;

    .line 2392301
    iput-object v3, v0, LX/IXQ;->t:Ljava/lang/String;

    .line 2392302
    invoke-static {v0}, LX/IXQ;->c(LX/IXQ;)V

    .line 2392303
    :cond_1
    return-void

    .line 2392304
    :cond_2
    const-string v1, "extra_instant_articles_can_log_open_link_on_settle"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2392305
    const-string v1, "extra_instant_articles_can_log_open_link_on_settle"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    .line 2392306
    :cond_3
    const-string v1, "extra_instant_articles_canonical_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2392307
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2392308
    iget-object v0, p0, LX/GmZ;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/Ckw;->b(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 2392219
    invoke-super {p0}, LX/Chc;->q()V

    .line 2392220
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GmZ;->ag:Z

    .line 2392221
    invoke-direct {p0}, LX/GmZ;->ab()V

    .line 2392222
    return-void
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 2392223
    invoke-super {p0}, LX/Chc;->r()V

    .line 2392224
    iget-object v0, p0, LX/GmZ;->I:LX/0Ot;

    if-eqz v0, :cond_0

    .line 2392225
    iget-object v0, p0, LX/GmZ;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cta;

    .line 2392226
    iget-object v1, v0, LX/Cta;->d:LX/CtZ;

    sget-object p0, LX/CtZ;->RESUMED:LX/CtZ;

    if-ne v1, p0, :cond_0

    .line 2392227
    sget-object v1, LX/CtZ;->PAUSED:LX/CtZ;

    iput-object v1, v0, LX/Cta;->d:LX/CtZ;

    .line 2392228
    :cond_0
    return-void
.end method

.method public final s()V
    .locals 14

    .prologue
    .line 2392229
    invoke-super {p0}, LX/Chc;->s()V

    .line 2392230
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXX;

    .line 2392231
    iget-object v6, v0, LX/IXX;->b:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    iput-wide v6, v0, LX/IXX;->q:J

    .line 2392232
    const-wide/16 v12, 0x0

    .line 2392233
    iget-wide v8, v0, LX/IXX;->q:J

    cmp-long v8, v8, v12

    if-nez v8, :cond_3

    .line 2392234
    :cond_0
    :goto_0
    iget-object v0, p0, LX/GmZ;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ClD;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IXX;

    .line 2392235
    iget-wide v6, v1, LX/IXX;->B:J

    move-wide v4, v6

    .line 2392236
    iget-object v6, v0, LX/ClD;->m:Landroid/content/Context;

    if-ne v6, v2, :cond_1

    iget-wide v6, v0, LX/ClD;->n:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_1

    .line 2392237
    iput-wide v4, v0, LX/ClD;->n:J

    .line 2392238
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GmZ;->ah:Z

    .line 2392239
    invoke-direct {p0}, LX/GmZ;->Z()V

    .line 2392240
    iget-object v0, p0, LX/GmZ;->Q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const-string v1, "native_article_close"

    invoke-virtual {v0, v1}, LX/Ckw;->b(Ljava/lang/String;)V

    .line 2392241
    iget-object v0, p0, LX/GmZ;->K:LX/0Ot;

    if-eqz v0, :cond_2

    .line 2392242
    iget-object v0, p0, LX/GmZ;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXQ;

    invoke-virtual {v0}, LX/IXQ;->a()V

    .line 2392243
    :cond_2
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXX;

    invoke-virtual {v0}, LX/IXX;->a()V

    .line 2392244
    return-void

    .line 2392245
    :cond_3
    iget-wide v8, v0, LX/IXX;->m:J

    cmp-long v8, v8, v12

    if-eqz v8, :cond_0

    .line 2392246
    iget-wide v8, v0, LX/IXX;->k:J

    cmp-long v8, v8, v12

    if-eqz v8, :cond_4

    iget-wide v8, v0, LX/IXX;->k:J

    iget-wide v10, v0, LX/IXX;->l:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_4

    iget-wide v8, v0, LX/IXX;->k:J

    :goto_1
    iput-wide v8, v0, LX/IXX;->l:J

    .line 2392247
    iget-wide v8, v0, LX/IXX;->n:J

    cmp-long v8, v8, v12

    if-eqz v8, :cond_5

    .line 2392248
    iget-wide v8, v0, LX/IXX;->n:J

    iget-wide v10, v0, LX/IXX;->l:J

    sub-long/2addr v8, v10

    invoke-static {v12, v13, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    iput-wide v8, v0, LX/IXX;->B:J

    .line 2392249
    :goto_2
    iget-wide v8, v0, LX/IXX;->p:J

    cmp-long v8, v8, v12

    if-eqz v8, :cond_0

    .line 2392250
    iget-wide v8, v0, LX/IXX;->p:J

    iget-wide v10, v0, LX/IXX;->l:J

    sub-long/2addr v8, v10

    iput-wide v8, v0, LX/IXX;->C:J

    goto/16 :goto_0

    .line 2392251
    :cond_4
    iget-wide v8, v0, LX/IXX;->m:J

    goto :goto_1

    .line 2392252
    :cond_5
    iput-wide v12, v0, LX/IXX;->B:J

    goto :goto_2
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 2392253
    invoke-super {p0}, LX/Chc;->t()V

    .line 2392254
    iget-object v0, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/ShareBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 2392255
    iget-object v0, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/ShareBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2392256
    iget-object v1, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2392257
    const/4 v0, 0x0

    iput-object v0, p0, LX/GmZ;->X:Lcom/facebook/instantarticles/view/ShareBar;

    .line 2392258
    :cond_0
    return-void
.end method

.method public v()V
    .locals 3

    .prologue
    .line 2392259
    invoke-super {p0}, LX/Chc;->v()V

    .line 2392260
    iget-object v0, p0, LX/GmZ;->K:LX/0Ot;

    if-eqz v0, :cond_0

    .line 2392261
    iget-object v0, p0, LX/GmZ;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXQ;

    .line 2392262
    iget-object v1, v0, LX/IXQ;->g:LX/Chv;

    iget-object v2, v0, LX/IXQ;->x:LX/ChN;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2392263
    :cond_0
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2392264
    iget-object v0, p0, LX/GmZ;->R:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXX;

    .line 2392265
    iget-object v1, v0, LX/IXX;->d:LX/Chv;

    iget-object v2, v0, LX/IXX;->j:LX/ChP;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2392266
    iget-object v1, v0, LX/IXX;->d:LX/Chv;

    iget-object v2, v0, LX/IXX;->h:LX/Chz;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2392267
    iget-object v1, v0, LX/IXX;->d:LX/Chv;

    iget-object v2, v0, LX/IXX;->i:LX/Ci2;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2392268
    :cond_1
    return-void
.end method

.method public y()I
    .locals 1

    .prologue
    .line 2392269
    invoke-virtual {p0}, LX/GmZ;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0308b4

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0308b2

    goto :goto_0
.end method

.method public final z()I
    .locals 1

    .prologue
    .line 2392270
    const v0, 0x7f0d167a

    return v0
.end method
