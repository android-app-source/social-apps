.class public LX/GbP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/10M;LX/10O;LX/2J4;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369473
    iput-object p1, p0, LX/GbP;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2369474
    invoke-virtual {p4, p2, p3}, LX/2J4;->a(LX/10M;LX/10O;)Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    move-result-object v0

    iput-object v0, p0, LX/GbP;->b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    .line 2369475
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 2

    .prologue
    .line 2369476
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2369477
    const/4 v0, 0x0

    .line 2369478
    :goto_0
    return v0

    .line 2369479
    :cond_0
    iget-object v0, p0, LX/GbP;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2369480
    iget-object v1, p0, LX/GbP;->b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    invoke-virtual {v1, v0}, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a(Ljava/lang/String;)V

    .line 2369481
    const/4 v0, 0x1

    goto :goto_0
.end method
