.class public LX/GZz;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GZz;


# instance fields
.field private final a:LX/GZj;


# direct methods
.method public constructor <init>(LX/GZj;LX/01T;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2367766
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2367767
    iput-object p1, p0, LX/GZz;->a:LX/GZj;

    .line 2367768
    sget-object v0, LX/0ax;->fS:Ljava/lang/String;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "{#%s}"

    const-string v3, "com.facebook.katana.profile.id"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "{%s 0}"

    const-string v3, "product_ref_id"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "{%s unknown}"

    const-string v3, "product_ref_type"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "{%s 0}"

    const-string v3, "arg_init_product_id"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "{%s 0}"

    const-string v3, "hide_page_header"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->STOREFRONT_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2367769
    sget-object v0, LX/0ax;->fT:Ljava/lang/String;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "{#%s}"

    const-string v3, "collection_id"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "{%s 0}"

    const-string v3, "product_ref_id"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "{%s unknown}"

    const-string v3, "product_ref_type"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "{%s Shop}"

    const-string v3, "title"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "{%s 0}"

    const-string v3, "hide_page_header"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/GZj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/01T;->FB4A:LX/01T;

    if-ne p2, v0, :cond_0

    const-class v0, Lcom/facebook/base/activity/ReactFragmentActivity;

    :goto_0
    sget-object v2, LX/0cQ;->COLLECTION_VIEW_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v1, v0, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2367770
    return-void

    .line 2367771
    :cond_0
    const-class v0, Lcom/facebook/base/activity/FragmentChromeActivity;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/GZz;
    .locals 5

    .prologue
    .line 2367772
    sget-object v0, LX/GZz;->b:LX/GZz;

    if-nez v0, :cond_1

    .line 2367773
    const-class v1, LX/GZz;

    monitor-enter v1

    .line 2367774
    :try_start_0
    sget-object v0, LX/GZz;->b:LX/GZz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2367775
    if-eqz v2, :cond_0

    .line 2367776
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2367777
    new-instance p0, LX/GZz;

    invoke-static {v0}, LX/GZj;->b(LX/0QB;)LX/GZj;

    move-result-object v3

    check-cast v3, LX/GZj;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-direct {p0, v3, v4}, LX/GZz;-><init>(LX/GZj;LX/01T;)V

    .line 2367778
    move-object v0, p0

    .line 2367779
    sput-object v0, LX/GZz;->b:LX/GZz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2367780
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2367781
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2367782
    :cond_1
    sget-object v0, LX/GZz;->b:LX/GZz;

    return-object v0

    .line 2367783
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2367784
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 2367785
    iget-object v0, p0, LX/GZz;->a:LX/GZj;

    .line 2367786
    iget-object v1, v0, LX/GZj;->a:LX/0Uh;

    const/16 v2, 0x3e

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2367787
    return v0
.end method
