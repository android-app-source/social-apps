.class public final LX/H80;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2432417
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2432418
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2432419
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2432420
    invoke-static {p0, p1}, LX/H80;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2432421
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2432422
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2432423
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2432424
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2432425
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/H80;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2432426
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2432427
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2432428
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 29

    .prologue
    .line 2432429
    const/16 v25, 0x0

    .line 2432430
    const/16 v24, 0x0

    .line 2432431
    const/16 v23, 0x0

    .line 2432432
    const/16 v22, 0x0

    .line 2432433
    const/16 v21, 0x0

    .line 2432434
    const/16 v20, 0x0

    .line 2432435
    const/16 v19, 0x0

    .line 2432436
    const/16 v18, 0x0

    .line 2432437
    const/16 v17, 0x0

    .line 2432438
    const/16 v16, 0x0

    .line 2432439
    const/4 v15, 0x0

    .line 2432440
    const/4 v14, 0x0

    .line 2432441
    const/4 v13, 0x0

    .line 2432442
    const/4 v12, 0x0

    .line 2432443
    const/4 v11, 0x0

    .line 2432444
    const/4 v10, 0x0

    .line 2432445
    const/4 v9, 0x0

    .line 2432446
    const/4 v8, 0x0

    .line 2432447
    const/4 v7, 0x0

    .line 2432448
    const/4 v6, 0x0

    .line 2432449
    const/4 v5, 0x0

    .line 2432450
    const/4 v4, 0x0

    .line 2432451
    const/4 v3, 0x0

    .line 2432452
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    .line 2432453
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2432454
    const/4 v3, 0x0

    .line 2432455
    :goto_0
    return v3

    .line 2432456
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2432457
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_10

    .line 2432458
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v26

    .line 2432459
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2432460
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1

    if-eqz v26, :cond_1

    .line 2432461
    const-string v27, "atom_size"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2

    .line 2432462
    const/4 v10, 0x1

    .line 2432463
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v25

    goto :goto_1

    .line 2432464
    :cond_2
    const-string v27, "bitrate"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 2432465
    const/4 v9, 0x1

    .line 2432466
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v24

    goto :goto_1

    .line 2432467
    :cond_3
    const-string v27, "default_quality"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 2432468
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    goto :goto_1

    .line 2432469
    :cond_4
    const-string v27, "hdAtomSize"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_5

    .line 2432470
    const/4 v8, 0x1

    .line 2432471
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v22

    goto :goto_1

    .line 2432472
    :cond_5
    const-string v27, "hdBitrate"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 2432473
    const/4 v7, 0x1

    .line 2432474
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v21

    goto :goto_1

    .line 2432475
    :cond_6
    const-string v27, "hd_playable_uri"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_7

    .line 2432476
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 2432477
    :cond_7
    const-string v27, "height"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 2432478
    const/4 v6, 0x1

    .line 2432479
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto/16 :goto_1

    .line 2432480
    :cond_8
    const-string v27, "id"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_9

    .line 2432481
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto/16 :goto_1

    .line 2432482
    :cond_9
    const-string v27, "is_live_streaming"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 2432483
    const/4 v5, 0x1

    .line 2432484
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto/16 :goto_1

    .line 2432485
    :cond_a
    const-string v27, "playable_duration_in_ms"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 2432486
    const/4 v4, 0x1

    .line 2432487
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 2432488
    :cond_b
    const-string v27, "playable_url"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 2432489
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 2432490
    :cond_c
    const-string v27, "title"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_d

    .line 2432491
    invoke-static/range {p0 .. p1}, LX/H7y;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 2432492
    :cond_d
    const-string v27, "url"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 2432493
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 2432494
    :cond_e
    const-string v27, "video_thumbnails"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_f

    .line 2432495
    invoke-static/range {p0 .. p1}, LX/H7z;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 2432496
    :cond_f
    const-string v27, "width"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 2432497
    const/4 v3, 0x1

    .line 2432498
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 2432499
    :cond_10
    const/16 v26, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2432500
    if-eqz v10, :cond_11

    .line 2432501
    const/4 v10, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v10, v1, v2}, LX/186;->a(III)V

    .line 2432502
    :cond_11
    if-eqz v9, :cond_12

    .line 2432503
    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v9, v1, v10}, LX/186;->a(III)V

    .line 2432504
    :cond_12
    const/4 v9, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 2432505
    if-eqz v8, :cond_13

    .line 2432506
    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 2432507
    :cond_13
    if-eqz v7, :cond_14

    .line 2432508
    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1, v8}, LX/186;->a(III)V

    .line 2432509
    :cond_14
    const/4 v7, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 2432510
    if-eqz v6, :cond_15

    .line 2432511
    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 2432512
    :cond_15
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 2432513
    if-eqz v5, :cond_16

    .line 2432514
    const/16 v5, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 2432515
    :cond_16
    if-eqz v4, :cond_17

    .line 2432516
    const/16 v4, 0x9

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 2432517
    :cond_17
    const/16 v4, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 2432518
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 2432519
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 2432520
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 2432521
    if-eqz v3, :cond_18

    .line 2432522
    const/16 v3, 0xe

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11, v4}, LX/186;->a(III)V

    .line 2432523
    :cond_18
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2432524
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2432525
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2432526
    if-eqz v0, :cond_0

    .line 2432527
    const-string v1, "atom_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432528
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2432529
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2432530
    if-eqz v0, :cond_1

    .line 2432531
    const-string v1, "bitrate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432532
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2432533
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2432534
    if-eqz v0, :cond_2

    .line 2432535
    const-string v1, "default_quality"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432536
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2432537
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2432538
    if-eqz v0, :cond_3

    .line 2432539
    const-string v1, "hdAtomSize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432540
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2432541
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2432542
    if-eqz v0, :cond_4

    .line 2432543
    const-string v1, "hdBitrate"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432544
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2432545
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2432546
    if-eqz v0, :cond_5

    .line 2432547
    const-string v1, "hd_playable_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432548
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2432549
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2432550
    if-eqz v0, :cond_6

    .line 2432551
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432552
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2432553
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2432554
    if-eqz v0, :cond_7

    .line 2432555
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432556
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2432557
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2432558
    if-eqz v0, :cond_8

    .line 2432559
    const-string v1, "is_live_streaming"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432560
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2432561
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2432562
    if-eqz v0, :cond_9

    .line 2432563
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432564
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2432565
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2432566
    if-eqz v0, :cond_a

    .line 2432567
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432568
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2432569
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432570
    if-eqz v0, :cond_b

    .line 2432571
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432572
    invoke-static {p0, v0, p2}, LX/H7y;->a(LX/15i;ILX/0nX;)V

    .line 2432573
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2432574
    if-eqz v0, :cond_c

    .line 2432575
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432576
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2432577
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432578
    if-eqz v0, :cond_10

    .line 2432579
    const-string v1, "video_thumbnails"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432580
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2432581
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2432582
    if-eqz v1, :cond_f

    .line 2432583
    const-string v3, "nodes"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432584
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2432585
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_e

    .line 2432586
    invoke-virtual {p0, v1, v3}, LX/15i;->q(II)I

    move-result v4

    .line 2432587
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2432588
    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, LX/15i;->g(II)I

    move-result v5

    .line 2432589
    if-eqz v5, :cond_d

    .line 2432590
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432591
    invoke-static {p0, v5, p2}, LX/H7Z;->a(LX/15i;ILX/0nX;)V

    .line 2432592
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2432593
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2432594
    :cond_e
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2432595
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2432596
    :cond_10
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2432597
    if-eqz v0, :cond_11

    .line 2432598
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432599
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2432600
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2432601
    return-void
.end method
