.class public LX/FQd;
.super LX/398;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FQd;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2239545
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2239546
    const-string v0, "fb://pagesmanager/{page}"

    const-string v1, "fb-pma://page/<page>"

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2239547
    return-void
.end method

.method public static a(LX/0QB;)LX/FQd;
    .locals 3

    .prologue
    .line 2239548
    sget-object v0, LX/FQd;->a:LX/FQd;

    if-nez v0, :cond_1

    .line 2239549
    const-class v1, LX/FQd;

    monitor-enter v1

    .line 2239550
    :try_start_0
    sget-object v0, LX/FQd;->a:LX/FQd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2239551
    if-eqz v2, :cond_0

    .line 2239552
    :try_start_1
    new-instance v0, LX/FQd;

    invoke-direct {v0}, LX/FQd;-><init>()V

    .line 2239553
    move-object v0, v0

    .line 2239554
    sput-object v0, LX/FQd;->a:LX/FQd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2239555
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2239556
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2239557
    :cond_1
    sget-object v0, LX/FQd;->a:LX/FQd;

    return-object v0

    .line 2239558
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2239559
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
