.class public LX/GdB;
.super LX/45y;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/GdB;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStore;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2373279
    invoke-direct {p0}, LX/45y;-><init>()V

    .line 2373280
    iput-object p1, p0, LX/GdB;->a:LX/0Ot;

    .line 2373281
    iput-object p2, p0, LX/GdB;->b:LX/0Or;

    .line 2373282
    iput-object p3, p0, LX/GdB;->c:LX/0Or;

    .line 2373283
    iput-object p4, p0, LX/GdB;->d:LX/0Or;

    .line 2373284
    return-void
.end method

.method public static a(LX/0QB;)LX/GdB;
    .locals 7

    .prologue
    .line 2373285
    sget-object v0, LX/GdB;->f:LX/GdB;

    if-nez v0, :cond_1

    .line 2373286
    const-class v1, LX/GdB;

    monitor-enter v1

    .line 2373287
    :try_start_0
    sget-object v0, LX/GdB;->f:LX/GdB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2373288
    if-eqz v2, :cond_0

    .line 2373289
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2373290
    new-instance v3, LX/GdB;

    const/16 v4, 0x140f

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1c35

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x17d

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/GdB;-><init>(LX/0Ot;LX/0Or;LX/0Or;LX/0Or;)V

    .line 2373291
    move-object v0, v3

    .line 2373292
    sput-object v0, LX/GdB;->f:LX/GdB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2373293
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2373294
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2373295
    :cond_1
    sget-object v0, LX/GdB;->f:LX/GdB;

    return-object v0

    .line 2373296
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2373297
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2373275
    iget-object v1, p0, LX/GdB;->e:Ljava/util/concurrent/Future;

    if-nez v1, :cond_0

    .line 2373276
    :goto_0
    return v0

    .line 2373277
    :cond_0
    iget-object v1, p0, LX/GdB;->e:Ljava/util/concurrent/Future;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2373278
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(ILandroid/os/Bundle;LX/45o;)Z
    .locals 3

    .prologue
    .line 2373273
    iget-object v0, p0, LX/GdB;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateJobLogic$1;

    invoke-direct {v1, p0, p3}, Lcom/facebook/fbreact/autoupdater/fbhttp/FbHttpUpdateJobLogic$1;-><init>(LX/GdB;LX/45o;)V

    const v2, -0x88c3733

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LX/GdB;->e:Ljava/util/concurrent/Future;

    .line 2373274
    const/4 v0, 0x1

    return v0
.end method
