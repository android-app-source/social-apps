.class public LX/Fsq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2297582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()LX/Fso;
    .locals 2

    .prologue
    .line 2297598
    new-instance v0, LX/Fsn;

    invoke-direct {v0}, LX/Fsn;-><init>()V

    .line 2297599
    const/4 v1, 0x0

    iput-object v1, v0, LX/Fsn;->a:Ljava/lang/String;

    .line 2297600
    move-object v0, v0

    .line 2297601
    const/4 v1, 0x2

    .line 2297602
    iput v1, v0, LX/Fsn;->e:I

    .line 2297603
    move-object v0, v0

    .line 2297604
    invoke-virtual {v0}, LX/Fsn;->b()LX/Fso;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0zO;)LX/Fso;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/Fso;"
        }
    .end annotation

    .prologue
    .line 2297593
    new-instance v0, LX/Fsn;

    invoke-direct {v0}, LX/Fsn;-><init>()V

    const-string v1, "section_id"

    sget-object v2, LX/4Zz;->FIRST:LX/4Zz;

    sget-object v3, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {p0, v1, v2, v3}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v1

    .line 2297594
    iput-object v1, v0, LX/Fsn;->b:LX/4a1;

    .line 2297595
    const/4 v2, 0x0

    iput-object v2, v0, LX/Fsn;->a:Ljava/lang/String;

    .line 2297596
    move-object v0, v0

    .line 2297597
    const-string v1, "end_cursor"

    sget-object v2, LX/4Zz;->FIRST:LX/4Zz;

    sget-object v3, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {p0, v1, v2, v3}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fsn;->b(LX/4a1;)LX/Fsn;

    move-result-object v0

    invoke-virtual {v0}, LX/Fsn;->b()LX/Fso;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Fso;LX/5SB;)LX/Fso;
    .locals 2

    .prologue
    .line 2297587
    invoke-virtual {p0}, LX/Fso;->a()LX/Fsn;

    move-result-object v1

    .line 2297588
    invoke-virtual {p1}, LX/5SB;->e()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2297589
    iput-object v0, v1, LX/Fsn;->f:Ljava/lang/String;

    .line 2297590
    move-object v0, v1

    .line 2297591
    invoke-virtual {v0}, LX/Fsn;->b()LX/Fso;

    .line 2297592
    invoke-virtual {v1}, LX/Fsn;->b()LX/Fso;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ILX/5SB;)LX/Fso;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2297583
    new-instance v0, LX/Fsn;

    invoke-direct {v0}, LX/Fsn;-><init>()V

    invoke-virtual {v0, p0}, LX/Fsn;->a(Ljava/lang/String;)LX/Fsn;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Fsn;->b(Ljava/lang/String;)LX/Fsn;

    move-result-object v0

    add-int/lit8 v1, p2, -0x1

    .line 2297584
    iput v1, v0, LX/Fsn;->e:I

    .line 2297585
    move-object v0, v0

    .line 2297586
    invoke-virtual {v0}, LX/Fsn;->b()LX/Fso;

    move-result-object v0

    invoke-static {v0, p3}, LX/Fsq;->a(LX/Fso;LX/5SB;)LX/Fso;

    move-result-object v0

    return-object v0
.end method
