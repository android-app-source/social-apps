.class public LX/Fhc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Fhc;


# instance fields
.field public final a:LX/Fhe;

.field public final b:LX/Fhk;

.field public final c:LX/Fhi;

.field public final d:LX/Fhj;

.field public final e:LX/Fhh;


# direct methods
.method public constructor <init>(LX/Fhe;LX/Fhk;LX/Fhi;LX/Fhj;LX/Fhh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273463
    iput-object p1, p0, LX/Fhc;->a:LX/Fhe;

    .line 2273464
    iput-object p2, p0, LX/Fhc;->b:LX/Fhk;

    .line 2273465
    iput-object p3, p0, LX/Fhc;->c:LX/Fhi;

    .line 2273466
    iput-object p4, p0, LX/Fhc;->d:LX/Fhj;

    .line 2273467
    iput-object p5, p0, LX/Fhc;->e:LX/Fhh;

    .line 2273468
    return-void
.end method

.method public static a(LX/0QB;)LX/Fhc;
    .locals 9

    .prologue
    .line 2273435
    sget-object v0, LX/Fhc;->f:LX/Fhc;

    if-nez v0, :cond_1

    .line 2273436
    const-class v1, LX/Fhc;

    monitor-enter v1

    .line 2273437
    :try_start_0
    sget-object v0, LX/Fhc;->f:LX/Fhc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2273438
    if-eqz v2, :cond_0

    .line 2273439
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2273440
    new-instance v3, LX/Fhc;

    invoke-static {v0}, LX/Fhe;->a(LX/0QB;)LX/Fhe;

    move-result-object v4

    check-cast v4, LX/Fhe;

    invoke-static {v0}, LX/Fhk;->a(LX/0QB;)LX/Fhk;

    move-result-object v5

    check-cast v5, LX/Fhk;

    invoke-static {v0}, LX/Fhi;->a(LX/0QB;)LX/Fhi;

    move-result-object v6

    check-cast v6, LX/Fhi;

    invoke-static {v0}, LX/Fhj;->a(LX/0QB;)LX/Fhj;

    move-result-object v7

    check-cast v7, LX/Fhj;

    invoke-static {v0}, LX/Fhh;->a(LX/0QB;)LX/Fhh;

    move-result-object v8

    check-cast v8, LX/Fhh;

    invoke-direct/range {v3 .. v8}, LX/Fhc;-><init>(LX/Fhe;LX/Fhk;LX/Fhi;LX/Fhj;LX/Fhh;)V

    .line 2273441
    move-object v0, v3

    .line 2273442
    sput-object v0, LX/Fhc;->f:LX/Fhc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273443
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2273444
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2273445
    :cond_1
    sget-object v0, LX/Fhc;->f:LX/Fhc;

    return-object v0

    .line 2273446
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2273447
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Fhc;LX/7HY;)LX/Fhd;
    .locals 2

    .prologue
    .line 2273458
    sget-object v0, LX/Fhb;->a:[I

    invoke-virtual {p1}, LX/7HY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2273459
    iget-object v0, p0, LX/Fhc;->b:LX/Fhk;

    :goto_0
    return-object v0

    .line 2273460
    :pswitch_0
    iget-object v0, p0, LX/Fhc;->c:LX/Fhi;

    goto :goto_0

    .line 2273461
    :pswitch_1
    iget-object v0, p0, LX/Fhc;->d:LX/Fhj;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/7HY;LX/7B6;)V
    .locals 1

    .prologue
    .line 2273469
    invoke-static {p0, p1}, LX/Fhc;->a(LX/Fhc;LX/7HY;)LX/Fhd;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/Fhd;->d(LX/7B6;)V

    .line 2273470
    return-void
.end method

.method public final a(LX/7HY;LX/7B6;I)V
    .locals 1

    .prologue
    .line 2273456
    invoke-static {p0, p1}, LX/Fhc;->a(LX/Fhc;LX/7HY;)LX/Fhd;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LX/Fhd;->a(LX/7B6;I)V

    .line 2273457
    return-void
.end method

.method public final b(LX/7HY;LX/7B6;I)V
    .locals 4

    .prologue
    .line 2273450
    invoke-static {p0, p1}, LX/Fhc;->a(LX/Fhc;LX/7HY;)LX/Fhd;

    move-result-object v0

    .line 2273451
    iget-object v1, v0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2273452
    if-nez v1, :cond_0

    .line 2273453
    :goto_0
    return-void

    .line 2273454
    :cond_0
    invoke-virtual {v0}, LX/Fhd;->b()LX/11o;

    move-result-object v2

    .line 2273455
    const-string v3, "network_batch_"

    invoke-static {v3, p3}, LX/Fhd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 p0, 0x0

    const p1, -0x5ac0e503

    invoke-static {v2, v3, v1, p0, p1}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    goto :goto_0
.end method

.method public final d(LX/7B6;)V
    .locals 1

    .prologue
    .line 2273448
    iget-object v0, p0, LX/Fhc;->a:LX/Fhe;

    invoke-virtual {v0, p1}, LX/Fhd;->d(LX/7B6;)V

    .line 2273449
    return-void
.end method
