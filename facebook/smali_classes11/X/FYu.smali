.class public final LX/FYu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "LX/7Jh;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:I

.field public final synthetic c:J

.field public final synthetic d:LX/FYx;


# direct methods
.method public constructor <init>(LX/FYx;Landroid/content/Context;IJ)V
    .locals 0

    .prologue
    .line 2256633
    iput-object p1, p0, LX/FYu;->d:LX/FYx;

    iput-object p2, p0, LX/FYu;->a:Landroid/content/Context;

    iput p3, p0, LX/FYu;->b:I

    iput-wide p4, p0, LX/FYu;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2256634
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2256635
    check-cast p1, Ljava/util/List;

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 2256636
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256637
    :cond_0
    :goto_0
    return-void

    .line 2256638
    :cond_1
    invoke-static {p1}, LX/15V;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 2256639
    new-instance v0, LX/15i;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7Jh;

    iget-object v1, v1, LX/7Jh;->c:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2256640
    const-string v1, "Saved2ItemClickHandler"

    invoke-virtual {v0, v1}, LX/15i;->a(Ljava/lang/String;)V

    .line 2256641
    sget-object v1, LX/16Z;->a:LX/16Z;

    invoke-virtual {v0, v1}, LX/15i;->a(LX/16a;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2256642
    new-instance v1, LX/3Qv;

    invoke-direct {v1}, LX/3Qv;-><init>()V

    const-string v2, "SAVED_STORY"

    .line 2256643
    iput-object v2, v1, LX/3Qv;->d:Ljava/lang/String;

    .line 2256644
    move-object v1, v1

    .line 2256645
    iput-object v6, v1, LX/3Qv;->b:Ljava/util/List;

    .line 2256646
    move-object v1, v1

    .line 2256647
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    .line 2256648
    iput-object v2, v1, LX/3Qv;->h:LX/04g;

    .line 2256649
    move-object v1, v1

    .line 2256650
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2256651
    iput-object v0, v1, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2256652
    move-object v0, v1

    .line 2256653
    iget-object v1, p0, LX/FYu;->d:LX/FYx;

    iget-object v2, p0, LX/FYu;->a:Landroid/content/Context;

    .line 2256654
    sget-object v4, LX/FYv;->a:[I

    iget-object v3, v1, LX/FYx;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->m()LX/2qY;

    move-result-object v3

    invoke-virtual {v3}, LX/2qY;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 2256655
    const v3, 0x7f083414

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    move-object v1, v3

    .line 2256656
    iput-object v1, v0, LX/3Qv;->p:Ljava/lang/String;

    .line 2256657
    move-object v0, v0

    .line 2256658
    iget v1, p0, LX/FYu;->b:I

    .line 2256659
    iput v1, v0, LX/3Qv;->e:I

    .line 2256660
    move-object v0, v0

    .line 2256661
    sget-object v1, LX/04D;->SAVED_DASHBOARD:LX/04D;

    .line 2256662
    iput-object v1, v0, LX/3Qv;->g:LX/04D;

    .line 2256663
    move-object v0, v0

    .line 2256664
    iget-object v1, p0, LX/FYu;->d:LX/FYx;

    iget-wide v2, p0, LX/FYu;->c:J

    iget-object v4, p0, LX/FYu;->a:Landroid/content/Context;

    invoke-virtual {v0}, LX/3Qv;->a()LX/3Qw;

    move-result-object v0

    invoke-static {v1, v2, v3, v4, v0}, LX/FYx;->a$redex0(LX/FYx;JLandroid/content/Context;LX/3Qw;)V

    .line 2256665
    iget-object v0, p0, LX/FYu;->d:LX/FYx;

    iget-object v0, v0, LX/FYx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BUA;

    invoke-virtual {v0, p1}, LX/BUA;->a(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2256666
    :pswitch_0
    const v3, 0x7f083413

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2256667
    :pswitch_1
    const v3, 0x7f083415

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
