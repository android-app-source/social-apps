.class public LX/GNC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GN0;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GN0",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/GDl;

.field public final b:LX/GDm;

.field public final c:LX/GDg;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/GNI;


# direct methods
.method public constructor <init>(LX/GDl;LX/GDm;LX/GDg;LX/0Or;LX/GNI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GDl;",
            "LX/GDm;",
            "LX/GDg;",
            "LX/0Or",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;",
            "LX/GNI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345534
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345535
    iput-object p1, p0, LX/GNC;->a:LX/GDl;

    .line 2345536
    iput-object p2, p0, LX/GNC;->b:LX/GDm;

    .line 2345537
    iput-object p3, p0, LX/GNC;->c:LX/GDg;

    .line 2345538
    iput-object p4, p0, LX/GNC;->d:LX/0Or;

    .line 2345539
    iput-object p5, p0, LX/GNC;->e:LX/GNI;

    .line 2345540
    return-void
.end method

.method public static a(LX/0QB;)LX/GNC;
    .locals 9

    .prologue
    .line 2345523
    const-class v1, LX/GNC;

    monitor-enter v1

    .line 2345524
    :try_start_0
    sget-object v0, LX/GNC;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2345525
    sput-object v2, LX/GNC;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2345526
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2345527
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2345528
    new-instance v3, LX/GNC;

    invoke-static {v0}, LX/GDl;->a(LX/0QB;)LX/GDl;

    move-result-object v4

    check-cast v4, LX/GDl;

    invoke-static {v0}, LX/GDm;->a(LX/0QB;)LX/GDm;

    move-result-object v5

    check-cast v5, LX/GDm;

    invoke-static {v0}, LX/GDg;->a(LX/0QB;)LX/GDg;

    move-result-object v6

    check-cast v6, LX/GDg;

    const/16 v7, 0x103d

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/GNI;->a(LX/0QB;)LX/GNI;

    move-result-object v8

    check-cast v8, LX/GNI;

    invoke-direct/range {v3 .. v8}, LX/GNC;-><init>(LX/GDl;LX/GDm;LX/GDg;LX/0Or;LX/GNI;)V

    .line 2345529
    move-object v0, v3

    .line 2345530
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2345531
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GNC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2345532
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2345533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/GNC;Landroid/view/View;LX/31Y;IILcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 2

    .prologue
    .line 2345518
    invoke-virtual {p2, p3}, LX/0ju;->a(I)LX/0ju;

    .line 2345519
    invoke-virtual {p2, p4}, LX/0ju;->b(I)LX/0ju;

    .line 2345520
    const v0, 0x7f080ac6

    new-instance v1, LX/GN9;

    invoke-direct {v1, p0, p5, p1}, LX/GN9;-><init>(LX/GNC;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/view/View;)V

    invoke-virtual {p2, v0, v1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2345521
    const v0, 0x7f080ab4

    new-instance v1, LX/GNA;

    invoke-direct {v1, p0}, LX/GNA;-><init>(LX/GNC;)V

    invoke-virtual {p2, v0, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2345522
    return-void
.end method

.method public static c(LX/GNC;Landroid/view/View;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 3

    .prologue
    .line 2345510
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2345511
    new-instance v1, LX/31Y;

    invoke-direct {v1, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2345512
    const v0, 0x7f080acd

    invoke-virtual {v1, v0}, LX/0ju;->a(I)LX/0ju;

    .line 2345513
    const v0, 0x7f080ace

    invoke-virtual {v1, v0}, LX/0ju;->b(I)LX/0ju;

    .line 2345514
    const v0, 0x7f080ac3

    new-instance v2, LX/GNB;

    invoke-direct {v2, p0, p2, p1}, LX/GNB;-><init>(LX/GNC;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/view/View;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2345515
    const v0, 0x7f080ab4

    new-instance v2, LX/GN2;

    invoke-direct {v2, p0, p1}, LX/GN2;-><init>(LX/GNC;Landroid/view/View;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2345516
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2345517
    return-void
.end method


# virtual methods
.method public final a(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345508
    check-cast p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345509
    new-instance v0, LX/GN5;

    invoke-direct {v0, p0, p2}, LX/GN5;-><init>(LX/GNC;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final a(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345490
    new-instance v0, LX/GN4;

    invoke-direct {v0, p0, p1, p2}, LX/GN4;-><init>(LX/GNC;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 2345507
    iget-object v0, p0, LX/GNC;->e:LX/GNI;

    iget-object v1, p0, LX/GNC;->b:LX/GDm;

    invoke-static {p3, p2, p1, v0, v1}, LX/GNI;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;Landroid/content/Context;LX/GNI;LX/GDm;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345506
    new-instance v0, LX/GN7;

    invoke-direct {v0, p0, p1}, LX/GN7;-><init>(LX/GNC;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .locals 1

    .prologue
    .line 2345541
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345542
    new-instance v0, LX/GN8;

    invoke-direct {v0, p0, p3}, LX/GN8;-><init>(LX/GNC;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2345505
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p0, p1, p3}, LX/GNC;->a(Landroid/view/View;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 6

    .prologue
    const v2, 0x5a0004

    .line 2345498
    iget-object v0, p0, LX/GNC;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2345499
    iget-object v0, p0, LX/GNC;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2345500
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2345501
    new-instance v2, LX/31Y;

    invoke-direct {v2, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2345502
    const v3, 0x7f080acf

    const v4, 0x7f080ad0

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, LX/GNC;->a(LX/GNC;Landroid/view/View;LX/31Y;IILcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2345503
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2345504
    return-void
.end method

.method public final synthetic b(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345497
    check-cast p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p0, p1, p2}, LX/GNC;->a(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345496
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p0, p3}, LX/GNC;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345494
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345495
    new-instance v0, LX/GN6;

    invoke-direct {v0, p0, p3}, LX/GN6;-><init>(LX/GNC;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final synthetic d(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345493
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p0, p1, p2, p3}, LX/GNC;->a(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345491
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345492
    new-instance v0, LX/GN3;

    invoke-direct {v0, p0, p2, p3}, LX/GN3;-><init>(LX/GNC;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method
