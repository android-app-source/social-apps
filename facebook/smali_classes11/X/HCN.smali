.class public final enum LX/HCN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HCN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HCN;

.field public static final enum TAB_ROW:LX/HCN;

.field public static final enum UPDATE_APP_HEADER:LX/HCN;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2438570
    new-instance v0, LX/HCN;

    const-string v1, "UPDATE_APP_HEADER"

    invoke-direct {v0, v1, v2}, LX/HCN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HCN;->UPDATE_APP_HEADER:LX/HCN;

    .line 2438571
    new-instance v0, LX/HCN;

    const-string v1, "TAB_ROW"

    invoke-direct {v0, v1, v3}, LX/HCN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HCN;->TAB_ROW:LX/HCN;

    .line 2438572
    const/4 v0, 0x2

    new-array v0, v0, [LX/HCN;

    sget-object v1, LX/HCN;->UPDATE_APP_HEADER:LX/HCN;

    aput-object v1, v0, v2

    sget-object v1, LX/HCN;->TAB_ROW:LX/HCN;

    aput-object v1, v0, v3

    sput-object v0, LX/HCN;->$VALUES:[LX/HCN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2438575
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HCN;
    .locals 1

    .prologue
    .line 2438574
    const-class v0, LX/HCN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HCN;

    return-object v0
.end method

.method public static values()[LX/HCN;
    .locals 1

    .prologue
    .line 2438573
    sget-object v0, LX/HCN;->$VALUES:[LX/HCN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HCN;

    return-object v0
.end method
