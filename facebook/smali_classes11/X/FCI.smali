.class public final enum LX/FCI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FCI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FCI;

.field public static final enum INVALID:LX/FCI;

.field public static final enum VALID_AND_EXPIRED:LX/FCI;

.field public static final enum VALID_AND_FRESH:LX/FCI;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2210178
    new-instance v0, LX/FCI;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v2}, LX/FCI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCI;->INVALID:LX/FCI;

    .line 2210179
    new-instance v0, LX/FCI;

    const-string v1, "VALID_AND_FRESH"

    invoke-direct {v0, v1, v3}, LX/FCI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCI;->VALID_AND_FRESH:LX/FCI;

    .line 2210180
    new-instance v0, LX/FCI;

    const-string v1, "VALID_AND_EXPIRED"

    invoke-direct {v0, v1, v4}, LX/FCI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCI;->VALID_AND_EXPIRED:LX/FCI;

    .line 2210181
    const/4 v0, 0x3

    new-array v0, v0, [LX/FCI;

    sget-object v1, LX/FCI;->INVALID:LX/FCI;

    aput-object v1, v0, v2

    sget-object v1, LX/FCI;->VALID_AND_FRESH:LX/FCI;

    aput-object v1, v0, v3

    sget-object v1, LX/FCI;->VALID_AND_EXPIRED:LX/FCI;

    aput-object v1, v0, v4

    sput-object v0, LX/FCI;->$VALUES:[LX/FCI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2210182
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FCI;
    .locals 1

    .prologue
    .line 2210183
    const-class v0, LX/FCI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FCI;

    return-object v0
.end method

.method public static values()[LX/FCI;
    .locals 1

    .prologue
    .line 2210184
    sget-object v0, LX/FCI;->$VALUES:[LX/FCI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FCI;

    return-object v0
.end method
