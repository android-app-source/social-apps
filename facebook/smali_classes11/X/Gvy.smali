.class public abstract LX/Gvy;
.super LX/4hY;
.source ""


# static fields
.field private static final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public final b:LX/0aG;

.field public final c:LX/0Zb;

.field public final d:Z

.field public final e:Landroid/app/Activity;

.field public final f:Lcom/facebook/platform/common/action/PlatformAppCall;

.field public final g:Ljava/util/concurrent/Executor;

.field public final h:LX/0TD;

.field public final i:LX/BM1;

.field public final j:LX/BKe;

.field public final l:I

.field public final m:Lcom/facebook/content/SecureContextHelper;

.field public n:Z

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2406255
    const-class v0, LX/Gvy;

    sput-object v0, LX/Gvy;->k:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0Zb;Ljava/util/concurrent/Executor;Landroid/app/Activity;ILcom/facebook/platform/common/action/PlatformAppCall;ZLX/0TD;LX/BM1;LX/BKe;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p8    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param

    .prologue
    .line 2406369
    invoke-direct {p0}, LX/4hY;-><init>()V

    .line 2406370
    iput p5, p0, LX/Gvy;->l:I

    .line 2406371
    iput-object p4, p0, LX/Gvy;->e:Landroid/app/Activity;

    .line 2406372
    iput-object p1, p0, LX/Gvy;->b:LX/0aG;

    .line 2406373
    iput-object p2, p0, LX/Gvy;->c:LX/0Zb;

    .line 2406374
    iput-object p6, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406375
    iput-boolean p7, p0, LX/Gvy;->d:Z

    .line 2406376
    iput-object p3, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    .line 2406377
    iput-object p8, p0, LX/Gvy;->h:LX/0TD;

    .line 2406378
    iput-object p9, p0, LX/Gvy;->i:LX/BM1;

    .line 2406379
    iput-object p10, p0, LX/Gvy;->j:LX/BKe;

    .line 2406380
    iput-object p11, p0, LX/Gvy;->m:Lcom/facebook/content/SecureContextHelper;

    .line 2406381
    return-void
.end method

.method public static c(Ljava/lang/String;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 1

    .prologue
    .line 2406363
    :try_start_0
    new-instance v0, LX/5m9;

    invoke-direct {v0}, LX/5m9;-><init>()V

    .line 2406364
    iput-object p0, v0, LX/5m9;->f:Ljava/lang/String;

    .line 2406365
    move-object v0, v0

    .line 2406366
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2406367
    :goto_0
    return-object v0

    .line 2406368
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 2406361
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2406362
    :goto_0
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end method

.method public final a(Ljava/util/ArrayList;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406334
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2406335
    invoke-static {p2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406336
    :goto_0
    return-object v0

    .line 2406337
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2406338
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406339
    :goto_1
    move-object v0, v0

    .line 2406340
    new-instance v1, LX/Gvu;

    invoke-direct {v1, p0}, LX/Gvu;-><init>(LX/Gvy;)V

    .line 2406341
    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2406342
    new-instance v1, LX/Gvt;

    invoke-direct {v1, p0, p2}, LX/Gvt;-><init>(LX/Gvy;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    .line 2406343
    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2406344
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2406345
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2406346
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_4

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2406347
    invoke-static {v0}, LX/Gvy;->e(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 2406348
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2406349
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2406350
    :cond_3
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2406351
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2406352
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2406353
    iget-object v1, p0, LX/Gvy;->h:LX/0TD;

    new-instance v4, LX/Gvx;

    invoke-direct {v4, p0, v2}, LX/Gvx;-><init>(LX/Gvy;Ljava/util/ArrayList;)V

    invoke-interface {v1, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 2406354
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2406355
    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2406356
    iget-object v1, p0, LX/Gvy;->h:LX/0TD;

    new-instance v2, LX/Gvw;

    invoke-direct {v2, p0, v3}, LX/Gvw;-><init>(LX/Gvy;Ljava/util/ArrayList;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 2406357
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2406358
    :cond_6
    invoke-static {v0}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406359
    new-instance v1, LX/Gvv;

    invoke-direct {v1, p0}, LX/Gvv;-><init>(LX/Gvy;)V

    .line 2406360
    iget-object v2, p0, LX/Gvy;->h:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2406297
    iget v0, p0, LX/Gvy;->l:I

    if-ne p1, v0, :cond_1

    .line 2406298
    if-nez p2, :cond_4

    .line 2406299
    if-eqz p3, :cond_5

    const-string v0, "com.facebook.platform.extra.COMPOSER_ERROR"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.facebook.platform.extra.COMPOSER_EXCEPTION"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2406300
    if-eqz v0, :cond_2

    .line 2406301
    iget-object v1, p0, LX/Gvy;->c:LX/0Zb;

    const-string v0, "platform_share_failed_with_error"

    invoke-virtual {p0, v0}, LX/Gvy;->b(Ljava/lang/String;)LX/8Or;

    move-result-object v0

    const-string v2, "com.facebook.platform.extra.COMPOSER_ERROR"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2406302
    iput-object v2, v0, LX/8Or;->f:Ljava/lang/String;

    .line 2406303
    move-object v2, v0

    .line 2406304
    const-string v0, "com.facebook.platform.extra.COMPOSER_EXCEPTION"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-virtual {v2, v0}, LX/8Or;->a(Ljava/lang/Throwable;)LX/8Or;

    move-result-object v0

    invoke-virtual {v0}, LX/8Or;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2406305
    iget-object v0, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406306
    const-string v1, "com.facebook.platform.extra.COMPOSER_ERROR"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "com.facebook.platform.extra.COMPOSER_EXCEPTION"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2406307
    const-string v1, "com.facebook.platform.extra.COMPOSER_EXCEPTION"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    const-string v2, "com.facebook.platform.extra.COMPOSER_ERROR"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2406308
    :goto_1
    move-object v0, v1

    .line 2406309
    invoke-virtual {p0, v0}, LX/4hY;->c(Landroid/os/Bundle;)V

    .line 2406310
    :cond_1
    :goto_2
    return-void

    .line 2406311
    :cond_2
    iget-object v0, p0, LX/Gvy;->c:LX/0Zb;

    const-string v1, "platform_share_cancel_dialog"

    invoke-virtual {p0, v1}, LX/Gvy;->b(Ljava/lang/String;)LX/8Or;

    move-result-object v1

    invoke-virtual {v1}, LX/8Or;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2406312
    iget-object v0, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    iget-boolean v1, p0, LX/Gvy;->n:Z

    const/4 p3, 0x1

    .line 2406313
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/platform/common/action/PlatformAppCall;->d()Z

    move-result v2

    if-eqz v2, :cond_8

    move p2, p3

    .line 2406314
    :goto_3
    if-eqz p2, :cond_9

    const-string v2, "didComplete"

    move-object p1, v2

    .line 2406315
    :goto_4
    if-eqz p2, :cond_a

    const-string v2, "completionGesture"

    .line 2406316
    :goto_5
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 2406317
    invoke-virtual {p2, p1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2406318
    if-eqz v1, :cond_3

    .line 2406319
    const-string p1, "cancel"

    invoke-virtual {p2, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406320
    :cond_3
    move-object v0, p2

    .line 2406321
    invoke-virtual {p0, v0}, LX/4hY;->d(Landroid/os/Bundle;)V

    goto :goto_2

    .line 2406322
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gvy;->a:Z

    .line 2406323
    invoke-virtual {p0, p3}, LX/Gvy;->a(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406324
    new-instance v1, LX/4At;

    iget-object v2, p0, LX/Gvy;->e:Landroid/app/Activity;

    const p1, 0x7f08360f

    invoke-direct {v1, v2, p1}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2406325
    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2406326
    new-instance v2, LX/Gvs;

    invoke-direct {v2, p0, v1}, LX/Gvs;-><init>(LX/Gvy;LX/4At;)V

    iget-object v1, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2406327
    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2406328
    :cond_6
    const-string v1, "com.facebook.platform.extra.COMPOSER_EXCEPTION"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2406329
    const-string v1, "com.facebook.platform.extra.COMPOSER_EXCEPTION"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    const-string v2, "An unknown error occurred."

    invoke-static {v0, v1, v2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    goto :goto_1

    .line 2406330
    :cond_7
    const-string v1, "ApplicationError"

    const-string v2, "com.facebook.platform.extra.COMPOSER_ERROR"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    goto/16 :goto_1

    .line 2406331
    :cond_8
    const/4 v2, 0x0

    move p2, v2

    goto :goto_3

    .line 2406332
    :cond_9
    const-string v2, "com.facebook.platform.extra.DID_COMPLETE"

    move-object p1, v2

    goto :goto_4

    .line 2406333
    :cond_a
    const-string v2, "com.facebook.platform.extra.COMPLETION_GESTURE"

    goto :goto_5
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2406277
    if-eqz p1, :cond_0

    .line 2406278
    if-eqz p1, :cond_2

    const-string v0, "is_ui_showing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2406279
    iput-boolean v0, p0, LX/Gvy;->a:Z

    .line 2406280
    const-string v0, "app_is_installed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/Gvy;->n:Z

    .line 2406281
    const-string v0, "app_has_publish"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/Gvy;->o:Z

    .line 2406282
    :cond_0
    iget-boolean v0, p0, LX/Gvy;->a:Z

    if-nez v0, :cond_1

    .line 2406283
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gvy;->a:Z

    .line 2406284
    iget-object v0, p0, LX/Gvy;->j:LX/BKe;

    .line 2406285
    invoke-static {v0}, LX/BKe;->e(LX/BKe;)V

    .line 2406286
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2406287
    new-instance v1, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;

    iget-object v2, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406288
    iget-object v3, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2406289
    invoke-direct {v1, v2}, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Params;-><init>(Ljava/lang/String;)V

    .line 2406290
    const-string v2, "app_info"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2406291
    iget-object v1, p0, LX/Gvy;->b:LX/0aG;

    const-string v2, "platform_get_app_permissions"

    const v3, -0x4ccea0fc

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2406292
    new-instance v1, LX/Gvq;

    invoke-direct {v1, p0}, LX/Gvq;-><init>(LX/Gvy;)V

    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2406293
    invoke-virtual {p0}, LX/Gvy;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406294
    if-nez v0, :cond_3

    .line 2406295
    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2406296
    :cond_3
    new-instance v1, LX/Gvr;

    invoke-direct {v1, p0}, LX/Gvr;-><init>(LX/Gvy;)V

    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2406268
    iget-object v0, p0, LX/Gvy;->j:LX/BKe;

    .line 2406269
    iget-object v1, v0, LX/BKe;->a:LX/11i;

    iget-object v2, v0, LX/BKe;->c:LX/BKd;

    invoke-interface {v1, v2}, LX/11i;->c(LX/0Pq;)V

    .line 2406270
    iget-object v0, p0, LX/Gvy;->c:LX/0Zb;

    const-string v1, "platform_share_failed_with_error"

    invoke-virtual {p0, v1}, LX/Gvy;->b(Ljava/lang/String;)LX/8Or;

    move-result-object v1

    .line 2406271
    iput-object p1, v1, LX/8Or;->f:Ljava/lang/String;

    .line 2406272
    move-object v1, v1

    .line 2406273
    invoke-virtual {v1}, LX/8Or;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2406274
    iget-object v0, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    const-string v1, "ApplicationError"

    invoke-static {v0, v1, p1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2406275
    invoke-virtual {p0, v0}, LX/4hY;->c(Landroid/os/Bundle;)V

    .line 2406276
    return-void
.end method

.method public b(Ljava/lang/String;)LX/8Or;
    .locals 2

    .prologue
    .line 2406262
    new-instance v0, LX/8Or;

    const-string v1, "platform_native_share"

    invoke-direct {v0, p1, v1}, LX/8Or;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v0

    .line 2406263
    iget-object v1, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406264
    iget-object p0, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v1, p0

    .line 2406265
    iput-object v1, v0, LX/8Or;->c:Ljava/lang/String;

    .line 2406266
    move-object v0, v0

    .line 2406267
    return-object v0
.end method

.method public b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406260
    const/4 v0, 0x0

    move-object v0, v0

    .line 2406261
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2406256
    const-string v0, "is_ui_showing"

    iget-boolean v1, p0, LX/Gvy;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2406257
    const-string v0, "app_is_installed"

    iget-boolean v1, p0, LX/Gvy;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2406258
    const-string v0, "app_has_publish"

    iget-boolean v1, p0, LX/Gvy;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2406259
    return-void
.end method
