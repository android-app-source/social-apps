.class public LX/GpB;
.super LX/Got;
.source ""

# interfaces
.implements LX/Gon;
.implements LX/Clr;
.implements LX/Clu;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:D

.field public final g:D

.field public final h:D

.field public final i:D

.field public final j:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

.field public final k:Landroid/graphics/Typeface;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/Integer;

.field public final n:Ljava/lang/Integer;

.field private final o:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

.field public final p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryInterfaces$StoreLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GpA;)V
    .locals 12

    .prologue
    .line 2394786
    invoke-direct {p0, p1}, LX/Got;-><init>(LX/Gos;)V

    .line 2394787
    iget-object v0, p1, LX/GpA;->b:Ljava/lang/String;

    iput-object v0, p0, LX/GpB;->a:Ljava/lang/String;

    .line 2394788
    iget-object v0, p1, LX/GpA;->c:Ljava/lang/String;

    iput-object v0, p0, LX/GpB;->b:Ljava/lang/String;

    .line 2394789
    iget-object v0, p1, LX/GpA;->d:Ljava/lang/String;

    iput-object v0, p0, LX/GpB;->c:Ljava/lang/String;

    .line 2394790
    iget-object v0, p1, LX/GpA;->f:Ljava/lang/String;

    iput-object v0, p0, LX/GpB;->d:Ljava/lang/String;

    .line 2394791
    iget-object v0, p1, LX/GpA;->e:Ljava/lang/String;

    iput-object v0, p0, LX/GpB;->e:Ljava/lang/String;

    .line 2394792
    iget-wide v0, p1, LX/GpA;->g:D

    iput-wide v0, p0, LX/GpB;->f:D

    .line 2394793
    iget-wide v0, p1, LX/GpA;->h:D

    iput-wide v0, p0, LX/GpB;->g:D

    .line 2394794
    iget-wide v0, p1, LX/GpA;->i:D

    iput-wide v0, p0, LX/GpB;->h:D

    .line 2394795
    iget-wide v0, p1, LX/GpA;->j:D

    iput-wide v0, p0, LX/GpB;->i:D

    .line 2394796
    iget-object v0, p1, LX/GpA;->k:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    iput-object v0, p0, LX/GpB;->j:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    .line 2394797
    iget-object v0, p1, LX/GpA;->l:Ljava/lang/String;

    invoke-static {v0}, LX/8Yg;->a(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, LX/GpB;->k:Landroid/graphics/Typeface;

    .line 2394798
    iget-object v0, p1, LX/GpA;->m:Ljava/lang/String;

    iput-object v0, p0, LX/GpB;->l:Ljava/lang/String;

    .line 2394799
    iget-object v0, p1, LX/GpA;->n:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    iput-object v0, p0, LX/GpB;->o:Lcom/facebook/graphql/enums/GraphQLStoreLocatorCardFormat;

    .line 2394800
    iget-object v0, p1, LX/GpA;->o:LX/0Px;

    iput-object v0, p0, LX/GpB;->p:LX/0Px;

    .line 2394801
    const/4 v2, -0x1

    const v3, -0xeeeeef

    .line 2394802
    iget-object v4, p0, LX/GpB;->j:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2394803
    iget-object v4, p0, LX/GpB;->l:Ljava/lang/String;

    .line 2394804
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "#"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2394805
    :goto_0
    move-object v5, v5

    .line 2394806
    if-eqz v5, :cond_1

    .line 2394807
    new-instance v4, LX/3rL;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const v7, 0x3f59999a    # 0.85f

    invoke-static {v6, v7}, LX/47Z;->b(IF)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, LX/3qk;->a(I)D

    move-result-wide v8

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    cmpg-double v5, v8, v10

    if-gez v5, :cond_0

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v4, v6, v2}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v2, v4

    .line 2394808
    :goto_2
    move-object v1, v2

    .line 2394809
    iget-object v0, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, LX/GpB;->m:Ljava/lang/Integer;

    .line 2394810
    iget-object v0, v1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, LX/GpB;->n:Ljava/lang/Integer;

    .line 2394811
    return-void

    :cond_0
    move v2, v3

    .line 2394812
    goto :goto_1

    .line 2394813
    :cond_1
    iget-object v4, p0, LX/GpB;->j:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->LIGHT:Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v2, LX/3rL;

    const v4, -0x19000001

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v4, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :cond_2
    new-instance v3, LX/3rL;

    const/high16 v4, -0x27000000

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v3, v4, v2}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v2, v3

    goto :goto_2

    :catch_0
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 2394785
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->MAP:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method
