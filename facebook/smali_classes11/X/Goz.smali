.class public final LX/Goz;
.super LX/Gor;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Gor",
        "<",
        "Lcom/facebook/instantshopping/model/data/CulturalMomentPopularMediaBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/CHX;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CHi;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/GoE;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V
    .locals 3

    .prologue
    .line 2394676
    invoke-direct {p0, p2, p3}, LX/Gor;-><init>(II)V

    .line 2394677
    new-instance v0, LX/GoE;

    invoke-interface {p1}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/Goz;->e:LX/GoE;

    .line 2394678
    return-void
.end method


# virtual methods
.method public final synthetic b()LX/Clr;
    .locals 1

    .prologue
    .line 2394679
    invoke-virtual {p0}, LX/Goz;->c()LX/Gp0;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/Gp0;
    .locals 2

    .prologue
    .line 2394680
    new-instance v0, LX/Gp0;

    invoke-direct {v0, p0}, LX/Gp0;-><init>(LX/Goz;)V

    return-object v0
.end method
