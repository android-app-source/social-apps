.class public final LX/GKV;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/GKX;


# direct methods
.method public constructor <init>(LX/GKX;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2340986
    iput-object p1, p0, LX/GKV;->b:LX/GKX;

    iput-object p2, p0, LX/GKV;->a:Landroid/content/Context;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2340987
    iget-object v0, p0, LX/GKV;->b:LX/GKX;

    iget-object v0, v0, LX/GKX;->a:LX/GMm;

    const-string v1, "https://m.facebook.com/payments_terms"

    iget-object v2, p0, LX/GKV;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, LX/GMm;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 2340988
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2340989
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2340990
    iget-object v0, p0, LX/GKV;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0124

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2340991
    return-void
.end method
