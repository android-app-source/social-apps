.class public final LX/GLZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Vd;

.field public final synthetic b:LX/GLb;


# direct methods
.method public constructor <init>(LX/GLb;LX/0Vd;)V
    .locals 0

    .prologue
    .line 2343081
    iput-object p1, p0, LX/GLZ;->b:LX/GLb;

    iput-object p2, p0, LX/GLZ;->a:LX/0Vd;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2343082
    iget-object v0, p0, LX/GLZ;->b:LX/GLb;

    .line 2343083
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2343084
    if-nez v0, :cond_0

    .line 2343085
    :goto_0
    return-void

    .line 2343086
    :cond_0
    iget-object v0, p0, LX/GLZ;->b:LX/GLb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GLb;->e(LX/GLb;Z)V

    .line 2343087
    iget-object v0, p0, LX/GLZ;->a:LX/0Vd;

    invoke-virtual {v0, p1}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V

    .line 2343088
    iget-object v0, p0, LX/GLZ;->b:LX/GLb;

    .line 2343089
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343090
    new-instance v1, LX/GFA;

    invoke-direct {v1}, LX/GFA;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2343091
    iget-object v0, p0, LX/GLZ;->b:LX/GLb;

    .line 2343092
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2343093
    if-nez v0, :cond_0

    .line 2343094
    :goto_0
    return-void

    .line 2343095
    :cond_0
    iget-object v0, p0, LX/GLZ;->b:LX/GLb;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GLb;->e(LX/GLb;Z)V

    .line 2343096
    iget-object v0, p0, LX/GLZ;->a:LX/0Vd;

    invoke-virtual {v0, p1}, LX/0Vd;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2343097
    check-cast p1, LX/0Px;

    invoke-direct {p0, p1}, LX/GLZ;->a(LX/0Px;)V

    return-void
.end method
