.class public final LX/GN8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public final synthetic b:LX/GNC;


# direct methods
.method public constructor <init>(LX/GNC;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 0

    .prologue
    .line 2345468
    iput-object p1, p0, LX/GN8;->b:LX/GNC;

    iput-object p2, p0, LX/GN8;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const v2, 0x5a0003

    .line 2345469
    if-eqz p2, :cond_1

    iget-object v0, p0, LX/GN8;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PAUSED:LX/GGB;

    if-ne v0, v1, :cond_1

    .line 2345470
    new-instance v0, LX/GGN;

    iget-object v1, p0, LX/GN8;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {v0, v1}, LX/GGN;-><init>(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2345471
    invoke-virtual {v0}, LX/GGN;->b()Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v0

    .line 2345472
    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    .line 2345473
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    .line 2345474
    iget-object v1, p0, LX/GN8;->b:LX/GNC;

    iget-object v1, v1, LX/GNC;->b:LX/GDm;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/A8v;->RESUME:LX/A8v;

    invoke-virtual {v1, v0, v2, v3}, LX/GDm;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;LX/A8v;)V

    .line 2345475
    :cond_0
    :goto_0
    return-void

    .line 2345476
    :cond_1
    if-nez p2, :cond_0

    .line 2345477
    iget-object v0, p0, LX/GN8;->b:LX/GNC;

    iget-object v0, v0, LX/GNC;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2345478
    iget-object v0, p0, LX/GN8;->b:LX/GNC;

    iget-object v0, v0, LX/GNC;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/GN8;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2345479
    iget-object v0, p0, LX/GN8;->b:LX/GNC;

    iget-object v1, p0, LX/GN8;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0, p1, v1}, LX/GNC;->c(LX/GNC;Landroid/view/View;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    goto :goto_0
.end method
