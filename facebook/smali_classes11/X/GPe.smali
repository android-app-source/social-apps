.class public LX/GPe;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/adspayments/protocol/GetBrazilianAddressDetailsParams;",
        "Lcom/facebook/adspayments/model/BusinessAddressDetails;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/GPe;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349240
    const-class v0, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2349241
    return-void
.end method

.method public static a(LX/0QB;)LX/GPe;
    .locals 4

    .prologue
    .line 2349242
    sget-object v0, LX/GPe;->c:LX/GPe;

    if-nez v0, :cond_1

    .line 2349243
    const-class v1, LX/GPe;

    monitor-enter v1

    .line 2349244
    :try_start_0
    sget-object v0, LX/GPe;->c:LX/GPe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2349245
    if-eqz v2, :cond_0

    .line 2349246
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2349247
    new-instance p0, LX/GPe;

    invoke-static {v0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {p0, v3}, LX/GPe;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349248
    move-object v0, p0

    .line 2349249
    sput-object v0, LX/GPe;->c:LX/GPe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2349250
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2349251
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2349252
    :cond_1
    sget-object v0, LX/GPe;->c:LX/GPe;

    return-object v0

    .line 2349253
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2349254
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/1pN;)Lcom/facebook/adspayments/model/BusinessAddressDetails;
    .locals 2

    .prologue
    .line 2349255
    invoke-virtual {p0}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 2349256
    new-instance v1, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    invoke-direct {v1, v0}, Lcom/facebook/adspayments/model/BusinessAddressDetails;-><init>(LX/0lF;)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2349257
    check-cast p1, Lcom/facebook/adspayments/protocol/GetBrazilianAddressDetailsParams;

    .line 2349258
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "/brazil_zip_details"

    .line 2349259
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2349260
    move-object v0, v0

    .line 2349261
    const-string v1, "get_brazil_address_details"

    .line 2349262
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349263
    move-object v0, v0

    .line 2349264
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "zip"

    .line 2349265
    iget-object v3, p1, Lcom/facebook/adspayments/protocol/GetBrazilianAddressDetailsParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2349266
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2349267
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2349268
    move-object v0, v0

    .line 2349269
    const-string v1, "GET"

    .line 2349270
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349271
    move-object v0, v0

    .line 2349272
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2349273
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349274
    move-object v0, v0

    .line 2349275
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/os/Parcelable;LX/1pN;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 2349276
    invoke-static {p2}, LX/GPe;->a(LX/1pN;)Lcom/facebook/adspayments/model/BusinessAddressDetails;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2349277
    invoke-static {p2}, LX/GPe;->a(LX/1pN;)Lcom/facebook/adspayments/model/BusinessAddressDetails;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349278
    const-string v0, "get_brazilian_address_details"

    return-object v0
.end method
