.class public LX/FBu;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FBu;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2209853
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2209854
    sget-object v0, LX/0ax;->gI:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->ZERO_DIALOG_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2209855
    return-void
.end method

.method public static a(LX/0QB;)LX/FBu;
    .locals 3

    .prologue
    .line 2209856
    sget-object v0, LX/FBu;->a:LX/FBu;

    if-nez v0, :cond_1

    .line 2209857
    const-class v1, LX/FBu;

    monitor-enter v1

    .line 2209858
    :try_start_0
    sget-object v0, LX/FBu;->a:LX/FBu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2209859
    if-eqz v2, :cond_0

    .line 2209860
    :try_start_1
    new-instance v0, LX/FBu;

    invoke-direct {v0}, LX/FBu;-><init>()V

    .line 2209861
    move-object v0, v0

    .line 2209862
    sput-object v0, LX/FBu;->a:LX/FBu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2209863
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2209864
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2209865
    :cond_1
    sget-object v0, LX/FBu;->a:LX/FBu;

    return-object v0

    .line 2209866
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2209867
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
