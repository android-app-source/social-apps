.class public final LX/H4h;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/H4i;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

.field public d:Ljava/lang/String;

.field public e:Landroid/net/Uri;

.field public f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

.field public g:Ljava/lang/String;

.field public h:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

.field public final synthetic k:LX/H4i;


# direct methods
.method public constructor <init>(LX/H4i;)V
    .locals 1

    .prologue
    .line 2423321
    iput-object p1, p0, LX/H4h;->k:LX/H4i;

    .line 2423322
    move-object v0, p1

    .line 2423323
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2423324
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2423320
    const-string v0, "NotificationsReactionImageBlockComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2423282
    if-ne p0, p1, :cond_1

    .line 2423283
    :cond_0
    :goto_0
    return v0

    .line 2423284
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2423285
    goto :goto_0

    .line 2423286
    :cond_3
    check-cast p1, LX/H4h;

    .line 2423287
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2423288
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2423289
    if-eq v2, v3, :cond_0

    .line 2423290
    iget-object v2, p0, LX/H4h;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/H4h;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/H4h;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2423291
    goto :goto_0

    .line 2423292
    :cond_5
    iget-object v2, p1, LX/H4h;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 2423293
    :cond_6
    iget-object v2, p0, LX/H4h;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/H4h;->b:Ljava/lang/String;

    iget-object v3, p1, LX/H4h;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2423294
    goto :goto_0

    .line 2423295
    :cond_8
    iget-object v2, p1, LX/H4h;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2423296
    :cond_9
    iget-object v2, p0, LX/H4h;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/H4h;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    iget-object v3, p1, LX/H4h;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2423297
    goto :goto_0

    .line 2423298
    :cond_b
    iget-object v2, p1, LX/H4h;->c:Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    if-nez v2, :cond_a

    .line 2423299
    :cond_c
    iget-object v2, p0, LX/H4h;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/H4h;->d:Ljava/lang/String;

    iget-object v3, p1, LX/H4h;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2423300
    goto :goto_0

    .line 2423301
    :cond_e
    iget-object v2, p1, LX/H4h;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2423302
    :cond_f
    iget-object v2, p0, LX/H4h;->e:Landroid/net/Uri;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/H4h;->e:Landroid/net/Uri;

    iget-object v3, p1, LX/H4h;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2423303
    goto :goto_0

    .line 2423304
    :cond_11
    iget-object v2, p1, LX/H4h;->e:Landroid/net/Uri;

    if-nez v2, :cond_10

    .line 2423305
    :cond_12
    iget-object v2, p0, LX/H4h;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/H4h;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p1, LX/H4h;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 2423306
    goto/16 :goto_0

    .line 2423307
    :cond_14
    iget-object v2, p1, LX/H4h;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-nez v2, :cond_13

    .line 2423308
    :cond_15
    iget-object v2, p0, LX/H4h;->g:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/H4h;->g:Ljava/lang/String;

    iget-object v3, p1, LX/H4h;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 2423309
    goto/16 :goto_0

    .line 2423310
    :cond_17
    iget-object v2, p1, LX/H4h;->g:Ljava/lang/String;

    if-nez v2, :cond_16

    .line 2423311
    :cond_18
    iget-object v2, p0, LX/H4h;->h:LX/2km;

    if-eqz v2, :cond_1a

    iget-object v2, p0, LX/H4h;->h:LX/2km;

    iget-object v3, p1, LX/H4h;->h:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 2423312
    goto/16 :goto_0

    .line 2423313
    :cond_1a
    iget-object v2, p1, LX/H4h;->h:LX/2km;

    if-nez v2, :cond_19

    .line 2423314
    :cond_1b
    iget-object v2, p0, LX/H4h;->i:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_1d

    iget-object v2, p0, LX/H4h;->i:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/H4h;->i:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1c
    move v0, v1

    .line 2423315
    goto/16 :goto_0

    .line 2423316
    :cond_1d
    iget-object v2, p1, LX/H4h;->i:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_1c

    .line 2423317
    :cond_1e
    iget-object v2, p0, LX/H4h;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_1f

    iget-object v2, p0, LX/H4h;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p1, LX/H4h;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2423318
    goto/16 :goto_0

    .line 2423319
    :cond_1f
    iget-object v2, p1, LX/H4h;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
