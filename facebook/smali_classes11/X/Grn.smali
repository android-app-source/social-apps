.class public LX/Grn;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0y;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CqV;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/68S;

.field public d:LX/67q;

.field public e:I

.field public f:Landroid/view/View;

.field public g:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

.field public h:Z

.field public i:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

.field private j:LX/Ctg;

.field private k:LX/68u;

.field public l:Lcom/facebook/fbui/glyph/GlyphView;

.field public m:I

.field private n:I

.field public o:Z


# direct methods
.method public constructor <init>(LX/Ctg;Lcom/facebook/storelocator/StoreLocatorMapDelegate;Landroid/view/View;Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;Lcom/facebook/fbui/glyph/GlyphView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2398715
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398716
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2398717
    iput-object v0, p0, LX/Grn;->a:LX/0Ot;

    .line 2398718
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2398719
    iput-object v0, p0, LX/Grn;->b:LX/0Ot;

    .line 2398720
    iput-boolean v2, p0, LX/Grn;->h:Z

    .line 2398721
    const-class v0, LX/Grn;

    invoke-static {v0, p0}, LX/Grn;->a(Ljava/lang/Class;LX/02k;)V

    .line 2398722
    iput-object p1, p0, LX/Grn;->j:LX/Ctg;

    .line 2398723
    iput-object p5, p0, LX/Grn;->l:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2398724
    iput-object p3, p0, LX/Grn;->f:Landroid/view/View;

    .line 2398725
    iput-object p4, p0, LX/Grn;->g:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    .line 2398726
    iput-object p2, p0, LX/Grn;->i:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 2398727
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1bf4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Grn;->m:I

    .line 2398728
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Grn;->n:I

    .line 2398729
    new-instance v0, LX/Grl;

    invoke-direct {v0, p0}, LX/Grl;-><init>(LX/Grn;)V

    iput-object v0, p0, LX/Grn;->d:LX/67q;

    .line 2398730
    new-instance v0, LX/Grm;

    invoke-direct {v0, p0}, LX/Grm;-><init>(LX/Grn;)V

    iput-object v0, p0, LX/Grn;->c:LX/68S;

    .line 2398731
    iput-boolean v2, p0, LX/Grn;->o:Z

    .line 2398732
    iget-object v0, p0, LX/Grn;->l:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Grk;

    invoke-direct {v1, p0}, LX/Grk;-><init>(LX/Grn;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2398733
    return-void
.end method

.method public static a(LX/Grn;II)V
    .locals 4

    .prologue
    .line 2398706
    invoke-static {p1, p2}, LX/68u;->a(II)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/Grn;->k:LX/68u;

    .line 2398707
    iget-object v0, p0, LX/Grn;->k:LX/68u;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, LX/68u;->a(J)LX/68u;

    .line 2398708
    iget-object v0, p0, LX/Grn;->k:LX/68u;

    new-instance v1, LX/3tD;

    invoke-direct {v1}, LX/3tD;-><init>()V

    invoke-virtual {v0, v1}, LX/68u;->a(Landroid/view/animation/Interpolator;)V

    .line 2398709
    iget-object v0, p0, LX/Grn;->k:LX/68u;

    iget-object v1, p0, LX/Grn;->d:LX/67q;

    invoke-virtual {v0, v1}, LX/68u;->a(LX/67q;)V

    .line 2398710
    iget-object v0, p0, LX/Grn;->k:LX/68u;

    iget-object v1, p0, LX/Grn;->c:LX/68S;

    invoke-virtual {v0, v1}, LX/68u;->a(LX/67p;)V

    .line 2398711
    iget-object v0, p0, LX/Grn;->k:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    .line 2398712
    return-void
.end method

.method public static a(LX/Grn;Z)V
    .locals 3

    .prologue
    .line 2398713
    iget-object v0, p0, LX/Grn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqV;

    sget-object v1, LX/CqU;->SPHERICAL_VIDEO:LX/CqU;

    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, LX/CqV;->a(ZLX/CqU;Landroid/view/View;)V

    .line 2398714
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Grn;

    const/16 v2, 0x359b

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x323e

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, LX/Grn;->a:LX/0Ot;

    iput-object v1, p1, LX/Grn;->b:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(LX/CrS;)V
    .locals 6

    .prologue
    .line 2398701
    iget-object v0, p0, LX/Grn;->j:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 2398702
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, LX/CrW;->i()I

    move-result v2

    iget v3, p0, LX/Grn;->n:I

    add-int/2addr v2, v3

    invoke-virtual {v0}, LX/CrW;->g()I

    move-result v3

    iget-object v4, p0, LX/Grn;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, LX/Grn;->n:I

    add-int/2addr v3, v4

    invoke-virtual {v0}, LX/CrW;->j()I

    move-result v4

    iget v5, p0, LX/Grn;->n:I

    sub-int/2addr v4, v5

    invoke-virtual {v0}, LX/CrW;->h()I

    move-result v0

    iget v5, p0, LX/Grn;->n:I

    sub-int/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398703
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 2398704
    iget-object v2, p0, LX/Grn;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-interface {v0, v2, v1}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2398705
    return-void
.end method
