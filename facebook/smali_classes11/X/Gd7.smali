.class public final enum LX/Gd7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gd7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gd7;

.field public static final enum NOOP:LX/Gd7;

.field public static final enum REVERT:LX/Gd7;

.field public static final enum UPDATE:LX/Gd7;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2373206
    new-instance v0, LX/Gd7;

    const-string v1, "NOOP"

    invoke-direct {v0, v1, v2}, LX/Gd7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gd7;->NOOP:LX/Gd7;

    .line 2373207
    new-instance v0, LX/Gd7;

    const-string v1, "REVERT"

    invoke-direct {v0, v1, v3}, LX/Gd7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gd7;->REVERT:LX/Gd7;

    .line 2373208
    new-instance v0, LX/Gd7;

    const-string v1, "UPDATE"

    invoke-direct {v0, v1, v4}, LX/Gd7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gd7;->UPDATE:LX/Gd7;

    .line 2373209
    const/4 v0, 0x3

    new-array v0, v0, [LX/Gd7;

    sget-object v1, LX/Gd7;->NOOP:LX/Gd7;

    aput-object v1, v0, v2

    sget-object v1, LX/Gd7;->REVERT:LX/Gd7;

    aput-object v1, v0, v3

    sget-object v1, LX/Gd7;->UPDATE:LX/Gd7;

    aput-object v1, v0, v4

    sput-object v0, LX/Gd7;->$VALUES:[LX/Gd7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2373205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gd7;
    .locals 1

    .prologue
    .line 2373211
    const-class v0, LX/Gd7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gd7;

    return-object v0
.end method

.method public static values()[LX/Gd7;
    .locals 1

    .prologue
    .line 2373210
    sget-object v0, LX/Gd7;->$VALUES:[LX/Gd7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gd7;

    return-object v0
.end method
