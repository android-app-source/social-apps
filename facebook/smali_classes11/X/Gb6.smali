.class public LX/Gb6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13F;
.implements LX/13G;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:LX/10M;

.field private c:LX/10N;

.field private d:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

.field public e:Landroid/content/res/Resources;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0SG;

.field private final h:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/10M;LX/10N;Landroid/content/res/Resources;LX/0Or;LX/0SG;LX/0ad;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/10M;",
            "LX/10N;",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0SG;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369063
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gb6;->d:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    .line 2369064
    iput-object p1, p0, LX/Gb6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2369065
    iput-object p2, p0, LX/Gb6;->b:LX/10M;

    .line 2369066
    iput-object p3, p0, LX/Gb6;->c:LX/10N;

    .line 2369067
    iput-object p4, p0, LX/Gb6;->e:Landroid/content/res/Resources;

    .line 2369068
    iput-object p5, p0, LX/Gb6;->f:LX/0Or;

    .line 2369069
    iput-object p6, p0, LX/Gb6;->g:LX/0SG;

    .line 2369070
    iput-object p7, p0, LX/Gb6;->h:LX/0ad;

    .line 2369071
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2369061
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2369060
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 2369044
    iget-object v0, p0, LX/Gb6;->e:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x3

    if-lt v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2369045
    if-nez v0, :cond_3

    iget-object v0, p0, LX/Gb6;->c:LX/10N;

    invoke-virtual {v0}, LX/10N;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Gb6;->c:LX/10N;

    invoke-virtual {v0}, LX/10N;->f()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2369046
    iget-object v0, p0, LX/Gb6;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2369047
    if-eqz v0, :cond_3

    iget-object v1, p0, LX/Gb6;->c:LX/10N;

    invoke-virtual {v1}, LX/10N;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Gb6;->b:LX/10M;

    invoke-virtual {v1, v0}, LX/10M;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Gb6;->c:LX/10N;

    invoke-virtual {v1}, LX/10N;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2369048
    :cond_0
    sget-object v1, LX/26p;->k:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    .line 2369049
    iget-object v2, p0, LX/Gb6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2369050
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 2369051
    :goto_1
    return-object v0

    .line 2369052
    :cond_1
    iget-object v1, p0, LX/Gb6;->d:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    if-eqz v1, :cond_3

    .line 2369053
    iget-object v2, p0, LX/Gb6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->l:LX/0Tn;

    invoke-virtual {v1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2369054
    iget-object v2, p0, LX/Gb6;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/26p;->m:LX/0Tn;

    invoke-virtual {v3, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v2, v0, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 2369055
    iget-object v0, p0, LX/Gb6;->d:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    iget v0, v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;->triggerGeneration:I

    if-le v0, v1, :cond_3

    .line 2369056
    iget-object v0, p0, LX/Gb6;->h:LX/0ad;

    sget-wide v4, LX/GbC;->a:J

    const-wide/32 v6, 0x48190800

    invoke-interface {v0, v4, v5, v6, v7}, LX/0ad;->a(JJ)J

    move-result-wide v0

    .line 2369057
    cmp-long v4, v2, v8

    if-eqz v4, :cond_2

    iget-object v4, p0, LX/Gb6;->g:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_3

    .line 2369058
    :cond_2
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_1

    .line 2369059
    :cond_3
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2369033
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/devicebasedlogin/nux/ActivateDeviceBasedLoginNuxActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2369034
    iget-object v1, p0, LX/Gb6;->d:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    if-eqz v1, :cond_0

    .line 2369035
    const-string v1, "generation"

    iget-object v2, p0, LX/Gb6;->d:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    iget v2, v2, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;->triggerGeneration:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2369036
    :cond_0
    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 2369043
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 2369040
    if-eqz p1, :cond_0

    .line 2369041
    check-cast p1, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    iput-object p1, p0, LX/Gb6;->d:Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    .line 2369042
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2369039
    const-string v0, "2504"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2369038
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2369037
    const-class v0, Lcom/facebook/devicebasedlogin/nux/DeviceBasedLoginNuxInterstitialFetchResult;

    return-object v0
.end method
