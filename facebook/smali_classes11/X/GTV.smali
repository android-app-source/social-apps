.class public final LX/GTV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;",
        ">;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/GTZ;


# direct methods
.method public constructor <init>(LX/GTZ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2355552
    iput-object p1, p0, LX/GTV;->b:LX/GTZ;

    iput-object p2, p0, LX/GTV;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2355553
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2355554
    if-eqz p1, :cond_0

    .line 2355555
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2355556
    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2355557
    :cond_0
    const-string v0, "redirect_dashboard"

    .line 2355558
    :goto_0
    return-object v0

    .line 2355559
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2355560
    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    move-result-object v0

    .line 2355561
    const-string v2, "invite_notification"

    iget-object v3, p0, LX/GTV;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2355562
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "redirect_dashboard"

    goto :goto_0

    :cond_2
    const-string v0, "invite_notification"

    goto :goto_0

    .line 2355563
    :cond_3
    iget-object v2, p0, LX/GTV;->b:LX/GTZ;

    iget-object v2, v2, LX/GTZ;->c:LX/0Uh;

    const/16 v3, 0x221

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2355564
    const-string v0, "one_page"

    goto :goto_0

    .line 2355565
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->c()Z

    move-result v2

    .line 2355566
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->d()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->d()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 2355567
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->d()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->a()I

    move-result v0

    .line 2355568
    :goto_1
    if-eqz v2, :cond_6

    if-lez v0, :cond_6

    const-string v0, "traveling"

    goto :goto_0

    .line 2355569
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 2355570
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->a()I

    move-result v0

    goto :goto_1

    .line 2355571
    :cond_6
    const-string v0, "one_page"

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method
