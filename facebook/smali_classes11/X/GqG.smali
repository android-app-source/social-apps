.class public LX/GqG;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Gpa;",
        ">;",
        "Lcom/facebook/instantshopping/view/block/ExpandableBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CIh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final d:Landroid/widget/ImageView;

.field public final e:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final f:Landroid/widget/RelativeLayout;

.field public g:Z


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2396267
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396268
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GqG;->g:Z

    .line 2396269
    const v0, 0x7f0d181c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/GqG;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396270
    const v0, 0x7f0d181d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/GqG;->d:Landroid/widget/ImageView;

    .line 2396271
    const v0, 0x7f0d181e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396272
    const v0, 0x7f0d181b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/GqG;->f:Landroid/widget/RelativeLayout;

    .line 2396273
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/GqG;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object p1

    check-cast p1, LX/Go0;

    invoke-static {v0}, LX/CIh;->b(LX/0QB;)LX/CIh;

    move-result-object v0

    check-cast v0, LX/CIh;

    iput-object p1, p0, LX/GqG;->a:LX/Go0;

    iput-object v0, p0, LX/GqG;->b:LX/CIh;

    .line 2396274
    return-void
.end method

.method public static c(LX/GqG;LX/GoE;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x64

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2396252
    iget-object v0, p0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 2396253
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GqG;->g:Z

    .line 2396254
    iget-object v0, p0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setAlpha(F)V

    .line 2396255
    iget-object v0, p0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 2396256
    iget-object v0, p0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2396257
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/GqG;->d:Landroid/widget/ImageView;

    const v2, 0x7f0400ba

    invoke-static {v0, v1, v2}, LX/CIX;->a(Landroid/content/Context;Landroid/view/View;I)V

    .line 2396258
    :cond_0
    :goto_0
    iget-object v0, p0, LX/GqG;->a:LX/Go0;

    const-string v1, "instant_shopping_element_click"

    new-instance v2, LX/GqF;

    invoke-direct {v2, p0, p1}, LX/GqF;-><init>(LX/GqG;LX/GoE;)V

    invoke-virtual {v0, v1, v2}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2396259
    return-void

    .line 2396260
    :cond_1
    iget-object v0, p0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2396261
    iput-boolean v3, p0, LX/GqG;->g:Z

    .line 2396262
    iget-object v0, p0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/GqE;

    invoke-direct {v1, p0}, LX/GqE;-><init>(LX/GqG;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2396263
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/GqG;->d:Landroid/widget/ImageView;

    const v2, 0x7f0400b1

    invoke-static {v0, v1, v2}, LX/CIX;->a(Landroid/content/Context;Landroid/view/View;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2396264
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 2396265
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2396266
    return-void
.end method
