.class public final enum LX/FP6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FP6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FP6;

.field public static final enum FILTER_REQUEST:LX/FP6;

.field public static final enum MAP_REGION_REQUEST:LX/FP6;

.field public static final enum PAGINATION_REQUEST:LX/FP6;

.field public static final enum RESULT_LIST_REQUEST:LX/FP6;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2236645
    new-instance v0, LX/FP6;

    const-string v1, "FILTER_REQUEST"

    invoke-direct {v0, v1, v2}, LX/FP6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP6;->FILTER_REQUEST:LX/FP6;

    .line 2236646
    new-instance v0, LX/FP6;

    const-string v1, "RESULT_LIST_REQUEST"

    invoke-direct {v0, v1, v3}, LX/FP6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP6;->RESULT_LIST_REQUEST:LX/FP6;

    .line 2236647
    new-instance v0, LX/FP6;

    const-string v1, "PAGINATION_REQUEST"

    invoke-direct {v0, v1, v4}, LX/FP6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP6;->PAGINATION_REQUEST:LX/FP6;

    .line 2236648
    new-instance v0, LX/FP6;

    const-string v1, "MAP_REGION_REQUEST"

    invoke-direct {v0, v1, v5}, LX/FP6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP6;->MAP_REGION_REQUEST:LX/FP6;

    .line 2236649
    const/4 v0, 0x4

    new-array v0, v0, [LX/FP6;

    sget-object v1, LX/FP6;->FILTER_REQUEST:LX/FP6;

    aput-object v1, v0, v2

    sget-object v1, LX/FP6;->RESULT_LIST_REQUEST:LX/FP6;

    aput-object v1, v0, v3

    sget-object v1, LX/FP6;->PAGINATION_REQUEST:LX/FP6;

    aput-object v1, v0, v4

    sget-object v1, LX/FP6;->MAP_REGION_REQUEST:LX/FP6;

    aput-object v1, v0, v5

    sput-object v0, LX/FP6;->$VALUES:[LX/FP6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2236650
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FP6;
    .locals 1

    .prologue
    .line 2236651
    const-class v0, LX/FP6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FP6;

    return-object v0
.end method

.method public static values()[LX/FP6;
    .locals 1

    .prologue
    .line 2236652
    sget-object v0, LX/FP6;->$VALUES:[LX/FP6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FP6;

    return-object v0
.end method
