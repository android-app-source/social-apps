.class public final LX/Fvm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/TimelineIntroCardBioView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/TimelineIntroCardBioView;)V
    .locals 0

    .prologue
    .line 2301907
    iput-object p1, p0, LX/Fvm;->a:Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2301911
    iget-object v0, p0, LX/Fvm;->a:Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    iget-object v0, v0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fvm;->a:Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    iget-object v0, v0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2301912
    iget-object v0, p0, LX/Fvm;->a:Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    iget-object v0, v0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2301913
    :cond_0
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2301908
    iget-object v0, p0, LX/Fvm;->a:Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    iget-object v0, v0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 2301909
    iget-object v0, p0, LX/Fvm;->a:Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    iget-object v0, v0, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 2301910
    :cond_0
    return-void
.end method
