.class public final LX/FQf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field public final a:LX/FQY;

.field private final b:LX/0ad;

.field public final c:LX/17T;


# direct methods
.method public constructor <init>(LX/FQY;LX/0ad;LX/17T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2239570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239571
    iput-object p1, p0, LX/FQf;->a:LX/FQY;

    .line 2239572
    iput-object p2, p0, LX/FQf;->b:LX/0ad;

    .line 2239573
    iput-object p3, p0, LX/FQf;->c:LX/17T;

    .line 2239574
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2239575
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2239576
    iget-object v0, p0, LX/FQf;->a:LX/FQY;

    invoke-virtual {v0, v2, v3}, LX/FQY;->a(J)Landroid/content/Intent;

    move-result-object v0

    .line 2239577
    if-eqz v0, :cond_0

    .line 2239578
    const-string v1, "popup_state"

    sget-object v4, LX/8A0;->MESSAGES:LX/8A0;

    invoke-virtual {v4}, LX/8A0;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2239579
    :cond_0
    move-object v0, v0

    .line 2239580
    if-eqz v0, :cond_1

    .line 2239581
    :goto_0
    return-object v0

    .line 2239582
    :cond_1
    iget-object v0, p0, LX/FQf;->b:LX/0ad;

    sget-short v1, LX/17g;->j:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2239583
    iget-object v0, p0, LX/FQf;->c:LX/17T;

    const-string v1, "com.facebook.pages.app"

    invoke-virtual {v0, v1}, LX/17T;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    move-object v0, v0

    .line 2239584
    goto :goto_0

    .line 2239585
    :cond_2
    const/4 p0, 0x1

    .line 2239586
    const-string v0, "https://m.facebook.com/messages/?pageID=%s"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2239587
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "force_in_app_browser"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "should_hide_menu"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "iab_origin"

    const-string v4, "messenger"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "custom_user_agent_suffix"

    const-string v4, "FB4A_Messaging_MSite"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2239588
    move-object v0, v0

    .line 2239589
    goto :goto_0
.end method
