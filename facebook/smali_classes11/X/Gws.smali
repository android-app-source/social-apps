.class public LX/Gws;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWW;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:Ljava/lang/String;

.field private static volatile j:LX/Gws;


# instance fields
.field public a:Z

.field public b:Z

.field public c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/BWV;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/48V;

.field public f:Landroid/webkit/WebView;

.field public final g:LX/44G;

.field private h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private i:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2407534
    const-class v0, LX/Gws;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Gws;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/44G;LX/48V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03R;)V
    .locals 1
    .param p4    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2407525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407526
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gws;->a:Z

    .line 2407527
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gws;->b:Z

    .line 2407528
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Gws;->c:Ljava/util/Set;

    .line 2407529
    iput-object p2, p0, LX/Gws;->e:LX/48V;

    .line 2407530
    iput-object p1, p0, LX/Gws;->g:LX/44G;

    .line 2407531
    iput-object p3, p0, LX/Gws;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2407532
    iput-object p4, p0, LX/Gws;->i:LX/03R;

    .line 2407533
    return-void
.end method

.method public static a(LX/0QB;)LX/Gws;
    .locals 7

    .prologue
    .line 2407535
    sget-object v0, LX/Gws;->j:LX/Gws;

    if-nez v0, :cond_1

    .line 2407536
    const-class v1, LX/Gws;

    monitor-enter v1

    .line 2407537
    :try_start_0
    sget-object v0, LX/Gws;->j:LX/Gws;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2407538
    if-eqz v2, :cond_0

    .line 2407539
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2407540
    new-instance p0, LX/Gws;

    invoke-static {v0}, LX/44G;->b(LX/0QB;)LX/44G;

    move-result-object v3

    check-cast v3, LX/44G;

    invoke-static {v0}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v4

    check-cast v4, LX/48V;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v6

    check-cast v6, LX/03R;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Gws;-><init>(LX/44G;LX/48V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03R;)V

    .line 2407541
    move-object v0, p0

    .line 2407542
    sput-object v0, LX/Gws;->j:LX/Gws;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2407543
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2407544
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2407545
    :cond_1
    sget-object v0, LX/Gws;->j:LX/Gws;

    return-object v0

    .line 2407546
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2407547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/BWV;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2407508
    invoke-static {p1, v3}, Lcom/facebook/katana/service/AppSession;->b(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 2407509
    const-class v1, LX/Gws;

    monitor-enter v1

    .line 2407510
    if-eqz p2, :cond_0

    .line 2407511
    :try_start_0
    iget-object v2, p0, LX/Gws;->c:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2407512
    :cond_0
    iget-boolean v2, p0, LX/Gws;->b:Z

    if-nez v2, :cond_1

    if-nez v0, :cond_2

    .line 2407513
    :cond_1
    monitor-exit v1

    .line 2407514
    :goto_0
    return-void

    .line 2407515
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gws;->b:Z

    .line 2407516
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2407517
    iput-boolean v3, p0, LX/Gws;->a:Z

    .line 2407518
    iget-object v0, p0, LX/Gws;->f:Landroid/webkit/WebView;

    invoke-static {v0}, LX/0sL;->b(Ljava/lang/Object;)V

    .line 2407519
    new-instance v0, Lcom/facebook/webview/BasicWebView;

    invoke-direct {v0, p1}, Lcom/facebook/webview/BasicWebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Gws;->f:Landroid/webkit/WebView;

    .line 2407520
    iget-object v0, p0, LX/Gws;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 2407521
    iget-object v6, p0, LX/Gws;->f:Landroid/webkit/WebView;

    new-instance v0, LX/Gwr;

    invoke-static {p1}, LX/38I;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Gws;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v5, p0, LX/Gws;->i:LX/03R;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/Gwr;-><init>(LX/Gws;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03R;)V

    invoke-virtual {v6, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2407522
    invoke-static {p1}, LX/38I;->p(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/FBg;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2407523
    iget-object v1, p0, LX/Gws;->e:LX/48V;

    iget-object v2, p0, LX/Gws;->f:Landroid/webkit/WebView;

    invoke-virtual {v1, v2, v0}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_0

    .line 2407524
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
