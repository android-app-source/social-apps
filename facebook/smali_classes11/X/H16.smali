.class public final LX/H16;
.super LX/1OM;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H0w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2414741
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2414742
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/H16;->b:Ljava/util/List;

    .line 2414743
    iput-object p1, p0, LX/H16;->a:Landroid/content/Context;

    .line 2414744
    check-cast p1, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    iget-object v0, p1, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->q:Ljava/util/List;

    .line 2414745
    iget-object v1, p0, LX/H16;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2414746
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H0v;

    .line 2414747
    instance-of p1, v1, LX/H0w;

    if-eqz p1, :cond_0

    .line 2414748
    iget-object p1, p0, LX/H16;->b:Ljava/util/List;

    check-cast v1, LX/H0w;

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2414749
    :cond_1
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2414750
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2414751
    const/4 v3, -0x1

    .line 2414752
    new-instance v0, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, LX/H16;->a:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    .line 2414753
    new-instance v1, LX/1au;

    invoke-direct {v1, v3, v3}, LX/1au;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2414754
    new-instance v1, LX/62U;

    invoke-direct {v1, v0}, LX/62U;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2414755
    iget-object v0, p0, LX/H16;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0w;

    .line 2414756
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2414757
    invoke-virtual {v0}, LX/H0v;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2414758
    invoke-virtual {v0}, LX/H0w;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2414759
    invoke-virtual {v0}, LX/H0w;->h()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    .line 2414760
    invoke-static {v2}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    if-lt v4, v3, :cond_0

    .line 2414761
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 p1, 0x0

    add-int/lit8 p2, v3, -0x3

    invoke-virtual {v2, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p1, "..."

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2414762
    :cond_0
    move-object v2, v2

    .line 2414763
    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2414764
    new-instance v2, LX/H15;

    invoke-direct {v2, p0, v0}, LX/H15;-><init>(LX/H16;LX/H0w;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2414765
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2414766
    iget-object v0, p0, LX/H16;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
