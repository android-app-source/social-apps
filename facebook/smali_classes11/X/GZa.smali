.class public final LX/GZa;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;)V
    .locals 0

    .prologue
    .line 2366723
    iput-object p1, p0, LX/GZa;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2366724
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2366725
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2366726
    if-nez p1, :cond_0

    .line 2366727
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Null storefront query response from server"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 2366728
    :goto_0
    return-void

    .line 2366729
    :cond_0
    iget-object v1, p0, LX/GZa;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    .line 2366730
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2366731
    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;

    .line 2366732
    iget-object v2, v1, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->w:LX/GZH;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/GZH;->b(Z)V

    .line 2366733
    invoke-static {v1, v0}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->b(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;Lcom/facebook/commerce/storefront/graphql/FetchStorefrontQueryModels$FetchStorefrontQueryModel;)V

    .line 2366734
    iget-object v2, v1, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->g:LX/7iU;

    const v0, 0x6c0002

    .line 2366735
    iget-object p0, v2, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {p0, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2366736
    iget-object p0, v2, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {p0, v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2366737
    :cond_1
    goto :goto_0
.end method
