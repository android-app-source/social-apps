.class public final LX/GPJ;
.super LX/GOC;
.source ""


# instance fields
.field public final synthetic a:LX/GP6;


# direct methods
.method public constructor <init>(LX/GP6;)V
    .locals 0

    .prologue
    .line 2348581
    iput-object p1, p0, LX/GPJ;->a:LX/GP6;

    invoke-direct {p0}, LX/GOC;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 2348582
    iget-object v0, p0, LX/GPJ;->a:LX/GP6;

    invoke-virtual {v0}, LX/GP4;->a()Z

    move-result v1

    .line 2348583
    iget-object v2, p0, LX/GPJ;->a:LX/GP6;

    if-nez v1, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget-object v3, p0, LX/GPJ;->a:LX/GP6;

    invoke-virtual {v3}, LX/GP6;->e()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v3

    invoke-static {v3}, LX/GQC;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)I

    move-result v3

    if-lt v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, LX/GP4;->a(Z)V

    .line 2348584
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/GPJ;->a:LX/GP6;

    iget-object v0, v0, LX/GP4;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2348585
    iget-object v0, p0, LX/GPJ;->a:LX/GP6;

    iget-object v1, p0, LX/GPJ;->a:LX/GP6;

    iget-object v1, v1, LX/GP6;->d:Ljava/util/concurrent/ExecutorService;

    .line 2348586
    invoke-virtual {v0, v1}, LX/GP4;->a(Ljava/util/concurrent/ExecutorService;)V

    .line 2348587
    :cond_0
    return-void

    .line 2348588
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
