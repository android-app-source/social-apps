.class public LX/Fsj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Fsj;


# instance fields
.field public final a:LX/Fsg;

.field public final b:LX/0tX;


# direct methods
.method public constructor <init>(LX/Fsg;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297479
    iput-object p1, p0, LX/Fsj;->a:LX/Fsg;

    .line 2297480
    iput-object p2, p0, LX/Fsj;->b:LX/0tX;

    .line 2297481
    return-void
.end method

.method public static a(LX/0QB;)LX/Fsj;
    .locals 5

    .prologue
    .line 2297482
    sget-object v0, LX/Fsj;->c:LX/Fsj;

    if-nez v0, :cond_1

    .line 2297483
    const-class v1, LX/Fsj;

    monitor-enter v1

    .line 2297484
    :try_start_0
    sget-object v0, LX/Fsj;->c:LX/Fsj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2297485
    if-eqz v2, :cond_0

    .line 2297486
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2297487
    new-instance p0, LX/Fsj;

    invoke-static {v0}, LX/Fsg;->a(LX/0QB;)LX/Fsg;

    move-result-object v3

    check-cast v3, LX/Fsg;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v3, v4}, LX/Fsj;-><init>(LX/Fsg;LX/0tX;)V

    .line 2297488
    move-object v0, p0

    .line 2297489
    sput-object v0, LX/Fsj;->c:LX/Fsj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297490
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2297491
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2297492
    :cond_1
    sget-object v0, LX/Fsj;->c:LX/Fsj;

    return-object v0

    .line 2297493
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2297494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0v6;LX/0zO;)LX/0zX;
    .locals 2
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;)",
            "LX/0zX",
            "<",
            "LX/FsL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297495
    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297496
    :goto_0
    invoke-static {v0}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/Fsh;

    invoke-direct {v1, p0}, LX/Fsh;-><init>(LX/Fsj;)V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0

    .line 2297497
    :cond_0
    iget-object v0, p0, LX/Fsj;->b:LX/0tX;

    invoke-virtual {v0, p2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/FsA;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method
