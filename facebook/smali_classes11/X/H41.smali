.class public LX/H41;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field public static final a:[LX/H44;

.field public static final b:I


# instance fields
.field public c:LX/H3o;

.field public d:Landroid/widget/TableLayout;

.field public final e:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2422248
    const/16 v0, 0x8

    new-array v0, v0, [LX/H44;

    const/4 v1, 0x0

    sget-object v2, LX/H44;->ALL:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/H44;->RESTAURANTS:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/H44;->COFFEE:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/H44;->NIGHTLIFE:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/H44;->OUTDOORS:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/H44;->ARTS:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/H44;->HOTELS:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/H44;->SHOPPING:LX/H44;

    aput-object v2, v0, v1

    .line 2422249
    sput-object v0, LX/H41;->a:[LX/H44;

    array-length v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    sput v0, LX/H41;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2422250
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/H41;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2422251
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2422246
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/H41;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2422247
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 2422228
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2422229
    new-instance v0, LX/H40;

    invoke-direct {v0, p0}, LX/H40;-><init>(LX/H41;)V

    iput-object v0, p0, LX/H41;->e:Landroid/view/View$OnClickListener;

    .line 2422230
    const v0, 0x7f030bbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2422231
    const v0, 0x7f0d1d39

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, LX/H41;->d:Landroid/widget/TableLayout;

    .line 2422232
    const/4 p3, -0x2

    const/4 v1, 0x0

    .line 2422233
    invoke-virtual {p0}, LX/H41;->getContext()Landroid/content/Context;

    move-result-object v3

    move v0, v1

    .line 2422234
    :goto_0
    sget v2, LX/H41;->b:I

    if-ge v0, v2, :cond_1

    .line 2422235
    new-instance v4, Landroid/widget/TableRow;

    invoke-direct {v4, v3}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    move v2, v1

    .line 2422236
    :goto_1
    const/4 v5, 0x4

    if-ge v2, v5, :cond_0

    .line 2422237
    sget-object v5, LX/H41;->a:[LX/H44;

    mul-int/lit8 p1, v0, 0x4

    add-int/2addr p1, v2

    aget-object v5, v5, p1

    .line 2422238
    invoke-static {v5, v3}, LX/H47;->a(LX/H44;Landroid/content/Context;)LX/H45;

    move-result-object v5

    .line 2422239
    iget-object p1, p0, LX/H41;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, p1}, LX/H45;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2422240
    new-instance p1, Landroid/widget/TableRow$LayoutParams;

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-direct {p1, v1, p3, p2}, Landroid/widget/TableRow$LayoutParams;-><init>(IIF)V

    invoke-virtual {v5, p1}, LX/H45;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2422241
    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 2422242
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2422243
    :cond_0
    iget-object v2, p0, LX/H41;->d:Landroid/widget/TableLayout;

    new-instance v5, Landroid/widget/TableLayout$LayoutParams;

    const/4 p1, -0x1

    invoke-direct {v5, p1, p3}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v4, v5}, Landroid/widget/TableLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2422244
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2422245
    :cond_1
    return-void
.end method
