.class public LX/Ghp;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ghn;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Ghp;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ghq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2384874
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ghp;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ghq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384871
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2384872
    iput-object p1, p0, LX/Ghp;->b:LX/0Ot;

    .line 2384873
    return-void
.end method

.method public static a(LX/0QB;)LX/Ghp;
    .locals 4

    .prologue
    .line 2384875
    sget-object v0, LX/Ghp;->c:LX/Ghp;

    if-nez v0, :cond_1

    .line 2384876
    const-class v1, LX/Ghp;

    monitor-enter v1

    .line 2384877
    :try_start_0
    sget-object v0, LX/Ghp;->c:LX/Ghp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2384878
    if-eqz v2, :cond_0

    .line 2384879
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2384880
    new-instance v3, LX/Ghp;

    const/16 p0, 0x230b

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ghp;-><init>(LX/0Ot;)V

    .line 2384881
    move-object v0, v3

    .line 2384882
    sput-object v0, LX/Ghp;->c:LX/Ghp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384883
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2384884
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2384885
    :cond_1
    sget-object v0, LX/Ghp;->c:LX/Ghp;

    return-object v0

    .line 2384886
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2384887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2384863
    check-cast p2, LX/Gho;

    .line 2384864
    iget-object v0, p0, LX/Ghp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/Gho;->a:LX/0Px;

    const/4 v1, 0x0

    .line 2384865
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    .line 2384866
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2384867
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p2

    invoke-virtual {p2, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    sget-object p2, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, p2}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    const p2, 0x7f0b0050

    invoke-virtual {v1, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2384868
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2384869
    :cond_0
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2384870
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2384861
    invoke-static {}, LX/1dS;->b()V

    .line 2384862
    const/4 v0, 0x0

    return-object v0
.end method
