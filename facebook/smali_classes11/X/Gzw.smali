.class public final LX/Gzw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2412515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2412516
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->valueOf(Ljava/lang/String;)Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2412517
    :catch_0
    move-exception v0

    .line 2412518
    throw v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2412519
    new-array v0, p1, [Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    return-object v0
.end method
