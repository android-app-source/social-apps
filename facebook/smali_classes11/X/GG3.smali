.class public LX/GG3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static k:Z

.field private static volatile l:LX/GG3;


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0lC;

.field public final c:LX/2U3;

.field public final d:LX/GG6;

.field public final e:Landroid/content/Context;

.field public final f:LX/0Uh;

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public i:LX/0oz;

.field public j:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2332598
    const/4 v0, 0x0

    sput-boolean v0, LX/GG3;->k:Z

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0lC;LX/2U3;LX/GG6;Landroid/content/Context;LX/0Uh;LX/0oz;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332589
    iput-object p1, p0, LX/GG3;->a:LX/0Zb;

    .line 2332590
    iput-object p2, p0, LX/GG3;->b:LX/0lC;

    .line 2332591
    iput-object p3, p0, LX/GG3;->c:LX/2U3;

    .line 2332592
    iput-object p4, p0, LX/GG3;->d:LX/GG6;

    .line 2332593
    iput-object p5, p0, LX/GG3;->e:Landroid/content/Context;

    .line 2332594
    iput-object p6, p0, LX/GG3;->f:LX/0Uh;

    .line 2332595
    iput-object p7, p0, LX/GG3;->i:LX/0oz;

    .line 2332596
    iput-object p8, p0, LX/GG3;->j:LX/0ad;

    .line 2332597
    return-void
.end method

.method public static J(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 1

    .prologue
    .line 2332587
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    invoke-virtual {v0}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static K(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2332584
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2332585
    iget-object p0, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v0, p0

    .line 2332586
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static L(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2332569
    invoke-static {p1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2332570
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2332571
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v2

    .line 2332572
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2332573
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2332574
    :goto_0
    return-object v0

    .line 2332575
    :cond_0
    :try_start_0
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2332576
    invoke-static {p1}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2332577
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2332578
    :cond_1
    const/4 v0, 0x0

    .line 2332579
    :goto_1
    move-object v0, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2332580
    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 2332581
    :catch_0
    move-exception v0

    .line 2332582
    iget-object v2, p0, LX/GG3;->c:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "failed getting currency for logging"

    invoke-virtual {v2, v3, v4, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 2332583
    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static M(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I
    .locals 1

    .prologue
    .line 2332568
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v0

    goto :goto_0
.end method

.method public static N(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2332559
    check-cast p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2332560
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332561
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->DAILY_BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    if-ne v0, v1, :cond_0

    .line 2332562
    const-string v0, "daily"

    .line 2332563
    :goto_0
    return-object v0

    .line 2332564
    :cond_0
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332565
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->LIFETIME_BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    if-ne v0, v1, :cond_1

    .line 2332566
    const-string v0, "lifetime"

    goto :goto_0

    .line 2332567
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static O(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I
    .locals 1

    .prologue
    .line 2332556
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2332557
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v0

    .line 2332558
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static P(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I
    .locals 1

    .prologue
    .line 2332553
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2332554
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v0

    .line 2332555
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static Q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2332550
    invoke-static {p0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2332551
    const-string v0, "edit"

    .line 2332552
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "create"

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/GG3;
    .locals 12

    .prologue
    .line 2332465
    sget-object v0, LX/GG3;->l:LX/GG3;

    if-nez v0, :cond_1

    .line 2332466
    const-class v1, LX/GG3;

    monitor-enter v1

    .line 2332467
    :try_start_0
    sget-object v0, LX/GG3;->l:LX/GG3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2332468
    if-eqz v2, :cond_0

    .line 2332469
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2332470
    new-instance v3, LX/GG3;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v6

    check-cast v6, LX/2U3;

    invoke-static {v0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v7

    check-cast v7, LX/GG6;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v10

    check-cast v10, LX/0oz;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, LX/GG3;-><init>(LX/0Zb;LX/0lC;LX/2U3;LX/GG6;Landroid/content/Context;LX/0Uh;LX/0oz;LX/0ad;)V

    .line 2332471
    move-object v0, v3

    .line 2332472
    sput-object v0, LX/GG3;->l:LX/GG3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2332473
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2332474
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2332475
    :cond_1
    sget-object v0, LX/GG3;->l:LX/GG3;

    return-object v0

    .line 2332476
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2332477
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/0oG;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2332528
    if-nez p1, :cond_0

    .line 2332529
    :goto_0
    return-void

    .line 2332530
    :cond_0
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2332531
    check-cast v0, Lcom/facebook/fbservice/service/ServiceException;

    .line 2332532
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2332533
    if-eqz v1, :cond_1

    .line 2332534
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2332535
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v1, v2

    .line 2332536
    if-eqz v1, :cond_1

    .line 2332537
    iget-object v1, v0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2332538
    iget-object v2, v1, Lcom/facebook/fbservice/service/OperationResult;->resultDataBundle:Landroid/os/Bundle;

    move-object v1, v2

    .line 2332539
    const-string v2, "originalExceptionMessage"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2332540
    const-string v2, "error_description"

    invoke-virtual {p0, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332541
    const-string v1, "error_type"

    .line 2332542
    iget-object v2, v0, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v2

    .line 2332543
    invoke-virtual {p0, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    goto :goto_0

    .line 2332544
    :cond_1
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2332545
    check-cast v0, LX/4Ua;

    .line 2332546
    iget-object v1, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v1, :cond_2

    .line 2332547
    const-string v1, "error_code"

    iget-object v2, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget v2, v2, Lcom/facebook/graphql/error/GraphQLError;->code:I

    invoke-virtual {p0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2332548
    const-string v1, "error_description"

    iget-object v0, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget-object v0, v0, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0

    .line 2332549
    :cond_2
    const-string v0, "error_description"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0
.end method

.method public static a(LX/GG3;LX/0oG;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2332599
    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2332600
    instance-of v0, p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    .line 2332601
    const-string v0, "story_graphql_id"

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332602
    :cond_0
    :try_start_0
    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->x()LX/4Cf;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2332603
    const-string v0, "creative_spec"

    iget-object v1, p0, LX/GG3;->b:LX/0lC;

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->x()LX/4Cf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    .line 2332604
    :cond_1
    :goto_0
    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2332605
    const-string v0, "targeting_spec"

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332606
    const-string v0, "audience_id"

    invoke-static {p2}, LX/GG3;->K(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332607
    const-string v0, "saved_audience_id"

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2332608
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v1, v2

    .line 2332609
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332610
    :cond_2
    const-string v0, "ad_status"

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2332611
    const-string v0, "currency"

    invoke-static {p0, p2}, LX/GG3;->L(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332612
    const-string v0, "budget"

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2332613
    const-string v0, "placement"

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332614
    const-string v0, "flow_id"

    iget-object v1, p0, LX/GG3;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332615
    const-string v0, "ad_account_id"

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332616
    const-string v0, "page_id"

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332617
    const-string v0, "duration"

    invoke-static {p2}, LX/GG3;->M(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2332618
    const-string v0, "start_time"

    iget-object v1, p0, LX/GG3;->d:LX/GG6;

    invoke-virtual {v1}, LX/GG6;->a()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2332619
    const-string v0, "end_time"

    iget-object v1, p0, LX/GG3;->d:LX/GG6;

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v2

    invoke-virtual {v1, v2}, LX/GG6;->c(I)J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2332620
    const-string v0, "budget_type"

    invoke-static {p2}, LX/GG3;->N(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332621
    const-string v0, "upper_estimate"

    invoke-static {p2}, LX/GG3;->O(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2332622
    const-string v0, "lower_estimate"

    invoke-static {p2}, LX/GG3;->P(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2332623
    const-string v0, "connection_quality_class"

    iget-object v1, p0, LX/GG3;->i:LX/0oz;

    invoke-virtual {v1}, LX/0oz;->c()LX/0p3;

    move-result-object v1

    invoke-virtual {v1}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332624
    const-string v0, "call_to_action_options2"

    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 2332625
    instance-of v2, p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-nez v2, :cond_4

    .line 2332626
    :cond_3
    :goto_1
    move-object v1, v1

    .line 2332627
    invoke-virtual {p1, v0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2332628
    invoke-static {p1, p3}, LX/GG3;->a(LX/0oG;Ljava/lang/Throwable;)V

    .line 2332629
    return-void

    .line 2332630
    :catch_0
    move-exception v0

    .line 2332631
    iget-object v1, p0, LX/GG3;->c:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "failed processing creative spec for logging"

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2332632
    :cond_4
    check-cast p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2332633
    iget-object v2, p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332634
    if-eqz v2, :cond_3

    .line 2332635
    iget-object v2, p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    move-object v2, v2

    .line 2332636
    if-eqz v2, :cond_3

    .line 2332637
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    move-object v1, v2

    .line 2332638
    goto :goto_1

    .line 2332639
    :cond_5
    iget-object v2, p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332640
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->B()Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    move-result-object v2

    .line 2332641
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_SHARE:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    if-ne v2, v3, :cond_6

    .line 2332642
    iget-object v2, p0, LX/GG3;->j:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/GDK;->n:S

    invoke-interface {v2, v3, v4, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    .line 2332643
    if-eqz v2, :cond_3

    .line 2332644
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2332645
    goto :goto_1

    .line 2332646
    :cond_6
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;->SINGLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLPostAttachmentType;

    if-ne v2, v3, :cond_3

    .line 2332647
    iget-object v2, p0, LX/GG3;->j:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/GDK;->r:S

    invoke-interface {v2, v3, v4, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    .line 2332648
    if-eqz v2, :cond_3

    .line 2332649
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2332650
    goto :goto_1
.end method

.method public static a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 2332463
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    invoke-static/range {v0 .. v8}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2332464
    return-void
.end method

.method public static a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2332478
    :try_start_0
    invoke-static {p1}, LX/GG3;->J(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p8, :cond_1

    .line 2332479
    :cond_0
    :goto_0
    return-void

    .line 2332480
    :cond_1
    iget-object v0, p0, LX/GG3;->a:LX/0Zb;

    const/4 v1, 0x1

    invoke-interface {v0, p2, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2332481
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2332482
    invoke-static {p0, v0, p1, p5}, LX/GG3;->a(LX/GG3;LX/0oG;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/Throwable;)V

    .line 2332483
    if-eqz p3, :cond_2

    .line 2332484
    const-string v1, "flow"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332485
    :cond_2
    if-eqz p6, :cond_3

    .line 2332486
    const-string v1, "flow_entry_point"

    invoke-virtual {v0, v1, p6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332487
    :cond_3
    if-eqz p4, :cond_4

    .line 2332488
    const-string v1, "flow_option"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332489
    :cond_4
    if-eqz p7, :cond_5

    .line 2332490
    const-string v1, "image_source_category"

    invoke-virtual {v0, v1, p7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2332491
    :cond_5
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2332492
    sget-boolean v0, LX/GG3;->k:Z

    if-eqz v0, :cond_0

    .line 2332493
    if-eqz p7, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\nIMAGE_SOURCE_CATEGORY:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2332494
    :goto_1
    if-eqz p6, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "\nFLOW_ENTRY_POINT:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2332495
    :goto_2
    iget-object v2, p0, LX/GG3;->e:Landroid/content/Context;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "EVENT:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nFLOW:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nFLOW_OPTION:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nENTRY_POINT:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2332496
    :catch_0
    move-exception v0

    .line 2332497
    iget-object v1, p0, LX/GG3;->c:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to log event "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332498
    sget-boolean v0, LX/GG3;->k:Z

    if-eqz v0, :cond_0

    .line 2332499
    iget-object v0, p0, LX/GG3;->e:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to log event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nFor flow "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2332500
    :cond_6
    :try_start_1
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2332501
    :cond_7
    const-string v0, ""
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(LX/A8v;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 8

    .prologue
    .line 2332502
    sget-object v0, LX/GG2;->a:[I

    invoke-virtual {p1}, LX/A8v;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2332503
    :goto_0
    return-void

    .line 2332504
    :pswitch_0
    const/4 v6, 0x0

    .line 2332505
    const-string v4, "client_pause_submit"

    const-string v5, "edit"

    move-object v2, p0

    move-object v3, p2

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332506
    goto :goto_0

    .line 2332507
    :pswitch_1
    const/4 v6, 0x0

    .line 2332508
    const-string v4, "client_resume_submit"

    const-string v5, "edit"

    move-object v2, p0

    move-object v3, p2

    move-object v7, v6

    invoke-static/range {v2 .. v7}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332509
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2332510
    const-string v2, "exit_flow"

    const-string v3, "create"

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332511
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2332512
    iget-object v1, p0, LX/GG3;->h:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const-string v2, "exit_flow"

    const-string v3, "walkthrough"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p1

    invoke-static/range {v0 .. v5}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332513
    return-void
.end method

.method public final e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2332514
    const-string v2, "exit_flow"

    const-string v3, "edit"

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332515
    return-void
.end method

.method public final g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2332516
    const-string v2, "enter_flow"

    const-string v3, "payments"

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332517
    return-void
.end method

.method public final o(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 6

    .prologue
    .line 2332518
    const-string v2, "change_flow_option"

    invoke-static {p1}, LX/GG3;->Q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "budget"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332519
    return-void
.end method

.method public final q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 6

    .prologue
    .line 2332520
    const-string v2, "change_flow_option"

    invoke-static {p1}, LX/GG3;->Q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "targeting"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332521
    return-void
.end method

.method public final r(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 6

    .prologue
    .line 2332522
    const-string v2, "change_flow_option"

    invoke-static {p1}, LX/GG3;->Q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "duration"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2332523
    return-void
.end method

.method public final t(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2332524
    const-string v2, "exit_flow"

    const-string v3, "image_picker"

    const-string v6, "creative_edit"

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v7, v4

    invoke-static/range {v0 .. v8}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2332525
    return-void
.end method

.method public final v(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2332526
    const-string v2, "enter_flow"

    const-string v3, "creative_edit"

    const-string v6, "promote_dialog"

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    move-object v7, v4

    invoke-static/range {v0 .. v8}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2332527
    return-void
.end method
