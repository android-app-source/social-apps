.class public final LX/Fdz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/SearchResultsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V
    .locals 0

    .prologue
    .line 2264661
    iput-object p1, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 2

    .prologue
    .line 2264662
    if-nez p2, :cond_0

    .line 2264663
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->J:LX/CvM;

    invoke-virtual {v0}, LX/CvM;->d()V

    .line 2264664
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->T:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 2264665
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u:LX/1k3;

    iget-object v1, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    invoke-virtual {v0, v1}, LX/1Kt;->b(LX/0g8;)V

    .line 2264666
    :goto_0
    return-void

    .line 2264667
    :cond_0
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->T:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2264668
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->u:LX/1k3;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1Kt;->a(LX/0g8;III)V

    .line 2264669
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    const/4 v3, 0x0

    .line 2264670
    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264671
    iget-object p1, v2, LX/Cyn;->m:LX/Cyc;

    move-object v2, p1

    .line 2264672
    iget-object p1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    if-eqz p1, :cond_0

    sget-object p1, LX/Cyc;->LOADED_LAST:LX/Cyc;

    if-eq v2, p1, :cond_0

    if-lez p3, :cond_0

    if-gtz p4, :cond_4

    :cond_0
    move v2, v3

    .line 2264673
    :goto_0
    move v0, v2

    .line 2264674
    if-nez v0, :cond_2

    .line 2264675
    :cond_1
    :goto_1
    return-void

    .line 2264676
    :cond_2
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->size()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    .line 2264677
    :goto_2
    move v0, v0

    .line 2264678
    if-eqz v0, :cond_1

    .line 2264679
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 2264680
    iput-boolean v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ad:Z

    .line 2264681
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    invoke-virtual {v0}, LX/Cyn;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2264682
    iget-object v0, p0, LX/Fdz;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-static {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->D(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    goto :goto_1

    .line 2264683
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 2264684
    :cond_4
    add-int p1, p2, p3

    .line 2264685
    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->D:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CyY;

    invoke-virtual {v2}, LX/CyY;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2264686
    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->D:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CyY;

    invoke-virtual {v2, p1, p3, p4}, LX/CyY;->a(III)Z

    move-result v2

    goto :goto_0

    .line 2264687
    :cond_5
    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->N:LX/1Qq;

    invoke-interface {v2, p1}, LX/1Qr;->h_(I)I

    move-result v2

    .line 2264688
    add-int/lit8 v2, v2, 0xa

    iget-object p1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {p1}, LX/CzA;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    if-lt v2, p1, :cond_6

    const/4 v2, 0x1

    goto :goto_0

    :cond_6
    move v2, v3

    goto :goto_0
.end method
