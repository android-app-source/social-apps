.class public LX/G74;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/63p;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2321001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321002
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)LX/03R;
    .locals 2

    .prologue
    .line 2321003
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    const-string v1, "android.intent.category.HOME"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    if-ne v0, v1, :cond_0

    .line 2321004
    sget-object v0, LX/03R;->YES:LX/03R;

    .line 2321005
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method
