.class public final LX/G3y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2316870
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 2316871
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316872
    :goto_0
    return v1

    .line 2316873
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_4

    .line 2316874
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2316875
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2316876
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 2316877
    const-string v6, "count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2316878
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2316879
    :cond_1
    const-string v6, "nodes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2316880
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2316881
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_2

    .line 2316882
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_2

    .line 2316883
    const/4 v6, 0x0

    .line 2316884
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_c

    .line 2316885
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316886
    :goto_3
    move v5, v6

    .line 2316887
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2316888
    :cond_2
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2316889
    goto :goto_1

    .line 2316890
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2316891
    :cond_4
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2316892
    if-eqz v0, :cond_5

    .line 2316893
    invoke-virtual {p1, v1, v4, v1}, LX/186;->a(III)V

    .line 2316894
    :cond_5
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 2316895
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 2316896
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316897
    :cond_8
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 2316898
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2316899
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2316900
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_8

    if-eqz v9, :cond_8

    .line 2316901
    const-string v10, "description_with_entities"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2316902
    const/4 v9, 0x0

    .line 2316903
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v10, :cond_10

    .line 2316904
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316905
    :goto_5
    move v8, v9

    .line 2316906
    goto :goto_4

    .line 2316907
    :cond_9
    const-string v10, "step_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2316908
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_4

    .line 2316909
    :cond_a
    const-string v10, "title"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2316910
    const/4 v9, 0x0

    .line 2316911
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v10, :cond_14

    .line 2316912
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316913
    :goto_6
    move v5, v9

    .line 2316914
    goto :goto_4

    .line 2316915
    :cond_b
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2316916
    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 2316917
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v7}, LX/186;->b(II)V

    .line 2316918
    const/4 v6, 0x2

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 2316919
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_c
    move v5, v6

    move v7, v6

    move v8, v6

    goto :goto_4

    .line 2316920
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316921
    :cond_e
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_f

    .line 2316922
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2316923
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2316924
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_e

    if-eqz v10, :cond_e

    .line 2316925
    const-string v11, "text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 2316926
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_7

    .line 2316927
    :cond_f
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2316928
    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 2316929
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_5

    :cond_10
    move v8, v9

    goto :goto_7

    .line 2316930
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2316931
    :cond_12
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_13

    .line 2316932
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2316933
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2316934
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_12

    if-eqz v10, :cond_12

    .line 2316935
    const-string v11, "text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 2316936
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_8

    .line 2316937
    :cond_13
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2316938
    invoke-virtual {p1, v9, v5}, LX/186;->b(II)V

    .line 2316939
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_6

    :cond_14
    move v5, v9

    goto :goto_8
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2316940
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2316941
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 2316942
    if-eqz v0, :cond_0

    .line 2316943
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316944
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2316945
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2316946
    if-eqz v0, :cond_2

    .line 2316947
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2316948
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2316949
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_1

    .line 2316950
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2, p3}, LX/G3z;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2316951
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2316952
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2316953
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2316954
    return-void
.end method
