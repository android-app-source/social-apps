.class public LX/GZ8;
.super LX/1OM;
.source ""


# instance fields
.field public final a:LX/7j6;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/0Zb;

.field public final d:LX/03V;

.field public final e:LX/7ix;

.field public final f:Landroid/content/Context;

.field public final g:J

.field public final h:Z

.field private final i:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

.field public j:LX/7iP;

.field public k:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

.field public l:I


# direct methods
.method public constructor <init>(LX/7j6;Lcom/facebook/content/SecureContextHelper;LX/0Zb;LX/03V;LX/7ix;Landroid/content/Context;JZLcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/7iP;)V
    .locals 1
    .param p6    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Lcom/facebook/commerce/core/intent/MerchantInfoViewData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/7iP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2366002
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2366003
    const/4 v0, 0x0

    iput v0, p0, LX/GZ8;->l:I

    .line 2366004
    iput-object p1, p0, LX/GZ8;->a:LX/7j6;

    .line 2366005
    iput-object p2, p0, LX/GZ8;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2366006
    iput-object p3, p0, LX/GZ8;->c:LX/0Zb;

    .line 2366007
    iput-object p4, p0, LX/GZ8;->d:LX/03V;

    .line 2366008
    iput-object p5, p0, LX/GZ8;->e:LX/7ix;

    .line 2366009
    iput-object p6, p0, LX/GZ8;->f:Landroid/content/Context;

    .line 2366010
    iput-wide p7, p0, LX/GZ8;->g:J

    .line 2366011
    iput-boolean p9, p0, LX/GZ8;->h:Z

    .line 2366012
    iput-object p10, p0, LX/GZ8;->i:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2366013
    iput-object p11, p0, LX/GZ8;->j:LX/7iP;

    .line 2366014
    return-void
.end method

.method private e(I)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;
    .locals 3

    .prologue
    .line 2365995
    iget-object v0, p0, LX/GZ8;->i:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-static {v0}, LX/GaI;->a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2365996
    add-int/lit8 p1, p1, -0x1

    .line 2365997
    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, LX/GZ8;->k:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2365998
    iget-object v0, p0, LX/GZ8;->k:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;->a()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    move-result-object v0

    .line 2365999
    :goto_0
    return-object v0

    .line 2366000
    :cond_1
    iget-object v0, p0, LX/GZ8;->d:LX/03V;

    const-string v1, "grid_item_view_holder"

    const-string v2, "Trying to get an invalid product index."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366001
    iget-object v0, p0, LX/GZ8;->k:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel$CollectionProductItemsModel$EdgesModel;->a()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2365994
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2366015
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 2366016
    new-instance v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2366017
    iget-object p2, p0, LX/GZ8;->e:LX/7ix;

    invoke-virtual {p2, v0}, LX/7ix;->a(Lcom/facebook/fbui/widget/contentview/ContentView;)LX/7iw;

    move-result-object v0

    .line 2366018
    iget-object p2, p0, LX/GZ8;->j:LX/7iP;

    .line 2366019
    iput-object p2, v0, LX/7iw;->d:LX/7iP;

    .line 2366020
    new-instance p2, LX/GZ7;

    invoke-direct {p2, v0}, LX/GZ7;-><init>(LX/7iw;)V

    move-object v0, p2

    .line 2366021
    :goto_0
    return-object v0

    .line 2366022
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2366023
    new-instance v1, Lcom/facebook/commerce/storefront/ui/GridProductItemView;

    iget-boolean v2, p0, LX/GZ8;->h:Z

    invoke-direct {v1, v0, v2}, Lcom/facebook/commerce/storefront/ui/GridProductItemView;-><init>(Landroid/content/Context;Z)V

    .line 2366024
    new-instance v2, LX/Ga4;

    iget-object p2, p0, LX/GZ8;->d:LX/03V;

    invoke-direct {v2, v1, v0, p2}, LX/Ga4;-><init>(Lcom/facebook/commerce/storefront/ui/GridProductItemView;Landroid/content/Context;LX/03V;)V

    move-object v0, v2

    .line 2366025
    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2365971
    instance-of v0, p1, LX/Ga4;

    if-eqz v0, :cond_1

    .line 2365972
    check-cast p1, LX/Ga4;

    .line 2365973
    invoke-direct {p0, p2}, LX/GZ8;->e(I)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    move-result-object v0

    .line 2365974
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2365975
    iget-object v1, p1, LX/Ga4;->l:Lcom/facebook/commerce/storefront/ui/GridProductItemView;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductCatalogImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2365976
    iget-object v3, v1, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v4, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2365977
    :goto_0
    iget-object v1, p1, LX/Ga4;->l:Lcom/facebook/commerce/storefront/ui/GridProductItemView;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2365978
    iget-object v3, v1, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2365979
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v1

    .line 2365980
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2365981
    iget-object v1, p1, LX/Ga4;->n:LX/03V;

    const-string v2, "grid_item_view_holder"

    const-string v3, "getHscrollItemPrice: item price is null"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365982
    :goto_1
    invoke-direct {p0, p2}, LX/GZ8;->e(I)Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    move-result-object v0

    .line 2365983
    new-instance v1, LX/GZ6;

    invoke-direct {v1, p0, v0}, LX/GZ6;-><init>(LX/GZ8;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;)V

    move-object v0, v1

    .line 2365984
    iget-object v1, p1, LX/Ga4;->l:Lcom/facebook/commerce/storefront/ui/GridProductItemView;

    invoke-virtual {v1, v0}, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2365985
    :cond_0
    :goto_2
    return-void

    .line 2365986
    :cond_1
    instance-of v0, p1, LX/GZ7;

    if-eqz v0, :cond_0

    .line 2365987
    check-cast p1, LX/GZ7;

    iget-object v0, p0, LX/GZ8;->i:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2365988
    iget-object v1, p1, LX/GZ7;->l:LX/7iw;

    invoke-static {v0, v1}, LX/GaF;->a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/7iw;)V

    .line 2365989
    goto :goto_2

    .line 2365990
    :cond_2
    iget-object v1, p1, LX/Ga4;->n:LX/03V;

    const-string v2, "grid_item_view_holder"

    const-string v3, "getHscrollItemImage: item image is invalid"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2365991
    :cond_3
    iget-object v2, p1, LX/Ga4;->l:Lcom/facebook/commerce/storefront/ui/GridProductItemView;

    .line 2365992
    iget-object v3, v2, Lcom/facebook/commerce/storefront/ui/GridProductItemView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2365993
    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2365968
    if-nez p1, :cond_0

    iget-object v0, p0, LX/GZ8;->i:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-static {v0}, LX/GaI;->a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2365969
    const/4 v0, 0x1

    .line 2365970
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2365967
    iget v1, p0, LX/GZ8;->l:I

    iget-object v0, p0, LX/GZ8;->i:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-static {v0}, LX/GaI;->a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
