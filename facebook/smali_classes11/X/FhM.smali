.class public LX/FhM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cwe;
.implements LX/2Sp;
.implements LX/7HS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Cwe;",
        "LX/2Sp;",
        "LX/7HS",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/8ht;

.field public final b:LX/FhP;

.field public final c:LX/FhN;

.field public final d:LX/FhO;

.field private final e:LX/0TU;

.field private final f:LX/0oz;

.field private final g:LX/0Uh;

.field private final h:LX/0ad;

.field private i:LX/2Sp;


# direct methods
.method public constructor <init>(LX/8ht;LX/0TV;LX/FhP;LX/FhN;LX/FhO;LX/0oz;LX/0Uh;LX/0ad;)V
    .locals 2
    .param p2    # LX/0TV;
        .annotation runtime Lcom/facebook/common/executors/SearchTypeaheadNetworkExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2272784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2272785
    iput-object p7, p0, LX/FhM;->g:LX/0Uh;

    .line 2272786
    iput-object p8, p0, LX/FhM;->h:LX/0ad;

    .line 2272787
    iput-object p1, p0, LX/FhM;->a:LX/8ht;

    .line 2272788
    iput-object p3, p0, LX/FhM;->b:LX/FhP;

    .line 2272789
    iput-object p4, p0, LX/FhM;->c:LX/FhN;

    .line 2272790
    iput-object p5, p0, LX/FhM;->d:LX/FhO;

    .line 2272791
    check-cast p2, LX/0TU;

    iput-object p2, p0, LX/FhM;->e:LX/0TU;

    .line 2272792
    iput-object p6, p0, LX/FhM;->f:LX/0oz;

    .line 2272793
    iget-object v0, p0, LX/FhM;->b:LX/FhP;

    invoke-virtual {v0, p0}, LX/7HT;->a(LX/2Sp;)V

    .line 2272794
    iget-object v0, p0, LX/FhM;->c:LX/FhN;

    invoke-virtual {v0, p0}, LX/7HT;->a(LX/2Sp;)V

    .line 2272795
    iget-object v0, p0, LX/FhM;->d:LX/FhO;

    invoke-virtual {v0, p0}, LX/7HT;->a(LX/2Sp;)V

    .line 2272796
    iget-object v0, p0, LX/FhM;->e:LX/0TU;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/0TU;->a(I)V

    .line 2272797
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "LX/7HY;",
            "LX/7Hn",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2272831
    sget-object v0, LX/7HY;->REMOTE:LX/7HY;

    iget-object v1, p0, LX/FhM;->b:LX/FhP;

    .line 2272832
    iget-object v2, v1, LX/7HT;->c:LX/7Hn;

    move-object v1, v2

    .line 2272833
    sget-object v2, LX/7HY;->REMOTE_ENTITY:LX/7HY;

    iget-object v3, p0, LX/FhM;->c:LX/FhN;

    .line 2272834
    iget-object v4, v3, LX/7HT;->c:LX/7Hn;

    move-object v3, v4

    .line 2272835
    sget-object v4, LX/7HY;->REMOTE_KEYWORD:LX/7HY;

    iget-object v5, p0, LX/FhM;->d:LX/FhO;

    .line 2272836
    iget-object p0, v5, LX/7HT;->c:LX/7Hn;

    move-object v5, p0

    .line 2272837
    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0P1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2272827
    iget-object v0, p0, LX/FhM;->b:LX/FhP;

    invoke-virtual {v0, p1}, LX/7HT;->a(LX/0P1;)V

    .line 2272828
    iget-object v0, p0, LX/FhM;->c:LX/FhN;

    invoke-virtual {v0, p1}, LX/7HT;->a(LX/0P1;)V

    .line 2272829
    iget-object v0, p0, LX/FhM;->d:LX/FhO;

    invoke-virtual {v0, p1}, LX/7HT;->a(LX/0P1;)V

    .line 2272830
    return-void
.end method

.method public final a(LX/2Sp;)V
    .locals 0

    .prologue
    .line 2272825
    iput-object p1, p0, LX/FhM;->i:LX/2Sp;

    .line 2272826
    return-void
.end method

.method public final a(LX/2Sq;)V
    .locals 1

    .prologue
    .line 2272838
    iget-object v0, p0, LX/FhM;->b:LX/FhP;

    invoke-virtual {v0, p1}, LX/7HT;->a(LX/2Sq;)V

    .line 2272839
    iget-object v0, p0, LX/FhM;->c:LX/FhN;

    invoke-virtual {v0, p1}, LX/7HT;->a(LX/2Sq;)V

    .line 2272840
    iget-object v0, p0, LX/FhM;->d:LX/FhO;

    invoke-virtual {v0, p1}, LX/7HT;->a(LX/2Sq;)V

    .line 2272841
    return-void
.end method

.method public final a(LX/7HZ;)V
    .locals 4

    .prologue
    .line 2272811
    iget-object v0, p0, LX/FhM;->b:LX/FhP;

    .line 2272812
    iget-object v1, v0, LX/7HT;->f:LX/7HZ;

    move-object v0, v1

    .line 2272813
    iget-object v1, p0, LX/FhM;->c:LX/FhN;

    .line 2272814
    iget-object v2, v1, LX/7HT;->f:LX/7HZ;

    move-object v1, v2

    .line 2272815
    iget-object v2, p0, LX/FhM;->d:LX/FhO;

    .line 2272816
    iget-object v3, v2, LX/7HT;->f:LX/7HZ;

    move-object v2, v3

    .line 2272817
    sget-object v3, LX/7HZ;->ERROR:LX/7HZ;

    if-eq v0, v3, :cond_0

    sget-object v3, LX/7HZ;->ERROR:LX/7HZ;

    if-eq v1, v3, :cond_0

    sget-object v3, LX/7HZ;->ERROR:LX/7HZ;

    if-ne v2, v3, :cond_2

    .line 2272818
    :cond_0
    sget-object v0, LX/7HZ;->ERROR:LX/7HZ;

    .line 2272819
    :goto_0
    iget-object v1, p0, LX/FhM;->i:LX/2Sp;

    if-eqz v1, :cond_1

    .line 2272820
    iget-object v1, p0, LX/FhM;->i:LX/2Sp;

    invoke-interface {v1, v0}, LX/2Sp;->a(LX/7HZ;)V

    .line 2272821
    :cond_1
    return-void

    .line 2272822
    :cond_2
    sget-object v3, LX/7HZ;->IDLE:LX/7HZ;

    if-ne v0, v3, :cond_3

    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    if-ne v1, v0, :cond_3

    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    if-ne v2, v0, :cond_3

    .line 2272823
    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    goto :goto_0

    .line 2272824
    :cond_3
    sget-object v0, LX/7HZ;->ACTIVE:LX/7HZ;

    goto :goto_0
.end method

.method public final a(LX/Cvp;)V
    .locals 1

    .prologue
    .line 2272807
    iget-object v0, p0, LX/FhM;->b:LX/FhP;

    invoke-virtual {v0, p1}, LX/FhH;->a(LX/Cvp;)V

    .line 2272808
    iget-object v0, p0, LX/FhM;->c:LX/FhN;

    invoke-virtual {v0, p1}, LX/FhH;->a(LX/Cvp;)V

    .line 2272809
    iget-object v0, p0, LX/FhM;->d:LX/FhO;

    invoke-virtual {v0, p1}, LX/FhH;->a(LX/Cvp;)V

    .line 2272810
    return-void
.end method

.method public final b(LX/7B6;)V
    .locals 2

    .prologue
    .line 2272799
    iget-object v0, p0, LX/FhM;->e:LX/0TU;

    invoke-interface {v0}, LX/0TV;->d()V

    move-object v0, p1

    .line 2272800
    check-cast v0, Lcom/facebook/search/api/GraphSearchQuery;

    .line 2272801
    iget-object v1, p0, LX/FhM;->a:LX/8ht;

    invoke-virtual {v1, v0}, LX/8ht;->i(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/FhM;->a:LX/8ht;

    invoke-virtual {v1, v0}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2272802
    if-nez v0, :cond_1

    iget-object v0, p0, LX/FhM;->h:LX/0ad;

    iget-object v1, p0, LX/FhM;->g:LX/0Uh;

    invoke-static {v0, v1}, LX/FhP;->a(LX/0ad;LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2272803
    :cond_1
    iget-object v0, p0, LX/FhM;->b:LX/FhP;

    invoke-virtual {v0, p1}, LX/7HT;->b(LX/7B6;)V

    .line 2272804
    :goto_1
    return-void

    .line 2272805
    :cond_2
    iget-object v0, p0, LX/FhM;->d:LX/FhO;

    invoke-virtual {v0, p1}, LX/7HT;->b(LX/7B6;)V

    .line 2272806
    iget-object v0, p0, LX/FhM;->c:LX/FhN;

    invoke-virtual {v0, p1}, LX/7HT;->b(LX/7B6;)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2272798
    iget-object v0, p0, LX/FhM;->b:LX/FhP;

    invoke-virtual {v0}, LX/7HT;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FhM;->c:LX/FhN;

    invoke-virtual {v0}, LX/7HT;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FhM;->d:LX/FhO;

    invoke-virtual {v0}, LX/7HT;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
