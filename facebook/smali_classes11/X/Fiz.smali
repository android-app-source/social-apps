.class public LX/Fiz;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Fix;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fj1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2276079
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Fiz;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Fj1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276076
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2276077
    iput-object p1, p0, LX/Fiz;->b:LX/0Ot;

    .line 2276078
    return-void
.end method

.method public static a(LX/0QB;)LX/Fiz;
    .locals 4

    .prologue
    .line 2276065
    const-class v1, LX/Fiz;

    monitor-enter v1

    .line 2276066
    :try_start_0
    sget-object v0, LX/Fiz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276067
    sput-object v2, LX/Fiz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276068
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276069
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276070
    new-instance v3, LX/Fiz;

    const/16 p0, 0x34f9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Fiz;-><init>(LX/0Ot;)V

    .line 2276071
    move-object v0, v3

    .line 2276072
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276073
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fiz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276074
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276075
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2276053
    check-cast p2, LX/Fiy;

    .line 2276054
    iget-object v0, p0, LX/Fiz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fj1;

    iget-object v1, p2, LX/Fiy;->a:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    const/4 v6, 0x0

    .line 2276055
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b0060

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0e0977

    invoke-static {p1, v6, v3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v3

    .line 2276056
    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v4

    sget-object v5, LX/CwF;->escape:LX/CwF;

    if-eq v4, v5, :cond_1

    .line 2276057
    iget-object v4, v0, LX/Fj1;->c:LX/8iE;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v0, LX/Fj1;->d:LX/FgS;

    invoke-virtual {v7}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v7

    .line 2276058
    sget-object p0, LX/0Q7;->a:LX/0Px;

    move-object p0, p0

    .line 2276059
    sget-object p2, LX/Fj1;->a:LX/8iB;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-virtual {v4, v5, v7, p0, p2}, LX/8iE;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 2276060
    :goto_0
    move-object v4, v4

    .line 2276061
    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b17d1

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v2, v0, LX/Fj1;->b:LX/0ad;

    sget-short v4, LX/100;->bB:S

    invoke-interface {v2, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b17cf

    invoke-interface {v2, v3}, LX/1Dh;->M(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2276062
    return-object v0

    :cond_0
    invoke-static {p1}, LX/Fii;->c(LX/1De;)LX/Fig;

    move-result-object v2

    goto :goto_1

    .line 2276063
    :cond_1
    new-instance v4, Landroid/text/SpannableString;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0822ee

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2276064
    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    const v7, 0x7f0e0978

    invoke-direct {v5, p1, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v7, 0x0

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result p0

    const/16 p2, 0x11

    invoke-virtual {v4, v5, v7, p0, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2276051
    invoke-static {}, LX/1dS;->b()V

    .line 2276052
    const/4 v0, 0x0

    return-object v0
.end method
