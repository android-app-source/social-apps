.class public LX/GDk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public a:LX/4At;

.field private b:LX/1Ck;

.field private c:LX/0tX;

.field public d:LX/GF4;

.field private e:LX/GNL;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;LX/GNL;LX/GF4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2330290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2330291
    iput-object p1, p0, LX/GDk;->b:LX/1Ck;

    .line 2330292
    iput-object p2, p0, LX/GDk;->c:LX/0tX;

    .line 2330293
    iput-object p4, p0, LX/GDk;->d:LX/GF4;

    .line 2330294
    iput-object p3, p0, LX/GDk;->e:LX/GNL;

    .line 2330295
    return-void
.end method

.method public static a(LX/0QB;)LX/GDk;
    .locals 7

    .prologue
    .line 2330296
    const-class v1, LX/GDk;

    monitor-enter v1

    .line 2330297
    :try_start_0
    sget-object v0, LX/GDk;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2330298
    sput-object v2, LX/GDk;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2330299
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2330300
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2330301
    new-instance p0, LX/GDk;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/GNL;->b(LX/0QB;)LX/GNL;

    move-result-object v5

    check-cast v5, LX/GNL;

    invoke-static {v0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v6

    check-cast v6, LX/GF4;

    invoke-direct {p0, v3, v4, v5, v6}, LX/GDk;-><init>(LX/1Ck;LX/0tX;LX/GNL;LX/GF4;)V

    .line 2330302
    move-object v0, p0

    .line 2330303
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2330304
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GDk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2330305
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2330306
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;LX/GDi;)V
    .locals 4

    .prologue
    .line 2330307
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2330308
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2330309
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v1, v2

    .line 2330310
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2330311
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v1, v2

    .line 2330312
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2330313
    :cond_0
    iget-object v0, p0, LX/GDk;->d:LX/GF4;

    new-instance v1, LX/GFP;

    const v2, 0x7f080b83

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GFP;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2330314
    :goto_0
    return-void

    .line 2330315
    :cond_1
    new-instance v1, LX/4CZ;

    invoke-direct {v1}, LX/4CZ;-><init>()V

    .line 2330316
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v0

    .line 2330317
    const-string v2, "ad_account_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330318
    iget-object v0, p0, LX/GDk;->e:LX/GNL;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/GNL;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)LX/4Ch;

    move-result-object v0

    .line 2330319
    const-string v2, "target_spec"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2330320
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2330321
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v0, v2

    .line 2330322
    const-string v2, "name"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330323
    new-instance v0, LX/4At;

    const v2, 0x7f080b7e

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p2, v2}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, LX/GDk;->a:LX/4At;

    .line 2330324
    iget-object v0, p0, LX/GDk;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2330325
    iget-object v0, p0, LX/GDk;->b:LX/1Ck;

    sget-object v2, LX/GDj;->CREATE_SAVED_AUDIENCE:LX/GDj;

    iget-object v3, p0, LX/GDk;->c:LX/0tX;

    .line 2330326
    new-instance p1, LX/AB6;

    invoke-direct {p1}, LX/AB6;-><init>()V

    move-object p1, p1

    .line 2330327
    const-string p2, "input"

    invoke-virtual {p1, p2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object p1

    check-cast p1, LX/AB6;

    move-object v1, p1

    .line 2330328
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v3, LX/GDh;

    invoke-direct {v3, p0, p3}, LX/GDh;-><init>(LX/GDk;LX/GDi;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
