.class public LX/GNZ;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/GNZ;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2346271
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2346272
    sget-object v0, LX/0ax;->ay:Ljava/lang/String;

    const-string v1, "{extra_ads_experience_id}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2346273
    return-void
.end method

.method public static a(LX/0QB;)LX/GNZ;
    .locals 3

    .prologue
    .line 2346274
    sget-object v0, LX/GNZ;->a:LX/GNZ;

    if-nez v0, :cond_1

    .line 2346275
    const-class v1, LX/GNZ;

    monitor-enter v1

    .line 2346276
    :try_start_0
    sget-object v0, LX/GNZ;->a:LX/GNZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2346277
    if-eqz v2, :cond_0

    .line 2346278
    :try_start_1
    new-instance v0, LX/GNZ;

    invoke-direct {v0}, LX/GNZ;-><init>()V

    .line 2346279
    move-object v0, v0

    .line 2346280
    sput-object v0, LX/GNZ;->a:LX/GNZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2346281
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2346282
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2346283
    :cond_1
    sget-object v0, LX/GNZ;->a:LX/GNZ;

    return-object v0

    .line 2346284
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2346285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
