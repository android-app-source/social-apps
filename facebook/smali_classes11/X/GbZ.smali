.class public final LX/GbZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V
    .locals 0

    .prologue
    .line 2369988
    iput-object p1, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2369989
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;

    move-result-object v1

    .line 2369990
    if-nez v1, :cond_1

    .line 2369991
    :cond_0
    :goto_0
    return-void

    .line 2369992
    :cond_1
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 2369993
    const-string v2, "true"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2369994
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2369995
    invoke-interface {v1}, LX/Gb4;->d()V

    .line 2369996
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->o(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V

    goto :goto_0

    .line 2369997
    :cond_2
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget v0, v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    if-ge v0, v3, :cond_3

    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget-boolean v0, v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->E:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->B:LX/GcK;

    invoke-interface {v0}, LX/GcK;->b()LX/GcT;

    move-result-object v0

    sget-object v2, LX/GcT;->SWITCH_TO_DBL:LX/GcT;

    if-eq v0, v2, :cond_3

    .line 2369998
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->h(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)I

    .line 2369999
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 2370000
    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;

    const v2, 0x7f083490

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/ui/DBLVerifyCurrentPinFragment;->d(I)V

    .line 2370001
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    const v2, 0x7f083490

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LX/Gb4;->a(Ljava/lang/String;)V

    .line 2370002
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget v0, v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->x:I

    if-lt v0, v3, :cond_0

    .line 2370003
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->n(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V

    goto :goto_0

    .line 2370004
    :cond_3
    if-eqz v1, :cond_0

    .line 2370005
    instance-of v0, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2370006
    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    const v2, 0x7f083473

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->d(I)V

    .line 2370007
    :cond_4
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    const v2, 0x7f083473

    invoke-virtual {v0, v2}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, LX/Gb4;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2370008
    iget-object v0, p0, LX/GbZ;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0, p1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Ljava/lang/Throwable;)V

    .line 2370009
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2370010
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/GbZ;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
