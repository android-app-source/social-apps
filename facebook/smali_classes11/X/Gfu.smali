.class public LX/Gfu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/Gfj;

.field public final b:LX/Gg1;

.field public final c:LX/Gff;


# direct methods
.method public constructor <init>(LX/Gfj;LX/Gg1;LX/Gff;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378239
    iput-object p1, p0, LX/Gfu;->a:LX/Gfj;

    .line 2378240
    iput-object p2, p0, LX/Gfu;->b:LX/Gg1;

    .line 2378241
    iput-object p3, p0, LX/Gfu;->c:LX/Gff;

    .line 2378242
    return-void
.end method

.method public static a(LX/0QB;)LX/Gfu;
    .locals 6

    .prologue
    .line 2378227
    const-class v1, LX/Gfu;

    monitor-enter v1

    .line 2378228
    :try_start_0
    sget-object v0, LX/Gfu;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378229
    sput-object v2, LX/Gfu;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378230
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378231
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378232
    new-instance p0, LX/Gfu;

    invoke-static {v0}, LX/Gfj;->a(LX/0QB;)LX/Gfj;

    move-result-object v3

    check-cast v3, LX/Gfj;

    invoke-static {v0}, LX/Gg1;->a(LX/0QB;)LX/Gg1;

    move-result-object v4

    check-cast v4, LX/Gg1;

    invoke-static {v0}, LX/Gff;->a(LX/0QB;)LX/Gff;

    move-result-object v5

    check-cast v5, LX/Gff;

    invoke-direct {p0, v3, v4, v5}, LX/Gfu;-><init>(LX/Gfj;LX/Gg1;LX/Gff;)V

    .line 2378233
    move-object v0, p0

    .line 2378234
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378235
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gfu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378236
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378237
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
