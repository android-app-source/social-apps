.class public final LX/GLf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/GLj;


# direct methods
.method public constructor <init>(LX/GLj;)V
    .locals 0

    .prologue
    .line 2343482
    iput-object p1, p0, LX/GLf;->a:LX/GLj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2343483
    iget-object v0, p0, LX/GLf;->a:LX/GLj;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2343484
    iput-object v1, v0, LX/GLj;->c:Ljava/lang/String;

    .line 2343485
    iget-object v0, p0, LX/GLf;->a:LX/GLj;

    iget-object v0, v0, LX/GLj;->c:Ljava/lang/String;

    iget-object v1, p0, LX/GLf;->a:LX/GLj;

    iget-object v1, v1, LX/GLj;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2343486
    :goto_0
    return-void

    .line 2343487
    :cond_0
    iget-object v0, p0, LX/GLf;->a:LX/GLj;

    iget-object v0, v0, LX/GLj;->c:Ljava/lang/String;

    invoke-static {v0}, LX/GLj;->b(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 2343488
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/GLi;

    .line 2343489
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 2343490
    sget-object v3, LX/GLi;->VALID:LX/GLi;

    if-ne v0, v3, :cond_4

    .line 2343491
    iget-object v3, p0, LX/GLf;->a:LX/GLj;

    iget-boolean v3, v3, LX/GLj;->j:Z

    if-nez v3, :cond_1

    iget-object v3, p0, LX/GLf;->a:LX/GLj;

    iget-object v3, v3, LX/GLj;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v3

    sget-object v4, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-ne v3, v4, :cond_2

    .line 2343492
    :cond_1
    iget-object v3, p0, LX/GLf;->a:LX/GLj;

    iget-object v3, v3, LX/GLj;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2343493
    iget-object v4, v3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v3, v4

    .line 2343494
    iput-object v1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    .line 2343495
    iget-object v3, p0, LX/GLf;->a:LX/GLj;

    iget-object v3, v3, LX/GLj;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2343496
    iget-object v4, v3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v3, v4

    .line 2343497
    iput-object v1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    .line 2343498
    :cond_2
    iget-object v3, p0, LX/GLf;->a:LX/GLj;

    iget-object v3, v3, LX/GLj;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3, v1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h(Ljava/lang/String;)V

    .line 2343499
    iget-object v3, p0, LX/GLf;->a:LX/GLj;

    iget-object v3, v3, LX/GLj;->c:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2343500
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-interface {p1, v2, v3, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 2343501
    :cond_3
    iget-object v3, p0, LX/GLf;->a:LX/GLj;

    .line 2343502
    iget-object v4, v3, LX/GHg;->b:LX/GCE;

    move-object v3, v4

    .line 2343503
    new-instance v4, LX/GFG;

    invoke-direct {v4}, LX/GFG;-><init>()V

    invoke-virtual {v3, v4}, LX/GCE;->a(LX/8wN;)V

    .line 2343504
    :cond_4
    iget-object v3, p0, LX/GLf;->a:LX/GLj;

    .line 2343505
    iput-object v1, v3, LX/GLj;->d:Ljava/lang/String;

    .line 2343506
    iget-object v1, p0, LX/GLf;->a:LX/GLj;

    invoke-static {v1, v0}, LX/GLj;->a$redex0(LX/GLj;LX/GLi;)V

    .line 2343507
    iget-object v1, p0, LX/GLf;->a:LX/GLj;

    .line 2343508
    iget-object v3, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v3

    .line 2343509
    sget-object v3, LX/GG8;->INVALID_URL:LX/GG8;

    sget-object v4, LX/GLi;->VALID:LX/GLi;

    if-ne v0, v4, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v3, v0}, LX/GCE;->a(LX/GG8;Z)V

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2343510
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2343511
    return-void
.end method
