.class public final LX/GLv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/SetAdAccountCurrenyTimeZoneMutationModels$FBPageSetAdAccountCurrenyTimeZoneModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/GJI;


# direct methods
.method public constructor <init>(LX/GJI;Lcom/facebook/adinterfaces/ui/BudgetOptionsView;LX/0Px;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2343818
    iput-object p1, p0, LX/GLv;->d:LX/GJI;

    iput-object p2, p0, LX/GLv;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    iput-object p3, p0, LX/GLv;->b:LX/0Px;

    iput-object p4, p0, LX/GLv;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2343819
    iget-object v0, p0, LX/GLv;->d:LX/GJI;

    .line 2343820
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2343821
    if-nez v0, :cond_0

    .line 2343822
    :goto_0
    return-void

    .line 2343823
    :cond_0
    iget-object v0, p0, LX/GLv;->d:LX/GJI;

    iget-object v0, v0, LX/GJI;->n:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Failed to change currency"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2343824
    iget-object v0, p0, LX/GLv;->d:LX/GJI;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GJI;->b(LX/GJI;Z)V

    .line 2343825
    iget-object v0, p0, LX/GLv;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    iget-object v1, p0, LX/GLv;->b:LX/0Px;

    iget-object v2, p0, LX/GLv;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->setSpinnerSelected(I)V

    .line 2343826
    iget-object v0, p0, LX/GLv;->d:LX/GJI;

    .line 2343827
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343828
    new-instance v1, LX/GFO;

    const/4 v2, 0x0

    iget-object v3, p0, LX/GLv;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080ad7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/GFO;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2343829
    iget-object v0, p0, LX/GLv;->d:LX/GJI;

    .line 2343830
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2343831
    if-nez v0, :cond_0

    .line 2343832
    :goto_0
    return-void

    .line 2343833
    :cond_0
    iget-object v0, p0, LX/GLv;->d:LX/GJI;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GJI;->b(LX/GJI;Z)V

    .line 2343834
    iget-object v0, p0, LX/GLv;->d:LX/GJI;

    .line 2343835
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343836
    new-instance v1, LX/GFT;

    iget-object v2, p0, LX/GLv;->d:LX/GJI;

    iget-object v2, v2, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GFT;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2343837
    iget-object v0, p0, LX/GLv;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v0}, LX/GJD;->f()V

    .line 2343838
    iget-object v0, p0, LX/GLv;->d:LX/GJI;

    invoke-virtual {v0}, LX/GJI;->u()V

    goto :goto_0
.end method
