.class public final LX/FxX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/Integer;

.field public final synthetic b:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

.field public final synthetic c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Ljava/lang/Integer;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V
    .locals 0

    .prologue
    .line 2305054
    iput-object p1, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iput-object p2, p0, LX/FxX;->a:Ljava/lang/Integer;

    iput-object p3, p0, LX/FxX;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x2fca696b

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2305055
    iget-object v0, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-boolean v0, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    if-eqz v0, :cond_0

    .line 2305056
    const v0, 0x1b4c386a

    invoke-static {v2, v2, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2305057
    :goto_0
    return-void

    .line 2305058
    :cond_0
    iget-object v0, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a$redex0(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Z)V

    .line 2305059
    iget-object v0, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v0, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->g:LX/BQ9;

    iget-object v1, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-wide v2, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->F:J

    .line 2305060
    const/4 v11, 0x0

    sget-object v12, LX/9lQ;->SELF:LX/9lQ;

    const-string v13, "fav_photos_replace_in_editing_view_tile_click"

    move-object v8, v0

    move-wide v9, v2

    invoke-static/range {v8 .. v13}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 2305061
    if-eqz v7, :cond_1

    .line 2305062
    iget-object v8, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2305063
    :cond_1
    iget-object v0, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v1, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v2, p0, LX/FxX;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->d(I)I

    move-result v1

    .line 2305064
    iput v1, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    .line 2305065
    iget-object v0, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v0, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->x:LX/FxI;

    iget-object v1, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v3, p0, LX/FxX;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    iget-object v2, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v4, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->B:LX/2yQ;

    iget-object v2, p0, LX/FxX;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v5, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->C:LX/2dD;

    move-object v2, p1

    .line 2305066
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2305067
    new-instance v9, LX/6WS;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2305068
    iput-object v4, v9, LX/0ht;->I:LX/2yQ;

    .line 2305069
    iput-object v5, v9, LX/0ht;->H:LX/2dD;

    .line 2305070
    invoke-virtual {v9}, LX/5OM;->c()LX/5OG;

    move-result-object v10

    .line 2305071
    iget v11, v3, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    move v11, v11

    .line 2305072
    if-nez v11, :cond_2

    .line 2305073
    const v11, 0x7f0815bf

    invoke-virtual {v10, v11}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v11

    .line 2305074
    iget-object v12, v3, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v12, v12

    .line 2305075
    invoke-virtual {v12}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v12

    .line 2305076
    new-instance v4, LX/FxH;

    invoke-direct {v4, v0, v12}, LX/FxH;-><init>(LX/FxI;LX/1U8;)V

    move-object v12, v4

    .line 2305077
    invoke-virtual {v11, v12}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2305078
    :cond_2
    iget-object v11, v0, LX/FxI;->c:LX/0ad;

    sget-short v12, LX/0wf;->D:S

    invoke-interface {v11, v12, v8}, LX/0ad;->a(SZ)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 2305079
    iget v11, v3, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    move v11, v11

    .line 2305080
    if-ne v11, v7, :cond_3

    move v8, v7

    .line 2305081
    :cond_3
    if-eqz v8, :cond_5

    const v7, 0x7f08160a

    :goto_1
    invoke-virtual {v10, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    .line 2305082
    new-instance v11, LX/FxF;

    invoke-direct {v11, v0, v1, v3}, LX/FxF;-><init>(LX/FxI;Landroid/support/v4/app/Fragment;Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;)V

    move-object v11, v11

    .line 2305083
    invoke-virtual {v7, v11}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2305084
    if-eqz v8, :cond_6

    const v7, 0x7f08160b

    :goto_2
    invoke-virtual {v10, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    .line 2305085
    iget-object v8, v0, LX/FxI;->l:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-nez v8, :cond_4

    .line 2305086
    new-instance v8, LX/FxE;

    invoke-direct {v8, v0, v1}, LX/FxE;-><init>(LX/FxI;Landroid/support/v4/app/Fragment;)V

    iput-object v8, v0, LX/FxI;->l:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 2305087
    :cond_4
    iget-object v8, v0, LX/FxI;->l:Landroid/view/MenuItem$OnMenuItemClickListener;

    move-object v8, v8

    .line 2305088
    invoke-virtual {v7, v8}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2305089
    :goto_3
    invoke-virtual {v9, v2}, LX/0ht;->a(Landroid/view/View;)V

    .line 2305090
    const v0, -0x73225e6b

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2305091
    :cond_5
    const v7, 0x7f081607

    goto :goto_1

    .line 2305092
    :cond_6
    const v7, 0x7f081606

    goto :goto_2

    .line 2305093
    :cond_7
    const v7, 0x7f081605

    invoke-virtual {v10, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    invoke-static {v0, v1}, LX/FxI;->a(LX/FxI;Landroid/support/v4/app/Fragment;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2305094
    const v7, 0x7f0815cc

    invoke-virtual {v10, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    invoke-static {v0, v1}, LX/FxI;->c(LX/FxI;Landroid/support/v4/app/Fragment;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3
.end method
