.class public final LX/FjE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/FjF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

.field public final synthetic c:LX/FjF;


# direct methods
.method public constructor <init>(LX/FjF;)V
    .locals 1

    .prologue
    .line 2276498
    iput-object p1, p0, LX/FjE;->c:LX/FjF;

    .line 2276499
    move-object v0, p1

    .line 2276500
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2276501
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2276502
    const-string v0, "SearchNullStateSuggestionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2276503
    if-ne p0, p1, :cond_1

    .line 2276504
    :cond_0
    :goto_0
    return v0

    .line 2276505
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2276506
    goto :goto_0

    .line 2276507
    :cond_3
    check-cast p1, LX/FjE;

    .line 2276508
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2276509
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2276510
    if-eq v2, v3, :cond_0

    .line 2276511
    iget-object v2, p0, LX/FjE;->a:LX/1Pp;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/FjE;->a:LX/1Pp;

    iget-object v3, p1, LX/FjE;->a:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2276512
    goto :goto_0

    .line 2276513
    :cond_5
    iget-object v2, p1, LX/FjE;->a:LX/1Pp;

    if-nez v2, :cond_4

    .line 2276514
    :cond_6
    iget-object v2, p0, LX/FjE;->b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/FjE;->b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    iget-object v3, p1, LX/FjE;->b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    invoke-virtual {v2, v3}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2276515
    goto :goto_0

    .line 2276516
    :cond_7
    iget-object v2, p1, LX/FjE;->b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
