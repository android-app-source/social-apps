.class public final LX/Fme;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2283232
    const-class v1, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;

    const v0, 0x6e88420b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FundraiserCharitySearch"

    const-string v6, "0ebcd2355c4124aeb18f6a46f9a20487"

    const-string v7, "fundraiser_charity_search"

    const-string v8, "10155129249816729"

    const-string v9, "10155259088731729"

    .line 2283233
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2283234
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2283235
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2283236
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2283237
    sparse-switch v0, :sswitch_data_0

    .line 2283238
    :goto_0
    return-object p1

    .line 2283239
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2283240
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2283241
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2283242
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2283243
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5975843b -> :sswitch_0
        -0x57b880fa -> :sswitch_3
        -0xf17c6bb -> :sswitch_2
        0x5ced2b0 -> :sswitch_1
        0x66f18c8 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2283244
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2283245
    :goto_1
    return v0

    .line 2283246
    :pswitch_0
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2283247
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
