.class public final LX/FCi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FCg;


# instance fields
.field public final synthetic a:LX/FCl;


# direct methods
.method public constructor <init>(LX/FCl;)V
    .locals 0

    .prologue
    .line 2210497
    iput-object p1, p0, LX/FCi;->a:LX/FCl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/FCf;)V
    .locals 5

    .prologue
    .line 2210498
    sget-object v0, LX/FCk;->a:[I

    invoke-virtual {p1}, LX/FCf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2210499
    :goto_0
    return-void

    .line 2210500
    :pswitch_0
    iget-object v0, p0, LX/FCi;->a:LX/FCl;

    .line 2210501
    iget-object v1, v0, LX/FCl;->g:LX/FCh;

    iget-object v2, v0, LX/FCl;->e:LX/FCg;

    .line 2210502
    iget-object v3, v1, LX/FCh;->h:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2210503
    const/4 v1, 0x0

    iput-object v1, v0, LX/FCl;->g:LX/FCh;

    .line 2210504
    const/4 v1, 0x0

    .line 2210505
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/FCl;->h:Z

    .line 2210506
    iget-object v2, v0, LX/FCl;->g:LX/FCh;

    if-eqz v2, :cond_0

    .line 2210507
    iget-object v2, v0, LX/FCl;->g:LX/FCh;

    invoke-virtual {v2}, LX/FCh;->b()V

    .line 2210508
    :goto_1
    goto :goto_0

    .line 2210509
    :cond_0
    iget-object v2, v0, LX/FCl;->d:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2210510
    iget-object v2, v0, LX/FCl;->c:LX/FCw;

    const/4 v3, 0x3

    .line 2210511
    iput v3, v2, LX/FCw;->b:I

    .line 2210512
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2210513
    iget-object v4, v0, LX/FCl;->a:Landroid/media/AudioManager;

    iget-object p0, v0, LX/FCl;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 p1, 0x2

    invoke-virtual {v4, p0, v3, p1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v4

    .line 2210514
    if-ne v4, v2, :cond_2

    .line 2210515
    :goto_2
    iget-object v2, v0, LX/FCl;->d:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FCh;

    iput-object v2, v0, LX/FCl;->g:LX/FCh;

    .line 2210516
    iget-object v2, v0, LX/FCl;->g:LX/FCh;

    iget-object v3, v0, LX/FCl;->e:LX/FCg;

    .line 2210517
    iget-object v4, v2, LX/FCh;->h:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2210518
    iget-object v2, v0, LX/FCl;->g:LX/FCh;

    .line 2210519
    new-instance v3, Landroid/media/MediaPlayer;

    invoke-direct {v3}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v3, v2, LX/FCh;->j:Landroid/media/MediaPlayer;

    .line 2210520
    iget-object v3, v2, LX/FCh;->e:LX/FCn;

    iget-object v4, v2, LX/FCh;->j:Landroid/media/MediaPlayer;

    .line 2210521
    iput-object v4, v3, LX/FCn;->c:Landroid/media/MediaPlayer;

    .line 2210522
    const/4 p0, -0x1

    iput p0, v3, LX/FCn;->f:I

    .line 2210523
    iget-object v3, v2, LX/FCh;->j:Landroid/media/MediaPlayer;

    new-instance v4, LX/FCb;

    invoke-direct {v4, v2}, LX/FCb;-><init>(LX/FCh;)V

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 2210524
    iget-object v3, v2, LX/FCh;->j:Landroid/media/MediaPlayer;

    new-instance v4, LX/FCc;

    invoke-direct {v4, v2}, LX/FCc;-><init>(LX/FCh;)V

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 2210525
    iget-object v3, v2, LX/FCh;->c:LX/0TD;

    new-instance v4, LX/FCd;

    invoke-direct {v4, v2, v1}, LX/FCd;-><init>(LX/FCh;Z)V

    invoke-interface {v3, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    iput-object v3, v2, LX/FCh;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2210526
    new-instance v3, LX/FCe;

    invoke-direct {v3, v2}, LX/FCe;-><init>(LX/FCh;)V

    .line 2210527
    iget-object v4, v2, LX/FCh;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object p0, v2, LX/FCh;->d:Ljava/util/concurrent/Executor;

    invoke-static {v4, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2210528
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/FCl;->i:Z

    goto :goto_1

    .line 2210529
    :cond_1
    iget-object v2, v0, LX/FCl;->a:Landroid/media/AudioManager;

    iget-object v3, v0, LX/FCl;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2210530
    goto :goto_1

    :cond_2
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
