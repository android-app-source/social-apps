.class public LX/GmO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GmO;


# instance fields
.field public final a:LX/GmQ;


# direct methods
.method public constructor <init>(LX/GmQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2391962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2391963
    iput-object p1, p0, LX/GmO;->a:LX/GmQ;

    .line 2391964
    return-void
.end method

.method public static a(LX/0QB;)LX/GmO;
    .locals 4

    .prologue
    .line 2391949
    sget-object v0, LX/GmO;->b:LX/GmO;

    if-nez v0, :cond_1

    .line 2391950
    const-class v1, LX/GmO;

    monitor-enter v1

    .line 2391951
    :try_start_0
    sget-object v0, LX/GmO;->b:LX/GmO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2391952
    if-eqz v2, :cond_0

    .line 2391953
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2391954
    new-instance p0, LX/GmO;

    invoke-static {v0}, LX/GmQ;->a(LX/0QB;)LX/GmQ;

    move-result-object v3

    check-cast v3, LX/GmQ;

    invoke-direct {p0, v3}, LX/GmO;-><init>(LX/GmQ;)V

    .line 2391955
    move-object v0, p0

    .line 2391956
    sput-object v0, LX/GmO;->b:LX/GmO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2391957
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2391958
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2391959
    :cond_1
    sget-object v0, LX/GmO;->b:LX/GmO;

    return-object v0

    .line 2391960
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2391961
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
