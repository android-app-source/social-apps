.class public final LX/Fz7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;)V
    .locals 0

    .prologue
    .line 2307518
    iput-object p1, p0, LX/Fz7;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0xd5b5d24

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2307519
    iget-object v1, p0, LX/Fz7;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307520
    iget-boolean p1, v1, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    move v1, p1

    .line 2307521
    if-eqz v1, :cond_0

    .line 2307522
    iget-object v1, p0, LX/Fz7;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    invoke-virtual {v1}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a()V

    .line 2307523
    iget-object v1, p0, LX/Fz7;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    invoke-static {v1}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->e(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;)V

    .line 2307524
    iget-object v1, p0, LX/Fz7;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    invoke-static {v1}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->d(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;)V

    .line 2307525
    const v1, -0x3896580f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2307526
    :goto_0
    return-void

    .line 2307527
    :cond_0
    iget-object v1, p0, LX/Fz7;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->c:LX/FzE;

    .line 2307528
    invoke-virtual {v1}, LX/FzE;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v1, LX/FzE;->l:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->k()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, LX/FzE;->l:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->l()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "megaphone"

    invoke-static/range {v3 .. v8}, Lcom/facebook/timeline/inforeview/typeahead/fragment/ProfileInfoTypeaheadActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2307529
    iget-object v4, v1, LX/FzE;->k:Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x7a4

    iget-object v6, v1, LX/FzE;->j:Landroid/app/Activity;

    invoke-interface {v4, v3, v5, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2307530
    const v1, 0x22714c2f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
