.class public LX/FNQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/6jO;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/FNQ;


# instance fields
.field private final b:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "LX/6jO;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/messaging/sms/SmsThreadManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/3MI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2232588
    new-instance v0, LX/FNP;

    invoke-direct {v0}, LX/FNP;-><init>()V

    sput-object v0, LX/FNQ;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2232589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232590
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/FNQ;->b:LX/0tf;

    .line 2232591
    return-void
.end method

.method public static a(LX/0QB;)LX/FNQ;
    .locals 6

    .prologue
    .line 2232592
    sget-object v0, LX/FNQ;->f:LX/FNQ;

    if-nez v0, :cond_1

    .line 2232593
    const-class v1, LX/FNQ;

    monitor-enter v1

    .line 2232594
    :try_start_0
    sget-object v0, LX/FNQ;->f:LX/FNQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232595
    if-eqz v2, :cond_0

    .line 2232596
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232597
    new-instance p0, LX/FNQ;

    invoke-direct {p0}, LX/FNQ;-><init>()V

    .line 2232598
    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(LX/0QB;)Lcom/facebook/messaging/sms/SmsThreadManager;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-static {v0}, LX/3MI;->a(LX/0QB;)LX/3MI;

    move-result-object v5

    check-cast v5, LX/3MI;

    .line 2232599
    iput-object v3, p0, LX/FNQ;->c:LX/0SG;

    iput-object v4, p0, LX/FNQ;->d:Lcom/facebook/messaging/sms/SmsThreadManager;

    iput-object v5, p0, LX/FNQ;->e:LX/3MI;

    .line 2232600
    move-object v0, p0

    .line 2232601
    sput-object v0, LX/FNQ;->f:LX/FNQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232602
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232603
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232604
    :cond_1
    sget-object v0, LX/FNQ;->f:LX/FNQ;

    return-object v0

    .line 2232605
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232606
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/6jO;D)Z
    .locals 3

    .prologue
    .line 2232607
    iget-wide v0, p0, LX/6jO;->b:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JD)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2232608
    iget-object v0, p0, LX/FNQ;->b:LX/0tf;

    invoke-virtual {v0, p1, p2}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jO;

    .line 2232609
    if-eqz v0, :cond_0

    .line 2232610
    iget-object v5, p0, LX/FNQ;->c:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    iget-wide v7, v0, LX/6jO;->d:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(J)J

    move-result-wide v5

    const-wide/32 v7, 0x5265c00

    cmp-long v5, v5, v7

    if-lez v5, :cond_2

    const/4 v5, 0x1

    :goto_0
    move v2, v5

    .line 2232611
    if-nez v2, :cond_0

    .line 2232612
    invoke-static {v0, p3, p4}, LX/FNQ;->a(LX/6jO;D)Z

    move-result v0

    .line 2232613
    :goto_1
    return v0

    .line 2232614
    :cond_0
    iget-object v0, p0, LX/FNQ;->d:Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v0

    .line 2232615
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    move v0, v1

    .line 2232616
    goto :goto_1

    .line 2232617
    :cond_1
    iget-object v2, p0, LX/FNQ;->e:LX/3MI;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/3MI;->a(Ljava/lang/String;)LX/6jO;

    move-result-object v0

    .line 2232618
    iget-object v1, p0, LX/FNQ;->b:LX/0tf;

    invoke-virtual {v1, p1, p2, v0}, LX/0tf;->b(JLjava/lang/Object;)V

    .line 2232619
    invoke-static {v0, p3, p4}, LX/FNQ;->a(LX/6jO;D)Z

    move-result v0

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method
