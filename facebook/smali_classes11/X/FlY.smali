.class public final enum LX/FlY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FlY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FlY;

.field public static final enum COMPLETE:LX/FlY;

.field public static final enum ERROR:LX/FlY;

.field public static final enum INITIAL:LX/FlY;

.field public static final enum PAGING:LX/FlY;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2280217
    new-instance v0, LX/FlY;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v2}, LX/FlY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FlY;->INITIAL:LX/FlY;

    .line 2280218
    new-instance v0, LX/FlY;

    const-string v1, "PAGING"

    invoke-direct {v0, v1, v3}, LX/FlY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FlY;->PAGING:LX/FlY;

    .line 2280219
    new-instance v0, LX/FlY;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/FlY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FlY;->ERROR:LX/FlY;

    .line 2280220
    new-instance v0, LX/FlY;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v5}, LX/FlY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FlY;->COMPLETE:LX/FlY;

    .line 2280221
    const/4 v0, 0x4

    new-array v0, v0, [LX/FlY;

    sget-object v1, LX/FlY;->INITIAL:LX/FlY;

    aput-object v1, v0, v2

    sget-object v1, LX/FlY;->PAGING:LX/FlY;

    aput-object v1, v0, v3

    sget-object v1, LX/FlY;->ERROR:LX/FlY;

    aput-object v1, v0, v4

    sget-object v1, LX/FlY;->COMPLETE:LX/FlY;

    aput-object v1, v0, v5

    sput-object v0, LX/FlY;->$VALUES:[LX/FlY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2280222
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FlY;
    .locals 1

    .prologue
    .line 2280223
    const-class v0, LX/FlY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FlY;

    return-object v0
.end method

.method public static values()[LX/FlY;
    .locals 1

    .prologue
    .line 2280224
    sget-object v0, LX/FlY;->$VALUES:[LX/FlY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FlY;

    return-object v0
.end method
