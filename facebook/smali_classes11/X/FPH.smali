.class public final enum LX/FPH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FPH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FPH;

.field public static final enum COUNT:LX/FPH;

.field public static final enum HUGE_CELL:LX/FPH;

.field public static final enum PAGINATION_LOADING_CELL:LX/FPH;

.field public static final enum SET_SEARCH_CELL:LX/FPH;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2236895
    new-instance v0, LX/FPH;

    const-string v1, "HUGE_CELL"

    invoke-direct {v0, v1, v2}, LX/FPH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPH;->HUGE_CELL:LX/FPH;

    .line 2236896
    new-instance v0, LX/FPH;

    const-string v1, "SET_SEARCH_CELL"

    invoke-direct {v0, v1, v3}, LX/FPH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPH;->SET_SEARCH_CELL:LX/FPH;

    .line 2236897
    new-instance v0, LX/FPH;

    const-string v1, "PAGINATION_LOADING_CELL"

    invoke-direct {v0, v1, v4}, LX/FPH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPH;->PAGINATION_LOADING_CELL:LX/FPH;

    .line 2236898
    new-instance v0, LX/FPH;

    const-string v1, "COUNT"

    invoke-direct {v0, v1, v5}, LX/FPH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPH;->COUNT:LX/FPH;

    .line 2236899
    const/4 v0, 0x4

    new-array v0, v0, [LX/FPH;

    sget-object v1, LX/FPH;->HUGE_CELL:LX/FPH;

    aput-object v1, v0, v2

    sget-object v1, LX/FPH;->SET_SEARCH_CELL:LX/FPH;

    aput-object v1, v0, v3

    sget-object v1, LX/FPH;->PAGINATION_LOADING_CELL:LX/FPH;

    aput-object v1, v0, v4

    sget-object v1, LX/FPH;->COUNT:LX/FPH;

    aput-object v1, v0, v5

    sput-object v0, LX/FPH;->$VALUES:[LX/FPH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2236902
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FPH;
    .locals 1

    .prologue
    .line 2236901
    const-class v0, LX/FPH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FPH;

    return-object v0
.end method

.method public static values()[LX/FPH;
    .locals 1

    .prologue
    .line 2236900
    sget-object v0, LX/FPH;->$VALUES:[LX/FPH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FPH;

    return-object v0
.end method
