.class public LX/F1J;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Z

.field private c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

.field private d:Lcom/facebook/fbui/widget/text/ImageWithTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2191555
    const v0, 0x7f0314b3

    invoke-direct {p0, p1, v0}, LX/F1J;-><init>(Landroid/content/Context;I)V

    .line 2191556
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 2191547
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 2191548
    const-class v0, LX/F1J;

    invoke-static {v0, p0}, LX/F1J;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2191549
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2191550
    const v0, 0x7f0d2efc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    iput-object v0, p0, LX/F1J;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    .line 2191551
    const v0, 0x7f0d119d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, LX/F1J;->d:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2191552
    iget-object v0, p0, LX/F1J;->a:LX/0wM;

    const v1, 0x7f0209c5

    const v2, -0x6e685d

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2191553
    iget-object v1, p0, LX/F1J;->d:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2191554
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/F1J;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/F1J;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2191567
    iget-boolean v0, p0, LX/F1J;->b:Z

    return v0
.end method

.method public getPhotoView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 2191568
    iget-object v0, p0, LX/F1J;->c:Lcom/facebook/attachments/photos/ui/PhotoAttachmentView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x38b4d888

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2191563
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2191564
    const/4 v1, 0x1

    .line 2191565
    iput-boolean v1, p0, LX/F1J;->b:Z

    .line 2191566
    const/16 v1, 0x2d

    const v2, -0x2eea4f89

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x58c41a9b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2191559
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2191560
    const/4 v1, 0x0

    .line 2191561
    iput-boolean v1, p0, LX/F1J;->b:Z

    .line 2191562
    const/16 v1, 0x2d

    const v2, -0x3574b146    # -4564829.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setShareOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2191557
    iget-object v0, p0, LX/F1J;->d:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2191558
    return-void
.end method
