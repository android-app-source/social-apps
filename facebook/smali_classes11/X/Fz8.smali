.class public final LX/Fz8;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fz9;


# direct methods
.method public constructor <init>(LX/Fz9;)V
    .locals 0

    .prologue
    .line 2307624
    iput-object p1, p0, LX/Fz8;->a:LX/Fz9;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V
    .locals 5

    .prologue
    .line 2307618
    if-nez p1, :cond_0

    .line 2307619
    const/4 v0, 0x0

    .line 2307620
    :goto_0
    move-object v0, v0

    .line 2307621
    iget-object v1, p0, LX/Fz8;->a:LX/Fz9;

    iget-object v1, v1, LX/Fz9;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    .line 2307622
    invoke-static {v1, v0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->a$redex0(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;LX/8QJ;)V

    .line 2307623
    return-void

    :cond_0
    new-instance v0, LX/8QJ;

    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    iget-object v2, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    iget-object v3, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, LX/8QJ;-><init>(LX/0Px;LX/0Px;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2307614
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2307615
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2307616
    iget-object v0, p0, LX/Fz8;->a:LX/Fz9;

    iget-object v0, v0, LX/Fz9;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    iget-object v0, v0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->e:LX/03V;

    const-string v1, "identitygrowth"

    const-string v2, "Privacy options fetch failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2307617
    :cond_0
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2307613
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-direct {p0, p1}, LX/Fz8;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;)V

    return-void
.end method
