.class public final enum LX/Fvt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fvt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fvt;

.field public static final enum EMPTY:LX/Fvt;

.field public static final enum FAVORITE_PHOTOS:LX/Fvt;

.field public static final enum SUGGESTED_PHOTOS:LX/Fvt;

.field private static mValues:[LX/Fvt;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2302147
    new-instance v0, LX/Fvt;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v2}, LX/Fvt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvt;->EMPTY:LX/Fvt;

    .line 2302148
    new-instance v0, LX/Fvt;

    const-string v1, "FAVORITE_PHOTOS"

    invoke-direct {v0, v1, v3}, LX/Fvt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvt;->FAVORITE_PHOTOS:LX/Fvt;

    .line 2302149
    new-instance v0, LX/Fvt;

    const-string v1, "SUGGESTED_PHOTOS"

    invoke-direct {v0, v1, v4}, LX/Fvt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvt;->SUGGESTED_PHOTOS:LX/Fvt;

    .line 2302150
    const/4 v0, 0x3

    new-array v0, v0, [LX/Fvt;

    sget-object v1, LX/Fvt;->EMPTY:LX/Fvt;

    aput-object v1, v0, v2

    sget-object v1, LX/Fvt;->FAVORITE_PHOTOS:LX/Fvt;

    aput-object v1, v0, v3

    sget-object v1, LX/Fvt;->SUGGESTED_PHOTOS:LX/Fvt;

    aput-object v1, v0, v4

    sput-object v0, LX/Fvt;->$VALUES:[LX/Fvt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2302156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static cachedValues()[LX/Fvt;
    .locals 1

    .prologue
    .line 2302153
    sget-object v0, LX/Fvt;->mValues:[LX/Fvt;

    if-nez v0, :cond_0

    .line 2302154
    invoke-static {}, LX/Fvt;->values()[LX/Fvt;

    move-result-object v0

    sput-object v0, LX/Fvt;->mValues:[LX/Fvt;

    .line 2302155
    :cond_0
    sget-object v0, LX/Fvt;->mValues:[LX/Fvt;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fvt;
    .locals 1

    .prologue
    .line 2302152
    const-class v0, LX/Fvt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fvt;

    return-object v0
.end method

.method public static values()[LX/Fvt;
    .locals 1

    .prologue
    .line 2302151
    sget-object v0, LX/Fvt;->$VALUES:[LX/Fvt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fvt;

    return-object v0
.end method
