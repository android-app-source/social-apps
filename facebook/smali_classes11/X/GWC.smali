.class public final LX/GWC;
.super LX/7id;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7id",
        "<",
        "LX/7ib;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;)V
    .locals 0

    .prologue
    .line 2360458
    iput-object p1, p0, LX/GWC;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-direct {p0}, LX/7id;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7ib;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2360459
    const-class v0, LX/7ib;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2360460
    iget-object v0, p0, LX/GWC;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v0, v0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->r:LX/7iT;

    sget-object v1, LX/7iO;->PRODUCT_DETAILS_PAGE:LX/7iO;

    .line 2360461
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2360462
    sget-object v3, LX/7iL;->EVENT:LX/7iL;

    iget-object v3, v3, LX/7iL;->value:Ljava/lang/String;

    sget-object v4, LX/7iQ;->OFFSITE_CHECKOUT_LINK_CLICK:LX/7iQ;

    iget-object v4, v4, LX/7iQ;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2360463
    sget-object v3, LX/7iL;->SECTION_TYPE:LX/7iL;

    iget-object v3, v3, LX/7iL;->value:Ljava/lang/String;

    iget-object v4, v1, LX/7iO;->value:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2360464
    sget-object v3, LX/7iL;->LOGGING_EVENT_TIME:LX/7iL;

    iget-object v3, v3, LX/7iL;->value:Ljava/lang/String;

    iget-object v4, v0, LX/7iT;->s:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2360465
    iget-object v3, v0, LX/7iT;->q:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2360466
    return-void
.end method
