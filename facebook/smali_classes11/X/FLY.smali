.class public final LX/FLY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 2227363
    const/4 v9, 0x0

    .line 2227364
    const-wide/16 v10, 0x0

    .line 2227365
    const/4 v8, 0x0

    .line 2227366
    const/4 v7, 0x0

    .line 2227367
    const/4 v6, 0x0

    .line 2227368
    const/4 v5, 0x0

    .line 2227369
    const/4 v4, 0x0

    .line 2227370
    const/4 v3, 0x0

    .line 2227371
    const/4 v2, 0x0

    .line 2227372
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 2227373
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2227374
    const/4 v2, 0x0

    .line 2227375
    :goto_0
    return v2

    .line 2227376
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v7, :cond_9

    .line 2227377
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2227378
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2227379
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v14, :cond_0

    if-eqz v3, :cond_0

    .line 2227380
    const-string v7, "adjusted_size"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2227381
    invoke-static/range {p0 .. p1}, LX/FLT;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 2227382
    :cond_1
    const-string v7, "creation_time"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2227383
    const/4 v2, 0x1

    .line 2227384
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 2227385
    :cond_2
    const-string v7, "creator"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2227386
    invoke-static/range {p0 .. p1}, LX/FLU;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 2227387
    :cond_3
    const-string v7, "fullsizedImageString"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2227388
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 2227389
    :cond_4
    const-string v7, "imageThumbnail"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2227390
    invoke-static/range {p0 .. p1}, LX/FLV;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto :goto_1

    .line 2227391
    :cond_5
    const-string v7, "legacy_attachment_id"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2227392
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto :goto_1

    .line 2227393
    :cond_6
    const-string v7, "message_object"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2227394
    invoke-static/range {p0 .. p1}, LX/FLW;->a(LX/15w;LX/186;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 2227395
    :cond_7
    const-string v7, "original_dimensions"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2227396
    invoke-static/range {p0 .. p1}, LX/FLX;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 2227397
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2227398
    :cond_9
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2227399
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2227400
    if-eqz v2, :cond_a

    .line 2227401
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2227402
    :cond_a
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2227403
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2227404
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2227405
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2227406
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2227407
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2227408
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v7

    move v13, v8

    move v8, v3

    move v15, v6

    move v6, v9

    move v9, v4

    move/from16 v16, v5

    move-wide v4, v10

    move/from16 v10, v16

    move v11, v15

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    .line 2227409
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2227410
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2227411
    if-eqz v0, :cond_1

    .line 2227412
    const-string v1, "adjusted_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227413
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2227414
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2227415
    if-eqz v1, :cond_0

    .line 2227416
    const-string v4, "uri"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227417
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2227418
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2227419
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2227420
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 2227421
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227422
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2227423
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2227424
    if-eqz v0, :cond_3

    .line 2227425
    const-string v1, "creator"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227426
    invoke-static {p0, v0, p2}, LX/FLU;->a(LX/15i;ILX/0nX;)V

    .line 2227427
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2227428
    if-eqz v0, :cond_4

    .line 2227429
    const-string v1, "fullsizedImageString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227430
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2227431
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2227432
    if-eqz v0, :cond_6

    .line 2227433
    const-string v1, "imageThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227434
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2227435
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2227436
    if-eqz v1, :cond_5

    .line 2227437
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227438
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2227439
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2227440
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2227441
    if-eqz v0, :cond_7

    .line 2227442
    const-string v1, "legacy_attachment_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227443
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2227444
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2227445
    if-eqz v0, :cond_8

    .line 2227446
    const-string v1, "message_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227447
    invoke-static {p0, v0, p2, p3}, LX/FLW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2227448
    :cond_8
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2227449
    if-eqz v0, :cond_b

    .line 2227450
    const-string v1, "original_dimensions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227451
    const-wide/16 v8, 0x0

    .line 2227452
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2227453
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 2227454
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_9

    .line 2227455
    const-string v6, "x"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227456
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 2227457
    :cond_9
    const/4 v4, 0x1

    invoke-virtual {p0, v0, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 2227458
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_a

    .line 2227459
    const-string v6, "y"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2227460
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 2227461
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2227462
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2227463
    return-void
.end method
