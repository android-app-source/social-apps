.class public LX/FYF;
.super LX/FXy;
.source ""


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;LX/0Ot;)V
    .locals 0
    .param p1    # Landroid/support/v4/app/FragmentActivity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255901
    invoke-direct {p0}, LX/FXy;-><init>()V

    .line 2255902
    iput-object p1, p0, LX/FYF;->a:Landroid/support/v4/app/FragmentActivity;

    .line 2255903
    iput-object p2, p0, LX/FYF;->b:LX/0Ot;

    .line 2255904
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2255900
    const v0, 0x7f0209c5

    return v0
.end method

.method public final a(LX/BO1;)LX/FXy;
    .locals 1

    .prologue
    .line 2255896
    invoke-static {p1}, LX/FZ3;->a(LX/BO1;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2255897
    :goto_0
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255898
    return-object p0

    .line 2255899
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2255892
    const v0, 0x7f081acb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2255895
    const-string v0, "share_button"

    return-object v0
.end method

.method public final b(LX/BO1;)Z
    .locals 3

    .prologue
    .line 2255893
    iget-object v0, p0, LX/FYF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FZ3;

    iget-object v1, p0, LX/FYF;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {p1}, LX/FZ3;->a(LX/BO1;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/FZ3;->a(Landroid/support/v4/app/FragmentActivity;Lcom/facebook/graphql/model/GraphQLEntity;)V

    .line 2255894
    const/4 v0, 0x1

    return v0
.end method
