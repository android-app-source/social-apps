.class public final LX/FuC;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/FuC;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/FuA;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/FuD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2299759
    const/4 v0, 0x0

    sput-object v0, LX/FuC;->a:LX/FuC;

    .line 2299760
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/FuC;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2299745
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2299746
    new-instance v0, LX/FuD;

    invoke-direct {v0}, LX/FuD;-><init>()V

    iput-object v0, p0, LX/FuC;->c:LX/FuD;

    .line 2299747
    return-void
.end method

.method public static declared-synchronized q()LX/FuC;
    .locals 2

    .prologue
    .line 2299763
    const-class v1, LX/FuC;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/FuC;->a:LX/FuC;

    if-nez v0, :cond_0

    .line 2299764
    new-instance v0, LX/FuC;

    invoke-direct {v0}, LX/FuC;-><init>()V

    sput-object v0, LX/FuC;->a:LX/FuC;

    .line 2299765
    :cond_0
    sget-object v0, LX/FuC;->a:LX/FuC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2299766
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2299761
    invoke-static {}, LX/1dS;->b()V

    .line 2299762
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x48634141

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2299754
    const/4 v1, 0x0

    iput v1, p5, LX/1no;->a:I

    .line 2299755
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0dc4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p5, LX/1no;->b:I

    .line 2299756
    const/16 v1, 0x1f

    const v2, 0x595d1c54

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2299757
    new-instance v0, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    invoke-direct {v0, p1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2299758
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2299753
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 2299749
    check-cast p3, LX/FuB;

    .line 2299750
    check-cast p2, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    iget-object v0, p3, LX/FuB;->a:Ljava/lang/String;

    .line 2299751
    invoke-virtual {p2, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2299752
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2299748
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2299744
    const/16 v0, 0xf

    return v0
.end method
