.class public LX/GN1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GN0;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GN0",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/GDl;

.field public final b:LX/GDm;

.field public final c:LX/GDg;

.field public d:LX/0ad;


# direct methods
.method public constructor <init>(LX/GDl;LX/GDm;LX/GDg;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345433
    iput-object p1, p0, LX/GN1;->a:LX/GDl;

    .line 2345434
    iput-object p2, p0, LX/GN1;->b:LX/GDm;

    .line 2345435
    iput-object p3, p0, LX/GN1;->c:LX/GDg;

    .line 2345436
    iput-object p4, p0, LX/GN1;->d:LX/0ad;

    .line 2345437
    return-void
.end method

.method public static a(LX/0QB;)LX/GN1;
    .locals 7

    .prologue
    .line 2345421
    const-class v1, LX/GN1;

    monitor-enter v1

    .line 2345422
    :try_start_0
    sget-object v0, LX/GN1;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2345423
    sput-object v2, LX/GN1;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2345424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2345425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2345426
    new-instance p0, LX/GN1;

    invoke-static {v0}, LX/GDl;->a(LX/0QB;)LX/GDl;

    move-result-object v3

    check-cast v3, LX/GDl;

    invoke-static {v0}, LX/GDm;->a(LX/0QB;)LX/GDm;

    move-result-object v4

    check-cast v4, LX/GDm;

    invoke-static {v0}, LX/GDg;->a(LX/0QB;)LX/GDg;

    move-result-object v5

    check-cast v5, LX/GDg;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/GN1;-><init>(LX/GDl;LX/GDm;LX/GDg;LX/0ad;)V

    .line 2345427
    move-object v0, p0

    .line 2345428
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2345429
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GN1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2345430
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2345431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/GN1;Landroid/view/View;LX/31Y;IILcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 2

    .prologue
    .line 2345394
    invoke-virtual {p2, p3}, LX/0ju;->a(I)LX/0ju;

    .line 2345395
    invoke-virtual {p2, p4}, LX/0ju;->b(I)LX/0ju;

    .line 2345396
    const v0, 0x7f080ac6

    new-instance v1, LX/GMp;

    invoke-direct {v1, p0, p5, p1}, LX/GMp;-><init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/view/View;)V

    invoke-virtual {p2, v0, v1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2345397
    const v0, 0x7f080ab4

    new-instance v1, LX/GMq;

    invoke-direct {v1, p0}, LX/GMq;-><init>(LX/GN1;)V

    invoke-virtual {p2, v0, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2345398
    return-void
.end method

.method public static a$redex0(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2345419
    iget-object v0, p0, LX/GN1;->b:LX/GDm;

    sget-object v1, LX/GGB;->PAUSED:LX/GGB;

    invoke-static {p1, v1}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GGB;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/A8v;->PAUSE:LX/A8v;

    invoke-virtual {v0, v1, v2, v3}, LX/GDm;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;LX/A8v;)V

    .line 2345420
    return-void
.end method


# virtual methods
.method public final a(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345417
    check-cast p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345418
    new-instance v0, LX/GMs;

    invoke-direct {v0, p0, p2}, LX/GMs;-><init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/widget/CompoundButton$OnCheckedChangeListener;
    .locals 1

    .prologue
    .line 2345415
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345416
    new-instance v0, LX/GMz;

    invoke-direct {v0, p0, p3}, LX/GMz;-><init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2345414
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p0, p1, p3}, LX/GN1;->a(Landroid/view/View;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 6

    .prologue
    .line 2345409
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2345410
    new-instance v2, LX/31Y;

    invoke-direct {v2, v0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2345411
    const v3, 0x7f080acf

    const v4, 0x7f080ad0

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, LX/GN1;->a(LX/GN1;Landroid/view/View;LX/31Y;IILcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2345412
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2345413
    return-void
.end method

.method public final b(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345407
    check-cast p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345408
    new-instance v0, LX/GMt;

    invoke-direct {v0, p0, p1, p2}, LX/GMt;-><init>(LX/GN1;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final b(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345405
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345406
    new-instance v0, LX/GMw;

    invoke-direct {v0, p0, p3}, LX/GMw;-><init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final c(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345403
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345404
    new-instance v0, LX/GMv;

    invoke-direct {v0, p0, p3}, LX/GMv;-><init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final d(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345401
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345402
    new-instance v0, LX/GMu;

    invoke-direct {v0, p0, p3}, LX/GMu;-><init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method

.method public final e(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2345399
    check-cast p3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345400
    new-instance v0, LX/GMr;

    invoke-direct {v0, p0, p2, p3}, LX/GMr;-><init>(LX/GN1;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-object v0
.end method
