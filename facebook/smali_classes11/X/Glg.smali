.class public final LX/Glg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GkH;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;)V
    .locals 0

    .prologue
    .line 2391171
    iput-object p1, p0, LX/Glg;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2391172
    iget-object v0, p0, LX/Glg;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->m:Ljava/lang/String;

    const-string v2, "Groups Initial Load Failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2391173
    iget-object v0, p0, LX/Glg;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    sget-object v1, LX/Glh;->FAILED:LX/Glh;

    invoke-virtual {v0, v1, v3, v3}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->a(LX/Glh;ZI)V

    .line 2391174
    return-void
.end method

.method public final a(Ljava/util/HashMap;ZI)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;ZI)V"
        }
    .end annotation

    .prologue
    .line 2391175
    if-eqz p2, :cond_0

    .line 2391176
    sget-object v0, LX/Glh;->LOADING:LX/Glh;

    .line 2391177
    :goto_0
    iget-object v1, p0, LX/Glg;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    iget-object v1, v1, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-virtual {v1, p1, p3}, LX/GlX;->a(Ljava/util/HashMap;I)V

    .line 2391178
    iget-object v1, p0, LX/Glg;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    invoke-virtual {v1, v0, p2, p3}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->a(LX/Glh;ZI)V

    .line 2391179
    return-void

    .line 2391180
    :cond_0
    if-nez p3, :cond_1

    sget-object v0, LX/Glh;->EMPTY:LX/Glh;

    goto :goto_0

    :cond_1
    sget-object v0, LX/Glh;->LOADED:LX/Glh;

    goto :goto_0
.end method
