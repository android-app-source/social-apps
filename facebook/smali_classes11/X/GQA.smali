.class public LX/GQA;
.super LX/GQ7;
.source ""


# instance fields
.field private final a:LX/0SG;

.field public final b:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349748
    invoke-direct {p0}, LX/GQ7;-><init>()V

    .line 2349749
    iput-object p1, p0, LX/GQA;->a:LX/0SG;

    .line 2349750
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/GQA;->b:Ljava/util/Calendar;

    .line 2349751
    return-void
.end method

.method private a(Ljava/lang/String;J)Z
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2349756
    const-string v0, "/"

    invoke-static {v0}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v1

    .line 2349757
    const/4 v0, 0x0

    invoke-static {v1, v0}, LX/0Ph;->a(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2349758
    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/0Ph;->a(Ljava/lang/Iterable;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2349759
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2349760
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2349761
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2349762
    add-int/lit8 v4, v0, -0x1

    .line 2349763
    if-ltz v4, :cond_0

    const/16 v5, 0xb

    if-le v4, v5, :cond_1

    .line 2349764
    :cond_0
    :goto_0
    move v0, v2

    .line 2349765
    return v0

    .line 2349766
    :cond_1
    iget-object v5, p0, LX/GQA;->b:Ljava/util/Calendar;

    invoke-virtual {v5, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2349767
    iget-object v5, p0, LX/GQA;->b:Ljava/util/Calendar;

    invoke-virtual {v5, v3}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit16 v5, v5, -0x7d0

    .line 2349768
    iget-object v6, p0, LX/GQA;->b:Ljava/util/Calendar;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 2349769
    if-gt v1, v5, :cond_2

    if-ne v1, v5, :cond_0

    if-lt v4, v6, :cond_0

    :cond_2
    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2349752
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2349753
    :cond_0
    :goto_0
    return v0

    .line 2349754
    :cond_1
    const-string v1, "\\d{2}\\/\\d{2}"

    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2349755
    iget-object v0, p0, LX/GQA;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, LX/GQA;->a(Ljava/lang/String;J)Z

    move-result v0

    goto :goto_0
.end method
