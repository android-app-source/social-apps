.class public LX/Fjr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/Fjr;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0WV;

.field private final d:LX/Fjj;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:LX/0Sh;

.field private final g:LX/13k;

.field private final h:LX/0en;

.field private final i:Landroid/app/DownloadManager;

.field private final j:LX/Fjq;

.field private final k:LX/0SG;

.field public final l:LX/1sf;

.field private final m:LX/0lC;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0WV;LX/0Sh;LX/Fjj;Lcom/facebook/content/SecureContextHelper;LX/13k;LX/0en;Landroid/app/DownloadManager;LX/Fjq;LX/0SG;LX/1sf;LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2277939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2277940
    iput-object p1, p0, LX/Fjr;->a:Landroid/content/Context;

    .line 2277941
    iput-object p2, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2277942
    iput-object p3, p0, LX/Fjr;->c:LX/0WV;

    .line 2277943
    iput-object p4, p0, LX/Fjr;->f:LX/0Sh;

    .line 2277944
    iput-object p5, p0, LX/Fjr;->d:LX/Fjj;

    .line 2277945
    iput-object p6, p0, LX/Fjr;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2277946
    iput-object p7, p0, LX/Fjr;->g:LX/13k;

    .line 2277947
    iput-object p8, p0, LX/Fjr;->h:LX/0en;

    .line 2277948
    iput-object p9, p0, LX/Fjr;->i:Landroid/app/DownloadManager;

    .line 2277949
    iput-object p10, p0, LX/Fjr;->j:LX/Fjq;

    .line 2277950
    iput-object p11, p0, LX/Fjr;->k:LX/0SG;

    .line 2277951
    iput-object p12, p0, LX/Fjr;->l:LX/1sf;

    .line 2277952
    iput-object p13, p0, LX/Fjr;->m:LX/0lC;

    .line 2277953
    return-void
.end method

.method public static a(LX/0QB;)LX/Fjr;
    .locals 3

    .prologue
    .line 2277929
    sget-object v0, LX/Fjr;->n:LX/Fjr;

    if-nez v0, :cond_1

    .line 2277930
    const-class v1, LX/Fjr;

    monitor-enter v1

    .line 2277931
    :try_start_0
    sget-object v0, LX/Fjr;->n:LX/Fjr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2277932
    if-eqz v2, :cond_0

    .line 2277933
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Fjr;->b(LX/0QB;)LX/Fjr;

    move-result-object v0

    sput-object v0, LX/Fjr;->n:LX/Fjr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2277934
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2277935
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2277936
    :cond_1
    sget-object v0, LX/Fjr;->n:LX/Fjr;

    return-object v0

    .line 2277937
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2277938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Fjr;LX/0m9;)Landroid/content/Intent;
    .locals 9

    .prologue
    const/4 v1, -0x1

    const/4 v6, 0x0

    .line 2277899
    :try_start_0
    iget-object v0, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->d:LX/0Tn;

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2277900
    :goto_0
    iget-object v1, p0, LX/Fjr;->c:LX/0WV;

    invoke-virtual {v1}, LX/0WV;->b()I

    move-result v7

    .line 2277901
    iget-object v1, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->h:LX/0Tn;

    invoke-interface {v1, v2, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2277902
    iget-object v2, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Fjg;->f:LX/0Tn;

    invoke-interface {v2, v3, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2277903
    iget-object v3, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/Fjg;->i:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 2277904
    iget-object v4, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/Fjg;->j:LX/0Tn;

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2277905
    iget-object v5, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v8, LX/Fjg;->o:LX/0Tn;

    invoke-interface {v5, v8, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2277906
    invoke-static {v1}, LX/Fjr;->a(Ljava/lang/String;)Z

    move-result v8

    .line 2277907
    if-ge v7, v0, :cond_0

    if-nez v8, :cond_1

    .line 2277908
    :cond_0
    const-string v1, "createintent_currentversion"

    invoke-virtual {p1, v1, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2277909
    const-string v1, "createintent_newversion"

    invoke-virtual {p1, v1, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2277910
    const-string v0, "createintent_isvalidfile"

    invoke-virtual {p1, v0, v8}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    move-object v0, v6

    .line 2277911
    :goto_1
    return-object v0

    .line 2277912
    :catch_0
    :try_start_1
    iget-object v0, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->d:LX/0Tn;

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 2277913
    :goto_2
    iget-object v2, p0, LX/Fjr;->d:LX/Fjj;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SelfUpdateNotifier.createIntent - Stored new_version data type does not match: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/Fjj;->a(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 2277914
    :catch_1
    const-string v0, "error getting stored string"

    goto :goto_2

    :cond_1
    move-object v0, p0

    .line 2277915
    new-instance v6, Landroid/content/Intent;

    iget-object v7, v0, LX/Fjr;->a:Landroid/content/Context;

    const-class v8, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2277916
    const/high16 v7, 0x4000000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2277917
    const-string v7, "local_uri"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2277918
    if-eqz v2, :cond_2

    .line 2277919
    const-string v7, "release_notes"

    invoke-virtual {v6, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2277920
    :cond_2
    const-string v7, "no_cancel"

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2277921
    if-eqz v3, :cond_3

    .line 2277922
    const v7, 0x8000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2277923
    :cond_3
    if-eqz v4, :cond_4

    .line 2277924
    const-string v7, "app_name"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2277925
    :cond_4
    if-eqz v5, :cond_5

    const-string v7, "no_megaphone"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2277926
    const-string v7, "megaphone"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2277927
    :cond_5
    move-object v0, v6

    .line 2277928
    goto :goto_1
.end method

.method private static a(LX/Fjr;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2277894
    if-nez p1, :cond_1

    .line 2277895
    :cond_0
    :goto_0
    return v0

    .line 2277896
    :cond_1
    iget-object v1, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->l:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2277897
    iget-object v2, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Fjg;->m:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 2277898
    if-ne p1, v1, :cond_0

    iget-object v1, p0, LX/Fjr;->k:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v1, v4, v2

    if-gez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2277885
    if-nez p0, :cond_1

    .line 2277886
    :cond_0
    :goto_0
    return v0

    .line 2277887
    :cond_1
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 2277888
    invoke-virtual {v1}, Ljava/net/URI;->isAbsolute()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2277889
    invoke-static {v1}, LX/0en;->a(Ljava/net/URI;)Ljava/io/File;

    move-result-object v1

    .line 2277890
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2277891
    const/4 v0, 0x1

    goto :goto_0

    .line 2277892
    :cond_2
    invoke-static {p0}, LX/0en;->a(Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_1

    .line 2277893
    :catch_0
    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/Fjr;
    .locals 14

    .prologue
    .line 2277801
    new-instance v0, LX/Fjr;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v3

    check-cast v3, LX/0WV;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/Fjj;->b(LX/0QB;)LX/Fjj;

    move-result-object v5

    check-cast v5, LX/Fjj;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/13k;->a(LX/0QB;)LX/13k;

    move-result-object v7

    check-cast v7, LX/13k;

    invoke-static {p0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v8

    check-cast v8, LX/0en;

    invoke-static {p0}, LX/2S1;->b(LX/0QB;)Landroid/app/DownloadManager;

    move-result-object v9

    check-cast v9, Landroid/app/DownloadManager;

    invoke-static {p0}, LX/Jbu;->b(LX/0QB;)LX/Jbu;

    move-result-object v10

    check-cast v10, LX/Fjq;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-static {p0}, LX/1sf;->b(LX/0QB;)LX/1sf;

    move-result-object v12

    check-cast v12, LX/1sf;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v13

    check-cast v13, LX/0lC;

    invoke-direct/range {v0 .. v13}, LX/Fjr;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0WV;LX/0Sh;LX/Fjj;Lcom/facebook/content/SecureContextHelper;LX/13k;LX/0en;Landroid/app/DownloadManager;LX/Fjq;LX/0SG;LX/1sf;LX/0lC;)V

    .line 2277802
    return-object v0
.end method

.method private static declared-synchronized d(LX/Fjr;)V
    .locals 7

    .prologue
    .line 2277863
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fjr;->m:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v0

    .line 2277864
    invoke-static {p0, v0}, LX/Fjr;->a(LX/Fjr;LX/0m9;)Landroid/content/Intent;

    move-result-object v1

    .line 2277865
    if-eqz v1, :cond_2

    .line 2277866
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2277867
    iget-object v0, p0, LX/Fjr;->g:LX/13k;

    .line 2277868
    iget-object v2, v0, LX/13k;->a:Landroid/app/Activity;

    move-object v0, v2

    .line 2277869
    instance-of v2, v0, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    .line 2277870
    if-nez v0, :cond_0

    .line 2277871
    iget-object v0, p0, LX/Fjr;->d:LX/Fjj;

    const-string v2, "selfupdate_skip_showing_activity"

    const-string v3, "currentactivity_null"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277872
    iget-object v0, p0, LX/Fjr;->j:LX/Fjq;

    invoke-interface {v0, v1}, LX/Fjq;->a(Landroid/content/Intent;)V

    .line 2277873
    invoke-static {p0}, LX/Fjr;->e(LX/Fjr;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2277874
    :goto_0
    monitor-exit p0

    return-void

    .line 2277875
    :cond_0
    if-nez v2, :cond_1

    :try_start_1
    iget-object v0, p0, LX/Fjr;->g:LX/13k;

    .line 2277876
    iget-boolean v3, v0, LX/13k;->b:Z

    move v0, v3

    .line 2277877
    if-nez v0, :cond_1

    .line 2277878
    iget-object v0, p0, LX/Fjr;->f:LX/0Sh;

    new-instance v2, Lcom/facebook/selfupdate/SelfUpdateNotifier$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/selfupdate/SelfUpdateNotifier$1;-><init>(LX/Fjr;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, LX/0Sh;->a(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2277879
    goto :goto_0

    .line 2277880
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2277881
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/Fjr;->d:LX/Fjj;

    const-string v1, "selfupdate_skip_showing_activity"

    const-string v3, "currently_showing_activity"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v4, "activity_already_shown"

    iget-object v5, p0, LX/Fjr;->g:LX/13k;

    .line 2277882
    iget-boolean v6, v5, LX/13k;->b:Z

    move v5, v6

    .line 2277883
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v3, v2, v4, v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 2277884
    :cond_2
    const-string v1, "could_not_create_intent_for_activity"

    invoke-virtual {p0, v1, v0}, LX/Fjr;->a(Ljava/lang/String;LX/0m9;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static e(LX/Fjr;)V
    .locals 4

    .prologue
    .line 2277860
    iget-object v0, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Fjg;->r:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2277861
    iget-object v1, p0, LX/Fjr;->d:LX/Fjj;

    const-string v2, "selfupdate_post_notification"

    const-string v3, "source"

    invoke-static {v3, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277862
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 10

    .prologue
    .line 2277850
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Fjg;->n:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2277851
    iget-object v1, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->o:LX/0Tn;

    const-string v3, "no_megaphone"

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2277852
    iget-object v2, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Fjg;->p:LX/0Tn;

    const-string v4, "application/vnd.android.package-archive"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2277853
    iget-object v2, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/Fjg;->d:LX/0Tn;

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v7

    .line 2277854
    invoke-static {p0, v7}, LX/Fjr;->a(LX/Fjr;I)Z

    move-result v2

    .line 2277855
    const/4 v4, 0x2

    if-ne v0, v4, :cond_0

    if-nez v2, :cond_0

    const-string v0, "no_megaphone"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "application/vnd.android.package-archive"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2277856
    invoke-static {p0}, LX/Fjr;->d(LX/Fjr;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2277857
    :goto_0
    monitor-exit p0

    return-void

    .line 2277858
    :cond_0
    :try_start_1
    iget-object v8, p0, LX/Fjr;->d:LX/Fjj;

    const-string v9, "selfupdate_skip_showing_activity"

    const-string v0, "megaphone_string"

    const-string v2, "mime_type"

    const-string v4, "downloaded_version"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "update_postponed"

    invoke-static {p0, v7}, LX/Fjr;->a(LX/Fjr;I)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    invoke-virtual {v8, v9, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2277859
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/0m9;)V
    .locals 10
    .param p2    # LX/0m9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, 0x0

    const/4 v4, -0x1

    .line 2277816
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2277817
    const-string v1, "caller_reason"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277818
    if-eqz p2, :cond_0

    .line 2277819
    const-string v1, "caller_data"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277820
    :cond_0
    const-string v1, "update_build"

    iget-object v2, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Fjg;->d:LX/0Tn;

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277821
    iget-object v1, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Fjg;->n:LX/0Tn;

    invoke-interface {v1, v2, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    .line 2277822
    const-string v2, "download_status"

    .line 2277823
    packed-switch v1, :pswitch_data_0

    .line 2277824
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UNKNOWN("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    move-object v1, v3

    .line 2277825
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277826
    const-string v1, "source"

    iget-object v2, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Fjg;->r:LX/0Tn;

    invoke-interface {v2, v3, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277827
    iget-object v1, p0, LX/Fjr;->d:LX/Fjj;

    const-string v2, "selfupdate_clean_update_info"

    invoke-virtual {v1, v2, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277828
    iget-object v0, p0, LX/Fjr;->j:LX/Fjq;

    invoke-interface {v0}, LX/Fjq;->a()V

    .line 2277829
    iget-object v0, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Fjg;->g:LX/0Tn;

    invoke-interface {v0, v1, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    .line 2277830
    cmp-long v0, v2, v8

    if-eqz v0, :cond_1

    .line 2277831
    :try_start_0
    iget-object v0, p0, LX/Fjr;->i:Landroid/app/DownloadManager;

    const/4 v1, 0x1

    new-array v1, v1, [J

    const/4 v4, 0x0

    aput-wide v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2277832
    :cond_1
    :goto_1
    iget-object v0, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Fjg;->h:LX/0Tn;

    invoke-interface {v0, v1, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2277833
    if-eqz v0, :cond_2

    .line 2277834
    :try_start_1
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2277835
    invoke-virtual {v2}, Ljava/net/URI;->isAbsolute()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "file"

    invoke-virtual {v2}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2277836
    :try_start_2
    invoke-static {v2}, LX/0en;->a(Ljava/net/URI;)Ljava/io/File;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    .line 2277837
    :goto_2
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2277838
    :cond_2
    :goto_3
    iget-object v0, p0, LX/Fjr;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const/16 v1, 0x24

    const-string v2, "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$"

    const-wide/32 v4, 0x500000

    invoke-static {v0, v1, v2, v4, v5}, LX/0en;->a(Ljava/io/File;ILjava/lang/String;J)Z

    .line 2277839
    iget-object v0, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->d:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->e:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->f:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->n:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->i:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->g:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->h:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->j:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->o:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->p:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2277840
    return-void

    .line 2277841
    :catch_0
    move-exception v0

    .line 2277842
    iget-object v1, p0, LX/Fjr;->d:LX/Fjj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to remove download ID from DownloadManager: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 2277843
    :pswitch_0
    const-string v3, "DOWNLOAD_STATUS_NOT_STARTED"

    goto/16 :goto_0

    .line 2277844
    :pswitch_1
    const-string v3, "DOWNLOAD_STATUS_STARTED"

    goto/16 :goto_0

    .line 2277845
    :pswitch_2
    const-string v3, "DOWNLOAD_STATUS_COMPLETED"

    goto/16 :goto_0

    .line 2277846
    :catch_1
    const/4 v2, 0x0

    .line 2277847
    goto/16 :goto_2

    .line 2277848
    :cond_3
    invoke-static {v0}, LX/0en;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    goto/16 :goto_2

    .line 2277849
    :catch_2
    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 2277808
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fjr;->m:LX/0lC;

    invoke-virtual {v0}, LX/0lC;->e()LX/0m9;

    move-result-object v0

    .line 2277809
    invoke-static {p0, v0}, LX/Fjr;->a(LX/Fjr;LX/0m9;)Landroid/content/Intent;

    move-result-object v1

    .line 2277810
    if-eqz v1, :cond_0

    .line 2277811
    iget-object v0, p0, LX/Fjr;->j:LX/Fjq;

    invoke-interface {v0, v1}, LX/Fjq;->a(Landroid/content/Intent;)V

    .line 2277812
    invoke-static {p0}, LX/Fjr;->e(LX/Fjr;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2277813
    :goto_0
    monitor-exit p0

    return-void

    .line 2277814
    :cond_0
    :try_start_1
    const-string v1, "could_not_create_intent_for_notification"

    invoke-virtual {p0, v1, v0}, LX/Fjr;->a(Ljava/lang/String;LX/0m9;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2277815
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 2277803
    iget-object v0, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Fjg;->d:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2277804
    if-eqz v0, :cond_0

    .line 2277805
    iget-object v1, p0, LX/Fjr;->k:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    .line 2277806
    iget-object v1, p0, LX/Fjr;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v4, LX/Fjg;->l:LX/0Tn;

    invoke-interface {v1, v4, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Fjg;->m:LX/0Tn;

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2277807
    :cond_0
    return-void
.end method
