.class public final enum LX/FP2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FP2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FP2;

.field public static final enum CURRENT_LOCATION:LX/FP2;

.field public static final enum INTERSECT:LX/FP2;

.field public static final enum INVALID:LX/FP2;

.field public static final enum KEYWORDS_PLACES:LX/FP2;

.field public static final enum PLACES_IN:LX/FP2;

.field public static final enum SPECIFIED_LOCATION:LX/FP2;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2236541
    new-instance v0, LX/FP2;

    const-string v1, "CURRENT_LOCATION"

    invoke-direct {v0, v1, v3}, LX/FP2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP2;->CURRENT_LOCATION:LX/FP2;

    .line 2236542
    new-instance v0, LX/FP2;

    const-string v1, "SPECIFIED_LOCATION"

    invoke-direct {v0, v1, v4}, LX/FP2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP2;->SPECIFIED_LOCATION:LX/FP2;

    .line 2236543
    new-instance v0, LX/FP2;

    const-string v1, "KEYWORDS_PLACES"

    invoke-direct {v0, v1, v5}, LX/FP2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP2;->KEYWORDS_PLACES:LX/FP2;

    .line 2236544
    new-instance v0, LX/FP2;

    const-string v1, "PLACES_IN"

    invoke-direct {v0, v1, v6}, LX/FP2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP2;->PLACES_IN:LX/FP2;

    .line 2236545
    new-instance v0, LX/FP2;

    const-string v1, "INTERSECT"

    invoke-direct {v0, v1, v7}, LX/FP2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP2;->INTERSECT:LX/FP2;

    .line 2236546
    new-instance v0, LX/FP2;

    const-string v1, "INVALID"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FP2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FP2;->INVALID:LX/FP2;

    .line 2236547
    const/4 v0, 0x6

    new-array v0, v0, [LX/FP2;

    sget-object v1, LX/FP2;->CURRENT_LOCATION:LX/FP2;

    aput-object v1, v0, v3

    sget-object v1, LX/FP2;->SPECIFIED_LOCATION:LX/FP2;

    aput-object v1, v0, v4

    sget-object v1, LX/FP2;->KEYWORDS_PLACES:LX/FP2;

    aput-object v1, v0, v5

    sget-object v1, LX/FP2;->PLACES_IN:LX/FP2;

    aput-object v1, v0, v6

    sget-object v1, LX/FP2;->INTERSECT:LX/FP2;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FP2;->INVALID:LX/FP2;

    aput-object v2, v0, v1

    sput-object v0, LX/FP2;->$VALUES:[LX/FP2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2236540
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FP2;
    .locals 1

    .prologue
    .line 2236538
    const-class v0, LX/FP2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FP2;

    return-object v0
.end method

.method public static values()[LX/FP2;
    .locals 1

    .prologue
    .line 2236539
    sget-object v0, LX/FP2;->$VALUES:[LX/FP2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FP2;

    return-object v0
.end method
