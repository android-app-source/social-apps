.class public final LX/GnC;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/GnF;

.field public final synthetic val$actionType:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

.field public final synthetic val$extras:Ljava/util/Map;

.field public final synthetic val$loggingParams:LX/GoE;

.field public final synthetic val$url:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/GnF;LX/GoE;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 2393481
    iput-object p1, p0, LX/GnC;->this$0:LX/GnF;

    iput-object p2, p0, LX/GnC;->val$loggingParams:LX/GoE;

    iput-object p3, p0, LX/GnC;->val$url:Ljava/lang/String;

    iput-object p4, p0, LX/GnC;->val$actionType:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    iput-object p5, p0, LX/GnC;->val$extras:Ljava/util/Map;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 2393482
    iget-object v0, p0, LX/GnC;->val$loggingParams:LX/GoE;

    if-eqz v0, :cond_0

    .line 2393483
    const-string v0, "logging_token"

    iget-object v1, p0, LX/GnC;->val$loggingParams:LX/GoE;

    .line 2393484
    iget-object p1, v1, LX/GoE;->b:Ljava/lang/String;

    move-object v1, p1

    .line 2393485
    invoke-virtual {p0, v0, v1}, LX/GnC;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393486
    const-string v0, "element_type"

    iget-object v1, p0, LX/GnC;->val$loggingParams:LX/GoE;

    .line 2393487
    iget-object p1, v1, LX/GoE;->a:Ljava/lang/String;

    move-object v1, p1

    .line 2393488
    invoke-virtual {p0, v0, v1}, LX/GnC;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393489
    :cond_0
    const-string v0, "action_url"

    iget-object v1, p0, LX/GnC;->val$url:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/GnC;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393490
    const-string v0, "action_type"

    iget-object v1, p0, LX/GnC;->val$actionType:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    invoke-virtual {p0, v0, v1}, LX/GnC;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393491
    iget-object v0, p0, LX/GnC;->val$extras:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 2393492
    iget-object v0, p0, LX/GnC;->val$extras:Ljava/util/Map;

    invoke-virtual {p0, v0}, LX/GnC;->putAll(Ljava/util/Map;)V

    .line 2393493
    :cond_1
    return-void
.end method
