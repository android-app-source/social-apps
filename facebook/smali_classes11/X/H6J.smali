.class public final LX/H6J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 0

    .prologue
    .line 2426078
    iput-object p1, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x44037550

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2426079
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-boolean v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    if-eqz v0, :cond_1

    .line 2426080
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->w()LX/H72;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2426081
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v1

    .line 2426082
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a:LX/H60;

    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2426083
    new-instance v3, LX/4HV;

    invoke-direct {v3}, LX/4HV;-><init>()V

    .line 2426084
    const-string v4, "offer_view_id"

    invoke-virtual {v3, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426085
    const-string v4, "actor_id"

    invoke-virtual {v3, v4, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426086
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2426087
    const-string v5, "client_mutation_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426088
    new-instance v4, LX/H6m;

    invoke-direct {v4}, LX/H6m;-><init>()V

    move-object v4, v4

    .line 2426089
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2426090
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2426091
    iget-object v4, v2, LX/H60;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2426092
    new-instance v1, LX/H6I;

    invoke-direct {v1, p0}, LX/H6I;-><init>(LX/H6J;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2426093
    :cond_0
    :goto_0
    const v0, -0x664a0e65

    invoke-static {v0, v8}, LX/02F;->a(II)V

    return-void

    .line 2426094
    :cond_1
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->w()LX/H72;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2426095
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-static {v0}, LX/H6T;->b(LX/H83;)Ljava/lang/String;

    move-result-object v2

    .line 2426096
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2426097
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ax:Ljava/lang/String;

    .line 2426098
    :cond_2
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->w()LX/H72;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v1

    .line 2426099
    :goto_1
    iget-object v0, p0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    move-object v4, v3

    move-object v5, v3

    move v7, v6

    .line 2426100
    invoke-static/range {v0 .. v7}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 2426101
    goto :goto_0

    :cond_3
    move-object v1, v3

    .line 2426102
    goto :goto_1
.end method
