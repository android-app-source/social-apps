.class public LX/FHy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2221256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221257
    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 1

    .prologue
    .line 2221258
    sget-object v0, LX/FHx;->PHASE_ONE:LX/FHx;

    invoke-static {p0, v0}, LX/FHy;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/FHx;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;LX/FHx;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 3

    .prologue
    .line 2221259
    new-instance v1, LX/5zn;

    invoke-direct {v1}, LX/5zn;-><init>()V

    .line 2221260
    invoke-virtual {v1, p0}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    .line 2221261
    iput-object p0, v1, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2221262
    const/4 v0, 0x1

    .line 2221263
    iput-boolean v0, v1, LX/5zn;->x:Z

    .line 2221264
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    .line 2221265
    :goto_0
    add-int/lit16 v0, v0, 0x1388

    .line 2221266
    sget-object v2, LX/FHx;->PHASE_ONE:LX/FHx;

    if-ne p1, v2, :cond_2

    .line 2221267
    iput v0, v1, LX/5zn;->v:I

    .line 2221268
    :cond_0
    :goto_1
    invoke-virtual {v1}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    return-object v0

    .line 2221269
    :cond_1
    iget v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    goto :goto_0

    .line 2221270
    :cond_2
    sget-object v2, LX/FHx;->PHASE_TWO:LX/FHx;

    if-ne p1, v2, :cond_0

    .line 2221271
    iput v0, v1, LX/5zn;->u:I

    .line 2221272
    goto :goto_1
.end method
