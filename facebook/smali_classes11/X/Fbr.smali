.class public LX/Fbr;
.super LX/FbH;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2261220
    invoke-direct {p0}, LX/FbH;-><init>()V

    .line 2261221
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbr;
    .locals 3

    .prologue
    .line 2261222
    const-class v1, LX/Fbr;

    monitor-enter v1

    .line 2261223
    :try_start_0
    sget-object v0, LX/Fbr;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261224
    sput-object v2, LX/Fbr;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261225
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261226
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2261227
    new-instance v0, LX/Fbr;

    invoke-direct {v0}, LX/Fbr;-><init>()V

    .line 2261228
    move-object v0, v0

    .line 2261229
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261230
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fbr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261231
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261232
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/8dO;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261233
    new-instance v0, LX/8dO;

    invoke-direct {v0}, LX/8dO;-><init>()V

    new-instance v1, LX/8dQ;

    invoke-direct {v1}, LX/8dQ;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    .line 2261234
    iput-object v2, v1, LX/8dQ;->Z:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2261235
    move-object v1, v1

    .line 2261236
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->q()Ljava/lang/String;

    move-result-object v2

    .line 2261237
    iput-object v2, v1, LX/8dQ;->ab:Ljava/lang/String;

    .line 2261238
    move-object v1, v1

    .line 2261239
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->b()I

    move-result v2

    .line 2261240
    iput v2, v1, LX/8dQ;->aa:I

    .line 2261241
    move-object v1, v1

    .line 2261242
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v2

    .line 2261243
    iput-object v2, v1, LX/8dQ;->q:LX/0Px;

    .line 2261244
    move-object v1, v1

    .line 2261245
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, -0x59a2740a

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2261246
    iput-object v2, v1, LX/8dQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2261247
    move-object v1, v1

    .line 2261248
    invoke-virtual {v1}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    .line 2261249
    iput-object v1, v0, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2261250
    move-object v0, v0

    .line 2261251
    return-object v0
.end method
