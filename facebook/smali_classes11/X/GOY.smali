.class public final LX/GOY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V
    .locals 0

    .prologue
    .line 2347660
    iput-object p1, p0, LX/GOY;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;
    .locals 3

    .prologue
    .line 2347655
    iget-object v0, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v0, v0

    .line 2347656
    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    .line 2347657
    sget-object v1, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->p:LX/0Rl;

    invoke-virtual {v0, v1}, LX/0wv;->b(LX/0Rl;)Z

    move-result v1

    .line 2347658
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/GOY;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    sget-short v2, LX/GOz;->a:S

    invoke-virtual {v1, v2}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(S)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->p:LX/0Rl;

    invoke-static {v1}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wv;->a(LX/0Rl;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->a(LX/0Px;)Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2347659
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-direct {p0, p1}, LX/GOY;->a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    move-result-object v0

    return-object v0
.end method
