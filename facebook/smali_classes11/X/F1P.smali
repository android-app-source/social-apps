.class public final enum LX/F1P;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F1P;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F1P;

.field public static final enum AFTER_INITIAL_ANIMATION:LX/F1P;

.field public static final enum BEFORE_INITIAL_ANIMATION:LX/F1P;

.field public static final enum DURING_INITIAL_ANIMATION:LX/F1P;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2191889
    new-instance v0, LX/F1P;

    const-string v1, "BEFORE_INITIAL_ANIMATION"

    invoke-direct {v0, v1, v2}, LX/F1P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F1P;->BEFORE_INITIAL_ANIMATION:LX/F1P;

    .line 2191890
    new-instance v0, LX/F1P;

    const-string v1, "DURING_INITIAL_ANIMATION"

    invoke-direct {v0, v1, v3}, LX/F1P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F1P;->DURING_INITIAL_ANIMATION:LX/F1P;

    .line 2191891
    new-instance v0, LX/F1P;

    const-string v1, "AFTER_INITIAL_ANIMATION"

    invoke-direct {v0, v1, v4}, LX/F1P;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F1P;->AFTER_INITIAL_ANIMATION:LX/F1P;

    .line 2191892
    const/4 v0, 0x3

    new-array v0, v0, [LX/F1P;

    sget-object v1, LX/F1P;->BEFORE_INITIAL_ANIMATION:LX/F1P;

    aput-object v1, v0, v2

    sget-object v1, LX/F1P;->DURING_INITIAL_ANIMATION:LX/F1P;

    aput-object v1, v0, v3

    sget-object v1, LX/F1P;->AFTER_INITIAL_ANIMATION:LX/F1P;

    aput-object v1, v0, v4

    sput-object v0, LX/F1P;->$VALUES:[LX/F1P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2191886
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F1P;
    .locals 1

    .prologue
    .line 2191888
    const-class v0, LX/F1P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F1P;

    return-object v0
.end method

.method public static values()[LX/F1P;
    .locals 1

    .prologue
    .line 2191887
    sget-object v0, LX/F1P;->$VALUES:[LX/F1P;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F1P;

    return-object v0
.end method
