.class public LX/FGm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FGm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2219537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219538
    return-void
.end method

.method public static a(LX/0QB;)LX/FGm;
    .locals 3

    .prologue
    .line 2219525
    sget-object v0, LX/FGm;->a:LX/FGm;

    if-nez v0, :cond_1

    .line 2219526
    const-class v1, LX/FGm;

    monitor-enter v1

    .line 2219527
    :try_start_0
    sget-object v0, LX/FGm;->a:LX/FGm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2219528
    if-eqz v2, :cond_0

    .line 2219529
    :try_start_1
    new-instance v0, LX/FGm;

    invoke-direct {v0}, LX/FGm;-><init>()V

    .line 2219530
    move-object v0, v0

    .line 2219531
    sput-object v0, LX/FGm;->a:LX/FGm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2219532
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2219533
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2219534
    :cond_1
    sget-object v0, LX/FGm;->a:LX/FGm;

    return-object v0

    .line 2219535
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2219536
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(LX/FHZ;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2219522
    iget-object v0, p0, LX/FHZ;->b:LX/FHY;

    sget-object v3, LX/FHY;->FAILED:LX/FHY;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Expected FAILED, got: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LX/FHZ;->b:LX/FHY;

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2219523
    iget-object v0, p0, LX/FHZ;->a:LX/FGc;

    iget-object v0, v0, LX/FGc;->f:Ljava/lang/Throwable;

    invoke-static {v0}, LX/FGm;->a(Ljava/lang/Throwable;)Z

    move-result v0

    return v0

    :cond_0
    move v0, v2

    .line 2219524
    goto :goto_0
.end method

.method public static a(Ljava/lang/Throwable;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2219510
    if-nez p0, :cond_0

    move v0, v1

    .line 2219511
    :goto_0
    return v0

    .line 2219512
    :cond_0
    invoke-static {p0}, LX/1Bz;->getCausalChain(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 2219513
    instance-of v4, v0, LX/FHh;

    if-nez v4, :cond_2

    instance-of v4, v0, Ljava/lang/RuntimeException;

    if-nez v4, :cond_2

    instance-of v4, v0, Ljava/io/FileNotFoundException;

    if-eqz v4, :cond_3

    :cond_2
    move v0, v1

    .line 2219514
    goto :goto_0

    .line 2219515
    :cond_3
    instance-of v4, v0, LX/2Oo;

    if-eqz v4, :cond_1

    .line 2219516
    check-cast v0, LX/2Oo;

    .line 2219517
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 2219518
    if-eqz v0, :cond_4

    .line 2219519
    iget-boolean v3, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mIsTransient:Z

    move v0, v3

    .line 2219520
    if-eqz v0, :cond_5

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    .line 2219521
    goto :goto_0
.end method
