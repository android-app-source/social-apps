.class public final LX/Gyj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/location/ui/LocationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 0

    .prologue
    .line 2410280
    iput-object p1, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2410281
    sget-object v0, Lcom/facebook/location/ui/LocationSettingsFragment;->a:Ljava/lang/Class;

    const-string v1, "Failed to load location history setting"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2410282
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-boolean v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->N:Z

    if-nez v0, :cond_0

    .line 2410283
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    sget-object v1, LX/Gyp;->FETCH_DATA:LX/Gyp;

    invoke-virtual {v0, v1}, LX/Gyq;->c(LX/Gyp;)V

    .line 2410284
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    sget-object v1, LX/Gyp;->OVERALL_TTI:LX/Gyp;

    invoke-virtual {v0, v1}, LX/Gyq;->c(LX/Gyp;)V

    .line 2410285
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    const/4 v1, 0x1

    .line 2410286
    iput-boolean v1, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->N:Z

    .line 2410287
    :cond_0
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    sget-object v1, LX/Gym;->ERROR:LX/Gym;

    invoke-static {v0, v1}, Lcom/facebook/location/ui/LocationSettingsFragment;->a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;LX/Gym;)V

    .line 2410288
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2410289
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2410290
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-boolean v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->N:Z

    if-nez v0, :cond_0

    .line 2410291
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    sget-object v3, LX/Gyp;->FETCH_DATA:LX/Gyp;

    invoke-virtual {v0, v3}, LX/Gyq;->b(LX/Gyp;)V

    .line 2410292
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->p:LX/Gyq;

    sget-object v3, LX/Gyp;->RENDER:LX/Gyp;

    invoke-virtual {v0, v3}, LX/Gyq;->a(LX/Gyp;)V

    .line 2410293
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    .line 2410294
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v3

    .line 2410295
    check-cast v3, Lcom/facebook/widget/CustomFrameLayout;

    move-object v3, v3

    .line 2410296
    new-instance v4, LX/Gyc;

    invoke-direct {v4, v0}, LX/Gyc;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {v3, v4}, Lcom/facebook/widget/CustomFrameLayout;->a(LX/10U;)V

    .line 2410297
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2410298
    check-cast v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v4, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-virtual {v3, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    .line 2410299
    iput-boolean v0, v4, Lcom/facebook/location/ui/LocationSettingsFragment;->J:Z

    .line 2410300
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2410301
    check-cast v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->h(II)Z

    move-result v4

    .line 2410302
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2410303
    check-cast v0, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/settings/read/BackgroundLocationSettingsReadGraphQLModels$LocationHistoryEnabledQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v5, 0x2

    invoke-virtual {v3, v0, v5}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 2410304
    :goto_0
    iget-object v3, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v3}, Lcom/facebook/location/ui/LocationSettingsFragment;->m(Lcom/facebook/location/ui/LocationSettingsFragment;)Z

    move-result v5

    .line 2410305
    iget-object v3, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v3, v3, Lcom/facebook/location/ui/LocationSettingsFragment;->K:LX/03R;

    sget-object v6, LX/03R;->UNSET:LX/03R;

    if-eq v3, v6, :cond_5

    move v3, v1

    .line 2410306
    :goto_1
    iget-object v6, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v4}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v7

    .line 2410307
    iput-object v7, v6, Lcom/facebook/location/ui/LocationSettingsFragment;->K:LX/03R;

    .line 2410308
    iget-object v6, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    .line 2410309
    iput-object v0, v6, Lcom/facebook/location/ui/LocationSettingsFragment;->L:LX/03R;

    .line 2410310
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v0, v4}, Lcom/facebook/location/ui/LocationSettingsFragment;->c$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V

    .line 2410311
    iget-object v6, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    if-eqz v4, :cond_6

    if-eqz v5, :cond_6

    move v0, v1

    :goto_2
    invoke-static {v6, v0}, Lcom/facebook/location/ui/LocationSettingsFragment;->b(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V

    .line 2410312
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v0, v4, v5}, Lcom/facebook/location/ui/LocationSettingsFragment;->a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;ZZ)V

    .line 2410313
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    sget-object v6, LX/Gym;->CONTENT:LX/Gym;

    invoke-static {v0, v6}, Lcom/facebook/location/ui/LocationSettingsFragment;->a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;LX/Gym;)V

    .line 2410314
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->y:Lcom/facebook/widget/BetterSwitch;

    if-nez v4, :cond_1

    if-eqz v5, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    invoke-virtual {v0, v2}, Lcom/facebook/widget/BetterSwitch;->setEnabled(Z)V

    .line 2410315
    if-nez v3, :cond_3

    if-nez v5, :cond_3

    if-eqz v4, :cond_3

    .line 2410316
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    .line 2410317
    invoke-static {v0}, Lcom/facebook/location/ui/LocationSettingsFragment;->s$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    .line 2410318
    :cond_3
    iget-object v0, p0, LX/Gyj;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    .line 2410319
    invoke-static {v0}, Lcom/facebook/location/ui/LocationSettingsFragment;->e$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    .line 2410320
    return-void

    :cond_4
    move v0, v2

    .line 2410321
    goto :goto_0

    :cond_5
    move v3, v2

    .line 2410322
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2410323
    goto :goto_2
.end method
