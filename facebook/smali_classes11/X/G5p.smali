.class public LX/G5p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Ljava/io/File;

.field public final c:I

.field public final d:I

.field public final e:LX/60y;

.field public final f:Z


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/io/File;IILX/60y;)V
    .locals 7

    .prologue
    .line 2319422
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, LX/G5p;-><init>(Ljava/io/File;Ljava/io/File;IILX/60y;Z)V

    .line 2319423
    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/io/File;IILX/60y;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2319411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319412
    const/4 v0, -0x1

    if-eq p3, v0, :cond_0

    if-ltz p3, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2319413
    const/4 v0, -0x2

    if-eq p4, v0, :cond_1

    if-ltz p4, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2319414
    iput-object p1, p0, LX/G5p;->a:Ljava/io/File;

    .line 2319415
    iput-object p2, p0, LX/G5p;->b:Ljava/io/File;

    .line 2319416
    iput p3, p0, LX/G5p;->c:I

    .line 2319417
    iput p4, p0, LX/G5p;->d:I

    .line 2319418
    iput-object p5, p0, LX/G5p;->e:LX/60y;

    .line 2319419
    iput-boolean p6, p0, LX/G5p;->f:Z

    .line 2319420
    return-void

    :cond_3
    move v0, v1

    .line 2319421
    goto :goto_0
.end method
