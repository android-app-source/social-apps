.class public LX/GPg;
.super LX/6sU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sU",
        "<",
        "Lcom/facebook/common/util/ParcelablePair",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/CreditCard;",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349292
    const-class v0, Ljava/lang/String;

    invoke-direct {p0, p1, v0}, LX/6sU;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2349293
    return-void
.end method

.method public static b(LX/0QB;)LX/GPg;
    .locals 2

    .prologue
    .line 2349294
    new-instance v1, LX/GPg;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/GPg;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349295
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2349296
    check-cast p1, Lcom/facebook/common/util/ParcelablePair;

    .line 2349297
    const-string v1, "/%s/csc_tokens"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, LX/73t;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/14O;

    move-result-object v0

    invoke-virtual {p0}, LX/GPg;->d()Ljava/lang/String;

    move-result-object v1

    .line 2349298
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349299
    move-object v0, v0

    .line 2349300
    const-string v1, "POST"

    .line 2349301
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349302
    move-object v0, v0

    .line 2349303
    const-string v1, "csc"

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/14O;->a(Ljava/util/Map;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2349304
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349305
    move-object v0, v0

    .line 2349306
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2349307
    const-string v0, "cached_csc_token"

    invoke-static {p2, v0}, LX/6sU;->a(LX/1pN;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349308
    const-string v0, "get_csc_token"

    return-object v0
.end method
