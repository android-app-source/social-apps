.class public LX/GoQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Yr;


# instance fields
.field private final a:LX/1Fb;


# direct methods
.method public constructor <init>(LX/1Fb;)V
    .locals 0

    .prologue
    .line 2394482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2394483
    iput-object p1, p0, LX/GoQ;->a:LX/1Fb;

    .line 2394484
    return-void
.end method


# virtual methods
.method public final clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2394485
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    const-string v1, "InstantShoppingPhoto is a client side only class"

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394480
    const/4 v0, 0x0

    return-object v0
.end method

.method public final eg_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394481
    iget-object v0, p0, LX/GoQ;->a:LX/1Fb;

    return-object v0
.end method

.method public final j()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394479
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$FBPhotoEncodings$PhotoEncodings;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394478
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394477
    const/4 v0, 0x0

    return-object v0
.end method
