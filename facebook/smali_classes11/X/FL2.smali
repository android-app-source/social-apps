.class public LX/FL2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/5zm;


# direct methods
.method public constructor <init>(LX/5zm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2226005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226006
    iput-object p1, p0, LX/FL2;->a:LX/5zm;

    .line 2226007
    return-void
.end method

.method private b(Lcom/facebook/messaging/service/model/ModifyThreadParams;)LX/4cQ;
    .locals 3

    .prologue
    .line 2226000
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 2226001
    iget-object v1, p0, LX/FL2;->a:LX/5zm;

    invoke-virtual {v1, v0}, LX/5zm;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zl;

    move-result-object v0

    .line 2226002
    if-nez v0, :cond_0

    .line 2226003
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Failed to attach image"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2226004
    :cond_0
    new-instance v1, LX/4cQ;

    const-string v2, "image"

    invoke-direct {v1, v2, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2225971
    check-cast p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2225972
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2225973
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225974
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tid"

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/ModifyThreadParams;->u()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225975
    const/4 v0, 0x0

    .line 2225976
    iget-object v2, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v2, v2

    .line 2225977
    if-eqz v2, :cond_1

    .line 2225978
    invoke-direct {p0, p1}, LX/FL2;->b(Lcom/facebook/messaging/service/model/ModifyThreadParams;)LX/4cQ;

    move-result-object v0

    .line 2225979
    :goto_0
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "setThreadImage"

    .line 2225980
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2225981
    move-object v2, v2

    .line 2225982
    const-string v3, "POST"

    .line 2225983
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2225984
    move-object v2, v2

    .line 2225985
    const-string v3, "method/messaging.setthreadimage"

    .line 2225986
    iput-object v3, v2, LX/14O;->d:Ljava/lang/String;

    .line 2225987
    move-object v2, v2

    .line 2225988
    iput-object v1, v2, LX/14O;->g:Ljava/util/List;

    .line 2225989
    move-object v1, v2

    .line 2225990
    sget-object v2, LX/14S;->STRING:LX/14S;

    .line 2225991
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 2225992
    move-object v1, v1

    .line 2225993
    if-eqz v0, :cond_0

    .line 2225994
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2225995
    iput-object v0, v1, LX/14O;->l:Ljava/util/List;

    .line 2225996
    :cond_0
    invoke-virtual {v1}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2225997
    :cond_1
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "delete"

    const-string v4, "1"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225998
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225999
    const/4 v0, 0x0

    return-object v0
.end method
