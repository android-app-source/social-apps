.class public LX/Gpp;
.super LX/Gpo;
.source ""


# instance fields
.field private final i:LX/Go0;


# direct methods
.method public constructor <init>(LX/Cow;LX/Go0;)V
    .locals 0

    .prologue
    .line 2395918
    invoke-direct {p0, p1}, LX/Gpo;-><init>(LX/Cow;)V

    .line 2395919
    iput-object p2, p0, LX/Gpp;->i:LX/Go0;

    .line 2395920
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2395917
    check-cast p1, LX/Clw;

    invoke-virtual {p0, p1}, LX/Cne;->a(LX/Clw;)V

    return-void
.end method

.method public final a(LX/Clw;)V
    .locals 4

    .prologue
    .line 2395921
    invoke-super {p0, p1}, LX/Gpo;->a(LX/Clw;)V

    .line 2395922
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395923
    instance-of v0, v0, LX/Gqi;

    if-nez v0, :cond_1

    .line 2395924
    :cond_0
    :goto_0
    return-void

    .line 2395925
    :cond_1
    instance-of v0, p1, LX/Goh;

    if-eqz v0, :cond_0

    .line 2395926
    check-cast p1, LX/Goh;

    invoke-interface {p1}, LX/Goh;->z()LX/CHZ;

    move-result-object v0

    .line 2395927
    if-eqz v0, :cond_0

    .line 2395928
    new-instance v1, LX/GoW;

    invoke-direct {v1, v0}, LX/GoW;-><init>(LX/CHZ;)V

    .line 2395929
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2395930
    new-instance v2, LX/GoU;

    invoke-virtual {v1}, LX/GoW;->f()LX/GoE;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/GoU;-><init>(Landroid/content/Context;LX/GoE;)V

    invoke-virtual {v2, v1}, LX/Cle;->a(LX/GoW;)LX/Cle;

    move-result-object v2

    invoke-virtual {v2}, LX/Cle;->a()LX/Clf;

    move-result-object v2

    move-object v2, v2

    .line 2395931
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395932
    check-cast v0, LX/Gqi;

    invoke-virtual {v1}, LX/GoF;->e()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;

    move-result-object v3

    invoke-virtual {v1}, LX/GoW;->g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v1

    .line 2395933
    iget-object p0, v0, LX/Gqi;->t:Lcom/facebook/widget/text/BetterTextView;

    .line 2395934
    iget-object p1, v2, LX/Clf;->a:Ljava/lang/CharSequence;

    move-object p1, p1

    .line 2395935
    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395936
    iget-object p0, v0, LX/Gqi;->r:LX/CIh;

    iget-object p1, v0, LX/Gqi;->t:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p1, v3, v1}, LX/CIh;->a(Landroid/widget/TextView;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)V

    .line 2395937
    goto :goto_0
.end method
