.class public LX/Gwk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/katana/server/LoginApprovalResendCodeParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2407321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407322
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2407323
    check-cast p1, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;

    .line 2407324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2407325
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "first_factor"

    .line 2407326
    iget-object v3, p1, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2407327
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2407328
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "login_approval_resend_code"

    .line 2407329
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2407330
    move-object v1, v1

    .line 2407331
    const-string v2, "POST"

    .line 2407332
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2407333
    move-object v1, v1

    .line 2407334
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2407335
    iget-wide v6, p1, Lcom/facebook/katana/server/LoginApprovalResendCodeParams;->a:J

    move-wide v4, v6

    .line 2407336
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/twofacsms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2407337
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2407338
    move-object v1, v1

    .line 2407339
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2407340
    move-object v0, v1

    .line 2407341
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2407342
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2407343
    move-object v0, v0

    .line 2407344
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2407345
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2407346
    const/4 v0, 0x0

    return-object v0
.end method
