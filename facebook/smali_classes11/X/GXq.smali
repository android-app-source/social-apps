.class public final enum LX/GXq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GXq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GXq;

.field public static final enum ACTN_ADD_PRODUCT_CLICK:LX/GXq;

.field public static final enum ACTN_CANCEL_ADD_PRODUCT:LX/GXq;

.field public static final enum ACTN_CANCEL_CURRENCY_SELECTION:LX/GXq;

.field public static final enum ACTN_CANCEL_EDIT_PRODUCT:LX/GXq;

.field public static final enum ACTN_EDIT_PRODUCT_CLICK:LX/GXq;

.field public static final enum ACTN_PRIVATE_MESSAGE_BACK_CLICK:LX/GXq;

.field public static final enum IMP_CURRENCY_SELECTION:LX/GXq;

.field public static final enum IMP_PRIVATE_MESSAGE_SELECTION:LX/GXq;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2364598
    new-instance v0, LX/GXq;

    const-string v1, "IMP_CURRENCY_SELECTION"

    const-string v2, "imp_currency_selection"

    invoke-direct {v0, v1, v4, v2}, LX/GXq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXq;->IMP_CURRENCY_SELECTION:LX/GXq;

    .line 2364599
    new-instance v0, LX/GXq;

    const-string v1, "ACTN_CANCEL_CURRENCY_SELECTION"

    const-string v2, "actn_cancel_currency_selection"

    invoke-direct {v0, v1, v5, v2}, LX/GXq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXq;->ACTN_CANCEL_CURRENCY_SELECTION:LX/GXq;

    .line 2364600
    new-instance v0, LX/GXq;

    const-string v1, "ACTN_ADD_PRODUCT_CLICK"

    const-string v2, "actn_add_product_click"

    invoke-direct {v0, v1, v6, v2}, LX/GXq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXq;->ACTN_ADD_PRODUCT_CLICK:LX/GXq;

    .line 2364601
    new-instance v0, LX/GXq;

    const-string v1, "ACTN_EDIT_PRODUCT_CLICK"

    const-string v2, "actn_edit_product_click"

    invoke-direct {v0, v1, v7, v2}, LX/GXq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXq;->ACTN_EDIT_PRODUCT_CLICK:LX/GXq;

    .line 2364602
    new-instance v0, LX/GXq;

    const-string v1, "ACTN_CANCEL_ADD_PRODUCT"

    const-string v2, "actn_cancel_add_product"

    invoke-direct {v0, v1, v8, v2}, LX/GXq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXq;->ACTN_CANCEL_ADD_PRODUCT:LX/GXq;

    .line 2364603
    new-instance v0, LX/GXq;

    const-string v1, "ACTN_CANCEL_EDIT_PRODUCT"

    const/4 v2, 0x5

    const-string v3, "actn_cancel_edit_product"

    invoke-direct {v0, v1, v2, v3}, LX/GXq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXq;->ACTN_CANCEL_EDIT_PRODUCT:LX/GXq;

    .line 2364604
    new-instance v0, LX/GXq;

    const-string v1, "IMP_PRIVATE_MESSAGE_SELECTION"

    const/4 v2, 0x6

    const-string v3, "imp_private_message_selection"

    invoke-direct {v0, v1, v2, v3}, LX/GXq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXq;->IMP_PRIVATE_MESSAGE_SELECTION:LX/GXq;

    .line 2364605
    new-instance v0, LX/GXq;

    const-string v1, "ACTN_PRIVATE_MESSAGE_BACK_CLICK"

    const/4 v2, 0x7

    const-string v3, "actn_private_message_back_click"

    invoke-direct {v0, v1, v2, v3}, LX/GXq;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GXq;->ACTN_PRIVATE_MESSAGE_BACK_CLICK:LX/GXq;

    .line 2364606
    const/16 v0, 0x8

    new-array v0, v0, [LX/GXq;

    sget-object v1, LX/GXq;->IMP_CURRENCY_SELECTION:LX/GXq;

    aput-object v1, v0, v4

    sget-object v1, LX/GXq;->ACTN_CANCEL_CURRENCY_SELECTION:LX/GXq;

    aput-object v1, v0, v5

    sget-object v1, LX/GXq;->ACTN_ADD_PRODUCT_CLICK:LX/GXq;

    aput-object v1, v0, v6

    sget-object v1, LX/GXq;->ACTN_EDIT_PRODUCT_CLICK:LX/GXq;

    aput-object v1, v0, v7

    sget-object v1, LX/GXq;->ACTN_CANCEL_ADD_PRODUCT:LX/GXq;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/GXq;->ACTN_CANCEL_EDIT_PRODUCT:LX/GXq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GXq;->IMP_PRIVATE_MESSAGE_SELECTION:LX/GXq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GXq;->ACTN_PRIVATE_MESSAGE_BACK_CLICK:LX/GXq;

    aput-object v2, v0, v1

    sput-object v0, LX/GXq;->$VALUES:[LX/GXq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2364608
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2364609
    iput-object p3, p0, LX/GXq;->value:Ljava/lang/String;

    .line 2364610
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GXq;
    .locals 1

    .prologue
    .line 2364611
    const-class v0, LX/GXq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GXq;

    return-object v0
.end method

.method public static values()[LX/GXq;
    .locals 1

    .prologue
    .line 2364607
    sget-object v0, LX/GXq;->$VALUES:[LX/GXq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GXq;

    return-object v0
.end method
