.class public final LX/GW9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;)V
    .locals 0

    .prologue
    .line 2360407
    iput-object p1, p0, LX/GW9;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2360408
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2360409
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2360410
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2360411
    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2360412
    iget-object v1, p0, LX/GW9;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-static {v1, v0}, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->a$redex0(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    .line 2360413
    iget-object v1, p0, LX/GW9;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v1, v1, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->j:LX/8Fe;

    new-instance v2, LX/GW8;

    invoke-direct {v2, p0, v0}, LX/GW8;-><init>(LX/GW9;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    .line 2360414
    const/4 v4, 0x0

    .line 2360415
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2360416
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2360417
    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_0

    .line 2360418
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2360419
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v6

    iget-object p1, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2360420
    invoke-virtual {v5, v3, v4}, LX/15i;->j(II)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {p1, v6, v4}, LX/15i;->h(II)Z

    move-result v4

    invoke-static {v3, v4}, LX/7j5;->a(IZ)Z

    move-result v4

    :cond_0
    move v3, v4

    .line 2360421
    if-eqz v3, :cond_1

    .line 2360422
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, LX/8Fe;->a(Ljava/lang/String;LX/8Fd;)V

    .line 2360423
    :cond_1
    iget-object v0, p0, LX/GW9;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v0, v0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->a:LX/7iU;

    const v3, 0x6c0003

    .line 2360424
    iget-object v1, v0, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2360425
    iget-object v1, v0, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v1, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2360426
    :cond_2
    return-void

    :cond_3
    move v3, v4

    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_0
.end method
