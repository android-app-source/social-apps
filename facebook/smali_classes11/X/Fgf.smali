.class public LX/Fgf;
.super LX/0gG;
.source ""

# interfaces
.implements LX/Fgb;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/Fgq;

.field public final c:LX/Fgv;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Cwd;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0gc;

.field private final f:Z

.field public g:Lcom/facebook/search/api/GraphSearchQuery;

.field private h:I

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cwd;",
            "LX/Fgb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/Boolean;LX/0gc;LX/Fgq;LX/Fgv;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270628
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2270629
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Fgf;->f:Z

    .line 2270630
    iput-object p5, p0, LX/Fgf;->b:LX/Fgq;

    .line 2270631
    iput-object p6, p0, LX/Fgf;->c:LX/Fgv;

    .line 2270632
    iput-object p2, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270633
    iput-object p1, p0, LX/Fgf;->a:Landroid/content/Context;

    .line 2270634
    iput-object p4, p0, LX/Fgf;->e:LX/0gc;

    .line 2270635
    iget-object v0, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/Cwd;->getTabs(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Fgf;->d:LX/0Px;

    .line 2270636
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    sget-object v1, LX/Cwd;->SCOPED:LX/Cwd;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2270637
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    sget-object v1, LX/Cwd;->GLOBAL:LX/Cwd;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2270638
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    .line 2270639
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    sget-object v1, LX/Cwd;->SCOPED:LX/Cwd;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2270640
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    sget-object v1, LX/Cwd;->GLOBAL:LX/Cwd;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2270641
    iget-object v0, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fgf;->c:LX/Fgv;

    iget-object v1, p0, LX/Fgf;->a:Landroid/content/Context;

    iget-object p1, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    iget-object p2, p0, LX/Fgf;->e:LX/0gc;

    invoke-virtual {v0, v1, p1, p2}, LX/Fgv;->a(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;LX/0gc;)LX/Fgu;

    move-result-object v0

    .line 2270642
    :goto_0
    iget-object v1, p0, LX/Fgf;->i:Ljava/util/Map;

    sget-object p1, LX/Cwd;->SCOPED:LX/Cwd;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2270643
    iget-object v0, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, Lcom/facebook/search/api/GraphSearchQuery;->b(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 2270644
    iget-object v1, p0, LX/Fgf;->i:Ljava/util/Map;

    sget-object p1, LX/Cwd;->GLOBAL:LX/Cwd;

    iget-object p2, p0, LX/Fgf;->b:LX/Fgq;

    iget-object p3, p0, LX/Fgf;->a:Landroid/content/Context;

    invoke-virtual {p2, p3, v0}, LX/Fgq;->a(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;)LX/Fgp;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2270645
    return-void

    .line 2270646
    :cond_0
    iget-object v0, p0, LX/Fgf;->b:LX/Fgq;

    iget-object v1, p0, LX/Fgf;->a:Landroid/content/Context;

    iget-object p1, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1, p1}, LX/Fgq;->a(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;)LX/Fgp;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)LX/Cwd;
    .locals 2

    .prologue
    .line 2270647
    const/4 v0, 0x0

    .line 2270648
    iget-object v1, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    if-eqz v1, :cond_0

    .line 2270649
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270650
    iget-object p0, v1, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v1, p0

    .line 2270651
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/7B5;->TAB:LX/7B5;

    invoke-virtual {v1}, LX/7B5;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2270652
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2270653
    :cond_0
    if-eqz v0, :cond_1

    sget-object v0, LX/Cwd;->SCOPED:LX/Cwd;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/Cwd;->GLOBAL:LX/Cwd;

    goto :goto_0
.end method

.method private r()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2270654
    iget-object v0, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270655
    iget-object v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v1

    .line 2270656
    if-nez v0, :cond_0

    .line 2270657
    const-string v0, ""

    .line 2270658
    :goto_0
    return-object v0

    .line 2270659
    :cond_0
    sget-object v1, LX/Fge;->b:[I

    invoke-virtual {v0}, LX/103;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2270660
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Search query had an invalid scoped entity type of %s"

    invoke-virtual {v0}, LX/103;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2270661
    :pswitch_0
    const v0, 0x7f0820b0

    .line 2270662
    :goto_1
    iget-object v1, p0, LX/Fgf;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2270663
    :pswitch_1
    const v0, 0x7f0820ae

    .line 2270664
    iget-object v1, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270665
    iget-object v2, v1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v1, v2

    .line 2270666
    iget-object v2, p0, LX/Fgf;->a:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v0, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2270667
    :pswitch_2
    const v0, 0x7f0820ad

    goto :goto_1

    .line 2270668
    :pswitch_3
    const v0, 0x7f0820af

    goto :goto_1

    .line 2270669
    :pswitch_4
    const v0, 0x7f0820b1

    goto :goto_1

    .line 2270670
    :pswitch_5
    const v0, 0x7f0820b2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2270621
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270622
    sget-object v1, LX/Fge;->a:[I

    invoke-virtual {v0}, LX/Cwd;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2270623
    iget-object v0, p0, LX/Fgf;->a:Landroid/content/Context;

    const v1, 0x7f0820ac

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2270624
    :pswitch_0
    invoke-direct {p0}, LX/Fgf;->r()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/7Hi;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270671
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 2270672
    iget-object v0, v0, LX/7B6;->c:Ljava/lang/String;

    .line 2270673
    invoke-direct {p0, v0}, LX/Fgf;->b(Ljava/lang/String;)LX/Cwd;

    move-result-object v0

    .line 2270674
    iget-object v1, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1}, LX/Fgb;->a(LX/7Hi;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/Fid;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270675
    invoke-direct {p0, p1}, LX/Fgf;->b(Ljava/lang/String;)LX/Cwd;

    move-result-object v0

    .line 2270676
    iget-object v1, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1}, LX/Fgb;->a(Ljava/lang/String;)LX/Fid;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2270677
    if-ltz p2, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2270678
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->f()Landroid/view/View;

    move-result-object v0

    .line 2270679
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2270680
    return-object v0

    .line 2270681
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 2270682
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1}, LX/Fgb;->a(I)V

    .line 2270683
    return-void
.end method

.method public final a(LX/0zw;Landroid/view/View;Z)V
    .locals 5
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;",
            "Landroid/view/View;",
            "Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2270684
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270685
    iget-object v4, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    const/4 v4, 0x0

    invoke-interface {v0, v4, p2, v2}, LX/Fgb;->a(LX/0zw;Landroid/view/View;Z)V

    .line 2270686
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270687
    :cond_0
    return-void
.end method

.method public final a(LX/7HP;)V
    .locals 4

    .prologue
    .line 2270723
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270724
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1}, LX/Fgb;->a(LX/7HP;)V

    .line 2270725
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270726
    :cond_0
    return-void
.end method

.method public final a(LX/Cvp;)V
    .locals 4

    .prologue
    .line 2270688
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270689
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1}, LX/Fgb;->a(LX/Cvp;)V

    .line 2270690
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270691
    :cond_0
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    sget-object v1, LX/Cwd;->SCOPED:LX/Cwd;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->h()LX/2SP;

    move-result-object v0

    .line 2270692
    if-eqz v0, :cond_1

    .line 2270693
    iget-object v1, p0, LX/Fgf;->g:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/2SP;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2270694
    :cond_1
    return-void
.end method

.method public final a(LX/Fh9;)V
    .locals 4

    .prologue
    .line 2270719
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270720
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1}, LX/Fgb;->a(LX/Fh9;)V

    .line 2270721
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270722
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;LX/Fh8;)V
    .locals 4

    .prologue
    .line 2270715
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270716
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1, p2}, LX/Fgb;->a(Landroid/content/Context;LX/Fh8;)V

    .line 2270717
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270718
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;LX/FhA;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2270711
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270712
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    const/4 v3, 0x0

    invoke-interface {v0, v3, p2, p3}, LX/Fgb;->a(Landroid/view/View;LX/FhA;Z)V

    .line 2270713
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270714
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2270706
    if-ltz p2, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2270707
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2270708
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2270709
    return-void

    .line 2270710
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 4

    .prologue
    .line 2270702
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270703
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1, p2}, LX/Fgb;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 2270704
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270705
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 3

    .prologue
    .line 2270698
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    sget-object v1, LX/Cwd;->SCOPED:LX/Cwd;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1}, LX/Fgb;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2270699
    invoke-static {p1}, Lcom/facebook/search/api/GraphSearchQuery;->b(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    .line 2270700
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    sget-object v2, LX/Cwd;->GLOBAL:LX/Cwd;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, v1}, LX/Fgb;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2270701
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270695
    invoke-direct {p0, p1}, LX/Fgf;->b(Ljava/lang/String;)LX/Cwd;

    move-result-object v0

    .line 2270696
    iget-object v1, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0, p1, p2}, LX/Fgb;->a(Ljava/lang/String;LX/0Px;)V

    .line 2270697
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7BE;Z)V
    .locals 2

    .prologue
    .line 2270625
    invoke-direct {p0, p1}, LX/Fgf;->b(Ljava/lang/String;)LX/Cwd;

    move-result-object v0

    .line 2270626
    iget-object v1, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, LX/Fgb;->a(Ljava/lang/String;LX/7BE;Z)V

    .line 2270627
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2270593
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2270592
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2270585
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2270586
    iput p1, p0, LX/Fgf;->h:I

    .line 2270587
    :goto_1
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2270588
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v2, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2270589
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 2270590
    goto :goto_0

    .line 2270591
    :cond_1
    return-void
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270617
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2270618
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    .line 2270619
    invoke-interface {v0}, LX/Fgb;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_0

    .line 2270620
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Qq;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270584
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->d()LX/1Qq;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/7HZ;
    .locals 3

    .prologue
    .line 2270583
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->e()LX/7HZ;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/view/View;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270581
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->f()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/0g8;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270580
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->g()LX/0g8;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/2SP;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270582
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->h()LX/2SP;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 3

    .prologue
    .line 2270594
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/FgU;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270595
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->j()LX/FgU;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/Fi7;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270596
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->k()LX/Fi7;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/Fid;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270597
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    iget-object v1, p0, LX/Fgf;->d:LX/0Px;

    iget v2, p0, LX/Fgf;->h:I

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->l()LX/Fid;

    move-result-object v0

    return-object v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2270598
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    sget-object v1, LX/Cwd;->GLOBAL:LX/Cwd;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->m()V

    .line 2270599
    return-void
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 2270600
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270601
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->n()V

    .line 2270602
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270603
    :cond_0
    iget-object v0, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2270604
    return-void
.end method

.method public final o()V
    .locals 4

    .prologue
    .line 2270605
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270606
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->o()V

    .line 2270607
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270608
    :cond_0
    return-void
.end method

.method public final p()V
    .locals 4

    .prologue
    .line 2270609
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270610
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->p()V

    .line 2270611
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270612
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 4

    .prologue
    .line 2270613
    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Fgf;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwd;

    .line 2270614
    iget-object v3, p0, LX/Fgf;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->q()V

    .line 2270615
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2270616
    :cond_0
    return-void
.end method
