.class public final LX/Fim;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Fin;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/search/model/EntityTypeaheadUnit;

.field public final synthetic b:LX/Fin;


# direct methods
.method public constructor <init>(LX/Fin;)V
    .locals 1

    .prologue
    .line 2275774
    iput-object p1, p0, LX/Fim;->b:LX/Fin;

    .line 2275775
    move-object v0, p1

    .line 2275776
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2275777
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2275778
    const-string v0, "SearchTypeaheadEntityComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2275779
    if-ne p0, p1, :cond_1

    .line 2275780
    :cond_0
    :goto_0
    return v0

    .line 2275781
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2275782
    goto :goto_0

    .line 2275783
    :cond_3
    check-cast p1, LX/Fim;

    .line 2275784
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2275785
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2275786
    if-eq v2, v3, :cond_0

    .line 2275787
    iget-object v2, p0, LX/Fim;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Fim;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    iget-object v3, p1, LX/Fim;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    invoke-virtual {v2, v3}, Lcom/facebook/search/model/EntityTypeaheadUnit;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2275788
    goto :goto_0

    .line 2275789
    :cond_4
    iget-object v2, p1, LX/Fim;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
