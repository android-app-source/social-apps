.class public final LX/G9o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/util/Comparator",
        "<",
        "LX/G9m;",
        ">;"
    }
.end annotation


# instance fields
.field private final average:F


# direct methods
.method public constructor <init>(F)V
    .locals 0

    .prologue
    .line 2323527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2323528
    iput p1, p0, LX/G9o;->average:F

    .line 2323529
    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 2323530
    check-cast p1, LX/G9m;

    check-cast p2, LX/G9m;

    .line 2323531
    iget v0, p2, LX/G9m;->a:F

    move v0, v0

    .line 2323532
    iget v1, p0, LX/G9o;->average:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 2323533
    iget v1, p1, LX/G9m;->a:F

    move v1, v1

    .line 2323534
    iget v2, p0, LX/G9o;->average:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 2323535
    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
