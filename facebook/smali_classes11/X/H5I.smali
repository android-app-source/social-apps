.class public LX/H5I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kl;",
        ":",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/H5I;


# instance fields
.field private final a:LX/E1f;

.field private final b:Landroid/os/Handler;

.field private final c:LX/7vW;

.field private final d:LX/7vZ;


# direct methods
.method public constructor <init>(LX/E1f;Landroid/os/Handler;LX/7vW;LX/7vZ;)V
    .locals 0
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2424068
    iput-object p1, p0, LX/H5I;->a:LX/E1f;

    .line 2424069
    iput-object p2, p0, LX/H5I;->b:Landroid/os/Handler;

    .line 2424070
    iput-object p3, p0, LX/H5I;->c:LX/7vW;

    .line 2424071
    iput-object p4, p0, LX/H5I;->d:LX/7vZ;

    .line 2424072
    return-void
.end method

.method public static a(LX/1De;)LX/1Di;
    .locals 3

    .prologue
    .line 2424073
    invoke-static {p0}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const v1, 0x7f0a06e0

    invoke-virtual {v0, v1}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x6

    const v2, 0x7f0b163a

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0033

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/H5I;
    .locals 7

    .prologue
    .line 2423994
    sget-object v0, LX/H5I;->e:LX/H5I;

    if-nez v0, :cond_1

    .line 2423995
    const-class v1, LX/H5I;

    monitor-enter v1

    .line 2423996
    :try_start_0
    sget-object v0, LX/H5I;->e:LX/H5I;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2423997
    if-eqz v2, :cond_0

    .line 2423998
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2423999
    new-instance p0, LX/H5I;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v3

    check-cast v3, LX/E1f;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v5

    check-cast v5, LX/7vW;

    invoke-static {v0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v6

    check-cast v6, LX/7vZ;

    invoke-direct {p0, v3, v4, v5, v6}, LX/H5I;-><init>(LX/E1f;Landroid/os/Handler;LX/7vW;LX/7vZ;)V

    .line 2424000
    move-object v0, p0

    .line 2424001
    sput-object v0, LX/H5I;->e:LX/H5I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424002
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2424003
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2424004
    :cond_1
    sget-object v0, LX/H5I;->e:LX/H5I;

    return-object v0

    .line 2424005
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2424006
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2424066
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;LX/Cfc;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "LX/Cfc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2424074
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v0

    .line 2424075
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    move-object v0, p3

    .line 2424076
    check-cast v0, LX/1Pr;

    new-instance v3, LX/E2Q;

    invoke-direct {v3, v1}, LX/E2Q;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2R;

    .line 2424077
    invoke-static {p2}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v1

    .line 2424078
    invoke-static {v1}, LX/BE6;->a(LX/2nq;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2424079
    iget-object v3, v0, LX/E2R;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2424080
    if-nez v3, :cond_0

    .line 2424081
    iget-object v3, p0, LX/H5I;->b:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/notifications/multirow/components/NotificationsActionListComponentSpec$1;

    invoke-direct {v4, p0, p3, v1}, Lcom/facebook/notifications/multirow/components/NotificationsActionListComponentSpec$1;-><init>(LX/H5I;LX/2km;LX/2nq;)V

    const-wide/16 v6, 0x7d0

    const v1, 0x6a46a756

    invoke-static {v3, v4, v6, v7, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2424082
    :cond_0
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2424083
    invoke-static {v1, p1}, LX/H5I;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Ljava/lang/String;

    move-result-object v1

    .line 2424084
    iput-object v1, v0, LX/E2R;->a:Ljava/lang/String;

    .line 2424085
    move-object v0, p3

    .line 2424086
    check-cast v0, LX/2kl;

    invoke-interface {v0}, LX/2kl;->md_()LX/2jc;

    move-result-object v0

    invoke-interface {v0, p2, p4}, LX/2jc;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/Cfc;)V

    move-object v0, p3

    .line 2424087
    check-cast v0, LX/2kk;

    invoke-static {p2}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2kk;->a(LX/2nq;)V

    .line 2424088
    check-cast p3, LX/1Pq;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    invoke-interface {p3, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2424089
    return-void
.end method

.method private static a(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/lang/String;)V
    .locals 9
    .param p3    # LX/2km;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2424051
    iget-object v0, p0, LX/H5I;->a:LX/E1f;

    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object v1, p3

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2424052
    iget-object v3, v1, LX/2jY;->a:Ljava/lang/String;

    move-object v4, v3

    .line 2424053
    move-object v1, p3

    check-cast v1, LX/2kp;

    invoke-interface {v1}, LX/2kp;->t()LX/2jY;

    move-result-object v1

    .line 2424054
    iget-object v3, v1, LX/2jY;->b:Ljava/lang/String;

    move-object v5, v3

    .line 2424055
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v1

    .line 2424056
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v1

    .line 2424057
    move-object v1, p3

    check-cast v1, LX/2kn;

    invoke-interface {v1}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v8

    move-object v1, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v8}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v0

    .line 2424058
    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, LX/Cfl;->d:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 2424059
    invoke-static {p2}, LX/E1q;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/2nq;

    move-result-object v1

    .line 2424060
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2424061
    iget-object v2, v0, LX/Cfl;->d:Landroid/content/Intent;

    const-string v3, "notification_id"

    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/BCw;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2424062
    :cond_0
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2424063
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2424064
    invoke-interface {p3, v1, v2, v0}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2424065
    return-void
.end method

.method public static a(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2424036
    sget-object v1, LX/H5H;->a:[I

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move-object v7, v0

    move-object v2, v0

    .line 2424037
    :goto_0
    iget-object v0, p0, LX/H5I;->c:LX/7vW;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2424038
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v3

    .line 2424039
    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2424040
    invoke-static {p0, p1, p2, p3, v7}, LX/H5I;->a(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;LX/Cfc;)V

    .line 2424041
    return-void

    .line 2424042
    :pswitch_0
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2424043
    sget-object v0, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    move-object v7, v0

    .line 2424044
    goto :goto_0

    .line 2424045
    :pswitch_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2424046
    sget-object v0, LX/Cfc;->EVENT_CARD_MAYBE_TAP:LX/Cfc;

    move-object v7, v0

    .line 2424047
    goto :goto_0

    .line 2424048
    :pswitch_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2424049
    sget-object v0, LX/Cfc;->EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

    move-object v7, v0

    .line 2424050
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2424021
    sget-object v1, LX/H5H;->a:[I

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move-object v7, v0

    move-object v2, v0

    .line 2424022
    :goto_0
    iget-object v0, p0, LX/H5I;->d:LX/7vZ;

    invoke-virtual {p1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2424023
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v3

    .line 2424024
    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v6}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2424025
    invoke-static {p0, p1, p2, p3, v7}, LX/H5I;->a(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;LX/Cfc;)V

    .line 2424026
    return-void

    .line 2424027
    :pswitch_0
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2424028
    sget-object v0, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    move-object v7, v0

    .line 2424029
    goto :goto_0

    .line 2424030
    :pswitch_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2424031
    sget-object v0, LX/Cfc;->EVENT_CARD_INTERESTED_TAP:LX/Cfc;

    move-object v7, v0

    .line 2424032
    goto :goto_0

    .line 2424033
    :pswitch_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2424034
    sget-object v0, LX/Cfc;->EVENT_CARD_UNWATCHED_TAP:LX/Cfc;

    move-object v7, v0

    .line 2424035
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V
    .locals 9
    .param p3    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            "TE;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2424007
    sget-object v0, LX/H5H;->a:[I

    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2424008
    invoke-static {p0, p2, p4, p3, p5}, LX/H5I;->a(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/lang/String;)V

    .line 2424009
    :goto_0
    return-void

    .line 2424010
    :pswitch_0
    move-object v2, p3

    check-cast v2, LX/2kn;

    invoke-interface {v2}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v6, "unknown"

    :goto_1
    move-object v2, p3

    .line 2424011
    check-cast v2, LX/2kn;

    invoke-interface {v2}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v7, "unknown"

    :goto_2
    move-object v2, p3

    .line 2424012
    check-cast v2, LX/2kn;

    invoke-interface {v2}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v8, "unknown"

    .line 2424013
    :goto_3
    invoke-virtual {p2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->q()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;)Z

    move-result v2

    .line 2424014
    if-eqz v2, :cond_3

    move-object v2, p0

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    .line 2424015
    invoke-static/range {v2 .. v8}, LX/H5I;->a(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2424016
    :goto_4
    goto :goto_0

    :cond_0
    move-object v2, p3

    .line 2424017
    check-cast v2, LX/2kn;

    invoke-interface {v2}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v2

    iget-object v6, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->a:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v2, p3

    .line 2424018
    check-cast v2, LX/2kn;

    invoke-interface {v2}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v2

    iget-object v7, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->b:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v2, p3

    .line 2424019
    check-cast v2, LX/2kn;

    invoke-interface {v2}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v2

    iget-object v8, v2, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;->c:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v2, p0

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    .line 2424020
    invoke-static/range {v2 .. v8}, LX/H5I;->b(LX/H5I;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
