.class public LX/Gw9;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Gw8;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2406688
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2406689
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;LX/8PB;)LX/Gw8;
    .locals 15

    .prologue
    .line 2406690
    new-instance v0, LX/Gw8;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v6

    check-cast v6, LX/1nC;

    invoke-static {p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/47E;->a(LX/0QB;)LX/47E;

    move-result-object v8

    check-cast v8, LX/47E;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v9

    check-cast v9, LX/74n;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, LX/0TD;

    invoke-static {p0}, LX/BM1;->a(LX/0QB;)LX/BM1;

    move-result-object v11

    check-cast v11, LX/BM1;

    const-class v1, LX/0i4;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/0i4;

    invoke-static {p0}, LX/BKe;->a(LX/0QB;)LX/BKe;

    move-result-object v13

    check-cast v13, LX/BKe;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v14

    check-cast v14, Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v14}, LX/Gw8;-><init>(Landroid/app/Activity;LX/8PB;LX/0aG;LX/0Zb;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/1nC;Ljava/util/concurrent/Executor;LX/47E;LX/74n;LX/0TD;LX/BM1;LX/0i4;LX/BKe;Lcom/facebook/content/SecureContextHelper;)V

    .line 2406691
    return-object v0
.end method
