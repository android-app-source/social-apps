.class public final LX/FHU;
.super LX/FHS;
.source ""


# instance fields
.field public final synthetic b:LX/FHX;

.field public final c:Z


# direct methods
.method public constructor <init>(LX/FHX;Z)V
    .locals 1

    .prologue
    .line 2220355
    iput-object p1, p0, LX/FHU;->b:LX/FHX;

    invoke-direct {p0}, LX/FHS;-><init>()V

    .line 2220356
    iput-boolean p2, p0, LX/FHU;->c:Z

    .line 2220357
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/File;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    .locals 6

    .prologue
    .line 2220358
    iget-object v0, p0, LX/FHU;->b:LX/FHX;

    iget-object v0, v0, LX/FHX;->b:LX/G5l;

    invoke-virtual {v0, p2}, LX/G5l;->a(Ljava/io/File;)LX/G5k;

    move-result-object v0

    .line 2220359
    iget-object v1, p0, LX/FHU;->b:LX/FHX;

    iget-object v2, v1, LX/FHX;->e:LX/1Er;

    const-string v3, "media_upload"

    sget-object v1, LX/5zj;->VIDEO_MMS:LX/5zj;

    iget-object v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    if-ne v1, v4, :cond_2

    const-string v0, ".3gp"

    :goto_0
    iget-object v1, p0, LX/FHU;->b:LX/FHX;

    iget-object v1, v1, LX/FHX;->r:LX/0Uh;

    const/16 v4, 0x268

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_1
    invoke-virtual {v2, v3, v0, v1}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 2220360
    new-instance v3, LX/FHT;

    invoke-direct {v3, p0, p3}, LX/FHT;-><init>(LX/FHU;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2220361
    const/4 v1, 0x0

    .line 2220362
    iget-object v2, p3, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 2220363
    new-instance v1, LX/7SP;

    invoke-direct {v1}, LX/7SP;-><init>()V

    iget-object v2, p0, LX/FHU;->b:LX/FHX;

    iget-object v2, v2, LX/FHX;->q:LX/7SH;

    iget-object v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    invoke-virtual {v2, v4}, LX/7SH;->a(Landroid/net/Uri;)Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2220364
    :cond_0
    iget-object v2, p0, LX/FHU;->b:LX/FHX;

    iget-object v2, v2, LX/FHX;->s:LX/2Mh;

    .line 2220365
    iget-object v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v5, LX/5zj;->VIDEO_MMS:LX/5zj;

    if-ne v4, v5, :cond_4

    .line 2220366
    iget-object v2, p0, LX/FHU;->b:LX/FHX;

    iget-object v2, v2, LX/FHX;->g:LX/2Mc;

    .line 2220367
    :cond_1
    :goto_2
    invoke-static {}, LX/7TH;->newBuilder()LX/7TI;

    move-result-object v4

    .line 2220368
    iput-object p2, v4, LX/7TI;->a:Ljava/io/File;

    .line 2220369
    move-object v4, v4

    .line 2220370
    iput-object v0, v4, LX/7TI;->b:Ljava/io/File;

    .line 2220371
    move-object v4, v4

    .line 2220372
    iput-object v2, v4, LX/7TI;->c:LX/2Md;

    .line 2220373
    move-object v2, v4

    .line 2220374
    iget-object v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->t:Landroid/graphics/RectF;

    .line 2220375
    iput-object v4, v2, LX/7TI;->d:Landroid/graphics/RectF;

    .line 2220376
    move-object v2, v2

    .line 2220377
    iget v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->v:I

    .line 2220378
    iput v4, v2, LX/7TI;->f:I

    .line 2220379
    move-object v2, v2

    .line 2220380
    iget v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->w:I

    .line 2220381
    iput v4, v2, LX/7TI;->g:I

    .line 2220382
    move-object v2, v2

    .line 2220383
    iput-object v3, v2, LX/7TI;->h:LX/60y;

    .line 2220384
    move-object v3, v2

    .line 2220385
    iget-boolean v2, p3, Lcom/facebook/ui/media/attachments/MediaResource;->n:Z

    if-eqz v2, :cond_6

    sget-object v2, LX/7Sv;->MIRROR_HORIZONTALLY:LX/7Sv;

    .line 2220386
    :goto_3
    iput-object v2, v3, LX/7TI;->e:LX/7Sv;

    .line 2220387
    move-object v2, v3

    .line 2220388
    iput-object v1, v2, LX/7TI;->n:LX/0Px;

    .line 2220389
    move-object v1, v2

    .line 2220390
    iget-boolean v2, p0, LX/FHU;->c:Z

    .line 2220391
    iput-boolean v2, v1, LX/7TI;->k:Z

    .line 2220392
    move-object v1, v1

    .line 2220393
    invoke-virtual {v1}, LX/7TI;->o()LX/7TH;

    move-result-object v1

    .line 2220394
    iget-object v2, p0, LX/FHU;->b:LX/FHX;

    iget-object v2, v2, LX/FHX;->c:LX/7TG;

    invoke-virtual {v2, v1}, LX/7TG;->a(LX/7TH;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v1, v1

    .line 2220395
    iput-object v1, p0, LX/FHU;->a:Ljava/util/concurrent/Future;

    .line 2220396
    iget-object v1, p0, LX/FHU;->b:LX/FHX;

    iget-object v1, v1, LX/FHX;->o:LX/FI1;

    iget-object v2, p0, LX/FHS;->a:Ljava/util/concurrent/Future;

    invoke-virtual {v1, p1, v2}, LX/FI1;->a(Ljava/lang/String;Ljava/util/concurrent/Future;)V

    .line 2220397
    :goto_4
    :try_start_0
    iget-object v1, p0, LX/FHS;->a:Ljava/util/concurrent/Future;

    const-wide/16 v2, 0x32

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v5, -0x6aecec3a

    invoke-static {v1, v2, v3, v4, v5}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2220398
    return-object v0

    .line 2220399
    :cond_2
    iget-object v1, p0, LX/FHU;->b:LX/FHX;

    invoke-static {v1, v0}, LX/FHX;->a(LX/FHX;LX/G5k;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    sget-object v1, LX/46h;->PREFER_SDCARD:LX/46h;

    goto/16 :goto_1

    .line 2220400
    :catch_0
    iget-object v1, p0, LX/FHU;->b:LX/FHX;

    iget-object v1, v1, LX/FHX;->o:LX/FI1;

    invoke-virtual {v1, p1}, LX/FI1;->c(Ljava/lang/String;)V

    goto :goto_4

    .line 2220401
    :catch_1
    move-exception v0

    .line 2220402
    new-instance v1, LX/7T9;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, LX/7T9;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2220403
    :catch_2
    move-exception v0

    .line 2220404
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 2220405
    new-instance v1, LX/7T9;

    invoke-direct {v1, v0}, LX/7T9;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2220406
    :cond_4
    iget-object v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v5, LX/5zj;->MONTAGE:LX/5zj;

    if-eq v4, v5, :cond_5

    iget-object v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v5, LX/5zj;->MONTAGE_FRONT:LX/5zj;

    if-eq v4, v5, :cond_5

    iget-object v4, p3, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object v5, LX/5zj;->MONTAGE_BACK:LX/5zj;

    if-ne v4, v5, :cond_7

    :cond_5
    const/4 v4, 0x1

    :goto_5
    move v4, v4

    .line 2220407
    if-eqz v4, :cond_1

    .line 2220408
    iget-object v2, p0, LX/FHU;->b:LX/FHX;

    iget-object v2, v2, LX/FHX;->t:LX/2Me;

    goto/16 :goto_2

    .line 2220409
    :cond_6
    sget-object v2, LX/7Sv;->NONE:LX/7Sv;

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    goto :goto_5
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    .locals 3

    .prologue
    .line 2220410
    iget-object v0, p2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 2220411
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220412
    iget-object v0, p0, LX/FHU;->b:LX/FHX;

    iget-object v0, v0, LX/FHX;->f:LX/2MM;

    iget-object v1, p2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    sget-object v2, LX/46h;->PREFER_SDCARD:LX/46h;

    invoke-virtual {v0, v1, v2}, LX/2MM;->a(Landroid/net/Uri;LX/46h;)LX/46f;

    move-result-object v1

    .line 2220413
    :try_start_0
    iget-object v0, v1, LX/46f;->a:Ljava/io/File;

    invoke-direct {p0, p1, v0, p2}, LX/FHU;->a(Ljava/lang/String;Ljava/io/File;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2220414
    invoke-virtual {v1}, LX/46f;->a()V

    return-object v0

    .line 2220415
    :catch_0
    move-exception v0

    .line 2220416
    :try_start_1
    const-class v2, Ljava/lang/Exception;

    invoke-static {v0, v2}, LX/1Bz;->propagateIfPossible(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 2220417
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2220418
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/46f;->a()V

    throw v0
.end method
