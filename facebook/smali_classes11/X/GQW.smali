.class public LX/GQW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/GQW;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/0Uo;

.field private final d:LX/2Dg;

.field public e:Z


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Uo;LX/2Dg;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/aldrin/status/annotations/IsAldrinEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Uo;",
            "LX/2Dg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2350178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2350179
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GQW;->e:Z

    .line 2350180
    iput-object p1, p0, LX/GQW;->a:LX/0Or;

    .line 2350181
    iput-object p2, p0, LX/GQW;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2350182
    iput-object p3, p0, LX/GQW;->c:LX/0Uo;

    .line 2350183
    iput-object p4, p0, LX/GQW;->d:LX/2Dg;

    .line 2350184
    return-void
.end method

.method public static a(LX/0QB;)LX/GQW;
    .locals 7

    .prologue
    .line 2350185
    sget-object v0, LX/GQW;->f:LX/GQW;

    if-nez v0, :cond_1

    .line 2350186
    const-class v1, LX/GQW;

    monitor-enter v1

    .line 2350187
    :try_start_0
    sget-object v0, LX/GQW;->f:LX/GQW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2350188
    if-eqz v2, :cond_0

    .line 2350189
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2350190
    new-instance v6, LX/GQW;

    const/16 v3, 0x1439

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {v0}, LX/2Dg;->a(LX/0QB;)LX/2Dg;

    move-result-object v5

    check-cast v5, LX/2Dg;

    invoke-direct {v6, p0, v3, v4, v5}, LX/GQW;-><init>(LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Uo;LX/2Dg;)V

    .line 2350191
    move-object v0, v6

    .line 2350192
    sput-object v0, LX/GQW;->f:LX/GQW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2350193
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2350194
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2350195
    :cond_1
    sget-object v0, LX/GQW;->f:LX/GQW;

    return-object v0

    .line 2350196
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2350197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2350198
    iget-boolean v0, p0, LX/GQW;->e:Z

    if-eqz v0, :cond_1

    .line 2350199
    :cond_0
    :goto_0
    return-void

    .line 2350200
    :cond_1
    iget-object v0, p0, LX/GQW;->c:LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2350201
    invoke-virtual {p0}, LX/GQW;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2350202
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/aldrin/transition/activity/AldrinTransitionActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2350203
    instance-of v1, p1, Landroid/app/Activity;

    if-nez v1, :cond_2

    .line 2350204
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2350205
    :cond_2
    iget-object v1, p0, LX/GQW;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2350206
    iget-object v0, p0, LX/GQW;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2350207
    :goto_0
    return v0

    .line 2350208
    :cond_0
    iget-object v0, p0, LX/GQW;->d:LX/2Dg;

    invoke-virtual {v0}, LX/2Dg;->d()Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v0

    .line 2350209
    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    .line 2350210
    goto :goto_0

    .line 2350211
    :cond_2
    sget-object v2, LX/GQV;->a:[I

    iget-object v0, v0, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 2350212
    goto :goto_0

    .line 2350213
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
