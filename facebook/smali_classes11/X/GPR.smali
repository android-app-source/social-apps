.class public final enum LX/GPR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GPR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GPR;

.field public static final enum CNPJ:LX/GPR;

.field public static final enum CPF:LX/GPR;

.field private static final REGEX:Ljava/util/regex/Pattern;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2348788
    new-instance v0, LX/GPR;

    const-string v1, "CNPJ"

    invoke-direct {v0, v1, v2}, LX/GPR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPR;->CNPJ:LX/GPR;

    new-instance v0, LX/GPR;

    const-string v1, "CPF"

    invoke-direct {v0, v1, v3}, LX/GPR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GPR;->CPF:LX/GPR;

    .line 2348789
    const/4 v0, 0x2

    new-array v0, v0, [LX/GPR;

    sget-object v1, LX/GPR;->CNPJ:LX/GPR;

    aput-object v1, v0, v2

    sget-object v1, LX/GPR;->CPF:LX/GPR;

    aput-object v1, v0, v3

    sput-object v0, LX/GPR;->$VALUES:[LX/GPR;

    .line 2348790
    const-string v0, "[0-9]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/GPR;->REGEX:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2348791
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static ofTaxId(Ljava/lang/String;)LX/GPR;
    .locals 4

    .prologue
    .line 2348792
    invoke-static {p0}, LX/GPR;->ofTaxIdOrNull(Ljava/lang/String;)LX/GPR;

    move-result-object v0

    .line 2348793
    invoke-static {}, LX/0Rj;->notNull()LX/0Rl;

    move-result-object v1

    const-string v2, "Brazilian Tax Id can only have 11 or 14 digits, with \'-\', \'.\', and \'/\'"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/GPq;->a(Ljava/lang/Object;LX/0Rl;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GPR;

    return-object v0
.end method

.method public static ofTaxIdOrNull(Ljava/lang/String;)LX/GPR;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2348794
    const-string v1, "[\\-\\./]"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2348795
    sget-object v2, LX/GPR;->REGEX:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2348796
    :goto_0
    return-object v0

    .line 2348797
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2348798
    :pswitch_1
    sget-object v0, LX/GPR;->CPF:LX/GPR;

    goto :goto_0

    .line 2348799
    :pswitch_2
    sget-object v0, LX/GPR;->CNPJ:LX/GPR;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/GPR;
    .locals 1

    .prologue
    .line 2348800
    const-class v0, LX/GPR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GPR;

    return-object v0
.end method

.method public static values()[LX/GPR;
    .locals 1

    .prologue
    .line 2348801
    sget-object v0, LX/GPR;->$VALUES:[LX/GPR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GPR;

    return-object v0
.end method
