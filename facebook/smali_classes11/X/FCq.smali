.class public LX/FCq;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2210689
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2210690
    const v0, 0x7f030c87

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2210691
    const v0, 0x7f0d1ecd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/FCq;->a:Landroid/widget/ImageView;

    .line 2210692
    const v0, 0x7f0d1ece

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/FCq;->b:Landroid/view/View;

    .line 2210693
    const v0, 0x7f0d1ecf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/FCq;->c:Landroid/widget/TextView;

    .line 2210694
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FCq;->f:Z

    .line 2210695
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2210681
    iget-object v0, p0, LX/FCq;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2210682
    iget-boolean v1, p0, LX/FCq;->g:Z

    if-eqz v1, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v1, :cond_0

    .line 2210683
    iget-object v1, p0, LX/FCq;->a:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2210684
    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 2210685
    :goto_0
    return-void

    .line 2210686
    :cond_0
    instance-of v1, v0, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v1, :cond_1

    .line 2210687
    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 2210688
    :cond_1
    iget-object v1, p0, LX/FCq;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, LX/FCq;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FCq;->e:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/FCq;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method private static a(LX/FCp;)Z
    .locals 1

    .prologue
    .line 2210680
    sget-object v0, LX/FCp;->OTHER_HIGHLIGHTED:LX/FCp;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/FCp;->OTHER_NORMAL:LX/FCp;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setColor(I)V
    .locals 1

    .prologue
    .line 2210696
    iget-object v0, p0, LX/FCq;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2210697
    invoke-virtual {p0, p1}, LX/FCq;->setBackgroundColor(I)V

    .line 2210698
    return-void
.end method

.method public setIsLoading(Z)V
    .locals 0

    .prologue
    .line 2210677
    iput-boolean p1, p0, LX/FCq;->g:Z

    .line 2210678
    invoke-direct {p0}, LX/FCq;->a()V

    .line 2210679
    return-void
.end method

.method public setIsPlaying(Z)V
    .locals 0

    .prologue
    .line 2210674
    iput-boolean p1, p0, LX/FCq;->f:Z

    .line 2210675
    invoke-direct {p0}, LX/FCq;->a()V

    .line 2210676
    return-void
.end method

.method public setTimerDuration(J)V
    .locals 5

    .prologue
    .line 2210666
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 2210667
    invoke-virtual {p0}, LX/FCq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08047a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2210668
    iget-object v1, p0, LX/FCq;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2210669
    :goto_0
    return-void

    .line 2210670
    :cond_0
    long-to-float v0, p1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 2210671
    div-int/lit8 v1, v0, 0x3c

    .line 2210672
    rem-int/lit8 v0, v0, 0x3c

    .line 2210673
    iget-object v2, p0, LX/FCq;->c:Landroid/widget/TextView;

    const-string v3, "%d:%02d"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v3, v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setType(LX/FCp;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const v8, 0x3e4ccccd    # 0.2f

    .line 2210618
    invoke-virtual {p0}, LX/FCq;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2210619
    sget-object v1, LX/FCo;->a:[I

    invoke-virtual {p1}, LX/FCp;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2210620
    :cond_0
    :goto_0
    return-void

    .line 2210621
    :pswitch_0
    iget-object v1, p0, LX/FCq;->a:Landroid/widget/ImageView;

    const v2, 0x7f01056a

    const v3, 0x7f0211aa

    invoke-static {v0, v2, v3}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2210622
    const v1, 0x7f01056b

    const v2, 0x7f02134e

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    .line 2210623
    const v1, 0x7f01056f

    const v2, 0x7f021358    # 1.7290008E38f

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    .line 2210624
    const v1, 0x7f01057d

    const v2, 0x7f021370

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v3

    .line 2210625
    const v1, 0x7f010581

    const v2, 0x7f021b14

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v2

    .line 2210626
    const v1, 0x7f010579

    const v6, 0x7f021368

    invoke-static {v0, v1, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    .line 2210627
    const v6, 0x7f010575

    const v7, 0x7f021360

    invoke-static {v0, v6, v7}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    move v10, v0

    move v0, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v10

    .line 2210628
    :goto_1
    invoke-virtual {p0}, LX/FCq;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b07bc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2210629
    invoke-virtual {p0, v6, v9, v6, v9}, LX/FCq;->setPadding(IIII)V

    .line 2210630
    invoke-virtual {p0, v5}, LX/FCq;->setBackgroundResource(I)V

    .line 2210631
    invoke-virtual {p0}, LX/FCq;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 2210632
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 2210633
    sget-object v5, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    .line 2210634
    iget-object v5, p0, LX/FCq;->b:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2210635
    iget-object v0, p0, LX/FCq;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/FCq;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2210636
    iget-object v0, p0, LX/FCq;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/FCq;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2210637
    invoke-virtual {p0}, LX/FCq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/FCq;->d:Landroid/graphics/drawable/Drawable;

    .line 2210638
    invoke-virtual {p0}, LX/FCq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/FCq;->e:Landroid/graphics/drawable/Drawable;

    .line 2210639
    invoke-direct {p0}, LX/FCq;->a()V

    .line 2210640
    invoke-static {p1}, LX/FCq;->a(LX/FCp;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2210641
    iget-object v0, p0, LX/FCq;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2210642
    iget-object v0, p0, LX/FCq;->b:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setAlpha(F)V

    .line 2210643
    iget-object v0, p0, LX/FCq;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setAlpha(F)V

    goto/16 :goto_0

    .line 2210644
    :pswitch_1
    iget-object v1, p0, LX/FCq;->a:Landroid/widget/ImageView;

    const v2, 0x7f010569

    const v3, 0x7f0211a8

    invoke-static {v0, v2, v3}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2210645
    const v1, 0x7f01056d

    const v2, 0x7f021347

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    .line 2210646
    const v1, 0x7f010571

    const v2, 0x7f021354    # 1.729E38f

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    .line 2210647
    const v1, 0x7f01057b

    const v2, 0x7f02136c

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v3

    .line 2210648
    const v1, 0x7f01057f

    const v2, 0x7f021b12

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v2

    .line 2210649
    const v1, 0x7f010577

    const v6, 0x7f021364

    invoke-static {v0, v1, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    .line 2210650
    const v6, 0x7f010573

    const v7, 0x7f02135c

    invoke-static {v0, v6, v7}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    move v10, v0

    move v0, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v10

    .line 2210651
    goto/16 :goto_1

    .line 2210652
    :pswitch_2
    const v1, 0x7f01056c

    const v2, 0x7f021351    # 1.7289994E38f

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    .line 2210653
    const v1, 0x7f010570

    const v2, 0x7f021359    # 1.729001E38f

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    .line 2210654
    const v1, 0x7f01057e

    const v2, 0x7f021371

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v3

    .line 2210655
    const v1, 0x7f010582

    const v2, 0x7f021b15

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v2

    .line 2210656
    const v1, 0x7f01057a

    const v6, 0x7f021369

    invoke-static {v0, v1, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    .line 2210657
    const v6, 0x7f010576

    const v7, 0x7f021361

    invoke-static {v0, v6, v7}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    move v10, v0

    move v0, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v10

    .line 2210658
    goto/16 :goto_1

    .line 2210659
    :pswitch_3
    const v1, 0x7f01056e

    const v2, 0x7f02134a

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v5

    .line 2210660
    const v1, 0x7f010572

    const v2, 0x7f021355    # 1.7290002E38f

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v4

    .line 2210661
    const v1, 0x7f01057c

    const v2, 0x7f02136d

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v3

    .line 2210662
    const v1, 0x7f010580

    const v2, 0x7f021b13

    invoke-static {v0, v1, v2}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v2

    .line 2210663
    const v1, 0x7f010578

    const v6, 0x7f021365

    invoke-static {v0, v1, v6}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v1

    .line 2210664
    const v6, 0x7f010574

    const v7, 0x7f02135d

    invoke-static {v0, v6, v7}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v0

    move v10, v0

    move v0, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v10

    .line 2210665
    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
