.class public final LX/GZx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 2367703
    const/4 v13, 0x0

    .line 2367704
    const/4 v12, 0x0

    .line 2367705
    const/4 v11, 0x0

    .line 2367706
    const/4 v10, 0x0

    .line 2367707
    const/4 v9, 0x0

    .line 2367708
    const/4 v8, 0x0

    .line 2367709
    const/4 v7, 0x0

    .line 2367710
    const/4 v6, 0x0

    .line 2367711
    const/4 v5, 0x0

    .line 2367712
    const/4 v4, 0x0

    .line 2367713
    const/4 v3, 0x0

    .line 2367714
    const/4 v2, 0x0

    .line 2367715
    const/4 v1, 0x0

    .line 2367716
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 2367717
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2367718
    const/4 v1, 0x0

    .line 2367719
    :goto_0
    return v1

    .line 2367720
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2367721
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_d

    .line 2367722
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 2367723
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2367724
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 2367725
    const-string v15, "__type__"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    const-string v15, "__typename"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 2367726
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v13

    goto :goto_1

    .line 2367727
    :cond_3
    const-string v15, "can_viewer_like"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 2367728
    const/4 v2, 0x1

    .line 2367729
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 2367730
    :cond_4
    const-string v15, "category_names"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 2367731
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 2367732
    :cond_5
    const-string v15, "category_type"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 2367733
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 2367734
    :cond_6
    const-string v15, "commerce_store"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 2367735
    invoke-static/range {p0 .. p1}, LX/7iq;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2367736
    :cond_7
    const-string v15, "does_viewer_like"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 2367737
    const/4 v1, 0x1

    .line 2367738
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 2367739
    :cond_8
    const-string v15, "id"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 2367740
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 2367741
    :cond_9
    const-string v15, "name"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 2367742
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 2367743
    :cond_a
    const-string v15, "page_likers"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 2367744
    invoke-static/range {p0 .. p1}, LX/7ii;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 2367745
    :cond_b
    const-string v15, "profile_picture"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 2367746
    invoke-static/range {p0 .. p1}, LX/7ij;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2367747
    :cond_c
    const-string v15, "viewer_profile_permissions"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 2367748
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2367749
    :cond_d
    const/16 v14, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 2367750
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 2367751
    if-eqz v2, :cond_e

    .line 2367752
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 2367753
    :cond_e
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2367754
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2367755
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2367756
    if-eqz v1, :cond_f

    .line 2367757
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 2367758
    :cond_f
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2367759
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2367760
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2367761
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2367762
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2367763
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
