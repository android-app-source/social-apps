.class public LX/HA4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8L;


# instance fields
.field public final a:LX/HDT;

.field public final b:LX/HAF;

.field public final c:LX/H9D;

.field private final d:LX/1ZF;

.field public final e:Landroid/content/Context;

.field public f:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/H8Z;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/HDT;LX/HAF;LX/H9E;LX/1ZF;Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p4    # LX/1ZF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434857
    iput-object p2, p0, LX/HA4;->b:LX/HAF;

    .line 2434858
    iput-object p1, p0, LX/HA4;->a:LX/HDT;

    .line 2434859
    invoke-virtual {p3, p6}, LX/H9E;->a(Landroid/view/View;)LX/H9D;

    move-result-object v0

    iput-object v0, p0, LX/HA4;->c:LX/H9D;

    .line 2434860
    iput-object p4, p0, LX/HA4;->d:LX/1ZF;

    .line 2434861
    iput-object p5, p0, LX/HA4;->e:Landroid/content/Context;

    .line 2434862
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/HA4;->f:Ljava/util/LinkedHashMap;

    .line 2434863
    invoke-static {p0}, LX/HAF;->a(LX/H8L;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/HA4;->g:LX/0Px;

    .line 2434864
    iget-object v0, p0, LX/HA4;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p2

    const/4 v0, 0x0

    move p1, v0

    :goto_0
    if-ge p1, p2, :cond_0

    iget-object v0, p0, LX/HA4;->g:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8E;

    .line 2434865
    iget-object p3, p0, LX/HA4;->a:LX/HDT;

    invoke-virtual {p3, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2434866
    add-int/lit8 v0, p1, 0x1

    move p1, v0

    goto :goto_0

    .line 2434867
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2434868
    iget-object v0, p0, LX/HA4;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2434869
    iget-object v0, p0, LX/HA4;->d:LX/1ZF;

    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2434870
    :goto_0
    return-void

    .line 2434871
    :cond_0
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f020722

    .line 2434872
    iput v1, v0, LX/108;->i:I

    .line 2434873
    move-object v0, v0

    .line 2434874
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2434875
    iget-object v1, p0, LX/HA4;->d:LX/1ZF;

    invoke-interface {v1, v0}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2434876
    iget-object v0, p0, LX/HA4;->d:LX/1ZF;

    new-instance v1, LX/HA2;

    invoke-direct {v1, p0}, LX/HA2;-><init>(LX/HA4;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_0
.end method

.method public final getSupportedActionTypes()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434877
    sget-object v0, LX/H9D;->a:LX/0Px;

    move-object v0, v0

    .line 2434878
    return-object v0
.end method
