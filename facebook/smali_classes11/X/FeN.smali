.class public LX/FeN;
.super LX/FeM;
.source ""


# instance fields
.field private final d:LX/0wM;


# direct methods
.method public constructor <init>(LX/0gc;LX/7BH;Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;LX/FZb;LX/CyE;LX/0Px;Landroid/content/Context;LX/0hL;LX/Fff;LX/0wM;)V
    .locals 0
    .param p1    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/7BH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/search/model/KeywordTypeaheadUnit;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/search/logging/api/SearchTypeaheadSession;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/8ci;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/FZb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/CyE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "LX/7BH;",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            "Lcom/facebook/search/logging/api/SearchTypeaheadSession;",
            "LX/8ci;",
            "Lcom/facebook/search/fragment/GraphSearchChildFragment$OnResultClickListener;",
            "LX/CyE;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;",
            "Landroid/content/Context;",
            "LX/0hL;",
            "LX/Fff;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2265752
    invoke-direct/range {p0 .. p12}, LX/FeM;-><init>(LX/0gc;LX/7BH;Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;LX/FZb;LX/CyE;LX/0Px;Landroid/content/Context;LX/0hL;LX/Fff;LX/0wM;)V

    .line 2265753
    iput-object p12, p0, LX/FeN;->d:LX/0wM;

    .line 2265754
    return-void
.end method


# virtual methods
.method public final A_(I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2265755
    iget-object v0, p0, LX/FeM;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2265756
    iget-object v1, p0, LX/FeM;->b:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2265757
    iget-object v1, p0, LX/FeN;->d:LX/0wM;

    iget-object v2, p0, LX/FeM;->b:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, LX/FeM;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0097

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2265758
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final G_(I)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2265759
    new-instance v0, Landroid/text/SpannableStringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  \n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0, p1}, LX/FeM;->G_(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2265760
    invoke-virtual {p0, p1}, LX/FeM;->e(I)I

    move-result v1

    .line 2265761
    invoke-virtual {p0, v1}, LX/FeM;->A_(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2265762
    if-eqz v1, :cond_0

    .line 2265763
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2265764
    new-instance v2, Landroid/text/style/ImageSpan;

    invoke-direct {v2, v1, v5}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 2265765
    const/16 v1, 0x21

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2265766
    :cond_0
    return-object v0
.end method

.method public final k_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2265767
    invoke-virtual {p0, p1}, LX/FeM;->e(I)I

    move-result v0

    .line 2265768
    iget-object v1, p0, LX/FeM;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->c()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
