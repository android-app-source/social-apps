.class public final LX/F9X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/F9Y;


# direct methods
.method public constructor <init>(LX/F9Y;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2205525
    iput-object p1, p0, LX/F9X;->b:LX/F9Y;

    iput-object p2, p0, LX/F9X;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2205526
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, LX/F9X;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/growth/nux/UserAccountNUXActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2205527
    const-string v2, "show_single_step"

    iget-object v0, p0, LX/F9X;->b:LX/F9Y;

    invoke-static {v0}, LX/F9Y;->b(LX/F9Y;)LX/0Px;

    move-result-object v0

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2205528
    iget-object v0, p0, LX/F9X;->b:LX/F9Y;

    iget-object v0, v0, LX/F9Y;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/F9X;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2205529
    const/4 v0, 0x1

    return v0
.end method
