.class public final LX/G0E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2308661
    iput-object p1, p0, LX/G0E;->c:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    iput-object p2, p0, LX/G0E;->a:Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    iput-object p3, p0, LX/G0E;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0xd9a01b6

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2308662
    iget-object v0, p0, LX/G0E;->c:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    iget-object v2, v0, Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/G0E;->c:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;->x:LX/G0B;

    iget-object v3, p0, LX/G0E;->a:Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    iget-object v4, p0, LX/G0E;->c:Lcom/facebook/timeline/legacycontact/MemorialProfilePictureActivity;

    iget-object v4, v4, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    sget-object v5, LX/8AB;->PROFILEPIC:LX/8AB;

    invoke-virtual {v0, v3, v4, v5}, LX/G0B;->a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;Ljava/lang/String;LX/8AB;)Landroid/content/Intent;

    move-result-object v3

    const/16 v4, 0x7c8

    iget-object v0, p0, LX/G0E;->b:Landroid/content/Context;

    const-class v5, Landroid/app/Activity;

    invoke-static {v0, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2308663
    const v0, -0x6ab928b2

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
