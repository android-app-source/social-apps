.class public final LX/FzX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2308133
    iput-object p1, p0, LX/FzX;->b:Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;

    iput-object p2, p0, LX/FzX;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x2

    const v0, 0x7aa03e32

    invoke-static {v6, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2308134
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2308135
    sget-object v2, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->v:Ljava/lang/String;

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/FzX;->b:Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;

    iget-object v5, v5, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2308136
    iget-object v2, p0, LX/FzX;->b:Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;

    iget-object v2, v2, Lcom/facebook/timeline/legacycontact/BeingLegacyContactActivity;->s:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/FzX;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2308137
    const v1, -0x39bbfe18

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
