.class public LX/FMu;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static volatile a:LX/Ed0;

.field private static final f:Landroid/os/Bundle;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/2Or;

.field public final e:Landroid/telephony/TelephonyManager;

.field public g:LX/0W3;

.field private final h:LX/FMl;

.field private final i:LX/2Uq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2231430
    const/4 v0, 0x0

    sput-object v0, LX/FMu;->a:LX/Ed0;

    .line 2231431
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, LX/FMu;->f:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Or;Landroid/telephony/TelephonyManager;LX/0W3;LX/FMl;LX/2Uq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231435
    iput-object p1, p0, LX/FMu;->b:Landroid/content/Context;

    .line 2231436
    iput-object p2, p0, LX/FMu;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2231437
    iput-object p3, p0, LX/FMu;->d:LX/2Or;

    .line 2231438
    iput-object p4, p0, LX/FMu;->e:Landroid/telephony/TelephonyManager;

    .line 2231439
    iput-object p5, p0, LX/FMu;->g:LX/0W3;

    .line 2231440
    iput-object p6, p0, LX/FMu;->h:LX/FMl;

    .line 2231441
    iput-object p7, p0, LX/FMu;->i:LX/2Uq;

    .line 2231442
    return-void
.end method

.method public static b(LX/0QB;)LX/FMu;
    .locals 8

    .prologue
    .line 2231432
    new-instance v0, LX/FMu;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/2Or;->b(LX/0QB;)LX/2Or;

    move-result-object v3

    check-cast v3, LX/2Or;

    invoke-static {p0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static {p0}, LX/FMl;->a(LX/0QB;)LX/FMl;

    move-result-object v6

    check-cast v6, LX/FMl;

    invoke-static {p0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v7

    check-cast v7, LX/2Uq;

    invoke-direct/range {v0 .. v7}, LX/FMu;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Or;Landroid/telephony/TelephonyManager;LX/0W3;LX/FMl;LX/2Uq;)V

    .line 2231433
    return-object v0
.end method

.method public static c(LX/FMu;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2231424
    const/4 v0, -0x1

    invoke-static {p0, v0}, LX/FMu;->h(LX/FMu;I)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/FMu;I)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 2231425
    sget-object v0, LX/FMu;->a:LX/Ed0;

    if-nez v0, :cond_0

    .line 2231426
    new-instance v0, LX/Ed7;

    iget-object v1, p0, LX/FMu;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Ed7;-><init>(Landroid/content/Context;)V

    sput-object v0, LX/FMu;->a:LX/Ed0;

    .line 2231427
    :cond_0
    iget-object v0, p0, LX/FMu;->h:LX/FMl;

    invoke-virtual {v0}, LX/FMl;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2231428
    sget-object v0, LX/FMu;->a:LX/Ed0;

    invoke-interface {v0, p1}, LX/Ed0;->a(I)Landroid/os/Bundle;

    move-result-object v0

    .line 2231429
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/FMu;->f:Landroid/os/Bundle;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Z
    .locals 3

    .prologue
    .line 2231421
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2231422
    invoke-static {p0, p1}, LX/FMu;->h(LX/FMu;I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "supportMmsContentDisposition"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2231423
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)I
    .locals 4

    .prologue
    .line 2231417
    iget-object v0, p0, LX/FMu;->g:LX/0W3;

    sget-wide v2, LX/0X5;->hq:J

    const v1, 0x4b000

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    .line 2231418
    iget-object v1, p0, LX/FMu;->i:LX/2Uq;

    invoke-static {p0, p1}, LX/FMu;->h(LX/FMu;I)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "maxMessageSize"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2231419
    iget-object v2, v1, LX/2Uq;->a:LX/0ad;

    sget v3, LX/6jD;->G:I

    invoke-interface {v2, v3, v0}, LX/0ad;->a(II)I

    move-result v2

    move v0, v2

    .line 2231420
    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2231416
    invoke-static {p0}, LX/FMu;->c(LX/FMu;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "enabledNotifyWapMMSC"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final g(I)Z
    .locals 3

    .prologue
    .line 2231415
    invoke-static {p0, p1}, LX/FMu;->h(LX/FMu;I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "enableMultipartSMS"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
