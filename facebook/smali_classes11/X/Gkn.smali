.class public final enum LX/Gkn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gkn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gkn;

.field public static final enum FAVORITES_SECTION:LX/Gkn;

.field public static final enum FILTERED_GROUPS_SECTION:LX/Gkn;

.field public static final enum HIDDEN_GROUPS_SECTION:LX/Gkn;

.field public static final enum RECENTLY_JOINED_SECTION:LX/Gkn;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2389480
    new-instance v0, LX/Gkn;

    const-string v1, "FAVORITES_SECTION"

    invoke-direct {v0, v1, v2}, LX/Gkn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    .line 2389481
    new-instance v0, LX/Gkn;

    const-string v1, "RECENTLY_JOINED_SECTION"

    invoke-direct {v0, v1, v3}, LX/Gkn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    .line 2389482
    new-instance v0, LX/Gkn;

    const-string v1, "FILTERED_GROUPS_SECTION"

    invoke-direct {v0, v1, v4}, LX/Gkn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    .line 2389483
    new-instance v0, LX/Gkn;

    const-string v1, "HIDDEN_GROUPS_SECTION"

    invoke-direct {v0, v1, v5}, LX/Gkn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    .line 2389484
    const/4 v0, 0x4

    new-array v0, v0, [LX/Gkn;

    sget-object v1, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    aput-object v1, v0, v2

    sget-object v1, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    aput-object v1, v0, v3

    sget-object v1, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    aput-object v1, v0, v4

    sget-object v1, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    aput-object v1, v0, v5

    sput-object v0, LX/Gkn;->$VALUES:[LX/Gkn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2389485
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gkn;
    .locals 1

    .prologue
    .line 2389486
    const-class v0, LX/Gkn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gkn;

    return-object v0
.end method

.method public static values()[LX/Gkn;
    .locals 1

    .prologue
    .line 2389487
    sget-object v0, LX/Gkn;->$VALUES:[LX/Gkn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gkn;

    return-object v0
.end method
