.class public final LX/GOe;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "Lcom/facebook/common/util/ParcelablePair",
        "<",
        "Ljava/lang/String;",
        "Landroid/net/Uri;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/model/PaymentOption;

.field public final synthetic b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V
    .locals 0

    .prologue
    .line 2347691
    iput-object p1, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    iput-object p2, p0, LX/GOe;->a:Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a(Lcom/facebook/common/util/ParcelablePair;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/util/ParcelablePair",
            "<",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2347692
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 2347693
    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/net/Uri;

    .line 2347694
    iget-object v2, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    iget-object v2, v2, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->K:LX/70D;

    invoke-virtual {v2}, LX/0QG;->a()V

    .line 2347695
    iget-object v2, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    iget-object v2, v2, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    iget-object v3, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    const-string v4, "payments_prepay_fund_success"

    invoke-virtual {v3, v4}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->h(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->q(Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v3

    iget-object v4, p0, LX/GOe;->a:Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    invoke-virtual {v3, v4}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2347696
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    iget-object v2, v2, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->z:LX/GNs;

    sget-short v3, LX/GOz;->a:S

    invoke-virtual {v2, v3}, LX/GNs;->a(S)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2347697
    iget-object v2, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    .line 2347698
    invoke-static {v2, v1, v0}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a$redex0(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Landroid/net/Uri;Ljava/lang/String;)V

    .line 2347699
    :goto_0
    return-void

    .line 2347700
    :cond_0
    iget-object v1, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    const v2, 0x7f082803

    const v3, 0x7f082804

    const v4, 0x7f080016

    new-instance v5, LX/GOd;

    invoke-direct {v5, p0, v0}, LX/GOd;-><init>(LX/GOe;Ljava/lang/String;)V

    invoke-static {v1, v2, v3, v4, v5}, LX/Gza;->a(Landroid/content/Context;IIILandroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v1

    .line 2347701
    invoke-virtual {v1, v6}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 2347702
    invoke-virtual {v1, v6}, LX/2EJ;->setCancelable(Z)V

    .line 2347703
    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2347704
    iget-object v1, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    const-string v2, "funding_success_dialog"

    const-string v3, "payment_id"

    invoke-static {v3, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2347705
    invoke-super {p0, p1}, LX/GNw;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2347706
    iget-object v0, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-static {v0, p1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2347707
    iget-object v0, p0, LX/GOe;->b:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->b(Z)V

    .line 2347708
    :cond_0
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2347709
    check-cast p1, Lcom/facebook/common/util/ParcelablePair;

    invoke-direct {p0, p1}, LX/GOe;->a(Lcom/facebook/common/util/ParcelablePair;)V

    return-void
.end method
