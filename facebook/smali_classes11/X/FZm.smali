.class public LX/FZm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/01T;

.field private final b:LX/1nD;

.field private final c:LX/Cvs;


# direct methods
.method public constructor <init>(LX/01T;LX/1nD;LX/Cvs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2258070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258071
    iput-object p1, p0, LX/FZm;->a:LX/01T;

    .line 2258072
    iput-object p2, p0, LX/FZm;->b:LX/1nD;

    .line 2258073
    iput-object p3, p0, LX/FZm;->c:LX/Cvs;

    .line 2258074
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 2258057
    iget-object v0, p0, LX/FZm;->a:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_0

    .line 2258058
    iget-object v0, p0, LX/FZm;->c:LX/Cvs;

    invoke-interface {v0}, LX/Cvs;->c()V

    .line 2258059
    :cond_0
    iget-object v0, p0, LX/FZm;->a:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;-><init>()V

    .line 2258060
    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2258061
    if-eqz v1, :cond_3

    const-string v2, "hashtag_feed_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    const-string v2, "hashtag_feed_title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 2258062
    if-eqz v2, :cond_2

    .line 2258063
    iget-object v2, p0, LX/FZm;->b:LX/1nD;

    .line 2258064
    sget-object v3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    const-string v4, "hashtag_feed_id"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string p0, "hashtag_feed_title"

    invoke-virtual {v1, p0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    sget-object p1, LX/8ci;->o:LX/8ci;

    invoke-virtual {v2, v3, v4, p0, p1}, LX/1nD;->a(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;

    move-result-object v3

    .line 2258065
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    move-object v1, v3

    .line 2258066
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2258067
    :goto_2
    return-object v0

    .line 2258068
    :cond_1
    new-instance v0, Lcom/facebook/search/fragment/GraphSearchFragment;

    invoke-direct {v0}, Lcom/facebook/search/fragment/GraphSearchFragment;-><init>()V

    goto :goto_0

    .line 2258069
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method
