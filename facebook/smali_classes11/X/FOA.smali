.class public final LX/FOA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/messaging/model/threads/ThreadSummary;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3NC;


# direct methods
.method public constructor <init>(LX/3NC;)V
    .locals 0

    .prologue
    .line 2234373
    iput-object p1, p0, LX/FOA;->a:LX/3NC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2234366
    check-cast p1, Lcom/facebook/messaging/model/threads/ThreadSummary;

    check-cast p2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2234367
    iget-wide v0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2234368
    const/4 v0, -0x1

    .line 2234369
    :goto_0
    return v0

    .line 2234370
    :cond_0
    iget-wide v0, p2, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2234371
    const/4 v0, 0x0

    goto :goto_0

    .line 2234372
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
