.class public LX/FtU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1DS;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/FtF;


# direct methods
.method public constructor <init>(LX/1DS;LX/0Ot;LX/FtF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/parts/FavoriteMediaPickerRootGroupPartDefinition;",
            ">;",
            "LX/FtF;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2298788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2298789
    iput-object p1, p0, LX/FtU;->a:LX/1DS;

    .line 2298790
    iput-object p2, p0, LX/FtU;->b:LX/0Ot;

    .line 2298791
    iput-object p3, p0, LX/FtU;->c:LX/FtF;

    .line 2298792
    return-void
.end method

.method public static a(LX/0QB;)LX/FtU;
    .locals 6

    .prologue
    .line 2298793
    const-class v1, LX/FtU;

    monitor-enter v1

    .line 2298794
    :try_start_0
    sget-object v0, LX/FtU;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2298795
    sput-object v2, LX/FtU;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2298796
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2298797
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2298798
    new-instance v5, LX/FtU;

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v3

    check-cast v3, LX/1DS;

    const/16 v4, 0x364d

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/FtF;->a(LX/0QB;)LX/FtF;

    move-result-object v4

    check-cast v4, LX/FtF;

    invoke-direct {v5, v3, p0, v4}, LX/FtU;-><init>(LX/1DS;LX/0Ot;LX/FtF;)V

    .line 2298799
    move-object v0, v5

    .line 2298800
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2298801
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FtU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2298802
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2298803
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
