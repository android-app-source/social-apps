.class public final LX/Feg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V
    .locals 0

    .prologue
    .line 2266098
    iput-object p1, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 2

    .prologue
    .line 2266072
    if-nez p2, :cond_0

    .line 2266073
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->X:LX/CvM;

    invoke-virtual {v0}, LX/CvM;->d()V

    .line 2266074
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C:LX/1k3;

    iget-object v1, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-virtual {v0, v1}, LX/1Kt;->b(LX/0g8;)V

    .line 2266075
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ax:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 2266076
    :goto_0
    return-void

    .line 2266077
    :cond_0
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ax:LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 4

    .prologue
    .line 2266078
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->C:LX/1k3;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1Kt;->a(LX/0g8;III)V

    .line 2266079
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2266080
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    if-eqz v1, :cond_0

    if-lez p3, :cond_0

    if-gtz p4, :cond_4

    :cond_0
    move v1, v3

    .line 2266081
    :goto_0
    move v0, v1

    .line 2266082
    if-nez v0, :cond_2

    .line 2266083
    :cond_1
    :goto_1
    return-void

    .line 2266084
    :cond_2
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-boolean v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aA:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->I:LX/0Uh;

    sget v1, LX/2SU;->ac:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2266085
    :cond_3
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    invoke-virtual {v0}, LX/FgF;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2266086
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->m:LX/Cve;

    iget-object v1, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2266087
    iget-object v2, v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2266088
    move-object v1, v2

    .line 2266089
    invoke-virtual {v0, v1}, LX/Cve;->a(LX/CwB;)V

    .line 2266090
    iget-object v0, p0, LX/Feg;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    sget-object v1, LX/Fg8;->ON_SCROLL:LX/Fg8;

    invoke-static {v0, v1}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a$redex0(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/Fg8;)V

    goto :goto_1

    .line 2266091
    :cond_4
    add-int p1, p2, p3

    .line 2266092
    iget-boolean v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aB:Z

    if-eqz v1, :cond_6

    .line 2266093
    add-int/lit8 v1, p4, -0x1

    if-lt p1, v1, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_0

    .line 2266094
    :cond_6
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CyY;

    invoke-virtual {v1}, LX/CyY;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2266095
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->Y:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CyY;

    invoke-virtual {v1, p1, p3, p4}, LX/CyY;->a(III)Z

    move-result v1

    goto :goto_0

    .line 2266096
    :cond_7
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ag:LX/1Qq;

    invoke-interface {v1, p1}, LX/1Qr;->h_(I)I

    move-result v1

    .line 2266097
    add-int/lit8 v1, v1, 0xa

    iget-object p1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {p1}, LX/CzE;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    if-lt v1, p1, :cond_8

    move v1, v2

    goto :goto_0

    :cond_8
    move v1, v3

    goto :goto_0
.end method
