.class public LX/Fff;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Fff;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268745
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v1, 0x7f08231b

    invoke-direct {p0, p1, v0, v1}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268746
    iput-object p2, p0, LX/Fff;->a:LX/0ad;

    .line 2268747
    return-void
.end method

.method public static a(LX/0QB;)LX/Fff;
    .locals 5

    .prologue
    .line 2268732
    sget-object v0, LX/Fff;->b:LX/Fff;

    if-nez v0, :cond_1

    .line 2268733
    const-class v1, LX/Fff;

    monitor-enter v1

    .line 2268734
    :try_start_0
    sget-object v0, LX/Fff;->b:LX/Fff;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268735
    if-eqz v2, :cond_0

    .line 2268736
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268737
    new-instance p0, LX/Fff;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/Fff;-><init>(Landroid/content/res/Resources;LX/0ad;)V

    .line 2268738
    move-object v0, p0

    .line 2268739
    sput-object v0, LX/Fff;->b:LX/Fff;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268740
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268741
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268742
    :cond_1
    sget-object v0, LX/Fff;->b:LX/Fff;

    return-object v0

    .line 2268743
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CwB;Ljava/lang/String;)LX/CwH;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268731
    invoke-virtual {p0, p1, p2}, LX/FfQ;->b(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
    .locals 1

    .prologue
    .line 2268728
    iget-object v0, p0, LX/Fff;->a:LX/0ad;

    invoke-static {v0, p1}, LX/1nE;->a(LX/0ad;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "graph_search_results_page_blended"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsFragment;

    move-result-object v0

    .line 2268729
    :goto_0
    return-object v0

    .line 2268730
    :cond_0
    const-string v0, "graph_search_results_page_blended"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->b(Ljava/lang/String;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic b()LX/Fdv;
    .locals 1

    .prologue
    .line 2268727
    invoke-virtual {p0}, LX/Fff;->c()Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(LX/CwB;Ljava/lang/String;)LX/CwA;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268726
    invoke-virtual {p0, p1, p2}, LX/FfQ;->a(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
    .locals 1

    .prologue
    .line 2268725
    const-string v0, ""

    invoke-virtual {p0, v0}, LX/Fff;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    move-result-object v0

    return-object v0
.end method
