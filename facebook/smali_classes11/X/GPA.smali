.class public final LX/GPA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/GPC;


# direct methods
.method public constructor <init>(LX/GPC;)V
    .locals 0

    .prologue
    .line 2348448
    iput-object p1, p0, LX/GPA;->a:LX/GPC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 2348449
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2348450
    iget-object v0, p0, LX/GPA;->a:LX/GPC;

    .line 2348451
    iget-object v2, v0, LX/GP4;->f:LX/GQ7;

    move-object v2, v2

    .line 2348452
    move-object v0, v2

    .line 2348453
    check-cast v0, LX/GQ9;

    .line 2348454
    invoke-static {v1}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v2

    .line 2348455
    sget-object p1, LX/GQ9;->a:Ljava/util/Set;

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0, v2}, LX/GQ7;->a(Z)V

    .line 2348456
    const/4 v0, 0x0

    .line 2348457
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2348458
    :cond_0
    :goto_0
    move v0, v0

    .line 2348459
    if-eqz v0, :cond_3

    .line 2348460
    invoke-static {v1}, LX/GQ9;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2348461
    iget-object v0, p0, LX/GPA;->a:LX/GPC;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/GP4;->a(Z)V

    .line 2348462
    :cond_1
    iget-object v0, p0, LX/GPA;->a:LX/GPC;

    iget-object v2, p0, LX/GPA;->a:LX/GPC;

    iget-object v2, v2, LX/GPC;->f:Ljava/util/concurrent/ExecutorService;

    .line 2348463
    invoke-virtual {v0, v2}, LX/GP4;->a(Ljava/util/concurrent/ExecutorService;)V

    .line 2348464
    :cond_2
    :goto_1
    iget-object v0, p0, LX/GPA;->a:LX/GPC;

    invoke-static {v1}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GPC;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V

    .line 2348465
    return-void

    .line 2348466
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 2348467
    iget-object v0, p0, LX/GPA;->a:LX/GPC;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/GP4;->a(Z)V

    goto :goto_1

    .line 2348468
    :cond_4
    invoke-static {v1}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2348469
    invoke-static {v2}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object p1

    .line 2348470
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getCardLength()I

    move-result p1

    if-lt v2, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2348471
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2348472
    return-void
.end method
