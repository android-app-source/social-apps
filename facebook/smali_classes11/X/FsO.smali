.class public LX/FsO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/FsO;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/BS3;

.field private final d:LX/0se;

.field private final e:LX/8p8;

.field private final f:LX/0rq;

.field private final g:LX/0sX;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Or;LX/BS3;LX/0rq;LX/0se;LX/8p8;LX/0sX;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/BS3;",
            "LX/0rq;",
            "LX/0se;",
            "LX/8p8;",
            "LX/0sX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2296856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296857
    iput-object p1, p0, LX/FsO;->a:LX/0ad;

    .line 2296858
    iput-object p2, p0, LX/FsO;->b:LX/0Or;

    .line 2296859
    iput-object p3, p0, LX/FsO;->c:LX/BS3;

    .line 2296860
    iput-object p4, p0, LX/FsO;->f:LX/0rq;

    .line 2296861
    iput-object p5, p0, LX/FsO;->d:LX/0se;

    .line 2296862
    iput-object p6, p0, LX/FsO;->e:LX/8p8;

    .line 2296863
    iput-object p7, p0, LX/FsO;->g:LX/0sX;

    .line 2296864
    return-void
.end method

.method public static a(LX/0QB;)LX/FsO;
    .locals 11

    .prologue
    .line 2296865
    sget-object v0, LX/FsO;->h:LX/FsO;

    if-nez v0, :cond_1

    .line 2296866
    const-class v1, LX/FsO;

    monitor-enter v1

    .line 2296867
    :try_start_0
    sget-object v0, LX/FsO;->h:LX/FsO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2296868
    if-eqz v2, :cond_0

    .line 2296869
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2296870
    new-instance v3, LX/FsO;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/BS3;->a(LX/0QB;)LX/BS3;

    move-result-object v6

    check-cast v6, LX/BS3;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v7

    check-cast v7, LX/0rq;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v8

    check-cast v8, LX/0se;

    invoke-static {v0}, LX/8p8;->b(LX/0QB;)LX/8p8;

    move-result-object v9

    check-cast v9, LX/8p8;

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v10

    check-cast v10, LX/0sX;

    invoke-direct/range {v3 .. v10}, LX/FsO;-><init>(LX/0ad;LX/0Or;LX/BS3;LX/0rq;LX/0se;LX/8p8;LX/0sX;)V

    .line 2296871
    move-object v0, v3

    .line 2296872
    sput-object v0, LX/FsO;->h:LX/FsO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2296873
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2296874
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2296875
    :cond_1
    sget-object v0, LX/FsO;->h:LX/FsO;

    return-object v0

    .line 2296876
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2296877
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/FsO;LX/0gW;Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW",
            "<*>;",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2296878
    invoke-static {p0}, LX/FsO;->c(LX/FsO;)Z

    move-result v0

    .line 2296879
    const-string v1, "cover_image_high_res_size"

    .line 2296880
    iget v2, p2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->c:I

    move v2, v2

    .line 2296881
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "media_type"

    iget-object v3, p0, LX/FsO;->f:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->c()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_pic_media_type"

    iget-object v3, p0, LX/FsO;->f:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->b()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "render_location"

    .line 2296882
    invoke-static {p0}, LX/FsO;->c(LX/FsO;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2296883
    const-string v3, "ANDROID_PROFILE_INTRO_CARD_HEADER"

    .line 2296884
    :goto_0
    move-object v3, v3

    .line 2296885
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_count"

    .line 2296886
    iget-object v3, p0, LX/FsO;->a:LX/0ad;

    sget-short v4, LX/0wf;->W:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2296887
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2296888
    :goto_1
    move-object v3, v3

    .line 2296889
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_id"

    .line 2296890
    iget-wide v7, p2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    move-wide v4, v7

    .line 2296891
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "fetch_intro_card"

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_bio"

    iget-object v2, p0, LX/FsO;->a:LX/0ad;

    sget-short v3, LX/0wf;->W:S

    invoke-interface {v2, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_suggested_bio"

    .line 2296892
    iget-boolean v2, p2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->d:Z

    move v2, v2

    .line 2296893
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_fav_photos"

    invoke-static {p0}, LX/FsO;->c(LX/FsO;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_fav_media"

    iget-object v2, p0, LX/FsO;->a:LX/0ad;

    sget-short v3, LX/0wf;->A:S

    invoke-interface {v2, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_suggested_photos"

    .line 2296894
    iget-boolean v2, p2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->e:Z

    move v2, v2

    .line 2296895
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_dominant_color"

    iget-object v2, p0, LX/FsO;->a:LX/0ad;

    sget-short v3, LX/0wf;->s:S

    invoke-interface {v2, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_external_links"

    iget-object v2, p0, LX/FsO;->e:LX/8p8;

    invoke-virtual {v2}, LX/8p8;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "fetch_expiration_information"

    .line 2296896
    iget-wide v7, p2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    move-wide v2, v7

    .line 2296897
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2296898
    iget-object v3, p0, LX/FsO;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    move v2, v3

    .line 2296899
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "automatic_photo_captioning_enabled"

    iget-object v2, p0, LX/FsO;->g:LX/0sX;

    invoke-virtual {v2}, LX/0sX;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2296900
    iget-object v0, p0, LX/FsO;->d:LX/0se;

    iget-object v1, p0, LX/FsO;->f:LX/0rq;

    invoke-virtual {v1}, LX/0rq;->c()LX/0wF;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2296901
    iget-object v0, p0, LX/FsO;->c:LX/BS3;

    invoke-virtual {v0, p1}, LX/BS3;->a(LX/0gW;)V

    .line 2296902
    return-void

    :cond_0
    const-string v3, "ANDROID_PROFILE_TILE"

    goto/16 :goto_0

    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_1
.end method

.method public static c(LX/FsO;)Z
    .locals 3

    .prologue
    .line 2296903
    iget-object v0, p0, LX/FsO;->a:LX/0ad;

    sget-short v1, LX/0wf;->W:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
