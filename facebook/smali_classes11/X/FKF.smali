.class public LX/FKF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/lang/Object;


# instance fields
.field private final c:LX/0Sh;

.field private final d:LX/03V;

.field private final e:LX/2Mq;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/FKE;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/FKH;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2224878
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 2224879
    const/16 v1, 0x64

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2224880
    const/16 v1, 0x65

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2224881
    const/16 v1, 0x66

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2224882
    move-object v0, v0

    .line 2224883
    sput-object v0, LX/FKF;->a:Landroid/util/SparseArray;

    .line 2224884
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 2224885
    const/16 v1, 0xc9

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2224886
    const/16 v1, 0xca

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2224887
    const/16 v1, 0xcb

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2224888
    const/16 v1, 0xcc

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2224889
    move-object v0, v0

    .line 2224890
    sput-object v0, LX/FKF;->b:Landroid/util/SparseArray;

    .line 2224891
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FKF;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/03V;LX/2Mq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224872
    iput-object p1, p0, LX/FKF;->c:LX/0Sh;

    .line 2224873
    iput-object p2, p0, LX/FKF;->d:LX/03V;

    .line 2224874
    iput-object p3, p0, LX/FKF;->e:LX/2Mq;

    .line 2224875
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/FKF;->g:Ljava/util/Map;

    .line 2224876
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/FKF;->f:Ljava/util/Map;

    .line 2224877
    return-void
.end method

.method public static a(LX/0QB;)LX/FKF;
    .locals 9

    .prologue
    .line 2224842
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2224843
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2224844
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2224845
    if-nez v1, :cond_0

    .line 2224846
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2224847
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2224848
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2224849
    sget-object v1, LX/FKF;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2224850
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2224851
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2224852
    :cond_1
    if-nez v1, :cond_4

    .line 2224853
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2224854
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2224855
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2224856
    new-instance p0, LX/FKF;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v8

    check-cast v8, LX/2Mq;

    invoke-direct {p0, v1, v7, v8}, LX/FKF;-><init>(LX/0Sh;LX/03V;LX/2Mq;)V

    .line 2224857
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2224858
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2224859
    if-nez v1, :cond_2

    .line 2224860
    sget-object v0, LX/FKF;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKF;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2224861
    :goto_1
    if-eqz v0, :cond_3

    .line 2224862
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2224863
    :goto_3
    check-cast v0, LX/FKF;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2224864
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2224865
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2224866
    :catchall_1
    move-exception v0

    .line 2224867
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2224868
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2224869
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2224870
    :cond_2
    :try_start_8
    sget-object v0, LX/FKF;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKF;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(I)S
    .locals 1

    .prologue
    .line 2224832
    sparse-switch p0, :sswitch_data_0

    .line 2224833
    const/16 v0, 0x33

    :goto_0
    return v0

    .line 2224834
    :sswitch_0
    const/16 v0, 0x36

    goto :goto_0

    .line 2224835
    :sswitch_1
    const/16 v0, 0x38

    goto :goto_0

    .line 2224836
    :sswitch_2
    const/16 v0, 0x37

    goto :goto_0

    .line 2224837
    :sswitch_3
    const/16 v0, 0x32

    goto :goto_0

    .line 2224838
    :sswitch_4
    const/16 v0, 0x30

    goto :goto_0

    .line 2224839
    :sswitch_5
    const/16 v0, 0x31

    goto :goto_0

    .line 2224840
    :sswitch_6
    const/4 v0, 0x3

    goto :goto_0

    .line 2224841
    :sswitch_7
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_2
        0x66 -> :sswitch_1
        0xc8 -> :sswitch_3
        0xc9 -> :sswitch_4
        0xca -> :sswitch_5
        0xcb -> :sswitch_7
        0xcc -> :sswitch_6
    .end sparse-switch
.end method

.method private static a(LX/FKE;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2224828
    iget v1, p0, LX/FKE;->b:I

    const/16 v2, 0x64

    if-ne v1, v2, :cond_1

    .line 2224829
    :cond_0
    :goto_0
    return v0

    .line 2224830
    :cond_1
    iget v1, p0, LX/FKE;->c:I

    const/16 v2, 0xcb

    if-eq v1, v2, :cond_2

    iget v1, p0, LX/FKE;->c:I

    const/16 v2, 0xcc

    if-ne v1, v2, :cond_0

    .line 2224831
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private declared-synchronized c()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2224778
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FKF;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 2224779
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2224780
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKE;

    .line 2224781
    invoke-static {v0}, LX/FKF;->a(LX/FKE;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2224782
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    .line 2224783
    iget-object v1, p0, LX/FKF;->g:Ljava/util/Map;

    iget-object v4, v0, LX/FKE;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/common/util/concurrent/SettableFuture;

    .line 2224784
    if-eqz v1, :cond_0

    .line 2224785
    new-instance v6, LX/FKH;

    iget-object v7, v0, LX/FKE;->a:Lcom/facebook/messaging/model/messages/Message;

    iget v4, v0, LX/FKE;->b:I

    const/16 v8, 0x65

    if-ne v4, v8, :cond_1

    move v4, v2

    :goto_1
    iget v0, v0, LX/FKE;->c:I

    const/16 v8, 0xcb

    if-ne v0, v8, :cond_2

    move v0, v2

    :goto_2
    invoke-direct {v6, v7, v4, v0}, LX/FKH;-><init>(Lcom/facebook/messaging/model/messages/Message;ZZ)V

    const v0, 0x11ff6c44

    invoke-static {v1, v6, v0}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2224786
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v4, v3

    .line 2224787
    goto :goto_1

    :cond_2
    move v0, v3

    goto :goto_2

    .line 2224788
    :cond_3
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/model/messages/Message;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FKH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2224818
    monitor-enter p0

    :try_start_0
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2224819
    iget-object v0, p0, LX/FKF;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKE;

    .line 2224820
    if-eqz v0, :cond_0

    .line 2224821
    iget-object v0, p0, LX/FKF;->d:LX/03V;

    const-string v2, "SendLifeCycleManager_old_state"

    const-string v3, "Send already in progress?"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2224822
    :cond_0
    new-instance v0, LX/FKE;

    invoke-direct {v0, p1}, LX/FKE;-><init>(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2224823
    iget-object v2, p0, LX/FKF;->f:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2224824
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2224825
    iget-object v2, p0, LX/FKF;->g:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2224826
    monitor-exit p0

    return-object v0

    .line 2224827
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 2224806
    monitor-enter p0

    :try_start_0
    const-string v0, "SendLifeCycleManager.setInsertPendingState"

    const v1, 0x12a6246f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2224807
    :try_start_1
    sget-object v0, LX/FKF;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2224808
    iget-object v0, p0, LX/FKF;->e:LX/2Mq;

    invoke-static {p2}, LX/FKF;->a(I)S

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2224809
    iget-object v0, p0, LX/FKF;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKE;

    .line 2224810
    if-eqz v0, :cond_0

    .line 2224811
    iput p2, v0, LX/FKE;->b:I

    .line 2224812
    invoke-direct {p0}, LX/FKF;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2224813
    :cond_0
    const v0, 0x5183eaa1

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2224814
    monitor-exit p0

    return-void

    .line 2224815
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2224816
    :catchall_0
    move-exception v0

    const v1, -0x49444896

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2224817
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2224789
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/FKF;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2224790
    iget-object v0, p0, LX/FKF;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FKE;

    .line 2224791
    if-eqz v0, :cond_3

    .line 2224792
    iput p2, v0, LX/FKE;->c:I

    .line 2224793
    invoke-direct {p0}, LX/FKF;->c()V

    .line 2224794
    invoke-static {v0}, LX/FKF;->a(LX/FKE;)Z

    move-result v0

    .line 2224795
    :goto_1
    iget-object v1, p0, LX/FKF;->e:LX/2Mq;

    const v4, 0x540001

    .line 2224796
    iget-object v2, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-interface {v2, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->c(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2224797
    iget-object v2, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-interface {v2, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2224798
    const-string v2, "non_specified_start"

    invoke-virtual {v1, p1, v2}, LX/2Mq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2224799
    :cond_0
    if-eqz v0, :cond_2

    .line 2224800
    iget-object v0, p0, LX/FKF;->e:LX/2Mq;

    invoke-static {p2}, LX/FKF;->a(I)S

    move-result v1

    .line 2224801
    iget-object v2, v0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x540001

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-interface {v2, v3, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2224802
    :goto_2
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    .line 2224803
    goto :goto_0

    .line 2224804
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/FKF;->e:LX/2Mq;

    invoke-static {p2}, LX/FKF;->a(I)S

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/2Mq;->a(Ljava/lang/String;S)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 2224805
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
