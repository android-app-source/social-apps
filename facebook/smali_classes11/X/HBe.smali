.class public final LX/HBe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;)V
    .locals 0

    .prologue
    .line 2437644
    iput-object p1, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2437685
    iget-object v0, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2437686
    :cond_0
    :goto_0
    return-void

    .line 2437687
    :cond_1
    iget-object v0, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080039

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2437688
    iget-object v0, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2437689
    iget-object v0, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2437645
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 2437646
    if-eqz p1, :cond_0

    .line 2437647
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2437648
    if-nez v0, :cond_1

    .line 2437649
    :cond_0
    :goto_0
    return-void

    .line 2437650
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2437651
    check-cast v0, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;

    .line 2437652
    invoke-virtual {v0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2437653
    if-eqz v1, :cond_2

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 2437654
    :cond_2
    const-class v0, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    const-string v1, "Model had empty profile picture URL"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2437655
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2437656
    :cond_3
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2437657
    iput-object v1, v3, LX/2dc;->h:Ljava/lang/String;

    .line 2437658
    move-object v1, v3

    .line 2437659
    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 2437660
    new-instance v2, LX/4XY;

    invoke-direct {v2}, LX/4XY;-><init>()V

    iget-object v3, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2437661
    iput-object v3, v2, LX/4XY;->ag:Ljava/lang/String;

    .line 2437662
    move-object v2, v2

    .line 2437663
    invoke-virtual {v0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2437664
    iput-object v3, v2, LX/4XY;->aT:Ljava/lang/String;

    .line 2437665
    move-object v2, v2

    .line 2437666
    iput-object v1, v2, LX/4XY;->bQ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2437667
    move-object v1, v2

    .line 2437668
    invoke-virtual {v0}, Lcom/facebook/pages/common/coverphotoreposition/FetchPageHeaderForCoverPhotoRepositionGraphQLModels$FetchPageHeaderForCoverPhotoRepositionQueryModel;->a()LX/0Px;

    move-result-object v0

    .line 2437669
    iput-object v0, v1, LX/4XY;->G:LX/0Px;

    .line 2437670
    move-object v0, v1

    .line 2437671
    invoke-virtual {v0}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 2437672
    iget-object v1, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    new-instance v2, LX/BPz;

    iget-object v3, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->e:LX/5SB;

    invoke-direct {v2, v3}, LX/BPz;-><init>(LX/5SB;)V

    .line 2437673
    iput-object v2, v1, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->f:LX/BPz;

    .line 2437674
    iget-object v1, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->f:LX/BPz;

    .line 2437675
    if-nez v0, :cond_4

    .line 2437676
    :goto_1
    iget-object v0, p0, LX/HBe;->a:Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;->b(Lcom/facebook/pages/common/coverphotoreposition/PagesCoverPhotoRepositionFragment;)V

    goto :goto_0

    .line 2437677
    :cond_4
    invoke-virtual {v1}, LX/BPy;->l()V

    .line 2437678
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v2

    iput-boolean v2, v1, LX/BPz;->a:Z

    .line 2437679
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ah()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    iput-object v2, v1, LX/BPz;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2437680
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    iput-object v2, v1, LX/BPz;->c:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2437681
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->aj()Z

    move-result v2

    iput-boolean v2, v1, LX/BPz;->d:Z

    .line 2437682
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/BPz;->e:Ljava/lang/String;

    .line 2437683
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/BPz;->f:LX/0Px;

    .line 2437684
    invoke-virtual {v1}, LX/BPy;->d()V

    goto :goto_1
.end method
