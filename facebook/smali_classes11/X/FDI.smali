.class public LX/FDI;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/2OQ;

.field public final c:LX/Ddb;

.field public final d:LX/2Oi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2211547
    const-class v0, LX/FDI;

    sput-object v0, LX/FDI;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2OQ;LX/Ddb;LX/2Oi;)V
    .locals 0
    .param p1    # LX/2OQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2211542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2211543
    iput-object p1, p0, LX/FDI;->b:LX/2OQ;

    .line 2211544
    iput-object p2, p0, LX/FDI;->c:LX/Ddb;

    .line 2211545
    iput-object p3, p0, LX/FDI;->d:LX/2Oi;

    .line 2211546
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threads/ThreadCriteria;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 1

    .prologue
    .line 2211421
    iget-object v0, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v0, p1}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;
    .locals 5

    .prologue
    .line 2211531
    iget-object v0, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v0}, LX/2OQ;->b()LX/0Px;

    move-result-object v0

    .line 2211532
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v1

    .line 2211533
    iput-object v0, v1, LX/6hv;->a:Ljava/util/List;

    .line 2211534
    move-object v1, v1

    .line 2211535
    iget-object v2, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v2}, LX/2OQ;->e()J

    move-result-wide v2

    .line 2211536
    iput-wide v2, v1, LX/6hv;->d:J

    .line 2211537
    move-object v1, v1

    .line 2211538
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2211539
    :goto_0
    iput-boolean v0, v1, LX/6hv;->b:Z

    .line 2211540
    move-object v0, v1

    .line 2211541
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)Lcom/facebook/messaging/service/model/FetchThreadListParams;
    .locals 3

    .prologue
    .line 2211517
    iget-object v0, p0, LX/FDI;->c:LX/Ddb;

    .line 2211518
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v1, v1

    .line 2211519
    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    move-object v2, v2

    .line 2211520
    invoke-virtual {v0, v1, v2}, LX/Ddb;->a(LX/6ek;LX/0rS;)LX/DdZ;

    move-result-object v0

    iget-object v0, v0, LX/DdZ;->a:LX/0rS;

    .line 2211521
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    move-object v1, v1

    .line 2211522
    if-eq v0, v1, :cond_0

    .line 2211523
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/6iI;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)LX/6iI;

    move-result-object v1

    .line 2211524
    iput-object v0, v1, LX/6iI;->a:LX/0rS;

    .line 2211525
    move-object v0, v1

    .line 2211526
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v1, v1

    .line 2211527
    iput-object v1, v0, LX/6iI;->g:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2211528
    move-object v0, v0

    .line 2211529
    invoke-virtual {v0}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object p1

    .line 2211530
    :cond_0
    return-object p1
.end method

.method public final a(LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;
    .locals 5

    .prologue
    .line 2211499
    iget-object v0, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v0, p1}, LX/2OQ;->d(LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v1

    .line 2211500
    iget-object v0, p0, LX/FDI;->d:LX/2Oi;

    invoke-virtual {v0}, LX/2Oi;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2211501
    iget-object v0, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v0, p1}, LX/2OQ;->b(LX/6ek;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_IN_MEMORY_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2211502
    :goto_0
    iget-object v3, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v3, p1}, LX/2OQ;->e(LX/6ek;)Lcom/facebook/messaging/model/folders/FolderCounts;

    move-result-object v3

    .line 2211503
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v4

    .line 2211504
    iput-object v0, v4, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2211505
    move-object v0, v4

    .line 2211506
    iput-object p1, v0, LX/6iK;->b:LX/6ek;

    .line 2211507
    move-object v0, v0

    .line 2211508
    iput-object v1, v0, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2211509
    move-object v0, v0

    .line 2211510
    iput-object v2, v0, LX/6iK;->d:Ljava/util/List;

    .line 2211511
    move-object v0, v0

    .line 2211512
    iput-object v3, v0, LX/6iK;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 2211513
    move-object v0, v0

    .line 2211514
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2211515
    return-object v0

    .line 2211516
    :cond_0
    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_IN_MEMORY_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    goto :goto_0
.end method

.method public final a(LX/1qK;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 2211426
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v3, v0

    .line 2211427
    const-string v0, "fetchThreadParams"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadParams;

    .line 2211428
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v1, v1

    .line 2211429
    invoke-virtual {p0, v1}, LX/FDI;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v4

    .line 2211430
    if-nez v4, :cond_4

    .line 2211431
    const/4 v1, 0x0

    .line 2211432
    :goto_0
    move-object v5, v1

    .line 2211433
    if-eqz v4, :cond_3

    .line 2211434
    iget-object v1, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2211435
    :goto_1
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadParams;->newBuilder()LX/6iM;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/6iM;->a(Lcom/facebook/messaging/service/model/FetchThreadParams;)LX/6iM;

    move-result-object v6

    .line 2211436
    iget-object v7, p0, LX/FDI;->c:LX/Ddb;

    .line 2211437
    iget-object v8, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    move-object v8, v8

    .line 2211438
    invoke-virtual {v7, v1, v8}, LX/Ddb;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0rS;)LX/DdZ;

    move-result-object v7

    iget-object v7, v7, LX/DdZ;->a:LX/0rS;

    .line 2211439
    iget-object v8, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    move-object v8, v8

    .line 2211440
    if-eq v7, v8, :cond_0

    .line 2211441
    iput-object v7, v6, LX/6iM;->b:LX/0rS;

    .line 2211442
    move-object v7, v6

    .line 2211443
    iget-object v8, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    move-object v8, v8

    .line 2211444
    iput-object v8, v7, LX/6iM;->c:LX/0rS;

    .line 2211445
    :cond_0
    if-nez v1, :cond_5

    .line 2211446
    invoke-virtual {v6}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v6

    .line 2211447
    :goto_2
    move-object v0, v6

    .line 2211448
    const-string v1, "fetchThreadParams"

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2211449
    const/4 v1, 0x0

    .line 2211450
    if-nez v4, :cond_7

    .line 2211451
    :cond_1
    :goto_3
    move v0, v1

    .line 2211452
    if-nez v0, :cond_2

    .line 2211453
    :goto_4
    return-object v2

    .line 2211454
    :cond_2
    if-nez v4, :cond_8

    .line 2211455
    sget-object v6, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211456
    :goto_5
    move-object v2, v6

    .line 2211457
    goto :goto_4

    :cond_3
    move-object v1, v2

    goto :goto_1

    :cond_4
    iget-object v1, p0, LX/FDI;->b:LX/2OQ;

    iget-object v5, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v5}, LX/2OQ;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v1

    goto :goto_0

    .line 2211458
    :cond_5
    iget-object v7, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v7, v1}, LX/2OQ;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)J

    move-result-wide v8

    .line 2211459
    iget-object v7, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v7, v1}, LX/2OQ;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)J

    move-result-wide v10

    .line 2211460
    cmp-long v7, v8, v10

    if-lez v7, :cond_6

    .line 2211461
    const/4 v7, 0x1

    .line 2211462
    iput-boolean v7, v6, LX/6iM;->f:Z

    .line 2211463
    move-object v7, v6

    .line 2211464
    iput-wide v8, v7, LX/6iM;->e:J

    .line 2211465
    :cond_6
    invoke-virtual {v6}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v6

    goto :goto_2

    .line 2211466
    :cond_7
    sget-object v3, LX/FDH;->a:[I

    .line 2211467
    iget-object v6, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    move-object v6, v6

    .line 2211468
    invoke-virtual {v6}, LX/0rS;->ordinal()I

    move-result v6

    aget v3, v3, v6

    packed-switch v3, :pswitch_data_0

    goto :goto_3

    .line 2211469
    :pswitch_0
    if-eqz v5, :cond_1

    .line 2211470
    iget v3, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v3, v3

    .line 2211471
    invoke-virtual {v5, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    goto :goto_3

    .line 2211472
    :pswitch_1
    iget-object v1, p0, LX/FDI;->b:LX/2OQ;

    iget-object v3, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2211473
    iget v6, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v6, v6

    .line 2211474
    invoke-virtual {v1, v3, v6}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Z

    move-result v1

    goto :goto_3

    .line 2211475
    :cond_8
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2211476
    new-instance v6, LX/6g4;

    iget-object v8, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    iget-object v9, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    invoke-direct {v6, v8, v9}, LX/6g4;-><init>(LX/0Px;LX/0Px;)V

    move-object v6, v6

    .line 2211477
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_9
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2211478
    iget-object v9, p0, LX/FDI;->d:LX/2Oi;

    invoke-virtual {v6}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v6

    invoke-virtual {v9, v6}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v6

    .line 2211479
    if-eqz v6, :cond_9

    .line 2211480
    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_6

    .line 2211481
    :cond_a
    if-nez v5, :cond_b

    const/4 v6, 0x0

    .line 2211482
    :goto_7
    iget-object v8, p0, LX/FDI;->b:LX/2OQ;

    iget-object v9, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8, v9, v6}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Z

    move-result v6

    if-eqz v6, :cond_c

    sget-object v6, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_IN_MEMORY_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2211483
    :goto_8
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v8

    .line 2211484
    iput-object v6, v8, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2211485
    move-object v6, v8

    .line 2211486
    iput-object v4, v6, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2211487
    move-object v6, v6

    .line 2211488
    iput-object v5, v6, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2211489
    move-object v6, v6

    .line 2211490
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 2211491
    iput-object v7, v6, LX/6iO;->e:LX/0Px;

    .line 2211492
    move-object v6, v6

    .line 2211493
    const-wide/16 v8, -0x1

    .line 2211494
    iput-wide v8, v6, LX/6iO;->g:J

    .line 2211495
    move-object v6, v6

    .line 2211496
    invoke-virtual {v6}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v6

    goto/16 :goto_5

    .line 2211497
    :cond_b
    invoke-virtual {v5}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v6

    goto :goto_7

    .line 2211498
    :cond_c
    sget-object v6, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_IN_MEMORY_CACHE_STALE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/6ek;LX/0rS;)Z
    .locals 2

    .prologue
    .line 2211422
    sget-object v0, LX/FDH;->a:[I

    invoke-virtual {p2}, LX/0rS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2211423
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2211424
    :pswitch_0
    iget-object v0, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v0, p1}, LX/2OQ;->a(LX/6ek;)Z

    move-result v0

    goto :goto_0

    .line 2211425
    :pswitch_1
    iget-object v0, p0, LX/FDI;->b:LX/2OQ;

    invoke-virtual {v0, p1}, LX/2OQ;->b(LX/6ek;)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
