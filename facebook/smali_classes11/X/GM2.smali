.class public final LX/GM2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bc9;


# instance fields
.field public final synthetic a:LX/GJI;

.field private b:I

.field private c:I

.field private d:I

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/GJI;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2343913
    iput-object p1, p0, LX/GM2;->a:LX/GJI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2343914
    iput v0, p0, LX/GM2;->b:I

    .line 2343915
    iput v0, p0, LX/GM2;->c:I

    .line 2343916
    const/4 v0, 0x0

    iput v0, p0, LX/GM2;->d:I

    .line 2343917
    const-string v0, ""

    iput-object v0, p0, LX/GM2;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;I)V
    .locals 3

    .prologue
    .line 2343879
    iget v0, p0, LX/GM2;->c:I

    iget-object v1, p0, LX/GM2;->a:LX/GJI;

    iget-object v1, v1, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2343880
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTextEditText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GM2;->e:Ljava/lang/String;

    .line 2343881
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getCursorPositionOnEditText()I

    move-result v0

    iput v0, p0, LX/GM2;->d:I

    .line 2343882
    :cond_0
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getId()I

    move-result v0

    if-eq p2, v0, :cond_2

    .line 2343883
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    invoke-virtual {v0}, LX/GJH;->y()V

    .line 2343884
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    iget v0, p0, LX/GM2;->b:I

    if-eq v0, p2, :cond_1

    .line 2343885
    iput p2, p0, LX/GM2;->b:I

    .line 2343886
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    .line 2343887
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343888
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2343889
    iget-object v1, p0, LX/GM2;->a:LX/GJI;

    iget-object v1, v1, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v0, v1}, LX/GG3;->o(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2343890
    :cond_1
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextEditText(Ljava/lang/CharSequence;)V

    .line 2343891
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    sget-object v1, LX/GMB;->EMPTY_UNSELECTED:LX/GMB;

    invoke-virtual {v0, v1}, LX/GJH;->a(LX/GMB;)V

    .line 2343892
    :goto_0
    iput p2, p0, LX/GM2;->c:I

    .line 2343893
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    .line 2343894
    iget-object v1, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v0}, LX/GJI;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    .line 2343895
    iget-object p0, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0}, LX/GJD;->getSelectedIndex()I

    move-result p0

    .line 2343896
    iget-object p1, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTag()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2343897
    iget-object p0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object p0

    .line 2343898
    :goto_1
    move-object p0, p0

    .line 2343899
    invoke-interface {v1, v2, p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2343900
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2343901
    new-instance v2, LX/GFl;

    iget-object p0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object p0

    invoke-direct {v2, p0}, LX/GFl;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2343902
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2343903
    sget-object v2, LX/GG8;->INVALID_BUDGET:LX/GG8;

    invoke-virtual {v0}, LX/GJI;->s()Z

    move-result p0

    invoke-virtual {v1, v2, p0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2343904
    invoke-virtual {v0}, LX/GJI;->p()V

    .line 2343905
    invoke-virtual {v0}, LX/GJI;->t()V

    .line 2343906
    invoke-virtual {v0}, LX/GJH;->z()V

    .line 2343907
    return-void

    .line 2343908
    :cond_2
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object v1, p0, LX/GM2;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextEditText(Ljava/lang/CharSequence;)V

    .line 2343909
    iget-object v0, p0, LX/GM2;->a:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget v1, p0, LX/GM2;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setSelectionOnEditText(I)V

    goto :goto_0

    .line 2343910
    :cond_3
    const/4 p1, -0x1

    if-eq p0, p1, :cond_4

    iget-object p1, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->a()I

    move-result p1

    if-lt p0, p1, :cond_5

    .line 2343911
    :cond_4
    const/4 p0, 0x0

    goto :goto_1

    .line 2343912
    :cond_5
    iget-object p1, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object p1

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object p0

    goto :goto_1
.end method
