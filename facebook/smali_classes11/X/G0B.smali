.class public LX/G0B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/17Y;

.field private b:Landroid/content/Context;

.field private c:LX/Bas;

.field private d:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

.field private e:LX/0kL;

.field private f:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17Y;LX/Bas;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;LX/0kL;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2308632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2308633
    iput-object p1, p0, LX/G0B;->b:Landroid/content/Context;

    .line 2308634
    iput-object p2, p0, LX/G0B;->a:LX/17Y;

    .line 2308635
    iput-object p3, p0, LX/G0B;->c:LX/Bas;

    .line 2308636
    iput-object p4, p0, LX/G0B;->d:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 2308637
    iput-object p5, p0, LX/G0B;->e:LX/0kL;

    .line 2308638
    iput-object p6, p0, LX/G0B;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308639
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/facebook/ipc/media/MediaItem;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2308602
    const-string v2, "extra_media_items"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2308603
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2308604
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2308605
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2308606
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2308607
    return-object v0

    :cond_0
    move v0, v1

    .line 2308608
    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;)Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 1

    .prologue
    .line 2308628
    const-string v0, "photo"

    invoke-static {p0, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2308629
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2308630
    return-object v0
.end method


# virtual methods
.method public final a()LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Vd",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2308631
    new-instance v0, LX/G0A;

    invoke-direct {v0, p0}, LX/G0A;-><init>(LX/G0B;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;Ljava/lang/String;LX/8AB;)Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2308618
    iget-object v0, p0, LX/G0B;->a:LX/17Y;

    iget-object v1, p0, LX/G0B;->b:Landroid/content/Context;

    sget-object v2, LX/0ax;->bY:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2308619
    const-string v1, "owner_id"

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2308620
    const-string v1, "title"

    iget-object v2, p0, LX/G0B;->b:Landroid/content/Context;

    const v3, 0x7f083352

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2308621
    const-string v1, "disable_adding_photos_to_albums"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2308622
    const-string v1, "extra_should_merge_camera_roll"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2308623
    const-string v1, "extra_should_show_suggested_photos"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2308624
    const-string v1, "extra_should_show_suggested_photos_before_camera_roll"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2308625
    const-string v1, "pick_hc_pic"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2308626
    const-string v1, "extra_simple_picker_launcher_configuration"

    new-instance v2, LX/8AA;

    invoke-direct {v2, p3}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v2}, LX/8AA;->i()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->j()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->l()LX/8AA;

    move-result-object v2

    sget-object v3, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v2, v3}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->o()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->m()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2308627
    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 2308616
    iget-object v0, p0, LX/G0B;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/G0B;->b:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2308617
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2308609
    if-nez p1, :cond_0

    .line 2308610
    const-string p1, ""

    .line 2308611
    :cond_0
    invoke-static {p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    .line 2308612
    iget-object v0, p0, LX/G0B;->c:LX/Bas;

    iget-object v7, p0, LX/G0B;->f:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p0, LX/G0B;->d:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;

    .line 2308613
    iget-object v5, v4, Lcom/facebook/caspian/ui/standardheader/StandardProfileImageFrame;->b:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-object v10, v5

    .line 2308614
    move v4, v3

    move v5, v3

    move v6, v3

    move-object v8, v1

    move-object v9, v1

    invoke-virtual/range {v0 .. v10}, LX/Bas;->a(LX/1bf;LX/1bf;ZZZZLcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;)V

    .line 2308615
    return-void
.end method
