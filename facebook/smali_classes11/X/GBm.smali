.class public final LX/GBm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 0

    .prologue
    .line 2327508
    iput-object p1, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x81f8f68

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327509
    iget-object v1, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v2, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v2, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v2}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v2

    .line 2327510
    iget-object v3, v1, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/GB4;->CHANGE_SEARCH_TYPE_CLICK:LX/GB4;

    invoke-virtual {p1}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "account_recovery"

    .line 2327511
    iput-object p1, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327512
    move-object v4, v4

    .line 2327513
    const-string p1, "search_type"

    invoke-virtual {v4, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 p1, 0x1

    invoke-interface {v3, v4, p1}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327514
    iget-object v1, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    sget-object v2, LX/GBv;->CONTACT_POINT:LX/GBv;

    if-ne v1, v2, :cond_1

    .line 2327515
    iget-object v1, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    sget-object v2, LX/GBv;->NAME:LX/GBv;

    invoke-static {v1, v2}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->a$redex0(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;LX/GBv;)V

    .line 2327516
    :cond_0
    :goto_0
    iget-object v1, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->c:LX/2Ne;

    iget-object v2, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v2, v2, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    invoke-virtual {v2}, LX/GBv;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2Ne;->b(Ljava/lang/String;)V

    .line 2327517
    const v1, -0x6db84a3c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2327518
    :cond_1
    iget-object v1, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->D:LX/GBv;

    sget-object v2, LX/GBv;->NAME:LX/GBv;

    if-ne v1, v2, :cond_0

    .line 2327519
    iget-object v1, p0, LX/GBm;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    sget-object v2, LX/GBv;->CONTACT_POINT:LX/GBv;

    invoke-static {v1, v2}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->a$redex0(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;LX/GBv;)V

    goto :goto_0
.end method
