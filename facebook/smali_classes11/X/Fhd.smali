.class public abstract LX/Fhd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/11i;

.field public final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public final e:LX/0ad;

.field public final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/7B6;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/BitSet;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Fhf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V
    .locals 1

    .prologue
    .line 2273548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273549
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Fhd;->f:Ljava/util/HashMap;

    .line 2273550
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Fhd;->c:Ljava/util/List;

    .line 2273551
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, LX/Fhd;->g:Ljava/util/BitSet;

    .line 2273552
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Fhd;->h:Ljava/util/List;

    .line 2273553
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Fhd;->i:Ljava/util/List;

    .line 2273554
    const/4 v0, 0x0

    iput v0, p0, LX/Fhd;->d:I

    .line 2273555
    iput-object p1, p0, LX/Fhd;->a:LX/11i;

    .line 2273556
    iput-object p2, p0, LX/Fhd;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2273557
    iput-object p3, p0, LX/Fhd;->e:LX/0ad;

    .line 2273558
    return-void
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2273547
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a()LX/Fhg;
.end method

.method public a(LX/7B6;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2273536
    iget v1, p0, LX/Fhd;->d:I

    .line 2273537
    iget v0, p0, LX/Fhd;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Fhd;->d:I

    .line 2273538
    iget-object v0, p0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2273539
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273540
    :goto_0
    return-void

    .line 2273541
    :cond_0
    iget-object v0, p0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2273542
    iget-object v0, p0, LX/Fhd;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2273543
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v0

    .line 2273544
    const-string v2, "end_to_end"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const v4, -0x6b39421d

    invoke-static {v0, v2, v3, v5, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273545
    const-string v2, "pre_fetch"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const v4, -0x15cdae1d

    invoke-static {v0, v2, v3, v5, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273546
    iget-object v0, p0, LX/Fhd;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x70020

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    goto :goto_0
.end method

.method public a(LX/7B6;I)V
    .locals 10

    .prologue
    .line 2273525
    iget-object v0, p0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/Integer;

    .line 2273526
    if-nez v6, :cond_0

    .line 2273527
    :goto_0
    return-void

    .line 2273528
    :cond_0
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v7

    .line 2273529
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2273530
    if-lez p2, :cond_1

    .line 2273531
    const-string v0, "IS_DELAYED_REQUEST"

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "DELAY_MS"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "QUERY_LENGTH"

    iget-object v5, p1, LX/7B6;->b:Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p1, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v5, v8, v9}, Ljava/lang/String;->codePointCount(II)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 2273532
    :cond_1
    const-string v1, "pre_fetch"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v3, -0x24b033c4

    invoke-static {v7, v1, v2, v0, v3}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273533
    const-string v0, "network"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const v3, -0x7a8fdfda

    invoke-static {v7, v0, v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273534
    iget-object v0, p0, LX/Fhd;->c:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2273535
    iget-object v0, p0, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    goto :goto_0
.end method

.method public a(LX/7B6;Ljava/util/List;LX/0P1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2273514
    iget-object v0, p0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2273515
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2273516
    :cond_0
    :goto_0
    return-void

    .line 2273517
    :cond_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2273518
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v2

    .line 2273519
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    const-string v4, "fetched_count"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    .line 2273520
    if-eqz p3, :cond_2

    invoke-virtual {p3}, LX/0P1;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2273521
    invoke-virtual {v3, p3}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2273522
    :cond_2
    const-string v4, "network"

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    const v5, 0xb4281dc

    invoke-static {v2, v4, v1, v3, v5}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273523
    const-string v3, "post_processing"

    const/4 v4, 0x0

    const v5, -0x3660d487

    invoke-static {v2, v3, v1, v4, v5}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273524
    iget-object v1, p0, LX/Fhd;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2273471
    iget-object v0, p0, LX/Fhd;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2273472
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v1

    .line 2273473
    iget-object v0, p0, LX/Fhd;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fhf;

    .line 2273474
    const-string v4, "post_processing_batch_"

    .line 2273475
    iget v5, v0, LX/Fhf;->a:I

    move v5, v5

    .line 2273476
    invoke-static {v4, v5}, LX/Fhd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 2273477
    iget-object v5, v0, LX/Fhf;->b:Ljava/lang/String;

    move-object v0, v5

    .line 2273478
    const v5, 0x70c3a5bd

    invoke-static {v1, v4, v0, v2, v5}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    goto :goto_0

    .line 2273479
    :cond_0
    iget-object v0, p0, LX/Fhd;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move-object v0, v1

    .line 2273480
    :goto_1
    iget-object v1, p0, LX/Fhd;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2273481
    :goto_2
    return-void

    .line 2273482
    :cond_1
    if-nez v0, :cond_3

    .line 2273483
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v0

    move-object v1, v0

    .line 2273484
    :goto_3
    iget-object v0, p0, LX/Fhd;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2273485
    const-string v4, "post_processing"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x122bcc49

    invoke-static {v1, v4, v5, v2, v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273486
    const-string v4, "end_to_end"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    const v6, -0x305f60df

    invoke-static {v1, v4, v5, v2, v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2273487
    iget-object v4, p0, LX/Fhd;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x70020

    const/4 v6, 0x2

    invoke-interface {v4, v5, v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2273488
    iget-object v4, p0, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v4, v0}, Ljava/util/BitSet;->clear(I)V

    goto :goto_4

    .line 2273489
    :cond_2
    iget-object v0, p0, LX/Fhd;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_2

    :cond_3
    move-object v1, v0

    goto :goto_3

    :cond_4
    move-object v0, v2

    goto :goto_1
.end method

.method public final b()LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<",
            "LX/Fhg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2273511
    iget-object v0, p0, LX/Fhd;->a:LX/11i;

    invoke-virtual {p0}, LX/Fhd;->a()LX/Fhg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2273512
    if-eqz v0, :cond_0

    .line 2273513
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Fhd;->a:LX/11i;

    invoke-virtual {p0}, LX/Fhd;->a()LX/Fhg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/7B6;)V
    .locals 5

    .prologue
    .line 2273503
    iget-object v0, p0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2273504
    if-nez v0, :cond_0

    .line 2273505
    :goto_0
    return-void

    .line 2273506
    :cond_0
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v1

    .line 2273507
    const-string v2, "pre_fetch"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7513402a

    invoke-static {v1, v2, v3, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;

    .line 2273508
    const-string v2, "end_to_end"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x5485e96b

    invoke-static {v1, v2, v3, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;

    .line 2273509
    iget-object v1, p0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2273510
    iget-object v1, p0, LX/Fhd;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public c(LX/7B6;)V
    .locals 1

    .prologue
    .line 2273501
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Fhd;->a(LX/7B6;I)V

    .line 2273502
    return-void
.end method

.method public d(LX/7B6;)V
    .locals 6

    .prologue
    .line 2273490
    iget-object v0, p0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2273491
    if-nez v0, :cond_0

    .line 2273492
    :goto_0
    return-void

    .line 2273493
    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2273494
    invoke-virtual {p0}, LX/Fhd;->b()LX/11o;

    move-result-object v2

    .line 2273495
    iget-object v3, p0, LX/Fhd;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x70020

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 2273496
    const-string v3, "end_to_end"

    const v4, -0x768d7459

    invoke-static {v2, v3, v1, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;

    .line 2273497
    const-string v3, "pre_fetch"

    const v4, -0x1853b07a

    invoke-static {v2, v3, v1, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;

    .line 2273498
    const-string v3, "network"

    const v4, -0x2d101237

    invoke-static {v2, v3, v1, v4}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;I)LX/11o;

    .line 2273499
    iget-object v1, p0, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2273500
    iget-object v1, p0, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->clear(I)V

    goto :goto_0
.end method
