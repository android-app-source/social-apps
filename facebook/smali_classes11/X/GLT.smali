.class public final LX/GLT;
.super LX/GFk;
.source ""


# instance fields
.field public final synthetic a:LX/GLb;


# direct methods
.method public constructor <init>(LX/GLb;)V
    .locals 0

    .prologue
    .line 2342976
    iput-object p1, p0, LX/GLT;->a:LX/GLb;

    invoke-direct {p0}, LX/GFk;-><init>()V

    return-void
.end method

.method private a(LX/GFj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2342977
    iget-object v0, p1, LX/GFj;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2342978
    iget-object v1, p1, LX/GFj;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2342979
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2342980
    :goto_0
    return-void

    .line 2342981
    :cond_0
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2342982
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2342983
    if-eqz v0, :cond_3

    .line 2342984
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2342985
    iput-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2342986
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2342987
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-boolean v0, v0, LX/GLb;->m:Z

    if-nez v0, :cond_1

    .line 2342988
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-object v0, v0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0, v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)V

    .line 2342989
    :cond_1
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-object v0, v0, LX/GLb;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz v0, :cond_2

    .line 2342990
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-object v0, v0, LX/GLb;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v0}, LX/GMo;->a(Landroid/view/View;)V

    .line 2342991
    :cond_2
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    .line 2342992
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2342993
    new-instance v1, LX/GFx;

    iget-object v2, p0, LX/GLT;->a:LX/GLb;

    iget-object v2, v2, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v2}, LX/GG6;->m(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GFx;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2342994
    :cond_3
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    const/4 v1, 0x4

    .line 2342995
    iput v1, v0, LX/GLb;->j:I

    .line 2342996
    iget-object v0, p1, LX/GFj;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2342997
    iget-object v1, p0, LX/GLT;->a:LX/GLb;

    iget-object v1, v1, LX/GLb;->b:LX/GG6;

    iget-object v2, p0, LX/GLT;->a:LX/GLb;

    iget-object v2, v2, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v1, v2}, LX/GG6;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-boolean v0, v0, LX/GLb;->m:Z

    if-nez v0, :cond_4

    .line 2342998
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->o(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2342999
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    invoke-virtual {v0}, LX/GLb;->b()V

    goto/16 :goto_0

    .line 2343000
    :cond_4
    iget-object v0, p0, LX/GLT;->a:LX/GLb;

    .line 2343001
    iget-object v1, p1, LX/GFj;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2343002
    const-string v2, "fetch_first_batch_audiences_task_key"

    invoke-virtual {v0, v1, v2, v3}, LX/GLb;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2343003
    check-cast p1, LX/GFj;

    invoke-direct {p0, p1}, LX/GLT;->a(LX/GFj;)V

    return-void
.end method
