.class public LX/GaB;
.super LX/1a1;
.source ""


# static fields
.field private static l:LX/0Zb;


# instance fields
.field private m:Landroid/content/Context;

.field private n:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

.field private o:Lcom/facebook/widget/text/BetterTextView;

.field private p:LX/GaA;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/text/BetterTextView;Landroid/content/Context;LX/7j6;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/0Zb;)V
    .locals 4

    .prologue
    .line 2368016
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2368017
    sput-object p5, LX/GaB;->l:LX/0Zb;

    .line 2368018
    iput-object p2, p0, LX/GaB;->m:Landroid/content/Context;

    .line 2368019
    iput-object p4, p0, LX/GaB;->n:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2368020
    const v0, 0x7f0d2708

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/GaB;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 2368021
    new-instance v0, LX/GaA;

    iget-object v1, p0, LX/GaB;->m:Landroid/content/Context;

    sget-object v2, LX/7iO;->STOREFRONT_COLLECTION:LX/7iO;

    sget-object v3, LX/GaB;->l:LX/0Zb;

    invoke-direct {v0, v1, p3, v2, v3}, LX/GaA;-><init>(Landroid/content/Context;LX/7j6;LX/7iO;LX/0Zb;)V

    iput-object v0, p0, LX/GaB;->p:LX/GaA;

    .line 2368022
    iget-object v0, p0, LX/GaB;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/GaB;->p:LX/GaA;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2368023
    return-void
.end method


# virtual methods
.method public final a(LX/GaH;I)V
    .locals 6

    .prologue
    .line 2368024
    iget-object v0, p0, LX/GaB;->p:LX/GaA;

    iget-object v1, p0, LX/GaB;->n:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2368025
    iput-object p1, v0, LX/GaA;->f:LX/GaH;

    .line 2368026
    iput-object v1, v0, LX/GaA;->e:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2368027
    div-int/lit8 v0, p2, 0xa

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    mul-int/lit8 v0, v0, 0xa

    .line 2368028
    iget-object v1, p0, LX/GaB;->m:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00a0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2368029
    iget-object v1, p0, LX/GaB;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2368030
    return-void
.end method
