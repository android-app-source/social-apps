.class public LX/Gyt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GyR;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:LX/0ka;

.field private final d:LX/0yH;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/0ka;LX/0yH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2410633
    iput-object p1, p0, LX/Gyt;->a:Landroid/content/Context;

    .line 2410634
    iput-object p2, p0, LX/Gyt;->b:Landroid/content/pm/PackageManager;

    .line 2410635
    iput-object p3, p0, LX/Gyt;->c:LX/0ka;

    .line 2410636
    iput-object p4, p0, LX/Gyt;->d:LX/0yH;

    .line 2410637
    return-void
.end method

.method public static a(LX/0QB;)LX/Gyt;
    .locals 5

    .prologue
    .line 2410638
    new-instance v4, LX/Gyt;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageManager;

    invoke-static {p0}, LX/0ka;->a(LX/0QB;)LX/0ka;

    move-result-object v2

    check-cast v2, LX/0ka;

    invoke-static {p0}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v3

    check-cast v3, LX/0yH;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Gyt;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;LX/0ka;LX/0yH;)V

    .line 2410639
    move-object v0, v4

    .line 2410640
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2410641
    iget-object v0, p0, LX/Gyt;->b:Landroid/content/pm/PackageManager;

    const-string v1, "android.hardware.wifi"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gyt;->d:LX/0yH;

    sget-object v1, LX/0yY;->LOCATION_SERVICES_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2410642
    iget-object v0, p0, LX/Gyt;->a:Landroid/content/Context;

    const v1, 0x7f0837a8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2410643
    iget-object v0, p0, LX/Gyt;->c:LX/0ka;

    invoke-virtual {v0}, LX/0ka;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2410644
    iget-object v0, p0, LX/Gyt;->a:Landroid/content/Context;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2410645
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Gyt;->a:Landroid/content/Context;

    const v1, 0x7f08002f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2410646
    const-string v0, "wifi"

    return-object v0
.end method

.method public final e()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2410647
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2410648
    iget-object v1, p0, LX/Gyt;->a:Landroid/content/Context;

    invoke-static {v1, v2, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
