.class public final LX/FsR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;",
        ">;",
        "LX/FsM;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FsT;


# direct methods
.method public constructor <init>(LX/FsT;)V
    .locals 0

    .prologue
    .line 2296985
    iput-object p1, p0, LX/FsR;->a:LX/FsT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2296986
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2296987
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2296988
    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;

    invoke-static {v0}, LX/5xF;->a(Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;)Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2296989
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->s()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v1

    .line 2296990
    invoke-static {v1}, LX/Fs4;->a(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;)V

    .line 2296991
    new-instance v2, LX/FsM;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->Y()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    .line 2296992
    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2296993
    invoke-direct {v2, v0, v1, v3}, LX/FsM;-><init>(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;LX/0ta;)V

    return-object v2
.end method
