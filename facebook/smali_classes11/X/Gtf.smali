.class public LX/Gtf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2402060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2402061
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2402062
    const-string v0, "mobile_page"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2402063
    const-string v1, "faceweb_modal"

    invoke-virtual {p1, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2402064
    const-string v2, "titlebar_with_modal_done"

    invoke-virtual {p1, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2402065
    const-string v3, "arg_is_checkpoint"

    invoke-virtual {p1, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 2402066
    const-string v4, "arg_is_blocking_checkpoint"

    invoke-virtual {p1, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 2402067
    const-string v5, "faceweb_nfx"

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 2402068
    const-string v6, "extra_launch_uri"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2402069
    if-nez v0, :cond_0

    if-eqz v6, :cond_0

    .line 2402070
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v6, "href"

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2402071
    :cond_0
    const-string v6, "uri_unhandled_report_category_name"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2402072
    invoke-static/range {v0 .. v6}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Ljava/lang/String;ZZZZZLjava/lang/String;)Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    move-result-object v0

    return-object v0
.end method
