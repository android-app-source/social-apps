.class public LX/Gq4;
.super LX/Cod;
.source ""

# interfaces
.implements LX/Gpt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/GpV;",
        ">;",
        "LX/Gpt;"
    }
.end annotation


# instance fields
.field public a:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CIj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Landroid/widget/LinearLayout;

.field public e:Landroid/view/View$OnClickListener;

.field public f:LX/Gpu;

.field public g:LX/GoE;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2396182
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396183
    const-class v0, LX/Gq4;

    invoke-static {v0, p0}, LX/Gq4;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396184
    const v0, 0x7f0d1810

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Gq4;->d:Landroid/widget/LinearLayout;

    .line 2396185
    new-instance v0, LX/Gq3;

    invoke-direct {v0, p0}, LX/Gq3;-><init>(LX/Gq4;)V

    iput-object v0, p0, LX/Gq4;->e:Landroid/view/View$OnClickListener;

    .line 2396186
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gq4;

    invoke-static {p0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v1

    check-cast v1, LX/GnF;

    invoke-static {p0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v2

    check-cast v2, LX/Go0;

    invoke-static {p0}, LX/CIj;->a(LX/0QB;)LX/CIj;

    move-result-object p0

    check-cast p0, LX/CIj;

    iput-object v1, p1, LX/Gq4;->a:LX/GnF;

    iput-object v2, p1, LX/Gq4;->b:LX/Go0;

    iput-object p0, p1, LX/Gq4;->c:LX/CIj;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;ZLX/GoE;)V
    .locals 6

    .prologue
    .line 2396133
    iput-object p3, p0, LX/Gq4;->g:LX/GoE;

    .line 2396134
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2396135
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->e()LX/CHX;

    move-result-object v2

    const/4 p3, 0x1

    .line 2396136
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 2396137
    const v4, 0x7f030963

    iget-object v5, p0, LX/Gq4;->d:Landroid/widget/LinearLayout;

    const/4 p1, 0x0

    invoke-virtual {v3, v4, v5, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;

    .line 2396138
    iget-object v4, p0, LX/Gq4;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2396139
    invoke-virtual {v3}, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->a()V

    .line 2396140
    invoke-virtual {v3, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->setImageUrl(Ljava/lang/String;)V

    .line 2396141
    iput-object v2, v3, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->d:LX/CHX;

    .line 2396142
    iget-object v4, p0, LX/Gq4;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396143
    if-eqz v1, :cond_0

    const-string v4, "OUT_OF_STOCK"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2396144
    invoke-virtual {v3, p3}, Lcom/facebook/instantshopping/view/widget/ColorPickerImageItemLayout;->setDisabled(Z)V

    .line 2396145
    :cond_0
    if-eqz p2, :cond_1

    .line 2396146
    iput-object v3, p0, LX/Gq4;->f:LX/Gpu;

    .line 2396147
    iget-object v3, p0, LX/Gq4;->f:LX/Gpu;

    invoke-interface {v3, p3}, LX/Gpu;->setIsSelected(Z)V

    .line 2396148
    :cond_1
    :goto_0
    return-void

    .line 2396149
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2396150
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    move v0, v0

    .line 2396151
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->e()LX/CHX;

    move-result-object v2

    const/4 p3, 0x1

    .line 2396152
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 2396153
    const v4, 0x7f030962

    iget-object v5, p0, LX/Gq4;->d:Landroid/widget/LinearLayout;

    const/4 p1, 0x0

    invoke-virtual {v3, v4, v5, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;

    .line 2396154
    iget-object v4, p0, LX/Gq4;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2396155
    invoke-virtual {v3}, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->a()V

    .line 2396156
    invoke-virtual {v3, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->setColor(I)V

    .line 2396157
    iput-object v2, v3, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->d:LX/CHX;

    .line 2396158
    iget-object v4, p0, LX/Gq4;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396159
    if-eqz v1, :cond_3

    const-string v4, "OUT_OF_STOCK"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2396160
    invoke-virtual {v3, p3}, Lcom/facebook/instantshopping/view/widget/ColorPickerColorItemLayout;->setDisabled(Z)V

    .line 2396161
    :cond_3
    if-eqz p2, :cond_4

    .line 2396162
    iput-object v3, p0, LX/Gq4;->f:LX/Gpu;

    .line 2396163
    iget-object v3, p0, LX/Gq4;->f:LX/Gpu;

    invoke-interface {v3, p3}, LX/Gpu;->setIsSelected(Z)V

    .line 2396164
    :cond_4
    goto :goto_0

    .line 2396165
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2396166
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->e()LX/CHX;

    move-result-object v2

    const/4 p3, 0x1

    .line 2396167
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 2396168
    const v4, 0x7f030966

    iget-object v5, p0, LX/Gq4;->d:Landroid/widget/LinearLayout;

    const/4 p1, 0x0

    invoke-virtual {v3, v4, v5, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;

    .line 2396169
    iget-object v4, p0, LX/Gq4;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2396170
    invoke-virtual {v3}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->a()V

    .line 2396171
    invoke-virtual {v3, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->setOptionText(Ljava/lang/String;)V

    .line 2396172
    iput-object v2, v3, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->b:LX/CHX;

    .line 2396173
    iget-object v4, p0, LX/Gq4;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396174
    if-eqz v1, :cond_6

    const-string v4, "OUT_OF_STOCK"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2396175
    invoke-virtual {v3, p3}, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;->setDisabled(Z)V

    .line 2396176
    :cond_6
    if-eqz p2, :cond_7

    .line 2396177
    iput-object v3, p0, LX/Gq4;->f:LX/Gpu;

    .line 2396178
    iget-object v3, p0, LX/Gq4;->f:LX/Gpu;

    invoke-interface {v3, p3}, LX/Gpu;->setIsSelected(Z)V

    .line 2396179
    iget-object v3, p0, LX/Gq4;->c:LX/CIj;

    .line 2396180
    iput-boolean p3, v3, LX/CIj;->a:Z

    .line 2396181
    :cond_7
    goto/16 :goto_0

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method
