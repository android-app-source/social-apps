.class public LX/FN4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FN4;


# instance fields
.field public a:LX/3Mo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2232160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232161
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FN4;->b:Ljava/util/Map;

    .line 2232162
    return-void
.end method

.method public static a(LX/0QB;)LX/FN4;
    .locals 4

    .prologue
    .line 2232168
    sget-object v0, LX/FN4;->c:LX/FN4;

    if-nez v0, :cond_1

    .line 2232169
    const-class v1, LX/FN4;

    monitor-enter v1

    .line 2232170
    :try_start_0
    sget-object v0, LX/FN4;->c:LX/FN4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232171
    if-eqz v2, :cond_0

    .line 2232172
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232173
    new-instance p0, LX/FN4;

    invoke-direct {p0}, LX/FN4;-><init>()V

    .line 2232174
    invoke-static {v0}, LX/3Mo;->a(LX/0QB;)LX/3Mo;

    move-result-object v3

    check-cast v3, LX/3Mo;

    .line 2232175
    iput-object v3, p0, LX/FN4;->a:LX/3Mo;

    .line 2232176
    move-object v0, p0

    .line 2232177
    sput-object v0, LX/FN4;->c:LX/FN4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232178
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232179
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232180
    :cond_1
    sget-object v0, LX/FN4;->c:LX/FN4;

    return-object v0

    .line 2232181
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2232163
    iget-object v0, p0, LX/FN4;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2232164
    if-eqz v0, :cond_0

    .line 2232165
    :goto_0
    return-object v0

    .line 2232166
    :cond_0
    iget-object v0, p0, LX/FN4;->a:LX/3Mo;

    invoke-virtual {v0, p1}, LX/3Mo;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2232167
    iget-object v1, p0, LX/FN4;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
