.class public final LX/H3r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FPE;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

.field public final synthetic c:LX/H3u;


# direct methods
.method public constructor <init>(LX/H3u;ILcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V
    .locals 0

    .prologue
    .line 2421963
    iput-object p1, p0, LX/H3r;->c:LX/H3u;

    iput p2, p0, LX/H3r;->a:I

    iput-object p3, p0, LX/H3r;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2421964
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->h:LX/H3w;

    if-nez v0, :cond_0

    .line 2421965
    :goto_0
    return-void

    .line 2421966
    :cond_0
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->h:LX/H3w;

    iget v1, p0, LX/H3r;->a:I

    .line 2421967
    iget-object v2, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-static {v2, v1}, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->a$redex0(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;I)V

    .line 2421968
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->d:LX/H1i;

    iget-object v1, p0, LX/H3r;->c:LX/H3u;

    iget-object v1, v1, LX/H3u;->e:Ljava/lang/String;

    iget-object v2, p0, LX/H3r;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2421969
    const-string p0, "place"

    invoke-static {v0, v1, v2, p0}, LX/H1i;->a(LX/H1i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2421970
    goto :goto_0
.end method

.method public final a(I)V
    .locals 6

    .prologue
    .line 2421943
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->h:LX/H3w;

    if-nez v0, :cond_0

    .line 2421944
    :goto_0
    return-void

    .line 2421945
    :cond_0
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->h:LX/H3w;

    iget v1, p0, LX/H3r;->a:I

    .line 2421946
    iget-object v2, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2421947
    if-ltz p1, :cond_1

    move v3, v4

    :goto_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2421948
    iget-object v3, v2, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    invoke-virtual {v3, v1}, LX/H3u;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    .line 2421949
    const-string v0, "Null place model cannot have hscroll photos"

    invoke-static {v3, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421950
    invoke-virtual {v3}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->l()LX/0Px;

    move-result-object v3

    .line 2421951
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    :goto_2
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2421952
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2421953
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    .line 2421954
    invoke-virtual {v3}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;->jL_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2421955
    invoke-virtual {v3}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;->jL_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    :cond_1
    move v3, v5

    .line 2421956
    goto :goto_1

    :cond_2
    move v4, v5

    .line 2421957
    goto :goto_2

    .line 2421958
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2421959
    iget-object v5, v2, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->c:LX/FPr;

    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v5, v4, v3, v0}, LX/FPr;->a(LX/0Px;Ljava/lang/String;Landroid/content/Context;)V

    .line 2421960
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->d:LX/H1i;

    iget-object v1, p0, LX/H3r;->c:LX/H3u;

    iget-object v1, v1, LX/H3u;->e:Ljava/lang/String;

    iget-object v2, p0, LX/H3r;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2421961
    const-string v3, "photo_in_collection"

    invoke-static {v0, v1, v2, v3}, LX/H1i;->a(LX/H1i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2421962
    goto :goto_0
.end method

.method public final a(LX/FQN;)V
    .locals 5

    .prologue
    .line 2421917
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->h:LX/H3w;

    if-nez v0, :cond_0

    .line 2421918
    :goto_0
    return-void

    .line 2421919
    :cond_0
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->h:LX/H3w;

    iget v1, p0, LX/H3r;->a:I

    .line 2421920
    if-ltz v1, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2421921
    iget-object v2, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    invoke-virtual {v2, v1}, LX/H3u;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    .line 2421922
    if-nez v2, :cond_2

    .line 2421923
    :goto_2
    goto :goto_0

    .line 2421924
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 2421925
    :cond_2
    sget-object v3, LX/H3z;->a:[I

    invoke-virtual {p1}, LX/FQN;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2421926
    iget-object v2, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-static {v2, v1}, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->a$redex0(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;I)V

    goto :goto_2

    .line 2421927
    :pswitch_0
    iget-object v3, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    .line 2421928
    iget-object v4, v3, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->c:LX/FPr;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->d:Ljava/lang/Class;

    invoke-virtual {v4, v2, v0, v1}, LX/FPr;->b(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2421929
    goto :goto_2

    .line 2421930
    :pswitch_1
    iget-object v3, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    .line 2421931
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2421932
    iget-object p0, v3, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->c:LX/FPr;

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, v0, v1, p1, v4}, LX/FPr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 2421933
    goto :goto_2

    .line 2421934
    :pswitch_2
    iget-object v3, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    .line 2421935
    iget-object v4, v3, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->c:LX/FPr;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->d:Ljava/lang/Class;

    invoke-virtual {v4, v2, p0, v0}, LX/FPr;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2421936
    goto :goto_2

    .line 2421937
    :pswitch_3
    iget-object v3, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    .line 2421938
    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object p0

    .line 2421939
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_3
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2421940
    iget-object v4, v3, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->c:LX/FPr;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->d:Ljava/lang/Class;

    invoke-virtual {v4, p0, v0, v1}, LX/FPr;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2421941
    goto :goto_2

    .line 2421942
    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 2421907
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->h:LX/H3w;

    if-nez v0, :cond_0

    .line 2421908
    :goto_0
    return-void

    .line 2421909
    :cond_0
    iget-object v0, p0, LX/H3r;->c:LX/H3u;

    iget-object v0, v0, LX/H3u;->h:LX/H3w;

    iget v1, p0, LX/H3r;->a:I

    .line 2421910
    iget-object v2, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    invoke-virtual {v2, v1}, LX/H3u;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    .line 2421911
    if-nez v2, :cond_1

    .line 2421912
    :goto_1
    goto :goto_0

    .line 2421913
    :cond_1
    iget-object v3, v0, LX/H3w;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    .line 2421914
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2421915
    iget-object v5, v3, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->c:LX/FPr;

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v5, p0, v0, v1, v4}, LX/FPr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 2421916
    goto :goto_1
.end method
