.class public final LX/H32;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2419559
    const-class v1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesGraphQLModels$SearchNearbyPlacesModel;

    const v0, 0x3e6fef51

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "SearchNearbyPlaces"

    const-string v6, "5d22930f43ec53e250bde90a3a072f2a"

    const-string v7, "nearby_search"

    const-string v8, "10155069969141729"

    const-string v9, "10155259090076729"

    .line 2419560
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2419561
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2419562
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2419563
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2419564
    sparse-switch v0, :sswitch_data_0

    .line 2419565
    :goto_0
    return-object p1

    .line 2419566
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2419567
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2419568
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2419569
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2419570
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2419571
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2419572
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2419573
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2419574
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2419575
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2419576
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2419577
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2419578
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2419579
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2419580
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e394131 -> :sswitch_a
        -0x75a8ea72 -> :sswitch_8
        -0x54fe4dbe -> :sswitch_b
        -0x46798828 -> :sswitch_5
        -0x3755aadd -> :sswitch_4
        -0x33cff17e -> :sswitch_7
        -0x23113c94 -> :sswitch_0
        0x66f18c8 -> :sswitch_1
        0x221abe99 -> :sswitch_c
        0x2c0a8f7e -> :sswitch_2
        0x2fb034ac -> :sswitch_3
        0x34e3348a -> :sswitch_9
        0x6ea98809 -> :sswitch_d
        0x73510ad3 -> :sswitch_e
        0x7c66a496 -> :sswitch_6
    .end sparse-switch
.end method
