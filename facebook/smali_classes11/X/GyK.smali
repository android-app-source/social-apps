.class public LX/GyK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PlaceNode:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/GyQ;


# direct methods
.method public constructor <init>(LX/GyQ;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2409872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2409873
    iput-object p1, p0, LX/GyK;->a:LX/GyQ;

    .line 2409874
    return-void
.end method

.method public static a(LX/0QB;)LX/GyK;
    .locals 4

    .prologue
    .line 2409875
    const-class v1, LX/GyK;

    monitor-enter v1

    .line 2409876
    :try_start_0
    sget-object v0, LX/GyK;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2409877
    sput-object v2, LX/GyK;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2409878
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2409879
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2409880
    new-instance p0, LX/GyK;

    invoke-static {v0}, LX/GyQ;->a(LX/0QB;)LX/GyQ;

    move-result-object v3

    check-cast v3, LX/GyQ;

    invoke-direct {p0, v3}, LX/GyK;-><init>(LX/GyQ;)V

    .line 2409881
    move-object v0, p0

    .line 2409882
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2409883
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GyK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2409884
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2409885
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
