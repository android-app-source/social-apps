.class public LX/G3c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:LX/2rX;

.field public l:I

.field public m:I

.field private n:LX/2uF;

.field public o:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

.field public p:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLX/2rX;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2315952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2315953
    iput v0, p0, LX/G3c;->l:I

    .line 2315954
    iput-object p1, p0, LX/G3c;->a:Ljava/lang/String;

    .line 2315955
    iput-object p2, p0, LX/G3c;->b:Ljava/lang/String;

    .line 2315956
    iput-object p3, p0, LX/G3c;->c:Ljava/lang/String;

    .line 2315957
    iput-object p4, p0, LX/G3c;->d:Ljava/lang/String;

    .line 2315958
    iput-boolean p5, p0, LX/G3c;->e:Z

    .line 2315959
    iput-boolean p6, p0, LX/G3c;->f:Z

    .line 2315960
    iput-boolean p7, p0, LX/G3c;->g:Z

    .line 2315961
    iput-boolean p8, p0, LX/G3c;->h:Z

    .line 2315962
    iput-boolean v0, p0, LX/G3c;->i:Z

    .line 2315963
    iput-boolean p9, p0, LX/G3c;->j:Z

    .line 2315964
    iput-object p10, p0, LX/G3c;->k:LX/2rX;

    .line 2315965
    return-void
.end method

.method public static a(LX/2uF;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I
    .locals 6

    .prologue
    .line 2315947
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2315948
    invoke-virtual {p0, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x1

    const-class v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v2, v1, v3, v4, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 2315949
    :goto_1
    return v0

    .line 2315950
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2315951
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private y()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2315946
    iget v1, p0, LX/G3c;->l:I

    iget v2, p0, LX/G3c;->m:I

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/G3c;->n:LX/2uF;

    iget v2, p0, LX/G3c;->l:I

    invoke-virtual {v1, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const v3, -0x7f5f8fc3

    const/4 v2, 0x1

    .line 2315915
    iput-object p1, p0, LX/G3c;->p:Ljava/lang/Object;

    .line 2315916
    iget-boolean v0, p0, LX/G3c;->j:Z

    if-eqz v0, :cond_7

    .line 2315917
    check-cast p1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;

    invoke-virtual {p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->a()Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2315918
    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/G3c;->n:LX/2uF;

    .line 2315919
    iget-object v0, p0, LX/G3c;->n:LX/2uF;

    const/4 p1, -0x1

    const v4, -0x7f5f8fc3

    .line 2315920
    new-instance v1, LX/3Si;

    invoke-direct {v1}, LX/3Si;-><init>()V

    .line 2315921
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-static {v0, v2}, LX/G3c;->a(LX/2uF;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I

    move-result v2

    .line 2315922
    if-eq v2, p1, :cond_0

    .line 2315923
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v1, v3, v2, v4}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 2315924
    :cond_0
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-static {v0, v2}, LX/G3c;->a(LX/2uF;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I

    move-result v2

    .line 2315925
    if-eq v2, p1, :cond_1

    .line 2315926
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v1, v3, v2, v4}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 2315927
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-static {v0, v2}, LX/G3c;->a(LX/2uF;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I

    move-result v2

    .line 2315928
    if-eq v2, p1, :cond_2

    .line 2315929
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v1, v3, v2, v4}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 2315930
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-static {v0, v2}, LX/G3c;->a(LX/2uF;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I

    move-result v2

    .line 2315931
    if-eq v2, p1, :cond_3

    .line 2315932
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v1, v3, v2, v4}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 2315933
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-static {v0, v2}, LX/G3c;->a(LX/2uF;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I

    move-result v2

    .line 2315934
    if-eq v2, p1, :cond_4

    .line 2315935
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v1, v3, v2, v4}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 2315936
    :cond_4
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-static {v0, v2}, LX/G3c;->a(LX/2uF;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I

    move-result v2

    .line 2315937
    if-eq v2, p1, :cond_5

    .line 2315938
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v1, v3, v2, v4}, LX/3Si;->c(LX/15i;II)LX/3Si;

    .line 2315939
    :cond_5
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    move-object v0, v1

    .line 2315940
    iput-object v0, p0, LX/G3c;->n:LX/2uF;

    .line 2315941
    iget-object v0, p0, LX/G3c;->n:LX/2uF;

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G3c;->m:I

    .line 2315942
    return-void

    .line 2315943
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_0

    .line 2315944
    :cond_7
    check-cast p1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;

    invoke-virtual {p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2315945
    if-eqz v0, :cond_8

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2315909
    iget-object v0, p0, LX/G3c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315910
    iget-object v0, p0, LX/G3c;->n:LX/2uF;

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2315911
    const-class v5, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v4, v3, v1, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 2315912
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 2315913
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2315914
    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2315966
    iget-object v0, p0, LX/G3c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315967
    invoke-direct {p0}, LX/G3c;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2315968
    const-string v0, ""

    .line 2315969
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2315970
    goto :goto_0

    .line 2315971
    :cond_1
    iget-object v0, p0, LX/G3c;->n:LX/2uF;

    iget v2, p0, LX/G3c;->l:I

    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2315903
    iget-object v0, p0, LX/G3c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315904
    invoke-direct {p0}, LX/G3c;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2315905
    const-string v0, ""

    .line 2315906
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2315907
    goto :goto_0

    .line 2315908
    :cond_1
    iget-object v0, p0, LX/G3c;->n:LX/2uF;

    iget v2, p0, LX/G3c;->l:I

    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2315897
    iget-object v0, p0, LX/G3c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315898
    invoke-direct {p0}, LX/G3c;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2315899
    const/4 v0, 0x0

    .line 2315900
    :goto_1
    return-object v0

    .line 2315901
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2315902
    :cond_1
    iget-object v0, p0, LX/G3c;->n:LX/2uF;

    iget v2, p0, LX/G3c;->l:I

    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v2, v0, v1, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    goto :goto_1
.end method

.method public final f()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 2315891
    iget-object v0, p0, LX/G3c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315892
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2315893
    iget-object v2, p0, LX/G3c;->n:LX/2uF;

    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2315894
    const-class v5, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v4, v3, v1, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2315895
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2315896
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2315887
    iget v0, p0, LX/G3c;->l:I

    iget v1, p0, LX/G3c;->m:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2315888
    iget v0, p0, LX/G3c;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G3c;->l:I

    .line 2315889
    return-void

    .line 2315890
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 2315886
    iget-object v0, p0, LX/G3c;->p:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
