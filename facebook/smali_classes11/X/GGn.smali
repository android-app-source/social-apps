.class public LX/GGn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;


# static fields
.field private static final k:LX/GGX;


# instance fields
.field private final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/GEG;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2334560
    invoke-static {}, LX/GGZ;->newBuilder()LX/GGZ;

    move-result-object v0

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2334561
    iput-object v1, v0, LX/GGZ;->a:LX/0Px;

    .line 2334562
    move-object v0, v0

    .line 2334563
    invoke-virtual {v0}, LX/GGZ;->a()LX/GGX;

    move-result-object v0

    sput-object v0, LX/GGn;->k:LX/GGX;

    return-void
.end method

.method public constructor <init>(LX/GEG;LX/GLd;LX/GEz;LX/GEY;LX/GEX;LX/GEy;LX/GEi;LX/GKj;LX/GIx;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334557
    iput-object p1, p0, LX/GGn;->j:LX/GEG;

    .line 2334558
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/GEZ;

    const v2, 0x7f030094

    sget-object v3, LX/GGn;->g:LX/GGX;

    sget-object v4, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v1, v2, p2, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030074

    sget-object v3, LX/GGn;->k:LX/GGX;

    sget-object v4, LX/8wK;->PACING:LX/8wK;

    invoke-direct {v1, v2, p8, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f03005c

    sget-object v3, LX/GGn;->c:LX/GGX;

    sget-object v4, LX/8wK;->FOOTER:LX/8wK;

    invoke-direct {v1, v2, p9, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GGn;->i:LX/0Px;

    .line 2334559
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1

    .prologue
    .line 2334555
    iget-object v0, p0, LX/GGn;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 11

    .prologue
    .line 2334550
    const-string v0, "page_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2334551
    invoke-static {p1}, LX/8wJ;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 2334552
    iget-object v2, p0, LX/GGn;->j:LX/GEG;

    sget-object v3, LX/8wL;->BOOST_LIVE:LX/8wL;

    .line 2334553
    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v4, v2

    move-object v5, v0

    move-object v6, v3

    move-object v7, p2

    move-object v8, v1

    invoke-virtual/range {v4 .. v10}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Ljava/lang/String;LX/8wL;LX/GCY;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2334554
    return-void
.end method
