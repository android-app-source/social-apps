.class public final enum LX/GxA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GxA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GxA;

.field public static final enum AUTHENTICATION_ERROR:LX/GxA;

.field public static final enum AUTHENTICATION_NETWORK_ERROR:LX/GxA;

.field public static final enum CONNECTION_ERROR:LX/GxA;

.field public static final enum INVALID_HTML_ERROR:LX/GxA;

.field public static final enum SITE_ERROR:LX/GxA;

.field public static final enum SSL_ERROR:LX/GxA;

.field public static final enum UNKNOWN_ERROR:LX/GxA;


# instance fields
.field private mErrorMessageId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2407750
    new-instance v0, LX/GxA;

    const-string v1, "AUTHENTICATION_NETWORK_ERROR"

    const v2, 0x7f083559

    invoke-direct {v0, v1, v4, v2}, LX/GxA;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GxA;->AUTHENTICATION_NETWORK_ERROR:LX/GxA;

    .line 2407751
    new-instance v0, LX/GxA;

    const-string v1, "AUTHENTICATION_ERROR"

    invoke-direct {v0, v1, v5}, LX/GxA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GxA;->AUTHENTICATION_ERROR:LX/GxA;

    .line 2407752
    new-instance v0, LX/GxA;

    const-string v1, "UNKNOWN_ERROR"

    const v2, 0x7f08355a

    invoke-direct {v0, v1, v6, v2}, LX/GxA;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GxA;->UNKNOWN_ERROR:LX/GxA;

    .line 2407753
    new-instance v0, LX/GxA;

    const-string v1, "SSL_ERROR"

    const v2, 0x7f083558

    invoke-direct {v0, v1, v7, v2}, LX/GxA;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GxA;->SSL_ERROR:LX/GxA;

    .line 2407754
    new-instance v0, LX/GxA;

    const-string v1, "CONNECTION_ERROR"

    const v2, 0x7f08003b

    invoke-direct {v0, v1, v8, v2}, LX/GxA;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GxA;->CONNECTION_ERROR:LX/GxA;

    .line 2407755
    new-instance v0, LX/GxA;

    const-string v1, "SITE_ERROR"

    const/4 v2, 0x5

    const v3, 0x7f08355a

    invoke-direct {v0, v1, v2, v3}, LX/GxA;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GxA;->SITE_ERROR:LX/GxA;

    .line 2407756
    new-instance v0, LX/GxA;

    const-string v1, "INVALID_HTML_ERROR"

    const/4 v2, 0x6

    const v3, 0x7f08355a

    invoke-direct {v0, v1, v2, v3}, LX/GxA;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GxA;->INVALID_HTML_ERROR:LX/GxA;

    .line 2407757
    const/4 v0, 0x7

    new-array v0, v0, [LX/GxA;

    sget-object v1, LX/GxA;->AUTHENTICATION_NETWORK_ERROR:LX/GxA;

    aput-object v1, v0, v4

    sget-object v1, LX/GxA;->AUTHENTICATION_ERROR:LX/GxA;

    aput-object v1, v0, v5

    sget-object v1, LX/GxA;->UNKNOWN_ERROR:LX/GxA;

    aput-object v1, v0, v6

    sget-object v1, LX/GxA;->SSL_ERROR:LX/GxA;

    aput-object v1, v0, v7

    sget-object v1, LX/GxA;->CONNECTION_ERROR:LX/GxA;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/GxA;->SITE_ERROR:LX/GxA;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GxA;->INVALID_HTML_ERROR:LX/GxA;

    aput-object v2, v0, v1

    sput-object v0, LX/GxA;->$VALUES:[LX/GxA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2407745
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2407746
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2407747
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2407748
    iput p3, p0, LX/GxA;->mErrorMessageId:I

    .line 2407749
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GxA;
    .locals 1

    .prologue
    .line 2407744
    const-class v0, LX/GxA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GxA;

    return-object v0
.end method

.method public static values()[LX/GxA;
    .locals 1

    .prologue
    .line 2407743
    sget-object v0, LX/GxA;->$VALUES:[LX/GxA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GxA;

    return-object v0
.end method


# virtual methods
.method public final getErrorMessageId()I
    .locals 1

    .prologue
    .line 2407742
    iget v0, p0, LX/GxA;->mErrorMessageId:I

    return v0
.end method
