.class public final LX/Ff1;
.super LX/DvC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;)V
    .locals 0

    .prologue
    .line 2266993
    iput-object p1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    invoke-direct {p0}, LX/DvC;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 11

    .prologue
    .line 2266994
    check-cast p1, LX/DvB;

    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 2266995
    iget-object v1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->v:LX/FZb;

    if-eqz v1, :cond_1

    .line 2266996
    goto :goto_1

    .line 2266997
    :cond_0
    :goto_0
    return-void

    .line 2266998
    :cond_1
    :goto_1
    iget-object v1, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    if-eqz v1, :cond_0

    .line 2266999
    const/4 v3, -0x1

    .line 2267000
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2267001
    iget-object v1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->w:LX/DxJ;

    .line 2267002
    iget-object v2, v1, LX/Dvb;->i:LX/Dvc;

    move-object v1, v2

    .line 2267003
    invoke-virtual {v1}, LX/Dvc;->d()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v8

    move v4, v0

    move-object v1, v6

    move v2, v0

    :goto_2
    if-ge v4, v8, :cond_4

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1U8;

    .line 2267004
    iget-object v9, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-interface {v0}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2267005
    instance-of v3, v0, LX/8IH;

    if-eqz v3, :cond_8

    invoke-interface {v0}, LX/1U8;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-interface {v0}, LX/1U8;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v9, 0x1eaef984

    if-ne v3, v9, :cond_8

    move-object v1, v0

    .line 2267006
    check-cast v1, LX/8IH;

    invoke-interface {v1}, LX/8IH;->z()Ljava/lang/String;

    move-result-object v1

    move v3, v2

    .line 2267007
    :cond_2
    :goto_3
    invoke-interface {v0}, LX/1U8;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-interface {v0}, LX/1U8;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v9

    const v10, 0x4984e12

    if-ne v9, v10, :cond_3

    .line 2267008
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2267009
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 2267010
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 2267011
    :cond_4
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2267012
    new-instance v6, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2267013
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2267014
    iget-object v0, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->i:LX/CvY;

    iget-object v1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267015
    iget-object v2, v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2267016
    move-object v1, v2

    .line 2267017
    sget-object v2, LX/8ch;->CLICK:LX/8ch;

    iget-object v4, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v4, v4, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->D:Ljava/util/Map;

    iget-object v5, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v4

    iget-object v5, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267018
    iget-object v7, v5, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v7, v7

    .line 2267019
    move-object v5, v7

    .line 2267020
    iget-object v7, p1, LX/DvB;->a:Ljava/lang/String;

    .line 2267021
    sget-object v8, LX/0Rg;->a:LX/0Rg;

    move-object v8, v8

    .line 2267022
    invoke-static {v5, v3, v7, v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2267023
    iget-object v0, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->r:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v6, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2267024
    :cond_5
    iget-object v0, p1, LX/DvB;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_6

    .line 2267025
    iget-object v0, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->i:LX/CvY;

    iget-object v1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267026
    iget-object v2, v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2267027
    move-object v1, v2

    .line 2267028
    sget-object v2, LX/8ch;->CLICK:LX/8ch;

    iget-object v4, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v4, v4, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->D:Ljava/util/Map;

    iget-object v5, p1, LX/DvB;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v4

    iget-object v5, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267029
    iget-object v6, v5, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v6, v6

    .line 2267030
    move-object v5, v6

    .line 2267031
    iget-object v6, p1, LX/DvB;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v6

    .line 2267032
    sget-object v7, LX/0Rg;->a:LX/0Rg;

    move-object v7, v7

    .line 2267033
    invoke-static {v5, v3, v6, v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2267034
    iget-object v0, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->p:LX/D3w;

    iget-object v1, p1, LX/DvB;->f:Lcom/facebook/graphql/model/GraphQLVideo;

    iget-object v2, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/04D;->RESULTS_PAGE_MIXED_MEDIA:LX/04D;

    invoke-virtual {v0, v1, v2, v3}, LX/D3w;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;)V

    goto/16 :goto_0

    .line 2267035
    :cond_6
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v5

    .line 2267036
    iget-object v0, p1, LX/DvB;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2267037
    const-string v0, "annotation_type"

    iget-object v1, p1, LX/DvB;->g:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2267038
    :cond_7
    iget-object v0, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->i:LX/CvY;

    iget-object v1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267039
    iget-object v2, v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2267040
    move-object v1, v2

    .line 2267041
    sget-object v2, LX/8ch;->CLICK:LX/8ch;

    iget-object v4, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v4, v4, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->D:Ljava/util/Map;

    iget-object v8, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v4

    iget-object v8, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267042
    iget-object v9, v8, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v9, v9

    .line 2267043
    move-object v8, v9

    .line 2267044
    iget-object v9, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-virtual {v5}, LX/0P2;->b()LX/0P1;

    move-result-object v5

    invoke-static {v8, v3, v9, v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2267045
    iget-object v0, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    iget-object v1, p0, LX/Ff1;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/9hF;->c(LX/0Px;)LX/9hE;

    move-result-object v2

    iget-object v3, p1, LX/DvB;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v2

    iget-object v3, p1, LX/DvB;->b:Landroid/net/Uri;

    invoke-static {v3}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v2

    sget-object v3, LX/74S;->SEARCH_PHOTO_RESULTS_PAGE:LX/74S;

    invoke-virtual {v2, v3}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v2

    const/4 v3, 0x1

    .line 2267046
    iput-boolean v3, v2, LX/9hD;->C:Z

    .line 2267047
    move-object v2, v2

    .line 2267048
    invoke-virtual {v2}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v2

    invoke-interface {v0, v1, v2, v6}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    goto/16 :goto_0

    :cond_8
    move v3, v2

    goto/16 :goto_3
.end method
