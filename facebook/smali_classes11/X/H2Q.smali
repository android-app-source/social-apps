.class public LX/H2Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutParams;",
        "Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2417223
    const-class v0, LX/H2Q;

    sput-object v0, LX/H2Q;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2417230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2417231
    iput-object p1, p0, LX/H2Q;->b:LX/0SG;

    .line 2417232
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2417233
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2417234
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "places"

    const-string v2, "{result=search_places:$..result_sections.edges..node.results.edges..node.id}"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2417235
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "hash"

    const-string v2, "{result=search_places:$..search_session_id}"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2417236
    new-instance v0, LX/14N;

    const-string v1, "xml_places"

    const-string v2, "GET"

    const-string v3, "nearbyandroidlayouts"

    sget-object v5, LX/14S;->JSONPARSER:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2417224
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2417225
    const-class v1, LX/H25;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H25;

    .line 2417226
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2417227
    iget-object v0, v0, LX/H25;->layouts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H28;

    .line 2417228
    iget-object v3, v0, LX/H28;->id:Ljava/lang/String;

    iget-object v0, v0, LX/H28;->layout:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2417229
    :cond_0
    new-instance v0, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v3, p0, LX/H2Q;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v0, v2, v4, v5, v1}, Lcom/facebook/nearby/protocol/FetchNearbyPlacesLayoutResult;-><init>(LX/0ta;JLjava/util/Map;)V

    return-object v0
.end method
