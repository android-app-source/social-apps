.class public LX/GRa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GRa;


# instance fields
.field public final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2351632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351633
    iput-object p1, p0, LX/GRa;->a:LX/0tX;

    .line 2351634
    return-void
.end method

.method public static a(LX/0QB;)LX/GRa;
    .locals 4

    .prologue
    .line 2351603
    sget-object v0, LX/GRa;->b:LX/GRa;

    if-nez v0, :cond_1

    .line 2351604
    const-class v1, LX/GRa;

    monitor-enter v1

    .line 2351605
    :try_start_0
    sget-object v0, LX/GRa;->b:LX/GRa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2351606
    if-eqz v2, :cond_0

    .line 2351607
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2351608
    new-instance p0, LX/GRa;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/GRa;-><init>(LX/0tX;)V

    .line 2351609
    move-object v0, p0

    .line 2351610
    sput-object v0, LX/GRa;->b:LX/GRa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2351611
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2351612
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2351613
    :cond_1
    sget-object v0, LX/GRa;->b:LX/GRa;

    return-object v0

    .line 2351614
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2351615
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2351623
    new-instance v0, LX/4Ct;

    invoke-direct {v0}, LX/4Ct;-><init>()V

    .line 2351624
    const-string v1, "user_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351625
    move-object v0, v0

    .line 2351626
    const-string v1, "application_id"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351627
    move-object v0, v0

    .line 2351628
    new-instance v1, LX/GS2;

    invoke-direct {v1}, LX/GS2;-><init>()V

    move-object v1, v1

    .line 2351629
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/GS2;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2351630
    iget-object v1, p0, LX/GRa;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2351631
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2351616
    new-instance v0, LX/4Cs;

    invoke-direct {v0}, LX/4Cs;-><init>()V

    .line 2351617
    const-string v1, "application_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351618
    move-object v0, v0

    .line 2351619
    new-instance v1, LX/GS1;

    invoke-direct {v1}, LX/GS1;-><init>()V

    move-object v1, v1

    .line 2351620
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/GS1;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2351621
    iget-object v1, p0, LX/GRa;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2351622
    return-void
.end method
