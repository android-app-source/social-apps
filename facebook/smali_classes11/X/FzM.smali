.class public LX/FzM;
.super LX/FzJ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FzJ",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadInferenceGraphQLModels$ProfileInfoTypeaheadInferenceQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/03V;

.field private final b:LX/0lB;


# direct methods
.method public constructor <init>(LX/9kE;LX/0tX;LX/03V;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2307836
    invoke-direct {p0, p1, p2}, LX/FzJ;-><init>(LX/9kE;LX/0tX;)V

    .line 2307837
    iput-object p3, p0, LX/FzM;->a:LX/03V;

    .line 2307838
    iput-object p4, p0, LX/FzM;->b:LX/0lB;

    .line 2307839
    return-void
.end method

.method public static a(LX/FzM;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2307840
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2307841
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2307842
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 2307843
    :try_start_0
    iget-object v4, p0, LX/FzM;->b:LX/0lB;

    invoke-virtual {v4}, LX/0lD;->b()LX/0lp;

    move-result-object v4

    sget-object v5, LX/1pL;->UTF8:LX/1pL;

    invoke-virtual {v4, v3, v5}, LX/0lp;->a(Ljava/io/OutputStream;LX/1pL;)LX/0nX;

    move-result-object v4

    .line 2307844
    invoke-virtual {v4}, LX/0nX;->f()V

    .line 2307845
    const-string v5, "profile_section"

    const-string v6, "profile_section"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307846
    const-string v5, "page_ids"

    invoke-virtual {v4, v5}, LX/0nX;->f(Ljava/lang/String;)V

    .line 2307847
    const-string v5, "typeahead_existing_fields_page_id"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2307848
    invoke-virtual {v4}, LX/0nX;->e()V

    .line 2307849
    invoke-virtual {v4}, LX/0nX;->g()V

    .line 2307850
    invoke-virtual {v4}, LX/0nX;->close()V

    .line 2307851
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-direct {v0, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2307852
    :catch_0
    iget-object v0, p0, LX/FzM;->a:LX/03V;

    const-string v1, "identitygrowth"

    const-string v2, "Failed to fetch typeahead inference graphql results because of error processing json"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307853
    const/4 v0, 0x0

    .line 2307854
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_1
.end method
