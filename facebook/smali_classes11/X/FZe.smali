.class public LX/FZe;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final F:Ljava/lang/String;

.field private static final G:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/CwI;",
            "LX/8ci;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cyo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:Z

.field private final D:LX/EQ3;

.field public E:Landroid/os/Bundle;

.field public final a:Landroid/content/Context;

.field public final b:LX/17W;

.field public final c:LX/1nG;

.field public final d:LX/FZW;

.field public final e:LX/FZX;

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2d1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:LX/0gc;

.field private final k:LX/Cvy;

.field private final l:LX/2Sc;

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:LX/Cve;

.field private o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FgF;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FfW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final r:LX/0ad;

.field public final s:LX/0Uh;

.field private final t:LX/Ffl;

.field public final u:LX/2Sd;

.field private v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FgG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

.field public x:LX/CwB;

.field public y:Lcom/facebook/search/api/GraphSearchQuery;

.field public z:LX/8ci;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2258015
    const-class v0, LX/FZe;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FZe;->F:Ljava/lang/String;

    .line 2258016
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/CwI;->BOOTSTRAP:LX/CwI;

    sget-object v2, LX/8ci;->a:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->SUGGESTION:LX/CwI;

    sget-object v2, LX/8ci;->a:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->SINGLE_STATE:LX/CwI;

    sget-object v2, LX/8ci;->b:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->RECENT_SEARCHES:LX/CwI;

    sget-object v2, LX/8ci;->f:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    sget-object v2, LX/8ci;->c:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->ECHO:LX/CwI;

    sget-object v2, LX/8ci;->d:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->ESCAPE:LX/CwI;

    sget-object v2, LX/8ci;->e:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->TRENDING_ENTITY:LX/CwI;

    sget-object v2, LX/8ci;->g:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->NULL_STATE_MODULE:LX/CwI;

    sget-object v2, LX/8ci;->v:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->SS_SEE_MORE_LINK:LX/CwI;

    sget-object v2, LX/8ci;->F:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/CwI;->SS_SEE_MORE_BUTTON:LX/CwI;

    sget-object v2, LX/8ci;->G:LX/8ci;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/FZe;->G:LX/0P1;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/17W;LX/1nG;LX/FZW;LX/FZX;LX/0gc;LX/Cvy;LX/2Sc;LX/Cve;LX/0ad;LX/0Uh;LX/Ffl;LX/2Sd;LX/EQ3;)V
    .locals 1
    .param p4    # LX/FZW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/FZX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2257976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2257977
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257978
    iput-object v0, p0, LX/FZe;->f:LX/0Ot;

    .line 2257979
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257980
    iput-object v0, p0, LX/FZe;->g:LX/0Ot;

    .line 2257981
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257982
    iput-object v0, p0, LX/FZe;->h:LX/0Ot;

    .line 2257983
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257984
    iput-object v0, p0, LX/FZe;->i:LX/0Ot;

    .line 2257985
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257986
    iput-object v0, p0, LX/FZe;->m:LX/0Ot;

    .line 2257987
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257988
    iput-object v0, p0, LX/FZe;->o:LX/0Ot;

    .line 2257989
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257990
    iput-object v0, p0, LX/FZe;->p:LX/0Ot;

    .line 2257991
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257992
    iput-object v0, p0, LX/FZe;->q:LX/0Ot;

    .line 2257993
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257994
    iput-object v0, p0, LX/FZe;->v:LX/0Ot;

    .line 2257995
    sget-object v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iput-object v0, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2257996
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2257997
    iput-object v0, p0, LX/FZe;->B:LX/0Ot;

    .line 2257998
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FZe;->C:Z

    .line 2257999
    const/4 v0, 0x0

    iput-object v0, p0, LX/FZe;->E:Landroid/os/Bundle;

    .line 2258000
    iput-object p1, p0, LX/FZe;->a:Landroid/content/Context;

    .line 2258001
    iput-object p2, p0, LX/FZe;->b:LX/17W;

    .line 2258002
    iput-object p3, p0, LX/FZe;->c:LX/1nG;

    .line 2258003
    iput-object p4, p0, LX/FZe;->d:LX/FZW;

    .line 2258004
    iput-object p5, p0, LX/FZe;->e:LX/FZX;

    .line 2258005
    iput-object p6, p0, LX/FZe;->j:LX/0gc;

    .line 2258006
    iput-object p7, p0, LX/FZe;->k:LX/Cvy;

    .line 2258007
    iput-object p8, p0, LX/FZe;->l:LX/2Sc;

    .line 2258008
    iput-object p9, p0, LX/FZe;->n:LX/Cve;

    .line 2258009
    iput-object p10, p0, LX/FZe;->r:LX/0ad;

    .line 2258010
    iput-object p11, p0, LX/FZe;->s:LX/0Uh;

    .line 2258011
    iput-object p12, p0, LX/FZe;->t:LX/Ffl;

    .line 2258012
    iput-object p13, p0, LX/FZe;->u:LX/2Sd;

    .line 2258013
    iput-object p14, p0, LX/FZe;->D:LX/EQ3;

    .line 2258014
    return-void
.end method

.method private static a(LX/CwI;Ljava/lang/String;)LX/8ci;
    .locals 1

    .prologue
    .line 2257970
    sget-object v0, LX/CwI;->ECHO:LX/CwI;

    if-ne p0, v0, :cond_0

    .line 2257971
    sget-object v0, LX/8ci;->d:LX/8ci;

    .line 2257972
    :goto_0
    return-object v0

    .line 2257973
    :cond_0
    sget-object v0, LX/CwI;->SUGGESTION:LX/CwI;

    if-ne p0, v0, :cond_1

    sget-object v0, LX/7BZ;->SINGLE_STATE_MODE:LX/7BZ;

    invoke-virtual {v0}, LX/7BZ;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2257974
    sget-object v0, LX/8ci;->b:LX/8ci;

    goto :goto_0

    .line 2257975
    :cond_1
    sget-object v0, LX/8ci;->a:LX/8ci;

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;LX/CwB;Landroid/os/Bundle;)LX/Fdv;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2257956
    invoke-static {p2}, LX/CwC;->b(LX/CwB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2257957
    invoke-interface {p2}, LX/CwB;->h()LX/0P1;

    move-result-object v0

    sget-object v1, LX/7B4;->MARKETPLACE:LX/7B4;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2257958
    iget-object v1, p0, LX/FZe;->d:LX/FZW;

    .line 2257959
    sget-object v2, LX/FZc;->REACT_NATIVE:LX/FZc;

    invoke-static {v1, v2, v0}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;

    move-object v0, v2

    .line 2257960
    :goto_0
    return-object v0

    .line 2257961
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne p1, v0, :cond_1

    .line 2257962
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/FZW;->b(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    move-result-object v0

    goto :goto_0

    .line 2257963
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne p1, v0, :cond_2

    .line 2257964
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0, p3}, LX/FZW;->f(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    move-result-object v0

    goto :goto_0

    .line 2257965
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne p1, v0, :cond_3

    .line 2257966
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0, p3, v1}, LX/FZW;->a(Landroid/os/Bundle;Z)Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    move-result-object v0

    goto :goto_0

    .line 2257967
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne p1, v0, :cond_4

    .line 2257968
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0, p3, v1}, LX/FZW;->a(Landroid/os/Bundle;Z)Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    move-result-object v0

    goto :goto_0

    .line 2257969
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t find child fragment for display style: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Landroid/os/Bundle;)LX/Fdv;
    .locals 4

    .prologue
    .line 2257938
    iget-object v0, p0, LX/FZe;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nD;

    .line 2257939
    iget-object v1, v0, LX/1nD;->g:LX/0P1;

    move-object v0, v1

    .line 2257940
    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2257941
    invoke-static {v0}, LX/0jj;->a(I)LX/0cQ;

    move-result-object v0

    .line 2257942
    if-nez v0, :cond_0

    .line 2257943
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0, p2}, LX/FZW;->a(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    move-result-object v0

    .line 2257944
    :goto_0
    return-object v0

    .line 2257945
    :cond_0
    sget-object v1, LX/FZd;->b:[I

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2257946
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t find child fragment for fragment type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0cQ;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2257947
    :pswitch_0
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0, p2}, LX/FZW;->f(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    move-result-object v0

    goto :goto_0

    .line 2257948
    :pswitch_1
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0, p2}, LX/FZW;->a(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    move-result-object v0

    goto :goto_0

    .line 2257949
    :pswitch_2
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    .line 2257950
    sget-object v1, LX/FZc;->RESULTS_PLACES:LX/FZc;

    invoke-static {v0, v1, p2}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    move-object v0, v1

    .line 2257951
    goto :goto_0

    .line 2257952
    :pswitch_3
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0, p2}, LX/FZW;->b(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    move-result-object v0

    goto :goto_0

    .line 2257953
    :pswitch_4
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    .line 2257954
    sget-object v1, LX/FZc;->RESULTS:LX/FZc;

    invoke-static {v0, v1, p2}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/fragment/SearchResultsFragment;

    move-object v0, v1

    .line 2257955
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(LX/FZe;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FZe;",
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2d1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1vC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FgF;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FfW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FgG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cyo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2257937
    iput-object p1, p0, LX/FZe;->f:LX/0Ot;

    iput-object p2, p0, LX/FZe;->g:LX/0Ot;

    iput-object p3, p0, LX/FZe;->h:LX/0Ot;

    iput-object p4, p0, LX/FZe;->i:LX/0Ot;

    iput-object p5, p0, LX/FZe;->m:LX/0Ot;

    iput-object p6, p0, LX/FZe;->o:LX/0Ot;

    iput-object p7, p0, LX/FZe;->p:LX/0Ot;

    iput-object p8, p0, LX/FZe;->q:LX/0Ot;

    iput-object p9, p0, LX/FZe;->v:LX/0Ot;

    iput-object p10, p0, LX/FZe;->B:LX/0Ot;

    return-void
.end method

.method public static a(LX/FZe;LX/CwB;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;ZZ)V
    .locals 10
    .param p1    # LX/CwB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2257875
    iget-object v0, p0, LX/FZe;->n:LX/Cve;

    iget-object v1, p3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, LX/Cve;->a(LX/CwB;Ljava/lang/String;)V

    .line 2257876
    invoke-interface {p1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2257877
    sget-object v0, LX/8ci;->p:LX/8ci;

    if-ne p2, v0, :cond_9

    const/4 v0, 0x1

    move v8, v0

    .line 2257878
    :goto_0
    if-eqz p5, :cond_0

    if-eqz v8, :cond_a

    :cond_0
    const/4 v0, 0x1

    .line 2257879
    :goto_1
    invoke-interface {p1}, LX/CwB;->i()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_e

    if-eqz v0, :cond_e

    .line 2257880
    const/4 v0, 0x0

    .line 2257881
    invoke-static {p1}, LX/Ffl;->b(LX/CwB;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/FZe;->s:LX/0Uh;

    sget v2, LX/2SU;->D:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/FZe;->r:LX/0ad;

    .line 2257882
    invoke-static {p1}, LX/CwC;->a(LX/CwB;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p1}, LX/CwC;->b(LX/CwB;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/1nE;->a(LX/0ad;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    :cond_1
    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 2257883
    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    move v0, v0

    .line 2257884
    if-eqz v0, :cond_3

    .line 2257885
    iget-object v0, p0, LX/FZe;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgG;

    .line 2257886
    iget-object v1, v0, LX/FgG;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2257887
    iget-object v1, v0, LX/FgG;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/search/results/loader/modules/SearchResultsObjectInitializer$1;

    invoke-direct {v2, v0}, Lcom/facebook/search/results/loader/modules/SearchResultsObjectInitializer$1;-><init>(LX/FgG;)V

    const v3, 0x327b83e8

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2257888
    :cond_3
    iget-object v0, p0, LX/FZe;->t:LX/Ffl;

    invoke-virtual {v0, p1}, LX/Ffl;->a(LX/CwB;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyI;

    .line 2257889
    iget-object v1, p0, LX/FZe;->q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FfW;

    invoke-virtual {v1, v0}, LX/FfW;->a(LX/CyI;)LX/FfQ;

    move-result-object v0

    .line 2257890
    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object v2

    .line 2257891
    instance-of v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v1, :cond_10

    move-object v1, p1

    .line 2257892
    check-cast v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v1

    .line 2257893
    sget-object v3, LX/CwF;->trending:LX/CwF;

    if-eq v1, v3, :cond_4

    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7BG;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2257894
    :cond_4
    const-string v1, "news_v2"

    .line 2257895
    :goto_3
    move-object v1, v1

    .line 2257896
    invoke-virtual {v0, p1, v1}, LX/FfQ;->c(LX/CwB;Ljava/lang/String;)LX/CwA;

    move-result-object v0

    invoke-interface {v0}, LX/CwA;->a()LX/CwB;

    move-result-object v0

    move-object v7, v0

    .line 2257897
    :goto_4
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 2257898
    new-instance v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;-><init>()V

    .line 2257899
    iget-object v0, p0, LX/FZe;->s:LX/0Uh;

    iget-object v2, p0, LX/FZe;->r:LX/0ad;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/0Uh;LX/0ad;Landroid/os/Bundle;)V

    .line 2257900
    invoke-virtual {v1, v7, p3, p2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2257901
    const/4 v0, 0x0

    .line 2257902
    if-eqz p5, :cond_11

    iget-object v2, p0, LX/FZe;->r:LX/0ad;

    sget-short v3, LX/100;->a:S

    invoke-interface {v2, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 2257903
    :cond_5
    :goto_5
    move v0, v0

    .line 2257904
    if-eqz v0, :cond_6

    .line 2257905
    iget-object v0, p0, LX/FZe;->r:LX/0ad;

    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/1nE;->a(LX/0ad;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2257906
    iget-object v0, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    move-object v2, v0

    .line 2257907
    const-string v0, "early_fetch_view_id"

    invoke-virtual {v9, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257908
    iget-object v0, p0, LX/FZe;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyo;

    invoke-virtual {v0, v2}, LX/Cyo;->a(Ljava/lang/String;)LX/Cyn;

    move-result-object v0

    .line 2257909
    invoke-virtual {v0}, LX/Cyn;->h()V

    .line 2257910
    const/4 v2, 0x0

    .line 2257911
    iput-object v2, v0, LX/Cyn;->s:LX/Fdy;

    .line 2257912
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/Cyn;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/0Px;)V

    .line 2257913
    :cond_6
    :goto_6
    const-string v0, "display_style"

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257914
    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2257915
    const-string v0, "browse_session_id"

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257916
    :cond_7
    const-string v0, "search_theme"

    iget-object v1, p0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/7BH;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2257917
    if-nez v8, :cond_d

    if-eqz p5, :cond_d

    iget-object v0, p0, LX/FZe;->r:LX/0ad;

    sget-short v1, LX/100;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2257918
    const-string v0, "typeahead_session_id"

    iget-object v1, p3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257919
    const-string v0, "candidate_session_id"

    iget-object v1, p3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257920
    const-string v0, "query_title"

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257921
    const-string v0, "query_function"

    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257922
    const-string v0, "query_vertical"

    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257923
    const-string v1, "source"

    if-eqz p2, :cond_c

    invoke-virtual {p2}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_7
    invoke-virtual {v9, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257924
    const-string v0, "use_open_search_bar"

    const/4 v1, 0x1

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2257925
    iget-object v0, p0, LX/FZe;->E:Landroid/os/Bundle;

    if-eqz v0, :cond_8

    .line 2257926
    const-string v0, "results_query_type"

    iget-object v1, p0, LX/FZe;->E:Landroid/os/Bundle;

    const-string v2, "results_query_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257927
    const-string v0, "results_query_role"

    iget-object v1, p0, LX/FZe;->E:Landroid/os/Bundle;

    const-string v2, "results_query_role"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257928
    :cond_8
    invoke-direct {p0, v6, v9}, LX/FZe;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Landroid/os/Bundle;)LX/Fdv;

    move-result-object v0

    .line 2257929
    :goto_8
    invoke-interface {v0, v7, p3, p2}, LX/Fdv;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2257930
    iget-object v1, p0, LX/FZe;->e:LX/FZX;

    invoke-interface {v0}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/FZX;->b(Landroid/support/v4/app/Fragment;)V

    .line 2257931
    return-void

    .line 2257932
    :cond_9
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_0

    .line 2257933
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2257934
    :cond_b
    iget-object v0, p0, LX/FZe;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgF;

    iget-object v2, p3, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v3

    move-object v4, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/FgF;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;LX/8ci;Z)V

    goto/16 :goto_6

    .line 2257935
    :cond_c
    const/4 v0, 0x0

    goto :goto_7

    .line 2257936
    :cond_d
    invoke-direct {p0, v6, p1, v9}, LX/FZe;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;LX/CwB;Landroid/os/Bundle;)LX/Fdv;

    move-result-object v0

    goto :goto_8

    :cond_e
    move-object v7, p1

    goto/16 :goto_4

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_10
    move-object v1, v2

    goto/16 :goto_3

    :cond_11
    invoke-static {p1}, LX/Ffl;->b(LX/CwB;)Z

    move-result v2

    if-nez v2, :cond_5

    const/4 v0, 0x1

    goto/16 :goto_5
.end method

.method private static a(LX/FZe;Lcom/facebook/search/model/KeywordTypeaheadUnit;Ljava/lang/String;LX/8ci;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2257868
    new-instance v0, Lcom/facebook/search/model/ReactionSearchData;

    move-object v2, v1

    move-object v3, p4

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/search/model/ReactionSearchData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2257869
    invoke-static {p1}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v1

    .line 2257870
    iput-object v0, v1, LX/CwH;->k:Lcom/facebook/search/model/ReactionSearchData;

    .line 2257871
    move-object v0, v1

    .line 2257872
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v1

    .line 2257873
    iget-object v3, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    invoke-static {v1}, LX/FZe;->b(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z

    move-result v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p3

    invoke-static/range {v0 .. v5}, LX/FZe;->a(LX/FZe;LX/CwB;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;ZZ)V

    .line 2257874
    return-void
.end method

.method private static a(LX/FZe;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2257720
    iget-object v0, p0, LX/FZe;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nD;

    iget-object v6, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v5, p5

    .line 2257721
    invoke-static {v0, v1, v4, v5, v6}, LX/1nD;->a(LX/1nD;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object p1

    const-string p2, "reaction_session_id"

    invoke-virtual {p1, p2, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const-string p2, "semantic"

    invoke-virtual {p1, p2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    move-object v1, p1

    .line 2257722
    iget-object v0, p0, LX/FZe;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2b67

    iget-object v3, p0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v3}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2257723
    return-void
.end method

.method public static a(LX/FZe;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/89z;)V
    .locals 4

    .prologue
    .line 2257862
    const v0, 0x25d6af

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2257863
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2257864
    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1, p2, v2}, LX/5ve;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2257865
    const-string v2, "page_view_referrer"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2257866
    iget-object v2, p0, LX/FZe;->b:LX/17W;

    iget-object v3, p0, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2257867
    return-void
.end method

.method public static a(LX/FZe;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2258017
    iget-object v0, p0, LX/FZe;->a:Landroid/content/Context;

    const/4 v2, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2258018
    iget-object v0, p0, LX/FZe;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/FZe;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2258019
    return-void
.end method

.method private a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Ljava/lang/String;Ljava/lang/String;LX/CwI;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 2257849
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2257850
    invoke-static/range {p4 .. p5}, LX/FZe;->b(LX/CwI;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2257851
    invoke-static/range {p4 .. p5}, LX/FZe;->a(LX/CwI;Ljava/lang/String;)LX/8ci;

    move-result-object v11

    .line 2257852
    iget-object v0, p0, LX/FZe;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vC;

    const v3, 0x1e0002

    invoke-virtual {v0, v3, v1, v2}, LX/1vC;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2257853
    iget-object v0, p0, LX/FZe;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vC;

    const v3, 0x1e000a

    invoke-virtual {v0, v3, v1, v2}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2257854
    iget-object v0, p0, LX/FZe;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfW;

    new-instance v3, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v3}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    invoke-virtual {v3, p3}, Lcom/facebook/reaction/ReactionQueryParams;->a(Ljava/lang/String;)Lcom/facebook/reaction/ReactionQueryParams;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/CfW;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    .line 2257855
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0}, LX/FZW;->c()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v0

    iput-object v0, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2257856
    iget-object v0, p0, LX/FZe;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIs;

    const/4 v3, 0x0

    invoke-virtual {v11}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v4, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    if-nez v4, :cond_0

    const/4 v8, 0x0

    :goto_0
    iget-object v4, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    if-nez v4, :cond_1

    const/4 v9, 0x0

    :goto_1
    const-string v10, "content"

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v10}, LX/CIs;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2257857
    iget-object v0, p0, LX/FZe;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2d1;

    invoke-virtual {v0}, LX/2d1;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v3, p0

    move-object v4, p1

    move-object v5, v1

    move-object v6, v11

    move-object v7, p3

    move-object v8, v2

    .line 2257858
    invoke-static/range {v3 .. v8}, LX/FZe;->a(LX/FZe;Lcom/facebook/search/model/KeywordTypeaheadUnit;Ljava/lang/String;LX/8ci;Ljava/lang/String;Ljava/lang/String;)V

    .line 2257859
    :goto_2
    return-void

    .line 2257860
    :cond_0
    iget-object v4, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v8, v4, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v4, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v9, v4, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v3, p0

    move-object v4, p2

    move-object v5, v1

    move-object v6, v11

    move-object v7, p3

    move-object v8, v2

    .line 2257861
    invoke-static/range {v3 .. v8}, LX/FZe;->a(LX/FZe;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private a(Landroid/support/v4/app/Fragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2257841
    instance-of v1, p1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    if-eqz v1, :cond_1

    check-cast p1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    .line 2257842
    iget-object v1, p1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;->q:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-static {v1}, LX/Ffl;->b(LX/CwB;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2257843
    if-eqz v1, :cond_1

    .line 2257844
    iget-object v1, p0, LX/FZe;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2d1;

    invoke-virtual {v1}, LX/2d1;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/FZe;->z:LX/8ci;

    sget-object v2, LX/8ci;->q:LX/8ci;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/FZe;->z:LX/8ci;

    sget-object v2, LX/8ci;->A:LX/8ci;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, LX/FZe;->z:LX/8ci;

    sget-object v2, LX/8ci;->B:LX/8ci;

    if-eq v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2257845
    if-nez v1, :cond_1

    .line 2257846
    :cond_0
    :goto_2
    return v0

    .line 2257847
    :cond_1
    iget-object v1, p0, LX/FZe;->z:LX/8ci;

    sget-object v2, LX/8ci;->l:LX/8ci;

    if-eq v1, v2, :cond_0

    .line 2257848
    iget-object v1, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v1, v1, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static b(LX/CwI;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .prologue
    .line 2257835
    sget-object v0, LX/CwI;->ECHO:LX/CwI;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/CwI;->SUGGESTION:LX/CwI;

    if-ne p0, v0, :cond_1

    sget-object v0, LX/7BZ;->SINGLE_STATE_MODE:LX/7BZ;

    invoke-virtual {v0}, LX/7BZ;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2257836
    :cond_0
    const-string v0, "ANDROID_SEARCH_LOCAL_SINGLE"

    .line 2257837
    :goto_0
    return-object v0

    .line 2257838
    :cond_1
    sget-object v0, LX/CwI;->RECENT_SEARCHES:LX/CwI;

    if-ne p0, v0, :cond_2

    .line 2257839
    const-string v0, "ANDROID_SEARCH_LOCAL_RECENT"

    goto :goto_0

    .line 2257840
    :cond_2
    const-string v0, "ANDROID_SEARCH_LOCAL_TYPEAHEAD"

    goto :goto_0
.end method

.method public static b(LX/FZe;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 2257821
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x285feb

    if-ne v0, v1, :cond_0

    .line 2257822
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2257823
    invoke-virtual {p4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p2, v1, p3}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2257824
    iget-object v1, p0, LX/FZe;->c:LX/1nG;

    invoke-virtual {v1, p1, p2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2257825
    iget-object v2, p0, LX/FZe;->b:LX/17W;

    iget-object v3, p0, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2257826
    :goto_0
    return-void

    .line 2257827
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x41e065f

    if-ne v0, v1, :cond_1

    .line 2257828
    new-instance v0, LX/5QQ;

    invoke-direct {v0}, LX/5QQ;-><init>()V

    invoke-virtual {v0, p2}, LX/5QQ;->a(Ljava/lang/String;)LX/5QQ;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/5QQ;->b(Ljava/lang/String;)LX/5QQ;

    move-result-object v0

    .line 2257829
    iget-object v1, v0, LX/5QQ;->a:Landroid/os/Bundle;

    move-object v0, v1

    .line 2257830
    iget-object v1, p0, LX/FZe;->c:LX/1nG;

    invoke-virtual {v1, p1, p2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2257831
    iget-object v2, p0, LX/FZe;->b:LX/17W;

    iget-object v3, p0, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2257832
    goto :goto_0

    .line 2257833
    :cond_1
    iget-object v0, p0, LX/FZe;->c:LX/1nG;

    invoke-virtual {v0, p1, p2}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2257834
    iget-object v1, p0, LX/FZe;->b:LX/17W;

    iget-object v2, p0, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public static b(LX/FZe;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2257816
    const v0, 0x403827a

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2257817
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2257818
    sget-object v2, Lcom/facebook/events/common/ActionSource;->GRAPH_SEARCH:Lcom/facebook/events/common/ActionSource;

    invoke-static {v1, v2}, Lcom/facebook/events/common/ActionSource;->putActionRef(Landroid/os/Bundle;Lcom/facebook/events/common/ActionSource;)V

    .line 2257819
    iget-object v2, p0, LX/FZe;->b:LX/17W;

    iget-object v3, p0, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2257820
    return-void
.end method

.method private static b(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z
    .locals 2

    .prologue
    .line 2257815
    invoke-static {p0}, LX/EQ3;->b(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/EQ2;

    move-result-object v0

    sget-object v1, LX/EQ2;->USE_SERP_ENDPOINT:LX/EQ2;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/model/EntityTypeaheadUnit;)V
    .locals 4
    .param p1    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2257783
    invoke-static {p1}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2257784
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v0

    .line 2257785
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v0

    .line 2257786
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    move-object v3, v0

    .line 2257787
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 2257788
    if-eqz v0, :cond_0

    .line 2257789
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 2257790
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {p0, v1, v2, v3, v0}, LX/FZe;->a(LX/FZe;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2257791
    :goto_1
    return-void

    .line 2257792
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2257793
    :cond_1
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->u:Ljava/lang/String;

    move-object v0, v0

    .line 2257794
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2257795
    iget-object v0, p0, LX/FZe;->b:LX/17W;

    iget-object v1, p0, LX/FZe;->a:Landroid/content/Context;

    .line 2257796
    iget-object v2, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->u:Ljava/lang/String;

    move-object v2, v2

    .line 2257797
    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    .line 2257798
    :cond_2
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2257799
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x25d6af

    if-eq v0, v1, :cond_3

    .line 2257800
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2257801
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x499e8e7

    if-ne v0, v1, :cond_4

    .line 2257802
    :cond_3
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2257803
    iget-object v1, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2257804
    iget-object v2, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2257805
    sget-object v3, LX/89z;->SEARCH_TYPEAHEAD:LX/89z;

    invoke-static {p0, v0, v1, v2, v3}, LX/FZe;->a(LX/FZe;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/89z;)V

    goto :goto_1

    .line 2257806
    :cond_4
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2257807
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x403827a

    if-ne v0, v1, :cond_5

    .line 2257808
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2257809
    invoke-static {p0, v0}, LX/FZe;->b(LX/FZe;Ljava/lang/String;)V

    goto :goto_1

    .line 2257810
    :cond_5
    iget-object v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2257811
    iget-object v1, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2257812
    iget-object v2, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2257813
    iget-object v3, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v3, v3

    .line 2257814
    invoke-static {p0, v0, v1, v2, v3}, LX/FZe;->b(LX/FZe;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V
    .locals 8

    .prologue
    .line 2257768
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    invoke-virtual {v0}, LX/FZW;->c()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v0

    iput-object v0, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2257769
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v0

    .line 2257770
    sget-object v1, LX/FZd;->a:[I

    invoke-virtual {v0}, LX/CwF;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2257771
    sget-object v0, LX/FZe;->G:LX/0P1;

    .line 2257772
    iget-object v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v1, v1

    .line 2257773
    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8ci;

    iget-object v3, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    invoke-static {p1}, LX/FZe;->b(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z

    move-result v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, LX/FZe;->a(LX/FZe;LX/CwB;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;ZZ)V

    .line 2257774
    :goto_0
    return-void

    .line 2257775
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b()Ljava/lang/String;

    move-result-object v3

    .line 2257776
    iget-object v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v4, v0

    .line 2257777
    iget-object v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->m:Ljava/lang/String;

    move-object v5, v0

    .line 2257778
    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/FZe;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Ljava/lang/String;Ljava/lang/String;LX/CwI;Ljava/lang/String;)V

    goto :goto_0

    .line 2257779
    :pswitch_1
    iget-object v0, p0, LX/FZe;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nD;

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jA_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v4

    invoke-virtual {v4}, LX/CwF;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/FZe;->G:LX/0P1;

    .line 2257780
    iget-object v6, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v6, v6

    .line 2257781
    invoke-virtual {v5, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8ci;

    iget-object v6, p0, LX/FZe;->w:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/Boolean;)Landroid/content/Intent;

    move-result-object v1

    .line 2257782
    iget-object v0, p0, LX/FZe;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2b67

    iget-object v3, p0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v3}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)V
    .locals 6

    .prologue
    .line 2257748
    iget-object v0, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2257749
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x30654a2e

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2257750
    iget-object v0, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->h:Landroid/net/Uri;

    move-object v0, v0

    .line 2257751
    if-nez v0, :cond_2

    const-string v0, ""

    .line 2257752
    :goto_1
    iget-object v1, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 2257753
    if-nez v1, :cond_3

    const-string v1, ""

    .line 2257754
    :goto_2
    iget-object v2, p0, LX/FZe;->b:LX/17W;

    iget-object v3, p0, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2257755
    iget-object v2, p0, LX/FZe;->l:LX/2Sc;

    sget-object v3, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    const-string v4, "Shortcut (id: %s) has bad field (path: %s)"

    .line 2257756
    iget-object v5, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2257757
    invoke-static {v4, v5, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2257758
    iget-object v1, p0, LX/FZe;->b:LX/17W;

    iget-object v2, p0, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2257759
    iget-object v1, p0, LX/FZe;->l:LX/2Sc;

    sget-object v2, LX/3Ql;->BAD_SUGGESTION:LX/3Ql;

    const-string v3, "Shortcut (id: %s) has bad field (fallback_path: %s)"

    .line 2257760
    iget-object v4, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2257761
    invoke-static {v3, v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2257762
    :cond_0
    return-void

    .line 2257763
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2257764
    :cond_2
    iget-object v0, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->h:Landroid/net/Uri;

    move-object v0, v0

    .line 2257765
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2257766
    :cond_3
    iget-object v1, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 2257767
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public final a(Z)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2257724
    iget-object v0, p0, LX/FZe;->j:LX/0gc;

    const-string v1, "chromeless:content:fragment:tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2257725
    instance-of v1, v0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2257726
    if-eqz v0, :cond_0

    move v0, v2

    .line 2257727
    :goto_1
    return v0

    .line 2257728
    :cond_0
    iget-object v1, p0, LX/FZe;->u:LX/2Sd;

    sget-object v4, LX/FZe;->F:Ljava/lang/String;

    if-eqz p1, :cond_1

    sget-object v0, LX/7CQ;->UP_BUTTON_PRESSED:LX/7CQ;

    :goto_2
    invoke-virtual {v1, v4, v0}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;)V

    .line 2257729
    iget-object v0, p0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v0}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2257730
    instance-of v0, v1, LX/FZU;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 2257731
    check-cast v0, LX/FZU;

    invoke-interface {v0, p1}, LX/FZU;->b(Z)Z

    move-result v0

    .line 2257732
    if-eqz v0, :cond_2

    move v0, v3

    .line 2257733
    goto :goto_1

    .line 2257734
    :cond_1
    sget-object v0, LX/7CQ;->BACK_BUTTON_PRESSED:LX/7CQ;

    goto :goto_2

    .line 2257735
    :cond_2
    iget-object v0, p0, LX/FZe;->d:LX/FZW;

    iget-object v4, p0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v4}, LX/FZW;->a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/suggestions/SuggestionsFragment;

    move-result-object v0

    .line 2257736
    invoke-direct {p0, v1}, LX/FZe;->a(Landroid/support/v4/app/Fragment;)Z

    move-result v4

    .line 2257737
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v5

    if-nez v5, :cond_4

    if-eqz v4, :cond_4

    .line 2257738
    instance-of v1, v1, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    if-eqz v1, :cond_3

    .line 2257739
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ad:Z

    .line 2257740
    :cond_3
    iget-object v1, p0, LX/FZe;->e:LX/FZX;

    invoke-virtual {v1, v0}, LX/FZX;->b(Landroid/support/v4/app/Fragment;)V

    .line 2257741
    iget-object v1, p0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    move v0, v3

    .line 2257742
    goto :goto_1

    .line 2257743
    :cond_4
    iget-object v0, p0, LX/FZe;->k:LX/Cvy;

    .line 2257744
    const-string v1, "exited_search_via_back_button"

    invoke-static {v0, v1}, LX/Cvy;->a(LX/Cvy;Ljava/lang/String;)V

    .line 2257745
    invoke-static {v0}, LX/Cvy;->i(LX/Cvy;)V

    .line 2257746
    move v0, v2

    .line 2257747
    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method
