.class public LX/FOH;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements LX/FOF;


# instance fields
.field public a:Lcom/facebook/user/tiles/UserTileDrawableController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/Context;

.field public c:Lcom/facebook/user/model/UserKey;

.field public d:I

.field public e:Z

.field public f:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2234935
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2234936
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2234933
    iget-object v0, p0, LX/FOH;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v0}, Lcom/facebook/user/tiles/UserTileDrawableController;->c()V

    .line 2234934
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2234931
    iget-object v0, p0, LX/FOH;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    invoke-virtual {v0}, Lcom/facebook/user/tiles/UserTileDrawableController;->d()V

    .line 2234932
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2234919
    iget-object v0, p0, LX/FOH;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 2234920
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    move-object v0, v1

    .line 2234921
    iget v1, p0, LX/FOH;->d:I

    iget v2, p0, LX/FOH;->d:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2234922
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2234923
    iget-boolean v1, p0, LX/FOH;->e:Z

    if-eqz v1, :cond_0

    .line 2234924
    iget-object v1, p0, LX/FOH;->f:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    .line 2234925
    iget v2, p0, LX/FOH;->d:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 2234926
    invoke-virtual {p1, v1, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2234927
    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    add-float/2addr v1, v2

    iget-object v3, p0, LX/FOH;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v2, v1, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2234928
    :cond_0
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2234929
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2234930
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 3

    .prologue
    .line 2234913
    iget-object v0, p0, LX/FOH;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 2234914
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    move-object v0, v1

    .line 2234915
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 2234916
    iget-boolean v1, p0, LX/FOH;->e:Z

    if-eqz v1, :cond_0

    .line 2234917
    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    iget-object v2, p0, LX/FOH;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 2234918
    :cond_0
    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 3

    .prologue
    .line 2234907
    iget-object v0, p0, LX/FOH;->a:Lcom/facebook/user/tiles/UserTileDrawableController;

    .line 2234908
    iget-object v1, v0, Lcom/facebook/user/tiles/UserTileDrawableController;->n:Landroid/graphics/drawable/ShapeDrawable;

    move-object v0, v1

    .line 2234909
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 2234910
    iget-boolean v1, p0, LX/FOH;->e:Z

    if-eqz v1, :cond_0

    .line 2234911
    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    iget-object v2, p0, LX/FOH;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 2234912
    :cond_0
    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2234904
    const/4 v0, 0x0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 2234906
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 2234905
    return-void
.end method
