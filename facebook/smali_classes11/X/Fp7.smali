.class public final enum LX/Fp7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fp7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fp7;

.field public static final enum CREATE:LX/Fp7;

.field public static final enum EDIT:LX/Fp7;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2290565
    new-instance v0, LX/Fp7;

    const-string v1, "CREATE"

    invoke-direct {v0, v1, v2}, LX/Fp7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fp7;->CREATE:LX/Fp7;

    .line 2290566
    new-instance v0, LX/Fp7;

    const-string v1, "EDIT"

    invoke-direct {v0, v1, v3}, LX/Fp7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fp7;->EDIT:LX/Fp7;

    .line 2290567
    const/4 v0, 0x2

    new-array v0, v0, [LX/Fp7;

    sget-object v1, LX/Fp7;->CREATE:LX/Fp7;

    aput-object v1, v0, v2

    sget-object v1, LX/Fp7;->EDIT:LX/Fp7;

    aput-object v1, v0, v3

    sput-object v0, LX/Fp7;->$VALUES:[LX/Fp7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2290568
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fp7;
    .locals 1

    .prologue
    .line 2290569
    const-class v0, LX/Fp7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fp7;

    return-object v0
.end method

.method public static values()[LX/Fp7;
    .locals 1

    .prologue
    .line 2290570
    sget-object v0, LX/Fp7;->$VALUES:[LX/Fp7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fp7;

    return-object v0
.end method
