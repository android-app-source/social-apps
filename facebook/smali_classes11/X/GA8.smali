.class public final LX/GA8;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static volatile a:LX/GA8;


# instance fields
.field public final b:LX/0Xw;

.field public final c:LX/GA1;

.field public d:Lcom/facebook/AccessToken;

.field public e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public f:Ljava/util/Date;


# direct methods
.method private constructor <init>(LX/0Xw;LX/GA1;)V
    .locals 4

    .prologue
    .line 2324723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2324724
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/GA8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2324725
    new-instance v0, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, LX/GA8;->f:Ljava/util/Date;

    .line 2324726
    const-string v0, "localBroadcastManager"

    invoke-static {p1, v0}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2324727
    const-string v0, "accessTokenCache"

    invoke-static {p2, v0}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2324728
    iput-object p1, p0, LX/GA8;->b:LX/0Xw;

    .line 2324729
    iput-object p2, p0, LX/GA8;->c:LX/GA1;

    .line 2324730
    return-void
.end method

.method public static a()LX/GA8;
    .locals 4

    .prologue
    .line 2324713
    sget-object v0, LX/GA8;->a:LX/GA8;

    if-nez v0, :cond_1

    .line 2324714
    const-class v1, LX/GA8;

    monitor-enter v1

    .line 2324715
    :try_start_0
    sget-object v0, LX/GA8;->a:LX/GA8;

    if-nez v0, :cond_0

    .line 2324716
    invoke-static {}, LX/GAK;->f()Landroid/content/Context;

    move-result-object v0

    .line 2324717
    invoke-static {v0}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v0

    .line 2324718
    new-instance v2, LX/GA1;

    invoke-direct {v2}, LX/GA1;-><init>()V

    .line 2324719
    new-instance v3, LX/GA8;

    invoke-direct {v3, v0, v2}, LX/GA8;-><init>(LX/0Xw;LX/GA1;)V

    sput-object v3, LX/GA8;->a:LX/GA8;

    .line 2324720
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2324721
    :cond_1
    sget-object v0, LX/GA8;->a:LX/GA8;

    return-object v0

    .line 2324722
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a(Lcom/facebook/AccessToken;LX/GA2;)LX/GAU;
    .locals 6

    .prologue
    .line 2324711
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2324712
    new-instance v0, LX/GAU;

    const-string v2, "me/permissions"

    sget-object v4, LX/GAZ;->GET:LX/GAZ;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/GAU;-><init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;)V

    return-object v0
.end method

.method public static a(LX/GA8;Lcom/facebook/AccessToken;Z)V
    .locals 4

    .prologue
    .line 2324692
    iget-object v0, p0, LX/GA8;->d:Lcom/facebook/AccessToken;

    .line 2324693
    iput-object p1, p0, LX/GA8;->d:Lcom/facebook/AccessToken;

    .line 2324694
    iget-object v1, p0, LX/GA8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2324695
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, LX/GA8;->f:Ljava/util/Date;

    .line 2324696
    if-eqz p2, :cond_0

    .line 2324697
    if-eqz p1, :cond_2

    .line 2324698
    iget-object v1, p0, LX/GA8;->c:LX/GA1;

    invoke-virtual {v1, p1}, LX/GA1;->a(Lcom/facebook/AccessToken;)V

    .line 2324699
    :cond_0
    :goto_0
    invoke-static {v0, p1}, LX/Gsc;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2324700
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.sdk.ACTION_CURRENT_ACCESS_TOKEN_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2324701
    const-string v2, "com.facebook.sdk.EXTRA_OLD_ACCESS_TOKEN"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2324702
    const-string v2, "com.facebook.sdk.EXTRA_NEW_ACCESS_TOKEN"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2324703
    iget-object v2, p0, LX/GA8;->b:LX/0Xw;

    invoke-virtual {v2, v1}, LX/0Xw;->a(Landroid/content/Intent;)Z

    .line 2324704
    :cond_1
    return-void

    .line 2324705
    :cond_2
    iget-object v1, p0, LX/GA8;->c:LX/GA1;

    .line 2324706
    iget-object v2, v1, LX/GA1;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "com.facebook.AccessTokenManager.CachedAccessToken"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2324707
    sget-boolean v2, LX/GAK;->k:Z

    move v2, v2

    .line 2324708
    if-eqz v2, :cond_3

    .line 2324709
    invoke-static {v1}, LX/GA1;->g(LX/GA1;)LX/GAa;

    move-result-object v2

    invoke-virtual {v2}, LX/GAa;->b()V

    .line 2324710
    :cond_3
    invoke-static {}, LX/GAK;->f()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/Gsc;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static b(Lcom/facebook/AccessToken;LX/GA2;)LX/GAU;
    .locals 6

    .prologue
    .line 2324668
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2324669
    const-string v0, "grant_type"

    const-string v1, "fb_extend_sso_token"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2324670
    new-instance v0, LX/GAU;

    const-string v2, "oauth/access_token"

    sget-object v4, LX/GAZ;->GET:LX/GAZ;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/GAU;-><init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;)V

    return-object v0
.end method

.method public static b(LX/GA8;LX/G9z;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 2324673
    iget-object v2, p0, LX/GA8;->d:Lcom/facebook/AccessToken;

    .line 2324674
    if-nez v2, :cond_1

    .line 2324675
    if-eqz p1, :cond_0

    .line 2324676
    new-instance v0, LX/GAA;

    const-string v1, "No current access token to refresh"

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    .line 2324677
    :cond_0
    :goto_0
    return-void

    .line 2324678
    :cond_1
    iget-object v0, p0, LX/GA8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2324679
    if-eqz p1, :cond_0

    .line 2324680
    new-instance v0, LX/GAA;

    const-string v1, "Refresh already in progress"

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2324681
    :cond_2
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, LX/GA8;->f:Ljava/util/Date;

    .line 2324682
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2324683
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 2324684
    new-instance v4, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 2324685
    new-instance v5, LX/GA7;

    invoke-direct {v5}, LX/GA7;-><init>()V

    .line 2324686
    new-instance v8, LX/GAX;

    const/4 v0, 0x2

    new-array v0, v0, [LX/GAU;

    new-instance v1, LX/GA3;

    invoke-direct {v1, p0, v4, v6, v7}, LX/GA3;-><init>(LX/GA8;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/util/Set;Ljava/util/Set;)V

    invoke-static {v2, v1}, LX/GA8;->a(Lcom/facebook/AccessToken;LX/GA2;)LX/GAU;

    move-result-object v1

    aput-object v1, v0, v3

    new-instance v1, LX/GA4;

    invoke-direct {v1, p0, v5}, LX/GA4;-><init>(LX/GA8;LX/GA7;)V

    invoke-static {v2, v1}, LX/GA8;->b(Lcom/facebook/AccessToken;LX/GA2;)LX/GAU;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-direct {v8, v0}, LX/GAX;-><init>([LX/GAU;)V

    .line 2324687
    new-instance v0, LX/GA6;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, LX/GA6;-><init>(LX/GA8;Lcom/facebook/AccessToken;LX/G9z;Ljava/util/concurrent/atomic/AtomicBoolean;LX/GA7;Ljava/util/Set;Ljava/util/Set;)V

    .line 2324688
    iget-object v1, v8, LX/GAX;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2324689
    iget-object v1, v8, LX/GAX;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2324690
    :cond_3
    invoke-static {v8}, LX/GAU;->b(LX/GAX;)LX/GAV;

    .line 2324691
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/AccessToken;)V
    .locals 1

    .prologue
    .line 2324671
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, LX/GA8;->a(LX/GA8;Lcom/facebook/AccessToken;Z)V

    .line 2324672
    return-void
.end method
