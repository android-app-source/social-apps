.class public LX/GMh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GKO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GKO",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/2U4;


# direct methods
.method public constructor <init>(LX/0tX;LX/2U4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345153
    iput-object p1, p0, LX/GMh;->a:LX/0tX;

    .line 2345154
    iput-object p2, p0, LX/GMh;->b:LX/2U4;

    .line 2345155
    return-void
.end method

.method public static b(LX/0QB;)LX/GMh;
    .locals 3

    .prologue
    .line 2345156
    new-instance v2, LX/GMh;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v1

    check-cast v1, LX/2U4;

    invoke-direct {v2, v0, v1}, LX/GMh;-><init>(LX/0tX;LX/2U4;)V

    .line 2345157
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7

    .prologue
    .line 2345158
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345159
    iget-object v1, p0, LX/GMh;->a:LX/0tX;

    iget-object v2, p0, LX/GMh;->b:LX/2U4;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v3

    .line 2345160
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v0, v0

    .line 2345161
    if-eqz v0, :cond_0

    .line 2345162
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v0, v0

    .line 2345163
    iget-object p1, v0, Lcom/facebook/adinterfaces/model/EventSpecModel;->b:Ljava/lang/String;

    move-object v0, p1

    .line 2345164
    :goto_0
    new-instance v4, LX/A8m;

    invoke-direct {v4}, LX/A8m;-><init>()V

    move-object v4, v4

    .line 2345165
    const-string v5, "story_id"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "event_boost_type"

    invoke-virtual {v5, v6, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "image_large_aspect_height"

    iget-object p1, v2, LX/2U4;->a:LX/0sa;

    invoke-virtual {p1}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v5, v6, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    const-string v6, "image_large_aspect_width"

    iget-object p1, v2, LX/2U4;->a:LX/0sa;

    invoke-virtual {p1}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v5, v6, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2345166
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    move-object v0, v4

    .line 2345167
    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/GMg;

    invoke-direct {v1, p0}, LX/GMg;-><init>(LX/GMh;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
