.class public LX/Fje;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/EQH;


# static fields
.field public static final C:LX/1X6;


# instance fields
.field public A:Lcom/facebook/widget/text/BetterTextView;

.field public B:LX/EQG;

.field private D:Ljava/lang/String;

.field public E:LX/5eI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5eI",
            "<",
            "LX/CzK;",
            "LX/1Ps;",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field public F:LX/5eI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/5eI",
            "<",
            "LX/1X6;",
            "LX/1Ps;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public G:LX/CzK;

.field public H:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:F

.field public J:Z

.field public K:LX/EQF;

.field public L:I

.field private M:LX/0fx;

.field public N:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

.field public O:LX/Fe2;

.field public P:Landroid/view/View$OnClickListener;

.field public Q:LX/Cw8;

.field public R:LX/EQE;

.field public S:LX/1DI;

.field private T:I

.field public U:I

.field private V:Z

.field public W:Z

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C8t;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8iF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Landroid/view/ViewStub;

.field public k:Lcom/facebook/widget/CustomLinearLayout;

.field public l:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

.field public m:Lcom/facebook/widget/text/BetterTextView;

.field public n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public o:Landroid/widget/LinearLayout;

.field public p:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fig/nullstateview/FigNullStateView;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0g8;

.field public r:Landroid/view/ViewGroup;

.field public s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public t:LX/EQC;

.field public u:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public v:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/widget/text/BetterButton;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lcom/facebook/widget/text/BetterTextView;

.field public y:Landroid/view/View;

.field public z:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2277202
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, LX/Fje;->C:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2277203
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2277204
    sget-object v0, LX/Cw8;->Search:LX/Cw8;

    sget-object v1, LX/EQE;->SPINNING_WHEEL:LX/EQE;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, LX/Fje;->a(LX/Cw8;LX/EQE;Z)V

    .line 2277205
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Cw8;)V
    .locals 2

    .prologue
    .line 2277206
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2277207
    sget-object v0, LX/EQE;->SPINNING_WHEEL:LX/EQE;

    const/4 v1, 0x1

    invoke-direct {p0, p2, v0, v1}, LX/Fje;->a(LX/Cw8;LX/EQE;Z)V

    .line 2277208
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Cw8;LX/EQE;)V
    .locals 1

    .prologue
    .line 2277209
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2277210
    const/4 v0, 0x1

    invoke-direct {p0, p2, p3, v0}, LX/Fje;->a(LX/Cw8;LX/EQE;Z)V

    .line 2277211
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Cw8;Z)V
    .locals 1

    .prologue
    .line 2277212
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2277213
    sget-object v0, LX/EQE;->SPINNING_WHEEL:LX/EQE;

    invoke-direct {p0, p2, v0, p3}, LX/Fje;->a(LX/Cw8;LX/EQE;Z)V

    .line 2277214
    return-void
.end method

.method public static a(LX/Cw8;)I
    .locals 2

    .prologue
    .line 2277215
    sget-object v0, LX/Fjc;->b:[I

    invoke-virtual {p0}, LX/Cw8;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2277216
    :pswitch_0
    const v0, 0x7f020629

    .line 2277217
    :goto_0
    return v0

    .line 2277218
    :pswitch_1
    const v0, 0x7f02062e

    goto :goto_0

    .line 2277219
    :pswitch_2
    const v0, 0x7f02062d

    goto :goto_0

    .line 2277220
    :pswitch_3
    const v0, 0x7f02062c

    goto :goto_0

    .line 2277221
    :pswitch_4
    const v0, 0x7f02062b

    goto :goto_0

    .line 2277222
    :pswitch_5
    const v0, 0x7f02062f

    goto :goto_0

    .line 2277223
    :pswitch_6
    const v0, 0x7f020630

    goto :goto_0

    .line 2277224
    :pswitch_7
    const v0, 0x7f02062a

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private a(LX/Cw8;LX/EQE;Z)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2277225
    const-class v0, LX/Fje;

    invoke-static {v0, p0}, LX/Fje;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2277226
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277227
    iput-object p1, p0, LX/Fje;->Q:LX/Cw8;

    .line 2277228
    iget-object v0, p0, LX/Fje;->e:LX/0Uh;

    sget v1, LX/2SU;->z:I

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 2277229
    iget-object v0, p0, LX/Fje;->d:LX/8iF;

    invoke-virtual {v0}, LX/8iF;->e()F

    move-result v0

    iput v0, p0, LX/Fje;->I:F

    .line 2277230
    new-instance v0, LX/5eI;

    iget-object v2, p0, LX/Fje;->b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

    new-instance v3, LX/1QG;

    invoke-direct {v3}, LX/1QG;-><init>()V

    invoke-direct {v0, v2, v3}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    iput-object v0, p0, LX/Fje;->E:LX/5eI;

    .line 2277231
    new-instance v0, LX/5eI;

    iget-object v2, p0, LX/Fje;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1QG;

    invoke-direct {v3}, LX/1QG;-><init>()V

    invoke-direct {v0, v2, v3}, LX/5eI;-><init>(LX/1Nt;LX/1PW;)V

    iput-object v0, p0, LX/Fje;->F:LX/5eI;

    .line 2277232
    iput-boolean v5, p0, LX/Fje;->J:Z

    .line 2277233
    iput-boolean v5, p0, LX/Fje;->W:Z

    .line 2277234
    iput-object p2, p0, LX/Fje;->R:LX/EQE;

    .line 2277235
    if-eqz v1, :cond_6

    const v0, 0x7f0312b7

    .line 2277236
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2277237
    const v0, 0x7f0d2b9d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/Fje;->j:Landroid/view/ViewStub;

    .line 2277238
    const/4 p2, 0x1

    const/4 v7, 0x0

    .line 2277239
    iget-object v0, p0, LX/Fje;->h:LX/0ad;

    sget-short v2, LX/100;->x:S

    invoke-interface {v0, v2, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2277240
    :cond_0
    :goto_1
    const v0, 0x7f0d2b99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2277241
    const v0, 0x7f0d0914

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Fje;->o:Landroid/widget/LinearLayout;

    .line 2277242
    const v0, 0x7f0d0595

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2277243
    iget-object v0, p0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2277244
    if-eqz v1, :cond_2

    .line 2277245
    if-eqz p3, :cond_1

    new-instance v0, LX/0g6;

    invoke-direct {p0}, LX/Fje;->getRecyclerView()Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0g6;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    :goto_2
    iput-object v0, p0, LX/Fje;->q:LX/0g8;

    .line 2277246
    :goto_3
    iget-object v0, p0, LX/Fje;->h:LX/0ad;

    sget-short v1, LX/100;->E:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, LX/Cw8;->People:LX/Cw8;

    if-ne p1, v0, :cond_4

    .line 2277247
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2b9c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v2, LX/FjU;

    invoke-direct {v2, p0}, LX/FjU;-><init>(LX/Fje;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, LX/Fje;->v:LX/0zw;

    .line 2277248
    :goto_4
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v0, LX/Cw8;->SearchDark:LX/Cw8;

    if-ne p1, v0, :cond_5

    const v0, 0x7f0a069f

    :goto_5
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, LX/Fje;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2277249
    new-instance v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Fje;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2277250
    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b175c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Fje;->U:I

    .line 2277251
    iget-object v0, p0, LX/Fje;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget v1, p0, LX/Fje;->U:I

    iget v2, p0, LX/Fje;->U:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(II)V

    .line 2277252
    new-instance v0, LX/EQC;

    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EQC;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Fje;->t:LX/EQC;

    .line 2277253
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    iget-object v1, p0, LX/Fje;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 2277254
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    iget-object v1, p0, LX/Fje;->t:LX/EQC;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 2277255
    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0822ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fje;->D:Ljava/lang/String;

    .line 2277256
    iput v4, p0, LX/Fje;->T:I

    .line 2277257
    iput-boolean v5, p0, LX/Fje;->V:Z

    .line 2277258
    sget-object v0, LX/EQG;->LOADING:LX/EQG;

    invoke-virtual {p0, v0}, LX/Fje;->setState(LX/EQG;)V

    .line 2277259
    return-void

    .line 2277260
    :cond_1
    new-instance v0, LX/0g7;

    invoke-direct {p0}, LX/Fje;->getRecyclerView()Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    goto/16 :goto_2

    .line 2277261
    :cond_2
    if-eqz p3, :cond_3

    new-instance v1, LX/An1;

    const v0, 0x7f0d04af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v1, v0}, LX/An1;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    move-object v0, v1

    :goto_6
    iput-object v0, p0, LX/Fje;->q:LX/0g8;

    goto/16 :goto_3

    :cond_3
    new-instance v1, LX/2iI;

    const v0, 0x7f0d04af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    move-object v0, v1

    goto :goto_6

    .line 2277262
    :cond_4
    new-instance v1, LX/0zw;

    const v0, 0x7f0d088c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v2, LX/FjW;

    invoke-direct {v2, p0}, LX/FjW;-><init>(LX/Fje;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, LX/Fje;->v:LX/0zw;

    goto/16 :goto_4

    .line 2277263
    :cond_5
    const v0, 0x7f0a0048

    goto/16 :goto_5

    .line 2277264
    :cond_6
    const v0, 0x7f0312bb

    goto/16 :goto_0

    .line 2277265
    :cond_7
    iget-object v0, p0, LX/Fje;->h:LX/0ad;

    sget-short v2, LX/100;->w:S

    invoke-interface {v0, v2, v7}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2277266
    iget-object v3, p0, LX/Fje;->j:Landroid/view/ViewStub;

    if-eqz v2, :cond_8

    const v0, 0x7f03047d

    :goto_7
    invoke-virtual {v3, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2277267
    iget-object v0, p0, LX/Fje;->j:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    .line 2277268
    const v0, 0x7f0d1732

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    iput-object v0, p0, LX/Fje;->l:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    .line 2277269
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 2277270
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v6, 0x7f01074e

    invoke-virtual {v0, v6, v3, p2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2277271
    iget-object v0, p0, LX/Fje;->l:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    iget v6, v3, Landroid/util/TypedValue;->data:I

    invoke-virtual {v0, v6}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a(I)V

    .line 2277272
    if-eqz v2, :cond_9

    .line 2277273
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2277274
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b042c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2277275
    :goto_8
    if-lez v0, :cond_0

    .line 2277276
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f03047b

    iget-object v7, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    invoke-static {v3, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2277277
    sub-int/2addr v0, v2

    goto :goto_8

    .line 2277278
    :cond_8
    const v0, 0x7f0312fb

    goto :goto_7

    .line 2277279
    :cond_9
    const v0, 0x7f0d2c28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Fje;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 2277280
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-gt v0, v2, :cond_a

    .line 2277281
    iget-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget v6, v3, Landroid/util/TypedValue;->data:I

    invoke-direct {v2, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2277282
    :goto_9
    iget-object v0, p0, LX/Fje;->h:LX/0ad;

    sget-short v2, LX/100;->v:S

    invoke-interface {v0, v2, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2277283
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f01074a

    invoke-virtual {v0, v2, v3, p2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2277284
    iget-object v0, p0, LX/Fje;->m:Lcom/facebook/widget/text/BetterTextView;

    iget v2, v3, Landroid/util/TypedValue;->data:I

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2277285
    iget-object v0, p0, LX/Fje;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2277286
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v6, 0x7f01074d

    invoke-virtual {v0, v6, v3, p2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2277287
    iget-object v0, p0, LX/Fje;->g:LX/0wM;

    const v6, 0x7f0217c2

    iget v3, v3, Landroid/util/TypedValue;->data:I

    invoke-virtual {v0, v6, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2277288
    :goto_a
    if-eqz v0, :cond_0

    .line 2277289
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v0, v7, v7, v3, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2277290
    iget-object v3, p0, LX/Fje;->m:Lcom/facebook/widget/text/BetterTextView;

    aget-object v6, v2, v7

    const/4 v7, 0x2

    aget-object v7, v2, v7

    const/4 p2, 0x3

    aget-object v2, v2, p2

    invoke-virtual {v3, v6, v0, v7, v2}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 2277291
    :cond_a
    iget-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget v6, v3, Landroid/util/TypedValue;->data:I

    invoke-direct {v2, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_9

    .line 2277292
    :cond_b
    iget-object v0, p0, LX/Fje;->g:LX/0wM;

    const v3, 0x7f0217c2

    const v6, -0xc4a668

    invoke-virtual {v0, v3, v6}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_a
.end method

.method private static a(Landroid/view/View;I)V
    .locals 0
    .param p0    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2277326
    if-eqz p0, :cond_0

    .line 2277327
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2277328
    :cond_0
    return-void
.end method

.method private static a(Landroid/view/View;J)V
    .locals 3

    .prologue
    .line 2277293
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    instance-of v0, v0, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_1

    .line 2277294
    :cond_0
    :goto_0
    return-void

    .line 2277295
    :cond_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2277296
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2277297
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2277298
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v1, p1

    check-cast v1, LX/Fje;

    const/16 v2, 0x2011

    invoke-static {v10, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v10}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

    invoke-static {v10}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v10}, LX/8iF;->a(LX/0QB;)LX/8iF;

    move-result-object v5

    check-cast v5, LX/8iF;

    invoke-static {v10}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    const/16 v7, 0x6c9

    invoke-static {v10, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v10}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v10}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 p0, 0x455

    invoke-static {v10, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    iput-object v2, v1, LX/Fje;->a:LX/0Ot;

    iput-object v3, v1, LX/Fje;->b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

    iput-object v4, v1, LX/Fje;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iput-object v5, v1, LX/Fje;->d:LX/8iF;

    iput-object v6, v1, LX/Fje;->e:LX/0Uh;

    iput-object v7, v1, LX/Fje;->f:LX/0Or;

    iput-object v8, v1, LX/Fje;->g:LX/0wM;

    iput-object v9, v1, LX/Fje;->h:LX/0ad;

    iput-object v10, v1, LX/Fje;->i:LX/0Ot;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2277299
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, LX/Fje;->a(Landroid/view/View;I)V

    .line 2277300
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2277301
    invoke-direct {p0}, LX/Fje;->f()V

    .line 2277302
    iget-object v0, p0, LX/Fje;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2277303
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->a(I)V

    .line 2277304
    iget-object v0, p0, LX/Fje;->t:LX/EQC;

    invoke-virtual {v0}, LX/EQC;->b()V

    .line 2277305
    iget-object v0, p0, LX/Fje;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/Fje;->S:LX/1DI;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2277306
    return-void
.end method

.method private static b(Landroid/view/View;J)V
    .locals 3

    .prologue
    .line 2277307
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    instance-of v0, v0, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_1

    .line 2277308
    :cond_0
    :goto_0
    return-void

    .line 2277309
    :cond_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2277310
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 2277311
    new-instance v1, LX/Fjb;

    invoke-direct {v1, p0}, LX/Fjb;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2277312
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2277313
    iget-object v0, p0, LX/Fje;->l:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    if-eqz v0, :cond_0

    .line 2277314
    iget-object v0, p0, LX/Fje;->l:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    invoke-virtual {v0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b()V

    .line 2277315
    :cond_0
    iget-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    if-eqz v0, :cond_1

    .line 2277316
    iget-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2277317
    :cond_1
    return-void
.end method

.method private getRecyclerView()Lcom/facebook/widget/recyclerview/BetterRecyclerView;
    .locals 2

    .prologue
    .line 2277318
    const v0, 0x7f0d04af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2277319
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2277320
    return-object v0
.end method

.method public static h(LX/Fje;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 2277081
    iget-boolean v0, p0, LX/Fje;->J:Z

    if-eqz v0, :cond_0

    .line 2277082
    const/4 v1, 0x0

    .line 2277083
    iget-object v0, p0, LX/Fje;->G:LX/CzK;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 2277084
    :goto_0
    if-nez v0, :cond_6

    iget-object v2, p0, LX/Fje;->R:LX/EQE;

    sget-object v5, LX/EQE;->GLOWING_STORIES:LX/EQE;

    if-eq v2, v5, :cond_6

    .line 2277085
    :goto_1
    iput-boolean v4, p0, LX/Fje;->J:Z

    .line 2277086
    :cond_0
    sget-object v0, LX/Fjc;->a:[I

    iget-object v1, p0, LX/Fje;->B:LX/EQG;

    invoke-virtual {v1}, LX/EQG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2277087
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unimplemented state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2277088
    :pswitch_0
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 2277089
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2277090
    invoke-direct {p0}, LX/Fje;->f()V

    .line 2277091
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-static {v0, v4}, LX/Fje;->a(Landroid/view/View;I)V

    .line 2277092
    :goto_2
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2277093
    iget-object v0, p0, LX/Fje;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2277094
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->a(I)V

    .line 2277095
    iget-object v0, p0, LX/Fje;->t:LX/EQC;

    invoke-virtual {v0}, LX/EQC;->b()V

    .line 2277096
    iget-object v0, p0, LX/Fje;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2277097
    :goto_3
    return-void

    .line 2277098
    :cond_1
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2277099
    iget-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    if-eqz v0, :cond_2

    .line 2277100
    iget-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2277101
    :cond_2
    iget-object v0, p0, LX/Fje;->l:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    if-eqz v0, :cond_3

    .line 2277102
    iget-object v0, p0, LX/Fje;->l:Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;

    invoke-virtual {v0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a()V

    .line 2277103
    :cond_3
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-static {v0, v3}, LX/Fje;->a(Landroid/view/View;I)V

    goto :goto_2

    .line 2277104
    :pswitch_1
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2277105
    invoke-direct {p0}, LX/Fje;->n()V

    .line 2277106
    iget-object v0, p0, LX/Fje;->t:LX/EQC;

    invoke-virtual {v0}, LX/EQC;->b()V

    .line 2277107
    iget-object v0, p0, LX/Fje;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto :goto_3

    .line 2277108
    :pswitch_2
    invoke-direct {p0}, LX/Fje;->m()V

    .line 2277109
    iget-boolean v0, p0, LX/Fje;->W:Z

    if-eqz v0, :cond_4

    .line 2277110
    iget-object v0, p0, LX/Fje;->t:LX/EQC;

    invoke-virtual {v0}, LX/EQC;->a()V

    .line 2277111
    :cond_4
    goto :goto_3

    .line 2277112
    :pswitch_3
    invoke-direct {p0}, LX/Fje;->m()V

    .line 2277113
    iget-object v0, p0, LX/Fje;->t:LX/EQC;

    invoke-virtual {v0}, LX/EQC;->b()V

    goto :goto_3

    .line 2277114
    :pswitch_4
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-static {v0, v3}, LX/Fje;->a(Landroid/view/View;I)V

    .line 2277115
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2277116
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2277117
    invoke-direct {p0}, LX/Fje;->f()V

    .line 2277118
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->a(I)V

    .line 2277119
    iget-object v0, p0, LX/Fje;->p:LX/0zw;

    if-eqz v0, :cond_b

    .line 2277120
    :goto_4
    iget-object v0, p0, LX/Fje;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2277121
    iget-object v0, p0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2277122
    invoke-virtual {p0}, LX/Fje;->requestLayout()V

    goto :goto_3

    .line 2277123
    :pswitch_5
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-static {v0, v3}, LX/Fje;->a(Landroid/view/View;I)V

    .line 2277124
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2277125
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/Fje;->D:Ljava/lang/String;

    iget-object v2, p0, LX/Fje;->S:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2277126
    invoke-direct {p0}, LX/Fje;->f()V

    .line 2277127
    iget-object v0, p0, LX/Fje;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2277128
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    invoke-interface {v0, v3}, LX/0g8;->a(I)V

    .line 2277129
    iget-object v0, p0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto/16 :goto_3

    .line 2277130
    :pswitch_6
    iget-object v0, p0, LX/Fje;->D:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/Fje;->a(Ljava/lang/String;)V

    .line 2277131
    invoke-virtual {p0}, LX/Fje;->b()V

    .line 2277132
    iget-object v0, p0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto/16 :goto_3

    .line 2277133
    :pswitch_7
    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0822d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Fje;->a(Ljava/lang/String;)V

    .line 2277134
    invoke-virtual {p0}, LX/Fje;->b()V

    .line 2277135
    iget-object v0, p0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto/16 :goto_3

    :cond_5
    move v0, v1

    .line 2277136
    goto/16 :goto_0

    .line 2277137
    :cond_6
    iget-object v2, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    if-eqz v2, :cond_7

    .line 2277138
    iget-object v2, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-virtual {p0, v2}, LX/Fje;->removeView(Landroid/view/View;)V

    .line 2277139
    :cond_7
    if-nez v0, :cond_8

    .line 2277140
    iget-object v0, p0, LX/Fje;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C8t;

    sget-object v1, LX/C8v;->GLOWING:LX/C8v;

    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    invoke-virtual {v0, v1, v2, v5, p0}, LX/C8t;->a(LX/C8v;Landroid/content/res/Resources;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    .line 2277141
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/Fje;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2277142
    :cond_8
    iget-object v0, p0, LX/Fje;->H:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 2277143
    iget-object v0, p0, LX/Fje;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C8t;

    iget v2, p0, LX/Fje;->I:F

    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 2277144
    invoke-static {v5, p0}, LX/C8t;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v6

    .line 2277145
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 2277146
    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f03039c

    const/4 v5, 0x0

    invoke-virtual {v8, v9, v6, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v7, v8

    .line 2277147
    iget-object v8, v0, LX/C8t;->h:LX/5eI;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/5eI;->a(Ljava/lang/Object;)V

    .line 2277148
    iget-object v8, v0, LX/C8t;->h:LX/5eI;

    invoke-virtual {v8, v7}, LX/5eI;->a(Landroid/view/View;)V

    .line 2277149
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2277150
    move-object v0, v6

    .line 2277151
    iput-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    .line 2277152
    :goto_5
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;->a:LX/1Cz;

    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Cz;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2277153
    iget-object v2, p0, LX/Fje;->E:LX/5eI;

    iget-object v5, p0, LX/Fje;->G:LX/CzK;

    invoke-virtual {v2, v5}, LX/5eI;->a(Ljava/lang/Object;)V

    .line 2277154
    iget-object v2, p0, LX/Fje;->E:LX/5eI;

    invoke-virtual {v2, v0}, LX/5eI;->a(Landroid/view/View;)V

    .line 2277155
    iget-object v2, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2277156
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030385

    iget-object v5, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2277157
    iget-object v1, p0, LX/Fje;->F:LX/5eI;

    sget-object v2, LX/Fje;->C:LX/1X6;

    invoke-virtual {v1, v2}, LX/5eI;->a(Ljava/lang/Object;)V

    .line 2277158
    iget-object v1, p0, LX/Fje;->F:LX/5eI;

    invoke-virtual {v1, v0}, LX/5eI;->a(Landroid/view/View;)V

    .line 2277159
    iget-object v1, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2277160
    iget-object v0, p0, LX/Fje;->R:LX/EQE;

    sget-object v1, LX/EQE;->GLOWING_STORIES:LX/EQE;

    if-ne v0, v1, :cond_a

    .line 2277161
    iget-object v0, p0, LX/Fje;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C8t;

    sget-object v1, LX/C8v;->GLOWING:LX/C8v;

    iget-object v2, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, LX/C8t;->a(Landroid/content/res/Resources;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v1, v2, v5}, LX/C8t;->a(LX/C8v;Landroid/view/ViewGroup;I)V

    .line 2277162
    :goto_6
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, LX/Fje;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2277163
    :cond_9
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {v0, p0}, LX/C8t;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    goto :goto_5

    .line 2277164
    :cond_a
    new-instance v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    .line 2277165
    iget v1, p0, LX/Fje;->U:I

    iget v2, p0, LX/Fje;->U:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(II)V

    .line 2277166
    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2277167
    iget-object v1, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_6

    .line 2277168
    :cond_b
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2b9a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/Fje;->p:LX/0zw;

    .line 2277169
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2277170
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0106d4

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_d

    iget v1, v0, Landroid/util/TypedValue;->type:I

    const/16 v2, 0x1c

    if-ne v1, v2, :cond_d

    .line 2277171
    iget-object v1, p0, LX/Fje;->g:LX/0wM;

    iget-object v2, p0, LX/Fje;->Q:LX/Cw8;

    invoke-static {v2}, LX/Fje;->a(LX/Cw8;)I

    move-result v2

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    .line 2277172
    :goto_7
    iget-object v0, p0, LX/Fje;->p:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/nullstateview/FigNullStateView;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 2277173
    iget-object v0, p0, LX/Fje;->p:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/nullstateview/FigNullStateView;

    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, LX/Fje;->Q:LX/Cw8;

    .line 2277174
    sget-object v3, LX/Fjc;->b:[I

    invoke-virtual {v2}, LX/Cw8;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_1

    .line 2277175
    :pswitch_8
    const v3, 0x7f0822cb

    .line 2277176
    :goto_8
    move v2, v3

    .line 2277177
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2277178
    const/4 v5, 0x0

    .line 2277179
    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2277180
    new-instance v2, LX/0zw;

    const v0, 0x7f0d2b9b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v3, LX/FjZ;

    invoke-direct {v3, p0}, LX/FjZ;-><init>(LX/Fje;)V

    invoke-direct {v2, v0, v3}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v2, p0, LX/Fje;->w:LX/0zw;

    .line 2277181
    iget-object v0, p0, LX/Fje;->w:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2277182
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fje;->P:Landroid/view/View$OnClickListener;

    .line 2277183
    sget-object v0, LX/Fjc;->b:[I

    iget-object v2, p0, LX/Fje;->Q:LX/Cw8;

    invoke-virtual {v2}, LX/Cw8;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_2

    .line 2277184
    :cond_c
    :goto_9
    goto/16 :goto_4

    .line 2277185
    :cond_d
    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, LX/Fje;->Q:LX/Cw8;

    invoke-static {v1}, LX/Fje;->a(LX/Cw8;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v1, v0

    goto :goto_7

    .line 2277186
    :pswitch_9
    const v3, 0x7f0822cc

    goto :goto_8

    .line 2277187
    :pswitch_a
    const v3, 0x7f0822cf

    goto :goto_8

    .line 2277188
    :pswitch_b
    const v3, 0x7f0822ce

    goto :goto_8

    .line 2277189
    :pswitch_c
    const v3, 0x7f0822cd

    goto :goto_8

    .line 2277190
    :pswitch_d
    const v3, 0x7f0822d0

    goto :goto_8

    .line 2277191
    :pswitch_e
    const v3, 0x7f0822d1

    goto :goto_8

    .line 2277192
    :pswitch_f
    const v3, 0x7f0822d2

    goto :goto_8

    .line 2277193
    :pswitch_10
    iget-object v0, p0, LX/Fje;->h:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/100;->D:S

    invoke-interface {v0, v2, v3, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2277194
    iget-object v0, p0, LX/Fje;->w:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2277195
    iget-object v0, p0, LX/Fje;->w:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    const v2, 0x7f0817ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2277196
    new-instance v0, LX/Fja;

    invoke-direct {v0, p0}, LX/Fja;-><init>(LX/Fje;)V

    iput-object v0, p0, LX/Fje;->P:Landroid/view/View$OnClickListener;

    goto :goto_9

    .line 2277197
    :pswitch_11
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2277198
    invoke-virtual {p0}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0106d6

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_c

    iget v1, v0, Landroid/util/TypedValue;->type:I

    const/16 v2, 0x1c

    if-ne v1, v2, :cond_c

    .line 2277199
    iget-object v1, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v2, p0, LX/Fje;->g:LX/0wM;

    invoke-virtual {p0}, LX/Fje;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f020bba

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v2, v3, v0}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2277200
    iput-object v0, v1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->l:Landroid/graphics/drawable/Drawable;

    .line 2277201
    goto/16 :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_a
        :pswitch_8
        :pswitch_9
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2277321
    invoke-direct {p0}, LX/Fje;->n()V

    .line 2277322
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2277323
    iget-object v0, p0, LX/Fje;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2277324
    iget-object v0, p0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2277325
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    const/16 v1, 0x8

    .line 2277014
    iget v0, p0, LX/Fje;->T:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Fje;->V:Z

    if-nez v0, :cond_1

    .line 2277015
    :cond_0
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, LX/Fje;->a(Landroid/view/View;I)V

    .line 2277016
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2277017
    invoke-direct {p0}, LX/Fje;->f()V

    .line 2277018
    iget-object v0, p0, LX/Fje;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2277019
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->a(I)V

    .line 2277020
    :goto_0
    return-void

    .line 2277021
    :cond_1
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    if-nez v0, :cond_3

    .line 2277022
    iget-object v0, p0, LX/Fje;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2277023
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget v1, p0, LX/Fje;->T:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/Fje;->b(Landroid/view/View;J)V

    .line 2277024
    iget-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    if-eqz v0, :cond_2

    .line 2277025
    iget-object v0, p0, LX/Fje;->k:Lcom/facebook/widget/CustomLinearLayout;

    iget v1, p0, LX/Fje;->T:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/Fje;->b(Landroid/view/View;J)V

    .line 2277026
    :cond_2
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    invoke-interface {v0}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    iget v1, p0, LX/Fje;->T:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/Fje;->a(Landroid/view/View;J)V

    goto :goto_0

    .line 2277027
    :cond_3
    iget-object v0, p0, LX/Fje;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2277028
    iget-object v0, p0, LX/Fje;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2277029
    invoke-direct {p0}, LX/Fje;->f()V

    .line 2277030
    iget-object v0, p0, LX/Fje;->r:Landroid/view/ViewGroup;

    iget v1, p0, LX/Fje;->T:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/Fje;->b(Landroid/view/View;J)V

    .line 2277031
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    invoke-interface {v0}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    iget v1, p0, LX/Fje;->T:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/Fje;->a(Landroid/view/View;J)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IILjava/lang/String;I)V
    .locals 2

    .prologue
    .line 2277034
    iget-object v0, p0, LX/Fje;->v:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    .line 2277035
    if-nez p1, :cond_0

    .line 2277036
    iget-object v0, p0, LX/Fje;->z:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2277037
    :goto_0
    iget-object v0, p0, LX/Fje;->z:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2277038
    iget-object v0, p0, LX/Fje;->A:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2277039
    return-void

    .line 2277040
    :cond_0
    iget-object v0, p0, LX/Fje;->z:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2277041
    iget-object v0, p0, LX/Fje;->A:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p4}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2277042
    iget-object v0, p0, LX/Fje;->z:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(LX/EQF;I)V
    .locals 2
    .param p1    # LX/EQF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2277043
    iget-object v0, p0, LX/Fje;->M:LX/0fx;

    if-eqz v0, :cond_0

    .line 2277044
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    iget-object v1, p0, LX/Fje;->M:LX/0fx;

    invoke-interface {v0, v1}, LX/0g8;->c(LX/0fx;)V

    .line 2277045
    :cond_0
    iput-object p1, p0, LX/Fje;->K:LX/EQF;

    .line 2277046
    iput p2, p0, LX/Fje;->L:I

    .line 2277047
    iget-object v0, p0, LX/Fje;->K:LX/EQF;

    if-nez v0, :cond_1

    .line 2277048
    :goto_0
    return-void

    .line 2277049
    :cond_1
    new-instance v0, LX/FjX;

    invoke-direct {v0, p0}, LX/FjX;-><init>(LX/Fje;)V

    iput-object v0, p0, LX/Fje;->M:LX/0fx;

    .line 2277050
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    iget-object v1, p0, LX/Fje;->M:LX/0fx;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    goto :goto_0
.end method

.method public final a(LX/EQG;Z)V
    .locals 1

    .prologue
    .line 2277051
    iget-object v0, p0, LX/Fje;->B:LX/EQG;

    if-ne v0, p1, :cond_0

    if-nez p2, :cond_0

    .line 2277052
    :goto_0
    return-void

    .line 2277053
    :cond_0
    iput-object p1, p0, LX/Fje;->B:LX/EQG;

    .line 2277054
    invoke-static {p0}, LX/Fje;->h(LX/Fje;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2277055
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    invoke-interface {v0, p1}, LX/0g8;->d(Landroid/view/View;)V

    .line 2277056
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2277032
    iget-object v0, p0, LX/Fje;->v:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2277033
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2277057
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    const/high16 v0, 0x42400000    # 48.0f

    invoke-direct {v1, v2, v2, v0, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2277058
    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2277059
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v0}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2277060
    iget-object v0, p0, LX/Fje;->v:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2277061
    iget-object v0, p0, LX/Fje;->v:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2277062
    return-void
.end method

.method public getScrollingViewProxy()LX/0g8;
    .locals 1

    .prologue
    .line 2277063
    iget-object v0, p0, LX/Fje;->q:LX/0g8;

    return-object v0
.end method

.method public getSwipeLayout()Lcom/facebook/widget/FbSwipeRefreshLayout;
    .locals 1

    .prologue
    .line 2277064
    iget-object v0, p0, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    return-object v0
.end method

.method public setFilterButtonClickListener(LX/Fe2;)V
    .locals 0

    .prologue
    .line 2277065
    iput-object p1, p0, LX/Fje;->O:LX/Fe2;

    .line 2277066
    return-void
.end method

.method public setIsInitialLoad(Z)V
    .locals 0

    .prologue
    .line 2277067
    iput-boolean p1, p0, LX/Fje;->V:Z

    .line 2277068
    return-void
.end method

.method public setResultPageFadeTransitionDuration(I)V
    .locals 0

    .prologue
    .line 2277069
    iput p1, p0, LX/Fje;->T:I

    .line 2277070
    return-void
.end method

.method public setRetryClickedListener(LX/1DI;)V
    .locals 0

    .prologue
    .line 2277071
    iput-object p1, p0, LX/Fje;->S:LX/1DI;

    .line 2277072
    return-void
.end method

.method public setSearchPivotClickListener(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V
    .locals 0

    .prologue
    .line 2277073
    iput-object p1, p0, LX/Fje;->N:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2277074
    return-void
.end method

.method public setState(LX/EQG;)V
    .locals 1

    .prologue
    .line 2277075
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Fje;->a(LX/EQG;Z)V

    .line 2277076
    return-void
.end method

.method public setTextViewQueryString(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2277077
    iget-object v0, p0, LX/Fje;->m:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_1

    .line 2277078
    iget-object v0, p0, LX/Fje;->h:LX/0ad;

    sget-char v1, LX/100;->u:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2277079
    iget-object v1, p0, LX/Fje;->m:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    aput-object p1, v2, v3

    invoke-static {v0, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2277080
    :cond_1
    return-void
.end method
