.class public final enum LX/FPm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FPm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FPm;

.field public static final enum FOUR_DOLLARS:LX/FPm;

.field public static final enum ONE_DOLLAR:LX/FPm;

.field public static final enum THREE_DOLLARS:LX/FPm;

.field public static final enum TWO_DOLLARS:LX/FPm;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2237908
    new-instance v0, LX/FPm;

    const-string v1, "ONE_DOLLAR"

    invoke-direct {v0, v1, v2}, LX/FPm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPm;->ONE_DOLLAR:LX/FPm;

    .line 2237909
    new-instance v0, LX/FPm;

    const-string v1, "TWO_DOLLARS"

    invoke-direct {v0, v1, v3}, LX/FPm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPm;->TWO_DOLLARS:LX/FPm;

    .line 2237910
    new-instance v0, LX/FPm;

    const-string v1, "THREE_DOLLARS"

    invoke-direct {v0, v1, v4}, LX/FPm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPm;->THREE_DOLLARS:LX/FPm;

    .line 2237911
    new-instance v0, LX/FPm;

    const-string v1, "FOUR_DOLLARS"

    invoke-direct {v0, v1, v5}, LX/FPm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPm;->FOUR_DOLLARS:LX/FPm;

    .line 2237912
    const/4 v0, 0x4

    new-array v0, v0, [LX/FPm;

    sget-object v1, LX/FPm;->ONE_DOLLAR:LX/FPm;

    aput-object v1, v0, v2

    sget-object v1, LX/FPm;->TWO_DOLLARS:LX/FPm;

    aput-object v1, v0, v3

    sget-object v1, LX/FPm;->THREE_DOLLARS:LX/FPm;

    aput-object v1, v0, v4

    sget-object v1, LX/FPm;->FOUR_DOLLARS:LX/FPm;

    aput-object v1, v0, v5

    sput-object v0, LX/FPm;->$VALUES:[LX/FPm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2237913
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FPm;
    .locals 1

    .prologue
    .line 2237914
    const-class v0, LX/FPm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FPm;

    return-object v0
.end method

.method public static values()[LX/FPm;
    .locals 1

    .prologue
    .line 2237915
    sget-object v0, LX/FPm;->$VALUES:[LX/FPm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FPm;

    return-object v0
.end method
