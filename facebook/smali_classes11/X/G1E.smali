.class public final enum LX/G1E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G1E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G1E;

.field public static final enum FULL_DATA:LX/G1E;

.field public static final enum ITEMS:LX/G1E;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2310867
    new-instance v0, LX/G1E;

    const-string v1, "FULL_DATA"

    invoke-direct {v0, v1, v2}, LX/G1E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1E;->FULL_DATA:LX/G1E;

    .line 2310868
    new-instance v0, LX/G1E;

    const-string v1, "ITEMS"

    invoke-direct {v0, v1, v3}, LX/G1E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1E;->ITEMS:LX/G1E;

    .line 2310869
    const/4 v0, 0x2

    new-array v0, v0, [LX/G1E;

    sget-object v1, LX/G1E;->FULL_DATA:LX/G1E;

    aput-object v1, v0, v2

    sget-object v1, LX/G1E;->ITEMS:LX/G1E;

    aput-object v1, v0, v3

    sput-object v0, LX/G1E;->$VALUES:[LX/G1E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2310870
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G1E;
    .locals 1

    .prologue
    .line 2310871
    const-class v0, LX/G1E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G1E;

    return-object v0
.end method

.method public static values()[LX/G1E;
    .locals 1

    .prologue
    .line 2310872
    sget-object v0, LX/G1E;->$VALUES:[LX/G1E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G1E;

    return-object v0
.end method
