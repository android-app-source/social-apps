.class public LX/GPi;
.super LX/6sU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sU",
        "<",
        "Lcom/facebook/common/util/ParcelablePair",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;",
        "Lcom/facebook/adspayments/model/Payment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/GPi;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349312
    const-class v0, Lcom/facebook/adspayments/model/Payment;

    invoke-direct {p0, p1, v0}, LX/6sU;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2349313
    return-void
.end method

.method public static a(LX/0QB;)LX/GPi;
    .locals 4

    .prologue
    .line 2349314
    sget-object v0, LX/GPi;->c:LX/GPi;

    if-nez v0, :cond_1

    .line 2349315
    const-class v1, LX/GPi;

    monitor-enter v1

    .line 2349316
    :try_start_0
    sget-object v0, LX/GPi;->c:LX/GPi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2349317
    if-eqz v2, :cond_0

    .line 2349318
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2349319
    new-instance p0, LX/GPi;

    invoke-static {v0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {p0, v3}, LX/GPi;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349320
    move-object v0, p0

    .line 2349321
    sput-object v0, LX/GPi;->c:LX/GPi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2349322
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2349323
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2349324
    :cond_1
    sget-object v0, LX/GPi;->c:LX/GPi;

    return-object v0

    .line 2349325
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2349326
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0lF;)Lcom/facebook/adspayments/model/Payment;
    .locals 15

    .prologue
    .line 2349327
    const-string v0, "payment_option"

    invoke-static {p0, v0}, LX/16N;->h(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2349328
    const-string v1, "metadata"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v10

    .line 2349329
    new-instance v1, Lcom/facebook/adspayments/model/Payment;

    const-string v2, "payment_details_id"

    invoke-static {p0, v2}, LX/16N;->f(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "amount"

    invoke-static {p0, v3}, LX/16N;->h(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 2349330
    new-instance v11, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v12, "currency"

    invoke-static {v3, v12}, LX/16N;->f(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "offsetted_amount"

    invoke-static {v3, v13}, LX/16N;->h(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v13

    invoke-virtual {v13}, LX/0lF;->x()J

    move-result-wide v13

    invoke-direct {v11, v12, v13, v14}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    move-object v3, v11

    .line 2349331
    const-string v4, "create_date"

    invoke-static {p0, v4}, LX/16N;->h(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->x()J

    move-result-wide v4

    const-string v6, "payment_method_type"

    invoke-static {v0, v6}, LX/16N;->g(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/7YH;->a(Ljava/lang/String;)LX/6zP;

    move-result-object v6

    const-string v7, "credential_id"

    invoke-static {v0, v7}, LX/16N;->f(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v10, :cond_1

    const-string v0, "external_url"

    invoke-static {v10, v0}, LX/16N;->i(LX/0lF;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    :goto_0
    const-string v0, "last_action_status"

    invoke-static {p0, v0}, LX/16N;->g(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/GPV;->of(Ljava/lang/String;)LX/GPV;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/facebook/adspayments/model/Payment;-><init>(Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;JLX/6zP;Ljava/lang/String;Landroid/net/Uri;LX/GPV;)V

    .line 2349332
    invoke-static {v1}, Lcom/facebook/adspayments/model/Boleto;->a(Lcom/facebook/adspayments/model/Payment;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/adspayments/model/Boleto;

    const-string v2, "boleto_number"

    invoke-static {v10, v2}, LX/16N;->f(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "download_link"

    invoke-static {v10, v3}, LX/16N;->i(LX/0lF;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/adspayments/model/Boleto;-><init>(Lcom/facebook/adspayments/model/Payment;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v1, v0

    :cond_0
    return-object v1

    .line 2349333
    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2349334
    check-cast p1, Lcom/facebook/common/util/ParcelablePair;

    .line 2349335
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 2349336
    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 2349337
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "/act_%s"

    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2349338
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2349339
    move-object v0, v2

    .line 2349340
    invoke-virtual {p0}, LX/GPi;->d()Ljava/lang/String;

    move-result-object v2

    .line 2349341
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349342
    move-object v0, v0

    .line 2349343
    const-string v2, "GET"

    .line 2349344
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349345
    move-object v0, v0

    .line 2349346
    const-string v2, "fields"

    const-string v3, "%s.payment_ids([\'%s\'])"

    const-string v4, "payment_details_list"

    invoke-static {v3, v4, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/14O;->a(Ljava/util/Map;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2349347
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349348
    move-object v0, v0

    .line 2349349
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2349350
    check-cast p1, Lcom/facebook/common/util/ParcelablePair;

    .line 2349351
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 2349352
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    const-string v2, "payment_details_list"

    invoke-static {v1, v2}, LX/16N;->h(LX/0lF;Ljava/lang/String;)LX/0lF;

    move-result-object v1

    const-string v2, "data"

    invoke-static {v1, v2}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v1

    .line 2349353
    new-instance v2, LX/GPh;

    invoke-direct {v2, v0}, LX/GPh;-><init>(Ljava/lang/String;)V

    move-object v0, v2

    .line 2349354
    invoke-static {v1, v0}, LX/0Ph;->c(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0Ph;->d(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-static {v0}, LX/GPi;->a(LX/0lF;)Lcom/facebook/adspayments/model/Payment;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349355
    const-string v0, "get_payment_details"

    return-object v0
.end method
