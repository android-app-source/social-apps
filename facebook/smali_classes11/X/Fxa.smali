.class public final LX/Fxa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:LX/Fxf;


# direct methods
.method public constructor <init>(LX/Fxf;)V
    .locals 0

    .prologue
    .line 2305445
    iput-object p1, p0, LX/Fxa;->a:LX/Fxf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 2305446
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    iget-boolean v0, v0, LX/Fxf;->c:Z

    if-nez v0, :cond_c

    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    iget-object v0, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305447
    iget-object v1, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v0, v1

    .line 2305448
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_c

    .line 2305449
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    iget-boolean v0, v0, LX/Fxf;->q:Z

    if-eqz v0, :cond_7

    .line 2305450
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305451
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/Fxf;->e:Ljava/util/List;

    .line 2305452
    const/4 v1, 0x2

    new-array v2, v1, [I

    .line 2305453
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305454
    iget-object v3, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v1, v3

    .line 2305455
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FxZ;

    .line 2305456
    iget-object v1, v1, LX/FxZ;->a:Landroid/view/View;

    .line 2305457
    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2305458
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 2305459
    const/4 v6, 0x0

    aget v6, v2, v6

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 2305460
    const/4 v6, 0x1

    aget v6, v2, v6

    iput v6, v5, Landroid/graphics/Rect;->top:I

    .line 2305461
    iget v6, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    add-int/2addr v6, p1

    iput v6, v5, Landroid/graphics/Rect;->right:I

    .line 2305462
    iget v6, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v6

    iput v1, v5, Landroid/graphics/Rect;->bottom:I

    .line 2305463
    iget-object v1, v0, LX/Fxf;->e:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2305464
    :cond_0
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305465
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v1

    .line 2305466
    invoke-static {v1}, LX/Fxf;->c(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    .line 2305467
    iget-object v1, v0, LX/Fxf;->d:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2305468
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/Fxf;->d:Ljava/util/List;

    .line 2305469
    :goto_1
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305470
    iget-object v3, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v1, v3

    .line 2305471
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FxZ;

    .line 2305472
    iget-object v1, v1, LX/FxZ;->a:Landroid/view/View;

    invoke-static {v1}, LX/Fxf;->c(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 2305473
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 2305474
    iget v6, v1, Landroid/graphics/Rect;->left:I

    iget v7, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    .line 2305475
    iget v7, v1, Landroid/graphics/Rect;->top:I

    iget p1, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, p1

    .line 2305476
    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 2305477
    iget v6, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result p1

    add-int/2addr v6, p1

    iput v6, v5, Landroid/graphics/Rect;->right:I

    .line 2305478
    iput v7, v5, Landroid/graphics/Rect;->top:I

    .line 2305479
    iget v6, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/2addr v1, v6

    iput v1, v5, Landroid/graphics/Rect;->bottom:I

    .line 2305480
    iget-object v1, v0, LX/Fxf;->d:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2305481
    :cond_1
    iget-object v1, v0, LX/Fxf;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_1

    .line 2305482
    :cond_2
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305483
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/Fxf;->g:Ljava/util/List;

    .line 2305484
    iget-object v1, v0, LX/Fxf;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 2305485
    iget-object v3, v0, LX/Fxf;->g:Ljava/util/List;

    invoke-static {v0, v1}, LX/Fxf;->a(LX/Fxf;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2305486
    :cond_3
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305487
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/Fxf;->f:Ljava/util/List;

    .line 2305488
    iget-object v1, v0, LX/Fxf;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 2305489
    iget-object v3, v0, LX/Fxf;->f:Ljava/util/List;

    invoke-static {v0, v1}, LX/Fxf;->a(LX/Fxf;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2305490
    :cond_4
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305491
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/Fxf;->h:Ljava/util/List;

    .line 2305492
    const/4 v1, 0x0

    .line 2305493
    iget-object v2, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305494
    iget-object v3, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v2, v3

    .line 2305495
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FxZ;

    .line 2305496
    new-instance v6, Landroid/view/View;

    iget-object v1, v1, LX/FxZ;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v6, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2305497
    iget-object v1, v0, LX/Fxf;->d:Ljava/util/List;

    add-int/lit8 v3, v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    const/4 v2, 0x1

    invoke-static {v0, v6, v1, v2}, LX/Fxf;->a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 2305498
    const v1, 0x7f0a0564

    invoke-virtual {v6, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2305499
    iget-object v1, v0, LX/Fxf;->h:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2305500
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->addView(Landroid/view/View;)V

    move v2, v3

    .line 2305501
    goto :goto_5

    .line 2305502
    :cond_5
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305503
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v5

    .line 2305504
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->p()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-result-object v6

    .line 2305505
    const/4 v1, 0x0

    .line 2305506
    iget-object v2, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305507
    iget-object v3, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v2, v3

    .line 2305508
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v1

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FxZ;

    .line 2305509
    iget-object p1, v1, LX/FxZ;->a:Landroid/view/View;

    .line 2305510
    invoke-virtual {v6, p1}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->removeView(Landroid/view/View;)V

    .line 2305511
    iget-object v1, v0, LX/Fxf;->d:Ljava/util/List;

    add-int/lit8 v3, v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    const/4 v2, 0x1

    invoke-static {v0, p1, v1, v2}, LX/Fxf;->a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 2305512
    invoke-virtual {v5, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    move v2, v3

    .line 2305513
    goto :goto_6

    .line 2305514
    :cond_6
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305515
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305516
    iget-object v3, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v2, v3

    .line 2305517
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, v0, LX/Fxf;->i:Ljava/util/Map;

    .line 2305518
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305519
    iget-object v3, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v2, v3

    .line 2305520
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, v0, LX/Fxf;->j:Ljava/util/Map;

    .line 2305521
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    iget-object v0, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, LX/Fxa;->a:LX/Fxf;

    iget-object v1, v1, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2305522
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    iget-object v0, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->requestLayout()V

    .line 2305523
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    iget-object v0, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->p()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;->setVisibility(I)V

    .line 2305524
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    const/4 v1, 0x0

    .line 2305525
    iput-boolean v1, v0, LX/Fxf;->q:Z

    .line 2305526
    :cond_7
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305527
    const/4 v1, 0x0

    .line 2305528
    iget-object v2, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305529
    iget-object v3, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v2, v3

    .line 2305530
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_7
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FxZ;

    .line 2305531
    iget-object v6, v1, LX/FxZ;->a:Landroid/view/View;

    iget-object v1, v0, LX/Fxf;->f:Ljava/util/List;

    add-int/lit8 v3, v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    invoke-static {v0, v6, v1}, LX/Fxf;->a(LX/Fxf;Landroid/view/View;Landroid/graphics/Rect;)V

    move v2, v3

    .line 2305532
    goto :goto_7

    .line 2305533
    :cond_8
    iget-object v1, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    .line 2305534
    iget-object v2, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->s:Ljava/util/List;

    move-object v1, v2

    .line 2305535
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FxZ;

    .line 2305536
    iget-object v3, v1, LX/FxZ;->d:Landroid/view/View;

    .line 2305537
    invoke-virtual {v3}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    check-cast v1, LX/Fxd;

    .line 2305538
    if-nez v1, :cond_9

    .line 2305539
    new-instance v1, LX/Fxd;

    invoke-direct {v1, v0, v3}, LX/Fxd;-><init>(LX/Fxf;Landroid/view/View;)V

    .line 2305540
    :cond_9
    const/4 v5, 0x0

    iput-boolean v5, v1, LX/Fxd;->c:Z

    .line 2305541
    invoke-virtual {v1}, LX/Fxd;->cancel()V

    .line 2305542
    invoke-virtual {v1}, LX/Fxd;->reset()V

    .line 2305543
    invoke-virtual {v3, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_8

    .line 2305544
    :cond_a
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305545
    iput-boolean v4, v0, LX/Fxf;->c:Z

    .line 2305546
    iget-object v0, p0, LX/Fxa;->a:LX/Fxf;

    iget-object v0, v0, LX/Fxf;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosWysiwygFragment;->o()Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;

    move-result-object v0

    .line 2305547
    iget-object v1, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305548
    iget v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->b:F

    move v2, v2

    .line 2305549
    iget v3, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->c:F

    move v3, v3

    .line 2305550
    const/4 v5, 0x0

    move v6, v5

    :goto_9
    iget-object v5, v1, LX/Fxf;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_b

    .line 2305551
    iget-object v5, v1, LX/Fxf;->e:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Rect;

    float-to-int v7, v2

    float-to-int p1, v3

    invoke-virtual {v5, v7, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 2305552
    iput v6, v1, LX/Fxf;->k:I

    .line 2305553
    invoke-static {v1}, LX/Fxf;->e(LX/Fxf;)Landroid/view/View;

    move-result-object v5

    .line 2305554
    invoke-virtual {v5}, Landroid/view/View;->bringToFront()V

    .line 2305555
    :cond_b
    iget-object v1, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305556
    iget v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->b:F

    move v2, v2

    .line 2305557
    iput v2, v1, LX/Fxf;->l:F

    .line 2305558
    iget-object v1, p0, LX/Fxa;->a:LX/Fxf;

    .line 2305559
    iget v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/DragAndDropFrame;->c:F

    move v0, v2

    .line 2305560
    iput v0, v1, LX/Fxf;->m:F

    .line 2305561
    :cond_c
    return v4

    .line 2305562
    :cond_d
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_9
.end method
