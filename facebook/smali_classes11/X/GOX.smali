.class public final LX/GOX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
        "LX/0am",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;)V
    .locals 0

    .prologue
    .line 2347654
    iput-object p1, p0, LX/GOX;->a:Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)LX/0am;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
            ")",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2347651
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->g()Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2347652
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v1, v1

    .line 2347653
    invoke-static {v1}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v1

    const-class v2, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    invoke-virtual {v1, v2}, LX/0wv;->a(Ljava/lang/Class;)LX/0wv;

    move-result-object v1

    invoke-virtual {v1}, LX/0wv;->a()LX/0am;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0am;->or(LX/0am;)LX/0am;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2347650
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-static {p1}, LX/GOX;->a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)LX/0am;

    move-result-object v0

    return-object v0
.end method
