.class public final LX/Gc0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:[I

.field public final synthetic c:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

.field public final synthetic d:LX/Gc1;


# direct methods
.method public constructor <init>(LX/Gc1;I[ILcom/facebook/auth/credentials/DBLFacebookCredentials;)V
    .locals 0

    .prologue
    .line 2370898
    iput-object p1, p0, LX/Gc0;->d:LX/Gc1;

    iput p2, p0, LX/Gc0;->a:I

    iput-object p3, p0, LX/Gc0;->b:[I

    iput-object p4, p0, LX/Gc0;->c:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 2370899
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    .line 2370900
    iget-object v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k:LX/0hs;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->g:Z

    if-eqz v1, :cond_0

    .line 2370901
    iget-object v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k:LX/0hs;

    invoke-virtual {v1}, LX/0ht;->l()V

    .line 2370902
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->g:Z

    .line 2370903
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->k:LX/0hs;

    .line 2370904
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2370905
    :cond_1
    :goto_0
    return v4

    .line 2370906
    :pswitch_0
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v1, p0, LX/Gc0;->a:I

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(ILandroid/view/View;Landroid/view/MotionEvent;)V

    .line 2370907
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    if-ne v0, v3, :cond_1

    .line 2370908
    iget-object v0, p0, LX/Gc0;->b:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2370909
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v1, p0, LX/Gc0;->a:I

    iput v1, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    .line 2370910
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 2370911
    :pswitch_1
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v1, p0, LX/Gc0;->a:I

    invoke-virtual {v0, v1, p1}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b(ILandroid/view/View;)V

    .line 2370912
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    iget v1, p0, LX/Gc0;->a:I

    if-ne v0, v1, :cond_1

    .line 2370913
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iput v3, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    .line 2370914
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->p()V

    goto :goto_0

    .line 2370915
    :pswitch_2
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    if-ltz v0, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    if-ltz v0, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    if-le v0, v1, :cond_1

    .line 2370916
    :cond_2
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v1, p0, LX/Gc0;->a:I

    invoke-virtual {v0, v1, p1}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->b(ILandroid/view/View;)V

    .line 2370917
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    iget v1, p0, LX/Gc0;->a:I

    if-ne v0, v1, :cond_1

    .line 2370918
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iput v3, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    .line 2370919
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->p()V

    goto/16 :goto_0

    .line 2370920
    :pswitch_3
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v1, p0, LX/Gc0;->a:I

    invoke-virtual {v0, v1, p1}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->c(ILandroid/view/View;)V

    .line 2370921
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->f:I

    iget v1, p0, LX/Gc0;->a:I

    if-ne v0, v1, :cond_1

    .line 2370922
    const-string v0, "add_user"

    iget-object v1, p0, LX/Gc0;->c:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {v1}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2370923
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->d:LX/2Ad;

    invoke-interface {v0, v4}, LX/2Ad;->l_(Z)V

    goto/16 :goto_0

    .line 2370924
    :cond_3
    iget-object v0, p0, LX/Gc0;->d:LX/Gc1;

    iget-object v0, v0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v1, p0, LX/Gc0;->c:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget v2, p0, LX/Gc0;->a:I

    iget-object v3, p0, LX/Gc0;->b:[I

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;Landroid/view/View;I[I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
