.class public final enum LX/H86;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H86;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H86;

.field public static final enum IN_STORE:LX/H86;

.field public static final enum ONLINE:LX/H86;


# instance fields
.field private final mTabName:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2432934
    new-instance v0, LX/H86;

    const-string v1, "ONLINE"

    const v2, 0x7f081cb7

    invoke-direct {v0, v1, v3, v2}, LX/H86;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H86;->ONLINE:LX/H86;

    .line 2432935
    new-instance v0, LX/H86;

    const-string v1, "IN_STORE"

    const v2, 0x7f081cb8

    invoke-direct {v0, v1, v4, v2}, LX/H86;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H86;->IN_STORE:LX/H86;

    .line 2432936
    const/4 v0, 0x2

    new-array v0, v0, [LX/H86;

    sget-object v1, LX/H86;->ONLINE:LX/H86;

    aput-object v1, v0, v3

    sget-object v1, LX/H86;->IN_STORE:LX/H86;

    aput-object v1, v0, v4

    sput-object v0, LX/H86;->$VALUES:[LX/H86;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2432937
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2432938
    iput p3, p0, LX/H86;->mTabName:I

    .line 2432939
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H86;
    .locals 1

    .prologue
    .line 2432940
    const-class v0, LX/H86;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H86;

    return-object v0
.end method

.method public static values()[LX/H86;
    .locals 1

    .prologue
    .line 2432941
    sget-object v0, LX/H86;->$VALUES:[LX/H86;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H86;

    return-object v0
.end method


# virtual methods
.method public final getTabNameStringRes()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 2432942
    iget v0, p0, LX/H86;->mTabName:I

    return v0
.end method
