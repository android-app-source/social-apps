.class public abstract LX/Gos;
.super LX/Gor;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/Gob;",
        ">",
        "LX/Gor",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/CHX;

.field public final c:LX/GoE;

.field public final d:LX/CHa;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLInterfaces$InstantShoppingAnnotationsFragment$;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/2uF;

.field public h:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:LX/CHZ;


# direct methods
.method public constructor <init>(LX/CHi;II)V
    .locals 3

    .prologue
    .line 2394588
    invoke-direct {p0, p2, p3}, LX/Gor;-><init>(II)V

    .line 2394589
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/Gos;->h:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394590
    invoke-interface {p1}, LX/CHg;->o()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;

    move-result-object v0

    invoke-static {v0}, LX/Got;->b(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->a:Ljava/lang/String;

    .line 2394591
    invoke-interface {p1}, LX/CHi;->j()LX/CHX;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->b:LX/CHX;

    .line 2394592
    new-instance v0, LX/GoE;

    invoke-interface {p1}, LX/CHg;->jt_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/CHg;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/Gos;->c:LX/GoE;

    .line 2394593
    invoke-interface {p1}, LX/CHg;->c()LX/CHa;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->d:LX/CHa;

    .line 2394594
    invoke-interface {p1}, LX/CHi;->m()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->e:Ljava/util/List;

    .line 2394595
    invoke-interface {p1}, LX/CHg;->jw_()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->f:Ljava/util/List;

    .line 2394596
    invoke-interface {p1}, LX/CHi;->n()LX/2uF;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->g:LX/2uF;

    .line 2394597
    return-void
.end method

.method public constructor <init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;II)V
    .locals 3

    .prologue
    .line 2394578
    invoke-direct {p0, p2, p3}, LX/Gor;-><init>(II)V

    .line 2394579
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/Gos;->h:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394580
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;->o()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;

    move-result-object v0

    invoke-static {v0}, LX/Got;->b(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->a:Ljava/lang/String;

    .line 2394581
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;->j()LX/CHX;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->b:LX/CHX;

    .line 2394582
    new-instance v0, LX/GoE;

    invoke-interface {p1}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/Gos;->c:LX/GoE;

    .line 2394583
    invoke-interface {p1}, LX/CHb;->c()LX/CHa;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->d:LX/CHa;

    .line 2394584
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;->m()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->e:Ljava/util/List;

    .line 2394585
    invoke-interface {p1}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->f:Ljava/util/List;

    .line 2394586
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;->n()LX/2uF;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->g:LX/2uF;

    .line 2394587
    return-void
.end method

.method public constructor <init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V
    .locals 3

    .prologue
    .line 2394568
    invoke-direct {p0, p2, p3}, LX/Gor;-><init>(II)V

    .line 2394569
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, LX/Gos;->h:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394570
    invoke-interface {p1}, LX/CHg;->o()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;

    move-result-object v0

    invoke-static {v0}, LX/Got;->b(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->a:Ljava/lang/String;

    .line 2394571
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->C()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ActionModel;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->b:LX/CHX;

    .line 2394572
    new-instance v0, LX/GoE;

    invoke-interface {p1}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/Gos;->c:LX/GoE;

    .line 2394573
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->M()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->d:LX/CHa;

    .line 2394574
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->m()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->e:Ljava/util/List;

    .line 2394575
    invoke-interface {p1}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->f:Ljava/util/List;

    .line 2394576
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->n()LX/2uF;

    move-result-object v0

    iput-object v0, p0, LX/Gos;->g:LX/2uF;

    .line 2394577
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Gos;
    .locals 0

    .prologue
    .line 2394565
    iput-object p1, p0, LX/Gos;->h:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394566
    return-object p0
.end method

.method public c()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1

    .prologue
    .line 2394567
    iget-object v0, p0, LX/Gos;->h:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method
