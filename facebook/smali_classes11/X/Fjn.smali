.class public final LX/Fjn;
.super LX/EmY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EmY",
        "<",
        "LX/Ema;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fjp;


# direct methods
.method public constructor <init>(LX/Fjp;)V
    .locals 0

    .prologue
    .line 2277670
    iput-object p1, p0, LX/Fjn;->a:LX/Fjp;

    invoke-direct {p0}, LX/EmY;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/Ema;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2277684
    const-class v0, LX/Ema;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2277671
    check-cast p1, LX/Ema;

    .line 2277672
    iget-object v0, p1, LX/EmS;->a:LX/EmM;

    move-object v0, v0

    .line 2277673
    sget-object v1, LX/EmM;->APP_UPDATE:LX/EmM;

    if-eq v0, v1, :cond_0

    .line 2277674
    :goto_0
    return-void

    .line 2277675
    :cond_0
    iget-object v0, p0, LX/Fjn;->a:LX/Fjp;

    .line 2277676
    iget-object v1, p1, LX/Ema;->c:LX/EmZ;

    move-object v1, v1

    .line 2277677
    sget-object v2, LX/EmZ;->QUEUE_DOWNLOAD:LX/EmZ;

    if-ne v1, v2, :cond_2

    .line 2277678
    iget-object v1, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fjj;

    const-string v2, "selfupdate_queue_download"

    const-string v3, "extra_args"

    .line 2277679
    iget-object p0, p1, LX/Ema;->d:Ljava/lang/String;

    move-object p0, p0

    .line 2277680
    invoke-static {v3, p0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277681
    :cond_1
    :goto_1
    goto :goto_0

    .line 2277682
    :cond_2
    sget-object v2, LX/EmZ;->CREATED_FILE:LX/EmZ;

    if-ne v1, v2, :cond_1

    .line 2277683
    iget-object v1, v0, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/Fjg;->g:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    goto :goto_1
.end method
