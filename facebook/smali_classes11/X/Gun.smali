.class public final LX/Gun;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 2403884
    iput-object p1, p0, LX/Gun;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403885
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403886
    const-string v0, "ShowShareComposerHandler"

    iput-object v0, p0, LX/Gun;->c:Ljava/lang/String;

    .line 2403887
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2403888
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gun;->e:Z

    .line 2403889
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gun;->d:Ljava/lang/String;

    .line 2403890
    return-void
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2403891
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403892
    iget-object v1, p0, LX/Gun;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const-string v2, "callback"

    invoke-interface {p2, v0, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2403893
    iput-object v2, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    .line 2403894
    const-string v1, "fbid"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403895
    const-string v2, "title"

    invoke-interface {p2, v0, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2403896
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2403897
    iget-object v0, p0, LX/Gun;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    const-string v1, "ShowShareComposerHandler"

    const-string v2, "blank fbid"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2403898
    :goto_0
    return-void

    .line 2403899
    :cond_0
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2403900
    iget-object v0, p0, LX/Gun;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    const-string v1, "ShowShareComposerHandler"

    const-string v2, "blank title"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2403901
    :cond_1
    iget-boolean v3, p0, LX/Gun;->e:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/Gun;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2403902
    const-string v0, "ShowShareComposerHandler"

    const-string v1, "duplicate onclick for share composer"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2403903
    :cond_2
    const-string v3, "caption"

    invoke-interface {p2, v0, v3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2403904
    const-string v4, "preview_image_url"

    invoke-interface {p2, v0, v4}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2403905
    const-string v5, "object_type"

    invoke-interface {p2, v0, v5}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403906
    new-instance v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v5, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v5}, LX/16z;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    .line 2403907
    new-instance v5, LX/39x;

    invoke-direct {v5}, LX/39x;-><init>()V

    .line 2403908
    iput-object v2, v5, LX/39x;->t:Ljava/lang/String;

    .line 2403909
    move-object v2, v5

    .line 2403910
    iput-object v3, v2, LX/39x;->r:Ljava/lang/String;

    .line 2403911
    move-object v2, v2

    .line 2403912
    new-instance v3, LX/4XB;

    invoke-direct {v3}, LX/4XB;-><init>()V

    new-instance v5, LX/2dc;

    invoke-direct {v5}, LX/2dc;-><init>()V

    .line 2403913
    iput-object v4, v5, LX/2dc;->h:Ljava/lang/String;

    .line 2403914
    move-object v4, v5

    .line 2403915
    invoke-virtual {v4}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2403916
    iput-object v4, v3, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2403917
    move-object v3, v3

    .line 2403918
    invoke-virtual {v3}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 2403919
    iput-object v3, v2, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2403920
    move-object v2, v2

    .line 2403921
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2403922
    iget-object v3, p0, LX/Gun;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v3, v3, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Z:LX/1Kf;

    const/4 v4, 0x0

    sget-object v5, LX/21D;->FACEWEB:LX/21D;

    const-string v6, "facewebShare"

    invoke-static {v0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v0

    .line 2403923
    iput-object v2, v0, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2403924
    move-object v0, v0

    .line 2403925
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v5, v6, v0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/16 v2, 0xb

    iget-object v5, p0, LX/Gun;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v5}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v5

    invoke-interface {v3, v4, v0, v2, v5}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2403926
    iput-object v1, p0, LX/Gun;->d:Ljava/lang/String;

    .line 2403927
    iput-boolean v7, p0, LX/Gun;->e:Z

    goto/16 :goto_0
.end method
