.class public final LX/GdR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;)V
    .locals 0

    .prologue
    .line 2373955
    iput-object p1, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 6

    .prologue
    .line 2373956
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    invoke-static {v0, p1}, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->d(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;I)V

    .line 2373957
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    .line 2373958
    iput p1, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->s:I

    .line 2373959
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->p:LX/Gda;

    sget-object v1, LX/Gda;->BACK_BUTTON:LX/Gda;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->p:LX/Gda;

    sget-object v1, LX/Gda;->NEWS_FEED_TAB:LX/Gda;

    if-ne v0, v1, :cond_1

    .line 2373960
    :cond_0
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v5, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->p:LX/Gda;

    .line 2373961
    :goto_0
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->c:LX/Gdb;

    iget-object v1, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v1, v1, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->l:LX/0gE;

    invoke-virtual {v1, p1}, LX/0gE;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    iget-object v2, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v4, v3, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->k:Ljava/util/List;

    move v3, p1

    invoke-virtual/range {v0 .. v5}, LX/Gdb;->a(Landroid/support/v4/app/Fragment;Landroid/content/Context;ILjava/util/List;LX/Gda;)V

    .line 2373962
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    const/4 v1, 0x0

    .line 2373963
    iput-boolean v1, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->q:Z

    .line 2373964
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    sget-object v1, LX/Gda;->INTERNAL_RESET:LX/Gda;

    .line 2373965
    iput-object v1, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->p:LX/Gda;

    .line 2373966
    return-void

    .line 2373967
    :cond_1
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-boolean v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->q:Z

    if-eqz v0, :cond_2

    sget-object v5, LX/Gda;->TOPIC_SWIPE:LX/Gda;

    goto :goto_0

    :cond_2
    sget-object v5, LX/Gda;->TOPIC_TAB:LX/Gda;

    goto :goto_0
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2373954
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2373948
    iget-object v1, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v2, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget v2, v2, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->r:I

    if-ne v2, v0, :cond_0

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    .line 2373949
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->q:Z

    .line 2373950
    iget-object v0, p0, LX/GdR;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    .line 2373951
    iput p1, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->r:I

    .line 2373952
    return-void

    .line 2373953
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
