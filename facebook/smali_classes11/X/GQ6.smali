.class public LX/GQ6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2349711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/5pG;)Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;
    .locals 13

    .prologue
    .line 2349712
    const-string v0, "budget"

    invoke-interface {p0, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v5

    .line 2349713
    new-instance v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    const-string v1, "flow_name"

    invoke-interface {p0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "account_id"

    invoke-interface {p0, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/6xj;->PICKER_SCREEN:LX/6xj;

    .line 2349714
    new-instance v7, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v8, "currency"

    invoke-interface {v5, v8}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/math/BigDecimal;

    const-string v10, "amount"

    invoke-interface {v5, v10}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v11

    invoke-direct {v9, v11, v12}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-direct {v7, v8, v9}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    move-object v4, v7

    .line 2349715
    const-string v6, "daily_budget"

    invoke-interface {v5, v6}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    const-string v6, "stored_balance_status"

    invoke-interface {p0, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/ADX;->of(Ljava/lang/String;)LX/ADX;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;-><init>(Ljava/lang/String;Ljava/lang/String;LX/6xj;Lcom/facebook/payments/currency/CurrencyAmount;ZLX/ADX;)V

    return-object v0
.end method
