.class public LX/Gil;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Landroid/widget/EditText;

.field private final d:Landroid/content/Context;

.field private e:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/EditText;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2386459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2386460
    iput-object p1, p0, LX/Gil;->d:Landroid/content/Context;

    .line 2386461
    iput-object p2, p0, LX/Gil;->c:Landroid/widget/EditText;

    .line 2386462
    iput p3, p0, LX/Gil;->a:I

    .line 2386463
    iput-object p4, p0, LX/Gil;->b:Ljava/lang/String;

    .line 2386464
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2386465
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    iget v1, p0, LX/Gil;->a:I

    if-le v0, v1, :cond_1

    .line 2386466
    iget-object v0, p0, LX/Gil;->e:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 2386467
    iget-object v0, p0, LX/Gil;->e:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 2386468
    :cond_0
    iget-object v0, p0, LX/Gil;->d:Landroid/content/Context;

    iget-object v1, p0, LX/Gil;->b:Ljava/lang/String;

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, LX/Gil;->e:Landroid/widget/Toast;

    .line 2386469
    iget-object v0, p0, LX/Gil;->e:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 2386470
    iget-object v0, p0, LX/Gil;->e:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2386471
    iget-object v0, p0, LX/Gil;->c:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 2386472
    iget-object v1, p0, LX/Gil;->c:Landroid/widget/EditText;

    iget-object v2, p0, LX/Gil;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    iget v3, p0, LX/Gil;->a:I

    invoke-interface {v2, v4, v3}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2386473
    iget-object v1, p0, LX/Gil;->c:Landroid/widget/EditText;

    iget v2, p0, LX/Gil;->a:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 2386474
    :cond_1
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2386475
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2386476
    return-void
.end method
