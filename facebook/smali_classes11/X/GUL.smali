.class public final LX/GUL;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 2357353
    iput-object p1, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2357354
    iget-object v0, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-boolean v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ak:Z

    if-nez v0, :cond_0

    .line 2357355
    iget-object v0, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    sget-object v1, LX/GUk;->FETCH_DATA:LX/GUk;

    invoke-virtual {v0, v1}, LX/GUl;->b(LX/GUk;)V

    .line 2357356
    iget-object v0, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    sget-object v1, LX/GUk;->RENDER:LX/GUk;

    invoke-virtual {v0, v1}, LX/GUl;->a(LX/GUk;)V

    .line 2357357
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2357358
    check-cast v0, Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;

    .line 2357359
    iget-object v1, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    .line 2357360
    invoke-static {v1, v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->a$redex0(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;)V

    .line 2357361
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2357362
    iget-object v0, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-boolean v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ak:Z

    if-nez v0, :cond_0

    .line 2357363
    iget-object v0, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    sget-object v1, LX/GUk;->FETCH_DATA:LX/GUk;

    invoke-virtual {v0, v1}, LX/GUl;->c(LX/GUk;)V

    .line 2357364
    iget-object v0, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->X:LX/GUl;

    sget-object v1, LX/GUk;->OVERALL_TTI:LX/GUk;

    invoke-virtual {v0, v1}, LX/GUl;->c(LX/GUk;)V

    .line 2357365
    iget-object v0, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    const/4 v1, 0x1

    .line 2357366
    iput-boolean v1, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ak:Z

    .line 2357367
    :cond_0
    iget-object v0, p0, LX/GUL;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->K(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    .line 2357368
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2357369
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/GUL;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
