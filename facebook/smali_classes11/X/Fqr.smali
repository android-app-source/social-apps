.class public LX/Fqr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/Fqr;


# instance fields
.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2294582
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "2"

    aput-object v1, v0, v3

    const-string v1, "23"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "28"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "24"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "25"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "31"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "29"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "21"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/Fqr;->c:Ljava/util/Collection;

    .line 2294583
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "26"

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/Fqr;->d:Ljava/util/Collection;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294585
    iput-object p1, p0, LX/Fqr;->e:LX/0ad;

    .line 2294586
    return-void
.end method

.method public static a(LX/0QB;)LX/Fqr;
    .locals 4

    .prologue
    .line 2294587
    sget-object v0, LX/Fqr;->f:LX/Fqr;

    if-nez v0, :cond_1

    .line 2294588
    const-class v1, LX/Fqr;

    monitor-enter v1

    .line 2294589
    :try_start_0
    sget-object v0, LX/Fqr;->f:LX/Fqr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2294590
    if-eqz v2, :cond_0

    .line 2294591
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2294592
    new-instance p0, LX/Fqr;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/Fqr;-><init>(LX/0ad;)V

    .line 2294593
    move-object v0, p0

    .line 2294594
    sput-object v0, LX/Fqr;->f:LX/Fqr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2294595
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2294596
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2294597
    :cond_1
    sget-object v0, LX/Fqr;->f:LX/Fqr;

    return-object v0

    .line 2294598
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2294599
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0zO;LX/0w5;LX/0t2;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO;",
            "LX/0w5",
            "<*>;",
            "LX/0t2;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2294600
    iget-object v0, p0, LX/Fqr;->e:LX/0ad;

    sget-short v1, LX/0wf;->j:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2294601
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 2294602
    invoke-virtual {p1}, LX/0zO;->d()LX/0w7;

    move-result-object v1

    sget-object v2, LX/Fqr;->c:Ljava/util/Collection;

    invoke-virtual {p3, v0, p2, v1, v2}, LX/0t2;->a(LX/0gW;LX/0w5;LX/0w7;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    .line 2294603
    :goto_0
    return-object v0

    .line 2294604
    :cond_0
    iget-object v0, p1, LX/0zO;->m:LX/0gW;

    move-object v0, v0

    .line 2294605
    invoke-virtual {p1}, LX/0zO;->d()LX/0w7;

    move-result-object v1

    sget-object v2, LX/Fqr;->d:Ljava/util/Collection;

    invoke-virtual {p3, v0, p2, v1, v2}, LX/0t2;->b(LX/0gW;LX/0w5;LX/0w7;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
