.class public final enum LX/FyW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FyW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FyW;

.field public static final enum COVER_PHOTO:LX/FyW;

.field public static final enum PROFILE_PHOTO:LX/FyW;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2306601
    new-instance v0, LX/FyW;

    const-string v1, "COVER_PHOTO"

    invoke-direct {v0, v1, v2}, LX/FyW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FyW;->COVER_PHOTO:LX/FyW;

    .line 2306602
    new-instance v0, LX/FyW;

    const-string v1, "PROFILE_PHOTO"

    invoke-direct {v0, v1, v3}, LX/FyW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FyW;->PROFILE_PHOTO:LX/FyW;

    .line 2306603
    const/4 v0, 0x2

    new-array v0, v0, [LX/FyW;

    sget-object v1, LX/FyW;->COVER_PHOTO:LX/FyW;

    aput-object v1, v0, v2

    sget-object v1, LX/FyW;->PROFILE_PHOTO:LX/FyW;

    aput-object v1, v0, v3

    sput-object v0, LX/FyW;->$VALUES:[LX/FyW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2306598
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FyW;
    .locals 1

    .prologue
    .line 2306600
    const-class v0, LX/FyW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FyW;

    return-object v0
.end method

.method public static values()[LX/FyW;
    .locals 1

    .prologue
    .line 2306599
    sget-object v0, LX/FyW;->$VALUES:[LX/FyW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FyW;

    return-object v0
.end method
