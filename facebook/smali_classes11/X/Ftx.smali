.class public LX/Ftx;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2299385
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2299386
    const-class v0, LX/Ftx;

    invoke-static {v0, p0}, LX/Ftx;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2299387
    const v0, 0x7f0305fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2299388
    invoke-virtual {p0}, LX/Ftx;->getContext()Landroid/content/Context;

    move-result-object v0

    const p1, 0x7f0217e1

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2299389
    new-instance p1, LX/0zw;

    const v0, 0x7f0d1066

    invoke-virtual {p0, v0}, LX/Ftx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {p1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object p1, p0, LX/Ftx;->b:LX/0zw;

    .line 2299390
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ftx;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object p0

    check-cast p0, LX/23P;

    iput-object p0, p1, LX/Ftx;->a:LX/23P;

    return-void
.end method
