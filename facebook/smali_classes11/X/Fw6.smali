.class public LX/Fw6;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/BP9;
.implements LX/2ea;


# instance fields
.field public a:LX/Fsr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FrB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Fw8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fv7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/FwD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/BQB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:I

.field public final i:Lcom/facebook/timeline/header/TimelineContextItemsSection;

.field private final j:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;",
            ">;>;"
        }
    .end annotation
.end field

.field public m:LX/BP0;

.field public n:LX/BQ1;

.field public o:LX/FrH;

.field public p:I

.field public q:I

.field public r:Z

.field public s:LX/FrA;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v1, -0x1

    .line 2302653
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2302654
    iput v1, p0, LX/Fw6;->p:I

    .line 2302655
    iput v1, p0, LX/Fw6;->q:I

    .line 2302656
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, LX/Fw6;

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v4

    check-cast v4, LX/Fsr;

    const-class v5, LX/FrB;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/FrB;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/Fw8;->a(LX/0QB;)LX/Fw8;

    move-result-object v7

    check-cast v7, LX/Fw8;

    const/16 v8, 0x3665

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/FwD;->a(LX/0QB;)LX/FwD;

    move-result-object v9

    check-cast v9, LX/FwD;

    invoke-static {v0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v0

    check-cast v0, LX/BQB;

    iput-object v4, v3, LX/Fw6;->a:LX/Fsr;

    iput-object v5, v3, LX/Fw6;->b:LX/FrB;

    iput-object v6, v3, LX/Fw6;->c:LX/0ad;

    iput-object v7, v3, LX/Fw6;->d:LX/Fw8;

    iput-object v8, v3, LX/Fw6;->e:LX/0Or;

    iput-object v9, v3, LX/Fw6;->f:LX/FwD;

    iput-object v0, v3, LX/Fw6;->g:LX/BQB;

    .line 2302657
    invoke-virtual {p0, v1}, LX/Fw6;->setBackgroundColor(I)V

    .line 2302658
    iget-object v0, p0, LX/Fw6;->g:LX/BQB;

    .line 2302659
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineInflateHeader"

    invoke-virtual {v1, v2}, LX/BQD;->a(Ljava/lang/String;)V

    .line 2302660
    const v0, 0x7f031564

    move v0, v0

    .line 2302661
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2302662
    iget-object v0, p0, LX/Fw6;->g:LX/BQB;

    .line 2302663
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineInflateHeader"

    invoke-virtual {v1, v2}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2302664
    const v0, 0x7f0d3021

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/TimelineContextItemsSection;

    iput-object v0, p0, LX/Fw6;->i:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    .line 2302665
    const v0, 0x7f0d301d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2302666
    new-instance v1, LX/0zw;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/Fw6;->j:LX/0zw;

    .line 2302667
    const v0, 0x7f0d301f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2302668
    new-instance v1, LX/0zw;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/Fw6;->k:LX/0zw;

    .line 2302669
    const v0, 0x7f0d301c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->b(I)LX/0am;

    move-result-object v0

    .line 2302670
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2302671
    new-instance v1, LX/0zw;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Fw6;->l:LX/0am;

    .line 2302672
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Fw6;->setOrientation(I)V

    .line 2302673
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, LX/Fw6;->h:I

    .line 2302674
    return-void

    .line 2302675
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Fw6;->l:LX/0am;

    goto :goto_0
.end method

.method public static e(LX/Fw6;)V
    .locals 6

    .prologue
    .line 2302676
    iget-object v0, p0, LX/Fw6;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fv7;

    iget-object v1, p0, LX/Fw6;->j:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;

    iget-object v2, p0, LX/Fw6;->k:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;

    iget-object v3, p0, LX/Fw6;->m:LX/BP0;

    invoke-virtual {v3}, LX/5SB;->i()Z

    move-result v3

    .line 2302677
    invoke-virtual {v1}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2302678
    if-eqz v3, :cond_1

    .line 2302679
    new-instance v5, LX/1EA;

    invoke-direct {v5}, LX/1EA;-><init>()V

    const p0, 0x7f081405

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2302680
    iput-object p0, v5, LX/1EA;->o:Ljava/lang/String;

    .line 2302681
    move-object v5, v5

    .line 2302682
    const p0, 0x7f08154d

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2302683
    iput-object p0, v5, LX/1EA;->l:Ljava/lang/String;

    .line 2302684
    move-object v5, v5

    .line 2302685
    const p0, 0x7f0814d8

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2302686
    iput-object p0, v5, LX/1EA;->m:Ljava/lang/String;

    .line 2302687
    move-object v5, v5

    .line 2302688
    const p0, 0x7f08154f

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2302689
    iput-object p0, v5, LX/1EA;->n:Ljava/lang/String;

    .line 2302690
    move-object v5, v5

    .line 2302691
    const p0, 0x7f020dfb

    .line 2302692
    iput p0, v5, LX/1EA;->e:I

    .line 2302693
    move-object v5, v5

    .line 2302694
    const/4 p0, 0x1

    .line 2302695
    iput-boolean p0, v5, LX/1EA;->q:Z

    .line 2302696
    move-object v5, v5

    .line 2302697
    invoke-virtual {v5}, LX/1EA;->a()LX/1EE;

    move-result-object v5

    move-object v4, v5

    .line 2302698
    :goto_0
    invoke-static {v0, v1, v4}, LX/Fv7;->a(LX/Fv7;Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;LX/1EE;)V

    .line 2302699
    invoke-virtual {v2}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object p0, v0, LX/Fv7;->b:LX/0W9;

    const/4 v1, 0x1

    invoke-static {v5, v4, p0, v1}, LX/1aH;->a(Landroid/content/res/Resources;LX/1EE;LX/0W9;Z)LX/1aH;

    move-result-object p0

    .line 2302700
    if-eqz v3, :cond_2

    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v2, v5}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonVisibility(I)V

    .line 2302701
    iget-object v5, p0, LX/1aH;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v5}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2302702
    iget-object v5, p0, LX/1aH;->b:Ljava/lang/String;

    iget-object v1, p0, LX/1aH;->a:Ljava/lang/String;

    iget-object p0, p0, LX/1aH;->c:Ljava/lang/String;

    invoke-virtual {v2, v5, v1, p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2302703
    if-eqz v3, :cond_3

    iget-object v5, v0, LX/Fv7;->c:LX/G2m;

    invoke-virtual {v5}, LX/G2m;->c()Landroid/view/View$OnClickListener;

    move-result-object v5

    :goto_2
    invoke-virtual {v2, v5}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setCheckinButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302704
    iget-boolean v5, v4, LX/1EE;->t:Z

    move v5, v5

    .line 2302705
    if-eqz v5, :cond_4

    iget-object v5, v0, LX/Fv7;->c:LX/G2m;

    .line 2302706
    iget-object p0, v5, LX/G2m;->i:Landroid/view/View$OnClickListener;

    if-nez p0, :cond_0

    .line 2302707
    new-instance p0, LX/G2l;

    invoke-direct {p0, v5}, LX/G2l;-><init>(LX/G2m;)V

    iput-object p0, v5, LX/G2m;->i:Landroid/view/View$OnClickListener;

    .line 2302708
    :cond_0
    iget-object p0, v5, LX/G2m;->i:Landroid/view/View$OnClickListener;

    move-object v5, p0

    .line 2302709
    :goto_3
    invoke-virtual {v2, v5}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setStatusButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302710
    iget-object v5, v0, LX/Fv7;->c:LX/G2m;

    invoke-virtual {v5}, LX/G2m;->b()Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/facebook/feed/inlinecomposer/InlineComposerFooterView;->setPhotoButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302711
    return-void

    .line 2302712
    :cond_1
    new-instance v5, LX/1EA;

    invoke-direct {v5}, LX/1EA;-><init>()V

    const p0, 0x7f08140c

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2302713
    iput-object p0, v5, LX/1EA;->o:Ljava/lang/String;

    .line 2302714
    move-object v5, v5

    .line 2302715
    const p0, 0x7f08154a

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2302716
    iput-object p0, v5, LX/1EA;->l:Ljava/lang/String;

    .line 2302717
    move-object v5, v5

    .line 2302718
    const p0, 0x7f0814da

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2302719
    iput-object p0, v5, LX/1EA;->m:Ljava/lang/String;

    .line 2302720
    move-object v5, v5

    .line 2302721
    const/4 p0, 0x0

    .line 2302722
    iput-boolean p0, v5, LX/1EA;->q:Z

    .line 2302723
    move-object v5, v5

    .line 2302724
    invoke-virtual {v5}, LX/1EA;->a()LX/1EE;

    move-result-object v5

    move-object v4, v5

    .line 2302725
    goto :goto_0

    .line 2302726
    :cond_2
    const/16 v5, 0x8

    goto :goto_1

    .line 2302727
    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    .line 2302728
    :cond_4
    iget-object v5, v0, LX/Fv7;->c:LX/G2m;

    invoke-virtual {v5}, LX/G2m;->a()Landroid/view/View$OnClickListener;

    move-result-object v5

    goto :goto_3
.end method

.method public static f(LX/Fw6;)V
    .locals 5

    .prologue
    .line 2302729
    const/4 v0, 0x1

    move v0, v0

    .line 2302730
    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Fw6;->d:LX/Fw8;

    iget-object v1, p0, LX/Fw6;->m:LX/BP0;

    iget-object v2, p0, LX/Fw6;->n:LX/BQ1;

    invoke-virtual {v0, v1, v2}, LX/Fw8;->a(LX/5SB;LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2302731
    iget-object v1, p0, LX/Fw6;->f:LX/FwD;

    const/4 v2, 0x0

    .line 2302732
    if-nez v0, :cond_0

    iget-object v3, v1, LX/FwD;->a:LX/0ad;

    sget-short v4, LX/0wf;->W:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 2302733
    if-eqz v0, :cond_2

    .line 2302734
    iget-object v0, p0, LX/Fw6;->s:LX/FrA;

    if-nez v0, :cond_1

    .line 2302735
    new-instance v0, LX/Fw5;

    invoke-direct {v0, p0}, LX/Fw5;-><init>(LX/Fw6;)V

    .line 2302736
    iget-object v1, p0, LX/Fw6;->b:LX/FrB;

    iget-object v2, p0, LX/Fw6;->o:LX/FrH;

    sget-object v3, LX/Fr2;->PROTILE_STYLE:LX/Fr2;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, LX/FrB;->a(LX/FrH;LX/Fr2;Landroid/view/View$OnClickListener;Ljava/lang/String;)LX/FrA;

    move-result-object v0

    iput-object v0, p0, LX/Fw6;->s:LX/FrA;

    .line 2302737
    :cond_1
    iget-object v0, p0, LX/Fw6;->i:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    iget-object v1, p0, LX/Fw6;->s:LX/FrA;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->setAdapter(LX/FrA;)V

    .line 2302738
    :goto_1
    return-void

    .line 2302739
    :cond_2
    iget-object v0, p0, LX/Fw6;->i:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->setVisibility(I)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2302740
    iget-boolean v0, p0, LX/Fw6;->r:Z

    return v0
.end method

.method public final a(LX/BP0;LX/BQ1;LX/FrH;)Z
    .locals 5

    .prologue
    .line 2302741
    const-string v0, "UserTimelineHeaderView.bindModel"

    const v1, 0x6685bd21

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2302742
    :try_start_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2302743
    if-nez p2, :cond_1

    .line 2302744
    :cond_0
    :goto_0
    move v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2302745
    const v1, 0x5f341333

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :catchall_0
    move-exception v0

    const v1, 0x7c96f7a0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2302746
    :cond_1
    iget-object v0, p0, LX/Fw6;->n:LX/BQ1;

    if-eq v0, p2, :cond_8

    move v0, v1

    .line 2302747
    :goto_1
    iput-object p1, p0, LX/Fw6;->m:LX/BP0;

    .line 2302748
    iput-object p2, p0, LX/Fw6;->n:LX/BQ1;

    .line 2302749
    iput-object p3, p0, LX/Fw6;->o:LX/FrH;

    .line 2302750
    invoke-virtual {p0}, LX/Fw6;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    .line 2302751
    if-nez v0, :cond_2

    iget v0, p0, LX/Fw6;->h:I

    if-ne v0, v3, :cond_2

    iget v0, p0, LX/Fw6;->p:I

    iget-object v4, p0, LX/Fw6;->n:LX/BQ1;

    .line 2302752
    iget p1, v4, LX/BPy;->c:I

    move v4, p1

    .line 2302753
    if-ge v0, v4, :cond_9

    .line 2302754
    :cond_2
    iput v3, p0, LX/Fw6;->h:I

    .line 2302755
    iget-object v0, p0, LX/Fw6;->n:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2302756
    iget-object v0, p0, LX/Fw6;->g:LX/BQB;

    .line 2302757
    iget-object v3, v0, LX/BQB;->c:LX/BQD;

    const-string v4, "TimelineBindHeader"

    invoke-virtual {v3, v4}, LX/BQD;->a(Ljava/lang/String;)V

    .line 2302758
    :cond_3
    invoke-static {p0}, LX/Fw6;->f(LX/Fw6;)V

    .line 2302759
    const/4 v0, 0x0

    .line 2302760
    iget-object v3, p0, LX/Fw6;->n:LX/BQ1;

    invoke-virtual {v3}, LX/BPy;->j()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, LX/Fw6;->m:LX/BP0;

    iget-object v4, p0, LX/Fw6;->n:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->X()Z

    move-result v4

    invoke-static {v3, v4}, LX/BQ3;->a(LX/5SB;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, LX/Fw6;->c:LX/0ad;

    sget-short v4, LX/0wf;->B:S

    invoke-interface {v3, v4, v0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v0, 0x1

    :cond_4
    move v0, v0

    .line 2302761
    if-eqz v0, :cond_5

    .line 2302762
    invoke-static {p0}, LX/Fw6;->e(LX/Fw6;)V

    .line 2302763
    :cond_5
    iget-object v0, p0, LX/Fw6;->l:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2302764
    :cond_6
    :goto_2
    iget-object v0, p0, LX/Fw6;->n:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2302765
    iget-object v0, p0, LX/Fw6;->g:LX/BQB;

    .line 2302766
    iget-object v3, v0, LX/BQB;->c:LX/BQD;

    const-string v4, "TimelineBindHeader"

    invoke-virtual {v3, v4}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2302767
    :cond_7
    iget-object v0, p0, LX/Fw6;->n:LX/BQ1;

    .line 2302768
    iget v3, v0, LX/BPy;->c:I

    move v0, v3

    .line 2302769
    iput v0, p0, LX/Fw6;->p:I

    .line 2302770
    iget-object v0, p0, LX/Fw6;->n:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_0

    move v2, v1

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 2302771
    goto/16 :goto_1

    .line 2302772
    :cond_9
    iget v0, p0, LX/Fw6;->q:I

    iget-object v1, p0, LX/Fw6;->o:LX/FrH;

    .line 2302773
    iget v3, v1, LX/BPB;->a:I

    move v1, v3

    .line 2302774
    if-ge v0, v1, :cond_0

    .line 2302775
    invoke-static {p0}, LX/Fw6;->f(LX/Fw6;)V

    .line 2302776
    iget-object v0, p0, LX/Fw6;->o:LX/FrH;

    .line 2302777
    iget v1, v0, LX/BPB;->a:I

    move v0, v1

    .line 2302778
    iput v0, p0, LX/Fw6;->q:I

    goto/16 :goto_0

    .line 2302779
    :cond_a
    iget-object v0, p0, LX/Fw6;->n:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v3, :cond_b

    .line 2302780
    iget-object v0, p0, LX/Fw6;->l:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    iget-object v3, p0, LX/Fw6;->n:LX/BQ1;

    iget-object v4, p0, LX/Fw6;->m:LX/BP0;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->a(LX/BPy;LX/5SB;)Z

    goto :goto_2

    .line 2302781
    :cond_b
    iget-object v0, p0, LX/Fw6;->l:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2302782
    iget-object v0, p0, LX/Fw6;->l:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2302783
    iget-object v0, p0, LX/Fw6;->i:Lcom/facebook/timeline/header/TimelineContextItemsSection;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/TimelineContextItemsSection;->removeAllViews()V

    .line 2302784
    iput-object v1, p0, LX/Fw6;->o:LX/FrH;

    .line 2302785
    iput-object v1, p0, LX/Fw6;->n:LX/BQ1;

    .line 2302786
    iput-object v1, p0, LX/Fw6;->m:LX/BP0;

    .line 2302787
    return-void
.end method

.method public getHeaderData()LX/BQ1;
    .locals 1

    .prologue
    .line 2302788
    iget-object v0, p0, LX/Fw6;->n:LX/BQ1;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x765393fb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2302789
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2302790
    const/4 v1, 0x1

    .line 2302791
    iput-boolean v1, p0, LX/Fw6;->r:Z

    .line 2302792
    const/16 v1, 0x2d

    const v2, 0x420e690f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4c5c989

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2302793
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2302794
    const/4 v1, 0x0

    .line 2302795
    iput-boolean v1, p0, LX/Fw6;->r:Z

    .line 2302796
    const/16 v1, 0x2d

    const v2, -0x496ed106

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
