.class public LX/HCR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2438645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2438646
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2438647
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2438648
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2438649
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2438650
    const-string v5, "com.facebook.katana.profile.id"

    invoke-virtual {v4, v5, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2438651
    new-instance v5, Lcom/facebook/pages/common/editpage/EditPageFragment;

    invoke-direct {v5}, Lcom/facebook/pages/common/editpage/EditPageFragment;-><init>()V

    .line 2438652
    invoke-virtual {v5, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2438653
    move-object v0, v5

    .line 2438654
    return-object v0

    .line 2438655
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
