.class public LX/GlB;
.super LX/Gkp;
.source ""


# instance fields
.field public final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/Gkq;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/Gkq;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/Gkq;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/Gkm;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2390003
    invoke-direct {p0, p1}, LX/Gkp;-><init>(Landroid/content/res/Resources;)V

    .line 2390004
    new-instance v0, LX/Gl7;

    invoke-direct {v0, p0}, LX/Gl7;-><init>(LX/GlB;)V

    iput-object v0, p0, LX/GlB;->c:Ljava/util/Comparator;

    .line 2390005
    new-instance v0, LX/Gl8;

    invoke-direct {v0, p0}, LX/Gl8;-><init>(LX/GlB;)V

    iput-object v0, p0, LX/GlB;->d:Ljava/util/Comparator;

    .line 2390006
    new-instance v0, LX/Gl9;

    invoke-direct {v0, p0}, LX/Gl9;-><init>(LX/GlB;)V

    iput-object v0, p0, LX/GlB;->e:Ljava/util/Comparator;

    .line 2390007
    sget-object v0, LX/Gkm;->RECENTLY_VISITED:LX/Gkm;

    iput-object v0, p0, LX/GlB;->f:LX/Gkm;

    .line 2390008
    return-void
.end method

.method public static b(JJ)I
    .locals 2

    .prologue
    .line 2390009
    cmp-long v0, p0, p2

    if-nez v0, :cond_0

    .line 2390010
    const/4 v0, 0x0

    .line 2390011
    :goto_0
    return v0

    .line 2390012
    :cond_0
    cmp-long v0, p0, p2

    if-lez v0, :cond_1

    .line 2390013
    const/4 v0, -0x1

    goto :goto_0

    .line 2390014
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2390015
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2390016
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2390017
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkq;

    .line 2390018
    iget-boolean v2, v0, LX/Gkq;->j:Z

    move v2, v2

    .line 2390019
    if-eqz v2, :cond_1

    .line 2390020
    iget-boolean v2, v0, LX/Gkq;->h:Z

    move v2, v2

    .line 2390021
    if-nez v2, :cond_1

    .line 2390022
    iget-boolean v2, v0, LX/Gkq;->g:Z

    move v0, v2

    .line 2390023
    if-eqz v0, :cond_0

    .line 2390024
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2390025
    :cond_2
    sget-object v0, LX/GlA;->a:[I

    iget-object v1, p0, LX/GlB;->f:LX/Gkm;

    invoke-virtual {v1}, LX/Gkm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2390026
    iget-object v0, p0, LX/GlB;->d:Ljava/util/Comparator;

    :goto_1
    move-object v0, v0

    .line 2390027
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2390028
    return-object p1

    .line 2390029
    :pswitch_0
    iget-object v0, p0, LX/GlB;->c:Ljava/util/Comparator;

    goto :goto_1

    .line 2390030
    :pswitch_1
    iget-object v0, p0, LX/GlB;->e:Ljava/util/Comparator;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(I)LX/GlC;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2390031
    invoke-super {p0, p1}, LX/Gkp;->b(I)LX/GlC;

    move-result-object v0

    .line 2390032
    const-string v1, "order"

    .line 2390033
    sget-object v2, LX/GlA;->a:[I

    iget-object p1, p0, LX/GlB;->f:LX/Gkm;

    invoke-virtual {p1}, LX/Gkm;->ordinal()I

    move-result p1

    aget v2, v2, p1

    packed-switch v2, :pswitch_data_0

    .line 2390034
    const-string v2, "viewer_visitation"

    :goto_0
    move-object v2, v2

    .line 2390035
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2390036
    const-string v1, "has_hidden"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2390037
    const-string v1, "has_favorited"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2390038
    const-string v1, "recently_added"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2390039
    return-object v0

    .line 2390040
    :pswitch_0
    const-string v2, "name"

    goto :goto_0

    .line 2390041
    :pswitch_1
    const-string v2, "last_activity_time"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final synthetic c()Ljava/lang/Enum;
    .locals 1

    .prologue
    .line 2390042
    invoke-virtual {p0}, LX/Gkp;->i()LX/Gkn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/Gkn;
    .locals 1

    .prologue
    .line 2390043
    sget-object v0, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    return-object v0
.end method
