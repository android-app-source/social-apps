.class public LX/GzL;
.super Landroid/preference/PreferenceCategory;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2411792
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2411793
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 3

    .prologue
    .line 2411794
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2411795
    const-string v0, "Loom"

    invoke-virtual {p0, v0}, LX/GzL;->setTitle(Ljava/lang/CharSequence;)V

    .line 2411796
    new-instance v0, LX/4ok;

    invoke-virtual {p0}, LX/GzL;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2411797
    sget-object v1, LX/09i;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2411798
    const-string v1, "Enable manual tracing"

    invoke-virtual {v0, v1}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2411799
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 2411800
    const-string v1, "Tap to enable manual controls (see notification)"

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOff(Ljava/lang/CharSequence;)V

    .line 2411801
    const-string v1, "Tap to disable manual controls"

    invoke-virtual {v0, v1}, LX/4ok;->setSummaryOn(Ljava/lang/CharSequence;)V

    .line 2411802
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2411803
    invoke-virtual {p0, v0}, LX/GzL;->addPreference(Landroid/preference/Preference;)Z

    .line 2411804
    return-void
.end method
