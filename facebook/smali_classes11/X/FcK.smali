.class public LX/FcK;
.super LX/FcJ;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FcJ",
        "<",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        "LX/Fd9;",
        "LX/FdJ;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "LX/Fd9;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "LX/FdJ;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2262297
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "set_search_price_category"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/FcK;->a:LX/0P1;

    .line 2262298
    new-instance v0, LX/FcF;

    invoke-direct {v0}, LX/FcF;-><init>()V

    sput-object v0, LX/FcK;->g:LX/FcE;

    .line 2262299
    new-instance v0, LX/FcG;

    invoke-direct {v0}, LX/FcG;-><init>()V

    sput-object v0, LX/FcK;->b:LX/FcE;

    .line 2262300
    new-instance v0, LX/FcH;

    invoke-direct {v0}, LX/FcH;-><init>()V

    sput-object v0, LX/FcK;->h:LX/FcE;

    return-void
.end method

.method public constructor <init>(LX/0wM;LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262301
    invoke-direct {p0, p1, p2, p3}, LX/FcJ;-><init>(LX/0wM;LX/0lC;LX/03V;)V

    .line 2262302
    return-void
.end method

.method public static a(LX/0QB;)LX/FcK;
    .locals 6

    .prologue
    .line 2262308
    const-class v1, LX/FcK;

    monitor-enter v1

    .line 2262309
    :try_start_0
    sget-object v0, LX/FcK;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2262310
    sput-object v2, LX/FcK;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2262311
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262312
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2262313
    new-instance p0, LX/FcK;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/FcK;-><init>(LX/0wM;LX/0lC;LX/03V;)V

    .line 2262314
    move-object v0, p0

    .line 2262315
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2262316
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FcK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262317
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2262318
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/5uu;LX/Fd9;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 2

    .prologue
    .line 2262303
    sget-object v0, LX/FcK;->a:LX/0P1;

    invoke-interface {p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/FcK;->a:LX/0P1;

    invoke-interface {p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2262304
    :goto_0
    invoke-virtual {p2, v0}, LX/Fd9;->setButtonsPerRow(I)V

    .line 2262305
    invoke-super/range {p0 .. p5}, LX/FcJ;->a(LX/5uu;LX/Fd8;LX/CyH;LX/FdU;LX/FdQ;)V

    .line 2262306
    return-void

    .line 2262307
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262295
    sget-object v0, LX/FcK;->g:LX/FcE;

    return-object v0
.end method

.method public final bridge synthetic a(LX/5uu;LX/Fd8;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 6

    .prologue
    .line 2262296
    move-object v2, p2

    check-cast v2, LX/Fd9;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/FcK;->a(LX/5uu;LX/Fd9;LX/CyH;LX/FdU;LX/FdQ;)V

    return-void
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V
    .locals 4

    .prologue
    .line 2262275
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2262276
    invoke-interface {p1}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262277
    invoke-interface {p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v2

    .line 2262278
    if-nez v2, :cond_0

    .line 2262279
    :goto_0
    return-void

    .line 2262280
    :cond_0
    sget-object v0, LX/FcJ;->c:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2262281
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unimplemented filter "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2262282
    :cond_1
    invoke-virtual {p0}, LX/FcJ;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, -0xa76f01

    move v1, v0

    .line 2262283
    :goto_1
    iget-object v3, p0, LX/FcJ;->d:LX/0wM;

    sget-object v0, LX/FcJ;->c:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2262284
    :cond_2
    const v0, -0x6e685d

    move v1, v0

    goto :goto_1
.end method

.method public final bridge synthetic a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 6

    .prologue
    .line 2262285
    move-object v2, p2

    check-cast v2, LX/Fd9;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/FcK;->a(LX/5uu;LX/Fd9;LX/CyH;LX/FdU;LX/FdQ;)V

    return-void
.end method

.method public final a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;Landroid/view/View;LX/CyH;Z)V
    .locals 1

    .prologue
    .line 2262286
    check-cast p2, LX/FdJ;

    .line 2262287
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    .line 2262288
    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p2, v0}, LX/FdJ;->setText(Ljava/lang/CharSequence;)V

    .line 2262289
    invoke-virtual {p2, p4}, LX/FdJ;->setChecked(Z)V

    .line 2262290
    return-void

    .line 2262291
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "LX/Fd9;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262292
    sget-object v0, LX/FcK;->b:LX/FcE;

    return-object v0
.end method

.method public final c()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "LX/FdJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262293
    sget-object v0, LX/FcK;->h:LX/FcE;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2262294
    const/16 v0, 0x14

    return v0
.end method
