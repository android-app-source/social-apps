.class public LX/FiK;
.super LX/Fi8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/2Sf;

.field public final c:LX/FgS;

.field private final d:Z

.field private final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/FiJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/2Sf;LX/FgS;LX/0ad;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2274957
    invoke-direct {p0}, LX/Fi8;-><init>()V

    .line 2274958
    iput-object p1, p0, LX/FiK;->a:Landroid/content/res/Resources;

    .line 2274959
    iput-object p2, p0, LX/FiK;->b:LX/2Sf;

    .line 2274960
    iput-object p3, p0, LX/FiK;->c:LX/FgS;

    .line 2274961
    sget-short v0, LX/100;->cf:S

    invoke-interface {p4, v0, v9}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/FiK;->d:Z

    .line 2274962
    iget-object v0, p0, LX/FiK;->a:Landroid/content/res/Resources;

    const v1, 0x7f08232f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/FiJ;

    sget-object v2, LX/CwF;->keyword:LX/CwF;

    iget-object v3, p0, LX/FiK;->a:Landroid/content/res/Resources;

    const v4, 0x7f082332

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/FiJ;-><init>(LX/CwF;Ljava/lang/String;)V

    iget-object v2, p0, LX/FiK;->a:Landroid/content/res/Resources;

    const v3, 0x7f08232e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/FiJ;

    sget-object v4, LX/CwF;->photos:LX/CwF;

    iget-object v5, p0, LX/FiK;->a:Landroid/content/res/Resources;

    const v6, 0x7f082331

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/FiJ;-><init>(LX/CwF;Ljava/lang/String;)V

    iget-object v4, p0, LX/FiK;->a:Landroid/content/res/Resources;

    const v5, 0x7f082330

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/FiJ;

    sget-object v6, LX/CwF;->videos:LX/CwF;

    iget-object v7, p0, LX/FiK;->a:Landroid/content/res/Resources;

    const v8, 0x7f082333

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, LX/FiJ;-><init>(LX/CwF;Ljava/lang/String;)V

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/FiK;->e:LX/0P1;

    .line 2274963
    return-void
.end method

.method public static a(LX/0QB;)LX/FiK;
    .locals 7

    .prologue
    .line 2274964
    const-class v1, LX/FiK;

    monitor-enter v1

    .line 2274965
    :try_start_0
    sget-object v0, LX/FiK;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2274966
    sput-object v2, LX/FiK;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2274967
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274968
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2274969
    new-instance p0, LX/FiK;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2Sf;->a(LX/0QB;)LX/2Sf;

    move-result-object v4

    check-cast v4, LX/2Sf;

    invoke-static {v0}, LX/FgS;->a(LX/0QB;)LX/FgS;

    move-result-object v5

    check-cast v5, LX/FgS;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FiK;-><init>(Landroid/content/res/Resources;LX/2Sf;LX/FgS;LX/0ad;)V

    .line 2274970
    move-object v0, p0

    .line 2274971
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2274972
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FiK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2274973
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2274974
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 2

    .prologue
    .line 2274975
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2274976
    iput-object p1, v0, LX/CwH;->b:Ljava/lang/String;

    .line 2274977
    move-object v0, v0

    .line 2274978
    iput-object p1, v0, LX/CwH;->d:Ljava/lang/String;

    .line 2274979
    move-object v0, v0

    .line 2274980
    const-string v1, "content"

    .line 2274981
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2274982
    move-object v0, v0

    .line 2274983
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2274984
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2274985
    move-object v0, v0

    .line 2274986
    iput-object p0, v0, LX/CwH;->l:LX/CwI;

    .line 2274987
    move-object v0, v0

    .line 2274988
    invoke-static {p1}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2274989
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2274990
    move-object v0, v0

    .line 2274991
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2274992
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 2274993
    sget-object v1, LX/103;->URL:LX/103;

    if-ne v0, v1, :cond_0

    .line 2274994
    sget-object v0, LX/CwI;->ECHO:LX/CwI;

    .line 2274995
    iget-object v1, p1, LX/7B6;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2274996
    invoke-static {v0, v1}, LX/FiK;->a(LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2274997
    :goto_0
    return-object v0

    .line 2274998
    :cond_0
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2274999
    iget-object v0, p0, LX/FiK;->e:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 2275000
    iget-object v0, p1, LX/7B6;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2275001
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2275002
    iget-object v1, p0, LX/FiK;->e:LX/0P1;

    invoke-virtual {v1, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FiJ;

    .line 2275003
    iget-object v2, v1, LX/FiJ;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2275004
    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, LX/CwI;->INJECTED_SUGGESTION:LX/CwI;

    iget-object v8, p0, LX/FiK;->e:LX/0P1;

    invoke-virtual {v8, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FiJ;

    .line 2275005
    iget-object v8, v5, LX/FiJ;->a:LX/CwF;

    move-object v5, v8

    .line 2275006
    invoke-static {v0}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2275007
    new-instance v9, LX/CwH;

    invoke-direct {v9}, LX/CwH;-><init>()V

    .line 2275008
    iput-object v0, v9, LX/CwH;->b:Ljava/lang/String;

    .line 2275009
    move-object v9, v9

    .line 2275010
    iput-object v0, v9, LX/CwH;->d:Ljava/lang/String;

    .line 2275011
    move-object v9, v9

    .line 2275012
    iput-object v1, v9, LX/CwH;->u:Ljava/lang/String;

    .line 2275013
    move-object v9, v9

    .line 2275014
    iput-object v2, v9, LX/CwH;->t:Ljava/lang/String;

    .line 2275015
    move-object v9, v9

    .line 2275016
    iput-boolean v3, v9, LX/CwH;->v:Z

    .line 2275017
    move-object v9, v9

    .line 2275018
    iput-object v8, v9, LX/CwH;->c:Ljava/lang/String;

    .line 2275019
    move-object v8, v9

    .line 2275020
    const-string v9, "content"

    .line 2275021
    iput-object v9, v8, LX/CwH;->e:Ljava/lang/String;

    .line 2275022
    move-object v8, v8

    .line 2275023
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 2275024
    iput-object v9, v8, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2275025
    move-object v8, v8

    .line 2275026
    iput-object v5, v8, LX/CwH;->g:LX/CwF;

    .line 2275027
    move-object v8, v8

    .line 2275028
    iput-object v4, v8, LX/CwH;->l:LX/CwI;

    .line 2275029
    move-object v8, v8

    .line 2275030
    invoke-virtual {v8}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v8

    move-object v0, v8

    .line 2275031
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2275032
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2275033
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2275034
    iget-object v0, p2, LX/7Hc;->b:LX/0Px;

    move-object v4, v0

    .line 2275035
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2275036
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v6

    .line 2275037
    sget-object v1, LX/Cwb;->ENTITY:LX/Cwb;

    invoke-virtual {v6, v1}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->E()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2275038
    sget-object v1, LX/Cwb;->KEYWORD:LX/Cwb;

    invoke-virtual {v6, v1}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FiK;->e:LX/0P1;

    invoke-virtual {v1}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v7

    move-object v1, v0

    check-cast v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2275039
    :cond_0
    invoke-interface {v3, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2275040
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-interface {v3, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275041
    sget-object v1, LX/Cwb;->KEYWORD:LX/Cwb;

    invoke-virtual {v6, v1}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2275042
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pz;

    invoke-direct {p0, p1}, LX/FiK;->b(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2275043
    :cond_1
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pz;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2275044
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2275045
    :cond_3
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2275046
    sget-object v0, LX/Cwb;->KEYWORD:LX/Cwb;

    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-direct {p0, p1}, LX/FiK;->b(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275047
    :cond_4
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2275048
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2275049
    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2275050
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwb;

    .line 2275051
    new-instance v4, LX/Cwa;

    invoke-direct {v4}, LX/Cwa;-><init>()V

    .line 2275052
    iput-object v0, v4, LX/Cwa;->a:LX/Cwb;

    .line 2275053
    move-object v4, v4

    .line 2275054
    sget-object v5, LX/Cwb;->ENTITY:LX/Cwb;

    invoke-virtual {v0, v5}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2275055
    const/4 v5, 0x0

    .line 2275056
    :goto_2
    move-object v5, v5

    .line 2275057
    iput-object v5, v4, LX/Cwa;->c:Ljava/lang/String;

    .line 2275058
    move-object v4, v4

    .line 2275059
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2275060
    iput-object v0, v4, LX/Cwa;->b:LX/0Px;

    .line 2275061
    move-object v0, v4

    .line 2275062
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2275063
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2275064
    new-instance v0, LX/Cwa;

    invoke-direct {v0}, LX/Cwa;-><init>()V

    sget-object v4, LX/Cwb;->NO_GROUP:LX/Cwb;

    .line 2275065
    iput-object v4, v0, LX/Cwa;->a:LX/Cwb;

    .line 2275066
    move-object v0, v0

    .line 2275067
    sget-object v4, Lcom/facebook/search/model/DividerTypeaheadUnit;->a:Lcom/facebook/search/model/DividerTypeaheadUnit;

    move-object v4, v4

    .line 2275068
    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 2275069
    iput-object v4, v0, LX/Cwa;->b:LX/0Px;

    .line 2275070
    move-object v0, v0

    .line 2275071
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2275072
    :cond_6
    iget-boolean v0, p0, LX/FiK;->d:Z

    if-eqz v0, :cond_7

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2Sf;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_7
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/2Sf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    goto :goto_3

    :cond_8
    iget-object v5, p0, LX/FiK;->a:Landroid/content/res/Resources;

    const v6, 0x7f082325

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 2

    .prologue
    .line 2275073
    sget-object v0, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    .line 2275074
    iget-object v1, p0, LX/FiK;->c:LX/FgS;

    .line 2275075
    iget-object p1, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, p1

    .line 2275076
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FiK;->c:LX/FgS;

    .line 2275077
    iget-object p1, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, p1

    .line 2275078
    iget-object p1, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, p1

    .line 2275079
    if-nez v1, :cond_1

    .line 2275080
    :cond_0
    const-string v1, ""

    .line 2275081
    :goto_0
    move-object v1, v1

    .line 2275082
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/FiK;->a(LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v1, p0, LX/FiK;->c:LX/FgS;

    .line 2275083
    iget-object p1, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, p1

    .line 2275084
    iget-object p1, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, p1

    .line 2275085
    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final b()LX/CwU;
    .locals 1

    .prologue
    .line 2275086
    sget-object v0, LX/CwU;->SINGLE_STATE:LX/CwU;

    return-object v0
.end method
