.class public final LX/Gu3;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V
    .locals 0

    .prologue
    .line 2403073
    iput-object p1, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 2403074
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e(I)V

    .line 2403075
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-wide v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 2403076
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    iget-object v1, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-object v1, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-wide v4, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 2403077
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403078
    iput-wide v6, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    .line 2403079
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2403080
    const-wide/16 v6, 0x0

    .line 2403081
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e(I)V

    .line 2403082
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-wide v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 2403083
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    iget-object v1, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-object v1, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-wide v4, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 2403084
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403085
    iput-wide v6, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->F:J

    .line 2403086
    :cond_0
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    if-eqz v0, :cond_1

    .line 2403087
    iget-object v0, p0, LX/Gu3;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->c()V

    .line 2403088
    :cond_1
    return-void
.end method
