.class public final LX/Fui;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fuj;


# direct methods
.method public constructor <init>(LX/Fuj;)V
    .locals 0

    .prologue
    .line 2300352
    iput-object p1, p0, LX/Fui;->a:LX/Fuj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x5e8f6463

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2300340
    iget-object v1, p0, LX/Fui;->a:LX/Fuj;

    iget-object v1, v1, LX/Fuj;->i:LX/Fwh;

    invoke-virtual {v1}, LX/Fwg;->e()V

    .line 2300341
    iget-object v1, p0, LX/Fui;->a:LX/Fuj;

    .line 2300342
    iget-object v3, v1, LX/Fuj;->d:LX/BQ1;

    .line 2300343
    iget-object v4, v3, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v3, v4

    .line 2300344
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->o()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 2300345
    iget-object v3, v1, LX/Fuj;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BQ9;

    iget-object v5, v1, LX/Fuj;->c:LX/BP0;

    .line 2300346
    iget-wide v7, v5, LX/5SB;->b:J

    move-wide v5, v7

    .line 2300347
    const/4 v11, 0x0

    sget-object v12, LX/9lQ;->SELF:LX/9lQ;

    const-string v13, "bio_add_prompt_suggested_click"

    move-object v8, v3

    move-wide v9, v5

    invoke-static/range {v8 .. v13}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 2300348
    if-eqz v7, :cond_0

    .line 2300349
    iget-object v8, v3, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300350
    :cond_0
    const/4 v3, 0x1

    invoke-static {v1, v4, v3}, LX/Fuj;->a(LX/Fuj;Ljava/lang/String;Z)V

    .line 2300351
    const v1, -0x2acedfe3

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
