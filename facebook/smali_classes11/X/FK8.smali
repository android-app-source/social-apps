.class public LX/FK8;
.super LX/4ok;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/push/prefs/IsMobileOnlineAvailabilityEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224653
    invoke-direct {p0, p1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2224654
    sget-object v0, LX/1qz;->a:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FK8;->setKey(Ljava/lang/String;)V

    .line 2224655
    const v0, 0x7f08040a

    invoke-virtual {p0, v0}, LX/FK8;->setTitle(I)V

    .line 2224656
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FK8;->setDefaultValue(Ljava/lang/Object;)V

    .line 2224657
    return-void
.end method

.method public static a(LX/0QB;)LX/FK8;
    .locals 3

    .prologue
    .line 2224658
    new-instance v1, LX/FK8;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v2, 0x155d

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/FK8;-><init>(Landroid/content/Context;LX/0Or;)V

    .line 2224659
    move-object v0, v1

    .line 2224660
    return-object v0
.end method
