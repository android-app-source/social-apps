.class public interface abstract LX/Fgb;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a(LX/7Hi;)LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)LX/Fid;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a(I)V
.end method

.method public abstract a(LX/0zw;Landroid/view/View;Z)V
    .param p1    # LX/0zw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;",
            "Landroid/view/View;",
            "Z)V"
        }
    .end annotation
.end method

.method public abstract a(LX/7HP;)V
.end method

.method public abstract a(LX/Cvp;)V
.end method

.method public abstract a(LX/Fh9;)V
.end method

.method public abstract a(Landroid/content/Context;LX/Fh8;)V
.end method

.method public abstract a(Landroid/view/View;LX/FhA;Z)V
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
.end method

.method public abstract a(Lcom/facebook/search/api/GraphSearchQuery;)V
.end method

.method public abstract a(Ljava/lang/String;LX/0Px;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;LX/7BE;Z)V
.end method

.method public abstract c()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d()LX/1Qq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract e()LX/7HZ;
.end method

.method public abstract f()Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract g()LX/0g8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract h()LX/2SP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract i()Lcom/facebook/search/api/GraphSearchQuery;
.end method

.method public abstract j()LX/FgU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()LX/Fi7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()LX/Fid;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract m()V
.end method

.method public abstract n()V
.end method

.method public abstract o()V
.end method

.method public abstract p()V
.end method

.method public abstract q()V
.end method
