.class public final enum LX/Gwv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gwv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gwv;

.field public static final enum DESERIALIZATION_ERROR:LX/Gwv;

.field public static final enum NETWORK_ERROR:LX/Gwv;

.field public static final enum NO_SESSION_ERROR:LX/Gwv;

.field public static final enum SERIALIZATION_ERROR:LX/Gwv;

.field public static final enum UNEXPECTED_REDIRECT:LX/Gwv;

.field public static final enum UNKNOWN_ERROR:LX/Gwv;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2407556
    new-instance v0, LX/Gwv;

    const-string v1, "DESERIALIZATION_ERROR"

    invoke-direct {v0, v1, v3}, LX/Gwv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gwv;->DESERIALIZATION_ERROR:LX/Gwv;

    .line 2407557
    new-instance v0, LX/Gwv;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v4}, LX/Gwv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gwv;->NETWORK_ERROR:LX/Gwv;

    .line 2407558
    new-instance v0, LX/Gwv;

    const-string v1, "NO_SESSION_ERROR"

    invoke-direct {v0, v1, v5}, LX/Gwv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gwv;->NO_SESSION_ERROR:LX/Gwv;

    .line 2407559
    new-instance v0, LX/Gwv;

    const-string v1, "SERIALIZATION_ERROR"

    invoke-direct {v0, v1, v6}, LX/Gwv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gwv;->SERIALIZATION_ERROR:LX/Gwv;

    .line 2407560
    new-instance v0, LX/Gwv;

    const-string v1, "UNEXPECTED_REDIRECT"

    invoke-direct {v0, v1, v7}, LX/Gwv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gwv;->UNEXPECTED_REDIRECT:LX/Gwv;

    .line 2407561
    new-instance v0, LX/Gwv;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Gwv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gwv;->UNKNOWN_ERROR:LX/Gwv;

    .line 2407562
    const/4 v0, 0x6

    new-array v0, v0, [LX/Gwv;

    sget-object v1, LX/Gwv;->DESERIALIZATION_ERROR:LX/Gwv;

    aput-object v1, v0, v3

    sget-object v1, LX/Gwv;->NETWORK_ERROR:LX/Gwv;

    aput-object v1, v0, v4

    sget-object v1, LX/Gwv;->NO_SESSION_ERROR:LX/Gwv;

    aput-object v1, v0, v5

    sget-object v1, LX/Gwv;->SERIALIZATION_ERROR:LX/Gwv;

    aput-object v1, v0, v6

    sget-object v1, LX/Gwv;->UNEXPECTED_REDIRECT:LX/Gwv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Gwv;->UNKNOWN_ERROR:LX/Gwv;

    aput-object v2, v0, v1

    sput-object v0, LX/Gwv;->$VALUES:[LX/Gwv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2407555
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gwv;
    .locals 1

    .prologue
    .line 2407553
    const-class v0, LX/Gwv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gwv;

    return-object v0
.end method

.method public static values()[LX/Gwv;
    .locals 1

    .prologue
    .line 2407554
    sget-object v0, LX/Gwv;->$VALUES:[LX/Gwv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gwv;

    return-object v0
.end method
