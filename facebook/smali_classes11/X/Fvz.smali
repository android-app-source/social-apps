.class public LX/Fvz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Fw8;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Fw8;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fw8;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2302435
    iput-object p1, p0, LX/Fvz;->a:LX/Fw8;

    .line 2302436
    iput-object p2, p0, LX/Fvz;->b:LX/0Or;

    .line 2302437
    return-void
.end method

.method public static a(LX/0QB;)LX/Fvz;
    .locals 1

    .prologue
    .line 2302438
    invoke-static {p0}, LX/Fvz;->b(LX/0QB;)LX/Fvz;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/Fvz;
    .locals 3

    .prologue
    .line 2302439
    new-instance v1, LX/Fvz;

    invoke-static {p0}, LX/Fw8;->a(LX/0QB;)LX/Fw8;

    move-result-object v0

    check-cast v0, LX/Fw8;

    const/16 v2, 0x369b

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/Fvz;-><init>(LX/Fw8;LX/0Or;)V

    .line 2302440
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;LX/FvZ;LX/BP0;)V
    .locals 11

    .prologue
    .line 2302441
    const v0, 0x7f0a00d5

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->setBackgroundResource(I)V

    .line 2302442
    new-instance v0, LX/Fvy;

    invoke-direct {v0, p0, p2, p3}, LX/Fvy;-><init>(LX/Fvz;LX/FvZ;LX/BP0;)V

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;->setOnDismissListener(Landroid/view/View$OnClickListener;)V

    .line 2302443
    iget-object v0, p0, LX/Fvz;->a:LX/Fw8;

    .line 2302444
    iget-object v1, v0, LX/Fw8;->a:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    const-string v2, "3621"

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2302445
    iget-object v0, p0, LX/Fvz;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    .line 2302446
    iget-wide v4, p3, LX/5SB;->b:J

    move-wide v2, v4

    .line 2302447
    iget-object v1, p3, LX/5SB;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2302448
    sget-object v9, LX/9lQ;->SELF:LX/9lQ;

    const-string v10, "nux_impression"

    move-object v5, v0

    move-wide v6, v2

    move-object v8, v1

    invoke-static/range {v5 .. v10}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2302449
    if-eqz v4, :cond_0

    .line 2302450
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2302451
    :cond_0
    return-void
.end method
