.class public LX/GLp;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public b:LX/GEU;

.field public c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public e:Lcom/facebook/resources/ui/FbButton;

.field public f:LX/2U3;

.field private g:LX/0ad;

.field public h:Z


# direct methods
.method public constructor <init>(LX/GEU;LX/2U3;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2343727
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2343728
    iput-object p1, p0, LX/GLp;->b:LX/GEU;

    .line 2343729
    iput-object p2, p0, LX/GLp;->f:LX/2U3;

    .line 2343730
    iput-object p3, p0, LX/GLp;->g:LX/0ad;

    .line 2343731
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2343732
    invoke-super {p0}, LX/GHg;->a()V

    .line 2343733
    iget-object v0, p0, LX/GLp;->b:LX/GEU;

    invoke-virtual {v0}, LX/GDY;->a()V

    .line 2343734
    iput-object v1, p0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2343735
    iput-object v1, p0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2343736
    iput-object v1, p0, LX/GLp;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2343737
    iput-object v1, p0, LX/GLp;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2343738
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2343739
    check-cast p1, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2343740
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2343741
    iput-object p1, p0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2343742
    const v0, 0x7f0d0499

    invoke-virtual {p1, v0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2343743
    const v0, 0x7f0d049a

    invoke-virtual {p1, v0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/GLp;->e:Lcom/facebook/resources/ui/FbButton;

    .line 2343744
    invoke-virtual {p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2343745
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2343746
    new-instance v2, LX/GLm;

    invoke-direct {v2, p0}, LX/GLm;-><init>(LX/GLp;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wQ;)V

    .line 2343747
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2343748
    new-instance v2, LX/GLn;

    invoke-direct {v2, p0, v0}, LX/GLn;-><init>(LX/GLp;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wQ;)V

    .line 2343749
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2343750
    new-instance v1, LX/GFp;

    invoke-direct {v1}, LX/GFp;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2343751
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2343752
    iput-object p1, p0, LX/GLp;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2343753
    return-void
.end method

.method public final h_(Z)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    .line 2343754
    iget-object v0, p0, LX/GLp;->g:LX/0ad;

    sget-short v1, LX/GDK;->u:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2343755
    if-eqz v0, :cond_0

    .line 2343756
    iget-boolean v0, p0, LX/GHg;->a:Z

    move v0, v0

    .line 2343757
    if-eqz v0, :cond_0

    .line 2343758
    iget-object v0, p0, LX/GLp;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2343759
    iget-object v0, p0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2343760
    if-nez p1, :cond_0

    .line 2343761
    const/4 p1, 0x0

    const/4 v6, 0x0

    .line 2343762
    iget-object v0, p0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2343763
    iget-object v1, p0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2343764
    const v2, 0x7f080ba7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2343765
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2343766
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    const v5, 0x7f0a036d

    invoke-static {v0, v5}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2343767
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v5, 0x21

    invoke-virtual {v3, v4, p1, v2, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2343768
    iget-object v2, p0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2343769
    iget-object v2, p0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2343770
    new-instance v2, LX/0wM;

    invoke-direct {v2, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2343771
    const v1, 0x7f0a036c

    invoke-static {v0, v1}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v1

    .line 2343772
    const v3, 0x7f021a0b

    const v4, 0x7f0a036b

    invoke-static {v0, v4}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v2, v3, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2343773
    iget-object v2, p0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2343774
    iget-object v1, p0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1, v0, v6, v6, v6}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2343775
    iget-object v0, p0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2343776
    :cond_0
    return-void
.end method
