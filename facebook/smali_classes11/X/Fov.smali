.class public final LX/Fov;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Foy;


# direct methods
.method public constructor <init>(LX/Foy;)V
    .locals 0

    .prologue
    .line 2290456
    iput-object p1, p0, LX/Fov;->a:LX/Foy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2290441
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2290442
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2290443
    if-eqz p1, :cond_1

    .line 2290444
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290445
    if-eqz v0, :cond_1

    .line 2290446
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290447
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel;

    .line 2290448
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2290449
    iget-object v1, p0, LX/Fov;->a:LX/Foy;

    iget-object v1, v1, LX/Foy;->e:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel;->a()LX/0Px;

    move-result-object v0

    .line 2290450
    if-eqz v0, :cond_0

    .line 2290451
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    .line 2290452
    iget-object v5, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->i:Ljava/util/ArrayList;

    new-instance p0, LX/Fon;

    sget-object p1, LX/Foo;->CATEGORY:LX/Foo;

    invoke-direct {p0, p1, v2}, LX/Fon;-><init>(LX/Foo;Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;)V

    invoke-virtual {v5, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2290453
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2290454
    :cond_0
    iget-object v2, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2290455
    :cond_1
    return-void
.end method
