.class public LX/FOx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile d:LX/FOx;


# instance fields
.field private b:LX/0i4;

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2236422
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, LX/FOx;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0i4;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0i4;",
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2236432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236433
    iput-object p1, p0, LX/FOx;->b:LX/0i4;

    .line 2236434
    iput-object p2, p0, LX/FOx;->c:LX/0Ot;

    .line 2236435
    return-void
.end method

.method public static a(LX/0QB;)LX/FOx;
    .locals 5

    .prologue
    .line 2236436
    sget-object v0, LX/FOx;->d:LX/FOx;

    if-nez v0, :cond_1

    .line 2236437
    const-class v1, LX/FOx;

    monitor-enter v1

    .line 2236438
    :try_start_0
    sget-object v0, LX/FOx;->d:LX/FOx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2236439
    if-eqz v2, :cond_0

    .line 2236440
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2236441
    new-instance v4, LX/FOx;

    const-class v3, LX/0i4;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0i4;

    const/16 p0, 0xc83

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/FOx;-><init>(LX/0i4;LX/0Ot;)V

    .line 2236442
    move-object v0, v4

    .line 2236443
    sput-object v0, LX/FOx;->d:LX/FOx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2236444
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2236445
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2236446
    :cond_1
    sget-object v0, LX/FOx;->d:LX/FOx;

    return-object v0

    .line 2236447
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2236448
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)LX/FOw;
    .locals 2

    .prologue
    .line 2236423
    iget-object v0, p0, LX/FOx;->b:LX/0i4;

    invoke-virtual {v0, p1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    .line 2236424
    sget-object v1, LX/FOx;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2236425
    sget-object v0, LX/FOw;->LOCATION_PERMISSION_OFF:LX/FOw;

    .line 2236426
    :goto_0
    return-object v0

    .line 2236427
    :cond_0
    iget-object v0, p0, LX/FOx;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v1, :cond_1

    .line 2236428
    sget-object v0, LX/FOw;->DEVICE_LOCATION_OFF:LX/FOw;

    goto :goto_0

    .line 2236429
    :cond_1
    iget-object v0, p0, LX/FOx;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    iget-object v0, v0, LX/1rv;->c:LX/0Rf;

    const-string v1, "network"

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2236430
    sget-object v0, LX/FOw;->DEVICE_NON_OPTIMAL_LOCATION_SETTING:LX/FOw;

    goto :goto_0

    .line 2236431
    :cond_2
    sget-object v0, LX/FOw;->OKAY:LX/FOw;

    goto :goto_0
.end method
