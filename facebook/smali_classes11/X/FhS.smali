.class public final LX/FhS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:LX/7BZ;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/7BZ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/7BZ;


# direct methods
.method public constructor <init>(ILX/7BZ;)V
    .locals 4

    .prologue
    .line 2273027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273028
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    new-array v1, v1, [LX/7BZ;

    const/4 v2, 0x0

    sget-object v3, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/FhS;->c:Ljava/util/ArrayList;

    .line 2273029
    sget-object v0, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    iput-object v0, p0, LX/FhS;->d:LX/7BZ;

    .line 2273030
    iput p1, p0, LX/FhS;->a:I

    .line 2273031
    iput-object p2, p0, LX/FhS;->b:LX/7BZ;

    .line 2273032
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2273033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273034
    new-instance v0, Ljava/util/ArrayList;

    new-array v1, v3, [LX/7BZ;

    sget-object v2, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    aput-object v2, v1, v4

    sget-object v2, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/FhS;->c:Ljava/util/ArrayList;

    .line 2273035
    sget-object v0, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    iput-object v0, p0, LX/FhS;->d:LX/7BZ;

    .line 2273036
    iput p1, p0, LX/FhS;->a:I

    .line 2273037
    if-eqz p2, :cond_1

    sget-object v0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->e:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7BZ;

    :goto_0
    iput-object v0, p0, LX/FhS;->b:LX/7BZ;

    .line 2273038
    iget-object v0, p0, LX/FhS;->b:LX/7BZ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FhS;->c:Ljava/util/ArrayList;

    iget-object v1, p0, LX/FhS;->b:LX/7BZ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2273039
    sget-object v0, LX/FhT;->a:Ljava/lang/Class;

    const-string v1, "ERROR : %s is not an accepted KeywordMode so defaulting to %s"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, LX/FhS;->b:LX/7BZ;

    aput-object v3, v2, v4

    iget-object v3, p0, LX/FhS;->d:LX/7BZ;

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2273040
    iget-object v0, p0, LX/FhS;->d:LX/7BZ;

    iput-object v0, p0, LX/FhS;->b:LX/7BZ;

    .line 2273041
    :cond_0
    return-void

    .line 2273042
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
