.class public final LX/F2C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F2L;


# direct methods
.method public constructor <init>(LX/F2L;)V
    .locals 0

    .prologue
    .line 2192895
    iput-object p1, p0, LX/F2C;->a:LX/F2L;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2192896
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2192897
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2192898
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2192899
    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2192900
    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->s()Ljava/lang/String;

    move-result-object v1

    .line 2192901
    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->m()Ljava/lang/String;

    move-result-object v0

    .line 2192902
    iget-object v2, p0, LX/F2C;->a:LX/F2L;

    iget-object v2, v2, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/F2C;->a:LX/F2L;

    iget-object v2, v2, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/F2C;->a:LX/F2L;

    iget-object v2, v2, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2192903
    :cond_0
    iget-object v2, p0, LX/F2C;->a:LX/F2L;

    .line 2192904
    iget-object p0, v2, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz p0, :cond_1

    .line 2192905
    iget-object p0, v2, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-static {p0}, LX/F3s;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)LX/F3s;

    move-result-object p0

    .line 2192906
    iput-object v1, p0, LX/F3s;->m:Ljava/lang/String;

    .line 2192907
    move-object p0, p0

    .line 2192908
    iput-object v0, p0, LX/F3s;->e:Ljava/lang/String;

    .line 2192909
    move-object p0, p0

    .line 2192910
    invoke-virtual {p0}, LX/F3s;->a()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    move-result-object p0

    iput-object p0, v2, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2192911
    invoke-static {v2}, LX/F2L;->b(LX/F2L;)V

    .line 2192912
    :cond_1
    return-void
.end method
