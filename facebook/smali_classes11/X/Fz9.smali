.class public final LX/Fz9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;)V
    .locals 0

    .prologue
    .line 2307625
    iput-object p1, p0, LX/Fz9;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x276992b1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2307626
    iget-object v1, p0, LX/Fz9;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->h:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v2, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v1, v2}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2307627
    iget-object v2, p0, LX/Fz9;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    iget-object v2, v2, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->d:LX/0Sh;

    new-instance v3, LX/Fz8;

    invoke-direct {v3, p0}, LX/Fz8;-><init>(LX/Fz9;)V

    invoke-virtual {v2, v1, v3}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2307628
    const v1, 0x287c5006

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
