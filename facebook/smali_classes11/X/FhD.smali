.class public LX/FhD;
.super LX/7HO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HO",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2272438
    invoke-direct {p0}, LX/7HO;-><init>()V

    .line 2272439
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/7Hi;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2272440
    check-cast p1, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2272441
    instance-of v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2272442
    iget-boolean p0, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v0, p0

    .line 2272443
    if-eqz v0, :cond_1

    .line 2272444
    check-cast p1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    const/4 v0, 0x0

    .line 2272445
    iget-object p0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object p0, p0

    .line 2272446
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p2

    const p3, 0x285feb

    if-ne p2, p3, :cond_2

    .line 2272447
    iget-object p0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object p0, p0

    .line 2272448
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, p2, :cond_0

    const/4 v0, 0x1

    .line 2272449
    :cond_0
    :goto_0
    move v0, v0

    .line 2272450
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 2272451
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    const p2, 0x25d6af

    if-ne p0, p2, :cond_0

    .line 2272452
    iget-boolean v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->h:Z

    move v0, v0

    .line 2272453
    goto :goto_0
.end method
