.class public LX/FIc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final l:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

.field private final c:LX/FIU;

.field public final d:LX/FIK;

.field private final e:LX/1qI;

.field private final f:LX/2MM;

.field private final g:LX/0Sh;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/2ML;

.field private j:J

.field private final k:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "LX/FIb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2222304
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FIc;->l:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/FIh;Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;LX/FIU;LX/FIK;LX/1qI;LX/2MM;LX/0Sh;LX/0Or;LX/2ML;)V
    .locals 4
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/media/upload/udp/ChunkedUDPUploadGK;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/FIh;",
            "Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;",
            "LX/FIU;",
            "LX/FIK;",
            "LX/1qI;",
            "LX/2MM;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2ML;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2222290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222291
    iput-object p1, p0, LX/FIc;->a:LX/0Or;

    .line 2222292
    iput-object p3, p0, LX/FIc;->b:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    .line 2222293
    iput-object p4, p0, LX/FIc;->c:LX/FIU;

    .line 2222294
    iput-object p5, p0, LX/FIc;->d:LX/FIK;

    .line 2222295
    iput-object p6, p0, LX/FIc;->e:LX/1qI;

    .line 2222296
    iput-object p7, p0, LX/FIc;->f:LX/2MM;

    .line 2222297
    iput-object p8, p0, LX/FIc;->g:LX/0Sh;

    .line 2222298
    iput-object p9, p0, LX/FIc;->h:LX/0Or;

    .line 2222299
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/32 v2, 0x927c0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    new-instance v1, LX/FIa;

    invoke-direct {v1, p0}, LX/FIa;-><init>(LX/FIc;)V

    invoke-virtual {v0, v1}, LX/0QN;->a(LX/0Qn;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/FIc;->k:LX/0QI;

    .line 2222300
    iget-object v0, p0, LX/FIc;->b:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    invoke-virtual {v0, p2}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->a(LX/FIh;)V

    .line 2222301
    iput-object p10, p0, LX/FIc;->i:LX/2ML;

    .line 2222302
    invoke-direct {p0}, LX/FIc;->a()V

    .line 2222303
    return-void
.end method

.method public static a(LX/0QB;)LX/FIc;
    .locals 7

    .prologue
    .line 2222373
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2222374
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2222375
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2222376
    if-nez v1, :cond_0

    .line 2222377
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2222378
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2222379
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2222380
    sget-object v1, LX/FIc;->l:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2222381
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2222382
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2222383
    :cond_1
    if-nez v1, :cond_4

    .line 2222384
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2222385
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2222386
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FIc;->b(LX/0QB;)LX/FIc;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2222387
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2222388
    if-nez v1, :cond_2

    .line 2222389
    sget-object v0, LX/FIc;->l:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIc;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2222390
    :goto_1
    if-eqz v0, :cond_3

    .line 2222391
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2222392
    :goto_3
    check-cast v0, LX/FIc;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2222393
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2222394
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2222395
    :catchall_1
    move-exception v0

    .line 2222396
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2222397
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2222398
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2222399
    :cond_2
    :try_start_8
    sget-object v0, LX/FIc;->l:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIc;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2222371
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    iput-wide v0, p0, LX/FIc;->j:J

    .line 2222372
    return-void
.end method

.method public static a$redex0(LX/FIc;LX/FIb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2222400
    if-eqz p1, :cond_0

    .line 2222401
    iget-object v0, p0, LX/FIc;->c:LX/FIU;

    iget-object v1, p1, LX/FIb;->a:LX/FIn;

    invoke-virtual {v0, v1, p2}, LX/FIU;->a(LX/FIn;Ljava/lang/String;)V

    .line 2222402
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)LX/FIc;
    .locals 11

    .prologue
    .line 2222369
    new-instance v0, LX/FIc;

    const/16 v1, 0x15e7

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/FIh;->a(LX/0QB;)LX/FIh;

    move-result-object v2

    check-cast v2, LX/FIh;

    invoke-static {p0}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->a(LX/0QB;)Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    invoke-static {p0}, LX/FIU;->a(LX/0QB;)LX/FIU;

    move-result-object v4

    check-cast v4, LX/FIU;

    invoke-static {p0}, LX/FIK;->a(LX/0QB;)LX/FIK;

    move-result-object v5

    check-cast v5, LX/FIK;

    invoke-static {p0}, LX/1qI;->a(LX/0QB;)LX/1qI;

    move-result-object v6

    check-cast v6, LX/1qI;

    invoke-static {p0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v7

    check-cast v7, LX/2MM;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    const/16 v9, 0x336

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v10

    check-cast v10, LX/2ML;

    invoke-direct/range {v0 .. v10}, LX/FIc;-><init>(LX/0Or;LX/FIh;Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;LX/FIU;LX/FIK;LX/1qI;LX/2MM;LX/0Sh;LX/0Or;LX/2ML;)V

    .line 2222370
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2222358
    iget-object v2, p0, LX/FIc;->h:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, LX/03R;->YES:LX/03R;

    if-ne v2, v3, :cond_3

    .line 2222359
    monitor-enter p0

    .line 2222360
    :try_start_0
    iget-object v2, p0, LX/FIc;->k:LX/0QI;

    invoke-interface {v2, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2222361
    monitor-exit p0

    .line 2222362
    :cond_0
    :goto_0
    return v0

    .line 2222363
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222364
    iget-wide v2, p2, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, LX/FIc;->f:LX/2MM;

    iget-object v3, p2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, LX/2MM;->b(Landroid/net/Uri;)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v0, v1

    .line 2222365
    goto :goto_0

    .line 2222366
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2222367
    :cond_2
    iget-object v2, p2, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->PHOTO:LX/2MK;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2222368
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 10

    .prologue
    .line 2222305
    iget-object v0, p0, LX/FIc;->f:LX/2MM;

    iget-object v1, p2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v6

    .line 2222306
    const-string v0, "Unable to get file for UDP Upload"

    invoke-static {v6, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2222307
    monitor-enter p0

    .line 2222308
    :try_start_0
    iget-object v0, p0, LX/FIc;->k:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIb;

    .line 2222309
    if-eqz v0, :cond_0

    iget-wide v0, v0, LX/FIb;->b:J

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2222310
    :cond_0
    new-instance v0, LX/FIb;

    iget-wide v2, p0, LX/FIc;->j:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, LX/FIc;->j:J

    long-to-int v1, v2

    iget-object v2, p0, LX/FIc;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, LX/FIb;-><init>(IJJ)V

    .line 2222311
    iget-object v1, p0, LX/FIc;->k:LX/0QI;

    invoke-interface {v1, p1, v0}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2222312
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2222313
    iget-object v0, p0, LX/FIc;->k:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIb;

    .line 2222314
    iget-object v1, p0, LX/FIc;->i:LX/2ML;

    invoke-virtual {v1, p2}, LX/2ML;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v2

    .line 2222315
    :try_start_1
    iget-object v1, p0, LX/FIc;->b:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    iget-object v3, v0, LX/FIb;->a:LX/FIn;

    invoke-virtual {v1, p2, v2, v3}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->b(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;LX/FIn;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 2222316
    :goto_0
    new-instance v2, LX/FIY;

    invoke-direct {v2, p0, v0}, LX/FIY;-><init>(LX/FIc;LX/FIb;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2222317
    :try_start_2
    iget-object v2, p0, LX/FIc;->c:LX/FIU;

    new-instance v3, LX/FIT;

    iget-object v4, v0, LX/FIb;->a:LX/FIn;

    iget v4, v4, LX/FIn;->b:I

    iget-object v0, v0, LX/FIb;->a:LX/FIn;

    iget-wide v8, v0, LX/FIn;->a:J

    invoke-direct {v3, v4, v8, v9, v6}, LX/FIT;-><init>(IJLjava/io/File;)V

    .line 2222318
    invoke-static {v2, v3}, LX/FIU;->b(LX/FIU;LX/FIT;)LX/FIS;

    move-result-object v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 2222319
    :try_start_3
    iget-object v0, v4, LX/FIS;->a:LX/FIn;

    .line 2222320
    invoke-static {v2, v0}, LX/FIU;->a(LX/FIU;LX/FIn;)LX/FIS;

    move-result-object v6

    .line 2222321
    if-nez v6, :cond_5

    .line 2222322
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2222323
    :goto_1
    move-object v0, v5

    .line 2222324
    invoke-static {v2, v4, v0}, LX/FIU;->a(LX/FIU;LX/FIS;Ljava/util/List;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 2222325
    :goto_2
    :try_start_4
    iget-object v0, v4, LX/FIS;->e:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v0, v0

    .line 2222326
    :goto_3
    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2222327
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 2222328
    :try_start_5
    iget-object v2, p0, LX/FIc;->k:LX/0QI;

    invoke-interface {v2}, LX/0QI;->c()V

    .line 2222329
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2222330
    :try_start_6
    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2222331
    const v2, -0x59ff7cf3

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 2222332
    :cond_2
    :goto_4
    :try_start_7
    iget-object v2, p0, LX/FIc;->d:LX/FIK;

    invoke-virtual {v2}, LX/FIK;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2222333
    iget-object v2, p0, LX/FIc;->e:LX/1qI;

    invoke-virtual {v2}, LX/1qI;->a()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 2222334
    :cond_3
    :try_start_8
    const-wide/16 v4, 0x226

    .line 2222335
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 2222336
    goto :goto_3

    .line 2222337
    :catch_0
    goto :goto_3

    .line 2222338
    :catchall_0
    move-exception v0

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw v0

    .line 2222339
    :catch_1
    move-exception v1

    .line 2222340
    const-class v3, LX/FIc;

    const-string v4, "Exception when refreshing server connection. Retrying upload without refresh"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2222341
    iget-object v1, p0, LX/FIc;->b:Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;

    iget-object v3, v0, LX/FIb;->a:LX/FIn;

    .line 2222342
    invoke-static {v1, p2, v2, v3}, Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;->c(Lcom/facebook/messaging/media/upload/udp/UDPUploadConnectionManager;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;LX/FIn;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 2222343
    goto :goto_0

    .line 2222344
    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2

    .line 2222345
    :catch_2
    move-exception v0

    .line 2222346
    const-class v1, LX/FIc;

    const-string v2, "Exception when trying to upload file through UDP."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2222347
    throw v0

    .line 2222348
    :cond_4
    const v1, 0x39412c41

    :try_start_c
    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2222349
    iget-object v1, p0, LX/FIc;->k:LX/0QI;

    invoke-interface {v1, p1}, LX/0QI;->b(Ljava/lang/Object;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2

    .line 2222350
    return-object v0

    :catch_3
    goto :goto_4

    .line 2222351
    :catch_4
    :try_start_d
    move-exception v0

    .line 2222352
    const-class v5, LX/FIU;

    const-string v6, "unable to create chunks from file"

    invoke-static {v5, v6, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2

    .line 2222353
    :cond_5
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2222354
    const/4 v5, 0x0

    :goto_5
    iget v3, v6, LX/FIS;->d:I

    if-ge v5, v3, :cond_6

    .line 2222355
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2222356
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 2222357
    :cond_6
    iget-object v5, v6, LX/FIS;->b:Ljava/io/File;

    const/16 v6, 0x5dc

    invoke-static {v5, v7, v6, v0}, LX/FIe;->a(Ljava/io/File;Ljava/util/List;ILX/FIn;)Ljava/util/List;

    move-result-object v5

    goto/16 :goto_1
.end method
