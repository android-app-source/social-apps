.class public LX/GfW;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GfX;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GfW",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/GfX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377532
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2377533
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/GfW;->b:LX/0Zi;

    .line 2377534
    iput-object p1, p0, LX/GfW;->a:LX/0Ot;

    .line 2377535
    return-void
.end method

.method public static a(LX/0QB;)LX/GfW;
    .locals 4

    .prologue
    .line 2377536
    const-class v1, LX/GfW;

    monitor-enter v1

    .line 2377537
    :try_start_0
    sget-object v0, LX/GfW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377538
    sput-object v2, LX/GfW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377539
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377540
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377541
    new-instance v3, LX/GfW;

    const/16 p0, 0x2118

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GfW;-><init>(LX/0Ot;)V

    .line 2377542
    move-object v0, v3

    .line 2377543
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377544
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GfW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377545
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2377547
    const v0, 0x6aa7466e

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2377548
    check-cast p2, LX/GfV;

    .line 2377549
    iget-object v0, p0, LX/GfW;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GfX;

    iget-object v1, p2, LX/GfV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x2

    const/4 p0, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 2377550
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2377551
    check-cast v0, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;

    .line 2377552
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0917

    invoke-interface {v2, v5, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0048

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    .line 2377553
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2377554
    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    move-object v0, v1

    .line 2377555
    invoke-virtual {v3, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v0

    const v3, 0x7f0a015d

    invoke-virtual {v0, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v3, 0x7f0b0050

    invoke-virtual {v0, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v3, 0x7f0b1063

    invoke-virtual {v0, v3}, LX/1ne;->s(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/1ne;->j(F)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const v3, 0x7f020aea

    invoke-virtual {v2, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2377556
    const v3, 0x6aa7466e

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2377557
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00ea

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b00ee

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00eb

    invoke-interface {v2, v5, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00e3

    invoke-interface {v2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00e9

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2377558
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2377559
    invoke-static {}, LX/1dS;->b()V

    .line 2377560
    iget v0, p1, LX/1dQ;->b:I

    .line 2377561
    packed-switch v0, :pswitch_data_0

    .line 2377562
    :goto_0
    return-object v2

    .line 2377563
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2377564
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2377565
    check-cast v1, LX/GfV;

    .line 2377566
    iget-object v3, p0, LX/GfW;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GfX;

    iget-object p1, v1, LX/GfV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/GfV;->b:LX/1Pn;

    .line 2377567
    check-cast p2, LX/1Pk;

    invoke-interface {p2}, LX/1Pk;->e()LX/1SX;

    move-result-object p0

    invoke-virtual {p0, p1, v0}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2377568
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6aa7466e
        :pswitch_0
    .end packed-switch
.end method
