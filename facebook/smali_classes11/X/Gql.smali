.class public LX/Gql;
.super LX/Cod;
.source ""

# interfaces
.implements LX/Gpv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/GpR;",
        ">;",
        "LX/Gpv;"
    }
.end annotation


# instance fields
.field public a:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/view/View;

.field private final d:Landroid/widget/FrameLayout;

.field public e:Z

.field public f:LX/GoE;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2396920
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396921
    const-class v0, LX/Gql;

    invoke-static {v0, p0}, LX/Gql;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396922
    const v0, 0x7f0d1886

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Gql;->c:Landroid/view/View;

    .line 2396923
    const v0, 0x7f0d1885

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Gql;->d:Landroid/widget/FrameLayout;

    .line 2396924
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gql;

    invoke-static {p0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v1

    check-cast v1, LX/GnF;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object p0

    check-cast p0, LX/Chv;

    iput-object v1, p1, LX/Gql;->a:LX/GnF;

    iput-object p0, p1, LX/Gql;->b:LX/Chv;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2396912
    iget-object v0, p0, LX/Gql;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2396913
    iget-object v0, p0, LX/Gql;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2396914
    return-void
.end method

.method public final a(LX/CHX;)V
    .locals 2

    .prologue
    .line 2396918
    iget-object v0, p0, LX/Gql;->d:Landroid/widget/FrameLayout;

    new-instance v1, LX/Gqk;

    invoke-direct {v1, p0, p1}, LX/Gqk;-><init>(LX/Gql;LX/CHX;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396919
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2396915
    iput-boolean p1, p0, LX/Gql;->e:Z

    .line 2396916
    iget-object v0, p0, LX/Gql;->c:Landroid/view/View;

    iget-boolean v1, p0, LX/Gql;->e:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    .line 2396917
    return-void
.end method
