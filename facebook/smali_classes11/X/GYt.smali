.class public LX/GYt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GYs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GYs",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/8Fe;

.field public final b:Ljava/lang/String;

.field public final c:LX/GYk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/GYk",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8Fe;Ljava/lang/String;LX/GYk;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/GYk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8Fe;",
            "Ljava/lang/String;",
            "LX/GYk",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2365822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2365823
    iput-object p1, p0, LX/GYt;->a:LX/8Fe;

    .line 2365824
    iput-object p2, p0, LX/GYt;->b:Ljava/lang/String;

    .line 2365825
    iput-object p3, p0, LX/GYt;->c:LX/GYk;

    .line 2365826
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2365827
    iget-object v0, p0, LX/GYt;->a:LX/8Fe;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/8Fe;->a(Z)V

    .line 2365828
    iget-object v0, p0, LX/GYt;->a:LX/8Fe;

    iget-object v1, p0, LX/GYt;->b:Ljava/lang/String;

    new-instance v2, LX/GYr;

    invoke-direct {v2, p0}, LX/GYr;-><init>(LX/GYt;)V

    invoke-virtual {v0, v1, v2}, LX/8Fe;->a(Ljava/lang/String;LX/8Fd;)V

    .line 2365829
    return-void
.end method
