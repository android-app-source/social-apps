.class public final enum LX/F7k;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F7k;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F7k;

.field public static final enum CURRENT:LX/F7k;

.field public static final enum DAILY_DIALOGUE_STYLE:LX/F7k;

.field public static final enum EVENT_INVITE_GUEST_STYLE:LX/F7k;

.field public static final enum NEW_STYLE_DOUBLE_STEP:LX/F7k;

.field public static final enum NEW_STYLE_SINGLE_STEP:LX/F7k;

.field public static final enum REJECT_REG_TERMS_DD_STYLE:LX/F7k;

.field public static final enum REJECT_REG_TERMS_DOUBLE_STEP:LX/F7k;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2202358
    new-instance v0, LX/F7k;

    const-string v1, "CURRENT"

    invoke-direct {v0, v1, v3}, LX/F7k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7k;->CURRENT:LX/F7k;

    .line 2202359
    new-instance v0, LX/F7k;

    const-string v1, "NEW_STYLE_DOUBLE_STEP"

    invoke-direct {v0, v1, v4}, LX/F7k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7k;->NEW_STYLE_DOUBLE_STEP:LX/F7k;

    .line 2202360
    new-instance v0, LX/F7k;

    const-string v1, "NEW_STYLE_SINGLE_STEP"

    invoke-direct {v0, v1, v5}, LX/F7k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7k;->NEW_STYLE_SINGLE_STEP:LX/F7k;

    .line 2202361
    new-instance v0, LX/F7k;

    const-string v1, "DAILY_DIALOGUE_STYLE"

    invoke-direct {v0, v1, v6}, LX/F7k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7k;->DAILY_DIALOGUE_STYLE:LX/F7k;

    .line 2202362
    new-instance v0, LX/F7k;

    const-string v1, "REJECT_REG_TERMS_DOUBLE_STEP"

    invoke-direct {v0, v1, v7}, LX/F7k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7k;->REJECT_REG_TERMS_DOUBLE_STEP:LX/F7k;

    .line 2202363
    new-instance v0, LX/F7k;

    const-string v1, "REJECT_REG_TERMS_DD_STYLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/F7k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7k;->REJECT_REG_TERMS_DD_STYLE:LX/F7k;

    .line 2202364
    new-instance v0, LX/F7k;

    const-string v1, "EVENT_INVITE_GUEST_STYLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/F7k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7k;->EVENT_INVITE_GUEST_STYLE:LX/F7k;

    .line 2202365
    const/4 v0, 0x7

    new-array v0, v0, [LX/F7k;

    sget-object v1, LX/F7k;->CURRENT:LX/F7k;

    aput-object v1, v0, v3

    sget-object v1, LX/F7k;->NEW_STYLE_DOUBLE_STEP:LX/F7k;

    aput-object v1, v0, v4

    sget-object v1, LX/F7k;->NEW_STYLE_SINGLE_STEP:LX/F7k;

    aput-object v1, v0, v5

    sget-object v1, LX/F7k;->DAILY_DIALOGUE_STYLE:LX/F7k;

    aput-object v1, v0, v6

    sget-object v1, LX/F7k;->REJECT_REG_TERMS_DOUBLE_STEP:LX/F7k;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/F7k;->REJECT_REG_TERMS_DD_STYLE:LX/F7k;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/F7k;->EVENT_INVITE_GUEST_STYLE:LX/F7k;

    aput-object v2, v0, v1

    sput-object v0, LX/F7k;->$VALUES:[LX/F7k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2202366
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F7k;
    .locals 1

    .prologue
    .line 2202367
    const-class v0, LX/F7k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F7k;

    return-object v0
.end method

.method public static values()[LX/F7k;
    .locals 1

    .prologue
    .line 2202368
    sget-object v0, LX/F7k;->$VALUES:[LX/F7k;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F7k;

    return-object v0
.end method
