.class public final LX/GE0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;",
        ">;",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GE2;


# direct methods
.method public constructor <init>(LX/GE2;)V
    .locals 0

    .prologue
    .line 2330783
    iput-object p1, p0, LX/GE0;->a:LX/GE2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2330784
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2330785
    if-eqz p1, :cond_0

    .line 2330786
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330787
    if-eqz v0, :cond_0

    .line 2330788
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330789
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2330790
    :cond_0
    const/4 v0, 0x0

    .line 2330791
    :goto_0
    return-object v0

    .line 2330792
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330793
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchBudgetRecommendationsQueryModels$FetchBudgetRecommendationsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationsModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2330794
    const/4 v0, 0x0

    return v0
.end method
