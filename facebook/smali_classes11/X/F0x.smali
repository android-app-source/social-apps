.class public LX/F0x;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/17W;

.field private c:LX/0wM;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;LX/17W;LX/0wM;)V
    .locals 1

    .prologue
    .line 2190991
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2190992
    iget-object v0, p1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;->a:LX/0Px;

    move-object v0, v0

    .line 2190993
    iput-object v0, p0, LX/F0x;->a:LX/0Px;

    .line 2190994
    iput-object p2, p0, LX/F0x;->b:LX/17W;

    .line 2190995
    iput-object p3, p0, LX/F0x;->c:LX/0wM;

    .line 2190996
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2191016
    iget-object v0, p0, LX/F0x;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2191015
    iget-object v0, p0, LX/F0x;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2191017
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2190998
    iget-object v0, p0, LX/F0x;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;

    .line 2190999
    if-nez p2, :cond_0

    .line 2191000
    new-instance p2, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;)V

    .line 2191001
    :goto_0
    iget-object v1, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2191002
    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2191003
    iget-object v1, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2191004
    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2191005
    sget-object v1, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2191006
    iget-object v1, p0, LX/F0x;->c:LX/0wM;

    const v2, 0x7f020740

    const v3, -0x4d4d4e

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2191007
    iget-object v1, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2191008
    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2191009
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2191010
    iget-object v1, p2, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2191011
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 2191012
    new-instance v1, LX/F0w;

    invoke-direct {v1, p0, v0, p2}, LX/F0w;-><init>(LX/F0x;Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;)V

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2191013
    return-object p2

    .line 2191014
    :cond_0
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 2190997
    const/4 v0, 0x1

    return v0
.end method
