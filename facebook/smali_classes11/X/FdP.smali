.class public final LX/FdP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2263457
    iput-object p1, p0, LX/FdP;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .prologue
    .line 2263437
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2263438
    iget-object v0, p0, LX/FdP;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->h:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2263439
    :goto_0
    iget-object v0, p0, LX/FdP;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v1, v0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->i:LX/Fcx;

    if-nez p1, :cond_1

    const-string v0, ""

    .line 2263440
    :goto_1
    iget-boolean v2, v1, LX/Fcx;->c:Z

    if-nez v2, :cond_2

    .line 2263441
    :goto_2
    iget-object v0, p0, LX/FdP;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->d:LX/Fc6;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fc6;->a(Ljava/lang/String;)V

    .line 2263442
    return-void

    .line 2263443
    :cond_0
    iget-object v0, p0, LX/FdP;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->h:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    .line 2263444
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2263445
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<b>\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\"</b>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2263446
    new-instance v3, Landroid/text/SpannableString;

    iget-object v4, v1, LX/Fcx;->h:Ljava/lang/String;

    const-string v5, "\\{(.*?)\\}"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v3, v1, LX/Fcx;->g:Landroid/text/SpannableString;

    .line 2263447
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    :goto_3
    iput-object v2, v1, LX/Fcx;->f:Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263448
    goto :goto_2

    .line 2263449
    :cond_3
    invoke-static {}, Lcom/facebook/search/results/protocol/filters/FilterValue;->g()LX/5ur;

    move-result-object v2

    .line 2263450
    iput-object v0, v2, LX/5ur;->a:Ljava/lang/String;

    .line 2263451
    move-object v2, v2

    .line 2263452
    iput-object v0, v2, LX/5ur;->b:Ljava/lang/String;

    .line 2263453
    move-object v2, v2

    .line 2263454
    iput-object v0, v2, LX/5ur;->c:Ljava/lang/String;

    .line 2263455
    move-object v2, v2

    .line 2263456
    invoke-virtual {v2}, LX/5ur;->f()Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v2

    goto :goto_3
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2263436
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2263435
    return-void
.end method
