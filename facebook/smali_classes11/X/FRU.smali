.class public LX/FRU;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/ui/PaymentsComponentViewGroup;",
        "LX/6vq",
        "<",
        "LX/FRr;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field public b:LX/FRr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2240426
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 2240427
    const v0, 0x7f030f12

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2240428
    const v0, 0x7f0d24a8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, LX/FRU;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2240429
    iget-object v0, p0, LX/FRU;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    invoke-virtual {v1}, LX/0xq;->ordinal()I

    move-result v1

    invoke-static {v1}, LX/0xq;->fromIndex(I)LX/0xq;

    move-result-object v1

    sget-object v2, LX/0xr;->REGULAR:LX/0xr;

    invoke-virtual {v2}, LX/0xr;->ordinal()I

    move-result v2

    invoke-static {v2}, LX/0xr;->fromIndex(I)LX/0xr;

    move-result-object v2

    iget-object p1, p0, LX/FRU;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object p1

    invoke-static {v0, v1, v2, p1}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2240430
    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 2

    .prologue
    .line 2240431
    iget-object v0, p0, LX/FRU;->b:LX/FRr;

    iget-object v0, v0, LX/FRr;->a:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 2240432
    :goto_0
    return-void

    .line 2240433
    :cond_0
    iget-object v0, p0, LX/FRU;->b:LX/FRr;

    iget v0, v0, LX/FRr;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 2240434
    iget-object v0, p0, LX/FRU;->b:LX/FRr;

    iget-object v0, v0, LX/FRr;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 2240435
    :cond_1
    iget-object v0, p0, LX/FRU;->b:LX/FRr;

    iget-object v0, v0, LX/FRr;->a:Landroid/content/Intent;

    iget-object v1, p0, LX/FRU;->b:LX/FRr;

    iget v1, v1, LX/FRr;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a(Landroid/content/Intent;I)V

    goto :goto_0
.end method
