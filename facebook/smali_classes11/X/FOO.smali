.class public LX/FOO;
.super LX/FON;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

.field public final b:J


# direct methods
.method public constructor <init>(ZLjava/lang/String;LX/0Px;Lcom/facebook/messaging/model/messages/ParticipantInfo;J)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/model/messages/ParticipantInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 2235084
    invoke-direct {p0, p1, p2, p3}, LX/FON;-><init>(ZLjava/lang/String;LX/0Px;)V

    .line 2235085
    iput-object p4, p0, LX/FOO;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2235086
    iput-wide p5, p0, LX/FOO;->b:J

    .line 2235087
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2235088
    const-class v0, LX/FOO;

    invoke-static {v0}, LX/0Qh;->toStringHelper(Ljava/lang/Class;)LX/0zA;

    move-result-object v0

    const-string v1, "name"

    .line 2235089
    iget-object v2, p0, LX/FON;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2235090
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "memberNames"

    .line 2235091
    iget-object v2, p0, LX/FON;->c:LX/0Px;

    move-object v2, v2

    .line 2235092
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "canonicalThreadParticipantInfo"

    .line 2235093
    iget-object v2, p0, LX/FOO;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-object v2, v2

    .line 2235094
    invoke-virtual {v0, v1, v2}, LX/0zA;->add(Ljava/lang/String;Ljava/lang/Object;)LX/0zA;

    move-result-object v0

    const-string v1, "canonicalThreadParticipantLastReadTime"

    .line 2235095
    iget-object v4, p0, LX/FOO;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v4, :cond_0

    iget-wide v4, p0, LX/FOO;->b:J

    :goto_0
    move-wide v2, v4

    .line 2235096
    invoke-virtual {v0, v1, v2, v3}, LX/0zA;->add(Ljava/lang/String;J)LX/0zA;

    move-result-object v0

    invoke-virtual {v0}, LX/0zA;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-wide/16 v4, -0x1

    goto :goto_0
.end method
