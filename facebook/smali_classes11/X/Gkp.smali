.class public abstract LX/Gkp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "mPageInfoModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "mPageInfoModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end field

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field private f:Landroid/content/res/Resources;

.field public g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 2389538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2389539
    iput-object p1, p0, LX/Gkp;->f:Landroid/content/res/Resources;

    .line 2389540
    return-void
.end method

.method public static l(LX/Gkp;)V
    .locals 4

    .prologue
    .line 2389528
    iget-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 2389529
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    .line 2389530
    :goto_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2389531
    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkq;

    .line 2389532
    iget-object v2, p0, LX/Gkp;->g:Ljava/util/HashMap;

    .line 2389533
    iget-object v3, v0, LX/Gkq;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2389534
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2389535
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2389536
    :cond_0
    iget-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0

    .line 2389537
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2389490
    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)LX/Gkq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2389525
    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2389526
    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkq;

    .line 2389527
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;"
        }
    .end annotation
.end method

.method public final a(LX/0Px;LX/15i;I)V
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "update"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Gkq;",
            ">;",
            "LX/15i;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2389514
    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2389515
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    .line 2389516
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2389517
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v2, p0, LX/Gkp;->g:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkq;

    .line 2389518
    iget-object v3, v0, LX/Gkq;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2389519
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2389520
    :cond_1
    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2389521
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2389522
    :cond_3
    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, LX/Gkp;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    .line 2389523
    invoke-static {p0}, LX/Gkp;->l(LX/Gkp;)V

    .line 2389524
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p2, p0, LX/Gkp;->a:LX/15i;

    iput p3, p0, LX/Gkp;->b:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(LX/Gkq;)V
    .locals 2

    .prologue
    .line 2389505
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    .line 2389506
    iget-object v1, p1, LX/Gkq;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2389507
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2389508
    :cond_0
    :goto_0
    return-void

    .line 2389509
    :cond_1
    iget-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    .line 2389510
    iget-object v1, p1, LX/Gkq;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2389511
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2389512
    iget-object v1, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2389513
    invoke-static {p0}, LX/Gkp;->l(LX/Gkp;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2389504
    invoke-virtual {p0}, LX/Gkp;->a()I

    move-result v0

    return v0
.end method

.method public b(I)LX/GlC;
    .locals 5

    .prologue
    .line 2389541
    new-instance v0, LX/GlC;

    invoke-direct {v0}, LX/GlC;-><init>()V

    move-object v0, v0

    .line 2389542
    const-string v1, "group_item_small_cover_photo_size"

    iget-object v2, p0, LX/Gkp;->f:Landroid/content/res/Resources;

    .line 2389543
    invoke-static {v2}, LX/DaX;->a(Landroid/content/res/Resources;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2389544
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2389545
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 2389546
    if-eqz v1, :cond_0

    .line 2389547
    const-string v1, "exclude_work_communities"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2389548
    :cond_0
    invoke-virtual {p0}, LX/Gkp;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2389549
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, LX/Gkp;->a:LX/15i;

    iget v3, p0, LX/Gkp;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "end_cursor"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2389550
    :cond_1
    const-string v1, "count"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2389551
    return-object v0

    .line 2389552
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final b(LX/Gkq;)V
    .locals 3

    .prologue
    .line 2389499
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    .line 2389500
    iget-object v1, p1, LX/Gkq;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2389501
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389502
    :goto_0
    return-void

    .line 2389503
    :cond_0
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/Gkp;->a:LX/15i;

    iget v2, p0, LX/Gkp;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {p0, v1, v0, v2}, LX/Gkp;->a(LX/0Px;LX/15i;I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public synthetic c()Ljava/lang/Enum;
    .locals 1

    .prologue
    .line 2389498
    invoke-virtual {p0}, LX/Gkp;->i()LX/Gkn;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2389496
    invoke-virtual {p0}, LX/Gkp;->f()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    .line 2389497
    :cond_1
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, LX/Gkp;->a:LX/15i;

    iget v4, p0, LX/Gkp;->b:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v3, v4, v0}, LX/15i;->h(II)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v3, p0, LX/Gkp;->a:LX/15i;

    iget v4, p0, LX/Gkp;->b:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v3, v4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2389495
    invoke-virtual {p0}, LX/Gkp;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Gkp;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2389494
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, LX/Gkp;->b:I

    monitor-exit v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract i()LX/Gkn;
.end method

.method public final j()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2389491
    iput-object v0, p0, LX/Gkp;->c:Ljava/util/ArrayList;

    .line 2389492
    iput-object v0, p0, LX/Gkp;->g:Ljava/util/HashMap;

    .line 2389493
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/Gkp;->a:LX/15i;

    const/4 v0, 0x0

    iput v0, p0, LX/Gkp;->b:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
