.class public final enum LX/FMW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FMW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FMW;

.field public static final enum MESSAGE_EXPIRED:LX/FMW;

.field public static final enum MESSAGE_NOT_FOUND:LX/FMW;

.field public static final enum NO_ERROR:LX/FMW;

.field public static final enum OTHER:LX/FMW;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2229794
    new-instance v0, LX/FMW;

    const-string v1, "NO_ERROR"

    invoke-direct {v0, v1, v2}, LX/FMW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMW;->NO_ERROR:LX/FMW;

    .line 2229795
    new-instance v0, LX/FMW;

    const-string v1, "MESSAGE_NOT_FOUND"

    invoke-direct {v0, v1, v3}, LX/FMW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMW;->MESSAGE_NOT_FOUND:LX/FMW;

    .line 2229796
    new-instance v0, LX/FMW;

    const-string v1, "MESSAGE_EXPIRED"

    invoke-direct {v0, v1, v4}, LX/FMW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMW;->MESSAGE_EXPIRED:LX/FMW;

    .line 2229797
    new-instance v0, LX/FMW;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v5}, LX/FMW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMW;->OTHER:LX/FMW;

    .line 2229798
    const/4 v0, 0x4

    new-array v0, v0, [LX/FMW;

    sget-object v1, LX/FMW;->NO_ERROR:LX/FMW;

    aput-object v1, v0, v2

    sget-object v1, LX/FMW;->MESSAGE_NOT_FOUND:LX/FMW;

    aput-object v1, v0, v3

    sget-object v1, LX/FMW;->MESSAGE_EXPIRED:LX/FMW;

    aput-object v1, v0, v4

    sget-object v1, LX/FMW;->OTHER:LX/FMW;

    aput-object v1, v0, v5

    sput-object v0, LX/FMW;->$VALUES:[LX/FMW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2229791
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FMW;
    .locals 1

    .prologue
    .line 2229793
    const-class v0, LX/FMW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FMW;

    return-object v0
.end method

.method public static values()[LX/FMW;
    .locals 1

    .prologue
    .line 2229792
    sget-object v0, LX/FMW;->$VALUES:[LX/FMW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FMW;

    return-object v0
.end method
