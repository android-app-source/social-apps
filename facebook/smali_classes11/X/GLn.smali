.class public final LX/GLn;
.super LX/GFo;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/GLp;


# direct methods
.method public constructor <init>(LX/GLp;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2343634
    iput-object p1, p0, LX/GLn;->b:LX/GLp;

    iput-object p2, p0, LX/GLn;->a:Landroid/content/Context;

    invoke-direct {p0}, LX/GFo;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 12

    .prologue
    .line 2343635
    check-cast p1, LX/GFn;

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2343636
    iget-object v0, p1, LX/GFn;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-object v0, v0

    .line 2343637
    if-nez v0, :cond_1

    .line 2343638
    iget-object v0, p0, LX/GLn;->b:LX/GLp;

    iget-boolean v0, v0, LX/GLp;->h:Z

    if-nez v0, :cond_0

    .line 2343639
    iget-object v0, p0, LX/GLn;->b:LX/GLp;

    iget-object v0, v0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2343640
    iget-object v0, p0, LX/GLn;->b:LX/GLp;

    .line 2343641
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343642
    sget-object v1, LX/GG8;->SERVER_VALIDATION_ERROR:LX/GG8;

    invoke-virtual {v0, v1, v6}, LX/GCE;->a(LX/GG8;Z)V

    .line 2343643
    :cond_0
    :goto_0
    return-void

    .line 2343644
    :cond_1
    iget-object v0, p1, LX/GFn;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-object v2, v0

    .line 2343645
    iget-object v0, p0, LX/GLn;->b:LX/GLp;

    .line 2343646
    iget-object v1, p1, LX/GFn;->b:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 2343647
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2343648
    if-eqz v1, :cond_4

    .line 2343649
    iget-object v5, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2343650
    iget-object v5, v0, LX/GLp;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v5, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2343651
    iget-object v5, v0, LX/GLp;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2343652
    :goto_1
    move v0, v3

    .line 2343653
    if-eqz v0, :cond_0

    .line 2343654
    iget-object v0, p0, LX/GLn;->b:LX/GLp;

    iget-object v0, v0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2343655
    new-instance v3, LX/0wM;

    invoke-direct {v3, v0}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2343656
    sget-object v0, LX/GLo;->b:[I

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2343657
    iget-object v0, p0, LX/GLn;->a:Landroid/content/Context;

    const v1, 0x7f0a0368

    invoke-static {v0, v1}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v1

    .line 2343658
    const v0, 0x7f021a0c

    iget-object v4, p0, LX/GLn;->a:Landroid/content/Context;

    const v5, 0x7f0a00d5

    invoke-static {v4, v5}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2343659
    iget-object v3, p0, LX/GLn;->b:LX/GLp;

    .line 2343660
    iget-object v4, v3, LX/GHg;->b:LX/GCE;

    move-object v3, v4

    .line 2343661
    sget-object v4, LX/GG8;->SERVER_VALIDATION_ERROR:LX/GG8;

    invoke-virtual {v3, v4, v6}, LX/GCE;->a(LX/GG8;Z)V

    .line 2343662
    :goto_2
    iget-object v3, p0, LX/GLn;->b:LX/GLp;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->a()Z

    move-result v4

    .line 2343663
    iput-boolean v4, v3, LX/GLp;->h:Z

    .line 2343664
    iget-object v3, p0, LX/GLn;->b:LX/GLp;

    iget-object v3, v3, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v4}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2343665
    iget-object v1, p0, LX/GLn;->b:LX/GLp;

    iget-object v1, v1, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1, v0, v7, v7, v7}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2343666
    iget-object v0, p0, LX/GLn;->b:LX/GLp;

    iget-object v0, v0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, v8}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2343667
    iget-object v0, p0, LX/GLn;->b:LX/GLp;

    .line 2343668
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343669
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v1, v1

    .line 2343670
    iget-object v0, p0, LX/GLn;->b:LX/GLp;

    iget-object v3, v0, LX/GLp;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->name()Ljava/lang/String;

    move-result-object v2

    const/4 v9, 0x1

    .line 2343671
    :try_start_0
    invoke-static {v3}, LX/GG3;->J(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v4

    if-nez v4, :cond_8
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 2343672
    :cond_2
    :goto_4
    goto/16 :goto_0

    .line 2343673
    :pswitch_0
    iget-object v0, p0, LX/GLn;->a:Landroid/content/Context;

    const v1, 0x7f0a0362

    invoke-static {v0, v1}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v1

    .line 2343674
    const v0, 0x7f021a0b

    iget-object v4, p0, LX/GLn;->a:Landroid/content/Context;

    const v5, 0x7f0a00d5

    invoke-static {v4, v5}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2343675
    iget-object v3, p0, LX/GLn;->b:LX/GLp;

    .line 2343676
    iget-object v4, v3, LX/GHg;->b:LX/GCE;

    move-object v3, v4

    .line 2343677
    sget-object v4, LX/GG8;->SERVER_VALIDATION_ERROR:LX/GG8;

    invoke-virtual {v3, v4, v8}, LX/GCE;->a(LX/GG8;Z)V

    goto :goto_2

    .line 2343678
    :pswitch_1
    iget-object v0, p0, LX/GLn;->a:Landroid/content/Context;

    const v1, 0x7f0a0365

    invoke-static {v0, v1}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v1

    .line 2343679
    const v0, 0x7f021a0b

    iget-object v4, p0, LX/GLn;->a:Landroid/content/Context;

    const v5, 0x7f0a00d5

    invoke-static {v4, v5}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2343680
    iget-object v3, p0, LX/GLn;->b:LX/GLp;

    .line 2343681
    iget-object v4, v3, LX/GHg;->b:LX/GCE;

    move-object v3, v4

    .line 2343682
    sget-object v4, LX/GG8;->SERVER_VALIDATION_ERROR:LX/GG8;

    invoke-virtual {v3, v4, v6}, LX/GCE;->a(LX/GG8;Z)V

    goto/16 :goto_2

    .line 2343683
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2343684
    :cond_4
    iget-object v5, v0, LX/GLp;->e:Lcom/facebook/resources/ui/FbButton;

    const/16 v9, 0x8

    invoke-virtual {v5, v9}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2343685
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->k()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v5

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->GENERIC:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    if-eq v5, v9, :cond_5

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->k()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v5

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    if-ne v5, v9, :cond_6

    .line 2343686
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->l()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v4

    .line 2343687
    :try_start_1
    iget-object v5, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v5, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_0

    .line 2343688
    :goto_5
    iget-object v4, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto/16 :goto_1

    .line 2343689
    :cond_6
    iget-object v5, v0, LX/GLp;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v5}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v5

    invoke-virtual {v5}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_7

    .line 2343690
    sget-object v3, LX/GLo;->a:[I

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->k()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->ordinal()I

    move-result v5

    aget v3, v3, v5

    packed-switch v3, :pswitch_data_1

    :goto_6
    :pswitch_2
    move v3, v4

    .line 2343691
    goto/16 :goto_1

    .line 2343692
    :pswitch_3
    iget-object v3, v0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2343693
    new-instance v5, LX/GFI;

    invoke-direct {v5, v2}, LX/GFI;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v3, v5}, LX/GCE;->a(LX/8wN;)V

    goto :goto_6

    .line 2343694
    :cond_7
    iget-object v4, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2343695
    const v5, 0x7f080b38

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2343696
    const v9, 0x7f080b37

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2343697
    new-instance v10, LX/47x;

    invoke-direct {v10, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2343698
    const-string v4, "[[component]]"

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->k()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v11

    .line 2343699
    sget-object p1, LX/GLo;->a:[I

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->ordinal()I

    move-result v1

    aget p1, p1, v1

    packed-switch p1, :pswitch_data_2

    .line 2343700
    const-string p1, ""

    :goto_7
    move-object v11, p1

    .line 2343701
    invoke-virtual {v5, v4, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2343702
    const-string v4, "[[tap_to_view]]"

    iget-object v5, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v11, 0x7f0a0100

    invoke-static {v5, v11}, LX/GMo;->a(Landroid/content/Context;I)I

    move-result v5

    .line 2343703
    new-instance v11, LX/GLk;

    invoke-direct {v11, v0, v2, v5}, LX/GLk;-><init>(LX/GLp;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;I)V

    .line 2343704
    iget-object p1, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    new-instance v1, LX/GLl;

    invoke-direct {v1, v0, v11}, LX/GLl;-><init>(LX/GLp;Landroid/text/style/ClickableSpan;)V

    invoke-virtual {p1, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2343705
    move-object v5, v11

    .line 2343706
    const/16 v11, 0x21

    invoke-virtual {v10, v4, v9, v5, v11}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2343707
    iget-object v4, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v10}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2343708
    iget-object v4, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto/16 :goto_1

    :catch_0
    goto/16 :goto_5

    .line 2343709
    :pswitch_4
    iget-object p1, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f080a63

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_7

    .line 2343710
    :pswitch_5
    iget-object p1, v0, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f080ae8

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_7

    .line 2343711
    :cond_8
    :try_start_2
    iget-object v4, v1, LX/GG3;->a:LX/0Zb;

    const-string v5, "render_warning_message"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2343712
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2343713
    const/4 v5, 0x0

    invoke-static {v1, v4, v3, v5}, LX/GG3;->a(LX/GG3;LX/0oG;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/Throwable;)V

    .line 2343714
    const-string v5, "error_description"

    invoke-virtual {v4, v5, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2343715
    const-string v5, "error_type"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2343716
    invoke-static {v3}, LX/GG3;->Q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v5

    .line 2343717
    const-string v6, "flow"

    invoke-virtual {v4, v6, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2343718
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2343719
    sget-boolean v4, LX/GG3;->k:Z

    if-eqz v4, :cond_2

    .line 2343720
    iget-object v4, v1, LX/GG3;->e:Landroid/content/Context;

    const-string v5, "EVENT:render_warning_message"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_4

    .line 2343721
    :catch_1
    move-exception v4

    .line 2343722
    iget-object v5, v1, LX/GG3;->c:LX/2U3;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to log event render_warning_message : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v4}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2343723
    sget-boolean v4, LX/GG3;->k:Z

    if-eqz v4, :cond_2

    .line 2343724
    iget-object v4, v1, LX/GG3;->e:Landroid/content/Context;

    const-string v5, "Failed to log event render_warning_message"

    invoke-static {v4, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
