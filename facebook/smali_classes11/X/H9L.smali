.class public LX/H9L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field public final d:LX/H8W;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/25G;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field private i:Ljava/lang/String;

.field private j:LX/H8E;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2434295
    const v0, 0x7f0208fa

    sput v0, LX/H9L;->a:I

    .line 2434296
    const v0, 0x7f081780

    sput v0, LX/H9L;->b:I

    .line 2434297
    const v0, 0x7f081781

    sput v0, LX/H9L;->c:I

    return-void
.end method

.method public constructor <init>(LX/H8W;LX/0Ot;LX/0Ot;LX/0Or;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)V
    .locals 1
    .param p5    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H8W;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/25G;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434287
    iput-object p1, p0, LX/H9L;->d:LX/H8W;

    .line 2434288
    iput-object p2, p0, LX/H9L;->e:LX/0Ot;

    .line 2434289
    iput-object p3, p0, LX/H9L;->f:LX/0Ot;

    .line 2434290
    iput-object p4, p0, LX/H9L;->g:LX/0Or;

    .line 2434291
    invoke-virtual {p5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->m()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v0

    iput-object v0, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434292
    invoke-virtual {p5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->d()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->d()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageLikeActionDataModel$CalloutTextModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/H9L;->i:Ljava/lang/String;

    .line 2434293
    return-void

    .line 2434294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/H9L;Z)V
    .locals 2

    .prologue
    .line 2434277
    iget-object v0, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->l()Z

    move-result v0

    .line 2434278
    iget-object v1, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    instance-of v1, v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    if-eqz v1, :cond_0

    if-eq v0, p1, :cond_0

    .line 2434279
    iget-object v0, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-static {v0}, LX/9YE;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)LX/9YE;

    move-result-object v0

    .line 2434280
    iput-boolean p1, v0, LX/9YE;->i:Z

    .line 2434281
    move-object v0, v0

    .line 2434282
    invoke-virtual {v0}, LX/9YE;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v0

    iput-object v0, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434283
    iget-object v0, p0, LX/H9L;->d:LX/H8W;

    new-instance v1, LX/HDZ;

    invoke-direct {v1, p1}, LX/HDZ;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/H8W;->a(LX/HDS;)V

    .line 2434284
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 2434298
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    iget-object v2, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, LX/H9L;->c:I

    :goto_0
    sget v3, LX/H9L;->a:I

    iget-object v5, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->j()Z

    move-result v5

    iget-object v6, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->l()Z

    move-result v7

    iget-object v8, p0, LX/H9L;->i:Ljava/lang/String;

    move v6, v4

    invoke-direct/range {v0 .. v8}, LX/HA7;-><init>(IIIIZZZLjava/lang/String;)V

    return-object v0

    :cond_0
    sget v2, LX/H9L;->b:I

    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434285
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9L;->b:I

    sget v3, LX/H9L;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2434270
    iget-object v0, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->l()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 2434271
    :goto_0
    iget-object v2, p0, LX/H9L;->d:LX/H8W;

    if-eqz v1, :cond_1

    sget-object v0, LX/9XI;->EVENT_TAPPED_LIKE:LX/9XI;

    :goto_1
    iget-object v3, p0, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434272
    invoke-static {p0, v1}, LX/H9L;->a$redex0(LX/H9L;Z)V

    .line 2434273
    iget-object v0, p0, LX/H9L;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    sget-object v2, LX/8Dp;->LIKE:LX/8Dp;

    new-instance v3, LX/H9I;

    invoke-direct {v3, p0, v1}, LX/H9I;-><init>(LX/H9L;Z)V

    new-instance v4, LX/H9J;

    invoke-direct {v4, p0, v1}, LX/H9J;-><init>(LX/H9L;Z)V

    invoke-virtual {v0, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2434274
    return-void

    .line 2434275
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 2434276
    :cond_1
    sget-object v0, LX/9XI;->EVENT_TAPPED_UNLIKE:LX/9XI;

    goto :goto_1
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434267
    iget-object v0, p0, LX/H9L;->j:LX/H8E;

    if-nez v0, :cond_0

    .line 2434268
    new-instance v0, LX/H9K;

    invoke-direct {v0, p0}, LX/H9K;-><init>(LX/H9L;)V

    iput-object v0, p0, LX/H9L;->j:LX/H8E;

    .line 2434269
    :cond_0
    iget-object v0, p0, LX/H9L;->j:LX/H8E;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
