.class public final LX/FVe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FUw;


# instance fields
.field public final synthetic a:LX/FVn;


# direct methods
.method public constructor <init>(LX/FVn;)V
    .locals 0

    .prologue
    .line 2250977
    iput-object p1, p0, LX/FVe;->a:LX/FVn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2250978
    iget-object v0, p0, LX/FVe;->a:LX/FVn;

    iget-object v0, v0, LX/FVn;->d:LX/FVO;

    .line 2250979
    sget-object v1, LX/FVM;->LOAD_MORE_FAILED:LX/FVM;

    iput-object v1, v0, LX/FVO;->b:LX/FVM;

    .line 2250980
    iget-object v1, v0, LX/FVO;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FVN;

    .line 2250981
    invoke-interface {v1}, LX/FVN;->f()V

    goto :goto_0

    .line 2250982
    :cond_0
    return-void
.end method

.method public final a(LX/FVy;)V
    .locals 12

    .prologue
    .line 2250983
    iget-object v0, p0, LX/FVe;->a:LX/FVn;

    sget-object v1, LX/FWA;->FROM_SERVER:LX/FWA;

    .line 2250984
    iget-object v2, p1, LX/FVy;->a:LX/0am;

    move-object v2, v2

    .line 2250985
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2250986
    invoke-virtual {v2, v3}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    .line 2250987
    iget-object v3, v0, LX/FVn;->r:LX/FWC;

    .line 2250988
    iget-object v4, v3, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2250989
    iget-object v5, v3, LX/FWC;->d:Ljava/util/ArrayList;

    const/4 v11, 0x0

    .line 2250990
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2250991
    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    .line 2250992
    :goto_0
    move-object v5, v6

    .line 2250993
    iput-object v5, v3, LX/FWC;->d:Ljava/util/ArrayList;

    .line 2250994
    iget-object v5, v3, LX/FWC;->e:LX/FWA;

    invoke-static {v5, v1}, LX/FWC;->a(LX/FWA;LX/FWA;)LX/FWA;

    move-result-object v5

    iput-object v5, v3, LX/FWC;->e:LX/FWA;

    .line 2250995
    iget-object v5, v3, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v3, v5, v4}, LX/FWC;->a(LX/FWC;Ljava/util/List;I)V

    .line 2250996
    const v4, -0x3c4e1bb1

    invoke-static {v3, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2250997
    iget-object v2, v0, LX/FVn;->d:LX/FVO;

    .line 2250998
    iget-boolean v3, p1, LX/FVy;->b:Z

    move v3, v3

    .line 2250999
    iput-boolean v3, v2, LX/FVO;->c:Z

    .line 2251000
    iget-object v0, p0, LX/FVe;->a:LX/FVn;

    iget-object v0, v0, LX/FVn;->d:LX/FVO;

    invoke-virtual {v0}, LX/FVO;->d()V

    .line 2251001
    return-void

    .line 2251002
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2251003
    invoke-static {v5}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v6

    goto :goto_0

    .line 2251004
    :cond_1
    invoke-static {v5}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v8

    .line 2251005
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/FVs;

    .line 2251006
    instance-of v7, v6, LX/FVt;

    const-string v9, "Last item of list should never be a header."

    invoke-static {v7, v9}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2251007
    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/FVs;

    .line 2251008
    instance-of v9, v7, LX/FVx;

    const-string v10, "First item of second list should always be a header."

    invoke-static {v9, v10}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2251009
    check-cast v6, LX/FVt;

    .line 2251010
    iget-object v9, v6, LX/FVt;->d:Ljava/lang/String;

    move-object v6, v9

    .line 2251011
    check-cast v7, LX/FVx;

    .line 2251012
    iget-object v9, v7, LX/FVx;->a:Ljava/lang/String;

    move-object v7, v9

    .line 2251013
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2251014
    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2251015
    :cond_2
    const/4 v6, 0x1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-interface {v2, v6, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object v6, v8

    .line 2251016
    goto :goto_0
.end method
