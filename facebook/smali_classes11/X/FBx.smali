.class public final LX/FBx;
.super Ljava/util/AbstractMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2209888
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 2209889
    iput-object p1, p0, LX/FBx;->a:Landroid/os/Bundle;

    .line 2209890
    return-void
.end method

.method public static a$redex0(LX/FBx;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2209891
    iget-object v0, p0, LX/FBx;->a:Landroid/os/Bundle;

    invoke-static {v0, p1, p3}, LX/FBy;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2209892
    return-object p2
.end method


# virtual methods
.method public final entrySet()Ljava/util/Set;
    .locals 2

    .prologue
    .line 2209893
    iget-object v0, p0, LX/FBx;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0wv;->a(Ljava/lang/Iterable;)LX/0wv;

    move-result-object v0

    new-instance v1, LX/FBw;

    invoke-direct {v1, p0}, LX/FBw;-><init>(LX/FBx;)V

    invoke-virtual {v0, v1}, LX/0wv;->a(LX/0QK;)LX/0wv;

    move-result-object v0

    invoke-virtual {v0}, LX/0wv;->c()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2209894
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FBx;->a:Landroid/os/Bundle;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2209895
    iget-object v0, p0, LX/FBx;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2209896
    check-cast p1, Ljava/lang/String;

    .line 2209897
    invoke-virtual {p0, p1}, LX/FBx;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {p0, p1, v0, p2}, LX/FBx;->a$redex0(LX/FBx;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2209898
    iget-object v0, p0, LX/FBx;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    return v0
.end method
