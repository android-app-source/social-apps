.class public LX/G4R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/G2T;


# direct methods
.method public constructor <init>(LX/G2T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317630
    iput-object p1, p0, LX/G4R;->a:LX/G2T;

    .line 2317631
    return-void
.end method

.method public static a(LX/0QB;)LX/G4R;
    .locals 4

    .prologue
    .line 2317632
    const-class v1, LX/G4R;

    monitor-enter v1

    .line 2317633
    :try_start_0
    sget-object v0, LX/G4R;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2317634
    sput-object v2, LX/G4R;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2317635
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317636
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2317637
    new-instance p0, LX/G4R;

    invoke-static {v0}, LX/G2T;->a(LX/0QB;)LX/G2T;

    move-result-object v3

    check-cast v3, LX/G2T;

    invoke-direct {p0, v3}, LX/G4R;-><init>(LX/G2T;)V

    .line 2317638
    move-object v0, p0

    .line 2317639
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2317640
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G4R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2317641
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2317642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/G4R;Ljava/util/List;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;",
            "Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2317643
    invoke-virtual {p2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2317644
    :cond_0
    return-void

    .line 2317645
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->l()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    .line 2317646
    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;

    .line 2317647
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemsConnectionFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2317648
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileViewFieldsModel;->j()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemsConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    .line 2317649
    :goto_1
    if-ge v1, v7, :cond_3

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2317650
    const/4 v8, 0x0

    .line 2317651
    sget-object v9, LX/G4Q;->a:[I

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->ordinal()I

    move-result p2

    aget v9, v9, p2

    packed-switch v9, :pswitch_data_0

    .line 2317652
    :goto_2
    if-eqz v8, :cond_2

    .line 2317653
    invoke-interface {v8}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-interface {p1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2317654
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2317655
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2317656
    :pswitch_0
    iget-object v8, p0, LX/G4R;->a:LX/G2T;

    invoke-virtual {v8, v0}, LX/G2T;->c(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)LX/1Fb;

    move-result-object v8

    goto :goto_2

    .line 2317657
    :pswitch_1
    invoke-static {v0}, LX/G2T;->b(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v8

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
