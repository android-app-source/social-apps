.class public final LX/GWe;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 2362487
    const/16 v18, 0x0

    .line 2362488
    const/16 v17, 0x0

    .line 2362489
    const/16 v16, 0x0

    .line 2362490
    const/4 v15, 0x0

    .line 2362491
    const/4 v14, 0x0

    .line 2362492
    const/4 v13, 0x0

    .line 2362493
    const/4 v12, 0x0

    .line 2362494
    const/4 v11, 0x0

    .line 2362495
    const/4 v10, 0x0

    .line 2362496
    const/4 v9, 0x0

    .line 2362497
    const/4 v8, 0x0

    .line 2362498
    const/4 v7, 0x0

    .line 2362499
    const/4 v6, 0x0

    .line 2362500
    const/4 v5, 0x0

    .line 2362501
    const/4 v4, 0x0

    .line 2362502
    const/4 v3, 0x0

    .line 2362503
    const/4 v2, 0x0

    .line 2362504
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 2362505
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2362506
    const/4 v2, 0x0

    .line 2362507
    :goto_0
    return v2

    .line 2362508
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2362509
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_c

    .line 2362510
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 2362511
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2362512
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 2362513
    const-string v20, "can_viewer_comment"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 2362514
    const/4 v7, 0x1

    .line 2362515
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    goto :goto_1

    .line 2362516
    :cond_2
    const-string v20, "can_viewer_comment_with_photo"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 2362517
    const/4 v6, 0x1

    .line 2362518
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    goto :goto_1

    .line 2362519
    :cond_3
    const-string v20, "can_viewer_comment_with_sticker"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 2362520
    const/4 v5, 0x1

    .line 2362521
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 2362522
    :cond_4
    const-string v20, "can_viewer_comment_with_video"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 2362523
    const/4 v4, 0x1

    .line 2362524
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v15

    goto :goto_1

    .line 2362525
    :cond_5
    const-string v20, "can_viewer_like"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 2362526
    const/4 v3, 0x1

    .line 2362527
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto :goto_1

    .line 2362528
    :cond_6
    const-string v20, "does_viewer_like"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 2362529
    const/4 v2, 0x1

    .line 2362530
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 2362531
    :cond_7
    const-string v20, "id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 2362532
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 2362533
    :cond_8
    const-string v20, "legacy_api_post_id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 2362534
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 2362535
    :cond_9
    const-string v20, "likers"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 2362536
    invoke-static/range {p0 .. p1}, LX/GWc;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 2362537
    :cond_a
    const-string v20, "top_level_comments"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 2362538
    invoke-static/range {p0 .. p1}, LX/GWd;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2362539
    :cond_b
    const-string v20, "url"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 2362540
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 2362541
    :cond_c
    const/16 v19, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2362542
    if-eqz v7, :cond_d

    .line 2362543
    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 2362544
    :cond_d
    if-eqz v6, :cond_e

    .line 2362545
    const/4 v6, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 2362546
    :cond_e
    if-eqz v5, :cond_f

    .line 2362547
    const/4 v5, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 2362548
    :cond_f
    if-eqz v4, :cond_10

    .line 2362549
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->a(IZ)V

    .line 2362550
    :cond_10
    if-eqz v3, :cond_11

    .line 2362551
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->a(IZ)V

    .line 2362552
    :cond_11
    if-eqz v2, :cond_12

    .line 2362553
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 2362554
    :cond_12
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2362555
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2362556
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2362557
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2362558
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2362559
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2362560
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2362561
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362562
    if-eqz v0, :cond_0

    .line 2362563
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362564
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362565
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362566
    if-eqz v0, :cond_1

    .line 2362567
    const-string v1, "can_viewer_comment_with_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362568
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362569
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362570
    if-eqz v0, :cond_2

    .line 2362571
    const-string v1, "can_viewer_comment_with_sticker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362572
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362573
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362574
    if-eqz v0, :cond_3

    .line 2362575
    const-string v1, "can_viewer_comment_with_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362576
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362577
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362578
    if-eqz v0, :cond_4

    .line 2362579
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362580
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362581
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362582
    if-eqz v0, :cond_5

    .line 2362583
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362584
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362585
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2362586
    if-eqz v0, :cond_6

    .line 2362587
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362588
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362589
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2362590
    if-eqz v0, :cond_7

    .line 2362591
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362592
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362593
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362594
    if-eqz v0, :cond_8

    .line 2362595
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362596
    invoke-static {p0, v0, p2}, LX/GWc;->a(LX/15i;ILX/0nX;)V

    .line 2362597
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362598
    if-eqz v0, :cond_9

    .line 2362599
    const-string v1, "top_level_comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362600
    invoke-static {p0, v0, p2}, LX/GWd;->a(LX/15i;ILX/0nX;)V

    .line 2362601
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2362602
    if-eqz v0, :cond_a

    .line 2362603
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362604
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362605
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2362606
    return-void
.end method
