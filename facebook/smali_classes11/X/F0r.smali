.class public final enum LX/F0r;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F0r;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F0r;

.field public static final enum COLLAGE_V1:LX/F0r;

.field public static final enum POLAROID_V1:LX/F0r;

.field public static final enum VIDEO_V1:LX/F0r;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2190889
    new-instance v0, LX/F0r;

    const-string v1, "POLAROID_V1"

    const-string v2, "polaroid_v1"

    invoke-direct {v0, v1, v3, v2}, LX/F0r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/F0r;->POLAROID_V1:LX/F0r;

    .line 2190890
    new-instance v0, LX/F0r;

    const-string v1, "COLLAGE_V1"

    const-string v2, "collage_v1"

    invoke-direct {v0, v1, v4, v2}, LX/F0r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/F0r;->COLLAGE_V1:LX/F0r;

    .line 2190891
    new-instance v0, LX/F0r;

    const-string v1, "VIDEO_V1"

    const-string v2, "video_v1"

    invoke-direct {v0, v1, v5, v2}, LX/F0r;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/F0r;->VIDEO_V1:LX/F0r;

    .line 2190892
    const/4 v0, 0x3

    new-array v0, v0, [LX/F0r;

    sget-object v1, LX/F0r;->POLAROID_V1:LX/F0r;

    aput-object v1, v0, v3

    sget-object v1, LX/F0r;->COLLAGE_V1:LX/F0r;

    aput-object v1, v0, v4

    sget-object v1, LX/F0r;->VIDEO_V1:LX/F0r;

    aput-object v1, v0, v5

    sput-object v0, LX/F0r;->$VALUES:[LX/F0r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2190893
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2190894
    iput-object p3, p0, LX/F0r;->name:Ljava/lang/String;

    .line 2190895
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F0r;
    .locals 1

    .prologue
    .line 2190896
    const-class v0, LX/F0r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F0r;

    return-object v0
.end method

.method public static values()[LX/F0r;
    .locals 1

    .prologue
    .line 2190897
    sget-object v0, LX/F0r;->$VALUES:[LX/F0r;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F0r;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2190898
    iget-object v0, p0, LX/F0r;->name:Ljava/lang/String;

    return-object v0
.end method
