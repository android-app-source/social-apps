.class public final LX/Fdt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fc4;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2264252
    iput-object p1, p0, LX/Fdt;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2264253
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7Hc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2264254
    iget-object v0, p4, LX/7Hc;->b:LX/0Px;

    move-object v0, v0

    .line 2264255
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fdt;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->w:LX/Fdo;

    if-nez v0, :cond_1

    .line 2264256
    :cond_0
    :goto_0
    return-void

    .line 2264257
    :cond_1
    iget-object v0, p0, LX/Fdt;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->w:LX/Fdo;

    .line 2264258
    iget-object v1, p4, LX/7Hc;->b:LX/0Px;

    move-object v1, v1

    .line 2264259
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p0

    .line 2264260
    iget-boolean p1, v0, LX/Fdo;->d:Z

    if-eqz p1, :cond_2

    iget-object p1, v0, LX/Fdo;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    if-eqz p1, :cond_2

    .line 2264261
    iget-object p1, v0, LX/Fdo;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {p0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2264262
    :cond_2
    invoke-virtual {p0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object p0

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    iput-object p0, v0, LX/Fdo;->g:LX/0Px;

    .line 2264263
    const p0, 0x1ecf3969

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2264264
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2264265
    return-void
.end method
