.class public final LX/GZQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;",
        ">;",
        "Lcom/facebook/commerce/core/intent/MerchantInfoViewData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GZR;


# direct methods
.method public constructor <init>(LX/GZR;)V
    .locals 0

    .prologue
    .line 2366533
    iput-object p1, p0, LX/GZQ;->a:LX/GZR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2366517
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2366518
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2366519
    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2366520
    if-nez v0, :cond_1

    move v1, v2

    :goto_0
    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    new-array v3, v2, [Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    aput-object v1, v3, v5

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    move v1, v2

    :goto_2
    if-eqz v1, :cond_6

    .line 2366521
    const/4 v1, 0x0

    .line 2366522
    :goto_3
    move-object v0, v1

    .line 2366523
    return-object v0

    .line 2366524
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->n()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2366525
    if-nez v1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v5

    goto :goto_0

    .line 2366526
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->n()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2366527
    new-array v4, v2, [Ljava/lang/CharSequence;

    invoke-virtual {v3, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v4}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    goto :goto_1

    .line 2366528
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->m()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2366529
    if-nez v1, :cond_5

    move v1, v2

    goto :goto_2

    :cond_5
    move v1, v5

    goto :goto_2

    .line 2366530
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->n()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v4, v1, LX/1vs;->b:I

    .line 2366531
    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->m()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v7, v1, LX/1vs;->b:I

    .line 2366532
    new-instance v1, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v7, v5}, LX/15i;->j(II)I

    move-result v5

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceMerchantPageFragmentModel;->k()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-direct/range {v1 .. v7}, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method
