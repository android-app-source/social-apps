.class public LX/GrS;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/view/View;

.field public d:Landroid/widget/ImageView;

.field public e:Landroid/widget/ImageView;

.field private final f:Landroid/view/Display;

.field private final g:Landroid/graphics/Point;

.field private final h:LX/Ctg;

.field public i:Z

.field public j:Z

.field public k:I

.field public l:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2398019
    const-class v0, LX/GrS;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GrS;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Ctg;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2398020
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398021
    iput-object p1, p0, LX/GrS;->h:LX/Ctg;

    .line 2398022
    invoke-interface {p1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d1841

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/GrS;->d:Landroid/widget/ImageView;

    .line 2398023
    invoke-interface {p1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d1842

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/GrS;->e:Landroid/widget/ImageView;

    .line 2398024
    invoke-interface {p1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d16c3

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GrS;->c:Landroid/view/View;

    .line 2398025
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2398026
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, LX/GrS;->f:Landroid/view/Display;

    .line 2398027
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/GrS;->g:Landroid/graphics/Point;

    .line 2398028
    iput-boolean v2, p0, LX/GrS;->i:Z

    .line 2398029
    iput-boolean v2, p0, LX/GrS;->j:Z

    .line 2398030
    iget-object v0, p0, LX/GrS;->f:Landroid/view/Display;

    iget-object v1, p0, LX/GrS;->g:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2398031
    iput v2, p0, LX/GrS;->k:I

    .line 2398032
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/GrS;->l:Landroid/os/Handler;

    .line 2398033
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, LX/GrS;

    const/16 v0, 0x259

    invoke-static {v2, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v2, p0, LX/GrS;->a:LX/0Ot;

    .line 2398034
    return-void
.end method

.method private a(IFFJZLandroid/view/animation/Interpolator;Ljava/util/concurrent/Callable;Ljava/util/concurrent/Callable;)Landroid/view/animation/Animation;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IFFJZ",
            "Landroid/view/animation/Interpolator;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Void;",
            ">;)",
            "Landroid/view/animation/Animation;"
        }
    .end annotation

    .prologue
    .line 2398035
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    .line 2398036
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2398037
    :goto_0
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2398038
    move-wide/from16 v0, p4

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2398039
    move/from16 v0, p6

    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 2398040
    new-instance v3, LX/GrI;

    move-object/from16 v0, p8

    move-object/from16 v1, p9

    invoke-direct {v3, p0, v0, v1}, LX/GrI;-><init>(LX/GrS;Ljava/util/concurrent/Callable;Ljava/util/concurrent/Callable;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2398041
    :goto_1
    return-object v2

    .line 2398042
    :cond_0
    if-nez p1, :cond_1

    .line 2398043
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x1

    const/4 v5, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    move v4, p2

    move v6, p3

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    goto :goto_0

    .line 2398044
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/view/View;I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2398011
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 2398012
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2398013
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 2398014
    aget v3, v1, v0

    if-gez v3, :cond_0

    .line 2398015
    aget v1, v1, v0

    add-int/2addr v1, v2

    .line 2398016
    :goto_0
    int-to-double v4, v1

    int-to-double v2, v2

    div-double v2, v4, v2

    const-wide v4, 0x3fd3333333333333L    # 0.3

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    :goto_1
    return v0

    .line 2398017
    :cond_0
    aget v1, v1, v0

    sub-int v1, p1, v1

    goto :goto_0

    .line 2398018
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 2397975
    iget-object v0, p0, LX/GrS;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GrS;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/GrS;->j:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/GrS;->k:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    .line 2397976
    :cond_0
    :goto_0
    return-void

    .line 2397977
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GrS;->j:Z

    .line 2397978
    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const-wide/16 v4, 0x1ac

    const/4 v6, 0x0

    new-instance v7, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    const/4 v8, 0x0

    new-instance v9, LX/GrJ;

    invoke-direct {v9, p0}, LX/GrJ;-><init>(LX/GrS;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, LX/GrS;->a(IFFJZLandroid/view/animation/Interpolator;Ljava/util/concurrent/Callable;Ljava/util/concurrent/Callable;)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AlphaAnimation;

    .line 2397979
    const/4 v1, 0x0

    const v2, -0x41b33333    # -0.2f

    const/4 v3, 0x0

    const-wide/16 v4, 0x1ac

    const/4 v6, 0x0

    new-instance v7, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v7}, Landroid/view/animation/LinearInterpolator;-><init>()V

    new-instance v8, LX/GrK;

    invoke-direct {v8, p0}, LX/GrK;-><init>(LX/GrS;)V

    new-instance v9, LX/GrL;

    invoke-direct {v9, p0, v0}, LX/GrL;-><init>(LX/GrS;Landroid/view/animation/AlphaAnimation;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, LX/GrS;->a(IFFJZLandroid/view/animation/Interpolator;Ljava/util/concurrent/Callable;Ljava/util/concurrent/Callable;)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/TranslateAnimation;

    .line 2397980
    const/4 v1, 0x0

    const v2, 0x3e4ccccd    # 0.2f

    const v3, -0x41b33333    # -0.2f

    const-wide/16 v4, 0x359

    const/4 v6, 0x0

    new-instance v7, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v7}, Landroid/view/animation/LinearInterpolator;-><init>()V

    new-instance v8, LX/GrM;

    invoke-direct {v8, p0}, LX/GrM;-><init>(LX/GrS;)V

    new-instance v9, LX/GrN;

    invoke-direct {v9, p0, v0}, LX/GrN;-><init>(LX/GrS;Landroid/view/animation/TranslateAnimation;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, LX/GrS;->a(IFFJZLandroid/view/animation/Interpolator;Ljava/util/concurrent/Callable;Ljava/util/concurrent/Callable;)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/TranslateAnimation;

    .line 2397981
    const/4 v1, 0x0

    const/4 v2, 0x0

    const v3, 0x3e4ccccd    # 0.2f

    const-wide/16 v4, 0x1ac

    const/4 v6, 0x0

    new-instance v7, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v7}, Landroid/view/animation/LinearInterpolator;-><init>()V

    new-instance v8, LX/GrO;

    invoke-direct {v8, p0}, LX/GrO;-><init>(LX/GrS;)V

    new-instance v9, LX/GrP;

    invoke-direct {v9, p0, v0}, LX/GrP;-><init>(LX/GrS;Landroid/view/animation/TranslateAnimation;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, LX/GrS;->a(IFFJZLandroid/view/animation/Interpolator;Ljava/util/concurrent/Callable;Ljava/util/concurrent/Callable;)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/TranslateAnimation;

    .line 2397982
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const-wide/16 v4, 0x1ac

    const/4 v6, 0x0

    new-instance v7, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    new-instance v8, LX/GrQ;

    invoke-direct {v8, p0}, LX/GrQ;-><init>(LX/GrS;)V

    new-instance v9, LX/GrR;

    invoke-direct {v9, p0, v0}, LX/GrR;-><init>(LX/GrS;Landroid/view/animation/TranslateAnimation;)V

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, LX/GrS;->a(IFFJZLandroid/view/animation/Interpolator;Ljava/util/concurrent/Callable;Ljava/util/concurrent/Callable;)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AlphaAnimation;

    .line 2397983
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    .line 2397984
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    .line 2397985
    iget-object v1, p0, LX/GrS;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0
.end method

.method public final a(LX/CrS;)V
    .locals 6

    .prologue
    .line 2398000
    iget-object v0, p0, LX/GrS;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 2398001
    :goto_0
    return-void

    .line 2398002
    :cond_0
    iget-object v0, p0, LX/GrS;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 2398003
    iget-object v1, p0, LX/GrS;->g:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, LX/GrS;->d:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 2398004
    iget-object v2, p0, LX/GrS;->d:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    .line 2398005
    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, LX/GrS;->d:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, LX/GrS;->d:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-direct {v3, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398006
    iget-object v1, p0, LX/GrS;->h:LX/Ctg;

    iget-object v2, p0, LX/GrS;->d:Landroid/widget/ImageView;

    invoke-interface {v1, v2, v3}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2398007
    iget-object v1, p0, LX/GrS;->g:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, LX/GrS;->e:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 2398008
    iget-object v2, p0, LX/GrS;->e:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 2398009
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, LX/GrS;->e:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, LX/GrS;->e:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-direct {v2, v1, v0, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398010
    iget-object v0, p0, LX/GrS;->h:LX/Ctg;

    iget-object v1, p0, LX/GrS;->e:Landroid/widget/ImageView;

    invoke-interface {v0, v1, v2}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final a(LX/Crd;)Z
    .locals 2

    .prologue
    .line 2397986
    sget-object v0, LX/Crd;->SCROLL_FINISHED:LX/Crd;

    if-ne p1, v0, :cond_0

    .line 2397987
    iget-object v0, p0, LX/GrS;->c:Landroid/view/View;

    iget-object v1, p0, LX/GrS;->g:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, LX/GrS;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2397988
    iget v0, p0, LX/GrS;->k:I

    if-lez v0, :cond_0

    .line 2397989
    invoke-virtual {p0}, LX/GrS;->a()V

    .line 2397990
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final m()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2397991
    iget-object v0, p0, LX/GrS;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2397992
    iget-object v0, p0, LX/GrS;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2397993
    iget-object v0, p0, LX/GrS;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2397994
    iget-object v0, p0, LX/GrS;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2397995
    iget-object v0, p0, LX/GrS;->l:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2397996
    iget-object v0, p0, LX/GrS;->c:Landroid/view/View;

    check-cast v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->e()V

    .line 2397997
    iput-boolean v2, p0, LX/GrS;->j:Z

    .line 2397998
    iput-boolean v2, p0, LX/GrS;->i:Z

    .line 2397999
    return-void
.end method
