.class public final LX/GPF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/GPG;


# direct methods
.method public constructor <init>(LX/GPG;)V
    .locals 0

    .prologue
    .line 2348530
    iput-object p1, p0, LX/GPF;->a:LX/GPG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 2348532
    iget-object v0, p0, LX/GPF;->a:LX/GPG;

    .line 2348533
    iget-object v1, v0, LX/GP4;->f:LX/GQ7;

    move-object v1, v1

    .line 2348534
    move-object v0, v1

    .line 2348535
    check-cast v0, LX/GQA;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GQA;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2348536
    iget-object v0, p0, LX/GPF;->a:LX/GPG;

    iget-object v1, p0, LX/GPF;->a:LX/GPG;

    iget-object v1, v1, LX/GPG;->d:Ljava/util/concurrent/ExecutorService;

    .line 2348537
    invoke-virtual {v0, v1}, LX/GP4;->a(Ljava/util/concurrent/ExecutorService;)V

    .line 2348538
    :goto_0
    return-void

    .line 2348539
    :cond_0
    iget-object v1, p0, LX/GPF;->a:LX/GPG;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v2, 0x5

    if-lt v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, LX/GP4;->a(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2348540
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2348531
    return-void
.end method
