.class public final LX/GJE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GJK;


# direct methods
.method public constructor <init>(LX/GJK;)V
    .locals 0

    .prologue
    .line 2338015
    iput-object p1, p0, LX/GJE;->a:LX/GJK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, 0x4e7835a0

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2338016
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/GJE;->a:LX/GJK;

    .line 2338017
    iget-object v3, v2, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v2, v3

    .line 2338018
    iget-object v3, p0, LX/GJE;->a:LX/GJK;

    iget v3, v3, LX/GJK;->k:I

    const/4 v7, 0x0

    .line 2338019
    sget-object v4, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    const v8, 0x7f080ac2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v1, v4, v8, v7}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    move-object v4, v2

    .line 2338020
    check-cast v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v4}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v9

    .line 2338021
    new-instance p1, LX/A93;

    invoke-direct {p1}, LX/A93;-><init>()V

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v4

    .line 2338022
    :goto_0
    iput-object v4, p1, LX/A93;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    .line 2338023
    move-object v4, p1

    .line 2338024
    new-instance v7, LX/A92;

    invoke-direct {v7}, LX/A92;-><init>()V

    invoke-static {v2}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object p1

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    .line 2338025
    iput-object p1, v7, LX/A92;->a:LX/0Px;

    .line 2338026
    move-object v7, v7

    .line 2338027
    invoke-virtual {v7}, LX/A92;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v7

    .line 2338028
    iput-object v7, v4, LX/A93;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    .line 2338029
    move-object v4, v4

    .line 2338030
    invoke-virtual {v4}, LX/A93;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v4

    invoke-virtual {v9, v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)V

    .line 2338031
    sget-object v4, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    .line 2338032
    iput-object v4, v9, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    .line 2338033
    invoke-virtual {v9, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2338034
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->j()I

    move-result v4

    .line 2338035
    iput v4, v9, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2338036
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->k()I

    move-result v4

    invoke-virtual {v9, v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(I)V

    .line 2338037
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v4

    invoke-virtual {v9, v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2338038
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v7

    invoke-virtual {v9, v4, v7}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2338039
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9, v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(Ljava/lang/String;)V

    .line 2338040
    const-string v4, "data"

    invoke-virtual {v8, v4, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2338041
    move-object v1, v8

    .line 2338042
    iget-object v2, p0, LX/GJE;->a:LX/GJK;

    .line 2338043
    iget-object v3, v2, LX/GHg;->b:LX/GCE;

    move-object v2, v3

    .line 2338044
    new-instance v3, LX/GFS;

    const/16 v4, 0x10

    invoke-direct {v3, v1, v4, v6}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v2, v3}, LX/GCE;->a(LX/8wN;)V

    .line 2338045
    const v1, 0x6ccc12d5

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move-object v4, v7

    .line 2338046
    goto :goto_0
.end method
