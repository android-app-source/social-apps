.class public LX/GK8;
.super LX/GIs;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GIs",
        "<",
        "Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final r:Ljava/text/NumberFormat;

.field public s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

.field public t:LX/0W9;

.field public u:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation
.end field

.field private w:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

.field public x:LX/GCE;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;LX/GIN;LX/GG6;LX/0W9;LX/GKy;LX/2U3;LX/GL2;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v7, 0x2

    .line 2340434
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p7

    move-object v4, p5

    move-object v5, p6

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/GIs;-><init>(LX/GIN;LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V

    .line 2340435
    iput-object p1, p0, LX/GK8;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2340436
    iput-object p4, p0, LX/GK8;->t:LX/0W9;

    .line 2340437
    iget-object v0, p0, LX/GK8;->t:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, LX/GK8;->r:Ljava/text/NumberFormat;

    .line 2340438
    iget-object v0, p0, LX/GK8;->r:Ljava/text/NumberFormat;

    invoke-virtual {v0, v7}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 2340439
    iget-object v0, p0, LX/GK8;->r:Ljava/text/NumberFormat;

    invoke-virtual {v0, v7}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 2340440
    return-void
.end method

.method public static b(LX/0QB;)LX/GK8;
    .locals 9

    .prologue
    .line 2340431
    new-instance v1, LX/GK8;

    invoke-static {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->b(LX/0QB;)Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-static {p0}, LX/GIN;->b(LX/0QB;)LX/GIN;

    move-result-object v3

    check-cast v3, LX/GIN;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v4

    check-cast v4, LX/GG6;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v6

    check-cast v6, LX/GKy;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v7

    check-cast v7, LX/2U3;

    invoke-static {p0}, LX/GL2;->b(LX/0QB;)LX/GL2;

    move-result-object v8

    check-cast v8, LX/GL2;

    invoke-direct/range {v1 .. v8}, LX/GK8;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;LX/GIN;LX/GG6;LX/0W9;LX/GKy;LX/2U3;LX/GL2;)V

    .line 2340432
    move-object v0, v1

    .line 2340433
    return-object v0
.end method

.method public static g(LX/GK8;)V
    .locals 4

    .prologue
    .line 2340426
    iget-object v0, p0, LX/GK8;->x:LX/GCE;

    iget-object v1, p0, LX/GK8;->u:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2340427
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v2

    .line 2340428
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v1, v2

    .line 2340429
    iget-object v2, p0, LX/GIr;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v3, p0, LX/GK8;->u:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    invoke-static {v0, v1, v2, v3}, LX/GJO;->a(LX/GCE;Lcom/facebook/graphql/enums/GraphQLCallToActionType;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    .line 2340430
    return-void
.end method

.method public static h(LX/GK8;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2340413
    iget-object v0, p0, LX/GK8;->u:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2340414
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v1, v1

    .line 2340415
    if-eqz v1, :cond_0

    .line 2340416
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v1, v1

    .line 2340417
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2340418
    :cond_0
    :goto_0
    return-void

    .line 2340419
    :cond_1
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v0, v1

    .line 2340420
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2340421
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/CharSequence;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/GIr;->f:LX/GIa;

    check-cast v1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b36

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2340422
    :goto_1
    iget-object v2, p0, LX/GK8;->w:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->setAddress(Ljava/lang/String;)V

    .line 2340423
    iget-object v1, p0, LX/GK8;->w:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->setRadius(D)V

    .line 2340424
    iget-object v0, p0, LX/GK8;->w:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    invoke-virtual {v0, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->setVisibility(I)V

    goto :goto_0

    .line 2340425
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2340405
    invoke-super {p0}, LX/GIs;->a()V

    .line 2340406
    iget-object v0, p0, LX/GK8;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2340407
    iget-object v0, p0, LX/GK8;->w:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340408
    iput-object v1, p0, LX/GK8;->x:LX/GCE;

    .line 2340409
    iput-object v1, p0, LX/GK8;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2340410
    iput-object v1, p0, LX/GK8;->f:LX/GIa;

    .line 2340411
    iput-object v1, p0, LX/GK8;->w:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    .line 2340412
    return-void
.end method

.method public final a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2340402
    invoke-super {p0, p1}, LX/GIs;->a(LX/GCE;)V

    .line 2340403
    iget-object v0, p0, LX/GK8;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2340404
    return-void
.end method

.method public final bridge synthetic a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2340350
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2340399
    invoke-super {p0, p1}, LX/GIs;->a(Landroid/os/Bundle;)V

    .line 2340400
    iget-object v0, p0, LX/GK8;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2340401
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2340398
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2340389
    check-cast p1, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2340390
    invoke-super {p0, p1}, LX/GIs;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2340391
    iget-object v0, p0, LX/GK8;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2340392
    iput-object p1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2340393
    iput-object p1, p0, LX/GK8;->u:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2340394
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2340395
    iget-object p1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v0, p1

    .line 2340396
    iput-object v0, p0, LX/GK8;->v:LX/0Px;

    .line 2340397
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 2340360
    iput-object p1, p0, LX/GK8;->f:LX/GIa;

    .line 2340361
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    .line 2340362
    iget-object v1, v0, LX/GIa;->p:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    move-object v0, v1

    .line 2340363
    iput-object v0, p0, LX/GK8;->w:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    .line 2340364
    iput-object p2, p0, LX/GK8;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2340365
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2340366
    iput-object v0, p0, LX/GK8;->x:LX/GCE;

    .line 2340367
    iget-object v0, p0, LX/GK8;->u:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2340368
    goto :goto_1

    .line 2340369
    :goto_0
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {v0, v3}, LX/GIa;->setLocationsSelectorVisibility(I)V

    .line 2340370
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {v0, v3}, LX/GIa;->setInterestsSelectorVisibility(I)V

    .line 2340371
    iget-object v0, p0, LX/GK8;->w:Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;

    iget-object v1, p0, LX/GK8;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2340372
    iget-object p1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->g:Landroid/view/View$OnClickListener;

    move-object v1, p1

    .line 2340373
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesLocalAwarenessAudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2340374
    invoke-virtual {p0, v2}, LX/GIr;->e(Z)V

    .line 2340375
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {v0, v3}, LX/GIa;->setAgeViewTopDividerVisibility(I)V

    .line 2340376
    invoke-virtual {p0, v2}, LX/GIr;->a(Z)V

    .line 2340377
    invoke-static {p0}, LX/GK8;->h(LX/GK8;)V

    .line 2340378
    iget-object v0, p0, LX/GK8;->x:LX/GCE;

    new-instance v1, LX/GK5;

    invoke-direct {v1, p0}, LX/GK5;-><init>(LX/GK8;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2340379
    iget-object v0, p0, LX/GK8;->x:LX/GCE;

    new-instance v1, LX/GK6;

    invoke-direct {v1, p0}, LX/GK6;-><init>(LX/GK8;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2340380
    iget-object v0, p0, LX/GK8;->x:LX/GCE;

    new-instance v1, LX/GK7;

    invoke-direct {v1, p0}, LX/GK7;-><init>(LX/GK8;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2340381
    invoke-static {p2}, LX/GIr;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340382
    return-void

    .line 2340383
    :goto_1
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    .line 2340384
    iget-object v1, v0, LX/GIa;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    move-object v0, v1

    .line 2340385
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->setVisibility(I)V

    .line 2340386
    iget-object v1, p0, LX/GK8;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    .line 2340387
    iget-object p1, v0, LX/GIa;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    move-object v0, p1

    .line 2340388
    iget-object p1, p0, LX/GIr;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1, v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2340357
    invoke-super {p0, p1}, LX/GIs;->b(Landroid/os/Bundle;)V

    .line 2340358
    iget-object v0, p0, LX/GK8;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2340359
    return-void
.end method

.method public final c()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2340351
    invoke-super {p0}, LX/GIs;->c()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2340352
    iget-object v1, p0, LX/GK8;->v:LX/0Px;

    .line 2340353
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    .line 2340354
    sget-object v1, LX/GGF;->HOME:LX/GGF;

    sget-object v2, LX/GGF;->RECENT:LX/GGF;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2340355
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    .line 2340356
    return-object v0
.end method
