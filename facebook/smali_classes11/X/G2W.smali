.class public LX/G2W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/G2W;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G2V;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/G2L;


# direct methods
.method public constructor <init>(LX/0Or;LX/G2L;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/G2V;",
            ">;",
            "LX/G2L;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314132
    iput-object p1, p0, LX/G2W;->a:LX/0Or;

    .line 2314133
    iput-object p2, p0, LX/G2W;->b:LX/G2L;

    .line 2314134
    return-void
.end method

.method public static a(LX/0QB;)LX/G2W;
    .locals 5

    .prologue
    .line 2314135
    sget-object v0, LX/G2W;->c:LX/G2W;

    if-nez v0, :cond_1

    .line 2314136
    const-class v1, LX/G2W;

    monitor-enter v1

    .line 2314137
    :try_start_0
    sget-object v0, LX/G2W;->c:LX/G2W;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2314138
    if-eqz v2, :cond_0

    .line 2314139
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2314140
    new-instance v4, LX/G2W;

    const/16 v3, 0x36cd

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/G2L;->a(LX/0QB;)LX/G2L;

    move-result-object v3

    check-cast v3, LX/G2L;

    invoke-direct {v4, p0, v3}, LX/G2W;-><init>(LX/0Or;LX/G2L;)V

    .line 2314141
    move-object v0, v4

    .line 2314142
    sput-object v0, LX/G2W;->c:LX/G2W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314143
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2314144
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2314145
    :cond_1
    sget-object v0, LX/G2W;->c:LX/G2W;

    return-object v0

    .line 2314146
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2314147
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;)LX/G1Q;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/G1Q;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2314148
    iget-object v0, p0, LX/G2W;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G2V;

    .line 2314149
    new-instance v1, LX/G1Q;

    invoke-direct {v1}, LX/G1Q;-><init>()V

    move-object v1, v1

    .line 2314150
    const-string v2, "profile_id"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "view_styles"

    iget-object v3, v0, LX/G2V;->f:LX/G2L;

    .line 2314151
    iget-object v4, v3, LX/G2L;->e:LX/0Uh;

    const/16 p0, 0x464

    const/4 p1, 0x0

    invoke-virtual {v4, p0, p1}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, LX/G2L;->d:LX/0Px;

    :goto_0
    move-object v3, v4

    .line 2314152
    invoke-virtual {v1, v2, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "section_types_list"

    invoke-virtual {v1, v2, p2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "first_item_count"

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_small_size"

    .line 2314153
    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v3, v3

    .line 2314154
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_big_size"

    .line 2314155
    invoke-static {}, LX/0wB;->c()I

    move-result v3

    .line 2314156
    iget-object v4, v0, LX/G2V;->a:LX/G2T;

    invoke-virtual {v4}, LX/G2T;->b()I

    move-result v4

    .line 2314157
    mul-int/lit8 p0, v3, 0x2

    if-ge v4, p0, :cond_1

    :goto_1
    move v3, v3

    .line 2314158
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "action_link_icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "fetch_dominant_color"

    iget-object v3, v0, LX/G2V;->b:LX/0ad;

    sget-short v4, LX/0wf;->t:S

    const/4 p0, 0x0

    invoke-interface {v3, v4, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "automatic_photo_captioning_enabled"

    iget-object v3, v0, LX/G2V;->e:LX/0sX;

    invoke-virtual {v3}, LX/0sX;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/G1Q;

    .line 2314159
    iget-object v2, v0, LX/G2V;->d:LX/0se;

    iget-object v3, v0, LX/G2V;->c:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->c()LX/0wF;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2314160
    move-object v0, v1

    .line 2314161
    return-object v0

    :cond_0
    sget-object v4, LX/G2L;->c:LX/0Px;

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1
.end method
