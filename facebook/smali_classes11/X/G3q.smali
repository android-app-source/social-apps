.class public LX/G3q;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/G3q;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/G3p;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/G3p;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2316255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2316256
    iput-object p1, p0, LX/G3q;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2316257
    iput-object p2, p0, LX/G3q;->b:LX/G3p;

    .line 2316258
    return-void
.end method

.method public static a(LX/0QB;)LX/G3q;
    .locals 5

    .prologue
    .line 2316259
    sget-object v0, LX/G3q;->c:LX/G3q;

    if-nez v0, :cond_1

    .line 2316260
    const-class v1, LX/G3q;

    monitor-enter v1

    .line 2316261
    :try_start_0
    sget-object v0, LX/G3q;->c:LX/G3q;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2316262
    if-eqz v2, :cond_0

    .line 2316263
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2316264
    new-instance p0, LX/G3q;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/G3p;->a(LX/0QB;)LX/G3p;

    move-result-object v4

    check-cast v4, LX/G3p;

    invoke-direct {p0, v3, v4}, LX/G3q;-><init>(Lcom/facebook/content/SecureContextHelper;LX/G3p;)V

    .line 2316265
    move-object v0, p0

    .line 2316266
    sput-object v0, LX/G3q;->c:LX/G3q;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2316267
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2316268
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2316269
    :cond_1
    sget-object v0, LX/G3q;->c:LX/G3q;

    return-object v0

    .line 2316270
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2316271
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 2316272
    iget-object v0, p0, LX/G3q;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2316273
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/content/ComponentName;

    const-class p0, Lcom/facebook/timeline/refresher/ProfileRefresherActivity;

    invoke-direct {v2, p2, p0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 2316274
    const-string v2, "refresher_configuration"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2316275
    move-object v1, v1

    .line 2316276
    const/16 v2, 0x56e3

    invoke-interface {v0, v1, v2, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2316277
    return-void
.end method
