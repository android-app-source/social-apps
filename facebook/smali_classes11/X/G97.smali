.class public final LX/G97;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:[B

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>([B)V
    .locals 0

    .prologue
    .line 2321611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321612
    iput-object p1, p0, LX/G97;->a:[B

    .line 2321613
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2321614
    iget-object v0, p0, LX/G97;->a:[B

    array-length v0, v0

    iget v1, p0, LX/G97;->b:I

    sub-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x8

    iget v1, p0, LX/G97;->c:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(I)I
    .locals 8

    .prologue
    const/16 v7, 0xff

    const/16 v6, 0x8

    const/4 v2, 0x0

    .line 2321615
    if-lez p1, :cond_0

    const/16 v0, 0x20

    if-gt p1, v0, :cond_0

    invoke-virtual {p0}, LX/G97;->a()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 2321616
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321617
    :cond_1
    iget v0, p0, LX/G97;->c:I

    if-lez v0, :cond_6

    .line 2321618
    iget v0, p0, LX/G97;->c:I

    rsub-int/lit8 v1, v0, 0x8

    .line 2321619
    if-ge p1, v1, :cond_3

    move v0, p1

    .line 2321620
    :goto_0
    sub-int/2addr v1, v0

    .line 2321621
    rsub-int/lit8 v3, v0, 0x8

    shr-int v3, v7, v3

    shl-int/2addr v3, v1

    .line 2321622
    iget-object v4, p0, LX/G97;->a:[B

    iget v5, p0, LX/G97;->b:I

    aget-byte v4, v4, v5

    and-int/2addr v3, v4

    shr-int v1, v3, v1

    .line 2321623
    sub-int/2addr p1, v0

    .line 2321624
    iget v3, p0, LX/G97;->c:I

    add-int/2addr v0, v3

    iput v0, p0, LX/G97;->c:I

    .line 2321625
    iget v0, p0, LX/G97;->c:I

    if-ne v0, v6, :cond_2

    .line 2321626
    iput v2, p0, LX/G97;->c:I

    .line 2321627
    iget v0, p0, LX/G97;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G97;->b:I

    :cond_2
    move v0, v1

    move v1, p1

    .line 2321628
    :goto_1
    if-lez v1, :cond_5

    .line 2321629
    :goto_2
    if-lt v1, v6, :cond_4

    .line 2321630
    shl-int/lit8 v0, v0, 0x8

    iget-object v2, p0, LX/G97;->a:[B

    iget v3, p0, LX/G97;->b:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v0, v2

    .line 2321631
    iget v2, p0, LX/G97;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/G97;->b:I

    .line 2321632
    add-int/lit8 v1, v1, -0x8

    goto :goto_2

    :cond_3
    move v0, v1

    .line 2321633
    goto :goto_0

    .line 2321634
    :cond_4
    if-lez v1, :cond_5

    .line 2321635
    rsub-int/lit8 v2, v1, 0x8

    .line 2321636
    shr-int v3, v7, v2

    shl-int/2addr v3, v2

    .line 2321637
    shl-int/2addr v0, v1

    iget-object v4, p0, LX/G97;->a:[B

    iget v5, p0, LX/G97;->b:I

    aget-byte v4, v4, v5

    and-int/2addr v3, v4

    shr-int v2, v3, v2

    or-int/2addr v0, v2

    .line 2321638
    iget v2, p0, LX/G97;->c:I

    add-int/2addr v1, v2

    iput v1, p0, LX/G97;->c:I

    .line 2321639
    :cond_5
    return v0

    :cond_6
    move v0, v2

    move v1, p1

    goto :goto_1
.end method
