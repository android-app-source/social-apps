.class public final LX/G6D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/wem/watermark/WatermarkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V
    .locals 0

    .prologue
    .line 2319804
    iput-object p1, p0, LX/G6D;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2319806
    if-eqz p1, :cond_0

    .line 2319807
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2319808
    if-eqz v0, :cond_0

    .line 2319809
    iget-object v1, p0, LX/G6D;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    .line 2319810
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2319811
    check-cast v0, LX/5QV;

    .line 2319812
    iput-object v0, v1, Lcom/facebook/wem/watermark/WatermarkActivity;->z:LX/5QV;

    .line 2319813
    iget-object v0, p0, LX/G6D;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->B:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/G6D;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v1, v1, Lcom/facebook/wem/watermark/WatermarkActivity;->z:LX/5QV;

    invoke-interface {v1}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/wem/watermark/WatermarkActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2319814
    iget-object v0, p0, LX/G6D;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    iget-object v1, p0, LX/G6D;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v1, v1, Lcom/facebook/wem/watermark/WatermarkActivity;->z:LX/5QV;

    invoke-interface {v1}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v1

    .line 2319815
    iget-object v2, v0, LX/G6Q;->b:Ljava/util/HashMap;

    const-string p0, "watermark_id"

    invoke-virtual {v2, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2319816
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2319817
    iget-object v0, p0, LX/G6D;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/G6Q;->c(Ljava/lang/String;)V

    .line 2319818
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2319805
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/G6D;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
