.class public final enum LX/Fvp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fvp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fvp;

.field public static final enum CONTENT:LX/Fvp;

.field public static final enum EMPTY:LX/Fvp;

.field private static mValues:[LX/Fvp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2302012
    new-instance v0, LX/Fvp;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v2}, LX/Fvp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvp;->EMPTY:LX/Fvp;

    .line 2302013
    new-instance v0, LX/Fvp;

    const-string v1, "CONTENT"

    invoke-direct {v0, v1, v3}, LX/Fvp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvp;->CONTENT:LX/Fvp;

    .line 2302014
    const/4 v0, 0x2

    new-array v0, v0, [LX/Fvp;

    sget-object v1, LX/Fvp;->EMPTY:LX/Fvp;

    aput-object v1, v0, v2

    sget-object v1, LX/Fvp;->CONTENT:LX/Fvp;

    aput-object v1, v0, v3

    sput-object v0, LX/Fvp;->$VALUES:[LX/Fvp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2302015
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static cachedValues()[LX/Fvp;
    .locals 1

    .prologue
    .line 2302016
    sget-object v0, LX/Fvp;->mValues:[LX/Fvp;

    if-nez v0, :cond_0

    .line 2302017
    invoke-static {}, LX/Fvp;->values()[LX/Fvp;

    move-result-object v0

    sput-object v0, LX/Fvp;->mValues:[LX/Fvp;

    .line 2302018
    :cond_0
    sget-object v0, LX/Fvp;->mValues:[LX/Fvp;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fvp;
    .locals 1

    .prologue
    .line 2302019
    const-class v0, LX/Fvp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fvp;

    return-object v0
.end method

.method public static values()[LX/Fvp;
    .locals 1

    .prologue
    .line 2302020
    sget-object v0, LX/Fvp;->$VALUES:[LX/Fvp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fvp;

    return-object v0
.end method
