.class public final enum LX/FpZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FpZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FpZ;

.field public static final enum ERROR:LX/FpZ;

.field public static final enum SUCCESS:LX/FpZ;

.field public static final enum SUCCESS_NO_DATA:LX/FpZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2291688
    new-instance v0, LX/FpZ;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/FpZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FpZ;->SUCCESS:LX/FpZ;

    .line 2291689
    new-instance v0, LX/FpZ;

    const-string v1, "SUCCESS_NO_DATA"

    invoke-direct {v0, v1, v3}, LX/FpZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FpZ;->SUCCESS_NO_DATA:LX/FpZ;

    .line 2291690
    new-instance v0, LX/FpZ;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/FpZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FpZ;->ERROR:LX/FpZ;

    .line 2291691
    const/4 v0, 0x3

    new-array v0, v0, [LX/FpZ;

    sget-object v1, LX/FpZ;->SUCCESS:LX/FpZ;

    aput-object v1, v0, v2

    sget-object v1, LX/FpZ;->SUCCESS_NO_DATA:LX/FpZ;

    aput-object v1, v0, v3

    sget-object v1, LX/FpZ;->ERROR:LX/FpZ;

    aput-object v1, v0, v4

    sput-object v0, LX/FpZ;->$VALUES:[LX/FpZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2291692
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FpZ;
    .locals 1

    .prologue
    .line 2291693
    const-class v0, LX/FpZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FpZ;

    return-object v0
.end method

.method public static values()[LX/FpZ;
    .locals 1

    .prologue
    .line 2291694
    sget-object v0, LX/FpZ;->$VALUES:[LX/FpZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FpZ;

    return-object v0
.end method
