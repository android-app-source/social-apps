.class public final LX/Fty;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/media/util/model/MediaModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ftz;


# direct methods
.method public constructor <init>(LX/Ftz;)V
    .locals 0

    .prologue
    .line 2299391
    iput-object p1, p0, LX/Fty;->a:LX/Ftz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2299392
    iget-object v1, p0, LX/Fty;->a:LX/Ftz;

    iget-object v0, p0, LX/Fty;->a:LX/Ftz;

    .line 2299393
    iget-object v2, v0, LX/Ftz;->g:LX/0ad;

    sget-short v3, LX/0wf;->A:S

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 2299394
    if-eqz v0, :cond_0

    sget-object v0, LX/Ftz;->d:Ljava/lang/String;

    :goto_0
    const/4 v6, 0x0

    .line 2299395
    iget-object v2, v1, LX/Ftz;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/Ftz;->a:Landroid/net/Uri;

    sget-object v4, LX/Ftz;->b:[Ljava/lang/String;

    const-string v7, "date_added DESC LIMIT 6"

    move-object v5, v0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2299396
    if-nez v2, :cond_2

    .line 2299397
    :goto_1
    move-object v0, v6

    .line 2299398
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2299399
    :goto_2
    return-object v0

    .line 2299400
    :cond_0
    sget-object v0, LX/Ftz;->c:Ljava/lang/String;

    goto :goto_0

    .line 2299401
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_2

    .line 2299402
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    const/4 v3, 0x6

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2299403
    :goto_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2299404
    new-instance v3, Lcom/facebook/media/util/model/MediaModel;

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2299405
    const-string v7, "1"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2299406
    sget-object v7, LX/1po;->PHOTO:LX/1po;

    .line 2299407
    :goto_4
    move-object v5, v7

    .line 2299408
    invoke-direct {v3, v4, v5}, Lcom/facebook/media/util/model/MediaModel;-><init>(Ljava/lang/String;LX/1po;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2299409
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_4
    sget-object v7, LX/1po;->VIDEO:LX/1po;

    goto :goto_4
.end method
