.class public LX/FL0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;",
        "Lcom/facebook/messaging/service/model/SendMessageToPendingThreadResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/FKo;

.field private final b:LX/FKy;


# direct methods
.method public constructor <init>(LX/FKo;LX/FKy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225897
    iput-object p1, p0, LX/FL0;->a:LX/FKo;

    .line 2225898
    iput-object p2, p0, LX/FL0;->b:LX/FKy;

    .line 2225899
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2225900
    check-cast p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;

    .line 2225901
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2225902
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2225903
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2225904
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2225905
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2225906
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2225907
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2225908
    iget-object v1, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v2, LX/5e9;->PENDING_THREAD:LX/5e9;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2225909
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2225910
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2225911
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->b:LX/0Px;

    move-object v1, v1

    .line 2225912
    invoke-static {v1}, LX/FKo;->a(Ljava/util/List;)LX/0lF;

    move-result-object v1

    .line 2225913
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "to"

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225914
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "use_existing_group"

    const-string v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225915
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v1

    .line 2225916
    invoke-static {v0, v2}, LX/FKy;->a(Ljava/util/List;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2225917
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "sendMessageToPendingThread"

    .line 2225918
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2225919
    move-object v1, v1

    .line 2225920
    const-string v2, "POST"

    .line 2225921
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2225922
    move-object v1, v1

    .line 2225923
    const-string v2, "/threads"

    .line 2225924
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2225925
    move-object v1, v1

    .line 2225926
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2225927
    move-object v0, v1

    .line 2225928
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2225929
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225930
    move-object v0, v0

    .line 2225931
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2225932
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2225933
    const-string v1, "thread_fbid"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v0

    .line 2225934
    new-instance v2, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadResult;

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadResult;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    return-object v2
.end method
