.class public final LX/F8h;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/F8P;

.field public final synthetic b:LX/F8j;


# direct methods
.method public constructor <init>(LX/F8j;LX/F8P;)V
    .locals 0

    .prologue
    .line 2204463
    iput-object p1, p0, LX/F8h;->b:LX/F8j;

    iput-object p2, p0, LX/F8h;->a:LX/F8P;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2204464
    iget-object v0, p0, LX/F8h;->b:LX/F8j;

    iget-object v0, v0, LX/F8j;->c:LX/F8S;

    iget-object v1, p0, LX/F8h;->a:LX/F8P;

    invoke-virtual {v0, v1}, LX/F8S;->a(LX/F8P;)V

    .line 2204465
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2204466
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2204467
    iget-object v0, p0, LX/F8h;->b:LX/F8j;

    iget-object v0, v0, LX/F8j;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2204468
    return-void
.end method
