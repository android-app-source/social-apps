.class public LX/Fk1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/FQU;

.field public final b:LX/FQV;

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0lF;)V
    .locals 11

    .prologue
    .line 2278056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278057
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 2278058
    const-string v1, "release_package"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 2278059
    const-string v2, "release_number"

    invoke-virtual {v0, v2}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2278060
    const-string v3, "application_name"

    invoke-virtual {v0, v3}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 2278061
    const-string v4, "application_version"

    invoke-virtual {v0, v4}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 2278062
    const-string v5, "download_url"

    invoke-virtual {v0, v5}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 2278063
    const-string v6, "allowed_networks"

    invoke-virtual {v0, v6}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 2278064
    const-string v7, "client_action"

    invoke-virtual {v0, v7}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 2278065
    const-string v8, "release_notes"

    invoke-virtual {v0, v8}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 2278066
    const-string v9, "file_mime_type"

    invoke-virtual {v0, v9}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v9

    .line 2278067
    const-string v10, "file_size"

    invoke-virtual {v0, v10}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2278068
    invoke-static {v7}, LX/16N;->d(LX/0lF;)I

    move-result v7

    invoke-static {v7}, LX/FQU;->fromInt(I)LX/FQU;

    move-result-object v7

    iput-object v7, p0, LX/Fk1;->a:LX/FQU;

    .line 2278069
    invoke-static {v6}, LX/16N;->d(LX/0lF;)I

    move-result v6

    invoke-static {v6}, LX/FQV;->fromInt(I)LX/FQV;

    move-result-object v6

    iput-object v6, p0, LX/Fk1;->b:LX/FQV;

    .line 2278070
    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, LX/Fk1;->c:Ljava/lang/String;

    .line 2278071
    invoke-static {v0}, LX/16N;->c(LX/0lF;)J

    move-result-wide v6

    iput-wide v6, p0, LX/Fk1;->d:J

    .line 2278072
    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fk1;->e:Ljava/lang/String;

    .line 2278073
    invoke-static {v2}, LX/16N;->d(LX/0lF;)I

    move-result v0

    iput v0, p0, LX/Fk1;->f:I

    .line 2278074
    invoke-static {v8}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fk1;->g:Ljava/lang/String;

    .line 2278075
    invoke-static {v3}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fk1;->h:Ljava/lang/String;

    .line 2278076
    invoke-static {v4}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fk1;->i:Ljava/lang/String;

    .line 2278077
    invoke-static {v9}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fk1;->j:Ljava/lang/String;

    .line 2278078
    return-void
.end method
