.class public final LX/G6q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

.field public final b:Landroid/widget/EditText;

.field private final c:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;Landroid/widget/EditText;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 2320790
    iput-object p1, p0, LX/G6q;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2320791
    iput-object p2, p0, LX/G6q;->b:Landroid/widget/EditText;

    .line 2320792
    iput-object p3, p0, LX/G6q;->c:Landroid/widget/EditText;

    .line 2320793
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2320789
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2320788
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2320777
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, LX/G6q;->b:Landroid/widget/EditText;

    if-nez v0, :cond_0

    .line 2320778
    iget-object v0, p0, LX/G6q;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget-object v0, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->j:LX/EiK;

    if-eqz v0, :cond_0

    .line 2320779
    iget-object v0, p0, LX/G6q;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget-object v0, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->j:LX/EiK;

    iget-object v1, p0, LX/G6q;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-static {v1}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->getCodeText(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/EiK;->a(Ljava/lang/String;)V

    .line 2320780
    iget-object v0, p0, LX/G6q;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-static {v0, v3}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->setCodeEnabled(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;Z)V

    .line 2320781
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, LX/G6q;->b:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    .line 2320782
    iget-object v0, p0, LX/G6q;->b:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2320783
    iget-object v0, p0, LX/G6q;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 2320784
    iget-object v0, p0, LX/G6q;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 2320785
    iget-object v0, p0, LX/G6q;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget-object v0, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView$DigitWatcher$1;

    invoke-direct {v1, p0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView$DigitWatcher$1;-><init>(LX/G6q;)V

    const v2, -0x45fe1fdc

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2320786
    iget-object v0, p0, LX/G6q;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-static {v0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->j(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)I

    .line 2320787
    :cond_1
    return-void
.end method
