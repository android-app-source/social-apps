.class public LX/FiC;
.super LX/Fi8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274675
    invoke-direct {p0}, LX/Fi8;-><init>()V

    .line 2274676
    return-void
.end method

.method public static a(LX/0QB;)LX/FiC;
    .locals 3

    .prologue
    .line 2274677
    const-class v1, LX/FiC;

    monitor-enter v1

    .line 2274678
    :try_start_0
    sget-object v0, LX/FiC;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2274679
    sput-object v2, LX/FiC;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2274680
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274681
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2274682
    new-instance v0, LX/FiC;

    invoke-direct {v0}, LX/FiC;-><init>()V

    .line 2274683
    move-object v0, v0

    .line 2274684
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2274685
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FiC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2274686
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2274687
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2274673
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2274674
    return-object v0
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 1

    .prologue
    .line 2274672
    const/4 v0, 0x0

    return-object v0
.end method
