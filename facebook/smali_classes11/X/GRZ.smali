.class public LX/GRZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryInterfaces$AppInviteFields;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryInterfaces$AppInviteFields;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2351596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351597
    const/4 v0, 0x0

    iput v0, p0, LX/GRZ;->a:I

    .line 2351598
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/GRZ;->b:Ljava/util/ArrayList;

    .line 2351599
    return-void
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 2351600
    iget-object v0, p0, LX/GRZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;
    .locals 1

    .prologue
    .line 2351601
    iget-object v0, p0, LX/GRZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryInterfaces$AppInviteFields;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2351602
    new-instance v0, LX/GRY;

    invoke-direct {v0, p0}, LX/GRY;-><init>(LX/GRZ;)V

    return-object v0
.end method
