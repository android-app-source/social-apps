.class public final LX/GDv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/lang/Object;",
        "LX/0Px",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8wL;

.field public final synthetic b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public final synthetic c:LX/GDy;


# direct methods
.method public constructor <init>(LX/GDy;LX/8wL;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2330631
    iput-object p1, p0, LX/GDv;->c:LX/GDy;

    iput-object p2, p0, LX/GDv;->a:LX/8wL;

    iput-object p3, p0, LX/GDv;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2330632
    const/4 v0, 0x0

    .line 2330633
    :try_start_0
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    iget-object v2, p0, LX/GDv;->a:LX/8wL;

    .line 2330634
    invoke-static {p1, v2}, LX/GDy;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/8wL;)LX/1vs;

    move-result-object v3

    move-object v1, v3

    .line 2330635
    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v3, v1, LX/1vs;->b:I

    .line 2330636
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2330637
    :try_start_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v0

    .line 2330638
    :goto_0
    const/4 v0, 0x1

    const v5, -0x2d0af161

    invoke-static {v2, v3, v0, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2330639
    const/4 v0, 0x1

    const v5, -0x2d0af161

    invoke-static {v2, v3, v0, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v6, 0x0

    const-class v7, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {v5, v0, v6, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 2330640
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2330641
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 2330642
    :catch_0
    move-exception v0

    .line 2330643
    iget-object v1, p0, LX/GDv;->c:LX/GDy;

    iget-object v1, v1, LX/GDy;->c:LX/2U3;

    const-class v2, LX/GDy;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fetchAvailableAudiences failed for objective "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/GDv;->a:LX/8wL;

    invoke-virtual {v4}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2330644
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2330645
    :goto_3
    return-object v0

    .line 2330646
    :cond_0
    :try_start_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 2330647
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2

    .line 2330648
    :cond_2
    iget-object v0, p0, LX/GDv;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const/4 v1, 0x0

    invoke-virtual {v2, v3, v1}, LX/15i;->j(II)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d(I)V

    .line 2330649
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v0

    goto :goto_3
.end method
