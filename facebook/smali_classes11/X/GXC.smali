.class public final LX/GXC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public final synthetic a:LX/GXD;

.field private final b:I

.field private c:Z

.field private final d:LX/GXK;


# direct methods
.method public constructor <init>(LX/GXD;ILX/GXK;)V
    .locals 0

    .prologue
    .line 2363238
    iput-object p1, p0, LX/GXC;->a:LX/GXD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2363239
    iput p2, p0, LX/GXC;->b:I

    .line 2363240
    iput-object p3, p0, LX/GXC;->d:LX/GXK;

    .line 2363241
    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2363136
    iget-boolean v0, p0, LX/GXC;->c:Z

    if-eqz v0, :cond_1

    .line 2363137
    iget-object v1, p0, LX/GXC;->d:LX/GXK;

    iget v2, p0, LX/GXC;->b:I

    add-int/lit8 v3, p3, -0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2363138
    iget-object v4, v1, LX/GXK;->l:LX/0Px;

    move-object v4, v4

    .line 2363139
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    move v4, v5

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 2363140
    iget-object v4, v1, LX/GXK;->l:LX/0Px;

    move-object v4, v4

    .line 2363141
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GXM;

    .line 2363142
    iget-object v0, v4, LX/GXM;->b:LX/0Px;

    move-object v4, v0

    .line 2363143
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    :goto_1
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 2363144
    iget-object v4, v1, LX/GXK;->l:LX/0Px;

    move-object v4, v4

    .line 2363145
    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GXM;

    .line 2363146
    iget-object v5, v4, LX/GXM;->d:LX/0am;

    move-object v5, v5

    .line 2363147
    const/4 v4, -0x1

    if-ne v3, v4, :cond_4

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    .line 2363148
    :goto_2
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v6

    if-nez v6, :cond_5

    .line 2363149
    :cond_0
    :goto_3
    move-object v0, v1

    .line 2363150
    iget-object v1, p0, LX/GXC;->a:LX/GXD;

    invoke-static {v1, v0}, LX/GXD;->b(LX/GXD;LX/GXK;)V

    .line 2363151
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GXC;->c:Z

    .line 2363152
    :cond_1
    return-void

    :cond_2
    move v4, v6

    .line 2363153
    goto :goto_0

    :cond_3
    move v5, v6

    .line 2363154
    goto :goto_1

    .line 2363155
    :cond_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    goto :goto_2

    .line 2363156
    :cond_5
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    if-eq v5, v6, :cond_0

    .line 2363157
    :cond_6
    new-instance v5, LX/GXI;

    invoke-direct {v5}, LX/GXI;-><init>()V

    .line 2363158
    iget-object v6, v1, LX/GXK;->a:LX/0am;

    move-object v6, v6

    .line 2363159
    iput-object v6, v5, LX/GXI;->a:LX/0am;

    .line 2363160
    move-object v5, v5

    .line 2363161
    iget-object v6, v1, LX/GXK;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2363162
    iput-object v6, v5, LX/GXI;->b:Ljava/lang/String;

    .line 2363163
    move-object v5, v5

    .line 2363164
    iget-object v6, v1, LX/GXK;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2363165
    iput-object v6, v5, LX/GXI;->c:Ljava/lang/String;

    .line 2363166
    move-object v5, v5

    .line 2363167
    iget-object v6, v1, LX/GXK;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2363168
    iput-object v6, v5, LX/GXI;->d:Ljava/lang/String;

    .line 2363169
    move-object v5, v5

    .line 2363170
    iget-boolean v6, v1, LX/GXK;->e:Z

    move v6, v6

    .line 2363171
    iput-boolean v6, v5, LX/GXI;->e:Z

    .line 2363172
    move-object v5, v5

    .line 2363173
    iget-object v6, v1, LX/GXK;->f:LX/0Px;

    move-object v6, v6

    .line 2363174
    iput-object v6, v5, LX/GXI;->f:LX/0Px;

    .line 2363175
    move-object v5, v5

    .line 2363176
    iget-object v6, v1, LX/GXK;->g:Ljava/lang/String;

    move-object v6, v6

    .line 2363177
    iput-object v6, v5, LX/GXI;->g:Ljava/lang/String;

    .line 2363178
    move-object v5, v5

    .line 2363179
    iget-object v6, v1, LX/GXK;->h:LX/0am;

    move-object v6, v6

    .line 2363180
    iput-object v6, v5, LX/GXI;->h:LX/0am;

    .line 2363181
    move-object v5, v5

    .line 2363182
    iget-object v6, v1, LX/GXK;->i:LX/0am;

    move-object v6, v6

    .line 2363183
    iput-object v6, v5, LX/GXI;->i:LX/0am;

    .line 2363184
    move-object v5, v5

    .line 2363185
    iget-boolean v6, v1, LX/GXK;->j:Z

    move v6, v6

    .line 2363186
    iput-boolean v6, v5, LX/GXI;->j:Z

    .line 2363187
    move-object v5, v5

    .line 2363188
    iget-boolean v6, v1, LX/GXK;->k:Z

    move v6, v6

    .line 2363189
    iput-boolean v6, v5, LX/GXI;->k:Z

    .line 2363190
    move-object v5, v5

    .line 2363191
    iget-object v6, v1, LX/GXK;->l:LX/0Px;

    move-object v6, v6

    .line 2363192
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    .line 2363193
    iput-object v6, v5, LX/GXI;->l:LX/0Px;

    .line 2363194
    move-object v5, v5

    .line 2363195
    iget-object v6, v1, LX/GXK;->m:LX/0am;

    move-object v6, v6

    .line 2363196
    iput-object v6, v5, LX/GXI;->m:LX/0am;

    .line 2363197
    move-object v5, v5

    .line 2363198
    iget-boolean v6, v1, LX/GXK;->n:Z

    move v6, v6

    .line 2363199
    iput-boolean v6, v5, LX/GXI;->n:Z

    .line 2363200
    move-object v5, v5

    .line 2363201
    iget-object v6, v1, LX/GXK;->o:LX/0Px;

    move-object v6, v6

    .line 2363202
    iput-object v6, v5, LX/GXI;->o:LX/0Px;

    .line 2363203
    move-object v5, v5

    .line 2363204
    iget-object v6, v1, LX/GXK;->p:LX/GXJ;

    move-object v6, v6

    .line 2363205
    iput-object v6, v5, LX/GXI;->p:LX/GXJ;

    .line 2363206
    move-object v5, v5

    .line 2363207
    iget-object v6, v1, LX/GXK;->q:Ljava/lang/String;

    move-object v6, v6

    .line 2363208
    iput-object v6, v5, LX/GXI;->q:Ljava/lang/String;

    .line 2363209
    move-object v5, v5

    .line 2363210
    iget-boolean v6, v1, LX/GXK;->r:Z

    move v6, v6

    .line 2363211
    iput-boolean v6, v5, LX/GXI;->r:Z

    .line 2363212
    move-object v5, v5

    .line 2363213
    iget-object v6, v1, LX/GXK;->s:LX/0am;

    move-object v6, v6

    .line 2363214
    iput-object v6, v5, LX/GXI;->s:LX/0am;

    .line 2363215
    move-object v5, v5

    .line 2363216
    iget-object v6, v1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v6, v6

    .line 2363217
    iput-object v6, v5, LX/GXI;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2363218
    move-object v5, v5

    .line 2363219
    move-object v5, v5

    .line 2363220
    iget-object v6, v1, LX/GXK;->l:LX/0Px;

    move-object v6, v6

    .line 2363221
    new-instance p3, LX/0Pz;

    invoke-direct {p3}, LX/0Pz;-><init>()V

    .line 2363222
    const/4 p1, 0x0

    move p2, p1

    :goto_4
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    if-ge p2, p1, :cond_8

    .line 2363223
    if-eq p2, v2, :cond_7

    .line 2363224
    invoke-virtual {v6, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p3, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2363225
    :goto_5
    add-int/lit8 p1, p2, 0x1

    move p2, p1

    goto :goto_4

    .line 2363226
    :cond_7
    invoke-virtual {v6, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/GXM;

    .line 2363227
    new-instance p4, LX/GXM;

    .line 2363228
    iget-object p5, p1, LX/GXM;->a:Ljava/lang/String;

    move-object p5, p5

    .line 2363229
    iget-object v0, p1, LX/GXM;->b:LX/0Px;

    move-object v0, v0

    .line 2363230
    iget-object v3, p1, LX/GXM;->c:LX/0Px;

    move-object p1, v3

    .line 2363231
    invoke-direct {p4, p5, v0, p1, v4}, LX/GXM;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0am;)V

    invoke-virtual {p3, p4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_5

    .line 2363232
    :cond_8
    invoke-virtual {p3}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    move-object v4, p1

    .line 2363233
    iget-object v6, v1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v6, v6

    .line 2363234
    invoke-virtual {v6}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v6

    invoke-static {v4, v6}, LX/GXH;->a(LX/0Px;LX/0Px;)LX/0Px;

    move-result-object v4

    .line 2363235
    iget-object v6, v1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v6, v6

    .line 2363236
    invoke-static {v5, v4, v6}, LX/GXH;->a(LX/GXI;LX/0Px;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    .line 2363237
    invoke-virtual {v5}, LX/GXI;->a()LX/GXK;

    move-result-object v1

    goto/16 :goto_3
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2363135
    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2363133
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GXC;->c:Z

    .line 2363134
    const/4 v0, 0x0

    return v0
.end method
