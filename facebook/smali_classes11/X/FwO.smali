.class public LX/FwO;
.super LX/1cC;
.source ""


# instance fields
.field public final a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

.field private final b:LX/FwQ;

.field public final c:LX/BQ1;


# direct methods
.method public constructor <init>(Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;LX/FwQ;LX/BQ1;)V
    .locals 0
    .param p1    # Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/FwQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303068
    invoke-direct {p0}, LX/1cC;-><init>()V

    .line 2303069
    iput-object p1, p0, LX/FwO;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    .line 2303070
    iput-object p2, p0, LX/FwO;->b:LX/FwQ;

    .line 2303071
    iput-object p3, p0, LX/FwO;->c:LX/BQ1;

    .line 2303072
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2303073
    iget-object v0, p0, LX/FwO;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2303074
    iget-object v0, p0, LX/FwO;->b:LX/FwQ;

    .line 2303075
    const/4 p0, 0x1

    invoke-static {v0, p0}, LX/FwQ;->a(LX/FwQ;I)V

    .line 2303076
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303077
    check-cast p2, LX/1ln;

    .line 2303078
    iget-object v0, p0, LX/FwO;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2303079
    const/4 v0, 0x0

    .line 2303080
    iget-object p1, p0, LX/FwO;->c:LX/BQ1;

    if-eqz p1, :cond_1

    .line 2303081
    if-eqz p2, :cond_3

    invoke-virtual {p2}, LX/1ln;->g()I

    move-result p1

    .line 2303082
    :goto_0
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v0

    .line 2303083
    :cond_0
    iget-object p3, p0, LX/FwO;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p3}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getWidth()I

    move-result p3

    if-gt p3, p1, :cond_4

    iget-object p1, p0, LX/FwO;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p1}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->getHeight()I

    move-result p1

    if-gt p1, v0, :cond_4

    sget-object v0, LX/BQ6;->FULL:LX/BQ6;

    .line 2303084
    :goto_1
    iget-object p1, p0, LX/FwO;->c:LX/BQ1;

    .line 2303085
    iput-object v0, p1, LX/BQ1;->j:LX/BQ6;

    .line 2303086
    :cond_1
    iget-object v0, p0, LX/FwO;->b:LX/FwQ;

    .line 2303087
    const/4 p0, 0x1

    invoke-static {v0, p0}, LX/FwQ;->b(LX/FwQ;I)V

    .line 2303088
    :cond_2
    return-void

    :cond_3
    move p1, v0

    .line 2303089
    goto :goto_0

    .line 2303090
    :cond_4
    sget-object v0, LX/BQ6;->SCALED_UP:LX/BQ6;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2303091
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303092
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2303093
    iget-object v0, p0, LX/FwO;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2303094
    iget-object v0, p0, LX/FwO;->b:LX/FwQ;

    .line 2303095
    const/4 p0, 0x1

    invoke-static {v0, p0}, LX/FwQ;->d(LX/FwQ;I)V

    .line 2303096
    :cond_0
    return-void
.end method
