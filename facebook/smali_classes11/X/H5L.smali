.class public LX/H5L;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/2kk;",
        ":",
        "LX/2kl;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/H5L",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424131
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2424132
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/H5L;->b:LX/0Zi;

    .line 2424133
    iput-object p1, p0, LX/H5L;->a:LX/0Ot;

    .line 2424134
    return-void
.end method

.method public static a(LX/0QB;)LX/H5L;
    .locals 4

    .prologue
    .line 2424185
    const-class v1, LX/H5L;

    monitor-enter v1

    .line 2424186
    :try_start_0
    sget-object v0, LX/H5L;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424187
    sput-object v2, LX/H5L;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424188
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424189
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424190
    new-instance v3, LX/H5L;

    const/16 p0, 0x2ad8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/H5L;-><init>(LX/0Ot;)V

    .line 2424191
    move-object v0, v3

    .line 2424192
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424193
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424194
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424195
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2424184
    const v0, 0x9687c6f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2424169
    check-cast p2, LX/H5K;

    .line 2424170
    iget-object v0, p0, LX/H5L;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;

    iget-object v1, p2, LX/H5K;->a:LX/2nq;

    const/4 p2, 0x2

    const/4 p0, 0x0

    .line 2424171
    invoke-interface {v1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 2424172
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 2424173
    const/4 v2, 0x0

    .line 2424174
    invoke-static {v5}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 2424175
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2424176
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2424177
    :goto_0
    iget-object v2, v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    .line 2424178
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    if-eqz v6, :cond_0

    const v4, 0x7f0a004f

    :goto_1
    invoke-interface {v7, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    .line 2424179
    const v7, 0x9687c6f

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2424180
    invoke-interface {v4, v7}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    .line 2424181
    const v7, 0x14fc168b

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2424182
    invoke-interface {v4, v7}, LX/1Dh;->e(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v7

    invoke-virtual {v2, v3}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v3

    sget-object v8, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v3

    invoke-virtual {v3}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v3

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v7

    const v8, 0x7f0a010a

    invoke-virtual {v7, v8}, LX/1nh;->i(I)LX/1nh;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/1up;->a(LX/1n6;)LX/1up;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v7, 0x7f0b0a8b

    invoke-interface {v3, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v7, 0x7f0b0a8b

    invoke-interface {v3, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/4 v7, 0x5

    const v8, 0x7f0b0a8c

    invoke-interface {v3, v7, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v4, v7}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    const v7, 0x7f0e05ec

    invoke-static {p1, p0, v7}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v7

    iget-object v8, v0, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->c:LX/3Cm;

    sget-object v9, LX/3DG;->JEWEL:LX/3DG;

    invoke-virtual {v8, v5, v9}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Landroid/text/Spannable;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-static {v0, p1, v1, v6, v2}, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->a(Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;LX/1De;LX/2nq;ZLX/1Ad;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0a6e

    invoke-interface {v2, p0, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0a6e

    invoke-interface {v2, p2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    const v4, 0x7f0b0a89

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b0a89

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b0a69

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2424183
    return-object v0

    :cond_0
    const v4, 0x7f0a0480

    goto/16 :goto_1

    :cond_1
    move-object v3, v2

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2424135
    invoke-static {}, LX/1dS;->b()V

    .line 2424136
    iget v1, p1, LX/1dQ;->b:I

    .line 2424137
    sparse-switch v1, :sswitch_data_0

    .line 2424138
    :goto_0
    return-object v0

    .line 2424139
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2424140
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 2424141
    check-cast v2, LX/H5K;

    .line 2424142
    iget-object v3, p0, LX/H5L;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;

    iget-object p1, v2, LX/H5K;->a:LX/2nq;

    iget-object p2, v2, LX/H5K;->b:LX/1Pn;

    .line 2424143
    check-cast p2, LX/2kk;

    invoke-interface {p2, p1}, LX/2kk;->b(LX/2nq;)Landroid/view/View$OnClickListener;

    move-result-object p0

    invoke-interface {p0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2424144
    goto :goto_0

    .line 2424145
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2424146
    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 2424147
    check-cast v2, LX/H5K;

    .line 2424148
    iget-object v3, p0, LX/H5L;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v3, v2, LX/H5K;->a:LX/2nq;

    iget-object v4, v2, LX/H5K;->b:LX/1Pn;

    const/4 p0, 0x1

    const/4 v1, 0x0

    .line 2424149
    move-object p1, v4

    check-cast p1, LX/1Pr;

    new-instance p2, LX/3D5;

    invoke-interface {v3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p2, v2}, LX/3D5;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-interface {p1, p2, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/3D6;

    .line 2424150
    iget-object p2, p1, LX/3D6;->a:LX/03R;

    move-object p2, p2

    .line 2424151
    sget-object v2, LX/03R;->UNSET:LX/03R;

    invoke-virtual {p2, v2}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_0

    move p2, p0

    :goto_1
    invoke-static {p2}, LX/0PB;->checkArgument(Z)V

    .line 2424152
    iget-object p2, p1, LX/3D6;->a:LX/03R;

    move-object p2, p2

    .line 2424153
    invoke-virtual {p2}, LX/03R;->asBoolean()Z

    move-result p2

    if-nez p2, :cond_1

    .line 2424154
    :goto_2
    invoke-static {p0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object p2

    .line 2424155
    iput-object p2, p1, LX/3D6;->a:LX/03R;

    .line 2424156
    move-object p1, v4

    .line 2424157
    check-cast p1, LX/2kk;

    invoke-interface {p1, v3, p0}, LX/2kk;->a(LX/2nq;Z)Z

    .line 2424158
    check-cast v4, LX/2kl;

    invoke-interface {v4}, LX/2kl;->md_()LX/2jc;

    move-result-object p2

    invoke-interface {v3}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz p0, :cond_2

    sget-object p1, LX/Cfc;->COLLAPSE_RICH_ATTACHMENT_TAP:LX/Cfc;

    invoke-virtual {p1}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object p1

    :goto_3
    invoke-interface {p2, v1, p1}, LX/2jc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    .line 2424159
    goto/16 :goto_0

    .line 2424160
    :sswitch_2
    check-cast p2, LX/48C;

    .line 2424161
    iget-object v0, p2, LX/48C;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2424162
    check-cast v1, LX/H5K;

    .line 2424163
    iget-object v2, p0, LX/H5L;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;

    iget-object v3, v1, LX/H5K;->a:LX/2nq;

    iget-object v4, v1, LX/H5K;->b:LX/1Pn;

    invoke-virtual {v2, v0, v3, v4}, Lcom/facebook/notifications/multirow/components/NotificationsComponentSpec;->a(Landroid/view/View;LX/2nq;LX/1Pn;)Z

    move-result v2

    .line 2424164
    move v0, v2

    .line 2424165
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    :cond_0
    move p2, v1

    .line 2424166
    goto :goto_1

    :cond_1
    move p0, v1

    .line 2424167
    goto :goto_2

    .line 2424168
    :cond_2
    sget-object p1, LX/Cfc;->EXPAND_RICH_ATTACHMENT_TAP:LX/Cfc;

    invoke-virtual {p1}, LX/Cfc;->name()Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x9687c6f -> :sswitch_0
        0x972d635 -> :sswitch_1
        0x14fc168b -> :sswitch_2
    .end sparse-switch
.end method
