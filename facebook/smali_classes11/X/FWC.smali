.class public LX/FWC;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/FVN;
.implements LX/2ht;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:LX/FWK;

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/FVs;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/FWA;

.field public f:LX/FWB;

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/FWF;

.field public i:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/view/LayoutInflater;LX/FWK;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2251771
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2251772
    sget-object v0, LX/FWA;->NONE:LX/FWA;

    iput-object v0, p0, LX/FWC;->e:LX/FWA;

    .line 2251773
    iput-object p1, p0, LX/FWC;->a:Landroid/content/res/Resources;

    .line 2251774
    iput-object p2, p0, LX/FWC;->b:Landroid/view/LayoutInflater;

    .line 2251775
    iput-object p3, p0, LX/FWC;->c:LX/FWK;

    .line 2251776
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    .line 2251777
    sget-object v0, LX/FWB;->IDLE:LX/FWB;

    iput-object v0, p0, LX/FWC;->f:LX/FWB;

    .line 2251778
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FWC;->g:Ljava/util/ArrayList;

    .line 2251779
    return-void
.end method

.method public static a(LX/FWA;LX/FWA;)LX/FWA;
    .locals 2

    .prologue
    .line 2251765
    sget-object v0, LX/FW9;->c:[I

    invoke-virtual {p1}, LX/FWA;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2251766
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No rule is specified for merging %s and %s"

    invoke-static {v1, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2251767
    :pswitch_0
    sget-object v0, LX/FWA;->FROM_CACHE:LX/FWA;

    if-ne p0, v0, :cond_0

    sget-object v0, LX/FWA;->FROM_CACHE:LX/FWA;

    .line 2251768
    :goto_0
    return-object v0

    .line 2251769
    :cond_0
    sget-object v0, LX/FWA;->FROM_SERVER:LX/FWA;

    goto :goto_0

    .line 2251770
    :pswitch_1
    sget-object v0, LX/FWA;->FROM_CACHE:LX/FWA;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/FWC;Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/FVs;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2251760
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    .line 2251761
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVs;

    invoke-interface {v0}, LX/FVs;->a()LX/FV0;

    move-result-object v0

    sget-object v1, LX/FV0;->SAVED_DASHBOARD_LIST_SECTION_HEADER:LX/FV0;

    if-ne v0, v1, :cond_0

    .line 2251762
    iget-object v0, p0, LX/FWC;->g:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2251763
    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 2251764
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2251753
    sget-object v0, LX/FW9;->b:[I

    invoke-static {p1}, LX/FV0;->fromOrdinal(I)LX/FV0;

    move-result-object v1

    invoke-virtual {v1}, LX/FV0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2251754
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected itemViewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2251755
    :pswitch_0
    iget-object v0, p0, LX/FWC;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03125c

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2251756
    :goto_0
    return-object v0

    .line 2251757
    :pswitch_1
    iget-object v0, p0, LX/FWC;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03125b

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2251758
    :pswitch_2
    iget-object v0, p0, LX/FWC;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03125a

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2251759
    :pswitch_3
    iget-object v0, p0, LX/FWC;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f031259

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2251744
    sget-object v0, LX/FW9;->b:[I

    invoke-static {p4}, LX/FV0;->fromOrdinal(I)LX/FV0;

    move-result-object v1

    invoke-virtual {v1}, LX/FV0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2251745
    :goto_0
    :pswitch_0
    return-void

    .line 2251746
    :pswitch_1
    check-cast p3, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;

    check-cast p2, LX/FVx;

    invoke-virtual {p3, p2}, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;->a(LX/FVx;)V

    goto :goto_0

    .line 2251747
    :pswitch_2
    check-cast p3, Lcom/facebook/saved/views/SavedDashboardSavedItemView;

    check-cast p2, LX/FVt;

    .line 2251748
    invoke-virtual {p3, p2}, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->a(LX/FVt;)V

    .line 2251749
    iget-object v0, p0, LX/FWC;->h:LX/FWF;

    .line 2251750
    iput-object v0, p3, Lcom/facebook/saved/views/SavedDashboardSavedItemView;->e:LX/FWF;

    .line 2251751
    goto :goto_0

    .line 2251752
    :pswitch_3
    iget-object v0, p0, LX/FWC;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/FVt;)V
    .locals 2

    .prologue
    .line 2251739
    iget-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2251740
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 2251741
    :goto_0
    return-void

    .line 2251742
    :cond_0
    iget-object v1, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2251743
    const v0, 0xa4d703

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251727
    if-nez p2, :cond_2

    .line 2251728
    sget-object v0, LX/FV0;->SAVED_DASHBOARD_LIST_SECTION_HEADER:LX/FV0;

    invoke-virtual {v0}, LX/FV0;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0, p3}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2251729
    :goto_0
    const/4 v2, -0x1

    .line 2251730
    iget-object v0, p0, LX/FWC;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p2

    const/4 v0, 0x0

    move v3, v2

    move v2, v0

    :goto_1
    if-ge v2, p2, :cond_0

    iget-object v0, p0, LX/FWC;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2251731
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p3

    if-lt p1, p3, :cond_0

    .line 2251732
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2251733
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2251734
    :cond_0
    move v2, v3

    .line 2251735
    if-gez v2, :cond_1

    .line 2251736
    const/4 v1, 0x0

    .line 2251737
    :goto_2
    return-object v1

    :cond_1
    move-object v0, v1

    .line 2251738
    check-cast v0, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;

    invoke-virtual {p0, v2}, LX/FWC;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FVx;

    invoke-virtual {v0, v2}, Lcom/facebook/saved/views/SavedDashboardSectionHeaderView;->a(LX/FVx;)V

    goto :goto_2

    :cond_2
    move-object v1, p2

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2251724
    sget-object v0, LX/FWB;->LOADING_MORE:LX/FWB;

    iput-object v0, p0, LX/FWC;->f:LX/FWB;

    .line 2251725
    const v0, -0xdfe328d

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2251726
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2251721
    sget-object v0, LX/FWB;->IDLE:LX/FWB;

    iput-object v0, p0, LX/FWC;->f:LX/FWB;

    .line 2251722
    const v0, -0x79544ff4

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2251723
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2251720
    iget-object v0, p0, LX/FWC;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a05e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 2

    .prologue
    .line 2251780
    iget-object v0, p0, LX/FWC;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b115b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2251681
    sget-object v0, LX/FWB;->IDLE:LX/FWB;

    iput-object v0, p0, LX/FWC;->f:LX/FWB;

    .line 2251682
    const v0, -0x27e99679

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2251683
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2251684
    sget-object v0, LX/FWB;->LOAD_MORE_FAILED:LX/FWB;

    iput-object v0, p0, LX/FWC;->f:LX/FWB;

    .line 2251685
    const v0, 0x3b5f28fe

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2251686
    return-void
.end method

.method public final f(I)Z
    .locals 3

    .prologue
    .line 2251687
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/FWC;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt p1, v0, :cond_1

    .line 2251688
    :cond_0
    const/4 v0, 0x0

    .line 2251689
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x0

    .line 2251690
    iget-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    move v0, v1

    .line 2251691
    :goto_1
    move v0, v0

    .line 2251692
    goto :goto_0

    :cond_2
    iget-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVs;

    invoke-interface {v0}, LX/FVs;->a()LX/FV0;

    move-result-object v0

    sget-object v2, LX/FV0;->SAVED_DASHBOARD_LIST_SECTION_HEADER:LX/FV0;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2251693
    sget-object v0, LX/FWB;->IDLE:LX/FWB;

    iput-object v0, p0, LX/FWC;->f:LX/FWB;

    .line 2251694
    const v0, -0x2c6c4fc0

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2251695
    return-void
.end method

.method public final getCount()I
    .locals 3

    .prologue
    .line 2251696
    iget-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 2251697
    sget-object v0, LX/FWB;->LOADING_MORE:LX/FWB;

    iget-object v2, p0, LX/FWC;->f:LX/FWB;

    invoke-virtual {v0, v2}, LX/FWB;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/FWB;->LOAD_MORE_FAILED:LX/FWB;

    iget-object v2, p0, LX/FWC;->f:LX/FWB;

    invoke-virtual {v0, v2}, LX/FWB;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2251698
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2251699
    iget-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2251700
    iget-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2251701
    :goto_0
    return-object v0

    .line 2251702
    :cond_0
    sget-object v0, LX/FW9;->a:[I

    iget-object v1, p0, LX/FWC;->f:LX/FWB;

    invoke-virtual {v1}, LX/FWB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2251703
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid position for current list state. Position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/FWC;->f:LX/FWB;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2251704
    :pswitch_0
    sget-object v0, LX/FV0;->SAVED_DASHBOARD_LOAD_MORE:LX/FV0;

    goto :goto_0

    .line 2251705
    :pswitch_1
    sget-object v0, LX/FV0;->SAVED_DASHBOARD_LOAD_MORE_FAILED:LX/FV0;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2251706
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 2251707
    iget-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2251708
    iget-object v0, p0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVs;

    invoke-interface {v0}, LX/FVs;->a()LX/FV0;

    move-result-object v0

    invoke-virtual {v0}, LX/FV0;->ordinal()I

    move-result v0

    .line 2251709
    :goto_0
    return v0

    .line 2251710
    :cond_0
    sget-object v0, LX/FW9;->a:[I

    iget-object v1, p0, LX/FWC;->f:LX/FWB;

    invoke-virtual {v1}, LX/FWB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2251711
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid position for current list state. Position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/FWC;->f:LX/FWB;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2251712
    :pswitch_0
    sget-object v0, LX/FV0;->SAVED_DASHBOARD_LOAD_MORE:LX/FV0;

    invoke-virtual {v0}, LX/FV0;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2251713
    :pswitch_1
    sget-object v0, LX/FV0;->SAVED_DASHBOARD_LOAD_MORE_FAILED:LX/FV0;

    invoke-virtual {v0}, LX/FV0;->ordinal()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2251714
    invoke-static {}, LX/FV0;->values()[LX/FV0;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 2251715
    invoke-virtual {p0, p1}, LX/FWC;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2251716
    instance-of v1, v0, LX/FVt;

    if-eqz v1, :cond_0

    check-cast v0, LX/FVt;

    .line 2251717
    iget-boolean v1, v0, LX/FVt;->j:Z

    move v0, v1

    .line 2251718
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2251719
    const/4 v0, 0x0

    return v0
.end method
