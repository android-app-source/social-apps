.class public final LX/Fsv;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FsE;

.field public final synthetic b:Ljava/lang/ref/WeakReference;

.field public final synthetic c:LX/Fsw;


# direct methods
.method public constructor <init>(LX/Fsw;LX/FsE;Ljava/lang/ref/WeakReference;)V
    .locals 0

    .prologue
    .line 2297766
    iput-object p1, p0, LX/Fsv;->c:LX/Fsw;

    iput-object p2, p0, LX/Fsv;->a:LX/FsE;

    iput-object p3, p0, LX/Fsv;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2297767
    iget-object v0, p0, LX/Fsv;->a:LX/FsE;

    const/4 v1, 0x1

    .line 2297768
    iput-boolean v1, v0, LX/FsE;->h:Z

    .line 2297769
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2297743
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2297744
    iget-object v0, p0, LX/Fsv;->a:LX/FsE;

    const/4 v1, 0x1

    .line 2297745
    iput-boolean v1, v0, LX/FsE;->h:Z

    .line 2297746
    iget-object v0, p0, LX/Fsv;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/TimelineFragment;

    .line 2297747
    if-nez v0, :cond_1

    .line 2297748
    :cond_0
    :goto_0
    return-void

    .line 2297749
    :cond_1
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2297750
    if-eqz v1, :cond_0

    .line 2297751
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2297752
    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel$FollowedProfilesModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2297753
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2297754
    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel$FollowedProfilesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel$FollowedProfilesModel;->a()LX/0Px;

    move-result-object v1

    .line 2297755
    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    if-nez v2, :cond_3

    .line 2297756
    :cond_2
    :goto_1
    goto :goto_0

    .line 2297757
    :cond_3
    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2297758
    iget-wide v6, v2, LX/5SB;->b:J

    move-wide v2, v6

    .line 2297759
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 2297760
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel$FollowedProfilesModel$NodesModel;

    .line 2297761
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel$FollowedProfilesModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2297762
    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    const/4 v3, 0x1

    .line 2297763
    iput-boolean v3, v2, LX/BQ1;->k:Z

    .line 2297764
    goto :goto_1

    .line 2297765
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2
.end method
