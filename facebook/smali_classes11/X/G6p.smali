.class public final LX/G6p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V
    .locals 1

    .prologue
    .line 2320766
    iput-object p1, p0, LX/G6p;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2320767
    const/16 v0, 0x64

    iput v0, p0, LX/G6p;->b:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;B)V
    .locals 0

    .prologue
    .line 2320768
    invoke-direct {p0, p1}, LX/G6p;-><init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2320769
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, LX/G6p;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget-wide v2, v2, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->e:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2320770
    :goto_0
    return v4

    .line 2320771
    :cond_0
    iget-object v0, p0, LX/G6p;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2320772
    iput-wide v2, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->e:J

    .line 2320773
    goto :goto_0
.end method
