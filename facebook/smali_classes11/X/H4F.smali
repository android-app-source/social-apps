.class public final enum LX/H4F;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H4F;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H4F;

.field public static final enum SEARCH_RADIUS_1:LX/H4F;

.field public static final enum SEARCH_RADIUS_10:LX/H4F;

.field public static final enum SEARCH_RADIUS_5:LX/H4F;


# instance fields
.field private final mRadius:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2422436
    new-instance v0, LX/H4F;

    const-string v1, "SEARCH_RADIUS_1"

    invoke-direct {v0, v1, v4, v3}, LX/H4F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H4F;->SEARCH_RADIUS_1:LX/H4F;

    .line 2422437
    new-instance v0, LX/H4F;

    const-string v1, "SEARCH_RADIUS_5"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v3, v2}, LX/H4F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H4F;->SEARCH_RADIUS_5:LX/H4F;

    .line 2422438
    new-instance v0, LX/H4F;

    const-string v1, "SEARCH_RADIUS_10"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v5, v2}, LX/H4F;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H4F;->SEARCH_RADIUS_10:LX/H4F;

    .line 2422439
    const/4 v0, 0x3

    new-array v0, v0, [LX/H4F;

    sget-object v1, LX/H4F;->SEARCH_RADIUS_1:LX/H4F;

    aput-object v1, v0, v4

    sget-object v1, LX/H4F;->SEARCH_RADIUS_5:LX/H4F;

    aput-object v1, v0, v3

    sget-object v1, LX/H4F;->SEARCH_RADIUS_10:LX/H4F;

    aput-object v1, v0, v5

    sput-object v0, LX/H4F;->$VALUES:[LX/H4F;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2422440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2422441
    iput p3, p0, LX/H4F;->mRadius:I

    .line 2422442
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H4F;
    .locals 1

    .prologue
    .line 2422443
    const-class v0, LX/H4F;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H4F;

    return-object v0
.end method

.method public static values()[LX/H4F;
    .locals 1

    .prologue
    .line 2422444
    sget-object v0, LX/H4F;->$VALUES:[LX/H4F;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H4F;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2422445
    iget v0, p0, LX/H4F;->mRadius:I

    return v0
.end method
