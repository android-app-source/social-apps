.class public final enum LX/GMf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GMf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GMf;

.field public static final enum UPLOAD_IMAGE_TASKS:LX/GMf;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2345130
    new-instance v0, LX/GMf;

    const-string v1, "UPLOAD_IMAGE_TASKS"

    invoke-direct {v0, v1, v2}, LX/GMf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMf;->UPLOAD_IMAGE_TASKS:LX/GMf;

    .line 2345131
    const/4 v0, 0x1

    new-array v0, v0, [LX/GMf;

    sget-object v1, LX/GMf;->UPLOAD_IMAGE_TASKS:LX/GMf;

    aput-object v1, v0, v2

    sput-object v0, LX/GMf;->$VALUES:[LX/GMf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2345129
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GMf;
    .locals 1

    .prologue
    .line 2345128
    const-class v0, LX/GMf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GMf;

    return-object v0
.end method

.method public static values()[LX/GMf;
    .locals 1

    .prologue
    .line 2345127
    sget-object v0, LX/GMf;->$VALUES:[LX/GMf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GMf;

    return-object v0
.end method
