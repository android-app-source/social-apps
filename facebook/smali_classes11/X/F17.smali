.class public final LX/F17;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/F18;


# direct methods
.method public constructor <init>(LX/F18;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2191166
    iput-object p1, p0, LX/F17;->c:LX/F18;

    iput-object p2, p0, LX/F17;->a:Landroid/view/View;

    iput-object p3, p0, LX/F17;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2191167
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2191168
    const v1, 0x7f0d2f04

    if-ne v0, v1, :cond_1

    .line 2191169
    iget-object v0, p0, LX/F17;->c:LX/F18;

    iget-object v0, v0, LX/F18;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    iget-object v1, p0, LX/F17;->c:LX/F18;

    iget-object v1, v1, LX/F18;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/F17;->c:LX/F18;

    iget-object v2, v2, LX/F18;->c:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iget-object v3, p0, LX/F17;->c:LX/F18;

    iget-object v3, v3, LX/F18;->d:LX/1Po;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->b(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 2191170
    if-eqz v0, :cond_0

    .line 2191171
    iget-object v1, p0, LX/F17;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2191172
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2191173
    :cond_1
    const v1, 0x7f0d2f05

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/F17;->c:LX/F18;

    iget-object v0, v0, LX/F18;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/F17;->c:LX/F18;

    iget-object v0, v0, LX/F18;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    if-eqz v0, :cond_0

    .line 2191174
    iget-object v0, p0, LX/F17;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2191175
    iget-object v1, p0, LX/F17;->c:LX/F18;

    iget-object v1, v1, LX/F18;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->h:LX/Es5;

    iget-object v2, p0, LX/F17;->c:LX/F18;

    iget-object v2, v2, LX/F18;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iget-object v3, p0, LX/F17;->b:Ljava/lang/String;

    sget-object v4, LX/CEz;->PERMALINK:LX/CEz;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/Es5;->a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Landroid/app/Activity;Ljava/lang/String;LX/CEz;)V

    goto :goto_0
.end method
