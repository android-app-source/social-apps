.class public final LX/GbO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2369471
    iput-object p1, p0, LX/GbO;->b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iput-object p2, p0, LX/GbO;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2369470
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2369450
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2369451
    if-eqz p1, :cond_0

    .line 2369452
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2369453
    if-nez v0, :cond_1

    .line 2369454
    :cond_0
    :goto_0
    return-void

    .line 2369455
    :cond_1
    iget-object v0, p0, LX/GbO;->b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2369456
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2369457
    check-cast v0, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/protocol/DBLPersistSessionMutationModels$DBLPersistSessionMutationModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2369458
    iget-object v0, p0, LX/GbO;->b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->q:LX/0Tn;

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2369459
    iget-object v1, p0, LX/GbO;->b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->a:LX/0fW;

    iget-object v2, p0, LX/GbO;->b:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->b:LX/0WJ;

    invoke-virtual {v2}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    iget-object v3, p0, LX/GbO;->a:Ljava/lang/String;

    .line 2369460
    new-instance v4, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;

    .line 2369461
    iget-object v5, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2369462
    iget-object v6, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2369463
    iget-object v7, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v7, v7

    .line 2369464
    iget-object v8, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    move-object v8, v8

    .line 2369465
    iget-object v9, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    move-object v9, v9

    .line 2369466
    iget-object v10, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->g:Ljava/lang/String;

    move-object v10, v10

    .line 2369467
    move-object v11, v3

    move v12, v0

    invoke-direct/range {v4 .. v12}, Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2369468
    invoke-static {v1, v4}, LX/0fW;->a(LX/0fW;Lcom/facebook/auth/credentials/DBLLocalAuthCredentials;)V

    .line 2369469
    goto :goto_0
.end method
