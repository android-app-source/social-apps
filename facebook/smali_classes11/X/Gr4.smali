.class public LX/Gr4;
.super LX/Cqj;
.source ""


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 2397166
    invoke-direct {p0, p1, p2}, LX/Cqj;-><init>(LX/Ctg;LX/CrK;)V

    .line 2397167
    new-instance v0, LX/Gr2;

    sget-object v2, LX/Cqw;->a:LX/Cqw;

    .line 2397168
    iget-object v1, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v3, v1

    .line 2397169
    check-cast v3, LX/Ctg;

    sget-object v4, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v5, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v6, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/Gr2;-><init>(LX/Gr4;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 2397170
    new-instance v0, LX/Gr3;

    sget-object v2, LX/Cqw;->b:LX/Cqw;

    .line 2397171
    iget-object v1, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v3, v1

    .line 2397172
    check-cast v3, LX/Ctg;

    sget-object v4, LX/Cqd;->MEDIA_FULLSCREEN:LX/Cqd;

    sget-object v5, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v6, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, LX/Gr3;-><init>(LX/Gr4;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 2397173
    return-void
.end method


# virtual methods
.method public final h()V
    .locals 2

    .prologue
    .line 2397162
    invoke-super {p0}, LX/Cqj;->h()V

    .line 2397163
    invoke-virtual {p0}, LX/CqX;->d()LX/Cqv;

    move-result-object v0

    sget-object v1, LX/Cqw;->a:LX/Cqw;

    if-eq v0, v1, :cond_0

    .line 2397164
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {p0, v0}, LX/CqX;->e(LX/Cqv;)V

    .line 2397165
    :cond_0
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2397161
    const/4 v0, 0x0

    return v0
.end method
