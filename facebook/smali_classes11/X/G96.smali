.class public final LX/G96;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:[I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 2321592
    invoke-direct {p0, p1, p1}, LX/G96;-><init>(II)V

    .line 2321593
    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    .prologue
    .line 2321600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321601
    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 2321602
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Both dimensions must be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321603
    :cond_1
    iput p1, p0, LX/G96;->a:I

    .line 2321604
    iput p2, p0, LX/G96;->b:I

    .line 2321605
    add-int/lit8 v0, p1, 0x1f

    div-int/lit8 v0, v0, 0x20

    iput v0, p0, LX/G96;->c:I

    .line 2321606
    iget v0, p0, LX/G96;->c:I

    mul-int/2addr v0, p2

    new-array v0, v0, [I

    iput-object v0, p0, LX/G96;->d:[I

    .line 2321607
    return-void
.end method

.method private constructor <init>(III[I)V
    .locals 0

    .prologue
    .line 2321594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321595
    iput p1, p0, LX/G96;->a:I

    .line 2321596
    iput p2, p0, LX/G96;->b:I

    .line 2321597
    iput p3, p0, LX/G96;->c:I

    .line 2321598
    iput-object p4, p0, LX/G96;->d:[I

    .line 2321599
    return-void
.end method


# virtual methods
.method public final a(IIII)V
    .locals 9

    .prologue
    .line 2321548
    if-ltz p2, :cond_0

    if-gez p1, :cond_1

    .line 2321549
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Left and top must be nonnegative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321550
    :cond_1
    if-lez p4, :cond_2

    if-gtz p3, :cond_3

    .line 2321551
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Height and width must be at least 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321552
    :cond_3
    add-int v1, p1, p3

    .line 2321553
    add-int v2, p2, p4

    .line 2321554
    iget v0, p0, LX/G96;->b:I

    if-gt v2, v0, :cond_4

    iget v0, p0, LX/G96;->a:I

    if-le v1, v0, :cond_6

    .line 2321555
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The region must fit inside the matrix"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321556
    :cond_5
    add-int/lit8 p2, p2, 0x1

    :cond_6
    if-ge p2, v2, :cond_7

    .line 2321557
    iget v0, p0, LX/G96;->c:I

    mul-int v3, p2, v0

    move v0, p1

    .line 2321558
    :goto_0
    if-ge v0, v1, :cond_5

    .line 2321559
    iget-object v4, p0, LX/G96;->d:[I

    div-int/lit8 v5, v0, 0x20

    add-int/2addr v5, v3

    aget v6, v4, v5

    const/4 v7, 0x1

    and-int/lit8 v8, v0, 0x1f

    shl-int/2addr v7, v8

    or-int/2addr v6, v7

    aput v6, v4, v5

    .line 2321560
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2321561
    :cond_7
    return-void
.end method

.method public final a(II)Z
    .locals 2

    .prologue
    .line 2321590
    iget v0, p0, LX/G96;->c:I

    mul-int/2addr v0, p2

    div-int/lit8 v1, p1, 0x20

    add-int/2addr v0, v1

    .line 2321591
    iget-object v1, p0, LX/G96;->d:[I

    aget v0, v1, v0

    and-int/lit8 v1, p1, 0x1f

    ushr-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(II)V
    .locals 5

    .prologue
    .line 2321608
    iget v0, p0, LX/G96;->c:I

    mul-int/2addr v0, p2

    div-int/lit8 v1, p1, 0x20

    add-int/2addr v0, v1

    .line 2321609
    iget-object v1, p0, LX/G96;->d:[I

    aget v2, v1, v0

    const/4 v3, 0x1

    and-int/lit8 v4, p1, 0x1f

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    aput v2, v1, v0

    .line 2321610
    return-void
.end method

.method public final c(II)V
    .locals 5

    .prologue
    .line 2321587
    iget v0, p0, LX/G96;->c:I

    mul-int/2addr v0, p2

    div-int/lit8 v1, p1, 0x20

    add-int/2addr v0, v1

    .line 2321588
    iget-object v1, p0, LX/G96;->d:[I

    aget v2, v1, v0

    const/4 v3, 0x1

    and-int/lit8 v4, p1, 0x1f

    shl-int/2addr v3, v4

    xor-int/2addr v2, v3

    aput v2, v1, v0

    .line 2321589
    return-void
.end method

.method public final clone()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2321586
    new-instance v1, LX/G96;

    iget v2, p0, LX/G96;->a:I

    iget v3, p0, LX/G96;->b:I

    iget v4, p0, LX/G96;->c:I

    iget-object v0, p0, LX/G96;->d:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    invoke-direct {v1, v2, v3, v4, v0}, LX/G96;-><init>(III[I)V

    return-object v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2321581
    instance-of v1, p1, LX/G96;

    if-nez v1, :cond_1

    .line 2321582
    :cond_0
    :goto_0
    return v0

    .line 2321583
    :cond_1
    check-cast p1, LX/G96;

    .line 2321584
    iget v1, p0, LX/G96;->a:I

    iget v2, p1, LX/G96;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/G96;->b:I

    iget v2, p1, LX/G96;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/G96;->c:I

    iget v2, p1, LX/G96;->c:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/G96;->d:[I

    iget-object v2, p1, LX/G96;->d:[I

    .line 2321585
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2321575
    iget v0, p0, LX/G96;->a:I

    .line 2321576
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/G96;->a:I

    add-int/2addr v0, v1

    .line 2321577
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/G96;->b:I

    add-int/2addr v0, v1

    .line 2321578
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/G96;->c:I

    add-int/2addr v0, v1

    .line 2321579
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/G96;->d:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2321580
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 2321562
    const-string v0, "X "

    const-string v1, "  "

    .line 2321563
    const-string v2, "\n"

    const/4 v4, 0x0

    .line 2321564
    new-instance v7, Ljava/lang/StringBuilder;

    iget v3, p0, LX/G96;->b:I

    iget v5, p0, LX/G96;->a:I

    add-int/lit8 v5, v5, 0x1

    mul-int/2addr v3, v5

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    move v3, v4

    .line 2321565
    :goto_0
    iget v5, p0, LX/G96;->b:I

    if-ge v3, v5, :cond_2

    move v5, v4

    .line 2321566
    :goto_1
    iget v6, p0, LX/G96;->a:I

    if-ge v5, v6, :cond_1

    .line 2321567
    invoke-virtual {p0, v5, v3}, LX/G96;->a(II)Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v6, v0

    :goto_2
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2321568
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_0
    move-object v6, v1

    .line 2321569
    goto :goto_2

    .line 2321570
    :cond_1
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2321571
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2321572
    :cond_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2321573
    move-object v0, v2

    .line 2321574
    return-object v0
.end method
