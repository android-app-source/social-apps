.class public final LX/GtM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400534
    iput-object p1, p0, LX/GtM;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2400535
    const-string v0, "reset_nux"

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2400536
    iget-object v0, p0, LX/GtM;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->l:LX/3Q7;

    const/4 v1, 0x1

    .line 2400537
    iget-object p1, v0, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p1

    sget-object p2, LX/0hM;->l:LX/0Tn;

    invoke-interface {p1, p2, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object p1

    invoke-interface {p1}, LX/0hN;->commit()V

    .line 2400538
    iget-object v0, p0, LX/GtM;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->l:LX/3Q7;

    .line 2400539
    iget-object v1, v0, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object p0, LX/0hM;->E:LX/0Tn;

    invoke-interface {v1, p0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    sget-object p0, LX/0hM;->F:LX/0Tn;

    invoke-interface {v1, p0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2400540
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
