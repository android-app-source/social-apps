.class public LX/Ffv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Fee;

.field public final d:Lcom/facebook/search/results/model/SearchResultsMutableContext;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Fee;)V
    .locals 0
    .param p3    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Fee;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2269141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2269142
    iput-object p1, p0, LX/Ffv;->a:LX/0tX;

    .line 2269143
    iput-object p2, p0, LX/Ffv;->b:LX/1Ck;

    .line 2269144
    iput-object p3, p0, LX/Ffv;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2269145
    iput-object p4, p0, LX/Ffv;->c:LX/Fee;

    .line 2269146
    return-void
.end method

.method public static synthetic a(LX/Ffv;Ljava/lang/String;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 2269147
    new-instance v1, LX/A1G;

    invoke-direct {v1}, LX/A1G;-><init>()V

    move-object v1, v1

    .line 2269148
    const-string v2, "call_site"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "query_function"

    invoke-virtual {p2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "before_cursor"

    invoke-virtual {v2, v3, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "after_cursor"

    invoke-virtual {v2, v3, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "tsid"

    .line 2269149
    iget-object v4, p2, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v4, v4

    .line 2269150
    iget-object v4, v4, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2269151
    if-eqz p5, :cond_0

    .line 2269152
    const-string v2, "count"

    invoke-virtual {v1, v2, p5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2269153
    :cond_0
    iget-object v2, p0, LX/Ffv;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v3, 0x3c

    invoke-virtual {v1, v3, v4}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    move-object v0, v1

    .line 2269154
    return-object v0
.end method

.method public static c(Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;)LX/0us;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2269155
    if-eqz p0, :cond_0

    .line 2269156
    invoke-virtual {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->b()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    move-result-object v0

    .line 2269157
    if-eqz v0, :cond_0

    .line 2269158
    invoke-virtual {v0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;->b()LX/0us;

    move-result-object v0

    .line 2269159
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLInterfaces$LiveFeedQuery;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2269160
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2269161
    if-eqz p0, :cond_1

    .line 2269162
    invoke-virtual {p0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->b()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;

    move-result-object v0

    .line 2269163
    if-eqz v0, :cond_1

    .line 2269164
    invoke-virtual {v0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedNodeModel;

    .line 2269165
    invoke-virtual {v0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedNodeModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2269166
    if-eqz v0, :cond_0

    .line 2269167
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2269168
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2269169
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2269170
    iget-object v0, p0, LX/Ffv;->b:LX/1Ck;

    const-string v1, "fetch_live_conversations_headload"

    new-instance v2, LX/Ffr;

    invoke-direct {v2, p0, p1, p2}, LX/Ffr;-><init>(LX/Ffv;Ljava/lang/String;Ljava/lang/Integer;)V

    new-instance v3, LX/Ffs;

    invoke-direct {v3, p0}, LX/Ffs;-><init>(LX/Ffv;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2269171
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2269172
    iget-object v0, p0, LX/Ffv;->b:LX/1Ck;

    const-string v1, "fetch_live_conversations_headload"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2269173
    iget-object v0, p0, LX/Ffv;->b:LX/1Ck;

    const-string v1, "fetch_live_conversations_tailload"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2269174
    return-void
.end method
