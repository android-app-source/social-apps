.class public final LX/F7B;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V
    .locals 0

    .prologue
    .line 2201327
    iput-object p1, p0, LX/F7B;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 2201328
    iget-object v0, p0, LX/F7B;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    .line 2201329
    iget-boolean v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->K:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->m(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2201330
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    const-class p1, Lcom/facebook/growth/contactimporter/StepInviteActivity;

    invoke-direct {v1, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2201331
    const-string p0, "ci_flow"

    iget-object p1, v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->H:LX/89v;

    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2201332
    const-string p0, "invitee_credentials"

    new-instance p1, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;

    iget-object p2, v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->Q:Ljava/util/Map;

    invoke-direct {p1, p2}, Lcom/facebook/ipc/model/FacebookPhonebookContactMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2201333
    iget-object p0, v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->h:Lcom/facebook/content/SecureContextHelper;

    const/4 p1, 0x0

    invoke-interface {p0, v1, p1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2201334
    :goto_0
    return-void

    .line 2201335
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2201336
    instance-of p0, v1, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;

    if-eqz p0, :cond_2

    .line 2201337
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;

    const/4 p0, -0x1

    invoke-virtual {v1, p0}, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;->b(I)V

    .line 2201338
    :cond_1
    :goto_1
    goto :goto_0

    .line 2201339
    :cond_2
    instance-of p0, v1, Lcom/facebook/growth/nux/UserAccountNUXActivity;

    if-eqz p0, :cond_1

    .line 2201340
    const-string p0, "contact_importer"

    .line 2201341
    check-cast v1, LX/8DG;

    invoke-interface {v1, p0}, LX/8DG;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
