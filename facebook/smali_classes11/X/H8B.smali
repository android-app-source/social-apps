.class public LX/H8B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433079
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2433080
    check-cast p1, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;

    .line 2433081
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2433082
    iget-object v0, p1, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2433083
    const-string v1, "Invalid place id!"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2433084
    const-string v0, "%s/flags"

    .line 2433085
    iget-object v1, p1, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2433086
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2433087
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "flag"

    .line 2433088
    iget-object v2, p1, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2433089
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2433090
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "value"

    const-string v2, "1"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2433091
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "entry_point"

    .line 2433092
    iget-object v2, p1, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2433093
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2433094
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "endpoint"

    .line 2433095
    iget-object v2, p1, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2433096
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2433097
    new-instance v0, LX/14N;

    const-string v1, "reportPlace"

    const-string v2, "POST"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2433098
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2433099
    const/4 v0, 0x0

    return-object v0
.end method
