.class public final LX/Gr8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/67q;


# instance fields
.field public final synthetic a:LX/Gr9;


# direct methods
.method public constructor <init>(LX/Gr9;)V
    .locals 0

    .prologue
    .line 2397524
    iput-object p1, p0, LX/Gr8;->a:LX/Gr9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/68u;)V
    .locals 6

    .prologue
    .line 2397525
    iget v0, p1, LX/68u;->C:F

    move v0, v0

    .line 2397526
    iget-object v1, p0, LX/Gr8;->a:LX/Gr9;

    iget v1, v1, LX/Gr9;->f:I

    int-to-double v2, v1

    float-to-double v0, v0

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 2397527
    iget-object v1, p0, LX/Gr8;->a:LX/Gr9;

    iget-object v1, v1, LX/Gr9;->d:Landroid/widget/ImageView;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 2397528
    return-void
.end method
