.class public final enum LX/GBv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GBv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GBv;

.field public static final enum CONTACT_POINT:LX/GBv;

.field public static final enum NAME:LX/GBv;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2327640
    new-instance v0, LX/GBv;

    const-string v1, "CONTACT_POINT"

    invoke-direct {v0, v1, v2}, LX/GBv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GBv;->CONTACT_POINT:LX/GBv;

    .line 2327641
    new-instance v0, LX/GBv;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v3}, LX/GBv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GBv;->NAME:LX/GBv;

    .line 2327642
    const/4 v0, 0x2

    new-array v0, v0, [LX/GBv;

    sget-object v1, LX/GBv;->CONTACT_POINT:LX/GBv;

    aput-object v1, v0, v2

    sget-object v1, LX/GBv;->NAME:LX/GBv;

    aput-object v1, v0, v3

    sput-object v0, LX/GBv;->$VALUES:[LX/GBv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2327643
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GBv;
    .locals 1

    .prologue
    .line 2327644
    const-class v0, LX/GBv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GBv;

    return-object v0
.end method

.method public static values()[LX/GBv;
    .locals 1

    .prologue
    .line 2327645
    sget-object v0, LX/GBv;->$VALUES:[LX/GBv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GBv;

    return-object v0
.end method
