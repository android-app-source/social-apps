.class public LX/F51;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/F53;

.field public c:I

.field public final d:I


# direct methods
.method public constructor <init>(LX/F53;Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2197893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2197894
    iput-object p2, p0, LX/F51;->a:Landroid/content/res/Resources;

    .line 2197895
    iput-object p1, p0, LX/F51;->b:LX/F53;

    .line 2197896
    iget-object v0, p0, LX/F51;->a:Landroid/content/res/Resources;

    const/high16 v1, 0x42700000    # 60.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    iput v0, p0, LX/F51;->d:I

    .line 2197897
    return-void
.end method

.method public static a(LX/0QB;)LX/F51;
    .locals 3

    .prologue
    .line 2197898
    new-instance v2, LX/F51;

    invoke-static {p0}, LX/F53;->a(LX/0QB;)LX/F53;

    move-result-object v0

    check-cast v0, LX/F53;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v2, v0, v1}, LX/F51;-><init>(LX/F53;Landroid/content/res/Resources;)V

    .line 2197899
    move-object v0, v2

    .line 2197900
    return-object v0
.end method
