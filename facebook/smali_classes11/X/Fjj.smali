.class public LX/Fjj;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2277587
    const-class v0, LX/Fjj;

    sput-object v0, LX/Fjj;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2277588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2277589
    iput-object p1, p0, LX/Fjj;->b:LX/0Zb;

    .line 2277590
    iput-object p2, p0, LX/Fjj;->c:LX/03V;

    .line 2277591
    return-void
.end method

.method public static b(LX/0QB;)LX/Fjj;
    .locals 3

    .prologue
    .line 2277592
    new-instance v2, LX/Fjj;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, v0, v1}, LX/Fjj;-><init>(LX/0Zb;LX/03V;)V

    .line 2277593
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2277594
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2277595
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2277596
    if-nez p2, :cond_0

    .line 2277597
    sget-object v0, LX/Fjj;->a:Ljava/lang/Class;

    invoke-static {v0, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2277598
    iget-object v0, p0, LX/Fjj;->c:LX/03V;

    const-string v1, "SelfUpdateService"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2277599
    :goto_0
    return-void

    .line 2277600
    :cond_0
    sget-object v0, LX/Fjj;->a:Ljava/lang/Class;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p2, p1, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2277601
    iget-object v0, p0, LX/Fjj;->c:LX/03V;

    const-string v1, "SelfUpdateService"

    invoke-virtual {v0, v1, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2277602
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2277603
    const-string v1, "self_update"

    .line 2277604
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2277605
    invoke-virtual {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2277606
    iget-object v1, p0, LX/Fjj;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2277607
    return-void
.end method
