.class public final LX/Fkx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cg8;

.field public final synthetic b:LX/Fky;


# direct methods
.method public constructor <init>(LX/Fky;LX/Cg8;)V
    .locals 0

    .prologue
    .line 2279257
    iput-object p1, p0, LX/Fkx;->b:LX/Fky;

    iput-object p2, p0, LX/Fkx;->a:LX/Cg8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2279258
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2279259
    iget-object v0, p0, LX/Fkx;->b:LX/Fky;

    iget-object v0, v0, LX/Fky;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v1, p0, LX/Fkx;->a:LX/Cg8;

    const/4 v3, 0x0

    .line 2279260
    if-eqz p1, :cond_0

    .line 2279261
    iget-object v2, v1, LX/Cg8;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-object v2, v2

    .line 2279262
    if-eqz v2, :cond_0

    .line 2279263
    iget-object v2, v1, LX/Cg8;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-object v2, v2

    .line 2279264
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;->d()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2279265
    iget-object v2, v1, LX/Cg8;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-object v2, v2

    .line 2279266
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;->d()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2279267
    iget-object v2, v1, LX/Cg8;->d:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;

    move-object v2, v2

    .line 2279268
    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionComposerActionFieldsModel$AuthorModel;->d()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 2279269
    :goto_0
    iget-object v4, v1, LX/Cg8;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2279270
    iget-object v5, v1, LX/Cg8;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2279271
    iget-object p0, v1, LX/Cg8;->e:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-object p0, p0

    .line 2279272
    invoke-static {v4, v5, v2, p0, p1}, LX/FlI;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2279273
    iget-object v4, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->A:LX/1Kf;

    iget-object v5, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->v:Landroid/content/Context;

    invoke-interface {v4, v3, v2, v5}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2279274
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move-object v2, v3

    goto :goto_0
.end method
