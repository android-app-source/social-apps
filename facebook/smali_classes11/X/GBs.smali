.class public final LX/GBs;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 0

    .prologue
    .line 2327552
    iput-object p1, p0, LX/GBs;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2327553
    iget-object v0, p0, LX/GBs;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2327554
    iget-object v0, p0, LX/GBs;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->u:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327555
    iget-object v0, p0, LX/GBs;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327556
    iget-object v0, p0, LX/GBs;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2327557
    :goto_0
    return-void

    .line 2327558
    :cond_0
    iget-object v0, p0, LX/GBs;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->u:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327559
    iget-object v0, p0, LX/GBs;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327560
    iget-object v0, p0, LX/GBs;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->v:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
