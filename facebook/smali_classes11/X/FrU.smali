.class public final LX/FrU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;",
        ">;",
        "LX/FsM;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FrS;


# direct methods
.method public constructor <init>(LX/FrS;)V
    .locals 0

    .prologue
    .line 2295600
    iput-object p1, p0, LX/FrU;->a:LX/FrS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    .prologue
    .line 2295601
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2295602
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2295603
    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;

    .line 2295604
    if-nez v0, :cond_0

    .line 2295605
    const/4 v1, 0x0

    .line 2295606
    :goto_0
    move-object v0, v1

    .line 2295607
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->B()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v1

    .line 2295608
    invoke-static {v1}, LX/Fs4;->a(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;)V

    .line 2295609
    new-instance v2, LX/FsM;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->az()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    .line 2295610
    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2295611
    invoke-direct {v2, v0, v1, v3}, LX/FsM;-><init>(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;LX/0ta;)V

    return-object v2

    .line 2295612
    :cond_0
    new-instance v1, LX/4XY;

    invoke-direct {v1}, LX/4XY;-><init>()V

    .line 2295613
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;

    move-result-object v2

    .line 2295614
    if-nez v2, :cond_1

    .line 2295615
    const/4 v3, 0x0

    .line 2295616
    :goto_1
    move-object v2, v3

    .line 2295617
    iput-object v2, v1, LX/4XY;->aa:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 2295618
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsPageFieldsModel;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v3, v2}, LX/5xF;->a(LX/15i;I)Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v2

    .line 2295619
    iput-object v2, v1, LX/4XY;->cG:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    .line 2295620
    invoke-virtual {v1}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    goto :goto_0

    .line 2295621
    :cond_1
    new-instance v5, LX/4ZF;

    invoke-direct {v5}, LX/4ZF;-><init>()V

    .line 2295622
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2295623
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2295624
    const/4 v3, 0x0

    move v4, v3

    :goto_2
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_2

    .line 2295625
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFieldsModel;

    .line 2295626
    if-nez v3, :cond_4

    .line 2295627
    const/4 v7, 0x0

    .line 2295628
    :goto_3
    move-object v3, v7

    .line 2295629
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2295630
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 2295631
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2295632
    iput-object v3, v5, LX/4ZF;->b:LX/0Px;

    .line 2295633
    :cond_3
    invoke-virtual {v5}, LX/4ZF;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v3

    goto :goto_1

    .line 2295634
    :cond_4
    new-instance v7, LX/4ZC;

    invoke-direct {v7}, LX/4ZC;-><init>()V

    .line 2295635
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFieldsModel;->b()Ljava/lang/String;

    move-result-object v8

    .line 2295636
    iput-object v8, v7, LX/4ZC;->b:Ljava/lang/String;

    .line 2295637
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFieldsModel;->c()Ljava/lang/String;

    move-result-object v8

    .line 2295638
    iput-object v8, v7, LX/4ZC;->c:Ljava/lang/String;

    .line 2295639
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFieldsModel;->d()Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel;

    move-result-object v8

    .line 2295640
    if-nez v8, :cond_5

    .line 2295641
    const/4 v9, 0x0

    .line 2295642
    :goto_4
    move-object v8, v9

    .line 2295643
    iput-object v8, v7, LX/4ZC;->d:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 2295644
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFieldsModel;->e()I

    move-result v8

    .line 2295645
    iput v8, v7, LX/4ZC;->f:I

    .line 2295646
    invoke-virtual {v7}, LX/4ZC;->a()Lcom/facebook/graphql/model/GraphQLTimelineSection;

    move-result-object v7

    goto :goto_3

    .line 2295647
    :cond_5
    new-instance v11, LX/4ZD;

    invoke-direct {v11}, LX/4ZD;-><init>()V

    .line 2295648
    invoke-virtual {v8}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 2295649
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    .line 2295650
    const/4 v9, 0x0

    move v10, v9

    :goto_5
    invoke-virtual {v8}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-ge v10, v9, :cond_6

    .line 2295651
    invoke-virtual {v8}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;

    .line 2295652
    if-nez v9, :cond_8

    .line 2295653
    const/4 v13, 0x0

    .line 2295654
    :goto_6
    move-object v9, v13

    .line 2295655
    invoke-virtual {v12, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2295656
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_5

    .line 2295657
    :cond_6
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 2295658
    iput-object v9, v11, LX/4ZD;->b:LX/0Px;

    .line 2295659
    :cond_7
    invoke-virtual {v8}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel;->b()LX/0us;

    move-result-object v9

    .line 2295660
    if-nez v9, :cond_9

    .line 2295661
    const/4 v10, 0x0

    .line 2295662
    :goto_7
    move-object v9, v10

    .line 2295663
    iput-object v9, v11, LX/4ZD;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2295664
    invoke-virtual {v11}, LX/4ZD;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v9

    goto :goto_4

    .line 2295665
    :cond_8
    new-instance v13, LX/4ZE;

    invoke-direct {v13}, LX/4ZE;-><init>()V

    .line 2295666
    invoke-virtual {v9}, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelinePageFirstUnitsConnectionFieldsModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p0

    .line 2295667
    iput-object p0, v13, LX/4ZE;->c:Lcom/facebook/graphql/model/FeedUnit;

    .line 2295668
    invoke-virtual {v13}, LX/4ZE;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;

    move-result-object v13

    goto :goto_6

    .line 2295669
    :cond_9
    new-instance v10, LX/17L;

    invoke-direct {v10}, LX/17L;-><init>()V

    .line 2295670
    invoke-interface {v9}, LX/0us;->a()Ljava/lang/String;

    move-result-object v12

    .line 2295671
    iput-object v12, v10, LX/17L;->c:Ljava/lang/String;

    .line 2295672
    invoke-interface {v9}, LX/0us;->b()Z

    move-result v12

    .line 2295673
    iput-boolean v12, v10, LX/17L;->d:Z

    .line 2295674
    invoke-interface {v9}, LX/0us;->c()Z

    move-result v12

    .line 2295675
    iput-boolean v12, v10, LX/17L;->e:Z

    .line 2295676
    invoke-interface {v9}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v12

    .line 2295677
    iput-object v12, v10, LX/17L;->f:Ljava/lang/String;

    .line 2295678
    invoke-virtual {v10}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v10

    goto :goto_7
.end method
