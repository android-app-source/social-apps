.class public final LX/Fsd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;",
        ">;",
        "LX/3Fw;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fse;


# direct methods
.method public constructor <init>(LX/Fse;)V
    .locals 0

    .prologue
    .line 2297257
    iput-object p1, p0, LX/Fsd;->a:LX/Fse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2297258
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2297259
    if-eqz p1, :cond_0

    .line 2297260
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297261
    if-eqz v0, :cond_0

    .line 2297262
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297263
    check-cast v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;->a()Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2297264
    :cond_0
    const/4 v0, 0x0

    .line 2297265
    :goto_0
    return-object v0

    .line 2297266
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297267
    check-cast v0, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel;->a()Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel$PeopleYouMayKnowModel;

    move-result-object v1

    .line 2297268
    new-instance v0, LX/3Fw;

    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel$PeopleYouMayKnowModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel$PeopleYouMayKnowModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/2dk;->a(LX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/friends/protocol/FetchPeopleYouMayKnowGraphQLModels$PeopleYouMayKnowQueryModel$PeopleYouMayKnowModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/2dk;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-direct {v0, v2, v1}, LX/3Fw;-><init>(LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;)V

    goto :goto_0
.end method
