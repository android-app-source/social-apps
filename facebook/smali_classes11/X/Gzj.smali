.class public final LX/Gzj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Gzi;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;)V
    .locals 0

    .prologue
    .line 2412245
    iput-object p1, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2412246
    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->c:LX/Gze;

    iget-object v1, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Gze;->c(Ljava/lang/String;)V

    .line 2412247
    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    invoke-static {v0, v7}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;Z)V

    .line 2412248
    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2412249
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2412250
    const-string v2, "show_snackbar_extra"

    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081688

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    .line 2412251
    iget-object v6, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v6

    .line 2412252
    const-string v6, "arg_get_quote_cta_label"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2412253
    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2412254
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2412255
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2412256
    :cond_0
    return-void

    .line 2412257
    :cond_1
    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    .line 2412258
    iget-object v6, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v6

    .line 2412259
    const-string v6, "arg_get_quote_cta_label"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2412260
    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->c:LX/Gze;

    iget-object v1, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Gze;->d(Ljava/lang/String;)V

    .line 2412261
    iget-object v0, p0, LX/Gzj;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;->a$redex0(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderConfirmationFragment;Z)V

    .line 2412262
    return-void
.end method
