.class public LX/GEO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331646
    iput-object p1, p0, LX/GEO;->a:LX/0tX;

    .line 2331647
    iput-object p2, p0, LX/GEO;->b:LX/1Ck;

    .line 2331648
    return-void
.end method

.method public static a(LX/0QB;)LX/GEO;
    .locals 5

    .prologue
    .line 2331649
    const-class v1, LX/GEO;

    monitor-enter v1

    .line 2331650
    :try_start_0
    sget-object v0, LX/GEO;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2331651
    sput-object v2, LX/GEO;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2331652
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2331653
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2331654
    new-instance p0, LX/GEO;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {p0, v3, v4}, LX/GEO;-><init>(LX/0tX;LX/1Ck;)V

    .line 2331655
    move-object v0, p0

    .line 2331656
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2331657
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GEO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2331658
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2331659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2331660
    iget-object v0, p0, LX/GEO;->b:LX/1Ck;

    sget-object v1, LX/GEN;->REACH_TASK:LX/GEN;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2331661
    return-void
.end method
