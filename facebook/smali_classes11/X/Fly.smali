.class public final LX/Fly;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/guestlist/FundraiserGuestListModels$FundraiserIncrementPersonalUpdatesCountMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:LX/0Zb;

.field private d:LX/03V;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILX/0Zb;LX/03V;)V
    .locals 0

    .prologue
    .line 2281766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2281767
    iput-object p1, p0, LX/Fly;->a:Ljava/lang/String;

    .line 2281768
    iput p2, p0, LX/Fly;->b:I

    .line 2281769
    iput-object p3, p0, LX/Fly;->c:LX/0Zb;

    .line 2281770
    iput-object p4, p0, LX/Fly;->d:LX/03V;

    .line 2281771
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2281779
    iget-object v0, p0, LX/Fly;->c:LX/0Zb;

    iget-object v1, p0, LX/Fly;->a:Ljava/lang/String;

    .line 2281780
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "fundraiser_personal_update_mutation_failure"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "fundraiser_page"

    .line 2281781
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2281782
    move-object v2, v2

    .line 2281783
    const-string v3, "fundraiser_campaign_id"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object v1, v2

    .line 2281784
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2281785
    iget-object v0, p0, LX/Fly;->d:LX/03V;

    const-string v1, "personal_update_mutation_failure"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2281786
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2281772
    iget-object v0, p0, LX/Fly;->c:LX/0Zb;

    iget-object v1, p0, LX/Fly;->a:Ljava/lang/String;

    iget v2, p0, LX/Fly;->b:I

    .line 2281773
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fundraiser_personal_update_mutation_success"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "fundraiser_page"

    .line 2281774
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2281775
    move-object p0, p0

    .line 2281776
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "selected_guest_count"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object v1, p0

    .line 2281777
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2281778
    return-void
.end method
