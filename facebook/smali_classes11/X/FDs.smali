.class public LX/FDs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile L:LX/FDs;

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final A:LX/FJx;

.field private final B:LX/2Mk;

.field private final C:LX/03V;

.field private final D:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final E:Lcom/facebook/messaging/media/download/MediaDownloadManager;

.field private final F:LX/0SG;

.field private final G:LX/2NC;

.field private final H:LX/2Ns;

.field private final I:LX/3RE;

.field private final J:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final K:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/2N4;

.field public final e:LX/FDq;

.field public final f:LX/3N0;

.field private final g:LX/2N7;

.field private final h:LX/2NI;

.field private final i:LX/2NJ;

.field private final j:LX/2Nm;

.field private final k:LX/2No;

.field private final l:LX/2Nr;

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6ci;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6cj;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/2NB;

.field private final p:LX/2NA;

.field private final q:LX/2Nq;

.field private final r:LX/2Np;

.field private final s:LX/2ND;

.field private final t:LX/2NE;

.field private final u:LX/2NG;

.field private final v:LX/0lB;

.field private final w:LX/6fD;

.field private final x:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/DdV;

.field private final z:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FK5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2214460
    const-class v0, LX/FDs;

    sput-object v0, LX/FDs;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/2N4;LX/FDq;LX/3N0;LX/2N7;LX/2NI;LX/2NJ;LX/2Nm;LX/2No;LX/2Nr;LX/0Ot;LX/0Ot;LX/2NB;LX/2NA;LX/2Nq;LX/2Np;LX/2ND;LX/2NE;LX/2NG;LX/0lB;LX/6fD;LX/0Or;LX/DdV;LX/0Or;LX/FJx;LX/2Mk;LX/03V;LX/0Or;Lcom/facebook/messaging/media/download/MediaDownloadManager;LX/0SG;LX/2NC;LX/2Ns;LX/3RE;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 1
    .param p23    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserKey;
        .end annotation
    .end param
    .param p29    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .param p36    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/6cy;",
            ">;",
            "LX/2N4;",
            "LX/FDq;",
            "LX/3N0;",
            "LX/2N7;",
            "LX/2NI;",
            "LX/2NJ;",
            "LX/2Nm;",
            "LX/2No;",
            "LX/2Nr;",
            "LX/0Ot",
            "<",
            "LX/6ci;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/6cj;",
            ">;",
            "LX/2NB;",
            "LX/2NA;",
            "LX/2Nq;",
            "LX/2Np;",
            "LX/2ND;",
            "LX/2NE;",
            "LX/2NG;",
            "LX/0lB;",
            "LX/6fD;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/DdV;",
            "LX/0Or",
            "<",
            "LX/FK5;",
            ">;",
            "LX/FJx;",
            "LX/2Mk;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/messaging/media/download/MediaDownloadManager;",
            "LX/0SG;",
            "LX/2NC;",
            "LX/2Ns;",
            "LX/3RE;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2214620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2214621
    iput-object p1, p0, LX/FDs;->b:LX/0Or;

    .line 2214622
    iput-object p2, p0, LX/FDs;->c:LX/0Or;

    .line 2214623
    iput-object p3, p0, LX/FDs;->d:LX/2N4;

    .line 2214624
    iput-object p4, p0, LX/FDs;->e:LX/FDq;

    .line 2214625
    iput-object p5, p0, LX/FDs;->f:LX/3N0;

    .line 2214626
    iput-object p6, p0, LX/FDs;->g:LX/2N7;

    .line 2214627
    iput-object p7, p0, LX/FDs;->h:LX/2NI;

    .line 2214628
    iput-object p8, p0, LX/FDs;->i:LX/2NJ;

    .line 2214629
    iput-object p9, p0, LX/FDs;->j:LX/2Nm;

    .line 2214630
    iput-object p10, p0, LX/FDs;->k:LX/2No;

    .line 2214631
    iput-object p11, p0, LX/FDs;->l:LX/2Nr;

    .line 2214632
    iput-object p12, p0, LX/FDs;->m:LX/0Ot;

    .line 2214633
    iput-object p13, p0, LX/FDs;->n:LX/0Ot;

    .line 2214634
    iput-object p14, p0, LX/FDs;->o:LX/2NB;

    .line 2214635
    move-object/from16 v0, p15

    iput-object v0, p0, LX/FDs;->p:LX/2NA;

    .line 2214636
    move-object/from16 v0, p16

    iput-object v0, p0, LX/FDs;->q:LX/2Nq;

    .line 2214637
    move-object/from16 v0, p17

    iput-object v0, p0, LX/FDs;->r:LX/2Np;

    .line 2214638
    move-object/from16 v0, p18

    iput-object v0, p0, LX/FDs;->s:LX/2ND;

    .line 2214639
    move-object/from16 v0, p19

    iput-object v0, p0, LX/FDs;->t:LX/2NE;

    .line 2214640
    move-object/from16 v0, p20

    iput-object v0, p0, LX/FDs;->u:LX/2NG;

    .line 2214641
    move-object/from16 v0, p21

    iput-object v0, p0, LX/FDs;->v:LX/0lB;

    .line 2214642
    move-object/from16 v0, p22

    iput-object v0, p0, LX/FDs;->w:LX/6fD;

    .line 2214643
    move-object/from16 v0, p23

    iput-object v0, p0, LX/FDs;->x:LX/0Or;

    .line 2214644
    move-object/from16 v0, p24

    iput-object v0, p0, LX/FDs;->y:LX/DdV;

    .line 2214645
    move-object/from16 v0, p25

    iput-object v0, p0, LX/FDs;->z:LX/0Or;

    .line 2214646
    move-object/from16 v0, p26

    iput-object v0, p0, LX/FDs;->A:LX/FJx;

    .line 2214647
    move-object/from16 v0, p27

    iput-object v0, p0, LX/FDs;->B:LX/2Mk;

    .line 2214648
    move-object/from16 v0, p28

    iput-object v0, p0, LX/FDs;->C:LX/03V;

    .line 2214649
    move-object/from16 v0, p29

    iput-object v0, p0, LX/FDs;->D:LX/0Or;

    .line 2214650
    move-object/from16 v0, p30

    iput-object v0, p0, LX/FDs;->E:Lcom/facebook/messaging/media/download/MediaDownloadManager;

    .line 2214651
    move-object/from16 v0, p31

    iput-object v0, p0, LX/FDs;->F:LX/0SG;

    .line 2214652
    move-object/from16 v0, p32

    iput-object v0, p0, LX/FDs;->G:LX/2NC;

    .line 2214653
    move-object/from16 v0, p33

    iput-object v0, p0, LX/FDs;->H:LX/2Ns;

    .line 2214654
    move-object/from16 v0, p34

    iput-object v0, p0, LX/FDs;->I:LX/3RE;

    .line 2214655
    move-object/from16 v0, p35

    iput-object v0, p0, LX/FDs;->J:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2214656
    move-object/from16 v0, p36

    iput-object v0, p0, LX/FDs;->K:LX/0Or;

    .line 2214657
    return-void
.end method

.method private static a(LX/FDs;Lcom/facebook/messaging/model/messages/MessagesCollection;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2214619
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/FDs;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Z)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/messaging/model/messages/MessagesCollection;Z)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            "Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2214564
    const-string v0, "DbInsertThreadsHandler.updateMessagesTables"

    const v2, -0x1c3bee18

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214565
    :try_start_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2214566
    const v0, 0x61cd5a9

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2214567
    :try_start_1
    new-instance v4, LX/0UE;

    invoke-direct {v4}, LX/0UE;-><init>()V

    .line 2214568
    new-instance v5, LX/0UE;

    invoke-direct {v5}, LX/0UE;-><init>()V

    .line 2214569
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v6, v0

    .line 2214570
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v1

    :goto_0
    if-ge v2, v7, :cond_1

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2214571
    iget-object v8, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v4, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2214572
    iget-object v8, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 2214573
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2214574
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2214575
    :cond_1
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    .line 2214576
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2214577
    sget-object v2, LX/0Rg;->a:LX/0Rg;

    move-object v2, v2

    .line 2214578
    :goto_1
    move-object v4, v2

    .line 2214579
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 2214580
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2214581
    iget-object v6, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 2214582
    iget-object v6, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 2214583
    :catchall_0
    move-exception v0

    const v1, 0x11a553ab

    :try_start_2
    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2214584
    :catchall_1
    move-exception v0

    const v1, -0x49c72c40

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2214585
    :cond_3
    :try_start_3
    new-instance v6, LX/0UE;

    invoke-direct {v6}, LX/0UE;-><init>()V

    .line 2214586
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 2214587
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2214588
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v9, v0

    .line 2214589
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v2, v1

    :goto_3
    if-ge v2, v10, :cond_8

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2214590
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 2214591
    if-nez v1, :cond_4

    .line 2214592
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 2214593
    :cond_4
    if-nez v1, :cond_5

    .line 2214594
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2214595
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2214596
    :cond_5
    const/4 v11, 0x1

    .line 2214597
    iget-boolean v12, v1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v12, :cond_c

    .line 2214598
    :cond_6
    :goto_5
    move v11, v11

    .line 2214599
    if-eqz v11, :cond_7

    .line 2214600
    iget-object v11, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-interface {v6, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2214601
    invoke-static {v0, v1}, LX/FDs;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2214602
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2214603
    :cond_7
    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2214604
    :cond_8
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v0, v0

    .line 2214605
    if-eqz v0, :cond_9

    .line 2214606
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/MessagesCollection;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 2214607
    invoke-static {v0}, LX/FDs;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2214608
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2214609
    :cond_9
    invoke-static {p0, v6}, LX/FDs;->a(LX/FDs;Ljava/util/Set;)V

    .line 2214610
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2214611
    invoke-static {p0, v0}, LX/FDs;->d(LX/FDs;Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_6

    .line 2214612
    :cond_a
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2214613
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2214614
    const v1, -0x3beeff25

    :try_start_4
    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2214615
    const v1, 0x480111af

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 2214616
    :cond_b
    :try_start_5
    const/4 v2, 0x2

    new-array v2, v2, [LX/0ux;

    const/4 v6, 0x0

    const-string v7, "msg_id"

    invoke-static {v7, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v7

    aput-object v7, v2, v6

    const/4 v6, 0x1

    const-string v7, "offline_threading_id"

    invoke-static {v7, v5}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v7

    aput-object v7, v2, v6

    invoke-static {v2}, LX/0uu;->b([LX/0ux;)LX/0uw;

    move-result-object v2

    .line 2214617
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v7

    add-int/2addr v6, v7

    .line 2214618
    const/4 v7, 0x0

    invoke-static {v0, v2, v7, v6}, LX/2N4;->a(LX/2N4;LX/0ux;Ljava/lang/String;I)LX/FDm;

    move-result-object v2

    iget-object v2, v2, LX/FDm;->a:Ljava/util/LinkedHashMap;

    goto/16 :goto_1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_c
    iget-boolean v12, v0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v12, :cond_d

    if-nez p2, :cond_6

    :cond_d
    const/4 v11, 0x0

    goto :goto_5
.end method

.method public static a(LX/0QB;)LX/FDs;
    .locals 3

    .prologue
    .line 2214554
    sget-object v0, LX/FDs;->L:LX/FDs;

    if-nez v0, :cond_1

    .line 2214555
    const-class v1, LX/FDs;

    monitor-enter v1

    .line 2214556
    :try_start_0
    sget-object v0, LX/FDs;->L:LX/FDs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2214557
    if-eqz v2, :cond_0

    .line 2214558
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/FDs;->b(LX/0QB;)LX/FDs;

    move-result-object v0

    sput-object v0, LX/FDs;->L:LX/FDs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214559
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2214560
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2214561
    :cond_1
    sget-object v0, LX/FDs;->L:LX/FDs;

    return-object v0

    .line 2214562
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2214563
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/6ek;Lcom/facebook/messaging/model/threads/ThreadSummary;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 2214549
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2214550
    const-string v1, "folder"

    iget-object v2, p0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214551
    const-string v1, "thread_key"

    iget-object v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214552
    const-string v1, "timestamp_ms"

    iget-wide v2, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214553
    return-object v0
.end method

.method private static a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 2214510
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    .line 2214511
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2214512
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    .line 2214513
    iput-object v1, v0, LX/6f7;->r:Ljava/util/List;

    .line 2214514
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v1, :cond_1

    .line 2214515
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    .line 2214516
    iput-object v1, v0, LX/6f7;->s:Lcom/facebook/messaging/model/share/SentShareAttachment;

    .line 2214517
    :cond_1
    iget-wide v2, p0, Lcom/facebook/messaging/model/messages/Message;->d:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 2214518
    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    .line 2214519
    iput-wide v2, v0, LX/6f7;->d:J

    .line 2214520
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2214521
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 2214522
    iput-object v1, v0, LX/6f7;->f:Ljava/lang/String;

    .line 2214523
    :cond_3
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 2214524
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    .line 2214525
    iput-object v1, v0, LX/6f7;->k:Ljava/lang/String;

    .line 2214526
    :cond_4
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    if-nez v1, :cond_5

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    if-eqz v1, :cond_5

    .line 2214527
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    invoke-virtual {v0, v1}, LX/6f7;->a(Ljava/util/Map;)LX/6f7;

    .line 2214528
    :cond_5
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    if-nez v1, :cond_6

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    if-eqz v1, :cond_6

    .line 2214529
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 2214530
    iput-object v1, v0, LX/6f7;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 2214531
    :cond_6
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    if-nez v1, :cond_7

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    if-eqz v1, :cond_7

    .line 2214532
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 2214533
    iput-object v1, v0, LX/6f7;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 2214534
    :cond_7
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    if-nez v1, :cond_8

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    if-eqz v1, :cond_8

    .line 2214535
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 2214536
    iput-object v1, v0, LX/6f7;->H:Lcom/facebook/messaging/business/commerce/model/retail/CommerceData;

    .line 2214537
    :cond_8
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    if-nez v1, :cond_9

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    if-eqz v1, :cond_9

    .line 2214538
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2214539
    iput-object v1, v0, LX/6f7;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2214540
    :cond_9
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-nez v1, :cond_a

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-eqz v1, :cond_a

    .line 2214541
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 2214542
    iput-object v1, v0, LX/6f7;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 2214543
    :cond_a
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    if-nez v1, :cond_b

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 2214544
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 2214545
    iput-object v1, v0, LX/6f7;->J:Ljava/lang/Integer;

    .line 2214546
    :cond_b
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->r:LX/6f3;

    sget-object v2, LX/6f3;->UNKNOWN:LX/6f3;

    if-ne v1, v2, :cond_c

    .line 2214547
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->r:LX/6f3;

    invoke-virtual {v0, v1}, LX/6f7;->a(LX/6f3;)LX/6f7;

    .line 2214548
    :cond_c
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Z)Lcom/facebook/messaging/model/messages/Message;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2214413
    new-instance v0, Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    invoke-direct {p0, v0, p2}, LX/FDs;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;Z)LX/0Px;

    move-result-object v0

    .line 2214414
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/model/threads/ThreadParticipant;ZJJ)Lcom/facebook/messaging/model/threads/ThreadParticipant;
    .locals 3

    .prologue
    .line 2214474
    new-instance v0, LX/6fz;

    invoke-direct {v0}, LX/6fz;-><init>()V

    .line 2214475
    invoke-virtual {v0, p0}, LX/6fz;->a(Lcom/facebook/messaging/model/threads/ThreadParticipant;)LX/6fz;

    .line 2214476
    if-eqz p1, :cond_0

    .line 2214477
    iput-wide p2, v0, LX/6fz;->b:J

    .line 2214478
    iput-wide p4, v0, LX/6fz;->c:J

    .line 2214479
    :goto_0
    invoke-virtual {v0}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    return-object v0

    .line 2214480
    :cond_0
    iput-wide p2, v0, LX/6fz;->e:J

    .line 2214481
    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 10

    .prologue
    .line 2214461
    const-string v0, "DbInsertThreadsHandler.refetchMostRecentMessages"

    const v1, -0x40601236

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214462
    :try_start_0
    iget-object v0, p2, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v7, v0

    .line 2214463
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-wide v2, v7, Lcom/facebook/messaging/model/messages/Message;->c:J

    const-wide/16 v4, -0x1

    const/16 v6, 0x64

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJI)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    move-result-object v0

    .line 2214464
    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2214465
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v1, v7, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2214466
    if-nez v3, :cond_0

    .line 2214467
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v1, v7, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2N4;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2214468
    :cond_0
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    iget-object v5, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214469
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2214470
    iget-object v0, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v0

    .line 2214471
    iget-wide v8, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v6, v8

    .line 2214472
    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214473
    const v0, 0x35b1d596

    invoke-static {v0}, LX/02m;->a(I)V

    return-object v1

    :catchall_0
    move-exception v0

    const v1, -0x24038a8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(Ljava/util/List;Ljava/util/Set;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2214404
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2214405
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2214406
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2214407
    new-instance v3, LX/6fz;

    invoke-direct {v3}, LX/6fz;-><init>()V

    .line 2214408
    invoke-virtual {v3, v0}, LX/6fz;->a(Lcom/facebook/messaging/model/threads/ThreadParticipant;)LX/6fz;

    .line 2214409
    iput-boolean p2, v3, LX/6fz;->f:Z

    .line 2214410
    invoke-virtual {v3}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2214411
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2214412
    :cond_1
    return-object v1
.end method

.method private a(LX/6ek;)V
    .locals 8

    .prologue
    .line 2214446
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2214447
    const v0, 0x6850ecb8

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214448
    :try_start_0
    const-string v0, "folders"

    const-string v2, "folder=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, LX/6ek;->dbName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214449
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2214450
    const-string v0, "last_fetch_action_id"

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214451
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214452
    const-string v3, "threads"

    const-string v4, "folder=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p1, LX/6ek;->dbName:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214453
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214454
    const v0, 0x64d14062

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214455
    return-void

    .line 2214456
    :catch_0
    move-exception v0

    .line 2214457
    :try_start_1
    sget-object v2, LX/FDs;->a:Ljava/lang/Class;

    const-string v3, "SQLException"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214458
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214459
    :catchall_0
    move-exception v0

    const v2, -0x65a5a4b9

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public static a(LX/FDs;LX/0Px;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2214444
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, LX/FDs;->a(LX/FDs;LX/0Px;JZ)V

    .line 2214445
    return-void
.end method

.method public static a(LX/FDs;LX/0Px;JZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;JZ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2214427
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2214428
    const v0, 0x48b1e30b

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214429
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214430
    invoke-static {p0, v0, p2, p3}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2214431
    if-eqz p4, :cond_0

    .line 2214432
    iget-object v4, p0, LX/FDs;->d:LX/2N4;

    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v4

    .line 2214433
    iget-object v5, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v5, v5, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-eqz v5, :cond_0

    .line 2214434
    iget-object v4, v4, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214435
    iget-wide v6, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->e:J

    iget-wide v4, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->e:J

    cmp-long v4, v6, v4

    if-lez v4, :cond_0

    .line 2214436
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2214437
    iget-object v4, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/6dQ;

    invoke-virtual {v4}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2214438
    const-string v5, "messages"

    const-string v6, "thread_key=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214439
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2214440
    :cond_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214441
    const v0, 0x46036056

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214442
    return-void

    .line 2214443
    :catchall_0
    move-exception v0

    const v1, -0x73933ad9

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private static a(LX/FDs;LX/2bA;J)V
    .locals 2

    .prologue
    .line 2214424
    iget-object v0, p0, LX/FDs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    .line 2214425
    invoke-virtual {v0, p1, p2, p3}, LX/48u;->b(LX/0To;J)V

    .line 2214426
    return-void
.end method

.method public static a(LX/FDs;LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V
    .locals 6

    .prologue
    .line 2214415
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214416
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2214417
    const-string v2, "folder"

    iget-object v3, p1, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214418
    const-string v2, "unread_count"

    iget v3, p2, Lcom/facebook/messaging/model/folders/FolderCounts;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214419
    const-string v2, "unseen_count"

    iget v3, p2, Lcom/facebook/messaging/model/folders/FolderCounts;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214420
    const-string v2, "last_seen_time"

    iget-wide v4, p2, Lcom/facebook/messaging/model/folders/FolderCounts;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214421
    const-string v2, "last_action_id"

    iget-wide v4, p2, Lcom/facebook/messaging/model/folders/FolderCounts;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214422
    const-string v2, "folder_counts"

    const/4 v3, 0x0

    const v4, 0x3ff03b40

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x5182fb8f

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2214423
    return-void
.end method

.method private static a(LX/FDs;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2214960
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2214961
    iget-object v0, p2, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v4, v0

    .line 2214962
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214963
    invoke-static {p1, v0}, LX/FDs;->a(LX/6ek;Lcom/facebook/messaging/model/threads/ThreadSummary;)Landroid/content/ContentValues;

    move-result-object v0

    .line 2214964
    const-string v6, "folders"

    const-string v7, ""

    const v8, -0x62280b99

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {v3, v6, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x31d37c9d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2214965
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2214966
    :cond_0
    iget-boolean v0, p2, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    move v0, v0

    .line 2214967
    if-eqz v0, :cond_1

    .line 2214968
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2214969
    const-string v1, "folder"

    iget-object v4, p1, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214970
    const-string v1, "thread_key"

    invoke-static {p1}, LX/6cr;->a(LX/6ek;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214971
    const-string v1, "timestamp_ms"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214972
    const-string v1, "folders"

    const-string v2, ""

    const v4, 0x1da7ebba

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v3, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x1b25b00b

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2214973
    :cond_1
    return-void
.end method

.method public static a(LX/FDs;LX/6ek;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2214683
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2214684
    :goto_0
    return-void

    .line 2214685
    :cond_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214686
    const/4 v1, 0x2

    new-array v1, v1, [LX/0ux;

    const/4 v2, 0x0

    sget-object v3, LX/6dZ;->a:LX/0U1;

    .line 2214687
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2214688
    iget-object v4, p1, LX/6ek;->dbName:Ljava/lang/String;

    invoke-static {v3, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LX/6dZ;->b:LX/0U1;

    .line 2214689
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2214690
    invoke-static {v3, p2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v1

    .line 2214691
    const-string v2, "folders"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static a(LX/FDs;Lcom/facebook/messaging/model/messages/Message;JJLX/6jT;)V
    .locals 10

    .prologue
    .line 2214954
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v2

    .line 2214955
    iget-object v0, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_0

    .line 2214956
    iget-object v0, p0, LX/FDs;->y:LX/DdV;

    iget-object v1, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v3, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    sget-object v6, LX/DdQ;->MESSAGE_ADDED:LX/DdQ;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-object v2, p1

    move-wide v4, p2

    move-object/from16 v7, p6

    invoke-virtual/range {v0 .. v8}, LX/DdV;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2214957
    invoke-static {p0, v0}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2214958
    invoke-static {p0, v0, p4, p5}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2214959
    :cond_0
    return-void
.end method

.method private static a(LX/FDs;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;ZJJJ)V
    .locals 12

    .prologue
    .line 2214935
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 2214936
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2214937
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v1, v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-nez v1, :cond_1

    .line 2214938
    :cond_0
    :goto_0
    return-void

    .line 2214939
    :cond_1
    const/4 v1, 0x0

    .line 2214940
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 2214941
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v9, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    .line 2214942
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v10, :cond_4

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2214943
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p3, :cond_2

    iget-wide v2, v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;->e:J

    cmp-long v2, v2, p4

    if-gez v2, :cond_3

    :cond_2
    move v1, p3

    move-wide/from16 v2, p4

    move-wide/from16 v4, p6

    .line 2214944
    invoke-static/range {v0 .. v5}, LX/FDs;->a(Lcom/facebook/messaging/model/threads/ThreadParticipant;ZJJ)Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2214945
    const/4 v0, 0x1

    .line 2214946
    :goto_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v1, v0

    goto :goto_1

    .line 2214947
    :cond_3
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_2

    .line 2214948
    :cond_4
    if-eqz v1, :cond_0

    .line 2214949
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, LX/6cv;->PARTICIPANT:LX/6cv;

    invoke-static {v0, p1, v8, v1}, LX/6cw;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;LX/6cv;)V

    .line 2214950
    const-wide/16 v0, 0x0

    cmp-long v0, p8, v0

    if-lez v0, :cond_0

    .line 2214951
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2214952
    const-string v1, "sequence_id"

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214953
    const-string v1, "threads"

    const-string v2, "thread_key=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v7, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 8

    .prologue
    .line 2214924
    const-string v0, "DbInsertThreadsHandler.updateThreadInFolderTable"

    const v1, 0x55c13826

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214925
    :try_start_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214926
    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    .line 2214927
    invoke-static {v1, p1}, LX/FDs;->a(LX/6ek;Lcom/facebook/messaging/model/threads/ThreadSummary;)Landroid/content/ContentValues;

    move-result-object v2

    .line 2214928
    iget-object v3, p0, LX/FDs;->e:LX/FDq;

    invoke-virtual {v3, v1}, LX/FDq;->a(LX/6ek;)J

    move-result-wide v4

    .line 2214929
    iget-wide v6, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v1, v6, v4

    if-ltz v1, :cond_0

    .line 2214930
    const-string v1, "folders"

    const-string v3, ""

    const v4, -0x1ef08e9f

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x2728b3b0

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214931
    :goto_0
    const v0, 0x4ec8656e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2214932
    return-void

    .line 2214933
    :cond_0
    :try_start_1
    const-string v1, "folders"

    const-string v2, "thread_key=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2214934
    :catchall_0
    move-exception v0

    const v1, -0x5fcb817

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    .locals 7

    .prologue
    .line 2214809
    const-string v0, "DbInsertThreadsHandler.updateThreadsTable"

    const v1, 0x1e50b0a9

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214810
    :try_start_0
    invoke-direct {p0, p1}, LX/FDs;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2214811
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2214812
    const-string v0, "thread_key"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214813
    const-string v0, "legacy_thread_id"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214814
    const-string v0, "action_id"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214815
    const-string v0, "sequence_id"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214816
    const-string v0, "refetch_action_id"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214817
    const-string v0, "last_visible_action_id"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214818
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2214819
    const-string v0, "name"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214820
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2214821
    const-string v0, "pic_hash"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->r:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214822
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2214823
    const-string v0, "pic"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->s:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214824
    :cond_2
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    if-eqz v0, :cond_8

    .line 2214825
    const-string v0, "media_preview"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->t:Lcom/facebook/messaging/model/threads/ThreadMediaPreview;

    .line 2214826
    new-instance v5, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214827
    const-string v4, "type"

    iget-object p1, v3, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->a:LX/6fx;

    invoke-virtual {p1}, LX/6fx;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, v4, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214828
    const-string p1, "thumbnail_uri"

    iget-object v4, v3, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->b:Landroid/net/Uri;

    if-nez v4, :cond_b

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v5, p1, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214829
    const-string v4, "sticker_id"

    iget-object p1, v3, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->c:Ljava/lang/String;

    invoke-virtual {v5, v4, p1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214830
    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2214831
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214832
    :goto_1
    const-string v0, "senders"

    iget-object v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->n:LX/0Px;

    invoke-static {v4}, LX/2N7;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214833
    const-string v0, "snippet"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->o:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214834
    const-string v0, "snippet_sender"

    iget-object v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->q:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-static {v4}, LX/2N7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214835
    const-string v0, "admin_snippet"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->p:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214836
    const-string v0, "timestamp_ms"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214837
    const-string v0, "last_read_timestamp_ms"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214838
    const-string v0, "approx_total_message_count"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->l:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214839
    const-string v0, "unread_message_count"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->m:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214840
    const-string v0, "last_fetch_time_ms"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214841
    const-string v0, "missed_call_status"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->y:LX/6g5;

    invoke-virtual {v3}, LX/6g5;->getValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214842
    const-string v0, "can_reply_to"

    iget-boolean v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->u:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2214843
    const-string v0, "cannot_reply_reason"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->v:Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLMessageThreadCannotReplyReason;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214844
    const-string v0, "is_subscribed"

    iget-boolean v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->w:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2214845
    const-string v0, "folder"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    iget-object v3, v3, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214846
    const-string v0, "draft"

    iget-object v3, p0, LX/FDs;->p:LX/2NA;

    iget-object v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    invoke-virtual {v3, v4}, LX/2NA;->a(Lcom/facebook/messaging/model/messages/MessageDraft;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214847
    const-string v0, "mute_until"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214848
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2214849
    const-string v3, "me_bubble_color"

    iget v4, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214850
    const-string v3, "other_bubble_color"

    iget v4, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214851
    const-string v3, "wallpaper_color"

    iget v4, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214852
    const-string v3, "custom_like_emoji"

    iget-object v4, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214853
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->g:Lcom/facebook/messaging/model/threads/NicknamesMap;

    .line 2214854
    iget-object v3, v0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    invoke-virtual {v3}, LX/0P1;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2214855
    iget-object v3, v0, Lcom/facebook/messaging/model/threads/NicknamesMap;->b:LX/0P1;

    invoke-static {v3}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v3

    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    .line 2214856
    :cond_3
    iget-object v3, v0, Lcom/facebook/messaging/model/threads/NicknamesMap;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2214857
    if-nez v0, :cond_9

    .line 2214858
    const-string v0, "custom_nicknames"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2214859
    :goto_2
    const-string v0, "last_fetch_action_id"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->I:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214860
    const-string v0, "initial_fetch_complete"

    iget-boolean v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->J:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2214861
    const-string v0, "outgoing_message_lifetime"

    iget v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->K:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214862
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    .line 2214863
    iget-boolean v3, v0, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    if-eqz v3, :cond_c

    iget-object v3, v0, Lcom/facebook/messaging/model/threads/RoomThreadData;->a:Landroid/net/Uri;

    if-eqz v3, :cond_c

    const/4 v3, 0x1

    :goto_3
    move v0, v3

    .line 2214864
    if-eqz v0, :cond_4

    .line 2214865
    const-string v0, "invite_uri"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/RoomThreadData;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214866
    :cond_4
    const-string v0, "is_last_message_sponsored"

    iget-boolean v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->M:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2214867
    const-string v0, "group_chat_rank"

    iget v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->P:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 2214868
    const-string v0, "game_data"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Q:LX/0P1;

    .line 2214869
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2214870
    const-string v4, "{}"

    .line 2214871
    :goto_4
    move-object v3, v4

    .line 2214872
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214873
    const-string v0, "is_joinable"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-boolean v3, v3, Lcom/facebook/messaging/model/threads/RoomThreadData;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2214874
    const-string v0, "requires_approval"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-boolean v3, v3, Lcom/facebook/messaging/model/threads/RoomThreadData;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2214875
    const-string v0, "rtc_call_info"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->G:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 2214876
    if-nez v3, :cond_f

    .line 2214877
    const/4 v4, 0x0

    .line 2214878
    :goto_5
    move-object v3, v4

    .line 2214879
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214880
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->R:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2214881
    const-string v0, "last_message_commerce_message_type"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->R:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214882
    :cond_5
    const-string v0, "is_thread_queue_enabled"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->T:LX/03R;

    invoke-virtual {v3}, LX/03R;->getDbValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214883
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/RoomThreadData;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 2214884
    const-string v0, "group_description"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/RoomThreadData;->f:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214885
    :cond_6
    const-string v0, "booking_requests"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->V:Lcom/facebook/messaging/model/threads/ThreadBookingRequests;

    invoke-static {v3}, LX/2NG;->a(Lcom/facebook/messaging/model/threads/ThreadBookingRequests;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214886
    const-string v0, "last_call_ms"

    iget-wide v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->H:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214887
    const-string v0, "is_discoverable"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-boolean v3, v3, Lcom/facebook/messaging/model/threads/RoomThreadData;->c:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2214888
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->N:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    if-eqz v0, :cond_7

    .line 2214889
    const-string v0, "last_sponsored_message_call_to_action"

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->N:Lcom/facebook/messaging/business/common/calltoaction/model/CallToAction;

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, LX/4gg;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214890
    :cond_7
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v0, :cond_a

    .line 2214891
    const-string v0, "montage_thread_key"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2214892
    :goto_6
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214893
    const-string v3, "threads"

    const-string v4, ""

    const v5, -0x1255b828

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, -0x34655a1

    invoke-static {v2}, LX/03h;->a(I)V

    .line 2214894
    iget-object v2, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2214895
    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    sget-object v4, LX/6cv;->PARTICIPANT:LX/6cv;

    invoke-static {v0, v2, v3, v4}, LX/6cw;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;LX/6cv;)V

    .line 2214896
    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->i:LX/0Px;

    sget-object v4, LX/6cv;->BOT:LX/6cv;

    invoke-static {v0, v2, v3, v4}, LX/6cw;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;LX/6cv;)V

    .line 2214897
    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->Y:Lcom/facebook/messaging/model/threads/RoomThreadData;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/RoomThreadData;->e:LX/0Px;

    invoke-static {v0, v2, v3}, LX/6cw;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;)V

    .line 2214898
    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->L:LX/0Px;

    invoke-static {v0, v2, v3}, LX/6cu;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;)V

    .line 2214899
    iget-object v2, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->L:LX/0Px;

    invoke-static {v0, v2}, LX/6ct;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 2214900
    iget-object v0, p0, LX/FDs;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK5;

    .line 2214901
    new-instance v2, LX/FK4;

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0, v3}, LX/FK5;->d(LX/FK5;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v3

    iget-object v4, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->C:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-direct {v2, v3, v4}, LX/FK4;-><init>(Lcom/facebook/messaging/model/threads/NotificationSetting;Lcom/facebook/messaging/model/threads/NotificationSetting;)V

    move-object v2, v2

    .line 2214902
    invoke-virtual {v2}, LX/FK4;->a()Z

    move-result v2

    if-nez v2, :cond_10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214903
    :goto_7
    const v0, 0x653f0352

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2214904
    return-void

    .line 2214905
    :cond_8
    :try_start_1
    const-string v0, "media_preview"

    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 2214906
    :catchall_0
    move-exception v0

    const v1, -0x156faa21

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2214907
    :cond_9
    :try_start_2
    const-string v3, "custom_nicknames"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2214908
    :cond_a
    const-string v0, "montage_thread_key"

    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->X:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_6

    .line 2214909
    :cond_b
    :try_start_3
    iget-object v4, v3, Lcom/facebook/messaging/model/threads/ThreadMediaPreview;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_c
    :try_start_4
    const/4 v3, 0x0

    goto/16 :goto_3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2214910
    :cond_d
    :try_start_5
    new-instance v6, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214911
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_8
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2214912
    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/threads/ThreadGameData;

    .line 2214913
    invoke-virtual {v6, v4}, LX/0m9;->j(Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string p2, "high_score_user"

    iget-object p3, v5, Lcom/facebook/messaging/model/threads/ThreadGameData;->a:Ljava/lang/String;

    invoke-virtual {v4, p2, p3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v4

    const-string p2, "high_score"

    iget v5, v5, Lcom/facebook/messaging/model/threads/ThreadGameData;->b:I

    invoke-virtual {v4, p2, v5}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    goto :goto_8

    .line 2214914
    :cond_e
    invoke-virtual {v6}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2214915
    :cond_f
    :try_start_6
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214916
    const-string v5, "call_state"

    iget-object v6, v3, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->b:LX/6g2;

    invoke-virtual {v6}, LX/6g2;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214917
    const-string v5, "server_info"

    iget-object v6, v3, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->c:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214918
    const-string v5, "initiator_id"

    iget-object v6, v3, Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;->d:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214919
    invoke-virtual {v4}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2214920
    :cond_10
    iget-object v3, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2214921
    iget-object v2, v0, LX/FK5;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bad;

    iget-object v4, v0, LX/FK5;->b:Landroid/content/Context;

    const-class v5, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;

    const-string v6, "NotificationsPrefsService.SYNC_THREAD_FROM_SERVER"

    invoke-virtual {v2, v4, v5, v6}, LX/Bad;->a(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2214922
    const-string v4, "THREAD_KEY_STRING"

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2214923
    iget-object v3, v0, LX/FK5;->b:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_7
.end method

.method public static a(LX/FDs;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2214787
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2214788
    :goto_0
    return-void

    .line 2214789
    :cond_0
    const-string v0, "DbInsertThreadsHandler.setPinnedThreadIdsInDb"

    const v1, -0xf9812be

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214790
    :try_start_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2214791
    const v0, -0x22201b1

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2214792
    :try_start_1
    const-string v0, "pinned_threads"

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214793
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2214794
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2214795
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2214796
    const-string v3, "thread_key"

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214797
    const-string v3, "display_order"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2214798
    iget-object v3, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6dQ;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2214799
    const-string v5, "pinned_threads"

    const-string v6, ""

    const v7, 0x67fa88a3

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v3, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v3, -0x38d81280    # -42989.5f

    invoke-static {v3}, LX/03h;->a(I)V

    .line 2214800
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2214801
    :cond_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214802
    const v0, 0x9c2d63a

    :try_start_2
    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2214803
    const v0, -0xe6922ce

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2214804
    :catch_0
    move-exception v0

    .line 2214805
    :try_start_3
    sget-object v1, LX/FDs;->a:Ljava/lang/Class;

    const-string v3, "SQLException"

    invoke-static {v1, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214806
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2214807
    :catchall_0
    move-exception v0

    const v1, 0x188d1e73

    :try_start_4
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2214808
    :catchall_1
    move-exception v0

    const v1, -0x6b8a29a9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static a(LX/FDs;Ljava/util/List;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2214785
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, LX/6cv;->PARTICIPANT:LX/6cv;

    invoke-static {v0, p2, p1, v1}, LX/6cw;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/List;LX/6cv;)V

    .line 2214786
    return-void
.end method

.method public static a(LX/FDs;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2214780
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2214781
    :goto_0
    return-void

    .line 2214782
    :cond_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214783
    const-string v1, "msg_id"

    invoke-static {v1, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    .line 2214784
    const-string v2, "messages"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 10

    .prologue
    .line 2214773
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2214774
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/messages/MessagesCollection;->c()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2214775
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    .line 2214776
    iget-object v1, p0, LX/FDs;->y:LX/DdV;

    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    sget-object v6, LX/DdQ;->MESSAGE_DELETED:LX/DdQ;

    sget-object v7, LX/6jT;->a:LX/6jT;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-wide v4, p2

    invoke-virtual/range {v1 .. v8}, LX/DdV;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2214777
    invoke-static {p0, v1}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2214778
    iget-wide v2, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-static {p0, v1, v2, v3}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2214779
    :cond_0
    return-void
.end method

.method private a(Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2214755
    const-string v0, "DbInsertThreadsHandler.getOfflineThreadingIdsForMessageIds"

    const v1, 0x8b0347d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214756
    :try_start_0
    const-string v0, "msg_id"

    invoke-static {v0, p1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v4

    .line 2214757
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214758
    const-string v1, "messages"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "msg_id"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "offline_threading_id"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "pending_send_media_attachment"

    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2214759
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2214760
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2214761
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2214762
    :cond_1
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2214763
    iget-object v0, p0, LX/FDs;->o:LX/2NB;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2NB;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2214764
    if-eqz v0, :cond_0

    .line 2214765
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2214766
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2214767
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2214768
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2214769
    :catchall_1
    move-exception v0

    const v1, 0x4d2de9c4    # 1.82361152E8f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2214770
    :cond_3
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2214771
    const v0, -0x57a32b06

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2214772
    return-void
.end method

.method private static b(LX/0QB;)LX/FDs;
    .locals 39

    .prologue
    .line 2214753
    new-instance v2, LX/FDs;

    const/16 v3, 0x274b

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2744

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v5

    check-cast v5, LX/2N4;

    invoke-static/range {p0 .. p0}, LX/FDq;->a(LX/0QB;)LX/FDq;

    move-result-object v6

    check-cast v6, LX/FDq;

    invoke-static/range {p0 .. p0}, LX/3N0;->a(LX/0QB;)LX/3N0;

    move-result-object v7

    check-cast v7, LX/3N0;

    invoke-static/range {p0 .. p0}, LX/2N7;->a(LX/0QB;)LX/2N7;

    move-result-object v8

    check-cast v8, LX/2N7;

    invoke-static/range {p0 .. p0}, LX/2NI;->a(LX/0QB;)LX/2NI;

    move-result-object v9

    check-cast v9, LX/2NI;

    invoke-static/range {p0 .. p0}, LX/2NJ;->a(LX/0QB;)LX/2NJ;

    move-result-object v10

    check-cast v10, LX/2NJ;

    invoke-static/range {p0 .. p0}, LX/2Nm;->a(LX/0QB;)LX/2Nm;

    move-result-object v11

    check-cast v11, LX/2Nm;

    invoke-static/range {p0 .. p0}, LX/2No;->a(LX/0QB;)LX/2No;

    move-result-object v12

    check-cast v12, LX/2No;

    invoke-static/range {p0 .. p0}, LX/2Nr;->a(LX/0QB;)LX/2Nr;

    move-result-object v13

    check-cast v13, LX/2Nr;

    const/16 v14, 0x273c

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x273d

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/2NB;->a(LX/0QB;)LX/2NB;

    move-result-object v16

    check-cast v16, LX/2NB;

    invoke-static/range {p0 .. p0}, LX/2NA;->a(LX/0QB;)LX/2NA;

    move-result-object v17

    check-cast v17, LX/2NA;

    invoke-static/range {p0 .. p0}, LX/2Nq;->a(LX/0QB;)LX/2Nq;

    move-result-object v18

    check-cast v18, LX/2Nq;

    invoke-static/range {p0 .. p0}, LX/2Np;->a(LX/0QB;)LX/2Np;

    move-result-object v19

    check-cast v19, LX/2Np;

    invoke-static/range {p0 .. p0}, LX/2ND;->a(LX/0QB;)LX/2ND;

    move-result-object v20

    check-cast v20, LX/2ND;

    invoke-static/range {p0 .. p0}, LX/2NE;->a(LX/0QB;)LX/2NE;

    move-result-object v21

    check-cast v21, LX/2NE;

    invoke-static/range {p0 .. p0}, LX/2NG;->a(LX/0QB;)LX/2NG;

    move-result-object v22

    check-cast v22, LX/2NG;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v23

    check-cast v23, LX/0lB;

    invoke-static/range {p0 .. p0}, LX/6fD;->a(LX/0QB;)LX/6fD;

    move-result-object v24

    check-cast v24, LX/6fD;

    const/16 v25, 0x12ce

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v25

    invoke-static/range {p0 .. p0}, LX/DdV;->a(LX/0QB;)LX/DdV;

    move-result-object v26

    check-cast v26, LX/DdV;

    const/16 v27, 0x28ea

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    invoke-static/range {p0 .. p0}, LX/FJx;->a(LX/0QB;)LX/FJx;

    move-result-object v28

    check-cast v28, LX/FJx;

    invoke-static/range {p0 .. p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v29

    check-cast v29, LX/2Mk;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v30

    check-cast v30, LX/03V;

    const/16 v31, 0x14ea

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v31

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/media/download/MediaDownloadManager;->a(LX/0QB;)Lcom/facebook/messaging/media/download/MediaDownloadManager;

    move-result-object v32

    check-cast v32, Lcom/facebook/messaging/media/download/MediaDownloadManager;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v33

    check-cast v33, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/2NC;->a(LX/0QB;)LX/2NC;

    move-result-object v34

    check-cast v34, LX/2NC;

    invoke-static/range {p0 .. p0}, LX/2Ns;->a(LX/0QB;)LX/2Ns;

    move-result-object v35

    check-cast v35, LX/2Ns;

    invoke-static/range {p0 .. p0}, LX/3RE;->a(LX/0QB;)LX/3RE;

    move-result-object v36

    check-cast v36, LX/3RE;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v37

    check-cast v37, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v38, 0x15e8

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v38

    invoke-direct/range {v2 .. v38}, LX/FDs;-><init>(LX/0Or;LX/0Or;LX/2N4;LX/FDq;LX/3N0;LX/2N7;LX/2NI;LX/2NJ;LX/2Nm;LX/2No;LX/2Nr;LX/0Ot;LX/0Ot;LX/2NB;LX/2NA;LX/2Nq;LX/2Np;LX/2ND;LX/2NE;LX/2NG;LX/0lB;LX/6fD;LX/0Or;LX/DdV;LX/0Or;LX/FJx;LX/2Mk;LX/03V;LX/0Or;Lcom/facebook/messaging/media/download/MediaDownloadManager;LX/0SG;LX/2NC;LX/2Ns;LX/3RE;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 2214754
    return-object v2
.end method

.method private b(Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 2214735
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2214736
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v1, v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-nez v1, :cond_0

    .line 2214737
    :goto_0
    return-object p1

    .line 2214738
    :cond_0
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214739
    invoke-static {p1, v0}, LX/FDs;->b(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2214740
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 2214741
    if-nez v2, :cond_2

    .line 2214742
    :goto_1
    move-object v1, v1

    .line 2214743
    iget-object v0, p0, LX/FDs;->D:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v2, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 2214744
    iget-object v0, p0, LX/FDs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    sget-object v2, LX/6cx;->l:LX/2bA;

    invoke-virtual {v0, v2, v4, v5}, LX/48u;->a(LX/0To;J)J

    move-result-wide v2

    .line 2214745
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 2214746
    iput-wide v2, v0, LX/6g6;->d:J

    .line 2214747
    move-object v0, v0

    .line 2214748
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    :goto_2
    move-object p1, v0

    .line 2214749
    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 2214750
    :cond_2
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v3

    .line 2214751
    iput-object v2, v3, LX/6g6;->B:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 2214752
    invoke-virtual {v3}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    goto :goto_1
.end method

.method private static b(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threads/ThreadSummary;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 14

    .prologue
    .line 2214707
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0}, LX/0PM;->a(I)Ljava/util/HashMap;

    move-result-object v2

    .line 2214708
    iget-object v3, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2214709
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2214710
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v5

    invoke-interface {v2, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2214711
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2214712
    :cond_0
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v3

    .line 2214713
    iget-object v0, v3, LX/6g6;->h:Ljava/util/List;

    move-object v0, v0

    .line 2214714
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 2214715
    iget-object v0, v3, LX/6g6;->h:Ljava/util/List;

    move-object v0, v0

    .line 2214716
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2214717
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2214718
    if-nez v1, :cond_1

    .line 2214719
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2214720
    :cond_1
    new-instance v6, LX/6fz;

    invoke-direct {v6}, LX/6fz;-><init>()V

    .line 2214721
    invoke-virtual {v6, v0}, LX/6fz;->a(Lcom/facebook/messaging/model/threads/ThreadParticipant;)LX/6fz;

    .line 2214722
    iget-wide v12, v6, LX/6fz;->b:J

    move-wide v8, v12

    .line 2214723
    iget-wide v10, v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b:J

    cmp-long v0, v8, v10

    if-gez v0, :cond_2

    .line 2214724
    iget-wide v8, v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->b:J

    .line 2214725
    iput-wide v8, v6, LX/6fz;->b:J

    .line 2214726
    :cond_2
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2214727
    iget-object v0, v6, LX/6fz;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2214728
    if-nez v0, :cond_3

    .line 2214729
    iget-object v0, v1, Lcom/facebook/messaging/model/threads/ThreadParticipant;->d:Ljava/lang/String;

    .line 2214730
    iput-object v0, v6, LX/6fz;->d:Ljava/lang/String;

    .line 2214731
    :cond_3
    invoke-virtual {v6}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2214732
    :cond_4
    iput-object v4, v3, LX/6g6;->h:Ljava/util/List;

    .line 2214733
    invoke-virtual {v3}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2214734
    return-object v0
.end method

.method public static b(LX/FDs;LX/0Px;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2214692
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2214693
    const v0, -0x7d309b22

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214694
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2214695
    const-string v4, "folders"

    const-string v5, "thread_key=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214696
    const-string v4, "pinned_threads"

    const-string v5, "thread_key=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214697
    const-string v4, "threads"

    const-string v5, "thread_key=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214698
    const-string v4, "messages"

    const-string v5, "thread_key=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214699
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2214700
    :cond_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214701
    const v0, -0x71a1a814

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214702
    return-void

    .line 2214703
    :catch_0
    move-exception v0

    .line 2214704
    :try_start_1
    sget-object v1, LX/FDs;->a:Ljava/lang/Class;

    const-string v3, "SQLException"

    invoke-static {v1, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214705
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214706
    :catchall_0
    move-exception v0

    const v1, 0x2a1035ba

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method private static b(LX/FDs;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x1

    .line 2214482
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214483
    const v1, -0x40646121

    invoke-static {v0, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214484
    :try_start_0
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v4

    .line 2214485
    const-string v1, "thread_key"

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2214486
    const-string v1, "msg_type"

    sget-object v2, LX/2uW;->PENDING_SEND:LX/2uW;

    iget v2, v2, LX/2uW;->dbKeyValue:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2214487
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v8

    .line 2214488
    const-string v1, "messages"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "msg_id"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "timestamp_ms"

    aput-object v5, v2, v3

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "timestamp_ms"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2214489
    :goto_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2214490
    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2214491
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 2214492
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2214493
    :catchall_0
    move-exception v1

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2214494
    :catchall_1
    move-exception v1

    const v2, 0x23c8e4e5

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1

    .line 2214495
    :cond_0
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 2214496
    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2214497
    add-long v2, p2, v10

    .line 2214498
    invoke-virtual {v8}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v4, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2214499
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 2214500
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2214501
    const-string v3, "timestamp_ms"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214502
    const-string v3, "messages"

    const-string v7, "msg_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v8, v9

    invoke-virtual {v0, v3, v2, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214503
    add-long v2, v4, v10

    move-wide v4, v2

    .line 2214504
    goto :goto_1

    .line 2214505
    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, v10

    move-wide v4, v2

    .line 2214506
    goto :goto_1

    .line 2214507
    :cond_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2214508
    const v1, -0x125c7fd3

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214509
    return-void
.end method

.method private b(Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 10

    .prologue
    .line 2214658
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214659
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    sget-object v1, LX/6ek;->MONTAGE:LX/6ek;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 2214660
    :cond_0
    :goto_0
    return-void

    .line 2214661
    :cond_1
    iget-object v0, p0, LX/FDs;->K:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2214662
    if-eqz v0, :cond_0

    .line 2214663
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    const/4 v4, 0x0

    .line 2214664
    iget-object v3, p0, LX/FDs;->K:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2214665
    if-nez v3, :cond_3

    move-object v2, v4

    .line 2214666
    :cond_2
    :goto_1
    move-object v1, v2

    .line 2214667
    if-eqz v1, :cond_0

    .line 2214668
    iget-wide v8, v1, Lcom/facebook/user/model/User;->N:J

    move-wide v2, v8

    .line 2214669
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 2214670
    const-string v4, "thread_key"

    .line 2214671
    iget-object v5, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, v5

    .line 2214672
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v6, v7, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v1

    .line 2214673
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2214674
    const-string v0, "montage_thread_key"

    invoke-static {v2, v3}, LX/2Mv;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214675
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214676
    const-string v2, "threads"

    invoke-virtual {v1}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v4, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 2214677
    :cond_3
    iget-object v5, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2214678
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2214679
    iget-object v5, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2214680
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v2, v1

    .line 2214681
    goto :goto_1

    :cond_4
    move-object v2, v4

    .line 2214682
    goto :goto_1
.end method

.method private b(Lcom/facebook/messaging/service/model/MarkThreadsParams;)V
    .locals 14

    .prologue
    .line 2213611
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 2213612
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2213613
    new-instance v0, LX/3CL;

    invoke-direct {v0}, LX/3CL;-><init>()V

    const-string v1, "last_read_timestamp_ms"

    .line 2213614
    iput-object v1, v0, LX/3CL;->a:Ljava/lang/String;

    .line 2213615
    move-object v7, v0

    .line 2213616
    new-instance v0, LX/3CL;

    invoke-direct {v0}, LX/3CL;-><init>()V

    const-string v1, "unread_message_count"

    .line 2213617
    iput-object v1, v0, LX/3CL;->a:Ljava/lang/String;

    .line 2213618
    move-object v8, v0

    .line 2213619
    const/4 v1, 0x0

    .line 2213620
    iget-object v9, p1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v10, :cond_4

    invoke-virtual {v9, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2213621
    const-string v2, "thread_key"

    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v11

    .line 2213622
    iget-boolean v2, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v2, :cond_2

    iget-wide v2, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    .line 2213623
    :goto_1
    new-instance v12, LX/494;

    invoke-direct {v12}, LX/494;-><init>()V

    .line 2213624
    iput-object v11, v12, LX/494;->a:LX/0ux;

    .line 2213625
    move-object v12, v12

    .line 2213626
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 2213627
    iput-object v2, v12, LX/494;->b:Ljava/lang/String;

    .line 2213628
    move-object v2, v12

    .line 2213629
    invoke-virtual {v2}, LX/494;->a()LX/495;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/3CL;->a(LX/0ux;)LX/3CL;

    .line 2213630
    iget-boolean v2, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v2, :cond_3

    const-wide/16 v2, 0x0

    .line 2213631
    :goto_2
    new-instance v12, LX/494;

    invoke-direct {v12}, LX/494;-><init>()V

    .line 2213632
    iput-object v11, v12, LX/494;->a:LX/0ux;

    .line 2213633
    move-object v12, v12

    .line 2213634
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 2213635
    iput-object v2, v12, LX/494;->b:Ljava/lang/String;

    .line 2213636
    move-object v2, v12

    .line 2213637
    invoke-virtual {v2}, LX/494;->a()LX/495;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/3CL;->a(LX/0ux;)LX/3CL;

    .line 2213638
    iget-wide v2, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    const-wide/16 v12, -0x1

    cmp-long v2, v2, v12

    if-eqz v2, :cond_1

    .line 2213639
    if-nez v1, :cond_0

    .line 2213640
    new-instance v1, LX/3CL;

    invoke-direct {v1}, LX/3CL;-><init>()V

    const-string v2, "sequence_id"

    .line 2213641
    iput-object v2, v1, LX/3CL;->a:Ljava/lang/String;

    .line 2213642
    move-object v1, v1

    .line 2213643
    :cond_0
    iget-wide v2, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 2213644
    new-instance v3, LX/494;

    invoke-direct {v3}, LX/494;-><init>()V

    .line 2213645
    iput-object v11, v3, LX/494;->a:LX/0ux;

    .line 2213646
    move-object v3, v3

    .line 2213647
    iput-object v2, v3, LX/494;->b:Ljava/lang/String;

    .line 2213648
    move-object v2, v3

    .line 2213649
    invoke-virtual {v2}, LX/494;->a()LX/495;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3CL;->a(LX/0ux;)LX/3CL;

    .line 2213650
    :cond_1
    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2213651
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 2213652
    :cond_2
    const-wide/16 v2, 0x0

    goto :goto_1

    .line 2213653
    :cond_3
    const-wide/16 v2, 0x1

    goto :goto_2

    .line 2213654
    :cond_4
    new-instance v0, LX/490;

    invoke-direct {v0}, LX/490;-><init>()V

    .line 2213655
    invoke-virtual {v7}, LX/3CL;->a()LX/2tn;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/490;->a(LX/0ux;)V

    .line 2213656
    invoke-virtual {v8}, LX/3CL;->a()LX/2tn;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/490;->a(LX/0ux;)V

    .line 2213657
    if-eqz v1, :cond_5

    .line 2213658
    invoke-virtual {v1}, LX/3CL;->a()LX/2tn;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/490;->a(LX/0ux;)V

    .line 2213659
    :cond_5
    const-string v1, "thread_key"

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v1

    .line 2213660
    new-instance v2, LX/492;

    invoke-direct {v2}, LX/492;-><init>()V

    const-string v3, "threads"

    .line 2213661
    iput-object v3, v2, LX/492;->a:Ljava/lang/String;

    .line 2213662
    move-object v2, v2

    .line 2213663
    iput-object v0, v2, LX/492;->b:LX/490;

    .line 2213664
    move-object v0, v2

    .line 2213665
    iput-object v1, v0, LX/492;->c:LX/0ux;

    .line 2213666
    move-object v0, v0

    .line 2213667
    new-instance v1, LX/493;

    invoke-direct {v1, v0}, LX/493;-><init>(LX/492;)V

    move-object v0, v1

    .line 2213668
    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    .line 2213669
    return-void
.end method

.method public static c(LX/FDs;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2214141
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/FDs;->a(Lcom/facebook/messaging/model/messages/Message;Z)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method private static d(LX/FDs;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 14
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2213812
    const-string v0, "DbInsertThreadsHandler.updateMessagesTablesInternal"

    const v1, -0x7bbe852f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213813
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2213814
    const-string v0, "msg_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213815
    const-string v0, "thread_key"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213816
    const-string v0, "action_id"

    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2213817
    const-string v0, "text"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213818
    const-string v0, "sender"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-static {v3}, LX/2N7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213819
    const-string v0, "is_not_forwardable"

    iget-boolean v2, p1, Lcom/facebook/messaging/model/messages/Message;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2213820
    const-string v0, "timestamp_ms"

    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2213821
    const-string v0, "msg_type"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    iget v2, v2, LX/2uW;->dbKeyValue:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2213822
    const-string v0, "affected_users"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->m:LX/0Px;

    invoke-static {v3}, LX/2N7;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213823
    const-string v0, "attachments"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    .line 2213824
    if-nez v3, :cond_1b

    .line 2213825
    const/4 v4, 0x0

    .line 2213826
    :goto_0
    move-object v2, v4

    .line 2213827
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213828
    const-string v0, "shares"

    iget-object v2, p0, LX/FDs;->i:LX/2NJ;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->j:LX/0Px;

    .line 2213829
    if-nez v3, :cond_22

    .line 2213830
    const/4 v4, 0x0

    .line 2213831
    :goto_1
    move-object v2, v4

    .line 2213832
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213833
    const-string v0, "sticker_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213834
    const-string v0, "client_tags"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    invoke-static {v2}, LX/2No;->a(LX/0P1;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213835
    const-string v0, "offline_threading_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213836
    const-string v0, "source"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213837
    const-string v0, "channel_source"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    invoke-virtual {v2}, LX/6f2;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213838
    const-string v0, "send_channel"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->r:LX/6f3;

    invoke-virtual {v2}, LX/6f3;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213839
    const-string v0, "is_non_authoritative"

    iget-boolean v2, p1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2213840
    invoke-static {p1}, LX/2Mk;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2213841
    const-string v0, "pending_send_media_attachment"

    iget-object v2, p0, LX/FDs;->o:LX/2NB;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v2, v3}, LX/2NB;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213842
    :cond_0
    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 2213843
    const-string v0, "timestamp_sent_ms"

    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2213844
    :cond_1
    const-string v0, "sent_share_attachment"

    iget-object v2, p0, LX/FDs;->j:LX/2Nm;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    .line 2213845
    if-nez v3, :cond_24

    .line 2213846
    const/4 v6, 0x0

    .line 2213847
    :goto_2
    move-object v2, v6

    .line 2213848
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213849
    const-string v0, "send_error"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-object v2, v2, LX/6fP;->serializedString:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213850
    const-string v0, "send_error_number"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget v2, v2, Lcom/facebook/messaging/model/send/SendError;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2213851
    const-string v0, "send_error_message"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213852
    const-string v0, "send_error_timestamp_ms"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-wide v2, v2, Lcom/facebook/messaging/model/send/SendError;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2213853
    const-string v0, "send_error_error_url"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/SendError;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213854
    const-string v0, "publicity"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->s:Lcom/facebook/messaging/model/messages/Publicity;

    .line 2213855
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Publicity;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2213856
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213857
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    if-eqz v0, :cond_2

    .line 2213858
    const-string v0, "send_queue_type"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    iget-object v2, v2, LX/6fM;->serializedValue:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213859
    :cond_2
    const-string v0, "payment_transaction"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 2213860
    if-nez v2, :cond_26

    .line 2213861
    const/4 v6, 0x0

    .line 2213862
    :goto_3
    move-object v2, v6

    .line 2213863
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213864
    const-string v0, "payment_request"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->C:Lcom/facebook/messaging/model/payment/PaymentRequestData;

    .line 2213865
    if-nez v2, :cond_27

    .line 2213866
    const/4 v6, 0x0

    .line 2213867
    :goto_4
    move-object v2, v6

    .line 2213868
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213869
    const-string v0, "has_unavailable_attachment"

    iget-boolean v2, p1, Lcom/facebook/messaging/model/messages/Message;->D:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2213870
    const-string v0, "app_attribution"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2213871
    if-nez v2, :cond_28

    .line 2213872
    const/4 v3, 0x0

    .line 2213873
    :goto_5
    move-object v2, v3

    .line 2213874
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213875
    const-string v0, "content_app_attribution"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    invoke-static {v2}, LX/2NC;->a(Lcom/facebook/messaging/model/attribution/ContentAppAttribution;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213876
    const-string v0, "xma"

    iget-object v2, p0, LX/FDs;->H:LX/2Ns;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v2, v3}, LX/2Ns;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213877
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->I:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;

    .line 2213878
    if-eqz v0, :cond_15

    .line 2213879
    const-string v2, "admin_text_type"

    .line 2213880
    sget-object v3, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->a:LX/0Rh;

    iget-object v4, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v3, v4}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2a

    sget-object v3, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->a:LX/0Rh;

    iget-object v4, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->c:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v3, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    :goto_6
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v3, v3

    .line 2213881
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2213882
    iget v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->d:I

    move v2, v2

    .line 2213883
    if-eqz v2, :cond_3

    .line 2213884
    const-string v2, "admin_text_theme_color"

    .line 2213885
    iget v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->d:I

    move v3, v3

    .line 2213886
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2213887
    :cond_3
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->e:Ljava/lang/String;

    move-object v2, v2

    .line 2213888
    if-eqz v2, :cond_4

    .line 2213889
    const-string v2, "admin_text_thread_icon_emoji"

    .line 2213890
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2213891
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213892
    :cond_4
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2213893
    if-eqz v2, :cond_5

    .line 2213894
    const-string v2, "admin_text_nickname"

    .line 2213895
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2213896
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213897
    :cond_5
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->g:Ljava/lang/String;

    move-object v2, v2

    .line 2213898
    if-eqz v2, :cond_6

    .line 2213899
    const-string v2, "admin_text_target_id"

    .line 2213900
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2213901
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213902
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->f()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2213903
    const-string v2, "admin_text_thread_message_lifetime"

    .line 2213904
    iget v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->h:I

    move v3, v3

    .line 2213905
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2213906
    :cond_7
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->i:LX/0Px;

    move-object v2, v2

    .line 2213907
    if-eqz v2, :cond_8

    .line 2213908
    const-string v3, "admin_text_thread_journey_color_choices"

    invoke-static {v2}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v2

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213909
    :cond_8
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->j:LX/0Px;

    move-object v2, v2

    .line 2213910
    if-eqz v2, :cond_9

    .line 2213911
    const-string v3, "admin_text_thread_journey_emoji_choices"

    invoke-static {v2}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v2

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213912
    :cond_9
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->k:LX/0Px;

    move-object v2, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2213913
    if-eqz v2, :cond_a

    .line 2213914
    :try_start_1
    const-string v3, "admin_text_thread_journey_nickname_choices"

    iget-object v4, p0, LX/FDs;->v:LX/0lB;

    invoke-virtual {v4, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch LX/28F; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213915
    :cond_a
    :try_start_2
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->l:LX/0Px;

    move-object v2, v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2213916
    if-eqz v2, :cond_b

    .line 2213917
    :try_start_3
    const-string v3, "admin_text_thread_journey_bot_choices"

    iget-object v4, p0, LX/FDs;->v:LX/0lB;

    invoke-virtual {v4, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch LX/28F; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2213918
    :cond_b
    :try_start_4
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->m:Ljava/lang/String;

    move-object v2, v2

    .line 2213919
    if-eqz v2, :cond_c

    .line 2213920
    const-string v2, "admin_text_thread_rtc_event"

    .line 2213921
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->m:Ljava/lang/String;

    move-object v3, v3

    .line 2213922
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213923
    :cond_c
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->n:Ljava/lang/String;

    move-object v2, v2

    .line 2213924
    if-eqz v2, :cond_d

    .line 2213925
    const-string v2, "admin_text_thread_rtc_server_info_data"

    .line 2213926
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->n:Ljava/lang/String;

    move-object v3, v3

    .line 2213927
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213928
    :cond_d
    const-string v2, "admin_text_thread_rtc_is_video_call"

    .line 2213929
    iget-boolean v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->o:Z

    move v3, v3

    .line 2213930
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2213931
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->p:Ljava/lang/String;

    move-object v2, v2

    .line 2213932
    if-eqz v2, :cond_e

    .line 2213933
    const-string v2, "admin_text_thread_ride_provider_name"

    .line 2213934
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->p:Ljava/lang/String;

    move-object v3, v3

    .line 2213935
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213936
    :cond_e
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->q:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$AdProperties;

    move-object v2, v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2213937
    if-eqz v2, :cond_f

    .line 2213938
    :try_start_5
    const-string v3, "admin_text_thread_ad_properties"

    iget-object v4, p0, LX/FDs;->v:LX/0lB;

    invoke-virtual {v4, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch LX/28F; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2213939
    :cond_f
    :try_start_6
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->r:Ljava/lang/String;

    move-object v2, v2

    .line 2213940
    if-eqz v2, :cond_10

    .line 2213941
    new-instance v2, LX/0m9;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v3}, LX/0m9;-><init>(LX/0mC;)V

    .line 2213942
    const-string v3, "game_type"

    .line 2213943
    iget-object v4, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->r:Ljava/lang/String;

    move-object v4, v4

    .line 2213944
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2213945
    const-string v3, "score"

    .line 2213946
    iget v4, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->s:I

    move v4, v4

    .line 2213947
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2213948
    const-string v3, "is_new_high_score"

    .line 2213949
    iget-boolean v4, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->t:Z

    move v4, v4

    .line 2213950
    invoke-virtual {v2, v3, v4}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2213951
    const-string v3, "admin_text_game_score_data"

    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213952
    :cond_10
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->u:Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo$EventReminderProperties;

    move-object v2, v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2213953
    if-eqz v2, :cond_11

    .line 2213954
    :try_start_7
    const-string v3, "admin_text_thread_event_reminder_properties"

    iget-object v4, p0, LX/FDs;->v:LX/0lB;

    invoke-virtual {v4, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch LX/28F; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2213955
    :cond_11
    :try_start_8
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->v:LX/6et;

    move-object v2, v2

    .line 2213956
    if-eqz v2, :cond_12

    .line 2213957
    const-string v2, "admin_text_joinable_event_type"

    .line 2213958
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->v:LX/6et;

    move-object v3, v3

    .line 2213959
    invoke-virtual {v3}, LX/6et;->toDbValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213960
    :cond_12
    const-string v2, "admin_text_is_joinable_promo"

    .line 2213961
    iget-boolean v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->w:Z

    move v3, v3

    .line 2213962
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2213963
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->x:Ljava/lang/String;

    move-object v2, v2

    .line 2213964
    if-eqz v2, :cond_13

    .line 2213965
    const-string v2, "admin_text_agent_intent_id"

    .line 2213966
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->x:Ljava/lang/String;

    move-object v3, v3

    .line 2213967
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213968
    :cond_13
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->y:Ljava/lang/String;

    move-object v2, v2

    .line 2213969
    if-eqz v2, :cond_14

    .line 2213970
    const-string v2, "admin_text_booking_request_id"

    .line 2213971
    iget-object v3, v0, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->y:Ljava/lang/String;

    move-object v3, v3

    .line 2213972
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213973
    :cond_14
    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->I()Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    move-result-object v2

    if-eqz v2, :cond_15

    .line 2213974
    const-string v2, "generic_admin_message_extensible_data"

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->I()Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/GenericAdminMessageExtensibleData;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213975
    :cond_15
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    if-eqz v0, :cond_16

    .line 2213976
    const-string v0, "message_lifetime"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2213977
    :cond_16
    const-string v0, "is_sponsored"

    iget-boolean v2, p1, Lcom/facebook/messaging/model/messages/Message;->M:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2213978
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->N:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 2213979
    const-string v0, "commerce_message_type"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->N:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213980
    :cond_17
    const-string v0, "customizations"

    iget-boolean v2, p1, Lcom/facebook/messaging/model/messages/Message;->O:Z

    .line 2213981
    if-eqz v2, :cond_2b

    const-string v3, "{\"border\":\"flowers\"}"

    :goto_7
    move-object v2, v3

    .line 2213982
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213983
    const-string v0, "metadata_at_text_ranges"

    iget-object v2, p0, LX/FDs;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->Q:LX/0Px;

    .line 2213984
    if-eqz v2, :cond_18

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2c

    .line 2213985
    :cond_18
    const/4 v3, 0x0

    .line 2213986
    :goto_8
    move-object v2, v3

    .line 2213987
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213988
    const-string v0, "platform_metadata"

    iget-object v2, p0, LX/FDs;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    .line 2213989
    if-eqz v2, :cond_19

    invoke-virtual {v2}, LX/0P1;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 2213990
    :cond_19
    const/4 v3, 0x0

    .line 2213991
    :goto_9
    move-object v2, v3

    .line 2213992
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213993
    const-string v0, "montage_reply_message_id"

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213994
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v0, :cond_1a

    .line 2213995
    :try_start_9
    const-string v0, "profile_ranges"

    iget-object v2, p0, LX/FDs;->v:LX/0lB;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    invoke-virtual {v2, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2213996
    :cond_1a
    :try_start_a
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2213997
    const-string v2, "messages"

    const-string v3, ""

    const v4, 0x58a58ab3

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x1682ed7d

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2213998
    invoke-direct {p0, p1}, LX/FDs;->e(Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2213999
    const v0, 0x16d82da

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2214000
    return-void

    .line 2214001
    :catch_0
    :try_start_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Nickname choices were not serializable."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 2214002
    :catchall_0
    move-exception v0

    const v1, 0x28bbdd79

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2214003
    :catch_1
    :try_start_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bot choices were not serializable."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2214004
    :catch_2
    move-exception v0

    .line 2214005
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Ad properties were not serializable."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2214006
    :catch_3
    move-exception v0

    .line 2214007
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Event Reminder properties were not serializable."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2214008
    :catch_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Profile ranges were not serializable."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 2214009
    :cond_1b
    :try_start_d
    new-instance v5, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/162;-><init>(LX/0mC;)V

    .line 2214010
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_a
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_21

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/attachment/Attachment;

    .line 2214011
    new-instance v7, LX/0m9;

    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v8}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214012
    const-string v8, "id"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->a:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214013
    const-string v8, "fbid"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->c:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214014
    const-string v8, "mime_type"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->d:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214015
    const-string v8, "filename"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->e:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214016
    const-string v8, "file_size"

    iget v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->f:I

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214017
    iget-object v8, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    if-eqz v8, :cond_1e

    .line 2214018
    const-string v8, "image_data_width"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget v9, v9, Lcom/facebook/messaging/model/attachment/ImageData;->a:I

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214019
    const-string v8, "image_data_height"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget v9, v9, Lcom/facebook/messaging/model/attachment/ImageData;->b:I

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214020
    iget-object v8, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v8, v8, Lcom/facebook/messaging/model/attachment/ImageData;->c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    if-eqz v8, :cond_1c

    .line 2214021
    const-string v8, "urls"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v9, v9, Lcom/facebook/messaging/model/attachment/ImageData;->c:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    invoke-static {v9}, LX/2NI;->a(Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214022
    :cond_1c
    iget-object v8, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v8, v8, Lcom/facebook/messaging/model/attachment/ImageData;->d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    if-eqz v8, :cond_1d

    .line 2214023
    const-string v8, "image_animated_urls"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v9, v9, Lcom/facebook/messaging/model/attachment/ImageData;->d:Lcom/facebook/messaging/model/attachment/AttachmentImageMap;

    invoke-static {v9}, LX/2NI;->a(Lcom/facebook/messaging/model/attachment/AttachmentImageMap;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214024
    :cond_1d
    const-string v8, "image_data_source"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v9, v9, Lcom/facebook/messaging/model/attachment/ImageData;->e:LX/5dT;

    iget v9, v9, LX/5dT;->intValue:I

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214025
    const-string v8, "render_as_sticker"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-boolean v9, v9, Lcom/facebook/messaging/model/attachment/ImageData;->f:Z

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2214026
    const-string v8, "mini_preview"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->g:Lcom/facebook/messaging/model/attachment/ImageData;

    iget-object v9, v9, Lcom/facebook/messaging/model/attachment/ImageData;->g:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214027
    :cond_1e
    iget-object v8, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    if-eqz v8, :cond_1f

    .line 2214028
    const-string v8, "video_data_width"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2214029
    iget v2, v9, Lcom/facebook/messaging/model/attachment/VideoData;->a:I

    move v9, v2

    .line 2214030
    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214031
    const-string v8, "video_data_height"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2214032
    iget v2, v9, Lcom/facebook/messaging/model/attachment/VideoData;->b:I

    move v9, v2

    .line 2214033
    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214034
    const-string v8, "video_data_rotation"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2214035
    iget v2, v9, Lcom/facebook/messaging/model/attachment/VideoData;->c:I

    move v9, v2

    .line 2214036
    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214037
    const-string v8, "video_data_length"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2214038
    iget v2, v9, Lcom/facebook/messaging/model/attachment/VideoData;->d:I

    move v9, v2

    .line 2214039
    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214040
    const-string v8, "video_data_source"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2214041
    iget-object v2, v9, Lcom/facebook/messaging/model/attachment/VideoData;->e:LX/5dX;

    move-object v9, v2

    .line 2214042
    iget v9, v9, LX/5dX;->intValue:I

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214043
    const-string v8, "video_data_url"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2214044
    iget-object v2, v9, Lcom/facebook/messaging/model/attachment/VideoData;->f:Landroid/net/Uri;

    move-object v9, v2

    .line 2214045
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214046
    iget-object v8, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2214047
    iget-object v9, v8, Lcom/facebook/messaging/model/attachment/VideoData;->g:Landroid/net/Uri;

    move-object v8, v9

    .line 2214048
    if-eqz v8, :cond_1f

    .line 2214049
    const-string v8, "video_data_thumbnail_url"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->h:Lcom/facebook/messaging/model/attachment/VideoData;

    .line 2214050
    iget-object v2, v9, Lcom/facebook/messaging/model/attachment/VideoData;->g:Landroid/net/Uri;

    move-object v9, v2

    .line 2214051
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214052
    :cond_1f
    iget-object v8, v4, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    if-eqz v8, :cond_20

    .line 2214053
    const-string v8, "is_voicemail"

    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 2214054
    iget-boolean v2, v9, Lcom/facebook/messaging/model/attachment/AudioData;->a:Z

    move v9, v2

    .line 2214055
    invoke-virtual {v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2214056
    const-string v8, "call_id"

    iget-object v4, v4, Lcom/facebook/messaging/model/attachment/Attachment;->i:Lcom/facebook/messaging/model/attachment/AudioData;

    .line 2214057
    iget-object v9, v4, Lcom/facebook/messaging/model/attachment/AudioData;->b:Ljava/lang/String;

    move-object v4, v9

    .line 2214058
    invoke-virtual {v7, v8, v4}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214059
    :cond_20
    invoke-virtual {v5, v7}, LX/162;->a(LX/0lF;)LX/162;

    goto/16 :goto_a

    .line 2214060
    :cond_21
    invoke-virtual {v5}, LX/162;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 2214061
    :cond_22
    :try_start_e
    new-instance v5, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v4}, LX/162;-><init>(LX/0mC;)V

    .line 2214062
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_b
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/share/Share;

    .line 2214063
    iget-object v7, v2, LX/2NJ;->a:LX/2NK;

    invoke-virtual {v7, v4}, LX/2NK;->a(Lcom/facebook/messaging/model/share/Share;)LX/0m9;

    move-result-object v4

    .line 2214064
    invoke-virtual {v5, v4}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_b

    .line 2214065
    :cond_23
    invoke-virtual {v5}, LX/162;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 2214066
    :cond_24
    :try_start_f
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214067
    const-string v7, "type"

    iget-object v8, v3, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    iget-object v8, v8, LX/6fR;->DBSerialValue:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214068
    sget-object v7, LX/6ck;->a:[I

    iget-object v8, v3, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    invoke-virtual {v8}, LX/6fR;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 2214069
    :goto_c
    invoke-virtual {v6}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    .line 2214070
    :pswitch_0
    const-string v7, "attachment"

    iget-object v8, v2, LX/2Nm;->a:LX/2NK;

    iget-object v9, v3, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    invoke-virtual {v8, v9}, LX/2NK;->a(Lcom/facebook/messaging/model/share/Share;)LX/0m9;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_c

    .line 2214071
    :pswitch_1
    const-string v7, "attachment"

    iget-object v8, v3, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    .line 2214072
    if-nez v8, :cond_25

    .line 2214073
    const/4 v10, 0x0

    .line 2214074
    :goto_d
    move-object v8, v10

    .line 2214075
    invoke-virtual {v6, v7, v8}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_c

    .line 2214076
    :cond_25
    new-instance v10, LX/0m9;

    sget-object v11, LX/0mC;->a:LX/0mC;

    invoke-direct {v10, v11}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214077
    const-string v11, "amount"

    iget-object v12, v8, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2214078
    iget-object v13, v12, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v12, v13

    .line 2214079
    invoke-virtual {v12}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214080
    const-string v11, "currency"

    iget-object v12, v8, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2214081
    iget-object v13, v12, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v12, v13

    .line 2214082
    invoke-virtual {v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214083
    const-string v11, "senderCredentialId"

    iget-wide v12, v8, Lcom/facebook/messaging/model/payment/SentPayment;->b:J

    invoke-virtual {v10, v11, v12, v13}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2214084
    const-string v11, "recipientId"

    iget-object v12, v8, Lcom/facebook/messaging/model/payment/SentPayment;->c:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214085
    const-string v11, "memoText"

    iget-object v12, v8, Lcom/facebook/messaging/model/payment/SentPayment;->d:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214086
    const-string v11, "pin"

    iget-object v12, v8, Lcom/facebook/messaging/model/payment/SentPayment;->e:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214087
    const-string v11, "fromPaymentTrigger"

    iget-boolean v12, v8, Lcom/facebook/messaging/model/payment/SentPayment;->g:Z

    invoke-virtual {v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2214088
    const-string v11, "groupThreadId"

    iget-object v12, v8, Lcom/facebook/messaging/model/payment/SentPayment;->j:Ljava/lang/String;

    invoke-virtual {v10, v11, v12}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_d
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 2214089
    :cond_26
    :try_start_10
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214090
    const-string v7, "id"

    .line 2214091
    iget-object v8, v2, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2214092
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214093
    const-string v7, "from"

    .line 2214094
    iget-wide v10, v2, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->b:J

    move-wide v8, v10

    .line 2214095
    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2214096
    const-string v7, "to"

    .line 2214097
    iget-wide v10, v2, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->c:J

    move-wide v8, v10

    .line 2214098
    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2214099
    const-string v7, "amount"

    .line 2214100
    iget v8, v2, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->d:I

    move v8, v8

    .line 2214101
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214102
    const-string v7, "currency"

    .line 2214103
    iget-object v8, v2, Lcom/facebook/messaging/model/payment/PaymentTransactionData;->e:Ljava/lang/String;

    move-object v8, v8

    .line 2214104
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214105
    invoke-virtual {v6}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_3
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 2214106
    :cond_27
    :try_start_11
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214107
    const-string v7, "id"

    .line 2214108
    iget-object v8, v2, Lcom/facebook/messaging/model/payment/PaymentRequestData;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2214109
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214110
    const-string v7, "from"

    .line 2214111
    iget-wide v10, v2, Lcom/facebook/messaging/model/payment/PaymentRequestData;->b:J

    move-wide v8, v10

    .line 2214112
    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2214113
    const-string v7, "to"

    .line 2214114
    iget-wide v10, v2, Lcom/facebook/messaging/model/payment/PaymentRequestData;->c:J

    move-wide v8, v10

    .line 2214115
    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2214116
    const-string v7, "amount"

    .line 2214117
    iget v8, v2, Lcom/facebook/messaging/model/payment/PaymentRequestData;->d:I

    move v8, v8

    .line 2214118
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214119
    const-string v7, "currency"

    .line 2214120
    iget-object v8, v2, Lcom/facebook/messaging/model/payment/PaymentRequestData;->e:Ljava/lang/String;

    move-object v8, v8

    .line 2214121
    invoke-virtual {v6, v7, v8}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214122
    invoke-virtual {v6}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 2214123
    :cond_28
    :try_start_12
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214124
    const-string v4, "app_id"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214125
    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_29

    .line 2214126
    const-string v4, "app_name"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214127
    :cond_29
    const-string v4, "app_key_hash"

    invoke-virtual {v2}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2214128
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :cond_2a
    :try_start_13
    sget-object v3, Lcom/facebook/messaging/model/messages/GenericAdminMessageInfo;->a:LX/0Rh;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLExtensibleMessageAdminTextType;

    invoke-virtual {v3, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    goto/16 :goto_6
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_2b
    :try_start_14
    const/4 v3, 0x0

    goto/16 :goto_7
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 2214129
    :cond_2c
    :try_start_15
    new-instance v4, LX/162;

    sget-object v3, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v3}, LX/162;-><init>(LX/0mC;)V

    .line 2214130
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_e
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;

    .line 2214131
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 2214132
    const-string v7, "type"

    iget-object v2, v3, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->a:LX/5do;

    iget v2, v2, LX/5do;->value:I

    invoke-virtual {v6, v7, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214133
    const-string v7, "offset"

    iget v2, v3, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->b:I

    invoke-virtual {v6, v7, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214134
    const-string v7, "length"

    iget v2, v3, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->c:I

    invoke-virtual {v6, v7, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2214135
    const-string v7, "data"

    iget-object v2, v3, Lcom/facebook/messaging/model/messagemetadata/MessageMetadataAtTextRange;->d:Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;

    invoke-interface {v2}, Lcom/facebook/messaging/model/messagemetadata/MessageMetadata;->a()LX/0m9;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2214136
    move-object v3, v6

    .line 2214137
    invoke-virtual {v4, v3}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_e

    .line 2214138
    :cond_2d
    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_8
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 2214139
    :cond_2e
    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/5dt;->a(LX/0P1;Z)LX/0m9;

    move-result-object v3

    move-object v3, v3

    .line 2214140
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_9

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/messages/Message;
    .locals 5

    .prologue
    .line 2213802
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    move-result-object v0

    .line 2213803
    iput-object p0, v0, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2213804
    move-object v0, v0

    .line 2213805
    sget-object v1, LX/2uW;->BEFORE_FIRST_SENTINEL:LX/2uW;

    .line 2213806
    iput-object v1, v0, LX/6f7;->l:LX/2uW;

    .line 2213807
    move-object v0, v0

    .line 2213808
    const-wide/16 v2, 0x0

    .line 2213809
    iput-wide v2, v0, LX/6f7;->c:J

    .line 2213810
    move-object v0, v0

    .line 2213811
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method private static e(LX/FDs;Lcom/facebook/messaging/service/model/FetchThreadListResult;)V
    .locals 4

    .prologue
    .line 2213796
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2213797
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v0, v1

    .line 2213798
    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    invoke-static {p0, v0, v2, v3}, LX/FDs;->a(LX/FDs;LX/0Px;J)V

    .line 2213799
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    if-eqz v0, :cond_0

    .line 2213800
    iget-object v0, p0, LX/FDs;->f:LX/3N0;

    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/3N0;->a(Ljava/util/List;)V

    .line 2213801
    :cond_0
    return-void
.end method

.method private e(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 8

    .prologue
    .line 2213784
    const-string v0, "DbInsertThreadsHandler.updateMessageReactionsTable"

    const v1, 0x10cc0d23

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213785
    :try_start_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2213786
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2213787
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->T:LX/18f;

    invoke-virtual {v0}, LX/18f;->e()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2213788
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->T:LX/18f;

    invoke-virtual {v1, v0}, LX/18f;->h(Ljava/lang/Object;)LX/0Py;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserKey;

    .line 2213789
    sget-object v6, LX/6cs;->a:Ljava/lang/String;

    iget-object v7, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213790
    sget-object v6, LX/6cs;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213791
    sget-object v1, LX/6cs;->c:Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213792
    const-string v1, "message_reactions"

    const/4 v6, 0x0

    const v7, 0x1edf718f

    invoke-static {v7}, LX/03h;->a(I)V

    invoke-virtual {v2, v1, v6, v3}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v1, -0x41ce72fd

    invoke-static {v1}, LX/03h;->a(I)V

    .line 2213793
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2213794
    :catchall_0
    move-exception v0

    const v1, 0x101c38cb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const v0, 0x4dbae9d0    # 3.91985664E8f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2213795
    return-void
.end method

.method private f(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2213779
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v2, LX/2uW;->PENDING_SEND:LX/2uW;

    if-eq v0, v2, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v2, LX/2uW;->FAILED_SEND:LX/2uW;

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 2213780
    :goto_0
    return v0

    .line 2213781
    :cond_1
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2213782
    iget-object v0, p0, LX/FDs;->x:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2213783
    if-eqz v0, :cond_2

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;J)Lcom/facebook/messaging/model/messages/Message;
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 2213750
    const-string v0, "DbInsertThreadsHandler.handleSentMessage"

    const v1, -0x47f203b4

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213751
    const/4 v1, 0x0

    .line 2213752
    :try_start_0
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/2N4;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2213753
    if-nez v2, :cond_0

    .line 2213754
    iget-object v0, p0, LX/FDs;->C:LX/03V;

    const-string v2, "sent_message_not_in_db"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received DeltaSentMessage for message that is not in the db with expected offlineThreadingId. Expected offlineThreadingId "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2213755
    const v0, -0x578b0469

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2213756
    :goto_0
    return-object p1

    .line 2213757
    :cond_0
    :try_start_1
    iget-wide v4, p1, Lcom/facebook/messaging/model/messages/Message;->d:J

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    iget-wide v4, v2, Lcom/facebook/messaging/model/messages/Message;->d:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    .line 2213758
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    iget-wide v4, v2, Lcom/facebook/messaging/model/messages/Message;->d:J

    .line 2213759
    iput-wide v4, v0, LX/6f7;->d:J

    .line 2213760
    move-object v0, v0

    .line 2213761
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object p1

    .line 2213762
    :cond_1
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 2213763
    const v0, 0x43eb5ce4

    :try_start_2
    invoke-static {v7, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2213764
    invoke-static {p1, v2}, LX/FDs;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2213765
    iget-object v0, v2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 2213766
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-static {p0, v2}, LX/FDs;->a(LX/FDs;Ljava/util/Set;)V

    .line 2213767
    invoke-static {p0, v1}, LX/FDs;->d(LX/FDs;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2213768
    iget-object v0, p0, LX/FDs;->F:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    sget-object v6, LX/6jT;->a:LX/6jT;

    move-object v0, p0

    move-wide v2, p2

    invoke-static/range {v0 .. v6}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/messages/Message;JJLX/6jT;)V

    .line 2213769
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {p0, v0, v2, v3}, LX/FDs;->b(LX/FDs;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2213770
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2213771
    if-eqz v7, :cond_2

    .line 2213772
    const v0, -0x34a64fcd    # -1.4266419E7f

    :try_start_3
    invoke-static {v7, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2213773
    :cond_2
    const v0, -0x10596dba

    invoke-static {v0}, LX/02m;->a(I)V

    move-object p1, v1

    .line 2213774
    goto :goto_0

    .line 2213775
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_3

    .line 2213776
    const v2, -0x52e4b13a

    :try_start_4
    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    :cond_3
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2213777
    :catchall_1
    move-exception v0

    const v1, -0x4aeb6492

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2213778
    :catchall_2
    move-exception v0

    move-object v1, v7

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2213743
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2213744
    const-string v0, "thread_key"

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213745
    const-string v0, "pic_hash"

    invoke-virtual {v1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213746
    const-string v0, "pic"

    invoke-virtual {v1, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213747
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2213748
    const-string v2, "threads"

    const-string v3, "thread_key=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2213749
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    invoke-virtual {v0, p1, v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;JJ)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 4

    .prologue
    .line 2213735
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v0

    .line 2213736
    iput-object p2, v0, LX/6g6;->P:Lcom/facebook/messaging/model/threads/ThreadRtcCallInfoData;

    .line 2213737
    move-object v0, v0

    .line 2213738
    iput-wide p5, v0, LX/6g6;->V:J

    .line 2213739
    move-object v0, v0

    .line 2213740
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2213741
    invoke-static {p0, v0, p3, p4}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2213742
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/DeleteMessagesParams;JZ)Lcom/facebook/messaging/service/model/DeleteMessagesResult;
    .locals 12

    .prologue
    const-wide/16 v2, -0x1

    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 2213719
    iget-object v0, p1, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2213720
    iget-object v1, p0, LX/FDs;->d:LX/2N4;

    invoke-virtual {v1, v0}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2213721
    if-nez v0, :cond_0

    .line 2213722
    sget-object v2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->a:Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    .line 2213723
    :goto_0
    return-object v2

    .line 2213724
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2213725
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v10

    .line 2213726
    new-instance v7, LX/0UE;

    invoke-direct {v7}, LX/0UE;-><init>()V

    .line 2213727
    iget-object v0, p1, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    invoke-direct {p0, v0, v10, v7}, LX/FDs;->a(Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;)V

    .line 2213728
    iget-object v0, p1, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    invoke-static {p0, v0}, LX/FDs;->a(LX/FDs;Ljava/util/Set;)V

    .line 2213729
    invoke-direct {p0, v1, p2, p3}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2213730
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJI)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    move-result-object v0

    .line 2213731
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p4, :cond_1

    .line 2213732
    invoke-virtual {p0, v1}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    move v8, v6

    .line 2213733
    :goto_1
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    invoke-virtual {v0, v1, v9}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2213734
    new-instance v2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    iget-object v5, p1, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    move-object v4, v1

    move-object v6, v10

    invoke-direct/range {v2 .. v8}, Lcom/facebook/messaging/service/model/DeleteMessagesResult;-><init>(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;Z)V

    goto :goto_0

    :cond_1
    move v8, v9

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 6

    .prologue
    .line 2213718
    const/4 v4, 0x1

    sget-object v5, LX/6jT;->a:LX/6jT;

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JZLX/6jT;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;JLX/6jT;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 6

    .prologue
    .line 2213717
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;JZLX/6jT;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;JZLX/6jT;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    .line 2213681
    const-string v0, "DbInsertThreadsHandler.handleNewMessageResult"

    const v1, 0xa0c75c8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213682
    :try_start_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 2213683
    const v0, -0x67b5eded

    invoke-static {v7, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2213684
    :try_start_1
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213685
    if-nez v1, :cond_0

    .line 2213686
    const v0, 0x754f2b5a

    :try_start_2
    invoke-static {v7, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213687
    const v0, -0x192fee60

    invoke-static {v0}, LX/02m;->a(I)V

    :goto_0
    return-object p1

    .line 2213688
    :cond_0
    :try_start_3
    iget-object v8, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2213689
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2213690
    if-nez v0, :cond_1

    invoke-static {v1}, LX/2Mk;->r(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2213691
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/2N4;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2213692
    :cond_1
    iget-wide v2, v1, Lcom/facebook/messaging/model/messages/Message;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    if-eqz v0, :cond_2

    iget-wide v2, v0, Lcom/facebook/messaging/model/messages/Message;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 2213693
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v1

    iget-wide v2, v0, Lcom/facebook/messaging/model/messages/Message;->d:J

    invoke-virtual {v1, v2, v3}, LX/6f7;->b(J)LX/6f7;

    move-result-object v1

    invoke-virtual {v1}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2213694
    :cond_2
    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->q:LX/6f2;

    sget-object v3, LX/6f2;->C2DM:LX/6f2;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-ne v2, v3, :cond_3

    if-eqz v0, :cond_3

    .line 2213695
    const v0, -0x6fd7b082

    :try_start_4
    invoke-static {v7, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2213696
    const v0, 0x20dfb776

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 p1, 0x0

    goto :goto_0

    .line 2213697
    :cond_3
    :try_start_5
    invoke-static {v1}, LX/2Mk;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, v1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v0, :cond_7

    .line 2213698
    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v0, v0

    .line 2213699
    if-eqz v0, :cond_7

    .line 2213700
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->b:Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-object v0, v0

    .line 2213701
    invoke-static {p0, v0}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/messages/MessagesCollection;)LX/0Px;

    .line 2213702
    :cond_5
    :goto_1
    invoke-direct {p0, v1}, LX/FDs;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2213703
    iget-wide v2, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {p0, v8, v2, v3}, LX/FDs;->b(LX/FDs;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2213704
    :goto_2
    invoke-direct {p0, v8, p1}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/service/model/NewMessageResult;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object p1

    .line 2213705
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2213706
    if-nez v0, :cond_6

    invoke-direct {p0, v1}, LX/FDs;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2213707
    iget-object v0, p0, LX/FDs;->C:LX/03V;

    const-string v1, "handleNewMessageResult_null_message"

    const-string v2, "refetchMostRecentMessages returned null new message"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213708
    :cond_6
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2213709
    const v0, -0x3b0ca7c6

    :try_start_6
    invoke-static {v7, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2213710
    const v0, 0x54ff820a

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 2213711
    :cond_7
    :try_start_7
    invoke-direct {p0, v1, p4}, LX/FDs;->a(Lcom/facebook/messaging/model/messages/Message;Z)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2213712
    if-nez v0, :cond_5

    .line 2213713
    invoke-virtual {p1}, Lcom/facebook/fbservice/results/BaseResult;->getClientTimeMs()J

    move-result-wide v4

    move-object v0, p0

    move-wide v2, p2

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/messages/Message;JJLX/6jT;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 2213714
    :catchall_0
    move-exception v0

    const v1, 0x2fa1e52c

    :try_start_8
    invoke-static {v7, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2213715
    :catchall_1
    move-exception v0

    const v1, 0x2f953a11

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2213716
    :cond_8
    :try_start_9
    iget-object v0, p0, LX/FDs;->E:Lcom/facebook/messaging/media/download/MediaDownloadManager;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/media/download/MediaDownloadManager;->a(LX/0Px;)Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 8

    .prologue
    .line 2213670
    const-string v0, "DbInsertThreadsHandler.handleInsertLocalAdminMessage"

    const v1, 0x21f5d36b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213671
    :try_start_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 2213672
    const v0, -0x29676373

    invoke-static {v7, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2213673
    :try_start_1
    invoke-static {p0, p1}, LX/FDs;->c(LX/FDs;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    .line 2213674
    const-wide/16 v2, -0x1

    iget-wide v4, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    sget-object v6, LX/6jT;->a:LX/6jT;

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/messages/Message;JJLX/6jT;)V

    .line 2213675
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213676
    const v0, -0x2e8d47e4

    :try_start_2
    invoke-static {v7, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213677
    const v0, 0x345f3ab8

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2213678
    return-void

    .line 2213679
    :catchall_0
    move-exception v0

    const v1, 0x33cad687

    :try_start_3
    invoke-static {v7, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2213680
    :catchall_1
    move-exception v0

    const v1, 0x13755c9b

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 1

    .prologue
    .line 2213606
    if-nez p1, :cond_0

    .line 2213607
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2213608
    :goto_0
    invoke-static {p0, v0}, LX/FDs;->b(LX/FDs;LX/0Px;)V

    .line 2213609
    return-void

    .line 2213610
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/MessageDraft;)V
    .locals 7
    .param p2    # Lcom/facebook/messaging/model/messages/MessageDraft;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2214142
    const-string v0, "DbInsertThreadsHandler.handleUpdateDraft"

    const v1, -0x61cac6f2

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214143
    :try_start_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2214144
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2214145
    const-string v2, "draft"

    iget-object v3, p0, LX/FDs;->p:LX/2NA;

    invoke-virtual {v3, p2}, LX/2NA;->a(Lcom/facebook/messaging/model/messages/MessageDraft;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214146
    const-string v2, "threads"

    const-string v3, "thread_key=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214147
    const v0, -0x1d718b83

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2214148
    return-void

    .line 2214149
    :catchall_0
    move-exception v0

    const v1, 0x1fe33142

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;JJJ)V
    .locals 11

    .prologue
    .line 2214150
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    invoke-static/range {v0 .. v9}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;ZJJJ)V

    .line 2214151
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/DeliveredReceiptParams;)V
    .locals 12

    .prologue
    .line 2214152
    iget-object v0, p1, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->b:Lcom/facebook/user/model/UserKey;

    move-object v2, v0

    .line 2214153
    iget-wide v10, p1, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->d:J

    move-wide v4, v10

    .line 2214154
    iget-wide v10, p1, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->e:J

    move-wide v8, v10

    .line 2214155
    iget-object v0, p1, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v0

    .line 2214156
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v9}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;ZJJJ)V

    .line 2214157
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;J)V
    .locals 6

    .prologue
    .line 2214158
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->d:Z

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2214159
    if-eqz v0, :cond_0

    .line 2214160
    :goto_1
    return-void

    .line 2214161
    :cond_0
    iget-object v0, p0, LX/FDs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    .line 2214162
    sget-object v1, LX/6cx;->b:LX/2bA;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/48u;->b(LX/0To;J)V

    .line 2214163
    sget-object v1, LX/6cx;->c:LX/2bA;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/48u;->b(LX/0To;J)V

    .line 2214164
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2214165
    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214166
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2214167
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2214168
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/FDs;->a(LX/FDs;Ljava/util/List;)V

    .line 2214169
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-static {p0, v0, p2, p3}, LX/FDs;->a(LX/FDs;LX/0Px;J)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;)V
    .locals 4

    .prologue
    .line 2214170
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2214171
    const v0, 0x50980c82

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214172
    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v0, v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-eqz v0, :cond_0

    .line 2214173
    iget-object v0, p0, LX/FDs;->w:LX/6fD;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v3, p2, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0, v2, v3}, LX/6fD;->c(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2214174
    const v0, 0x5616dcbe

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214175
    :goto_0
    return-void

    .line 2214176
    :cond_0
    :try_start_1
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {p0, v0}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/messages/MessagesCollection;)LX/0Px;

    .line 2214177
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214178
    const v0, 0x63c792a6

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    .line 2214179
    :catch_0
    move-exception v0

    .line 2214180
    :try_start_2
    sget-object v2, LX/FDs;->a:Ljava/lang/Class;

    const-string v3, "SQLException"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214181
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2214182
    :catchall_0
    move-exception v0

    const v2, -0x3a160bf7

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;)V
    .locals 7

    .prologue
    .line 2214183
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2214184
    const v0, -0xbc29b0e

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214185
    :try_start_0
    iget-wide v4, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->e:J

    .line 2214186
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2214187
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->b:LX/6ek;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {p0, v1, v3}, LX/FDs;->a(LX/FDs;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;)V

    .line 2214188
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v3, v1

    .line 2214189
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214190
    invoke-static {p0, v0, v4, v5}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2214191
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2214192
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->d:LX/0Px;

    if-eqz v0, :cond_1

    .line 2214193
    iget-object v0, p0, LX/FDs;->f:LX/3N0;

    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/3N0;->a(Ljava/util/List;)V

    .line 2214194
    :cond_1
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214195
    const v0, -0x4a1d9120

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214196
    return-void

    .line 2214197
    :catch_0
    move-exception v0

    .line 2214198
    :try_start_1
    sget-object v1, LX/FDs;->a:Ljava/lang/Class;

    const-string v3, "SQLException"

    invoke-static {v1, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214199
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214200
    :catchall_0
    move-exception v0

    const v1, 0x76711333

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2214201
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2214202
    const v0, -0x142ad959

    invoke-static {v2, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214203
    :try_start_0
    iget-object v0, p0, LX/FDs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    .line 2214204
    const-string v3, "folders"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214205
    const-string v3, "pinned_threads"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214206
    sget-object v3, LX/6cx;->b:LX/2bA;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/48u;->a(LX/0To;I)V

    .line 2214207
    sget-object v3, LX/6cx;->c:LX/2bA;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, LX/48u;->a(LX/0To;I)V

    .line 2214208
    sget-object v3, LX/6cx;->i:LX/2bA;

    invoke-virtual {v0, v3}, LX/48u;->b(LX/0To;)V

    .line 2214209
    sget-object v3, LX/6cx;->j:LX/2bA;

    invoke-virtual {v0, v3}, LX/48u;->b(LX/0To;)V

    .line 2214210
    const-string v0, "threads"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214211
    const-string v0, "msg_type"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, LX/2uW;->PENDING_SEND:LX/2uW;

    iget v5, v5, LX/2uW;->dbKeyValue:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, LX/2uW;->FAILED_SEND:LX/2uW;

    iget v5, v5, LX/2uW;->dbKeyValue:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 2214212
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2214213
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2214214
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v5, v5

    .line 2214215
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214216
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2214217
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2214218
    :cond_0
    const-string v0, "thread_key"

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v0

    .line 2214219
    const/4 v1, 0x2

    new-array v1, v1, [LX/0ux;

    const/4 v4, 0x0

    aput-object v3, v1, v4

    const/4 v3, 0x1

    aput-object v0, v1, v3

    invoke-static {v1}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v0

    invoke-static {v0}, LX/0uu;->a(LX/0ux;)LX/0ux;

    move-result-object v0

    .line 2214220
    const-string v1, "messages"

    invoke-virtual {v0}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214221
    iget-object v0, p0, LX/FDs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    sget-object v1, LX/6ek;->MONTAGE:LX/6ek;

    invoke-static {v1}, LX/6cx;->a(LX/6ek;)LX/2bA;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/48u;->b(LX/0To;)V

    .line 2214222
    invoke-virtual {p0, p1}, LX/FDs;->b(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2214223
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214224
    const v0, 0x23d13830

    invoke-static {v2, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214225
    return-void

    .line 2214226
    :catchall_0
    move-exception v0

    const v1, -0x42923cd5

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 3

    .prologue
    .line 2214227
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2214228
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v1, v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-nez v1, :cond_0

    .line 2214229
    const/4 v0, 0x0

    .line 2214230
    :cond_0
    invoke-virtual {p0, v0, p1}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2214231
    return-void
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadResult;Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;)V
    .locals 4

    .prologue
    .line 2214232
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2214233
    const v0, 0x7190bc8e

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214234
    :try_start_0
    iget-object v0, p0, LX/FDs;->w:LX/6fD;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v3, p2, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0, v2, v3}, LX/6fD;->c(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2214235
    const v0, 0x158b534f

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214236
    :goto_0
    return-void

    .line 2214237
    :cond_0
    :try_start_1
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {p0, v0}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/messages/MessagesCollection;)LX/0Px;

    .line 2214238
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214239
    const v0, 0x6bde008b

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto :goto_0

    .line 2214240
    :catch_0
    move-exception v0

    .line 2214241
    :try_start_2
    sget-object v2, LX/FDs;->a:Ljava/lang/Class;

    const-string v3, "SQLException"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214242
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2214243
    :catchall_0
    move-exception v0

    const v2, -0x6a04e2e3

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadResult;Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 12

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 2214244
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2214245
    const v0, -0x59b28c11

    invoke-static {v4, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214246
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2214247
    :try_start_0
    iget-object v5, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2214248
    if-eqz p1, :cond_0

    iget-object v6, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v6, v6, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-eqz v6, :cond_0

    .line 2214249
    iget-object v6, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    if-eqz v6, :cond_0

    .line 2214250
    iget-object v6, p0, LX/FDs;->w:LX/6fD;

    iget-object v7, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v8, p1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v6, v7, v8}, LX/6fD;->c(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2214251
    const-string v6, "messages"

    const-string v7, "thread_key=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v4, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214252
    :cond_0
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v6

    iget-wide v8, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->c:J

    .line 2214253
    iput-wide v8, v6, LX/6g6;->G:J

    .line 2214254
    move-object v0, v6

    .line 2214255
    const/4 v6, 0x1

    .line 2214256
    iput-boolean v6, v0, LX/6g6;->H:Z

    .line 2214257
    move-object v0, v0

    .line 2214258
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2214259
    invoke-static {p0, v0}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2214260
    iget-wide v6, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-static {p0, v0, v6, v7}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2214261
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    if-eqz v0, :cond_1

    .line 2214262
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {p0, v0}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/messages/MessagesCollection;)LX/0Px;

    .line 2214263
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2214264
    iget-object v6, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v6, v6

    .line 2214265
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    :goto_0
    if-ge v1, v7, :cond_4

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2214266
    invoke-direct {p0, v0}, LX/FDs;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2214267
    iget-wide v0, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 2214268
    :goto_1
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 2214269
    invoke-static {p0, v5, v0, v1}, LX/FDs;->b(LX/FDs;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2214270
    :cond_1
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    if-eqz v0, :cond_2

    .line 2214271
    iget-object v0, p0, LX/FDs;->f:LX/3N0;

    iget-object v1, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    invoke-virtual {v0, v1}, LX/3N0;->a(Ljava/util/List;)V

    .line 2214272
    :cond_2
    invoke-direct {p0, p2}, LX/FDs;->b(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2214273
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214274
    const v0, -0x249131b4

    invoke-static {v4, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214275
    return-void

    .line 2214276
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2214277
    :catch_0
    move-exception v0

    .line 2214278
    :try_start_1
    sget-object v1, LX/FDs;->a:Ljava/lang/Class;

    const-string v2, "SQLException"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214279
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214280
    :catchall_0
    move-exception v0

    const v1, 0x5177bcec

    invoke-static {v4, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    :cond_4
    move-wide v0, v2

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/service/model/MarkThreadsParams;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2214281
    iget-object v0, p1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2214282
    :goto_0
    return-void

    .line 2214283
    :cond_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2214284
    const v0, -0x67ac5aa0

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214285
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    .line 2214286
    sget-object v2, LX/6iW;->ARCHIVED:LX/6iW;

    if-eq v0, v2, :cond_1

    sget-object v2, LX/6iW;->SPAM:LX/6iW;

    if-ne v0, v2, :cond_5

    .line 2214287
    :cond_1
    new-instance v4, LX/4yA;

    invoke-direct {v4}, LX/4yA;-><init>()V

    .line 2214288
    iget-object v5, p1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_4

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2214289
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v1, :cond_3

    .line 2214290
    iget-object v1, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->f:LX/6ek;

    .line 2214291
    if-nez v1, :cond_2

    .line 2214292
    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    .line 2214293
    :cond_2
    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v1, v0}, LX/4yA;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4yA;

    .line 2214294
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2214295
    :cond_4
    invoke-virtual {v4}, LX/4yA;->a()LX/3CE;

    move-result-object v1

    .line 2214296
    invoke-virtual {v1}, LX/18f;->e()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ek;

    .line 2214297
    invoke-virtual {v1, v0}, LX/3CE;->e(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-static {p0, v0, v4}, LX/FDs;->a(LX/FDs;LX/6ek;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 2214298
    :catch_0
    move-exception v0

    .line 2214299
    :try_start_1
    sget-object v1, LX/FDs;->a:Ljava/lang/Class;

    const-string v2, "SQLException"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214300
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2214301
    :catchall_0
    move-exception v0

    const v1, 0x3a57020d

    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 2214302
    :cond_5
    :try_start_2
    sget-object v2, LX/6iW;->READ:LX/6iW;

    if-ne v0, v2, :cond_9

    .line 2214303
    iget-object v2, p1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    :goto_3
    if-ge v1, v4, :cond_7

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2214304
    iget-boolean v5, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v5, :cond_6

    .line 2214305
    iget-object v5, p0, LX/FDs;->I:LX/3RE;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2214306
    if-nez v0, :cond_b

    .line 2214307
    :cond_6
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2214308
    :cond_7
    iget-object v0, p1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 2214309
    iget-object v0, p1, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    const-wide/16 v9, 0x0

    .line 2214310
    iget-object v7, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/6dQ;

    invoke-virtual {v7}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 2214311
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 2214312
    iget-boolean v7, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v7, :cond_c

    iget-wide v7, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    .line 2214313
    :goto_5
    iget-boolean v13, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v13, :cond_d

    .line 2214314
    :goto_6
    const-string v13, "last_read_timestamp_ms"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v12, v13, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214315
    const-string v7, "unread_message_count"

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v12, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214316
    iget-wide v7, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    const-wide/16 v9, -0x1

    cmp-long v7, v7, v9

    if-eqz v7, :cond_8

    .line 2214317
    const-string v7, "sequence_id"

    iget-wide v9, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v12, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2214318
    :cond_8
    const-string v7, "threads"

    const-string v8, "thread_key=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v13, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v13}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v9, v10

    invoke-virtual {v11, v7, v12, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2214319
    :cond_9
    :goto_7
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2214320
    const v0, 0x36a96f41

    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    goto/16 :goto_0

    .line 2214321
    :cond_a
    :try_start_3
    invoke-direct {p0, p1}, LX/FDs;->b(Lcom/facebook/messaging/service/model/MarkThreadsParams;)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_7

    .line 2214322
    :cond_b
    :try_start_4
    iget-object v6, v5, LX/3RE;->a:LX/13O;

    const-string v7, "chat_head_mute_state"

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/13O;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_c
    move-wide v7, v9

    .line 2214323
    goto :goto_5

    .line 2214324
    :cond_d
    const-wide/16 v9, 0x1

    goto :goto_6
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 10

    .prologue
    .line 2214325
    const-string v0, "DbInsertThreadsHandler.handleInsertPendingOrFailedSentMessage"

    const v1, -0x680b9b69

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2214326
    :try_start_0
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    .line 2214327
    const v0, -0x3d99b380

    invoke-static {v9, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2214328
    :try_start_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2214329
    iget-object v1, p0, LX/FDs;->d:LX/2N4;

    invoke-virtual {v1, v0}, LX/2N4;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2214330
    invoke-static {p1, v0}, LX/DdV;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2214331
    const v0, -0x6933e872

    :try_start_2
    invoke-static {v9, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2214332
    const v0, -0x702b618a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2214333
    :goto_0
    return-void

    .line 2214334
    :cond_0
    :try_start_3
    invoke-static {p0, p1}, LX/FDs;->c(LX/FDs;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    .line 2214335
    iget-object v0, p0, LX/FDs;->d:LX/2N4;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2214336
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_1

    .line 2214337
    iget-object v1, p0, LX/FDs;->y:LX/DdV;

    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    const-wide/16 v4, -0x1

    sget-object v6, LX/DdQ;->MESSAGE_ADDED:LX/DdQ;

    sget-object v7, LX/6jT;->a:LX/6jT;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    move-object v3, p1

    invoke-virtual/range {v1 .. v8}, LX/DdV;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/Message;JLX/DdQ;LX/6jT;Ljava/lang/Boolean;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2214338
    invoke-static {p0, v1}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2214339
    iget-wide v2, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-static {p0, v1, v2, v3}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2214340
    :cond_1
    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2214341
    const v0, 0x7ce56c52

    :try_start_4
    invoke-static {v9, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2214342
    const v0, 0x1f5c04e4

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2214343
    :catchall_0
    move-exception v0

    const v1, -0x3efca53d

    :try_start_5
    invoke-static {v9, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2214344
    :catchall_1
    move-exception v0

    const v1, 0x5fcc9c7e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final b(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2214345
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2214346
    const v0, -0x6c113649

    invoke-static {v3, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214347
    :try_start_0
    iget-wide v4, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    .line 2214348
    iget-object v6, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    .line 2214349
    iget-object v0, p0, LX/FDs;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cy;

    .line 2214350
    invoke-static {v6}, LX/6cx;->a(LX/6ek;)LX/2bA;

    move-result-object v2

    invoke-virtual {v0, v2, v4, v5}, LX/48u;->b(LX/0To;J)V

    .line 2214351
    invoke-static {v6}, LX/6cx;->b(LX/6ek;)LX/2bA;

    move-result-object v2

    iget-wide v8, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->j:J

    invoke-virtual {v0, v2, v8, v9}, LX/48u;->b(LX/0To;J)V

    .line 2214352
    invoke-static {v6}, LX/6cx;->c(LX/6ek;)LX/2bA;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual {v0, v2, v7}, LX/48u;->b(LX/0To;Z)V

    .line 2214353
    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->i:Z

    if-eqz v0, :cond_3

    .line 2214354
    iget-object v7, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->e:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_1

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2214355
    iget-object v9, p0, LX/FDs;->d:LX/2N4;

    invoke-virtual {v9, v0}, LX/2N4;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2214356
    if-eqz v0, :cond_0

    .line 2214357
    if-nez v0, :cond_7

    .line 2214358
    sget-object v9, LX/0Q7;->a:LX/0Px;

    move-object v9, v9

    .line 2214359
    :goto_1
    invoke-static {p0, v9}, LX/FDs;->b(LX/FDs;LX/0Px;)V

    .line 2214360
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2214361
    :cond_1
    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->f:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    :goto_2
    if-ge v1, v7, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2214362
    iget-object v8, p0, LX/FDs;->d:LX/2N4;

    invoke-virtual {v8, v0}, LX/2N4;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2214363
    if-eqz v0, :cond_2

    .line 2214364
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    invoke-static {p0, v6, v8}, LX/FDs;->a(LX/FDs;LX/6ek;Ljava/util/List;)V

    .line 2214365
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2214366
    :cond_3
    invoke-direct {p0, v6}, LX/FDs;->a(LX/6ek;)V

    .line 2214367
    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    invoke-static {p0, v6, v0}, LX/FDs;->a(LX/FDs;LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V

    .line 2214368
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {p0, v6, v0}, LX/FDs;->a(LX/FDs;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;)V

    .line 2214369
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2214370
    iget-boolean v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->i:Z

    if-eqz v1, :cond_6

    .line 2214371
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v0, v1

    .line 2214372
    const/4 v1, 0x1

    invoke-static {p0, v0, v4, v5, v1}, LX/FDs;->a(LX/FDs;LX/0Px;JZ)V

    .line 2214373
    :goto_3
    iget-object v0, p0, LX/FDs;->f:LX/3N0;

    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/3N0;->a(Ljava/util/List;)V

    .line 2214374
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    if-eqz v0, :cond_5

    .line 2214375
    iget-object v0, p0, LX/FDs;->J:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0db;->aD:LX/0Tn;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a()J

    move-result-wide v4

    invoke-interface {v0, v1, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2214376
    iget-object v0, p0, LX/FDs;->A:LX/FJx;

    .line 2214377
    iget-object v1, v0, LX/FJx;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/messaging/prefs/notifications/GlobalNotificationPrefsSyncUtil$2;

    sget-object v4, LX/FJx;->a:Ljava/lang/Class;

    const-string v5, "synchronizeAfterServerChange"

    invoke-direct {v2, v0, v4, v5}, Lcom/facebook/messaging/prefs/notifications/GlobalNotificationPrefsSyncUtil$2;-><init>(LX/FJx;Ljava/lang/Class;Ljava/lang/String;)V

    const v4, -0x1f8c6815

    invoke-static {v1, v2, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2214378
    :cond_5
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214379
    const v0, -0x5e1a00f8

    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214380
    return-void

    .line 2214381
    :cond_6
    :try_start_1
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v0, v1

    .line 2214382
    invoke-static {p0, v0, v4, v5}, LX/FDs;->a(LX/FDs;LX/0Px;J)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 2214383
    :catch_0
    move-exception v0

    .line 2214384
    :try_start_2
    sget-object v1, LX/FDs;->a:Ljava/lang/Class;

    const-string v2, "SQLException"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2214385
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2214386
    :catchall_0
    move-exception v0

    const v1, 0x7ea0e184

    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 2214387
    :cond_7
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v9

    goto/16 :goto_1
.end method

.method public final c(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V
    .locals 4

    .prologue
    .line 2214388
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2214389
    const v0, 0x55d9a83

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214390
    :try_start_0
    sget-object v0, LX/6cx;->i:LX/2bA;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    invoke-static {p0, v0, v2, v3}, LX/FDs;->a(LX/FDs;LX/2bA;J)V

    .line 2214391
    invoke-static {p0, p1}, LX/FDs;->e(LX/FDs;Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2214392
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214393
    const v0, 0x7871bb5a

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214394
    return-void

    .line 2214395
    :catchall_0
    move-exception v0

    const v2, -0x5e823f3b

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final d(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V
    .locals 4

    .prologue
    .line 2214396
    iget-object v0, p0, LX/FDs;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2214397
    const v0, 0x224debdd

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214398
    :try_start_0
    sget-object v0, LX/6cx;->j:LX/2bA;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    invoke-static {p0, v0, v2, v3}, LX/FDs;->a(LX/FDs;LX/2bA;J)V

    .line 2214399
    invoke-static {p0, p1}, LX/FDs;->e(LX/FDs;Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2214400
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2214401
    const v0, -0x3cd77f0a

    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2214402
    return-void

    .line 2214403
    :catchall_0
    move-exception v0

    const v2, -0x31e57f57

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
