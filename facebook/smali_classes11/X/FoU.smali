.class public final LX/FoU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;)V
    .locals 0

    .prologue
    .line 2289533
    iput-object p1, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2289534
    iget-object v0, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    invoke-virtual {v0, p3}, LX/Fob;->a(I)LX/Foa;

    move-result-object v0

    .line 2289535
    iget-object v1, v0, LX/Foa;->a:LX/FoZ;

    move-object v0, v1

    .line 2289536
    sget-object v1, LX/FoZ;->SEE_MORE:LX/FoZ;

    if-ne v0, v1, :cond_2

    .line 2289537
    iget-object v0, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->e:LX/17Y;

    iget-object v1, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->ha:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2289538
    const-string v1, "extra_should_show_daf_disclosure"

    iget-object v2, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-boolean v2, v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->o:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2289539
    iget-object v1, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->p:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    if-eqz v1, :cond_0

    .line 2289540
    const-string v1, "extra_daf_disclosure_model"

    iget-object v2, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->p:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2289541
    :cond_0
    iget-object v1, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2289542
    const-string v1, "extra_charity_search_category"

    iget-object v2, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->m:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2289543
    const-string v1, "extra_charity_search_category_translated_name"

    iget-object v2, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2289544
    :cond_1
    if-eqz v0, :cond_2

    .line 2289545
    iget-object v1, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a:LX/BOa;

    .line 2289546
    iget-object v2, v1, LX/BOa;->a:LX/0Zb;

    const-string v3, "fundraiser_create_promo_see_more_charities"

    const/4 p1, 0x0

    invoke-static {v1, v3, p1}, LX/BOa;->b(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2289547
    iget-object v1, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->d:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x82

    iget-object v3, p0, LX/FoU;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2289548
    :cond_2
    return-void
.end method
