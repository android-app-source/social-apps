.class public LX/FqQ;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2293864
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2293865
    return-void
.end method

.method public static a(LX/2c3;LX/26s;Lcom/facebook/composer/publish/ComposerPublishServiceHandler;LX/G4S;)LX/1qM;
    .locals 3
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/timeline/service/TimelineSectionQueue;
    .end annotation

    .prologue
    .line 2293858
    new-instance v0, LX/2m1;

    new-instance v1, LX/2m1;

    new-instance v2, LX/2m1;

    invoke-direct {v2, p2, p3}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v1, p1, v2}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    invoke-direct {v0, p0, v1}, LX/2m1;-><init>(LX/26t;LX/1qM;)V

    return-object v0
.end method

.method public static b()Ljava/lang/Boolean;
    .locals 1
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/timeline/annotations/IsMleDeleteEnabled;
    .end annotation

    .prologue
    .line 2293863
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static getInstanceForTest_BitmapMemoryCache(LX/0QA;)LX/1Fg;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2293862
    invoke-static {p0}, LX/1t0;->a(LX/0QB;)LX/1Fg;

    move-result-object v0

    check-cast v0, LX/1Fg;

    return-object v0
.end method

.method public static getInstanceForTest_GraphQLCacheManager(LX/0QA;)LX/1mR;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2293866
    invoke-static {p0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v0

    check-cast v0, LX/1mR;

    return-object v0
.end method

.method public static getInstanceForTest_SimpleImageMemoryCache(LX/0QA;)LX/1Fg;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2293861
    invoke-static {p0}, LX/4eZ;->a(LX/0QB;)LX/1Fg;

    move-result-object v0

    check-cast v0, LX/1Fg;

    return-object v0
.end method

.method public static getInstanceForTest_TimelineDbCache(LX/0QA;)LX/Fqw;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2293860
    invoke-static {p0}, LX/Fqw;->a(LX/0QB;)LX/Fqw;

    move-result-object v0

    check-cast v0, LX/Fqw;

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2293859
    return-void
.end method
