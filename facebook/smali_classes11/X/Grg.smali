.class public LX/Grg;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CqV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/Ctg;

.field private c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

.field private d:Lcom/facebook/storelocator/StoreLocatorRecyclerView;


# direct methods
.method public constructor <init>(LX/Ctg;Lcom/facebook/storelocator/StoreLocatorMapDelegate;Lcom/facebook/storelocator/StoreLocatorRecyclerView;)V
    .locals 3

    .prologue
    .line 2398637
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398638
    const-class v0, LX/Grg;

    invoke-static {v0, p0}, LX/Grg;->a(Ljava/lang/Class;LX/02k;)V

    .line 2398639
    iput-object p1, p0, LX/Grg;->b:LX/Ctg;

    .line 2398640
    iput-object p2, p0, LX/Grg;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 2398641
    iput-object p3, p0, LX/Grg;->d:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 2398642
    iget-object v0, p0, LX/Grg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CqV;

    const/4 v1, 0x0

    sget-object v2, LX/CqU;->CANVAS_MAP:LX/CqU;

    invoke-virtual {v0, v1, v2}, LX/CqV;->a(ZLX/CqU;)V

    .line 2398643
    iget-object v0, p0, LX/Grg;->d:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/storelocator/StoreLocatorRecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2398644
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Grg;

    const/16 p0, 0x323e

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, LX/Grg;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(LX/CrS;)V
    .locals 3

    .prologue
    .line 2398631
    iget-object v0, p0, LX/Grg;->b:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 2398632
    iget-object v1, p0, LX/Cts;->a:LX/Ctg;

    move-object v1, v1

    .line 2398633
    iget-object v2, p0, LX/Grg;->d:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 2398634
    iget-object p0, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, p0

    .line 2398635
    invoke-interface {v1, v2, v0}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2398636
    return-void
.end method
