.class public final LX/GqJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CHX;

.field public final synthetic b:LX/CHX;

.field public final synthetic c:LX/GoE;

.field public final synthetic d:LX/GqK;


# direct methods
.method public constructor <init>(LX/GqK;LX/CHX;LX/CHX;LX/GoE;)V
    .locals 0

    .prologue
    .line 2396282
    iput-object p1, p0, LX/GqJ;->d:LX/GqK;

    iput-object p2, p0, LX/GqJ;->a:LX/CHX;

    iput-object p3, p0, LX/GqJ;->b:LX/CHX;

    iput-object p4, p0, LX/GqJ;->c:LX/GoE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x2

    const v3, -0x1c4a3267

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2396283
    iget-object v0, p0, LX/GqJ;->d:LX/GqK;

    iget-object v0, v0, LX/GqK;->p:LX/Gpd;

    .line 2396284
    iget-boolean v4, v0, LX/Gpd;->f:Z

    move v4, v4

    .line 2396285
    iget-object v0, p0, LX/GqJ;->d:LX/GqK;

    iget-object v5, v0, LX/GqK;->p:LX/Gpd;

    if-nez v4, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, LX/Gpd;->a(Z)V

    .line 2396286
    iget-object v0, p0, LX/GqJ;->d:LX/GqK;

    iget-object v5, v0, LX/GqK;->a:LX/GnF;

    iget-object v0, p0, LX/GqJ;->d:LX/GqK;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v6

    if-eqz v4, :cond_1

    iget-object v0, p0, LX/GqJ;->a:LX/CHX;

    :goto_1
    iget-object v7, p0, LX/GqJ;->c:LX/GoE;

    new-instance v8, LX/GqI;

    invoke-direct {v8, p0, v4}, LX/GqI;-><init>(LX/GqJ;Z)V

    invoke-virtual {v5, v6, v0, v7, v8}, LX/GnF;->a(Landroid/content/Context;LX/CHX;LX/GoE;Ljava/util/Map;)V

    .line 2396287
    iget-object v0, p0, LX/GqJ;->d:LX/GqK;

    iget-object v0, v0, LX/GqK;->b:LX/Chv;

    new-instance v5, LX/Gna;

    if-nez v4, :cond_2

    :goto_2
    invoke-direct {v5, v1}, LX/Gna;-><init>(Z)V

    invoke-virtual {v0, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2396288
    iget-object v0, p0, LX/GqJ;->d:LX/GqK;

    iget-object v0, v0, LX/GqK;->c:LX/1mR;

    iget-object v1, p0, LX/GqJ;->d:LX/GqK;

    iget-object v1, v1, LX/GqK;->f:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object v2, p0, LX/GqJ;->d:LX/GqK;

    iget-object v2, v2, LX/GqK;->e:LX/CIb;

    .line 2396289
    iget-object v4, v2, LX/CIb;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2396290
    invoke-virtual {v1, v2}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2396291
    const v0, -0x549c0e82

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    :cond_0
    move v0, v2

    .line 2396292
    goto :goto_0

    .line 2396293
    :cond_1
    iget-object v0, p0, LX/GqJ;->b:LX/CHX;

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2396294
    goto :goto_2
.end method
