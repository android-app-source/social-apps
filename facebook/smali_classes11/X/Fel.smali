.class public final LX/Fel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V
    .locals 0

    .prologue
    .line 2266554
    iput-object p1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x1e

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x330709f3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2266555
    iget-object v1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266556
    iget-object v2, v1, LX/Fje;->B:LX/EQG;

    move-object v1, v2

    .line 2266557
    sget-object v2, LX/EQG;->EMPTY:LX/EQG;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->h()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2266558
    iget-object v1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2266559
    iget-object v1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    sget-object v2, LX/EQG;->LOADING_MORE:LX/EQG;

    invoke-virtual {v1, v2}, LX/Fje;->setState(LX/EQG;)V

    .line 2266560
    :cond_0
    :goto_0
    iget-object v1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    invoke-interface {v1}, LX/0g8;->q()I

    move-result v1

    if-le v1, v3, :cond_1

    .line 2266561
    iget-object v1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->g(I)V

    .line 2266562
    :cond_1
    iget-object v1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/0g8;->d(I)V

    .line 2266563
    const v1, -0x515c9461

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2266564
    :cond_2
    iget-object v1, p0, LX/Fel;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    sget-object v2, LX/EQG;->LOADING_FINISHED:LX/EQG;

    invoke-virtual {v1, v2}, LX/Fje;->setState(LX/EQG;)V

    goto :goto_0
.end method
