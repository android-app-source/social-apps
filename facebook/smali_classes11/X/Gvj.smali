.class public LX/Gvj;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/6Gg;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6Gg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2406017
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2406018
    iput-object p1, p0, LX/Gvj;->a:Landroid/content/Context;

    .line 2406019
    iput-object p2, p0, LX/Gvj;->b:LX/6Gg;

    .line 2406020
    return-void
.end method

.method public static a(LX/0QB;)LX/Gvj;
    .locals 5

    .prologue
    .line 2406021
    new-instance v2, LX/Gvj;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 2406022
    new-instance v4, LX/6Gg;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v4, v1, v3}, LX/6Gg;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 2406023
    move-object v1, v4

    .line 2406024
    check-cast v1, LX/6Gg;

    invoke-direct {v2, v0, v1}, LX/Gvj;-><init>(Landroid/content/Context;LX/6Gg;)V

    .line 2406025
    move-object v0, v2

    .line 2406026
    return-object v0
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 1

    .prologue
    .line 2406027
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2406028
    const-string v0, "Bug Reporting - internal"

    invoke-virtual {p0, v0}, LX/Gvj;->setTitle(Ljava/lang/CharSequence;)V

    .line 2406029
    new-instance v0, LX/4ok;

    iget-object p1, p0, LX/Gvj;->a:Landroid/content/Context;

    invoke-direct {v0, p1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2406030
    sget-object p1, LX/6G4;->c:LX/0Tn;

    invoke-virtual {v0, p1}, LX/4oi;->a(LX/0Tn;)V

    .line 2406031
    const p1, 0x7f0818e7

    invoke-virtual {v0, p1}, LX/4oi;->setTitle(I)V

    .line 2406032
    const p1, 0x7f0818e8

    invoke-virtual {v0, p1}, LX/4oi;->setSummary(I)V

    .line 2406033
    const/4 p1, 0x1

    move p1, p1

    .line 2406034
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2406035
    invoke-virtual {p0, v0}, LX/Gvj;->addPreference(Landroid/preference/Preference;)Z

    .line 2406036
    iget-object v0, p0, LX/Gvj;->b:LX/6Gg;

    invoke-virtual {p0, v0}, LX/Gvj;->addPreference(Landroid/preference/Preference;)Z

    .line 2406037
    return-void
.end method
