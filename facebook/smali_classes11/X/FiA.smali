.class public LX/FiA;
.super LX/Fi8;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/FgS;

.field private final c:LX/2Sf;

.field public final d:LX/0W9;

.field private final e:LX/03V;

.field private final f:I

.field private final g:LX/0ad;

.field private h:I

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/03V;LX/FgS;LX/2Sf;LX/0W9;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274568
    invoke-direct {p0}, LX/Fi8;-><init>()V

    .line 2274569
    const/4 v0, 0x0

    iput v0, p0, LX/FiA;->i:I

    .line 2274570
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FiA;->j:Ljava/util/List;

    .line 2274571
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FiA;->k:Ljava/util/List;

    .line 2274572
    iput-object p1, p0, LX/FiA;->a:Landroid/content/res/Resources;

    .line 2274573
    iput-object p3, p0, LX/FiA;->b:LX/FgS;

    .line 2274574
    iput-object p4, p0, LX/FiA;->c:LX/2Sf;

    .line 2274575
    iput-object p2, p0, LX/FiA;->e:LX/03V;

    .line 2274576
    iput-object p5, p0, LX/FiA;->d:LX/0W9;

    .line 2274577
    sget v0, LX/100;->r:I

    const/4 v1, 0x3

    invoke-interface {p6, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/FiA;->f:I

    .line 2274578
    iput-object p6, p0, LX/FiA;->g:LX/0ad;

    .line 2274579
    iget v0, p0, LX/FiA;->f:I

    iput v0, p0, LX/FiA;->h:I

    .line 2274580
    return-void
.end method

.method private a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;Ljava/util/List;Ljava/util/List;LX/7HZ;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2274512
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274513
    iget-object v0, p0, LX/FiA;->e:LX/03V;

    const-string v1, "EmptyKeywordTypeaheadError"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No keyword units available on cleanup with typeahead text: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2274514
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, LX/FiA;->i:I

    .line 2274515
    iput-object p3, p0, LX/FiA;->j:Ljava/util/List;

    .line 2274516
    iput-object p4, p0, LX/FiA;->k:Ljava/util/List;

    .line 2274517
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2274518
    new-instance v0, LX/Cwa;

    invoke-direct {v0}, LX/Cwa;-><init>()V

    sget-object v1, LX/Cwb;->KEYWORD:LX/Cwb;

    .line 2274519
    iput-object v1, v0, LX/Cwa;->a:LX/Cwb;

    .line 2274520
    move-object v0, v0

    .line 2274521
    invoke-static {p3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2274522
    iput-object v1, v0, LX/Cwa;->b:LX/0Px;

    .line 2274523
    move-object v0, v0

    .line 2274524
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274525
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2274526
    new-instance v0, LX/Cwa;

    invoke-direct {v0}, LX/Cwa;-><init>()V

    sget-object v1, LX/Cwb;->NO_GROUP:LX/Cwb;

    .line 2274527
    iput-object v1, v0, LX/Cwa;->a:LX/Cwb;

    .line 2274528
    move-object v0, v0

    .line 2274529
    sget-object v1, Lcom/facebook/search/model/DividerTypeaheadUnit;->a:Lcom/facebook/search/model/DividerTypeaheadUnit;

    move-object v1, v1

    .line 2274530
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2274531
    iput-object v1, v0, LX/Cwa;->b:LX/0Px;

    .line 2274532
    move-object v0, v0

    .line 2274533
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274534
    new-instance v0, LX/Cwa;

    invoke-direct {v0}, LX/Cwa;-><init>()V

    sget-object v1, LX/Cwb;->ENTITY:LX/Cwb;

    .line 2274535
    iput-object v1, v0, LX/Cwa;->a:LX/Cwb;

    .line 2274536
    move-object v0, v0

    .line 2274537
    invoke-static {p4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2274538
    iput-object v1, v0, LX/Cwa;->b:LX/0Px;

    .line 2274539
    move-object v0, v0

    .line 2274540
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move-object v4, p4

    move-object v5, p3

    .line 2274541
    sget-object p1, LX/7HZ;->ACTIVE:LX/7HZ;

    if-ne v3, p1, :cond_2

    .line 2274542
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 2274543
    :goto_0
    move-object v0, p1

    .line 2274544
    invoke-virtual {v6, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2274545
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/2Sf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 2274546
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2274547
    :goto_1
    if-eqz p1, :cond_4

    invoke-static {p1, v2}, LX/FiA;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 p1, 0x1

    .line 2274548
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_5

    if-eqz p1, :cond_5

    .line 2274549
    sget-object p1, LX/0Q7;->a:LX/0Px;

    move-object p1, p1

    .line 2274550
    goto :goto_0

    .line 2274551
    :cond_3
    const/4 p1, 0x0

    goto :goto_1

    .line 2274552
    :cond_4
    const/4 p1, 0x0

    goto :goto_2

    .line 2274553
    :cond_5
    new-instance p1, LX/Cwa;

    invoke-direct {p1}, LX/Cwa;-><init>()V

    sget-object p2, LX/Cwb;->KEYWORD:LX/Cwb;

    .line 2274554
    iput-object p2, p1, LX/Cwa;->a:LX/Cwb;

    .line 2274555
    move-object p1, p1

    .line 2274556
    sget-object p2, LX/CwI;->ESCAPE:LX/CwI;

    invoke-static {v0, v1, p2, v2}, LX/FiA;->a(LX/FiA;Lcom/facebook/search/api/GraphSearchQuery;LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object p2

    .line 2274557
    invoke-static {p2}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object p2

    sget-object v3, LX/CwF;->escape:LX/CwF;

    .line 2274558
    iput-object v3, p2, LX/CwH;->g:LX/CwF;

    .line 2274559
    move-object p2, p2

    .line 2274560
    const/4 v3, 0x0

    .line 2274561
    iput-object v3, p2, LX/CwH;->t:Ljava/lang/String;

    .line 2274562
    move-object p2, p2

    .line 2274563
    invoke-virtual {p2}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object p2

    move-object p2, p2

    .line 2274564
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    .line 2274565
    iput-object p2, p1, LX/Cwa;->b:LX/0Px;

    .line 2274566
    move-object p1, p1

    .line 2274567
    invoke-virtual {p1}, LX/Cwa;->a()LX/Cwc;

    move-result-object p1

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(LX/FiA;Lcom/facebook/search/api/GraphSearchQuery;LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 3

    .prologue
    .line 2274489
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2274490
    iput-object p3, v0, LX/CwH;->b:Ljava/lang/String;

    .line 2274491
    move-object v0, v0

    .line 2274492
    iget-object v1, p0, LX/FiA;->a:Landroid/content/res/Resources;

    const v2, 0x7f08232d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2274493
    iput-object v1, v0, LX/CwH;->t:Ljava/lang/String;

    .line 2274494
    move-object v0, v0

    .line 2274495
    iput-object p3, v0, LX/CwH;->d:Ljava/lang/String;

    .line 2274496
    move-object v0, v0

    .line 2274497
    const-string v1, "content"

    .line 2274498
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2274499
    move-object v0, v0

    .line 2274500
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2274501
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2274502
    move-object v0, v0

    .line 2274503
    iput-object p2, v0, LX/CwH;->l:LX/CwI;

    .line 2274504
    move-object v0, v0

    .line 2274505
    invoke-static {p3}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2274506
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2274507
    move-object v0, v0

    .line 2274508
    invoke-static {p1}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v1

    .line 2274509
    iput-object v1, v0, LX/CwH;->x:LX/0Px;

    .line 2274510
    move-object v0, v0

    .line 2274511
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/FiA;Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;LX/CwF;)Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 3

    .prologue
    .line 2274463
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2274464
    iput-object p2, v0, LX/CwH;->b:Ljava/lang/String;

    .line 2274465
    move-object v0, v0

    .line 2274466
    iget-object v1, p0, LX/FiA;->a:Landroid/content/res/Resources;

    const v2, 0x7f08232d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2274467
    iput-object v1, v0, LX/CwH;->t:Ljava/lang/String;

    .line 2274468
    move-object v0, v0

    .line 2274469
    iput-object p2, v0, LX/CwH;->d:Ljava/lang/String;

    .line 2274470
    move-object v0, v0

    .line 2274471
    const-string v1, "content"

    .line 2274472
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2274473
    move-object v0, v0

    .line 2274474
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2274475
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2274476
    move-object v0, v0

    .line 2274477
    sget-object v1, LX/CwI;->INJECTED_SUGGESTION:LX/CwI;

    .line 2274478
    iput-object v1, v0, LX/CwH;->l:LX/CwI;

    .line 2274479
    move-object v0, v0

    .line 2274480
    iput-object p3, v0, LX/CwH;->g:LX/CwF;

    .line 2274481
    move-object v0, v0

    .line 2274482
    invoke-static {p2}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2274483
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2274484
    move-object v0, v0

    .line 2274485
    invoke-static {p1}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v1

    .line 2274486
    iput-object v1, v0, LX/CwH;->x:LX/0Px;

    .line 2274487
    move-object v0, v0

    .line 2274488
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/util/List;Ljava/util/List;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2274424
    if-gtz p4, :cond_1

    .line 2274425
    :cond_0
    :goto_0
    return-void

    .line 2274426
    :cond_1
    invoke-static {p1}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2274427
    :goto_1
    if-ge v2, p4, :cond_0

    .line 2274428
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    const-string v1, ""

    .line 2274429
    iput-object v1, v0, LX/CwH;->b:Ljava/lang/String;

    .line 2274430
    move-object v0, v0

    .line 2274431
    const-string v1, ""

    .line 2274432
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2274433
    move-object v0, v0

    .line 2274434
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2274435
    iput-object v1, v0, LX/CwH;->x:LX/0Px;

    .line 2274436
    move-object v0, v0

    .line 2274437
    const-string v1, ""

    .line 2274438
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2274439
    move-object v0, v0

    .line 2274440
    const/4 v1, 0x1

    .line 2274441
    iput-boolean v1, v0, LX/CwH;->w:Z

    .line 2274442
    move-object v0, v0

    .line 2274443
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    move-object v0, v0

    .line 2274444
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2274445
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2274446
    :cond_2
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274447
    instance-of v3, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v3, :cond_3

    .line 2274448
    check-cast v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2274449
    iget-object v3, p0, LX/FiA;->a:Landroid/content/res/Resources;

    const v5, 0x7f082338

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 2274450
    iget-object v8, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v8, v8

    .line 2274451
    iget-object p3, p0, LX/FiA;->d:LX/0W9;

    invoke-virtual {p3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object p3

    invoke-virtual {v8, p3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2274452
    sget-object v5, LX/CwF;->photos:LX/CwF;

    invoke-static {p0, p1, v3, v5}, LX/FiA;->a(LX/FiA;Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;LX/CwF;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v3

    move-object v3, v3

    .line 2274453
    invoke-static {p2, v3}, LX/FiA;->a(Ljava/util/List;Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_2
    sub-int v3, p4, v3

    .line 2274454
    if-eqz v3, :cond_0

    .line 2274455
    iget-object v5, p0, LX/FiA;->a:Landroid/content/res/Resources;

    const v6, 0x7f082339

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 2274456
    iget-object p3, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object p3, p3

    .line 2274457
    iget-object p4, p0, LX/FiA;->d:LX/0W9;

    invoke-virtual {p4}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p3

    aput-object p3, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2274458
    sget-object v6, LX/CwF;->keyword:LX/CwF;

    invoke-static {p0, p1, v5, v6}, LX/FiA;->a(LX/FiA;Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;LX/CwF;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v5

    move-object v0, v5

    .line 2274459
    invoke-static {p2, v0}, LX/FiA;->a(Ljava/util/List;Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    sub-int p4, v3, v0

    .line 2274460
    if-nez p4, :cond_3

    goto/16 :goto_0

    :cond_4
    move v3, v2

    .line 2274461
    goto :goto_2

    :cond_5
    move v0, v2

    .line 2274462
    goto :goto_3
.end method

.method private a(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2274406
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 2274407
    iget v3, p0, LX/FiA;->h:I

    if-lt v0, v3, :cond_1

    .line 2274408
    :cond_0
    return-void

    .line 2274409
    :cond_1
    iget-object v3, p0, LX/FiA;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    .line 2274410
    iget v3, p0, LX/FiA;->h:I

    sub-int/2addr v3, v0

    move v4, v0

    .line 2274411
    :goto_0
    if-ge v4, v5, :cond_0

    if-lez v3, :cond_0

    .line 2274412
    if-ge v4, v5, :cond_5

    .line 2274413
    iget-object v0, p0, LX/FiA;->j:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2274414
    iget-object v6, p0, LX/FiA;->m:Ljava/lang/String;

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/FiA;->m:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2274415
    invoke-static {p1, v0}, LX/FiA;->a(Ljava/util/List;Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    sub-int v0, v3, v0

    .line 2274416
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2274417
    goto :goto_1

    .line 2274418
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2274419
    :goto_3
    move-object v0, v0

    .line 2274420
    invoke-static {p1, v0}, LX/FiA;->a(Ljava/util/List;Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    sub-int v0, v3, v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v3

    goto :goto_2

    :cond_6
    invoke-static {v0}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v6

    const/4 v7, 0x1

    .line 2274421
    iput-boolean v7, v6, LX/CwH;->w:Z

    .line 2274422
    move-object v6, v6

    .line 2274423
    invoke-virtual {v6}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    goto :goto_3
.end method

.method public static a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2274405
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/List;Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 2274399
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2274400
    invoke-virtual {p1, v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274401
    const/4 v0, 0x0

    .line 2274402
    :goto_0
    return v0

    .line 2274403
    :cond_1
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2274404
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2274324
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274325
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274326
    const/4 v0, 0x1

    .line 2274327
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2274350
    iget-object v0, p0, LX/FiA;->b:LX/FgS;

    invoke-virtual {v0}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/FiA;->d:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FiA;->m:Ljava/lang/String;

    .line 2274351
    iget-object v0, p0, LX/FiA;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, LX/7Hc;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274352
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v0

    .line 2274353
    :goto_0
    return-object v0

    .line 2274354
    :cond_0
    iget v0, p0, LX/FiA;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/FiA;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    iget-object v0, p0, LX/FiA;->l:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2274355
    iget-object v0, p0, LX/FiA;->l:Ljava/util/List;

    iput-object v0, p0, LX/FiA;->j:Ljava/util/List;

    .line 2274356
    const/4 v0, 0x0

    iput-object v0, p0, LX/FiA;->l:Ljava/util/List;

    .line 2274357
    :cond_1
    invoke-virtual {p2}, LX/7Hc;->c()I

    move-result v0

    if-lez v0, :cond_2

    .line 2274358
    invoke-virtual {p2}, LX/7Hc;->c()I

    move-result v0

    iget v1, p0, LX/FiA;->f:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/FiA;->h:I

    .line 2274359
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    iget v0, p0, LX/FiA;->h:I

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2274360
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2274361
    const/4 v2, 0x0

    .line 2274362
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2274363
    invoke-virtual {p2}, LX/7Hc;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v5, v0

    :goto_1
    if-ge v5, v8, :cond_6

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274364
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v1

    sget-object v9, LX/Cwb;->KEYWORD:LX/Cwb;

    if-ne v1, v9, :cond_5

    .line 2274365
    instance-of v1, v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v1, :cond_3

    .line 2274366
    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2274367
    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->c(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2274368
    invoke-interface {v6, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2274369
    if-nez v1, :cond_4

    .line 2274370
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    iget v10, p0, LX/FiA;->h:I

    if-ge v1, v10, :cond_3

    .line 2274371
    iget-object v1, p0, LX/FiA;->m:Ljava/lang/String;

    invoke-static {v0, v1}, LX/FiA;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2274372
    const/4 v1, 0x1

    .line 2274373
    :goto_2
    invoke-interface {v6, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2274374
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 2274375
    :cond_3
    :goto_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    .line 2274376
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->J()I

    move-result v10

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->J()I

    move-result v11

    if-le v10, v11, :cond_3

    .line 2274377
    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2274378
    invoke-interface {v6, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2274379
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2274380
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v1

    sget-object v9, LX/Cwb;->ENTITY:LX/Cwb;

    if-ne v1, v9, :cond_3

    .line 2274381
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2274382
    :cond_6
    const/4 v0, 0x0

    .line 2274383
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    if-nez v2, :cond_7

    .line 2274384
    sget-object v0, LX/CwI;->ECHO:LX/CwI;

    iget-object v1, p0, LX/FiA;->m:Ljava/lang/String;

    invoke-static {p0, p1, v0, v1}, LX/FiA;->a(LX/FiA;Lcom/facebook/search/api/GraphSearchQuery;LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2274385
    const/4 v0, 0x1

    .line 2274386
    :cond_7
    invoke-direct {p0, v3}, LX/FiA;->a(Ljava/util/List;)V

    .line 2274387
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    iget v5, p0, LX/FiA;->h:I

    if-ge v1, v5, :cond_8

    if-nez v0, :cond_8

    if-nez v2, :cond_8

    .line 2274388
    sget-object v0, LX/CwI;->ECHO:LX/CwI;

    iget-object v1, p0, LX/FiA;->m:Ljava/lang/String;

    invoke-static {p0, p1, v0, v1}, LX/FiA;->a(LX/FiA;Lcom/facebook/search/api/GraphSearchQuery;LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    invoke-static {v3, v0}, LX/FiA;->a(Ljava/util/List;Lcom/facebook/search/model/KeywordTypeaheadUnit;)Z

    .line 2274389
    :cond_8
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, LX/FiA;->h:I

    if-ge v0, v1, :cond_9

    .line 2274390
    iget v0, p0, LX/FiA;->h:I

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-direct {p0, p1, v3, v4, v0}, LX/FiA;->a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/util/List;Ljava/util/List;I)V

    .line 2274391
    :cond_9
    iget v0, p0, LX/FiA;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    iget-object v0, p0, LX/FiA;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    .line 2274392
    :goto_4
    iget-object v1, p0, LX/FiA;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_c

    const/4 v1, 0x1

    .line 2274393
    :goto_5
    if-eqz v0, :cond_a

    if-eqz v1, :cond_a

    iget-object v0, p0, LX/FiA;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, LX/FiA;->j:Ljava/util/List;

    invoke-static {v0}, LX/FiA;->b(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2274394
    iput-object v3, p0, LX/FiA;->l:Ljava/util/List;

    .line 2274395
    iget-object v3, p0, LX/FiA;->j:Ljava/util/List;

    .line 2274396
    :cond_a
    iget-object v2, p0, LX/FiA;->m:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v5, p4

    invoke-direct/range {v0 .. v5}, LX/FiA;->a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;Ljava/util/List;Ljava/util/List;LX/7HZ;)LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 2274397
    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    .line 2274398
    :cond_c
    const/4 v1, 0x0

    goto :goto_5

    :cond_d
    move v1, v2

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 2

    .prologue
    .line 2274349
    sget-object v0, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    iget-object v1, p0, LX/FiA;->b:LX/FgS;

    invoke-virtual {v1}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, LX/FiA;->a(LX/FiA;Lcom/facebook/search/api/GraphSearchQuery;LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2274343
    const/4 v0, 0x0

    iput v0, p0, LX/FiA;->i:I

    .line 2274344
    iget-object v0, p0, LX/FiA;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2274345
    iget-object v0, p0, LX/FiA;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2274346
    const/4 v0, 0x0

    iput-object v0, p0, LX/FiA;->l:Ljava/util/List;

    .line 2274347
    iget v0, p0, LX/FiA;->f:I

    iput v0, p0, LX/FiA;->h:I

    .line 2274348
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/Fid;)Z
    .locals 6

    .prologue
    .line 2274328
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2274329
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2274330
    const/4 v0, 0x0

    .line 2274331
    iget-object v1, p0, LX/FiA;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2274332
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v5

    if-nez v5, :cond_0

    iget v5, p0, LX/FiA;->h:I

    if-ge v1, v5, :cond_0

    .line 2274333
    add-int/lit8 v1, v1, 0x1

    .line 2274334
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v0, v1

    move v1, v0

    .line 2274335
    goto :goto_0

    .line 2274336
    :cond_1
    iget-object v0, p0, LX/FiA;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274337
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2274338
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2274339
    :cond_3
    iget v0, p0, LX/FiA;->h:I

    sub-int/2addr v0, v1

    invoke-direct {p0, p1, v3, v4, v0}, LX/FiA;->a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/util/List;Ljava/util/List;I)V

    .line 2274340
    iget-object v0, p0, LX/FiA;->b:LX/FgS;

    invoke-virtual {v0}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/FiA;->d:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 2274341
    sget-object v5, LX/7HZ;->IDLE:LX/7HZ;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/FiA;->a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;Ljava/util/List;Ljava/util/List;LX/7HZ;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/Fid;->a(Ljava/util/List;)V

    .line 2274342
    const/4 v0, 0x1

    return v0
.end method
