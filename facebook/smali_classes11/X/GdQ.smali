.class public abstract LX/GdQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:LX/2zA;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/2zA;)V
    .locals 1

    .prologue
    .line 2373939
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LX/GdQ;-><init>(Ljava/lang/String;Ljava/lang/String;LX/2zA;I)V

    .line 2373940
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/2zA;I)V
    .locals 0

    .prologue
    .line 2373941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2373942
    iput-object p1, p0, LX/GdQ;->b:Ljava/lang/String;

    .line 2373943
    iput-object p2, p0, LX/GdQ;->c:Ljava/lang/String;

    .line 2373944
    iput-object p3, p0, LX/GdQ;->d:LX/2zA;

    .line 2373945
    iput p4, p0, LX/GdQ;->a:I

    .line 2373946
    return-void
.end method


# virtual methods
.method public abstract c()Landroid/support/v4/app/Fragment;
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2373947
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/GdQ;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GdQ;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
