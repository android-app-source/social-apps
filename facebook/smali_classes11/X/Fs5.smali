.class public LX/Fs5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2296554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/0zX;LX/0zX;)LX/0zX;
    .locals 1
    .param p0    # LX/0zX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/0zX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zX",
            "<TT;>;",
            "LX/0zX",
            "<TT;>;)",
            "LX/0zX",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2296555
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 2296556
    :cond_0
    const/4 v0, 0x0

    .line 2296557
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0zX;->c(LX/0zX;)LX/0zX;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0zX;->b(LX/0zX;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/FsJ;LX/FsJ;)LX/FsJ;
    .locals 11

    .prologue
    .line 2296558
    new-instance v0, LX/FsJ;

    iget-object v1, p0, LX/FsJ;->a:LX/0zX;

    iget-object v2, p1, LX/FsJ;->a:LX/0zX;

    invoke-static {v1, v2}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v1

    iget-object v2, p0, LX/FsJ;->b:LX/0zX;

    iget-object v3, p1, LX/FsJ;->b:LX/0zX;

    invoke-static {v2, v3}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v2

    iget-object v3, p0, LX/FsJ;->c:LX/0zX;

    iget-object v4, p1, LX/FsJ;->c:LX/0zX;

    invoke-static {v3, v4}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v3

    iget-object v4, p0, LX/FsJ;->e:LX/0zX;

    iget-object v5, p1, LX/FsJ;->e:LX/0zX;

    invoke-static {v4, v5}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v4

    iget-object v5, p0, LX/FsJ;->d:LX/0zX;

    iget-object v6, p1, LX/FsJ;->d:LX/0zX;

    invoke-static {v5, v6}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v5

    iget-object v6, p0, LX/FsJ;->f:LX/0zX;

    iget-object v7, p1, LX/FsJ;->f:LX/0zX;

    invoke-static {v6, v7}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v6

    iget-object v7, p0, LX/FsJ;->g:LX/0zX;

    iget-object v8, p1, LX/FsJ;->g:LX/0zX;

    invoke-static {v7, v8}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v7

    iget-object v8, p0, LX/FsJ;->h:LX/0zX;

    iget-object v9, p1, LX/FsJ;->h:LX/0zX;

    invoke-static {v8, v9}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v8

    iget-object v9, p0, LX/FsJ;->i:LX/0zX;

    iget-object v10, p1, LX/FsJ;->i:LX/0zX;

    invoke-static {v9, v10}, LX/Fs5;->a(LX/0zX;LX/0zX;)LX/0zX;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/FsJ;-><init>(LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;)V

    return-object v0
.end method
