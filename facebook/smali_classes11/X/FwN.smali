.class public LX/FwN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9hF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/9hF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303042
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303043
    iput-object p1, p0, LX/FwN;->a:Landroid/content/Context;

    .line 2303044
    iput-object p2, p0, LX/FwN;->b:LX/0Or;

    .line 2303045
    iput-object p3, p0, LX/FwN;->c:LX/0Or;

    .line 2303046
    return-void
.end method

.method public static a(LX/0QB;)LX/FwN;
    .locals 6

    .prologue
    .line 2303047
    const-class v1, LX/FwN;

    monitor-enter v1

    .line 2303048
    :try_start_0
    sget-object v0, LX/FwN;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2303049
    sput-object v2, LX/FwN;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2303050
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2303051
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2303052
    new-instance v4, LX/FwN;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v5, 0xf2f

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x2e6c

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/FwN;-><init>(Landroid/content/Context;LX/0Or;LX/0Or;)V

    .line 2303053
    move-object v0, v4

    .line 2303054
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2303055
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FwN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2303056
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2303057
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/1Fb;LX/74S;)V
    .locals 4
    .param p1    # Lcom/facebook/drawee/view/DraweeView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303058
    if-eqz p3, :cond_1

    .line 2303059
    iget-object v0, p0, LX/FwN;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    invoke-static {p3}, LX/9hF;->a(Ljava/lang/String;)LX/9hE;

    move-result-object v0

    .line 2303060
    :goto_0
    if-eqz p5, :cond_0

    .line 2303061
    invoke-static {p2, p5}, LX/FwM;->a(Ljava/lang/String;LX/1Fb;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/0Px;)LX/9hD;

    .line 2303062
    :cond_0
    invoke-virtual {v0, p6}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 2303063
    iget-object v0, p0, LX/FwN;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    iget-object v2, p0, LX/FwN;->a:Landroid/content/Context;

    .line 2303064
    if-eqz p1, :cond_2

    if-eqz p4, :cond_2

    invoke-static {p2, p1, p4}, LX/FwM;->a(Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;)LX/9hN;

    move-result-object v3

    :goto_1
    move-object v3, v3

    .line 2303065
    invoke-interface {v0, v2, v1, v3}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2303066
    return-void

    .line 2303067
    :cond_1
    iget-object v0, p0, LX/FwN;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method
