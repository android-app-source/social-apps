.class public final LX/Gdg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsInterfaces$AdGeoCircle;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GDA;

.field public final synthetic b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GDA;)V
    .locals 0

    .prologue
    .line 2374411
    iput-object p1, p0, LX/Gdg;->b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    iput-object p2, p0, LX/Gdg;->a:LX/GDA;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2374412
    iget-object v0, p0, LX/Gdg;->b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    iget-object v0, v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->f:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get radius"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2374413
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2374414
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2374415
    iget-object v1, p0, LX/Gdg;->a:LX/GDA;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, LX/GDA;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;)V

    .line 2374416
    return-void

    .line 2374417
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2374418
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;

    goto :goto_0
.end method
