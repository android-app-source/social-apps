.class public LX/GJJ;
.super LX/GJI;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/adinterfaces/ui/BudgetOptionsView;",
        ">",
        "LX/GJI",
        "<TT;",
        "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
        ">;"
    }
.end annotation


# instance fields
.field public g:LX/2U3;

.field private h:LX/GMU;

.field private final i:LX/GG6;

.field private j:LX/GGB;

.field private k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;


# direct methods
.method public constructor <init>(LX/GG6;LX/2U3;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2338468
    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object v8, p2

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    invoke-direct/range {v0 .. v10}, LX/GJI;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;LX/2U3;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2338469
    iput-object p2, p0, LX/GJJ;->g:LX/2U3;

    .line 2338470
    iput-object p4, p0, LX/GJJ;->h:LX/GMU;

    .line 2338471
    iput-object p1, p0, LX/GJJ;->i:LX/GG6;

    .line 2338472
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;
    .locals 7

    .prologue
    .line 2338466
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    .line 2338467
    iget-object v1, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080a53

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/GJJ;->i:LX/GG6;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v6

    invoke-virtual {v5, v6}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/GJJ;->i:LX/GG6;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v0

    invoke-virtual {v5, v0}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2338455
    iget-object v0, p0, LX/GJJ;->j:LX/GGB;

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GJJ;->j:LX/GGB;

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne v0, v1, :cond_1

    .line 2338456
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    invoke-static {p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 2338457
    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v1, v1

    .line 2338458
    invoke-static {v1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 2338459
    :goto_0
    return-object v0

    .line 2338460
    :cond_1
    iget-object v0, p0, LX/GJJ;->h:LX/GMU;

    iget-object v1, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2338461
    if-nez v0, :cond_2

    .line 2338462
    const/4 v0, 0x0

    goto :goto_0

    .line 2338463
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 2338464
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2338465
    invoke-static {v0}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(LX/GGB;)V
    .locals 2

    .prologue
    .line 2338446
    iget-object v0, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    if-ne v0, v1, :cond_0

    .line 2338447
    iget-object v0, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    move-object v0, v0

    .line 2338448
    const v1, 0x7f080abd

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2338449
    :goto_0
    return-void

    .line 2338450
    :cond_0
    sget-object v0, LX/GGB;->INACTIVE:LX/GGB;

    if-eq p1, v0, :cond_1

    sget-object v0, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne p1, v0, :cond_2

    .line 2338451
    :cond_1
    iget-object v0, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    move-object v0, v0

    .line 2338452
    const v1, 0x7f080ad6

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    goto :goto_0

    .line 2338453
    :cond_2
    iget-object v0, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    move-object v0, v0

    .line 2338454
    const v1, 0x7f080abc

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    goto :goto_0
.end method

.method public bridge synthetic a(LX/GJD;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2338445
    check-cast p1, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GJI;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2338444
    check-cast p1, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GJI;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2338443
    check-cast p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {p0, p1}, LX/GJJ;->a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V

    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V
    .locals 1

    .prologue
    .line 2338439
    invoke-super {p0, p1}, LX/GJI;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2338440
    iput-object p1, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    .line 2338441
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    iput-object v0, p0, LX/GJJ;->j:LX/GGB;

    .line 2338442
    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2338473
    invoke-super {p0, p1, p2}, LX/GJI;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2338474
    iget-object v0, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    if-ne v0, v1, :cond_1

    .line 2338475
    :cond_0
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338476
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b()LX/8wQ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2338477
    :goto_0
    return-void

    .line 2338478
    :cond_1
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338479
    iget-object v1, v0, LX/GCE;->e:LX/0ad;

    move-object v0, v1

    .line 2338480
    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/GDK;->E:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2338481
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2338482
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d08a6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b55

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2338483
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d03ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b56

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2338484
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d10c3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b57

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2338485
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d10c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b58

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2338486
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d0d37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/GMb;

    invoke-direct {v3, p0}, LX/GMb;-><init>(LX/GJJ;)V

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2338487
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d08a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b54

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2338488
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2338489
    const-string v2, "budget"

    invoke-virtual {p2, v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/util/List;Ljava/lang/String;)LX/8wQ;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/GCE;->a(LX/8wQ;)V

    goto/16 :goto_0

    .line 2338490
    :cond_2
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338491
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b()LX/8wQ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    goto/16 :goto_0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;
    .locals 7

    .prologue
    .line 2338437
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    .line 2338438
    iget-object v1, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080a54

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/GJJ;->i:LX/GG6;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v6

    invoke-virtual {v5, v6}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/GJJ;->i:LX/GG6;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v0

    invoke-virtual {v5, v0}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 2338427
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2338428
    invoke-static {p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2338429
    iget-object v2, p0, LX/GJJ;->j:LX/GGB;

    sget-object v3, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LX/GJJ;->j:LX/GGB;

    sget-object v3, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v2, v3, :cond_0

    .line 2338430
    iget-object v0, p0, LX/GJJ;->h:LX/GMU;

    iget-object v2, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2338431
    :cond_0
    if-nez v0, :cond_1

    .line 2338432
    const/4 v0, 0x0

    .line 2338433
    :goto_0
    return-object v0

    .line 2338434
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v2

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v4

    invoke-static {}, LX/GMU;->a()Ljava/text/NumberFormat;

    move-result-object v0

    invoke-static {v2, v4, v5, v0}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2338435
    iget-object v0, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-static {v0}, LX/GMU;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2338436
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 4

    .prologue
    .line 2338408
    iget-object v0, p0, LX/GJJ;->j:LX/GGB;

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GJJ;->j:LX/GGB;

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne v0, v1, :cond_1

    .line 2338409
    :cond_0
    :goto_0
    return-object p1

    :cond_1
    iget-object v0, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2338410
    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2338411
    invoke-static {p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 2338412
    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 2338413
    invoke-virtual {v2, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-gtz v1, :cond_2

    .line 2338414
    iget-object v1, p0, LX/GJJ;->g:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "Total budget must be greater than the current budget"

    invoke-virtual {v1, v2, v3}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2338415
    const/4 v1, 0x0

    .line 2338416
    :goto_1
    move-object p1, v1

    .line 2338417
    goto :goto_0

    :cond_2
    new-instance v1, LX/A9B;

    invoke-direct {v1}, LX/A9B;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2338418
    iput-object v3, v1, LX/A9B;->a:Ljava/lang/String;

    .line 2338419
    move-object v1, v1

    .line 2338420
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v3

    .line 2338421
    iput v3, v1, LX/A9B;->b:I

    .line 2338422
    move-object v1, v1

    .line 2338423
    invoke-virtual {v2}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2338424
    iput-object v2, v1, LX/A9B;->c:Ljava/lang/String;

    .line 2338425
    move-object v1, v1

    .line 2338426
    invoke-virtual {v1}, LX/A9B;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    goto :goto_1
.end method

.method public g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2338399
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2338400
    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    .line 2338401
    sget-object v0, LX/GMc;->a:[I

    iget-object v2, p0, LX/GJJ;->k:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v2

    invoke-virtual {v2}, LX/GGB;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2338402
    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2338403
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v2

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    .line 2338404
    iget-object v3, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v3, v3

    .line 2338405
    invoke-static {v3}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2338406
    :pswitch_0
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2338407
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v0

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public h()LX/GMT;
    .locals 4

    .prologue
    .line 2338380
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2338381
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2338382
    sget-object v0, LX/GMT;->NONE:LX/GMT;

    .line 2338383
    :goto_0
    return-object v0

    .line 2338384
    :cond_0
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2338385
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2338386
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2338387
    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v1, v1

    .line 2338388
    invoke-static {v1}, LX/GMU;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2338389
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v2, v2

    .line 2338390
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2338391
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2338392
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v2, v2

    .line 2338393
    invoke-static {v2}, LX/GMU;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 2338394
    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-lez v1, :cond_1

    .line 2338395
    sget-object v0, LX/GMT;->MAX:LX/GMT;

    goto :goto_0

    .line 2338396
    :cond_1
    iget-object v1, p0, LX/GJJ;->j:LX/GGB;

    sget-object v3, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v1, v3, :cond_2

    iget-object v1, p0, LX/GJJ;->j:LX/GGB;

    sget-object v3, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v1, v3, :cond_2

    iget-object v1, p0, LX/GJJ;->j:LX/GGB;

    sget-object v3, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v1, v3, :cond_2

    iget-object v1, p0, LX/GJJ;->j:LX/GGB;

    sget-object v3, LX/GGB;->PENDING:LX/GGB;

    if-ne v1, v3, :cond_3

    :cond_2
    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_3

    .line 2338397
    sget-object v0, LX/GMT;->MIN:LX/GMT;

    goto :goto_0

    .line 2338398
    :cond_3
    sget-object v0, LX/GMT;->NONE:LX/GMT;

    goto :goto_0
.end method

.method public i()Landroid/text/Spanned;
    .locals 6

    .prologue
    .line 2338366
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2338367
    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2338368
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2338369
    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v1, v1

    .line 2338370
    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v1

    if-gtz v1, :cond_1

    .line 2338371
    :cond_0
    const/4 v0, 0x0

    .line 2338372
    :goto_0
    return-object v0

    .line 2338373
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    .line 2338374
    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v1, v1

    .line 2338375
    invoke-static {v1}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 2338376
    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v1, v1

    .line 2338377
    invoke-static {v1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 2338378
    invoke-virtual {p0}, LX/GJJ;->g()Ljava/lang/String;

    move-result-object v1

    .line 2338379
    iget-object v2, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v3, 0x7f080a60

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Landroid/text/Spanned;
    .locals 8

    .prologue
    .line 2338355
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2338356
    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->u()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2338357
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2338358
    :cond_0
    const/4 v0, 0x0

    .line 2338359
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v1, 0x7f080a5f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2338360
    iget-object v4, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v4, v4

    .line 2338361
    invoke-static {v4}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->u()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v4

    .line 2338362
    iget-object v5, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v5, v5

    .line 2338363
    invoke-static {v5}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->u()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v5

    invoke-static {v5}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v6

    .line 2338364
    iget-object v5, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v5, v5

    .line 2338365
    invoke-static {v5}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v5

    invoke-static {v4, v6, v7, v5}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method
