.class public final enum LX/FEa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FEa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FEa;

.field public static final enum ALLOWED:LX/FEa;

.field public static final enum NOT_IN_GK:LX/FEa;

.field public static final enum NOT_SUPPORTED:LX/FEa;


# instance fields
.field public value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2216526
    new-instance v0, LX/FEa;

    const-string v1, "ALLOWED"

    const-string v2, "allowed"

    invoke-direct {v0, v1, v3, v2}, LX/FEa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEa;->ALLOWED:LX/FEa;

    .line 2216527
    new-instance v0, LX/FEa;

    const-string v1, "NOT_SUPPORTED"

    const-string v2, "not_supported"

    invoke-direct {v0, v1, v4, v2}, LX/FEa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEa;->NOT_SUPPORTED:LX/FEa;

    .line 2216528
    new-instance v0, LX/FEa;

    const-string v1, "NOT_IN_GK"

    const-string v2, "not_in_gk"

    invoke-direct {v0, v1, v5, v2}, LX/FEa;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEa;->NOT_IN_GK:LX/FEa;

    .line 2216529
    const/4 v0, 0x3

    new-array v0, v0, [LX/FEa;

    sget-object v1, LX/FEa;->ALLOWED:LX/FEa;

    aput-object v1, v0, v3

    sget-object v1, LX/FEa;->NOT_SUPPORTED:LX/FEa;

    aput-object v1, v0, v4

    sget-object v1, LX/FEa;->NOT_IN_GK:LX/FEa;

    aput-object v1, v0, v5

    sput-object v0, LX/FEa;->$VALUES:[LX/FEa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2216530
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2216531
    iput-object p3, p0, LX/FEa;->value:Ljava/lang/String;

    .line 2216532
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FEa;
    .locals 1

    .prologue
    .line 2216533
    const-class v0, LX/FEa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FEa;

    return-object v0
.end method

.method public static values()[LX/FEa;
    .locals 1

    .prologue
    .line 2216534
    sget-object v0, LX/FEa;->$VALUES:[LX/FEa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FEa;

    return-object v0
.end method
