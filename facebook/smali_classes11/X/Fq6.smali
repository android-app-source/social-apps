.class public final LX/Fq6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fq7;


# direct methods
.method public constructor <init>(LX/Fq7;)V
    .locals 0

    .prologue
    .line 2292361
    iput-object p1, p0, LX/Fq6;->a:LX/Fq7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2292362
    iget-object v0, p0, LX/Fq6;->a:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->k:LX/G4x;

    if-eqz v0, :cond_0

    .line 2292363
    iget-object v0, p0, LX/Fq6;->a:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->k:LX/G4x;

    sget-object v1, LX/G5A;->FAILED:LX/G5A;

    invoke-virtual {v0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2292364
    iget-object v0, p0, LX/Fq6;->a:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292365
    :cond_0
    iget-object v0, p0, LX/Fq6;->a:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->o:LX/FrV;

    if-eqz v0, :cond_1

    .line 2292366
    iget-object v0, p0, LX/Fq6;->a:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    invoke-virtual {v0}, LX/Frd;->e()LX/FsJ;

    move-result-object v0

    .line 2292367
    iget-object v1, p0, LX/Fq6;->a:LX/Fq7;

    iget-object v1, v1, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Frd;->b(LX/FsJ;)V

    .line 2292368
    :cond_1
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2292369
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2292370
    iget-object v0, p0, LX/Fq6;->a:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BPq;

    new-instance v1, LX/G4g;

    iget-object v2, p0, LX/Fq6;->a:LX/Fq7;

    iget-object v2, v2, LX/Fq7;->m:LX/5SB;

    .line 2292371
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2292372
    iget-object v3, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v3, v3

    .line 2292373
    invoke-direct {v1, v2, v3}, LX/G4g;-><init>(Landroid/os/ParcelUuid;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2292374
    return-void
.end method
