.class public final LX/GtS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:LX/4oi;

.field public final synthetic b:LX/4or;

.field public final synthetic c:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;LX/4oi;LX/4or;)V
    .locals 0

    .prologue
    .line 2400575
    iput-object p1, p0, LX/GtS;->c:Lcom/facebook/katana/InternSettingsActivity;

    iput-object p2, p0, LX/GtS;->a:LX/4oi;

    iput-object p3, p0, LX/GtS;->b:LX/4or;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2400576
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_2

    instance-of v0, p1, Landroid/preference/EditTextPreference;

    if-eqz v0, :cond_2

    move-object v0, p2

    .line 2400577
    check-cast v0, Ljava/lang/String;

    .line 2400578
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 2400579
    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2400580
    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2400581
    :cond_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_a

    .line 2400582
    const-string v3, "facebook.com"

    .line 2400583
    :cond_1
    :goto_0
    move-object v3, v3

    .line 2400584
    check-cast p1, Landroid/preference/EditTextPreference;

    .line 2400585
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2400586
    invoke-virtual {p1, v3}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    .line 2400587
    :goto_1
    return v2

    .line 2400588
    :cond_2
    const-string v0, "facebook.com"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 2400589
    :goto_2
    iget-object v3, p0, LX/GtS;->c:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v3, v3, Lcom/facebook/katana/InternSettingsActivity;->m:LX/1Mj;

    const/4 v5, 0x0

    .line 2400590
    invoke-static {v3}, LX/1Mj;->e(LX/1Mj;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2400591
    if-nez v4, :cond_b

    move v4, v5

    .line 2400592
    :goto_3
    move v3, v4

    .line 2400593
    if-eqz v3, :cond_7

    .line 2400594
    iget-object v3, p0, LX/GtS;->a:LX/4oi;

    invoke-virtual {v3, v1}, LX/4oi;->setChecked(Z)V

    .line 2400595
    const-string v3, "Cache cleared"

    .line 2400596
    :goto_4
    iget-object v4, p0, LX/GtS;->b:LX/4or;

    invoke-virtual {v4}, LX/4or;->getOnPreferenceChangeListener()Landroid/preference/Preference$OnPreferenceChangeListener;

    move-result-object v4

    iget-object v5, p0, LX/GtS;->b:LX/4or;

    const-string v6, "cache"

    invoke-interface {v4, v5, v6}, Landroid/preference/Preference$OnPreferenceChangeListener;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2400597
    iget-object v4, p0, LX/GtS;->b:LX/4or;

    const-string v5, "cache"

    invoke-virtual {v4, v5}, LX/4or;->setValue(Ljava/lang/String;)V

    .line 2400598
    :cond_3
    if-eqz v0, :cond_9

    .line 2400599
    check-cast p2, Ljava/lang/String;

    .line 2400600
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2400601
    const-string v0, "www."

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "b-www."

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2400602
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "b-www."

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 2400603
    :cond_4
    invoke-static {p2}, LX/00L;->setReportHost(Ljava/lang/String;)V

    .line 2400604
    :cond_5
    :goto_5
    iget-object v0, p0, LX/GtS;->c:Lcom/facebook/katana/InternSettingsActivity;

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v2, v1

    .line 2400605
    goto :goto_1

    :cond_6
    move v0, v2

    .line 2400606
    goto :goto_2

    .line 2400607
    :cond_7
    iget-object v4, p0, LX/GtS;->a:LX/4oi;

    if-nez v0, :cond_8

    move v3, v1

    :goto_6
    invoke-virtual {v4, v3}, LX/4oi;->setChecked(Z)V

    .line 2400608
    const-string v3, "Cache cleared. Please install the Facebook root certificate."

    goto :goto_4

    :cond_8
    move v3, v2

    .line 2400609
    goto :goto_6

    .line 2400610
    :cond_9
    const-string v0, "b-www.facebook.com"

    invoke-static {v0}, LX/00L;->setReportHost(Ljava/lang/String;)V

    goto :goto_5

    .line 2400611
    :cond_a
    const-string v4, "facebook.com"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2400612
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".facebook.com"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 2400613
    :cond_b
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_c
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/security/cert/X509Certificate;

    .line 2400614
    const-string p1, "CN=TheFacebookRootCA"

    invoke-virtual {v4}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v4

    invoke-interface {v4}, Ljava/security/Principal;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2400615
    const/4 v4, 0x1

    goto/16 :goto_3

    :cond_d
    move v4, v5

    .line 2400616
    goto/16 :goto_3
.end method
