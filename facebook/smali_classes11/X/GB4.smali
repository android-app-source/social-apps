.class public final enum LX/GB4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GB4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GB4;

.field public static final enum AUTO_IDENTIFY_FAILED:LX/GB4;

.field public static final enum AUTO_IDENTIFY_PERFORMED:LX/GB4;

.field public static final enum AUTO_IDENTIFY_REJECTED:LX/GB4;

.field public static final enum AUTO_IDENTIFY_STARTED:LX/GB4;

.field public static final enum BACKGROUND_FETCH_DEVICE_DATA:LX/GB4;

.field public static final enum BOUNCE_FROM_MSITE:LX/GB4;

.field public static final enum CAPTCHA_REQUIRED:LX/GB4;

.field public static final enum CHANGE_PASSWORD_SUBMITTED:LX/GB4;

.field public static final enum CHANGE_PASSWORD_VIEWED:LX/GB4;

.field public static final enum CHANGE_SEARCH_TYPE_CLICK:LX/GB4;

.field public static final enum CODE_ENTRY_VIEWED:LX/GB4;

.field public static final enum CODE_SUBMITTED:LX/GB4;

.field public static final enum CUID_READY:LX/GB4;

.field public static final enum DEVICE_DATA_READY:LX/GB4;

.field public static final enum EMAIL_LISTED_BEFORE_SMS:LX/GB4;

.field public static final enum FB4A_ACCOUNT_RECOVERY:LX/GB4;

.field public static final enum FDR_VIEWED:LX/GB4;

.field public static final enum FORGOT_PASSWORD_CLICK:LX/GB4;

.field public static final enum FRIEND_SEARCH_PERFORMED:LX/GB4;

.field public static final enum FRIEND_SEARCH_SENT:LX/GB4;

.field public static final enum FRIEND_SEARCH_VIEWED:LX/GB4;

.field public static final enum HELP_CENTER_CLICK:LX/GB4;

.field public static final enum INITIATE_VIEWED:LX/GB4;

.field public static final enum LIST_SHOWN:LX/GB4;

.field public static final enum LOGIN_FROM_MSITE:LX/GB4;

.field public static final enum LOGOUT_OPTION_SELECTED:LX/GB4;

.field public static final enum MSITE_ACCOUNT_RECOVERY:LX/GB4;

.field public static final enum PARALLEL_SEARCH_READY:LX/GB4;

.field public static final enum PASSWORD_SHOWN:LX/GB4;

.field public static final enum RECOVERY_SUCCESS:LX/GB4;

.field public static final enum SEARCH_PERFORMED:LX/GB4;

.field public static final enum SEARCH_SENT:LX/GB4;

.field public static final enum SEARCH_VIEWED:LX/GB4;

.field public static final enum SENT_CODE_EMAIL:LX/GB4;

.field public static final enum SENT_CODE_SMS:LX/GB4;

.field public static final enum SHOW_HIDE_PASSWORD:LX/GB4;

.field public static final enum SHOW_PASSWORD_CHECKED:LX/GB4;

.field public static final enum SHOW_PASSWORD_UNCHECKED:LX/GB4;

.field public static final enum SMS_CODE_SEARCHED:LX/GB4;

.field public static final enum TEST_BOUNCE_FROM_MSITE:LX/GB4;

.field public static final enum UNSET_BOUNCE_FROM_MSITE:LX/GB4;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2326349
    new-instance v0, LX/GB4;

    const-string v1, "CHANGE_SEARCH_TYPE_CLICK"

    const-string v2, "change_search_type_click"

    invoke-direct {v0, v1, v4, v2}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->CHANGE_SEARCH_TYPE_CLICK:LX/GB4;

    .line 2326350
    new-instance v0, LX/GB4;

    const-string v1, "SHOW_HIDE_PASSWORD"

    const-string v2, "show_password"

    invoke-direct {v0, v1, v5, v2}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SHOW_HIDE_PASSWORD:LX/GB4;

    .line 2326351
    new-instance v0, LX/GB4;

    const-string v1, "SHOW_PASSWORD_CHECKED"

    const-string v2, "show_password_checked"

    invoke-direct {v0, v1, v6, v2}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SHOW_PASSWORD_CHECKED:LX/GB4;

    .line 2326352
    new-instance v0, LX/GB4;

    const-string v1, "SHOW_PASSWORD_UNCHECKED"

    const-string v2, "show_password_unchecked"

    invoke-direct {v0, v1, v7, v2}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SHOW_PASSWORD_UNCHECKED:LX/GB4;

    .line 2326353
    new-instance v0, LX/GB4;

    const-string v1, "FORGOT_PASSWORD_CLICK"

    const-string v2, "forgot_password_click"

    invoke-direct {v0, v1, v8, v2}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->FORGOT_PASSWORD_CLICK:LX/GB4;

    .line 2326354
    new-instance v0, LX/GB4;

    const-string v1, "HELP_CENTER_CLICK"

    const/4 v2, 0x5

    const-string v3, "help_center_click"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->HELP_CENTER_CLICK:LX/GB4;

    .line 2326355
    new-instance v0, LX/GB4;

    const-string v1, "FB4A_ACCOUNT_RECOVERY"

    const/4 v2, 0x6

    const-string v3, "fb4a_account_recovery"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->FB4A_ACCOUNT_RECOVERY:LX/GB4;

    .line 2326356
    new-instance v0, LX/GB4;

    const-string v1, "MSITE_ACCOUNT_RECOVERY"

    const/4 v2, 0x7

    const-string v3, "msite_account_recovery"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->MSITE_ACCOUNT_RECOVERY:LX/GB4;

    .line 2326357
    new-instance v0, LX/GB4;

    const-string v1, "LOGIN_FROM_MSITE"

    const/16 v2, 0x8

    const-string v3, "login_from_msite"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->LOGIN_FROM_MSITE:LX/GB4;

    .line 2326358
    new-instance v0, LX/GB4;

    const-string v1, "TEST_BOUNCE_FROM_MSITE"

    const/16 v2, 0x9

    const-string v3, "test_bounce_from_msite"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->TEST_BOUNCE_FROM_MSITE:LX/GB4;

    .line 2326359
    new-instance v0, LX/GB4;

    const-string v1, "UNSET_BOUNCE_FROM_MSITE"

    const/16 v2, 0xa

    const-string v3, "unset_bounce_from_msite"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->UNSET_BOUNCE_FROM_MSITE:LX/GB4;

    .line 2326360
    new-instance v0, LX/GB4;

    const-string v1, "BOUNCE_FROM_MSITE"

    const/16 v2, 0xb

    const-string v3, "bounce_from_msite"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->BOUNCE_FROM_MSITE:LX/GB4;

    .line 2326361
    new-instance v0, LX/GB4;

    const-string v1, "BACKGROUND_FETCH_DEVICE_DATA"

    const/16 v2, 0xc

    const-string v3, "background_fetch_device_data"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->BACKGROUND_FETCH_DEVICE_DATA:LX/GB4;

    .line 2326362
    new-instance v0, LX/GB4;

    const-string v1, "DEVICE_DATA_READY"

    const/16 v2, 0xd

    const-string v3, "device_data_ready"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->DEVICE_DATA_READY:LX/GB4;

    .line 2326363
    new-instance v0, LX/GB4;

    const-string v1, "CUID_READY"

    const/16 v2, 0xe

    const-string v3, "cuid_ready"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->CUID_READY:LX/GB4;

    .line 2326364
    new-instance v0, LX/GB4;

    const-string v1, "PARALLEL_SEARCH_READY"

    const/16 v2, 0xf

    const-string v3, "parallel_search_ready"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->PARALLEL_SEARCH_READY:LX/GB4;

    .line 2326365
    new-instance v0, LX/GB4;

    const-string v1, "AUTO_IDENTIFY_STARTED"

    const/16 v2, 0x10

    const-string v3, "auto_identify_started"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->AUTO_IDENTIFY_STARTED:LX/GB4;

    .line 2326366
    new-instance v0, LX/GB4;

    const-string v1, "AUTO_IDENTIFY_PERFORMED"

    const/16 v2, 0x11

    const-string v3, "auto_identify_performed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->AUTO_IDENTIFY_PERFORMED:LX/GB4;

    .line 2326367
    new-instance v0, LX/GB4;

    const-string v1, "AUTO_IDENTIFY_FAILED"

    const/16 v2, 0x12

    const-string v3, "auto_identify_failed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->AUTO_IDENTIFY_FAILED:LX/GB4;

    .line 2326368
    new-instance v0, LX/GB4;

    const-string v1, "AUTO_IDENTIFY_REJECTED"

    const/16 v2, 0x13

    const-string v3, "auto_identify_rejected"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->AUTO_IDENTIFY_REJECTED:LX/GB4;

    .line 2326369
    new-instance v0, LX/GB4;

    const-string v1, "SEARCH_VIEWED"

    const/16 v2, 0x14

    const-string v3, "search_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SEARCH_VIEWED:LX/GB4;

    .line 2326370
    new-instance v0, LX/GB4;

    const-string v1, "SEARCH_SENT"

    const/16 v2, 0x15

    const-string v3, "search_sent"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SEARCH_SENT:LX/GB4;

    .line 2326371
    new-instance v0, LX/GB4;

    const-string v1, "SEARCH_PERFORMED"

    const/16 v2, 0x16

    const-string v3, "search_performed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SEARCH_PERFORMED:LX/GB4;

    .line 2326372
    new-instance v0, LX/GB4;

    const-string v1, "CAPTCHA_REQUIRED"

    const/16 v2, 0x17

    const-string v3, "ar_search_captcha_required"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->CAPTCHA_REQUIRED:LX/GB4;

    .line 2326373
    new-instance v0, LX/GB4;

    const-string v1, "LIST_SHOWN"

    const/16 v2, 0x18

    const-string v3, "list_shown"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->LIST_SHOWN:LX/GB4;

    .line 2326374
    new-instance v0, LX/GB4;

    const-string v1, "FRIEND_SEARCH_VIEWED"

    const/16 v2, 0x19

    const-string v3, "friend_search_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->FRIEND_SEARCH_VIEWED:LX/GB4;

    .line 2326375
    new-instance v0, LX/GB4;

    const-string v1, "FRIEND_SEARCH_SENT"

    const/16 v2, 0x1a

    const-string v3, "friend_search_sent"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->FRIEND_SEARCH_SENT:LX/GB4;

    .line 2326376
    new-instance v0, LX/GB4;

    const-string v1, "FRIEND_SEARCH_PERFORMED"

    const/16 v2, 0x1b

    const-string v3, "friend_search_performed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->FRIEND_SEARCH_PERFORMED:LX/GB4;

    .line 2326377
    new-instance v0, LX/GB4;

    const-string v1, "INITIATE_VIEWED"

    const/16 v2, 0x1c

    const-string v3, "initiate_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->INITIATE_VIEWED:LX/GB4;

    .line 2326378
    new-instance v0, LX/GB4;

    const-string v1, "EMAIL_LISTED_BEFORE_SMS"

    const/16 v2, 0x1d

    const-string v3, "email_listed_before_sms"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->EMAIL_LISTED_BEFORE_SMS:LX/GB4;

    .line 2326379
    new-instance v0, LX/GB4;

    const-string v1, "SENT_CODE_EMAIL"

    const/16 v2, 0x1e

    const-string v3, "sent_code_email"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SENT_CODE_EMAIL:LX/GB4;

    .line 2326380
    new-instance v0, LX/GB4;

    const-string v1, "SENT_CODE_SMS"

    const/16 v2, 0x1f

    const-string v3, "sent_code_sms"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SENT_CODE_SMS:LX/GB4;

    .line 2326381
    new-instance v0, LX/GB4;

    const-string v1, "SMS_CODE_SEARCHED"

    const/16 v2, 0x20

    const-string v3, "sms_code_searched"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->SMS_CODE_SEARCHED:LX/GB4;

    .line 2326382
    new-instance v0, LX/GB4;

    const-string v1, "CODE_ENTRY_VIEWED"

    const/16 v2, 0x21

    const-string v3, "code_entry_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->CODE_ENTRY_VIEWED:LX/GB4;

    .line 2326383
    new-instance v0, LX/GB4;

    const-string v1, "CODE_SUBMITTED"

    const/16 v2, 0x22

    const-string v3, "ar_code_submitted"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->CODE_SUBMITTED:LX/GB4;

    .line 2326384
    new-instance v0, LX/GB4;

    const-string v1, "LOGOUT_OPTION_SELECTED"

    const/16 v2, 0x23

    const-string v3, "logout_option_selected"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->LOGOUT_OPTION_SELECTED:LX/GB4;

    .line 2326385
    new-instance v0, LX/GB4;

    const-string v1, "CHANGE_PASSWORD_VIEWED"

    const/16 v2, 0x24

    const-string v3, "change_password_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->CHANGE_PASSWORD_VIEWED:LX/GB4;

    .line 2326386
    new-instance v0, LX/GB4;

    const-string v1, "PASSWORD_SHOWN"

    const/16 v2, 0x25

    const-string v3, "ar_password_shown"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->PASSWORD_SHOWN:LX/GB4;

    .line 2326387
    new-instance v0, LX/GB4;

    const-string v1, "CHANGE_PASSWORD_SUBMITTED"

    const/16 v2, 0x26

    const-string v3, "change_password_submitted"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->CHANGE_PASSWORD_SUBMITTED:LX/GB4;

    .line 2326388
    new-instance v0, LX/GB4;

    const-string v1, "RECOVERY_SUCCESS"

    const/16 v2, 0x27

    const-string v3, "recovery_success"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->RECOVERY_SUCCESS:LX/GB4;

    .line 2326389
    new-instance v0, LX/GB4;

    const-string v1, "FDR_VIEWED"

    const/16 v2, 0x28

    const-string v3, "fdr_viewed"

    invoke-direct {v0, v1, v2, v3}, LX/GB4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GB4;->FDR_VIEWED:LX/GB4;

    .line 2326390
    const/16 v0, 0x29

    new-array v0, v0, [LX/GB4;

    sget-object v1, LX/GB4;->CHANGE_SEARCH_TYPE_CLICK:LX/GB4;

    aput-object v1, v0, v4

    sget-object v1, LX/GB4;->SHOW_HIDE_PASSWORD:LX/GB4;

    aput-object v1, v0, v5

    sget-object v1, LX/GB4;->SHOW_PASSWORD_CHECKED:LX/GB4;

    aput-object v1, v0, v6

    sget-object v1, LX/GB4;->SHOW_PASSWORD_UNCHECKED:LX/GB4;

    aput-object v1, v0, v7

    sget-object v1, LX/GB4;->FORGOT_PASSWORD_CLICK:LX/GB4;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/GB4;->HELP_CENTER_CLICK:LX/GB4;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GB4;->FB4A_ACCOUNT_RECOVERY:LX/GB4;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GB4;->MSITE_ACCOUNT_RECOVERY:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/GB4;->LOGIN_FROM_MSITE:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/GB4;->TEST_BOUNCE_FROM_MSITE:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/GB4;->UNSET_BOUNCE_FROM_MSITE:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/GB4;->BOUNCE_FROM_MSITE:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/GB4;->BACKGROUND_FETCH_DEVICE_DATA:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/GB4;->DEVICE_DATA_READY:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/GB4;->CUID_READY:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/GB4;->PARALLEL_SEARCH_READY:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/GB4;->AUTO_IDENTIFY_STARTED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/GB4;->AUTO_IDENTIFY_PERFORMED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/GB4;->AUTO_IDENTIFY_FAILED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/GB4;->AUTO_IDENTIFY_REJECTED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/GB4;->SEARCH_VIEWED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/GB4;->SEARCH_SENT:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/GB4;->SEARCH_PERFORMED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/GB4;->CAPTCHA_REQUIRED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/GB4;->LIST_SHOWN:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/GB4;->FRIEND_SEARCH_VIEWED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/GB4;->FRIEND_SEARCH_SENT:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/GB4;->FRIEND_SEARCH_PERFORMED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/GB4;->INITIATE_VIEWED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/GB4;->EMAIL_LISTED_BEFORE_SMS:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/GB4;->SENT_CODE_EMAIL:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/GB4;->SENT_CODE_SMS:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/GB4;->SMS_CODE_SEARCHED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/GB4;->CODE_ENTRY_VIEWED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/GB4;->CODE_SUBMITTED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/GB4;->LOGOUT_OPTION_SELECTED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/GB4;->CHANGE_PASSWORD_VIEWED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/GB4;->PASSWORD_SHOWN:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/GB4;->CHANGE_PASSWORD_SUBMITTED:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/GB4;->RECOVERY_SUCCESS:LX/GB4;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/GB4;->FDR_VIEWED:LX/GB4;

    aput-object v2, v0, v1

    sput-object v0, LX/GB4;->$VALUES:[LX/GB4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2326393
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2326394
    iput-object p3, p0, LX/GB4;->mEventName:Ljava/lang/String;

    .line 2326395
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GB4;
    .locals 1

    .prologue
    .line 2326396
    const-class v0, LX/GB4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GB4;

    return-object v0
.end method

.method public static values()[LX/GB4;
    .locals 1

    .prologue
    .line 2326392
    sget-object v0, LX/GB4;->$VALUES:[LX/GB4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GB4;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2326391
    iget-object v0, p0, LX/GB4;->mEventName:Ljava/lang/String;

    return-object v0
.end method
