.class public final LX/H5v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/H5w;


# direct methods
.method public constructor <init>(LX/H5w;)V
    .locals 0

    .prologue
    .line 2425578
    iput-object p1, p0, LX/H5v;->a:LX/H5w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2425579
    const-string v0, "OfferBrowserUpdater"

    const-string v1, "Error saving offer"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2425580
    iget-object v0, p0, LX/H5v;->a:LX/H5w;

    check-cast p1, Ljava/lang/Exception;

    .line 2425581
    const-string v1, "unique-code"

    iget-object v2, v0, LX/H5w;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2425582
    :goto_0
    return-void

    .line 2425583
    :cond_0
    const/4 v3, 0x0

    .line 2425584
    const v2, 0x1e9bf2

    move-object v1, p1

    check-cast v1, LX/4Ua;

    .line 2425585
    iget-object p0, v1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, p0

    .line 2425586
    iget v1, v1, Lcom/facebook/graphql/error/GraphQLError;->code:I

    if-ne v2, v1, :cond_1

    .line 2425587
    const-string v1, "claim_limit_hit"

    invoke-static {v0, v1, v3}, LX/H5w;->a(LX/H5w;Ljava/lang/String;Ljava/lang/String;)V

    .line 2425588
    :goto_1
    goto :goto_0

    .line 2425589
    :cond_1
    check-cast p1, LX/4Ua;

    .line 2425590
    const-string v1, "claim_failed"

    invoke-static {v0, v1, v3}, LX/H5w;->a(LX/H5w;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2425591
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2425592
    if-eqz p1, :cond_3

    .line 2425593
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425594
    if-eqz v0, :cond_3

    .line 2425595
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425596
    check-cast v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2425597
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425598
    check-cast v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewClaimToWalletMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    invoke-static {v0}, LX/H83;->a(LX/H70;)LX/H83;

    move-result-object v0

    .line 2425599
    iget-object v1, p0, LX/H5v;->a:LX/H5w;

    .line 2425600
    iget-object v2, v1, LX/H5w;->a:LX/H6T;

    invoke-virtual {v2, v0}, LX/H6T;->a(LX/H83;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2425601
    const-string v2, "expired"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/H5w;->a(LX/H5w;Ljava/lang/String;Ljava/lang/String;)V

    .line 2425602
    :cond_0
    :goto_0
    iget-object v1, p0, LX/H5v;->a:LX/H5w;

    .line 2425603
    iget-object v2, v1, LX/H5w;->i:Ljava/lang/String;

    if-nez v2, :cond_1

    iget-object v2, v1, LX/H5w;->j:Ljava/lang/String;

    if-nez v2, :cond_1

    iget-object v2, v1, LX/H5w;->j:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 2425604
    :cond_1
    iget-object v2, v1, LX/H5w;->e:LX/0Zb;

    iget-object v3, v1, LX/H5w;->f:LX/H82;

    iget-object v4, v1, LX/H5w;->i:Ljava/lang/String;

    iget-object p0, v1, LX/H5w;->j:Ljava/lang/String;

    iget-object p1, v1, LX/H5w;->k:Ljava/lang/String;

    invoke-virtual {v3, v0, v4, p0, p1}, LX/H82;->a(LX/H83;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2425605
    :cond_2
    :goto_1
    return-void

    .line 2425606
    :cond_3
    const-string v0, "OfferBrowserUpdater"

    const-string v1, "Error saving offer"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2425607
    :cond_4
    const-string v2, "unique-code"

    iget-object v3, v1, LX/H5w;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v0}, LX/H83;->I()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2425608
    :cond_5
    invoke-virtual {v0}, LX/H83;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, "unique-code"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2425609
    const-string v2, "unique_code_success"

    invoke-virtual {v0}, LX/H83;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LX/H83;->I()Z

    move-result p1

    invoke-static {v1, v2, v3, p1}, LX/H5w;->a(LX/H5w;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2425610
    :goto_2
    goto :goto_0

    .line 2425611
    :cond_6
    const-string v2, "claim_success"

    const/4 v3, 0x0

    invoke-virtual {v0}, LX/H83;->I()Z

    move-result p1

    invoke-static {v1, v2, v3, p1}, LX/H5w;->a(LX/H5w;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2
.end method
