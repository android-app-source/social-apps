.class public LX/FAK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/FAK;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/FAG;

.field private c:LX/FAI;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2206930
    const-class v0, LX/FAK;

    sput-object v0, LX/FAK;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206929
    return-void
.end method

.method public static a(LX/0QB;)LX/FAK;
    .locals 1

    .prologue
    .line 2206925
    new-instance v0, LX/FAK;

    invoke-direct {v0}, LX/FAK;-><init>()V

    .line 2206926
    move-object v0, v0

    .line 2206927
    return-object v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HardcodedIPAddressUse"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2206922
    const/4 v2, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 2206923
    :pswitch_0
    return v0

    .line 2206924
    :sswitch_0
    const-string v3, "localhost"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    goto :goto_0

    :sswitch_1
    const-string v3, "127.0.0.1"

    invoke-virtual {p0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x47ccd86d -> :sswitch_0
        0x59c3b57d -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static b(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 2206919
    new-instance v0, LX/FAJ;

    invoke-direct {v0, p0, p1}, LX/FAJ;-><init>(Ljava/lang/String;I)V

    .line 2206920
    sget-object v1, LX/FAK;->a:Ljava/lang/Class;

    const-string v2, "Socket connection made to %s:%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2206921
    return-void
.end method

.method private static c(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2206931
    new-instance v0, LX/FAJ;

    invoke-direct {v0, p0, p1}, LX/FAJ;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method private static d(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 2206918
    new-instance v0, Ljava/net/ConnectException;

    const-string v1, "Connection refused"

    invoke-direct {v0, v1}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()LX/FAI;
    .locals 1

    .prologue
    .line 2206917
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FAK;->c:LX/FAI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/FAI;)V
    .locals 3

    .prologue
    .line 2206909
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/FAK;->c:LX/FAI;

    .line 2206910
    sget-object v0, LX/FAH;->a:[I

    invoke-virtual {p1}, LX/FAI;->ordinal()I

    move-result v1

    aget v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 2206911
    :goto_0
    :pswitch_0
    monitor-exit p0

    return-void

    .line 2206912
    :pswitch_1
    :try_start_1
    new-instance v0, LX/FAG;

    invoke-direct {v0, p0}, LX/FAG;-><init>(LX/FAK;)V

    iput-object v0, p0, LX/FAK;->b:LX/FAG;

    invoke-static {v0}, Ljava/net/Socket;->setSocketImplFactory(Ljava/net/SocketImplFactory;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206913
    goto :goto_0

    .line 2206914
    :catch_0
    move-exception v0

    .line 2206915
    :try_start_2
    sget-object v1, LX/FAK;->a:Ljava/lang/Class;

    const-string v2, "Couldn\'t enable strict socket mode"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2206916
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 2206898
    sget-object v0, LX/FAH;->a:[I

    iget-object v1, p0, LX/FAK;->c:LX/FAI;

    invoke-virtual {v1}, LX/FAI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2206899
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2206900
    :pswitch_1
    invoke-static {p1, p2}, LX/FAK;->b(Ljava/lang/String;I)V

    goto :goto_0

    .line 2206901
    :pswitch_2
    invoke-static {p1, p2}, LX/FAK;->c(Ljava/lang/String;I)V

    goto :goto_0

    .line 2206902
    :pswitch_3
    invoke-static {p1, p2}, LX/FAK;->d(Ljava/lang/String;I)V

    goto :goto_0

    .line 2206903
    :pswitch_4
    invoke-static {p1}, LX/FAK;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2206904
    invoke-static {p1, p2}, LX/FAK;->b(Ljava/lang/String;I)V

    goto :goto_0

    .line 2206905
    :pswitch_5
    invoke-static {p1}, LX/FAK;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2206906
    invoke-static {p1, p2}, LX/FAK;->c(Ljava/lang/String;I)V

    goto :goto_0

    .line 2206907
    :pswitch_6
    invoke-static {p1}, LX/FAK;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2206908
    invoke-static {p1, p2}, LX/FAK;->d(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
