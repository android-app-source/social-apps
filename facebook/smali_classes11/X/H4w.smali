.class public final LX/H4w;
.super LX/3Eh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2423529
    iput-object p1, p0, LX/H4w;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-direct {p0, p2}, LX/3Eh;-><init>(Ljava/lang/Long;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 8

    .prologue
    .line 2423530
    invoke-super {p0, p1}, LX/3Eh;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 2423531
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2423532
    if-eqz v0, :cond_2

    .line 2423533
    iget-object v0, p0, LX/H4w;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    const/4 p0, 0x0

    .line 2423534
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;

    .line 2423535
    iget-object v2, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->Q:LX/1rn;

    invoke-virtual {v2}, LX/1rn;->c()V

    .line 2423536
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v2}, LX/3T7;->c()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v1, v1, Lcom/facebook/notifications/protocol/methods/FetchGraphQLNotificationsResult;->a:LX/3T7;

    invoke-virtual {v1}, LX/3T7;->b()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2423537
    invoke-static {v0, p0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->a(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;Z)V

    .line 2423538
    :cond_0
    iget-object v1, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v1, :cond_1

    .line 2423539
    iget-object v1, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ah:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2423540
    :cond_1
    iget-object v1, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->z:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x350005

    const/4 p0, 0x2

    invoke-interface {v1, v2, p0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2423541
    :goto_0
    return-void

    .line 2423542
    :cond_2
    iget-object v0, p0, LX/H4w;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    .line 2423543
    iget-object v1, p1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v1, v1

    .line 2423544
    if-eqz v1, :cond_3

    .line 2423545
    iget-object v1, p1, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v1, v1

    .line 2423546
    if-eqz v1, :cond_3

    .line 2423547
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2423548
    iget-object v2, p1, Lcom/facebook/fbservice/service/OperationResult;->errorCode:LX/1nY;

    move-object v2, v2

    .line 2423549
    invoke-virtual {v2}, LX/1nY;->ordinal()I

    move-result v2

    .line 2423550
    iget-object v3, p1, Lcom/facebook/fbservice/service/OperationResult;->errorDescription:Ljava/lang/String;

    move-object v3, v3

    .line 2423551
    if-nez v2, :cond_4

    .line 2423552
    :goto_1
    move-object v1, v1

    .line 2423553
    sget-object v2, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->b:Ljava/lang/Class;

    invoke-static {v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2423554
    iget-object v2, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->h:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->b:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_sync_failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2423555
    :cond_3
    iget-object v0, p0, LX/H4w;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->r(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f081154

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2423556
    invoke-super {p0, p1}, LX/3Eh;->onFailure(Ljava/lang/Throwable;)V

    .line 2423557
    iget-object v0, p0, LX/H4w;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->r(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    .line 2423558
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2423559
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/H4w;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
