.class public final LX/F7V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 0

    .prologue
    .line 2201820
    iput-object p1, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 2201821
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2201804
    iget-object v2, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v2}, LX/F73;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2201805
    :cond_0
    :goto_0
    return-void

    .line 2201806
    :cond_1
    iget-object v2, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v3, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget v3, v3, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->P:I

    add-int v4, p2, p3

    add-int/lit8 v4, v4, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2201807
    iput v3, v2, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->P:I

    .line 2201808
    iget-object v2, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    sget-object v3, LX/89v;->FRIENDS_CENTER:LX/89v;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    .line 2201809
    iget-object v3, v2, LX/F73;->o:LX/F72;

    move-object v2, v3

    .line 2201810
    sget-object v3, LX/F72;->DEFAULT:LX/F72;

    if-ne v2, v3, :cond_0

    .line 2201811
    iget-object v2, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->A:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 2201812
    iget-object v2, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->A:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2201813
    :cond_2
    iget-object v2, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v2}, LX/F73;->e()I

    move-result v3

    .line 2201814
    add-int v2, p2, p3

    add-int/lit8 v4, v3, 0x3

    if-gt v2, v4, :cond_3

    move v2, v0

    .line 2201815
    :goto_1
    add-int v4, p2, p3

    add-int/lit8 v4, v4, 0x5

    if-lt v4, v3, :cond_4

    if-eqz v2, :cond_4

    .line 2201816
    :goto_2
    if-eqz v0, :cond_0

    .line 2201817
    iget-object v0, p0, LX/F7V;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->q(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 2201818
    goto :goto_1

    :cond_4
    move v0, v1

    .line 2201819
    goto :goto_2
.end method
