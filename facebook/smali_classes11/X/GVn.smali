.class public LX/GVn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2359926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2359927
    const-string v0, ""

    iput-object v0, p0, LX/GVn;->a:Ljava/lang/String;

    .line 2359928
    const-string v0, ""

    iput-object v0, p0, LX/GVn;->b:Ljava/lang/String;

    .line 2359929
    const-string v0, ""

    iput-object v0, p0, LX/GVn;->c:Ljava/lang/String;

    .line 2359930
    iput v1, p0, LX/GVn;->d:I

    .line 2359931
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 2359932
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2359933
    const-string v2, "app_id"

    const-string v3, "string"

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 2359934
    if-eqz v2, :cond_0

    .line 2359935
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/GVn;->a:Ljava/lang/String;

    .line 2359936
    :cond_0
    const-string v2, "fb_mobile_app_name"

    const-string v3, "string"

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 2359937
    if-eqz v2, :cond_1

    .line 2359938
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GVn;->b:Ljava/lang/String;

    .line 2359939
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2359940
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 2359941
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v2, p0, LX/GVn;->c:Ljava/lang/String;

    .line 2359942
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v0, p0, LX/GVn;->d:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2359943
    :goto_0
    invoke-static {p1}, LX/0sK;->a(Landroid/content/Context;)LX/0sK;

    move-result-object v0

    invoke-virtual {v0}, LX/0sK;->c()I

    move-result v0

    .line 2359944
    if-nez v0, :cond_2

    .line 2359945
    iget v0, p0, LX/GVn;->d:I

    .line 2359946
    :cond_2
    iput v0, p0, LX/GVn;->e:I

    .line 2359947
    return-void

    .line 2359948
    :catch_0
    move-exception v0

    .line 2359949
    const-string v2, "React"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to find PackageInfo for current App : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
