.class public LX/FCl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/FCl;


# instance fields
.field public final a:Landroid/media/AudioManager;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FCh;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/FCw;

.field public final d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/FCh;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/FCg;

.field public final f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field public g:LX/FCh;

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/media/AudioManager;LX/0Or;LX/FCw;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/AudioManager;",
            "LX/0Or",
            "<",
            "LX/FCh;",
            ">;",
            "LX/FCw;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2210558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210559
    new-instance v0, LX/FCi;

    invoke-direct {v0, p0}, LX/FCi;-><init>(LX/FCl;)V

    iput-object v0, p0, LX/FCl;->e:LX/FCg;

    .line 2210560
    new-instance v0, LX/FCj;

    invoke-direct {v0, p0}, LX/FCj;-><init>(LX/FCl;)V

    iput-object v0, p0, LX/FCl;->f:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 2210561
    iput-object p1, p0, LX/FCl;->a:Landroid/media/AudioManager;

    .line 2210562
    iput-object p2, p0, LX/FCl;->b:LX/0Or;

    .line 2210563
    iput-object p3, p0, LX/FCl;->c:LX/FCw;

    .line 2210564
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/FCl;->d:Ljava/util/Queue;

    .line 2210565
    return-void
.end method

.method public static a(LX/0QB;)LX/FCl;
    .locals 6

    .prologue
    .line 2210566
    sget-object v0, LX/FCl;->j:LX/FCl;

    if-nez v0, :cond_1

    .line 2210567
    const-class v1, LX/FCl;

    monitor-enter v1

    .line 2210568
    :try_start_0
    sget-object v0, LX/FCl;->j:LX/FCl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2210569
    if-eqz v2, :cond_0

    .line 2210570
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2210571
    new-instance v5, LX/FCl;

    invoke-static {v0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    const/16 v4, 0x2665

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/FCw;->a(LX/0QB;)LX/FCw;

    move-result-object v4

    check-cast v4, LX/FCw;

    invoke-direct {v5, v3, p0, v4}, LX/FCl;-><init>(Landroid/media/AudioManager;LX/0Or;LX/FCw;)V

    .line 2210572
    move-object v0, v5

    .line 2210573
    sput-object v0, LX/FCl;->j:LX/FCl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2210574
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2210575
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2210576
    :cond_1
    sget-object v0, LX/FCl;->j:LX/FCl;

    return-object v0

    .line 2210577
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2210578
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2210579
    iget-object v0, p0, LX/FCl;->g:LX/FCh;

    if-eqz v0, :cond_0

    .line 2210580
    iget-object v0, p0, LX/FCl;->g:LX/FCh;

    invoke-virtual {v0}, LX/FCh;->b()V

    .line 2210581
    :cond_0
    iget-object v0, p0, LX/FCl;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCh;

    .line 2210582
    invoke-virtual {v0}, LX/FCh;->b()V

    .line 2210583
    iget-object v3, v0, LX/FCh;->h:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 2210584
    goto :goto_0

    .line 2210585
    :cond_1
    iget-object v0, p0, LX/FCl;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2210586
    iget-boolean v0, p0, LX/FCl;->i:Z

    if-eqz v0, :cond_2

    .line 2210587
    iput-boolean v1, p0, LX/FCl;->i:Z

    .line 2210588
    const/4 v0, 0x1

    .line 2210589
    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
