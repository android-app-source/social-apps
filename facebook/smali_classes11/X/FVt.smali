.class public LX/FVt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FVs;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

.field public final h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

.field public final l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;


# direct methods
.method public constructor <init>(LX/FVr;)V
    .locals 1

    .prologue
    .line 2251356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2251357
    iget-object v0, p1, LX/FVr;->a:Ljava/lang/String;

    iput-object v0, p0, LX/FVt;->a:Ljava/lang/String;

    .line 2251358
    iget-object v0, p1, LX/FVr;->b:Ljava/lang/String;

    iput-object v0, p0, LX/FVt;->b:Ljava/lang/String;

    .line 2251359
    iget-object v0, p1, LX/FVr;->c:Ljava/lang/String;

    iput-object v0, p0, LX/FVt;->c:Ljava/lang/String;

    .line 2251360
    iget-object v0, p1, LX/FVr;->d:Ljava/lang/String;

    iput-object v0, p0, LX/FVt;->d:Ljava/lang/String;

    .line 2251361
    iget-object v0, p1, LX/FVr;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    iput-object v0, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 2251362
    iget-object v0, p1, LX/FVr;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    iput-object v0, p0, LX/FVt;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    .line 2251363
    iget-object v0, p1, LX/FVr;->i:Ljava/lang/String;

    iput-object v0, p0, LX/FVt;->i:Ljava/lang/String;

    .line 2251364
    iget-boolean v0, p1, LX/FVr;->j:Z

    iput-boolean v0, p0, LX/FVt;->j:Z

    .line 2251365
    iget-object v0, p1, LX/FVr;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    iput-object v0, p0, LX/FVt;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    .line 2251366
    iget-object v0, p1, LX/FVr;->f:Ljava/lang/String;

    iput-object v0, p0, LX/FVt;->e:Ljava/lang/String;

    .line 2251367
    iget-object v0, p1, LX/FVr;->e:Ljava/lang/String;

    iput-object v0, p0, LX/FVt;->f:Ljava/lang/String;

    .line 2251368
    iget-object v0, p1, LX/FVr;->l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    iput-object v0, p0, LX/FVt;->l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    .line 2251369
    return-void
.end method


# virtual methods
.method public final a()LX/FV0;
    .locals 1

    .prologue
    .line 2251370
    sget-object v0, LX/FV0;->SAVED_DASHBOARD_SAVED_ITEM:LX/FV0;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2251371
    if-ne p0, p1, :cond_1

    .line 2251372
    :cond_0
    :goto_0
    return v0

    .line 2251373
    :cond_1
    instance-of v2, p1, LX/FVt;

    if-nez v2, :cond_2

    move v0, v1

    .line 2251374
    goto :goto_0

    .line 2251375
    :cond_2
    check-cast p1, LX/FVt;

    .line 2251376
    iget-object v2, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 2251377
    iget-object v3, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2251378
    if-eq v2, v3, :cond_0

    .line 2251379
    iget-object v2, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    if-eqz v2, :cond_3

    .line 2251380
    iget-object v2, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v2, v2

    .line 2251381
    if-eqz v2, :cond_3

    iget-object v2, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v2

    .line 2251382
    iget-object v3, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2251383
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2251384
    iget-object v0, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251385
    iget-object v0, p0, LX/FVt;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2251386
    const/4 v0, 0x0

    .line 2251387
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FVt;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2251388
    iget-object v2, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->v()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    iget-object v3, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    iget-object v3, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->z()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
