.class public final enum LX/F8L;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F8L;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F8L;

.field public static final enum DEFAULT:LX/F8L;

.field public static final enum FAILURE:LX/F8L;

.field public static final enum LOADING_MORE:LX/F8L;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2203955
    new-instance v0, LX/F8L;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/F8L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8L;->DEFAULT:LX/F8L;

    .line 2203956
    new-instance v0, LX/F8L;

    const-string v1, "LOADING_MORE"

    invoke-direct {v0, v1, v3}, LX/F8L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8L;->LOADING_MORE:LX/F8L;

    .line 2203957
    new-instance v0, LX/F8L;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v4}, LX/F8L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8L;->FAILURE:LX/F8L;

    .line 2203958
    const/4 v0, 0x3

    new-array v0, v0, [LX/F8L;

    sget-object v1, LX/F8L;->DEFAULT:LX/F8L;

    aput-object v1, v0, v2

    sget-object v1, LX/F8L;->LOADING_MORE:LX/F8L;

    aput-object v1, v0, v3

    sget-object v1, LX/F8L;->FAILURE:LX/F8L;

    aput-object v1, v0, v4

    sput-object v0, LX/F8L;->$VALUES:[LX/F8L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2203959
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F8L;
    .locals 1

    .prologue
    .line 2203960
    const-class v0, LX/F8L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F8L;

    return-object v0
.end method

.method public static values()[LX/F8L;
    .locals 1

    .prologue
    .line 2203961
    sget-object v0, LX/F8L;->$VALUES:[LX/F8L;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F8L;

    return-object v0
.end method
