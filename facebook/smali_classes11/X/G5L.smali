.class public LX/G5L;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/search/api/SearchTypeaheadResult;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation
.end field

.field public final f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/lang/String;Ljava/util/List;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2318683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318684
    iput-object p1, p0, LX/G5L;->a:Ljava/lang/String;

    .line 2318685
    iput-object p2, p0, LX/G5L;->b:Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 2318686
    iput p3, p0, LX/G5L;->d:I

    .line 2318687
    iput-object p4, p0, LX/G5L;->c:Ljava/lang/String;

    .line 2318688
    iput-object p5, p0, LX/G5L;->e:Ljava/util/List;

    .line 2318689
    iput-wide p6, p0, LX/G5L;->f:J

    .line 2318690
    return-void
.end method
