.class public final LX/H53;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V
    .locals 1

    .prologue
    .line 2423593
    iput-object p1, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423594
    const/4 v0, 0x0

    iput v0, p0, LX/H53;->b:I

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 2423595
    iget-object v0, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v0, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->r:LX/2ix;

    invoke-virtual {v0, p1, p2}, LX/2ix;->a(LX/0g8;I)V

    .line 2423596
    iput p2, p0, LX/H53;->b:I

    .line 2423597
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 3

    .prologue
    .line 2423598
    iget-object v0, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    .line 2423599
    iput p2, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ar:I

    .line 2423600
    iget-object v0, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    .line 2423601
    iput p3, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->aq:I

    .line 2423602
    iget-object v1, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    add-int v0, p2, p3

    iget-object v2, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v2, v2, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    invoke-virtual {v2}, LX/1OM;->ij_()I

    move-result v2

    if-ne v0, v2, :cond_2

    add-int v0, p2, p3

    add-int/lit8 v0, v0, -0x1

    iget-object v2, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget v2, v2, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ao:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2423603
    :goto_0
    iput v0, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ao:I

    .line 2423604
    iget v0, p0, LX/H53;->b:I

    if-eqz v0, :cond_0

    .line 2423605
    iget-object v0, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    const/4 v1, 0x1

    .line 2423606
    iput-boolean v1, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ap:Z

    .line 2423607
    :cond_0
    iget-object v0, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v0, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-lez v0, :cond_1

    add-int v0, p2, p3

    iget-object v1, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v1, v1, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    if-lt v0, v1, :cond_1

    iget-object v0, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-boolean v0, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ak:Z

    if-eqz v0, :cond_3

    .line 2423608
    :cond_1
    :goto_1
    return-void

    .line 2423609
    :cond_2
    add-int v0, p2, p3

    iget-object v2, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget v2, v2, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->ao:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 2423610
    :cond_3
    iget-object v0, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    iget-object v0, v0, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->T:LX/2kw;

    .line 2423611
    iget-object v1, v0, LX/2kw;->i:LX/2kx;

    sget-object v2, LX/2kx;->LOADING:LX/2kx;

    if-ne v1, v2, :cond_4

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 2423612
    if-eqz v0, :cond_1

    .line 2423613
    iget-object v0, p0, LX/H53;->a:Lcom/facebook/notifications/multirow/NotificationsFeedFragment;

    invoke-static {v0}, Lcom/facebook/notifications/multirow/NotificationsFeedFragment;->v(Lcom/facebook/notifications/multirow/NotificationsFeedFragment;)V

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method
