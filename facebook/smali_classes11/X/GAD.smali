.class public final enum LX/GAD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GAD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GAD;

.field public static final enum LOGIN_RECOVERABLE:LX/GAD;

.field public static final enum OTHER:LX/GAD;

.field public static final enum TRANSIENT:LX/GAD;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2324810
    new-instance v0, LX/GAD;

    const-string v1, "LOGIN_RECOVERABLE"

    invoke-direct {v0, v1, v2}, LX/GAD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAD;->LOGIN_RECOVERABLE:LX/GAD;

    .line 2324811
    new-instance v0, LX/GAD;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v3}, LX/GAD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAD;->OTHER:LX/GAD;

    .line 2324812
    new-instance v0, LX/GAD;

    const-string v1, "TRANSIENT"

    invoke-direct {v0, v1, v4}, LX/GAD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAD;->TRANSIENT:LX/GAD;

    .line 2324813
    const/4 v0, 0x3

    new-array v0, v0, [LX/GAD;

    sget-object v1, LX/GAD;->LOGIN_RECOVERABLE:LX/GAD;

    aput-object v1, v0, v2

    sget-object v1, LX/GAD;->OTHER:LX/GAD;

    aput-object v1, v0, v3

    sget-object v1, LX/GAD;->TRANSIENT:LX/GAD;

    aput-object v1, v0, v4

    sput-object v0, LX/GAD;->$VALUES:[LX/GAD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2324814
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GAD;
    .locals 1

    .prologue
    .line 2324815
    const-class v0, LX/GAD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GAD;

    return-object v0
.end method

.method public static values()[LX/GAD;
    .locals 1

    .prologue
    .line 2324816
    sget-object v0, LX/GAD;->$VALUES:[LX/GAD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GAD;

    return-object v0
.end method
