.class public final LX/EzZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Eza;

.field public final synthetic b:LX/Ezb;


# direct methods
.method public constructor <init>(LX/Ezb;LX/Eza;)V
    .locals 0

    .prologue
    .line 2187325
    iput-object p1, p0, LX/EzZ;->b:LX/Ezb;

    iput-object p2, p0, LX/EzZ;->a:LX/Eza;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2ff363ef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2187326
    iget-object v1, p0, LX/EzZ;->b:LX/Ezb;

    iget-object v1, v1, LX/Ezb;->b:LX/Ezh;

    iget-object v2, p0, LX/EzZ;->a:LX/Eza;

    invoke-virtual {v2}, LX/1a1;->e()I

    move-result v2

    .line 2187327
    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 2187328
    :cond_0
    :goto_0
    const v1, 0x7c552afa

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2187329
    :cond_1
    iget-object v4, v1, LX/Ezh;->c:LX/Ezl;

    invoke-virtual {v4, v2}, LX/Ezl;->a(I)Lcom/facebook/friends/model/PersonYouMayKnow;

    move-result-object v4

    .line 2187330
    if-eqz v4, :cond_0

    .line 2187331
    iget-object v5, v4, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v11, v5

    .line 2187332
    invoke-virtual {v4}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    .line 2187333
    iput-object v9, v4, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2187334
    sget-object v5, LX/Ezf;->a:[I

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2187335
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    :goto_1
    move-object v5, v5

    .line 2187336
    invoke-virtual {v4, v5}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2187337
    iget-object v5, v1, LX/Ezh;->a:LX/2iT;

    invoke-virtual {v4}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v6

    invoke-virtual {v4}, Lcom/facebook/friends/model/PersonYouMayKnow;->b()Ljava/lang/String;

    sget-object v8, LX/2h7;->PYMK_TIMELINE:LX/2h7;

    new-instance v10, LX/Eze;

    invoke-direct {v10, v1, v4, v9, v11}, LX/Eze;-><init>(LX/Ezh;Lcom/facebook/friends/model/PersonYouMayKnow;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    invoke-virtual/range {v5 .. v10}, LX/2iT;->a(JLX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)V

    .line 2187338
    iget-object v4, v1, LX/Ezh;->b:LX/Ezb;

    invoke-virtual {v4, v2}, LX/1OM;->i_(I)V

    goto :goto_0

    .line 2187339
    :pswitch_0
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_1

    .line 2187340
    :pswitch_1
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_1

    .line 2187341
    :pswitch_2
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_1

    .line 2187342
    :pswitch_3
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
