.class public LX/Gn5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0So;

.field public final c:LX/GnF;

.field public d:Landroid/app/Activity;

.field public e:LX/1P1;

.field public f:LX/GpO;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0So;LX/GnF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393388
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2393389
    iput-object p1, p0, LX/Gn5;->a:Landroid/content/Context;

    .line 2393390
    iput-object p2, p0, LX/Gn5;->b:LX/0So;

    .line 2393391
    iput-object p3, p0, LX/Gn5;->c:LX/GnF;

    .line 2393392
    return-void
.end method

.method public static a(LX/0QB;)LX/Gn5;
    .locals 6

    .prologue
    .line 2393393
    const-class v1, LX/Gn5;

    monitor-enter v1

    .line 2393394
    :try_start_0
    sget-object v0, LX/Gn5;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2393395
    sput-object v2, LX/Gn5;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2393396
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393397
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2393398
    new-instance p0, LX/Gn5;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v5

    check-cast v5, LX/GnF;

    invoke-direct {p0, v3, v4, v5}, LX/Gn5;-><init>(Landroid/content/Context;LX/0So;LX/GnF;)V

    .line 2393399
    move-object v0, p0

    .line 2393400
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2393401
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gn5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393402
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2393403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/CHX;)Z
    .locals 2

    .prologue
    .line 2393404
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1xP;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->OPEN_URL:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
