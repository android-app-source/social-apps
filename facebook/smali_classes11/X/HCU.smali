.class public final LX/HCU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;)V
    .locals 0

    .prologue
    .line 2438706
    iput-object p1, p0, LX/HCU;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2438704
    iget-object v0, p0, LX/HCU;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "failed to get details of current fragment"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2438705
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2438678
    check-cast p1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;

    .line 2438679
    iget-object v0, p0, LX/HCU;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2438680
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v2, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0Tp;->b(Z)V

    .line 2438681
    invoke-virtual {p1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;

    .line 2438682
    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    move v4, v2

    :goto_1
    invoke-static {v4}, LX/0Tp;->b(Z)V

    .line 2438683
    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v2

    :goto_2
    invoke-static {v4}, LX/0Tp;->b(Z)V

    .line 2438684
    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    :goto_3
    invoke-static {v2}, LX/0Tp;->b(Z)V

    .line 2438685
    iget-object v2, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->k()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2438686
    iget-object v2, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2438687
    iget-object v2, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2438688
    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->n()LX/0Px;

    move-result-object v2

    iget-object v4, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->k:Landroid/widget/LinearLayout;

    invoke-static {v0, v2, v4}, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->a(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;LX/0Px;Landroid/widget/LinearLayout;)V

    .line 2438689
    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->a()LX/0Px;

    move-result-object v2

    iget-object v4, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->l:Landroid/widget/LinearLayout;

    invoke-static {v0, v2, v4}, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->a(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;LX/0Px;Landroid/widget/LinearLayout;)V

    .line 2438690
    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->l()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2438691
    iget-object v2, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->o:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2438692
    :goto_4
    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->o()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_5
    if-ge v3, v5, :cond_5

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;

    .line 2438693
    invoke-virtual {v2}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->j()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$PreviewTextModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel;->a()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$TabsModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v2

    iget-object p0, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->m:Landroid/widget/LinearLayout;

    invoke-static {v0, v6, v2, p0}, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->a(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;Ljava/lang/String;Ljava/lang/String;Landroid/widget/LinearLayout;)V

    .line 2438694
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_0
    move v1, v3

    .line 2438695
    goto/16 :goto_0

    :cond_1
    move v4, v3

    .line 2438696
    goto/16 :goto_1

    :cond_2
    move v4, v3

    .line 2438697
    goto/16 :goto_2

    :cond_3
    move v2, v3

    .line 2438698
    goto :goto_3

    .line 2438699
    :cond_4
    iget-object v2, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->n:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    goto :goto_4

    .line 2438700
    :cond_5
    const-class v2, LX/1ZF;

    invoke-virtual {v0, v2}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ZF;

    .line 2438701
    if-eqz v2, :cond_6

    .line 2438702
    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel;->m()Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/editpage/graphql/FetchTemplateDetailsQueryModels$FetchTemplateDetailsQueryModel$TemplatesModel$NameModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2438703
    :cond_6
    return-void
.end method
