.class public final LX/G4G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# instance fields
.field public final synthetic a:LX/G4B;

.field public final synthetic b:Lcom/facebook/timeline/service/ProfileLoadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4B;)V
    .locals 0

    .prologue
    .line 2317274
    iput-object p1, p0, LX/G4G;->b:Lcom/facebook/timeline/service/ProfileLoadManager;

    iput-object p2, p0, LX/G4G;->a:LX/G4B;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2317275
    iget-object v0, p0, LX/G4G;->a:LX/G4B;

    invoke-virtual {v0}, LX/G4B;->b()V

    .line 2317276
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2317277
    instance-of v0, p1, LX/FsK;

    if-eqz v0, :cond_1

    .line 2317278
    iget-object v0, p0, LX/G4G;->b:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v0, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->b:Lcom/facebook/timeline/service/TimelineImagePrefetcher;

    check-cast p1, LX/FsK;

    new-instance v1, LX/G4P;

    iget-object v2, p0, LX/G4G;->a:LX/G4B;

    invoke-direct {v1, v2}, LX/G4P;-><init>(LX/G4B;)V

    .line 2317279
    iget-object v2, v0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->d:LX/G4R;

    .line 2317280
    if-eqz p1, :cond_0

    iget-object v3, p1, LX/FsK;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;

    if-eqz v3, :cond_0

    iget-object v3, p1, LX/FsK;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    move-result-object v3

    if-nez v3, :cond_2

    .line 2317281
    :cond_0
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2317282
    :goto_0
    move-object v2, v3

    .line 2317283
    invoke-static {v0, v1, v2}, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->a(Lcom/facebook/timeline/service/TimelineImagePrefetcher;LX/G4P;Ljava/util/List;)V

    .line 2317284
    :cond_1
    return-void

    .line 2317285
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2317286
    iget-object v3, p1, LX/FsK;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    .line 2317287
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v7, :cond_4

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;

    .line 2317288
    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object p0

    .line 2317289
    if-eqz p0, :cond_3

    .line 2317290
    invoke-static {v2, v5, v3, p0}, LX/G4R;->a(LX/G4R;Ljava/util/List;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFieldsModel;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)V

    .line 2317291
    :cond_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2317292
    :cond_4
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2317293
    iget-object v0, p0, LX/G4G;->a:LX/G4B;

    invoke-virtual {v0}, LX/G4B;->b()V

    .line 2317294
    return-void
.end method
