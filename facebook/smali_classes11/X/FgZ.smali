.class public LX/FgZ;
.super LX/FgU;
.source ""


# instance fields
.field public final b:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

.field private final c:LX/33u;

.field private final d:LX/Bnj;

.field private final e:LX/Bnu;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;LX/7Hg;LX/7Hl;LX/7HW;LX/7Hk;LX/2Sd;LX/33u;LX/Bnj;LX/FgS;)V
    .locals 6
    .param p1    # Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270531
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/FgU;-><init>(LX/7Hg;LX/7Hl;LX/7HW;LX/7Hk;LX/2Sd;)V

    .line 2270532
    iput-object p7, p0, LX/FgZ;->c:LX/33u;

    .line 2270533
    iput-object p1, p0, LX/FgZ;->b:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2270534
    iput-object p8, p0, LX/FgZ;->d:LX/Bnj;

    .line 2270535
    new-instance v0, LX/Bnu;

    new-instance v1, LX/FgY;

    invoke-direct {v1, p0, p9}, LX/FgY;-><init>(LX/FgZ;LX/FgS;)V

    invoke-direct {v0, v1}, LX/Bnu;-><init>(LX/Bnp;)V

    iput-object v0, p0, LX/FgZ;->e:LX/Bnu;

    .line 2270536
    iget-object v0, p0, LX/FgZ;->d:LX/Bnj;

    iget-object v1, p0, LX/FgZ;->e:LX/Bnu;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2270537
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2270538
    invoke-direct {p0}, LX/FgZ;->i()LX/5pX;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2270539
    invoke-direct {p0}, LX/FgZ;->i()LX/5pX;

    move-result-object v0

    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    .line 2270540
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2270541
    const-string v2, "reactTag"

    iget-object v3, p0, LX/FgZ;->b:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v3}, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->m()I

    move-result v3

    invoke-interface {v1, v2, v3}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2270542
    iget-object v2, p0, LX/FgZ;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2270543
    const-string v2, "query"

    iget-object v3, p0, LX/FgZ;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2270544
    :cond_0
    const-string v2, "tabBarSelectedIndex"

    const-string v3, "0"

    invoke-interface {v1, v2, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2270545
    move-object v1, v1

    .line 2270546
    invoke-interface {v0, p1, v1}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2270547
    :cond_1
    return-void
.end method

.method private i()LX/5pX;
    .locals 1

    .prologue
    .line 2270548
    iget-object v0, p0, LX/FgZ;->c:LX/33u;

    invoke-virtual {v0}, LX/33u;->c()LX/33y;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FgZ;->c:LX/33u;

    invoke-virtual {v0}, LX/33u;->c()LX/33y;

    move-result-object v0

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7Hi;Ljava/lang/String;)LX/7HN;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/7HN",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270549
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/32G;)V
    .locals 2

    .prologue
    .line 2270550
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/7B6;)V
    .locals 2

    .prologue
    .line 2270551
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/7Hi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270557
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/Cvp;)V
    .locals 0

    .prologue
    .line 2270552
    return-void
.end method

.method public final a(LX/Fgp;)V
    .locals 2

    .prologue
    .line 2270553
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2270554
    if-eqz p1, :cond_0

    .line 2270555
    const-string v0, "kFBSearchContextSearchFocus"

    invoke-direct {p0, v0}, LX/FgZ;->a(Ljava/lang/String;)V

    .line 2270556
    :cond_0
    return-void
.end method

.method public final b()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270530
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(LX/7Hi;Ljava/lang/String;)LX/7Hi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270558
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270528
    iget-object v0, p0, LX/FgZ;->d:LX/Bnj;

    iget-object v1, p0, LX/FgZ;->e:LX/Bnu;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2270529
    return-void
.end method

.method public final b(LX/7B6;)Z
    .locals 1

    .prologue
    .line 2270514
    iget-object v0, p1, LX/7B6;->b:Ljava/lang/String;

    iput-object v0, p0, LX/FgZ;->f:Ljava/lang/String;

    .line 2270515
    const-string v0, "kFBSearchContextSearchEvent"

    invoke-direct {p0, v0}, LX/FgZ;->a(Ljava/lang/String;)V

    .line 2270516
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/7Hi;)LX/7Hc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270527
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2270526
    iget-object v0, p0, LX/FgZ;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2270524
    const-string v0, "kFBSearchContextSearchClear"

    invoke-direct {p0, v0}, LX/FgZ;->a(Ljava/lang/String;)V

    .line 2270525
    return-void
.end method

.method public final d(LX/7Hi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270523
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2270521
    const-string v0, "kFBSearchContextSearchTapped"

    invoke-direct {p0, v0}, LX/FgZ;->a(Ljava/lang/String;)V

    .line 2270522
    return-void
.end method

.method public final f()LX/0P2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270520
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 2270519
    return-void
.end method

.method public final h()Lcom/facebook/search/model/TypeaheadUnit;
    .locals 2

    .prologue
    .line 2270518
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final mI_()V
    .locals 2

    .prologue
    .line 2270517
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "React typeahead doesn\'t support method"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
