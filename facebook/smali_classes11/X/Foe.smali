.class public final LX/Foe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

.field private b:LX/Fod;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;LX/Fod;)V
    .locals 0

    .prologue
    .line 2289894
    iput-object p1, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2289895
    iput-object p2, p0, LX/Foe;->b:LX/Fod;

    .line 2289896
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2289897
    iget-object v0, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    const/4 v1, 0x0

    .line 2289898
    iput-object v1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2289899
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2289900
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2289901
    if-eqz p1, :cond_0

    .line 2289902
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2289903
    if-nez v0, :cond_1

    .line 2289904
    :cond_0
    :goto_0
    return-void

    .line 2289905
    :cond_1
    sget-object v0, LX/Foc;->a:[I

    iget-object v1, p0, LX/Foe;->b:LX/Fod;

    invoke-virtual {v1}, LX/Fod;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2289906
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2289907
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;

    .line 2289908
    iget-object v1, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    .line 2289909
    iput-object v3, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2289910
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_6

    .line 2289911
    iget-object v1, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    .line 2289912
    iput-object v3, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->f:Ljava/lang/String;

    .line 2289913
    iget-object v1, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    .line 2289914
    iput-boolean v4, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->g:Z

    .line 2289915
    :goto_1
    iget-object v1, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    iget-object v1, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->i:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v2, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    iget-boolean v2, v2, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->g:Z

    .line 2289916
    iget-object v3, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    .line 2289917
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2289918
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 2289919
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    if-ge v5, v8, :cond_4

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel$EdgesModel;

    .line 2289920
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel$EdgesModel;->a()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 2289921
    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel$EdgesModel;->a()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    move-result-object v4

    const/4 p1, 0x0

    const/4 v9, 0x1

    .line 2289922
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->l()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_c

    :cond_2
    move p0, v9

    :goto_3
    if-eqz p0, :cond_e

    :goto_4
    if-eqz v9, :cond_f

    .line 2289923
    const/4 v9, 0x0

    .line 2289924
    :goto_5
    move-object v4, v9

    .line 2289925
    if-eqz v4, :cond_3

    .line 2289926
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2289927
    :cond_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 2289928
    :cond_4
    move-object v4, v6

    .line 2289929
    iget-boolean v5, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->o:Z

    .line 2289930
    iput-boolean v2, v3, LX/Fob;->f:Z

    .line 2289931
    iput-boolean v5, v3, LX/Fob;->g:Z

    .line 2289932
    iget-object v6, v3, LX/Fob;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2289933
    iget-boolean v6, v3, LX/Fob;->f:Z

    if-nez v6, :cond_5

    .line 2289934
    iget-object v6, v3, LX/Fob;->d:Ljava/util/ArrayList;

    new-instance v7, LX/Foa;

    sget-object v8, LX/FoZ;->SEE_MORE:LX/FoZ;

    invoke-direct {v7, v8}, LX/Foa;-><init>(LX/FoZ;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2289935
    :cond_5
    const v6, 0x447484ed

    invoke-static {v3, v6}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2289936
    goto/16 :goto_0

    .line 2289937
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v3, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2289938
    iput-object v1, v3, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->f:Ljava/lang/String;

    .line 2289939
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoCharitiesModel$OtherCharitiesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v3, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, LX/15i;->h(II)Z

    move-result v1

    .line 2289940
    iput-boolean v1, v3, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->g:Z

    .line 2289941
    goto/16 :goto_1

    .line 2289942
    :pswitch_1
    iget-object v0, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    iget-object v1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->i:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    .line 2289943
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2289944
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;

    .line 2289945
    iget-object v2, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    .line 2289946
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;

    move-result-object v3

    .line 2289947
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-virtual {v3}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2289948
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;->j()Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;

    move-result-object v5

    .line 2289949
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_11

    :cond_7
    :goto_6
    move v3, v3

    .line 2289950
    if-nez v3, :cond_10

    .line 2289951
    :cond_8
    const/4 v3, 0x0

    .line 2289952
    :goto_7
    move-object v3, v3

    .line 2289953
    if-eqz v3, :cond_9

    .line 2289954
    iget-object v4, v3, LX/Foa;->a:LX/FoZ;

    move-object v4, v4

    .line 2289955
    sget-object v5, LX/FoZ;->HIGHLIGHTED_CHARITY:LX/FoZ;

    if-ne v4, v5, :cond_9

    .line 2289956
    sget-object v4, LX/FoZ;->HIGHLIGHTED_CHARITY:LX/FoZ;

    invoke-static {v2, v4}, LX/Fob;->a(LX/Fob;LX/FoZ;)I

    move-result v4

    .line 2289957
    iget-object v5, v2, LX/Fob;->d:Ljava/util/ArrayList;

    new-instance v6, LX/Foa;

    sget-object v1, LX/FoZ;->OTHER_CHARITIES_HEADER:LX/FoZ;

    invoke-direct {v6, v1}, LX/Foa;-><init>(LX/FoZ;)V

    invoke-virtual {v5, v4, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2289958
    iget-object v5, v2, LX/Fob;->d:Ljava/util/ArrayList;

    new-instance v6, LX/Foa;

    sget-object v1, LX/FoZ;->DIVIDER:LX/FoZ;

    invoke-direct {v6, v1}, LX/Foa;-><init>(LX/FoZ;)V

    invoke-virtual {v5, v4, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2289959
    iget-object v5, v2, LX/Fob;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v4, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2289960
    iget-object v5, v2, LX/Fob;->d:Ljava/util/ArrayList;

    new-instance v6, LX/Foa;

    sget-object v1, LX/FoZ;->DIVIDER:LX/FoZ;

    invoke-direct {v6, v1}, LX/Foa;-><init>(LX/FoZ;)V

    invoke-virtual {v5, v4, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2289961
    const v4, 0x5416aad0

    invoke-static {v2, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2289962
    :cond_9
    goto/16 :goto_0

    .line 2289963
    :pswitch_2
    iget-object v0, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    iget-object v1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->i:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    .line 2289964
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2289965
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;

    invoke-virtual {v1, v0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoTitlesModel;)V

    goto/16 :goto_0

    .line 2289966
    :pswitch_3
    iget-object v0, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    iget-object v1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->i:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    .line 2289967
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2289968
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;

    .line 2289969
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;->l()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->i:Ljava/lang/String;

    .line 2289970
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;->j()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->j:Ljava/lang/String;

    .line 2289971
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;->k()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->a(J)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2289972
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoPrefillModel;->k()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->k:Ljava/lang/Long;

    .line 2289973
    :cond_a
    goto/16 :goto_0

    .line 2289974
    :pswitch_4
    iget-object v0, p0, LX/Foe;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    iget-object v1, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->i:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    .line 2289975
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2289976
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2289977
    if-eqz v0, :cond_b

    iget-object v2, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    if-eqz v2, :cond_b

    .line 2289978
    iput-object v0, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->p:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2289979
    iget-object v2, v1, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->b:LX/Fob;

    new-instance v3, LX/Foa;

    invoke-direct {v3, v0}, LX/Foa;-><init>(Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;)V

    .line 2289980
    iget-object v1, v3, LX/Foa;->a:LX/FoZ;

    move-object v1, v1

    .line 2289981
    sget-object v0, LX/FoZ;->DAF_DISCLOSURE:LX/FoZ;

    if-ne v1, v0, :cond_b

    .line 2289982
    sget-object v1, LX/FoZ;->DAF_DISCLOSURE:LX/FoZ;

    invoke-static {v2, v1}, LX/Fob;->a(LX/Fob;LX/FoZ;)I

    move-result v1

    .line 2289983
    iget-object v0, v2, LX/Fob;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2289984
    const v1, -0x4c3cfd0c

    invoke-static {v2, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2289985
    :cond_b
    goto/16 :goto_0

    .line 2289986
    :cond_c
    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object p0

    iget p0, p0, LX/1vs;->b:I

    .line 2289987
    if-nez p0, :cond_d

    move p0, v9

    goto/16 :goto_3

    :cond_d
    move p0, p1

    goto/16 :goto_3

    .line 2289988
    :cond_e
    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v9

    iget-object p0, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    .line 2289989
    invoke-virtual {p0, v9, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    goto/16 :goto_4

    .line 2289990
    :cond_f
    new-instance v9, LX/Foa;

    invoke-direct {v9, v4}, LX/Foa;-><init>(Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;)V

    goto/16 :goto_5

    :cond_10
    new-instance v3, LX/Foa;

    invoke-direct {v3, v0}, LX/Foa;-><init>(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;)V

    goto/16 :goto_7

    .line 2289991
    :cond_11
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->k()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    if-eqz v6, :cond_12

    .line 2289992
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel$HighlightedCharityModel;->k()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2289993
    invoke-virtual {v6, v5, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    move v3, v4

    goto/16 :goto_6

    :cond_12
    move v3, v4

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
