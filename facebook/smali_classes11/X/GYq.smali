.class public LX/GYq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/GYs;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2365805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2365806
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GYq;->b:Z

    .line 2365807
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2365808
    iget-object v1, p0, LX/GYq;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2365809
    iput-boolean v0, p0, LX/GYq;->b:Z

    .line 2365810
    :goto_0
    return v0

    .line 2365811
    :cond_0
    iget-object v0, p0, LX/GYq;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GYs;

    .line 2365812
    invoke-interface {v0}, LX/GYs;->a()V

    .line 2365813
    const/4 v0, 0x1

    goto :goto_0
.end method
