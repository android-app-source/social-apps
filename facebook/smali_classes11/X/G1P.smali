.class public LX/G1P;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;",
            "LX/G1P;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/lang/String;


# instance fields
.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLInterfaces$TimelinePromptApproximateCountFields;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2311110
    new-instance v0, LX/G1O;

    invoke-direct {v0}, LX/G1O;-><init>()V

    sput-object v0, LX/G1P;->a:LX/0QK;

    .line 2311111
    sget-object v0, LX/0ax;->cW:Ljava/lang/String;

    sget-object v1, LX/5Oz;->TIMELINE_PENDING_REQUESTS_PROMPT:LX/5Oz;

    invoke-virtual {v1}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/5P0;->REQUESTS:LX/5P0;

    invoke-virtual {v2}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/G1P;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0am;LX/0am;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLInterfaces$TimelinePromptApproximateCountFields;",
            ">;",
            "LX/0am",
            "<",
            "Landroid/os/Bundle;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2311088
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2311089
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, LX/G1P;->b:LX/0am;

    .line 2311090
    iput-object p2, p0, LX/G1P;->c:LX/0am;

    .line 2311091
    iput-object p3, p0, LX/G1P;->d:Ljava/lang/String;

    .line 2311092
    iput-object p4, p0, LX/G1P;->e:Ljava/lang/String;

    .line 2311093
    iput-boolean p5, p0, LX/G1P;->f:Z

    .line 2311094
    return-void
.end method

.method public static a(LX/15i;ILjava/lang/String;)LX/G1P;
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "newInstanceFromFriendingPossibilities"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2311095
    new-instance v0, LX/G1P;

    new-instance v1, LX/5x9;

    invoke-direct {v1}, LX/5x9;-><init>()V

    invoke-virtual {p0, p1, v5}, LX/15i;->j(II)I

    move-result v2

    .line 2311096
    iput v2, v1, LX/5x9;->a:I

    .line 2311097
    move-object v1, v1

    .line 2311098
    const/4 v10, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2311099
    new-instance v6, LX/186;

    const/16 v7, 0x80

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    .line 2311100
    invoke-virtual {v6, v10}, LX/186;->c(I)V

    .line 2311101
    iget v7, v1, LX/5x9;->a:I

    invoke-virtual {v6, v9, v7, v9}, LX/186;->a(III)V

    .line 2311102
    invoke-virtual {v6}, LX/186;->d()I

    move-result v7

    .line 2311103
    invoke-virtual {v6, v7}, LX/186;->d(I)V

    .line 2311104
    invoke-virtual {v6}, LX/186;->e()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 2311105
    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2311106
    new-instance v6, LX/15i;

    move-object v9, v8

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2311107
    new-instance v7, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    invoke-direct {v7, v6}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;-><init>(LX/15i;)V

    .line 2311108
    move-object v1, v7

    .line 2311109
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    sget-object v4, LX/G1P;->g:Ljava/lang/String;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, LX/G1P;-><init>(LX/0am;LX/0am;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method
