.class public LX/FV8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FV3;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FV3",
        "<",
        "LX/FVH;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0kL;

.field public final c:LX/FW1;

.field private final d:LX/5up;

.field public final e:LX/FVu;

.field private f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/res/Resources;LX/0kL;LX/FW1;LX/5up;LX/FVu;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/saved/gating/annotations/IsSavedDashboardDeleteMenuEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "Landroid/content/res/Resources;",
            "LX/0kL;",
            "LX/FW1;",
            "LX/5up;",
            "LX/FVu;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250396
    iput-object p1, p0, LX/FV8;->f:LX/0Or;

    .line 2250397
    iput-object p2, p0, LX/FV8;->a:Landroid/content/res/Resources;

    .line 2250398
    iput-object p3, p0, LX/FV8;->b:LX/0kL;

    .line 2250399
    iput-object p4, p0, LX/FV8;->c:LX/FW1;

    .line 2250400
    iput-object p5, p0, LX/FV8;->d:LX/5up;

    .line 2250401
    iput-object p6, p0, LX/FV8;->e:LX/FVu;

    .line 2250402
    return-void
.end method

.method public static a(LX/0QB;)LX/FV8;
    .locals 10

    .prologue
    .line 2250384
    const-class v1, LX/FV8;

    monitor-enter v1

    .line 2250385
    :try_start_0
    sget-object v0, LX/FV8;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2250386
    sput-object v2, LX/FV8;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2250387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2250389
    new-instance v3, LX/FV8;

    const/16 v4, 0x35e

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static {v0}, LX/FW1;->a(LX/0QB;)LX/FW1;

    move-result-object v7

    check-cast v7, LX/FW1;

    invoke-static {v0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v8

    check-cast v8, LX/5up;

    invoke-static {v0}, LX/FVu;->a(LX/0QB;)LX/FVu;

    move-result-object v9

    check-cast v9, LX/FVu;

    invoke-direct/range {v3 .. v9}, LX/FV8;-><init>(LX/0Or;Landroid/content/res/Resources;LX/0kL;LX/FW1;LX/5up;LX/FVu;)V

    .line 2250390
    move-object v0, v3

    .line 2250391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2250392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FV8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2250393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2250394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/FVH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2250383
    const-class v0, LX/FVH;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2250403
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Z
    .locals 6

    .prologue
    .line 2250374
    check-cast p1, LX/FVH;

    const/4 v5, 0x1

    .line 2250375
    invoke-interface {p1}, LX/FVH;->f()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    .line 2250376
    new-instance v1, LX/FV7;

    invoke-direct {v1, p0, p1, v0}, LX/FV7;-><init>(LX/FV8;LX/FVH;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 2250377
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2250378
    iget-object v0, p0, LX/FV8;->d:LX/5up;

    invoke-interface {p1}, LX/FVE;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "native_saved_dashboard"

    const-string v4, "delete_button"

    invoke-virtual {v0, v2, v3, v4, v1}, LX/5up;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2250379
    :goto_0
    invoke-interface {p1}, LX/FVE;->b()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/FVt;

    if-eqz v0, :cond_0

    .line 2250380
    iget-object v1, p0, LX/FV8;->c:LX/FW1;

    new-instance v2, LX/FW2;

    invoke-interface {p1}, LX/FVE;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVt;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-static {v0, v4, v5}, LX/FVu;->a(LX/FVt;Lcom/facebook/graphql/enums/GraphQLSavedState;Z)LX/FVt;

    move-result-object v0

    invoke-direct {v2, v0}, LX/FW2;-><init>(LX/FVt;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2250381
    :cond_0
    return v5

    .line 2250382
    :cond_1
    iget-object v0, p0, LX/FV8;->d:LX/5up;

    invoke-interface {p1}, LX/FVE;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "native_saved_dashboard"

    const-string v4, "delete_button"

    invoke-virtual {v0, v2, v3, v4, v1}, LX/5up;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2250373
    iget-object v0, p0, LX/FV8;->a:Landroid/content/res/Resources;

    const v1, 0x7f081ae2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2250365
    check-cast p1, LX/FVH;

    const/4 v1, 0x0

    .line 2250366
    invoke-interface {p1}, LX/FVE;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2250367
    :goto_0
    return v0

    .line 2250368
    :cond_0
    invoke-interface {p1}, LX/FVH;->f()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    .line 2250369
    iget-object v0, p0, LX/FV8;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2250370
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2250371
    const/4 v0, 0x1

    goto :goto_0

    .line 2250372
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2250364
    const-string v0, "delete_button"

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2250363
    const v0, 0x7f0217a1

    return v0
.end method
