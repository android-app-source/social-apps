.class public LX/F5V;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/F5V;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2198842
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2198843
    const-string v0, "FBGroupsDiscoveryCategoryRoute"

    sget-object v1, LX/0ax;->W:Ljava/lang/String;

    const-string v2, "category_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/F5V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198844
    const-string v0, "FBGroupsDiscoveryCategoriesRoute"

    const-string v1, "/groups_discovery_categories"

    sget-object v2, LX/0ax;->Y:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, LX/F5V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2198845
    const-string v0, "FBGroupsDiscoveryTagRoute"

    sget-object v1, LX/0ax;->Z:Ljava/lang/String;

    const-string v2, "tag_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/F5V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198846
    return-void
.end method

.method public static a(LX/0QB;)LX/F5V;
    .locals 3

    .prologue
    .line 2198847
    sget-object v0, LX/F5V;->a:LX/F5V;

    if-nez v0, :cond_1

    .line 2198848
    const-class v1, LX/F5V;

    monitor-enter v1

    .line 2198849
    :try_start_0
    sget-object v0, LX/F5V;->a:LX/F5V;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2198850
    if-eqz v2, :cond_0

    .line 2198851
    :try_start_1
    new-instance v0, LX/F5V;

    invoke-direct {v0}, LX/F5V;-><init>()V

    .line 2198852
    move-object v0, v0

    .line 2198853
    sput-object v0, LX/F5V;->a:LX/F5V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2198854
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2198855
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2198856
    :cond_1
    sget-object v0, LX/F5V;->a:LX/F5V;

    return-object v0

    .line 2198857
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2198858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2198859
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2198860
    const-string v1, "route"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198861
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2198862
    const-string v1, "uri"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2198863
    :cond_0
    const-class v1, Lcom/facebook/base/activity/ReactFragmentActivity;

    sget-object v2, LX/0cQ;->GENERAL_GROUPS_REACT_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, p3, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2198864
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2198865
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/F5V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2198866
    return-void
.end method
