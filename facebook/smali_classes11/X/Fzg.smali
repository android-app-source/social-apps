.class public final LX/Fzg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fzo;


# direct methods
.method public constructor <init>(LX/Fzo;)V
    .locals 0

    .prologue
    .line 2308203
    iput-object p1, p0, LX/Fzg;->a:LX/Fzo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2308204
    check-cast p1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2308205
    if-nez p1, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_5

    :cond_0
    :goto_2
    if-eqz v0, :cond_6

    .line 2308206
    const/4 v0, 0x0

    .line 2308207
    :goto_3
    return-object v0

    .line 2308208
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2308209
    if-nez v2, :cond_2

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_0

    .line 2308210
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2308211
    invoke-virtual {v3, v2, v1}, LX/15i;->g(II)I

    move-result v2

    if-nez v2, :cond_4

    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_1

    .line 2308212
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2308213
    invoke-virtual {v3, v2, v1}, LX/15i;->g(II)I

    move-result v2

    invoke-virtual {v3, v2, v1}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_2

    .line 2308214
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
