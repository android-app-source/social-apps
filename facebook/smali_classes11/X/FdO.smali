.class public final LX/FdO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fc4;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2263421
    iput-object p1, p0, LX/FdO;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2263422
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7Hc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2263423
    iget-object v0, p4, LX/7Hc;->b:LX/0Px;

    move-object v0, v0

    .line 2263424
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FdO;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->i:LX/Fcx;

    if-nez v0, :cond_1

    .line 2263425
    :cond_0
    :goto_0
    return-void

    .line 2263426
    :cond_1
    iget-object v0, p0, LX/FdO;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->i:LX/Fcx;

    .line 2263427
    iget-object v1, p4, LX/7Hc;->b:LX/0Px;

    move-object v1, v1

    .line 2263428
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p0

    .line 2263429
    iget-boolean p1, v0, LX/Fcx;->c:Z

    if-eqz p1, :cond_2

    iget-object p1, v0, LX/Fcx;->f:Lcom/facebook/search/results/protocol/filters/FilterValue;

    if-eqz p1, :cond_2

    .line 2263430
    iget-object p1, v0, LX/Fcx;->f:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {p0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2263431
    :cond_2
    invoke-virtual {p0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object p0

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    iput-object p0, v0, LX/Fcx;->i:LX/0Px;

    .line 2263432
    const p0, -0x185be6c2

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2263433
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2263434
    return-void
.end method
