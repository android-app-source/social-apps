.class public final synthetic LX/H3s;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2421971
    invoke-static {}, LX/FOw;->values()[LX/FOw;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/H3s;->b:[I

    :try_start_0
    sget-object v0, LX/H3s;->b:[I

    sget-object v1, LX/FOw;->LOCATION_PERMISSION_OFF:LX/FOw;

    invoke-virtual {v1}, LX/FOw;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v0, LX/H3s;->b:[I

    sget-object v1, LX/FOw;->DEVICE_LOCATION_OFF:LX/FOw;

    invoke-virtual {v1}, LX/FOw;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, LX/H3s;->b:[I

    sget-object v1, LX/FOw;->DEVICE_NON_OPTIMAL_LOCATION_SETTING:LX/FOw;

    invoke-virtual {v1}, LX/FOw;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    .line 2421972
    :goto_2
    invoke-static {}, LX/H3t;->values()[LX/H3t;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/H3s;->a:[I

    :try_start_3
    sget-object v0, LX/H3s;->a:[I

    sget-object v1, LX/H3t;->HEADER_CATEGORY_PICKER:LX/H3t;

    invoke-virtual {v1}, LX/H3t;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, LX/H3s;->a:[I

    sget-object v1, LX/H3t;->CELL_CATEGORY_PICKER:LX/H3t;

    invoke-virtual {v1}, LX/H3t;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v0, LX/H3s;->a:[I

    sget-object v1, LX/H3t;->HEADER_NEAR_YOUR_LOCATION:LX/H3t;

    invoke-virtual {v1}, LX/H3t;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, LX/H3s;->a:[I

    sget-object v1, LX/H3t;->CELL_TURN_ON_LOCATION_SERVICES:LX/H3t;

    invoke-virtual {v1}, LX/H3t;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, LX/H3s;->a:[I

    sget-object v1, LX/H3t;->PLACES_HUGE_CELL:LX/H3t;

    invoke-virtual {v1}, LX/H3t;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, LX/H3s;->a:[I

    sget-object v1, LX/H3t;->LOADING_CELL:LX/H3t;

    invoke-virtual {v1}, LX/H3t;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    goto :goto_8

    :catch_1
    goto :goto_7

    :catch_2
    goto :goto_6

    :catch_3
    goto :goto_5

    :catch_4
    goto :goto_4

    :catch_5
    goto :goto_3

    :catch_6
    goto :goto_2

    :catch_7
    goto :goto_1

    :catch_8
    goto :goto_0
.end method
