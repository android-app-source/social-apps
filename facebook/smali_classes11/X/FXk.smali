.class public LX/FXk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/support/annotation/MainThread;
.end annotation


# instance fields
.field private final a:LX/AUC;

.field private final b:LX/FXs;

.field private final c:LX/FWt;

.field private final d:LX/0kL;

.field private final e:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "LX/FXj;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/AUC;LX/FXs;LX/FWt;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0kL;)V
    .locals 2

    .prologue
    .line 2255362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2255363
    new-instance v0, LX/01J;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/FXk;->e:LX/01J;

    .line 2255364
    iput-object p1, p0, LX/FXk;->a:LX/AUC;

    .line 2255365
    iput-object p2, p0, LX/FXk;->b:LX/FXs;

    .line 2255366
    iput-object p3, p0, LX/FXk;->c:LX/FWt;

    .line 2255367
    iput-object p4, p0, LX/FXk;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2255368
    iput-object p5, p0, LX/FXk;->d:LX/0kL;

    .line 2255369
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/FXj;
    .locals 7

    .prologue
    .line 2255370
    iget-object v0, p0, LX/FXk;->e:LX/01J;

    invoke-virtual {v0, p1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FXj;

    .line 2255371
    if-nez v0, :cond_0

    .line 2255372
    new-instance v0, LX/FXj;

    iget-object v1, p0, LX/FXk;->b:LX/FXs;

    iget-object v2, p0, LX/FXk;->a:LX/AUC;

    iget-object v3, p0, LX/FXk;->c:LX/FWt;

    iget-object v4, p0, LX/FXk;->d:LX/0kL;

    iget-object v5, p0, LX/FXk;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, LX/FXj;-><init>(LX/FXs;LX/AUC;LX/FWt;LX/0kL;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;)V

    .line 2255373
    iget-object v1, p0, LX/FXk;->e:LX/01J;

    invoke-virtual {v1, p1, v0}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2255374
    :cond_0
    return-object v0
.end method
