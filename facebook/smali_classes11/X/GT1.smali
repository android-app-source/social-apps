.class public final LX/GT1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GRZ;

.field public final synthetic b:LX/GTB;


# direct methods
.method public constructor <init>(LX/GTB;LX/GRZ;)V
    .locals 0

    .prologue
    .line 2355116
    iput-object p1, p0, LX/GT1;->b:LX/GTB;

    iput-object p2, p0, LX/GT1;->a:LX/GRZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x39bfab3

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2355117
    iget-object v1, p0, LX/GT1;->b:LX/GTB;

    iget-object v1, v1, LX/GTB;->h:LX/GRv;

    const-string v2, "app_invite_chevron_button_did_tapped"

    iget-object v3, p0, LX/GT1;->a:LX/GRZ;

    iget-object v4, p0, LX/GT1;->a:LX/GRZ;

    .line 2355118
    iget v6, v4, LX/GRZ;->a:I

    move v4, v6

    .line 2355119
    invoke-virtual {v3, v4}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/GRv;->a(Ljava/lang/String;Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;)V

    .line 2355120
    iget-object v1, p0, LX/GT1;->b:LX/GTB;

    iget-object v2, p0, LX/GT1;->a:LX/GRZ;

    iget-object v3, p0, LX/GT1;->a:LX/GRZ;

    .line 2355121
    iget v4, v3, LX/GRZ;->a:I

    move v3, v4

    .line 2355122
    invoke-virtual {v2, v3}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v2

    const/4 p0, 0x1

    const/4 v11, 0x0

    .line 2355123
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2355124
    new-instance v4, LX/6WS;

    invoke-direct {v4, v3}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2355125
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    const v7, 0x7f083773

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, p0, v11, v7}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    .line 2355126
    const v7, 0x7f020818

    invoke-virtual {v6, v7}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2355127
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    const/4 v7, 0x2

    const v8, 0x7f083777

    new-array v9, p0, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;->c()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v3, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v11, v8}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    .line 2355128
    const v7, 0x7f02089a

    invoke-virtual {v6, v7}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2355129
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    const/4 v7, 0x3

    const v8, 0x7f08377b

    new-array v9, p0, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->mx_()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v3, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v11, v8}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    .line 2355130
    const v7, 0x7f02092a

    invoke-virtual {v6, v7}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2355131
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    const/4 v7, 0x4

    const v8, 0x7f08377f

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v11, v8}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    .line 2355132
    const v7, 0x7f0208cf

    invoke-virtual {v6, v7}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2355133
    new-instance v6, LX/GT8;

    invoke-direct {v6, v1, v3, v2}, LX/GT8;-><init>(LX/GTB;Landroid/content/Context;Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;)V

    .line 2355134
    iput-object v6, v4, LX/5OM;->p:LX/5OO;

    .line 2355135
    invoke-virtual {v4, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2355136
    const v1, -0x4421a632

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
