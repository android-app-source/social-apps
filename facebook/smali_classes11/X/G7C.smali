.class public LX/G7C;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final i:Ljava/lang/Object;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0SG;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/121;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/G7G;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/G7H;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2321131
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/G7C;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0SG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/121;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/G7G;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/G7H;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2321121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321122
    iput-object p1, p0, LX/G7C;->a:Landroid/content/Context;

    .line 2321123
    iput-object p2, p0, LX/G7C;->b:LX/0SG;

    .line 2321124
    iput-object p3, p0, LX/G7C;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2321125
    iput-object p4, p0, LX/G7C;->d:LX/0Ot;

    .line 2321126
    iput-object p5, p0, LX/G7C;->e:LX/0Ot;

    .line 2321127
    iput-object p6, p0, LX/G7C;->f:LX/0Ot;

    .line 2321128
    iput-object p7, p0, LX/G7C;->g:LX/0Ot;

    .line 2321129
    iput-object p8, p0, LX/G7C;->h:LX/0Or;

    .line 2321130
    return-void
.end method

.method public static a(LX/0yY;)I
    .locals 3

    .prologue
    .line 2321106
    sget-object v0, LX/G7A;->a:[I

    invoke-virtual {p0}, LX/0yY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2321107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported zero feature: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/0yY;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321108
    :pswitch_0
    const v0, 0x7f080e4e

    .line 2321109
    :goto_0
    return v0

    .line 2321110
    :pswitch_1
    const v0, 0x7f080e4f

    goto :goto_0

    .line 2321111
    :pswitch_2
    const v0, 0x7f080e4d

    goto :goto_0

    .line 2321112
    :pswitch_3
    const v0, 0x7f080e4c

    goto :goto_0

    .line 2321113
    :pswitch_4
    const v0, 0x7f080e4b

    goto :goto_0

    .line 2321114
    :pswitch_5
    const v0, 0x7f080e50

    goto :goto_0

    .line 2321115
    :pswitch_6
    const v0, 0x7f080e48

    goto :goto_0

    .line 2321116
    :pswitch_7
    const v0, 0x7f080e49

    goto :goto_0

    .line 2321117
    :pswitch_8
    const v0, 0x7f080e41

    goto :goto_0

    .line 2321118
    :pswitch_9
    const v0, 0x7f080e4a

    goto :goto_0

    .line 2321119
    :pswitch_a
    const v0, 0x7f080e4a

    goto :goto_0

    .line 2321120
    :pswitch_b
    const v0, 0x7f080e49

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static a(LX/G7C;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0yY;)LX/G7B;
    .locals 11
    .param p0    # LX/G7C;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2321075
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2321076
    sget-object v0, LX/0yY;->SEND_MESSAGE_INTERSTITIAL:LX/0yY;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/0yY;->SEND_STICKER_INTERSTITIAL:LX/0yY;

    if-ne p2, v0, :cond_1

    .line 2321077
    :cond_0
    sget-object v0, LX/G7B;->DIRECT_CALL:LX/G7B;

    .line 2321078
    :goto_0
    return-object v0

    .line 2321079
    :cond_1
    sget-object v0, LX/G7B;->STANDARD_ZERO_DIALOG:LX/G7B;

    goto :goto_0

    .line 2321080
    :cond_2
    sget-object v0, LX/0yY;->ATTACHMENT_DOWNLOAD_INTERSTITIAL:LX/0yY;

    if-eq p2, v0, :cond_3

    sget-object v0, LX/0yY;->ATTACHMENT_DOWNLOAD_MMS_INTERSTITIAL:LX/0yY;

    if-eq p2, v0, :cond_3

    sget-object v0, LX/0yY;->ATTACHMENT_UPLOAD_INTERSTITIAL:LX/0yY;

    if-eq p2, v0, :cond_3

    sget-object v0, LX/0yY;->IMAGE_SEARCH_INTERSTITIAL:LX/0yY;

    if-eq p2, v0, :cond_3

    sget-object v0, LX/0yY;->VIEW_TIMELINE_INTERSTITIAL:LX/0yY;

    if-ne p2, v0, :cond_4

    .line 2321081
    :cond_3
    sget-object v0, LX/G7B;->STANDARD_ZERO_DIALOG:LX/G7B;

    goto :goto_0

    .line 2321082
    :cond_4
    const/4 v3, 0x0

    .line 2321083
    iget-object v1, p0, LX/G7C;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_7

    move v1, v3

    .line 2321084
    :goto_1
    move v0, v1

    .line 2321085
    if-eqz v0, :cond_5

    sget-object v0, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    if-eq p2, v0, :cond_5

    sget-object v0, LX/0yY;->VIDEO_SAVE_INTERSTITIAL:LX/0yY;

    if-eq p2, v0, :cond_5

    sget-object v0, LX/0yY;->AUDIO_PLAY_INTERSTITIAL:LX/0yY;

    if-ne p2, v0, :cond_6

    .line 2321086
    :cond_5
    sget-object v0, LX/G7B;->DIRECT_CALL:LX/G7B;

    goto :goto_0

    .line 2321087
    :cond_6
    sget-object v0, LX/G7B;->SMS_ZERO_DIALOG:LX/G7B;

    goto :goto_0

    .line 2321088
    :cond_7
    iget-object v1, p0, LX/G7C;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G7H;

    .line 2321089
    iget-object v2, p0, LX/G7C;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0yH;

    sget-object v4, LX/0yY;->SMS_THREAD_INTERSTITIAL:LX/0yY;

    invoke-virtual {v2, v4}, LX/0yH;->a(LX/0yY;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 2321090
    const-string v2, "skip_dont_warn_again"

    invoke-virtual {v1, v2, p2, p1}, LX/G7H;->a(Ljava/lang/String;LX/0yY;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    move v1, v3

    .line 2321091
    goto :goto_1

    .line 2321092
    :cond_8
    iget-object v2, p0, LX/G7C;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/G7D;->c:LX/0Tn;

    invoke-interface {v2, v4, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 2321093
    const/16 v4, 0xa

    if-le v2, v4, :cond_9

    .line 2321094
    const-string v2, "skip_exceed_total_impression"

    invoke-virtual {v1, v2, p2, p1}, LX/G7H;->a(Ljava/lang/String;LX/0yY;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    move v1, v3

    .line 2321095
    goto :goto_1

    .line 2321096
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2321097
    sget-object v5, LX/G7D;->b:LX/0Tn;

    invoke-virtual {v5, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v4

    check-cast v4, LX/0Tn;

    move-object v4, v4

    .line 2321098
    iget-object v5, p0, LX/G7C;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-wide/16 v7, -0x1

    invoke-interface {v5, v4, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v5

    .line 2321099
    iget-object v7, p0, LX/G7C;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    .line 2321100
    sub-long v5, v7, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->abs(J)J

    move-result-wide v5

    const-wide/32 v9, 0x5265c00

    cmp-long v5, v5, v9

    if-lez v5, :cond_a

    .line 2321101
    iget-object v3, p0, LX/G7C;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v5, LX/G7D;->c:LX/0Tn;

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v3, v5, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2, v4, v7, v8}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2321102
    const-string v2, "show_dialog"

    invoke-virtual {v1, v2, p2, p1}, LX/G7H;->a(Ljava/lang/String;LX/0yY;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2321103
    const/4 v1, 0x1

    goto/16 :goto_1

    .line 2321104
    :cond_a
    const-string v2, "skip_exceed_daily_impression"

    invoke-virtual {v1, v2, p2, p1}, LX/G7H;->a(Ljava/lang/String;LX/0yY;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    move v1, v3

    .line 2321105
    goto/16 :goto_1
.end method

.method public static a(LX/0QB;)LX/G7C;
    .locals 7

    .prologue
    .line 2321048
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2321049
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2321050
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2321051
    if-nez v1, :cond_0

    .line 2321052
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321053
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2321054
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2321055
    sget-object v1, LX/G7C;->i:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2321056
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2321057
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2321058
    :cond_1
    if-nez v1, :cond_4

    .line 2321059
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2321060
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2321061
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/G7C;->b(LX/0QB;)LX/G7C;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2321062
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2321063
    if-nez v1, :cond_2

    .line 2321064
    sget-object v0, LX/G7C;->i:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G7C;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2321065
    :goto_1
    if-eqz v0, :cond_3

    .line 2321066
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2321067
    :goto_3
    check-cast v0, LX/G7C;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2321068
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2321069
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2321070
    :catchall_1
    move-exception v0

    .line 2321071
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2321072
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2321073
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2321074
    :cond_2
    :try_start_8
    sget-object v0, LX/G7C;->i:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G7C;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static b(LX/0yY;)LX/0yY;
    .locals 1

    .prologue
    .line 2321039
    sget-object v0, LX/0yY;->ATTACHMENT_DOWNLOAD_MMS_INTERSTITIAL:LX/0yY;

    if-ne p0, v0, :cond_1

    .line 2321040
    sget-object p0, LX/0yY;->ATTACHMENT_DOWNLOAD_INTERSTITIAL:LX/0yY;

    .line 2321041
    :cond_0
    :goto_0
    return-object p0

    .line 2321042
    :cond_1
    sget-object v0, LX/0yY;->VIDEO_SAVE_INTERSTITIAL:LX/0yY;

    if-ne p0, v0, :cond_2

    .line 2321043
    sget-object p0, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    goto :goto_0

    .line 2321044
    :cond_2
    sget-object v0, LX/0yY;->SEND_MEDIA_FILE_INTERSTITIAL:LX/0yY;

    if-ne p0, v0, :cond_0

    .line 2321045
    sget-object p0, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/G7C;
    .locals 9

    .prologue
    .line 2321046
    new-instance v0, LX/G7C;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v4, 0xbd8

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3909

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x13ba

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x390a

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x14d1

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/G7C;-><init>(Landroid/content/Context;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 2321047
    return-object v0
.end method
