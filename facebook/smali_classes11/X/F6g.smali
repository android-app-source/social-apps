.class public LX/F6g;
.super LX/F6e;
.source ""


# instance fields
.field private s:LX/F6f;

.field private t:Landroid/text/Spanned;

.field private u:Z

.field private v:Z


# direct methods
.method public constructor <init>(LX/9Tk;LX/F6f;LX/0Tf;LX/0W9;Landroid/content/Context;LX/89v;Ljava/util/Map;Landroid/text/Spanned;JLX/F6i;LX/F6k;)V
    .locals 15
    .param p3    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/89v;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/util/Map;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Landroid/text/Spanned;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/F6i;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/F6k;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9Tk;",
            "LX/F6f;",
            "LX/0Tf;",
            "LX/0W9;",
            "Landroid/content/Context;",
            "LX/89v;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;",
            "Landroid/text/Spanned;",
            "J",
            "LX/F6i;",
            "LX/F6k;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200753
    move-object v3, p0

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p11

    move-object/from16 v7, p12

    move-object/from16 v8, p1

    move-object/from16 v9, p7

    move-wide/from16 v10, p9

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    invoke-direct/range {v3 .. v13}, LX/F6e;-><init>(Landroid/content/Context;LX/89v;LX/F6i;LX/F6k;LX/9Tk;Ljava/util/Map;JLX/0Tf;LX/0W9;)V

    .line 2200754
    move-object/from16 v0, p2

    iput-object v0, p0, LX/F6g;->s:LX/F6f;

    .line 2200755
    move-object/from16 v0, p8

    iput-object v0, p0, LX/F6g;->t:Landroid/text/Spanned;

    .line 2200756
    iget-object v2, p0, LX/F6g;->s:LX/F6f;

    invoke-virtual {v2}, LX/F6f;->b()Z

    move-result v2

    iput-boolean v2, p0, LX/F6g;->u:Z

    .line 2200757
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/model/FacebookPhonebookContact;)J
    .locals 2

    .prologue
    .line 2200758
    iget-wide v0, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->recordId:J

    return-wide v0
.end method

.method public final bridge synthetic a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2200759
    invoke-super/range {p0 .. p5}, LX/F6e;->a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2200760
    iget-boolean v0, p0, LX/F6g;->u:Z

    if-nez v0, :cond_0

    .line 2200761
    invoke-super {p0, p1, p2, p3}, LX/F6e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2200762
    :goto_0
    return-object p2

    .line 2200763
    :cond_0
    if-nez p1, :cond_4

    .line 2200764
    iget-boolean v0, p0, LX/F6g;->v:Z

    if-nez v0, :cond_1

    .line 2200765
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/F6g;->v:Z

    .line 2200766
    iget-object v0, p0, LX/F6g;->s:LX/F6f;

    invoke-virtual {v0}, LX/F6f;->a()V

    .line 2200767
    :cond_1
    if-eqz p2, :cond_2

    instance-of v0, p2, Landroid/widget/LinearLayout;

    if-nez v0, :cond_3

    .line 2200768
    :cond_2
    iget-object v0, p0, LX/F6e;->l:Landroid/view/LayoutInflater;

    const v1, 0x7f030669

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2200769
    :cond_3
    const v0, 0x7f0d11a1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2200770
    iget-object v1, p0, LX/F6g;->t:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2200771
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2200772
    const v0, 0x7f0d1458

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, LX/F6e;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/F6b;

    invoke-virtual {v1}, LX/F6b;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2200773
    :cond_4
    if-eqz p2, :cond_5

    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 2200774
    :cond_5
    invoke-super {p0, p1, p2, p3}, LX/F6e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 2200775
    :cond_6
    invoke-super {p0, p1, v1, v1}, LX/F6e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2200776
    const v0, 0x7f0d1602

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2200777
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2200778
    iget-object v0, p0, LX/F6e;->e:Landroid/content/Context;

    const v1, 0x7f08339f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2200779
    iget-object v0, p0, LX/F6e;->e:Landroid/content/Context;

    const v1, 0x7f0833a1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
