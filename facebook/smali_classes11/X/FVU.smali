.class public LX/FVU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/FW5;

.field private b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/16H;

.field public final d:LX/FWL;


# direct methods
.method public constructor <init>(LX/FW5;LX/16H;LX/FWL;)V
    .locals 1
    .param p1    # LX/FW5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250810
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/FVU;->b:LX/0am;

    .line 2250811
    iput-object p1, p0, LX/FVU;->a:LX/FW5;

    .line 2250812
    iput-object p2, p0, LX/FVU;->c:LX/16H;

    .line 2250813
    iput-object p3, p0, LX/FVU;->d:LX/FWL;

    .line 2250814
    return-void
.end method

.method public static a(LX/FVU;LX/0am;LX/FW7;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;",
            "LX/FW7;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2250815
    iget-object v0, p0, LX/FVU;->a:LX/FW5;

    .line 2250816
    invoke-static {v0, p2}, LX/FW5;->c(LX/FW5;LX/FW7;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v1

    .line 2250817
    if-nez v1, :cond_0

    .line 2250818
    new-instance v2, Lcom/facebook/saved/fragment/SavedItemsListFragment;

    invoke-direct {v2}, Lcom/facebook/saved/fragment/SavedItemsListFragment;-><init>()V

    move-object v1, v2

    .line 2250819
    check-cast v1, Lcom/facebook/base/fragment/FbFragment;

    .line 2250820
    iget-object v2, v0, LX/FW5;->a:LX/0gc;

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    iget v3, v0, LX/FW5;->b:I

    invoke-virtual {p2}, LX/FW7;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v2

    invoke-virtual {v2}, LX/0hH;->c()I

    .line 2250821
    iget-object v2, v0, LX/FW5;->a:LX/0gc;

    invoke-virtual {v2}, LX/0gc;->b()Z

    .line 2250822
    move-object v1, v1

    .line 2250823
    :cond_0
    move-object v1, v1

    .line 2250824
    invoke-virtual {v0}, LX/FW5;->a()Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2250825
    if-ne v1, v2, :cond_2

    .line 2250826
    :goto_0
    iget-object v0, p0, LX/FVU;->b:LX/0am;

    invoke-virtual {v0, p1}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2250827
    iget-object v0, p0, LX/FVU;->a:LX/FW5;

    invoke-virtual {v0}, LX/FW5;->a()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2250828
    instance-of v1, v0, LX/FVT;

    if-eqz v1, :cond_1

    .line 2250829
    check-cast v0, LX/FVT;

    invoke-interface {v0, p1}, LX/FVT;->a(LX/0am;)V

    .line 2250830
    :cond_1
    iput-object p1, p0, LX/FVU;->b:LX/0am;

    .line 2250831
    return-void

    .line 2250832
    :cond_2
    iget-object v3, v0, LX/FW5;->a:LX/0gc;

    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0hH;->c(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    .line 2250833
    if-eqz v2, :cond_3

    .line 2250834
    invoke-virtual {v1, v2}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2250835
    :cond_3
    invoke-virtual {v1}, LX/0hH;->c()I

    .line 2250836
    iget-object v1, v0, LX/FW5;->a:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->b()Z

    .line 2250837
    iput-object p2, v0, LX/FW5;->c:LX/FW7;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;
    .locals 2

    .prologue
    .line 2250838
    iget-object v0, p0, LX/FVU;->b:LX/0am;

    invoke-static {v0}, LX/FWL;->a(LX/0am;)LX/0am;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    return-object v0
.end method

.method public final a(LX/79Y;)V
    .locals 14

    .prologue
    .line 2250839
    iget-object v0, p0, LX/FVU;->c:LX/16H;

    invoke-virtual {p0}, LX/FVU;->a()Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v1

    .line 2250840
    iget-object v2, p1, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v2, v2

    .line 2250841
    const/4 v13, 0x0

    .line 2250842
    iget-object v11, v0, LX/16H;->b:LX/0gh;

    const-string v12, "saved_dashboard"

    const-string v3, "action_name"

    const-string v4, "saved_dashboard_section_list_item_clicked"

    const-string v5, "current_section_type"

    const-string v7, "next_section_type"

    const-string v9, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    move-object v6, v1

    move-object v8, v2

    invoke-static/range {v3 .. v10}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    invoke-virtual {v11, v12, v13, v13, v3}, LX/0gh;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2250843
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    sget-object v1, LX/FW7;->SAVED_ITEMS_LIST:LX/FW7;

    invoke-static {p0, v0, v1}, LX/FVU;->a(LX/FVU;LX/0am;LX/FW7;)V

    .line 2250844
    return-void
.end method
