.class public final LX/FHm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public final synthetic a:LX/FHn;

.field private b:LX/FHl;

.field private c:Ljava/lang/String;

.field private d:LX/FHr;

.field private e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FHr;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/4d1;


# direct methods
.method public constructor <init>(LX/FHn;LX/FHl;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2220939
    iput-object p1, p0, LX/FHm;->a:LX/FHn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220940
    iput-object v0, p0, LX/FHm;->c:Ljava/lang/String;

    .line 2220941
    iput-object v0, p0, LX/FHm;->d:LX/FHr;

    .line 2220942
    iput-object p2, p0, LX/FHm;->b:LX/FHl;

    .line 2220943
    new-instance v0, LX/4d1;

    invoke-direct {v0}, LX/4d1;-><init>()V

    iput-object v0, p0, LX/FHm;->f:LX/4d1;

    .line 2220944
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2220980
    iget-object v0, p0, LX/FHm;->d:LX/FHr;

    iget-object v0, v0, LX/FHr;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/14U;)Z
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 2220965
    iget-object v1, p0, LX/FHm;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FHm;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2220966
    :goto_0
    return v0

    .line 2220967
    :cond_0
    iget-object v1, p0, LX/FHm;->c:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 2220968
    iget-object v1, p0, LX/FHm;->b:LX/FHl;

    iget-object v1, v1, LX/FHl;->b:Ljava/lang/String;

    iput-object v1, p0, LX/FHm;->c:Ljava/lang/String;

    .line 2220969
    :cond_1
    new-instance v1, LX/FHk;

    iget-object v2, p0, LX/FHm;->a:LX/FHn;

    iget-object v3, p0, LX/FHm;->b:LX/FHl;

    iget-object v3, v3, LX/FHl;->c:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    iget-object v3, p0, LX/FHm;->b:LX/FHl;

    iget-object v3, v3, LX/FHl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {v1, v2, v4, v5, v3}, LX/FHk;-><init>(LX/FHn;JLcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2220970
    iput-object v1, p1, LX/14U;->a:LX/4ck;

    .line 2220971
    iget-object v1, p0, LX/FHm;->f:LX/4d1;

    .line 2220972
    iput-object v1, p1, LX/14U;->c:LX/4d1;

    .line 2220973
    iget-object v1, p0, LX/FHm;->b:LX/FHl;

    iget-object v1, v1, LX/FHl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->AUDIO:LX/2MK;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/FHm;->b:LX/FHl;

    iget-object v1, v1, LX/FHl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->PHOTO:LX/2MK;

    if-ne v1, v2, :cond_3

    .line 2220974
    :cond_2
    new-instance v9, LX/7ys;

    iget-object v1, p0, LX/FHm;->a:LX/FHn;

    iget-object v1, v1, LX/FHn;->f:LX/0ad;

    sget-short v2, LX/FGS;->c:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    iget-object v1, p0, LX/FHm;->a:LX/FHn;

    iget-object v1, v1, LX/FHn;->f:LX/0ad;

    sget v2, LX/FGS;->a:I

    const/16 v3, 0x400

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    iget-object v2, p0, LX/FHm;->a:LX/FHn;

    iget-object v2, v2, LX/FHn;->f:LX/0ad;

    sget-char v3, LX/FGS;->b:C

    const-string v4, "SHA256"

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v0, v1, v2}, LX/7ys;-><init>(ZILjava/lang/String;)V

    .line 2220975
    :goto_1
    iget-object v0, p0, LX/FHm;->a:LX/FHn;

    iget-object v10, v0, LX/FHn;->d:LX/FHe;

    new-instance v0, LX/FHc;

    iget-object v1, p0, LX/FHm;->c:Ljava/lang/String;

    iget-object v2, p0, LX/FHm;->b:LX/FHl;

    iget-object v3, v2, LX/FHl;->c:Ljava/io/File;

    iget-object v2, p0, LX/FHm;->b:LX/FHl;

    iget-object v4, v2, LX/FHl;->d:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, p0, LX/FHm;->b:LX/FHl;

    iget-object v5, v2, LX/FHl;->e:Ljava/lang/String;

    iget-object v2, p0, LX/FHm;->b:LX/FHl;

    iget-object v6, v2, LX/FHl;->f:Ljava/lang/String;

    iget-object v2, p0, LX/FHm;->b:LX/FHl;

    iget-object v7, v2, LX/FHl;->g:LX/FHg;

    iget-object v2, p0, LX/FHm;->b:LX/FHl;

    iget-object v8, v2, LX/FHl;->h:LX/FHj;

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, LX/FHc;-><init>(Ljava/lang/String;LX/14U;Ljava/io/File;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/FHg;LX/FHj;LX/7ys;)V

    .line 2220976
    new-instance v1, LX/FHd;

    invoke-direct {v1, v10, v0}, LX/FHd;-><init>(LX/FHe;LX/FHc;)V

    move-object v0, v1

    .line 2220977
    iget-object v1, p0, LX/FHm;->a:LX/FHn;

    iget-object v1, v1, LX/FHn;->b:LX/0TD;

    invoke-interface {v1, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/FHm;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2220978
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2220979
    :cond_3
    new-instance v9, LX/7ys;

    invoke-direct {v9}, LX/7ys;-><init>()V

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2220981
    iget-object v0, p0, LX/FHm;->d:LX/FHr;

    iget-object v0, v0, LX/FHr;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2220964
    iget-object v0, p0, LX/FHm;->d:LX/FHr;

    iget-object v0, v0, LX/FHr;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2220961
    iget-object v0, p0, LX/FHm;->d:LX/FHr;

    .line 2220962
    iget-object p0, v0, LX/FHr;->b:Ljava/lang/String;

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2220963
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 5

    .prologue
    .line 2220945
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/FHm;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const-wide/16 v2, 0x64

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v4, -0x1b959878

    invoke-static {v0, v2, v3, v1, v4}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHr;

    iput-object v0, p0, LX/FHm;->d:LX/FHr;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2220946
    const/4 v0, 0x1

    .line 2220947
    :goto_0
    return v0

    .line 2220948
    :catch_0
    iget-object v0, p0, LX/FHm;->a:LX/FHn;

    iget-object v0, v0, LX/FHn;->c:LX/6ef;

    iget-object v1, p0, LX/FHm;->b:LX/FHl;

    iget-object v1, v1, LX/FHl;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/6ef;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2220949
    iget-object v0, p0, LX/FHm;->b:LX/FHl;

    iget-object v0, v0, LX/FHl;->h:LX/FHj;

    if-eqz v0, :cond_1

    .line 2220950
    iget-object v0, p0, LX/FHm;->b:LX/FHl;

    iget-object v0, v0, LX/FHl;->h:LX/FHj;

    sget-object v1, LX/FHi;->USER_CANCELLED:LX/FHi;

    invoke-virtual {v0, v1}, LX/FHj;->a(LX/FHi;)V

    .line 2220951
    :cond_1
    iget-object v0, p0, LX/FHm;->f:LX/4d1;

    invoke-virtual {v0}, LX/4d1;->a()Z

    .line 2220952
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0

    .line 2220953
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 2220954
    iget-object v0, p0, LX/FHm;->b:LX/FHl;

    iget-object v0, v0, LX/FHl;->h:LX/FHj;

    if-eqz v0, :cond_2

    .line 2220955
    iget-object v0, p0, LX/FHm;->b:LX/FHl;

    iget-object v2, v0, LX/FHl;->h:LX/FHj;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v3, v0}, LX/FHj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2220956
    :cond_2
    invoke-static {v1}, LX/FGm;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2220957
    throw v1

    .line 2220958
    :cond_3
    const-string v0, ""

    goto :goto_1

    .line 2220959
    :cond_4
    iget-object v0, p0, LX/FHm;->a:LX/FHn;

    iget-object v0, v0, LX/FHn;->e:LX/2Mo;

    invoke-virtual {v0}, LX/2Mo;->a()V

    .line 2220960
    const/4 v0, 0x0

    goto :goto_0
.end method
