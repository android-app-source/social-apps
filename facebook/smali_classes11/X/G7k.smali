.class public final LX/G7k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/G7V;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2wX;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/G7i;

    invoke-direct {v0, p0, p1}, LX/G7i;-><init>(LX/G7k;LX/2wX;)V

    invoke-virtual {p1, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2wX;Lcom/google/android/gms/auth/api/credentials/Credential;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Lcom/google/android/gms/auth/api/credentials/Credential;",
            ")",
            "LX/2wg",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/G7h;

    invoke-direct {v0, p0, p1, p2}, LX/G7h;-><init>(LX/G7k;LX/2wX;Lcom/google/android/gms/auth/api/credentials/Credential;)V

    invoke-virtual {p1, v0}, LX/2wX;->b(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2wX;Lcom/google/android/gms/auth/api/credentials/CredentialRequest;)LX/2wg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2wX;",
            "Lcom/google/android/gms/auth/api/credentials/CredentialRequest;",
            ")",
            "LX/2wg",
            "<",
            "LX/G7U;",
            ">;"
        }
    .end annotation

    new-instance v0, LX/G7g;

    invoke-direct {v0, p0, p1, p2}, LX/G7g;-><init>(LX/G7k;LX/2wX;Lcom/google/android/gms/auth/api/credentials/CredentialRequest;)V

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2we;)LX/2we;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/2wX;Lcom/google/android/gms/auth/api/credentials/HintRequest;)Landroid/app/PendingIntent;
    .locals 4

    const-string v0, "client must not be null"

    invoke-static {p1, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "request must not be null"

    invoke-static {p2, v0}, LX/1ol;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, LX/G7R;->e:LX/2vs;

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2vs;)Z

    move-result v0

    const-string v1, "Auth.CREDENTIALS_API must be added to GoogleApiClient to use this API"

    invoke-static {v0, v1}, LX/1ol;->b(ZLjava/lang/Object;)V

    invoke-virtual {p1}, LX/2wX;->b()Landroid/content/Context;

    sget-object v0, LX/G7R;->a:LX/2vn;

    invoke-virtual {p1, v0}, LX/2wX;->a(LX/2vo;)LX/2wJ;

    move-result-object v0

    check-cast v0, LX/G7l;

    iget-object v1, v0, LX/G7l;->d:LX/G7Q;

    move-object v0, v1

    if-eqz v0, :cond_0

    goto :goto_1

    :goto_0
    move-object v0, v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.auth.api.credentials.PICKER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.credentials.RequestType"

    const-string v3, "Hints"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.credentials.HintRequest"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.credentials.PasswordSpecification"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    invoke-virtual {p1}, LX/2wX;->b()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x7d0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    :cond_0
    :goto_1
    sget-object v0, Lcom/google/android/gms/auth/api/credentials/PasswordSpecification;->a:Lcom/google/android/gms/auth/api/credentials/PasswordSpecification;

    goto :goto_0
.end method
