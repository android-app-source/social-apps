.class public LX/H8a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2lA;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/content/Context;

.field public i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433540
    const v0, 0x7f020772

    sput v0, LX/H8a;->a:I

    .line 2433541
    const v0, 0x7f08165d

    sput v0, LX/H8a;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 0
    .param p6    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2lA;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433532
    iput-object p1, p0, LX/H8a;->c:LX/0Ot;

    .line 2433533
    iput-object p2, p0, LX/H8a;->d:LX/0Ot;

    .line 2433534
    iput-object p3, p0, LX/H8a;->e:LX/0Ot;

    .line 2433535
    iput-object p4, p0, LX/H8a;->f:LX/0Ot;

    .line 2433536
    iput-object p5, p0, LX/H8a;->g:LX/0Ot;

    .line 2433537
    iput-object p7, p0, LX/H8a;->h:Landroid/content/Context;

    .line 2433538
    iput-object p6, p0, LX/H8a;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433539
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 7

    .prologue
    .line 2433507
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8a;->b:I

    sget v3, LX/H8a;->a:I

    const/4 v4, 0x1

    .line 2433508
    iget-object v5, p0, LX/H8a;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    .line 2433509
    new-instance v6, LX/8A4;

    invoke-direct {v6, v5}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object p0, LX/8A3;->CREATE_ADS:LX/8A3;

    invoke-virtual {v6, p0}, LX/8A4;->a(LX/8A3;)Z

    move-result v6

    move v5, v6

    .line 2433510
    move v5, v5

    .line 2433511
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433530
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8a;->b:I

    sget v3, LX/H8a;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2433513
    iget-object v0, p0, LX/H8a;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XI;->EVENT_TAPPED_MANAGE_ADS:LX/9XI;

    iget-object v2, p0, LX/H8a;->i:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433514
    const/4 v1, 0x0

    .line 2433515
    iget-object v0, p0, LX/H8a;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2lA;

    const/4 v2, 0x0

    .line 2433516
    invoke-static {v0}, LX/2lA;->g(LX/2lA;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2433517
    :goto_0
    move v0, v2

    .line 2433518
    if-eqz v0, :cond_0

    .line 2433519
    iget-object v0, p0, LX/H8a;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2433520
    const-string v0, "fb-ama://?ref=fb_ma_android"

    move-object v0, v0

    .line 2433521
    invoke-static {v0}, LX/2lA;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2433522
    :cond_0
    if-nez v1, :cond_1

    .line 2433523
    iget-object v0, p0, LX/H8a;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "admin_page_manage_ads"

    const-string v3, "Failed to resolve ads manager intent!"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2433524
    :cond_1
    if-nez v1, :cond_2

    .line 2433525
    iget-object v0, p0, LX/H8a;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v1, p0, LX/H8a;->h:Landroid/content/Context;

    sget-object v2, LX/0ax;->m:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 2433526
    :cond_2
    if-nez v1, :cond_3

    .line 2433527
    iget-object v0, p0, LX/H8a;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "admin_page_manage_ads"

    const-string v2, "Failed to resolve ads manager URI!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2433528
    :goto_1
    return-void

    .line 2433529
    :cond_3
    iget-object v0, p0, LX/H8a;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/H8a;->h:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    :cond_4
    iget-object v3, v0, LX/2lA;->a:LX/0ad;

    sget-short v4, LX/ADP;->c:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v2

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433512
    const/4 v0, 0x0

    return-object v0
.end method
