.class public LX/Gg3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/Gfn;

.field public final b:LX/3mL;

.field public final c:LX/1LV;

.field public final d:LX/Ge4;

.field public final e:LX/2xr;


# direct methods
.method public constructor <init>(LX/Gfn;LX/3mL;LX/1LV;LX/Ge4;LX/2xr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378437
    iput-object p1, p0, LX/Gg3;->a:LX/Gfn;

    .line 2378438
    iput-object p2, p0, LX/Gg3;->b:LX/3mL;

    .line 2378439
    iput-object p3, p0, LX/Gg3;->c:LX/1LV;

    .line 2378440
    iput-object p4, p0, LX/Gg3;->d:LX/Ge4;

    .line 2378441
    iput-object p5, p0, LX/Gg3;->e:LX/2xr;

    .line 2378442
    return-void
.end method

.method public static a(LX/0QB;)LX/Gg3;
    .locals 9

    .prologue
    .line 2378443
    const-class v1, LX/Gg3;

    monitor-enter v1

    .line 2378444
    :try_start_0
    sget-object v0, LX/Gg3;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378445
    sput-object v2, LX/Gg3;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378446
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378447
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378448
    new-instance v3, LX/Gg3;

    const-class v4, LX/Gfn;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Gfn;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v5

    check-cast v5, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v6

    check-cast v6, LX/1LV;

    invoke-static {v0}, LX/Ge4;->a(LX/0QB;)LX/Ge4;

    move-result-object v7

    check-cast v7, LX/Ge4;

    invoke-static {v0}, LX/2xr;->b(LX/0QB;)LX/2xr;

    move-result-object v8

    check-cast v8, LX/2xr;

    invoke-direct/range {v3 .. v8}, LX/Gg3;-><init>(LX/Gfn;LX/3mL;LX/1LV;LX/Ge4;LX/2xr;)V

    .line 2378449
    move-object v0, v3

    .line 2378450
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378451
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gg3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378452
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378453
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
