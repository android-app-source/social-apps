.class public LX/FaZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FaB;


# instance fields
.field public final a:LX/CwT;

.field private final b:LX/13B;

.field private final c:LX/FaY;

.field public final d:LX/0ad;

.field private e:Z


# direct methods
.method public constructor <init>(LX/CwT;LX/13B;LX/FaY;LX/0ad;)V
    .locals 1
    .param p1    # LX/CwT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2258918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258919
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2258920
    iput-object p1, p0, LX/FaZ;->a:LX/CwT;

    .line 2258921
    iput-object p2, p0, LX/FaZ;->b:LX/13B;

    .line 2258922
    iput-object p3, p0, LX/FaZ;->c:LX/FaY;

    .line 2258923
    iput-object p4, p0, LX/FaZ;->d:LX/0ad;

    .line 2258924
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FaZ;->e:Z

    .line 2258925
    return-void
.end method


# virtual methods
.method public final a(LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2258926
    iget-object v0, p0, LX/FaZ;->b:LX/13B;

    iget-object v1, p0, LX/FaZ;->a:LX/CwT;

    invoke-virtual {v0, v1, p1}, LX/13B;->a(LX/CwT;LX/0P1;)V

    .line 2258927
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 2258928
    iget-object v0, p0, LX/FaZ;->c:LX/FaY;

    sget-object v1, LX/FaF;->LEARNING_NUX:LX/FaF;

    invoke-virtual {v0, v1}, LX/FaY;->a(LX/FaF;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 2258929
    iget-object v1, p0, LX/FaZ;->d:LX/0ad;

    sget-short v2, LX/100;->ae:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/FaZ;->d:LX/0ad;

    sget-short v2, LX/100;->af:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 2258930
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FaZ;->a:LX/CwT;

    .line 2258931
    iget-boolean v1, v0, LX/CwT;->e:Z

    move v0, v1

    .line 2258932
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2258933
    iget-object v0, p0, LX/FaZ;->a:LX/CwT;

    const/4 v1, 0x0

    .line 2258934
    iput-boolean v1, v0, LX/CwT;->e:Z

    .line 2258935
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2258936
    iget-boolean v0, p0, LX/FaZ;->e:Z

    if-nez v0, :cond_0

    .line 2258937
    iget-object v0, p0, LX/FaZ;->b:LX/13B;

    iget-object v1, p0, LX/FaZ;->a:LX/CwT;

    invoke-virtual {v0, v1}, LX/13B;->a(LX/CwT;)V

    .line 2258938
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FaZ;->e:Z

    .line 2258939
    :cond_0
    return-void
.end method
