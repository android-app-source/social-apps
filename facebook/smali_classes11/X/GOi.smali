.class public final LX/GOi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
        "Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V
    .locals 0

    .prologue
    .line 2347976
    iput-object p1, p0, LX/GOi;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2347974
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v0, v0

    .line 2347975
    const-class v1, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    invoke-static {v0, v1}, LX/0Ph;->b(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    return-object v0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2347977
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-static {p1}, LX/GOi;->a(Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    move-result-object v0

    return-object v0
.end method
