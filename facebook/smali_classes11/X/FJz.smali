.class public LX/FJz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/FJz;


# instance fields
.field private final b:LX/0WJ;

.field private final c:LX/0aG;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:LX/FJx;

.field private final f:Ljava/util/concurrent/ScheduledExecutorService;

.field private g:LX/1ML;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public h:LX/6j4;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/service/model/SetSettingsParams;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public j:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile l:LX/FK1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2224339
    const-class v0, LX/FJz;

    sput-object v0, LX/FJz;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0WJ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/FJx;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224333
    iput-object p1, p0, LX/FJz;->b:LX/0WJ;

    .line 2224334
    iput-object p2, p0, LX/FJz;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2224335
    iput-object p3, p0, LX/FJz;->c:LX/0aG;

    .line 2224336
    iput-object p4, p0, LX/FJz;->e:LX/FJx;

    .line 2224337
    iput-object p5, p0, LX/FJz;->f:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2224338
    return-void
.end method

.method public static a(LX/0QB;)LX/FJz;
    .locals 9

    .prologue
    .line 2224319
    sget-object v0, LX/FJz;->m:LX/FJz;

    if-nez v0, :cond_1

    .line 2224320
    const-class v1, LX/FJz;

    monitor-enter v1

    .line 2224321
    :try_start_0
    sget-object v0, LX/FJz;->m:LX/FJz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2224322
    if-eqz v2, :cond_0

    .line 2224323
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2224324
    new-instance v3, LX/FJz;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v6

    check-cast v6, LX/0aG;

    invoke-static {v0}, LX/FJx;->a(LX/0QB;)LX/FJx;

    move-result-object v7

    check-cast v7, LX/FJx;

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct/range {v3 .. v8}, LX/FJz;-><init>(LX/0WJ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/FJx;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 2224325
    move-object v0, v3

    .line 2224326
    sput-object v0, LX/FJz;->m:LX/FJz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2224327
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2224328
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2224329
    :cond_1
    sget-object v0, LX/FJz;->m:LX/FJz;

    return-object v0

    .line 2224330
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2224331
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/FJz;)V
    .locals 5

    .prologue
    .line 2224239
    iget-object v0, p0, LX/FJz;->f:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/prefs/notifications/GlobalNotificationPrefsSynchronizer$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/prefs/notifications/GlobalNotificationPrefsSynchronizer$1;-><init>(LX/FJz;)V

    iget-wide v2, p0, LX/FJz;->j:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2224240
    return-void
.end method

.method public static declared-synchronized f(LX/FJz;)V
    .locals 4

    .prologue
    .line 2224306
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FJz;->h:LX/6j4;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 2224307
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2224308
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/FJz;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2224309
    iget-object v0, p0, LX/FJz;->g:LX/1ML;

    if-nez v0, :cond_0

    .line 2224310
    iget-object v0, p0, LX/FJz;->h:LX/6j4;

    .line 2224311
    new-instance v1, Lcom/facebook/messaging/service/model/SetSettingsParams;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/SetSettingsParams;-><init>(LX/6j4;)V

    move-object v0, v1

    .line 2224312
    iput-object v0, p0, LX/FJz;->i:Lcom/facebook/messaging/service/model/SetSettingsParams;

    .line 2224313
    const/4 v0, 0x0

    iput-object v0, p0, LX/FJz;->h:LX/6j4;

    .line 2224314
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2224315
    const-string v1, "setSettingsParams"

    iget-object v2, p0, LX/FJz;->i:Lcom/facebook/messaging/service/model/SetSettingsParams;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2224316
    iget-object v1, p0, LX/FJz;->c:LX/0aG;

    const-string v2, "update_user_settings"

    const v3, -0x55acdd16

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, LX/FJz;->g:LX/1ML;

    .line 2224317
    iget-object v0, p0, LX/FJz;->g:LX/1ML;

    new-instance v1, LX/FJy;

    invoke-direct {v1, p0}, LX/FJy;-><init>(LX/FJz;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2224318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized g(LX/FJz;)V
    .locals 2

    .prologue
    .line 2224298
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/FJz;->g:LX/1ML;

    .line 2224299
    const/4 v0, 0x0

    iput-object v0, p0, LX/FJz;->i:Lcom/facebook/messaging/service/model/SetSettingsParams;

    .line 2224300
    const-wide/16 v0, 0xfa0

    iput-wide v0, p0, LX/FJz;->j:J

    .line 2224301
    const/4 v0, 0x0

    iput v0, p0, LX/FJz;->k:I

    .line 2224302
    invoke-static {p0}, LX/FJz;->f(LX/FJz;)V

    .line 2224303
    invoke-static {p0}, LX/FJz;->i(LX/FJz;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2224304
    monitor-exit p0

    return-void

    .line 2224305
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized h(LX/FJz;)V
    .locals 6

    .prologue
    .line 2224279
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/FJz;->g:LX/1ML;

    .line 2224280
    iget-object v0, p0, LX/FJz;->h:LX/6j4;

    if-nez v0, :cond_0

    .line 2224281
    new-instance v0, LX/6j4;

    invoke-direct {v0}, LX/6j4;-><init>()V

    iput-object v0, p0, LX/FJz;->h:LX/6j4;

    .line 2224282
    :cond_0
    iget-object v0, p0, LX/FJz;->h:LX/6j4;

    const/4 v1, 0x1

    .line 2224283
    iput-boolean v1, v0, LX/6j4;->a:Z

    .line 2224284
    iget-object v0, p0, LX/FJz;->h:LX/6j4;

    iget-object v1, p0, LX/FJz;->i:Lcom/facebook/messaging/service/model/SetSettingsParams;

    .line 2224285
    iget-object v2, v1, Lcom/facebook/messaging/service/model/SetSettingsParams;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-object v1, v2

    .line 2224286
    iput-object v1, v0, LX/6j4;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2224287
    const/4 v0, 0x0

    iput-object v0, p0, LX/FJz;->i:Lcom/facebook/messaging/service/model/SetSettingsParams;

    .line 2224288
    iget v0, p0, LX/FJz;->k:I

    int-to-long v0, v0

    const-wide/16 v2, 0x5

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2224289
    iget v0, p0, LX/FJz;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/FJz;->k:I

    .line 2224290
    const-wide/16 v0, 0x2

    iget-wide v2, p0, LX/FJz;->j:J

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/FJz;->j:J

    .line 2224291
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to update thread notification settings. Retrying in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/FJz;->j:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2224292
    invoke-static {p0}, LX/FJz;->e(LX/FJz;)V

    .line 2224293
    :goto_0
    invoke-static {p0}, LX/FJz;->i(LX/FJz;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2224294
    monitor-exit p0

    return-void

    .line 2224295
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, LX/FJz;->k:I

    .line 2224296
    const-wide/16 v0, 0xfa0

    iput-wide v0, p0, LX/FJz;->j:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2224297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static i(LX/FJz;)V
    .locals 1

    .prologue
    .line 2224275
    iget-object v0, p0, LX/FJz;->l:LX/FK1;

    .line 2224276
    if-eqz v0, :cond_0

    .line 2224277
    invoke-virtual {v0}, LX/FK1;->a()V

    .line 2224278
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2224255
    iget-object v0, p0, LX/FJz;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2224256
    :goto_0
    return-void

    .line 2224257
    :cond_0
    iget-object v0, p0, LX/FJz;->e:LX/FJx;

    invoke-virtual {v0}, LX/FJx;->e()LX/FK4;

    move-result-object v0

    .line 2224258
    monitor-enter p0

    .line 2224259
    :try_start_0
    iget-object v1, p0, LX/FJz;->h:LX/6j4;

    if-nez v1, :cond_1

    invoke-virtual {v0}, LX/FK4;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2224260
    monitor-exit p0

    goto :goto_0

    .line 2224261
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2224262
    iget-object v0, p0, LX/FJz;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->J:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    .line 2224263
    monitor-enter p0

    .line 2224264
    :try_start_2
    iget-object v4, p0, LX/FJz;->h:LX/6j4;

    if-nez v4, :cond_2

    .line 2224265
    new-instance v4, LX/6j4;

    invoke-direct {v4}, LX/6j4;-><init>()V

    iput-object v4, p0, LX/FJz;->h:LX/6j4;

    .line 2224266
    const-wide/16 v4, 0xfa0

    iput-wide v4, p0, LX/FJz;->j:J

    .line 2224267
    invoke-static {p0}, LX/FJz;->e(LX/FJz;)V

    .line 2224268
    :cond_2
    iget-object v4, p0, LX/FJz;->h:LX/6j4;

    move-object v1, v4

    .line 2224269
    const/4 v2, 0x1

    .line 2224270
    iput-boolean v2, v1, LX/6j4;->a:Z

    .line 2224271
    iput-object v0, v1, LX/6j4;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2224272
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2224273
    invoke-static {p0}, LX/FJz;->i(LX/FJz;)V

    goto :goto_0

    .line 2224274
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 2224242
    iget-object v0, p0, LX/FJz;->e:LX/FJx;

    invoke-virtual {v0}, LX/FJx;->e()LX/FK4;

    move-result-object v0

    .line 2224243
    invoke-virtual {v0}, LX/FK4;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2224244
    :goto_0
    return-void

    .line 2224245
    :cond_0
    monitor-enter p0

    .line 2224246
    :try_start_0
    iget-object v1, p0, LX/FJz;->i:Lcom/facebook/messaging/service/model/SetSettingsParams;

    if-nez v1, :cond_1

    .line 2224247
    iget-object v1, p0, LX/FJz;->h:LX/6j4;

    if-nez v1, :cond_1

    .line 2224248
    iget-object v1, p0, LX/FJz;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2224249
    sget-object v2, LX/0db;->J:LX/0Tn;

    .line 2224250
    iget-object v0, v0, LX/FK4;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 2224251
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2224252
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2224253
    invoke-static {p0}, LX/FJz;->i(LX/FJz;)V

    goto :goto_0

    .line 2224254
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 2224241
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FJz;->g:LX/1ML;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FJz;->h:LX/6j4;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FJz;->i:Lcom/facebook/messaging/service/model/SetSettingsParams;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
