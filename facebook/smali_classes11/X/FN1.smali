.class public LX/FN1;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:Lcom/facebook/messaging/sms/SmsThreadManager;

.field private final d:LX/FMd;

.field private final e:LX/30m;

.field private final f:LX/FNi;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMu;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/FMw;

.field private final i:LX/FMc;

.field private final j:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

.field public final k:LX/2Or;

.field public final l:LX/3MF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2231878
    const-string v0, "content://sms/queued"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/FN1;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/messaging/sms/SmsThreadManager;LX/FMd;LX/30m;LX/FMc;LX/FNi;LX/0Ot;LX/FMw;Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;LX/2Or;LX/3MF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/messaging/sms/SmsThreadManager;",
            "LX/FMd;",
            "LX/30m;",
            "LX/FMc;",
            "LX/FNi;",
            "LX/0Ot",
            "<",
            "LX/FMu;",
            ">;",
            "LX/FMw;",
            "Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;",
            "LX/2Or;",
            "LX/3MF;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231866
    iput-object p1, p0, LX/FN1;->b:Landroid/content/Context;

    .line 2231867
    iput-object p2, p0, LX/FN1;->c:Lcom/facebook/messaging/sms/SmsThreadManager;

    .line 2231868
    iput-object p3, p0, LX/FN1;->d:LX/FMd;

    .line 2231869
    iput-object p4, p0, LX/FN1;->e:LX/30m;

    .line 2231870
    iput-object p6, p0, LX/FN1;->f:LX/FNi;

    .line 2231871
    iput-object p7, p0, LX/FN1;->g:LX/0Ot;

    .line 2231872
    iput-object p5, p0, LX/FN1;->i:LX/FMc;

    .line 2231873
    iput-object p8, p0, LX/FN1;->h:LX/FMw;

    .line 2231874
    iput-object p9, p0, LX/FN1;->j:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    .line 2231875
    iput-object p10, p0, LX/FN1;->k:LX/2Or;

    .line 2231876
    iput-object p11, p0, LX/FN1;->l:LX/3MF;

    .line 2231877
    return-void
.end method

.method private b(Lcom/facebook/messaging/model/messages/Message;)Landroid/net/Uri;
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 2232001
    iget-object v0, p0, LX/FN1;->e:LX/30m;

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2232002
    const/4 v10, 0x0

    move-object v7, v0

    move v11, v8

    move v12, v9

    invoke-static/range {v7 .. v12}, LX/30m;->a(LX/30m;ZZLjava/lang/String;II)V

    .line 2232003
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0}, LX/FMQ;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2232004
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 2232005
    const-string v2, "date"

    iget-wide v4, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2232006
    iget-object v2, p0, LX/FN1;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2232007
    invoke-virtual {v2, v0, v1, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2232008
    iget-object v1, p0, LX/FN1;->b:Landroid/content/Context;

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, LX/554;->a(Landroid/content/Context;Landroid/net/Uri;II)Z

    .line 2232009
    return-object v0
.end method

.method private b(Lcom/facebook/messaging/model/messages/Message;Z)Landroid/net/Uri;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v12, 0x0

    .line 2231984
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v10

    .line 2231985
    iget-object v5, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 2231986
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2231987
    new-instance v0, LX/FMS;

    const-string v1, "Empty message body."

    invoke-direct {v0, v1}, LX/FMS;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2231988
    :cond_0
    iget-object v0, p0, LX/FN1;->c:Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v0, v10, v11}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v1

    .line 2231989
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v2, :cond_1

    .line 2231990
    new-instance v0, LX/FMS;

    const-string v1, "Not single recipient thread."

    invoke-direct {v0, v1}, LX/FMS;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2231991
    :cond_1
    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2231992
    new-instance v2, LX/FMS;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Not supported destination: "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/FMS;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2231993
    :cond_2
    iget-object v0, p0, LX/FN1;->k:LX/2Or;

    invoke-virtual {v0}, LX/2Or;->r()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v2, p0, LX/FN1;->l:LX/3MF;

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/3MF;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2231994
    :goto_0
    :try_start_0
    iget v1, p1, Lcom/facebook/messaging/model/messages/Message;->V:I

    iget-object v0, p0, LX/FN1;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, LX/FN1;->a:Landroid/net/Uri;

    const/4 v6, 0x0

    iget-wide v8, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-eqz p2, :cond_3

    const-wide/16 v10, 0x0

    :cond_3
    invoke-static/range {v1 .. v11}, LX/554;->a(ILandroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJ)Landroid/net/Uri;

    move-result-object v0

    .line 2231995
    iget-object v1, p0, LX/FN1;->e:LX/30m;

    invoke-virtual {v1}, LX/30m;->a()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2231996
    return-object v0

    .line 2231997
    :cond_4
    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    goto :goto_0

    .line 2231998
    :catch_0
    move-exception v0

    .line 2231999
    const-string v1, "SmsSender"

    const-string v2, "Queue sms msg failed."

    new-array v3, v12, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2232000
    new-instance v1, LX/FMS;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/FMS;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private c(Lcom/facebook/messaging/model/messages/Message;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2232010
    iget-object v0, p0, LX/FN1;->e:LX/30m;

    invoke-static {p1}, LX/30m;->a(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v2, v2, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    iget-object v3, p0, LX/FN1;->c:Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/facebook/messaging/sms/SmsThreadManager;->b(J)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/30m;->b(Ljava/lang/String;II)V

    .line 2232011
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0}, LX/FMQ;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2232012
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 2232013
    const-string v2, "date"

    iget-wide v4, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2232014
    const-string v2, "st"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2232015
    iget-object v2, p0, LX/FN1;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2232016
    invoke-virtual {v2, v0, v1, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2232017
    return-object v0
.end method

.method private d(Lcom/facebook/messaging/model/messages/Message;)Landroid/net/Uri;
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2231914
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v2, v2, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2231915
    invoke-static {p1}, LX/30m;->a(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v0

    .line 2231916
    iget-object v1, p0, LX/FN1;->e:LX/30m;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v2, v2, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    iget-object v3, p0, LX/FN1;->c:Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/facebook/messaging/sms/SmsThreadManager;->b(J)I

    move-result v3

    .line 2231917
    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v8, v1

    move-object v11, v0

    move v12, v2

    move v13, v3

    invoke-static/range {v8 .. v13}, LX/30m;->a(LX/30m;ZZLjava/lang/String;II)V

    .line 2231918
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 2231919
    :try_start_0
    iget-object v0, p0, LX/FN1;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMu;

    iget v1, p1, Lcom/facebook/messaging/model/messages/Message;->V:I

    invoke-virtual {v0, v1}, LX/FMu;->b(I)I

    move-result v0

    .line 2231920
    iget-object v1, p0, LX/FN1;->j:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    invoke-virtual {v1, p1, v0, v7}, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->a(Lcom/facebook/messaging/model/messages/Message;ILjava/util/HashSet;)LX/EdY;

    move-result-object v0

    .line 2231921
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    .line 2231922
    iget-object v1, p0, LX/FN1;->c:Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v1

    .line 2231923
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2231924
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2231925
    iget-object v4, p0, LX/FN1;->k:LX/2Or;

    invoke-virtual {v4}, LX/2Or;->r()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2231926
    iget-object v4, p0, LX/FN1;->l:LX/3MF;

    invoke-virtual {v4, v1}, LX/3MF;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 2231927
    :goto_1
    invoke-static {v4}, LX/Eds;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2231928
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p0, LX/FN1;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FMu;

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2231929
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 2231930
    :cond_3
    :goto_2
    move v1, v9

    .line 2231931
    if-eqz v1, :cond_c

    .line 2231932
    :goto_3
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {v6, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2231933
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2231934
    :cond_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v6, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v2, v1

    .line 2231935
    array-length v1, v2

    if-gtz v1, :cond_5

    .line 2231936
    new-instance v0, LX/EdT;

    const-string v1, "No valid sending destination(s)."

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch LX/EdT; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2231937
    :catch_0
    move-exception v0

    .line 2231938
    :try_start_1
    const-string v1, "SmsSender"

    const-string v2, "Queue mms failed."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2231939
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2231940
    :catchall_0
    move-exception v0

    move-object v1, v0

    invoke-virtual {v7}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 2231941
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_4

    .line 2231942
    :cond_5
    :try_start_2
    new-instance v1, LX/Edn;

    invoke-direct {v1}, LX/Edn;-><init>()V

    .line 2231943
    array-length v5, v2

    .line 2231944
    if-lez v5, :cond_6

    .line 2231945
    new-array v3, v5, [LX/EdS;

    .line 2231946
    const/4 v4, 0x0

    :goto_5
    if-ge v4, v5, :cond_7

    .line 2231947
    new-instance v6, LX/EdS;

    aget-object v8, v2, v4

    invoke-direct {v6, v8}, LX/EdS;-><init>(Ljava/lang/String;)V

    aput-object v6, v3, v4

    .line 2231948
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 2231949
    :cond_6
    const/4 v3, 0x0

    :cond_7
    move-object v2, v3

    .line 2231950
    if-eqz v2, :cond_8

    .line 2231951
    iget-object v3, v1, LX/EdM;->a:LX/Ede;

    const/16 v4, 0x97

    invoke-virtual {v3, v2, v4}, LX/Ede;->a([LX/EdS;I)V

    .line 2231952
    :cond_8
    iget-wide v2, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 2231953
    iget-object v4, v1, LX/EdM;->a:LX/Ede;

    const/16 v5, 0x85

    invoke-virtual {v4, v2, v3, v5}, LX/Ede;->a(JI)V

    .line 2231954
    iput-object v0, v1, LX/EdV;->b:LX/EdY;

    .line 2231955
    iget-object v0, p0, LX/FN1;->b:Landroid/content/Context;

    invoke-static {v0}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v0

    .line 2231956
    iget-object v2, p0, LX/FN1;->f:LX/FNi;

    iget-object v3, p0, LX/FN1;->b:Landroid/content/Context;

    invoke-virtual {v2, v3}, LX/FNi;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 2231957
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 2231958
    new-instance v2, LX/EdS;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v2, v3}, LX/EdS;-><init>([B)V

    invoke-virtual {v1, v2}, LX/EdM;->a(LX/EdS;)V

    .line 2231959
    :cond_9
    sget-object v2, LX/551;->a:Landroid/net/Uri;

    iget v3, p1, Lcom/facebook/messaging/model/messages/Message;->V:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/Edh;->a(LX/EdM;Landroid/net/Uri;ILjava/lang/String;ZLjava/util/Map;)Landroid/net/Uri;

    move-result-object v2

    .line 2231960
    new-instance v0, Landroid/content/ContentValues;

    const/4 v3, 0x7

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 2231961
    const-string v3, "proto_type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231962
    const-string v3, "msg_id"

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2231963
    const-string v3, "msg_type"

    invoke-virtual {v1}, LX/EdM;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231964
    const-string v1, "err_type"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231965
    const-string v1, "err_code"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231966
    const-string v1, "retry_index"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231967
    const-string v1, "due_time"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2231968
    iget-object v1, p0, LX/FN1;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, LX/552;->a:Landroid/net/Uri;

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catch LX/EdT; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2231969
    invoke-virtual {v7}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 2231970
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_6

    .line 2231971
    :cond_a
    throw v1

    :cond_b
    return-object v2

    :cond_c
    move-object v4, v5

    goto/16 :goto_3

    :cond_d
    move-object v4, v1

    goto/16 :goto_1

    .line 2231972
    :cond_e
    invoke-static {v1}, LX/FMu;->c(LX/FMu;)Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "aliasEnabled"

    invoke-virtual {v11, v12, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 2231973
    if-eqz v11, :cond_3

    .line 2231974
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v12

    .line 2231975
    invoke-static {v1}, LX/FMu;->c(LX/FMu;)Landroid/os/Bundle;

    move-result-object v11

    const-string v13, "aliasMinChars"

    const/4 v2, 0x2

    invoke-virtual {v11, v13, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 2231976
    invoke-static {v1}, LX/FMu;->c(LX/FMu;)Landroid/os/Bundle;

    move-result-object v13

    const-string v2, "aliasMaxChars"

    const/16 v3, 0x30

    invoke-virtual {v13, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    .line 2231977
    if-lt v12, v11, :cond_3

    if-gt v12, v13, :cond_3

    .line 2231978
    invoke-virtual {v4, v9}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Ljava/lang/Character;->isLetter(C)Z

    move-result v11

    if-eqz v11, :cond_3

    move v11, v10

    .line 2231979
    :goto_7
    if-ge v11, v12, :cond_10

    .line 2231980
    invoke-virtual {v4, v11}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 2231981
    invoke-static {v13}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v2

    if-nez v2, :cond_f

    const/16 v2, 0x2e

    if-ne v13, v2, :cond_3

    .line 2231982
    :cond_f
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    :cond_10
    move v9, v10

    .line 2231983
    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;Z)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2231879
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2231880
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2231881
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2231882
    iget-object v0, p0, LX/FN1;->i:LX/FMc;

    invoke-virtual {v0}, LX/FMc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2231883
    iget-object v0, p0, LX/FN1;->i:LX/FMc;

    iget-wide v4, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    .line 2231884
    iget-wide v8, v0, LX/FMc;->g:J

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    iput-wide v8, v0, LX/FMc;->g:J

    .line 2231885
    :cond_0
    iget-object v0, p0, LX/FN1;->c:Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/facebook/messaging/sms/SmsThreadManager;->d(J)Z

    move-result v3

    .line 2231886
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0}, LX/FMQ;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2231887
    invoke-direct {p0, p1}, LX/FN1;->c(Lcom/facebook/messaging/model/messages/Message;)Landroid/net/Uri;
    :try_end_0
    .catch LX/EdT; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2231888
    :goto_1
    if-nez v0, :cond_5

    .line 2231889
    new-instance v0, LX/FMS;

    const-string v1, "Message URI was null"

    invoke-direct {v0, v1}, LX/FMS;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v2

    .line 2231890
    goto :goto_0

    .line 2231891
    :cond_2
    :try_start_1
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0}, LX/FMQ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2231892
    invoke-direct {p0, p1}, LX/FN1;->b(Lcom/facebook/messaging/model/messages/Message;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 2231893
    :cond_3
    iget-object v0, p0, LX/FN1;->j:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2231894
    invoke-direct {p0, p1}, LX/FN1;->d(Lcom/facebook/messaging/model/messages/Message;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    .line 2231895
    :cond_4
    invoke-direct {p0, p1, v3}, LX/FN1;->b(Lcom/facebook/messaging/model/messages/Message;Z)Landroid/net/Uri;
    :try_end_1
    .catch LX/EdT; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 2231896
    :catch_0
    move-exception v0

    .line 2231897
    new-instance v1, LX/FMS;

    invoke-direct {v1, v0}, LX/FMS;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2231898
    :cond_5
    invoke-static {v0}, LX/FMQ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 2231899
    if-eqz p2, :cond_6

    .line 2231900
    iget-object v4, p0, LX/FN1;->d:LX/FMd;

    iget-object v5, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, LX/FMd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2231901
    :cond_6
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    .line 2231902
    if-eqz v3, :cond_7

    .line 2231903
    iget-object v7, p0, LX/FN1;->c:Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v7, v0, v4, v5}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Landroid/net/Uri;J)J

    move-result-wide v4

    .line 2231904
    :cond_7
    iget-object v0, p0, LX/FN1;->h:LX/FMw;

    invoke-virtual {v0, v6, v4, v5}, LX/FMw;->a(Ljava/lang/String;J)V

    .line 2231905
    if-eqz v3, :cond_a

    .line 2231906
    iget-object v0, p0, LX/FN1;->c:Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/facebook/messaging/sms/SmsThreadManager;->b(J)I

    move-result v0

    .line 2231907
    invoke-static {p1}, LX/30m;->a(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v3

    .line 2231908
    if-gt v0, v1, :cond_8

    if-eqz v3, :cond_9

    :cond_8
    move v2, v1

    .line 2231909
    :cond_9
    iget-object v1, p0, LX/FN1;->e:LX/30m;

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v4, v4, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    .line 2231910
    const-string v5, "sms_takeover_create_thread"

    invoke-static {v1, v5}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2231911
    invoke-static {v5, v2, v3, v4, v0}, LX/30m;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;ZLjava/lang/String;II)V

    .line 2231912
    invoke-static {v1, v5}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2231913
    :cond_a
    return-object v6
.end method
