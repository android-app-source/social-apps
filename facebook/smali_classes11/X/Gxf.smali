.class public final LX/Gxf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:[Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2408370
    iput-object p1, p0, LX/Gxf;->b:Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;

    iput-object p2, p0, LX/Gxf;->a:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2408363
    iget-object v0, p0, LX/Gxf;->a:[Ljava/lang/String;

    aget-object v0, v0, p3

    .line 2408364
    iget-object v1, p0, LX/Gxf;->b:Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;

    invoke-static {v1}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->a(Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;)Ljava/lang/String;

    move-result-object v1

    .line 2408365
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2408366
    iget-object v2, p0, LX/Gxf;->b:Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;

    iget-object v2, v2, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->r:LX/Gxx;

    invoke-virtual {v2, v1, v0}, LX/Gxx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2408367
    iget-object v1, p0, LX/Gxf;->b:Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;

    iget-object v1, v1, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->q:LX/0dz;

    invoke-virtual {v1, v0}, LX/0dz;->a(Ljava/lang/String;)V

    .line 2408368
    :cond_0
    iget-object v0, p0, LX/Gxf;->b:Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;

    invoke-virtual {v0}, Lcom/facebook/languages/switcher/activity/LanguageSwitcherBookmarksActivity;->finish()V

    .line 2408369
    return-void
.end method
