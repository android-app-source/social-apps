.class public LX/Gqt;
.super LX/Cqm;
.source ""


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 7

    .prologue
    .line 2397016
    invoke-direct {p0, p1, p2}, LX/Cqm;-><init>(LX/Ctg;LX/CrK;)V

    .line 2397017
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->a:LX/Cqw;

    .line 2397018
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2397019
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_ASPECT_FIT:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 2397020
    new-instance v0, LX/Cqf;

    sget-object v1, LX/Cqw;->b:LX/Cqw;

    .line 2397021
    iget-object v2, p0, LX/CqX;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2397022
    check-cast v2, LX/Ctg;

    sget-object v3, LX/Cqd;->MEDIA_FULLSCREEN:LX/Cqd;

    sget-object v4, LX/Cqe;->OVERLAY_MEDIA:LX/Cqe;

    sget-object v5, LX/Cqc;->ANNOTATION_DEFAULT:LX/Cqc;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    invoke-virtual {p0, v0}, LX/CqX;->a(LX/Cqf;)V

    .line 2397023
    return-void
.end method


# virtual methods
.method public final i()Lcom/facebook/richdocument/view/widget/SlideshowView;
    .locals 2

    .prologue
    .line 2397024
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2397025
    :goto_0
    instance-of v1, v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    if-nez v1, :cond_0

    .line 2397026
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 2397027
    :cond_0
    check-cast v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 2397028
    const/4 v0, 0x0

    return v0
.end method
