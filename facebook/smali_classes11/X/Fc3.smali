.class public LX/Fc3;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261916
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2261917
    return-void
.end method

.method public static a(LX/0QB;)LX/Fc3;
    .locals 3

    .prologue
    .line 2261827
    const-class v1, LX/Fc3;

    monitor-enter v1

    .line 2261828
    :try_start_0
    sget-object v0, LX/Fc3;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261829
    sput-object v2, LX/Fc3;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261830
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261831
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2261832
    new-instance v0, LX/Fc3;

    invoke-direct {v0}, LX/Fc3;-><init>()V

    .line 2261833
    move-object v0, v0

    .line 2261834
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261835
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fc3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261836
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261837
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261838
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261839
    if-nez v0, :cond_0

    .line 2261840
    const/4 v0, 0x0

    .line 2261841
    :goto_0
    return-object v0

    .line 2261842
    :cond_0
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 2261843
    iput-object v2, v1, LX/8dX;->L:Ljava/lang/String;

    .line 2261844
    move-object v1, v1

    .line 2261845
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, -0x5cf17590

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2261846
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2261847
    move-object v1, v1

    .line 2261848
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v2

    .line 2261849
    iput-wide v2, v1, LX/8dX;->r:J

    .line 2261850
    move-object v1, v1

    .line 2261851
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 2261852
    if-nez v2, :cond_1

    .line 2261853
    const/4 v3, 0x0

    .line 2261854
    :goto_1
    move-object v2, v3

    .line 2261855
    iput-object v2, v1, LX/8dX;->aQ:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    .line 2261856
    move-object v1, v1

    .line 2261857
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 2261858
    if-nez v2, :cond_2

    .line 2261859
    const/4 v3, 0x0

    .line 2261860
    :goto_2
    move-object v2, v3

    .line 2261861
    iput-object v2, v1, LX/8dX;->aG:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    .line 2261862
    move-object v1, v1

    .line 2261863
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v2

    .line 2261864
    iput-object v2, v1, LX/8dX;->aV:Ljava/lang/String;

    .line 2261865
    move-object v1, v1

    .line 2261866
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->cV()Ljava/lang/String;

    move-result-object v2

    .line 2261867
    iput-object v2, v1, LX/8dX;->B:Ljava/lang/String;

    .line 2261868
    move-object v1, v1

    .line 2261869
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->H()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v2

    .line 2261870
    if-nez v2, :cond_3

    .line 2261871
    const/4 v3, 0x0

    .line 2261872
    :goto_3
    move-object v2, v3

    .line 2261873
    iput-object v2, v1, LX/8dX;->e:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    .line 2261874
    move-object v1, v1

    .line 2261875
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kB()Lcom/facebook/graphql/model/GraphQLVideoShare;

    move-result-object v2

    .line 2261876
    if-nez v2, :cond_4

    .line 2261877
    const/4 v3, 0x0

    .line 2261878
    :goto_4
    move-object v2, v3

    .line 2261879
    iput-object v2, v1, LX/8dX;->aY:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    .line 2261880
    move-object v1, v1

    .line 2261881
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2261882
    if-nez v0, :cond_5

    .line 2261883
    const/4 v2, 0x0

    .line 2261884
    :goto_5
    move-object v0, v2

    .line 2261885
    iput-object v0, v1, LX/8dX;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    .line 2261886
    move-object v0, v1

    .line 2261887
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2261888
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    .line 2261889
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2261890
    move-object v0, v1

    .line 2261891
    goto :goto_0

    :cond_1
    new-instance v3, LX/8dm;

    invoke-direct {v3}, LX/8dm;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 2261892
    iput-object v4, v3, LX/8dm;->b:Ljava/lang/String;

    .line 2261893
    move-object v3, v3

    .line 2261894
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/FbH;->a(LX/0Px;)LX/0Px;

    move-result-object v4

    .line 2261895
    iput-object v4, v3, LX/8dm;->a:LX/0Px;

    .line 2261896
    move-object v3, v3

    .line 2261897
    invoke-virtual {v3}, LX/8dm;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v3

    goto :goto_1

    :cond_2
    new-instance v3, LX/8dk;

    invoke-direct {v3}, LX/8dk;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 2261898
    iput-object v4, v3, LX/8dk;->b:Ljava/lang/String;

    .line 2261899
    move-object v3, v3

    .line 2261900
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/FbH;->a(LX/0Px;)LX/0Px;

    move-result-object v4

    .line 2261901
    iput-object v4, v3, LX/8dk;->a:LX/0Px;

    .line 2261902
    move-object v3, v3

    .line 2261903
    invoke-virtual {v3}, LX/8dk;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v3

    goto :goto_2

    :cond_3
    new-instance v3, LX/8dW;

    invoke-direct {v3}, LX/8dW;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;->a()I

    move-result v4

    .line 2261904
    iput v4, v3, LX/8dW;->a:I

    .line 2261905
    move-object v3, v3

    .line 2261906
    invoke-virtual {v3}, LX/8dW;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v3

    goto :goto_3

    :cond_4
    new-instance v3, LX/8dn;

    invoke-direct {v3}, LX/8dn;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoShare;->a()Ljava/lang/String;

    move-result-object v4

    .line 2261907
    iput-object v4, v3, LX/8dn;->a:Ljava/lang/String;

    .line 2261908
    move-object v3, v3

    .line 2261909
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoShare;->j()I

    move-result v4

    .line 2261910
    iput v4, v3, LX/8dn;->b:I

    .line 2261911
    move-object v3, v3

    .line 2261912
    invoke-virtual {v3}, LX/8dn;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$VideoShareModel;

    move-result-object v3

    goto :goto_4

    :cond_5
    new-instance v2, LX/8dg;

    invoke-direct {v2}, LX/8dg;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bA()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    .line 2261913
    iput-object v3, v2, LX/8dg;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2261914
    move-object v2, v2

    .line 2261915
    invoke-virtual {v2}, LX/8dg;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LinkMediaModel;

    move-result-object v2

    goto/16 :goto_5
.end method
