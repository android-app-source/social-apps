.class public LX/H4i;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/H4i;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/H4i",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2423325
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2423326
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/H4i;->b:LX/0Zi;

    .line 2423327
    iput-object p1, p0, LX/H4i;->a:LX/0Ot;

    .line 2423328
    return-void
.end method

.method public static a(LX/0QB;)LX/H4i;
    .locals 4

    .prologue
    .line 2423329
    sget-object v0, LX/H4i;->c:LX/H4i;

    if-nez v0, :cond_1

    .line 2423330
    const-class v1, LX/H4i;

    monitor-enter v1

    .line 2423331
    :try_start_0
    sget-object v0, LX/H4i;->c:LX/H4i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2423332
    if-eqz v2, :cond_0

    .line 2423333
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2423334
    new-instance v3, LX/H4i;

    const/16 p0, 0x2ac7

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/H4i;-><init>(LX/0Ot;)V

    .line 2423335
    move-object v0, v3

    .line 2423336
    sput-object v0, LX/H4i;->c:LX/H4i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2423337
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2423338
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2423339
    :cond_1
    sget-object v0, LX/H4i;->c:LX/H4i;

    return-object v0

    .line 2423340
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2423341
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 6

    .prologue
    .line 2423342
    check-cast p2, LX/H4h;

    .line 2423343
    iget-object v0, p0, LX/H4i;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    iget-object v2, p2, LX/H4h;->f:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p2, LX/H4h;->g:Ljava/lang/String;

    iget-object v4, p2, LX/H4h;->h:LX/2km;

    iget-object v5, p2, LX/H4h;->i:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-object v1, p1

    .line 2423344
    invoke-static/range {v0 .. v5}, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->b(Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2423345
    return-void
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 6

    .prologue
    .line 2423346
    check-cast p2, LX/H4h;

    .line 2423347
    iget-object v0, p0, LX/H4i;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    iget-object v2, p2, LX/H4h;->j:Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v3, p2, LX/H4h;->g:Ljava/lang/String;

    iget-object v4, p2, LX/H4h;->h:LX/2km;

    iget-object v5, p2, LX/H4h;->i:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-object v1, p1

    .line 2423348
    invoke-static/range {v0 .. v5}, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->b(Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;Ljava/lang/String;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2423349
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2423350
    const v0, -0x18bdc8a0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2423351
    check-cast p2, LX/H4h;

    .line 2423352
    iget-object v0, p0, LX/H4i;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;

    iget-object v2, p2, LX/H4h;->a:Landroid/net/Uri;

    iget-object v3, p2, LX/H4h;->b:Ljava/lang/String;

    iget-object v4, p2, LX/H4h;->d:Ljava/lang/String;

    iget-object v5, p2, LX/H4h;->e:Landroid/net/Uri;

    move-object v1, p1

    const/4 v7, 0x0

    const/16 p2, 0x8

    const/4 p1, 0x1

    const/4 p0, 0x2

    .line 2423353
    iget-object v6, v0, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->c:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1Ad;

    .line 2423354
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x6

    const v10, 0x7f010717

    invoke-interface {v8, v9, v10}, LX/1Dh;->p(II)LX/1Dh;

    move-result-object v8

    .line 2423355
    const v9, -0x18bdc8a0

    const/4 v10, 0x0

    invoke-static {v1, v9, v10}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 2423356
    invoke-interface {v8, v9}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v9

    invoke-static {v1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v10

    const v11, 0x7f0a010a

    invoke-virtual {v10, v11}, LX/1nh;->i(I)LX/1nh;

    move-result-object v10

    invoke-virtual {v10}, LX/1n6;->b()LX/1dc;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/1up;->a(LX/1dc;)LX/1up;

    move-result-object v9

    invoke-virtual {v6, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v10

    sget-object v11, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v10, v11}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v10

    invoke-virtual {v10}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const v10, 0x7f0b1636

    invoke-interface {v9, v10}, LX/1Di;->q(I)LX/1Di;

    move-result-object v9

    const v10, 0x7f0b1636

    invoke-interface {v9, v10}, LX/1Di;->i(I)LX/1Di;

    move-result-object v9

    const v10, 0x7f0b163a

    invoke-interface {v9, p2, v10}, LX/1Di;->c(II)LX/1Di;

    move-result-object v9

    const v10, 0x7f0b163d

    invoke-interface {v9, p0, v10}, LX/1Di;->c(II)LX/1Di;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v9, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-interface {v9, v10}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v9

    const v10, 0x7f0b163a

    invoke-interface {v9, p0, v10}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, p1}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v10

    const v11, 0x3f91eb85    # 1.14f

    invoke-virtual {v10, v11}, LX/1ne;->j(F)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v10, v11}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v10

    const v11, 0x7f0a010c

    invoke-virtual {v10, v11}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    sget-object v11, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p0, LX/0xr;->MEDIUM:LX/0xr;

    invoke-static {v1, v11, p0, v7}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v10

    const v11, 0x7f0b0050

    invoke-virtual {v10, v11}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    :goto_0
    invoke-interface {v9, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-interface {v8, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    .line 2423357
    if-eqz v5, :cond_0

    .line 2423358
    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v8

    invoke-virtual {v6, v5}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v6

    sget-object v9, Lcom/facebook/notifications/components/partdefinition/NotificationsReactionImageBlockComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v9}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v6

    invoke-virtual {v6}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v6

    invoke-virtual {v8, v6}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v8, 0x7f0b1632

    invoke-interface {v6, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const v8, 0x7f0b1632

    invoke-interface {v6, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v6

    const v8, 0x7f0b163a

    invoke-interface {v6, p2, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    .line 2423359
    const v8, -0x18b24aa7

    const/4 v9, 0x0

    invoke-static {v1, v8, v9}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v8

    move-object v8, v8

    .line 2423360
    invoke-interface {v6, v8}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2423361
    :cond_0
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2423362
    return-object v0

    .line 2423363
    :cond_1
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    const v10, 0x3faa3d71    # 1.33f

    invoke-virtual {v7, v10}, LX/1ne;->j(F)LX/1ne;

    move-result-object v7

    const/4 v10, 0x3

    invoke-virtual {v7, v10}, LX/1ne;->j(I)LX/1ne;

    move-result-object v7

    const v10, 0x7f0b004e

    invoke-virtual {v7, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v10}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v7

    const v10, 0x7f0a010e

    invoke-virtual {v7, v10}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2423364
    invoke-static {}, LX/1dS;->b()V

    .line 2423365
    iget v0, p1, LX/1dQ;->b:I

    .line 2423366
    sparse-switch v0, :sswitch_data_0

    .line 2423367
    :goto_0
    return-object v2

    .line 2423368
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2423369
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/H4i;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 2423370
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2423371
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/H4i;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x18bdc8a0 -> :sswitch_0
        -0x18b24aa7 -> :sswitch_1
    .end sparse-switch
.end method
