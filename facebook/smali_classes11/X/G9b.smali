.class public final LX/G9b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/G9K;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2322970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322971
    new-instance v0, LX/G9K;

    sget-object v1, LX/G9I;->e:LX/G9I;

    invoke-direct {v0, v1}, LX/G9K;-><init>(LX/G9I;)V

    iput-object v0, p0, LX/G9b;->a:LX/G9K;

    .line 2322972
    return-void
.end method

.method private a(LX/G9P;Ljava/util/Map;)LX/G99;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G9P;",
            "Ljava/util/Map",
            "<",
            "LX/G8t;",
            "*>;)",
            "LX/G99;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2322973
    invoke-virtual {p1}, LX/G9P;->b()LX/G9i;

    move-result-object v5

    .line 2322974
    invoke-virtual {p1}, LX/G9P;->a()LX/G9d;

    move-result-object v0

    .line 2322975
    iget-object v2, v0, LX/G9d;->c:LX/G9c;

    move-object v6, v2

    .line 2322976
    invoke-virtual {p1}, LX/G9P;->c()[B

    move-result-object v0

    .line 2322977
    invoke-static {v0, v5, v6}, LX/G9Q;->a([BLX/G9i;LX/G9c;)[LX/G9Q;

    move-result-object v7

    .line 2322978
    array-length v3, v7

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v7, v0

    .line 2322979
    iget v8, v4, LX/G9Q;->a:I

    move v4, v8

    .line 2322980
    add-int/2addr v2, v4

    .line 2322981
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2322982
    :cond_0
    new-array v8, v2, [B

    .line 2322983
    array-length v9, v7

    move v4, v1

    move v0, v1

    :goto_1
    if-ge v4, v9, :cond_2

    aget-object v2, v7, v4

    .line 2322984
    iget-object v3, v2, LX/G9Q;->b:[B

    move-object v10, v3

    .line 2322985
    iget v3, v2, LX/G9Q;->a:I

    move v11, v3

    .line 2322986
    invoke-direct {p0, v10, v11}, LX/G9b;->a([BI)V

    move v2, v0

    move v0, v1

    .line 2322987
    :goto_2
    if-ge v0, v11, :cond_1

    .line 2322988
    add-int/lit8 v3, v2, 0x1

    aget-byte v12, v10, v0

    aput-byte v12, v8, v2

    .line 2322989
    add-int/lit8 v0, v0, 0x1

    move v2, v3

    goto :goto_2

    .line 2322990
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2322991
    :cond_2
    invoke-static {v8, v5, v6, p2}, LX/G9a;->a([BLX/G9i;LX/G9c;Ljava/util/Map;)LX/G99;

    move-result-object v0

    return-object v0
.end method

.method private a([BI)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2322992
    array-length v2, p1

    .line 2322993
    new-array v3, v2, [I

    move v1, v0

    .line 2322994
    :goto_0
    if-ge v1, v2, :cond_0

    .line 2322995
    aget-byte v4, p1, v1

    and-int/lit16 v4, v4, 0xff

    aput v4, v3, v1

    .line 2322996
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2322997
    :cond_0
    array-length v1, p1

    sub-int/2addr v1, p2

    .line 2322998
    :try_start_0
    iget-object v2, p0, LX/G9b;->a:LX/G9K;

    invoke-virtual {v2, v3, v1}, LX/G9K;->a([II)V
    :try_end_0
    .catch LX/G9M; {:try_start_0 .. :try_end_0} :catch_0

    .line 2322999
    :goto_1
    if-ge v0, p2, :cond_1

    .line 2323000
    aget v1, v3, v0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 2323001
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2323002
    :catch_0
    sget-boolean v0, LX/G8r;->a:Z

    if-eqz v0, :cond_2

    new-instance v0, LX/G8s;

    invoke-direct {v0}, LX/G8s;-><init>()V

    :goto_2
    move-object v0, v0

    .line 2323003
    throw v0

    .line 2323004
    :cond_1
    return-void

    :cond_2
    sget-object v0, LX/G8s;->c:LX/G8s;

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/G96;Ljava/util/Map;)LX/G99;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G96;",
            "Ljava/util/Map",
            "<",
            "LX/G8t;",
            "*>;)",
            "LX/G99;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2323005
    new-instance v3, LX/G9P;

    invoke-direct {v3, p1}, LX/G9P;-><init>(LX/G96;)V

    .line 2323006
    :try_start_0
    invoke-direct {p0, v3, p2}, LX/G9b;->a(LX/G9P;Ljava/util/Map;)LX/G99;
    :try_end_0
    .catch LX/G8v; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/G8s; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 2323007
    :goto_0
    return-object v0

    .line 2323008
    :catch_0
    move-exception v1

    move-object v2, v1

    move-object v1, v0

    .line 2323009
    :goto_1
    :try_start_1
    iget-object v0, v3, LX/G9P;->c:LX/G9d;

    if-nez v0, :cond_5

    .line 2323010
    :goto_2
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 2323011
    iput-object v4, v3, LX/G9P;->b:LX/G9i;

    .line 2323012
    iput-object v4, v3, LX/G9P;->c:LX/G9d;

    .line 2323013
    iput-boolean v0, v3, LX/G9P;->d:Z

    .line 2323014
    invoke-virtual {v3}, LX/G9P;->b()LX/G9i;

    .line 2323015
    invoke-virtual {v3}, LX/G9P;->a()LX/G9d;

    .line 2323016
    const/4 v0, 0x0

    :goto_3
    iget-object v4, v3, LX/G9P;->a:LX/G96;

    .line 2323017
    iget v5, v4, LX/G96;->a:I

    move v4, v5

    .line 2323018
    if-ge v0, v4, :cond_2

    .line 2323019
    add-int/lit8 v4, v0, 0x1

    :goto_4
    iget-object v5, v3, LX/G9P;->a:LX/G96;

    .line 2323020
    iget p1, v5, LX/G96;->b:I

    move v5, p1

    .line 2323021
    if-ge v4, v5, :cond_1

    .line 2323022
    iget-object v5, v3, LX/G9P;->a:LX/G96;

    invoke-virtual {v5, v0, v4}, LX/G96;->a(II)Z

    move-result v5

    iget-object p1, v3, LX/G9P;->a:LX/G96;

    invoke-virtual {p1, v4, v0}, LX/G96;->a(II)Z

    move-result p1

    if-eq v5, p1, :cond_0

    .line 2323023
    iget-object v5, v3, LX/G9P;->a:LX/G96;

    invoke-virtual {v5, v4, v0}, LX/G96;->c(II)V

    .line 2323024
    iget-object v5, v3, LX/G9P;->a:LX/G96;

    invoke-virtual {v5, v0, v4}, LX/G96;->c(II)V

    .line 2323025
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 2323026
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2323027
    :cond_2
    invoke-direct {p0, v3, p2}, LX/G9b;->a(LX/G9P;Ljava/util/Map;)LX/G99;

    move-result-object v0

    .line 2323028
    new-instance v3, LX/G9f;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, LX/G9f;-><init>(Z)V

    .line 2323029
    iput-object v3, v0, LX/G99;->e:Ljava/lang/Object;
    :try_end_1
    .catch LX/G8v; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/G8s; {:try_start_1 .. :try_end_1} :catch_3

    .line 2323030
    goto :goto_0

    .line 2323031
    :catch_1
    move-exception v0

    .line 2323032
    :goto_5
    if-eqz v2, :cond_3

    .line 2323033
    throw v2

    .line 2323034
    :catch_2
    move-exception v1

    move-object v2, v0

    .line 2323035
    goto :goto_1

    .line 2323036
    :cond_3
    if-eqz v1, :cond_4

    .line 2323037
    throw v1

    .line 2323038
    :cond_4
    throw v0

    .line 2323039
    :catch_3
    move-exception v0

    goto :goto_5

    .line 2323040
    :cond_5
    iget-object v0, v3, LX/G9P;->c:LX/G9d;

    .line 2323041
    iget-byte v4, v0, LX/G9d;->d:B

    move v0, v4

    .line 2323042
    invoke-static {v0}, LX/G9R;->a(I)LX/G9R;

    move-result-object v0

    .line 2323043
    iget-object v4, v3, LX/G9P;->a:LX/G96;

    .line 2323044
    iget p1, v4, LX/G96;->b:I

    move v4, p1

    .line 2323045
    iget-object p1, v3, LX/G9P;->a:LX/G96;

    invoke-virtual {v0, p1, v4}, LX/G9R;->a(LX/G96;I)V

    goto :goto_2
.end method
