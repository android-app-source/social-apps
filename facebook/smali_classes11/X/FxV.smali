.class public final LX/FxV;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V
    .locals 0

    .prologue
    .line 2304994
    iput-object p1, p0, LX/FxV;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 12

    .prologue
    .line 2304995
    iget-object v0, p0, LX/FxV;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    .line 2304996
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0815ff

    invoke-virtual {v0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)LX/4BY;

    move-result-object v1

    .line 2304997
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->g:LX/BQ9;

    iget-wide v3, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->F:J

    .line 2304998
    const/4 v9, 0x0

    sget-object v10, LX/9lQ;->SELF:LX/9lQ;

    const-string v11, "fav_photos_edit_save_click"

    move-object v6, v2

    move-wide v7, v3

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2304999
    if-eqz v5, :cond_0

    .line 2305000
    iget-object v6, v2, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2305001
    :cond_0
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->q:LX/Fxh;

    iget-object v3, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    iget-object v4, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->r:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a(I)Ljava/util/List;

    move-result-object v4

    .line 2305002
    new-instance v6, LX/4Ih;

    invoke-direct {v6}, LX/4Ih;-><init>()V

    .line 2305003
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2305004
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;

    .line 2305005
    iget v9, v5, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->a:I

    move v9, v9

    .line 2305006
    if-nez v9, :cond_2

    .line 2305007
    iget-object v9, v5, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->b:Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-object v5, v9

    .line 2305008
    invoke-virtual {v5}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v5

    invoke-interface {v5}, LX/5vo;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2305009
    :cond_2
    iget-object v9, v5, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    move-object v9, v9

    .line 2305010
    iget-object v3, v9, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$FavoriteMediaModel;

    move-object v9, v3

    .line 2305011
    if-nez v9, :cond_1

    .line 2305012
    iget-object v9, v2, LX/Fxh;->c:LX/FxN;

    .line 2305013
    iget-object v3, v5, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoritePhotoVideo;->c:Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;

    move-object v5, v3

    .line 2305014
    invoke-virtual {v9, v5}, LX/FxN;->a(Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;)V

    goto :goto_0

    .line 2305015
    :cond_3
    const-string v5, "photo_ids"

    invoke-virtual {v6, v5, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2305016
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 2305017
    const-string v7, "publish_feed_story"

    invoke-virtual {v6, v7, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2305018
    const-string v5, "collage_layout"

    invoke-virtual {v6, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2305019
    new-instance v5, LX/5yE;

    invoke-direct {v5}, LX/5yE;-><init>()V

    move-object v5, v5

    .line 2305020
    const-string v7, "input"

    invoke-virtual {v5, v7, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2305021
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2305022
    iget-object v6, v2, LX/Fxh;->b:LX/1mR;

    sget-object v7, Lcom/facebook/timeline/header/intro/edit/FavPhotosQueryExecutor;->a:Ljava/util/Set;

    invoke-virtual {v6, v7}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2305023
    iget-object v6, v2, LX/Fxh;->a:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v2, v5

    .line 2305024
    new-instance v3, LX/FxW;

    invoke-direct {v3, v0, v1}, LX/FxW;-><init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;LX/4BY;)V

    iget-object v1, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->f:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2305025
    return-void
.end method
