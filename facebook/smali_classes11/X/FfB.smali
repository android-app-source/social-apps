.class public final LX/FfB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ii;


# instance fields
.field public final synthetic a:LX/Cw8;

.field public final synthetic b:LX/FfG;


# direct methods
.method public constructor <init>(LX/FfG;LX/Cw8;)V
    .locals 0

    .prologue
    .line 2267690
    iput-object p1, p0, LX/FfB;->b:LX/FfG;

    iput-object p2, p0, LX/FfB;->a:LX/Cw8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 2267691
    iget-object v0, p0, LX/FfB;->b:LX/FfG;

    iget-object v0, v0, LX/FfG;->d:LX/FfJ;

    iget-object v1, p0, LX/FfB;->a:LX/Cw8;

    invoke-virtual {v0, v1}, LX/FfJ;->c(LX/Cw8;)LX/0Px;

    move-result-object v0

    .line 2267692
    iget-object v1, p0, LX/FfB;->b:LX/FfG;

    iget-object v1, v1, LX/FfG;->g:Ljava/util/Map;

    iget-object v2, p0, LX/FfB;->a:LX/Cw8;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2267693
    if-eqz v0, :cond_0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    add-int/lit8 v1, p1, -0x1

    if-ltz v1, :cond_0

    .line 2267694
    iget-object v1, p0, LX/FfB;->b:LX/FfG;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/SeeMoreResultPageUnit;

    iget-object v2, p0, LX/FfB;->a:LX/Cw8;

    invoke-static {v1, v0, v2}, LX/FfG;->a$redex0(LX/FfG;Lcom/facebook/search/model/SeeMoreResultPageUnit;LX/Cw8;)V

    .line 2267695
    :cond_0
    :goto_0
    return-void

    .line 2267696
    :cond_1
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2267697
    iget-object v1, p0, LX/FfB;->b:LX/FfG;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/SeeMoreResultPageUnit;

    iget-object v2, p0, LX/FfB;->a:LX/Cw8;

    invoke-static {v1, v0, v2}, LX/FfG;->a$redex0(LX/FfG;Lcom/facebook/search/model/SeeMoreResultPageUnit;LX/Cw8;)V

    goto :goto_0
.end method
