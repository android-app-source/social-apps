.class public LX/Fe8;
.super LX/0hi;
.source ""

# interfaces
.implements LX/0hk;


# instance fields
.field public a:LX/FeD;


# direct methods
.method public constructor <init>(LX/FeD;)V
    .locals 0
    .param p1    # LX/FeD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2265188
    invoke-direct {p0}, LX/0hi;-><init>()V

    .line 2265189
    iput-object p1, p0, LX/Fe8;->a:LX/FeD;

    .line 2265190
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2265191
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fe8;->a:LX/FeD;

    .line 2265192
    invoke-super {p0}, LX/0hi;->a()V

    .line 2265193
    return-void
.end method

.method public final a(LX/Cxk;)V
    .locals 1

    .prologue
    .line 2265194
    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2265195
    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    .line 2265196
    iput-object p1, v0, LX/FeD;->a:LX/Cxk;

    .line 2265197
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2
    .annotation build Lcom/facebook/controllercallbacks/api/SetUp;
    .end annotation

    .annotation build Lcom/facebook/controllercallbacks/api/TraceMethod;
    .end annotation

    .prologue
    .line 2265198
    :try_start_0
    const-string v0, "SearchResultsControllerCallbacksDispatcher.onResume"

    const v1, -0x7ea0b0a1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2265199
    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    invoke-virtual {v0}, LX/0hD;->kw_()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 2265200
    :try_start_1
    const-string v0, "SearchResultsMutationsController"

    const v1, 0x1d13f6b6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2265201
    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    invoke-virtual {v0}, LX/FeD;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2265202
    const v0, 0xe35541c

    :try_start_2
    invoke-static {v0}, LX/02m;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2265203
    :cond_0
    const v0, -0x1368ab31

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2265204
    return-void

    .line 2265205
    :catchall_0
    move-exception v0

    const v1, -0x75af15e

    :try_start_3
    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2265206
    :catchall_1
    move-exception v0

    const v1, 0x61e08413

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final d()V
    .locals 1
    .annotation build Lcom/facebook/controllercallbacks/api/TearDown;
    .end annotation

    .prologue
    .line 2265207
    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    invoke-virtual {v0}, LX/0hD;->kw_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2265208
    iget-object v0, p0, LX/Fe8;->a:LX/FeD;

    invoke-virtual {v0}, LX/FeD;->d()V

    .line 2265209
    :cond_0
    return-void
.end method
