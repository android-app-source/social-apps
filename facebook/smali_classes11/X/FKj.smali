.class public LX/FKj;
.super LX/FKJ;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FKj;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225480
    sget-object v0, LX/6iW;->ARCHIVED:LX/6iW;

    invoke-direct {p0, p1, v0}, LX/FKJ;-><init>(LX/0Or;LX/6iW;)V

    .line 2225481
    return-void
.end method

.method public static a(LX/0QB;)LX/FKj;
    .locals 4

    .prologue
    .line 2225482
    sget-object v0, LX/FKj;->a:LX/FKj;

    if-nez v0, :cond_1

    .line 2225483
    const-class v1, LX/FKj;

    monitor-enter v1

    .line 2225484
    :try_start_0
    sget-object v0, LX/FKj;->a:LX/FKj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2225485
    if-eqz v2, :cond_0

    .line 2225486
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2225487
    new-instance v3, LX/FKj;

    const/16 p0, 0x14ea

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/FKj;-><init>(LX/0Or;)V

    .line 2225488
    move-object v0, v3

    .line 2225489
    sput-object v0, LX/FKj;->a:LX/FKj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2225490
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2225491
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2225492
    :cond_1
    sget-object v0, LX/FKj;->a:LX/FKj;

    return-object v0

    .line 2225493
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2225494
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
