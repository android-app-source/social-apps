.class public LX/FKq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/PostGameScoreParams;",
        "Lcom/facebook/messaging/service/model/PostGameScoreResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225582
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2225583
    check-cast p1, Lcom/facebook/messaging/service/model/PostGameScoreParams;

    .line 2225584
    const-string v0, "%s/game_scores"

    .line 2225585
    iget-object v5, p1, Lcom/facebook/messaging/service/model/PostGameScoreParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v5, :cond_0

    .line 2225586
    iget-object v5, p1, Lcom/facebook/messaging/service/model/PostGameScoreParams;->c:Ljava/lang/String;

    .line 2225587
    :goto_0
    move-object v1, v5

    .line 2225588
    invoke-static {v1}, LX/DoA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2225589
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2225590
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225591
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "game_type"

    .line 2225592
    iget-object v4, p1, Lcom/facebook/messaging/service/model/PostGameScoreParams;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2225593
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225594
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "score"

    .line 2225595
    iget v4, p1, Lcom/facebook/messaging/service/model/PostGameScoreParams;->e:I

    move v4, v4

    .line 2225596
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225597
    new-instance v2, LX/14O;

    invoke-direct {v2}, LX/14O;-><init>()V

    const-string v3, "postGameScore"

    .line 2225598
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2225599
    move-object v2, v2

    .line 2225600
    const-string v3, "POST"

    .line 2225601
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2225602
    move-object v2, v2

    .line 2225603
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2225604
    move-object v0, v2

    .line 2225605
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2225606
    move-object v0, v0

    .line 2225607
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2225608
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225609
    move-object v0, v0

    .line 2225610
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v5, p1, Lcom/facebook/messaging/service/model/PostGameScoreParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2225611
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225612
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2225613
    new-instance v1, Lcom/facebook/messaging/service/model/PostGameScoreResult;

    const-string v2, "success"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0lF;->a(Z)Z

    move-result v0

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/PostGameScoreResult;-><init>(Z)V

    return-object v1
.end method
