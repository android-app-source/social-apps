.class public LX/GX0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZZZLX/0am;LX/0am;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2362947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2362948
    iput-boolean p1, p0, LX/GX0;->a:Z

    .line 2362949
    iput-boolean p2, p0, LX/GX0;->b:Z

    .line 2362950
    iput-boolean p3, p0, LX/GX0;->c:Z

    .line 2362951
    iput-object p4, p0, LX/GX0;->d:LX/0am;

    .line 2362952
    iput-object p5, p0, LX/GX0;->e:LX/0am;

    .line 2362953
    iput-object p6, p0, LX/GX0;->f:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    .line 2362954
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2362956
    if-eqz p1, :cond_0

    instance-of v2, p1, LX/GXM;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2362957
    :cond_1
    :goto_0
    return v0

    .line 2362958
    :cond_2
    if-eq p0, p1, :cond_1

    .line 2362959
    check-cast p1, LX/GX0;

    .line 2362960
    iget-boolean v2, p0, LX/GX0;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2362961
    iget-boolean v3, p1, LX/GX0;->a:Z

    move v3, v3

    .line 2362962
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GX0;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2362963
    iget-boolean v3, p1, LX/GX0;->b:Z

    move v3, v3

    .line 2362964
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/GX0;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2362965
    iget-boolean v3, p1, LX/GX0;->c:Z

    move v3, v3

    .line 2362966
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GX0;->d:LX/0am;

    .line 2362967
    iget-object v3, p1, LX/GX0;->d:LX/0am;

    move-object v3, v3

    .line 2362968
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GX0;->e:LX/0am;

    .line 2362969
    iget-object v3, p1, LX/GX0;->e:LX/0am;

    move-object v3, v3

    .line 2362970
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GX0;->f:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    .line 2362971
    iget-object v3, p1, LX/GX0;->f:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    move-object v3, v3

    .line 2362972
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2362955
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LX/GX0;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/GX0;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/GX0;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/GX0;->d:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/GX0;->e:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/GX0;->f:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
