.class public LX/Gph;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/InstantShoppingScrubbableGIFBlockView;",
        "Lcom/facebook/instantshopping/model/data/InstantShoppingScrubbableGIFBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/GoE;

.field private g:LX/GqW;


# direct methods
.method public constructor <init>(LX/GqW;)V
    .locals 2

    .prologue
    .line 2395530
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395531
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Gph;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object p1

    check-cast p1, LX/Go7;

    invoke-static {v0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v0

    check-cast v0, LX/Go4;

    iput-object p1, p0, LX/Gph;->d:LX/Go7;

    iput-object v0, p0, LX/Gph;->e:LX/Go4;

    .line 2395532
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 8

    .prologue
    .line 2395533
    check-cast p1, LX/GpD;

    .line 2395534
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395535
    check-cast v0, LX/GqW;

    iput-object v0, p0, LX/Gph;->g:LX/GqW;

    .line 2395536
    iget-object v0, p0, LX/Gph;->g:LX/GqW;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 2395537
    iget-object v0, p1, LX/GpD;->a:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->N()LX/8Ys;

    move-result-object v0

    move-object v1, v0

    .line 2395538
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v0

    iput-object v0, p0, LX/Gph;->f:LX/GoE;

    .line 2395539
    if-nez v1, :cond_0

    .line 2395540
    :goto_0
    return-void

    .line 2395541
    :cond_0
    invoke-interface {v1}, LX/8Ys;->r()I

    move-result v4

    .line 2395542
    invoke-interface {v1}, LX/8Ys;->C()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    .line 2395543
    invoke-interface {v1}, LX/8Ys;->D()I

    move-result v0

    int-to-float v0, v0

    invoke-interface {v1}, LX/8Ys;->m()I

    move-result v2

    int-to-float v2, v2

    div-float v3, v0, v2

    .line 2395544
    iget-object v0, p0, LX/Gph;->g:LX/GqW;

    invoke-interface {v1}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v2

    const/4 v5, 0x1

    .line 2395545
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2395546
    iput-object v6, v0, LX/GqW;->d:Landroid/net/Uri;

    .line 2395547
    invoke-static {v2}, LX/CoV;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Cqw;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/Cos;->a(LX/Cqw;)V

    .line 2395548
    const/4 v2, 0x0

    .line 2395549
    iget-object v6, v0, LX/GqW;->d:Landroid/net/Uri;

    if-nez v6, :cond_3

    .line 2395550
    :goto_1
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v6

    if-nez v6, :cond_2

    .line 2395551
    :goto_2
    iget-object v0, p0, LX/Gph;->g:LX/GqW;

    iget-object v1, p0, LX/Gph;->f:LX/GoE;

    .line 2395552
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2395553
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v2, v1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->setLoggingParams(LX/GoE;)V

    .line 2395554
    :cond_1
    goto :goto_0

    .line 2395555
    :cond_2
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v6

    check-cast v6, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    .line 2395556
    iput-boolean v5, v6, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->o:Z

    .line 2395557
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v6

    check-cast v6, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v6, v3}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->setAspectRatio(F)V

    .line 2395558
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v6

    check-cast v6, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v6, v4}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->setDuration(I)V

    .line 2395559
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v6

    check-cast v6, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v7, v0, LX/GqW;->m:LX/GrS;

    .line 2395560
    iput-object v7, v6, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->E:LX/GrS;

    .line 2395561
    goto :goto_2

    .line 2395562
    :cond_3
    new-instance v6, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingScrubbableGIFBlockViewImpl$DownloadTask;

    invoke-direct {v6, v0}, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingScrubbableGIFBlockViewImpl$DownloadTask;-><init>(LX/GqW;)V

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v7

    const/4 p1, 0x1

    new-array p1, p1, [Landroid/net/Uri;

    iget-object v1, v0, LX/GqW;->d:Landroid/net/Uri;

    aput-object v1, p1, v2

    invoke-virtual {v6, v7, p1}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395563
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 2395564
    iget-object v0, p0, LX/Gph;->g:LX/GqW;

    const-string v1, "numberOfTimesInstructionsAnimated"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2395565
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2395566
    invoke-virtual {v0}, LX/Cos;->j()LX/Ct1;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v2, v1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->setNumberOfTimesInstructionsAnimated(I)V

    .line 2395567
    :cond_0
    iget-object v0, p0, LX/Gph;->d:LX/Go7;

    const-string v1, "scrubbablegif_element_start"

    iget-object v2, p0, LX/Gph;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395568
    iget-object v0, p0, LX/Gph;->e:LX/Go4;

    iget-object v1, p0, LX/Gph;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2395569
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395570
    invoke-super {p0, p1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 2395571
    iget-object v0, p0, LX/Gph;->d:LX/Go7;

    const-string v1, "scrubbablegif_element_end"

    iget-object v2, p0, LX/Gph;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395572
    iget-object v0, p0, LX/Gph;->e:LX/Go4;

    iget-object v1, p0, LX/Gph;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2395573
    const-string v0, "numberOfTimesInstructionsAnimated"

    iget-object v1, p0, LX/Gph;->g:LX/GqW;

    .line 2395574
    invoke-virtual {v1}, LX/Cos;->j()LX/Ct1;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2395575
    invoke-virtual {v1}, LX/Cos;->j()LX/Ct1;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getNumberOfTimesInstructionsAnimated()I

    move-result v2

    .line 2395576
    :goto_0
    move v1, v2

    .line 2395577
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2395578
    return-void

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method
