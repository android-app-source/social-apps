.class public final LX/F6v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/F73;


# direct methods
.method public constructor <init>(LX/F73;J)V
    .locals 0

    .prologue
    .line 2200952
    iput-object p1, p0, LX/F6v;->b:LX/F73;

    iput-wide p2, p0, LX/F6v;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v6, -0x1

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x794e2667

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2200953
    iget-object v0, p0, LX/F6v;->b:LX/F73;

    iget-object v0, v0, LX/F73;->g:Ljava/util/Map;

    iget-wide v2, p0, LX/F6v;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2200954
    if-nez v0, :cond_0

    .line 2200955
    const v0, -0x9941516

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2200956
    :goto_0
    return-void

    .line 2200957
    :cond_0
    iget-object v2, p0, LX/F6v;->b:LX/F73;

    iget-object v2, v2, LX/F73;->t:LX/F7X;

    invoke-virtual {v0}, LX/F7L;->a()J

    move-result-wide v4

    .line 2200958
    invoke-static {v2}, LX/F7X;->a(LX/F7X;)V

    .line 2200959
    iget-object v7, v2, LX/F7X;->b:Ljava/util/LinkedList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v7

    .line 2200960
    const/4 v8, -0x1

    if-eq v7, v8, :cond_1

    .line 2200961
    iget-object v8, v2, LX/F7X;->b:Ljava/util/LinkedList;

    invoke-virtual {v8, v7}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 2200962
    iget-object v8, v2, LX/F7X;->c:Ljava/util/LinkedList;

    invoke-virtual {v8, v7}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 2200963
    :cond_1
    iget-object v7, v2, LX/F7X;->b:Ljava/util/LinkedList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2200964
    iget-object v7, v2, LX/F7X;->c:Ljava/util/LinkedList;

    iget-object v8, v2, LX/F7X;->a:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2200965
    iget-object v2, p0, LX/F6v;->b:LX/F73;

    invoke-virtual {v0}, LX/F7L;->a()J

    move-result-wide v4

    .line 2200966
    const/4 v7, 0x0

    move v8, v7

    :goto_1
    iget-object v7, v2, LX/F73;->e:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v8, v7, :cond_5

    .line 2200967
    iget-object v7, v2, LX/F73;->e:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/F7L;

    invoke-virtual {v7}, LX/F7L;->a()J

    move-result-wide v9

    cmp-long v7, v9, v4

    if-nez v7, :cond_4

    .line 2200968
    :goto_2
    move v2, v8

    .line 2200969
    if-eq v2, v6, :cond_2

    .line 2200970
    iget-object v3, p0, LX/F6v;->b:LX/F73;

    iget-object v3, v3, LX/F73;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2200971
    :cond_2
    iget-object v2, p0, LX/F6v;->b:LX/F73;

    invoke-virtual {v0}, LX/F7L;->a()J

    move-result-wide v4

    .line 2200972
    const/4 v7, 0x0

    move v8, v7

    :goto_3
    iget-object v7, v2, LX/F73;->k:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v8, v7, :cond_7

    .line 2200973
    iget-object v7, v2, LX/F73;->k:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/F7L;

    invoke-virtual {v7}, LX/F7L;->a()J

    move-result-wide v9

    cmp-long v7, v9, v4

    if-nez v7, :cond_6

    .line 2200974
    :goto_4
    move v0, v8

    .line 2200975
    if-eq v0, v6, :cond_3

    .line 2200976
    iget-object v2, p0, LX/F6v;->b:LX/F73;

    iget-object v2, v2, LX/F73;->k:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2200977
    :cond_3
    iget-object v0, p0, LX/F6v;->b:LX/F73;

    const v2, 0x6d5177ba

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2200978
    const v0, 0x8494500

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2200979
    :cond_4
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_1

    .line 2200980
    :cond_5
    const/4 v8, -0x1

    goto :goto_2

    .line 2200981
    :cond_6
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_3

    .line 2200982
    :cond_7
    const/4 v8, -0x1

    goto :goto_4
.end method
