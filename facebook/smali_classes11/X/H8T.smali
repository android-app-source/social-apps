.class public final LX/H8T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/H8U;


# direct methods
.method public constructor <init>(LX/H8U;)V
    .locals 0

    .prologue
    .line 2433409
    iput-object p1, p0, LX/H8T;->a:LX/H8U;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2433410
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2433411
    iget-object v0, p0, LX/H8T;->a:LX/H8U;

    iget-object v0, v0, LX/H8U;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00H;

    .line 2433412
    iget-object v1, v0, LX/00H;->h:Ljava/lang/String;

    move-object v0, v1

    .line 2433413
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2433414
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2433415
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2433416
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2433417
    iget-object v0, p0, LX/H8T;->a:LX/H8U;

    iget-object v0, v0, LX/H8U;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/H8T;->a:LX/H8U;

    iget-object v2, v2, LX/H8U;->f:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2433418
    :cond_0
    return-void
.end method
