.class public LX/FRF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wC;


# instance fields
.field private a:LX/712;


# direct methods
.method public constructor <init>(LX/712;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240205
    iput-object p1, p0, LX/FRF;->a:LX/712;

    .line 2240206
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2240207
    sget-object v0, LX/FRE;->a:[I

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v1

    invoke-virtual {v1}, LX/71I;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2240208
    iget-object v0, p0, LX/FRF;->a:LX/712;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/712;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2240209
    :pswitch_0
    check-cast p2, LX/FRD;

    .line 2240210
    if-nez p3, :cond_0

    new-instance p3, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;-><init>(Landroid/content/Context;)V

    .line 2240211
    :goto_1
    iput-object p1, p3, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 2240212
    invoke-virtual {p3, p2}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->a(LX/FRD;)V

    .line 2240213
    move-object v0, p3

    .line 2240214
    goto :goto_0

    .line 2240215
    :cond_0
    check-cast p3, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
