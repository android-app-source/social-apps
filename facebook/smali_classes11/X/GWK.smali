.class public final LX/GWK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2361485
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 2361486
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2361487
    iget-object v1, p0, LX/GWK;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2361488
    iget-object v3, p0, LX/GWK;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2361489
    iget-object v5, p0, LX/GWK;->i:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2361490
    iget-object v6, p0, LX/GWK;->j:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2361491
    iget-object v7, p0, LX/GWK;->k:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2361492
    const/16 v8, 0xb

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2361493
    iget-boolean v8, p0, LX/GWK;->a:Z

    invoke-virtual {v0, v10, v8}, LX/186;->a(IZ)V

    .line 2361494
    iget-boolean v8, p0, LX/GWK;->b:Z

    invoke-virtual {v0, v4, v8}, LX/186;->a(IZ)V

    .line 2361495
    const/4 v8, 0x2

    iget-boolean v9, p0, LX/GWK;->c:Z

    invoke-virtual {v0, v8, v9}, LX/186;->a(IZ)V

    .line 2361496
    const/4 v8, 0x3

    iget-boolean v9, p0, LX/GWK;->d:Z

    invoke-virtual {v0, v8, v9}, LX/186;->a(IZ)V

    .line 2361497
    const/4 v8, 0x4

    iget-boolean v9, p0, LX/GWK;->e:Z

    invoke-virtual {v0, v8, v9}, LX/186;->a(IZ)V

    .line 2361498
    const/4 v8, 0x5

    iget-boolean v9, p0, LX/GWK;->f:Z

    invoke-virtual {v0, v8, v9}, LX/186;->a(IZ)V

    .line 2361499
    const/4 v8, 0x6

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 2361500
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2361501
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2361502
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2361503
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2361504
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2361505
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2361506
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2361507
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2361508
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2361509
    new-instance v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    invoke-direct {v1, v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;-><init>(LX/15i;)V

    .line 2361510
    return-object v1
.end method
