.class public LX/Ge0;
.super LX/25J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25J",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/Ge0;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0tX;

.field public final e:Ljava/util/concurrent/Executor;

.field public final f:LX/0Zb;

.field private final g:LX/17Q;

.field public final h:LX/0bH;

.field public final i:LX/189;

.field private j:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/99Z;

.field private final l:Ljava/util/concurrent/ScheduledExecutorService;

.field public final m:LX/0ad;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Zb;LX/17Q;LX/189;LX/0bH;LX/1Ck;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374906
    invoke-direct {p0}, LX/25J;-><init>()V

    .line 2374907
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Ge0;->a:Ljava/util/Set;

    .line 2374908
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Ge0;->b:Ljava/util/Set;

    .line 2374909
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Ge0;->c:Ljava/util/Set;

    .line 2374910
    iput-object p1, p0, LX/Ge0;->d:LX/0tX;

    .line 2374911
    iput-object p2, p0, LX/Ge0;->e:Ljava/util/concurrent/Executor;

    .line 2374912
    iput-object p3, p0, LX/Ge0;->f:LX/0Zb;

    .line 2374913
    iput-object p4, p0, LX/Ge0;->g:LX/17Q;

    .line 2374914
    iput-object p5, p0, LX/Ge0;->i:LX/189;

    .line 2374915
    iput-object p6, p0, LX/Ge0;->h:LX/0bH;

    .line 2374916
    iput-object p7, p0, LX/Ge0;->j:LX/1Ck;

    .line 2374917
    new-instance v0, LX/99Z;

    sget-object v1, LX/Ge5;->a:LX/0Tn;

    invoke-direct {v0, p8, v1}, LX/99Z;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V

    iput-object v0, p0, LX/Ge0;->k:LX/99Z;

    .line 2374918
    iput-object p9, p0, LX/Ge0;->l:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2374919
    iput-object p10, p0, LX/Ge0;->m:LX/0ad;

    .line 2374920
    return-void
.end method

.method public static a(LX/0QB;)LX/Ge0;
    .locals 14

    .prologue
    .line 2374921
    sget-object v0, LX/Ge0;->n:LX/Ge0;

    if-nez v0, :cond_1

    .line 2374922
    const-class v1, LX/Ge0;

    monitor-enter v1

    .line 2374923
    :try_start_0
    sget-object v0, LX/Ge0;->n:LX/Ge0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2374924
    if-eqz v2, :cond_0

    .line 2374925
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2374926
    new-instance v3, LX/Ge0;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v7

    check-cast v7, LX/17Q;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v8

    check-cast v8, LX/189;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v9

    check-cast v9, LX/0bH;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v3 .. v13}, LX/Ge0;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Zb;LX/17Q;LX/189;LX/0bH;LX/1Ck;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ScheduledExecutorService;LX/0ad;)V

    .line 2374927
    move-object v0, v3

    .line 2374928
    sput-object v0, LX/Ge0;->n:LX/Ge0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2374929
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2374930
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2374931
    :cond_1
    sget-object v0, LX/Ge0;->n:LX/Ge0;

    return-object v0

    .line 2374932
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2374933
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z
    .locals 4

    .prologue
    .line 2374934
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 2374935
    iget-object v0, p0, LX/Ge0;->c:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p0, LX/Ge0;->m:LX/0ad;

    sget v2, LX/9Is;->a:I

    const/16 v3, 0x19

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z
    .locals 4

    .prologue
    .line 2374936
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    const/4 v0, 0x0

    .line 2374937
    iget-object v1, p0, LX/Ge0;->m:LX/0ad;

    sget v2, LX/9Is;->c:I

    const/16 v3, 0x8

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    if-gtz v1, :cond_1

    .line 2374938
    :cond_0
    :goto_0
    return v0

    .line 2374939
    :cond_1
    iget-object v1, p0, LX/Ge0;->m:LX/0ad;

    sget v2, LX/9Is;->f:I

    const/4 v3, 0x5

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    .line 2374940
    iget-object v2, p0, LX/Ge0;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    add-int/2addr v1, p2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->u()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 4

    .prologue
    .line 2374941
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 2374942
    iget-object v0, p0, LX/Ge0;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2374943
    iget-object v0, p0, LX/Ge0;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2374944
    iget-object v0, p0, LX/Ge0;->j:LX/1Ck;

    const/4 v1, 0x0

    new-instance v2, LX/Gdx;

    invoke-direct {v2, p0, p1}, LX/Gdx;-><init>(LX/Ge0;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)V

    new-instance v3, LX/Gdy;

    invoke-direct {v3, p0, p1}, LX/Gdy;-><init>(LX/Ge0;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2374945
    return-void
.end method
