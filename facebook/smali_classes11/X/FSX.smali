.class public LX/FSX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/FSB;

.field private final b:LX/0Uq;


# direct methods
.method public constructor <init>(LX/FSB;LX/0Uq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2242076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2242077
    iput-object p1, p0, LX/FSX;->a:LX/FSB;

    .line 2242078
    iput-object p2, p0, LX/FSX;->b:LX/0Uq;

    .line 2242079
    return-void
.end method

.method public static b(LX/0QB;)LX/FSX;
    .locals 3

    .prologue
    .line 2242080
    new-instance v2, LX/FSX;

    invoke-static {p0}, LX/FSC;->a(LX/0QB;)LX/FSC;

    move-result-object v0

    check-cast v0, LX/FSB;

    invoke-static {p0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v1

    check-cast v1, LX/0Uq;

    invoke-direct {v2, v0, v1}, LX/FSX;-><init>(LX/FSB;LX/0Uq;)V

    .line 2242081
    return-object v2
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2242082
    iget-object v0, p0, LX/FSX;->b:LX/0Uq;

    invoke-virtual {v0}, LX/0Uq;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2242083
    iget-object v0, p0, LX/FSX;->a:LX/FSB;

    invoke-interface {v0}, LX/FSB;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2242084
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/FSV;

    const-string v1, "App is not initialized"

    invoke-direct {v0, v1}, LX/FSV;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
