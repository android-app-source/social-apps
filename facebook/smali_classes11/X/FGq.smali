.class public final LX/FGq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
        "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Landroid/net/Uri;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 0

    .prologue
    .line 2219575
    iput-object p1, p0, LX/FGq;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FGq;->a:Landroid/net/Uri;

    iput-object p3, p0, LX/FGq;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 2219576
    check-cast p1, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 2219577
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/FGq;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2219578
    iget-object v1, p0, LX/FGq;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->F:Landroid/content/Context;

    iget-object v2, p0, LX/FGq;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2219579
    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2219580
    invoke-static {v1, v2, v3}, LX/6bn;->a(Landroid/content/Context;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 2219581
    invoke-static {v0, v1}, LX/1t3;->c(Ljava/io/File;Ljava/io/File;)V

    .line 2219582
    invoke-static {p1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
