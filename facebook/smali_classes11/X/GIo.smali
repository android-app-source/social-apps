.class public final LX/GIo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GIr;


# direct methods
.method public constructor <init>(LX/GIr;)V
    .locals 0

    .prologue
    .line 2336994
    iput-object p1, p0, LX/GIo;->a:LX/GIr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, 0x16d2b8ba

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2336995
    iget-object v0, p0, LX/GIo;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v4, v0

    check-cast v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336996
    :try_start_0
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2336997
    iget-object v0, p0, LX/GIo;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->h:LX/GG6;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2336998
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    move-object v1, v2

    .line 2336999
    invoke-virtual {v0, v1}, LX/GG6;->c(LX/0Px;)Ljava/lang/String;

    move-result-object v6

    .line 2337000
    if-nez v6, :cond_0

    .line 2337001
    iget-object v0, p0, LX/GIo;->a:LX/GIr;

    .line 2337002
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2337003
    new-instance v1, LX/GFP;

    iget-object v2, p0, LX/GIo;->a:LX/GIr;

    iget-object v2, v2, LX/GIr;->f:LX/GIa;

    invoke-virtual {v2}, LX/GIa;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080a9c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GFP;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2337004
    const v0, -0x71c32096

    invoke-static {v0, v7}, LX/02F;->a(II)V

    .line 2337005
    :goto_0
    return-void

    .line 2337006
    :catch_0
    move-exception v0

    .line 2337007
    iget-object v1, p0, LX/GIo;->a:LX/GIr;

    iget-object v1, v1, LX/GIr;->m:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "Catch UnsupportedEncodingException: "

    invoke-virtual {v1, v2, v3, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2337008
    const v0, -0x14141b36

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_0

    .line 2337009
    :cond_0
    iget-object v0, p0, LX/GIo;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->l:LX/GKy;

    iget-object v1, p0, LX/GIo;->a:LX/GIr;

    iget-object v1, v1, LX/GIr;->f:LX/GIa;

    invoke-virtual {v1}, LX/GIa;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v4

    invoke-virtual {v4}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v6}, LX/GKy;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2337010
    if-nez v0, :cond_1

    .line 2337011
    iget-object v0, p0, LX/GIo;->a:LX/GIr;

    .line 2337012
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2337013
    new-instance v1, LX/GFP;

    iget-object v2, p0, LX/GIo;->a:LX/GIr;

    iget-object v2, v2, LX/GIr;->f:LX/GIa;

    invoke-virtual {v2}, LX/GIa;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080a9c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GFP;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2337014
    const v0, 0x6fbcaa44

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_0

    .line 2337015
    :cond_1
    iget-object v1, p0, LX/GIo;->a:LX/GIr;

    .line 2337016
    iget-object v2, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v2

    .line 2337017
    new-instance v2, LX/GFS;

    invoke-direct {v2, v0, v8}, LX/GFS;-><init>(Landroid/content/Intent;I)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2337018
    const v0, 0x5db5c55e

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_0
.end method
