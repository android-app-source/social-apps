.class public LX/FMd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FMd;


# instance fields
.field private a:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2230150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2230151
    invoke-static {}, LX/1Ei;->a()LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/FMd;->a:LX/0Ri;

    .line 2230152
    return-void
.end method

.method public static a(LX/0QB;)LX/FMd;
    .locals 3

    .prologue
    .line 2230138
    sget-object v0, LX/FMd;->b:LX/FMd;

    if-nez v0, :cond_1

    .line 2230139
    const-class v1, LX/FMd;

    monitor-enter v1

    .line 2230140
    :try_start_0
    sget-object v0, LX/FMd;->b:LX/FMd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2230141
    if-eqz v2, :cond_0

    .line 2230142
    :try_start_1
    new-instance v0, LX/FMd;

    invoke-direct {v0}, LX/FMd;-><init>()V

    .line 2230143
    move-object v0, v0

    .line 2230144
    sput-object v0, LX/FMd;->b:LX/FMd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2230145
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2230146
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2230147
    :cond_1
    sget-object v0, LX/FMd;->b:LX/FMd;

    return-object v0

    .line 2230148
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2230149
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2230137
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FMd;->a:LX/0Ri;

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2230153
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FMd;->a:LX/0Ri;

    invoke-interface {v0, p1, p2}, LX/0Ri;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2230154
    monitor-exit p0

    return-void

    .line 2230155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2230136
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FMd;->a:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, p1}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2230133
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FMd;->a:LX/0Ri;

    invoke-interface {v0, p1}, LX/0Ri;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2230134
    monitor-exit p0

    return-void

    .line 2230135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
