.class public final enum LX/FCH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FCH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FCH;

.field public static final enum CLEAR_ALL:LX/FCH;

.field public static final enum CLEAR_DISK:LX/FCH;

.field public static final enum CLEAR_MEMORY:LX/FCH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2210171
    new-instance v0, LX/FCH;

    const-string v1, "CLEAR_MEMORY"

    invoke-direct {v0, v1, v2}, LX/FCH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCH;->CLEAR_MEMORY:LX/FCH;

    .line 2210172
    new-instance v0, LX/FCH;

    const-string v1, "CLEAR_DISK"

    invoke-direct {v0, v1, v3}, LX/FCH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCH;->CLEAR_DISK:LX/FCH;

    .line 2210173
    new-instance v0, LX/FCH;

    const-string v1, "CLEAR_ALL"

    invoke-direct {v0, v1, v4}, LX/FCH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCH;->CLEAR_ALL:LX/FCH;

    .line 2210174
    const/4 v0, 0x3

    new-array v0, v0, [LX/FCH;

    sget-object v1, LX/FCH;->CLEAR_MEMORY:LX/FCH;

    aput-object v1, v0, v2

    sget-object v1, LX/FCH;->CLEAR_DISK:LX/FCH;

    aput-object v1, v0, v3

    sget-object v1, LX/FCH;->CLEAR_ALL:LX/FCH;

    aput-object v1, v0, v4

    sput-object v0, LX/FCH;->$VALUES:[LX/FCH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2210175
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FCH;
    .locals 1

    .prologue
    .line 2210176
    const-class v0, LX/FCH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FCH;

    return-object v0
.end method

.method public static values()[LX/FCH;
    .locals 1

    .prologue
    .line 2210177
    sget-object v0, LX/FCH;->$VALUES:[LX/FCH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FCH;

    return-object v0
.end method
