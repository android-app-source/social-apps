.class public final enum LX/GCm;
.super LX/GCi;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;III)V
    .locals 6

    .prologue
    .line 2329186
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v4}, LX/GCi;-><init>(Ljava/lang/String;III)V

    return-void
.end method


# virtual methods
.method public final getUri(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2329187
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tel:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2329188
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    move-object v1, v1

    .line 2329189
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final isAvailable(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z
    .locals 2

    .prologue
    .line 2329190
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    move-object v0, v0

    .line 2329191
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
