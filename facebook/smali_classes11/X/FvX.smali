.class public LX/FvX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FvX;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/BPp;


# direct methods
.method public constructor <init>(LX/0ad;LX/BPp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2301643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2301644
    iput-object p1, p0, LX/FvX;->a:LX/0ad;

    .line 2301645
    iput-object p2, p0, LX/FvX;->b:LX/BPp;

    .line 2301646
    return-void
.end method

.method public static a(LX/0QB;)LX/FvX;
    .locals 5

    .prologue
    .line 2301647
    sget-object v0, LX/FvX;->c:LX/FvX;

    if-nez v0, :cond_1

    .line 2301648
    const-class v1, LX/FvX;

    monitor-enter v1

    .line 2301649
    :try_start_0
    sget-object v0, LX/FvX;->c:LX/FvX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2301650
    if-eqz v2, :cond_0

    .line 2301651
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2301652
    new-instance p0, LX/FvX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/BPp;->a(LX/0QB;)LX/BPp;

    move-result-object v4

    check-cast v4, LX/BPp;

    invoke-direct {p0, v3, v4}, LX/FvX;-><init>(LX/0ad;LX/BPp;)V

    .line 2301653
    move-object v0, p0

    .line 2301654
    sput-object v0, LX/FvX;->c:LX/FvX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2301655
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2301656
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2301657
    :cond_1
    sget-object v0, LX/FvX;->c:LX/FvX;

    return-object v0

    .line 2301658
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2301659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/5SB;LX/BQ1;)V
    .locals 1

    .prologue
    .line 2301660
    instance-of v0, p0, LX/BP0;

    if-eqz v0, :cond_0

    .line 2301661
    check-cast p0, LX/BP0;

    invoke-virtual {p1}, LX/BQ1;->W()Z

    move-result v0

    .line 2301662
    iput-boolean v0, p0, LX/BP0;->d:Z

    .line 2301663
    :cond_0
    return-void
.end method
