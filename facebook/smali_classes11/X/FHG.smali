.class public final enum LX/FHG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHG;

.field public static final enum compression:LX/FHG;

.field public static final enum dedup:LX/FHG;

.field public static final enum upload:LX/FHG;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2220123
    new-instance v0, LX/FHG;

    const-string v1, "compression"

    invoke-direct {v0, v1, v2}, LX/FHG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHG;->compression:LX/FHG;

    new-instance v0, LX/FHG;

    const-string v1, "upload"

    invoke-direct {v0, v1, v3}, LX/FHG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHG;->upload:LX/FHG;

    new-instance v0, LX/FHG;

    const-string v1, "dedup"

    invoke-direct {v0, v1, v4}, LX/FHG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHG;->dedup:LX/FHG;

    .line 2220124
    const/4 v0, 0x3

    new-array v0, v0, [LX/FHG;

    sget-object v1, LX/FHG;->compression:LX/FHG;

    aput-object v1, v0, v2

    sget-object v1, LX/FHG;->upload:LX/FHG;

    aput-object v1, v0, v3

    sget-object v1, LX/FHG;->dedup:LX/FHG;

    aput-object v1, v0, v4

    sput-object v0, LX/FHG;->$VALUES:[LX/FHG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2220126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHG;
    .locals 1

    .prologue
    .line 2220127
    const-class v0, LX/FHG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHG;

    return-object v0
.end method

.method public static values()[LX/FHG;
    .locals 1

    .prologue
    .line 2220125
    sget-object v0, LX/FHG;->$VALUES:[LX/FHG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHG;

    return-object v0
.end method
