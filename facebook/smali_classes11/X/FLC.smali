.class public LX/FLC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/FLC;


# instance fields
.field public final a:LX/18V;

.field private final b:LX/FKj;

.field private final c:LX/FKm;

.field private final d:LX/FKn;

.field public final e:LX/3QL;

.field public final f:LX/FKZ;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Ux;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/18V;LX/FKj;LX/FKm;LX/FKn;LX/3QL;LX/FKZ;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/FKj;",
            "LX/FKm;",
            "LX/FKn;",
            "LX/3QL;",
            "Lcom/facebook/messaging/service/methods/MqttMarkThreadHandler;",
            "LX/0Or",
            "<",
            "LX/7Ux;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2226173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226174
    iput-object p1, p0, LX/FLC;->a:LX/18V;

    .line 2226175
    iput-object p2, p0, LX/FLC;->b:LX/FKj;

    .line 2226176
    iput-object p3, p0, LX/FLC;->c:LX/FKm;

    .line 2226177
    iput-object p4, p0, LX/FLC;->d:LX/FKn;

    .line 2226178
    iput-object p5, p0, LX/FLC;->e:LX/3QL;

    .line 2226179
    iput-object p6, p0, LX/FLC;->f:LX/FKZ;

    .line 2226180
    iput-object p7, p0, LX/FLC;->g:LX/0Or;

    .line 2226181
    return-void
.end method

.method public static a(LX/FLC;LX/6iW;)LX/FKJ;
    .locals 1

    .prologue
    .line 2226182
    sget-object v0, LX/6iW;->ARCHIVED:LX/6iW;

    if-ne p1, v0, :cond_0

    .line 2226183
    iget-object v0, p0, LX/FLC;->b:LX/FKj;

    .line 2226184
    :goto_0
    return-object v0

    .line 2226185
    :cond_0
    sget-object v0, LX/6iW;->READ:LX/6iW;

    if-ne p1, v0, :cond_1

    .line 2226186
    iget-object v0, p0, LX/FLC;->c:LX/FKm;

    goto :goto_0

    .line 2226187
    :cond_1
    sget-object v0, LX/6iW;->SPAM:LX/6iW;

    if-ne p1, v0, :cond_2

    .line 2226188
    iget-object v0, p0, LX/FLC;->d:LX/FKn;

    goto :goto_0

    .line 2226189
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public static a(LX/0QB;)LX/FLC;
    .locals 14

    .prologue
    .line 2226190
    sget-object v0, LX/FLC;->h:LX/FLC;

    if-nez v0, :cond_1

    .line 2226191
    const-class v1, LX/FLC;

    monitor-enter v1

    .line 2226192
    :try_start_0
    sget-object v0, LX/FLC;->h:LX/FLC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2226193
    if-eqz v2, :cond_0

    .line 2226194
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2226195
    new-instance v3, LX/FLC;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {v0}, LX/FKj;->a(LX/0QB;)LX/FKj;

    move-result-object v5

    check-cast v5, LX/FKj;

    .line 2226196
    new-instance v6, LX/FKm;

    const/16 v7, 0x14ea

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct {v6, v7}, LX/FKm;-><init>(LX/0Or;)V

    .line 2226197
    move-object v6, v6

    .line 2226198
    check-cast v6, LX/FKm;

    invoke-static {v0}, LX/FKn;->a(LX/0QB;)LX/FKn;

    move-result-object v7

    check-cast v7, LX/FKn;

    invoke-static {v0}, LX/3QL;->b(LX/0QB;)LX/3QL;

    move-result-object v8

    check-cast v8, LX/3QL;

    .line 2226199
    new-instance v12, LX/FKZ;

    invoke-static {v0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v9

    check-cast v9, LX/2Hu;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v10

    check-cast v10, LX/0So;

    invoke-static {v0}, LX/2Hv;->a(LX/0QB;)LX/2Hv;

    move-result-object v11

    check-cast v11, LX/2Hv;

    const/16 v13, 0x14ea

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct {v12, v9, v10, v11, v13}, LX/FKZ;-><init>(LX/2Hu;LX/0So;LX/2Hv;LX/0Or;)V

    .line 2226200
    move-object v9, v12

    .line 2226201
    check-cast v9, LX/FKZ;

    const/16 v10, 0x38d9

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/FLC;-><init>(LX/18V;LX/FKj;LX/FKm;LX/FKn;LX/3QL;LX/FKZ;LX/0Or;)V

    .line 2226202
    move-object v0, v3

    .line 2226203
    sput-object v0, LX/FLC;->h:LX/FLC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2226204
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2226205
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2226206
    :cond_1
    sget-object v0, LX/FLC;->h:LX/FLC;

    return-object v0

    .line 2226207
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2226208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
