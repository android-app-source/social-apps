.class public LX/FQL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2239005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239006
    const/4 v0, 0x0

    iput v0, p0, LX/FQL;->a:I

    .line 2239007
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Z)LX/FQL;
    .locals 6

    .prologue
    .line 2239008
    new-instance v0, LX/FQL;

    invoke-direct {v0}, LX/FQL;-><init>()V

    .line 2239009
    if-nez p0, :cond_1

    .line 2239010
    :cond_0
    :goto_0
    return-object v0

    .line 2239011
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->n()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;

    move-result-object v1

    .line 2239012
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPagePlaceInfoFragmentModel$OverallStarRatingModel;->c()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_2

    .line 2239013
    iget v1, v0, LX/FQL;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/FQL;->a:I

    .line 2239014
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->p()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    .line 2239015
    iget v1, v0, LX/FQL;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, LX/FQL;->a:I

    .line 2239016
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->w()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2239017
    iget v1, v0, LX/FQL;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, LX/FQL;->a:I

    .line 2239018
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->q()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2239019
    iget v1, v0, LX/FQL;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, LX/FQL;->a:I

    .line 2239020
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->j()I

    move-result v1

    if-lez v1, :cond_0

    .line 2239021
    iget v1, v0, LX/FQL;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, LX/FQL;->a:I

    goto :goto_0
.end method
