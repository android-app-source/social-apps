.class public final LX/GhI;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionSurfacesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/gametime/ui/GametimeActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/GametimeActivity;)V
    .locals 0

    .prologue
    .line 2383823
    iput-object p1, p0, LX/GhI;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionSurfacesQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v13, 0x6

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 2383825
    iget-object v0, p0, LX/GhI;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2383826
    iput-object v1, v0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    .line 2383827
    if-eqz p1, :cond_2

    .line 2383828
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2383829
    if-eqz v0, :cond_2

    .line 2383830
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2383831
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionSurfacesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeReactionSurfacesQueryModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v8, v0, LX/1vs;->b:I

    .line 2383832
    invoke-virtual {v6, v8, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v6, v8, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2383833
    invoke-virtual {v6, v8, v12}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_1

    .line 2383834
    invoke-virtual {v6, v8, v12}, LX/15i;->g(II)I

    move-result v0

    .line 2383835
    invoke-virtual {v6, v0, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 2383836
    :goto_1
    iget-object v0, p0, LX/GhI;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    iget-object v9, v0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    new-instance v0, LX/GhT;

    sget-object v1, LX/GiR;->TAIL_LOAD:LX/GiR;

    invoke-virtual {v6, v8, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v8, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x3

    invoke-virtual {v6, v8, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x2

    invoke-virtual {v6, v8, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/GhT;-><init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2383837
    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 2383838
    :cond_2
    iget-object v0, p0, LX/GhI;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    iget-object v0, v0, Lcom/facebook/gametime/ui/GametimeActivity;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2383839
    iget-object v0, p0, LX/GhI;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    invoke-static {v0}, Lcom/facebook/gametime/ui/GametimeActivity;->p(Lcom/facebook/gametime/ui/GametimeActivity;)V

    .line 2383840
    :cond_3
    iget-object v0, p0, LX/GhI;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    invoke-static {v0}, Lcom/facebook/gametime/ui/GametimeActivity;->q(Lcom/facebook/gametime/ui/GametimeActivity;)V

    .line 2383841
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2383842
    iget-object v0, p0, LX/GhI;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    invoke-static {v0}, Lcom/facebook/gametime/ui/GametimeActivity;->p(Lcom/facebook/gametime/ui/GametimeActivity;)V

    .line 2383843
    iget-object v0, p0, LX/GhI;->a:Lcom/facebook/gametime/ui/GametimeActivity;

    invoke-static {v0}, Lcom/facebook/gametime/ui/GametimeActivity;->q(Lcom/facebook/gametime/ui/GametimeActivity;)V

    .line 2383844
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2383824
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/GhI;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
