.class public LX/G0z;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2310594
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/G4m;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2310595
    invoke-virtual {p0}, LX/BPB;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2310596
    invoke-virtual {p0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;

    .line 2310597
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLModels$TimelineTaggedMediaSetFieldsModel$MediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2310598
    :goto_0
    const-string v1, "has_tagged_mediaset"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2310599
    :goto_1
    return-void

    .line 2310600
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2310601
    :cond_1
    const-string v0, "has_tagged_mediaset"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2310602
    const-string v0, "land_on_uploads_tab"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1
.end method
