.class public final LX/Ffu;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$FetchLiveFeedStoriesGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ffv;


# direct methods
.method public constructor <init>(LX/Ffv;)V
    .locals 0

    .prologue
    .line 2269140
    iput-object p1, p0, LX/Ffu;->a:LX/Ffv;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2269131
    iget-object v0, p0, LX/Ffu;->a:LX/Ffv;

    iget-object v0, v0, LX/Ffv;->c:LX/Fee;

    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->LIVE_CONVERSATION_FETCH_FAIL:LX/3Ql;

    invoke-direct {v1, v2, p1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, LX/Fee;->a(LX/7C4;)V

    .line 2269132
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2269133
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2269134
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2269135
    check-cast v0, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$FetchLiveFeedStoriesGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$FetchLiveFeedStoriesGraphQLModel;->a()Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;

    move-result-object v5

    .line 2269136
    if-nez v5, :cond_0

    .line 2269137
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "filtered_query was null"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/Ffu;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2269138
    :goto_0
    return-void

    .line 2269139
    :cond_0
    iget-object v0, p0, LX/Ffu;->a:LX/Ffv;

    iget-object v0, v0, LX/Ffv;->c:LX/Fee;

    invoke-static {v5}, LX/Ffv;->d(Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;)LX/0Px;

    move-result-object v1

    invoke-static {v5}, LX/Ffv;->c(Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;)LX/0us;

    move-result-object v2

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$LiveFeedQueryModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {v0 .. v5}, LX/Fee;->b(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
