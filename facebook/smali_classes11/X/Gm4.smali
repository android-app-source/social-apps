.class public final LX/Gm4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Gm7;


# direct methods
.method public constructor <init>(LX/Gm7;)V
    .locals 0

    .prologue
    .line 2391557
    iput-object p1, p0, LX/Gm4;->a:LX/Gm7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x7bff7ec7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2391558
    iget-object v1, p0, LX/Gm4;->a:LX/Gm7;

    iget-object v1, v1, LX/Gm7;->d:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    if-eqz v1, :cond_2

    .line 2391559
    iget-object v1, p0, LX/Gm4;->a:LX/Gm7;

    iget-object v1, v1, LX/Gm7;->d:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    iget-object v2, p0, LX/Gm4;->a:LX/Gm7;

    iget-object v2, v2, LX/Gm7;->c:LX/GmD;

    .line 2391560
    sget-object v4, LX/GmC;->UNINVITED:LX/GmC;

    iput-object v4, v2, LX/GmD;->c:LX/GmC;

    .line 2391561
    iget-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->o:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    .line 2391562
    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2391563
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GmD;

    .line 2391564
    iget-object v4, v4, LX/GmD;->b:Ljava/lang/String;

    iget-object p1, v2, LX/GmD;->b:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2391565
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2391566
    :cond_1
    iget-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a:LX/Glv;

    invoke-virtual {v4}, LX/1OM;->notifyDataSetChanged()V

    .line 2391567
    sget-object v4, LX/GmG;->UNDO:LX/GmG;

    iget-object p0, v2, LX/GmD;->b:Ljava/lang/String;

    invoke-static {v1, v4, p0}, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a(Lcom/facebook/growth/contactinviter/ContactInviterFragment;LX/GmG;Ljava/lang/String;)V

    .line 2391568
    iget-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->g:LX/0if;

    sget-object p0, LX/0ig;->X:LX/0ih;

    const-string p1, "contact_invite_list_item_undo_button_click"

    invoke-virtual {v4, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2391569
    :cond_2
    const v1, -0x5189b3a7

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
