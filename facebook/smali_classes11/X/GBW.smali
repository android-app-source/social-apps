.class public final LX/GBW;
.super LX/79T;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 0

    .prologue
    .line 2327030
    iput-object p1, p0, LX/GBW;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-direct {p0}, LX/79T;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2327017
    iget-object v0, p0, LX/GBW;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    .line 2327018
    iget-wide v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->R:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 2327019
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->h:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->R:J

    .line 2327020
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->d:LX/2Ne;

    iget-object v2, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    .line 2327021
    iget-object v3, v1, LX/2Ne;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/GB4;->SMS_CODE_SEARCHED:LX/GB4;

    invoke-virtual {v5}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "account_recovery"

    .line 2327022
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2327023
    move-object v4, v4

    .line 2327024
    const-string v5, "crypted_id"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2327025
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->M:Z

    .line 2327026
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->e:LX/GAz;

    .line 2327027
    iput-object v0, v1, LX/GAz;->e:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    .line 2327028
    invoke-static {v1}, LX/GAz;->d(LX/GAz;)V

    .line 2327029
    return-void
.end method
