.class public LX/GEd;
.super LX/GEc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GEc",
        "<",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GIt;


# direct methods
.method public constructor <init>(LX/GIt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331953
    invoke-direct {p0}, LX/GEc;-><init>()V

    .line 2331954
    iput-object p1, p0, LX/GEd;->a:LX/GIt;

    .line 2331955
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2331951
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2331952
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2331950
    iget-object v0, p0, LX/GEd;->a:LX/GIt;

    return-object v0
.end method
