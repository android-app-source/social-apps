.class public final LX/GdU;
.super LX/GdQ;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;Ljava/lang/String;Ljava/lang/String;LX/2zA;)V
    .locals 0

    .prologue
    .line 2373981
    iput-object p1, p0, LX/GdU;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    invoke-direct {p0, p2, p3, p4}, LX/GdQ;-><init>(Ljava/lang/String;Ljava/lang/String;LX/2zA;)V

    return-void
.end method


# virtual methods
.method public final c()Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2373982
    iget-object v0, p0, LX/GdU;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->f:LX/0Yn;

    .line 2373983
    const-string v1, "FRIENDS_FEED_ONLY"

    .line 2373984
    iget-object v2, v0, LX/0Yn;->h:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2373985
    iget-object v2, v0, LX/0Yn;->a:LX/0ad;

    invoke-static {v0}, LX/0Yn;->e(LX/0Yn;)LX/0ak;

    move-result-object v3

    invoke-interface {v3}, LX/0ak;->d()C

    move-result v3

    invoke-interface {v2, v3, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, v0, LX/0Yn;->h:LX/0am;

    .line 2373986
    :cond_0
    iget-object v2, v0, LX/0Yn;->h:LX/0am;

    invoke-virtual {v2, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v0, v1

    .line 2373987
    new-instance v1, Lcom/facebook/api/feedtype/newsfeed/NewsFeedType;

    new-instance v2, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;

    const-string v3, "TOP_STORIES"

    invoke-direct {v2, v0, v3}, Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lcom/facebook/api/feedtype/FeedType$Name;->a:Lcom/facebook/api/feedtype/FeedType$Name;

    invoke-static {v0}, LX/2tT;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/facebook/api/feedtype/newsfeed/NewsFeedType;-><init>(Lcom/facebook/api/feedtype/newsfeed/NewsFeedTypeValue;Lcom/facebook/api/feedtype/FeedType$Name;Ljava/lang/String;)V

    .line 2373988
    iget-object v0, p0, LX/GdU;->a:Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/feed/switcher/NewsFeedSwitcherFragment;->m:Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;

    .line 2373989
    iput-object v1, v0, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->a:Lcom/facebook/api/feedtype/FeedType;

    .line 2373990
    move-object v0, v0

    .line 2373991
    invoke-virtual {v0}, Lcom/facebook/feed/fragment/NewsFeedFragment$Builder;->d()Lcom/facebook/feed/fragment/NewsFeedFragment;

    move-result-object v0

    return-object v0
.end method
