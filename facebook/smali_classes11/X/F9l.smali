.class public LX/F9l;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/F9l;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2205916
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2205917
    const-string v0, "findfriends?ci_flow={%s %s}&ccu_ref={%s %s}&force_show_legal_screen={!%s false}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "ci_flow"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LX/89v;->UNKNOWN:LX/89v;

    iget-object v3, v3, LX/89v;->value:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "ccu_ref"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "UNKNOWN"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "force_show_legal_screen"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/growth/friendfinder/FriendFinderStartActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2205918
    sget-object v0, LX/0ax;->eM:Ljava/lang/String;

    const-string v1, "{QUICK_PROMOTION}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/growth/promotion/FriendingPossibilitiesActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2205919
    sget-object v0, LX/0ax;->eN:Ljava/lang/String;

    const-class v1, Lcom/facebook/growth/promotion/NativeNameActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2205920
    sget-object v0, LX/0ax;->eZ:Ljava/lang/String;

    const-class v1, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2205921
    sget-object v0, LX/0ax;->cU:Ljava/lang/String;

    const-class v1, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2205922
    sget-object v0, LX/0ax;->eK:Ljava/lang/String;

    const-string v1, "{external_photo_source}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/growth/nux/NuxProfilePhotoWrapperActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2205923
    return-void
.end method

.method public static a(LX/0QB;)LX/F9l;
    .locals 3

    .prologue
    .line 2205924
    sget-object v0, LX/F9l;->a:LX/F9l;

    if-nez v0, :cond_1

    .line 2205925
    const-class v1, LX/F9l;

    monitor-enter v1

    .line 2205926
    :try_start_0
    sget-object v0, LX/F9l;->a:LX/F9l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2205927
    if-eqz v2, :cond_0

    .line 2205928
    :try_start_1
    new-instance v0, LX/F9l;

    invoke-direct {v0}, LX/F9l;-><init>()V

    .line 2205929
    move-object v0, v0

    .line 2205930
    sput-object v0, LX/F9l;->a:LX/F9l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2205931
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2205932
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2205933
    :cond_1
    sget-object v0, LX/F9l;->a:LX/F9l;

    return-object v0

    .line 2205934
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2205935
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
