.class public final enum LX/GDM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GDM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GDM;

.field public static final enum TASK_TARGETING_COUNTRY_CODE:LX/GDM;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2329762
    new-instance v0, LX/GDM;

    const-string v1, "TASK_TARGETING_COUNTRY_CODE"

    invoke-direct {v0, v1, v2}, LX/GDM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GDM;->TASK_TARGETING_COUNTRY_CODE:LX/GDM;

    .line 2329763
    const/4 v0, 0x1

    new-array v0, v0, [LX/GDM;

    sget-object v1, LX/GDM;->TASK_TARGETING_COUNTRY_CODE:LX/GDM;

    aput-object v1, v0, v2

    sput-object v0, LX/GDM;->$VALUES:[LX/GDM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2329764
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GDM;
    .locals 1

    .prologue
    .line 2329765
    const-class v0, LX/GDM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GDM;

    return-object v0
.end method

.method public static values()[LX/GDM;
    .locals 1

    .prologue
    .line 2329766
    sget-object v0, LX/GDM;->$VALUES:[LX/GDM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GDM;

    return-object v0
.end method
