.class public LX/G4B;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/timeline/service/PrefetchCallback;"
    }
.end annotation


# instance fields
.field private final a:LX/G4D;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/timeline/service/PrefetchListener$PrefetchFinishListener",
            "<TT;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private c:I

.field public d:Z


# direct methods
.method public constructor <init>(LX/G4D;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/service/PrefetchListener$PrefetchFinishListener",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2317153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317154
    iput v0, p0, LX/G4B;->c:I

    .line 2317155
    iput-boolean v0, p0, LX/G4B;->d:Z

    .line 2317156
    iput-object p1, p0, LX/G4B;->a:LX/G4D;

    .line 2317157
    iput-object p2, p0, LX/G4B;->b:Ljava/lang/Object;

    .line 2317158
    return-void
.end method

.method public static e(LX/G4B;)V
    .locals 5

    .prologue
    .line 2317159
    iget-boolean v0, p0, LX/G4B;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/G4B;->c:I

    if-nez v0, :cond_0

    .line 2317160
    iget-object v0, p0, LX/G4B;->a:LX/G4D;

    .line 2317161
    iget-object v1, p0, LX/G4B;->b:Ljava/lang/Object;

    move-object v1, v1

    .line 2317162
    check-cast v1, LX/G4C;

    .line 2317163
    iget-object v2, v1, LX/G4C;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    move-object v2, v2

    .line 2317164
    if-nez v2, :cond_1

    .line 2317165
    :cond_0
    :goto_0
    return-void

    .line 2317166
    :cond_1
    iget-object v2, v0, LX/G4D;->b:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v3, v0, LX/G4D;->a:LX/G4K;

    .line 2317167
    iget-object v4, v2, Lcom/facebook/timeline/service/ProfileLoadManager;->j:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    new-instance p0, Lcom/facebook/timeline/service/ProfileLoadManager$2;

    invoke-direct {p0, v2, v3, v1}, Lcom/facebook/timeline/service/ProfileLoadManager$2;-><init>(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4K;LX/G4C;)V

    const v0, 0x4e146ba8    # 6.2252083E8f

    invoke-static {v4, p0, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2317168
    invoke-static {v2, v3}, Lcom/facebook/timeline/service/ProfileLoadManager;->c$redex0(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4K;)V

    .line 2317169
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2317170
    iget v0, p0, LX/G4B;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G4B;->c:I

    .line 2317171
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2317172
    iget v0, p0, LX/G4B;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/G4B;->c:I

    .line 2317173
    invoke-static {p0}, LX/G4B;->e(LX/G4B;)V

    .line 2317174
    return-void
.end method
