.class public final LX/F8z;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:LX/8DK;

.field public final synthetic b:Lcom/facebook/growth/nux/UserAccountNUXActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/nux/UserAccountNUXActivity;LX/8DK;)V
    .locals 0

    .prologue
    .line 2204868
    iput-object p1, p0, LX/F8z;->b:Lcom/facebook/growth/nux/UserAccountNUXActivity;

    iput-object p2, p0, LX/F8z;->a:LX/8DK;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 3

    .prologue
    .line 2204869
    iget-object v0, p0, LX/F8z;->b:Lcom/facebook/growth/nux/UserAccountNUXActivity;

    iget-object v0, v0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F8z;->b:Lcom/facebook/growth/nux/UserAccountNUXActivity;

    iget-object v0, v0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    if-nez v0, :cond_1

    .line 2204870
    :cond_0
    :goto_0
    return-void

    .line 2204871
    :cond_1
    iget-object v0, p0, LX/F8z;->b:Lcom/facebook/growth/nux/UserAccountNUXActivity;

    iget-object v0, v0, Lcom/facebook/growth/nux/UserAccountNUXActivity;->z:LX/F8v;

    iget-object v1, p0, LX/F8z;->b:Lcom/facebook/growth/nux/UserAccountNUXActivity;

    iget-object v1, v1, Lcom/facebook/growth/nux/UserAccountNUXActivity;->q:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, LX/F8v;->e(I)Ljava/lang/String;

    move-result-object v0

    .line 2204872
    const-string v1, "contact_importer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/F8z;->a:LX/8DK;

    sget-object v2, LX/8DK;->SKIPPED:LX/8DK;

    invoke-virtual {v1, v2}, LX/8DK;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2204873
    iget-object v0, p0, LX/F8z;->b:Lcom/facebook/growth/nux/UserAccountNUXActivity;

    invoke-static {v0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->p(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V

    goto :goto_0

    .line 2204874
    :cond_2
    iget-object v1, p0, LX/F8z;->b:Lcom/facebook/growth/nux/UserAccountNUXActivity;

    iget-object v2, p0, LX/F8z;->a:LX/8DK;

    invoke-static {v1, v0, v2}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->a(Lcom/facebook/growth/nux/UserAccountNUXActivity;Ljava/lang/String;LX/8DK;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2204875
    iget-object v0, p0, LX/F8z;->b:Lcom/facebook/growth/nux/UserAccountNUXActivity;

    invoke-static {v0}, Lcom/facebook/growth/nux/UserAccountNUXActivity;->l(Lcom/facebook/growth/nux/UserAccountNUXActivity;)V

    goto :goto_0
.end method
