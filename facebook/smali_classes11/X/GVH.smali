.class public final LX/GVH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/6ZS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GVI;


# direct methods
.method public constructor <init>(LX/GVI;)V
    .locals 0

    .prologue
    .line 2358813
    iput-object p1, p0, LX/GVH;->a:LX/GVI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2358814
    iget-object v0, p0, LX/GVH;->a:LX/GVI;

    iget-object v0, v0, LX/GVI;->b:LX/2gk;

    iget-object v0, v0, LX/2gk;->g:LX/2bj;

    const-string v1, "location_opt_in_google_play_location_status_check_failed"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    .line 2358815
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2358816
    check-cast p1, LX/6ZS;

    .line 2358817
    iget-object v0, p0, LX/GVH;->a:LX/GVI;

    iget-object v0, v0, LX/GVI;->b:LX/2gk;

    iget-object v0, v0, LX/2gk;->g:LX/2bj;

    .line 2358818
    sget-object v1, LX/GV8;->a:[I

    .line 2358819
    iget-object v2, p1, LX/6ZS;->b:LX/6ZT;

    move-object v2, v2

    .line 2358820
    invoke-virtual {v2}, LX/6ZT;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2358821
    :goto_0
    iget-object v0, p1, LX/6ZS;->b:LX/6ZT;

    move-object v0, v0

    .line 2358822
    sget-object v1, LX/6ZT;->EASY_RESOLUTION_POSSIBLE:LX/6ZT;

    if-ne v0, v1, :cond_0

    .line 2358823
    iget-object v0, p0, LX/GVH;->a:LX/GVI;

    iget-object v0, v0, LX/GVI;->b:LX/2gk;

    iget-object v1, p0, LX/GVH;->a:LX/GVI;

    iget-object v1, v1, LX/GVI;->a:Landroid/content/Context;

    .line 2358824
    new-instance v2, Landroid/content/Intent;

    const-class p0, Lcom/facebook/backgroundlocation/upsell/UpsellContainerActivity;

    invoke-direct {v2, v1, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2358825
    const-string p0, "gms_dialog_surface"

    const-string p1, "surafce_first_news_feed_after_login"

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2358826
    const-string p0, "gms_dialog_mechanism"

    const-string p1, "mechanism_passive"

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2358827
    iget-object p0, v0, LX/2gk;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2358828
    invoke-static {v0}, LX/2gk;->d(LX/2gk;)LX/0Tn;

    move-result-object v2

    .line 2358829
    if-nez v2, :cond_1

    .line 2358830
    :cond_0
    :goto_1
    return-void

    .line 2358831
    :pswitch_0
    const-string v1, "location_opt_in_google_play_location_status_check_dialog_is_possible"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2358832
    :pswitch_1
    const-string v1, "location_opt_in_google_play_location_status_check_dialog_already_attempted"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2358833
    :pswitch_2
    const-string v1, "location_opt_in_google_play_location_not_needed"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2358834
    :pswitch_3
    const-string v1, "location_opt_in_google_play_location_not_possible"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2358835
    :pswitch_4
    const-string v1, "location_opt_in_google_play_location_status_check_failed"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2358836
    :cond_1
    iget-object p0, v0, LX/2gk;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    const/4 p1, 0x1

    invoke-interface {p0, v2, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
