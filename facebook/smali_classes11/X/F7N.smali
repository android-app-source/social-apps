.class public final LX/F7N;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V
    .locals 0

    .prologue
    .line 2201704
    iput-object p1, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2201705
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v1, LX/F72;->FAILURE:LX/F72;

    invoke-virtual {v0, v1}, LX/F73;->a(LX/F72;)V

    .line 2201706
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v2}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    iget-object v4, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v4, v4, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    sget-object v5, LX/9Tj;->FRIENDABLE_CONTACTS_FETCH_FAILED:LX/9Tj;

    invoke-virtual/range {v0 .. v5}, LX/9Tk;->a(Ljava/lang/String;JILX/9Tj;)V

    .line 2201707
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2201708
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2201709
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 2201710
    check-cast p1, LX/0Px;

    const/4 v2, 0x0

    .line 2201711
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2201712
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;

    .line 2201713
    iget-object v1, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->i:LX/F7X;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, LX/F7X;->b(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2201714
    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2201715
    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->m()LX/1vs;

    move-result-object v1

    iget-object v8, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v9, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2201716
    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->k()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    sget-object v11, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v11

    :try_start_1
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2201717
    new-instance v11, LX/F7K;

    invoke-direct {v11}, LX/F7K;-><init>()V

    .line 2201718
    iput-wide v6, v11, LX/F7K;->a:J

    .line 2201719
    move-object v6, v11

    .line 2201720
    if-eqz v1, :cond_1

    invoke-virtual {v8, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2201721
    :goto_1
    iput-object v1, v6, LX/F7K;->b:Ljava/lang/String;

    .line 2201722
    move-object v1, v6

    .line 2201723
    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v6

    .line 2201724
    iput-object v6, v1, LX/F7K;->c:Ljava/lang/String;

    .line 2201725
    move-object v6, v1

    .line 2201726
    if-eqz v9, :cond_2

    invoke-virtual {v10, v9, v2}, LX/15i;->j(II)I

    move-result v1

    .line 2201727
    :goto_2
    iput v1, v6, LX/F7K;->d:I

    .line 2201728
    move-object v1, v6

    .line 2201729
    iput-boolean v2, v1, LX/F7K;->e:Z

    .line 2201730
    move-object v1, v1

    .line 2201731
    invoke-virtual {v1}, LX/F7K;->a()LX/F7L;

    move-result-object v1

    .line 2201732
    iget-object v6, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v6, v6, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2201733
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2201734
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    .line 2201735
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2201736
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 2201737
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    .line 2201738
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 2201739
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v1, LX/F72;->DEFAULT:LX/F72;

    invoke-virtual {v0, v1}, LX/F73;->a(LX/F72;)V

    .line 2201740
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-boolean v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->N:Z

    if-nez v0, :cond_4

    .line 2201741
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    const/4 v1, 0x1

    .line 2201742
    iput-boolean v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->N:Z

    .line 2201743
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v2}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v4

    iget-object v5, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v5}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v5

    invoke-virtual/range {v0 .. v6}, LX/9Tk;->a(Ljava/lang/String;JIJ)V

    .line 2201744
    :cond_4
    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2201745
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->q(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2201746
    :goto_3
    return-void

    .line 2201747
    :cond_5
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v1, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v2}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v2

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v4

    iget-object v5, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v5}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v5

    sget-object v7, LX/9Tj;->FRIENDABLE_CONTACTS_PAGE_FETCHED:LX/9Tj;

    invoke-virtual/range {v0 .. v7}, LX/9Tk;->a(Ljava/lang/String;JIJLX/9Tj;)V

    .line 2201748
    iget-object v0, p0, LX/F7N;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    invoke-virtual {v0, v8}, LX/F73;->a(Ljava/util/List;)V

    goto :goto_3
.end method
