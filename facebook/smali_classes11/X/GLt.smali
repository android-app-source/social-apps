.class public final LX/GLt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/GLu;


# direct methods
.method public constructor <init>(LX/GLu;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2343794
    iput-object p1, p0, LX/GLt;->b:LX/GLu;

    iput-object p2, p0, LX/GLt;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 2343795
    iget-object v0, p0, LX/GLt;->b:LX/GLu;

    iget-object v0, v0, LX/GLu;->e:LX/GJI;

    iget-object v1, p0, LX/GLt;->b:LX/GLu;

    iget-object v1, v1, LX/GLu;->c:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    iget-object v2, p0, LX/GLt;->b:LX/GLu;

    iget-object v2, v2, LX/GLu;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    iget-object v3, p0, LX/GLt;->a:Ljava/lang/String;

    iget-object v4, p0, LX/GLt;->b:LX/GLu;

    iget-object v4, v4, LX/GLu;->b:Ljava/lang/String;

    iget-object v5, p0, LX/GLt;->b:LX/GLu;

    iget-object v5, v5, LX/GLu;->a:LX/0Px;

    .line 2343796
    const/4 v6, 0x1

    invoke-static {v0, v6}, LX/GJI;->b(LX/GJI;Z)V

    .line 2343797
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->A()LX/1vs;

    move-result-object v6

    iget-object p0, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2343798
    new-instance p1, LX/4CY;

    invoke-direct {p1}, LX/4CY;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object p2

    .line 2343799
    const-string v2, "ad_account_id"

    invoke-virtual {p1, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2343800
    move-object p1, p1

    .line 2343801
    const-string p2, "currency"

    invoke-virtual {p1, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2343802
    move-object p1, p1

    .line 2343803
    const/4 p2, 0x0

    invoke-virtual {p0, v6, p2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 2343804
    const-string p0, "timezone"

    invoke-virtual {p1, p0, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2343805
    move-object v6, p1

    .line 2343806
    new-instance p0, LX/ACK;

    invoke-direct {p0}, LX/ACK;-><init>()V

    move-object p0, p0

    .line 2343807
    const-string p1, "input"

    invoke-virtual {p0, p1, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/ACK;

    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 2343808
    iget-object p0, v0, LX/GJI;->l:LX/0tX;

    invoke-virtual {p0, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2343809
    iget-object p0, v0, LX/GJI;->o:LX/0Sh;

    new-instance p1, LX/GLv;

    invoke-direct {p1, v0, v1, v5, v4}, LX/GLv;-><init>(LX/GJI;Lcom/facebook/adinterfaces/ui/BudgetOptionsView;LX/0Px;Ljava/lang/String;)V

    invoke-virtual {p0, v6, p1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2343810
    return-void
.end method
