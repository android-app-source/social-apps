.class public LX/FKD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3QW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224767
    return-void
.end method

.method public static b(LX/0QB;)LX/FKD;
    .locals 5

    .prologue
    .line 2224768
    new-instance v4, LX/FKD;

    invoke-direct {v4}, LX/FKD;-><init>()V

    .line 2224769
    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/3QW;->a(LX/0QB;)LX/3QW;

    move-result-object v2

    check-cast v2, LX/3QW;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    .line 2224770
    iput-object v0, v4, LX/FKD;->a:LX/0Uh;

    iput-object v1, v4, LX/FKD;->b:LX/0ad;

    iput-object v2, v4, LX/FKD;->c:LX/3QW;

    iput-object v3, v4, LX/FKD;->d:LX/0W3;

    .line 2224771
    return-object v4
.end method


# virtual methods
.method public final b()Z
    .locals 4

    .prologue
    .line 2224772
    iget-object v0, p0, LX/FKD;->d:LX/0W3;

    sget-wide v2, LX/0X5;->gc:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method
