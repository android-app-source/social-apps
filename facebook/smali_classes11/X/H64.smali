.class public final LX/H64;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 0

    .prologue
    .line 2425810
    iput-object p1, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x2

    const v0, 0x28c93aea

    invoke-static {v6, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2425811
    iget-object v1, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425812
    iput-boolean v2, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->az:Z

    .line 2425813
    iget-object v1, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->f:LX/H6T;

    iget-object v2, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v3, v3, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v3}, LX/H83;->f()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v4, v4, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v4}, LX/H83;->x()Ljava/lang/String;

    move-result-object v4

    .line 2425814
    sget-object v5, LX/0ax;->hI:Ljava/lang/String;

    const-string p1, "UTF-8"

    invoke-static {v3, p1}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v5, p1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2425815
    iget-object p1, v1, LX/H6T;->j:LX/17Y;

    invoke-interface {p1, v2, v5}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 2425816
    iget-object p1, v1, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1, v5, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2425817
    iget-object v1, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->h:LX/0Zb;

    iget-object v2, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->i:LX/H82;

    const-string v3, "opened_barcode"

    iget-object v4, p0, LX/H64;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v4, v4, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2425818
    const v1, 0x481ce728    # 160668.62f

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
