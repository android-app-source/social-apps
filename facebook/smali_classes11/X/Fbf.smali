.class public LX/Fbf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2260843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260844
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2260845
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v0

    .line 2260846
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
            ")",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$ModuleResultEdge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2260847
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2260848
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2260849
    goto :goto_0
.end method
