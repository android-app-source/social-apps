.class public final enum LX/GA9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GA9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GA9;

.field public static final enum CLIENT_TOKEN:LX/GA9;

.field public static final enum DEVICE_AUTH:LX/GA9;

.field public static final enum FACEBOOK_APPLICATION_NATIVE:LX/GA9;

.field public static final enum FACEBOOK_APPLICATION_SERVICE:LX/GA9;

.field public static final enum FACEBOOK_APPLICATION_WEB:LX/GA9;

.field public static final enum NONE:LX/GA9;

.field public static final enum TEST_USER:LX/GA9;

.field public static final enum WEB_VIEW:LX/GA9;


# instance fields
.field private final canExtendToken:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2324731
    new-instance v0, LX/GA9;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4, v4}, LX/GA9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/GA9;->NONE:LX/GA9;

    .line 2324732
    new-instance v0, LX/GA9;

    const-string v1, "FACEBOOK_APPLICATION_WEB"

    invoke-direct {v0, v1, v3, v3}, LX/GA9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/GA9;->FACEBOOK_APPLICATION_WEB:LX/GA9;

    .line 2324733
    new-instance v0, LX/GA9;

    const-string v1, "FACEBOOK_APPLICATION_NATIVE"

    invoke-direct {v0, v1, v5, v3}, LX/GA9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/GA9;->FACEBOOK_APPLICATION_NATIVE:LX/GA9;

    .line 2324734
    new-instance v0, LX/GA9;

    const-string v1, "FACEBOOK_APPLICATION_SERVICE"

    invoke-direct {v0, v1, v6, v3}, LX/GA9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/GA9;->FACEBOOK_APPLICATION_SERVICE:LX/GA9;

    .line 2324735
    new-instance v0, LX/GA9;

    const-string v1, "WEB_VIEW"

    invoke-direct {v0, v1, v7, v4}, LX/GA9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/GA9;->WEB_VIEW:LX/GA9;

    .line 2324736
    new-instance v0, LX/GA9;

    const-string v1, "TEST_USER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/GA9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/GA9;->TEST_USER:LX/GA9;

    .line 2324737
    new-instance v0, LX/GA9;

    const-string v1, "CLIENT_TOKEN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/GA9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/GA9;->CLIENT_TOKEN:LX/GA9;

    .line 2324738
    new-instance v0, LX/GA9;

    const-string v1, "DEVICE_AUTH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, LX/GA9;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/GA9;->DEVICE_AUTH:LX/GA9;

    .line 2324739
    const/16 v0, 0x8

    new-array v0, v0, [LX/GA9;

    sget-object v1, LX/GA9;->NONE:LX/GA9;

    aput-object v1, v0, v4

    sget-object v1, LX/GA9;->FACEBOOK_APPLICATION_WEB:LX/GA9;

    aput-object v1, v0, v3

    sget-object v1, LX/GA9;->FACEBOOK_APPLICATION_NATIVE:LX/GA9;

    aput-object v1, v0, v5

    sget-object v1, LX/GA9;->FACEBOOK_APPLICATION_SERVICE:LX/GA9;

    aput-object v1, v0, v6

    sget-object v1, LX/GA9;->WEB_VIEW:LX/GA9;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GA9;->TEST_USER:LX/GA9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GA9;->CLIENT_TOKEN:LX/GA9;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GA9;->DEVICE_AUTH:LX/GA9;

    aput-object v2, v0, v1

    sput-object v0, LX/GA9;->$VALUES:[LX/GA9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 2324740
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2324741
    iput-boolean p3, p0, LX/GA9;->canExtendToken:Z

    .line 2324742
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GA9;
    .locals 1

    .prologue
    .line 2324743
    const-class v0, LX/GA9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GA9;

    return-object v0
.end method

.method public static values()[LX/GA9;
    .locals 1

    .prologue
    .line 2324744
    sget-object v0, LX/GA9;->$VALUES:[LX/GA9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GA9;

    return-object v0
.end method


# virtual methods
.method public final canExtendToken()Z
    .locals 1

    .prologue
    .line 2324745
    iget-boolean v0, p0, LX/GA9;->canExtendToken:Z

    return v0
.end method
