.class public final LX/G2z;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/3Fw;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G2o;

.field public final synthetic b:LX/1Pc;

.field public final synthetic c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;LX/G2o;LX/1Pc;)V
    .locals 0

    .prologue
    .line 2314694
    iput-object p1, p0, LX/G2z;->c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iput-object p2, p0, LX/G2z;->a:LX/G2o;

    iput-object p3, p0, LX/G2z;->b:LX/1Pc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2314695
    iget-object v0, p0, LX/G2z;->a:LX/G2o;

    new-instance v1, LX/17L;

    invoke-direct {v1}, LX/17L;-><init>()V

    .line 2314696
    iput-boolean v3, v1, LX/17L;->d:Z

    .line 2314697
    move-object v1, v1

    .line 2314698
    invoke-virtual {v1}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    .line 2314699
    iput-object v1, v0, LX/G2o;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2314700
    iget-object v0, p0, LX/G2z;->b:LX/1Pc;

    check-cast v0, LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, LX/G2z;->a:LX/G2o;

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2314701
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2314702
    check-cast p1, LX/3Fw;

    .line 2314703
    iget-object v0, p0, LX/G2z;->a:LX/G2o;

    iget-object v1, p1, LX/3Fw;->a:LX/0Px;

    .line 2314704
    iget-object v2, v0, LX/G2o;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2314705
    iget-object v0, p0, LX/G2z;->a:LX/G2o;

    iget-object v1, p1, LX/3Fw;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2314706
    iput-object v1, v0, LX/G2o;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2314707
    iget-object v0, p0, LX/G2z;->b:LX/1Pc;

    check-cast v0, LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/G2z;->a:LX/G2o;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2314708
    return-void
.end method
