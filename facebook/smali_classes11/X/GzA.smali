.class public final enum LX/GzA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GzA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GzA;

.field public static final enum CANCEL:LX/GzA;

.field public static final enum ERROR:LX/GzA;

.field public static final enum SUCCESS:LX/GzA;


# instance fields
.field private final loggingValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2411196
    new-instance v0, LX/GzA;

    const-string v1, "SUCCESS"

    const-string v2, "success"

    invoke-direct {v0, v1, v3, v2}, LX/GzA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GzA;->SUCCESS:LX/GzA;

    .line 2411197
    new-instance v0, LX/GzA;

    const-string v1, "CANCEL"

    const-string v2, "cancel"

    invoke-direct {v0, v1, v4, v2}, LX/GzA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GzA;->CANCEL:LX/GzA;

    .line 2411198
    new-instance v0, LX/GzA;

    const-string v1, "ERROR"

    const-string v2, "error"

    invoke-direct {v0, v1, v5, v2}, LX/GzA;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GzA;->ERROR:LX/GzA;

    .line 2411199
    const/4 v0, 0x3

    new-array v0, v0, [LX/GzA;

    sget-object v1, LX/GzA;->SUCCESS:LX/GzA;

    aput-object v1, v0, v3

    sget-object v1, LX/GzA;->CANCEL:LX/GzA;

    aput-object v1, v0, v4

    sget-object v1, LX/GzA;->ERROR:LX/GzA;

    aput-object v1, v0, v5

    sput-object v0, LX/GzA;->$VALUES:[LX/GzA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2411193
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2411194
    iput-object p3, p0, LX/GzA;->loggingValue:Ljava/lang/String;

    .line 2411195
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GzA;
    .locals 1

    .prologue
    .line 2411200
    const-class v0, LX/GzA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GzA;

    return-object v0
.end method

.method public static values()[LX/GzA;
    .locals 1

    .prologue
    .line 2411192
    sget-object v0, LX/GzA;->$VALUES:[LX/GzA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GzA;

    return-object v0
.end method


# virtual methods
.method public final getLoggingValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2411191
    iget-object v0, p0, LX/GzA;->loggingValue:Ljava/lang/String;

    return-object v0
.end method
