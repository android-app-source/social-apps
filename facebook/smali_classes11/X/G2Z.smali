.class public LX/G2Z;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:Landroid/widget/TextView;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Landroid/widget/ImageView;

.field public e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2314252
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2314253
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/G2Z;->setOrientation(I)V

    .line 2314254
    const p1, 0x7f0310a0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2314255
    const p1, 0x7f0d27a1

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/G2Z;->a:Landroid/widget/TextView;

    .line 2314256
    const p1, 0x7f0d27a2

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/G2Z;->b:Landroid/widget/TextView;

    .line 2314257
    const p1, 0x7f0d279f

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, LX/G2Z;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2314258
    const p1, 0x7f0d27a0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, LX/G2Z;->d:Landroid/widget/ImageView;

    .line 2314259
    const p1, 0x7f0d279e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/G2Z;->e:Landroid/view/View;

    .line 2314260
    return-void
.end method


# virtual methods
.method public setOnAddFriendClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2314261
    iget-object v0, p0, LX/G2Z;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2314262
    return-void
.end method

.method public setProfilePictureController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 2314263
    iget-object v0, p0, LX/G2Z;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2314264
    return-void
.end method
