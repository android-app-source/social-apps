.class public final LX/GZU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/commerce/core/intent/MerchantInfoViewData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;J)V
    .locals 0

    .prologue
    .line 2366563
    iput-object p1, p0, LX/GZU;->b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    iput-wide p2, p0, LX/GZU;->a:J

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2366564
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2366556
    check-cast p1, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2366557
    if-nez p1, :cond_0

    .line 2366558
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Null or empty merchant query response from server"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/GZU;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2366559
    :cond_0
    iget-object v0, p0, LX/GZU;->b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    .line 2366560
    iput-object p1, v0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->t:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2366561
    iget-object v0, p0, LX/GZU;->b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    iget-wide v2, p0, LX/GZU;->a:J

    invoke-static {v0, v2, v3}, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->a$redex0(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;J)V

    .line 2366562
    return-void
.end method
