.class public LX/GU9;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 2357019
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2357020
    iput-object p1, p0, LX/GU9;->a:Landroid/content/res/Resources;

    .line 2357021
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/GU9;->b:Landroid/graphics/Paint;

    .line 2357022
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2357023
    iget-object v0, p0, LX/GU9;->c:Landroid/graphics/Bitmap;

    iget-object v1, p0, LX/GU9;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2357024
    return-void
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2357025
    const/4 v0, -0x3

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 9

    .prologue
    .line 2357026
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 2357027
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, LX/GU9;->c:Landroid/graphics/Bitmap;

    .line 2357028
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, LX/GU9;->c:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2357029
    iget-object v3, p0, LX/GU9;->a:Landroid/content/res/Resources;

    const v4, 0x7f0a00d5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2357030
    iget-object v3, p0, LX/GU9;->a:Landroid/content/res/Resources;

    const v4, 0x7f020101

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2357031
    iget-object v4, p0, LX/GU9;->a:Landroid/content/res/Resources;

    const v5, 0x7f0b238f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2357032
    iget-object v5, p0, LX/GU9;->a:Landroid/content/res/Resources;

    const v6, 0x7f0b238d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2357033
    div-int/lit8 v6, v1, 0x2

    sub-int/2addr v6, v5

    sub-int v7, v0, v4

    div-int/lit8 v8, v1, 0x2

    add-int/2addr v5, v8

    invoke-virtual {v3, v4, v6, v7, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2357034
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2357035
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 2357036
    iget-object v4, p0, LX/GU9;->a:Landroid/content/res/Resources;

    const v5, 0x106000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2357037
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2357038
    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 2357039
    div-int/lit8 v1, v1, 0x2

    .line 2357040
    const/4 v4, 0x0

    int-to-float v5, v1

    int-to-float v6, v1

    invoke-virtual {v2, v4, v5, v6, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2357041
    int-to-float v0, v0

    int-to-float v4, v1

    int-to-float v1, v1

    invoke-virtual {v2, v0, v4, v1, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2357042
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 2357043
    iget-object v0, p0, LX/GU9;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2357044
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 2357045
    iget-object v0, p0, LX/GU9;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 2357046
    return-void
.end method
