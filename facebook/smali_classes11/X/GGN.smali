.class public LX/GGN;
.super LX/GGI;
.source ""


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/CreativeAdModel;

.field public b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

.field public c:Lcom/facebook/adinterfaces/model/EventSpecModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

.field public h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2334109
    invoke-direct {p0}, LX/GGI;-><init>()V

    .line 2334110
    return-void
.end method

.method public constructor <init>(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 1

    .prologue
    .line 2334111
    invoke-direct {p0, p1}, LX/GGI;-><init>(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V

    .line 2334112
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2334113
    iput-object v0, p0, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2334114
    return-void
.end method


# virtual methods
.method public synthetic a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
    .locals 1

    .prologue
    .line 2334115
    invoke-virtual {p0}, LX/GGN;->b()Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;
    .locals 1

    .prologue
    .line 2334116
    new-instance v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {v0, p0}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;-><init>(LX/GGN;)V

    return-object v0
.end method
