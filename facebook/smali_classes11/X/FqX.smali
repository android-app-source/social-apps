.class public final LX/FqX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EpI;


# instance fields
.field public final synthetic a:LX/Fqc;


# direct methods
.method public constructor <init>(LX/Fqc;)V
    .locals 0

    .prologue
    .line 2293944
    iput-object p1, p0, LX/FqX;->a:LX/Fqc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 3

    .prologue
    .line 2293945
    iget-object v0, p0, LX/FqX;->a:LX/Fqc;

    iget-object v0, v0, LX/Fqc;->x:LX/Fy4;

    .line 2293946
    if-eqz p1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2293947
    :goto_0
    iget-object v2, v0, LX/Fy4;->e:LX/BQ1;

    invoke-virtual {v2, v1}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2293948
    if-eqz p2, :cond_1

    .line 2293949
    iget-object v1, v0, LX/Fy4;->e:LX/BQ1;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v1, v2}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2293950
    :goto_1
    invoke-static {v0}, LX/Fy4;->c(LX/Fy4;)V

    .line 2293951
    iget-object v0, p0, LX/FqX;->a:LX/Fqc;

    iget-object v0, v0, LX/Fqc;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BPp;

    new-instance v1, LX/BPi;

    invoke-direct {v1, p1, p2}, LX/BPi;-><init>(ZZ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2293952
    return-void

    .line 2293953
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    .line 2293954
    :cond_1
    iget-object v1, v0, LX/Fy4;->e:LX/BQ1;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v1, v2}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    goto :goto_1
.end method
