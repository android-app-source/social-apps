.class public LX/G5n;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2MV;

.field private final c:LX/61l;

.field private final d:LX/03V;

.field private final e:LX/5Ov;

.field private final f:LX/5Ox;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/61m;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2319290
    const-class v0, LX/G5n;

    sput-object v0, LX/G5n;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2MV;LX/61l;LX/03V;LX/5Ov;LX/5Ox;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2MV;",
            "LX/61l;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/5Ov;",
            "LX/5Ox;",
            "LX/0Ot",
            "<",
            "LX/61m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319292
    iput-object p1, p0, LX/G5n;->b:LX/2MV;

    .line 2319293
    iput-object p2, p0, LX/G5n;->c:LX/61l;

    .line 2319294
    iput-object p3, p0, LX/G5n;->d:LX/03V;

    .line 2319295
    iput-object p4, p0, LX/G5n;->e:LX/5Ov;

    .line 2319296
    iput-object p5, p0, LX/G5n;->f:LX/5Ox;

    .line 2319297
    iput-object p6, p0, LX/G5n;->g:LX/0Ot;

    .line 2319298
    return-void
.end method


# virtual methods
.method public final a(LX/G5p;)V
    .locals 24

    .prologue
    .line 2319299
    const/4 v4, 0x0

    .line 2319300
    const/4 v3, 0x0

    .line 2319301
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/G5n;->e:LX/5Ov;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/G5p;->a:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/5Ov;->a(Ljava/lang/String;)Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->a()Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    .line 2319302
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/G5n;->c:LX/61l;

    invoke-virtual {v2, v10}, LX/61l;->a(Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;)LX/61k;

    move-result-object v11

    .line 2319303
    move-object/from16 v0, p0

    iget-object v2, v0, LX/G5n;->c:LX/61l;

    invoke-virtual {v2, v10}, LX/61l;->b(Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;)LX/61k;

    move-result-object v14

    .line 2319304
    move-object/from16 v0, p1

    iget-boolean v2, v0, LX/G5p;->f:Z

    if-eqz v2, :cond_c

    .line 2319305
    move-object/from16 v0, p0

    iget-object v2, v0, LX/G5n;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/61m;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/G5p;->a:Ljava/io/File;

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/61m;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v2

    move-object v8, v2

    .line 2319306
    :goto_0
    move-object/from16 v0, p1

    iget v2, v0, LX/G5p;->c:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v6, v2

    .line 2319307
    move-object/from16 v0, p1

    iget v2, v0, LX/G5p;->d:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v4, v2

    .line 2319308
    const-wide/16 v12, 0x0

    cmp-long v2, v6, v12

    if-gez v2, :cond_0

    .line 2319309
    const-wide/16 v6, 0x0

    .line 2319310
    :cond_0
    const-wide/16 v12, 0x0

    cmp-long v2, v4, v12

    if-gez v2, :cond_11

    .line 2319311
    iget-wide v4, v8, LX/60x;->a:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v4, v12

    move-wide v12, v4

    .line 2319312
    :goto_1
    iget v2, v11, LX/61k;->c:I

    invoke-virtual {v10, v2}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->b(I)V

    .line 2319313
    if-eqz v14, :cond_1

    .line 2319314
    iget v2, v14, LX/61k;->c:I

    invoke-virtual {v10, v2}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->b(I)V

    .line 2319315
    :cond_1
    iget v2, v11, LX/61k;->c:I

    const/4 v4, 0x0

    invoke-virtual {v10, v2, v6, v7, v4}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->a(IJI)V

    .line 2319316
    invoke-virtual {v10}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->d()J

    move-result-wide v16

    .line 2319317
    const/high16 v2, 0x100000

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v15

    .line 2319318
    move-object/from16 v0, p0

    iget-object v2, v0, LX/G5n;->f:LX/5Ox;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/G5p;->b:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/5Ox;->a(Ljava/lang/String;)Lcom/facebook/ffmpeg/FFMpegMediaMuxer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->a()Lcom/facebook/ffmpeg/FFMpegMediaMuxer;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v9

    .line 2319319
    :try_start_2
    iget-object v2, v11, LX/61k;->b:Lcom/facebook/ffmpeg/FFMpegMediaFormat;

    invoke-virtual {v9, v2}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->a(Lcom/facebook/ffmpeg/FFMpegMediaFormat;)Lcom/facebook/ffmpeg/FFMpegAVStream;

    move-result-object v18

    .line 2319320
    const/4 v2, 0x0

    .line 2319321
    if-eqz v14, :cond_2

    .line 2319322
    iget-object v2, v14, LX/61k;->b:Lcom/facebook/ffmpeg/FFMpegMediaFormat;

    invoke-virtual {v9, v2}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->a(Lcom/facebook/ffmpeg/FFMpegMediaFormat;)Lcom/facebook/ffmpeg/FFMpegAVStream;

    move-result-object v2

    .line 2319323
    :cond_2
    invoke-virtual {v9}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->b()V

    .line 2319324
    iget v3, v8, LX/60x;->d:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/facebook/ffmpeg/FFMpegAVStream;->a(I)V

    .line 2319325
    new-instance v3, Lcom/facebook/ffmpeg/FFMpegBufferInfo;

    invoke-direct {v3}, Lcom/facebook/ffmpeg/FFMpegBufferInfo;-><init>()V

    .line 2319326
    iget-object v4, v11, LX/61k;->b:Lcom/facebook/ffmpeg/FFMpegMediaFormat;

    const-string v5, "csd-0"

    invoke-virtual {v4, v5}, Lcom/facebook/ffmpeg/FFMpegMediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v19

    .line 2319327
    if-eqz v19, :cond_3

    .line 2319328
    const/4 v4, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v5

    const-wide/16 v6, 0x0

    const/4 v8, 0x2

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->a(IIJI)V

    .line 2319329
    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/facebook/ffmpeg/FFMpegAVStream;->a(Lcom/facebook/ffmpeg/FFMpegBufferInfo;Ljava/nio/ByteBuffer;)V

    .line 2319330
    :cond_3
    if-eqz v14, :cond_4

    .line 2319331
    iget-object v4, v14, LX/61k;->b:Lcom/facebook/ffmpeg/FFMpegMediaFormat;

    const-string v5, "csd-0"

    invoke-virtual {v4, v5}, Lcom/facebook/ffmpeg/FFMpegMediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v19

    .line 2319332
    if-eqz v19, :cond_4

    .line 2319333
    const/4 v4, 0x0

    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v5

    const-wide/16 v6, 0x0

    const/4 v8, 0x2

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->a(IIJI)V

    .line 2319334
    move-object/from16 v0, v19

    invoke-virtual {v2, v3, v0}, Lcom/facebook/ffmpeg/FFMpegAVStream;->a(Lcom/facebook/ffmpeg/FFMpegBufferInfo;Ljava/nio/ByteBuffer;)V

    .line 2319335
    :cond_4
    sub-long v20, v12, v16

    .line 2319336
    :cond_5
    invoke-virtual {v10}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->d()J

    move-result-wide v22

    .line 2319337
    invoke-virtual {v10}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->e()I

    move-result v4

    .line 2319338
    invoke-virtual {v10}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->c()I

    move-result v8

    .line 2319339
    cmp-long v5, v22, v12

    if-gtz v5, :cond_8

    .line 2319340
    const/4 v5, 0x0

    invoke-virtual {v10, v15, v5}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->a(Ljava/nio/ByteBuffer;I)I

    move-result v5

    .line 2319341
    const/4 v6, -0x1

    if-eq v5, v6, :cond_8

    .line 2319342
    iget v6, v11, LX/61k;->c:I

    if-ne v4, v6, :cond_d

    .line 2319343
    const/4 v4, 0x0

    sub-long v6, v22, v16

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->a(IIJI)V

    .line 2319344
    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v15}, Lcom/facebook/ffmpeg/FFMpegAVStream;->a(Lcom/facebook/ffmpeg/FFMpegBufferInfo;Ljava/nio/ByteBuffer;)V

    .line 2319345
    :cond_6
    :goto_2
    move-object/from16 v0, p1

    iget-object v4, v0, LX/G5p;->e:LX/60y;

    if-eqz v4, :cond_7

    .line 2319346
    move-wide/from16 v0, v22

    long-to-double v4, v0

    move-wide/from16 v0, v20

    long-to-double v6, v0

    div-double/2addr v4, v6

    .line 2319347
    move-object/from16 v0, p1

    iget-object v6, v0, LX/G5p;->e:LX/60y;

    invoke-interface {v6, v4, v5}, LX/60y;->a(D)V

    .line 2319348
    :cond_7
    invoke-virtual {v10}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->b()Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result v4

    if-nez v4, :cond_5

    .line 2319349
    :cond_8
    move-object/from16 v0, p1

    iget-object v2, v0, LX/G5p;->e:LX/60y;

    if-eqz v2, :cond_9

    .line 2319350
    move-object/from16 v0, p1

    iget-object v2, v0, LX/G5p;->e:LX/60y;

    invoke-interface {v2}, LX/60y;->a()V

    .line 2319351
    :cond_9
    if-eqz v10, :cond_a

    .line 2319352
    invoke-virtual {v10}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->g()V

    .line 2319353
    :cond_a
    if-eqz v9, :cond_b

    .line 2319354
    invoke-virtual {v9}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->c()V

    .line 2319355
    :cond_b
    return-void

    .line 2319356
    :cond_c
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/G5n;->b:LX/2MV;

    move-object/from16 v0, p1

    iget-object v4, v0, LX/G5p;->a:Ljava/io/File;

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v2, v4}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v2

    move-object v8, v2

    goto/16 :goto_0

    .line 2319357
    :cond_d
    if-eqz v14, :cond_6

    :try_start_4
    iget v6, v14, LX/61k;->c:I

    if-ne v4, v6, :cond_6

    .line 2319358
    const/4 v4, 0x0

    sub-long v6, v22, v16

    invoke-virtual/range {v3 .. v8}, Lcom/facebook/ffmpeg/FFMpegBufferInfo;->a(IIJI)V

    .line 2319359
    invoke-virtual {v2, v3, v15}, Lcom/facebook/ffmpeg/FFMpegAVStream;->a(Lcom/facebook/ffmpeg/FFMpegBufferInfo;Ljava/nio/ByteBuffer;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_2

    .line 2319360
    :catch_0
    move-exception v2

    move-object v3, v9

    move-object v4, v10

    .line 2319361
    :goto_3
    :try_start_5
    sget-object v5, LX/G5n;->a:Ljava/lang/Class;

    const-string v6, "Exception"

    invoke-static {v5, v6, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2319362
    move-object/from16 v0, p0

    iget-object v5, v0, LX/G5n;->d:LX/03V;

    const-string v6, "VideoTrimOperation_Exception"

    invoke-virtual {v5, v6, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2319363
    move-object/from16 v0, p1

    iget-object v5, v0, LX/G5p;->b:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 2319364
    const-class v5, LX/G5m;

    invoke-static {v2, v5}, LX/1Bz;->propagateIfInstanceOf(Ljava/lang/Throwable;Ljava/lang/Class;)V

    .line 2319365
    new-instance v5, LX/G5m;

    const-string v6, "Failed to resize video"

    invoke-direct {v5, v6, v2}, LX/G5m;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2319366
    :catchall_0
    move-exception v2

    move-object v10, v4

    :goto_4
    move-object/from16 v0, p1

    iget-object v4, v0, LX/G5p;->e:LX/60y;

    if-eqz v4, :cond_e

    .line 2319367
    move-object/from16 v0, p1

    iget-object v4, v0, LX/G5p;->e:LX/60y;

    invoke-interface {v4}, LX/60y;->a()V

    .line 2319368
    :cond_e
    if-eqz v10, :cond_f

    .line 2319369
    invoke-virtual {v10}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->g()V

    .line 2319370
    :cond_f
    if-eqz v3, :cond_10

    .line 2319371
    invoke-virtual {v3}, Lcom/facebook/ffmpeg/FFMpegMediaMuxer;->c()V

    :cond_10
    throw v2

    .line 2319372
    :catchall_1
    move-exception v2

    move-object v10, v4

    goto :goto_4

    :catchall_2
    move-exception v2

    goto :goto_4

    :catchall_3
    move-exception v2

    move-object v3, v9

    goto :goto_4

    .line 2319373
    :catch_1
    move-exception v2

    goto :goto_3

    :catch_2
    move-exception v2

    move-object v4, v10

    goto :goto_3

    :cond_11
    move-wide v12, v4

    goto/16 :goto_1
.end method
