.class public LX/FwJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FwE;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FwJ;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303022
    return-void
.end method

.method public static a(LX/0QB;)LX/FwJ;
    .locals 3

    .prologue
    .line 2303009
    sget-object v0, LX/FwJ;->a:LX/FwJ;

    if-nez v0, :cond_1

    .line 2303010
    const-class v1, LX/FwJ;

    monitor-enter v1

    .line 2303011
    :try_start_0
    sget-object v0, LX/FwJ;->a:LX/FwJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2303012
    if-eqz v2, :cond_0

    .line 2303013
    :try_start_1
    new-instance v0, LX/FwJ;

    invoke-direct {v0}, LX/FwJ;-><init>()V

    .line 2303014
    move-object v0, v0

    .line 2303015
    sput-object v0, LX/FwJ;->a:LX/FwJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2303016
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2303017
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2303018
    :cond_1
    sget-object v0, LX/FwJ;->a:LX/FwJ;

    return-object v0

    .line 2303019
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2303020
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2303008
    return-void
.end method

.method public final a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;LX/1bf;LX/1Fb;)V
    .locals 0

    .prologue
    .line 2303007
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2303006
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2303005
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2303001
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 2303004
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 2303003
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 2303002
    return-void
.end method
