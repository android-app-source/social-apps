.class public LX/FHD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;
.implements LX/4B8;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/2Nx;

.field private final b:LX/CN2;

.field private final c:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

.field private final d:LX/03V;

.field private final e:LX/FHb;

.field private final f:LX/2Mn;

.field public g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2220106
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FHD;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/2Nx;LX/CN2;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/03V;LX/FHb;LX/2Mn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2220098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220099
    iput-object p1, p0, LX/FHD;->a:LX/2Nx;

    .line 2220100
    iput-object p2, p0, LX/FHD;->b:LX/CN2;

    .line 2220101
    iput-object p3, p0, LX/FHD;->c:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 2220102
    iput-object p4, p0, LX/FHD;->d:LX/03V;

    .line 2220103
    iput-object p5, p0, LX/FHD;->e:LX/FHb;

    .line 2220104
    iput-object p6, p0, LX/FHD;->f:LX/2Mn;

    .line 2220105
    return-void
.end method

.method public static a(LX/0QB;)LX/FHD;
    .locals 14

    .prologue
    .line 2219982
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2219983
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2219984
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2219985
    if-nez v1, :cond_0

    .line 2219986
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2219987
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2219988
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2219989
    sget-object v1, LX/FHD;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2219990
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2219991
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2219992
    :cond_1
    if-nez v1, :cond_4

    .line 2219993
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2219994
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2219995
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2219996
    new-instance v7, LX/FHD;

    invoke-static {v0}, LX/2Nx;->a(LX/0QB;)LX/2Nx;

    move-result-object v8

    check-cast v8, LX/2Nx;

    invoke-static {v0}, LX/CN2;->a(LX/0QB;)LX/CN2;

    move-result-object v9

    check-cast v9, LX/CN2;

    invoke-static {v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v10

    check-cast v10, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {v0}, LX/FHb;->a(LX/0QB;)LX/FHb;

    move-result-object v12

    check-cast v12, LX/FHb;

    invoke-static {v0}, LX/2Mn;->a(LX/0QB;)LX/2Mn;

    move-result-object v13

    check-cast v13, LX/2Mn;

    invoke-direct/range {v7 .. v13}, LX/FHD;-><init>(LX/2Nx;LX/CN2;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/03V;LX/FHb;LX/2Mn;)V

    .line 2219997
    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    .line 2219998
    iput-object v8, v7, LX/FHD;->g:LX/0ad;

    .line 2219999
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2220000
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2220001
    if-nez v1, :cond_2

    .line 2220002
    sget-object v0, LX/FHD;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHD;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2220003
    :goto_1
    if-eqz v0, :cond_3

    .line 2220004
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2220005
    :goto_3
    check-cast v0, LX/FHD;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2220006
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2220007
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2220008
    :catchall_1
    move-exception v0

    .line 2220009
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2220010
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2220011
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2220012
    :cond_2
    :try_start_8
    sget-object v0, LX/FHD;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHD;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    .line 2220061
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2220062
    const-string v0, "mediaResource"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2220063
    invoke-static {v0}, LX/5zt;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2220064
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v1, "MediaResource is not a photo."

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2220065
    :goto_0
    return-object v0

    .line 2220066
    :cond_0
    iget-object v2, p0, LX/FHD;->c:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v2, v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v2

    .line 2220067
    const-string v0, "photoQuality"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    .line 2220068
    const-string v3, "phase"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2220069
    invoke-static {p0, v2, v0, v1}, LX/FHD;->a(LX/FHD;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;I)Ljava/io/File;

    move-result-object v0

    .line 2220070
    if-nez v0, :cond_1

    .line 2220071
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2220072
    goto :goto_0

    .line 2220073
    :cond_1
    iget-object v1, p0, LX/FHD;->g:LX/0ad;

    sget-short v3, LX/FGS;->r:S

    invoke-interface {v1, v3, v8}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2220074
    iget-wide v4, v2, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    cmp-long v1, v4, v10

    if-lez v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    iget-wide v6, v2, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 2220075
    iget-object v0, p0, LX/FHD;->a:LX/2Nx;

    iget-object v1, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/2Nx;->a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2220076
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2220077
    :cond_2
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v1

    .line 2220078
    iput-object v2, v1, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2220079
    move-object v1, v1

    .line 2220080
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2220081
    iput-object v0, v1, LX/5zn;->b:Landroid/net/Uri;

    .line 2220082
    move-object v0, v1

    .line 2220083
    const-string v1, "image/jpeg"

    .line 2220084
    iput-object v1, v0, LX/5zn;->q:Ljava/lang/String;

    .line 2220085
    move-object v0, v0

    .line 2220086
    iput v8, v0, LX/5zn;->j:I

    .line 2220087
    move-object v0, v0

    .line 2220088
    iput v8, v0, LX/5zn;->k:I

    .line 2220089
    move-object v0, v0

    .line 2220090
    sget-object v1, LX/47d;->UNDEFINED:LX/47d;

    .line 2220091
    iput-object v1, v0, LX/5zn;->l:LX/47d;

    .line 2220092
    move-object v0, v0

    .line 2220093
    iput-wide v10, v0, LX/5zn;->r:J

    .line 2220094
    move-object v0, v0

    .line 2220095
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2220096
    iget-object v1, p0, LX/FHD;->c:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2220097
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/FHD;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;I)Ljava/io/File;
    .locals 11

    .prologue
    .line 2220018
    iget-object v0, p0, LX/FHD;->e:LX/FHb;

    invoke-virtual {v0, p1, p3}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;I)V

    .line 2220019
    iget-object v0, p0, LX/FHD;->f:LX/2Mn;

    .line 2220020
    const/4 v1, 0x2

    if-ne p3, v1, :cond_4

    .line 2220021
    :cond_0
    :goto_0
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 2220022
    iget v0, p2, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->b:I

    move v2, v0

    .line 2220023
    iget v0, p2, Lcom/facebook/messaging/media/photoquality/PhotoQuality;->c:I

    move v3, v0

    .line 2220024
    iget-object v0, p0, LX/FHD;->a:LX/2Nx;

    invoke-virtual {v0, v1, v2, v3}, LX/2Nx;->a(Landroid/net/Uri;II)Ljava/io/File;

    move-result-object v4

    .line 2220025
    if-eqz v4, :cond_1

    .line 2220026
    iget-object v0, p0, LX/FHD;->e:LX/FHb;

    const/4 v6, 0x1

    move-object v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v6}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;IILjava/io/File;IZ)V

    .line 2220027
    iget-object v0, p0, LX/FHD;->f:LX/2Mn;

    const/4 v6, 0x1

    move-object v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v6}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;IILjava/io/File;IZ)V

    .line 2220028
    :goto_1
    return-object v4

    .line 2220029
    :cond_1
    iget-object v0, p0, LX/FHD;->b:LX/CN2;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "media_upload"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ".jpg"

    sget-object v6, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual/range {v0 .. v6}, LX/CN2;->a(Landroid/net/Uri;IILjava/lang/String;Ljava/lang/String;LX/46h;)LX/CN4;

    move-result-object v0

    .line 2220030
    sget-object v4, LX/FHC;->a:[I

    iget-object v5, v0, LX/CN4;->a:LX/CN3;

    invoke-virtual {v5}, LX/CN3;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2220031
    iget-object v0, p0, LX/FHD;->d:LX/03V;

    const-string v1, "photo_resize_invalid_state"

    const-string v2, "Result state unknown"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2220032
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown resize state returned from PhotoResizeHelper.resizePhoto()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2220033
    :pswitch_0
    iget-object v8, v0, LX/CN4;->b:Ljava/io/File;

    .line 2220034
    iget-object v4, p0, LX/FHD;->e:LX/FHb;

    const/4 v10, 0x0

    move-object v5, p1

    move v6, v2

    move v7, v3

    move v9, p3

    invoke-virtual/range {v4 .. v10}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;IILjava/io/File;IZ)V

    .line 2220035
    iget-object v4, p0, LX/FHD;->f:LX/2Mn;

    const/4 v10, 0x0

    move-object v5, p1

    move v6, v2

    move v7, v3

    move v9, p3

    invoke-virtual/range {v4 .. v10}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;IILjava/io/File;IZ)V

    .line 2220036
    iget-object v0, p0, LX/FHD;->a:LX/2Nx;

    .line 2220037
    invoke-static {v0}, LX/2Nx;->a(LX/2Nx;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2220038
    const/4 v4, 0x0

    .line 2220039
    :goto_2
    move-object v0, v4

    .line 2220040
    if-eqz v0, :cond_2

    .line 2220041
    :try_start_0
    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    .line 2220042
    iget-object v0, p0, LX/FHD;->a:LX/2Nx;

    invoke-virtual {v0, v1, v2, v3}, LX/2Nx;->a(Landroid/net/Uri;II)Ljava/io/File;

    move-result-object v4

    .line 2220043
    if-eqz v4, :cond_2

    .line 2220044
    invoke-virtual {v8}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    :cond_2
    move-object v4, v8

    .line 2220045
    goto :goto_1

    .line 2220046
    :pswitch_1
    iget-object v2, p0, LX/FHD;->a:LX/2Nx;

    invoke-virtual {v2, v1}, LX/2Nx;->a(Landroid/net/Uri;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2220047
    iget-object v0, v0, LX/CN4;->c:Ljava/lang/Exception;

    .line 2220048
    iget-object v1, p0, LX/FHD;->g:LX/0ad;

    sget-short v2, LX/FGS;->p:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    iget-object v1, p0, LX/FHD;->g:LX/0ad;

    sget v4, LX/FGS;->q:I

    const v5, 0x17d7840

    invoke-interface {v1, v4, v5}, LX/0ad;->a(II)I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-gtz v1, :cond_3

    .line 2220049
    iget-object v1, p0, LX/FHD;->e:LX/FHb;

    invoke-virtual {v1, p1, p3, v0}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/Throwable;)V

    .line 2220050
    iget-object v1, p0, LX/FHD;->f:LX/2Mn;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, p3, v2, v0}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;IZLjava/lang/Throwable;)V

    .line 2220051
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 2220052
    :cond_3
    iget-object v1, p0, LX/FHD;->e:LX/FHb;

    invoke-virtual {v1, p1, p3, v0}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/Throwable;)V

    .line 2220053
    iget-object v1, p0, LX/FHD;->f:LX/2Mn;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p3, v2, v0}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;IZLjava/lang/Throwable;)V

    .line 2220054
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 2220055
    :cond_4
    invoke-static {p1}, LX/FHE;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;

    move-result-object v1

    .line 2220056
    iget-object v2, v0, LX/2Mn;->d:LX/0QI;

    invoke-interface {v2, v1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FHF;

    .line 2220057
    if-eqz v1, :cond_0

    .line 2220058
    iget-object v2, v1, LX/FHF;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2220059
    iget-object v1, v1, LX/FHF;->b:LX/0SW;

    .line 2220060
    const-string v3, "compression_start"

    invoke-static {v2, v3, v1}, LX/2Mn;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0SW;)V

    goto/16 :goto_0

    :cond_5
    invoke-static {v1, v2, v3}, LX/2Nx;->b(Landroid/net/Uri;II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "resized"

    invoke-static {v0, v4, v8, v5}, LX/2Nx;->a(LX/2Nx;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final cancelOperation(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2220017
    const/4 v0, 0x0

    return v0
.end method

.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2220013
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2220014
    const-string v1, "photo_transcode"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2220015
    invoke-direct {p0, p1}, LX/FHD;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2220016
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
