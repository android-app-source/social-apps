.class public final LX/Ge6;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$FetchPaginatedPYMLWithLargeImageItemsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2375038
    const-class v1, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$FetchPaginatedPYMLWithLargeImageItemsQueryModel;

    const v0, 0x74db3a0a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "FetchPaginatedPYMLWithLargeImageItemsQuery"

    const-string v6, "4ca5b3b3b7dd4311e9086f9357c22b86"

    const-string v7, "viewer"

    const-string v8, "10155255204831729"

    const/4 v9, 0x0

    .line 2375039
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2375040
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2375041
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2375042
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2375043
    sparse-switch v0, :sswitch_data_0

    .line 2375044
    :goto_0
    return-object p1

    .line 2375045
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2375046
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2375047
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2375048
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2375049
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2375050
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2375051
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2375052
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2375053
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2375054
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2375055
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2375056
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x513764de -> :sswitch_b
        -0x4aeac1fa -> :sswitch_a
        -0x41a91745 -> :sswitch_9
        -0xdbacfae -> :sswitch_2
        -0x6fe61e8 -> :sswitch_5
        0x5a7510f -> :sswitch_3
        0xa1fa812 -> :sswitch_1
        0xe50e2a0 -> :sswitch_6
        0x15888c51 -> :sswitch_0
        0x4a83e03c -> :sswitch_7
        0x5be9c255 -> :sswitch_8
        0x670b906a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2375057
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2375058
    :goto_1
    return v0

    .line 2375059
    :pswitch_0
    const-string v2, "11"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2375060
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x620
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
