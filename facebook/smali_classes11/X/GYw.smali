.class public LX/GYw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GYs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GYs",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/GY2;

.field private final b:LX/1Ck;

.field public final c:Ljava/lang/String;

.field public final d:LX/GYk;


# direct methods
.method public constructor <init>(LX/GY2;LX/1Ck;Ljava/lang/String;LX/GYk;)V
    .locals 0
    .param p2    # LX/1Ck;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/GYk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GY2;",
            "LX/1Ck;",
            "Ljava/lang/String;",
            "LX/GYk",
            "<",
            "Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2365838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2365839
    iput-object p1, p0, LX/GYw;->a:LX/GY2;

    .line 2365840
    iput-object p2, p0, LX/GYw;->b:LX/1Ck;

    .line 2365841
    iput-object p3, p0, LX/GYw;->c:Ljava/lang/String;

    .line 2365842
    iput-object p4, p0, LX/GYw;->d:LX/GYk;

    .line 2365843
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2365844
    iget-object v0, p0, LX/GYw;->b:LX/1Ck;

    const-class v1, LX/GYw;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GYw;->a:LX/GY2;

    iget-object v3, p0, LX/GYw;->c:Ljava/lang/String;

    .line 2365845
    new-instance v4, LX/7k7;

    invoke-direct {v4}, LX/7k7;-><init>()V

    move-object v4, v4

    .line 2365846
    const-string v5, "page_id"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/7k7;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2365847
    sget-object v5, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v4, v5}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    .line 2365848
    iget-object v5, v2, LX/GY2;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2365849
    new-instance v3, LX/GYv;

    invoke-direct {v3, p0}, LX/GYv;-><init>(LX/GYw;)V

    move-object v3, v3

    .line 2365850
    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2365851
    return-void
.end method
