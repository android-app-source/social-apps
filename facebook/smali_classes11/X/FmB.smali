.class public LX/FmB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qa;


# instance fields
.field public a:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/6r5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/privacy/PrivacyOperationsClient;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/6qb;

.field private e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2282147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282148
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 2282134
    iget-object v0, p0, LX/FmB;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2282135
    iget-object v0, p0, LX/FmB;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2282136
    :goto_0
    return-object v0

    .line 2282137
    :cond_0
    iget-object v0, p0, LX/FmB;->b:LX/6r5;

    invoke-virtual {v0, p1}, LX/6r5;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2282138
    iget-object v1, p0, LX/FmB;->c:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v2, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v1, v2}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2282139
    new-instance v2, LX/FmA;

    invoke-direct {v2, p0}, LX/FmA;-><init>(LX/FmB;)V

    iget-object p1, p0, LX/FmB;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2282140
    move-object v1, v1

    .line 2282141
    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->b(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/FmB;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2282142
    iget-object v0, p0, LX/FmB;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public final a(LX/6qb;)V
    .locals 2

    .prologue
    .line 2282144
    iput-object p1, p0, LX/FmB;->d:LX/6qb;

    .line 2282145
    iget-object v0, p0, LX/FmB;->b:LX/6r5;

    iget-object v1, p0, LX/FmB;->d:LX/6qb;

    invoke-virtual {v0, v1}, LX/6r5;->a(LX/6qb;)V

    .line 2282146
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2282143
    iget-object v0, p0, LX/FmB;->b:LX/6r5;

    invoke-virtual {v0}, LX/6r5;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FmB;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
