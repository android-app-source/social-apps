.class public final LX/GYr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Fd;


# instance fields
.field public final synthetic a:LX/GYt;


# direct methods
.method public constructor <init>(LX/GYt;)V
    .locals 0

    .prologue
    .line 2365814
    iput-object p1, p0, LX/GYr;->a:LX/GYt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2365815
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PageViewerContextLoadable failure. pageId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/GYr;->a:LX/GYt;

    iget-object v1, v1, LX/GYt;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2365816
    iget-object v1, p0, LX/GYr;->a:LX/GYt;

    iget-object v1, v1, LX/GYt;->c:LX/GYk;

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2, v0}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v0, v2}, LX/GYk;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2365817
    iget-object v0, p0, LX/GYr;->a:LX/GYt;

    iget-object v0, v0, LX/GYt;->a:LX/8Fe;

    invoke-virtual {v0}, LX/8Fe;->b()V

    .line 2365818
    return-void
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1
    .param p1    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365819
    iget-object v0, p0, LX/GYr;->a:LX/GYt;

    iget-object v0, v0, LX/GYt;->c:LX/GYk;

    invoke-interface {v0, p1}, LX/GYk;->a(Ljava/lang/Object;)V

    .line 2365820
    iget-object v0, p0, LX/GYr;->a:LX/GYt;

    iget-object v0, v0, LX/GYt;->a:LX/8Fe;

    invoke-virtual {v0}, LX/8Fe;->b()V

    .line 2365821
    return-void
.end method
