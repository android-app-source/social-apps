.class public LX/Fra;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/Fs2;

.field public final b:LX/BP0;

.field public final c:LX/BQB;

.field public final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final e:LX/G13;

.field private final f:LX/FsN;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FsG;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/concurrent/Executor;

.field private final i:Lcom/facebook/common/callercontext/CallerContext;

.field private final j:Landroid/content/Context;

.field private final k:LX/Fs3;

.field private final l:LX/Fwh;

.field private final m:LX/Fx5;

.field private n:Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BP0;LX/BQB;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/Fs3;LX/G13;LX/FsN;LX/0Or;Ljava/util/concurrent/Executor;LX/Fwh;LX/Fx5;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BQB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BP0;",
            "LX/BQB;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/Fs3;",
            "LX/G13;",
            "LX/FsN;",
            "LX/0Or",
            "<",
            "LX/FsG;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "LX/Fwh;",
            "LX/Fx5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2295827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2295828
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fra;->n:Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    .line 2295829
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fra;->o:Z

    .line 2295830
    iput-object p1, p0, LX/Fra;->j:Landroid/content/Context;

    .line 2295831
    iput-object p6, p0, LX/Fra;->k:LX/Fs3;

    .line 2295832
    iput-object p4, p0, LX/Fra;->i:Lcom/facebook/common/callercontext/CallerContext;

    .line 2295833
    iput-object p2, p0, LX/Fra;->b:LX/BP0;

    .line 2295834
    iput-object p3, p0, LX/Fra;->c:LX/BQB;

    .line 2295835
    iput-object p5, p0, LX/Fra;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2295836
    iput-object p7, p0, LX/Fra;->e:LX/G13;

    .line 2295837
    iput-object p8, p0, LX/Fra;->f:LX/FsN;

    .line 2295838
    iput-object p9, p0, LX/Fra;->g:LX/0Or;

    .line 2295839
    iput-object p10, p0, LX/Fra;->h:Ljava/util/concurrent/Executor;

    .line 2295840
    iput-object p11, p0, LX/Fra;->l:LX/Fwh;

    .line 2295841
    iput-object p12, p0, LX/Fra;->m:LX/Fx5;

    .line 2295842
    return-void
.end method

.method public static b(LX/Fra;LX/FrX;LX/36O;Lcom/facebook/common/callercontext/CallerContext;)LX/FrX;
    .locals 3
    .param p0    # LX/Fra;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/FrX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/36O;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2295817
    if-eqz p1, :cond_0

    .line 2295818
    :goto_0
    return-object p1

    .line 2295819
    :cond_0
    invoke-static {p0}, LX/Fra;->f(LX/Fra;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    move-result-object v0

    .line 2295820
    iget-object v1, p0, LX/Fra;->f:LX/FsN;

    .line 2295821
    new-instance v2, LX/0v6;

    const-string p0, "TimelineHeader"

    invoke-direct {v2, p0}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2295822
    invoke-virtual {v1, v2, v0, p2, p3}, LX/FsN;->a(LX/0v6;Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/36O;Lcom/facebook/common/callercontext/CallerContext;)LX/FsE;

    move-result-object p0

    .line 2295823
    iget-object p1, v1, LX/FsN;->c:LX/0tX;

    invoke-virtual {p1, v2}, LX/0tX;->a(LX/0v6;)V

    .line 2295824
    invoke-virtual {v1, p0}, LX/FsN;->a(LX/FsE;)V

    .line 2295825
    move-object v0, p0

    .line 2295826
    new-instance p1, LX/FrX;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, LX/FrX;-><init>(LX/FsE;LX/FsJ;)V

    goto :goto_0
.end method

.method public static e(LX/Fra;)LX/Fs2;
    .locals 10

    .prologue
    .line 2295843
    iget-object v0, p0, LX/Fra;->a:LX/Fs2;

    if-nez v0, :cond_0

    .line 2295844
    iget-object v0, p0, LX/Fra;->k:LX/Fs3;

    iget-object v1, p0, LX/Fra;->j:Landroid/content/Context;

    new-instance v2, LX/9lP;

    iget-object v3, p0, LX/Fra;->b:LX/BP0;

    .line 2295845
    iget-wide v8, v3, LX/5SB;->b:J

    move-wide v4, v8

    .line 2295846
    iget-object v3, p0, LX/Fra;->b:LX/BP0;

    .line 2295847
    iget-wide v8, v3, LX/5SB;->a:J

    move-wide v6, v8

    .line 2295848
    invoke-direct {v2, v4, v5, v6, v7}, LX/9lP;-><init>(JJ)V

    iget-object v3, p0, LX/Fra;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/Fs3;->a(Landroid/content/Context;LX/9lP;Lcom/facebook/common/callercontext/CallerContext;)LX/Fs2;

    move-result-object v0

    iput-object v0, p0, LX/Fra;->a:LX/Fs2;

    .line 2295849
    :cond_0
    iget-object v0, p0, LX/Fra;->a:LX/Fs2;

    return-object v0
.end method

.method private static f(LX/Fra;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;
    .locals 9

    .prologue
    .line 2295810
    iget-object v0, p0, LX/Fra;->n:Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    if-nez v0, :cond_0

    .line 2295811
    iget-object v1, p0, LX/Fra;->e:LX/G13;

    iget-object v0, p0, LX/Fra;->b:LX/BP0;

    .line 2295812
    iget-wide v7, v0, LX/5SB;->b:J

    move-wide v2, v7

    .line 2295813
    iget-object v0, p0, LX/Fra;->b:LX/BP0;

    .line 2295814
    iget-object v4, v0, LX/BP0;->b:LX/0am;

    move-object v4, v4

    .line 2295815
    iget-object v0, p0, LX/Fra;->l:LX/Fwh;

    invoke-virtual {v0}, LX/Fwh;->h()Z

    move-result v5

    iget-object v0, p0, LX/Fra;->m:LX/Fx5;

    invoke-virtual {v0}, LX/Fx5;->h()Z

    move-result v6

    invoke-virtual/range {v1 .. v6}, LX/G13;->a(JLX/0am;ZZ)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    move-result-object v0

    iput-object v0, p0, LX/Fra;->n:Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    .line 2295816
    :cond_0
    iget-object v0, p0, LX/Fra;->n:Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    return-object v0
.end method


# virtual methods
.method public final a(I)LX/FsE;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2295804
    iget-object v0, p0, LX/Fra;->c:LX/BQB;

    if-eqz v0, :cond_0

    .line 2295805
    iget-object v0, p0, LX/Fra;->c:LX/BQB;

    invoke-virtual {v0}, LX/BQB;->f()V

    .line 2295806
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 2295807
    iget-object v0, p0, LX/Fra;->f:LX/FsN;

    invoke-static {p0}, LX/Fra;->f(LX/Fra;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    move-result-object v1

    sget-object v2, LX/0zS;->d:LX/0zS;

    iget-object v3, p0, LX/Fra;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/FsN;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/1Zp;

    move-result-object v0

    .line 2295808
    :goto_0
    new-instance v1, LX/FsE;

    invoke-direct {v1, v4, v0, v4, v4}, LX/FsE;-><init>(LX/1Zp;LX/1Zp;LX/1Zp;LX/1Zp;)V

    return-object v1

    .line 2295809
    :cond_1
    iget-object v0, p0, LX/Fra;->f:LX/FsN;

    invoke-static {p0}, LX/Fra;->f(LX/Fra;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    move-result-object v1

    sget-object v2, LX/0zS;->b:LX/0zS;

    iget-object v3, p0, LX/Fra;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/FsN;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/1Zp;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2295801
    iget-boolean v0, p0, LX/Fra;->o:Z

    if-eqz v0, :cond_0

    .line 2295802
    :goto_0
    return-void

    .line 2295803
    :cond_0
    invoke-static {p0}, LX/Fra;->e(LX/Fra;)LX/Fs2;

    move-result-object v0

    invoke-virtual {v0}, LX/Fs2;->a()V

    goto :goto_0
.end method
