.class public LX/GiY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/GiX;",
        "Ljava/util/ArrayList",
        "<",
        "LX/GiZ;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2386052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2386053
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2386035
    check-cast p1, LX/GiX;

    .line 2386036
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2386037
    const-string v1, "fields"

    const-string v2, ","

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "hash"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "file_name"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2386038
    const-string v1, "only_pending"

    const-string v2, "true"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2386039
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "Get device library info from GLC"

    .line 2386040
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2386041
    move-object v1, v1

    .line 2386042
    const-string v2, "GET"

    .line 2386043
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2386044
    move-object v1, v1

    .line 2386045
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, LX/GiX;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/libs"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2386046
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2386047
    move-object v1, v1

    .line 2386048
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 2386049
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 2386050
    move-object v1, v1

    .line 2386051
    invoke-virtual {v1, v0}, LX/14O;->a(Ljava/util/Map;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2386054
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2386055
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2386056
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v2, "data"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2386057
    if-eqz v0, :cond_1

    .line 2386058
    invoke-virtual {v0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v2

    .line 2386059
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2386060
    new-instance v3, LX/GiZ;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-direct {v3, v0}, LX/GiZ;-><init>(LX/0lF;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 2386061
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
