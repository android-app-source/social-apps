.class public LX/H0i;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H0g;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Z

.field public d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/H0J;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 2414201
    new-instance v0, LX/H0g;

    sget-object v1, LX/H0h;->TITLE:LX/H0h;

    invoke-direct {v0, v1}, LX/H0g;-><init>(LX/H0h;)V

    new-instance v1, LX/H0g;

    sget-object v2, LX/H0h;->DESCRIPTION:LX/H0h;

    invoke-direct {v1, v2}, LX/H0g;-><init>(LX/H0h;)V

    new-instance v2, LX/H0g;

    sget-object v3, LX/H0h;->FIELD_LABEL:LX/H0h;

    const v4, 0x7f081518

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/H0g;-><init>(LX/H0h;Ljava/lang/Integer;)V

    new-instance v3, LX/H0g;

    sget-object v4, LX/H0h;->FIELD_EDIT_TEXT:LX/H0h;

    sget-object v5, LX/H0f;->TITLE:LX/H0f;

    invoke-virtual {v5}, LX/H0f;->toInt()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/H0g;-><init>(LX/H0h;Ljava/lang/Integer;)V

    new-instance v4, LX/H0g;

    sget-object v5, LX/H0h;->FIELD_LABEL:LX/H0h;

    const v6, 0x7f08151a

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {v4, v5, v6}, LX/H0g;-><init>(LX/H0h;Ljava/lang/Integer;)V

    new-instance v5, LX/H0g;

    sget-object v6, LX/H0h;->FIELD_EDIT_TEXT:LX/H0h;

    sget-object v7, LX/H0f;->DESCRIPTION:LX/H0f;

    invoke-virtual {v7}, LX/H0f;->toInt()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v5, v6, v7}, LX/H0g;-><init>(LX/H0h;Ljava/lang/Integer;)V

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/H0i;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2414197
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2414198
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/H0i;->c:Z

    .line 2414199
    iput-object p1, p0, LX/H0i;->b:Landroid/content/Context;

    .line 2414200
    return-void
.end method

.method public static c(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 2

    .prologue
    .line 2414184
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2414185
    if-eqz v0, :cond_0

    .line 2414186
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2414187
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 2414188
    const/16 v1, 0x28

    if-le v0, v1, :cond_0

    .line 2414189
    new-instance v0, LX/Gzs;

    const-string v1, "Form title field overflowed"

    invoke-direct {v0, v1}, LX/Gzs;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2414190
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2414191
    if-eqz v0, :cond_1

    .line 2414192
    iget-object v0, p0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2414193
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 2414194
    const/16 v1, 0xc8

    if-le v0, v1, :cond_1

    .line 2414195
    new-instance v0, LX/Gzs;

    const-string v1, "Form description field overflowed"

    invoke-direct {v0, v1}, LX/Gzs;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2414196
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2414202
    sget-object v0, LX/H0h;->TITLE:LX/H0h;

    invoke-virtual {v0}, LX/H0h;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2414203
    iget-object v0, p0, LX/H0i;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a8

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414204
    new-instance v0, LX/H0Q;

    invoke-direct {v0, v1}, LX/H0Q;-><init>(Landroid/view/View;)V

    .line 2414205
    :goto_0
    return-object v0

    .line 2414206
    :cond_0
    sget-object v0, LX/H0h;->DESCRIPTION:LX/H0h;

    invoke-virtual {v0}, LX/H0h;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2414207
    iget-object v0, p0, LX/H0i;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a6

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414208
    new-instance v0, LX/H0R;

    invoke-direct {v0, v1}, LX/H0R;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2414209
    :cond_1
    sget-object v0, LX/H0h;->FIELD_LABEL:LX/H0h;

    invoke-virtual {v0}, LX/H0h;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2414210
    iget-object v0, p0, LX/H0i;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a2

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414211
    new-instance v0, LX/H0R;

    invoke-direct {v0, v1}, LX/H0R;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2414212
    :cond_2
    sget-object v0, LX/H0h;->FIELD_EDIT_TEXT:LX/H0h;

    invoke-virtual {v0}, LX/H0h;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_3

    .line 2414213
    iget-object v0, p0, LX/H0i;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2414214
    new-instance v0, LX/H0d;

    invoke-direct {v0, p0, v1}, LX/H0d;-><init>(LX/H0i;Landroid/view/View;)V

    goto :goto_0

    .line 2414215
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2414144
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2414145
    sget-object v1, LX/H0h;->TITLE:LX/H0h;

    invoke-virtual {v1}, LX/H0h;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2414146
    check-cast p1, LX/H0Q;

    .line 2414147
    iget-object v0, p0, LX/H0i;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081516

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414148
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/H0Q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2414149
    :goto_0
    return-void

    .line 2414150
    :cond_0
    sget-object v1, LX/H0h;->DESCRIPTION:LX/H0h;

    invoke-virtual {v1}, LX/H0h;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2414151
    check-cast p1, LX/H0R;

    .line 2414152
    iget-object v0, p0, LX/H0i;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081517

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414153
    invoke-virtual {p1, v0}, LX/H0R;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2414154
    :cond_1
    sget-object v1, LX/H0h;->FIELD_LABEL:LX/H0h;

    invoke-virtual {v1}, LX/H0h;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 2414155
    check-cast p1, LX/H0R;

    .line 2414156
    sget-object v0, LX/H0i;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0g;

    iget-object v0, v0, LX/H0g;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2414157
    iget-object v1, p0, LX/H0i;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414158
    invoke-virtual {p1, v0}, LX/H0R;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2414159
    :cond_2
    sget-object v1, LX/H0h;->FIELD_EDIT_TEXT:LX/H0h;

    invoke-virtual {v1}, LX/H0h;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 2414160
    check-cast p1, LX/H0c;

    .line 2414161
    sget-object v0, LX/H0i;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0g;

    iget-object v0, v0, LX/H0g;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2414162
    iget-object v1, p0, LX/H0i;->d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    iget-boolean v2, p0, LX/H0i;->c:Z

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2414163
    sget-object p0, LX/H0f;->TITLE:LX/H0f;

    invoke-virtual {p0}, LX/H0f;->toInt()I

    move-result p0

    if-ne v0, p0, :cond_7

    .line 2414164
    sget-object p0, LX/H0f;->TITLE:LX/H0f;

    iput-object p0, p1, LX/H0c;->p:LX/H0f;

    .line 2414165
    iget-object p0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f081519

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2414166
    iget-object p2, v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    move-object p2, p2

    .line 2414167
    invoke-static {p1, p0, p2}, LX/H0c;->a(LX/H0c;Ljava/lang/String;Ljava/lang/String;)V

    .line 2414168
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    .line 2414169
    :cond_3
    const/16 p0, 0x28

    invoke-virtual {p1, v3, p0}, LX/H0c;->b(II)V

    .line 2414170
    if-eqz v2, :cond_6

    if-nez v3, :cond_6

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08152c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2414171
    :goto_1
    invoke-static {p1, v3}, LX/H0c;->a(LX/H0c;Ljava/lang/String;)V

    .line 2414172
    :cond_4
    :goto_2
    goto/16 :goto_0

    .line 2414173
    :cond_5
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid viewType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    move-object v3, v4

    .line 2414174
    goto :goto_1

    .line 2414175
    :cond_7
    sget-object p0, LX/H0f;->DESCRIPTION:LX/H0f;

    invoke-virtual {p0}, LX/H0f;->toInt()I

    move-result p0

    if-ne v0, p0, :cond_4

    .line 2414176
    sget-object p0, LX/H0f;->DESCRIPTION:LX/H0f;

    iput-object p0, p1, LX/H0c;->p:LX/H0f;

    .line 2414177
    iget-object p0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f08151b

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    .line 2414178
    iget-object p2, v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    move-object p2, p2

    .line 2414179
    invoke-static {p1, p0, p2}, LX/H0c;->a(LX/H0c;Ljava/lang/String;Ljava/lang/String;)V

    .line 2414180
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    .line 2414181
    :cond_8
    const/16 p0, 0xc8

    invoke-virtual {p1, v3, p0}, LX/H0c;->b(II)V

    .line 2414182
    if-eqz v2, :cond_9

    if-nez v3, :cond_9

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08152c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2414183
    :cond_9
    invoke-static {p1, v4}, LX/H0c;->a(LX/H0c;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final b(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2414122
    iget-object v0, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2414123
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414124
    const-string v0, ""

    .line 2414125
    iput-object v0, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    .line 2414126
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2414127
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2414128
    const-string v0, ""

    .line 2414129
    iput-object v0, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    .line 2414130
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2414131
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2414132
    iput-boolean v1, p0, LX/H0i;->c:Z

    .line 2414133
    iget-object v0, p0, LX/H0i;->e:LX/H0J;

    if-eqz v0, :cond_2

    .line 2414134
    iget-object v0, p0, LX/H0i;->e:LX/H0J;

    invoke-virtual {v0}, LX/H0J;->a()V

    .line 2414135
    :cond_2
    new-instance v0, LX/Gzr;

    const-string v1, "Form title is required"

    invoke-direct {v0, v1}, LX/Gzr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2414136
    :cond_3
    iget-object v0, p1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2414137
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2414138
    iput-boolean v1, p0, LX/H0i;->c:Z

    .line 2414139
    iget-object v0, p0, LX/H0i;->e:LX/H0J;

    if-eqz v0, :cond_4

    .line 2414140
    iget-object v0, p0, LX/H0i;->e:LX/H0J;

    invoke-virtual {v0}, LX/H0J;->a()V

    .line 2414141
    :cond_4
    new-instance v0, LX/Gzr;

    const-string v1, "Form description is required"

    invoke-direct {v0, v1}, LX/Gzr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2414142
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/H0i;->c:Z

    .line 2414143
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2414121
    sget-object v0, LX/H0i;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0g;

    iget-object v0, v0, LX/H0g;->a:LX/H0h;

    invoke-virtual {v0}, LX/H0h;->toInt()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2414118
    iget-object v0, p0, LX/H0i;->d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    if-nez v0, :cond_0

    .line 2414119
    const/4 v0, 0x0

    .line 2414120
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/H0i;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
