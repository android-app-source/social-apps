.class public LX/Gjt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/view/Window;


# direct methods
.method public constructor <init>(Landroid/view/Window;)V
    .locals 0

    .prologue
    .line 2388567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2388568
    iput-object p1, p0, LX/Gjt;->a:Landroid/view/Window;

    .line 2388569
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 2388570
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 2388571
    iget-object v0, p0, LX/Gjt;->a:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x1706

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 2388572
    :goto_0
    return-void

    .line 2388573
    :cond_0
    iget-object v0, p0, LX/Gjt;->a:Landroid/view/Window;

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 2388574
    iget-object v0, p0, LX/Gjt;->a:Landroid/view/Window;

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/16 v2, 0x800

    .line 2388575
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 2388576
    iget-object v0, p0, LX/Gjt;->a:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 2388577
    :goto_0
    return-void

    .line 2388578
    :cond_0
    iget-object v0, p0, LX/Gjt;->a:Landroid/view/Window;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 2388579
    iget-object v0, p0, LX/Gjt;->a:Landroid/view/Window;

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    goto :goto_0
.end method
