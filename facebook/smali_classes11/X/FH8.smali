.class public final enum LX/FH8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FH8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FH8;

.field public static final enum NOT_REQUIRED:LX/FH8;

.field public static final enum SKIPPED_FROM_CACHE:LX/FH8;

.field public static final enum SKIPPED_FROM_SERVER:LX/FH8;

.field public static final enum UPLOAD:LX/FH8;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2219870
    new-instance v0, LX/FH8;

    const-string v1, "NOT_REQUIRED"

    invoke-direct {v0, v1, v2}, LX/FH8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FH8;->NOT_REQUIRED:LX/FH8;

    .line 2219871
    new-instance v0, LX/FH8;

    const-string v1, "SKIPPED_FROM_CACHE"

    invoke-direct {v0, v1, v3}, LX/FH8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FH8;->SKIPPED_FROM_CACHE:LX/FH8;

    .line 2219872
    new-instance v0, LX/FH8;

    const-string v1, "SKIPPED_FROM_SERVER"

    invoke-direct {v0, v1, v4}, LX/FH8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FH8;->SKIPPED_FROM_SERVER:LX/FH8;

    .line 2219873
    new-instance v0, LX/FH8;

    const-string v1, "UPLOAD"

    invoke-direct {v0, v1, v5}, LX/FH8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FH8;->UPLOAD:LX/FH8;

    .line 2219874
    const/4 v0, 0x4

    new-array v0, v0, [LX/FH8;

    sget-object v1, LX/FH8;->NOT_REQUIRED:LX/FH8;

    aput-object v1, v0, v2

    sget-object v1, LX/FH8;->SKIPPED_FROM_CACHE:LX/FH8;

    aput-object v1, v0, v3

    sget-object v1, LX/FH8;->SKIPPED_FROM_SERVER:LX/FH8;

    aput-object v1, v0, v4

    sget-object v1, LX/FH8;->UPLOAD:LX/FH8;

    aput-object v1, v0, v5

    sput-object v0, LX/FH8;->$VALUES:[LX/FH8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2219875
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FH8;
    .locals 1

    .prologue
    .line 2219876
    const-class v0, LX/FH8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FH8;

    return-object v0
.end method

.method public static values()[LX/FH8;
    .locals 1

    .prologue
    .line 2219877
    sget-object v0, LX/FH8;->$VALUES:[LX/FH8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FH8;

    return-object v0
.end method
