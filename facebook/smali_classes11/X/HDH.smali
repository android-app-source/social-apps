.class public final enum LX/HDH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HDH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HDH;

.field public static final enum ADD:LX/HDH;

.field public static final enum EDIT:LX/HDH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2441081
    new-instance v0, LX/HDH;

    const-string v1, "EDIT"

    invoke-direct {v0, v1, v2}, LX/HDH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HDH;->EDIT:LX/HDH;

    .line 2441082
    new-instance v0, LX/HDH;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v3}, LX/HDH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HDH;->ADD:LX/HDH;

    .line 2441083
    const/4 v0, 0x2

    new-array v0, v0, [LX/HDH;

    sget-object v1, LX/HDH;->EDIT:LX/HDH;

    aput-object v1, v0, v2

    sget-object v1, LX/HDH;->ADD:LX/HDH;

    aput-object v1, v0, v3

    sput-object v0, LX/HDH;->$VALUES:[LX/HDH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2441086
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HDH;
    .locals 1

    .prologue
    .line 2441085
    const-class v0, LX/HDH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HDH;

    return-object v0
.end method

.method public static values()[LX/HDH;
    .locals 1

    .prologue
    .line 2441084
    sget-object v0, LX/HDH;->$VALUES:[LX/HDH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HDH;

    return-object v0
.end method
