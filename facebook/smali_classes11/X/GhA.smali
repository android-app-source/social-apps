.class public final LX/GhA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 2383480
    const/16 v16, 0x0

    .line 2383481
    const/4 v15, 0x0

    .line 2383482
    const/4 v14, 0x0

    .line 2383483
    const/4 v11, 0x0

    .line 2383484
    const-wide/16 v12, 0x0

    .line 2383485
    const/4 v10, 0x0

    .line 2383486
    const/4 v9, 0x0

    .line 2383487
    const/4 v8, 0x0

    .line 2383488
    const/4 v7, 0x0

    .line 2383489
    const/4 v6, 0x0

    .line 2383490
    const/4 v5, 0x0

    .line 2383491
    const/4 v4, 0x0

    .line 2383492
    const/4 v3, 0x0

    .line 2383493
    const/4 v2, 0x0

    .line 2383494
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_10

    .line 2383495
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2383496
    const/4 v2, 0x0

    .line 2383497
    :goto_0
    return v2

    .line 2383498
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_d

    .line 2383499
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2383500
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2383501
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v19, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    if-eq v6, v0, :cond_0

    if-eqz v2, :cond_0

    .line 2383502
    const-string v6, "acting_team"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2383503
    invoke-static/range {p0 .. p1}, LX/9pS;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2383504
    :cond_1
    const-string v6, "acting_team_short_name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2383505
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 2383506
    :cond_2
    const-string v6, "fact_clock"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2383507
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 2383508
    :cond_3
    const-string v6, "fact_message"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2383509
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v7, v2

    goto :goto_1

    .line 2383510
    :cond_4
    const-string v6, "fact_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2383511
    const/4 v2, 0x1

    .line 2383512
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2383513
    :cond_5
    const-string v6, "feedback"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2383514
    invoke-static/range {p0 .. p1}, LX/9pQ;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 2383515
    :cond_6
    const-string v6, "header_text"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2383516
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2383517
    :cond_7
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2383518
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2383519
    :cond_8
    const-string v6, "preview_comment"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2383520
    invoke-static/range {p0 .. p1}, LX/9pV;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2383521
    :cond_9
    const-string v6, "score_text"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2383522
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 2383523
    :cond_a
    const-string v6, "sequence_number"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2383524
    const/4 v2, 0x1

    .line 2383525
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v10, v6

    goto/16 :goto_1

    .line 2383526
    :cond_b
    const-string v6, "story"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2383527
    invoke-static/range {p0 .. p1}, LX/9pX;->a(LX/15w;LX/186;)I

    move-result v2

    move v9, v2

    goto/16 :goto_1

    .line 2383528
    :cond_c
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2383529
    :cond_d
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2383530
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2383531
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2383532
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2383533
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2383534
    if-eqz v3, :cond_e

    .line 2383535
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2383536
    :cond_e
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2383537
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2383538
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2383539
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2383540
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2383541
    if-eqz v8, :cond_f

    .line 2383542
    const/16 v2, 0xa

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10, v3}, LX/186;->a(III)V

    .line 2383543
    :cond_f
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2383544
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_10
    move/from16 v17, v15

    move/from16 v18, v16

    move v15, v10

    move/from16 v16, v14

    move v14, v9

    move v10, v5

    move v9, v4

    move-wide v4, v12

    move v13, v8

    move v12, v7

    move v8, v2

    move v7, v11

    move v11, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2383545
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2383546
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2383547
    if-eqz v0, :cond_0

    .line 2383548
    const-string v1, "acting_team"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383549
    invoke-static {p0, v0, p2, p3}, LX/9pS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383550
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383551
    if-eqz v0, :cond_1

    .line 2383552
    const-string v1, "acting_team_short_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383553
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383554
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383555
    if-eqz v0, :cond_2

    .line 2383556
    const-string v1, "fact_clock"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383557
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383558
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383559
    if-eqz v0, :cond_3

    .line 2383560
    const-string v1, "fact_message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383561
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383562
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2383563
    cmp-long v2, v0, v4

    if-eqz v2, :cond_4

    .line 2383564
    const-string v2, "fact_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383565
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2383566
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383567
    if-eqz v0, :cond_5

    .line 2383568
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383569
    invoke-static {p0, v0, p2, p3}, LX/9pQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383570
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383571
    if-eqz v0, :cond_6

    .line 2383572
    const-string v1, "header_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383573
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383574
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383575
    if-eqz v0, :cond_7

    .line 2383576
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383577
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383578
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383579
    if-eqz v0, :cond_8

    .line 2383580
    const-string v1, "preview_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383581
    invoke-static {p0, v0, p2, p3}, LX/9pV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383582
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383583
    if-eqz v0, :cond_9

    .line 2383584
    const-string v1, "score_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383585
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383586
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2383587
    if-eqz v0, :cond_a

    .line 2383588
    const-string v1, "sequence_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383589
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2383590
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383591
    if-eqz v0, :cond_b

    .line 2383592
    const-string v1, "story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383593
    invoke-static {p0, v0, p2, p3}, LX/9pX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383594
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2383595
    return-void
.end method
