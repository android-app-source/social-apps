.class public LX/FuS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1DZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1DZ",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0qn;


# direct methods
.method public constructor <init>(LX/0qn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300009
    iput-object p1, p0, LX/FuS;->a:LX/0qn;

    .line 2300010
    return-void
.end method

.method public static a(LX/0QB;)LX/FuS;
    .locals 2

    .prologue
    .line 2300011
    new-instance v1, LX/FuS;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-direct {v1, v0}, LX/FuS;-><init>(LX/0qn;)V

    .line 2300012
    move-object v0, v1

    .line 2300013
    return-object v0
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2300014
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2300015
    instance-of v0, p0, LX/G54;

    if-eqz v0, :cond_0

    instance-of v0, p1, LX/G54;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 8
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 2300016
    invoke-static {p1, p2}, LX/FuS;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2300017
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2300018
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 2300019
    const/4 v2, 0x0

    .line 2300020
    iget-object v3, p0, LX/FuS;->a:LX/0qn;

    invoke-virtual {v3, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, LX/FuS;->a:LX/0qn;

    invoke-virtual {v3, p2}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v3, v4, :cond_6

    .line 2300021
    :cond_0
    :goto_0
    move v1, v2

    .line 2300022
    :goto_1
    move v0, v1

    .line 2300023
    :goto_2
    return v0

    :cond_1
    invoke-static {p1, p2}, LX/FuS;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2300024
    instance-of v0, p1, LX/G55;

    if-eqz v0, :cond_8

    instance-of v0, p2, LX/G55;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2300025
    if-nez v0, :cond_2

    .line 2300026
    instance-of v0, p1, LX/G4z;

    if-eqz v0, :cond_9

    instance-of v0, p2, LX/G4z;

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2300027
    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 2300028
    :cond_4
    if-eq p1, p2, :cond_5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_5
    const/4 v1, 0x1

    :goto_5
    move v1, v1

    .line 2300029
    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_7
    const/4 v1, 0x0

    goto :goto_5

    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;J)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;J)Z"
        }
    .end annotation

    .prologue
    .line 2300030
    invoke-static {p1, p2}, LX/FuS;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2300031
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 2300032
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2300033
    :cond_0
    :goto_0
    move v0, v1

    .line 2300034
    :goto_1
    return v0

    .line 2300035
    :cond_1
    invoke-static {p1, p2}, LX/FuS;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2300036
    check-cast p1, LX/G54;

    check-cast p2, LX/G54;

    .line 2300037
    iget-object v0, p1, LX/G54;->a:LX/G58;

    iget-object v1, p2, LX/G54;->a:LX/G58;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2300038
    goto :goto_1

    .line 2300039
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v3

    cmp-long v2, v3, p3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method
