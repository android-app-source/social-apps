.class public final synthetic LX/GG4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I

.field public static final synthetic e:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2332651
    invoke-static {}, LX/GGG;->values()[LX/GGG;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GG4;->e:[I

    :try_start_0
    sget-object v0, LX/GG4;->e:[I

    sget-object v1, LX/GGG;->REGION:LX/GGG;

    invoke-virtual {v1}, LX/GGG;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_23

    :goto_0
    :try_start_1
    sget-object v0, LX/GG4;->e:[I

    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    invoke-virtual {v1}, LX/GGG;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_22

    .line 2332652
    :goto_1
    invoke-static {}, LX/GGB;->values()[LX/GGB;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GG4;->d:[I

    :try_start_2
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_21

    :goto_2
    :try_start_3
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_20

    :goto_3
    :try_start_4
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1f

    :goto_4
    :try_start_5
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->PAUSED:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1e

    :goto_5
    :try_start_6
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->FINISHED:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1d

    :goto_6
    :try_start_7
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->EXTENDABLE:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1c

    :goto_7
    :try_start_8
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->REJECTED:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1b

    :goto_8
    :try_start_9
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1a

    :goto_9
    :try_start_a
    sget-object v0, LX/GG4;->d:[I

    sget-object v1, LX/GGB;->ERROR:LX/GGB;

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_19

    .line 2332653
    :goto_a
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->values()[Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GG4;->c:[I

    :try_start_b
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_18

    :goto_b
    :try_start_c
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_17

    :goto_c
    :try_start_d
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_16

    :goto_d
    :try_start_e
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_15

    :goto_e
    :try_start_f
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_14

    :goto_f
    :try_start_10
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_13

    :goto_10
    :try_start_11
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_12

    :goto_11
    :try_start_12
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_11

    :goto_12
    :try_start_13
    sget-object v0, LX/GG4;->c:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_10

    .line 2332654
    :goto_13
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->values()[Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GG4;->b:[I

    :try_start_14
    sget-object v0, LX/GG4;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_f

    :goto_14
    :try_start_15
    sget-object v0, LX/GG4;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_e

    :goto_15
    :try_start_16
    sget-object v0, LX/GG4;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_d

    :goto_16
    :try_start_17
    sget-object v0, LX/GG4;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_c

    :goto_17
    :try_start_18
    sget-object v0, LX/GG4;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_b

    :goto_18
    :try_start_19
    sget-object v0, LX/GG4;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_a

    :goto_19
    :try_start_1a
    sget-object v0, LX/GG4;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_9

    :goto_1a
    :try_start_1b
    sget-object v0, LX/GG4;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_8

    .line 2332655
    :goto_1b
    invoke-static {}, LX/8wL;->values()[LX/8wL;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GG4;->a:[I

    :try_start_1c
    sget-object v0, LX/GG4;->a:[I

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_7

    :goto_1c
    :try_start_1d
    sget-object v0, LX/GG4;->a:[I

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_6

    :goto_1d
    :try_start_1e
    sget-object v0, LX/GG4;->a:[I

    sget-object v1, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_5

    :goto_1e
    :try_start_1f
    sget-object v0, LX/GG4;->a:[I

    sget-object v1, LX/8wL;->PROMOTE_CTA:LX/8wL;

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_4

    :goto_1f
    :try_start_20
    sget-object v0, LX/GG4;->a:[I

    sget-object v1, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_3

    :goto_20
    :try_start_21
    sget-object v0, LX/GG4;->a:[I

    sget-object v1, LX/8wL;->BOOST_LIVE:LX/8wL;

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_2

    :goto_21
    :try_start_22
    sget-object v0, LX/GG4;->a:[I

    sget-object v1, LX/8wL;->PAGE_LIKE:LX/8wL;

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_1

    :goto_22
    :try_start_23
    sget-object v0, LX/GG4;->a:[I

    sget-object v1, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_0

    :goto_23
    return-void

    :catch_0
    goto :goto_23

    :catch_1
    goto :goto_22

    :catch_2
    goto :goto_21

    :catch_3
    goto :goto_20

    :catch_4
    goto :goto_1f

    :catch_5
    goto :goto_1e

    :catch_6
    goto :goto_1d

    :catch_7
    goto :goto_1c

    :catch_8
    goto :goto_1b

    :catch_9
    goto :goto_1a

    :catch_a
    goto/16 :goto_19

    :catch_b
    goto/16 :goto_18

    :catch_c
    goto/16 :goto_17

    :catch_d
    goto/16 :goto_16

    :catch_e
    goto/16 :goto_15

    :catch_f
    goto/16 :goto_14

    :catch_10
    goto/16 :goto_13

    :catch_11
    goto/16 :goto_12

    :catch_12
    goto/16 :goto_11

    :catch_13
    goto/16 :goto_10

    :catch_14
    goto/16 :goto_f

    :catch_15
    goto/16 :goto_e

    :catch_16
    goto/16 :goto_d

    :catch_17
    goto/16 :goto_c

    :catch_18
    goto/16 :goto_b

    :catch_19
    goto/16 :goto_a

    :catch_1a
    goto/16 :goto_9

    :catch_1b
    goto/16 :goto_8

    :catch_1c
    goto/16 :goto_7

    :catch_1d
    goto/16 :goto_6

    :catch_1e
    goto/16 :goto_5

    :catch_1f
    goto/16 :goto_4

    :catch_20
    goto/16 :goto_3

    :catch_21
    goto/16 :goto_2

    :catch_22
    goto/16 :goto_1

    :catch_23
    goto/16 :goto_0
.end method
