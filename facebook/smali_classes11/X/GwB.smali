.class public LX/GwB;
.super LX/4hr;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4hr",
        "<",
        "LX/Gwa;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static d:LX/0Xm;


# instance fields
.field private final c:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2406737
    const-class v0, LX/GwB;

    sput-object v0, LX/GwB;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Or;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/Gwa;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2406734
    const v0, 0x10004

    invoke-direct {p0, p1, v0}, LX/4hr;-><init>(LX/0Or;I)V

    .line 2406735
    iput-object p2, p0, LX/GwB;->c:Landroid/content/Context;

    .line 2406736
    return-void
.end method

.method public static a(LX/0QB;)LX/GwB;
    .locals 5

    .prologue
    .line 2406698
    const-class v1, LX/GwB;

    monitor-enter v1

    .line 2406699
    :try_start_0
    sget-object v0, LX/GwB;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2406700
    sput-object v2, LX/GwB;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2406701
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2406702
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2406703
    new-instance v4, LX/GwB;

    const/16 v3, 0x25b6

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, p0, v3}, LX/GwB;-><init>(LX/0Or;Landroid/content/Context;)V

    .line 2406704
    move-object v0, v4

    .line 2406705
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2406706
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GwB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2406707
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2406708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Landroid/os/Message;LX/4ht;)V
    .locals 10

    .prologue
    .line 2406709
    check-cast p2, LX/Gwa;

    .line 2406710
    iget-object v0, p2, LX/Gwa;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2406711
    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    if-eqz v1, :cond_1

    .line 2406712
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 2406713
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 2406714
    const v2, 0x10005

    iput v2, v1, Landroid/os/Message;->what:I

    .line 2406715
    move-object v1, v1

    .line 2406716
    iget-object v2, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 2406717
    iget-object v3, p0, LX/GwB;->c:Landroid/content/Context;

    .line 2406718
    invoke-static {v3}, LX/6Xe;->a(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v4

    .line 2406719
    :try_start_0
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/json/JSONObject;

    .line 2406720
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2406721
    const-string v6, "com.facebook.platform.APPLINK_ARGS"

    const-string v7, "com.facebook.platform.APPLINK_ARGS"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406722
    const-string v6, "com.facebook.platform.APPLINK_TAP_TIME_UTC"

    const-string v7, "com.facebook.platform.APPLINK_TAP_TIME_UTC"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v4, v5

    .line 2406723
    :goto_0
    invoke-static {v3}, LX/6Xe;->a(Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v5

    .line 2406724
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2406725
    invoke-static {v3}, LX/6Xe;->b(Landroid/content/Context;)V

    .line 2406726
    move-object v0, v4

    .line 2406727
    if-eqz v0, :cond_0

    .line 2406728
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 2406729
    :cond_0
    :try_start_1
    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2406730
    :cond_1
    :goto_1
    return-void

    .line 2406731
    :catch_0
    move-exception v0

    .line 2406732
    sget-object v1, LX/GwB;->b:Ljava/lang/Class;

    const-string v2, "Unable to respond to get install data request"

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2406733
    :catch_1
    const/4 v4, 0x0

    goto :goto_0
.end method
