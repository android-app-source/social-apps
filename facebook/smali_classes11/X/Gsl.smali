.class public final LX/Gsl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/Gsm;


# direct methods
.method public constructor <init>(LX/Gsm;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2400157
    iput-object p1, p0, LX/Gsl;->b:LX/Gsm;

    iput-object p2, p0, LX/Gsl;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 2400158
    iget-object v0, p0, LX/Gsl;->b:LX/Gsm;

    iget-object v0, v0, LX/Gsm;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ps;

    .line 2400159
    invoke-virtual {v0}, LX/0ps;->c()Lcom/facebook/device/resourcemonitor/DataUsageInfo;

    move-result-object v3

    .line 2400160
    invoke-virtual {v0}, LX/0ps;->d()V

    .line 2400161
    invoke-virtual {v3}, Lcom/facebook/device/resourcemonitor/DataUsageInfo;->a()J

    .line 2400162
    iget-object v0, p0, LX/Gsl;->a:Landroid/content/Context;

    const-string v1, "Data usage reset"

    const v2, 0x1080027

    const-string v4, "Usage up to this point: %d kb"

    invoke-virtual {v3}, Lcom/facebook/device/resourcemonitor/DataUsageInfo;->a()J

    move-result-wide v6

    const-wide/16 v10, 0x400

    div-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Gsl;->a:Landroid/content/Context;

    const v6, 0x7f080036

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-static/range {v0 .. v9}, LX/2Tz;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Z)LX/2EJ;

    move-result-object v0

    .line 2400163
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 2400164
    return v9
.end method
