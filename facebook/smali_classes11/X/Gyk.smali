.class public final LX/Gyk;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Landroid/support/v4/app/DialogFragment;

.field public final synthetic c:Lcom/facebook/location/ui/LocationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/location/ui/LocationSettingsFragment;ZLandroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 2410346
    iput-object p1, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    iput-boolean p2, p0, LX/Gyk;->a:Z

    iput-object p3, p0, LX/Gyk;->b:Landroid/support/v4/app/DialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2410324
    iget-boolean v0, p0, LX/Gyk;->a:Z

    if-nez v0, :cond_2

    move v0, v1

    .line 2410325
    :goto_0
    iget-object v3, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v3, v0}, Lcom/facebook/location/ui/LocationSettingsFragment;->c$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V

    .line 2410326
    iget-object v3, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v3}, Lcom/facebook/location/ui/LocationSettingsFragment;->m(Lcom/facebook/location/ui/LocationSettingsFragment;)Z

    move-result v3

    .line 2410327
    iget-object v4, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v4, v4, Lcom/facebook/location/ui/LocationSettingsFragment;->y:Lcom/facebook/widget/BetterSwitch;

    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-virtual {v4, v2}, Lcom/facebook/widget/BetterSwitch;->setEnabled(Z)V

    .line 2410328
    iget-object v1, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v1, v0, v3}, Lcom/facebook/location/ui/LocationSettingsFragment;->a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;ZZ)V

    .line 2410329
    iget-object v0, p0, LX/Gyk;->b:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2410330
    iget-object v0, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->i:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2410331
    return-void

    :cond_2
    move v0, v2

    .line 2410332
    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2410333
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2410334
    iget-boolean v0, p0, LX/Gyk;->a:Z

    if-nez v0, :cond_0

    .line 2410335
    iget-object v0, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    .line 2410336
    iput-boolean v2, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->J:Z

    .line 2410337
    :cond_0
    iget-object v0, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-boolean v3, p0, LX/Gyk;->a:Z

    invoke-static {v3}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    .line 2410338
    iput-object v3, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->K:LX/03R;

    .line 2410339
    iget-object v0, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v0}, Lcom/facebook/location/ui/LocationSettingsFragment;->m(Lcom/facebook/location/ui/LocationSettingsFragment;)Z

    move-result v3

    .line 2410340
    iget-object v4, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-boolean v0, p0, LX/Gyk;->a:Z

    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v4, v0}, Lcom/facebook/location/ui/LocationSettingsFragment;->b(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V

    .line 2410341
    iget-object v0, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->y:Lcom/facebook/widget/BetterSwitch;

    iget-boolean v4, p0, LX/Gyk;->a:Z

    if-nez v4, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    invoke-virtual {v0, v2}, Lcom/facebook/widget/BetterSwitch;->setEnabled(Z)V

    .line 2410342
    iget-object v0, p0, LX/Gyk;->c:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-boolean v1, p0, LX/Gyk;->a:Z

    invoke-static {v0, v1, v3}, Lcom/facebook/location/ui/LocationSettingsFragment;->a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;ZZ)V

    .line 2410343
    iget-object v0, p0, LX/Gyk;->b:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2410344
    return-void

    :cond_3
    move v0, v2

    .line 2410345
    goto :goto_0
.end method
