.class public final LX/G2w;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/G2o;

.field public final synthetic c:LX/1Pc;

.field public final synthetic d:LX/2dx;

.field public final synthetic e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;Ljava/util/List;LX/G2o;LX/1Pc;LX/2dx;)V
    .locals 0

    .prologue
    .line 2314671
    iput-object p1, p0, LX/G2w;->e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iput-object p2, p0, LX/G2w;->a:Ljava/util/List;

    iput-object p3, p0, LX/G2w;->b:LX/G2o;

    iput-object p4, p0, LX/G2w;->c:LX/1Pc;

    iput-object p5, p0, LX/G2w;->d:LX/2dx;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2314672
    const/4 v0, 0x0

    iget-object v1, p0, LX/G2w;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2314673
    iget-object v0, p0, LX/G2w;->e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v3, v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->f:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowPagePartDefinition;

    new-instance v4, LX/G32;

    iget-object v0, p0, LX/G2w;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    iget-object v5, p0, LX/G2w;->e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v6, p0, LX/G2w;->b:LX/G2o;

    iget-object v7, p0, LX/G2w;->c:LX/1Pc;

    .line 2314674
    new-instance v9, LX/G2x;

    invoke-direct {v9, v5, v6, v1, v7}, LX/G2x;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;LX/G2o;ILX/1Pc;)V

    move-object v5, v9

    .line 2314675
    iget-object v6, p0, LX/G2w;->d:LX/2dx;

    iget-object v7, p0, LX/G2w;->b:LX/G2o;

    invoke-direct {v4, v0, v5, v6, v7}, LX/G32;-><init>(Lcom/facebook/friends/model/PersonYouMayKnow;LX/G2x;LX/2dx;LX/0jW;)V

    invoke-virtual {p1, v3, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2314676
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2314677
    :cond_0
    iget-object v0, p0, LX/G2w;->b:LX/G2o;

    .line 2314678
    iget-object v1, v0, LX/G2o;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-object v0, v1

    .line 2314679
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2314680
    iget-object v0, p0, LX/G2w;->e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->h:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowLoadingPartDefinition;

    invoke-virtual {p1, v0, v8}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2314681
    :goto_1
    return-void

    .line 2314682
    :cond_1
    iget-object v0, p0, LX/G2w;->e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->g:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSeeAllPartDefinition;

    invoke-virtual {p1, v0, v8}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final c(I)V
    .locals 7

    .prologue
    .line 2314683
    iget-object v0, p0, LX/G2w;->e:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v1, p0, LX/G2w;->b:LX/G2o;

    iget-object v2, p0, LX/G2w;->c:LX/1Pc;

    iget-object v3, p0, LX/G2w;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 2314684
    add-int/lit8 v4, v3, -0x3

    if-lt p1, v4, :cond_0

    .line 2314685
    iget-object v4, v1, LX/G2o;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-object v4, v4

    .line 2314686
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2314687
    :cond_0
    :goto_0
    return-void

    .line 2314688
    :cond_1
    iget-object v4, v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->k:LX/1Ck;

    const-string v5, "FETCH_PYMK_TASK_KEY"

    new-instance v6, LX/G2y;

    invoke-direct {v6, v0, v1}, LX/G2y;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;LX/G2o;)V

    new-instance p0, LX/G2z;

    invoke-direct {p0, v0, v1, v2}, LX/G2z;-><init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;LX/G2o;LX/1Pc;)V

    invoke-virtual {v4, v5, v6, p0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
