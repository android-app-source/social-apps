.class public LX/FWJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FWJ;


# instance fields
.field public final a:LX/1nG;

.field private final b:LX/FWK;

.field public final c:LX/0SG;


# direct methods
.method public constructor <init>(LX/1nG;LX/FWK;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252008
    iput-object p1, p0, LX/FWJ;->a:LX/1nG;

    .line 2252009
    iput-object p2, p0, LX/FWJ;->b:LX/FWK;

    .line 2252010
    iput-object p3, p0, LX/FWJ;->c:LX/0SG;

    .line 2252011
    return-void
.end method

.method public static a(LX/0QB;)LX/FWJ;
    .locals 6

    .prologue
    .line 2251994
    sget-object v0, LX/FWJ;->d:LX/FWJ;

    if-nez v0, :cond_1

    .line 2251995
    const-class v1, LX/FWJ;

    monitor-enter v1

    .line 2251996
    :try_start_0
    sget-object v0, LX/FWJ;->d:LX/FWJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2251997
    if-eqz v2, :cond_0

    .line 2251998
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2251999
    new-instance p0, LX/FWJ;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v3

    check-cast v3, LX/1nG;

    invoke-static {v0}, LX/FWK;->a(LX/0QB;)LX/FWK;

    move-result-object v4

    check-cast v4, LX/FWK;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/FWJ;-><init>(LX/1nG;LX/FWK;LX/0SG;)V

    .line 2252000
    move-object v0, p0

    .line 2252001
    sput-object v0, LX/FWJ;->d:LX/FWJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2252002
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2252003
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2252004
    :cond_1
    sget-object v0, LX/FWJ;->d:LX/FWJ;

    return-object v0

    .line 2252005
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2252006
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;
    .locals 14
    .param p0    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/FVt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2251928
    if-eqz p0, :cond_0

    .line 2251929
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2251930
    if-eqz v0, :cond_0

    .line 2251931
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2251932
    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;->a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2251933
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2251934
    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;->a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2251935
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2251936
    :goto_0
    return-object v0

    .line 2251937
    :cond_1
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2251938
    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;->a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2251939
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2251940
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemsEdgeModel;

    .line 2251941
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemsEdgeModel;->k()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    move-result-object v2

    if-nez v2, :cond_3

    move v2, v3

    :goto_2
    if-eqz v2, :cond_5

    move v2, v3

    :goto_3
    if-nez v2, :cond_2

    .line 2251942
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemsEdgeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2251943
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemsEdgeModel;->k()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v0, 0x0

    .line 2251944
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->p()LX/1vs;

    move-result-object v9

    iget v9, v9, LX/1vs;->b:I

    if-nez v9, :cond_7

    move-object v9, v10

    .line 2251945
    :goto_4
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->o()LX/1vs;

    move-result-object v11

    iget v11, v11, LX/1vs;->b:I

    if-nez v11, :cond_8

    move-object v11, v10

    .line 2251946
    :goto_5
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->a()LX/1vs;

    move-result-object v12

    iget v12, v12, LX/1vs;->b:I

    if-nez v12, :cond_9

    move-object v12, v10

    .line 2251947
    :goto_6
    new-instance v13, LX/FVr;

    invoke-direct {v13}, LX/FVr;-><init>()V

    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->l()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-result-object p0

    .line 2251948
    iput-object p0, v13, LX/FVr;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 2251949
    move-object v13, v13

    .line 2251950
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->m()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    move-result-object p0

    .line 2251951
    iput-object p0, v13, LX/FVr;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    .line 2251952
    move-object v13, v13

    .line 2251953
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->n()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-result-object p0

    .line 2251954
    iput-object p0, v13, LX/FVr;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    .line 2251955
    move-object v13, v13

    .line 2251956
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->q()Ljava/lang/String;

    move-result-object p0

    .line 2251957
    iput-object p0, v13, LX/FVr;->e:Ljava/lang/String;

    .line 2251958
    move-object v13, v13

    .line 2251959
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    if-nez p0, :cond_a

    .line 2251960
    :goto_7
    iput-object v10, v13, LX/FVr;->f:Ljava/lang/String;

    .line 2251961
    move-object v10, v13

    .line 2251962
    iput-object v9, v10, LX/FVr;->a:Ljava/lang/String;

    .line 2251963
    move-object v9, v10

    .line 2251964
    iput-object v11, v9, LX/FVr;->b:Ljava/lang/String;

    .line 2251965
    move-object v9, v9

    .line 2251966
    iput-object v12, v9, LX/FVr;->c:Ljava/lang/String;

    .line 2251967
    move-object v9, v9

    .line 2251968
    iput-boolean v0, v9, LX/FVr;->j:Z

    .line 2251969
    move-object v9, v9

    .line 2251970
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->j()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-result-object v10

    .line 2251971
    iput-object v10, v9, LX/FVr;->l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    .line 2251972
    move-object v9, v9

    .line 2251973
    move-object v8, v9

    .line 2251974
    invoke-virtual {v7, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2251975
    iput-object v2, v8, LX/FVr;->d:Ljava/lang/String;

    .line 2251976
    move-object v2, v8

    .line 2251977
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemsEdgeModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2251978
    iput-object v1, v2, LX/FVr;->i:Ljava/lang/String;

    .line 2251979
    move-object v1, v2

    .line 2251980
    invoke-virtual {v1}, LX/FVr;->a()LX/FVt;

    move-result-object v1

    .line 2251981
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 2251982
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemsEdgeModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2251983
    if-nez v2, :cond_4

    move v2, v3

    goto/16 :goto_2

    :cond_4
    move v2, v4

    goto/16 :goto_2

    .line 2251984
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemsEdgeModel;->j()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2251985
    invoke-virtual {v7, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    goto/16 :goto_3

    .line 2251986
    :cond_6
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2251987
    goto/16 :goto_0

    .line 2251988
    :cond_7
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->p()LX/1vs;

    move-result-object v9

    iget-object v11, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    invoke-virtual {v11, v9, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_4

    .line 2251989
    :cond_8
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->o()LX/1vs;

    move-result-object v11

    iget-object v12, v11, LX/1vs;->a:LX/15i;

    iget v11, v11, LX/1vs;->b:I

    .line 2251990
    invoke-virtual {v12, v11, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_5

    .line 2251991
    :cond_9
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->a()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    .line 2251992
    invoke-virtual {v13, v12, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_6

    .line 2251993
    :cond_a
    invoke-virtual {v8}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_7
.end method

.method public static d(LX/FVt;)Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 1
    .param p0    # LX/FVt;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251918
    if-eqz p0, :cond_0

    .line 2251919
    iget-object v0, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v0

    .line 2251920
    if-nez v0, :cond_1

    .line 2251921
    :cond_0
    const/4 v0, 0x0

    .line 2251922
    :goto_0
    return-object v0

    .line 2251923
    :cond_1
    iget-object v0, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v0

    .line 2251924
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->J()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    goto :goto_0
.end method

.method public static final f(LX/FVt;)Z
    .locals 2

    .prologue
    .line 2251925
    invoke-static {p0}, LX/FWJ;->d(LX/FVt;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    .line 2251926
    iget-boolean v1, p0, LX/FVt;->j:Z

    move v1, v1

    .line 2251927
    if-eqz v1, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1, v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
