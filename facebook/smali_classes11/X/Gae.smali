.class public LX/Gae;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Gad;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2368557
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2368558
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;Ljava/lang/Runnable;)LX/Gad;
    .locals 28

    .prologue
    .line 2368559
    new-instance v1, LX/Gad;

    invoke-static/range {p0 .. p0}, LX/1Pz;->a(LX/0QB;)LX/1Pz;

    move-result-object v8

    check-cast v8, LX/1Pz;

    invoke-static/range {p0 .. p0}, LX/1Q0;->a(LX/0QB;)LX/1Q0;

    move-result-object v9

    check-cast v9, LX/1Q0;

    const-class v2, LX/1Q1;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/1Q1;

    invoke-static/range {p0 .. p0}, LX/1Q2;->a(LX/0QB;)LX/1Q2;

    move-result-object v11

    check-cast v11, LX/1Q2;

    invoke-static/range {p0 .. p0}, LX/1Q3;->a(LX/0QB;)LX/1Q3;

    move-result-object v12

    check-cast v12, LX/1Q3;

    invoke-static/range {p0 .. p0}, LX/1Q4;->a(LX/0QB;)LX/1Q4;

    move-result-object v13

    check-cast v13, LX/1Q4;

    const-class v2, LX/1Q5;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/1Q5;

    const-class v2, LX/1Q6;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/1Q6;

    invoke-static/range {p0 .. p0}, LX/1Q7;->a(LX/0QB;)LX/1Q7;

    move-result-object v16

    check-cast v16, LX/1Q7;

    const-class v2, LX/1QA;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/1QA;

    invoke-static/range {p0 .. p0}, LX/1QB;->a(LX/0QB;)LX/1QB;

    move-result-object v18

    check-cast v18, LX/1QB;

    const-class v2, LX/1QC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/1QC;

    invoke-static/range {p0 .. p0}, LX/1QD;->a(LX/0QB;)LX/1QD;

    move-result-object v20

    check-cast v20, LX/1QD;

    const-class v2, LX/1QE;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/1QE;

    invoke-static/range {p0 .. p0}, LX/1QF;->a(LX/0QB;)LX/1QF;

    move-result-object v22

    check-cast v22, LX/1QF;

    invoke-static/range {p0 .. p0}, LX/1QG;->a(LX/0QB;)LX/1QG;

    move-result-object v23

    check-cast v23, LX/1QG;

    invoke-static/range {p0 .. p0}, LX/99Q;->a(LX/0QB;)LX/99Q;

    move-result-object v24

    check-cast v24, LX/99Q;

    const-class v2, LX/1QI;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/1QI;

    const-class v2, LX/1QJ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/1QJ;

    invoke-static/range {p0 .. p0}, LX/1QK;->a(LX/0QB;)LX/1QK;

    move-result-object v27

    check-cast v27, LX/1QK;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v27}, LX/Gad;-><init>(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;Ljava/lang/Runnable;LX/1Pz;LX/1Q0;LX/1Q1;LX/1Q2;LX/1Q3;LX/1Q4;LX/1Q5;LX/1Q6;LX/1Q7;LX/1QA;LX/1QB;LX/1QC;LX/1QD;LX/1QE;LX/1QF;LX/1QG;LX/99Q;LX/1QI;LX/1QJ;LX/1QK;)V

    .line 2368560
    return-object v1
.end method
