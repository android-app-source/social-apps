.class public final LX/FBd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/service/AppSession;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/278;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/katana/service/AppSession;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/278;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2209611
    iput-object p1, p0, LX/FBd;->a:Lcom/facebook/katana/service/AppSession;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2209612
    invoke-static {p2}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/FBd;->b:Ljava/util/Set;

    .line 2209613
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2209614
    iget-object v0, p0, LX/FBd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/278;

    .line 2209615
    invoke-virtual {v0, p1}, LX/278;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2209616
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2209617
    iget-object v0, p0, LX/FBd;->a:Lcom/facebook/katana/service/AppSession;

    iget-object v0, v0, Lcom/facebook/katana/service/AppSession;->r:LX/2A0;

    invoke-virtual {v0}, LX/2A0;->c()V

    .line 2209618
    iget-object v0, p0, LX/FBd;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/278;

    .line 2209619
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/278;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2209620
    :cond_0
    return-void
.end method
