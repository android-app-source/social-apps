.class public LX/FUz;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field public a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2250258
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2250259
    iput-object p1, p0, LX/FUz;->a:Landroid/content/Context;

    .line 2250260
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 3

    .prologue
    .line 2250261
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2250262
    const-string v0, "Saved"

    invoke-virtual {p0, v0}, LX/FUz;->setTitle(Ljava/lang/CharSequence;)V

    .line 2250263
    new-instance v1, Landroid/preference/Preference;

    iget-object v0, p0, LX/FUz;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2250264
    const-string v0, "Pre-cache Saved Dashboard"

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2250265
    iget-object v0, p0, LX/FUz;->a:Landroid/content/Context;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    .line 2250266
    invoke-static {v0}, LX/FWe;->b(LX/0QB;)LX/FWe;

    move-result-object v0

    check-cast v0, LX/FWe;

    .line 2250267
    new-instance v2, LX/FUy;

    invoke-direct {v2, p0, v0}, LX/FUy;-><init>(LX/FUz;LX/FWe;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2250268
    invoke-virtual {p0, v1}, LX/FUz;->addPreference(Landroid/preference/Preference;)Z

    .line 2250269
    return-void
.end method
