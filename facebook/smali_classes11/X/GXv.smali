.class public final LX/GXv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GXy;


# direct methods
.method public constructor <init>(LX/GXy;)V
    .locals 0

    .prologue
    .line 2364645
    iput-object p1, p0, LX/GXv;->a:LX/GXy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2364646
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2364647
    iget-object v0, p0, LX/GXv;->a:LX/GXy;

    .line 2364648
    iget-object v1, v0, LX/GXy;->d:LX/1mR;

    const-string p1, "GraphQLCommerceRequestTag"

    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object p1

    invoke-virtual {v1, p1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2364649
    iget-object v0, p0, LX/GXv;->a:LX/GXy;

    iget-object v0, v0, LX/GXy;->e:LX/7jB;

    sget-object v1, LX/7jD;->CREATE:LX/7jD;

    invoke-static {v1}, LX/7jI;->a(LX/7jD;)LX/7jI;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2364650
    return-void
.end method
