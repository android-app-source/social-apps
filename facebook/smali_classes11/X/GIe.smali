.class public final LX/GIe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GIr;


# direct methods
.method public constructor <init>(LX/GIr;)V
    .locals 0

    .prologue
    .line 2336918
    iput-object p1, p0, LX/GIe;->a:LX/GIr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2336919
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 2336920
    :goto_0
    return-void

    .line 2336921
    :cond_0
    const-string v0, "selectedTokens"

    invoke-static {p2, v0}, LX/4By;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 2336922
    iget-object v1, p0, LX/GIe;->a:LX/GIr;

    if-eqz v0, :cond_1

    :goto_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/GIr;->b:LX/0Px;

    .line 2336923
    iget-object v0, p0, LX/GIe;->a:LX/GIr;

    iget-object v1, p0, LX/GIe;->a:LX/GIr;

    iget-object v1, v1, LX/GIr;->b:LX/0Px;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/GIr;->c:LX/0Px;

    .line 2336924
    iget-object v0, p0, LX/GIe;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->i:LX/GL2;

    .line 2336925
    iget-object v1, v0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    move-object v0, v1

    .line 2336926
    iget-object v1, p0, LX/GIe;->a:LX/GIr;

    iget-object v1, v1, LX/GIr;->b:LX/0Px;

    sget-object v2, LX/GIr;->n:LX/1jt;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 2336927
    iget-object v0, p0, LX/GIe;->a:LX/GIr;

    invoke-virtual {v0}, LX/GIr;->b()V

    .line 2336928
    iget-object v0, p0, LX/GIe;->a:LX/GIr;

    invoke-virtual {v0}, LX/GIr;->e()V

    goto :goto_0

    .line 2336929
    :cond_1
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_1
.end method
