.class public LX/GF0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GLp;

.field private b:LX/0ad;


# direct methods
.method public constructor <init>(LX/GLp;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332240
    iput-object p1, p0, LX/GF0;->a:LX/GLp;

    .line 2332241
    iput-object p2, p0, LX/GF0;->b:LX/0ad;

    .line 2332242
    return-void
.end method

.method public static a(LX/0QB;)LX/GF0;
    .locals 1

    .prologue
    .line 2332238
    invoke-static {p0}, LX/GF0;->b(LX/0QB;)LX/GF0;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GF0;
    .locals 5

    .prologue
    .line 2332233
    new-instance v2, LX/GF0;

    .line 2332234
    new-instance v4, LX/GLp;

    invoke-static {p0}, LX/GEU;->a(LX/0QB;)LX/GEU;

    move-result-object v0

    check-cast v0, LX/GEU;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v1

    check-cast v1, LX/2U3;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v0, v1, v3}, LX/GLp;-><init>(LX/GEU;LX/2U3;LX/0ad;)V

    .line 2332235
    move-object v0, v4

    .line 2332236
    check-cast v0, LX/GLp;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/GF0;-><init>(LX/GLp;LX/0ad;)V

    .line 2332237
    return-object v2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332243
    const v0, 0x7f030096

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2332227
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2332228
    :cond_0
    :goto_0
    return v0

    .line 2332229
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    .line 2332230
    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-ne v1, v2, :cond_2

    .line 2332231
    iget-object v1, p0, LX/GF0;->b:LX/0ad;

    sget-short v2, LX/GDK;->A:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_0

    .line 2332232
    :cond_2
    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332226
    iget-object v0, p0, LX/GF0;->a:LX/GLp;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332225
    sget-object v0, LX/8wK;->INFO_CARD:LX/8wK;

    return-object v0
.end method
