.class public final LX/HCe;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2439181
    const-class v1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    const v0, -0x18bc5234

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchEditPageQuery"

    const-string v6, "50bcfca9d680a6141c30639cd04e3669"

    const-string v7, "node"

    const-string v8, "10155261262301729"

    const-string v9, "10155263176676729"

    .line 2439182
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2439183
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2439184
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2439167
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2439168
    sparse-switch v0, :sswitch_data_0

    .line 2439169
    :goto_0
    return-object p1

    .line 2439170
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2439171
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2439172
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2439173
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2439174
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2439175
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2439176
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2439177
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2439178
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2439179
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2439180
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x71e86c6d -> :sswitch_8
        -0x5300bcfe -> :sswitch_6
        -0x2fe52f35 -> :sswitch_0
        -0x27903942 -> :sswitch_3
        -0x17657c0a -> :sswitch_4
        -0x99a550b -> :sswitch_7
        0x2ed4a921 -> :sswitch_a
        0x50f8fde2 -> :sswitch_5
        0x57a06f49 -> :sswitch_1
        0x76794678 -> :sswitch_2
        0x78326898 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2439159
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 2439160
    :goto_1
    return v0

    .line 2439161
    :pswitch_1
    const-string v2, "3"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "6"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    const-string v2, "5"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :pswitch_4
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :pswitch_5
    const-string v2, "7"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    .line 2439162
    :pswitch_6
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2439163
    :pswitch_7
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2439164
    :pswitch_8
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2439165
    :pswitch_9
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 2439166
    :pswitch_a
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
