.class public final LX/GxQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/GxZ;


# direct methods
.method public constructor <init>(LX/GxZ;)V
    .locals 0

    .prologue
    .line 2408098
    iput-object p1, p0, LX/GxQ;->a:LX/GxZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/GxA;)V
    .locals 5

    .prologue
    .line 2408099
    iget-object v0, p0, LX/GxQ;->a:LX/GxZ;

    iget v0, v0, LX/GxZ;->t:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 2408100
    new-instance v0, Lcom/facebook/katana/webview/RefreshableFacewebWebViewContainer$2$1;

    invoke-direct {v0, p0}, Lcom/facebook/katana/webview/RefreshableFacewebWebViewContainer$2$1;-><init>(LX/GxQ;)V

    .line 2408101
    iget-object v1, p0, LX/GxQ;->a:LX/GxZ;

    iget-object v1, v1, LX/GxZ;->s:Landroid/os/Handler;

    invoke-static {v1, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2408102
    iget-object v1, p0, LX/GxQ;->a:LX/GxZ;

    iget-object v1, v1, LX/GxZ;->s:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    const v4, 0x6652a742

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2408103
    iget-object v0, p0, LX/GxQ;->a:LX/GxZ;

    .line 2408104
    iget v1, v0, LX/GxZ;->t:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v0, LX/GxZ;->t:I

    .line 2408105
    :goto_0
    return-void

    .line 2408106
    :cond_0
    iget-object v0, p0, LX/GxQ;->a:LX/GxZ;

    .line 2408107
    iget-object v1, v0, LX/GxZ;->o:Landroid/view/View;

    const v2, 0x7f0d105d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2408108
    if-nez v1, :cond_1

    .line 2408109
    :goto_1
    iget-object v0, p0, LX/GxQ;->a:LX/GxZ;

    sget-object v1, LX/GxY;->CONTENT_STATE_ERROR:LX/GxY;

    invoke-virtual {v0, v1}, LX/GxZ;->a(LX/GxY;)V

    goto :goto_0

    .line 2408110
    :cond_1
    if-nez p1, :cond_2

    .line 2408111
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2408112
    :cond_2
    invoke-virtual {p1}, LX/GxA;->getErrorMessageId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public final a(LX/GxE;)V
    .locals 2

    .prologue
    .line 2408113
    sget-object v0, LX/GxE;->PAGE_STATE_SUCCESS:LX/GxE;

    if-eq p1, v0, :cond_0

    .line 2408114
    iget-object v0, p0, LX/GxQ;->a:LX/GxZ;

    sget-object v1, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    invoke-virtual {v0, v1}, LX/GxZ;->a(LX/GxY;)V

    .line 2408115
    :cond_0
    return-void
.end method
