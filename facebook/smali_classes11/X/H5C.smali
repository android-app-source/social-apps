.class public final LX/H5C;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/H5D;


# direct methods
.method public constructor <init>(LX/H5D;)V
    .locals 1

    .prologue
    .line 2423747
    iput-object p1, p0, LX/H5C;->d:LX/H5D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423748
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/H5C;->a:Ljava/util/Map;

    .line 2423749
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/H5C;->b:Ljava/util/List;

    .line 2423750
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/H5C;->c:Ljava/util/Map;

    .line 2423751
    return-void
.end method

.method private a(LX/2nq;)V
    .locals 2

    .prologue
    .line 2423744
    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2423745
    iget-object v0, p0, LX/H5C;->c:Ljava/util/Map;

    invoke-interface {p1}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423746
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2423724
    iget-object v0, p0, LX/H5C;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(LX/2nq;LX/2nq;)V
    .locals 3

    .prologue
    .line 2423738
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 2423739
    invoke-interface {p2}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2423740
    :cond_0
    :goto_0
    return-void

    .line 2423741
    :cond_1
    iget-object v1, p0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2423742
    iget-object v1, p0, LX/H5C;->b:Ljava/util/List;

    iget-object v2, p0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2423743
    invoke-direct {p0, p2}, LX/H5C;->a(LX/2nq;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2423729
    iget-object v0, p0, LX/H5C;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2423730
    iget-object v1, p0, LX/H5C;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move v1, v0

    .line 2423731
    :goto_0
    iget-object v0, p0, LX/H5C;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2423732
    iget-object v0, p0, LX/H5C;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 2423733
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2423734
    iget-object v2, p0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423735
    invoke-direct {p0, v0}, LX/H5C;->a(LX/2nq;)V

    .line 2423736
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2423737
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2423725
    iget-object v0, p0, LX/H5C;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2423726
    iget-object v0, p0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2423727
    iget-object v0, p0, LX/H5C;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2423728
    return-void
.end method
