.class public LX/GAi;
.super Ljava/io/OutputStream;
.source ""

# interfaces
.implements LX/GAh;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/GAU;",
            "LX/GAk;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field private c:LX/GAU;

.field private d:LX/GAk;

.field public e:I


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 2326081
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 2326082
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/GAi;->a:Ljava/util/Map;

    .line 2326083
    iput-object p1, p0, LX/GAi;->b:Landroid/os/Handler;

    .line 2326084
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 2326065
    iget-object v0, p0, LX/GAi;->d:LX/GAk;

    if-nez v0, :cond_0

    .line 2326066
    new-instance v0, LX/GAk;

    iget-object v1, p0, LX/GAi;->b:Landroid/os/Handler;

    iget-object v2, p0, LX/GAi;->c:LX/GAU;

    invoke-direct {v0, v1, v2}, LX/GAk;-><init>(Landroid/os/Handler;LX/GAU;)V

    iput-object v0, p0, LX/GAi;->d:LX/GAk;

    .line 2326067
    iget-object v0, p0, LX/GAi;->a:Ljava/util/Map;

    iget-object v1, p0, LX/GAi;->c:LX/GAU;

    iget-object v2, p0, LX/GAi;->d:LX/GAk;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2326068
    :cond_0
    iget-object v0, p0, LX/GAi;->d:LX/GAk;

    invoke-virtual {v0, p1, p2}, LX/GAk;->b(J)V

    .line 2326069
    iget v0, p0, LX/GAi;->e:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, LX/GAi;->e:I

    .line 2326070
    return-void
.end method

.method public final a(LX/GAU;)V
    .locals 1

    .prologue
    .line 2326077
    iput-object p1, p0, LX/GAi;->c:LX/GAU;

    .line 2326078
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/GAi;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAk;

    :goto_0
    iput-object v0, p0, LX/GAi;->d:LX/GAk;

    .line 2326079
    return-void

    .line 2326080
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 2326075
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/GAi;->a(J)V

    .line 2326076
    return-void
.end method

.method public final write([B)V
    .locals 2

    .prologue
    .line 2326073
    array-length v0, p1

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/GAi;->a(J)V

    .line 2326074
    return-void
.end method

.method public final write([BII)V
    .locals 2

    .prologue
    .line 2326071
    int-to-long v0, p3

    invoke-virtual {p0, v0, v1}, LX/GAi;->a(J)V

    .line 2326072
    return-void
.end method
