.class public final LX/Fxb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/Fxf;


# direct methods
.method public constructor <init>(LX/Fxf;)V
    .locals 0

    .prologue
    .line 2305563
    iput-object p1, p0, LX/Fxb;->a:LX/Fxf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2305564
    iget-object v1, p0, LX/Fxb;->a:LX/Fxf;

    iget-boolean v1, v1, LX/Fxf;->c:Z

    if-nez v1, :cond_0

    .line 2305565
    :goto_0
    return v0

    .line 2305566
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2305567
    :pswitch_0
    iget-object v0, p0, LX/Fxb;->a:LX/Fxf;

    invoke-virtual {v0}, LX/Fxf;->c()Z

    move-result v0

    goto :goto_0

    .line 2305568
    :pswitch_1
    iget-object v0, p0, LX/Fxb;->a:LX/Fxf;

    iget v0, v0, LX/Fxf;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 2305569
    iget-object v0, p0, LX/Fxb;->a:LX/Fxf;

    invoke-static {v0}, LX/Fxf;->e(LX/Fxf;)Landroid/view/View;

    move-result-object v0

    .line 2305570
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, LX/Fxb;->a:LX/Fxf;

    iget v2, v2, LX/Fxf;->l:F

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 2305571
    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, LX/Fxb;->a:LX/Fxf;

    iget v2, v2, LX/Fxf;->m:F

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 2305572
    iget-object v0, p0, LX/Fxb;->a:LX/Fxf;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-static {v0, v1, v2}, LX/Fxf;->b(LX/Fxf;FF)V

    .line 2305573
    iget-object v0, p0, LX/Fxb;->a:LX/Fxf;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 2305574
    iput v1, v0, LX/Fxf;->l:F

    .line 2305575
    iget-object v0, p0, LX/Fxb;->a:LX/Fxf;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    .line 2305576
    iput v1, v0, LX/Fxf;->m:F

    .line 2305577
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
