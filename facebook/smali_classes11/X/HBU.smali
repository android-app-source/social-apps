.class public final LX/HBU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bi2;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;)V
    .locals 0

    .prologue
    .line 2437215
    iput-object p1, p0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;)V
    .locals 6

    .prologue
    .line 2437216
    iget-object v0, p0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    iget-object v0, v0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437217
    iget-object v1, v0, LX/HBY;->e:LX/0Px;

    move-object v0, v1

    .line 2437218
    invoke-static {v0}, LX/8A4;->a(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2437219
    iget-object v0, p0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    const/4 v1, 0x0

    .line 2437220
    invoke-static {v0, p1, p2, v1}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a$redex0(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2437221
    :goto_0
    return-void

    .line 2437222
    :cond_0
    new-instance v1, LX/4At;

    iget-object v0, p0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0817c1

    invoke-direct {v1, v0, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2437223
    iget-object v0, p0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    iget-object v0, v0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iH;

    iget-object v2, p0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    iget-object v2, v2, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->l:LX/HBY;

    .line 2437224
    iget-wide v4, v2, LX/HBY;->a:J

    move-wide v2, v4

    .line 2437225
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/HBT;

    invoke-direct {v3, p0, p1, p2, v1}, LX/HBT;-><init>(LX/HBU;Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;LX/4At;)V

    iget-object v1, p0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    iget-object v1, v1, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v2, v3, v1}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
