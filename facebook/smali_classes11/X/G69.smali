.class public final LX/G69;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9fc;


# instance fields
.field public final synthetic a:Lcom/facebook/wem/watermark/WatermarkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/wem/watermark/WatermarkActivity;)V
    .locals 0

    .prologue
    .line 2319769
    iput-object p1, p0, LX/G69;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 3

    .prologue
    .line 2319770
    iget-object v0, p0, LX/G69;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2319771
    iput-object v1, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->y:Landroid/net/Uri;

    .line 2319772
    iget-object v0, p0, LX/G69;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->C:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/G69;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v1, v1, Lcom/facebook/wem/watermark/WatermarkActivity;->y:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/wem/watermark/WatermarkActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2319773
    iget-object v0, p0, LX/G69;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->A:LX/G6J;

    iget-object v1, p0, LX/G69;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v1, v1, Lcom/facebook/wem/watermark/WatermarkActivity;->y:Landroid/net/Uri;

    .line 2319774
    iput-object v1, v0, LX/G6J;->e:Landroid/net/Uri;

    .line 2319775
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2319776
    return-void
.end method

.method public final a(Ljava/lang/Throwable;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 2

    .prologue
    .line 2319777
    iget-object v0, p0, LX/G69;->a:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget-object v0, v0, Lcom/facebook/wem/watermark/WatermarkActivity;->s:LX/G6Q;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/G6Q;->c(Ljava/lang/String;)V

    .line 2319778
    return-void
.end method
