.class public LX/FZX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hF;


# instance fields
.field private final a:LX/0gc;

.field private final b:LX/0Uh;

.field public c:Ljava/lang/String;

.field public d:LX/0gh;

.field public e:Landroid/os/Handler;

.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z


# direct methods
.method public constructor <init>(LX/0gc;LX/0Uh;LX/0gh;Landroid/os/Handler;)V
    .locals 1
    .param p1    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2257382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2257383
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FZX;->g:Z

    .line 2257384
    iput-object p1, p0, LX/FZX;->a:LX/0gc;

    .line 2257385
    iput-object p2, p0, LX/FZX;->b:LX/0Uh;

    .line 2257386
    iput-object p3, p0, LX/FZX;->d:LX/0gh;

    .line 2257387
    iput-object p4, p0, LX/FZX;->e:Landroid/os/Handler;

    .line 2257388
    const/4 v0, 0x0

    iput-object v0, p0, LX/FZX;->f:Ljava/lang/ref/WeakReference;

    .line 2257389
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2257378
    invoke-virtual {p0}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2257379
    instance-of v1, v0, LX/0fh;

    if-eqz v1, :cond_0

    .line 2257380
    check-cast v0, LX/0fh;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    .line 2257381
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)Z
    .locals 1

    .prologue
    .line 2257377
    invoke-virtual {p0}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/FZX;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FZX;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2257371
    invoke-virtual {p0}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2257372
    instance-of v1, v0, LX/0hF;

    if-eqz v1, :cond_0

    .line 2257373
    check-cast v0, LX/0hF;

    invoke-interface {v0}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v0

    .line 2257374
    :goto_0
    return-object v0

    .line 2257375
    :cond_0
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2257376
    goto :goto_0
.end method

.method public final b(Landroid/support/v4/app/Fragment;)V
    .locals 6

    .prologue
    .line 2257297
    invoke-virtual {p0}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2257298
    invoke-virtual {p0, p1}, LX/FZX;->a(Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2257299
    :goto_0
    return-void

    .line 2257300
    :cond_0
    iget-object v1, p0, LX/FZX;->a:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0hH;->c(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    .line 2257301
    if-eqz v0, :cond_2

    if-eq v0, p1, :cond_2

    .line 2257302
    sget-object v2, LX/FZc;->SUGGESTIONS:LX/FZc;

    invoke-virtual {v2}, LX/FZc;->getTag()Ljava/lang/String;

    move-result-object v2

    .line 2257303
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    move-object v3, v3

    .line 2257304
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, LX/FZX;->b:LX/0Uh;

    sget v3, LX/2SU;->j:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2257305
    :cond_1
    invoke-virtual {v1, v0}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2257306
    :cond_2
    :goto_1
    invoke-virtual {v1}, LX/0hH;->c()I

    .line 2257307
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/FZX;->f:Ljava/lang/ref/WeakReference;

    .line 2257308
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;

    move-object v0, v0

    .line 2257309
    iput-object v0, p0, LX/FZX;->c:Ljava/lang/String;

    .line 2257310
    iget-object v0, p0, LX/FZX;->c:Ljava/lang/String;

    sget-object v1, LX/FZc;->SUGGESTIONS:LX/FZc;

    invoke-virtual {v1}, LX/FZc;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 2257311
    check-cast v0, Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2257312
    iget-object v1, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->P:LX/8i2;

    iget-object v2, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Y:Landroid/view/ContextThemeWrapper;

    invoke-virtual {v1, v2}, LX/8i2;->a(Landroid/content/Context;)V

    .line 2257313
    iget-object v1, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->N:LX/FiS;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    .line 2257314
    iget-object v3, v1, LX/FiS;->e:LX/CzG;

    .line 2257315
    iget-object v4, v3, LX/CzG;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v5, v4

    .line 2257316
    if-nez v5, :cond_5

    .line 2257317
    :cond_3
    :goto_2
    iget-boolean v0, p0, LX/FZX;->g:Z

    if-eqz v0, :cond_e

    .line 2257318
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FZX;->g:Z

    .line 2257319
    :goto_3
    goto :goto_0

    .line 2257320
    :cond_4
    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    goto :goto_1

    .line 2257321
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/7BG;->g(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 2257322
    const-string v3, "1710062295913737"

    move-object v4, v3

    .line 2257323
    :goto_4
    if-eqz v4, :cond_3

    .line 2257324
    iget-object v3, v1, LX/FiS;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gt;

    .line 2257325
    iput-object v4, v3, LX/0gt;->a:Ljava/lang/String;

    .line 2257326
    move-object v3, v3

    .line 2257327
    iget-object v4, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v4, v4

    .line 2257328
    if-eqz v4, :cond_6

    .line 2257329
    const-string v4, "results_vertical"

    .line 2257330
    iget-object v0, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v0, v0

    .line 2257331
    invoke-virtual {v3, v4, v0}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    .line 2257332
    :cond_6
    invoke-virtual {v5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 2257333
    const-string v4, "query_function"

    invoke-virtual {v5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    .line 2257334
    :cond_7
    invoke-virtual {v5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 2257335
    const-string v4, "query"

    invoke-virtual {v5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    .line 2257336
    :cond_8
    iget-object v4, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v4, v4

    .line 2257337
    if-eqz v4, :cond_9

    .line 2257338
    const-string v4, "results_source"

    .line 2257339
    iget-object v0, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v0, v0

    .line 2257340
    invoke-virtual {v0}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    .line 2257341
    :cond_9
    iget-object v4, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v4, v4

    .line 2257342
    if-eqz v4, :cond_a

    .line 2257343
    iget-object v4, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v4, v4

    .line 2257344
    iget-object v4, v4, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    if-eqz v4, :cond_a

    .line 2257345
    const-string v4, "typeahead_sid"

    .line 2257346
    iget-object v0, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v0, v0

    .line 2257347
    iget-object v0, v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    .line 2257348
    :cond_a
    iget-object v4, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    move-object v4, v4

    .line 2257349
    if-eqz v4, :cond_b

    .line 2257350
    const-string v4, "session_id"

    .line 2257351
    iget-object v0, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    move-object v0, v0

    .line 2257352
    invoke-virtual {v3, v4, v0}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    .line 2257353
    :cond_b
    invoke-virtual {v3, v2}, LX/0gt;->a(Landroid/content/Context;)V

    .line 2257354
    iget-object v3, v1, LX/FiS;->e:LX/CzG;

    .line 2257355
    const/4 v4, 0x0

    iput-object v4, v3, LX/CzG;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2257356
    goto/16 :goto_2

    .line 2257357
    :cond_c
    invoke-virtual {v5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v3

    invoke-interface {v3}, LX/8ef;->m()Ljava/lang/String;

    move-result-object v3

    .line 2257358
    if-eqz v3, :cond_3

    .line 2257359
    sget-object v4, LX/FiS;->c:LX/0Rf;

    .line 2257360
    iget-object v0, v5, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v0, v0

    .line 2257361
    invoke-virtual {v4, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2257362
    sget-object v4, LX/FiS;->a:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v3, v4

    .line 2257363
    move-object v4, v3

    goto/16 :goto_4

    .line 2257364
    :cond_d
    sget-object v4, LX/FiS;->b:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v3, v4

    .line 2257365
    move-object v4, v3

    goto/16 :goto_4

    .line 2257366
    :cond_e
    iget-object v0, p0, LX/FZX;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/search/fragment/GraphSearchChildFragmentNavigator$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/search/fragment/GraphSearchChildFragmentNavigator$1;-><init>(LX/FZX;Landroid/support/v4/app/Fragment;)V

    const v2, 0x5c3c75c

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2257367
    goto/16 :goto_3
.end method

.method public final c()Landroid/support/v4/app/Fragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2257368
    iget-object v0, p0, LX/FZX;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2257369
    const/4 v0, 0x0

    .line 2257370
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FZX;->a:LX/0gc;

    iget-object v1, p0, LX/FZX;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0
.end method
