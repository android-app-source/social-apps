.class public final LX/Gzm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/model/FormData;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;)V
    .locals 0

    .prologue
    .line 2412328
    iput-object p1, p0, LX/Gzm;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2412329
    iget-object v0, p0, LX/Gzm;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a:LX/Gzy;

    iget-object v1, p0, LX/Gzm;->a:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    .line 2412330
    new-instance v2, LX/H00;

    invoke-direct {v2}, LX/H00;-><init>()V

    move-object v2, v2

    .line 2412331
    const-string v3, "page_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2412332
    iget-object v3, v0, LX/Gzy;->a:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object p0, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, p0}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    sget-object p0, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, p0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v3, LX/Gzx;

    invoke-direct {v3, v0}, LX/Gzx;-><init>(LX/Gzy;)V

    invoke-static {}, LX/0TA;->b()LX/0TD;

    move-result-object p0

    invoke-static {v2, v3, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2412333
    return-object v0
.end method
