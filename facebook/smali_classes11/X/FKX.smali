.class public LX/FKX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/CreateGroupParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/FKo;


# direct methods
.method public constructor <init>(LX/0Or;LX/FKo;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/FKo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225194
    iput-object p2, p0, LX/FKX;->b:LX/FKo;

    .line 2225195
    iput-object p1, p0, LX/FKX;->a:LX/0Or;

    .line 2225196
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2225197
    check-cast p1, Lcom/facebook/messaging/service/model/CreateGroupParams;

    .line 2225198
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2225199
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "name"

    .line 2225200
    iget-object v3, p1, Lcom/facebook/messaging/service/model/CreateGroupParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2225201
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225202
    iget-object v0, p1, Lcom/facebook/messaging/service/model/CreateGroupParams;->c:LX/0Px;

    move-object v2, v0

    .line 2225203
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2225204
    new-instance v4, Lcom/facebook/user/model/UserFbidIdentifier;

    iget-object v0, p0, LX/FKX;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v0}, Lcom/facebook/user/model/UserFbidIdentifier;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225205
    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2225206
    invoke-static {v3}, LX/FKo;->a(Ljava/util/List;)LX/0lF;

    move-result-object v0

    .line 2225207
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "recipients"

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225208
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "createGroup"

    .line 2225209
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2225210
    move-object v0, v0

    .line 2225211
    const-string v2, "POST"

    .line 2225212
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2225213
    move-object v0, v0

    .line 2225214
    const-string v2, "me/group_threads"

    .line 2225215
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 2225216
    move-object v0, v0

    .line 2225217
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2225218
    move-object v0, v0

    .line 2225219
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2225220
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225221
    move-object v0, v0

    .line 2225222
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2225223
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2225224
    const-string v1, "thread_id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
