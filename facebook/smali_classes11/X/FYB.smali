.class public LX/FYB;
.super LX/FXy;
.source ""


# instance fields
.field private final a:LX/BUA;

.field public final b:LX/0tQ;

.field private final c:LX/19w;

.field private d:LX/1A0;


# direct methods
.method public constructor <init>(LX/BUA;LX/0tQ;LX/19w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255824
    invoke-direct {p0}, LX/FXy;-><init>()V

    .line 2255825
    iput-object p1, p0, LX/FYB;->a:LX/BUA;

    .line 2255826
    iput-object p2, p0, LX/FYB;->b:LX/0tQ;

    .line 2255827
    iput-object p3, p0, LX/FYB;->c:LX/19w;

    .line 2255828
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2255829
    iget-object v0, p0, LX/FYB;->d:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    if-ne v0, v1, :cond_0

    const v0, 0x7f020841

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f02094e

    goto :goto_0
.end method

.method public final a(LX/BO1;)LX/FXy;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2255830
    invoke-interface {p1}, LX/BO1;->F()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/FYB;->b:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2255831
    iget-object v1, p0, LX/FYB;->c:LX/19w;

    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    iput-object v1, p0, LX/FYB;->d:LX/1A0;

    .line 2255832
    iget-object v1, p0, LX/FYB;->d:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/FYB;->d:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/FYB;->d:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2255833
    :cond_1
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255834
    :goto_0
    return-object p0

    .line 2255835
    :cond_2
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255836
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2255837
    iget-object v0, p0, LX/FYB;->d:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    if-ne v0, v1, :cond_0

    .line 2255838
    sget-object v0, LX/FYA;->a:[I

    iget-object v1, p0, LX/FYB;->b:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2255839
    const v0, 0x7f081ada

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 2255840
    :goto_1
    return-object v0

    .line 2255841
    :cond_0
    sget-object v0, LX/FYA;->a:[I

    iget-object v1, p0, LX/FYB;->b:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2255842
    const v0, 0x7f081ad7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v0, v0

    .line 2255843
    goto :goto_1

    .line 2255844
    :pswitch_0
    const v0, 0x7f081ad9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2255845
    :pswitch_1
    const v0, 0x7f081adb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2255846
    :pswitch_2
    const v0, 0x7f081ad6

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2255847
    :pswitch_3
    const v0, 0x7f081ad8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2255848
    const-string v0, "video_pause_resume_download_button"

    return-object v0
.end method

.method public final b(LX/BO1;)Z
    .locals 15

    .prologue
    .line 2255849
    invoke-interface/range {p1 .. p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v3

    .line 2255850
    invoke-interface/range {p1 .. p1}, LX/BO1;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2255851
    iget-object v0, p0, LX/FYB;->d:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_PAUSED:LX/1A0;

    if-ne v0, v1, :cond_0

    .line 2255852
    new-instance v1, LX/BUL;

    const-string v4, ""

    const-string v5, "saved dashboard"

    invoke-interface/range {p1 .. p1}, LX/BO1;->V()J

    move-result-wide v6

    invoke-interface/range {p1 .. p1}, LX/BO1;->j()Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, LX/BO1;->k()Ljava/lang/String;

    move-result-object v9

    invoke-interface/range {p1 .. p1}, LX/BO1;->m()Ljava/lang/String;

    move-result-object v10

    invoke-interface/range {p1 .. p1}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v11

    invoke-interface/range {p1 .. p1}, LX/BO1;->C()Ljava/lang/String;

    move-result-object v12

    invoke-interface/range {p1 .. p1}, LX/BO1;->D()Ljava/lang/String;

    move-result-object v13

    invoke-interface/range {p1 .. p1}, LX/BO1;->E()Ljava/lang/String;

    move-result-object v14

    invoke-direct/range {v1 .. v14}, LX/BUL;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2255853
    iget-object v0, p0, LX/FYB;->a:LX/BUA;

    invoke-virtual {v0, v1}, LX/BUA;->a(LX/BUL;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2255854
    iget-object v0, p0, LX/FYB;->a:LX/BUA;

    invoke-virtual {v0, v3}, LX/BUA;->a(Ljava/lang/String;)V

    .line 2255855
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2255856
    :cond_0
    iget-object v0, p0, LX/FYB;->a:LX/BUA;

    invoke-virtual {v0, v3}, LX/BUA;->d(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
