.class public final enum LX/FZc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FZc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FZc;

.field public static final enum AWARENESS_NULL_STATE:LX/FZc;

.field public static final enum REACT_NATIVE:LX/FZc;

.field public static final enum RESULTS:LX/FZc;

.field public static final enum RESULTS_KEYWORD:LX/FZc;

.field public static final enum RESULTS_KEYWORD_TABS:LX/FZc;

.field public static final enum RESULTS_PHOTO:LX/FZc;

.field public static final enum RESULTS_PLACES:LX/FZc;

.field public static final enum RESULTS_SEE_MORE:LX/FZc;

.field public static final enum RESULTS_TEXT:LX/FZc;

.field public static final enum SUGGESTIONS:LX/FZc;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2257707
    new-instance v0, LX/FZc;

    const-string v1, "AWARENESS_NULL_STATE"

    invoke-direct {v0, v1, v3}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->AWARENESS_NULL_STATE:LX/FZc;

    .line 2257708
    new-instance v0, LX/FZc;

    const-string v1, "RESULTS_TEXT"

    invoke-direct {v0, v1, v4}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->RESULTS_TEXT:LX/FZc;

    .line 2257709
    new-instance v0, LX/FZc;

    const-string v1, "RESULTS_PHOTO"

    invoke-direct {v0, v1, v5}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->RESULTS_PHOTO:LX/FZc;

    .line 2257710
    new-instance v0, LX/FZc;

    const-string v1, "RESULTS_KEYWORD_TABS"

    invoke-direct {v0, v1, v6}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->RESULTS_KEYWORD_TABS:LX/FZc;

    .line 2257711
    new-instance v0, LX/FZc;

    const-string v1, "RESULTS_KEYWORD"

    invoke-direct {v0, v1, v7}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->RESULTS_KEYWORD:LX/FZc;

    .line 2257712
    new-instance v0, LX/FZc;

    const-string v1, "SUGGESTIONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->SUGGESTIONS:LX/FZc;

    .line 2257713
    new-instance v0, LX/FZc;

    const-string v1, "RESULTS_SEE_MORE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->RESULTS_SEE_MORE:LX/FZc;

    .line 2257714
    new-instance v0, LX/FZc;

    const-string v1, "REACT_NATIVE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->REACT_NATIVE:LX/FZc;

    .line 2257715
    new-instance v0, LX/FZc;

    const-string v1, "RESULTS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->RESULTS:LX/FZc;

    .line 2257716
    new-instance v0, LX/FZc;

    const-string v1, "RESULTS_PLACES"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/FZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FZc;->RESULTS_PLACES:LX/FZc;

    .line 2257717
    const/16 v0, 0xa

    new-array v0, v0, [LX/FZc;

    sget-object v1, LX/FZc;->AWARENESS_NULL_STATE:LX/FZc;

    aput-object v1, v0, v3

    sget-object v1, LX/FZc;->RESULTS_TEXT:LX/FZc;

    aput-object v1, v0, v4

    sget-object v1, LX/FZc;->RESULTS_PHOTO:LX/FZc;

    aput-object v1, v0, v5

    sget-object v1, LX/FZc;->RESULTS_KEYWORD_TABS:LX/FZc;

    aput-object v1, v0, v6

    sget-object v1, LX/FZc;->RESULTS_KEYWORD:LX/FZc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FZc;->SUGGESTIONS:LX/FZc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FZc;->RESULTS_SEE_MORE:LX/FZc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FZc;->REACT_NATIVE:LX/FZc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FZc;->RESULTS:LX/FZc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/FZc;->RESULTS_PLACES:LX/FZc;

    aput-object v2, v0, v1

    sput-object v0, LX/FZc;->$VALUES:[LX/FZc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2257706
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FZc;
    .locals 1

    .prologue
    .line 2257703
    const-class v0, LX/FZc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FZc;

    return-object v0
.end method

.method public static values()[LX/FZc;
    .locals 1

    .prologue
    .line 2257705
    sget-object v0, LX/FZc;->$VALUES:[LX/FZc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FZc;

    return-object v0
.end method


# virtual methods
.method public final getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2257704
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "graphsearch_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/FZc;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
