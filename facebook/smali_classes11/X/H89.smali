.class public LX/H89;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0aG;

.field private final b:LX/1dw;


# direct methods
.method public constructor <init>(LX/0aG;LX/1dw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433031
    iput-object p1, p0, LX/H89;->a:LX/0aG;

    .line 2433032
    iput-object p2, p0, LX/H89;->b:LX/1dw;

    .line 2433033
    return-void
.end method

.method public static a(LX/0QB;)LX/H89;
    .locals 5

    .prologue
    .line 2433019
    const-class v1, LX/H89;

    monitor-enter v1

    .line 2433020
    :try_start_0
    sget-object v0, LX/H89;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2433021
    sput-object v2, LX/H89;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2433022
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2433023
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2433024
    new-instance p0, LX/H89;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/1dw;->a(LX/0QB;)LX/1dw;

    move-result-object v4

    check-cast v4, LX/1dw;

    invoke-direct {p0, v3, v4}, LX/H89;-><init>(LX/0aG;LX/1dw;)V

    .line 2433025
    move-object v0, p0

    .line 2433026
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2433027
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H89;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2433028
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2433029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/H89;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433014
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2433015
    if-eqz p1, :cond_0

    .line 2433016
    invoke-virtual {v0, p2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2433017
    :cond_0
    iget-object v1, p0, LX/H89;->a:LX/0aG;

    const v2, -0x38c2b01b

    invoke-static {v1, p3, v0, v2}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    .line 2433018
    iget-object v1, p0, LX/H89;->b:LX/1dw;

    invoke-virtual {v1, v0}, LX/1dw;->a(LX/1MF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/H89;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433011
    const-string v2, "native_page_profile"

    const-string v3, "actionbar_button"

    new-instance v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v0, 0x25d6af

    invoke-direct {v5, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    invoke-static/range {v0 .. v5}, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/enums/GraphQLObjectType;)LX/69v;

    move-result-object v0

    .line 2433012
    new-instance v1, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;

    invoke-direct {v1, v0}, Lcom/facebook/api/feedcache/memory/ToggleSaveParams;-><init>(LX/69v;)V

    move-object v0, v1

    .line 2433013
    const-string v1, "togglePlaceSaveParams"

    const-string v2, "toggle_place_save_from_page"

    invoke-static {p0, v0, v1, v2}, LX/H89;->a(LX/H89;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
