.class public LX/Fcn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FcI;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FcI",
        "<",
        "Lcom/facebook/widget/text/BetterTextView;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2262757
    new-instance v0, LX/Fcl;

    invoke-direct {v0}, LX/Fcl;-><init>()V

    sput-object v0, LX/Fcn;->a:LX/FcE;

    .line 2262758
    new-instance v0, LX/Fcm;

    invoke-direct {v0}, LX/Fcm;-><init>()V

    sput-object v0, LX/Fcn;->b:LX/FcE;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262756
    return-void
.end method

.method public static a(LX/0QB;)LX/Fcn;
    .locals 3

    .prologue
    .line 2262744
    const-class v1, LX/Fcn;

    monitor-enter v1

    .line 2262745
    :try_start_0
    sget-object v0, LX/Fcn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2262746
    sput-object v2, LX/Fcn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2262747
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262748
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2262749
    new-instance v0, LX/Fcn;

    invoke-direct {v0}, LX/Fcn;-><init>()V

    .line 2262750
    move-object v0, v0

    .line 2262751
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2262752
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fcn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262753
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2262754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)LX/CyH;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2262743
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262734
    sget-object v0, LX/Fcn;->a:LX/FcE;

    return-object v0
.end method

.method public final a(LX/CyH;)LX/FcT;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2262742
    new-instance v0, LX/FcT;

    const-string v1, "Unsupported"

    invoke-direct {v0, v1, v2, v2}, LX/FcT;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;LX/4FP;)V

    return-object v0
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V
    .locals 1

    .prologue
    .line 2262739
    check-cast p2, Lcom/facebook/widget/text/BetterTextView;

    .line 2262740
    invoke-interface {p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2262741
    return-void
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 1

    .prologue
    .line 2262736
    check-cast p2, Lcom/facebook/widget/text/BetterTextView;

    .line 2262737
    const-string v0, "Unsupported"

    invoke-virtual {p2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2262738
    return-void
.end method

.method public final b()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262735
    sget-object v0, LX/Fcn;->b:LX/FcE;

    return-object v0
.end method
