.class public LX/H4A;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbButton;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Landroid/view/View$OnClickListener;

.field private final d:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2422379
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/H4A;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2422380
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2422381
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/H4A;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2422382
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2422383
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2422384
    const/4 v0, 0x0

    iput-object v0, p0, LX/H4A;->c:Landroid/view/View$OnClickListener;

    .line 2422385
    new-instance v0, LX/H49;

    invoke-direct {v0, p0}, LX/H49;-><init>(LX/H4A;)V

    iput-object v0, p0, LX/H4A;->d:Landroid/view/View$OnClickListener;

    .line 2422386
    const v0, 0x7f030bc2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2422387
    const v0, 0x7f0d1d43

    invoke-virtual {p0, v0}, LX/H4A;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/H4A;->a:Lcom/facebook/resources/ui/FbButton;

    .line 2422388
    iget-object v0, p0, LX/H4A;->a:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/H4A;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2422389
    const v0, 0x7f0d1d42

    invoke-virtual {p0, v0}, LX/H4A;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/H4A;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2422390
    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2422391
    iput-object p1, p0, LX/H4A;->c:Landroid/view/View$OnClickListener;

    .line 2422392
    return-void
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2422393
    iget-object v0, p0, LX/H4A;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2422394
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2422395
    iget-object v0, p0, LX/H4A;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2422396
    return-void
.end method
