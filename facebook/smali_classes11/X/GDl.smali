.class public LX/GDl;
.super LX/GDY;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GDY",
        "<",
        "LX/AAu;",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentDeleteMutationModels$BoostedComponentDeleteMutationModel;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/GF4;

.field public final c:LX/GG3;

.field private d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/2U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2330369
    invoke-direct/range {p0 .. p5}, LX/GDY;-><init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/2U3;)V

    .line 2330370
    iput-object p3, p0, LX/GDl;->b:LX/GF4;

    .line 2330371
    iput-object p4, p0, LX/GDl;->c:LX/GG3;

    .line 2330372
    return-void
.end method

.method public static a(LX/0QB;)LX/GDl;
    .locals 9

    .prologue
    .line 2330358
    const-class v1, LX/GDl;

    monitor-enter v1

    .line 2330359
    :try_start_0
    sget-object v0, LX/GDl;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2330360
    sput-object v2, LX/GDl;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2330361
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2330362
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2330363
    new-instance v3, LX/GDl;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v6

    check-cast v6, LX/GF4;

    invoke-static {v0}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v7

    check-cast v7, LX/GG3;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v8

    check-cast v8, LX/2U3;

    invoke-direct/range {v3 .. v8}, LX/GDl;-><init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/2U3;)V

    .line 2330364
    move-object v0, v3

    .line 2330365
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2330366
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GDl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2330367
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2330368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)LX/0zP;
    .locals 4

    .prologue
    .line 2330337
    new-instance v0, LX/AAu;

    invoke-direct {v0}, LX/AAu;-><init>()V

    move-object v0, v0

    .line 2330338
    const-string v1, "input"

    .line 2330339
    new-instance v2, LX/4DD;

    invoke-direct {v2}, LX/4DD;-><init>()V

    .line 2330340
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v3

    .line 2330341
    invoke-virtual {v3}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p2

    move-object v3, p2

    .line 2330342
    const-string p2, "boosted_component_app"

    invoke-virtual {v2, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330343
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v3

    .line 2330344
    const-string p2, "page_id"

    invoke-virtual {v2, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330345
    iget-object v3, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v3, v3

    .line 2330346
    if-eqz v3, :cond_0

    .line 2330347
    iget-object v3, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v3, v3

    .line 2330348
    iget-object p2, v3, Lcom/facebook/adinterfaces/model/EventSpecModel;->a:Ljava/lang/String;

    move-object v3, p2

    .line 2330349
    const-string p2, "target_id"

    invoke-virtual {v2, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330350
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2330351
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v3

    .line 2330352
    const-string p2, "object_id"

    invoke-virtual {v2, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330353
    :cond_1
    iget-object v3, p0, LX/GDl;->c:LX/GG3;

    .line 2330354
    iget-object p2, v3, LX/GG3;->g:Ljava/lang/String;

    move-object v3, p2

    .line 2330355
    const-string p2, "flow_id"

    invoke-virtual {v2, p2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330356
    move-object v2, v2

    .line 2330357
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/AAu;

    return-object v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2330334
    iput-object p1, p0, LX/GDl;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330335
    const v0, 0x7f080af0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/GDY;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;IZ)V

    .line 2330336
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/adinterfaces/protocol/BoostedComponentDeleteMutationModels$BoostedComponentDeleteMutationModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2330330
    iget-object v0, p0, LX/GDl;->c:LX/GG3;

    iget-object v1, p0, LX/GDl;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1}, LX/GG3;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2330331
    iput-object v3, p0, LX/GDl;->d:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330332
    iget-object v0, p0, LX/GDl;->b:LX/GF4;

    new-instance v1, LX/GFV;

    iget-object v2, p0, LX/GDY;->a:LX/4At;

    invoke-direct {v1, v2, v3}, LX/GFV;-><init>(LX/4At;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2330333
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2330329
    const/4 v0, 0x0

    return v0
.end method
