.class public final LX/G4F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/service/ProfileLoadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/service/ProfileLoadManager;)V
    .locals 0

    .prologue
    .line 2317264
    iput-object p1, p0, LX/G4F;->a:Lcom/facebook/timeline/service/ProfileLoadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x2e71eb24

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2317265
    iget-object v1, p0, LX/G4F;->a:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v1, v1, Lcom/facebook/timeline/service/ProfileLoadManager;->l:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2317266
    iget-object v1, p0, LX/G4F;->a:Lcom/facebook/timeline/service/ProfileLoadManager;

    .line 2317267
    iget-object v2, v1, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    invoke-virtual {v2}, LX/G4L;->c()LX/0Px;

    move-result-object p1

    .line 2317268
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    const/4 v2, 0x0

    move p0, v2

    :goto_0
    if-ge p0, p2, :cond_0

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/G4K;

    .line 2317269
    iget p3, v2, LX/G4K;->b:I

    add-int/lit8 p3, p3, 0x1

    iput p3, v2, LX/G4K;->b:I

    .line 2317270
    invoke-virtual {v1, v2}, Lcom/facebook/timeline/service/ProfileLoadManager;->a(LX/G4K;)V

    .line 2317271
    add-int/lit8 v2, p0, 0x1

    move p0, v2

    goto :goto_0

    .line 2317272
    :cond_0
    iget-object v2, v1, Lcom/facebook/timeline/service/ProfileLoadManager;->q:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->c()V

    .line 2317273
    :cond_1
    const/16 v1, 0x27

    const v2, -0x141990bf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
