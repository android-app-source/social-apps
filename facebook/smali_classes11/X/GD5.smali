.class public final LX/GD5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68J;


# instance fields
.field public final synthetic a:LX/GD6;


# direct methods
.method public constructor <init>(LX/GD6;)V
    .locals 0

    .prologue
    .line 2329395
    iput-object p1, p0, LX/GD5;->a:LX/GD6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/680;)V
    .locals 7

    .prologue
    .line 2329396
    iget-object v0, p0, LX/GD5;->a:LX/GD6;

    iget-object v0, v0, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->s:LX/GEO;

    invoke-virtual {v0}, LX/GEO;->a()V

    .line 2329397
    iget-object v0, p0, LX/GD5;->a:LX/GD6;

    iget-object v0, v0, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    sget-object v1, LX/GMY;->LOADING:LX/GMY;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setState(LX/GMY;)V

    .line 2329398
    iget-object v0, p0, LX/GD5;->a:LX/GD6;

    iget-object v0, v0, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-boolean v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->J:Z

    if-nez v0, :cond_1

    .line 2329399
    iget-object v0, p0, LX/GD5;->a:LX/GD6;

    iget-object v0, v0, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    const/4 v1, 0x1

    .line 2329400
    iput-boolean v1, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->J:Z

    .line 2329401
    iget-object v0, p0, LX/GD5;->a:LX/GD6;

    iget-object v0, v0, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->q:LX/GNE;

    iget-object v1, p0, LX/GD5;->a:LX/GD6;

    iget-object v1, v1, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    .line 2329402
    iget-object v2, v0, LX/GNE;->a:LX/GND;

    .line 2329403
    iget-object v3, p1, LX/680;->k:LX/31h;

    move-object v3, v3

    .line 2329404
    invoke-static {v3, v1}, LX/GND;->a(LX/31h;Landroid/view/View;)D

    move-result-wide v3

    .line 2329405
    iget-wide v5, v2, LX/GND;->a:D

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->log(D)D

    move-result-wide v3

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    invoke-static {v5, v6}, Ljava/lang/Math;->log(D)D

    move-result-wide v5

    div-double/2addr v3, v5

    .line 2329406
    invoke-virtual {p1}, LX/680;->c()LX/692;

    move-result-object v5

    iget v5, v5, LX/692;->b:F

    float-to-double v5, v5

    add-double/2addr v3, v5

    double-to-float v3, v3

    move v2, v3

    .line 2329407
    move v0, v2

    .line 2329408
    invoke-static {v0}, LX/680;->c(F)F

    move-result v1

    iput v1, p1, LX/680;->a:F

    .line 2329409
    iget-object v1, p1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v1}, Lcom/facebook/android/maps/MapView;->getZoom()F

    move-result v1

    iget v2, p1, LX/680;->a:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 2329410
    iget-object v1, p1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v2, p1, LX/680;->a:F

    invoke-virtual {p1}, LX/680;->k()F

    move-result v3

    invoke-virtual {p1}, LX/680;->l()F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/android/maps/MapView;->c(FFF)Z

    .line 2329411
    iget-object v1, p1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v1}, Lcom/facebook/android/maps/MapView;->invalidate()V

    .line 2329412
    :cond_0
    iget-object v0, p0, LX/GD5;->a:LX/GD6;

    iget-object v0, v0, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->q:LX/GNE;

    iget-object v1, p0, LX/GD5;->a:LX/GD6;

    iget-object v1, v1, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    .line 2329413
    iget-object v2, v0, LX/GNE;->a:LX/GND;

    .line 2329414
    iget-object v3, p1, LX/680;->k:LX/31h;

    move-object v3, v3

    .line 2329415
    invoke-static {v3, v1}, LX/GND;->a(LX/31h;Landroid/view/View;)D

    move-result-wide v3

    .line 2329416
    iget-wide v5, v2, LX/GND;->b:D

    div-double v3, v5, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->log(D)D

    move-result-wide v3

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    invoke-static {v5, v6}, Ljava/lang/Math;->log(D)D

    move-result-wide v5

    div-double/2addr v3, v5

    .line 2329417
    invoke-virtual {p1}, LX/680;->c()LX/692;

    move-result-object v5

    iget v5, v5, LX/692;->b:F

    float-to-double v5, v5

    sub-double v3, v5, v3

    double-to-float v3, v3

    move v2, v3

    .line 2329418
    move v0, v2

    .line 2329419
    invoke-static {v0}, LX/680;->c(F)F

    move-result v1

    iput v1, p1, LX/680;->b:F

    .line 2329420
    iget-object v1, p1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v1}, Lcom/facebook/android/maps/MapView;->getZoom()F

    move-result v1

    iget v2, p1, LX/680;->b:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 2329421
    iget-object v1, p1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    iget v2, p1, LX/680;->b:F

    invoke-virtual {p1}, LX/680;->k()F

    move-result v3

    invoke-virtual {p1}, LX/680;->l()F

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/android/maps/MapView;->c(FFF)Z

    .line 2329422
    iget-object v1, p1, LX/680;->A:Lcom/facebook/android/maps/MapView;

    invoke-virtual {v1}, Lcom/facebook/android/maps/MapView;->invalidate()V

    .line 2329423
    :cond_1
    iget-object v0, p0, LX/GD5;->a:LX/GD6;

    iget-object v0, v0, LX/GD6;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    invoke-virtual {p1, v0}, LX/680;->a(LX/67w;)V

    .line 2329424
    return-void
.end method
