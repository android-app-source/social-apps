.class public LX/Fv3;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ezi;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/BPp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/friends/ui/SmartButtonLite;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2300719
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2300720
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Fv3;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    const/16 v5, 0x225c

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-static {v0}, LX/BPp;->a(LX/0QB;)LX/BPp;

    move-result-object v0

    check-cast v0, LX/BPp;

    iput-object v3, v2, LX/Fv3;->a:LX/23P;

    iput-object v4, v2, LX/Fv3;->b:LX/0wM;

    iput-object v5, v2, LX/Fv3;->c:LX/0Ot;

    iput-object p1, v2, LX/Fv3;->d:LX/0ad;

    iput-object v0, v2, LX/Fv3;->e:LX/BPp;

    .line 2300721
    const v0, 0x7f0314da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2300722
    const v0, 0x7f0d0062

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/ui/SmartButtonLite;

    iput-object v0, p0, LX/Fv3;->f:Lcom/facebook/friends/ui/SmartButtonLite;

    .line 2300723
    return-void
.end method

.method public static a(LX/Fv3;IIIIZ)V
    .locals 4

    .prologue
    .line 2300724
    invoke-virtual {p0}, LX/Fv3;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2300725
    iget-object v1, p0, LX/Fv3;->f:Lcom/facebook/friends/ui/SmartButtonLite;

    if-eqz p5, :cond_0

    iget-object v2, p0, LX/Fv3;->a:LX/23P;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    :cond_0
    invoke-virtual {v1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 2300726
    iget-object v0, p0, LX/Fv3;->f:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/Fv3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2300727
    iget-object v0, p0, LX/Fv3;->f:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p0}, LX/Fv3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextColor(I)V

    .line 2300728
    iget-object v0, p0, LX/Fv3;->f:Lcom/facebook/friends/ui/SmartButtonLite;

    iget-object v1, p0, LX/Fv3;->b:LX/0wM;

    invoke-virtual {p0}, LX/Fv3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0}, LX/Fv3;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2300729
    return-void
.end method
