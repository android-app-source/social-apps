.class public LX/Fyh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final b:LX/0tX;

.field public final c:LX/0Sh;

.field public final d:LX/03V;


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0tX;LX/0Sh;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2307076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2307077
    iput-object p1, p0, LX/Fyh;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2307078
    iput-object p2, p0, LX/Fyh;->b:LX/0tX;

    .line 2307079
    iput-object p3, p0, LX/Fyh;->c:LX/0Sh;

    .line 2307080
    iput-object p4, p0, LX/Fyh;->d:LX/03V;

    .line 2307081
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2307082
    iget-object v0, p0, LX/Fyh;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v0, p1, p2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2307083
    return-void
.end method
