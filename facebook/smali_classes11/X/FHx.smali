.class public final enum LX/FHx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHx;

.field public static final enum PHASE_ONE:LX/FHx;

.field public static final enum PHASE_TWO:LX/FHx;


# instance fields
.field private phase:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2221249
    new-instance v0, LX/FHx;

    const-string v1, "PHASE_ONE"

    invoke-direct {v0, v1, v3, v2}, LX/FHx;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FHx;->PHASE_ONE:LX/FHx;

    .line 2221250
    new-instance v0, LX/FHx;

    const-string v1, "PHASE_TWO"

    invoke-direct {v0, v1, v2, v4}, LX/FHx;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FHx;->PHASE_TWO:LX/FHx;

    .line 2221251
    new-array v0, v4, [LX/FHx;

    sget-object v1, LX/FHx;->PHASE_ONE:LX/FHx;

    aput-object v1, v0, v3

    sget-object v1, LX/FHx;->PHASE_TWO:LX/FHx;

    aput-object v1, v0, v2

    sput-object v0, LX/FHx;->$VALUES:[LX/FHx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2221255
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/FHx;->phase:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHx;
    .locals 1

    .prologue
    .line 2221254
    const-class v0, LX/FHx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHx;

    return-object v0
.end method

.method public static values()[LX/FHx;
    .locals 1

    .prologue
    .line 2221253
    sget-object v0, LX/FHx;->$VALUES:[LX/FHx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHx;

    return-object v0
.end method


# virtual methods
.method public final getIndex()I
    .locals 1

    .prologue
    .line 2221252
    iget v0, p0, LX/FHx;->phase:I

    return v0
.end method
