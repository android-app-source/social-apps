.class public final LX/Gyf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/location/ui/LocationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 0

    .prologue
    .line 2410263
    iput-object p1, p0, LX/Gyf;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x37197683

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2410252
    iget-object v1, p0, LX/Gyf;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v1, v1, Lcom/facebook/location/ui/LocationSettingsFragment;->o:LX/GyW;

    .line 2410253
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "location_settings_learn_more"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "background_location"

    .line 2410254
    iput-object p1, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2410255
    move-object v3, v3

    .line 2410256
    iget-object p1, v1, LX/GyW;->a:LX/0Zb;

    invoke-interface {p1, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2410257
    iget-object v1, p0, LX/Gyf;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    .line 2410258
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string p0, "fb"

    invoke-virtual {v3, p0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string p0, "faceweb"

    invoke-virtual {v3, p0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string p0, "f"

    invoke-virtual {v3, p0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string p0, "href"

    const-string p1, "/settings/location/learnmore"

    invoke-virtual {v3, p0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 2410259
    new-instance p0, Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    invoke-direct {p0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2410260
    invoke-virtual {p0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2410261
    iget-object v3, v1, Lcom/facebook/location/ui/LocationSettingsFragment;->j:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2410262
    const v1, 0x2f5a9047

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
