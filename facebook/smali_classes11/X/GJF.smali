.class public final LX/GJF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GJK;


# direct methods
.method public constructor <init>(LX/GJK;)V
    .locals 0

    .prologue
    .line 2338047
    iput-object p1, p0, LX/GJF;->a:LX/GJK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2338048
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 2338049
    :cond_0
    :goto_0
    return-void

    .line 2338050
    :cond_1
    const-string v0, "data"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2338051
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v1

    iget-object v2, p0, LX/GJF;->a:LX/GJK;

    iget-object v2, v2, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, LX/GJF;->a:LX/GJK;

    iget-object v1, v1, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 2338052
    :cond_2
    iget-object v1, p0, LX/GJF;->a:LX/GJK;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    .line 2338053
    iput-object v2, v1, LX/GJK;->o:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2338054
    iget-object v1, p0, LX/GJF;->a:LX/GJK;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v2

    .line 2338055
    iput v2, v1, LX/GJK;->p:I

    .line 2338056
    iget-object v1, p0, LX/GJF;->a:LX/GJK;

    iget-object v1, v1, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    iget-object v2, p0, LX/GJF;->a:LX/GJK;

    iget v2, v2, LX/GJK;->k:I

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2338057
    iget-object v1, p0, LX/GJF;->a:LX/GJK;

    iget-object v1, v1, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v2

    .line 2338058
    iput v2, v1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2338059
    iget-object v1, p0, LX/GJF;->a:LX/GJK;

    iget-object v1, v1, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2338060
    iget-object v0, p0, LX/GJF;->a:LX/GJK;

    iget-object v0, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    iget-object v1, p0, LX/GJF;->a:LX/GJK;

    iget-object v1, v1, LX/GJK;->l:LX/GK4;

    invoke-virtual {v1}, LX/GK4;->a()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->a(Landroid/text/Spanned;)V

    .line 2338061
    iget-object v0, p0, LX/GJF;->a:LX/GJK;

    iget-object v0, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    iget-object v1, p0, LX/GJF;->a:LX/GJK;

    iget-object v1, v1, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    check-cast v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v2, p0, LX/GJF;->a:LX/GJK;

    iget-object v2, v2, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v2, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v2, p0, LX/GJF;->a:LX/GJK;

    iget-object v2, v2, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v2, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v3, v2}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;Landroid/content/res/Resources;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->setSummaryText(Landroid/text/Spanned;)V

    .line 2338062
    iget-object v0, p0, LX/GJF;->a:LX/GJK;

    invoke-static {v0}, LX/GJK;->A(LX/GJK;)V

    goto/16 :goto_0
.end method
