.class public LX/HBN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:Landroid/os/Bundle;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2437139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2437140
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, LX/HBN;->b:Ljava/util/Map;

    .line 2437141
    return-void
.end method

.method public static a(LX/0QB;)LX/HBN;
    .locals 3

    .prologue
    .line 2437142
    const-class v1, LX/HBN;

    monitor-enter v1

    .line 2437143
    :try_start_0
    sget-object v0, LX/HBN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2437144
    sput-object v2, LX/HBN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2437145
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2437146
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2437147
    new-instance v0, LX/HBN;

    invoke-direct {v0}, LX/HBN;-><init>()V

    .line 2437148
    move-object v0, v0

    .line 2437149
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2437150
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HBN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2437151
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2437152
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2437153
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/HBN;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;)V
    .locals 1

    .prologue
    .line 2437154
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/HBN;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2437155
    monitor-exit p0

    return-void

    .line 2437156
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2437157
    const/4 v0, 0x0

    iput-object v0, p0, LX/HBN;->a:Landroid/os/Bundle;

    .line 2437158
    iget-object v0, p0, LX/HBN;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2437159
    return-void
.end method
