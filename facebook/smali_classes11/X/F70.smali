.class public final LX/F70;
.super LX/2hO;
.source ""


# instance fields
.field public final synthetic a:LX/F73;


# direct methods
.method public constructor <init>(LX/F73;)V
    .locals 0

    .prologue
    .line 2201039
    iput-object p1, p0, LX/F70;->a:LX/F73;

    invoke-direct {p0}, LX/2hO;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 10

    .prologue
    .line 2201040
    check-cast p1, LX/2iB;

    const/4 v4, -0x1

    .line 2201041
    if-nez p1, :cond_1

    .line 2201042
    :cond_0
    :goto_0
    return-void

    .line 2201043
    :cond_1
    iget-object v0, p0, LX/F70;->a:LX/F73;

    iget-object v0, v0, LX/F73;->g:Ljava/util/Map;

    iget-wide v2, p1, LX/2iB;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2201044
    if-eqz v0, :cond_0

    .line 2201045
    iget-object v1, p0, LX/F70;->a:LX/F73;

    invoke-virtual {v0}, LX/F7L;->a()J

    move-result-wide v2

    .line 2201046
    const/4 v5, 0x0

    move v6, v5

    :goto_1
    iget-object v5, v1, LX/F73;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_5

    .line 2201047
    iget-object v5, v1, LX/F73;->f:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/F7L;

    invoke-virtual {v5}, LX/F7L;->a()J

    move-result-wide v7

    cmp-long v5, v7, v2

    if-nez v5, :cond_4

    .line 2201048
    :goto_2
    move v1, v6

    .line 2201049
    if-eq v1, v4, :cond_2

    .line 2201050
    iget-object v2, p0, LX/F70;->a:LX/F73;

    iget-object v2, v2, LX/F73;->f:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2201051
    :cond_2
    iget-object v1, p0, LX/F70;->a:LX/F73;

    invoke-virtual {v0}, LX/F7L;->a()J

    move-result-wide v2

    .line 2201052
    const/4 v5, 0x0

    move v6, v5

    :goto_3
    iget-object v5, v1, LX/F73;->l:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_7

    .line 2201053
    iget-object v5, v1, LX/F73;->l:Ljava/util/List;

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/F7L;

    invoke-virtual {v5}, LX/F7L;->a()J

    move-result-wide v7

    cmp-long v5, v7, v2

    if-nez v5, :cond_6

    .line 2201054
    :goto_4
    move v0, v6

    .line 2201055
    if-eq v0, v4, :cond_3

    .line 2201056
    iget-object v1, p0, LX/F70;->a:LX/F73;

    iget-object v1, v1, LX/F73;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2201057
    :cond_3
    iget-object v0, p0, LX/F70;->a:LX/F73;

    const v1, 0x20bff86b

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2201058
    :cond_4
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 2201059
    :cond_5
    const/4 v6, -0x1

    goto :goto_2

    .line 2201060
    :cond_6
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_3

    .line 2201061
    :cond_7
    const/4 v6, -0x1

    goto :goto_4
.end method
