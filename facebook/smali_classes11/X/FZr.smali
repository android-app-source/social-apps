.class public LX/FZr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FZr;


# instance fields
.field public final a:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2258096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258097
    iput-object p1, p0, LX/FZr;->a:LX/0aG;

    .line 2258098
    return-void
.end method

.method public static a(LX/0QB;)LX/FZr;
    .locals 4

    .prologue
    .line 2258099
    sget-object v0, LX/FZr;->b:LX/FZr;

    if-nez v0, :cond_1

    .line 2258100
    const-class v1, LX/FZr;

    monitor-enter v1

    .line 2258101
    :try_start_0
    sget-object v0, LX/FZr;->b:LX/FZr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2258102
    if-eqz v2, :cond_0

    .line 2258103
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2258104
    new-instance p0, LX/FZr;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-direct {p0, v3}, LX/FZr;-><init>(LX/0aG;)V

    .line 2258105
    move-object v0, p0

    .line 2258106
    sput-object v0, LX/FZr;->b:LX/FZr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2258107
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2258108
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2258109
    :cond_1
    sget-object v0, LX/FZr;->b:LX/FZr;

    return-object v0

    .line 2258110
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2258111
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
