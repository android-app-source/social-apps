.class public final LX/H09;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2413484
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2413485
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2413486
    :goto_0
    return v1

    .line 2413487
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2413488
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_7

    .line 2413489
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2413490
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2413491
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 2413492
    const-string v7, "__type__"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "__typename"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2413493
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    goto :goto_1

    .line 2413494
    :cond_3
    const-string v7, "config_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2413495
    invoke-static {p0, p1}, LX/H08;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2413496
    :cond_4
    const-string v7, "description"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2413497
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2413498
    :cond_5
    const-string v7, "flow_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2413499
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLComponentFlowType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLComponentFlowType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_1

    .line 2413500
    :cond_6
    const-string v7, "name"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2413501
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2413502
    :cond_7
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2413503
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2413504
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2413505
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2413506
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2413507
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2413508
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 2413509
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2413510
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2413511
    if-eqz v0, :cond_0

    .line 2413512
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413513
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2413514
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2413515
    if-eqz v0, :cond_1

    .line 2413516
    const-string v1, "config_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413517
    invoke-static {p0, v0, p2, p3}, LX/H08;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2413518
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2413519
    if-eqz v0, :cond_2

    .line 2413520
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413521
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2413522
    :cond_2
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2413523
    if-eqz v0, :cond_3

    .line 2413524
    const-string v0, "flow_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413525
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2413526
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2413527
    if-eqz v0, :cond_4

    .line 2413528
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2413529
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2413530
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2413531
    return-void
.end method
