.class public LX/FVx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FVs;


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2251489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2251490
    iput-object p1, p0, LX/FVx;->a:Ljava/lang/String;

    .line 2251491
    return-void
.end method


# virtual methods
.method public final a()LX/FV0;
    .locals 1

    .prologue
    .line 2251492
    sget-object v0, LX/FV0;->SAVED_DASHBOARD_LIST_SECTION_HEADER:LX/FV0;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2251493
    if-ne p0, p1, :cond_0

    .line 2251494
    const/4 v0, 0x1

    .line 2251495
    :goto_0
    return v0

    .line 2251496
    :cond_0
    instance-of v0, p1, LX/FVx;

    if-nez v0, :cond_1

    .line 2251497
    const/4 v0, 0x0

    goto :goto_0

    .line 2251498
    :cond_1
    check-cast p1, LX/FVx;

    .line 2251499
    iget-object v0, p0, LX/FVx;->a:Ljava/lang/String;

    iget-object v1, p1, LX/FVx;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2251500
    iget-object v0, p0, LX/FVx;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
