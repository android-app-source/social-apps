.class public LX/GIx;
.super LX/GIw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GIw",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/base/activity/FbFragmentActivity;


# direct methods
.method public constructor <init>(LX/GK4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337464
    invoke-direct {p0, p1}, LX/GIw;-><init>(LX/GK4;)V

    .line 2337465
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337466
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0, p1, p2}, LX/GIw;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2

    .prologue
    .line 2337467
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    iput-object v0, p0, LX/GIx;->a:Lcom/facebook/base/activity/FbFragmentActivity;

    .line 2337468
    invoke-super {p0, p1, p2}, LX/GIw;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337469
    return-void
.end method

.method public final b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337470
    new-instance v0, LX/GIu;

    invoke-direct {v0, p0}, LX/GIu;-><init>(LX/GIx;)V

    return-object v0
.end method

.method public final c()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337471
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337472
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2337473
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337474
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337475
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337476
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337477
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337478
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337479
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337480
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337481
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337482
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    .line 2337483
    return-void
.end method
