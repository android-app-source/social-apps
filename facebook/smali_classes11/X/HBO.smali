.class public final enum LX/HBO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HBO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HBO;

.field public static final enum DETAIL:LX/HBO;

.field public static final enum TITLE:LX/HBO;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2437160
    new-instance v0, LX/HBO;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v2}, LX/HBO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HBO;->TITLE:LX/HBO;

    .line 2437161
    new-instance v0, LX/HBO;

    const-string v1, "DETAIL"

    invoke-direct {v0, v1, v3}, LX/HBO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HBO;->DETAIL:LX/HBO;

    .line 2437162
    const/4 v0, 0x2

    new-array v0, v0, [LX/HBO;

    sget-object v1, LX/HBO;->TITLE:LX/HBO;

    aput-object v1, v0, v2

    sget-object v1, LX/HBO;->DETAIL:LX/HBO;

    aput-object v1, v0, v3

    sput-object v0, LX/HBO;->$VALUES:[LX/HBO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2437163
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HBO;
    .locals 1

    .prologue
    .line 2437164
    const-class v0, LX/HBO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HBO;

    return-object v0
.end method

.method public static values()[LX/HBO;
    .locals 1

    .prologue
    .line 2437165
    sget-object v0, LX/HBO;->$VALUES:[LX/HBO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HBO;

    return-object v0
.end method
