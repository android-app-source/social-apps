.class public LX/GZA;
.super LX/GZ5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GZ5",
        "<",
        "LX/Ga2;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

.field private b:Landroid/content/Context;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/7iP;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/7iP;)V
    .locals 1

    .prologue
    .line 2366030
    invoke-direct {p0}, LX/GZ5;-><init>()V

    .line 2366031
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GZA;->c:Ljava/util/List;

    .line 2366032
    iput-object p1, p0, LX/GZA;->b:Landroid/content/Context;

    .line 2366033
    iput-object p2, p0, LX/GZA;->a:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2366034
    iput-object p3, p0, LX/GZA;->d:LX/7iP;

    .line 2366035
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2366036
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2366037
    new-instance v0, LX/Ga1;

    iget-object v1, p0, LX/GZA;->b:Landroid/content/Context;

    iget-object v2, p0, LX/GZA;->d:LX/7iP;

    invoke-direct {v0, v1, v2}, LX/Ga1;-><init>(Landroid/content/Context;LX/7iP;)V

    .line 2366038
    new-instance v1, LX/1a3;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, LX/1a3;-><init>(II)V

    .line 2366039
    invoke-virtual {v0, v1}, LX/Ga1;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2366040
    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, LX/Ga1;->setBackgroundResource(I)V

    .line 2366041
    new-instance v1, LX/Ga0;

    invoke-direct {v1, v0}, LX/Ga0;-><init>(LX/Ga1;)V

    invoke-virtual {v0, v1}, LX/Ga1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2366042
    new-instance v1, LX/Ga2;

    invoke-direct {v1, v0}, LX/Ga2;-><init>(LX/Ga1;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2366043
    check-cast p1, LX/Ga2;

    .line 2366044
    iget-object v0, p0, LX/GZA;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    move-object v0, v0

    .line 2366045
    iget-object v1, p0, LX/GZA;->a:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    invoke-virtual {p1, v0, v1}, LX/Ga2;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)V

    .line 2366046
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2366047
    const/4 v0, 0x2

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2366048
    iget-object v0, p0, LX/GZA;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
