.class public final enum LX/Fr2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fr2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fr2;

.field public static final enum INTRO_CARD_PUBLIC_ABOUT_ITEMS_STYLE:LX/Fr2;

.field public static final enum PROTILE_STYLE:LX/Fr2;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2294923
    new-instance v0, LX/Fr2;

    const-string v1, "PROTILE_STYLE"

    invoke-direct {v0, v1, v2}, LX/Fr2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fr2;->PROTILE_STYLE:LX/Fr2;

    .line 2294924
    new-instance v0, LX/Fr2;

    const-string v1, "INTRO_CARD_PUBLIC_ABOUT_ITEMS_STYLE"

    invoke-direct {v0, v1, v3}, LX/Fr2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fr2;->INTRO_CARD_PUBLIC_ABOUT_ITEMS_STYLE:LX/Fr2;

    .line 2294925
    const/4 v0, 0x2

    new-array v0, v0, [LX/Fr2;

    sget-object v1, LX/Fr2;->PROTILE_STYLE:LX/Fr2;

    aput-object v1, v0, v2

    sget-object v1, LX/Fr2;->INTRO_CARD_PUBLIC_ABOUT_ITEMS_STYLE:LX/Fr2;

    aput-object v1, v0, v3

    sput-object v0, LX/Fr2;->$VALUES:[LX/Fr2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2294926
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fr2;
    .locals 1

    .prologue
    .line 2294927
    const-class v0, LX/Fr2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fr2;

    return-object v0
.end method

.method public static values()[LX/Fr2;
    .locals 1

    .prologue
    .line 2294928
    sget-object v0, LX/Fr2;->$VALUES:[LX/Fr2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fr2;

    return-object v0
.end method
