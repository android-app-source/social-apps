.class public LX/Fvi;
.super LX/Fv4;
.source ""

# interfaces
.implements LX/Fv6;


# instance fields
.field public final e:LX/FwD;

.field public final f:LX/Fue;

.field private final g:LX/Fuj;

.field private final h:LX/Fvz;

.field public final i:LX/Fw8;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/Fw7;

.field public final l:Z

.field public final m:LX/Fv9;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/8p8;

.field private q:LX/FvY;

.field public r:LX/FvZ;

.field private s:LX/Fva;

.field public t:LX/Fvw;

.field private u:I

.field private final v:[I


# direct methods
.method public constructor <init>(LX/Fuf;LX/Fuk;LX/FwD;LX/Fw8;LX/0ad;LX/8p8;LX/Fvz;LX/0Ot;LX/Fw7;LX/0Ot;LX/0Ot;Landroid/content/Context;LX/BQ1;LX/G4m;LX/BP0;ZLX/Fv9;)V
    .locals 4
    .param p12    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/Fv9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fuf;",
            "LX/Fuk;",
            "LX/FwD;",
            "LX/Fw8;",
            "LX/0ad;",
            "LX/8p8;",
            "LX/Fvz;",
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;",
            "LX/Fw7;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Landroid/content/Context;",
            "LX/BQ1;",
            "LX/G4m;",
            "LX/BP0;",
            "Z",
            "LX/Fv9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2301875
    move-object/from16 v0, p12

    move-object/from16 v1, p15

    move-object/from16 v2, p13

    invoke-direct {p0, v0, p5, v1, v2}, LX/Fv4;-><init>(Landroid/content/Context;LX/0ad;LX/BP0;LX/BQ1;)V

    .line 2301876
    const/4 v3, -0x1

    iput v3, p0, LX/Fvi;->u:I

    .line 2301877
    invoke-static {}, LX/Fvh;->cachedValues()[LX/Fvh;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    iput-object v3, p0, LX/Fvi;->v:[I

    .line 2301878
    iput-object p3, p0, LX/Fvi;->e:LX/FwD;

    .line 2301879
    iput-object p4, p0, LX/Fvi;->i:LX/Fw8;

    .line 2301880
    iput-object p8, p0, LX/Fvi;->j:LX/0Ot;

    .line 2301881
    iput-object p9, p0, LX/Fvi;->k:LX/Fw7;

    .line 2301882
    move/from16 v0, p16

    iput-boolean v0, p0, LX/Fvi;->l:Z

    .line 2301883
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Fvi;->m:LX/Fv9;

    .line 2301884
    iput-object p10, p0, LX/Fvi;->n:LX/0Ot;

    .line 2301885
    iput-object p11, p0, LX/Fvi;->o:LX/0Ot;

    .line 2301886
    iput-object p6, p0, LX/Fvi;->p:LX/8p8;

    .line 2301887
    iput-object p7, p0, LX/Fvi;->h:LX/Fvz;

    .line 2301888
    invoke-direct {p0}, LX/Fvi;->m()LX/Fva;

    move-result-object v3

    move-object/from16 v0, p13

    move-object/from16 v1, p14

    move-object/from16 v2, p15

    invoke-virtual {p1, v3, v0, v1, v2}, LX/Fuf;->a(LX/Fva;LX/BQ1;LX/G4m;LX/BP0;)LX/Fue;

    move-result-object v3

    iput-object v3, p0, LX/Fvi;->f:LX/Fue;

    .line 2301889
    invoke-direct {p0}, LX/Fvi;->j()LX/FvY;

    move-result-object v3

    move-object/from16 v0, p13

    move-object/from16 v1, p15

    invoke-virtual {p2, v3, v0, v1}, LX/Fuk;->a(LX/FvY;LX/BQ1;LX/BP0;)LX/Fuj;

    move-result-object v3

    iput-object v3, p0, LX/Fvi;->g:LX/Fuj;

    .line 2301890
    return-void
.end method

.method private b(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2301874
    iget-object v0, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private e()Z
    .locals 5

    .prologue
    .line 2301872
    iget-object v0, p0, LX/Fvi;->e:LX/FwD;

    iget-object v1, p0, LX/Fv4;->d:LX/BQ1;

    iget-object v2, p0, LX/Fv4;->c:LX/BP0;

    iget-object v3, p0, LX/Fvi;->e:LX/FwD;

    iget-object v4, p0, LX/Fv4;->c:LX/BP0;

    invoke-virtual {v3, v4}, LX/FwD;->a(LX/5SB;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/FwD;->a(LX/BQ1;LX/5SB;Z)Ljava/lang/Integer;

    move-result-object v0

    move-object v0, v0

    .line 2301873
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()LX/FvY;
    .locals 1

    .prologue
    .line 2301869
    iget-object v0, p0, LX/Fvi;->q:LX/FvY;

    if-nez v0, :cond_0

    .line 2301870
    new-instance v0, LX/FvY;

    invoke-direct {v0, p0}, LX/FvY;-><init>(LX/Fvi;)V

    iput-object v0, p0, LX/Fvi;->q:LX/FvY;

    .line 2301871
    :cond_0
    iget-object v0, p0, LX/Fvi;->q:LX/FvY;

    return-object v0
.end method

.method public static l(LX/Fvi;)V
    .locals 4

    .prologue
    .line 2301861
    iget-object v0, p0, LX/Fvi;->i:LX/Fw8;

    iget-object v1, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301862
    const/4 v2, 0x0

    .line 2301863
    iput-boolean v2, v1, LX/BQ1;->c:Z

    .line 2301864
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Fw8;->c:Z

    .line 2301865
    iget-object v2, v0, LX/Fw8;->a:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    const-string v3, "3621"

    invoke-virtual {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->d(Ljava/lang/String;)V

    .line 2301866
    iget-object v0, p0, LX/Fvi;->t:LX/Fvw;

    invoke-virtual {v0}, LX/Fvw;->b()V

    .line 2301867
    const v0, -0x798b3f13

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2301868
    return-void
.end method

.method private m()LX/Fva;
    .locals 1

    .prologue
    .line 2301858
    iget-object v0, p0, LX/Fvi;->s:LX/Fva;

    if-nez v0, :cond_0

    .line 2301859
    new-instance v0, LX/Fva;

    invoke-direct {v0, p0}, LX/Fva;-><init>(LX/Fvi;)V

    iput-object v0, p0, LX/Fvi;->s:LX/Fva;

    .line 2301860
    :cond_0
    iget-object v0, p0, LX/Fvi;->s:LX/Fva;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2301891
    invoke-static {}, LX/Fvh;->cachedValues()[LX/Fvh;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2301852
    invoke-static {}, LX/Fvh;->cachedValues()[LX/Fvh;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2301853
    sget-object v1, LX/Fvb;->a:[I

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2301854
    invoke-static {p1}, LX/Fv4;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2301855
    :pswitch_0
    const v0, 0x7f0314c6

    invoke-direct {p0, v0, p2}, LX/Fvi;->b(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2301856
    :pswitch_1
    const v0, 0x7f0314e6

    invoke-direct {p0, v0, p2}, LX/Fvi;->b(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2301857
    :pswitch_2
    const v0, 0x7f0314e5

    invoke-direct {p0, v0, p2}, LX/Fvi;->b(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2301851
    invoke-static {}, LX/Fvh;->cachedValues()[LX/Fvh;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a([Z)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2301827
    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2301828
    sget-object v0, LX/Fvh;->MEGAPHONE:LX/Fvh;

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v0

    aput-boolean v2, p1, v0

    .line 2301829
    sget-object v0, LX/Fvh;->BIO:LX/Fvh;

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v0

    aput-boolean v2, p1, v0

    .line 2301830
    sget-object v0, LX/Fvh;->INTRO_CARD:LX/Fvh;

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v0

    :cond_0
    move v1, v2

    .line 2301831
    :cond_1
    :goto_0
    aput-boolean v1, p1, v0

    .line 2301832
    return-void

    .line 2301833
    :cond_2
    sget-object v0, LX/Fvh;->MEGAPHONE:LX/Fvh;

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v3

    iget-boolean v0, p0, LX/Fvi;->l:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/Fvi;->i:LX/Fw8;

    iget-object v4, p0, LX/Fv4;->c:LX/BP0;

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0, v4, v5}, LX/Fw8;->a(LX/5SB;LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    aput-boolean v0, p1, v3

    .line 2301834
    sget-object v0, LX/Fvh;->BIO:LX/Fvh;

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v0

    invoke-direct {p0}, LX/Fvi;->e()Z

    move-result v3

    aput-boolean v3, p1, v0

    .line 2301835
    sget-object v0, LX/Fvh;->INTRO_CARD:LX/Fvh;

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v0

    .line 2301836
    iget-object v4, p0, LX/Fv4;->c:LX/BP0;

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    iget-object v6, p0, LX/Fvi;->k:LX/Fw7;

    iget-object v7, p0, LX/Fv4;->c:LX/BP0;

    iget-object v8, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v6, v7, v8}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v6

    .line 2301837
    iget-object v3, v5, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v3, v3

    .line 2301838
    invoke-static {v3}, LX/FwD;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;)I

    move-result v3

    if-lez v3, :cond_4

    .line 2301839
    const/4 v3, 0x1

    .line 2301840
    :goto_2
    move v3, v3

    .line 2301841
    move v3, v3

    .line 2301842
    if-nez v3, :cond_1

    .line 2301843
    iget-object v3, p0, LX/Fvi;->p:LX/8p8;

    invoke-virtual {v3}, LX/8p8;->c()Z

    move-result v3

    if-nez v3, :cond_6

    .line 2301844
    iget-object v3, p0, LX/Fvi;->e:LX/FwD;

    iget-object v4, p0, LX/Fv4;->d:LX/BQ1;

    iget-object v5, p0, LX/Fv4;->c:LX/BP0;

    iget-object v6, p0, LX/Fvi;->k:LX/Fw7;

    iget-object v7, p0, LX/Fv4;->c:LX/BP0;

    iget-object v8, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v6, v7, v8}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/FwD;->a(LX/BQ1;LX/5SB;LX/Fve;)Ljava/lang/Integer;

    move-result-object v3

    move-object v3, v3

    .line 2301845
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x3

    invoke-static {v3, v4}, LX/3CW;->c(II)Z

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x1

    :goto_3
    move v3, v3

    .line 2301846
    if-eqz v3, :cond_0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2301847
    goto :goto_1

    .line 2301848
    :cond_4
    sget-object v3, LX/Fve;->COLLAPSED:LX/Fve;

    if-ne v6, v3, :cond_5

    .line 2301849
    invoke-static {v4, v5}, LX/FwD;->b(LX/5SB;LX/BQ1;)Z

    move-result v3

    goto :goto_2

    .line 2301850
    :cond_5
    invoke-virtual {v4}, LX/5SB;->i()Z

    move-result v3

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    goto :goto_3
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2301804
    invoke-static {}, LX/Fvh;->cachedValues()[LX/Fvh;

    move-result-object v0

    aget-object v0, v0, p2

    .line 2301805
    iget-object v3, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    .line 2301806
    iget v4, p0, LX/Fvi;->u:I

    if-ne v4, v3, :cond_0

    iget-object v4, p0, LX/Fvi;->v:[I

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v5

    aget v4, v4, v5

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301807
    iget v6, v5, LX/BPy;->c:I

    move v5, v6

    .line 2301808
    if-ne v4, v5, :cond_0

    move v1, v2

    .line 2301809
    :goto_0
    return v1

    .line 2301810
    :cond_0
    iput v3, p0, LX/Fvi;->u:I

    .line 2301811
    iget-object v3, p0, LX/Fvi;->v:[I

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v4

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301812
    iget v6, v5, LX/BPy;->c:I

    move v5, v6

    .line 2301813
    aput v5, v3, v4

    .line 2301814
    sget-object v3, LX/Fvh;->MEGAPHONE:LX/Fvh;

    if-ne v0, v3, :cond_2

    .line 2301815
    iget-object v0, p0, LX/Fvi;->h:LX/Fvz;

    check-cast p1, Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;

    .line 2301816
    iget-object v2, p0, LX/Fvi;->r:LX/FvZ;

    if-nez v2, :cond_1

    .line 2301817
    new-instance v2, LX/FvZ;

    invoke-direct {v2, p0}, LX/FvZ;-><init>(LX/Fvi;)V

    iput-object v2, p0, LX/Fvi;->r:LX/FvZ;

    .line 2301818
    :cond_1
    iget-object v2, p0, LX/Fvi;->r:LX/FvZ;

    move-object v2, v2

    .line 2301819
    iget-object v3, p0, LX/Fv4;->c:LX/BP0;

    invoke-virtual {v0, p1, v2, v3}, LX/Fvz;->a(Lcom/facebook/timeline/header/TimelineIntroMegaphoneView;LX/FvZ;LX/BP0;)V

    goto :goto_0

    .line 2301820
    :cond_2
    sget-object v3, LX/Fvh;->BIO:LX/Fvh;

    if-ne v0, v3, :cond_4

    .line 2301821
    iget-object v3, p0, LX/Fvi;->g:LX/Fuj;

    check-cast p1, Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    iget-boolean v0, p0, LX/Fvi;->l:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/Fvi;->i:LX/Fw8;

    iget-object v4, p0, LX/Fv4;->c:LX/BP0;

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0, v4, v5}, LX/Fw8;->a(LX/5SB;LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, p1, v0}, LX/Fuj;->a(Lcom/facebook/timeline/header/TimelineIntroCardBioView;Z)Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 2301822
    :cond_4
    sget-object v3, LX/Fvh;->INTRO_CARD:LX/Fvh;

    if-ne v0, v3, :cond_7

    .line 2301823
    iget-object v4, p0, LX/Fvi;->f:LX/Fue;

    move-object v0, p1

    check-cast v0, Lcom/facebook/timeline/header/TimelineIntroCardView;

    invoke-direct {p0}, LX/Fvi;->e()Z

    move-result v3

    if-nez v3, :cond_6

    move v3, v1

    :goto_2
    iget-boolean v5, p0, LX/Fvi;->l:Z

    if-nez v5, :cond_5

    iget-object v5, p0, LX/Fvi;->i:LX/Fw8;

    iget-object v6, p0, LX/Fv4;->c:LX/BP0;

    iget-object v7, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v5, v6, v7}, LX/Fw8;->a(LX/5SB;LX/BQ1;)Z

    move-result v5

    if-eqz v5, :cond_5

    move v2, v1

    :cond_5
    invoke-virtual {v4, v0, v3, v2}, LX/Fue;->a(Lcom/facebook/timeline/header/TimelineIntroCardView;ZZ)V

    .line 2301824
    new-instance v0, Lcom/facebook/timeline/header/TimelineIntroCardAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/facebook/timeline/header/TimelineIntroCardAdapter$1;-><init>(LX/Fvi;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_6
    move v3, v2

    .line 2301825
    goto :goto_2

    .line 2301826
    :cond_7
    invoke-static {p2}, LX/Fv4;->c(I)Z

    move-result v1

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2301799
    const/4 v0, -0x1

    iput v0, p0, LX/Fvi;->u:I

    move v0, v1

    .line 2301800
    :goto_0
    iget-object v2, p0, LX/Fvi;->v:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2301801
    iget-object v2, p0, LX/Fvi;->v:[I

    aput v1, v2, v0

    .line 2301802
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2301803
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2301798
    invoke-virtual {p0, p1}, LX/Fv4;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fvh;

    invoke-virtual {v0}, LX/Fvh;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2301797
    invoke-static {}, LX/Fvh;->cachedValues()[LX/Fvh;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
