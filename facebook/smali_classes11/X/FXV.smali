.class public abstract LX/FXV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/support/annotation/MainThread;
.end annotation


# static fields
.field public static final a:LX/FXV;

.field public static final b:LX/FXV;

.field public static final c:LX/FXV;

.field public static final d:LX/FXV;

.field public static final e:LX/FXV;

.field public static final f:LX/FXV;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2255145
    new-instance v0, LX/FXW;

    invoke-direct {v0}, LX/FXW;-><init>()V

    sput-object v0, LX/FXV;->a:LX/FXV;

    .line 2255146
    new-instance v0, LX/FXX;

    invoke-direct {v0}, LX/FXX;-><init>()V

    sput-object v0, LX/FXV;->b:LX/FXV;

    .line 2255147
    new-instance v0, LX/FXZ;

    invoke-direct {v0}, LX/FXZ;-><init>()V

    sput-object v0, LX/FXV;->c:LX/FXV;

    .line 2255148
    new-instance v0, LX/FXa;

    invoke-direct {v0}, LX/FXa;-><init>()V

    sput-object v0, LX/FXV;->d:LX/FXV;

    .line 2255149
    new-instance v0, LX/FXb;

    invoke-direct {v0}, LX/FXb;-><init>()V

    sput-object v0, LX/FXV;->e:LX/FXV;

    .line 2255150
    new-instance v0, LX/FXh;

    invoke-direct {v0}, LX/FXh;-><init>()V

    sput-object v0, LX/FXV;->f:LX/FXV;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2255143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2255144
    return-void
.end method


# virtual methods
.method public a(LX/FXj;)V
    .locals 0

    .prologue
    .line 2255142
    return-void
.end method

.method public a(LX/FXj;I)V
    .locals 5

    .prologue
    .line 2255112
    const-string v0, "LoadItemsController"

    const-string v1, "Unhandled: %s %s: numItems = %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/FXj;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 2255113
    iget-object v4, p1, LX/FXj;->a:LX/FXV;

    move-object v4, v4

    .line 2255114
    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2255115
    return-void
.end method

.method public a(LX/FXj;LX/BO1;)V
    .locals 3

    .prologue
    .line 2255139
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2255140
    iget-object v2, p1, LX/FXj;->a:LX/FXV;

    move-object v2, v2

    .line 2255141
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(LX/FXj;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2255151
    const-string v0, "LoadItemsController"

    const-string v1, "Unhandled: %s %s: error: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, LX/FXj;->f:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 2255152
    iget-object v4, p1, LX/FXj;->a:LX/FXV;

    move-object v4, v4

    .line 2255153
    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2255154
    return-void
.end method

.method public b(LX/FXj;)V
    .locals 3

    .prologue
    .line 2255136
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2255137
    iget-object v2, p1, LX/FXj;->a:LX/FXV;

    move-object v2, v2

    .line 2255138
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(LX/FXj;)V
    .locals 3

    .prologue
    .line 2255133
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2255134
    iget-object v2, p1, LX/FXj;->a:LX/FXV;

    move-object v2, v2

    .line 2255135
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d(LX/FXj;)V
    .locals 3

    .prologue
    .line 2255130
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2255131
    iget-object v2, p1, LX/FXj;->a:LX/FXV;

    move-object v2, v2

    .line 2255132
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e(LX/FXj;)V
    .locals 3

    .prologue
    .line 2255127
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2255128
    iget-object v2, p1, LX/FXj;->a:LX/FXV;

    move-object v2, v2

    .line 2255129
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2255116
    :try_start_0
    const-class v0, LX/FXV;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2255117
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v4

    invoke-static {v4}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2255118
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 2255119
    if-ne v4, p0, :cond_0

    .line 2255120
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2255121
    :goto_1
    move-object v0, v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2255122
    return-object v0

    .line 2255123
    :catch_0
    move-exception v0

    .line 2255124
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2255125
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2255126
    :cond_1
    const-string v0, "<unknown>"

    goto :goto_1
.end method
