.class public LX/FwQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/BQB;

.field private b:LX/Fsr;


# direct methods
.method public constructor <init>(LX/BQB;LX/Fsr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303100
    iput-object p1, p0, LX/FwQ;->a:LX/BQB;

    .line 2303101
    iput-object p2, p0, LX/FwQ;->b:LX/Fsr;

    .line 2303102
    return-void
.end method

.method public static a(LX/0QB;)LX/FwQ;
    .locals 1

    .prologue
    .line 2303103
    invoke-static {p0}, LX/FwQ;->b(LX/0QB;)LX/FwQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/FwQ;I)V
    .locals 2

    .prologue
    .line 2303104
    iget-object v0, p0, LX/FwQ;->a:LX/BQB;

    invoke-static {p1}, LX/FwQ;->c(I)LX/BPA;

    move-result-object v1

    .line 2303105
    sget-object p0, LX/BPA;->PHOTO_LOW_RES:LX/BPA;

    if-eq v1, p0, :cond_0

    sget-object p0, LX/BPA;->PHOTO_MINI_PREVIEW:LX/BPA;

    if-ne v1, p0, :cond_2

    :cond_0
    iget-object p0, v0, LX/BQB;->f:LX/BPC;

    .line 2303106
    iget-object p1, p0, LX/BPC;->e:LX/BPA;

    move-object p0, p1

    .line 2303107
    sget-object p1, LX/BPA;->PHOTO_NOT_LOADED:LX/BPA;

    if-ne p0, p1, :cond_2

    .line 2303108
    iget-object p0, v0, LX/BQB;->c:LX/BQD;

    const-string p1, "TimelineLoadCoverPhotoLowRes"

    invoke-virtual {p0, p1}, LX/BQD;->a(Ljava/lang/String;)V

    .line 2303109
    :cond_1
    :goto_0
    return-void

    .line 2303110
    :cond_2
    iget-object p0, v0, LX/BQB;->f:LX/BPC;

    .line 2303111
    iget-object p1, p0, LX/BPC;->e:LX/BPA;

    move-object p0, p1

    .line 2303112
    sget-object p1, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-eq p0, p1, :cond_1

    .line 2303113
    iget-object p0, v0, LX/BQB;->c:LX/BQD;

    const-string p1, "TimelineLoadCoverPhoto"

    invoke-virtual {p0, p1}, LX/BQD;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/FwQ;LX/BPA;)V
    .locals 1

    .prologue
    .line 2303114
    iget-object v0, p0, LX/FwQ;->b:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->A()LX/FvM;

    move-result-object v0

    invoke-interface {v0, p1}, LX/FvM;->a(LX/BPA;)V

    .line 2303115
    return-void
.end method

.method public static b(LX/0QB;)LX/FwQ;
    .locals 3

    .prologue
    .line 2303116
    new-instance v2, LX/FwQ;

    invoke-static {p0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v0

    check-cast v0, LX/BQB;

    invoke-static {p0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v1

    check-cast v1, LX/Fsr;

    invoke-direct {v2, v0, v1}, LX/FwQ;-><init>(LX/BQB;LX/Fsr;)V

    .line 2303117
    return-object v2
.end method

.method public static b(LX/FwQ;I)V
    .locals 1

    .prologue
    .line 2303118
    invoke-static {p1}, LX/FwQ;->c(I)LX/BPA;

    move-result-object v0

    invoke-static {p0, v0}, LX/FwQ;->a(LX/FwQ;LX/BPA;)V

    .line 2303119
    return-void
.end method

.method private static c(I)LX/BPA;
    .locals 3

    .prologue
    .line 2303120
    packed-switch p0, :pswitch_data_0

    .line 2303121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected resolution: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2303122
    :pswitch_0
    sget-object v0, LX/BPA;->PHOTO_MINI_PREVIEW:LX/BPA;

    .line 2303123
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static d(LX/FwQ;I)V
    .locals 1

    .prologue
    .line 2303124
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    sget-object v0, LX/BPA;->PHOTO_HIGH_RES_FAILED:LX/BPA;

    .line 2303125
    :goto_0
    invoke-static {p0, v0}, LX/FwQ;->a(LX/FwQ;LX/BPA;)V

    .line 2303126
    return-void

    .line 2303127
    :cond_0
    sget-object v0, LX/BPA;->PHOTO_MINI_PREVIEW_FAILED:LX/BPA;

    goto :goto_0
.end method
