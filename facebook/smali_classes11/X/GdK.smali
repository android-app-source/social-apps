.class public final enum LX/GdK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GdK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GdK;

.field public static final enum BOTTOM:LX/GdK;

.field public static final enum CENTER:LX/GdK;

.field public static final enum FIT:LX/GdK;

.field public static final enum TOP:LX/GdK;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2373583
    new-instance v0, LX/GdK;

    const-string v1, "FIT"

    invoke-direct {v0, v1, v2}, LX/GdK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GdK;->FIT:LX/GdK;

    .line 2373584
    new-instance v0, LX/GdK;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, LX/GdK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GdK;->TOP:LX/GdK;

    .line 2373585
    new-instance v0, LX/GdK;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v4}, LX/GdK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GdK;->CENTER:LX/GdK;

    .line 2373586
    new-instance v0, LX/GdK;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v5}, LX/GdK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GdK;->BOTTOM:LX/GdK;

    .line 2373587
    const/4 v0, 0x4

    new-array v0, v0, [LX/GdK;

    sget-object v1, LX/GdK;->FIT:LX/GdK;

    aput-object v1, v0, v2

    sget-object v1, LX/GdK;->TOP:LX/GdK;

    aput-object v1, v0, v3

    sget-object v1, LX/GdK;->CENTER:LX/GdK;

    aput-object v1, v0, v4

    sget-object v1, LX/GdK;->BOTTOM:LX/GdK;

    aput-object v1, v0, v5

    sput-object v0, LX/GdK;->$VALUES:[LX/GdK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2373588
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GdK;
    .locals 1

    .prologue
    .line 2373589
    const-class v0, LX/GdK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GdK;

    return-object v0
.end method

.method public static values()[LX/GdK;
    .locals 1

    .prologue
    .line 2373590
    sget-object v0, LX/GdK;->$VALUES:[LX/GdK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GdK;

    return-object v0
.end method
