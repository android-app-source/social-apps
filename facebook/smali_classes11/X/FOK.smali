.class public LX/FOK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/FOL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/FOE;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2235021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235022
    return-void
.end method

.method private static a(LX/FOK;Landroid/content/Context;Landroid/graphics/drawable/Drawable$Callback;IILX/0Px;Z)LX/FOI;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/graphics/drawable/Drawable$Callback;",
            "II",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;Z)",
            "LX/FOI;"
        }
    .end annotation

    .prologue
    .line 2235017
    iget-object v0, p0, LX/FOK;->a:LX/FOL;

    const/4 v4, 0x3

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v5, p6

    invoke-virtual/range {v0 .. v5}, LX/FOL;->a(Landroid/content/Context;IIIZ)LX/FOI;

    move-result-object v0

    .line 2235018
    invoke-virtual {v0, p2}, LX/FOI;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2235019
    invoke-virtual {v0, p5}, LX/FOI;->a(LX/0Px;)V

    .line 2235020
    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/graphics/drawable/Drawable$Callback;IILX/0Px;)LX/FOI;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/graphics/drawable/Drawable$Callback;",
            "II",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "LX/FOI;"
        }
    .end annotation

    .prologue
    .line 2235016
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, LX/FOK;->a(LX/FOK;Landroid/content/Context;Landroid/graphics/drawable/Drawable$Callback;IILX/0Px;Z)LX/FOI;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/FOK;
    .locals 3

    .prologue
    .line 2235011
    new-instance v2, LX/FOK;

    invoke-direct {v2}, LX/FOK;-><init>()V

    .line 2235012
    const-class v0, LX/FOL;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/FOL;

    const-class v1, LX/FOE;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/FOE;

    .line 2235013
    iput-object v0, v2, LX/FOK;->a:LX/FOL;

    iput-object v1, v2, LX/FOK;->b:LX/FOE;

    .line 2235014
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/graphics/drawable/Drawable$Callback;LX/0Px;)LX/FOI;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/graphics/drawable/Drawable$Callback;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "LX/FOI;"
        }
    .end annotation

    .prologue
    .line 2235015
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b125b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b125c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/FOK;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable$Callback;IILX/0Px;)LX/FOI;

    move-result-object v0

    return-object v0
.end method
