.class public final LX/FE1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DgO;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V
    .locals 0

    .prologue
    .line 2215964
    iput-object p1, p0, LX/FE1;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 1

    .prologue
    .line 2215967
    iget-object v0, p0, LX/FE1;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    invoke-static {v0, p1}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->a$redex0(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Lcom/facebook/android/maps/model/LatLng;)V

    .line 2215968
    return-void
.end method

.method public final a(Lcom/facebook/messaging/location/sending/NearbyPlace;)V
    .locals 2

    .prologue
    .line 2215969
    iget-object v0, p0, LX/FE1;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    .line 2215970
    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2215971
    invoke-static {v1}, Lcom/facebook/messaging/event/sending/EventMessageParams;->h(Lcom/facebook/messaging/event/sending/EventMessageParams;)V

    .line 2215972
    iput-object p1, v1, Lcom/facebook/messaging/event/sending/EventMessageParams;->f:Lcom/facebook/messaging/location/sending/NearbyPlace;

    .line 2215973
    sget-object p0, LX/Dgb;->NEARBY_PLACE:LX/Dgb;

    iput-object p0, v1, Lcom/facebook/messaging/event/sending/EventMessageParams;->g:LX/Dgb;

    .line 2215974
    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c:Landroid/widget/TextView;

    iget-object p0, p1, Lcom/facebook/messaging/location/sending/NearbyPlace;->b:Ljava/lang/String;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2215975
    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    if-eqz v1, :cond_0

    .line 2215976
    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    iget-object p0, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    invoke-virtual {v1, p0}, LX/FEF;->a(Lcom/facebook/messaging/event/sending/EventMessageParams;)V

    .line 2215977
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 1

    .prologue
    .line 2215965
    iget-object v0, p0, LX/FE1;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    invoke-static {v0, p1}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->a$redex0(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Lcom/facebook/android/maps/model/LatLng;)V

    .line 2215966
    return-void
.end method
