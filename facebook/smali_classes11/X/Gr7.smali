.class public LX/Gr7;
.super LX/62T;
.source ""

# interfaces
.implements LX/CqN;


# static fields
.field public static final u:Landroid/util/SparseIntArray;


# instance fields
.field public t:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "LX/Gr6;",
            ">;"
        }
    .end annotation
.end field

.field public v:Landroid/support/v7/widget/RecyclerView;

.field public w:LX/1Od;

.field public x:Z

.field public y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2397486
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, LX/Gr7;->u:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1

    .prologue
    .line 2397480
    invoke-direct {p0, p1, p3}, LX/62T;-><init>(Landroid/content/Context;I)V

    .line 2397481
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/Gr7;->t:LX/0YU;

    .line 2397482
    iput-object p2, p0, LX/Gr7;->v:Landroid/support/v7/widget/RecyclerView;

    .line 2397483
    iget-object v0, p0, LX/Gr7;->v:Landroid/support/v7/widget/RecyclerView;

    new-instance p1, LX/Gr5;

    invoke-direct {p1, p0}, LX/Gr5;-><init>(LX/Gr7;)V

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->setViewCacheExtension(LX/1PB;)V

    .line 2397484
    sget-object v0, LX/Gr7;->u:Landroid/util/SparseIntArray;

    const/16 p1, 0x73

    const/16 p3, 0x64

    invoke-virtual {v0, p1, p3}, Landroid/util/SparseIntArray;->append(II)V

    .line 2397485
    return-void
.end method

.method public static synthetic a(LX/Gr7;Landroid/view/View;LX/1Od;)V
    .locals 0

    .prologue
    .line 2397479
    invoke-super {p0, p1, p2}, LX/1OR;->a(Landroid/view/View;LX/1Od;)V

    return-void
.end method

.method private c(Landroid/view/View;I)V
    .locals 5

    .prologue
    .line 2397461
    iget-object v0, p0, LX/Gr7;->v:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v1

    .line 2397462
    invoke-virtual {p0, p1}, LX/1OR;->e(Landroid/view/View;)V

    .line 2397463
    iget v0, v1, LX/1a1;->e:I

    move v2, v0

    .line 2397464
    iget-object v0, p0, LX/Gr7;->t:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gr6;

    .line 2397465
    if-nez v0, :cond_0

    .line 2397466
    new-instance v0, LX/Gr6;

    sget-object v3, LX/Gr7;->u:Landroid/util/SparseIntArray;

    const/4 v4, 0x3

    invoke-virtual {v3, v2, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    invoke-direct {v0, p0, v3}, LX/Gr6;-><init>(LX/Gr7;I)V

    .line 2397467
    iget-object v3, p0, LX/Gr7;->t:LX/0YU;

    invoke-virtual {v3, v2, v0}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 2397468
    :cond_0
    iget-object v2, v0, LX/Gr6;->d:LX/Gr7;

    iget-object v3, v1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v2, v3}, LX/Gr7;->n(LX/Gr7;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2397469
    invoke-static {v0, p2}, LX/Gr6;->d(LX/Gr6;I)I

    move-result v2

    .line 2397470
    if-eq v2, p2, :cond_2

    .line 2397471
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 2397472
    iget-object v3, v0, LX/Gr6;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1a1;

    .line 2397473
    if-eqz v3, :cond_1

    .line 2397474
    iget-object v4, v0, LX/Gr6;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v4, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2397475
    iget-object v4, v0, LX/Gr6;->d:LX/Gr7;

    iget-object v3, v3, LX/1a1;->a:Landroid/view/View;

    iget-object p0, v0, LX/Gr6;->d:LX/Gr7;

    iget-object p0, p0, LX/Gr7;->w:LX/1Od;

    invoke-static {v4, v3, p0}, LX/Gr7;->a(LX/Gr7;Landroid/view/View;LX/1Od;)V

    .line 2397476
    :cond_1
    iget-object v2, v0, LX/Gr6;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2397477
    :cond_2
    :goto_0
    return-void

    .line 2397478
    :cond_3
    iget-object v2, v0, LX/Gr6;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private m(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 2397454
    iget-object v0, p0, LX/Gr7;->v:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 2397455
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Cs4;

    if-eqz v1, :cond_0

    .line 2397456
    check-cast v0, LX/Cs4;

    .line 2397457
    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 2397458
    instance-of v1, v0, LX/Cnx;

    if-eqz v1, :cond_0

    .line 2397459
    check-cast v0, LX/Cnx;

    invoke-interface {v0}, LX/Cnx;->c()Z

    move-result v0

    .line 2397460
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(LX/Gr7;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 2397447
    iget-object v0, p0, LX/Gr7;->v:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 2397448
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Cs4;

    if-eqz v1, :cond_0

    .line 2397449
    check-cast v0, LX/Cs4;

    .line 2397450
    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 2397451
    instance-of v1, v0, LX/Cny;

    if-eqz v1, :cond_0

    .line 2397452
    check-cast v0, LX/Cny;

    invoke-interface {v0}, LX/Cny;->b()Z

    move-result v0

    .line 2397453
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILX/1Od;)V
    .locals 1

    .prologue
    .line 2397393
    invoke-virtual {p0, p1}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/1OR;->a(Landroid/view/View;LX/1Od;)V

    .line 2397394
    return-void
.end method

.method public final a(LX/1Od;)V
    .locals 3

    .prologue
    .line 2397440
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/1OR;->v()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2397441
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object v1

    .line 2397442
    invoke-direct {p0, v1}, LX/Gr7;->m(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2397443
    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v2

    invoke-direct {p0, v1, v2}, LX/Gr7;->c(Landroid/view/View;I)V

    .line 2397444
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2397445
    :cond_1
    invoke-super {p0, p1}, LX/62T;->a(LX/1Od;)V

    .line 2397446
    return-void
.end method

.method public final a(LX/1Od;LX/1Ok;II)V
    .locals 0

    .prologue
    .line 2397437
    invoke-super {p0, p1, p2, p3, p4}, LX/62T;->a(LX/1Od;LX/1Ok;II)V

    .line 2397438
    iput-object p1, p0, LX/Gr7;->w:LX/1Od;

    .line 2397439
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V
    .locals 4

    .prologue
    .line 2397423
    invoke-super {p0, p1, p2}, LX/62T;->a(Landroid/support/v7/widget/RecyclerView;LX/1Od;)V

    .line 2397424
    const/4 v3, 0x0

    .line 2397425
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gr7;->x:Z

    move v2, v3

    .line 2397426
    :goto_0
    iget-object v0, p0, LX/Gr7;->t:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->a()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2397427
    iget-object v0, p0, LX/Gr7;->t:LX/0YU;

    iget-object v1, p0, LX/Gr7;->t:LX/0YU;

    invoke-virtual {v1, v2}, LX/0YU;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gr6;

    move p1, v3

    .line 2397428
    :goto_1
    iget-object v1, v0, LX/Gr6;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2397429
    iget-object v1, v0, LX/Gr6;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1a1;

    iget-object v1, v1, LX/1a1;->a:Landroid/view/View;

    invoke-super {p0, v1, p2}, LX/62T;->a(Landroid/view/View;LX/1Od;)V

    .line 2397430
    add-int/lit8 v1, p1, 0x1

    move p1, v1

    goto :goto_1

    .line 2397431
    :cond_0
    iget-object v0, v0, LX/Gr6;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2397432
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1a1;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-super {p0, v0, p2}, LX/62T;->a(Landroid/view/View;LX/1Od;)V

    goto :goto_2

    .line 2397433
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2397434
    :cond_2
    iget-object v0, p0, LX/Gr7;->t:LX/0YU;

    invoke-virtual {v0}, LX/0YU;->b()V

    .line 2397435
    iput-boolean v3, p0, LX/Gr7;->x:Z

    .line 2397436
    return-void
.end method

.method public final a(Landroid/view/View;LX/1Od;)V
    .locals 2

    .prologue
    .line 2397416
    invoke-direct {p0, p1}, LX/Gr7;->m(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2397417
    iget-object v0, p0, LX/Gr7;->v:Landroid/support/v7/widget/RecyclerView;

    .line 2397418
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2397419
    iget-object v1, p0, LX/Gr7;->v:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1OM;->d(LX/1a1;)V

    .line 2397420
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, p1, v0}, LX/Gr7;->c(Landroid/view/View;I)V

    .line 2397421
    :goto_0
    return-void

    .line 2397422
    :cond_0
    invoke-super {p0, p1, p2}, LX/62T;->a(Landroid/view/View;LX/1Od;)V

    goto :goto_0
.end method

.method public final c(LX/1Od;LX/1Ok;)V
    .locals 1

    .prologue
    .line 2397412
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gr7;->y:Z

    .line 2397413
    invoke-super {p0, p1, p2}, LX/62T;->c(LX/1Od;LX/1Ok;)V

    .line 2397414
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gr7;->y:Z

    .line 2397415
    return-void
.end method

.method public final c_(II)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2397395
    iget-object v0, p0, LX/Gr7;->w:LX/1Od;

    if-nez v0, :cond_0

    move v0, v1

    .line 2397396
    :goto_0
    return v0

    .line 2397397
    :cond_0
    iget-boolean v0, p0, LX/Gr7;->x:Z

    if-eqz v0, :cond_1

    move v0, v1

    .line 2397398
    goto :goto_0

    .line 2397399
    :cond_1
    if-ltz p1, :cond_2

    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v0

    if-lt p1, v0, :cond_3

    :cond_2
    move v0, v1

    .line 2397400
    goto :goto_0

    .line 2397401
    :cond_3
    iget-object v0, p0, LX/Gr7;->t:LX/0YU;

    invoke-virtual {v0, p2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gr6;

    .line 2397402
    if-eqz v0, :cond_4

    iget-object v2, v0, LX/Gr6;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 2397403
    goto :goto_0

    .line 2397404
    :cond_4
    if-eqz v0, :cond_5

    .line 2397405
    invoke-static {v0, p1}, LX/Gr6;->d(LX/Gr6;I)I

    move-result v2

    if-eq v2, p1, :cond_6

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 2397406
    if-nez v0, :cond_5

    move v0, v1

    .line 2397407
    goto :goto_0

    .line 2397408
    :cond_5
    iget-object v0, p0, LX/Gr7;->w:LX/1Od;

    invoke-virtual {v0, p1}, LX/1Od;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2397409
    invoke-virtual {p0, v0}, LX/1OR;->b(Landroid/view/View;)V

    .line 2397410
    invoke-direct {p0, v0, p1}, LX/Gr7;->c(Landroid/view/View;I)V

    .line 2397411
    const/4 v0, 0x1

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    goto :goto_1
.end method
