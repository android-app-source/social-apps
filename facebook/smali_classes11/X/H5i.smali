.class public LX/H5i;
.super LX/1Cd;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:I

.field private final c:LX/0SG;

.field private final d:LX/3Cl;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/H5j;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;Landroid/content/Context;LX/3Cl;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0SG;",
            "Landroid/content/Context;",
            "Lcom/facebook/notifications/widget/NotificationsRenderer;",
            "LX/0Ot",
            "<",
            "LX/1rn;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424841
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 2424842
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/H5i;->e:Ljava/util/Map;

    .line 2424843
    iput-object p1, p0, LX/H5i;->a:LX/0Zb;

    .line 2424844
    iput-object p2, p0, LX/H5i;->c:LX/0SG;

    .line 2424845
    iput-object p4, p0, LX/H5i;->d:LX/3Cl;

    .line 2424846
    iput-object p5, p0, LX/H5i;->f:LX/0Ot;

    .line 2424847
    iget-object v0, p0, LX/H5i;->d:LX/3Cl;

    invoke-virtual {v0, p3}, LX/3Cl;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, LX/H5i;->b:I

    .line 2424848
    return-void
.end method

.method public static a(LX/0QB;)LX/H5i;
    .locals 7

    .prologue
    .line 2424759
    new-instance v1, LX/H5i;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/3Cl;->a(LX/0QB;)LX/3Cl;

    move-result-object v5

    check-cast v5, LX/3Cl;

    const/16 v6, 0xe8b

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, LX/H5i;-><init>(LX/0Zb;LX/0SG;Landroid/content/Context;LX/3Cl;LX/0Ot;)V

    .line 2424760
    move-object v0, v1

    .line 2424761
    return-object v0
.end method

.method public static declared-synchronized b(LX/H5i;LX/2nq;)LX/H5j;
    .locals 3

    .prologue
    .line 2424831
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2424832
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_2

    .line 2424833
    :cond_0
    const/4 v0, 0x0

    .line 2424834
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2424835
    :cond_2
    :try_start_1
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 2424836
    iget-object v0, p0, LX/H5i;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5j;

    .line 2424837
    if-nez v0, :cond_1

    .line 2424838
    new-instance v0, LX/H5j;

    invoke-direct {v0}, LX/H5j;-><init>()V

    .line 2424839
    iget-object v2, p0, LX/H5i;->e:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2424840
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static c(Ljava/lang/Object;)LX/2nq;
    .locals 3
    .param p0    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2424817
    instance-of v1, p0, LX/1Rk;

    if-nez v1, :cond_1

    .line 2424818
    :cond_0
    :goto_0
    return-object v0

    .line 2424819
    :cond_1
    check-cast p0, LX/1Rk;

    .line 2424820
    iget-object v1, p0, LX/1Rk;->a:LX/1RA;

    move-object v1, v1

    .line 2424821
    if-eqz v1, :cond_0

    .line 2424822
    iget-object v1, p0, LX/1Rk;->a:LX/1RA;

    move-object v1, v1

    .line 2424823
    iget-object v2, v1, LX/1RA;->b:Ljava/lang/Object;

    move-object v1, v2

    .line 2424824
    if-eqz v1, :cond_0

    .line 2424825
    iget-object v1, p0, LX/1Rk;->a:LX/1RA;

    move-object v1, v1

    .line 2424826
    iget-object v2, v1, LX/1RA;->b:Ljava/lang/Object;

    move-object v1, v2

    .line 2424827
    instance-of v1, v1, LX/2nq;

    if-eqz v1, :cond_0

    .line 2424828
    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 2424829
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 2424830
    check-cast v0, LX/2nq;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0g8;Ljava/lang/Object;II)V
    .locals 3
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2424798
    invoke-interface {p1, p4}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    .line 2424799
    if-nez v0, :cond_1

    .line 2424800
    :cond_0
    :goto_0
    return-void

    .line 2424801
    :cond_1
    invoke-static {p2}, LX/H5i;->c(Ljava/lang/Object;)LX/2nq;

    move-result-object v1

    .line 2424802
    if-eqz v1, :cond_0

    .line 2424803
    invoke-static {p0, v1}, LX/H5i;->b(LX/H5i;LX/2nq;)LX/H5j;

    move-result-object v1

    .line 2424804
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 2424805
    iput v2, v1, LX/H5j;->c:I

    .line 2424806
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    const/4 v2, 0x0

    .line 2424807
    instance-of p0, p2, LX/1Rk;

    if-nez p0, :cond_3

    .line 2424808
    :cond_2
    :goto_1
    move v2, v2

    .line 2424809
    iget-object p0, v1, LX/H5j;->b:Landroid/util/SparseArray;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, v2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2424810
    add-int v0, p3, p4

    .line 2424811
    iput v0, v1, LX/H5j;->a:I

    .line 2424812
    goto :goto_0

    .line 2424813
    :cond_3
    check-cast p2, LX/1Rk;

    .line 2424814
    iget-object p0, p2, LX/1Rk;->a:LX/1RA;

    move-object p0, p0

    .line 2424815
    if-eqz p0, :cond_2

    .line 2424816
    iget v2, p2, LX/1Rk;->b:I

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2424770
    invoke-static {p1}, LX/H5i;->c(Ljava/lang/Object;)LX/2nq;

    move-result-object v0

    .line 2424771
    if-nez v0, :cond_1

    .line 2424772
    instance-of v0, p1, LX/1Rk;

    if-nez v0, :cond_2

    .line 2424773
    :cond_0
    :goto_0
    return-void

    .line 2424774
    :cond_1
    invoke-static {p0, v0}, LX/H5i;->b(LX/H5i;LX/2nq;)LX/H5j;

    move-result-object v1

    .line 2424775
    iget-object v2, p0, LX/H5i;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2424776
    iput-wide v2, v1, LX/H5j;->d:J

    .line 2424777
    iget-object v2, v1, LX/H5j;->f:LX/03R;

    move-object v2, v2

    .line 2424778
    sget-object v3, LX/03R;->UNSET:LX/03R;

    invoke-virtual {v2, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2424779
    invoke-interface {v0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2424780
    invoke-interface {v0}, LX/2nq;->k()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    .line 2424781
    iput-object v0, v1, LX/H5j;->f:LX/03R;

    .line 2424782
    goto :goto_0

    .line 2424783
    :cond_2
    check-cast p1, LX/1Rk;

    .line 2424784
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 2424785
    if-eqz v0, :cond_0

    .line 2424786
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 2424787
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 2424788
    if-eqz v0, :cond_0

    .line 2424789
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 2424790
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 2424791
    instance-of v0, v0, LX/3Ci;

    if-eqz v0, :cond_0

    .line 2424792
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 2424793
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 2424794
    check-cast v0, LX/3Ci;

    .line 2424795
    iget-object v1, v0, LX/3Ci;->a:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v0, v1

    .line 2424796
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    if-ne v0, v1, :cond_0

    .line 2424797
    iget-object v0, p0, LX/H5i;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rn;

    invoke-virtual {v0}, LX/1rn;->a()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2424762
    invoke-static {p1}, LX/H5i;->c(Ljava/lang/Object;)LX/2nq;

    move-result-object v0

    .line 2424763
    if-nez v0, :cond_0

    .line 2424764
    :goto_0
    return-void

    .line 2424765
    :cond_0
    invoke-static {p0, v0}, LX/H5i;->b(LX/H5i;LX/2nq;)LX/H5j;

    move-result-object v0

    .line 2424766
    iget-wide v6, v0, LX/H5j;->d:J

    move-wide v2, v6

    .line 2424767
    iget-object v1, p0, LX/H5i;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 2424768
    iget-wide v6, v0, LX/H5j;->e:J

    add-long/2addr v6, v2

    iput-wide v6, v0, LX/H5j;->e:J

    .line 2424769
    goto :goto_0
.end method
