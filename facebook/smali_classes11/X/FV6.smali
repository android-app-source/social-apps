.class public LX/FV6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/16H;

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/FV3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/16H;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/16H;",
            "Ljava/util/Set",
            "<",
            "LX/FV3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250349
    iput-object p1, p0, LX/FV6;->a:LX/16H;

    .line 2250350
    iput-object p2, p0, LX/FV6;->b:Ljava/util/Set;

    .line 2250351
    return-void
.end method

.method public static a(LX/0QB;)LX/FV6;
    .locals 5

    .prologue
    .line 2250352
    new-instance v1, LX/FV6;

    invoke-static {p0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v0

    check-cast v0, LX/16H;

    .line 2250353
    new-instance v2, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v3

    new-instance v4, LX/FVG;

    invoke-direct {v4, p0}, LX/FVG;-><init>(LX/0QB;)V

    invoke-direct {v2, v3, v4}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v2

    .line 2250354
    invoke-direct {v1, v0, v2}, LX/FV6;-><init>(LX/16H;Ljava/util/Set;)V

    .line 2250355
    move-object v0, v1

    .line 2250356
    return-object v0
.end method
