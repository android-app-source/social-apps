.class public final LX/FNN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Ut;


# direct methods
.method public constructor <init>(LX/2Ut;)V
    .locals 0

    .prologue
    .line 2232530
    iput-object p1, p0, LX/FNN;->a:LX/2Ut;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2232531
    const/4 v8, 0x0

    .line 2232532
    :try_start_0
    iget-object v0, p0, LX/FNN;->a:LX/2Ut;

    invoke-static {v0}, LX/2Ut;->k(LX/2Ut;)V

    .line 2232533
    iget-object v0, p0, LX/FNN;->a:LX/2Ut;

    iget-object v0, v0, LX/2Ut;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Kr;->X:LX/0Tn;

    iget-object v2, p0, LX/FNN;->a:LX/2Ut;

    iget-object v2, v2, LX/2Ut;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2232534
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2232535
    :goto_0
    return-object v0

    .line 2232536
    :catch_0
    move-exception v0

    .line 2232537
    iget-object v1, p0, LX/FNN;->a:LX/2Ut;

    iget-object v1, v1, LX/2Ut;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3Kr;->X:LX/0Tn;

    iget-object v3, p0, LX/FNN;->a:LX/2Ut;

    iget-object v3, v3, LX/2Ut;->f:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/32 v6, 0x2932e00

    add-long/2addr v4, v6

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2232538
    const-string v1, "LocalSpamRankingUpdateBackgroundTask"

    const-string v2, "Failed to update SMS contacts spam and ranking scores"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2232539
    new-instance v0, LX/2YS;

    invoke-direct {v0, v8}, LX/2YS;-><init>(Z)V

    goto :goto_0
.end method
