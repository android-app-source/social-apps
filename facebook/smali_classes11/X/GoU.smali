.class public final LX/GoU;
.super LX/Cle;
.source ""


# instance fields
.field private final c:Landroid/content/Context;

.field private d:LX/GoE;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/GoE;)V
    .locals 0

    .prologue
    .line 2394504
    invoke-direct {p0, p1}, LX/Cle;-><init>(Landroid/content/Context;)V

    .line 2394505
    iput-object p1, p0, LX/GoU;->c:Landroid/content/Context;

    .line 2394506
    iput-object p2, p0, LX/GoU;->d:LX/GoE;

    .line 2394507
    return-void
.end method


# virtual methods
.method public final a(LX/Clb;)LX/Cle;
    .locals 2

    .prologue
    .line 2394508
    if-nez p1, :cond_0

    .line 2394509
    :goto_0
    return-object p0

    .line 2394510
    :cond_0
    sget-object v0, LX/GoT;->a:[I

    invoke-virtual {p1}, LX/Clb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2394511
    :pswitch_0
    const v0, 0x7f0e0a10

    invoke-virtual {p0, v0}, LX/Cle;->a(I)LX/Cle;

    goto :goto_0

    .line 2394512
    :pswitch_1
    const v0, 0x7f0e0a12

    invoke-virtual {p0, v0}, LX/Cle;->a(I)LX/Cle;

    goto :goto_0

    .line 2394513
    :pswitch_2
    const v0, 0x7f0e0a13

    invoke-virtual {p0, v0}, LX/Cle;->a(I)LX/Cle;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;)LX/Cll;
    .locals 4

    .prologue
    .line 2394514
    sget-object v0, LX/GoT;->b:[I

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->b()Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2394515
    invoke-super {p0, p1}, LX/Cle;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;)LX/Cll;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2394516
    :pswitch_0
    new-instance v0, LX/GoS;

    invoke-virtual {p1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    move-result-object v1

    iget-object v2, p0, LX/GoU;->c:Landroid/content/Context;

    iget-object v3, p0, LX/GoU;->d:LX/GoE;

    invoke-direct {v0, v1, v2, v3}, LX/GoS;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;LX/GoE;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
