.class public LX/Fso;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/4a1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:LX/4a1;

.field public final f:Z

.field public final g:Z

.field public final h:I

.field public final i:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;LX/4a1;Ljava/lang/String;LX/4a1;ILjava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/4a1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2297563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297564
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "timelineFetch"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fso;->a:Ljava/lang/String;

    .line 2297565
    iput-object p1, p0, LX/Fso;->b:Ljava/lang/String;

    .line 2297566
    iput-object p2, p0, LX/Fso;->c:LX/4a1;

    .line 2297567
    iput-object p3, p0, LX/Fso;->d:Ljava/lang/String;

    .line 2297568
    iput-object p4, p0, LX/Fso;->e:LX/4a1;

    .line 2297569
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/Fso;->f:Z

    .line 2297570
    const-string v0, ""

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/Fso;->g:Z

    .line 2297571
    iput p5, p0, LX/Fso;->h:I

    .line 2297572
    iput-object p6, p0, LX/Fso;->i:Ljava/lang/String;

    .line 2297573
    return-void

    .line 2297574
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Ljava/lang/String;LX/4a1;Ljava/lang/String;LX/4a1;ILjava/lang/String;B)V
    .locals 0

    .prologue
    .line 2297575
    invoke-direct/range {p0 .. p6}, LX/Fso;-><init>(Ljava/lang/String;LX/4a1;Ljava/lang/String;LX/4a1;ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()LX/Fsn;
    .locals 1

    .prologue
    .line 2297576
    new-instance v0, LX/Fsn;

    invoke-direct {v0, p0}, LX/Fsn;-><init>(LX/Fso;)V

    return-object v0
.end method
