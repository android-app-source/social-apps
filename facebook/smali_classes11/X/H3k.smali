.class public final enum LX/H3k;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H3k;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H3k;

.field public static final enum INTENT_VIEW_FRAGMENT:LX/H3k;

.field public static final enum MAP_VIEW_FRAGMENT:LX/H3k;

.field public static final enum RESULT_LIST_FRAGMENT:LX/H3k;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2421538
    new-instance v0, LX/H3k;

    const-string v1, "INTENT_VIEW_FRAGMENT"

    invoke-direct {v0, v1, v2}, LX/H3k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    .line 2421539
    new-instance v0, LX/H3k;

    const-string v1, "RESULT_LIST_FRAGMENT"

    invoke-direct {v0, v1, v3}, LX/H3k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    .line 2421540
    new-instance v0, LX/H3k;

    const-string v1, "MAP_VIEW_FRAGMENT"

    invoke-direct {v0, v1, v4}, LX/H3k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H3k;->MAP_VIEW_FRAGMENT:LX/H3k;

    .line 2421541
    const/4 v0, 0x3

    new-array v0, v0, [LX/H3k;

    sget-object v1, LX/H3k;->INTENT_VIEW_FRAGMENT:LX/H3k;

    aput-object v1, v0, v2

    sget-object v1, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    aput-object v1, v0, v3

    sget-object v1, LX/H3k;->MAP_VIEW_FRAGMENT:LX/H3k;

    aput-object v1, v0, v4

    sput-object v0, LX/H3k;->$VALUES:[LX/H3k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2421542
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H3k;
    .locals 1

    .prologue
    .line 2421543
    const-class v0, LX/H3k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H3k;

    return-object v0
.end method

.method public static values()[LX/H3k;
    .locals 1

    .prologue
    .line 2421544
    sget-object v0, LX/H3k;->$VALUES:[LX/H3k;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H3k;

    return-object v0
.end method
