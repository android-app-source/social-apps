.class public final LX/Gn4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Gn3;


# instance fields
.field public final synthetic a:LX/Gn5;

.field private b:J

.field private c:F

.field private d:F

.field private e:I

.field private f:LX/0lF;


# direct methods
.method public constructor <init>(LX/Gn5;LX/0lF;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2393381
    iput-object p1, p0, LX/Gn4;->a:LX/Gn5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2393382
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Gn4;->b:J

    .line 2393383
    iput v2, p0, LX/Gn4;->c:F

    .line 2393384
    iput v2, p0, LX/Gn4;->d:F

    .line 2393385
    iput-object p2, p0, LX/Gn4;->f:LX/0lF;

    .line 2393386
    iget-object v0, p1, LX/Gn5;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Gn4;->e:I

    .line 2393387
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2393340
    iget-object v0, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v0, v0, LX/Gn5;->f:LX/GpO;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gn4;->a:LX/Gn5;

    const/4 v1, 0x0

    .line 2393341
    iget-object v2, v0, LX/Gn5;->e:LX/1P1;

    if-nez v2, :cond_6

    .line 2393342
    :cond_0
    :goto_0
    move v0, v1

    .line 2393343
    if-nez v0, :cond_2

    .line 2393344
    :cond_1
    :goto_1
    return v6

    .line 2393345
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 2393346
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/Gn4;->c:F

    .line 2393347
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/Gn4;->d:F

    goto :goto_1

    .line 2393348
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, LX/Gn4;->d:F

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 2393349
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, LX/Gn4;->c:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 2393350
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, LX/Gn4;->d:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 2393351
    iget v2, p0, LX/Gn4;->e:I

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-ltz v2, :cond_1

    iget v2, p0, LX/Gn4;->e:I

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_3

    iget v2, p0, LX/Gn4;->e:I

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-ltz v2, :cond_1

    :cond_3
    iget-object v2, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v2, v2, LX/Gn5;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/Gn4;->b:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 2393352
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 2393353
    iget-object v0, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v0, v0, LX/Gn5;->f:LX/GpO;

    .line 2393354
    iget-object v1, v0, LX/GpO;->b:LX/CHX;

    if-nez v1, :cond_7

    const/4 v1, 0x0

    :goto_2
    move-object v1, v1

    .line 2393355
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2393356
    iget-object v0, p0, LX/Gn4;->f:LX/0lF;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 2393357
    :goto_3
    const/4 p1, 0x1

    .line 2393358
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2393359
    const-string v3, "BrowserLiteIntent.EXTRA_ANIMATION"

    const/4 v4, 0x4

    new-array v4, v4, [I

    const/4 v5, 0x0

    const v7, 0x7f0400a0

    aput v7, v4, v5

    const v5, 0x7f04001c

    aput v5, v4, p1

    const/4 v5, 0x2

    const v7, 0x7f04001b

    aput v7, v4, v5

    const/4 v5, 0x3

    const v7, 0x7f04009f

    aput v7, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 2393360
    const-string v3, "extra_enable_swipe_down_to_dismiss"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2393361
    const-string v3, "tracking_codes"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2393362
    const-string v3, "iab_click_source"

    const-string v4, "canvas_swipe_to_open_ads"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2393363
    const-string v3, "force_in_app_browser"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2393364
    move-object v2, v2

    .line 2393365
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2393366
    const-string v0, "swipe_to_open"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393367
    iget-object v0, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v0, v0, LX/Gn5;->c:LX/GnF;

    iget-object v1, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v1, v1, LX/Gn5;->a:Landroid/content/Context;

    iget-object v3, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v3, v3, LX/Gn5;->f:LX/GpO;

    invoke-interface {v3}, LX/GoL;->getAction()LX/CHX;

    move-result-object v3

    iget-object v4, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v4, v4, LX/Gn5;->f:LX/GpO;

    invoke-interface {v4}, LX/GoY;->D()LX/GoE;

    move-result-object v4

    .line 2393368
    iget-object v7, v0, LX/GnF;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v7, v2, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2393369
    invoke-interface {v3}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v7

    invoke-interface {v3}, LX/CHW;->e()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, v4, v7, p1, v5}, LX/GnF;->a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V

    .line 2393370
    invoke-static {v0}, LX/GnF;->a(LX/GnF;)V

    .line 2393371
    iget-object v7, v0, LX/GnF;->f:LX/Go2;

    const/4 p1, 0x1

    .line 2393372
    iput-boolean p1, v7, LX/Go2;->h:Z

    .line 2393373
    iget-object v0, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v0, v0, LX/Gn5;->d:Landroid/app/Activity;

    if-eqz v0, :cond_4

    .line 2393374
    iget-object v0, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v0, v0, LX/Gn5;->d:Landroid/app/Activity;

    const v1, 0x7f0400a0

    const v2, 0x7f04001c

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2393375
    :cond_4
    iget-object v0, p0, LX/Gn4;->a:LX/Gn5;

    iget-object v0, v0, LX/Gn5;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/Gn4;->b:J

    goto/16 :goto_1

    .line 2393376
    :cond_5
    iget-object v0, p0, LX/Gn4;->f:LX/0lF;

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2393377
    :cond_6
    iget-object v2, v0, LX/Gn5;->e:LX/1P1;

    invoke-virtual {v2}, LX/1P1;->n()I

    move-result v2

    .line 2393378
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    add-int/lit8 v3, v2, 0x1

    iget-object v4, v0, LX/Gn5;->e:LX/1P1;

    invoke-virtual {v4}, LX/1OR;->D()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 2393379
    iget-object v3, v0, LX/Gn5;->e:LX/1P1;

    invoke-virtual {v3, v2}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    .line 2393380
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    iget-object v3, v0, LX/Gn5;->e:LX/1P1;

    invoke-virtual {v3}, LX/1OR;->x()I

    move-result v3

    sub-int/2addr v2, v3

    if-gtz v2, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_7
    iget-object v1, v0, LX/GpO;->b:LX/CHX;

    invoke-interface {v1}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
