.class public final LX/GPI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/GP6;


# direct methods
.method public constructor <init>(LX/GP6;)V
    .locals 0

    .prologue
    .line 2348569
    iput-object p1, p0, LX/GPI;->a:LX/GP6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 6

    .prologue
    .line 2348570
    iget-object v0, p0, LX/GPI;->a:LX/GP6;

    invoke-virtual {v0}, LX/GP6;->e()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    .line 2348571
    if-eqz p2, :cond_1

    .line 2348572
    iget-object v0, p0, LX/GPI;->a:LX/GP6;

    iget-object v2, v0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-ne v1, v0, :cond_0

    const v0, 0x7f0827b2

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbEditText;->setHint(I)V

    .line 2348573
    iget-object v0, p0, LX/GPI;->a:LX/GP6;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/GP4;->a(Z)V

    .line 2348574
    iget-object v0, p0, LX/GPI;->a:LX/GP6;

    invoke-static {v1}, LX/GQC;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)I

    move-result v2

    .line 2348575
    invoke-virtual {v0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/text/InputFilter;

    const/4 v5, 0x0

    new-instance p1, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {p1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object p1, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 2348576
    :goto_1
    iget-object v0, p0, LX/GPI;->a:LX/GP6;

    invoke-virtual {v0, v1, p2}, LX/GP6;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Z)V

    .line 2348577
    return-void

    .line 2348578
    :cond_0
    const v0, 0x7f0827b3

    goto :goto_0

    .line 2348579
    :cond_1
    iget-object v0, p0, LX/GPI;->a:LX/GP6;

    iget-object v0, v0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    const v2, 0x7f0827b1

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setHint(I)V

    .line 2348580
    iget-object v0, p0, LX/GPI;->a:LX/GP6;

    invoke-virtual {v0}, LX/GP4;->i()Z

    goto :goto_1
.end method
