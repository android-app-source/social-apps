.class public final LX/GM3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GJI;


# direct methods
.method public constructor <init>(LX/GJI;)V
    .locals 0

    .prologue
    .line 2343918
    iput-object p1, p0, LX/GM3;->a:LX/GJI;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2343919
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x5a0008

    sget-object v2, LX/GE1;->FETCH_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v2}, LX/GE1;->hashCode()I

    move-result v2

    const/4 v3, 0x3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2343920
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    .line 2343921
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2343922
    if-nez v0, :cond_0

    .line 2343923
    :goto_0
    return-void

    .line 2343924
    :cond_0
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    iget-boolean v0, v0, LX/GJI;->r:Z

    if-eqz v0, :cond_1

    .line 2343925
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    .line 2343926
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343927
    new-instance v1, LX/GFO;

    invoke-direct {v1, p1}, LX/GFO;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2343928
    :goto_1
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GJI;->b(LX/GJI;Z)V

    goto :goto_0

    .line 2343929
    :cond_1
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    const/4 v1, 0x1

    .line 2343930
    iput-boolean v1, v0, LX/GJI;->s:Z

    .line 2343931
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    .line 2343932
    const/4 v1, 0x0

    :goto_2
    iget-object v2, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->getRadioButtonsSize()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 2343933
    iget-object v2, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->g(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2343934
    iget-object v2, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    iget-object v3, v0, LX/GJI;->b:Landroid/content/res/Resources;

    invoke-static {v0}, LX/GJI;->G(LX/GJI;)I

    move-result p1

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->a(Ljava/lang/String;I)V

    .line 2343935
    iget-object v2, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    iget-object v3, v0, LX/GJI;->b:Landroid/content/res/Resources;

    invoke-static {v0}, LX/GJI;->H(LX/GJI;)I

    move-result p1

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/GJD;->a(Ljava/lang/CharSequence;I)V

    .line 2343936
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2343937
    :cond_3
    goto :goto_1
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2343938
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    const v3, 0x5a0008

    .line 2343939
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    .line 2343940
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2343941
    if-nez v0, :cond_1

    .line 2343942
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v1, LX/GE1;->FETCH_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v1}, LX/GE1;->hashCode()I

    move-result v1

    const/4 v2, 0x4

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2343943
    :cond_0
    :goto_0
    return-void

    .line 2343944
    :cond_1
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v1, LX/GE1;->FETCH_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v1}, LX/GE1;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2343945
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GJI;->b(LX/GJI;Z)V

    .line 2343946
    if-eqz p1, :cond_0

    .line 2343947
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0, p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;)Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2343948
    iget-object v0, p0, LX/GM3;->a:LX/GJI;

    invoke-virtual {v0}, LX/GJI;->c()V

    goto :goto_0
.end method
