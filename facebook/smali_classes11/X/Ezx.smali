.class public final LX/Ezx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Ezx;


# instance fields
.field private final a:LX/Ezy;


# direct methods
.method public constructor <init>(LX/Ezy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2187736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187737
    iput-object p1, p0, LX/Ezx;->a:LX/Ezy;

    .line 2187738
    return-void
.end method

.method public static a(LX/0QB;)LX/Ezx;
    .locals 4

    .prologue
    .line 2187739
    sget-object v0, LX/Ezx;->b:LX/Ezx;

    if-nez v0, :cond_1

    .line 2187740
    const-class v1, LX/Ezx;

    monitor-enter v1

    .line 2187741
    :try_start_0
    sget-object v0, LX/Ezx;->b:LX/Ezx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2187742
    if-eqz v2, :cond_0

    .line 2187743
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2187744
    new-instance p0, LX/Ezx;

    invoke-static {v0}, LX/Ezy;->a(LX/0QB;)LX/Ezy;

    move-result-object v3

    check-cast v3, LX/Ezy;

    invoke-direct {p0, v3}, LX/Ezx;-><init>(LX/Ezy;)V

    .line 2187745
    move-object v0, p0

    .line 2187746
    sput-object v0, LX/Ezx;->b:LX/Ezx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2187747
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2187748
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2187749
    :cond_1
    sget-object v0, LX/Ezx;->b:LX/Ezx;

    return-object v0

    .line 2187750
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2187751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 2

    .prologue
    .line 2187752
    iget-object v0, p0, LX/Ezx;->a:LX/Ezy;

    iget-object v0, v0, LX/Ezy;->d:Ljava/util/concurrent/atomic/AtomicInteger;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 2187753
    return-void
.end method
