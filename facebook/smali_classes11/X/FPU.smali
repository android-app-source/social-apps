.class public final LX/FPU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;)V
    .locals 0

    .prologue
    .line 2237266
    iput-object p1, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2237267
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {v0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v2

    .line 2237268
    if-nez v2, :cond_0

    .line 2237269
    :goto_0
    return-void

    .line 2237270
    :cond_0
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sget-object v3, LX/FPZ;->CELL:LX/FPZ;

    move v1, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;ILcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;LX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2237271
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    iget-object v1, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->e:Ljava/lang/Class;

    invoke-virtual {v0, v2, v4, v1, v3}, LX/FPr;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 6

    .prologue
    .line 2237272
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {v0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v2

    .line 2237273
    if-nez v2, :cond_1

    .line 2237274
    :cond_0
    :goto_0
    return-void

    .line 2237275
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    .line 2237276
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sget-object v3, LX/FPZ;->PHOTO_IN_COLLECTION:LX/FPZ;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move v1, p1

    invoke-static/range {v0 .. v5}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;ILcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;LX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 2237277
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2237278
    if-ltz p2, :cond_2

    move v1, v2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2237279
    invoke-static {v0, p1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v1

    .line 2237280
    const-string v4, "Null place model cannot have hscroll photos"

    invoke-static {v1, v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237281
    invoke-virtual {v1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->l()LX/0Px;

    move-result-object v1

    .line 2237282
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge p2, v4, :cond_3

    :goto_2
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2237283
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2237284
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;

    .line 2237285
    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;->jL_()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237286
    invoke-virtual {v1}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$Photo320FragmentModel;->jL_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    :cond_2
    move v1, v3

    .line 2237287
    goto :goto_1

    :cond_3
    move v2, v3

    .line 2237288
    goto :goto_2

    .line 2237289
    :cond_4
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2237290
    iget-object v3, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    invoke-virtual {v2, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v2, v1, v4}, LX/FPr;->a(LX/0Px;Ljava/lang/String;Landroid/content/Context;)V

    .line 2237291
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    if-eqz v0, :cond_0

    .line 2237292
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2237293
    iget-object v1, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237294
    iget-object v0, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v1, v0

    .line 2237295
    invoke-virtual {v1}, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->e()V

    .line 2237296
    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V
    .locals 2

    .prologue
    .line 2237297
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->x:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2237298
    iget-object v0, p0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    const/4 v1, 0x0

    .line 2237299
    iput-object v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->y:Ljava/lang/String;

    .line 2237300
    return-void
.end method
