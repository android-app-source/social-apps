.class public final LX/FNV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel;",
        ">;",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FNW;


# direct methods
.method public constructor <init>(LX/FNW;)V
    .locals 0

    .prologue
    .line 2232651
    iput-object p1, p0, LX/FNV;->a:LX/FNW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2232652
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2232653
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2232654
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2232655
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2232656
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2232657
    check-cast v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchCrowdReportedNumbersModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$SmsSpamStatusFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2232658
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    new-instance v1, LX/FNU;

    invoke-direct {v1, p0}, LX/FNU;-><init>(LX/FNV;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 2232659
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2232660
    goto :goto_0
.end method
