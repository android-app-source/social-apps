.class public LX/Gry;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static g:LX/Gry;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field private f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2398978
    const-class v0, LX/Gry;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Gry;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2398926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2398927
    return-void
.end method

.method private static a(LX/Gry;)LX/Gry;
    .locals 2

    .prologue
    .line 2398928
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/Gry;->f:J

    .line 2398929
    sput-object p0, LX/Gry;->g:LX/Gry;

    .line 2398930
    return-object p0
.end method

.method public static a(Landroid/content/Context;)LX/Gry;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2398931
    sget-object v0, LX/Gry;->g:LX/Gry;

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-object v2, LX/Gry;->g:LX/Gry;

    iget-wide v2, v2, LX/Gry;->f:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2398932
    sget-object v0, LX/Gry;->g:LX/Gry;

    .line 2398933
    :cond_0
    :goto_0
    return-object v0

    .line 2398934
    :cond_1
    invoke-static {p0}, LX/Gry;->c(Landroid/content/Context;)LX/Gry;

    move-result-object v0

    .line 2398935
    if-nez v0, :cond_2

    .line 2398936
    invoke-static {p0}, LX/Gry;->d(Landroid/content/Context;)LX/Gry;

    move-result-object v0

    .line 2398937
    if-nez v0, :cond_2

    .line 2398938
    new-instance v0, LX/Gry;

    invoke-direct {v0}, LX/Gry;-><init>()V

    .line 2398939
    :cond_2
    move-object v7, v0

    .line 2398940
    const/4 v0, 0x3

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "aid"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "androidid"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "limit_tracking"

    aput-object v1, v2, v0

    .line 2398941
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.facebook.katana.provider.AttributionIdProvider"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2398942
    const-string v0, "content://com.facebook.katana.provider.AttributionIdProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2398943
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 2398944
    if-eqz v0, :cond_d

    .line 2398945
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2398946
    :goto_2
    move-object v0, v0

    .line 2398947
    if-eqz v0, :cond_3

    .line 2398948
    iput-object v0, v7, LX/Gry;->d:Ljava/lang/String;

    .line 2398949
    :cond_3
    if-nez v1, :cond_5

    .line 2398950
    invoke-static {v7}, LX/Gry;->a(LX/Gry;)LX/Gry;

    move-result-object v0

    goto :goto_0

    .line 2398951
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.facebook.wakizashi.provider.AttributionIdProvider"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 2398952
    const-string v0, "content://com.facebook.wakizashi.provider.AttributionIdProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    .line 2398953
    :cond_5
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2398954
    if-eqz v1, :cond_6

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2398955
    :cond_6
    invoke-static {v7}, LX/Gry;->a(LX/Gry;)LX/Gry;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2398956
    if-eqz v1, :cond_0

    .line 2398957
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 2398958
    :cond_7
    :try_start_2
    const-string v0, "aid"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2398959
    const-string v2, "androidid"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 2398960
    const-string v3, "limit_tracking"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 2398961
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, LX/Gry;->b:Ljava/lang/String;

    .line 2398962
    if-lez v2, :cond_8

    if-lez v3, :cond_8

    .line 2398963
    iget-object v0, v7, LX/Gry;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2398964
    if-nez v0, :cond_8

    .line 2398965
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, LX/Gry;->c:Ljava/lang/String;

    .line 2398966
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v7, LX/Gry;->e:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2398967
    :cond_8
    if-eqz v1, :cond_9

    .line 2398968
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2398969
    :cond_9
    invoke-static {v7}, LX/Gry;->a(LX/Gry;)LX/Gry;

    move-result-object v0

    goto/16 :goto_0

    .line 2398970
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 2398971
    :goto_3
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Caught unexpected exception in getAttributionId(): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2398972
    if-eqz v1, :cond_a

    .line 2398973
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_a
    move-object v0, v6

    goto/16 :goto_0

    .line 2398974
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v6, :cond_b

    .line 2398975
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v0

    .line 2398976
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_4

    .line 2398977
    :catch_1
    move-exception v0

    goto :goto_3

    :cond_c
    move-object v1, v6

    goto/16 :goto_1

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method public static c(Landroid/content/Context;)LX/Gry;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2398886
    :try_start_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v0, v2, :cond_0

    .line 2398887
    new-instance v0, LX/GAA;

    const-string v2, "getAndroidId cannot be called on the main thread."

    invoke-direct {v0, v2}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2398888
    :catch_0
    move-exception v0

    .line 2398889
    const-string v2, "android_id"

    invoke-static {v2, v0}, LX/Gsc;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v1

    .line 2398890
    :goto_0
    return-object v0

    .line 2398891
    :cond_0
    :try_start_1
    const-string v0, "com.google.android.gms.common.GooglePlayServicesUtil"

    const-string v2, "isGooglePlayServicesAvailable"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/Gsc;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2398892
    if-nez v0, :cond_1

    move-object v0, v1

    .line 2398893
    goto :goto_0

    .line 2398894
    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v2, v0, v3}, LX/Gsc;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2398895
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move-object v0, v1

    .line 2398896
    goto :goto_0

    .line 2398897
    :cond_3
    const-string v0, "com.google.android.gms.ads.identifier.AdvertisingIdClient"

    const-string v2, "getAdvertisingIdInfo"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/Gsc;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2398898
    if-nez v0, :cond_4

    move-object v0, v1

    .line 2398899
    goto :goto_0

    .line 2398900
    :cond_4
    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v2, v0, v3}, LX/Gsc;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 2398901
    if-nez v3, :cond_5

    move-object v0, v1

    .line 2398902
    goto :goto_0

    .line 2398903
    :cond_5
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getId"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-static {v0, v2, v4}, LX/Gsc;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 2398904
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v4, "isLimitAdTrackingEnabled"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-static {v2, v4, v5}, LX/Gsc;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 2398905
    if-eqz v0, :cond_6

    if-nez v4, :cond_7

    :cond_6
    move-object v0, v1

    .line 2398906
    goto :goto_0

    .line 2398907
    :cond_7
    new-instance v2, LX/Gry;

    invoke-direct {v2}, LX/Gry;-><init>()V

    .line 2398908
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v0, v5}, LX/Gsc;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, LX/Gry;->c:Ljava/lang/String;

    .line 2398909
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3, v4, v0}, LX/Gsc;->a(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v2, LX/Gry;->e:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v2

    .line 2398910
    goto/16 :goto_0
.end method

.method public static d(Landroid/content/Context;)LX/Gry;
    .locals 4

    .prologue
    .line 2398911
    new-instance v1, LX/Grx;

    invoke-direct {v1}, LX/Grx;-><init>()V

    .line 2398912
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.ads.identifier.service.START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2398913
    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2398914
    const/4 v2, 0x1

    const v3, 0x66dc4455

    invoke-static {p0, v0, v1, v2, v3}, LX/04O;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2398915
    :try_start_0
    new-instance v2, LX/Grw;

    invoke-virtual {v1}, LX/Grx;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {v2, v0}, LX/Grw;-><init>(Landroid/os/IBinder;)V

    .line 2398916
    new-instance v0, LX/Gry;

    invoke-direct {v0}, LX/Gry;-><init>()V

    .line 2398917
    invoke-virtual {v2}, LX/Grw;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, LX/Gry;->c:Ljava/lang/String;

    .line 2398918
    invoke-virtual {v2}, LX/Grw;->b()Z

    move-result v2

    iput-boolean v2, v0, LX/Gry;->e:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2398919
    const v2, -0x7426a7c3

    invoke-static {p0, v1, v2}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 2398920
    :goto_0
    return-object v0

    .line 2398921
    :catch_0
    move-exception v0

    .line 2398922
    :try_start_1
    const-string v2, "android_id"

    invoke-static {v2, v0}, LX/Gsc;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2398923
    const v0, -0x396555c8

    invoke-static {p0, v1, v0}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    .line 2398924
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2398925
    :catchall_0
    move-exception v0

    const v2, 0x13d9a6d1

    invoke-static {p0, v1, v2}, LX/04O;->a(Landroid/content/Context;Landroid/content/ServiceConnection;I)V

    throw v0
.end method
