.class public LX/Fdl;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/search/results/protocol/filters/FilterValue;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field public b:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/5uu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2263995
    const v0, 0x7f0312a2

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 2263996
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Fdl;->c:Ljava/util/HashSet;

    .line 2263997
    iput-object p1, p0, LX/Fdl;->a:Landroid/content/Context;

    .line 2263998
    return-void
.end method

.method public static b(LX/Fdl;)Lcom/facebook/search/results/protocol/filters/FilterValue;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2263999
    iget-object v1, p0, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 2264000
    :cond_0
    :goto_0
    return-object v0

    .line 2264001
    :cond_1
    iget-object v1, p0, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2264002
    iget-object v1, p0, LX/Fdl;->b:LX/0ad;

    sget-short v2, LX/100;->I:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2264003
    if-nez v1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2264004
    iget-object v1, p0, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 2264005
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2264006
    iget-object v3, p0, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2264007
    invoke-virtual {p0, v0}, LX/Fdl;->getPosition(Ljava/lang/Object;)I

    move-result v3

    if-gez v3, :cond_1

    .line 2264008
    invoke-super {p0, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 2264009
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2264010
    :cond_2
    const v0, 0x7246f723

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2264011
    return-void
.end method

.method public final a(LX/5uu;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2264012
    iget-object v1, p0, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 2264013
    iput-object p1, p0, LX/Fdl;->d:LX/5uu;

    .line 2264014
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2264015
    iget-object v1, p0, LX/Fdl;->b:LX/0ad;

    sget-short v3, LX/100;->I:S

    invoke-interface {v1, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 2264016
    invoke-interface {p1}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2264017
    invoke-interface {p1}, LX/5uu;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez v3, :cond_1

    .line 2264018
    :cond_0
    invoke-static {}, Lcom/facebook/search/results/protocol/filters/FilterValue;->g()LX/5ur;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 2264019
    iput-object v7, v6, LX/5ur;->a:Ljava/lang/String;

    .line 2264020
    move-object v6, v6

    .line 2264021
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 2264022
    iput-object v7, v6, LX/5ur;->b:Ljava/lang/String;

    .line 2264023
    move-object v6, v6

    .line 2264024
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v7

    .line 2264025
    iput-boolean v7, v6, LX/5ur;->d:Z

    .line 2264026
    move-object v6, v6

    .line 2264027
    invoke-virtual {v6}, LX/5ur;->f()Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v6

    .line 2264028
    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2264029
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2264030
    iget-object v0, p0, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2264031
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2264032
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 2264033
    const v0, -0x408df990

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2264034
    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 2264035
    if-nez p2, :cond_0

    .line 2264036
    iget-object v0, p0, LX/Fdl;->a:Landroid/content/Context;

    const v1, 0x7f0312a2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2264037
    :cond_0
    invoke-virtual {p0, p1}, LX/Fdl;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2264038
    const v0, 0x7f0d2b77

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2264039
    iget-object v1, v4, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2264040
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2264041
    const v0, 0x7f0d2b93

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbCheckBox;

    .line 2264042
    const v0, 0x7f0d2b94

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/resources/ui/FbRadioButton;

    .line 2264043
    iget-object v0, p0, LX/Fdl;->b:LX/0ad;

    sget-short v1, LX/100;->I:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2264044
    if-eqz v2, :cond_3

    .line 2264045
    invoke-virtual {v3, v6}, Lcom/facebook/resources/ui/FbCheckBox;->setVisibility(I)V

    .line 2264046
    invoke-virtual {v5, v7}, Lcom/facebook/resources/ui/FbRadioButton;->setVisibility(I)V

    .line 2264047
    iget-object v0, p0, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2264048
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 2264049
    :cond_1
    :goto_0
    const v0, 0x7f0d2b75

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2264050
    new-instance v0, LX/Fdk;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/Fdk;-><init>(LX/Fdl;ZLcom/facebook/resources/ui/FbCheckBox;Lcom/facebook/search/results/protocol/filters/FilterValue;Lcom/facebook/resources/ui/FbRadioButton;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2264051
    return-object p2

    .line 2264052
    :cond_2
    invoke-virtual {v3, v6}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    goto :goto_0

    .line 2264053
    :cond_3
    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbCheckBox;->setVisibility(I)V

    .line 2264054
    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbRadioButton;->setVisibility(I)V

    .line 2264055
    invoke-static {p0}, LX/Fdl;->b(LX/Fdl;)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2264056
    invoke-static {p0}, LX/Fdl;->b(LX/Fdl;)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/facebook/search/results/protocol/filters/FilterValue;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v5, v0}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    goto :goto_0
.end method
