.class public final LX/GZd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:J

.field public final synthetic c:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

.field public final synthetic d:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

.field public final synthetic e:LX/15i;

.field public final synthetic f:I

.field public final synthetic g:Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;JJLcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/15i;I)V
    .locals 0

    .prologue
    .line 2366762
    iput-object p1, p0, LX/GZd;->g:Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    iput-wide p2, p0, LX/GZd;->a:J

    iput-wide p4, p0, LX/GZd;->b:J

    iput-object p6, p0, LX/GZd;->c:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    iput-object p7, p0, LX/GZd;->d:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    iput-object p8, p0, LX/GZd;->e:LX/15i;

    iput p9, p0, LX/GZd;->f:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2366750
    const-class v0, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    const-string v1, "Could not fetch collection"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2366751
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2366752
    check-cast p1, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;

    .line 2366753
    if-nez p1, :cond_1

    .line 2366754
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Null collection query response from server"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/GZd;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2366755
    :cond_0
    :goto_0
    return-void

    .line 2366756
    :cond_1
    iget-object v0, p0, LX/GZd;->g:Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    .line 2366757
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2366758
    if-eqz v0, :cond_0

    .line 2366759
    iget-object v1, p0, LX/GZd;->g:Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    iget-wide v2, p0, LX/GZd;->a:J

    iget-wide v4, p0, LX/GZd;->b:J

    iget-object v7, p0, LX/GZd;->c:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    iget-object v8, p0, LX/GZd;->d:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    iget-object v9, p0, LX/GZd;->e:LX/15i;

    iget v10, p0, LX/GZd;->f:I

    move-object v6, p1

    .line 2366760
    invoke-static/range {v1 .. v10}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a$redex0(Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;JJLcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/15i;I)V

    .line 2366761
    goto :goto_0
.end method
