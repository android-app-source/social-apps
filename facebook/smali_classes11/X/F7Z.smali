.class public final LX/F7Z;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

.field public final synthetic b:Landroid/widget/ProgressBar;

.field public final synthetic c:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;Lcom/facebook/growth/friendfinder/FriendFinderIntroView;Landroid/widget/ProgressBar;)V
    .locals 0

    .prologue
    .line 2202203
    iput-object p1, p0, LX/F7Z;->c:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iput-object p2, p0, LX/F7Z;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    iput-object p3, p0, LX/F7Z;->b:Landroid/widget/ProgressBar;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2202166
    iget-object v0, p0, LX/F7Z;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a()V

    .line 2202167
    iget-object v0, p0, LX/F7Z;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->setVisibility(I)V

    .line 2202168
    iget-object v0, p0, LX/F7Z;->b:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2202169
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2202170
    check-cast p1, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;

    .line 2202171
    iget-object v0, p0, LX/F7Z;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->setVisibility(I)V

    .line 2202172
    iget-object v0, p0, LX/F7Z;->b:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2202173
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;->a()Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2202174
    invoke-virtual {p1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;->a()Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel$ActorModel;->a()Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;

    move-result-object v0

    iget-object v1, p0, LX/F7Z;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    const/4 v11, 0x2

    const/4 v5, 0x0

    .line 2202175
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;->j()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v2, v11, :cond_2

    .line 2202176
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a()V

    .line 2202177
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/friends/protocol/FetchDailyDialogueContactImporterModels$DailyDialogueContactImporterQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2202178
    iget-object v2, p0, LX/F7Z;->c:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v3, p0, LX/F7Z;->a:Lcom/facebook/growth/friendfinder/FriendFinderIntroView;

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2202179
    if-nez v0, :cond_7

    move v4, v5

    :goto_1
    if-eqz v4, :cond_9

    move v4, v5

    :goto_2
    if-nez v4, :cond_1

    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v5, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    if-eqz v4, :cond_1

    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v6, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    if-nez v4, :cond_b

    .line 2202180
    :cond_1
    :goto_3
    return-void

    .line 2202181
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2202182
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2202183
    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;->j()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v4, v5

    :goto_4
    if-ge v4, v9, :cond_5

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;

    .line 2202184
    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;->b()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-nez v3, :cond_4

    const/4 v3, 0x1

    :goto_5
    if-nez v3, :cond_3

    .line 2202185
    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;->b()LX/1vs;

    move-result-object v3

    iget-object v10, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v10, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2202186
    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2202187
    :cond_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_4

    .line 2202188
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFieldsModel;->b()LX/1vs;

    move-result-object v3

    iget-object v10, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2202189
    invoke-virtual {v10, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    goto :goto_5

    .line 2202190
    :cond_5
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-lt v2, v11, :cond_6

    .line 2202191
    invoke-virtual {v1, v6}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->setFacepileFaces(Ljava/util/List;)V

    .line 2202192
    invoke-virtual {v0}, Lcom/facebook/friends/protocol/FriendsWhoUsedContactImporterGraphQLModels$FacepileFriendsConnectionModel;->a()I

    move-result v2

    invoke-virtual {v1, v7, v2}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a(Ljava/util/List;I)V

    goto/16 :goto_0

    .line 2202193
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a()V

    goto/16 :goto_0

    .line 2202194
    :cond_7
    invoke-virtual {v1, v0, v9}, LX/15i;->g(II)I

    move-result v4

    if-nez v4, :cond_8

    move v4, v5

    goto/16 :goto_1

    :cond_8
    move v4, v6

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v4

    if-nez v4, :cond_a

    move v4, v5

    goto/16 :goto_2

    :cond_a
    move v4, v6

    goto/16 :goto_2

    .line 2202195
    :cond_b
    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    .line 2202196
    iget-object v7, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v7, v4}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a(Landroid/app/Activity;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 2202197
    instance-of v7, v4, Lcom/facebook/events/invite/EventsExtendedInviteActivity;

    move v4, v7

    .line 2202198
    if-nez v4, :cond_c

    .line 2202199
    invoke-virtual {v1, v0, v9}, LX/15i;->g(II)I

    move-result v4

    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v7

    .line 2202200
    invoke-virtual {v1, v4, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v7, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2202201
    :cond_c
    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v5, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v7

    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v5, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v8

    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v5, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v4

    invoke-virtual {v3, v7, v8, v4}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->a(Ljava/lang/String;II)V

    .line 2202202
    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v6, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v6, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v7

    const-class v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v6, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v4

    invoke-virtual {v3, v5, v7, v4}, Lcom/facebook/growth/friendfinder/FriendFinderIntroView;->b(Ljava/lang/String;II)V

    goto/16 :goto_3
.end method
