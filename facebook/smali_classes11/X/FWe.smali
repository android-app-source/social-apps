.class public LX/FWe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/FWZ;


# direct methods
.method public constructor <init>(LX/1Ck;LX/FWZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252378
    iput-object p1, p0, LX/FWe;->a:LX/1Ck;

    .line 2252379
    iput-object p2, p0, LX/FWe;->b:LX/FWZ;

    .line 2252380
    return-void
.end method

.method public static a(LX/FWe;LX/FUw;)LX/0Vd;
    .locals 1
    .param p0    # LX/FWe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FUw;",
            ")",
            "LX/0Vd",
            "<",
            "LX/FVy;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2252376
    new-instance v0, LX/FWd;

    invoke-direct {v0, p0, p1}, LX/FWd;-><init>(LX/FWe;LX/FUw;)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/FWe;
    .locals 1

    .prologue
    .line 2252375
    invoke-static {p0}, LX/FWe;->b(LX/0QB;)LX/FWe;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/FWe;
    .locals 3

    .prologue
    .line 2252373
    new-instance v2, LX/FWe;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/FWZ;->b(LX/0QB;)LX/FWZ;

    move-result-object v1

    check-cast v1, LX/FWZ;

    invoke-direct {v2, v0, v1}, LX/FWe;-><init>(LX/1Ck;LX/FWZ;)V

    .line 2252374
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;LX/FUw;)V
    .locals 3
    .param p2    # LX/FUw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FVy;",
            ">;",
            "LX/FUw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2252371
    iget-object v0, p0, LX/FWe;->a:LX/1Ck;

    const-string v1, "task_key_fetch_cached_only_saved_items"

    invoke-static {p0, p2}, LX/FWe;->a(LX/FWe;LX/FUw;)LX/0Vd;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2252372
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2252369
    iget-object v0, p0, LX/FWe;->a:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2252370
    return-void
.end method

.method public final b(LX/0am;LX/FUw;)V
    .locals 2
    .param p2    # LX/FUw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            ">;",
            "LX/FUw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2252367
    iget-object v0, p0, LX/FWe;->b:LX/FWZ;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/FWZ;->a(LX/0am;LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/FWe;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/FUw;)V

    .line 2252368
    return-void
.end method

.method public final b(Lcom/google/common/util/concurrent/ListenableFuture;LX/FUw;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FVy;",
            ">;",
            "LX/FUw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2252365
    iget-object v0, p0, LX/FWe;->a:LX/1Ck;

    const-string v1, "task_key_fetch_fresh_saved_items"

    invoke-static {p0, p2}, LX/FWe;->a(LX/FWe;LX/FUw;)LX/0Vd;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2252366
    return-void
.end method
