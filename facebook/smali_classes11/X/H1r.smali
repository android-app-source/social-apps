.class public LX/H1r;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:Ljava/lang/String;

.field public final c:LX/H1q;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0lC;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0lC;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2415981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2415982
    iput-object p1, p0, LX/H1r;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2415983
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/H1r;->b:Ljava/lang/String;

    .line 2415984
    new-instance v0, LX/H1q;

    invoke-direct {v0, p2}, LX/H1q;-><init>(LX/0lC;)V

    iput-object v0, p0, LX/H1r;->c:LX/H1q;

    .line 2415985
    iget-object v0, p0, LX/H1r;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/H1r;->c(LX/H1r;)LX/0Tn;

    move-result-object p1

    const-string p2, ""

    invoke-interface {v0, p1, p2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2415986
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 2415987
    iget-object p1, p0, LX/H1r;->c:LX/H1q;

    invoke-virtual {p1, v0}, LX/H1q;->a(Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    .line 2415988
    :goto_0
    move-object v0, v0

    .line 2415989
    iput-object v0, p0, LX/H1r;->d:LX/0Px;

    .line 2415990
    return-void

    .line 2415991
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2415992
    goto :goto_0
.end method

.method public static c(LX/H1r;)LX/0Tn;
    .locals 3

    .prologue
    .line 2415980
    sget-object v0, LX/H2F;->a:LX/0Tn;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "category_history/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/H1r;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method
