.class public final LX/F0R;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2189555
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2189556
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189557
    :goto_0
    return v1

    .line 2189558
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189559
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2189560
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2189561
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2189562
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2189563
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2189564
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2189565
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2189566
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2189567
    const/4 v3, 0x0

    .line 2189568
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_a

    .line 2189569
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189570
    :goto_3
    move v2, v3

    .line 2189571
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2189572
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2189573
    goto :goto_1

    .line 2189574
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2189575
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2189576
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2189577
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2189578
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 2189579
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2189580
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2189581
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 2189582
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2189583
    :cond_7
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_4

    .line 2189584
    :cond_8
    const-string v6, "image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2189585
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_4

    .line 2189586
    :cond_9
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2189587
    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 2189588
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2189589
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_a
    move v2, v3

    move v4, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2189590
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2189591
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189592
    if-eqz v0, :cond_3

    .line 2189593
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189594
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2189595
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2189596
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p1, 0x0

    .line 2189597
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2189598
    invoke-virtual {p0, v2, p1}, LX/15i;->g(II)I

    move-result v3

    .line 2189599
    if-eqz v3, :cond_0

    .line 2189600
    const-string v3, "__type__"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189601
    invoke-static {p0, v2, p1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2189602
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2189603
    if-eqz v3, :cond_1

    .line 2189604
    const-string p1, "image"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189605
    invoke-static {p0, v3, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2189606
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2189607
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2189608
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2189609
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2189610
    return-void
.end method
