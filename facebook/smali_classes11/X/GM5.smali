.class public final LX/GM5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GJI;


# direct methods
.method public constructor <init>(LX/GJI;)V
    .locals 0

    .prologue
    .line 2343991
    iput-object p1, p0, LX/GM5;->a:LX/GJI;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2343992
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x5a0008

    sget-object v2, LX/GE1;->FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v2}, LX/GE1;->hashCode()I

    move-result v2

    const/4 v3, 0x3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2343993
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    .line 2343994
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2343995
    if-eqz v0, :cond_0

    .line 2343996
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GJI;->b(LX/GJI;Z)V

    .line 2343997
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    invoke-virtual {v0}, LX/GJH;->z()V

    .line 2343998
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-boolean v0, v0, LX/GJI;->r:Z

    if-nez v0, :cond_0

    .line 2343999
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    iget-object v1, v1, LX/GJI;->b:Landroid/content/res/Resources;

    iget-object v2, p0, LX/GM5;->a:LX/GJI;

    invoke-static {v2}, LX/GJI;->G(LX/GJI;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextSuffixTextView(Ljava/lang/CharSequence;)V

    .line 2344000
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    iget-object v1, v1, LX/GJI;->b:Landroid/content/res/Resources;

    iget-object v2, p0, LX/GM5;->a:LX/GJI;

    invoke-static {v2}, LX/GJI;->H(LX/GJI;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setContentDescriptionSuffixTextView(Ljava/lang/CharSequence;)V

    .line 2344001
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    const/4 v1, 0x1

    .line 2344002
    iput-boolean v1, v0, LX/GJI;->s:Z

    .line 2344003
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2344004
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    const v5, 0x5a0008

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2344005
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    .line 2344006
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2344007
    if-nez v0, :cond_1

    .line 2344008
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v1, LX/GE1;->FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v1}, LX/GE1;->hashCode()I

    move-result v1

    const/4 v2, 0x4

    invoke-interface {v0, v5, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2344009
    :cond_0
    :goto_0
    return-void

    .line 2344010
    :cond_1
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v1, LX/GE1;->FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v1}, LX/GE1;->hashCode()I

    move-result v1

    const/4 v2, 0x2

    invoke-interface {v0, v5, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2344011
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    invoke-static {v0, v4}, LX/GJI;->b(LX/GJI;Z)V

    .line 2344012
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    iget-object v1, v1, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTextEditText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GJH;->a(Ljava/lang/String;)LX/GMB;

    move-result-object v0

    .line 2344013
    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    invoke-virtual {v1, v0}, LX/GJH;->a(LX/GMB;)V

    .line 2344014
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->a()I

    move-result v1

    sget-object v2, LX/GE2;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 2344015
    sget-object v1, LX/GMB;->EMPTY_UNSELECTED:LX/GMB;

    if-ne v0, v1, :cond_2

    .line 2344016
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    iget-object v1, v1, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    goto :goto_0

    .line 2344017
    :cond_2
    sget-object v1, LX/GMB;->EMPTY_SELECTED:LX/GMB;

    if-ne v0, v1, :cond_3

    .line 2344018
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v3, v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2344019
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    iget-object v0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v3, v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2344020
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    .line 2344021
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2344022
    new-instance v1, LX/GFl;

    invoke-direct {v1, v3}, LX/GFl;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2344023
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    .line 2344024
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2344025
    sget-object v1, LX/GG8;->INVALID_BUDGET:LX/GG8;

    invoke-virtual {v0, v1, v4}, LX/GCE;->a(LX/GG8;Z)V

    goto :goto_0

    .line 2344026
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v0

    .line 2344027
    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    iget-object v1, v1, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2344028
    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    invoke-virtual {v1}, LX/GJI;->t()V

    .line 2344029
    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    invoke-static {v1, v0}, LX/GJI;->c(LX/GJI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)V

    .line 2344030
    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    iget-object v1, v1, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2344031
    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    iget-object v1, v1, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2344032
    iget-object v1, p0, LX/GM5;->a:LX/GJI;

    .line 2344033
    iget-object v2, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v2

    .line 2344034
    new-instance v2, LX/GFl;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-direct {v2, v0}, LX/GFl;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2344035
    iget-object v0, p0, LX/GM5;->a:LX/GJI;

    .line 2344036
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2344037
    sget-object v1, LX/GG8;->INVALID_BUDGET:LX/GG8;

    iget-object v2, p0, LX/GM5;->a:LX/GJI;

    invoke-virtual {v2}, LX/GJI;->s()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    goto/16 :goto_0
.end method
