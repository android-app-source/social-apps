.class public final LX/FlF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserDeletionModels$FundraiserDeleteMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FlI;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/FlI;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2279841
    iput-object p1, p0, LX/FlF;->a:LX/FlI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2279842
    iput-object p2, p0, LX/FlF;->b:Ljava/lang/String;

    .line 2279843
    return-void
.end method

.method private a()V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2279854
    iget-object v0, p0, LX/FlF;->a:LX/FlI;

    iget-object v0, v0, LX/FlI;->n:LX/FkM;

    invoke-virtual {v0}, LX/FkM;->a()V

    .line 2279855
    iget-object v0, p0, LX/FlF;->a:LX/FlI;

    iget-object v0, v0, LX/FlI;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2279856
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2279845
    iget-object v0, p0, LX/FlF;->a:LX/FlI;

    iget-object v0, v0, LX/FlI;->n:LX/FkM;

    invoke-virtual {v0}, LX/FkM;->a()V

    .line 2279846
    iget-object v0, p0, LX/FlF;->a:LX/FlI;

    iget-object v0, v0, LX/FlI;->c:Landroid/content/Context;

    const v1, 0x7f08332f

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2279847
    iget-object v0, p0, LX/FlF;->a:LX/FlI;

    iget-object v0, v0, LX/FlI;->b:LX/0Zb;

    iget-object v1, p0, LX/FlF;->b:Ljava/lang/String;

    .line 2279848
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "fundraiser_delete_mutation_failure"

    invoke-direct {v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "social_good"

    .line 2279849
    iput-object p0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2279850
    move-object v2, v2

    .line 2279851
    const-string p0, "fundraiser_campaign_id"

    invoke-virtual {v2, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object v1, v2

    .line 2279852
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2279853
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279844
    invoke-direct {p0}, LX/FlF;->a()V

    return-void
.end method
