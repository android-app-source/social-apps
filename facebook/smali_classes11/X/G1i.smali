.class public final LX/G1i;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2312744
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2312745
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2312746
    :goto_0
    return v1

    .line 2312747
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2312748
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2312749
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2312750
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2312751
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2312752
    const-string v6, "collage_layout"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2312753
    invoke-static {p0, p1}, LX/5vu;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2312754
    :cond_2
    const-string v6, "node"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2312755
    invoke-static {p0, p1}, LX/G1f;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2312756
    :cond_3
    const-string v6, "subtitle"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2312757
    invoke-static {p0, p1}, LX/G1g;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2312758
    :cond_4
    const-string v6, "title"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2312759
    invoke-static {p0, p1}, LX/G1h;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2312760
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2312761
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2312762
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2312763
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2312764
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2312765
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2312766
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2312767
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312768
    if-eqz v0, :cond_0

    .line 2312769
    const-string v1, "collage_layout"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312770
    invoke-static {p0, v0, p2}, LX/5vu;->a(LX/15i;ILX/0nX;)V

    .line 2312771
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312772
    if-eqz v0, :cond_1

    .line 2312773
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312774
    invoke-static {p0, v0, p2, p3}, LX/G1f;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2312775
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312776
    if-eqz v0, :cond_2

    .line 2312777
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312778
    invoke-static {p0, v0, p2}, LX/G1g;->a(LX/15i;ILX/0nX;)V

    .line 2312779
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312780
    if-eqz v0, :cond_3

    .line 2312781
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312782
    invoke-static {p0, v0, p2}, LX/G1h;->a(LX/15i;ILX/0nX;)V

    .line 2312783
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2312784
    return-void
.end method
