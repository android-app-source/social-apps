.class public LX/Fbj;
.super LX/FbD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbD",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbj;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260988
    invoke-direct {p0}, LX/FbD;-><init>()V

    .line 2260989
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbj;
    .locals 3

    .prologue
    .line 2260976
    sget-object v0, LX/Fbj;->a:LX/Fbj;

    if-nez v0, :cond_1

    .line 2260977
    const-class v1, LX/Fbj;

    monitor-enter v1

    .line 2260978
    :try_start_0
    sget-object v0, LX/Fbj;->a:LX/Fbj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260979
    if-eqz v2, :cond_0

    .line 2260980
    :try_start_1
    new-instance v0, LX/Fbj;

    invoke-direct {v0}, LX/Fbj;-><init>()V

    .line 2260981
    move-object v0, v0

    .line 2260982
    sput-object v0, LX/Fbj;->a:LX/Fbj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260983
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260984
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260985
    :cond_1
    sget-object v0, LX/Fbj;->a:LX/Fbj;

    return-object v0

    .line 2260986
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260987
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260974
    invoke-static {p1}, LX/Fbf;->b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2260975
    if-eqz v1, :cond_0

    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;-><init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
