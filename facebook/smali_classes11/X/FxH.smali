.class public final LX/FxH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/1U8;

.field public final synthetic b:LX/FxI;


# direct methods
.method public constructor <init>(LX/FxI;LX/1U8;)V
    .locals 0

    .prologue
    .line 2304635
    iput-object p1, p0, LX/FxH;->b:LX/FxI;

    iput-object p2, p0, LX/FxH;->a:LX/1U8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 11

    .prologue
    .line 2304636
    iget-object v0, p0, LX/FxH;->b:LX/FxI;

    iget-object v0, v0, LX/FxI;->d:LX/BQ9;

    iget-object v1, p0, LX/FxH;->b:LX/FxI;

    iget-wide v2, v1, LX/FxI;->a:J

    .line 2304637
    const/4 v8, 0x0

    sget-object v9, LX/9lQ;->SELF:LX/9lQ;

    const-string v10, "fav_photos_edit_view_large_click"

    move-object v5, v0

    move-wide v6, v2

    invoke-static/range {v5 .. v10}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2304638
    if-eqz v4, :cond_0

    .line 2304639
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2304640
    :cond_0
    iget-object v0, p0, LX/FxH;->b:LX/FxI;

    iget-object v1, p0, LX/FxH;->a:LX/1U8;

    const/4 v5, 0x0

    .line 2304641
    invoke-interface {v1}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2304642
    invoke-interface {v1}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v8

    .line 2304643
    :goto_0
    sget-object v10, LX/74S;->TIMELINE_INTRO_CARD_FAV_PHOTO:LX/74S;

    .line 2304644
    iget-object v4, v0, LX/FxI;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FwN;

    invoke-interface {v1}, LX/1U8;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v9

    move-object v7, v5

    invoke-virtual/range {v4 .. v10}, LX/FwN;->a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/1Fb;LX/74S;)V

    .line 2304645
    const/4 v0, 0x1

    return v0

    :cond_1
    move-object v8, v5

    goto :goto_0
.end method
