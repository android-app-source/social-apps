.class public final LX/GVI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/2gk;


# direct methods
.method public constructor <init>(LX/2gk;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2358837
    iput-object p1, p0, LX/GVI;->b:LX/2gk;

    iput-object p2, p0, LX/GVI;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2358838
    iget-object v0, p0, LX/GVI;->b:LX/2gk;

    iget-object v0, v0, LX/2gk;->l:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    .line 2358839
    iget-object v1, p0, LX/GVI;->b:LX/2gk;

    iget-object v1, v1, LX/2gk;->n:LX/0yG;

    if-eq v0, v1, :cond_0

    .line 2358840
    iget-object v1, p0, LX/GVI;->b:LX/2gk;

    iget-object v1, v1, LX/2gk;->g:LX/2bj;

    const-string v2, "location_opt_in_location_permission_req_allowed"

    invoke-virtual {v1, v2, v0}, LX/2bj;->a(Ljava/lang/String;LX/0yG;)V

    .line 2358841
    :cond_0
    iget-object v0, p0, LX/GVI;->b:LX/2gk;

    iget-object v0, v0, LX/2gk;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6ZU;

    new-instance v1, LX/2si;

    invoke-direct {v1}, LX/2si;-><init>()V

    invoke-virtual {v0, v1}, LX/6ZU;->a(LX/2si;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GVH;

    invoke-direct {v1, p0}, LX/GVH;-><init>(LX/GVI;)V

    iget-object v2, p0, LX/GVI;->b:LX/2gk;

    iget-object v2, v2, LX/2gk;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2358842
    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2358843
    iget-object v0, p0, LX/GVI;->b:LX/2gk;

    iget-object v0, v0, LX/2gk;->g:LX/2bj;

    const-string v1, "location_opt_in_location_permission_req_denied"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    .line 2358844
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2358845
    iget-object v0, p0, LX/GVI;->b:LX/2gk;

    iget-object v0, v0, LX/2gk;->g:LX/2bj;

    const-string v1, "location_opt_in_location_permission_req_canceled"

    invoke-virtual {v0, v1}, LX/2bj;->a(Ljava/lang/String;)V

    .line 2358846
    return-void
.end method
