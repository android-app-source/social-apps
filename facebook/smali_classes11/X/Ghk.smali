.class public final LX/Ghk;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ghl;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/Ghu;

.field public b:LX/Ghu;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/Ghl;


# direct methods
.method public constructor <init>(LX/Ghl;)V
    .locals 1

    .prologue
    .line 2384753
    iput-object p1, p0, LX/Ghk;->e:LX/Ghl;

    .line 2384754
    move-object v0, p1

    .line 2384755
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2384756
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2384757
    const-string v0, "GametimeMatchComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2384733
    if-ne p0, p1, :cond_1

    .line 2384734
    :cond_0
    :goto_0
    return v0

    .line 2384735
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2384736
    goto :goto_0

    .line 2384737
    :cond_3
    check-cast p1, LX/Ghk;

    .line 2384738
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2384739
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2384740
    if-eq v2, v3, :cond_0

    .line 2384741
    iget-object v2, p0, LX/Ghk;->a:LX/Ghu;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ghk;->a:LX/Ghu;

    iget-object v3, p1, LX/Ghk;->a:LX/Ghu;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2384742
    goto :goto_0

    .line 2384743
    :cond_5
    iget-object v2, p1, LX/Ghk;->a:LX/Ghu;

    if-nez v2, :cond_4

    .line 2384744
    :cond_6
    iget-object v2, p0, LX/Ghk;->b:LX/Ghu;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Ghk;->b:LX/Ghu;

    iget-object v3, p1, LX/Ghk;->b:LX/Ghu;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2384745
    goto :goto_0

    .line 2384746
    :cond_8
    iget-object v2, p1, LX/Ghk;->b:LX/Ghu;

    if-nez v2, :cond_7

    .line 2384747
    :cond_9
    iget-object v2, p0, LX/Ghk;->c:LX/0Px;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Ghk;->c:LX/0Px;

    iget-object v3, p1, LX/Ghk;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2384748
    goto :goto_0

    .line 2384749
    :cond_b
    iget-object v2, p1, LX/Ghk;->c:LX/0Px;

    if-nez v2, :cond_a

    .line 2384750
    :cond_c
    iget-object v2, p0, LX/Ghk;->d:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/Ghk;->d:Ljava/lang/String;

    iget-object v3, p1, LX/Ghk;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2384751
    goto :goto_0

    .line 2384752
    :cond_d
    iget-object v2, p1, LX/Ghk;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
