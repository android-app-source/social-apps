.class public LX/FVX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/ViewStub;

.field public c:LX/FVn;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250851
    return-void
.end method

.method public static a(LX/0QB;)LX/FVX;
    .locals 1

    .prologue
    .line 2250852
    new-instance v0, LX/FVX;

    invoke-direct {v0}, LX/FVX;-><init>()V

    .line 2250853
    move-object v0, v0

    .line 2250854
    return-object v0
.end method

.method public static e(LX/FVX;)V
    .locals 3

    .prologue
    .line 2250855
    iget-object v0, p0, LX/FVX;->c:LX/FVn;

    .line 2250856
    iget-object v1, v0, LX/FVn;->t:LX/FVy;

    if-eqz v1, :cond_0

    .line 2250857
    iget-object v1, v0, LX/FVn;->t:LX/FVy;

    sget-object v2, LX/FWA;->FROM_SERVER:LX/FWA;

    invoke-static {v0, v1, v2}, LX/FVn;->a$redex0(LX/FVn;LX/FVy;LX/FWA;)V

    .line 2250858
    :cond_0
    invoke-static {p0}, LX/FVX;->g(LX/FVX;)V

    .line 2250859
    return-void
.end method

.method public static g(LX/FVX;)V
    .locals 2

    .prologue
    .line 2250860
    iget-object v0, p0, LX/FVX;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2250861
    iget-object v0, p0, LX/FVX;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2250862
    iget-object v0, p0, LX/FVX;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2250863
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 2250864
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 2250865
    iget-object v0, p0, LX/FVX;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FVX;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2250866
    :cond_0
    :goto_0
    return-void

    .line 2250867
    :cond_1
    if-nez p2, :cond_0

    .line 2250868
    invoke-static {p0}, LX/FVX;->e(LX/FVX;)V

    goto :goto_0
.end method
