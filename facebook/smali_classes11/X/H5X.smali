.class public LX/H5X;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/H5V;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H5Y;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2424484
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/H5X;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H5Y;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424485
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2424486
    iput-object p1, p0, LX/H5X;->b:LX/0Ot;

    .line 2424487
    return-void
.end method

.method public static a(LX/0QB;)LX/H5X;
    .locals 4

    .prologue
    .line 2424488
    const-class v1, LX/H5X;

    monitor-enter v1

    .line 2424489
    :try_start_0
    sget-object v0, LX/H5X;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424490
    sput-object v2, LX/H5X;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424491
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424492
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424493
    new-instance v3, LX/H5X;

    const/16 p0, 0x2ade

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/H5X;-><init>(LX/0Ot;)V

    .line 2424494
    move-object v0, v3

    .line 2424495
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424496
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5X;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424497
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424498
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2424499
    const v0, -0x55b65c29

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2424500
    check-cast p2, LX/H5W;

    .line 2424501
    iget-object v0, p0, LX/H5X;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5Y;

    iget-object v2, p2, LX/H5W;->a:Ljava/lang/String;

    iget v3, p2, LX/H5W;->b:I

    iget-object v4, p2, LX/H5W;->c:Ljava/lang/String;

    iget-object v5, p2, LX/H5W;->d:Landroid/view/View$OnClickListener;

    move-object v1, p1

    const/4 p1, 0x2

    const/4 p0, 0x1

    const/4 v8, 0x0

    .line 2424502
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/16 v6, 0x8

    invoke-interface {v0, v6, v8}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const v6, 0x7f0b0a90

    invoke-interface {v0, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    const/4 v6, 0x6

    const v7, 0x7f0b0a95

    invoke-interface {v0, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const v6, 0x7f0a0097

    invoke-interface {v0, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    .line 2424503
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a00a8

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0a93

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    invoke-interface {v0, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2424504
    if-lez v3, :cond_0

    .line 2424505
    const/4 v2, 0x1

    .line 2424506
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2424507
    const v7, 0x7f081182

    invoke-virtual {v1, v7}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2424508
    const v7, 0x7f081183

    new-array p0, v2, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p0, p1

    invoke-virtual {v1, v7, p0}, LX/1De;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2424509
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2424510
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a00a8

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0a93

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->d()LX/1X1;

    move-result-object v6

    move-object v6, v6

    .line 2424511
    invoke-interface {v0, v6}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    .line 2424512
    :cond_0
    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    .line 2424513
    const/4 p1, 0x1

    .line 2424514
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0a93

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a008b

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p1}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0a90

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const/4 v7, 0x6

    const p0, 0x7f0b0a95

    invoke-interface {v6, v7, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x7

    const p0, 0x7f0b0a94

    invoke-interface {v6, v7, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p1}, LX/1Di;->c(I)LX/1Di;

    move-result-object v6

    const/4 v7, 0x5

    const/4 p0, 0x0

    invoke-interface {v6, v7, p0}, LX/1Di;->k(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v6

    .line 2424515
    const v7, -0x55b65c29

    const/4 p0, 0x0

    invoke-static {v1, v7, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2424516
    invoke-interface {v6, v7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v6, v6

    .line 2424517
    invoke-interface {v0, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    .line 2424518
    :cond_1
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    invoke-interface {v6, v0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v6

    const v7, 0x7f0a009f

    invoke-virtual {v6, v7}, LX/25Q;->i(I)LX/25Q;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0a98

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const/4 v7, 0x3

    invoke-interface {v6, v7, v8}, LX/1Di;->k(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    invoke-interface {v0, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    .line 2424519
    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2424520
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2424521
    invoke-static {}, LX/1dS;->b()V

    .line 2424522
    iget v0, p1, LX/1dQ;->b:I

    .line 2424523
    packed-switch v0, :pswitch_data_0

    .line 2424524
    :goto_0
    return-object v2

    .line 2424525
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2424526
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2424527
    check-cast v1, LX/H5W;

    .line 2424528
    iget-object p1, p0, LX/H5X;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/H5W;->d:Landroid/view/View$OnClickListener;

    .line 2424529
    invoke-interface {p1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2424530
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x55b65c29
        :pswitch_0
    .end packed-switch
.end method
