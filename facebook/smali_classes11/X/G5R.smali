.class public LX/G5R;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/G5L;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:Z

.field public f:Z

.field public final g:LX/0Zb;

.field private final h:LX/0hx;

.field private final i:LX/0So;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0hx;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318761
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318762
    iput-object p1, p0, LX/G5R;->g:LX/0Zb;

    .line 2318763
    iput-object p2, p0, LX/G5R;->h:LX/0hx;

    .line 2318764
    iput-object p3, p0, LX/G5R;->i:LX/0So;

    .line 2318765
    return-void
.end method

.method public static a(LX/G5R;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2318730
    invoke-static/range {p0 .. p5}, LX/G5R;->b(LX/G5R;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/util/List;)V

    .line 2318731
    iget-object v0, p0, LX/G5R;->h:LX/0hx;

    const-string v1, "tap_search_result"

    .line 2318732
    iget-object p0, v0, LX/0hx;->c:LX/0gh;

    invoke-virtual {p0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2318733
    return-void
.end method

.method public static b(LX/0QB;)LX/G5R;
    .locals 4

    .prologue
    .line 2318759
    new-instance v3, LX/G5R;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v1

    check-cast v1, LX/0hx;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v2

    check-cast v2, LX/0So;

    invoke-direct {v3, v0, v1, v2}, LX/G5R;-><init>(LX/0Zb;LX/0hx;LX/0So;)V

    .line 2318760
    return-object v3
.end method

.method public static b(LX/G5R;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2318757
    iget-object v8, p0, LX/G5R;->b:Ljava/util/List;

    new-instance v0, LX/G5L;

    iget-object v1, p0, LX/G5R;->i:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v6

    move-object v1, p1

    move-object v2, p3

    move v3, p4

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, LX/G5L;-><init>(Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/lang/String;Ljava/util/List;J)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2318758
    return-void
.end method

.method public static f(LX/G5R;)Lorg/json/JSONArray;
    .locals 13

    .prologue
    .line 2318738
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 2318739
    iget-object v0, p0, LX/G5R;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G5L;

    .line 2318740
    const/4 v3, 0x0

    .line 2318741
    const-wide/16 v5, -0x1

    .line 2318742
    iget-object v4, v0, LX/G5L;->b:Lcom/facebook/search/api/SearchTypeaheadResult;

    if-eqz v4, :cond_2

    .line 2318743
    iget-object v3, v0, LX/G5L;->b:Lcom/facebook/search/api/SearchTypeaheadResult;

    iget-wide v5, v3, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    .line 2318744
    iget-object v3, v0, LX/G5L;->b:Lcom/facebook/search/api/SearchTypeaheadResult;

    iget-object v4, v3, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    .line 2318745
    iget-object v3, v0, LX/G5L;->b:Lcom/facebook/search/api/SearchTypeaheadResult;

    iget-object v3, v3, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-wide v7, v5

    move-object v5, v4

    move-object v4, v3

    .line 2318746
    :goto_1
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 2318747
    iget-object v3, v0, LX/G5L;->b:Lcom/facebook/search/api/SearchTypeaheadResult;

    if-eqz v3, :cond_0

    .line 2318748
    iget-object v3, v0, LX/G5L;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 2318749
    iget-wide v11, v3, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-virtual {v6, v11, v12}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    goto :goto_2

    .line 2318750
    :cond_0
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 2318751
    :try_start_0
    const-string v3, "event"

    iget-object v10, v0, LX/G5L;->a:Ljava/lang/String;

    invoke-virtual {v9, v3, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v10, "id"

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v10, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v7, "type"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v7, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v5, "friend_status"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "query"

    iget-object v5, v0, LX/G5L;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "position"

    iget v5, v0, LX/G5L;->d:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "results"

    invoke-virtual {v6}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "timestamp_ms"

    iget-wide v5, v0, LX/G5L;->f:J

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2318752
    :goto_3
    move-object v0, v9

    .line 2318753
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto/16 :goto_0

    .line 2318754
    :cond_1
    return-object v1

    .line 2318755
    :catch_0
    move-exception v3

    .line 2318756
    const-string v4, "SearchClickEvent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JSONException when ENCODING data: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_2
    move-object v4, v3

    move-wide v7, v5

    move-object v5, v3

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/SearchTypeaheadResult;Ljava/lang/String;ILjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2318736
    const-string v1, "click"

    move-object v0, p0

    move-object v2, p2

    move-object v3, p1

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/G5R;->a(LX/G5R;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/util/List;)V

    .line 2318737
    return-void
.end method

.method public final c(Lcom/facebook/search/api/SearchTypeaheadResult;Ljava/lang/String;Ljava/util/List;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2318734
    const-string v1, "inline_page_like_request"

    move-object v0, p0

    move-object v2, p2

    move-object v3, p1

    move v4, p4

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/G5R;->a(LX/G5R;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/search/api/SearchTypeaheadResult;ILjava/util/List;)V

    .line 2318735
    return-void
.end method
