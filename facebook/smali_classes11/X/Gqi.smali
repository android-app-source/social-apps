.class public LX/Gqi;
.super LX/Gqh;
.source ""


# instance fields
.field public r:LX/CIh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final t:Lcom/facebook/widget/text/BetterTextView;

.field private final u:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;LX/Ctg;)V
    .locals 3

    .prologue
    .line 2396890
    invoke-direct {p0, p2, p1}, LX/Gqh;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 2396891
    const-class v0, LX/Gqi;

    invoke-static {v0, p0}, LX/Gqi;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396892
    iput-object p1, p0, LX/Gqi;->u:Landroid/view/View;

    .line 2396893
    iget-object v0, p0, LX/Gqi;->u:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 2396894
    if-nez v0, :cond_0

    .line 2396895
    new-instance v0, LX/1a3;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, LX/1a3;-><init>(II)V

    .line 2396896
    :cond_0
    iget-object v1, p0, LX/Gqi;->s:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    iput v1, v0, LX/1a3;->width:I

    .line 2396897
    iget-object v1, p0, LX/Gqi;->u:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2396898
    const v0, 0x7f0d0bd4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Gqi;->t:Lcom/facebook/widget/text/BetterTextView;

    .line 2396899
    iget-object v0, p0, LX/Gqi;->t:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2396900
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gqi;

    invoke-static {p0}, LX/CIh;->b(LX/0QB;)LX/CIh;

    move-result-object v1

    check-cast v1, LX/CIh;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object v1, p1, LX/Gqi;->r:LX/CIh;

    iput-object p0, p1, LX/Gqi;->s:LX/0hB;

    return-void
.end method

.method public static c(Landroid/view/View;)LX/Cow;
    .locals 3

    .prologue
    .line 2396887
    new-instance v1, LX/Gqi;

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/Ctg;

    check-cast v0, LX/Ctg;

    invoke-direct {v1, p0, v0}, LX/Gqi;-><init>(Landroid/view/View;LX/Ctg;)V

    return-object v1
.end method


# virtual methods
.method public final a(LX/GoE;)V
    .locals 0

    .prologue
    .line 2396888
    return-void
.end method

.method public final i()Landroid/view/View;
    .locals 1

    .prologue
    .line 2396889
    iget-object v0, p0, LX/Gqi;->u:Landroid/view/View;

    return-object v0
.end method
