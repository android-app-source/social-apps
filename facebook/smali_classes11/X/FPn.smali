.class public final enum LX/FPn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FPn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FPn;

.field public static final enum CITY:LX/FPn;

.field public static final enum CITY_MAP_REGION:LX/FPn;

.field public static final enum NONE:LX/FPn;

.field public static final enum USER_CENTERED:LX/FPn;

.field public static final enum USER_CENTERED_MAP_REGION:LX/FPn;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2237943
    new-instance v0, LX/FPn;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/FPn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPn;->NONE:LX/FPn;

    .line 2237944
    new-instance v0, LX/FPn;

    const-string v1, "USER_CENTERED"

    invoke-direct {v0, v1, v3}, LX/FPn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPn;->USER_CENTERED:LX/FPn;

    .line 2237945
    new-instance v0, LX/FPn;

    const-string v1, "USER_CENTERED_MAP_REGION"

    invoke-direct {v0, v1, v4}, LX/FPn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPn;->USER_CENTERED_MAP_REGION:LX/FPn;

    .line 2237946
    new-instance v0, LX/FPn;

    const-string v1, "CITY"

    invoke-direct {v0, v1, v5}, LX/FPn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPn;->CITY:LX/FPn;

    .line 2237947
    new-instance v0, LX/FPn;

    const-string v1, "CITY_MAP_REGION"

    invoke-direct {v0, v1, v6}, LX/FPn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPn;->CITY_MAP_REGION:LX/FPn;

    .line 2237948
    const/4 v0, 0x5

    new-array v0, v0, [LX/FPn;

    sget-object v1, LX/FPn;->NONE:LX/FPn;

    aput-object v1, v0, v2

    sget-object v1, LX/FPn;->USER_CENTERED:LX/FPn;

    aput-object v1, v0, v3

    sget-object v1, LX/FPn;->USER_CENTERED_MAP_REGION:LX/FPn;

    aput-object v1, v0, v4

    sget-object v1, LX/FPn;->CITY:LX/FPn;

    aput-object v1, v0, v5

    sget-object v1, LX/FPn;->CITY_MAP_REGION:LX/FPn;

    aput-object v1, v0, v6

    sput-object v0, LX/FPn;->$VALUES:[LX/FPn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2237949
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FPn;
    .locals 1

    .prologue
    .line 2237950
    const-class v0, LX/FPn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FPn;

    return-object v0
.end method

.method public static values()[LX/FPn;
    .locals 1

    .prologue
    .line 2237951
    sget-object v0, LX/FPn;->$VALUES:[LX/FPn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FPn;

    return-object v0
.end method
