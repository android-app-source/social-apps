.class public LX/FUq;
.super Landroid/widget/ScrollView;
.source ""

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements LX/5r7;


# static fields
.field private static a:Ljava/lang/reflect/Field;

.field private static b:Z


# instance fields
.field private final c:LX/FUn;

.field private final d:Landroid/widget/OverScroller;

.field private e:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field private g:Z

.field public h:Z

.field private i:Z

.field public j:Z

.field public k:Z

.field private l:LX/F6L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I

.field private p:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2250043
    const/4 v0, 0x0

    sput-boolean v0, LX/FUq;->b:Z

    return-void
.end method

.method public constructor <init>(LX/5pX;)V
    .locals 1

    .prologue
    .line 2249976
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FUq;-><init>(LX/5pX;LX/F6L;)V

    .line 2249977
    return-void
.end method

.method public constructor <init>(LX/5pX;LX/F6L;)V
    .locals 3
    .param p2    # LX/F6L;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2249978
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 2249979
    new-instance v0, LX/FUn;

    invoke-direct {v0}, LX/FUn;-><init>()V

    iput-object v0, p0, LX/FUq;->c:LX/FUn;

    .line 2249980
    iput-boolean v1, p0, LX/FUq;->j:Z

    .line 2249981
    iput-object v2, p0, LX/FUq;->l:LX/F6L;

    .line 2249982
    const/4 v0, 0x0

    iput v0, p0, LX/FUq;->o:I

    .line 2249983
    iput-object p2, p0, LX/FUq;->l:LX/F6L;

    .line 2249984
    sget-boolean v0, LX/FUq;->b:Z

    if-nez v0, :cond_0

    .line 2249985
    sput-boolean v1, LX/FUq;->b:Z

    .line 2249986
    :try_start_0
    const-class v0, Landroid/widget/ScrollView;

    const-string v1, "mScroller"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 2249987
    sput-object v0, LX/FUq;->a:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2249988
    :cond_0
    :goto_0
    sget-object v0, LX/FUq;->a:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_2

    .line 2249989
    :try_start_1
    sget-object v0, LX/FUq;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2249990
    instance-of v1, v0, Landroid/widget/OverScroller;

    if-eqz v1, :cond_1

    .line 2249991
    check-cast v0, Landroid/widget/OverScroller;

    iput-object v0, p0, LX/FUq;->d:Landroid/widget/OverScroller;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2249992
    :goto_1
    invoke-virtual {p0, p0}, LX/FUq;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 2249993
    return-void

    .line 2249994
    :catch_0
    const-string v0, "React"

    const-string v1, "Failed to get mScroller field for ScrollView! This app will exhibit the bounce-back scrolling bug :("

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2249995
    :cond_1
    :try_start_2
    const-string v0, "React"

    const-string v1, "Failed to cast mScroller field in ScrollView (probably due to OEM changes to AOSP)! This app will exhibit the bounce-back scrolling bug :("

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2249996
    const/4 v0, 0x0

    iput-object v0, p0, LX/FUq;->d:Landroid/widget/OverScroller;
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 2249997
    :catch_1
    move-exception v0

    .line 2249998
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to get mScroller from ScrollView!"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2249999
    :cond_2
    iput-object v2, p0, LX/FUq;->d:Landroid/widget/OverScroller;

    goto :goto_1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2250000
    invoke-direct {p0}, LX/FUq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250001
    iget-object v0, p0, LX/FUq;->l:LX/F6L;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250002
    iget-object v0, p0, LX/FUq;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250003
    iget-object v0, p0, LX/FUq;->l:LX/F6L;

    iget-object v1, p0, LX/FUq;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/F6L;->a(Ljava/lang/String;)V

    .line 2250004
    :cond_0
    return-void
.end method

.method public static c(LX/FUq;)V
    .locals 2

    .prologue
    .line 2250005
    invoke-direct {p0}, LX/FUq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250006
    iget-object v0, p0, LX/FUq;->l:LX/F6L;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250007
    iget-object v0, p0, LX/FUq;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250008
    iget-object v0, p0, LX/FUq;->l:LX/F6L;

    iget-object v1, p0, LX/FUq;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/F6L;->b(Ljava/lang/String;)V

    .line 2250009
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 2250010
    iget-object v0, p0, LX/FUq;->l:LX/F6L;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FUq;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FUq;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMaxScrollY()I
    .locals 3

    .prologue
    .line 2250011
    iget-object v0, p0, LX/FUq;->p:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 2250012
    invoke-virtual {p0}, LX/FUq;->getHeight()I

    move-result v1

    invoke-virtual {p0}, LX/FUq;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, LX/FUq;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2250013
    const/4 v2, 0x0

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2250014
    iget-boolean v0, p0, LX/FUq;->i:Z

    if-nez v0, :cond_1

    .line 2250015
    :cond_0
    :goto_0
    return-void

    .line 2250016
    :cond_1
    iget-object v0, p0, LX/FUq;->e:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250017
    iget-object v0, p0, LX/FUq;->e:Landroid/graphics/Rect;

    invoke-static {p0, v0}, LX/5r8;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2250018
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/FUq;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2250019
    instance-of v1, v0, LX/5r7;

    if-eqz v1, :cond_0

    .line 2250020
    check-cast v0, LX/5r7;

    invoke-interface {v0}, LX/5r7;->a()V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 2250021
    iget-object v0, p0, LX/FUq;->e:Landroid/graphics/Rect;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2250022
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2250023
    iget v0, p0, LX/FUq;->o:I

    if-eqz v0, :cond_0

    .line 2250024
    invoke-virtual {p0, v4}, LX/FUq;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2250025
    iget-object v1, p0, LX/FUq;->n:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p0}, LX/FUq;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2250026
    iget-object v1, p0, LX/FUq;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, LX/FUq;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/FUq;->getHeight()I

    move-result v3

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2250027
    iget-object v0, p0, LX/FUq;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2250028
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->draw(Landroid/graphics/Canvas;)V

    .line 2250029
    return-void
.end method

.method public final fling(I)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 2250030
    iget-object v0, p0, LX/FUq;->d:Landroid/widget/OverScroller;

    if-eqz v0, :cond_2

    .line 2250031
    invoke-virtual {p0}, LX/FUq;->getHeight()I

    move-result v0

    invoke-virtual {p0}, LX/FUq;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/FUq;->getPaddingTop()I

    move-result v1

    sub-int v4, v0, v1

    .line 2250032
    iget-object v0, p0, LX/FUq;->d:Landroid/widget/OverScroller;

    invoke-virtual {p0}, LX/FUq;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, LX/FUq;->getScrollY()I

    move-result v2

    const v8, 0x7fffffff

    div-int/lit8 v10, v4, 0x2

    move v4, p1

    move v5, v3

    move v6, v3

    move v7, v3

    move v9, v3

    invoke-virtual/range {v0 .. v10}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    .line 2250033
    invoke-virtual {p0}, LX/FUq;->postInvalidateOnAnimation()V

    .line 2250034
    :goto_0
    iget-boolean v0, p0, LX/FUq;->k:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, LX/FUq;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2250035
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FUq;->h:Z

    .line 2250036
    invoke-direct {p0}, LX/FUq;->b()V

    .line 2250037
    invoke-static {p0}, LX/FUt;->d(Landroid/view/ViewGroup;)V

    .line 2250038
    new-instance v0, Lcom/facebook/react/views/scroll/ReactScrollView$1;

    invoke-direct {v0, p0}, Lcom/facebook/react/views/scroll/ReactScrollView$1;-><init>(LX/FUq;)V

    .line 2250039
    const-wide/16 v2, 0x14

    invoke-virtual {p0, v0, v2, v3}, LX/FUq;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    .line 2250040
    :cond_1
    return-void

    .line 2250041
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->fling(I)V

    goto :goto_0
.end method

.method public getRemoveClippedSubviews()Z
    .locals 1

    .prologue
    .line 2250042
    iget-boolean v0, p0, LX/FUq;->i:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x27f6e169

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2249966
    invoke-super {p0}, Landroid/widget/ScrollView;->onAttachedToWindow()V

    .line 2249967
    iget-boolean v1, p0, LX/FUq;->i:Z

    if-eqz v1, :cond_0

    .line 2249968
    invoke-virtual {p0}, LX/FUq;->a()V

    .line 2249969
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x5747bbc0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2249970
    iput-object p2, p0, LX/FUq;->p:Landroid/view/View;

    .line 2249971
    iget-object v0, p0, LX/FUq;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2249972
    return-void
.end method

.method public final onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2249973
    iget-object v0, p0, LX/FUq;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2249974
    const/4 v0, 0x0

    iput-object v0, p0, LX/FUq;->p:Landroid/view/View;

    .line 2249975
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2249904
    iget-boolean v2, p0, LX/FUq;->j:Z

    if-nez v2, :cond_1

    .line 2249905
    :cond_0
    :goto_0
    return v0

    .line 2249906
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2249907
    invoke-static {p0, p1}, LX/5sB;->a(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 2249908
    invoke-static {p0}, LX/FUt;->b(Landroid/view/ViewGroup;)V

    .line 2249909
    iput-boolean v1, p0, LX/FUq;->g:Z

    .line 2249910
    invoke-direct {p0}, LX/FUq;->b()V

    move v0, v1

    .line 2249911
    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 2249912
    invoke-virtual {p0}, LX/FUq;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, LX/FUq;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/FUq;->scrollTo(II)V

    .line 2249913
    return-void
.end method

.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    .prologue
    .line 2249914
    iget-object v0, p0, LX/FUq;->p:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2249915
    :cond_0
    :goto_0
    return-void

    .line 2249916
    :cond_1
    invoke-virtual {p0}, LX/FUq;->getScrollY()I

    move-result v0

    .line 2249917
    invoke-direct {p0}, LX/FUq;->getMaxScrollY()I

    move-result v1

    .line 2249918
    if-le v0, v1, :cond_0

    .line 2249919
    invoke-virtual {p0}, LX/FUq;->getScrollX()I

    move-result v0

    invoke-virtual {p0, v0, v1}, LX/FUq;->scrollTo(II)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2249920
    invoke-static {p1, p2}, LX/5qs;->a(II)V

    .line 2249921
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/FUq;->setMeasuredDimension(II)V

    .line 2249922
    return-void
.end method

.method public final onOverScrolled(IIZZ)V
    .locals 2

    .prologue
    .line 2249923
    iget-object v0, p0, LX/FUq;->d:Landroid/widget/OverScroller;

    if-eqz v0, :cond_0

    .line 2249924
    iget-object v0, p0, LX/FUq;->d:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FUq;->d:Landroid/widget/OverScroller;

    invoke-virtual {v0}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v0

    iget-object v1, p0, LX/FUq;->d:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->getFinalY()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2249925
    invoke-direct {p0}, LX/FUq;->getMaxScrollY()I

    move-result v0

    .line 2249926
    if-lt p2, v0, :cond_0

    .line 2249927
    iget-object v1, p0, LX/FUq;->d:Landroid/widget/OverScroller;

    invoke-virtual {v1}, Landroid/widget/OverScroller;->abortAnimation()V

    move p2, v0

    .line 2249928
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onOverScrolled(IIZZ)V

    .line 2249929
    return-void
.end method

.method public onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 2249930
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 2249931
    iget-object v0, p0, LX/FUq;->c:LX/FUn;

    invoke-virtual {v0, p1, p2}, LX/FUn;->a(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2249932
    iget-boolean v0, p0, LX/FUq;->i:Z

    if-eqz v0, :cond_0

    .line 2249933
    invoke-virtual {p0}, LX/FUq;->a()V

    .line 2249934
    :cond_0
    iget-boolean v0, p0, LX/FUq;->h:Z

    if-eqz v0, :cond_1

    .line 2249935
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FUq;->f:Z

    .line 2249936
    :cond_1
    invoke-static {p0}, LX/FUt;->a(Landroid/view/ViewGroup;)V

    .line 2249937
    :cond_2
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x60e0154b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2249938
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onSizeChanged(IIII)V

    .line 2249939
    iget-boolean v1, p0, LX/FUq;->i:Z

    if-eqz v1, :cond_0

    .line 2249940
    invoke-virtual {p0}, LX/FUq;->a()V

    .line 2249941
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x4bbb91c3    # 2.4585094E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x2

    const v1, 0x530b1242

    invoke-static {v3, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2249942
    iget-boolean v2, p0, LX/FUq;->j:Z

    if-nez v2, :cond_0

    .line 2249943
    const v2, 0x25a2f271

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2249944
    :goto_0
    return v0

    .line 2249945
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    .line 2249946
    if-ne v2, v4, :cond_1

    iget-boolean v2, p0, LX/FUq;->g:Z

    if-eqz v2, :cond_1

    .line 2249947
    invoke-static {p0}, LX/FUt;->c(Landroid/view/ViewGroup;)V

    .line 2249948
    iput-boolean v0, p0, LX/FUq;->g:Z

    .line 2249949
    invoke-static {p0}, LX/FUq;->c(LX/FUq;)V

    .line 2249950
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x623dbfdf

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setEndFillColor(I)V
    .locals 2

    .prologue
    .line 2249951
    iget v0, p0, LX/FUq;->o:I

    if-eq p1, v0, :cond_0

    .line 2249952
    iput p1, p0, LX/FUq;->o:I

    .line 2249953
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, LX/FUq;->o:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/FUq;->n:Landroid/graphics/drawable/Drawable;

    .line 2249954
    :cond_0
    return-void
.end method

.method public setRemoveClippedSubviews(Z)V
    .locals 1

    .prologue
    .line 2249955
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/FUq;->e:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 2249956
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/FUq;->e:Landroid/graphics/Rect;

    .line 2249957
    :cond_0
    iput-boolean p1, p0, LX/FUq;->i:Z

    .line 2249958
    invoke-virtual {p0}, LX/FUq;->a()V

    .line 2249959
    return-void
.end method

.method public setScrollEnabled(Z)V
    .locals 0

    .prologue
    .line 2249960
    iput-boolean p1, p0, LX/FUq;->j:Z

    .line 2249961
    return-void
.end method

.method public setScrollPerfTag(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2249962
    iput-object p1, p0, LX/FUq;->m:Ljava/lang/String;

    .line 2249963
    return-void
.end method

.method public setSendMomentumEvents(Z)V
    .locals 0

    .prologue
    .line 2249964
    iput-boolean p1, p0, LX/FUq;->k:Z

    .line 2249965
    return-void
.end method
