.class public final LX/GU6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2356916
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2356917
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2356918
    :goto_0
    return v1

    .line 2356919
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2356920
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2356921
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2356922
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2356923
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2356924
    const-string v3, "location"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2356925
    const/4 v2, 0x0

    .line 2356926
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2356927
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2356928
    :goto_2
    move v0, v2

    .line 2356929
    goto :goto_1

    .line 2356930
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2356931
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2356932
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2356933
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2356934
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 2356935
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2356936
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2356937
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 2356938
    const-string v4, "reverse_geocode"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2356939
    const/4 v3, 0x0

    .line 2356940
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_b

    .line 2356941
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2356942
    :goto_4
    move v0, v3

    .line 2356943
    goto :goto_3

    .line 2356944
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2356945
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2356946
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    .line 2356947
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2356948
    :cond_9
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_a

    .line 2356949
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2356950
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2356951
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_9

    if-eqz v4, :cond_9

    .line 2356952
    const-string v5, "city"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2356953
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_5

    .line 2356954
    :cond_a
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2356955
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2356956
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_4

    :cond_b
    move v0, v3

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2356957
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2356958
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2356959
    if-eqz v0, :cond_2

    .line 2356960
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2356961
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2356962
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2356963
    if-eqz v1, :cond_1

    .line 2356964
    const-string p1, "reverse_geocode"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2356965
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2356966
    const/4 p1, 0x0

    invoke-virtual {p0, v1, p1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p1

    .line 2356967
    if-eqz p1, :cond_0

    .line 2356968
    const-string v0, "city"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2356969
    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2356970
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2356971
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2356972
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2356973
    return-void
.end method
