.class public final LX/FlH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FlI;


# direct methods
.method public constructor <init>(LX/FlI;)V
    .locals 0

    .prologue
    .line 2279900
    iput-object p1, p0, LX/FlH;->a:LX/FlI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/FlI;B)V
    .locals 0

    .prologue
    .line 2279885
    invoke-direct {p0, p1}, LX/FlH;-><init>(LX/FlI;)V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2279886
    if-eqz p1, :cond_0

    .line 2279887
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2279888
    if-eqz v0, :cond_0

    .line 2279889
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2279890
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;->a()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel$FundraiserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2279891
    iget-object v0, p0, LX/FlH;->a:LX/FlI;

    iget-boolean v1, v0, LX/FlI;->q:Z

    .line 2279892
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2279893
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;->a()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel$FundraiserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel$FundraiserModel;->j()Z

    move-result v0

    if-eq v1, v0, :cond_0

    .line 2279894
    iget-object v1, p0, LX/FlH;->a:LX/FlI;

    .line 2279895
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2279896
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;->a()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel$FundraiserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel$FundraiserModel;->j()Z

    move-result v0

    .line 2279897
    iput-boolean v0, v1, LX/FlI;->q:Z

    .line 2279898
    iget-object v0, p0, LX/FlH;->a:LX/FlI;

    invoke-static {v0}, LX/FlI;->a(LX/FlI;)V

    .line 2279899
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2279875
    iget-object v0, p0, LX/FlH;->a:LX/FlI;

    iget-object v0, v0, LX/FlI;->b:LX/0Zb;

    iget-object v1, p0, LX/FlH;->a:LX/FlI;

    iget-object v1, v1, LX/FlI;->p:Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;

    .line 2279876
    iget-object p0, v1, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    move-object v1, p0

    .line 2279877
    iget-object p0, v1, LX/FlJ;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2279878
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fundraiser_follow_mutation_failure"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "fundraiser_page"

    .line 2279879
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2279880
    move-object p0, p0

    .line 2279881
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object v1, p0

    .line 2279882
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2279883
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279884
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/FlH;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
