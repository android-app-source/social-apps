.class public LX/FUt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2250062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2250078
    sget-object v0, LX/FUv;->SCROLL:LX/FUv;

    invoke-static {p0, v0}, LX/FUt;->a(Landroid/view/ViewGroup;LX/FUv;)V

    .line 2250079
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;LX/FUv;)V
    .locals 9

    .prologue
    .line 2250071
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2250072
    if-nez v1, :cond_0

    .line 2250073
    :goto_0
    return-void

    .line 2250074
    :cond_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    .line 2250075
    const-class v2, LX/5rQ;

    invoke-virtual {v0, v2}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2250076
    iget-object v2, v0, LX/5rQ;->a:LX/5s9;

    move-object v8, v2

    .line 2250077
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollX()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    move-object v1, p1

    invoke-static/range {v0 .. v7}, LX/FUu;->a(ILX/FUv;IIIIII)LX/FUu;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/5s9;->a(LX/5r0;)V

    goto :goto_0
.end method

.method public static b(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2250069
    sget-object v0, LX/FUv;->BEGIN_DRAG:LX/FUv;

    invoke-static {p0, v0}, LX/FUt;->a(Landroid/view/ViewGroup;LX/FUv;)V

    .line 2250070
    return-void
.end method

.method public static c(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2250067
    sget-object v0, LX/FUv;->END_DRAG:LX/FUv;

    invoke-static {p0, v0}, LX/FUt;->a(Landroid/view/ViewGroup;LX/FUv;)V

    .line 2250068
    return-void
.end method

.method public static d(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2250065
    sget-object v0, LX/FUv;->MOMENTUM_BEGIN:LX/FUv;

    invoke-static {p0, v0}, LX/FUt;->a(Landroid/view/ViewGroup;LX/FUv;)V

    .line 2250066
    return-void
.end method

.method public static e(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2250063
    sget-object v0, LX/FUv;->MOMENTUM_END:LX/FUv;

    invoke-static {p0, v0}, LX/FUt;->a(Landroid/view/ViewGroup;LX/FUv;)V

    .line 2250064
    return-void
.end method
