.class public final LX/GDP;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GKs;

.field public final synthetic b:LX/GDS;


# direct methods
.method public constructor <init>(LX/GDS;LX/GKs;)V
    .locals 0

    .prologue
    .line 2329803
    iput-object p1, p0, LX/GDP;->b:LX/GDS;

    iput-object p2, p0, LX/GDP;->a:LX/GKs;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2329804
    iget-object v0, p0, LX/GDP;->b:LX/GDS;

    invoke-static {v0, p1}, LX/GDS;->a$redex0(LX/GDS;Ljava/lang/Throwable;)V

    .line 2329805
    iget-object v0, p0, LX/GDP;->a:LX/GKs;

    invoke-virtual {v0, p1}, LX/GKs;->a(Ljava/lang/Throwable;)V

    .line 2329806
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2329807
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2329808
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329809
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesReverseGeocodeQueryModels$AdInterfacesReverseGeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    const v3, 0x6158e9ce

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2329810
    :try_start_2
    iget-object v2, p0, LX/GDP;->a:LX/GKs;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/GKs;->a(Ljava/lang/String;)V

    .line 2329811
    :goto_1
    return-void

    .line 2329812
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 2329813
    :catch_0
    move-exception v0

    .line 2329814
    iget-object v1, p0, LX/GDP;->b:LX/GDS;

    invoke-static {v1, v0}, LX/GDS;->a$redex0(LX/GDS;Ljava/lang/Throwable;)V

    .line 2329815
    iget-object v1, p0, LX/GDP;->a:LX/GKs;

    invoke-virtual {v1, v0}, LX/GKs;->a(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
