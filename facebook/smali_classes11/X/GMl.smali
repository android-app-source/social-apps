.class public final LX/GMl;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:I

.field public final synthetic c:LX/GMm;


# direct methods
.method public constructor <init>(LX/GMm;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 2345222
    iput-object p1, p0, LX/GMl;->c:LX/GMm;

    iput-object p2, p0, LX/GMl;->a:Landroid/content/Context;

    iput p3, p0, LX/GMl;->b:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2345214
    iget-object v0, p0, LX/GMl;->c:LX/GMm;

    iget-object v1, p0, LX/GMl;->a:Landroid/content/Context;

    .line 2345215
    sget-object p0, LX/GMm;->c:LX/01T;

    sget-object p1, LX/01T;->FB4A:LX/01T;

    if-eq p0, p1, :cond_0

    .line 2345216
    :goto_0
    return-void

    .line 2345217
    :cond_0
    new-instance p0, Landroid/content/Intent;

    const-class p1, Lcom/facebook/adinterfaces/AdInterfacesTermsOfServiceActivity;

    invoke-direct {p0, v1, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2345218
    iget-object p1, v0, LX/GMm;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2345219
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2345220
    iget v0, p0, LX/GMl;->b:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2345221
    return-void
.end method
