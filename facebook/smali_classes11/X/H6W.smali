.class public final LX/H6W;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/1OZ;


# instance fields
.field public final A:Landroid/widget/TextView;

.field public final B:Landroid/widget/TextView;

.field public final C:Lcom/facebook/resources/ui/FbButton;

.field public D:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private E:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

.field public final synthetic l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

.field public m:LX/H83;

.field public n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public o:Landroid/widget/TextView;

.field public p:Lcom/facebook/offers/views/OfferExpirationTimerView;

.field private q:Landroid/view/View;

.field public r:Landroid/widget/LinearLayout;

.field private s:Landroid/widget/LinearLayout;

.field public t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public u:Landroid/widget/TextView;

.field public v:Landroid/widget/TextView;

.field public w:Lcom/facebook/resources/ui/FbButton;

.field public x:Landroid/widget/LinearLayout;

.field private final y:Landroid/widget/LinearLayout;

.field public z:Lcom/facebook/video/player/RichVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OffersWalletAdapter;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2426874
    iput-object p1, p0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    .line 2426875
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2426876
    const v0, 0x7f0d1e5b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/H6W;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2426877
    const v0, 0x7f0d1e5c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/H6W;->o:Landroid/widget/TextView;

    .line 2426878
    const v0, 0x7f0d1e5d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/views/OfferExpirationTimerView;

    iput-object v0, p0, LX/H6W;->p:Lcom/facebook/offers/views/OfferExpirationTimerView;

    .line 2426879
    const v0, 0x7f0d1e5e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/H6W;->q:Landroid/view/View;

    .line 2426880
    const v0, 0x7f0d1e5f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/H6W;->r:Landroid/widget/LinearLayout;

    .line 2426881
    iget-object v0, p1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030c57

    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/H6W;->s:Landroid/widget/LinearLayout;

    .line 2426882
    iget-object v0, p0, LX/H6W;->r:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/H6W;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2426883
    iget-object v0, p0, LX/H6W;->r:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1e62

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/H6W;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2426884
    iget-object v0, p0, LX/H6W;->r:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1e23

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/H6W;->u:Landroid/widget/TextView;

    .line 2426885
    iget-object v0, p0, LX/H6W;->r:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1e24

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/H6W;->v:Landroid/widget/TextView;

    .line 2426886
    iget-object v0, p0, LX/H6W;->r:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1e25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/H6W;->w:Lcom/facebook/resources/ui/FbButton;

    .line 2426887
    const v0, 0x7f0d1e60

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/H6W;->x:Landroid/widget/LinearLayout;

    .line 2426888
    iget-object v0, p1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030c58

    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/H6W;->y:Landroid/widget/LinearLayout;

    .line 2426889
    iget-object v0, p0, LX/H6W;->x:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/H6W;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2426890
    iget-object v0, p0, LX/H6W;->y:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1e23

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/H6W;->A:Landroid/widget/TextView;

    .line 2426891
    iget-object v0, p0, LX/H6W;->y:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1e24

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/H6W;->B:Landroid/widget/TextView;

    .line 2426892
    iget-object v0, p0, LX/H6W;->y:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1e25

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/H6W;->C:Lcom/facebook/resources/ui/FbButton;

    .line 2426893
    iget-object v0, p0, LX/H6W;->y:Landroid/widget/LinearLayout;

    const v1, 0x7f0d1e63

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, LX/H6W;->z:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2426894
    iget-object v0, p0, LX/H6W;->z:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/offers/fragment/OffersWalletAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, LX/H6T;->a(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/util/List;)V

    .line 2426895
    const v0, 0x7f0d1e61

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/H6W;->D:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2426896
    new-instance v0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    invoke-direct {v0, v1, v2, v4}, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;-><init>(Landroid/content/Context;LX/H6T;Z)V

    iput-object v0, p0, LX/H6W;->E:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    .line 2426897
    iget-object v0, p0, LX/H6W;->D:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/H6W;->E:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2426898
    new-instance v0, LX/1P0;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v4, v4}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    .line 2426899
    iget-object v1, p0, LX/H6W;->D:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2426900
    iget-object v0, p0, LX/H6W;->D:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 2426901
    iget-object v0, p0, LX/H6W;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426902
    iget-object v0, p0, LX/H6W;->A:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426903
    iget-object v0, p0, LX/H6W;->B:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426904
    iget-object v0, p0, LX/H6W;->p:Lcom/facebook/offers/views/OfferExpirationTimerView;

    invoke-virtual {v0, p0}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426905
    iget-object v0, p0, LX/H6W;->q:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2426906
    return-void
.end method

.method public static x(LX/H6W;)V
    .locals 2

    .prologue
    .line 2426907
    iget-object v0, p0, LX/H6W;->E:Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;

    iget-object v1, p0, LX/H6W;->m:LX/H83;

    .line 2426908
    invoke-virtual {v1}, LX/H83;->b()Ljava/util/List;

    move-result-object p0

    iput-object p0, v0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->e:Ljava/util/List;

    .line 2426909
    iput-object v1, v0, Lcom/facebook/offers/fragment/OfferMultiPhotoCarouselAdapter;->f:LX/H83;

    .line 2426910
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;Landroid/view/View;IJ)V
    .locals 0

    .prologue
    .line 2426911
    invoke-virtual {p0, p2}, LX/H6W;->onClick(Landroid/view/View;)V

    .line 2426912
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x560e69a8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2426913
    iget-object v1, p0, LX/H6W;->q:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 2426914
    const/4 v3, 0x0

    .line 2426915
    new-instance v4, LX/5OM;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v4, v1}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2426916
    const v1, 0x7f11001d

    invoke-virtual {v4, v1}, LX/5OM;->b(I)V

    .line 2426917
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    check-cast v1, LX/3Ai;

    .line 2426918
    iget-object v2, p0, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->q()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2426919
    iget-object v2, p0, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->q()Z

    move-result v2

    if-eqz v2, :cond_5

    const v2, 0x7f081cc5

    :goto_0
    invoke-virtual {v1, v2}, LX/3Ai;->setTitle(I)Landroid/view/MenuItem;

    .line 2426920
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    check-cast v1, LX/3Ai;

    .line 2426921
    iget-object v2, p0, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->r()Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v1, v2}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2426922
    iget-object v2, p0, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->r()Z

    move-result v2

    if-eqz v2, :cond_7

    const v2, 0x7f081cc6

    :goto_2
    invoke-virtual {v1, v2}, LX/3Ai;->setTitle(I)Landroid/view/MenuItem;

    .line 2426923
    new-instance v1, LX/H6V;

    invoke-direct {v1, p0}, LX/H6V;-><init>(LX/H6W;)V

    .line 2426924
    iput-object v1, v4, LX/5OM;->p:LX/5OO;

    .line 2426925
    invoke-virtual {v4, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2426926
    invoke-virtual {v4}, LX/0ht;->d()V

    .line 2426927
    :goto_3
    const v1, 0x593d99ab

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2426928
    :cond_0
    iget-object v1, p0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p0, LX/H6W;->m:LX/H83;

    const-string v3, "wallet"

    invoke-virtual {v1, v2, v3}, LX/H6T;->a(LX/H83;Ljava/lang/String;)V

    .line 2426929
    iget-object v1, p0, LX/H6W;->m:LX/H83;

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 2426930
    invoke-virtual {v1}, LX/H83;->w()LX/H72;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, LX/H83;->w()LX/H72;

    move-result-object v2

    invoke-interface {v2}, LX/H72;->I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v2

    if-nez v2, :cond_8

    :cond_1
    move-object v2, v3

    .line 2426931
    :goto_4
    move-object v1, v2

    .line 2426932
    iget-object v2, p0, LX/H6W;->m:LX/H83;

    invoke-virtual {v2}, LX/H83;->E()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_2

    .line 2426933
    iget-object v1, p0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    iget-object v3, p0, LX/H6W;->m:LX/H83;

    invoke-virtual {v3}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    iget-object v3, p0, LX/H6W;->m:LX/H83;

    invoke-virtual {v3}, LX/H83;->w()LX/H72;

    move-result-object v3

    invoke-interface {v3}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/H6W;->m:LX/H83;

    invoke-static {v4}, LX/H6T;->b(LX/H83;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, LX/H6T;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_3

    .line 2426934
    :cond_2
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2426935
    iget-object v2, p0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v3, p0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v3, v3, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    .line 2426936
    iget-object v4, v2, LX/H6T;->q:LX/17d;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, LX/17d;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v4

    .line 2426937
    if-nez v4, :cond_3

    .line 2426938
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2426939
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2426940
    :cond_3
    iget-object v5, v2, LX/H6T;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v5, v4, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2426941
    goto/16 :goto_3

    .line 2426942
    :cond_4
    iget-object v1, p0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v2, p0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    iget-object v3, p0, LX/H6W;->m:LX/H83;

    invoke-virtual {v1, v2, v3}, LX/H6T;->a(Landroid/content/Context;LX/H83;)V

    goto/16 :goto_3

    .line 2426943
    :cond_5
    const v2, 0x7f081cc4

    goto/16 :goto_0

    :cond_6
    move v2, v3

    .line 2426944
    goto/16 :goto_1

    .line 2426945
    :cond_7
    const v2, 0x7f081cc7

    goto/16 :goto_2

    .line 2426946
    :cond_8
    invoke-virtual {v1}, LX/H83;->w()LX/H72;

    move-result-object v2

    invoke-interface {v2}, LX/H72;->I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;->d()LX/0Px;

    move-result-object v4

    .line 2426947
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    .line 2426948
    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryAttachmentFieldsModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_9
    move-object v2, v3

    .line 2426949
    goto/16 :goto_4
.end method
