.class public final LX/GY7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/CommerceStoreCreateMutationModels$CommerceStoreCreateMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V
    .locals 0

    .prologue
    .line 2364784
    iput-object p1, p0, LX/GY7;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2364785
    sget-object v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->g:Ljava/lang/String;

    const-string v1, "create shop mutation failed"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2364786
    iget-object v0, p0, LX/GY7;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->r:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2364787
    iget-object v0, p0, LX/GY7;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814c5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2364788
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2364789
    iget-object v0, p0, LX/GY7;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->r:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2364790
    iget-object v0, p0, LX/GY7;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    const/4 v1, 0x1

    .line 2364791
    iput-boolean v1, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->k:Z

    .line 2364792
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2364793
    const-string v1, "extra_add_tab_type"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2364794
    iget-object v1, p0, LX/GY7;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2364795
    if-eqz v1, :cond_0

    .line 2364796
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2364797
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2364798
    :cond_0
    iget-object v0, p0, LX/GY7;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7j6;

    iget-object v1, p0, LX/GY7;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    iget-wide v2, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    invoke-virtual {v0, v2, v3}, LX/7j6;->a(J)V

    .line 2364799
    return-void
.end method
