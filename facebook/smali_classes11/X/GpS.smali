.class public LX/GpS;
.super LX/GpR;
.source ""


# instance fields
.field private final d:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;

.field public e:Z


# direct methods
.method public constructor <init>(LX/Gpv;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;)V
    .locals 0

    .prologue
    .line 2394964
    invoke-direct {p0, p1}, LX/GpR;-><init>(LX/Gpv;)V

    .line 2394965
    iput-object p2, p0, LX/GpS;->d:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;

    .line 2394966
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2394967
    invoke-virtual {p0}, LX/GpR;->b()V

    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2394968
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2394969
    check-cast v0, LX/Gpv;

    invoke-interface {v0}, LX/Gpv;->a()V

    .line 2394970
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2394971
    check-cast v0, LX/Gpz;

    .line 2394972
    iget-object v1, p0, LX/GpS;->d:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;

    invoke-interface {v1}, LX/CHb;->d()LX/8Z4;

    move-result-object v1

    invoke-interface {v1}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v1

    .line 2394973
    iget-object v2, v0, LX/Gpz;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2394974
    iget-boolean v1, p0, LX/GpS;->e:Z

    if-eqz v1, :cond_0

    .line 2394975
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2394976
    iget-object v2, v0, LX/Gpz;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2394977
    :cond_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2394978
    check-cast v0, LX/Gpv;

    iget-object v1, p0, LX/GpS;->d:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;

    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;->j()LX/CHX;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Gpv;->a(LX/CHX;)V

    .line 2394979
    iget-object v0, p0, LX/GpS;->d:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;

    invoke-interface {v0}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/GpS;->d:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;

    invoke-interface {v1}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/Go7;->a(Ljava/lang/String;Ljava/lang/String;)LX/GoE;

    move-result-object v1

    .line 2394980
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2394981
    check-cast v0, LX/Gpz;

    .line 2394982
    iput-object v1, v0, LX/Gpz;->d:LX/GoE;

    .line 2394983
    return-void
.end method
