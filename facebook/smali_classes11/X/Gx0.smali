.class public final LX/Gx0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Gwu;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/GxF;


# direct methods
.method public constructor <init>(LX/GxF;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2407646
    iput-object p1, p0, LX/Gx0;->b:LX/GxF;

    iput-object p2, p0, LX/Gx0;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Gwv;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2407647
    sget-object v0, LX/GxF;->z:Ljava/lang/Class;

    const-string v1, "Failed to load components store. Error: %s   Message: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, LX/Gwv;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2407648
    return-void
.end method

.method public final a(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;)V
    .locals 4

    .prologue
    .line 2407649
    :try_start_0
    iget-object v0, p0, LX/Gx0;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2407650
    iget-object v1, p0, LX/Gx0;->b:LX/GxF;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2407651
    :try_start_1
    iget-object v2, p0, LX/Gx0;->b:LX/GxF;

    iget-object v2, v2, LX/GxF;->B:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2407652
    iget-object v0, p0, LX/Gx0;->b:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->e()V

    .line 2407653
    monitor-exit v1

    .line 2407654
    :goto_0
    return-void

    .line 2407655
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2407656
    :catch_0
    move-exception v0

    .line 2407657
    iget-object v1, p0, LX/Gx0;->b:LX/GxF;

    iget-object v1, v1, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    sget-object v2, LX/GxF;->z:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "FacewebComponentsStore failed to deserialize skeleton."

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
