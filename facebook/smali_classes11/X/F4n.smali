.class public final enum LX/F4n;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F4n;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F4n;

.field public static final enum TASK_CREATE_GROUP:LX/F4n;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2197315
    new-instance v0, LX/F4n;

    const-string v1, "TASK_CREATE_GROUP"

    invoke-direct {v0, v1, v2}, LX/F4n;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F4n;->TASK_CREATE_GROUP:LX/F4n;

    .line 2197316
    const/4 v0, 0x1

    new-array v0, v0, [LX/F4n;

    sget-object v1, LX/F4n;->TASK_CREATE_GROUP:LX/F4n;

    aput-object v1, v0, v2

    sput-object v0, LX/F4n;->$VALUES:[LX/F4n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2197317
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F4n;
    .locals 1

    .prologue
    .line 2197318
    const-class v0, LX/F4n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F4n;

    return-object v0
.end method

.method public static values()[LX/F4n;
    .locals 1

    .prologue
    .line 2197314
    sget-object v0, LX/F4n;->$VALUES:[LX/F4n;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F4n;

    return-object v0
.end method
