.class public final LX/GI7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/GIA;


# direct methods
.method public constructor <init>(LX/GIA;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2335737
    iput-object p1, p0, LX/GI7;->b:LX/GIA;

    iput-object p2, p0, LX/GI7;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;)V
    .locals 4

    .prologue
    const v3, 0x5a0007

    const/4 v1, 0x0

    .line 2335738
    if-nez p1, :cond_0

    .line 2335739
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2335740
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    .line 2335741
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2335742
    new-instance v1, LX/GFO;

    invoke-direct {v1}, LX/GFO;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2335743
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x3

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2335744
    :goto_0
    return-void

    .line 2335745
    :cond_0
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v0, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2335746
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2335747
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2335748
    iget-object v2, p0, LX/GI7;->b:LX/GIA;

    iget-object v3, p0, LX/GI7;->b:LX/GIA;

    iget-boolean v3, v3, LX/GIA;->m:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 2335749
    :goto_1
    iput-boolean v0, v2, LX/GIA;->m:Z

    .line 2335750
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v1

    .line 2335751
    iput-object v1, v0, LX/GIA;->i:LX/0Px;

    .line 2335752
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v1, p0, LX/GI7;->b:LX/GIA;

    iget-object v1, v1, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    invoke-static {v1}, LX/A93;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)LX/A93;

    move-result-object v1

    .line 2335753
    iput-object p1, v1, LX/A93;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    .line 2335754
    move-object v1, v1

    .line 2335755
    invoke-virtual {v1}, LX/A93;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)V

    .line 2335756
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    invoke-static {v0}, LX/GIA;->b(LX/GIA;)V

    .line 2335757
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->i:LX/0Px;

    iget-object v1, p0, LX/GI7;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/GG6;->a(LX/0Px;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 2335758
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335759
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    move-object v0, v1

    .line 2335760
    iget-object v1, p0, LX/GI7;->b:LX/GIA;

    iget-object v1, v1, LX/GIA;->i:LX/0Px;

    iget-object v2, p0, LX/GI7;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/GG6;->a(LX/0Px;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->setSelected(I)V

    .line 2335761
    :cond_1
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    .line 2335762
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2335763
    new-instance v1, LX/GFj;

    iget-object v2, p0, LX/GI7;->b:LX/GIA;

    iget-object v2, v2, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/GI7;->b:LX/GIA;

    iget-object v3, v3, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/GFj;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 2335764
    goto :goto_1
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 3

    .prologue
    .line 2335765
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x5a0007

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2335766
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2335767
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x5a0007

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2335768
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2335769
    iget-object v0, p0, LX/GI7;->b:LX/GIA;

    .line 2335770
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2335771
    new-instance v1, LX/GFO;

    invoke-direct {v1, p1}, LX/GFO;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2335772
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2335773
    check-cast p1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    invoke-direct {p0, p1}, LX/GI7;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;)V

    return-void
.end method
