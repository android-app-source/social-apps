.class public LX/GiZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public mFbid:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mFileName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "file_name"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mFilePath:Ljava/lang/String;

.field public mHash:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "hash"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2386322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2386323
    iput-object v0, p0, LX/GiZ;->mFileName:Ljava/lang/String;

    .line 2386324
    iput-object v0, p0, LX/GiZ;->mFilePath:Ljava/lang/String;

    .line 2386325
    iput-object v0, p0, LX/GiZ;->mHash:Ljava/lang/String;

    .line 2386326
    iput-object v0, p0, LX/GiZ;->mFbid:Ljava/lang/String;

    .line 2386327
    return-void
.end method

.method public constructor <init>(LX/0lF;)V
    .locals 4

    .prologue
    .line 2386320
    const-string v0, "file_name"

    invoke-static {p1, v0}, LX/GiZ;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "hash"

    invoke-static {p1, v2}, LX/GiZ;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "id"

    invoke-static {p1, v3}, LX/GiZ;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, LX/GiZ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2386321
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2386328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2386329
    iput-object p1, p0, LX/GiZ;->mFileName:Ljava/lang/String;

    .line 2386330
    iput-object p2, p0, LX/GiZ;->mFilePath:Ljava/lang/String;

    .line 2386331
    iput-object p3, p0, LX/GiZ;->mHash:Ljava/lang/String;

    .line 2386332
    iput-object p4, p0, LX/GiZ;->mFbid:Ljava/lang/String;

    .line 2386333
    return-void
.end method

.method public static a(Landroid/util/JsonReader;)LX/GiZ;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 2386303
    new-instance v0, LX/GiZ;

    invoke-direct {v0}, LX/GiZ;-><init>()V

    .line 2386304
    :try_start_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 2386305
    :goto_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2386306
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 2386307
    const-string v2, "file_name"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2386308
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/GiZ;->mFileName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2386309
    :catch_0
    move-exception v0

    .line 2386310
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2386311
    :cond_0
    :try_start_1
    const-string v2, "hash"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2386312
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/GiZ;->mHash:Ljava/lang/String;

    goto :goto_0

    .line 2386313
    :cond_1
    const-string v2, "id"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2386314
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/GiZ;->mFbid:Ljava/lang/String;

    goto :goto_0

    .line 2386315
    :cond_2
    const-string v2, "file_path"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2386316
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/GiZ;->mFilePath:Ljava/lang/String;

    goto :goto_0

    .line 2386317
    :cond_3
    invoke-virtual {p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 2386318
    :cond_4
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2386319
    return-object v0
.end method

.method private static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2386300
    invoke-virtual {p0, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2386301
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2386302
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2386293
    instance-of v2, p1, LX/GiZ;

    if-nez v2, :cond_1

    .line 2386294
    :cond_0
    :goto_0
    return v0

    .line 2386295
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 2386296
    goto :goto_0

    .line 2386297
    :cond_2
    check-cast p1, LX/GiZ;

    .line 2386298
    iget-object v2, p0, LX/GiZ;->mFileName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, LX/GiZ;->mFileName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/GiZ;->mHash:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p1, LX/GiZ;->mHash:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2386299
    iget-object v2, p0, LX/GiZ;->mFileName:Ljava/lang/String;

    iget-object v3, p1, LX/GiZ;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/GiZ;->mHash:Ljava/lang/String;

    iget-object v3, p1, LX/GiZ;->mHash:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2386292
    iget-object v0, p0, LX/GiZ;->mHash:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/GiZ;->mHash:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
