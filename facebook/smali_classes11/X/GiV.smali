.class public LX/GiV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public mFbid:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mHash:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "hash"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public mLibs:Ljava/util/ArrayList;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "libs"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/GiZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0lF;)V
    .locals 4

    .prologue
    .line 2385963
    const-string v0, "hash"

    invoke-static {p1, v0}, LX/GiV;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "id"

    invoke-static {p1, v1}, LX/GiV;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, v0, v1, v2}, LX/GiV;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2385964
    const-string v0, "libs"

    invoke-virtual {p1, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2385965
    if-eqz v0, :cond_0

    .line 2385966
    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v1

    .line 2385967
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2385968
    iget-object v2, p0, LX/GiV;->mLibs:Ljava/util/ArrayList;

    new-instance v3, LX/GiZ;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-direct {v3, v0}, LX/GiZ;-><init>(LX/0lF;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2385969
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "LX/GiZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2385970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2385971
    iput-object p1, p0, LX/GiV;->mHash:Ljava/lang/String;

    .line 2385972
    iput-object p2, p0, LX/GiV;->mFbid:Ljava/lang/String;

    .line 2385973
    iput-object p3, p0, LX/GiV;->mLibs:Ljava/util/ArrayList;

    .line 2385974
    return-void
.end method

.method public static a(Landroid/util/JsonReader;)LX/GiV;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2385975
    new-instance v0, LX/GiV;

    invoke-direct {v0, v1, v1, v1}, LX/GiV;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2385976
    :try_start_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginObject()V

    .line 2385977
    :goto_0
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2385978
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 2385979
    const-string v2, "hash"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2385980
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/GiV;->mHash:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2385981
    :catch_0
    move-exception v0

    .line 2385982
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2385983
    :cond_0
    :try_start_1
    const-string v2, "id"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2385984
    invoke-virtual {p0}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/GiV;->mFbid:Ljava/lang/String;

    goto :goto_0

    .line 2385985
    :cond_1
    const-string v2, "libs"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v1

    sget-object v2, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-eq v1, v2, :cond_3

    .line 2385986
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2385987
    invoke-virtual {p0}, Landroid/util/JsonReader;->beginArray()V

    .line 2385988
    :goto_1
    invoke-virtual {p0}, Landroid/util/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2385989
    invoke-static {p0}, LX/GiZ;->a(Landroid/util/JsonReader;)LX/GiZ;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2385990
    :cond_2
    invoke-virtual {p0}, Landroid/util/JsonReader;->endArray()V

    .line 2385991
    move-object v1, v1

    .line 2385992
    iput-object v1, v0, LX/GiV;->mLibs:Ljava/util/ArrayList;

    goto :goto_0

    .line 2385993
    :cond_3
    invoke-virtual {p0}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 2385994
    :cond_4
    invoke-virtual {p0}, Landroid/util/JsonReader;->endObject()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2385995
    return-object v0
.end method

.method private static a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2385996
    invoke-virtual {p0, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2385997
    invoke-virtual {p0, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2385998
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2385999
    instance-of v1, p1, LX/GiV;

    if-nez v1, :cond_1

    .line 2386000
    :cond_0
    :goto_0
    return v0

    .line 2386001
    :cond_1
    if-ne p1, p0, :cond_2

    .line 2386002
    const/4 v0, 0x1

    goto :goto_0

    .line 2386003
    :cond_2
    check-cast p1, LX/GiV;

    .line 2386004
    iget-object v1, p0, LX/GiV;->mHash:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/GiV;->mHash:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2386005
    iget-object v0, p0, LX/GiV;->mHash:Ljava/lang/String;

    iget-object v1, p1, LX/GiV;->mHash:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2386006
    iget-object v0, p0, LX/GiV;->mHash:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/GiV;->mHash:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
