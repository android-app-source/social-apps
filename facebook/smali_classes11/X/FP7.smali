.class public LX/FP7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2236727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236728
    return-void
.end method

.method public static a(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FP4;
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 2236678
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d()LX/FP2;

    move-result-object v0

    .line 2236679
    sget-object v1, LX/FP5;->b:[I

    invoke-virtual {v0}, LX/FP2;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2236680
    sget-object v6, LX/FPn;->NONE:LX/FPn;

    move-object v3, v4

    move-object v2, v4

    .line 2236681
    :goto_0
    new-instance v0, LX/FP4;

    .line 2236682
    iget-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v1, v1

    .line 2236683
    iget-object v5, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-object v5, v5

    .line 2236684
    iget-object v7, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v7, v7

    .line 2236685
    iget-object v8, v7, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-object v7, v8

    .line 2236686
    const/4 v8, 0x0

    invoke-static {v6}, LX/FP7;->a(LX/FPn;)Z

    move-result v9

    .line 2236687
    iget-boolean v10, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->b:Z

    move v11, v10

    .line 2236688
    move-object v10, v4

    invoke-direct/range {v0 .. v11}, LX/FP4;-><init>(Landroid/location/Location;Landroid/location/Location;Ljava/lang/String;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;LX/FPn;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;FZLjava/lang/String;Z)V

    return-object v0

    .line 2236689
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v0, v0

    .line 2236690
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236691
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v2, v0

    .line 2236692
    sget-object v6, LX/FPn;->USER_CENTERED:LX/FPn;

    move-object v3, v4

    .line 2236693
    goto :goto_0

    .line 2236694
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2236695
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236696
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    move-object v3, v0

    .line 2236697
    sget-object v6, LX/FPn;->CITY:LX/FPn;

    move-object v2, v4

    .line 2236698
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FP4;
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2236699
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v10, v0

    .line 2236700
    invoke-static {v10}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236701
    iget-object v0, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-object v4, v0

    .line 2236702
    iget-object v0, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->c:Ljava/lang/String;

    move-object v3, v0

    .line 2236703
    if-eqz v4, :cond_4

    move v6, v1

    .line 2236704
    :goto_0
    if-eqz v3, :cond_5

    move v0, v1

    .line 2236705
    :goto_1
    iget-object v5, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b:Landroid/location/Location;

    move-object v5, v5

    .line 2236706
    if-nez v5, :cond_0

    if-nez v6, :cond_0

    if-eqz v0, :cond_6

    :cond_0
    move v5, v1

    :goto_2
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2236707
    iget-object v5, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b:Landroid/location/Location;

    move-object v5, v5

    .line 2236708
    if-eqz v5, :cond_1

    if-nez v0, :cond_7

    :cond_1
    move v5, v1

    :goto_3
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2236709
    if-eqz v0, :cond_2

    if-nez v6, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2236710
    new-instance v0, LX/FP4;

    .line 2236711
    iget-object v1, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a:Landroid/location/Location;

    move-object v1, v1

    .line 2236712
    iget-object v2, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b:Landroid/location/Location;

    move-object v2, v2

    .line 2236713
    iget-object v5, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->f:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-object v5, v5

    .line 2236714
    iget-object v6, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    move-object v6, v6

    .line 2236715
    iget-object v7, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v7, v7

    .line 2236716
    iget-object v8, v7, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-object v7, v8

    .line 2236717
    const/4 v8, 0x0

    .line 2236718
    iget-object v9, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    move-object v9, v9

    .line 2236719
    invoke-static {v9}, LX/FP7;->a(LX/FPn;)Z

    move-result v9

    .line 2236720
    iget-object v11, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j:Ljava/lang/String;

    move-object v10, v11

    .line 2236721
    iget-boolean v11, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->b:Z

    move v11, v11

    .line 2236722
    invoke-direct/range {v0 .. v11}, LX/FP4;-><init>(Landroid/location/Location;Landroid/location/Location;Ljava/lang/String;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;LX/FPn;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;FZLjava/lang/String;Z)V

    return-object v0

    :cond_4
    move v6, v2

    .line 2236723
    goto :goto_0

    :cond_5
    move v0, v2

    .line 2236724
    goto :goto_1

    :cond_6
    move v5, v2

    .line 2236725
    goto :goto_2

    :cond_7
    move v5, v2

    .line 2236726
    goto :goto_3
.end method

.method private static a(LX/FPn;)Z
    .locals 1

    .prologue
    .line 2236653
    sget-object v0, LX/FPn;->USER_CENTERED_MAP_REGION:LX/FPn;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/FPn;->CITY_MAP_REGION:LX/FPn;

    if-eq p0, v0, :cond_0

    sget-object v0, LX/FPn;->CITY:LX/FPn;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FP4;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 2236654
    sget-object v6, LX/FPn;->CITY_MAP_REGION:LX/FPn;

    .line 2236655
    new-instance v0, LX/FP4;

    .line 2236656
    iget-object v1, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v1, v1

    .line 2236657
    iget-object v3, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-object v5, v3

    .line 2236658
    iget-object v3, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v3, v3

    .line 2236659
    iget-object v4, v3, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-object v7, v4

    .line 2236660
    const/4 v8, 0x0

    invoke-static {v6}, LX/FP7;->a(LX/FPn;)Z

    move-result v9

    .line 2236661
    iget-boolean v3, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->b:Z

    move v11, v3

    .line 2236662
    move-object v3, v2

    move-object v4, v2

    move-object v10, v2

    invoke-direct/range {v0 .. v11}, LX/FP4;-><init>(Landroid/location/Location;Landroid/location/Location;Ljava/lang/String;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;LX/FPn;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;FZLjava/lang/String;Z)V

    return-object v0
.end method

.method public static b(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)LX/FP4;
    .locals 12

    .prologue
    .line 2236663
    iget-object v0, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v10, v0

    .line 2236664
    new-instance v0, LX/FP4;

    .line 2236665
    iget-object v1, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a:Landroid/location/Location;

    move-object v1, v1

    .line 2236666
    iget-object v2, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->b:Landroid/location/Location;

    move-object v2, v2

    .line 2236667
    iget-object v3, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2236668
    iget-object v4, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->g:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    move-object v4, v4

    .line 2236669
    iget-object v5, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->f:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-object v5, v5

    .line 2236670
    iget-object v6, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    move-object v6, v6

    .line 2236671
    iget-object v7, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    move-object v7, v7

    .line 2236672
    const/4 v8, 0x0

    .line 2236673
    iget-object v9, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    move-object v9, v9

    .line 2236674
    invoke-static {v9}, LX/FP7;->a(LX/FPn;)Z

    move-result v9

    .line 2236675
    iget-object v11, v10, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->j:Ljava/lang/String;

    move-object v10, v11

    .line 2236676
    iget-boolean v11, p0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->b:Z

    move v11, v11

    .line 2236677
    invoke-direct/range {v0 .. v11}, LX/FP4;-><init>(Landroid/location/Location;Landroid/location/Location;Ljava/lang/String;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;LX/FPn;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;FZLjava/lang/String;Z)V

    return-object v0
.end method
