.class public abstract LX/FF7;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2216871
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2216872
    return-void
.end method

.method public static a(LX/FFF;Landroid/view/ViewGroup;LX/FOK;)LX/FF7;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2216873
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2216874
    sget-object v1, LX/FF5;->a:[I

    invoke-virtual {p0}, LX/FFF;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2216875
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2216876
    :pswitch_0
    const v1, 0x7f030768

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2216877
    new-instance v0, LX/FF8;

    invoke-direct {v0, v1}, LX/FF8;-><init>(Landroid/view/View;)V

    .line 2216878
    :goto_0
    return-object v0

    .line 2216879
    :pswitch_1
    const v1, 0x7f030767

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2216880
    new-instance v0, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;

    invoke-direct {v0, v1, p2}, Lcom/facebook/messaging/games/list/GameListRowViewHolder$RowItemViewHolder;-><init>(Landroid/view/View;LX/FOK;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract a(ILX/FFD;LX/FF9;)V
.end method
