.class public final LX/Gwp;
.super Landroid/app/ProgressDialog;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V
    .locals 2

    .prologue
    .line 2407432
    iput-object p1, p0, LX/Gwp;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    .line 2407433
    invoke-direct {p0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 2407434
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Gwp;->requestWindowFeature(I)Z

    .line 2407435
    invoke-virtual {p1}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082a50

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Gwp;->setMessage(Ljava/lang/CharSequence;)V

    .line 2407436
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Gwp;->setCanceledOnTouchOutside(Z)V

    .line 2407437
    return-void
.end method


# virtual methods
.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2407438
    invoke-virtual {p0}, LX/Gwp;->dismiss()V

    .line 2407439
    iget-object v0, p0, LX/Gwp;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2407440
    iget-object v0, p0, LX/Gwp;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->p:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 2407441
    :goto_0
    return-void

    .line 2407442
    :cond_0
    iget-object v0, p0, LX/Gwp;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->finish()V

    goto :goto_0
.end method
