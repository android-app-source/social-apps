.class public LX/GZ3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0wM;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0wM;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2365901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2365902
    iput-object p1, p0, LX/GZ3;->a:LX/0wM;

    .line 2365903
    iput-object p2, p0, LX/GZ3;->b:Landroid/content/res/Resources;

    .line 2365904
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365905
    if-nez p0, :cond_0

    .line 2365906
    const-string v0, ""

    .line 2365907
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365908
    invoke-static {p0}, LX/GZ3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/GZ3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
