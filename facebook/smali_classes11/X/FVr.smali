.class public final LX/FVr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

.field public h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

.field public l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2251315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2251316
    return-void
.end method

.method public static a(LX/FVt;)LX/FVr;
    .locals 2

    .prologue
    .line 2251317
    new-instance v0, LX/FVr;

    invoke-direct {v0}, LX/FVr;-><init>()V

    .line 2251318
    iget-object v1, p0, LX/FVt;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2251319
    iput-object v1, v0, LX/FVr;->d:Ljava/lang/String;

    .line 2251320
    move-object v0, v0

    .line 2251321
    iget-object v1, p0, LX/FVt;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2251322
    iput-object v1, v0, LX/FVr;->b:Ljava/lang/String;

    .line 2251323
    move-object v0, v0

    .line 2251324
    iget-object v1, p0, LX/FVt;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2251325
    iput-object v1, v0, LX/FVr;->a:Ljava/lang/String;

    .line 2251326
    move-object v0, v0

    .line 2251327
    iget-object v1, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2251328
    iput-object v1, v0, LX/FVr;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 2251329
    move-object v0, v0

    .line 2251330
    iget-object v1, p0, LX/FVt;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    move-object v1, v1

    .line 2251331
    iput-object v1, v0, LX/FVr;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    .line 2251332
    move-object v0, v0

    .line 2251333
    iget-object v1, p0, LX/FVt;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2251334
    iput-object v1, v0, LX/FVr;->c:Ljava/lang/String;

    .line 2251335
    move-object v0, v0

    .line 2251336
    iget-object v1, p0, LX/FVt;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-object v1, v1

    .line 2251337
    iput-object v1, v0, LX/FVr;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    .line 2251338
    move-object v0, v0

    .line 2251339
    iget-object v1, p0, LX/FVt;->i:Ljava/lang/String;

    move-object v1, v1

    .line 2251340
    iput-object v1, v0, LX/FVr;->i:Ljava/lang/String;

    .line 2251341
    move-object v1, v0

    .line 2251342
    invoke-virtual {p0}, LX/FVt;->i()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2251343
    :goto_0
    iput-object v0, v1, LX/FVr;->f:Ljava/lang/String;

    .line 2251344
    move-object v0, v1

    .line 2251345
    iget-object v1, p0, LX/FVt;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2251346
    iput-object v1, v0, LX/FVr;->e:Ljava/lang/String;

    .line 2251347
    move-object v0, v0

    .line 2251348
    iget-boolean v1, p0, LX/FVt;->j:Z

    move v1, v1

    .line 2251349
    iput-boolean v1, v0, LX/FVr;->j:Z

    .line 2251350
    move-object v0, v0

    .line 2251351
    iget-object v1, p0, LX/FVt;->l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-object v1, v1

    .line 2251352
    iput-object v1, v0, LX/FVr;->l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    .line 2251353
    move-object v0, v0

    .line 2251354
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/FVt;->i()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/FVt;
    .locals 2

    .prologue
    .line 2251355
    new-instance v0, LX/FVt;

    invoke-direct {v0, p0}, LX/FVt;-><init>(LX/FVr;)V

    return-object v0
.end method
