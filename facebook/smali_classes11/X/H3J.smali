.class public LX/H3J;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;",
        "Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/0SG;

.field private final c:Landroid/content/res/Resources;

.field public final d:LX/0lC;

.field private final e:LX/0sO;


# direct methods
.method public constructor <init>(LX/0SG;Landroid/content/res/Resources;LX/0lC;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2420758
    invoke-direct {p0, p4}, LX/0ro;-><init>(LX/0sO;)V

    .line 2420759
    iput-object p1, p0, LX/H3J;->b:LX/0SG;

    .line 2420760
    iput-object p2, p0, LX/H3J;->c:Landroid/content/res/Resources;

    .line 2420761
    iput-object p3, p0, LX/H3J;->d:LX/0lC;

    .line 2420762
    iput-object p4, p0, LX/H3J;->e:LX/0sO;

    .line 2420763
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2420764
    check-cast p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2420765
    invoke-virtual {p3}, LX/15w;->J()LX/0lG;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2420766
    const-string v3, "result_sections"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 2420767
    const-string v4, "edges"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    .line 2420768
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 2420769
    const-string v4, "search_session_id"

    invoke-virtual {v0, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v4

    move v0, v1

    .line 2420770
    :goto_0
    invoke-virtual {v5}, LX/0lF;->e()I

    move-result v6

    if-ge v0, v6, :cond_7

    .line 2420771
    invoke-virtual {v5, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v6

    const-string v7, "node"

    invoke-virtual {v6, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 2420772
    const-string v7, "results"

    invoke-virtual {v6, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    const-string v8, "edges"

    invoke-virtual {v7, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 2420773
    const-string v8, "result_category"

    invoke-virtual {v6, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v6

    .line 2420774
    const-string v8, "PLACES"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2420775
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2420776
    invoke-virtual {v7}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2420777
    iget-object v8, p0, LX/H3J;->d:LX/0lC;

    const-string p3, "node"

    invoke-virtual {v0, p3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    const-class p3, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v8, v0, p3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2420778
    new-instance v8, Lcom/facebook/nearby/model/TypeaheadPlace;

    invoke-direct {v8, v0}, Lcom/facebook/nearby/model/TypeaheadPlace;-><init>(Lcom/facebook/graphql/model/GraphQLNode;)V

    invoke-virtual {v5, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2420779
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v7, v0

    .line 2420780
    :goto_2
    invoke-virtual {v3}, LX/0lF;->e()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 2420781
    invoke-virtual {v3, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v0

    .line 2420782
    const-string v5, "result_category"

    invoke-virtual {v0, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    .line 2420783
    const-string v6, "TOPICS_AND_REGION"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2420784
    const-string v1, "topic_suggestions"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, "nodes"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2420785
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2420786
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 2420787
    iget-object v5, p0, LX/H3J;->d:LX/0lC;

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v6, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    invoke-virtual {v5, v1, v6}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;

    .line 2420788
    new-instance v5, Lcom/facebook/nearby/common/SearchSuggestion;

    invoke-direct {v5, v1}, Lcom/facebook/nearby/common/SearchSuggestion;-><init>(Lcom/facebook/graphql/model/GraphQLNearbySearchSuggestion;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2420789
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v6, v1

    .line 2420790
    :goto_4
    if-nez v6, :cond_4

    .line 2420791
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "missing TOPICS_AND_REGION node in response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2420792
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 2420793
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2420794
    :cond_4
    if-nez v7, :cond_5

    .line 2420795
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "missing PLACES node in response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v2

    invoke-virtual {v2}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2420796
    :cond_5
    new-instance v0, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v2, p0, LX/H3J;->b:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v5, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->b:Ljava/lang/String;

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/nearby/protocol/SearchNearbyPlacesResult;-><init>(LX/0ta;JLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    return-object v0

    :cond_6
    move-object v6, v2

    goto :goto_4

    :cond_7
    move-object v7, v2

    goto/16 :goto_2
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2420797
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 8

    .prologue
    .line 2420798
    check-cast p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;

    .line 2420799
    iget-object v0, p0, LX/H3J;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b14f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2420800
    new-instance v1, LX/H32;

    invoke-direct {v1}, LX/H32;-><init>()V

    move-object v1, v1

    .line 2420801
    const-string v2, "profile_pic_size"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "search_latitude"

    .line 2420802
    iget-object v6, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->a:Lcom/facebook/nearby/protocol/SearchArea;

    iget-object v6, v6, Lcom/facebook/nearby/protocol/SearchArea;->a:Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    move-wide v4, v6

    .line 2420803
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "search_longitude"

    .line 2420804
    iget-object v6, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->a:Lcom/facebook/nearby/protocol/SearchArea;

    iget-object v6, v6, Lcom/facebook/nearby/protocol/SearchArea;->a:Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    move-wide v4, v6

    .line 2420805
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "query"

    .line 2420806
    iget-object v3, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2420807
    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\\\\"

    const-string v6, "\\\\\\\\"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\\("

    const-string v6, "\\\\("

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    const-string v6, "\\\\,"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\\."

    const-string v6, "\\\\."

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\\)"

    const-string v6, "\\\\)"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\\<"

    const-string v6, "\\\\<"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\\>"

    const-string v6, "\\\\>"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2420808
    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "result_categories"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "TOPICS_AND_REGION"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "PLACES"

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v0

    const-string v2, "user_altitude"

    iget v3, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->d:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "user_altitude_accuracy"

    iget v3, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->e:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "user_heading"

    iget v3, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->f:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "user_heading_accuracy"

    iget v3, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->g:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "user_speed"

    iget v3, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->h:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2420809
    iget-object v0, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->a:Lcom/facebook/nearby/protocol/SearchArea;

    iget v0, v0, Lcom/facebook/nearby/protocol/SearchArea;->b:I

    move v0, v0

    .line 2420810
    int-to-long v2, v0

    .line 2420811
    iget-object v0, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->c:Landroid/location/Location;

    move-object v0, v0

    .line 2420812
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 2420813
    const-string v4, "search_distance"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2420814
    :cond_0
    if-eqz v0, :cond_1

    .line 2420815
    const-string v2, "user_location_latitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "user_location_longitude"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "user_location_accuracy"

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "user_location_stale_time"

    iget v3, p1, Lcom/facebook/nearby/protocol/SearchNearbyPlacesParams;->i:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2420816
    :cond_1
    return-object v1
.end method
