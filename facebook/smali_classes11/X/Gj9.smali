.class public final LX/Gj9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;)V
    .locals 0

    .prologue
    .line 2387124
    iput-object p1, p0, LX/Gj9;->a:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2387126
    iget-object v1, p0, LX/Gj9;->a:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    .line 2387127
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2387128
    check-cast v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2387129
    iput-object v0, v1, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->d:Ljava/lang/String;

    .line 2387130
    iget-object v1, p0, LX/Gj9;->a:Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;

    .line 2387131
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2387132
    check-cast v0, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/create/FetchThemePreviewGraphQLModels$FetchThemePreviewQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;->setupCells(Lcom/facebook/greetingcards/create/GreetingCardThemeGrid;LX/2uF;)V

    .line 2387133
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2387134
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Error loading themes"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2387135
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2387125
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Gj9;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
