.class public final LX/GQw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final isImplicit:Z

.field private final jsonString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2350920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2350921
    iput-object p1, p0, LX/GQw;->jsonString:Ljava/lang/String;

    .line 2350922
    iput-boolean p2, p0, LX/GQw;->isImplicit:Z

    .line 2350923
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2350924
    new-instance v0, LX/GQx;

    iget-object v1, p0, LX/GQw;->jsonString:Ljava/lang/String;

    iget-boolean v2, p0, LX/GQw;->isImplicit:Z

    invoke-direct {v0, v1, v2}, LX/GQx;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method
