.class public LX/GXN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GXN;


# instance fields
.field public final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2364089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2364090
    iput-object p1, p0, LX/GXN;->a:Landroid/content/res/Resources;

    .line 2364091
    return-void
.end method

.method public static a(LX/0QB;)LX/GXN;
    .locals 4

    .prologue
    .line 2364076
    sget-object v0, LX/GXN;->b:LX/GXN;

    if-nez v0, :cond_1

    .line 2364077
    const-class v1, LX/GXN;

    monitor-enter v1

    .line 2364078
    :try_start_0
    sget-object v0, LX/GXN;->b:LX/GXN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2364079
    if-eqz v2, :cond_0

    .line 2364080
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2364081
    new-instance p0, LX/GXN;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/GXN;-><init>(Landroid/content/res/Resources;)V

    .line 2364082
    move-object v0, p0

    .line 2364083
    sput-object v0, LX/GXN;->b:LX/GXN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2364084
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2364085
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2364086
    :cond_1
    sget-object v0, LX/GXN;->b:LX/GXN;

    return-object v0

    .line 2364087
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2364088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;Z)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;
    .locals 10

    .prologue
    .line 2364041
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;->a()I

    move-result v0

    .line 2364042
    :goto_0
    new-instance v1, LX/GWK;

    invoke-direct {v1}, LX/GWK;-><init>()V

    .line 2364043
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->b()Z

    move-result v2

    iput-boolean v2, v1, LX/GWK;->a:Z

    .line 2364044
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->c()Z

    move-result v2

    iput-boolean v2, v1, LX/GWK;->b:Z

    .line 2364045
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->d()Z

    move-result v2

    iput-boolean v2, v1, LX/GWK;->c:Z

    .line 2364046
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->e()Z

    move-result v2

    iput-boolean v2, v1, LX/GWK;->d:Z

    .line 2364047
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mz_()Z

    move-result v2

    iput-boolean v2, v1, LX/GWK;->e:Z

    .line 2364048
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mA_()Z

    move-result v2

    iput-boolean v2, v1, LX/GWK;->f:Z

    .line 2364049
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/GWK;->g:Ljava/lang/String;

    .line 2364050
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/GWK;->h:Ljava/lang/String;

    .line 2364051
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v2

    iput-object v2, v1, LX/GWK;->i:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    .line 2364052
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    iput-object v2, v1, LX/GWK;->j:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    .line 2364053
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/GWK;->k:Ljava/lang/String;

    .line 2364054
    move-object v2, v1

    .line 2364055
    new-instance v3, LX/GWL;

    invoke-direct {v3}, LX/GWL;-><init>()V

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_1
    add-int/2addr v0, v1

    .line 2364056
    iput v0, v3, LX/GWL;->a:I

    .line 2364057
    move-object v0, v3

    .line 2364058
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 2364059
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2364060
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 2364061
    iget v5, v0, LX/GWL;->a:I

    invoke-virtual {v4, v7, v5, v7}, LX/186;->a(III)V

    .line 2364062
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2364063
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2364064
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2364065
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2364066
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2364067
    new-instance v5, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    invoke-direct {v5, v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;-><init>(LX/15i;)V

    .line 2364068
    move-object v0, v5

    .line 2364069
    iput-object v0, v2, LX/GWK;->i:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    .line 2364070
    move-object v0, v2

    .line 2364071
    iput-boolean p1, v0, LX/GWK;->f:Z

    .line 2364072
    move-object v0, v0

    .line 2364073
    invoke-virtual {v0}, LX/GWK;->a()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    move-result-object v0

    return-object v0

    .line 2364074
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2364075
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method
