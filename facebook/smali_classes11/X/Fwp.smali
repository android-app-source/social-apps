.class public final LX/Fwp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<T",
        "LoadedResultType;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)V
    .locals 0

    .prologue
    .line 2303785
    iput-object p1, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2303786
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 2303787
    iget-object v0, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    iget-object v0, v0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "load failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2303788
    :cond_0
    iget-object v0, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    iget-object v1, v0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->s:Lcom/facebook/timeline/header/intro/edit/TimelineLoadingFragment;

    iget-object v0, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    iget-object v0, v0, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    .line 2303789
    if-eqz v0, :cond_1

    const v2, 0x7f080039

    .line 2303790
    :goto_0
    invoke-static {}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->newBuilder()LX/62Q;

    move-result-object p0

    sget-object p1, LX/1lD;->ERROR:LX/1lD;

    .line 2303791
    iput-object p1, p0, LX/62Q;->a:LX/1lD;

    .line 2303792
    move-object p0, p0

    .line 2303793
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2303794
    iput-object v2, p0, LX/62Q;->b:Ljava/lang/String;

    .line 2303795
    move-object v2, p0

    .line 2303796
    invoke-virtual {v2}, LX/62Q;->a()Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-result-object p0

    .line 2303797
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2303798
    check-cast v2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 p1, 0x0

    invoke-virtual {v2, p0, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    .line 2303799
    return-void

    .line 2303800
    :cond_1
    const v2, 0x7f08003f

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "LoadedResultType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2303801
    iget-object v0, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    invoke-static {v0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->u(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;)Landroid/os/Bundle;

    move-result-object v0

    .line 2303802
    iget-object v1, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2303803
    iget-object v1, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    invoke-virtual {v1, v0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->d(Landroid/os/Bundle;)V

    .line 2303804
    :cond_0
    iget-object v1, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->a(Ljava/lang/Object;Landroid/os/Bundle;)V

    .line 2303805
    iget-object v1, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->a()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2303806
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2303807
    iget-object v0, p0, LX/Fwp;->a:Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;

    invoke-static {v0, v1}, Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;->b(Lcom/facebook/timeline/header/intro/edit/IntroCardEditActivity;Landroid/support/v4/app/Fragment;)V

    .line 2303808
    return-void
.end method
