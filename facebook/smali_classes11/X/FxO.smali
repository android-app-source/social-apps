.class public final LX/FxO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/TimelineCommonGraphQLModels$TimelinePhotoUriQueryModel;",
        ">;",
        "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/FxP;


# direct methods
.method public constructor <init>(LX/FxP;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2304858
    iput-object p1, p0, LX/FxO;->b:LX/FxP;

    iput-object p2, p0, LX/FxO;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2304859
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2304860
    if-eqz p1, :cond_0

    .line 2304861
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2304862
    if-eqz v0, :cond_0

    .line 2304863
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2304864
    check-cast v0, Lcom/facebook/timeline/protocol/TimelineCommonGraphQLModels$TimelinePhotoUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineCommonGraphQLModels$TimelinePhotoUriQueryModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2304865
    :cond_0
    const/4 v0, 0x0

    .line 2304866
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LX/FxO;->a:Ljava/lang/String;

    .line 2304867
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2304868
    check-cast v0, Lcom/facebook/timeline/protocol/TimelineCommonGraphQLModels$TimelinePhotoUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineCommonGraphQLModels$TimelinePhotoUriQueryModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageUriFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/Fx4;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-result-object v0

    goto :goto_0
.end method
