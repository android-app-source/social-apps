.class public final LX/F6H;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/react/GroupsReactDataFetcher$1;)V
    .locals 0

    .prologue
    .line 2200321
    iput-object p1, p0, LX/F6H;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2200322
    iget-object v0, p0, LX/F6H;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    iget-object v0, v0, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;->c:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2200323
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2200324
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2200325
    if-nez p1, :cond_0

    .line 2200326
    iget-object v0, p0, LX/F6H;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    iget-object v0, v0, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;->c:LX/0TF;

    invoke-interface {v0, v1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2200327
    :goto_0
    return-void

    .line 2200328
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2200329
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2200330
    if-nez v0, :cond_1

    .line 2200331
    iget-object v0, p0, LX/F6H;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    iget-object v0, v0, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;->c:LX/0TF;

    invoke-interface {v0, v1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2200332
    :cond_1
    iget-object v1, p0, LX/F6H;->a:Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    iget-object v1, v1, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;->c:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
