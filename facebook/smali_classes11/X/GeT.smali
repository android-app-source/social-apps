.class public final LX/GeT;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/GeU;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;

.field public b:LX/5KI;

.field public final synthetic c:LX/GeU;


# direct methods
.method public constructor <init>(LX/GeU;)V
    .locals 1

    .prologue
    .line 2375669
    iput-object p1, p0, LX/GeT;->c:LX/GeU;

    .line 2375670
    move-object v0, p1

    .line 2375671
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2375672
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2375673
    const-string v0, "CSFBFeedExplanationContent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2375674
    if-ne p0, p1, :cond_1

    .line 2375675
    :cond_0
    :goto_0
    return v0

    .line 2375676
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2375677
    goto :goto_0

    .line 2375678
    :cond_3
    check-cast p1, LX/GeT;

    .line 2375679
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2375680
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2375681
    if-eq v2, v3, :cond_0

    .line 2375682
    iget-object v2, p0, LX/GeT;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/GeT;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/GeT;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2375683
    goto :goto_0

    .line 2375684
    :cond_5
    iget-object v2, p1, LX/GeT;->a:Lcom/facebook/java2js/JSValue;

    if-nez v2, :cond_4

    .line 2375685
    :cond_6
    iget-object v2, p0, LX/GeT;->b:LX/5KI;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/GeT;->b:LX/5KI;

    iget-object v3, p1, LX/GeT;->b:LX/5KI;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2375686
    goto :goto_0

    .line 2375687
    :cond_7
    iget-object v2, p1, LX/GeT;->b:LX/5KI;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
