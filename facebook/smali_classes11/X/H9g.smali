.class public LX/H9g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H89;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Landroid/app/Activity;

.field public final j:Landroid/view/View;

.field public k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2434535
    const v0, 0x7f0209ae

    sput v0, LX/H9g;->a:I

    .line 2434536
    const v0, 0x7f081794

    sput v0, LX/H9g;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p7    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/H89;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434538
    iput-object p1, p0, LX/H9g;->c:LX/0Ot;

    .line 2434539
    iput-object p2, p0, LX/H9g;->d:LX/0Ot;

    .line 2434540
    iput-object p3, p0, LX/H9g;->e:LX/0Ot;

    .line 2434541
    iput-object p4, p0, LX/H9g;->f:LX/0Ot;

    .line 2434542
    iput-object p5, p0, LX/H9g;->g:LX/0Ot;

    .line 2434543
    iput-object p6, p0, LX/H9g;->h:LX/0Ot;

    .line 2434544
    check-cast p8, Landroid/app/Activity;

    iput-object p8, p0, LX/H9g;->i:Landroid/app/Activity;

    .line 2434545
    iput-object p7, p0, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434546
    iput-object p9, p0, LX/H9g;->j:Landroid/view/View;

    .line 2434547
    return-void
.end method

.method public static a$redex0(LX/H9g;LX/8Dz;)V
    .locals 4

    .prologue
    .line 2434514
    iget-object v0, p0, LX/H9g;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    sget-object v1, LX/8Dp;->REPORT_PLACE:LX/8Dp;

    new-instance v2, LX/H9e;

    invoke-direct {v2, p0, p1}, LX/H9e;-><init>(LX/H9g;LX/8Dz;)V

    new-instance v3, LX/H9f;

    invoke-direct {v3, p0}, LX/H9f;-><init>(LX/H9g;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2434515
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2434534
    new-instance v0, LX/HA7;

    sget v2, LX/H9g;->b:I

    sget v3, LX/H9g;->a:I

    iget-object v5, p0, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/8A4;->a(LX/0Px;)Z

    move-result v5

    if-nez v5, :cond_0

    move v5, v4

    :goto_0
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    move v5, v1

    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434533
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9g;->b:I

    sget v3, LX/H9g;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 8

    .prologue
    .line 2434517
    iget-object v0, p0, LX/H9g;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XI;->EVENT_TAPPED_REPORT:LX/9XI;

    iget-object v2, p0, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434518
    iget-object v0, p0, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2434519
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2434520
    new-instance v0, LX/6WS;

    iget-object v1, p0, LX/H9g;->i:Landroid/app/Activity;

    invoke-direct {v0, v1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2434521
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 2434522
    iget-object v2, p0, LX/H9g;->i:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081795

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v4, v2}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    .line 2434523
    iget-object v2, p0, LX/H9g;->i:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081796

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v5, v2}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    .line 2434524
    iget-object v2, p0, LX/H9g;->i:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08179d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v6, v2}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    .line 2434525
    iget-object v2, p0, LX/H9g;->i:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081798

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v7, v2}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    .line 2434526
    new-instance v2, LX/H9d;

    invoke-direct {v2, p0}, LX/H9d;-><init>(LX/H9g;)V

    invoke-virtual {v1, v2}, LX/5OG;->a(LX/5OE;)V

    .line 2434527
    iget-object v1, p0, LX/H9g;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2434528
    :goto_0
    return-void

    .line 2434529
    :cond_0
    const-string v0, "/report/id/?fbid=%s"

    iget-object v1, p0, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2434530
    sget-object v1, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2434531
    iget-object v0, p0, LX/H9g;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/H9g;->i:Landroid/app/Activity;

    invoke-interface {v0, v2, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2434532
    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434516
    const/4 v0, 0x0

    return-object v0
.end method
