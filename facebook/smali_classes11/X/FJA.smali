.class public final LX/FJA;
.super LX/0wa;
.source ""


# instance fields
.field public final synthetic a:LX/FJB;


# direct methods
.method public constructor <init>(LX/FJB;)V
    .locals 0

    .prologue
    .line 2223153
    iput-object p1, p0, LX/FJA;->a:LX/FJB;

    invoke-direct {p0}, LX/0wa;-><init>()V

    return-void
.end method

.method private b(J)D
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    .line 2223146
    long-to-double v2, p1

    const-wide v4, 0x412e848000000000L    # 1000000.0

    div-double/2addr v2, v4

    .line 2223147
    iget-object v4, p0, LX/FJA;->a:LX/FJB;

    iget-wide v4, v4, LX/FJB;->j:D

    cmpl-double v4, v4, v0

    if-lez v4, :cond_0

    iget-object v0, p0, LX/FJA;->a:LX/FJB;

    iget-wide v0, v0, LX/FJB;->j:D

    sub-double v0, v2, v0

    .line 2223148
    :cond_0
    iget-object v4, p0, LX/FJA;->a:LX/FJB;

    .line 2223149
    iput-wide v2, v4, LX/FJB;->j:D

    .line 2223150
    iget-object v2, p0, LX/FJA;->a:LX/FJB;

    .line 2223151
    iget-wide v7, v2, LX/FJB;->i:D

    add-double/2addr v7, v0

    iput-wide v7, v2, LX/FJB;->i:D

    .line 2223152
    return-wide v0
.end method


# virtual methods
.method public final a(J)V
    .locals 9

    .prologue
    .line 2223123
    iget-object v0, p0, LX/FJA;->a:LX/FJB;

    iget-boolean v0, v0, LX/FJB;->h:Z

    if-eqz v0, :cond_0

    .line 2223124
    iget-object v0, p0, LX/FJA;->a:LX/FJB;

    iget-object v0, v0, LX/FJB;->c:LX/0wY;

    iget-object v1, p0, LX/FJA;->a:LX/FJB;

    iget-object v1, v1, LX/FJB;->d:LX/FJA;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 2223125
    :cond_0
    iget-object v0, p0, LX/FJA;->a:LX/FJB;

    iget-object v0, v0, LX/FJB;->m:LX/FJ6;

    if-nez v0, :cond_2

    .line 2223126
    :cond_1
    :goto_0
    return-void

    .line 2223127
    :cond_2
    invoke-direct {p0, p1, p2}, LX/FJA;->b(J)D

    move-result-wide v0

    .line 2223128
    iget-object v2, p0, LX/FJA;->a:LX/FJB;

    iget-object v2, v2, LX/FJB;->m:LX/FJ6;

    invoke-virtual {v2, v0, v1}, LX/FJ6;->a(D)V

    .line 2223129
    sget-object v2, LX/FJ9;->a:[I

    iget-object v3, p0, LX/FJA;->a:LX/FJB;

    iget-object v3, v3, LX/FJB;->m:LX/FJ6;

    invoke-virtual {v3}, LX/FJ6;->b()LX/FJ8;

    move-result-object v3

    invoke-virtual {v3}, LX/FJ8;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2223130
    :pswitch_0
    iget-object v0, p0, LX/FJA;->a:LX/FJB;

    invoke-static {v0}, LX/FJB;->o(LX/FJB;)V

    .line 2223131
    iget-object v0, p0, LX/FJA;->a:LX/FJB;

    iget-object v0, v0, LX/FJB;->n:LX/FJ6;

    invoke-virtual {v0}, LX/FJ6;->b()LX/FJ8;

    move-result-object v0

    sget-object v1, LX/FJ8;->NOT_READY:LX/FJ8;

    if-eq v0, v1, :cond_1

    .line 2223132
    iget-object v0, p0, LX/FJA;->a:LX/FJB;

    iget-object v0, v0, LX/FJB;->m:LX/FJ6;

    .line 2223133
    iget-wide v5, v0, LX/FJ6;->j:D

    const-wide/16 v7, 0x0

    cmpl-double v5, v5, v7

    if-lez v5, :cond_3

    .line 2223134
    :goto_1
    goto :goto_0

    .line 2223135
    :pswitch_1
    iget-object v2, p0, LX/FJA;->a:LX/FJB;

    invoke-static {v2}, LX/FJB;->o(LX/FJB;)V

    .line 2223136
    iget-object v2, p0, LX/FJA;->a:LX/FJB;

    iget-object v2, v2, LX/FJB;->n:LX/FJ6;

    invoke-virtual {v2, v0, v1}, LX/FJ6;->a(D)V

    goto :goto_0

    .line 2223137
    :pswitch_2
    iget-object v2, p0, LX/FJA;->a:LX/FJB;

    invoke-static {v2}, LX/FJB;->o(LX/FJB;)V

    .line 2223138
    iget-object v2, p0, LX/FJA;->a:LX/FJB;

    iget-object v2, v2, LX/FJB;->n:LX/FJ6;

    invoke-virtual {v2, v0, v1}, LX/FJ6;->a(D)V

    .line 2223139
    iget-object v0, p0, LX/FJA;->a:LX/FJB;

    .line 2223140
    iget-object v1, v0, LX/FJB;->n:LX/FJ6;

    iput-object v1, v0, LX/FJB;->m:LX/FJ6;

    .line 2223141
    const/4 v1, 0x0

    iput-object v1, v0, LX/FJB;->n:LX/FJ6;

    .line 2223142
    invoke-static {v0}, LX/FJB;->h(LX/FJB;)I

    move-result v1

    iput v1, v0, LX/FJB;->k:I

    .line 2223143
    invoke-static {v0}, LX/FJB;->k(LX/FJB;)I

    move-result v1

    iput v1, v0, LX/FJB;->l:I

    .line 2223144
    goto :goto_0

    .line 2223145
    :cond_3
    iget-wide v5, v0, LX/FJ6;->i:D

    const-wide v7, 0x40b7700000000000L    # 6000.0

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->max(DD)D

    move-result-wide v5

    iput-wide v5, v0, LX/FJ6;->j:D

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
