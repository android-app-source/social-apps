.class public final LX/GB3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/growth/model/DeviceOwnerData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2EH;


# direct methods
.method public constructor <init>(LX/2EH;)V
    .locals 0

    .prologue
    .line 2326325
    iput-object p1, p0, LX/GB3;->a:LX/2EH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2326326
    sget-object v0, LX/2EH;->a:Ljava/lang/Class;

    const-string v1, "Account Recovery Parallel Search Device Data fetch failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2326327
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2326328
    check-cast p1, Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2326329
    if-nez p1, :cond_0

    .line 2326330
    :goto_0
    return-void

    .line 2326331
    :cond_0
    iget-object v0, p0, LX/GB3;->a:LX/2EH;

    .line 2326332
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2326333
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2326334
    const-string v1, "email"

    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2326335
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2326336
    const-string v1, "phone"

    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2326337
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->b()LX/0Px;

    move-result-object v1

    .line 2326338
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2326339
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/model/FullName;

    .line 2326340
    invoke-virtual {v1}, Lcom/facebook/growth/model/FullName;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2326341
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2326342
    const-string v1, "name"

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2326343
    :cond_4
    const/4 v1, 0x0

    .line 2326344
    :try_start_0
    iget-object v3, v0, LX/2EH;->e:LX/0lB;

    invoke-virtual {v3, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2326345
    :goto_2
    move-object v0, v1

    .line 2326346
    iget-object v1, p0, LX/GB3;->a:LX/2EH;

    iget-object v1, v1, LX/2EH;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/GAw;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0

    .line 2326347
    :catch_0
    move-exception v2

    .line 2326348
    sget-object v3, LX/2EH;->a:Ljava/lang/Class;

    const-string v4, "Account Recovery Parallel Search Device Data JSON Encode failed"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
