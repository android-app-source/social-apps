.class public final LX/FRj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wC;


# instance fields
.field private final a:LX/6zq;

.field private final b:LX/725;

.field private final c:LX/712;


# direct methods
.method public constructor <init>(LX/6zq;LX/725;LX/712;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240691
    iput-object p1, p0, LX/FRj;->a:LX/6zq;

    .line 2240692
    iput-object p2, p0, LX/FRj;->b:LX/725;

    .line 2240693
    iput-object p3, p0, LX/FRj;->c:LX/712;

    .line 2240694
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2240695
    sget-object v0, LX/FRi;->a:[I

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v1

    invoke-virtual {v1}, LX/71I;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2240696
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal row type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/6vm;->a()LX/71I;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2240697
    :pswitch_0
    iget-object v0, p0, LX/FRj;->c:LX/712;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/712;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2240698
    :goto_0
    return-object v0

    .line 2240699
    :pswitch_1
    iget-object v0, p0, LX/FRj;->a:LX/6zq;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/6zq;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2240700
    :pswitch_2
    if-nez p3, :cond_0

    new-instance p3, LX/FR3;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/FR3;-><init>(Landroid/content/Context;)V

    :goto_1
    move-object v0, p3

    .line 2240701
    goto :goto_0

    .line 2240702
    :pswitch_3
    check-cast p2, LX/FRD;

    .line 2240703
    if-nez p3, :cond_1

    new-instance p3, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;-><init>(Landroid/content/Context;)V

    .line 2240704
    :goto_2
    invoke-virtual {p3, p2}, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;->a(LX/FRD;)V

    .line 2240705
    iput-object p1, p3, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 2240706
    move-object v0, p3

    .line 2240707
    goto :goto_0

    .line 2240708
    :pswitch_4
    check-cast p2, LX/FRp;

    .line 2240709
    if-nez p3, :cond_2

    new-instance p3, LX/FRT;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/FRT;-><init>(Landroid/content/Context;)V

    .line 2240710
    :goto_3
    iput-object p1, p3, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 2240711
    iget-object v0, p2, LX/FRp;->a:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v0, v0

    .line 2240712
    iput-object v0, p3, LX/FRT;->b:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2240713
    invoke-static {p3}, LX/FRT;->c(LX/FRT;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2240714
    iget-object v0, p3, LX/FRT;->c:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const p2, 0x7f081e17

    invoke-virtual {v0, p2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(I)V

    .line 2240715
    :goto_4
    move-object v0, p3

    .line 2240716
    goto :goto_0

    .line 2240717
    :pswitch_5
    iget-object v0, p0, LX/FRj;->b:LX/725;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/725;->a(LX/6qh;LX/6vm;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2240718
    :pswitch_6
    check-cast p2, LX/FRr;

    .line 2240719
    if-nez p3, :cond_4

    new-instance p3, LX/FRU;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p3, v0}, LX/FRU;-><init>(Landroid/content/Context;)V

    .line 2240720
    :goto_5
    iput-object p1, p3, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a:LX/6qh;

    .line 2240721
    const/4 p4, 0x0

    .line 2240722
    iput-object p2, p3, LX/FRU;->b:LX/FRr;

    .line 2240723
    iget-object v0, p3, LX/FRU;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    iget-object p1, p3, LX/FRU;->b:LX/FRr;

    iget-object p1, p1, LX/FRr;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2240724
    iget-object v0, p3, LX/FRU;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    iget-object p1, p3, LX/FRU;->b:LX/FRr;

    iget-object p1, p1, LX/FRr;->e:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2240725
    iget-object v0, p3, LX/FRU;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    iget-object p1, p2, LX/FRr;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4, p4, p1, p4}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2240726
    move-object v0, p3

    .line 2240727
    goto :goto_0

    :cond_0
    check-cast p3, LX/FR3;

    goto :goto_1

    .line 2240728
    :cond_1
    check-cast p3, Lcom/facebook/payments/history/picker/PaymentHistoryRowItemView;

    goto :goto_2

    .line 2240729
    :cond_2
    check-cast p3, LX/FRT;

    goto :goto_3

    .line 2240730
    :cond_3
    iget-object v0, p3, LX/FRT;->c:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const p2, 0x7f081e18

    invoke-virtual {v0, p2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(I)V

    goto :goto_4

    .line 2240731
    :cond_4
    check-cast p3, LX/FRU;

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
