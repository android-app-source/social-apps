.class public final LX/GbS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Ljava/lang/Runnable;

.field public final synthetic c:LX/GbV;


# direct methods
.method public constructor <init>(LX/GbV;Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2369761
    iput-object p1, p0, LX/GbS;->c:LX/GbV;

    iput-object p2, p0, LX/GbS;->a:Landroid/content/Context;

    iput-object p3, p0, LX/GbS;->b:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2369762
    iget-object v0, p0, LX/GbS;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, LX/GbS;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2369763
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2369764
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2369765
    if-eqz p1, :cond_1

    .line 2369766
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2369767
    check-cast v0, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;

    .line 2369768
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_1

    .line 2369769
    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/settings/FetchDBLNonceInfoQueryModels$FetchDBLNonceInfoQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, LX/GbS;->c:LX/GbV;

    invoke-static {v2, v1, v0}, LX/GbV;->a$redex0(LX/GbV;LX/15i;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2369770
    :cond_1
    :goto_0
    iget-object v0, p0, LX/GbS;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, LX/GbS;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2369771
    return-void

    :catch_0
    goto :goto_0
.end method
