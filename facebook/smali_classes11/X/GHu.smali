.class public final LX/GHu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GHx;


# direct methods
.method public constructor <init>(LX/GHx;)V
    .locals 0

    .prologue
    .line 2335320
    iput-object p1, p0, LX/GHu;->a:LX/GHx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x175a57da

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2335321
    iget-object v0, p0, LX/GHu;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->r:Ljava/util/List;

    iget-object v2, p0, LX/GHu;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2335322
    iget-object v2, p0, LX/GHu;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->r:Ljava/util/List;

    iget-object v3, p0, LX/GHu;->a:LX/GHx;

    iget-object v3, v3, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2335323
    iget-object v2, p0, LX/GHu;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object v3, p0, LX/GHu;->a:LX/GHx;

    iget-object v3, v3, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-virtual {v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;)V

    .line 2335324
    iget-object v2, p0, LX/GHu;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335325
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    if-nez v3, :cond_3

    .line 2335326
    :goto_0
    iget-object v2, p0, LX/GHu;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2335327
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    if-nez v3, :cond_4

    .line 2335328
    :goto_1
    iget-object v2, p0, LX/GHu;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2335329
    iget-object v0, p0, LX/GHu;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->h()V

    .line 2335330
    iget-object v0, p0, LX/GHu;->a:LX/GHx;

    const/4 v2, 0x0

    .line 2335331
    iput-object v2, v0, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    .line 2335332
    :goto_2
    iget-object v0, p0, LX/GHu;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 2335333
    iget-object v0, p0, LX/GHu;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->l()V

    .line 2335334
    :cond_0
    const v0, -0xc2cf994

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2335335
    :cond_1
    iget-object v2, p0, LX/GHu;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 2335336
    :goto_3
    iget-object v2, p0, LX/GHu;->a:LX/GHx;

    iget-object v3, p0, LX/GHu;->a:LX/GHx;

    iget-object v3, v3, LX/GHx;->r:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    .line 2335337
    iput-object v0, v2, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    .line 2335338
    iget-object v0, p0, LX/GHu;->a:LX/GHx;

    invoke-static {v0}, LX/GHx;->h(LX/GHx;)V

    goto :goto_2

    .line 2335339
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_3
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_1
.end method
