.class public abstract LX/FXP;
.super LX/BYF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DAO::",
        "LX/AU0;",
        ">",
        "LX/BYF;"
    }
.end annotation


# instance fields
.field private a:LX/AU0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TDAO;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2255060
    invoke-direct {p0}, LX/BYF;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)LX/AU0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TDAO;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2255061
    iget-object v0, p0, LX/FXP;->a:LX/AU0;

    invoke-interface {v0, p1}, LX/AU0;->a(I)Z

    .line 2255062
    iget-object v0, p0, LX/FXP;->a:LX/AU0;

    return-object v0
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 2255063
    if-eqz p1, :cond_0

    .line 2255064
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot call changeCursor(cursor), must call changeCursor(cursor, daoItem)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2255065
    :cond_0
    return-void
.end method

.method public final a(Landroid/database/Cursor;LX/AU0;)V
    .locals 0
    .param p2    # LX/AU0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "TDAO;)V"
        }
    .end annotation

    .prologue
    .line 2255066
    iput-object p2, p0, LX/FXP;->a:LX/AU0;

    .line 2255067
    invoke-super {p0, p1}, LX/BYF;->a(Landroid/database/Cursor;)V

    .line 2255068
    return-void
.end method

.method public final e()LX/AU0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDAO;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2255069
    iget-object v0, p0, LX/FXP;->a:LX/AU0;

    if-nez v0, :cond_0

    .line 2255070
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getDAOItem called from an illegal context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2255071
    :cond_0
    iget-object v0, p0, LX/FXP;->a:LX/AU0;

    return-object v0
.end method
