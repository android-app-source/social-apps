.class public final LX/G3V;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V
    .locals 0

    .prologue
    .line 2315684
    iput-object p1, p0, LX/G3V;->a:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2315685
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2315686
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2315687
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315688
    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    const/4 v0, 0x0

    .line 2315689
    :goto_2
    if-nez v0, :cond_5

    const-string v0, "https://static.xx.fbcdn.net/rsrc.php/v1/y2/r/pQ_103u9HMH.jpg"

    :goto_3
    iget-object v1, p0, LX/G3V;->a:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    .line 2315690
    iget-object v2, v1, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    iget-object v3, v1, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->e:Lcom/facebook/user/model/User;

    .line 2315691
    iget-object p0, v3, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v3, p0

    .line 2315692
    invoke-virtual {v3}, Lcom/facebook/user/model/Name;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v2, v0, v3, p0}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2315693
    return-void

    .line 2315694
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315695
    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2315696
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2315697
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315698
    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2315699
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    .line 2315700
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315701
    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2315702
    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2315703
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315704
    check-cast v0, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineCoverPhotoUriQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2315705
    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
