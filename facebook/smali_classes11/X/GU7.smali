.class public final LX/GU7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2356974
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 2356975
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2356976
    :goto_0
    return v1

    .line 2356977
    :cond_0
    const-string v9, "is_sharing_enabled"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2356978
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    .line 2356979
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 2356980
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2356981
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2356982
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 2356983
    const-string v9, "friends_sharing"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2356984
    invoke-static {p0, p1}, LX/GU3;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2356985
    :cond_2
    const-string v9, "is_traveling"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2356986
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 2356987
    :cond_3
    const-string v9, "upsell"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2356988
    invoke-static {p0, p1}, LX/GU3;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2356989
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2356990
    :cond_5
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2356991
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2356992
    if-eqz v3, :cond_6

    .line 2356993
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 2356994
    :cond_6
    if-eqz v0, :cond_7

    .line 2356995
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 2356996
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2356997
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2356998
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2356999
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2357000
    if-eqz v0, :cond_0

    .line 2357001
    const-string v1, "friends_sharing"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2357002
    invoke-static {p0, v0, p2, p3}, LX/GU3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2357003
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2357004
    if-eqz v0, :cond_1

    .line 2357005
    const-string v1, "is_sharing_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2357006
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2357007
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2357008
    if-eqz v0, :cond_2

    .line 2357009
    const-string v1, "is_traveling"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2357010
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2357011
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2357012
    if-eqz v0, :cond_3

    .line 2357013
    const-string v1, "upsell"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2357014
    invoke-static {p0, v0, p2, p3}, LX/GU3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2357015
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2357016
    return-void
.end method
