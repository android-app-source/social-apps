.class public LX/H9G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

.field private final c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

.field private final d:Landroid/content/Context;

.field public final e:LX/CYI;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/content/SecureContextHelper;

.field public h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;LX/CYI;LX/0Ot;Lcom/facebook/content/SecureContextHelper;)V
    .locals 5
    .param p1    # Lcom/facebook/graphql/enums/GraphQLPageActionType;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonInterfaces$PageCallToActionButtonData;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLInterfaces$CallToActionAdminConfig$;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            "Lcom/facebook/pages/common/surface/calltoaction/common/PageCallToActionButtonHandler;",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434162
    iput-object p1, p0, LX/H9G;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2434163
    iput-object p2, p0, LX/H9G;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2434164
    iput-object p3, p0, LX/H9G;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2434165
    iput-object p4, p0, LX/H9G;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434166
    iput-object p5, p0, LX/H9G;->d:Landroid/content/Context;

    .line 2434167
    iput-object p6, p0, LX/H9G;->e:LX/CYI;

    .line 2434168
    iput-object p7, p0, LX/H9G;->f:LX/0Ot;

    .line 2434169
    iput-object p8, p0, LX/H9G;->g:Lcom/facebook/content/SecureContextHelper;

    .line 2434170
    iget-object v0, p0, LX/H9G;->e:LX/CYI;

    new-instance v1, LX/CY6;

    invoke-direct {v1}, LX/CY6;-><init>()V

    iget-object v2, p0, LX/H9G;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2434171
    iput-wide v2, v1, LX/CY6;->a:J

    .line 2434172
    move-object v1, v1

    .line 2434173
    iget-object v2, p0, LX/H9G;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v2

    .line 2434174
    iput-object v2, v1, LX/CY6;->b:Ljava/lang/String;

    .line 2434175
    move-object v1, v1

    .line 2434176
    iget-object v2, p0, LX/H9G;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v2

    .line 2434177
    iput-object v2, v1, LX/CY6;->c:LX/0Px;

    .line 2434178
    move-object v1, v1

    .line 2434179
    iget-object v2, p0, LX/H9G;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2434180
    iput-object v2, v1, LX/CY6;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2434181
    move-object v1, v1

    .line 2434182
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->MOBILE_PAGE_PRESENCE_CALL_TO_ACTION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 2434183
    iput-object v2, v1, LX/CY6;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 2434184
    move-object v1, v1

    .line 2434185
    invoke-virtual {v1}, LX/CY6;->a()LX/CY7;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CYI;->a(LX/CY7;)LX/CY8;

    .line 2434186
    return-void
.end method

.method private f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2434187
    iget-object v0, p0, LX/H9G;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H9G;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/H9G;->c:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/H9G;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private g()I
    .locals 2

    .prologue
    .line 2434188
    iget-object v0, p0, LX/H9G;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-nez v0, :cond_0

    .line 2434189
    const v0, 0x7f020999

    .line 2434190
    :goto_0
    return v0

    .line 2434191
    :cond_0
    sget-object v0, LX/H9F;->a:[I

    iget-object v1, p0, LX/H9G;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2434192
    const v0, 0x7f020999

    goto :goto_0

    .line 2434193
    :pswitch_0
    const v0, 0x7f020970

    goto :goto_0

    .line 2434194
    :pswitch_1
    const v0, 0x7f0207ab

    goto :goto_0

    .line 2434195
    :pswitch_2
    const v0, 0x7f020956

    goto :goto_0

    .line 2434196
    :pswitch_3
    const v0, 0x7f0208cd

    goto :goto_0

    .line 2434197
    :pswitch_4
    const v0, 0x7f020989

    goto :goto_0

    .line 2434198
    :pswitch_5
    const v0, 0x7f020850

    goto :goto_0

    .line 2434199
    :pswitch_6
    const v0, 0x7f020837

    goto :goto_0

    .line 2434200
    :pswitch_7
    const v0, 0x7f0209f9

    goto :goto_0

    .line 2434201
    :pswitch_8
    const v0, 0x7f0208ed

    goto :goto_0

    .line 2434202
    :pswitch_9
    const v0, 0x7f020938

    goto :goto_0

    .line 2434203
    :pswitch_a
    const v0, 0x7f020809

    goto :goto_0

    .line 2434204
    :pswitch_b
    const v0, 0x7f02074d

    goto :goto_0

    .line 2434205
    :pswitch_c
    const v0, 0x7f0208a9

    goto :goto_0

    .line 2434206
    :pswitch_d
    const v0, 0x7f020855

    goto :goto_0

    .line 2434207
    :pswitch_e
    const v0, 0x7f0209c9

    goto :goto_0

    .line 2434208
    :pswitch_f
    const v0, 0x7f020952

    goto :goto_0

    .line 2434209
    :pswitch_10
    const v0, 0x7f0207b0

    goto :goto_0

    .line 2434210
    :pswitch_11
    const v0, 0x7f020a20

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434211
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    invoke-direct {p0}, LX/H9G;->f()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, LX/H9G;->g()I

    move-result v3

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(ILjava/lang/String;IIZ)V

    return-object v0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434212
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    invoke-direct {p0}, LX/H9G;->f()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, LX/H9G;->g()I

    move-result v3

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(ILjava/lang/String;IIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2434213
    iget-object v0, p0, LX/H9G;->e:LX/CYI;

    invoke-virtual {v0}, LX/CYI;->onClick()V

    .line 2434214
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434215
    const/4 v0, 0x0

    return-object v0
.end method
