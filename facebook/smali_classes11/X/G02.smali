.class public final LX/G02;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;)V
    .locals 0

    .prologue
    .line 2308395
    iput-object p1, p0, LX/G02;->a:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2308396
    iget-object v0, p0, LX/G02;->a:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->w:LX/Fze;

    new-instance v1, LX/G01;

    invoke-direct {v1, p0, p1}, LX/G01;-><init>(LX/G02;Ljava/lang/String;)V

    .line 2308397
    iget-object v2, v0, LX/Fze;->d:LX/1Ck;

    const-string v3, "CLEAR_MEMORIAL_COVER_PHOTO_CACHE"

    new-instance p0, LX/Fzc;

    invoke-direct {p0, v0}, LX/Fzc;-><init>(LX/Fze;)V

    new-instance p1, LX/Fzd;

    invoke-direct {p1, v0, v1}, LX/Fzd;-><init>(LX/Fze;LX/0TF;)V

    invoke-virtual {v2, v3, p0, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2308398
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2308399
    iget-object v0, p0, LX/G02;->a:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->x:LX/G0B;

    const v1, 0x7f08336c

    invoke-virtual {v0, v1}, LX/G0B;->a(I)V

    .line 2308400
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2308401
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, LX/G02;->a(Ljava/lang/String;)V

    return-void
.end method
