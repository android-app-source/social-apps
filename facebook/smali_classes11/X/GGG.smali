.class public final enum LX/GGG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GGG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GGG;

.field public static final enum ADDRESS:LX/GGG;

.field public static final enum REGION:LX/GGG;


# instance fields
.field public final key:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2333434
    new-instance v0, LX/GGG;

    const-string v1, "REGION"

    const-string v2, "region"

    invoke-direct {v0, v1, v3, v2}, LX/GGG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GGG;->REGION:LX/GGG;

    .line 2333435
    new-instance v0, LX/GGG;

    const-string v1, "ADDRESS"

    const-string v2, "address"

    invoke-direct {v0, v1, v4, v2}, LX/GGG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GGG;->ADDRESS:LX/GGG;

    .line 2333436
    const/4 v0, 0x2

    new-array v0, v0, [LX/GGG;

    sget-object v1, LX/GGG;->REGION:LX/GGG;

    aput-object v1, v0, v3

    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    aput-object v1, v0, v4

    sput-object v0, LX/GGG;->$VALUES:[LX/GGG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2333431
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2333432
    iput-object p3, p0, LX/GGG;->key:Ljava/lang/String;

    .line 2333433
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GGG;
    .locals 1

    .prologue
    .line 2333430
    const-class v0, LX/GGG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GGG;

    return-object v0
.end method

.method public static values()[LX/GGG;
    .locals 1

    .prologue
    .line 2333429
    sget-object v0, LX/GGG;->$VALUES:[LX/GGG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GGG;

    return-object v0
.end method
