.class public final LX/GaD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field private c:Z

.field private d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/commerce/core/intent/MerchantInfoViewData;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/Ga8;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2368043
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2368044
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GaD;->d:LX/0am;

    .line 2368045
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GaD;->f:LX/0am;

    .line 2368046
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GaD;->g:LX/0am;

    .line 2368047
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GaD;->h:LX/0am;

    .line 2368048
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GaD;->i:LX/0am;

    .line 2368049
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GaD;->j:LX/0am;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)LX/GaD;
    .locals 1

    .prologue
    .line 2368050
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GaD;->d:LX/0am;

    .line 2368051
    return-object p0
.end method

.method public final a()LX/GaE;
    .locals 12

    .prologue
    .line 2368052
    new-instance v0, LX/GaE;

    iget-boolean v1, p0, LX/GaD;->a:Z

    iget-boolean v2, p0, LX/GaD;->b:Z

    iget-boolean v3, p0, LX/GaD;->c:Z

    iget-object v4, p0, LX/GaD;->d:LX/0am;

    iget-boolean v5, p0, LX/GaD;->e:Z

    iget-object v6, p0, LX/GaD;->f:LX/0am;

    iget-object v7, p0, LX/GaD;->g:LX/0am;

    iget-object v8, p0, LX/GaD;->h:LX/0am;

    iget-object v9, p0, LX/GaD;->i:LX/0am;

    iget-object v10, p0, LX/GaD;->j:LX/0am;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, LX/GaE;-><init>(ZZZLX/0am;ZLX/0am;LX/0am;LX/0am;LX/0am;LX/0am;B)V

    return-object v0
.end method
