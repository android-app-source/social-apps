.class public LX/FXN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/FXN;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/ElQ;

.field public final c:LX/79a;

.field public final d:LX/Eke;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/FWt;

.field public final g:LX/1Sd;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/FZ3;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0lp;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/ElQ;LX/79a;LX/1Sd;LX/0Ot;LX/FWt;LX/0Ot;LX/FZ3;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/ElQ;",
            "LX/79a;",
            "LX/1Sd;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;",
            "LX/FWt;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/FZ3;",
            "LX/0Ot",
            "<",
            "LX/0lp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wp;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2255002
    iput-object p1, p0, LX/FXN;->a:Landroid/content/res/Resources;

    .line 2255003
    iput-object p2, p0, LX/FXN;->b:LX/ElQ;

    .line 2255004
    iput-object p3, p0, LX/FXN;->c:LX/79a;

    .line 2255005
    new-instance v0, LX/Eke;

    invoke-direct {v0, p4}, LX/Eke;-><init>(LX/1Se;)V

    iput-object v0, p0, LX/FXN;->d:LX/Eke;

    .line 2255006
    iput-object p5, p0, LX/FXN;->e:LX/0Ot;

    .line 2255007
    iput-object p6, p0, LX/FXN;->f:LX/FWt;

    .line 2255008
    iput-object p4, p0, LX/FXN;->g:LX/1Sd;

    .line 2255009
    iput-object p7, p0, LX/FXN;->h:LX/0Ot;

    .line 2255010
    iput-object p8, p0, LX/FXN;->i:LX/FZ3;

    .line 2255011
    iput-object p9, p0, LX/FXN;->j:LX/0Ot;

    .line 2255012
    iput-object p10, p0, LX/FXN;->k:LX/0Ot;

    .line 2255013
    return-void
.end method

.method private static a(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 2255014
    const/4 v0, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2255015
    shl-int/lit8 v2, v0, 0x20

    or-int/2addr v2, v1

    int-to-long v2, v2

    move-wide v0, v2

    .line 2255016
    return-wide v0
.end method

.method public static a(LX/0QB;)LX/FXN;
    .locals 14

    .prologue
    .line 2255017
    sget-object v0, LX/FXN;->l:LX/FXN;

    if-nez v0, :cond_1

    .line 2255018
    const-class v1, LX/FXN;

    monitor-enter v1

    .line 2255019
    :try_start_0
    sget-object v0, LX/FXN;->l:LX/FXN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2255020
    if-eqz v2, :cond_0

    .line 2255021
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2255022
    new-instance v3, LX/FXN;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/Eld;->a(LX/0QB;)LX/ElQ;

    move-result-object v5

    check-cast v5, LX/ElQ;

    invoke-static {v0}, LX/79a;->a(LX/0QB;)LX/79a;

    move-result-object v6

    check-cast v6, LX/79a;

    invoke-static {v0}, LX/1Sd;->a(LX/0QB;)LX/1Sd;

    move-result-object v7

    check-cast v7, LX/1Sd;

    const/16 v8, 0xb19

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/FWt;->a(LX/0QB;)LX/FWt;

    move-result-object v9

    check-cast v9, LX/FWt;

    const/16 v10, 0x132e

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/FZ3;->b(LX/0QB;)LX/FZ3;

    move-result-object v11

    check-cast v11, LX/FZ3;

    const/16 v12, 0x1409

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x12e4

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/FXN;-><init>(Landroid/content/res/Resources;LX/ElQ;LX/79a;LX/1Sd;LX/0Ot;LX/FWt;LX/0Ot;LX/FZ3;LX/0Ot;LX/0Ot;)V

    .line 2255023
    move-object v0, v3

    .line 2255024
    sput-object v0, LX/FXN;->l:LX/FXN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2255025
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2255026
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2255027
    :cond_1
    sget-object v0, LX/FXN;->l:LX/FXN;

    return-object v0

    .line 2255028
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2255029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/FXq;)V
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/FXq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2255030
    iget-object v0, p0, LX/FXN;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b229b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2255031
    const/4 v1, 0x0

    .line 2255032
    iget-object v0, p0, LX/FXN;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2255033
    const/4 v0, 0x1

    move v0, v0

    .line 2255034
    if-eqz v0, :cond_1

    .line 2255035
    const-string v0, "MPEG_DASH"

    .line 2255036
    :goto_0
    iget-object v1, p0, LX/FXN;->b:LX/ElQ;

    invoke-virtual {v1, v2}, LX/ElQ;->a(I)LX/ElO;

    move-result-object v1

    .line 2255037
    iget-object v4, v1, LX/ElO;->b:LX/ElN;

    const-string v5, "response_format"

    const-string v6, "flatbuffer"

    invoke-virtual {v4, v5, v6}, LX/ElN;->a(Ljava/lang/String;Ljava/lang/String;)LX/ElN;

    .line 2255038
    move-object v1, v1

    .line 2255039
    const-string v4, "4"

    invoke-static {v4}, LX/FXN;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5, p1}, LX/ElO;->a(JLjava/lang/String;)LX/ElO;

    move-result-object v1

    const-string v4, "0"

    invoke-static {v4}, LX/FXN;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5, p2}, LX/ElO;->a(JLjava/lang/String;)LX/ElO;

    move-result-object v1

    const-string v4, "1"

    invoke-static {v4}, LX/FXN;->a(Ljava/lang/String;)J

    move-result-wide v4

    const/16 v6, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v6}, LX/ElO;->a(JLjava/lang/Number;)LX/ElO;

    move-result-object v1

    const-string v4, "3"

    invoke-static {v4}, LX/FXN;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v4, v5, v6}, LX/ElO;->a(JLjava/lang/Number;)LX/ElO;

    move-result-object v1

    const-string v4, "2"

    invoke-static {v4}, LX/FXN;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v4, v5, v3}, LX/ElO;->a(JLjava/lang/Number;)LX/ElO;

    move-result-object v1

    const-string v3, "7"

    invoke-static {v3}, LX/FXN;->a(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5, v0}, LX/ElO;->a(JLjava/lang/String;)LX/ElO;

    move-result-object v1

    new-instance v3, LX/FXM;

    if-nez p2, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v3, p0, p1, v0, p3}, LX/FXM;-><init>(LX/FXN;Ljava/lang/String;ZLX/FXq;)V

    .line 2255040
    iget-object v0, v1, LX/ElO;->b:LX/ElN;

    .line 2255041
    iget-object v2, v0, LX/ElN;->a:LX/Ekt;

    .line 2255042
    iget-object v4, v2, LX/Ekt;->c:LX/Eki;

    .line 2255043
    iput-object v3, v4, LX/Eki;->e:LX/FXM;

    .line 2255044
    move-object v0, v1

    .line 2255045
    iget-object v1, v0, LX/ElO;->c:LX/0n9;

    if-eqz v1, :cond_0

    .line 2255046
    iget-object v1, v0, LX/ElO;->c:LX/0n9;

    invoke-static {}, LX/0nC;->a()LX/0nC;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0nA;->a(LX/0nD;)V

    .line 2255047
    iget-object v1, v0, LX/ElO;->c:LX/0n9;

    invoke-virtual {v1}, LX/0nA;->f()V

    .line 2255048
    iget-object v1, v0, LX/ElO;->b:LX/ElN;

    const-string v2, "query_params"

    iget-object v3, v0, LX/ElO;->c:LX/0n9;

    invoke-virtual {v1, v2, v3}, LX/ElN;->a(Ljava/lang/String;LX/0nA;)LX/ElN;

    .line 2255049
    :cond_0
    iget-object v1, v0, LX/ElO;->b:LX/ElN;

    invoke-virtual {v1}, LX/ElN;->a()LX/Ekk;

    .line 2255050
    return-void

    .line 2255051
    :cond_1
    iget-object v0, p0, LX/FXN;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wp;

    invoke-virtual {v0}, LX/0wp;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2255052
    const-string v0, "WEBM_DASH"

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 2255053
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto/16 :goto_0
.end method
