.class public final enum LX/H4K;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H4K;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H4K;

.field public static final enum COUNT:LX/H4K;

.field public static final enum PLACE_CELL:LX/H4K;

.field public static final enum QUERY_SUGGESTION_CELL:LX/H4K;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2422572
    new-instance v0, LX/H4K;

    const-string v1, "QUERY_SUGGESTION_CELL"

    invoke-direct {v0, v1, v2}, LX/H4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H4K;->QUERY_SUGGESTION_CELL:LX/H4K;

    .line 2422573
    new-instance v0, LX/H4K;

    const-string v1, "PLACE_CELL"

    invoke-direct {v0, v1, v3}, LX/H4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H4K;->PLACE_CELL:LX/H4K;

    .line 2422574
    new-instance v0, LX/H4K;

    const-string v1, "COUNT"

    invoke-direct {v0, v1, v4}, LX/H4K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H4K;->COUNT:LX/H4K;

    .line 2422575
    const/4 v0, 0x3

    new-array v0, v0, [LX/H4K;

    sget-object v1, LX/H4K;->QUERY_SUGGESTION_CELL:LX/H4K;

    aput-object v1, v0, v2

    sget-object v1, LX/H4K;->PLACE_CELL:LX/H4K;

    aput-object v1, v0, v3

    sget-object v1, LX/H4K;->COUNT:LX/H4K;

    aput-object v1, v0, v4

    sput-object v0, LX/H4K;->$VALUES:[LX/H4K;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2422576
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H4K;
    .locals 1

    .prologue
    .line 2422577
    const-class v0, LX/H4K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H4K;

    return-object v0
.end method

.method public static values()[LX/H4K;
    .locals 1

    .prologue
    .line 2422578
    sget-object v0, LX/H4K;->$VALUES:[LX/H4K;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H4K;

    return-object v0
.end method
