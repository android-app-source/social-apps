.class public LX/GiT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0gh;


# direct methods
.method public constructor <init>(LX/0gh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2385921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2385922
    iput-object p1, p0, LX/GiT;->a:LX/0gh;

    .line 2385923
    return-void
.end method

.method public static b(LX/0QB;)LX/GiT;
    .locals 2

    .prologue
    .line 2385924
    new-instance v1, LX/GiT;

    invoke-static {p0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-direct {v1, v0}, LX/GiT;-><init>(LX/0gh;)V

    .line 2385925
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2385926
    const-string v0, "referrer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2385927
    const-string v0, "ref"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2385928
    const-string v0, "gametime_ref"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v0, v0, LX/87b;

    if-eqz v0, :cond_1

    const-string v0, "gametime_ref"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/87b;

    iget-object v0, v0, LX/87b;->value:Ljava/lang/String;

    .line 2385929
    :goto_0
    if-eqz v1, :cond_2

    .line 2385930
    iget-object v0, p0, LX/GiT;->a:LX/0gh;

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2385931
    :cond_0
    :goto_1
    return-void

    .line 2385932
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2385933
    :cond_2
    if-eqz v2, :cond_3

    .line 2385934
    iget-object v0, p0, LX/GiT;->a:LX/0gh;

    invoke-virtual {v0, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto :goto_1

    .line 2385935
    :cond_3
    if-eqz v0, :cond_0

    .line 2385936
    iget-object v1, p0, LX/GiT;->a:LX/0gh;

    invoke-virtual {v1, v0}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/base/activity/FbFragmentActivity;)V
    .locals 1

    .prologue
    .line 2385937
    invoke-virtual {p1}, Lcom/facebook/base/activity/FbFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GiT;->a(Landroid/os/Bundle;)V

    .line 2385938
    iget-object v0, p0, LX/GiT;->a:LX/0gh;

    invoke-virtual {v0, p1}, LX/0gh;->a(Landroid/app/Activity;)V

    .line 2385939
    return-void
.end method

.method public final b(Lcom/facebook/base/activity/FbFragmentActivity;)V
    .locals 2

    .prologue
    .line 2385940
    iget-object v0, p0, LX/GiT;->a:LX/0gh;

    const-string v1, "gametime_tab_switch"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2385941
    iget-object v0, p0, LX/GiT;->a:LX/0gh;

    invoke-virtual {v0, p1}, LX/0gh;->a(Landroid/app/Activity;)V

    .line 2385942
    return-void
.end method
