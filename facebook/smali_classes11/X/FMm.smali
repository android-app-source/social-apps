.class public LX/FMm;
.super LX/0Yd;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2230625
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.facebook.messaging.sms.COMPOSE_SMS"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.facebook.messaging.sms.HEADLESS_SEND"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.facebook.messaging.sms.REQUEST_SEND_MESSAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.facebook.messaging.sms.MESSAGE_SENT"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.facebook.messaging.sms.DOWNLOAD_MMS"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.facebook.messaging.sms.MMS_DOWNLOADED"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.facebook.messaging.sms.MARK_PENDING_MMS"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.facebook.messaging.sms.E2E_TEST_RECEIVING_SMS"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.facebook.messaging.sms.SHORTCODE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.facebook.messaging.sms.LIKE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.facebook.messaging.sms.DISMISS_NOTIFICATION"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.facebook.messaging.sms.DELETE_TEMP_FILE"

    aput-object v2, v0, v1

    sput-object v0, LX/FMm;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    .line 2230626
    new-instance v1, LX/FMg;

    invoke-direct {v1}, LX/FMg;-><init>()V

    .line 2230627
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 2230628
    sget-object v3, LX/FMm;->a:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 2230629
    invoke-virtual {v2, v5, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2230630
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2230631
    :cond_0
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v0, v0

    .line 2230632
    invoke-direct {p0, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    .line 2230633
    return-void
.end method
