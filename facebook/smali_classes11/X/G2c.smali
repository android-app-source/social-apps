.class public LX/G2c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/G2b;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/G2c;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314347
    return-void
.end method

.method public static a(LX/0QB;)LX/G2c;
    .locals 3

    .prologue
    .line 2314348
    sget-object v0, LX/G2c;->a:LX/G2c;

    if-nez v0, :cond_1

    .line 2314349
    const-class v1, LX/G2c;

    monitor-enter v1

    .line 2314350
    :try_start_0
    sget-object v0, LX/G2c;->a:LX/G2c;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2314351
    if-eqz v2, :cond_0

    .line 2314352
    :try_start_1
    new-instance v0, LX/G2c;

    invoke-direct {v0}, LX/G2c;-><init>()V

    .line 2314353
    move-object v0, v0

    .line 2314354
    sput-object v0, LX/G2c;->a:LX/G2c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314355
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2314356
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2314357
    :cond_1
    sget-object v0, LX/G2c;->a:LX/G2c;

    return-object v0

    .line 2314358
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2314359
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2314360
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2314361
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2314362
    return-void
.end method
