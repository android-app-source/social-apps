.class public final LX/G95;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private a:[I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2321544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321545
    const/4 v0, 0x0

    iput v0, p0, LX/G95;->b:I

    .line 2321546
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/G95;->a:[I

    .line 2321547
    return-void
.end method

.method private constructor <init>([II)V
    .locals 0

    .prologue
    .line 2321540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321541
    iput-object p1, p0, LX/G95;->a:[I

    .line 2321542
    iput p2, p0, LX/G95;->b:I

    .line 2321543
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2321535
    iget-object v0, p0, LX/G95;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x20

    if-le p1, v0, :cond_0

    .line 2321536
    add-int/lit8 v0, p1, 0x1f

    div-int/lit8 v0, v0, 0x20

    new-array v0, v0, [I

    move-object v0, v0

    .line 2321537
    iget-object v1, p0, LX/G95;->a:[I

    iget-object v2, p0, LX/G95;->a:[I

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2321538
    iput-object v0, p0, LX/G95;->a:[I

    .line 2321539
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2321527
    if-ltz p2, :cond_0

    const/16 v0, 0x20

    if-le p2, v0, :cond_1

    .line 2321528
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Num bits must be between 0 and 32"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321529
    :cond_1
    iget v0, p0, LX/G95;->b:I

    add-int/2addr v0, p2

    invoke-direct {p0, v0}, LX/G95;->b(I)V

    .line 2321530
    :goto_0
    if-lez p2, :cond_3

    .line 2321531
    add-int/lit8 v0, p2, -0x1

    shr-int v0, p1, v0

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, LX/G95;->a(Z)V

    .line 2321532
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    .line 2321533
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2321534
    :cond_3
    return-void
.end method

.method public final a(I[BII)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2321518
    move v4, v2

    move v0, p1

    :goto_0
    if-ge v4, p4, :cond_2

    move v1, v2

    move v3, v0

    move v0, v2

    .line 2321519
    :goto_1
    const/16 v5, 0x8

    if-ge v1, v5, :cond_1

    .line 2321520
    invoke-virtual {p0, v3}, LX/G95;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2321521
    const/4 v5, 0x1

    rsub-int/lit8 v6, v1, 0x7

    shl-int/2addr v5, v6

    or-int/2addr v0, v5

    .line 2321522
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 2321523
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2321524
    :cond_1
    add-int v1, p3, v4

    int-to-byte v0, v0

    aput-byte v0, p2, v1

    .line 2321525
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v0, v3

    goto :goto_0

    .line 2321526
    :cond_2
    return-void
.end method

.method public final a(LX/G95;)V
    .locals 3

    .prologue
    .line 2321512
    iget v1, p1, LX/G95;->b:I

    .line 2321513
    iget v0, p0, LX/G95;->b:I

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, LX/G95;->b(I)V

    .line 2321514
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2321515
    invoke-virtual {p1, v0}, LX/G95;->a(I)Z

    move-result v2

    invoke-virtual {p0, v2}, LX/G95;->a(Z)V

    .line 2321516
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2321517
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 2321507
    iget v0, p0, LX/G95;->b:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/G95;->b(I)V

    .line 2321508
    if-eqz p1, :cond_0

    .line 2321509
    iget-object v0, p0, LX/G95;->a:[I

    iget v1, p0, LX/G95;->b:I

    div-int/lit8 v1, v1, 0x20

    aget v2, v0, v1

    const/4 v3, 0x1

    iget v4, p0, LX/G95;->b:I

    and-int/lit8 v4, v4, 0x1f

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    aput v2, v0, v1

    .line 2321510
    :cond_0
    iget v0, p0, LX/G95;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G95;->b:I

    .line 2321511
    return-void
.end method

.method public final a(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2321485
    iget-object v1, p0, LX/G95;->a:[I

    div-int/lit8 v2, p1, 0x20

    aget v1, v1, v2

    and-int/lit8 v2, p1, 0x1f

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2321506
    iget v0, p0, LX/G95;->b:I

    add-int/lit8 v0, v0, 0x7

    div-int/lit8 v0, v0, 0x8

    return v0
.end method

.method public final b(LX/G95;)V
    .locals 4

    .prologue
    .line 2321500
    iget-object v0, p0, LX/G95;->a:[I

    array-length v0, v0

    iget-object v1, p1, LX/G95;->a:[I

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 2321501
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Sizes don\'t match"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321502
    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LX/G95;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 2321503
    iget-object v1, p0, LX/G95;->a:[I

    aget v2, v1, v0

    iget-object v3, p1, LX/G95;->a:[I

    aget v3, v3, v0

    xor-int/2addr v2, v3

    aput v2, v1, v0

    .line 2321504
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2321505
    :cond_1
    return-void
.end method

.method public final clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2321499
    new-instance v1, LX/G95;

    iget-object v0, p0, LX/G95;->a:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iget v2, p0, LX/G95;->b:I

    invoke-direct {v1, v0, v2}, LX/G95;-><init>([II)V

    return-object v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2321495
    instance-of v1, p1, LX/G95;

    if-nez v1, :cond_1

    .line 2321496
    :cond_0
    :goto_0
    return v0

    .line 2321497
    :cond_1
    check-cast p1, LX/G95;

    .line 2321498
    iget v1, p0, LX/G95;->b:I

    iget v2, p1, LX/G95;->b:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/G95;->a:[I

    iget-object v2, p1, LX/G95;->a:[I

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2321494
    iget v0, p0, LX/G95;->b:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, LX/G95;->a:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2321486
    new-instance v2, Ljava/lang/StringBuilder;

    iget v0, p0, LX/G95;->b:I

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2321487
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, LX/G95;->b:I

    if-ge v0, v1, :cond_2

    .line 2321488
    and-int/lit8 v1, v0, 0x7

    if-nez v1, :cond_0

    .line 2321489
    const/16 v1, 0x20

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2321490
    :cond_0
    invoke-virtual {p0, v0}, LX/G95;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x58

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2321491
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2321492
    :cond_1
    const/16 v1, 0x2e

    goto :goto_1

    .line 2321493
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
