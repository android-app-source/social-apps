.class public LX/Fcs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fcs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;)",
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262818
    if-nez p0, :cond_0

    .line 2262819
    const/4 v0, 0x0

    .line 2262820
    :goto_0
    return-object v0

    .line 2262821
    :cond_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2262822
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;

    .line 2262823
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;->a()LX/5uu;

    move-result-object v0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2262824
    invoke-interface {v0}, LX/5uu;->d()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 2262825
    :goto_2
    move-object v0, v0

    .line 2262826
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262827
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2262828
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2262829
    :cond_2
    invoke-interface {v0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v8, v7

    move v6, v7

    :goto_3
    if-ge v8, v10, :cond_3

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262830
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v5

    .line 2262831
    :goto_4
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move v6, v4

    goto :goto_3

    .line 2262832
    :cond_3
    new-instance v4, LX/5ux;

    invoke-direct {v4}, LX/5ux;-><init>()V

    .line 2262833
    if-nez v6, :cond_4

    .line 2262834
    :goto_5
    iput-boolean v5, v4, LX/5ux;->a:Z

    .line 2262835
    invoke-interface {v0}, LX/5uu;->d()Ljava/lang/String;

    move-result-object v5

    .line 2262836
    iput-object v5, v4, LX/5ux;->b:Ljava/lang/String;

    .line 2262837
    const-string v5, "default"

    .line 2262838
    iput-object v5, v4, LX/5ux;->c:Ljava/lang/String;

    .line 2262839
    invoke-virtual {v4}, LX/5ux;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v4

    .line 2262840
    new-instance v5, LX/5uz;

    invoke-direct {v5}, LX/5uz;-><init>()V

    .line 2262841
    iput-object v4, v5, LX/5uz;->a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    .line 2262842
    move-object v4, v5

    .line 2262843
    invoke-virtual {v4}, LX/5uz;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v4

    .line 2262844
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2262845
    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262846
    invoke-interface {v0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    :goto_6
    if-ge v7, v8, :cond_5

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262847
    invoke-static {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262848
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    :cond_4
    move v5, v7

    .line 2262849
    goto :goto_5

    .line 2262850
    :cond_5
    new-instance v4, LX/5uy;

    invoke-direct {v4}, LX/5uy;-><init>()V

    .line 2262851
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2262852
    iput-object v5, v4, LX/5uy;->a:LX/0Px;

    .line 2262853
    invoke-virtual {v4}, LX/5uy;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v4

    .line 2262854
    invoke-static {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->a(LX/5uu;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object v5

    invoke-static {v5}, LX/5v2;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;)LX/5v2;

    move-result-object v5

    .line 2262855
    iput-object v4, v5, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 2262856
    invoke-virtual {v5}, LX/5v2;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object v0

    goto/16 :goto_2

    :cond_6
    move v4, v6

    goto :goto_4
.end method

.method public static a(LX/0QB;)LX/Fcs;
    .locals 3

    .prologue
    .line 2262857
    sget-object v0, LX/Fcs;->a:LX/Fcs;

    if-nez v0, :cond_1

    .line 2262858
    const-class v1, LX/Fcs;

    monitor-enter v1

    .line 2262859
    :try_start_0
    sget-object v0, LX/Fcs;->a:LX/Fcs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2262860
    if-eqz v2, :cond_0

    .line 2262861
    :try_start_1
    new-instance v0, LX/Fcs;

    invoke-direct {v0}, LX/Fcs;-><init>()V

    .line 2262862
    move-object v0, v0

    .line 2262863
    sput-object v0, LX/Fcs;->a:LX/Fcs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262864
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2262865
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2262866
    :cond_1
    sget-object v0, LX/Fcs;->a:LX/Fcs;

    return-object v0

    .line 2262867
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2262868
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
