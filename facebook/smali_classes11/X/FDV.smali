.class public LX/FDV;
.super LX/Dny;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/2Om;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNK;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNX;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FML;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ud;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMI;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2212525
    const-class v0, LX/FDV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FDV;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2Om;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Om;",
            "LX/0Ot",
            "<",
            "LX/FNK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FML;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ud;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2212526
    const-string v0, "SmsAggregationRowCacheServiceHandler"

    invoke-direct {p0, v0}, LX/Dny;-><init>(Ljava/lang/String;)V

    .line 2212527
    iput-object p1, p0, LX/FDV;->b:LX/2Om;

    .line 2212528
    iput-object p2, p0, LX/FDV;->c:LX/0Ot;

    .line 2212529
    iput-object p5, p0, LX/FDV;->f:LX/0Ot;

    .line 2212530
    iput-object p3, p0, LX/FDV;->d:LX/0Ot;

    .line 2212531
    iput-object p4, p0, LX/FDV;->e:LX/0Ot;

    .line 2212532
    iput-object p6, p0, LX/FDV;->g:LX/0Ot;

    .line 2212533
    iput-object p7, p0, LX/FDV;->h:LX/0Ot;

    .line 2212534
    return-void
.end method

.method private a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/fbservice/service/OperationResult;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2212535
    sget-object v1, LX/6el;->SMS:LX/6el;

    .line 2212536
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v2, v0

    .line 2212537
    iget-object v0, p0, LX/FDV;->b:LX/2Om;

    invoke-virtual {v0, v1}, LX/2Om;->a(LX/6el;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2212538
    iget-object v0, p0, LX/FDV;->b:LX/2Om;

    invoke-virtual {v0, v1}, LX/2Om;->c(LX/6el;)J

    move-result-wide v4

    .line 2212539
    iget-wide v8, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->g:J

    move-wide v6, v8

    .line 2212540
    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 2212541
    const/4 v0, 0x0

    invoke-static {p0, v0, v2}, LX/FDV;->b(LX/FDV;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6ek;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p2

    .line 2212542
    :cond_0
    :goto_0
    return-object p2

    .line 2212543
    :cond_1
    iget-object v0, p0, LX/FDV;->b:LX/2Om;

    invoke-virtual {v0, v1}, LX/2Om;->b(LX/6el;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212544
    invoke-static {p0, v0, v2}, LX/FDV;->b(LX/FDV;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6ek;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p2

    goto :goto_0

    .line 2212545
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    .line 2212546
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2212547
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2212548
    iget-object v0, p0, LX/FDV;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNK;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v3

    iget-wide v4, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 2212549
    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v3, v3

    .line 2212550
    invoke-virtual {v0, v4, v5, v3}, LX/FNK;->a(JLX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212551
    invoke-static {p0, v0, v2}, LX/FDV;->b(LX/FDV;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6ek;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    move-result-object v0

    .line 2212552
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iget-wide v4, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->e:J

    invoke-static {p0, v2, v4, v5, v1}, LX/FDV;->a(LX/FDV;Lcom/facebook/messaging/model/threads/ThreadsCollection;JLX/6el;)V

    .line 2212553
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p2

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/fbservice/service/OperationResult;LX/0Rf;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;",
            "Lcom/facebook/fbservice/service/OperationResult;",
            "LX/0Rf",
            "<",
            "LX/6el;",
            ">;",
            "LX/1qM;",
            ")",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 2212554
    invoke-virtual {p2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    .line 2212555
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2212556
    if-nez v2, :cond_0

    .line 2212557
    invoke-static {p0}, LX/FDV;->a(LX/FDV;)V

    .line 2212558
    :goto_0
    return-object p2

    .line 2212559
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e()J

    move-result-wide v4

    .line 2212560
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2212561
    invoke-virtual {p3}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6el;

    .line 2212562
    invoke-static {p0, v1, p4}, LX/FDV;->a(LX/FDV;LX/6el;LX/1qM;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2212563
    if-eqz v1, :cond_1

    iget-wide v8, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v7, v8, v4

    if-gtz v7, :cond_1

    .line 2212564
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2212565
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2212566
    invoke-static {p0}, LX/FDV;->a(LX/FDV;)V

    goto :goto_0

    .line 2212567
    :cond_3
    iget v1, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v1, v1

    .line 2212568
    invoke-static {v3, v2, v1}, LX/FDV;->a(Ljava/util/List;Lcom/facebook/messaging/model/threads/ThreadsCollection;I)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v4

    .line 2212569
    new-instance v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SMS:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->b:LX/6ek;

    iget-object v5, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->d:LX/0Px;

    iget-object v0, p0, LX/FDV;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ud;

    invoke-virtual {v0}, LX/2Ud;->a()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;LX/0Px;J)V

    .line 2212570
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p2

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/fbservice/service/OperationResult;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2212571
    sget-object v1, LX/6el;->SMS:LX/6el;

    .line 2212572
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v2, v0

    .line 2212573
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    move-object v0, v0

    .line 2212574
    invoke-static {p0, v1, v0}, LX/FDV;->a(LX/FDV;LX/6el;LX/0rS;)Z

    move-result v0

    .line 2212575
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2212576
    if-eqz v0, :cond_1

    .line 2212577
    iget-object v0, p0, LX/FDV;->b:LX/2Om;

    invoke-virtual {v0, v1}, LX/2Om;->b(LX/6el;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212578
    invoke-static {v0, v2}, LX/FDV;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    move-object v0, v0

    .line 2212579
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    :goto_0
    move-object p2, v0

    .line 2212580
    :cond_0
    return-object p2

    .line 2212581
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2212582
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2212583
    invoke-virtual {v3}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2212584
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    iget-wide v4, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 2212585
    iget-object v0, p0, LX/FDV;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNK;

    invoke-virtual {v0, v4, v5, v2}, LX/FNK;->a(JLX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212586
    invoke-static {v0, v2}, LX/FDV;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2212587
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2212588
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iget-wide v4, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    invoke-static {p0, v2, v4, v5, v1}, LX/FDV;->a(LX/FDV;Lcom/facebook/messaging/model/threads/ThreadsCollection;JLX/6el;)V

    .line 2212589
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/fbservice/service/OperationResult;LX/0Rf;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/service/model/FetchThreadListParams;",
            "Lcom/facebook/fbservice/service/OperationResult;",
            "LX/0Rf",
            "<",
            "LX/6el;",
            ">;",
            "LX/1qM;",
            ")",
            "Lcom/facebook/fbservice/service/OperationResult;"
        }
    .end annotation

    .prologue
    .line 2212673
    invoke-virtual {p2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2212674
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2212675
    if-nez v2, :cond_0

    .line 2212676
    invoke-static {p0}, LX/FDV;->a(LX/FDV;)V

    .line 2212677
    :goto_0
    return-object p2

    .line 2212678
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2212679
    invoke-virtual {p3}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6el;

    .line 2212680
    invoke-static {p0, v1, p4}, LX/FDV;->a(LX/FDV;LX/6el;LX/1qM;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2212681
    if-eqz v1, :cond_1

    .line 2212682
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2212683
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2212684
    invoke-static {p0}, LX/FDV;->a(LX/FDV;)V

    goto :goto_0

    .line 2212685
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v1

    invoke-static {v3, v2, v1}, LX/FDV;->a(Ljava/util/List;Lcom/facebook/messaging/model/threads/ThreadsCollection;I)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v1

    .line 2212686
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v2

    .line 2212687
    iput-object v1, v2, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2212688
    move-object v1, v2

    .line 2212689
    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SMS:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2212690
    iput-object v2, v1, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2212691
    move-object v2, v1

    .line 2212692
    iget-object v1, p0, LX/FDV;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Ud;

    invoke-virtual {v1}, LX/2Ud;->a()J

    move-result-wide v4

    .line 2212693
    iput-wide v4, v2, LX/6iK;->j:J

    .line 2212694
    move-object v1, v2

    .line 2212695
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    .line 2212696
    iput-object v2, v1, LX/6iK;->b:LX/6ek;

    .line 2212697
    move-object v1, v1

    .line 2212698
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    .line 2212699
    iput-object v0, v1, LX/6iK;->d:Ljava/util/List;

    .line 2212700
    move-object v0, v1

    .line 2212701
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2212702
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object p2

    goto :goto_0
.end method

.method private a(LX/1qM;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 13

    .prologue
    .line 2212590
    sget-object v0, LX/6ek;->SMS_SPAM:LX/6ek;

    invoke-static {p1, v0}, LX/FDV;->a(LX/1qM;LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v1

    .line 2212591
    iget-object v0, p0, LX/FDV;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNX;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    const/4 v11, 0x0

    .line 2212592
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2212593
    :cond_0
    const/4 v2, 0x0

    .line 2212594
    :goto_0
    move-object v0, v2

    .line 2212595
    return-object v0

    .line 2212596
    :cond_1
    iget-object v2, v0, LX/FNX;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FM8;

    invoke-virtual {v2, v1}, LX/FM8;->a(Lcom/facebook/messaging/model/threads/ThreadsCollection;)LX/FM7;

    move-result-object v2

    .line 2212597
    iget-object v6, v2, LX/FM7;->a:Ljava/lang/String;

    .line 2212598
    iget-boolean v7, v2, LX/FM7;->b:Z

    .line 2212599
    iget v8, v2, LX/FM7;->c:I

    .line 2212600
    if-nez v8, :cond_3

    .line 2212601
    iget-object v2, v0, LX/FNX;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080969

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2212602
    :goto_1
    invoke-virtual {v1, v11}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    iget-wide v4, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 2212603
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadCustomization;->newBuilder()LX/6fr;

    move-result-object v9

    iget-object v2, v0, LX/FNX;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Rc;

    invoke-virtual {v2}, LX/3Rc;->a()I

    move-result v2

    .line 2212604
    iput v2, v9, LX/6fr;->b:I

    .line 2212605
    move-object v2, v9

    .line 2212606
    invoke-virtual {v2}, LX/6fr;->g()Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-result-object v9

    .line 2212607
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v2

    sget-object v10, LX/6ek;->INBOX:LX/6ek;

    .line 2212608
    iput-object v10, v2, LX/6g6;->A:LX/6ek;

    .line 2212609
    move-object v2, v2

    .line 2212610
    sget-object v10, LX/FNX;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2212611
    iput-object v10, v2, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2212612
    move-object v2, v2

    .line 2212613
    iput-boolean v11, v2, LX/6g6;->u:Z

    .line 2212614
    move-object v2, v2

    .line 2212615
    iput-wide v4, v2, LX/6g6;->j:J

    .line 2212616
    move-object v2, v2

    .line 2212617
    iput-wide v4, v2, LX/6g6;->M:J

    .line 2212618
    move-object v2, v2

    .line 2212619
    int-to-long v10, v8

    .line 2212620
    iput-wide v10, v2, LX/6g6;->m:J

    .line 2212621
    move-object v2, v2

    .line 2212622
    if-eqz v7, :cond_2

    const-wide/16 v4, 0x0

    .line 2212623
    :cond_2
    iput-wide v4, v2, LX/6g6;->k:J

    .line 2212624
    move-object v4, v2

    .line 2212625
    iget-object v2, v0, LX/FNX;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FM8;

    invoke-virtual {v2, v3}, LX/FM8;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 2212626
    iput-object v2, v4, LX/6g6;->h:Ljava/util/List;

    .line 2212627
    move-object v2, v4

    .line 2212628
    iput-object v9, v2, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2212629
    move-object v2, v2

    .line 2212630
    iput-object v6, v2, LX/6g6;->o:Ljava/lang/String;

    .line 2212631
    move-object v2, v2

    .line 2212632
    invoke-virtual {v2}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    goto/16 :goto_0

    .line 2212633
    :cond_3
    iget-object v2, v0, LX/FNX;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08096a

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto :goto_1
.end method

.method private static a(LX/FDV;LX/6el;LX/1qM;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2212634
    sget-object v0, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    invoke-static {p0, p1, v0}, LX/FDV;->a(LX/FDV;LX/6el;LX/0rS;)Z

    move-result v0

    .line 2212635
    if-eqz v0, :cond_1

    .line 2212636
    iget-object v0, p0, LX/FDV;->b:LX/2Om;

    invoke-virtual {v0, p1}, LX/2Om;->b(LX/6el;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212637
    :cond_0
    :goto_0
    return-object v0

    .line 2212638
    :cond_1
    sget-object v0, LX/FDU;->a:[I

    invoke-virtual {p1}, LX/6el;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2212639
    sget-object v0, LX/FDV;->a:Ljava/lang/String;

    const-string v1, "unhandled sms aggregation type."

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2212640
    const/4 v0, 0x0

    .line 2212641
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 2212642
    iget-object v1, p0, LX/FDV;->b:LX/2Om;

    iget-wide v2, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    invoke-virtual {v1, p1, v0, v2, v3}, LX/2Om;->a(LX/6el;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    goto :goto_0

    .line 2212643
    :pswitch_0
    invoke-direct {p0, p2}, LX/FDV;->a(LX/1qM;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212644
    if-nez v0, :cond_2

    .line 2212645
    iget-object v1, p0, LX/FDV;->b:LX/2Om;

    sget-object v2, LX/6el;->SPAM:LX/6el;

    invoke-virtual {v1, v2}, LX/2Om;->d(LX/6el;)V

    goto :goto_1

    .line 2212646
    :pswitch_1
    invoke-direct {p0, p2}, LX/FDV;->b(LX/1qM;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212647
    if-nez v0, :cond_2

    .line 2212648
    iget-object v1, p0, LX/FDV;->b:LX/2Om;

    sget-object v2, LX/6el;->BUSINESS:LX/6el;

    invoke-virtual {v1, v2}, LX/2Om;->d(LX/6el;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/util/List;Lcom/facebook/messaging/model/threads/ThreadsCollection;I)Lcom/facebook/messaging/model/threads/ThreadsCollection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            ">;",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;",
            "I)",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;"
        }
    .end annotation

    .prologue
    .line 2212649
    invoke-static {p0}, LX/DoE;->a(Ljava/util/List;)V

    .line 2212650
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {p0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2212651
    const/4 v2, 0x0

    .line 2212652
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2212653
    :cond_0
    :goto_0
    move-object v0, p1

    .line 2212654
    return-object v0

    .line 2212655
    :cond_1
    invoke-static {v0, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/DoE;->a(Ljava/util/Collection;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object p1

    .line 2212656
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->e()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 2212657
    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v1, v1

    .line 2212658
    invoke-virtual {v1, v2, p2}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    .line 2212659
    new-instance p1, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-direct {p1, v1, v2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    goto :goto_0
.end method

.method private static a(LX/1qM;LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;
    .locals 3

    .prologue
    .line 2212660
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v0

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    .line 2212661
    iput-object v1, v0, LX/6iI;->a:LX/0rS;

    .line 2212662
    move-object v0, v0

    .line 2212663
    iput-object p1, v0, LX/6iI;->b:LX/6ek;

    .line 2212664
    move-object v0, v0

    .line 2212665
    sget-object v1, LX/6em;->SMS:LX/6em;

    .line 2212666
    iput-object v1, v0, LX/6iI;->c:LX/6em;

    .line 2212667
    move-object v0, v0

    .line 2212668
    invoke-virtual {v0}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v0

    .line 2212669
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2212670
    const-string v2, "fetchThreadListParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2212671
    new-instance v0, LX/1qK;

    const-string v2, "fetch_thread_list"

    invoke-direct {v0, v2, v1}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2212672
    invoke-interface {p0, v0}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2212484
    new-array v0, v3, [Lcom/facebook/messaging/model/threads/ThreadSummary;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 2212485
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2212486
    new-instance v2, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-direct {v2, v0, v3}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2212487
    sget-object v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_IN_MEMORY_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2212488
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v3

    .line 2212489
    iput-object v0, v3, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2212490
    move-object v0, v3

    .line 2212491
    iput-object p1, v0, LX/6iK;->b:LX/6ek;

    .line 2212492
    move-object v0, v0

    .line 2212493
    iput-object v2, v0, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2212494
    move-object v0, v0

    .line 2212495
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2212496
    iput-object v1, v0, LX/6iK;->d:Ljava/util/List;

    .line 2212497
    move-object v0, v0

    .line 2212498
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2212499
    return-object v0
.end method

.method private a(LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2212500
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 2212501
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2212502
    if-nez v0, :cond_6

    .line 2212503
    const/4 v4, 0x0

    .line 2212504
    :goto_1
    move-object v0, v4

    .line 2212505
    if-eqz v0, :cond_0

    .line 2212506
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2212507
    :cond_0
    sget-object v0, LX/6el;->SPAM:LX/6el;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/6el;->BUSINESS:LX/6el;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2212508
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2212509
    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2212510
    sget-object v0, LX/6el;->SMS:LX/6el;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2212511
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6el;

    .line 2212512
    iget-object v3, p0, LX/FDV;->b:LX/2Om;

    invoke-virtual {v3, v0}, LX/2Om;->e(LX/6el;)V

    goto :goto_2

    .line 2212513
    :cond_4
    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2212514
    iget-object v0, p0, LX/FDV;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ow;

    invoke-virtual {v0, p1}, LX/2Ow;->a(LX/0Px;)V

    .line 2212515
    :cond_5
    return-void

    .line 2212516
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v6

    .line 2212517
    iget-object v4, p0, LX/FDV;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FMI;

    invoke-virtual {v4, v6, v7}, LX/FMI;->a(J)Ljava/util/List;

    move-result-object v4

    .line 2212518
    if-eqz v4, :cond_8

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_8

    .line 2212519
    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2212520
    iget-object v5, p0, LX/FDV;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FNX;

    invoke-virtual {v5, v4}, LX/FNX;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2212521
    sget-object v4, LX/6el;->SPAM:LX/6el;

    goto :goto_1

    .line 2212522
    :cond_7
    iget-object v5, p0, LX/FDV;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FML;

    invoke-virtual {v5, v4}, LX/FML;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2212523
    sget-object v4, LX/6el;->BUSINESS:LX/6el;

    goto/16 :goto_1

    .line 2212524
    :cond_8
    sget-object v4, LX/6el;->SMS:LX/6el;

    goto/16 :goto_1
.end method

.method private static a(LX/FDV;)V
    .locals 2

    .prologue
    .line 2212360
    iget-object v0, p0, LX/FDV;->b:LX/2Om;

    sget-object v1, LX/6el;->SPAM:LX/6el;

    invoke-virtual {v0, v1}, LX/2Om;->d(LX/6el;)V

    .line 2212361
    iget-object v0, p0, LX/FDV;->b:LX/2Om;

    sget-object v1, LX/6el;->BUSINESS:LX/6el;

    invoke-virtual {v0, v1}, LX/2Om;->d(LX/6el;)V

    .line 2212362
    return-void
.end method

.method private static a(LX/FDV;Lcom/facebook/messaging/model/threads/ThreadsCollection;JLX/6el;)V
    .locals 2

    .prologue
    .line 2212363
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212364
    iget-object v1, p0, LX/FDV;->b:LX/2Om;

    invoke-virtual {v1, p4, v0, p2, p3}, LX/2Om;->a(LX/6el;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2212365
    return-void
.end method

.method private static a(LX/FDV;LX/6el;LX/0rS;)Z
    .locals 2

    .prologue
    .line 2212366
    sget-object v0, LX/FDU;->b:[I

    invoke-virtual {p2}, LX/0rS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2212367
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2212368
    :pswitch_0
    iget-object v0, p0, LX/FDV;->b:LX/2Om;

    invoke-virtual {v0, p1}, LX/2Om;->a(LX/6el;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/FDV;
    .locals 8

    .prologue
    .line 2212369
    new-instance v0, LX/FDV;

    invoke-static {p0}, LX/2Om;->a(LX/0QB;)LX/2Om;

    move-result-object v1

    check-cast v1, LX/2Om;

    const/16 v2, 0x29af

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x29b5

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2984

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xcfc

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2982

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xce9

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/FDV;-><init>(LX/2Om;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2212370
    return-object v0
.end method

.method private b(LX/1qM;)Lcom/facebook/messaging/model/threads/ThreadSummary;
    .locals 13

    .prologue
    .line 2212371
    sget-object v0, LX/6ek;->SMS_BUSINESS:LX/6ek;

    invoke-static {p1, v0}, LX/FDV;->a(LX/1qM;LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v1

    .line 2212372
    iget-object v0, p0, LX/FDV;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FML;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    const/4 v11, 0x0

    .line 2212373
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2212374
    :cond_0
    const/4 v2, 0x0

    .line 2212375
    :goto_0
    move-object v0, v2

    .line 2212376
    return-object v0

    .line 2212377
    :cond_1
    iget-object v2, v0, LX/FML;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FM8;

    invoke-virtual {v2, v1}, LX/FM8;->a(Lcom/facebook/messaging/model/threads/ThreadsCollection;)LX/FM7;

    move-result-object v2

    .line 2212378
    iget-object v6, v2, LX/FM7;->a:Ljava/lang/String;

    .line 2212379
    iget-boolean v7, v2, LX/FM7;->b:Z

    .line 2212380
    iget v8, v2, LX/FM7;->c:I

    .line 2212381
    if-nez v8, :cond_3

    .line 2212382
    iget-object v2, v0, LX/FML;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08096b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2212383
    :goto_1
    invoke-virtual {v1, v11}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->a(I)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    iget-wide v4, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    .line 2212384
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadCustomization;->newBuilder()LX/6fr;

    move-result-object v9

    iget-object v2, v0, LX/FML;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3Rc;

    invoke-virtual {v2}, LX/3Rc;->a()I

    move-result v2

    .line 2212385
    iput v2, v9, LX/6fr;->b:I

    .line 2212386
    move-object v2, v9

    .line 2212387
    invoke-virtual {v2}, LX/6fr;->g()Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-result-object v9

    .line 2212388
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v2

    sget-object v10, LX/6ek;->INBOX:LX/6ek;

    .line 2212389
    iput-object v10, v2, LX/6g6;->A:LX/6ek;

    .line 2212390
    move-object v2, v2

    .line 2212391
    sget-object v10, LX/FML;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2212392
    iput-object v10, v2, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2212393
    move-object v2, v2

    .line 2212394
    iput-boolean v11, v2, LX/6g6;->u:Z

    .line 2212395
    move-object v2, v2

    .line 2212396
    iput-wide v4, v2, LX/6g6;->j:J

    .line 2212397
    move-object v2, v2

    .line 2212398
    iput-wide v4, v2, LX/6g6;->M:J

    .line 2212399
    move-object v2, v2

    .line 2212400
    int-to-long v10, v8

    .line 2212401
    iput-wide v10, v2, LX/6g6;->m:J

    .line 2212402
    move-object v2, v2

    .line 2212403
    if-eqz v7, :cond_2

    const-wide/16 v4, 0x0

    .line 2212404
    :cond_2
    iput-wide v4, v2, LX/6g6;->k:J

    .line 2212405
    move-object v4, v2

    .line 2212406
    iget-object v2, v0, LX/FML;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FM8;

    invoke-virtual {v2, v3}, LX/FM8;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 2212407
    iput-object v2, v4, LX/6g6;->h:Ljava/util/List;

    .line 2212408
    move-object v2, v4

    .line 2212409
    iput-object v9, v2, LX/6g6;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2212410
    move-object v2, v2

    .line 2212411
    iput-object v6, v2, LX/6g6;->o:Ljava/lang/String;

    .line 2212412
    move-object v2, v2

    .line 2212413
    invoke-virtual {v2}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    goto/16 :goto_0

    .line 2212414
    :cond_3
    iget-object v2, v0, LX/FML;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08096c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v11

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto :goto_1
.end method

.method private static b(LX/FDV;Lcom/facebook/messaging/model/threads/ThreadSummary;LX/6ek;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;
    .locals 8
    .param p0    # LX/FDV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2212415
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2212416
    if-nez p1, :cond_0

    .line 2212417
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2212418
    :goto_0
    new-instance v4, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    const/4 v1, 0x1

    invoke-direct {v4, v0, v1}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2212419
    new-instance v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SMS:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    iget-object v0, p0, LX/FDV;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ud;

    invoke-virtual {v0}, LX/2Ud;->a()J

    move-result-wide v6

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;LX/0Px;J)V

    return-object v1

    .line 2212420
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2212421
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212422
    const-string v1, "fetchThreadListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2212423
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    move-object v2, v1

    .line 2212424
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2212425
    invoke-virtual {v2}, LX/0Rf;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 2212426
    :goto_0
    return-object v0

    .line 2212427
    :cond_0
    sget-object v3, LX/6el;->SMS:LX/6el;

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2212428
    invoke-direct {p0, v0, v1}, LX/FDV;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/fbservice/service/OperationResult;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2212429
    :cond_1
    invoke-direct {p0, v0, v1, v2, p2}, LX/FDV;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/fbservice/service/OperationResult;LX/0Rf;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2212430
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212431
    const-string v1, "fetchMoreThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;

    .line 2212432
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->f:LX/0Rf;

    move-object v2, v1

    .line 2212433
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2212434
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/0Rf;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v0, v1

    .line 2212435
    :goto_0
    return-object v0

    .line 2212436
    :cond_1
    sget-object v3, LX/6el;->SMS:LX/6el;

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2212437
    invoke-direct {p0, v0, v1}, LX/FDV;->a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/fbservice/service/OperationResult;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2212438
    :cond_2
    invoke-direct {p0, v0, v1, v2, p2}, LX/FDV;->a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/fbservice/service/OperationResult;LX/0Rf;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    .line 2212439
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212440
    const-string v1, "fetchThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadParams;

    .line 2212441
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v0, v1

    .line 2212442
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2212443
    const/4 v8, 0x0

    .line 2212444
    if-nez v0, :cond_1

    .line 2212445
    :goto_0
    move-object v1, v8

    .line 2212446
    if-nez v1, :cond_0

    .line 2212447
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212448
    :goto_1
    return-object v0

    .line 2212449
    :cond_0
    invoke-static {p0, v1, p2}, LX/FDV;->a(LX/FDV;LX/6el;LX/1qM;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2212450
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2212451
    new-instance v3, Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2212452
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2212453
    const/4 v5, 0x1

    invoke-direct {v3, v0, v4, v5}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 2212454
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v0

    sget-object v4, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2212455
    iput-object v4, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2212456
    move-object v4, v0

    .line 2212457
    iget-object v0, p0, LX/FDV;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ud;

    invoke-virtual {v0}, LX/2Ud;->a()J

    move-result-wide v6

    .line 2212458
    iput-wide v6, v4, LX/6iO;->g:J

    .line 2212459
    move-object v0, v4

    .line 2212460
    iput-object v1, v0, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2212461
    move-object v0, v0

    .line 2212462
    iput-object v3, v0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2212463
    move-object v0, v0

    .line 2212464
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2212465
    iput-object v1, v0, LX/6iO;->e:LX/0Px;

    .line 2212466
    move-object v0, v0

    .line 2212467
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2212468
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1

    .line 2212469
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v10

    long-to-int v9, v10

    packed-switch v9, :pswitch_data_0

    goto :goto_0

    .line 2212470
    :pswitch_0
    sget-object v8, LX/6el;->BUSINESS:LX/6el;

    goto :goto_0

    .line 2212471
    :pswitch_1
    sget-object v8, LX/6el;->SMS:LX/6el;

    goto :goto_0

    .line 2212472
    :pswitch_2
    sget-object v8, LX/6el;->SPAM:LX/6el;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x66
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2212473
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212474
    const-string v1, "markThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;

    .line 2212475
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2212476
    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    invoke-direct {p0, v0}, LX/FDV;->a(LX/0Px;)V

    .line 2212477
    return-object v1
.end method

.method public final l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2212478
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212479
    const-string v1, "deleteThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    .line 2212480
    iget-object v1, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v0, v1

    .line 2212481
    invoke-direct {p0, v0}, LX/FDV;->a(LX/0Px;)V

    .line 2212482
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212483
    return-object v0
.end method
