.class public final LX/FXU;
.super LX/1OX;
.source ""


# instance fields
.field private final a:LX/FXs;

.field public final b:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/FXs;)V
    .locals 2

    .prologue
    .line 2255088
    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 2255089
    new-instance v0, LX/01J;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/FXU;->b:LX/01J;

    .line 2255090
    iput-object p1, p0, LX/FXU;->a:LX/FXs;

    .line 2255091
    return-void
.end method

.method public static a$redex0(LX/FXU;Landroid/support/v7/widget/RecyclerView;)V
    .locals 4

    .prologue
    .line 2255092
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2255093
    instance-of v0, v0, LX/FYr;

    if-nez v0, :cond_0

    .line 2255094
    const-string v0, "LoadItemsController"

    const-string v1, "Adapter is not Saved2TailLoadingAdapter!"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2255095
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2255096
    check-cast v0, LX/FYr;

    .line 2255097
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/FYr;->g:Z

    .line 2255098
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2255099
    iget-object v1, p0, LX/FXU;->a:LX/FXs;

    iget-object v2, p0, LX/FXU;->c:Ljava/lang/String;

    new-instance v3, LX/FXT;

    invoke-direct {v3, p0, v0, p1}, LX/FXT;-><init>(LX/FXU;LX/FYr;Landroid/support/v7/widget/RecyclerView;)V

    .line 2255100
    iget-object p0, v1, LX/FXs;->a:LX/FXN;

    iget-object v0, v1, LX/FXs;->d:LX/01J;

    invoke-virtual {v0, v2}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object p1, v1, LX/FXs;->b:LX/FXr;

    invoke-virtual {p1, v3}, LX/FXr;->a(LX/FXS;)LX/FXq;

    move-result-object p1

    invoke-virtual {p0, v2, v0, p1}, LX/FXN;->a(Ljava/lang/String;Ljava/lang/String;LX/FXq;)V

    .line 2255101
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3

    .prologue
    .line 2255102
    invoke-super {p0, p1, p2, p3}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 2255103
    if-nez p2, :cond_0

    iget-object v0, p0, LX/FXU;->b:LX/01J;

    iget-object v1, p0, LX/FXU;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/01J;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_1

    .line 2255104
    :cond_0
    :goto_0
    return-void

    .line 2255105
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 2255106
    invoke-virtual {v0}, LX/1OR;->D()I

    move-result v1

    .line 2255107
    invoke-virtual {v0}, LX/1OR;->v()I

    move-result v2

    .line 2255108
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    .line 2255109
    if-lez v1, :cond_0

    if-ge v2, v1, :cond_0

    add-int/lit8 v0, v0, 0x3

    if-lt v0, v1, :cond_0

    .line 2255110
    iget-object v0, p0, LX/FXU;->b:LX/01J;

    iget-object v1, p0, LX/FXU;->c:Ljava/lang/String;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2255111
    invoke-static {p0, p1}, LX/FXU;->a$redex0(LX/FXU;Landroid/support/v7/widget/RecyclerView;)V

    goto :goto_0
.end method
