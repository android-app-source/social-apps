.class public final LX/F0l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/F0m;


# direct methods
.method public constructor <init>(LX/F0m;Landroid/view/View;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2190515
    iput-object p1, p0, LX/F0l;->c:LX/F0m;

    iput-object p2, p0, LX/F0l;->a:Landroid/view/View;

    iput-object p3, p0, LX/F0l;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 2190516
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2190517
    const v1, 0x7f0d2f04

    if-ne v0, v1, :cond_1

    .line 2190518
    iget-object v0, p0, LX/F0l;->c:LX/F0m;

    iget-object v0, v0, LX/F0m;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    iget-object v1, p0, LX/F0l;->c:LX/F0m;

    iget-object v1, v1, LX/F0m;->a:Lcom/facebook/graphql/model/FeedUnit;

    iget-object v2, p0, LX/F0l;->c:LX/F0m;

    iget-object v2, v2, LX/F0m;->c:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iget-object v3, p0, LX/F0l;->c:LX/F0m;

    iget-object v3, v3, LX/F0m;->d:LX/1Po;

    .line 2190519
    invoke-static {v1}, LX/F0s;->a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v4

    .line 2190520
    invoke-static {v1}, LX/F0s;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2190521
    new-instance v4, LX/F0n;

    invoke-direct {v4, v0, v2}, LX/F0n;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;)V

    move-object v4, v4

    .line 2190522
    :goto_0
    move-object v0, v4

    .line 2190523
    if-eqz v0, :cond_0

    .line 2190524
    iget-object v1, p0, LX/F0l;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2190525
    :cond_0
    :goto_1
    const/4 v0, 0x1

    return v0

    .line 2190526
    :cond_1
    const v1, 0x7f0d2f05

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/F0l;->c:LX/F0m;

    iget-object v0, v0, LX/F0m;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/F0l;->c:LX/F0m;

    iget-object v0, v0, LX/F0m;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    if-eqz v0, :cond_0

    .line 2190527
    iget-object v0, p0, LX/F0l;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2190528
    iget-object v1, p0, LX/F0l;->c:LX/F0m;

    iget-object v1, v1, LX/F0m;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;->h:LX/Es5;

    iget-object v2, p0, LX/F0l;->c:LX/F0m;

    iget-object v2, v2, LX/F0m;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iget-object v3, p0, LX/F0l;->b:Ljava/lang/String;

    sget-object v4, LX/CEz;->PERMALINK:LX/CEz;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/Es5;->a(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Landroid/app/Activity;Ljava/lang/String;LX/CEz;)V

    goto :goto_1

    .line 2190529
    :cond_2
    const/4 v5, 0x1

    .line 2190530
    instance-of p1, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz p1, :cond_5

    .line 2190531
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    .line 2190532
    const-string p1, "friendversary_profile_pictures"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 2190533
    :cond_3
    :goto_2
    move v5, v5

    .line 2190534
    if-eqz v5, :cond_4

    .line 2190535
    const/4 p1, 0x0

    .line 2190536
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    const/4 v5, 0x1

    :goto_3
    move v5, v5

    .line 2190537
    if-eqz v5, :cond_7

    new-instance v5, LX/39x;

    invoke-direct {v5}, LX/39x;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p1

    .line 2190538
    iput-object p1, v5, LX/39x;->t:Ljava/lang/String;

    .line 2190539
    move-object p1, v5

    .line 2190540
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v5

    const/4 v1, 0x0

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 2190541
    iput-object v5, p1, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2190542
    move-object v5, p1

    .line 2190543
    invoke-virtual {v5}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 2190544
    :goto_4
    new-instance p1, LX/F0o;

    invoke-direct {p1, v0, v4, v3, v5}, LX/F0o;-><init>(Lcom/facebook/goodwill/feed/rows/ThrowbackCampaignFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;LX/1Po;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    move-object v4, p1

    .line 2190545
    goto/16 :goto_0

    .line 2190546
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 2190547
    :cond_5
    instance-of p1, v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    if-eqz p1, :cond_6

    .line 2190548
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    .line 2190549
    sget-object p1, LX/F0r;->POLAROID_V1:LX/F0r;

    invoke-virtual {p1}, LX/F0r;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 2190550
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2190551
    :cond_7
    const/4 v5, 0x0

    goto :goto_4

    :cond_8
    move v5, p1

    goto :goto_3
.end method
