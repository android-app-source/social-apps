.class public final LX/GdN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;)V
    .locals 0

    .prologue
    .line 2373814
    iput-object p1, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x27a6fc82

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2373815
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-boolean v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->o:Z

    if-eqz v0, :cond_1

    .line 2373816
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v2, LX/0ig;->bb:LX/0ih;

    const-string v3, "go_to_feed"

    invoke-virtual {v0, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2373817
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    const/4 v2, 0x0

    .line 2373818
    iput-boolean v2, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->o:Z

    .line 2373819
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    const/4 v5, 0x1

    .line 2373820
    iget-object v2, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0pG;

    invoke-virtual {v2}, LX/0pG;->a()LX/0gC;

    move-result-object v2

    .line 2373821
    instance-of v3, v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    if-eqz v3, :cond_0

    .line 2373822
    check-cast v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-virtual {v2}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->P()V

    .line 2373823
    :cond_0
    iget-object v2, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/17X;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget-object v4, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2373824
    sget-object v3, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v3}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v3

    .line 2373825
    const-string v4, "target_tab_name"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2373826
    const-string v3, "jump_to_top"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2373827
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2373828
    const-string v4, "tabbar_target_intent"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2373829
    const-string v4, "extra_launch_uri"

    iget-object v2, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0fU;

    .line 2373830
    iget-object p1, v2, LX/0fU;->n:Ljava/lang/String;

    move-object v2, p1

    .line 2373831
    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2373832
    const-string v2, "POP_TO_ROOT"

    invoke-virtual {v3, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2373833
    iget-object v2, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2373834
    iget-object v2, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0fJ;

    invoke-interface {v2, v3}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 2373835
    iget-object v2, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2373836
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->k:Landroid/widget/Button;

    const v2, 0x7f082eef

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 2373837
    :goto_0
    const v0, -0x5c28a952

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2373838
    :cond_1
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-boolean v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->n:Z

    if-nez v0, :cond_2

    .line 2373839
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v2, LX/0ig;->bb:LX/0ih;

    const-string v3, "start_prefetch"

    invoke-virtual {v0, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2373840
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->a:LX/DC7;

    iget-object v2, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v2, v2, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->p:LX/DC2;

    invoke-interface {v0, v2}, LX/DC7;->a(LX/DC2;)V

    goto :goto_0

    .line 2373841
    :cond_2
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0if;

    sget-object v2, LX/0ig;->bb:LX/0ih;

    const-string v3, "cancel_prefetch"

    invoke-virtual {v0, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2373842
    iget-object v0, p0, LX/GdN;->a:Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;

    iget-object v0, v0, Lcom/facebook/feed/offlinefeed/settings/OfflineFeedSettingsFragment;->a:LX/DC7;

    invoke-interface {v0}, LX/DC7;->a()V

    goto :goto_0
.end method
