.class public LX/F6J;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/5pX;

.field public final b:LX/1Ck;

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3H7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pX;LX/1Ck;LX/0Ot;)V
    .locals 0
    .param p1    # LX/5pX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pX;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/3H7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200343
    iput-object p1, p0, LX/F6J;->a:LX/5pX;

    .line 2200344
    iput-object p2, p0, LX/F6J;->b:LX/1Ck;

    .line 2200345
    iput-object p3, p0, LX/F6J;->c:LX/0Ot;

    .line 2200346
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/5Go;LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/5Go;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2200347
    iget-object v0, p0, LX/F6J;->b:LX/1Ck;

    sget-object v1, LX/F6I;->FETCH_HEADER:LX/F6I;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2200348
    iget-object v0, p0, LX/F6J;->b:LX/1Ck;

    sget-object v1, LX/F6I;->FETCH_PERMALINK_STORY:LX/F6I;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2200349
    iget-object v0, p0, LX/F6J;->a:LX/5pX;

    new-instance v1, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/groups/react/GroupsReactDataFetcher$1;-><init>(LX/F6J;Ljava/lang/String;LX/5Go;LX/0TF;)V

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Runnable;)V

    .line 2200350
    return-void
.end method
