.class public LX/GJg;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2339388
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2339389
    return-void
.end method

.method public static a(LX/0QB;)LX/GJg;
    .locals 1

    .prologue
    .line 2339390
    new-instance v0, LX/GJg;

    invoke-direct {v0}, LX/GJg;-><init>()V

    .line 2339391
    move-object v0, v0

    .line 2339392
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339393
    check-cast p1, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2339394
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2339395
    const/4 v0, 0x0

    .line 2339396
    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 2339397
    if-eqz p2, :cond_0

    .line 2339398
    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setVisibility(I)V

    .line 2339399
    :cond_0
    iget-object v0, p0, LX/GJg;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/GJg;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/GJg;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/GJg;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PAUSED:LX/GGB;

    if-ne v0, v1, :cond_2

    .line 2339400
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080a9b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2339401
    :goto_0
    return-void

    .line 2339402
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080a9c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2339403
    iput-object p1, p0, LX/GJg;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2339404
    return-void
.end method
