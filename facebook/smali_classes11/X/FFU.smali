.class public LX/FFU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zb;

.field public final c:LX/0So;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2217474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2217475
    iput-object p1, p0, LX/FFU;->b:LX/0Zb;

    .line 2217476
    iput-object p2, p0, LX/FFU;->c:LX/0So;

    .line 2217477
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FFU;->a:Ljava/util/Map;

    .line 2217478
    return-void
.end method

.method public static b(LX/0QB;)LX/FFU;
    .locals 3

    .prologue
    .line 2217479
    new-instance v2, LX/FFU;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-direct {v2, v0, v1}, LX/FFU;-><init>(LX/0Zb;LX/0So;)V

    .line 2217480
    return-object v2
.end method
