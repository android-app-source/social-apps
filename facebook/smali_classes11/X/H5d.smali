.class public LX/H5d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/2kk;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/33W;

.field public final b:LX/2jO;


# direct methods
.method public constructor <init>(LX/33W;LX/2jO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2424630
    iput-object p1, p0, LX/H5d;->a:LX/33W;

    .line 2424631
    iput-object p2, p0, LX/H5d;->b:LX/2jO;

    .line 2424632
    return-void
.end method

.method public static a(LX/0QB;)LX/H5d;
    .locals 5

    .prologue
    .line 2424633
    const-class v1, LX/H5d;

    monitor-enter v1

    .line 2424634
    :try_start_0
    sget-object v0, LX/H5d;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424635
    sput-object v2, LX/H5d;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424636
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424637
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424638
    new-instance p0, LX/H5d;

    invoke-static {v0}, LX/33W;->a(LX/0QB;)LX/33W;

    move-result-object v3

    check-cast v3, LX/33W;

    invoke-static {v0}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v4

    check-cast v4, LX/2jO;

    invoke-direct {p0, v3, v4}, LX/H5d;-><init>(LX/33W;LX/2jO;)V

    .line 2424639
    move-object v0, p0

    .line 2424640
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424641
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424642
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424643
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
