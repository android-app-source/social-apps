.class public LX/GeK;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GeI;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2375500
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/GeK;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375501
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2375502
    iput-object p1, p0, LX/GeK;->b:LX/0Ot;

    .line 2375503
    return-void
.end method

.method public static a(LX/0QB;)LX/GeK;
    .locals 4

    .prologue
    .line 2375504
    const-class v1, LX/GeK;

    monitor-enter v1

    .line 2375505
    :try_start_0
    sget-object v0, LX/GeK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2375506
    sput-object v2, LX/GeK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2375507
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2375508
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2375509
    new-instance v3, LX/GeK;

    const/16 p0, 0x20fc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GeK;-><init>(LX/0Ot;)V

    .line 2375510
    move-object v0, v3

    .line 2375511
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2375512
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GeK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375513
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2375514
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2375515
    check-cast p2, LX/GeJ;

    .line 2375516
    iget-object v0, p0, LX/GeK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;

    iget-object v1, p2, LX/GeJ;->a:Lcom/facebook/java2js/JSValue;

    .line 2375517
    const-string v2, "actor"

    invoke-virtual {v1, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    .line 2375518
    const-string p0, "__native_object__"

    invoke-virtual {v2, p0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    const-class p0, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2, p0}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPage;

    .line 2375519
    iget-object p0, v0, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;->b:LX/CE9;

    invoke-virtual {p0, p1}, LX/CE9;->c(LX/1De;)LX/CE8;

    move-result-object p0

    sget-object p2, Lcom/facebook/feedplugins/pyml/rows/components/CSFBFeedActorProfilePictureSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p2}, LX/CE8;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/CE8;

    move-result-object p0

    const p2, 0x7f0b08f9

    invoke-virtual {p0, p2}, LX/CE8;->h(I)LX/CE8;

    move-result-object p0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/CE8;->b(Ljava/lang/String;)LX/CE8;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2375520
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2375521
    invoke-static {}, LX/1dS;->b()V

    .line 2375522
    const/4 v0, 0x0

    return-object v0
.end method
