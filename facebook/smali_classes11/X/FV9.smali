.class public LX/FV9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FV3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FV3",
        "<",
        "LX/FVF;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/FWX;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/FWX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250420
    iput-object p1, p0, LX/FV9;->a:Landroid/content/res/Resources;

    .line 2250421
    iput-object p2, p0, LX/FV9;->b:LX/FWX;

    .line 2250422
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/FVF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2250418
    const-class v0, LX/FVF;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2250417
    iget-object v0, p0, LX/FV9;->a:Landroid/content/res/Resources;

    const v1, 0x7f081ae1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Z
    .locals 4

    .prologue
    .line 2250409
    check-cast p1, LX/FVF;

    .line 2250410
    iget-object v0, p0, LX/FV9;->b:LX/FWX;

    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2250411
    invoke-interface {p1}, LX/FVF;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2250412
    const/4 v2, 0x0

    iget-object v3, v0, LX/FWX;->b:Landroid/content/res/Resources;

    const p0, 0x7f0814fe

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 p0, 0x1

    const/4 p2, 0x0

    invoke-static {v1, v2, v3, p0, p2}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v2

    .line 2250413
    iget-object v3, v0, LX/FWX;->f:LX/BNE;

    invoke-interface {p1}, LX/FVE;->a()Ljava/lang/String;

    move-result-object p0

    new-instance p2, LX/FWW;

    invoke-direct {p2, v0, v2, p1, v1}, LX/FWW;-><init>(LX/FWX;LX/4BY;LX/FVF;Landroid/app/Activity;)V

    invoke-virtual {v3, p0, p2}, LX/BNE;->a(Ljava/lang/String;LX/BND;)V

    .line 2250414
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2250415
    :cond_0
    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2}, LX/FWX;->a$redex0(LX/FWX;LX/FVF;Landroid/app/Activity;LX/55r;)V

    .line 2250416
    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2250408
    iget-object v0, p0, LX/FV9;->a:Landroid/content/res/Resources;

    const v1, 0x7f081adf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2250406
    check-cast p1, LX/FVF;

    .line 2250407
    invoke-interface {p1}, LX/FVF;->c()Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2250405
    const-string v0, "review_button"

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2250404
    const v0, 0x7f0217a2

    return v0
.end method
