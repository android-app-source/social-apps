.class public LX/GGx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;


# static fields
.field public static final p:LX/GGX;

.field private static final q:LX/GGX;


# instance fields
.field public i:LX/GGX;

.field public j:LX/GGX;

.field public k:LX/GGX;

.field public l:LX/GGX;

.field private m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/GH0;

.field public o:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2334610
    new-instance v0, LX/GGu;

    invoke-direct {v0}, LX/GGu;-><init>()V

    sput-object v0, LX/GGx;->p:LX/GGX;

    .line 2334611
    new-instance v0, LX/GGv;

    invoke-direct {v0}, LX/GGv;-><init>()V

    sput-object v0, LX/GGx;->q:LX/GGX;

    return-void
.end method

.method public constructor <init>(LX/GF0;LX/GH0;LX/GEk;LX/GEh;LX/GEw;LX/GIX;LX/GLd;LX/GEz;LX/GEg;LX/GEi;LX/GEX;LX/GEy;LX/GKQ;LX/GEf;LX/GEx;LX/GEu;LX/GEY;LX/GMS;LX/GEb;LX/GEp;LX/GKj;LX/GIG;LX/GJ7;LX/0ad;)V
    .locals 7
    .param p5    # LX/GEw;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostPostBoostComponent;
        .end annotation
    .end param
    .param p13    # LX/GKQ;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
        .end annotation
    .end param
    .param p14    # LX/GEf;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostPostBoostComponent;
        .end annotation
    .end param
    .param p15    # LX/GEx;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostPostBoostComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334621
    new-instance v1, LX/GGq;

    invoke-direct {v1, p0}, LX/GGq;-><init>(LX/GGx;)V

    iput-object v1, p0, LX/GGx;->i:LX/GGX;

    .line 2334622
    new-instance v1, LX/GGr;

    invoke-direct {v1, p0}, LX/GGr;-><init>(LX/GGx;)V

    iput-object v1, p0, LX/GGx;->j:LX/GGX;

    .line 2334623
    new-instance v1, LX/GGs;

    invoke-direct {v1, p0}, LX/GGs;-><init>(LX/GGx;)V

    iput-object v1, p0, LX/GGx;->k:LX/GGX;

    .line 2334624
    new-instance v1, LX/GGt;

    invoke-direct {v1, p0}, LX/GGt;-><init>(LX/GGx;)V

    iput-object v1, p0, LX/GGx;->l:LX/GGX;

    .line 2334625
    move-object/from16 v0, p24

    iput-object v0, p0, LX/GGx;->o:LX/0ad;

    .line 2334626
    iput-object p2, p0, LX/GGx;->n:LX/GH0;

    .line 2334627
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03006e

    sget-object v4, LX/GGx;->e:LX/GGX;

    sget-object v5, LX/8wK;->AD_PREVIEW:LX/8wK;

    move-object/from16 v0, p13

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p20

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p16

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p19

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p15

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03004d

    iget-object v4, p0, LX/GGx;->l:LX/GGX;

    sget-object v5, LX/8wK;->BOOST_TYPE:LX/8wK;

    move-object/from16 v0, p23

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03003f

    iget-object v4, p0, LX/GGx;->k:LX/GGX;

    sget-object v5, LX/8wK;->CALL_TO_ACTION:LX/8wK;

    move-object/from16 v0, p22

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03007b

    const/4 v4, 0x0

    sget-object v5, LX/GGx;->h:LX/GGX;

    sget-object v6, LX/8wK;->BOOST_SLIDESHOW_INFO:LX/8wK;

    invoke-direct {v2, v3, v4, v5, v6}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030094

    iget-object v4, p0, LX/GGx;->j:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v2, v3, p7, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030049

    iget-object v4, p0, LX/GGx;->i:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v2, v3, p6, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03008d

    sget-object v4, LX/GGx;->d:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING_DESCRIPTION:LX/8wK;

    move-object/from16 v0, p18

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030074

    sget-object v4, LX/GGx;->q:LX/GGX;

    sget-object v5, LX/8wK;->PACING:LX/8wK;

    move-object/from16 v0, p21

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030074

    sget-object v4, LX/GGx;->p:LX/GGX;

    sget-object v5, LX/8wK;->PACING:LX/8wK;

    move-object/from16 v0, p21

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p17

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p14

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/GGx;->m:LX/0Px;

    .line 2334628
    return-void
.end method

.method public static b(LX/0QB;)LX/GGx;
    .locals 25

    .prologue
    .line 2334618
    new-instance v0, LX/GGx;

    invoke-static/range {p0 .. p0}, LX/GF0;->a(LX/0QB;)LX/GF0;

    move-result-object v1

    check-cast v1, LX/GF0;

    invoke-static/range {p0 .. p0}, LX/GH0;->a(LX/0QB;)LX/GH0;

    move-result-object v2

    check-cast v2, LX/GH0;

    invoke-static/range {p0 .. p0}, LX/GEk;->a(LX/0QB;)LX/GEk;

    move-result-object v3

    check-cast v3, LX/GEk;

    invoke-static/range {p0 .. p0}, LX/GEh;->a(LX/0QB;)LX/GEh;

    move-result-object v4

    check-cast v4, LX/GEh;

    invoke-static/range {p0 .. p0}, LX/GDG;->a(LX/0QB;)LX/GEw;

    move-result-object v5

    check-cast v5, LX/GEw;

    invoke-static/range {p0 .. p0}, LX/GIX;->a(LX/0QB;)LX/GIX;

    move-result-object v6

    check-cast v6, LX/GIX;

    invoke-static/range {p0 .. p0}, LX/GLd;->b(LX/0QB;)LX/GLd;

    move-result-object v7

    check-cast v7, LX/GLd;

    invoke-static/range {p0 .. p0}, LX/GEz;->a(LX/0QB;)LX/GEz;

    move-result-object v8

    check-cast v8, LX/GEz;

    invoke-static/range {p0 .. p0}, LX/GEg;->a(LX/0QB;)LX/GEg;

    move-result-object v9

    check-cast v9, LX/GEg;

    invoke-static/range {p0 .. p0}, LX/GEi;->a(LX/0QB;)LX/GEi;

    move-result-object v10

    check-cast v10, LX/GEi;

    invoke-static/range {p0 .. p0}, LX/GEX;->a(LX/0QB;)LX/GEX;

    move-result-object v11

    check-cast v11, LX/GEX;

    invoke-static/range {p0 .. p0}, LX/GEy;->a(LX/0QB;)LX/GEy;

    move-result-object v12

    check-cast v12, LX/GEy;

    invoke-static/range {p0 .. p0}, LX/GCJ;->a(LX/0QB;)LX/GKQ;

    move-result-object v13

    check-cast v13, LX/GKQ;

    invoke-static/range {p0 .. p0}, LX/GCh;->a(LX/0QB;)LX/GEf;

    move-result-object v14

    check-cast v14, LX/GEf;

    invoke-static/range {p0 .. p0}, LX/GDJ;->a(LX/0QB;)LX/GEx;

    move-result-object v15

    check-cast v15, LX/GEx;

    invoke-static/range {p0 .. p0}, LX/GEu;->a(LX/0QB;)LX/GEu;

    move-result-object v16

    check-cast v16, LX/GEu;

    invoke-static/range {p0 .. p0}, LX/GEY;->a(LX/0QB;)LX/GEY;

    move-result-object v17

    check-cast v17, LX/GEY;

    invoke-static/range {p0 .. p0}, LX/GMS;->a(LX/0QB;)LX/GMS;

    move-result-object v18

    check-cast v18, LX/GMS;

    invoke-static/range {p0 .. p0}, LX/GEb;->a(LX/0QB;)LX/GEb;

    move-result-object v19

    check-cast v19, LX/GEb;

    invoke-static/range {p0 .. p0}, LX/GEp;->a(LX/0QB;)LX/GEp;

    move-result-object v20

    check-cast v20, LX/GEp;

    invoke-static/range {p0 .. p0}, LX/GKj;->a(LX/0QB;)LX/GKj;

    move-result-object v21

    check-cast v21, LX/GKj;

    invoke-static/range {p0 .. p0}, LX/GIG;->a(LX/0QB;)LX/GIG;

    move-result-object v22

    check-cast v22, LX/GIG;

    invoke-static/range {p0 .. p0}, LX/GJ7;->a(LX/0QB;)LX/GJ7;

    move-result-object v23

    check-cast v23, LX/GJ7;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v24

    check-cast v24, LX/0ad;

    invoke-direct/range {v0 .. v24}, LX/GGx;-><init>(LX/GF0;LX/GH0;LX/GEk;LX/GEh;LX/GEw;LX/GIX;LX/GLd;LX/GEz;LX/GEg;LX/GEi;LX/GEX;LX/GEy;LX/GKQ;LX/GEf;LX/GEx;LX/GEu;LX/GEY;LX/GMS;LX/GEb;LX/GEp;LX/GKj;LX/GIG;LX/GJ7;LX/0ad;)V

    .line 2334619
    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334617
    iget-object v0, p0, LX/GGx;->m:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 6

    .prologue
    .line 2334612
    const-string v0, "storyId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2334613
    const-string v0, "page_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2334614
    invoke-static {p1}, LX/8wJ;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 2334615
    iget-object v0, p0, LX/GGx;->n:LX/GH0;

    sget-object v4, LX/8wL;->BOOST_POST:LX/8wL;

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, LX/GH0;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8wL;LX/GCY;)V

    .line 2334616
    return-void
.end method
