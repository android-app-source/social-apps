.class public final LX/H6V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:LX/H6W;


# direct methods
.method public constructor <init>(LX/H6W;)V
    .locals 0

    .prologue
    .line 2426828
    iput-object p1, p0, LX/H6V;->a:LX/H6W;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 13

    .prologue
    .line 2426829
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2426830
    const v1, 0x7f0d3236

    if-ne v0, v1, :cond_1

    .line 2426831
    iget-object v0, p0, LX/H6V;->a:LX/H6W;

    iget-object v0, v0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->d:LX/H6T;

    iget-object v1, p0, LX/H6V;->a:LX/H6W;

    iget-object v1, v1, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->c:Landroid/app/Activity;

    iget-object v2, p0, LX/H6V;->a:LX/H6W;

    iget-object v2, v2, LX/H6W;->m:LX/H83;

    const-string v3, "wallet"

    invoke-virtual {v0, v1, v2, v3}, LX/H6T;->a(Landroid/content/Context;LX/H83;Ljava/lang/String;)V

    .line 2426832
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2426833
    :cond_1
    const v1, 0x7f0d3235

    if-ne v0, v1, :cond_2

    .line 2426834
    iget-object v0, p0, LX/H6V;->a:LX/H6W;

    iget-object v0, v0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->e:LX/H6c;

    if-eqz v0, :cond_0

    .line 2426835
    iget-object v0, p0, LX/H6V;->a:LX/H6W;

    iget-object v0, v0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->e:LX/H6c;

    iget-object v1, p0, LX/H6V;->a:LX/H6W;

    iget-object v1, v1, LX/H6W;->m:LX/H83;

    .line 2426836
    iget-object v2, v1, LX/H83;->b:LX/H70;

    move-object v1, v2

    .line 2426837
    const/4 v10, 0x1

    .line 2426838
    iget-object v4, v0, LX/H6c;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    .line 2426839
    iput-boolean v10, v4, Lcom/facebook/offers/fragment/OffersWalletFragment;->p:Z

    .line 2426840
    iget-object v4, v0, LX/H6c;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v4, v4, Lcom/facebook/offers/fragment/OffersWalletFragment;->a:LX/H60;

    invoke-interface {v1}, LX/H6y;->me_()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/H6c;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget v7, v6, Lcom/facebook/offers/fragment/OffersWalletFragment;->l:I

    invoke-interface {v1}, LX/H6y;->y()Z

    move-result v6

    if-nez v6, :cond_5

    move v8, v10

    :goto_1
    iget-object v6, v0, LX/H6c;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v6, v6, Lcom/facebook/offers/fragment/OffersWalletFragment;->f:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    move-object v6, v1

    invoke-virtual/range {v4 .. v9}, LX/H60;->a(Ljava/lang/String;LX/H70;IZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2426841
    iget-object v5, v0, LX/H6c;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    invoke-virtual {v5, v10}, Lcom/facebook/offers/fragment/OffersWalletFragment;->a(Z)V

    .line 2426842
    new-instance v5, LX/H6b;

    invoke-direct {v5, v0, v1}, LX/H6b;-><init>(LX/H6c;LX/H70;)V

    invoke-static {v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2426843
    goto :goto_0

    .line 2426844
    :cond_2
    const v1, 0x7f0d3237

    if-ne v0, v1, :cond_0

    .line 2426845
    iget-object v0, p0, LX/H6V;->a:LX/H6W;

    iget-object v0, v0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->f:LX/H6e;

    if-eqz v0, :cond_0

    .line 2426846
    iget-object v0, p0, LX/H6V;->a:LX/H6W;

    iget-object v0, v0, LX/H6W;->l:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->f:LX/H6e;

    iget-object v1, p0, LX/H6V;->a:LX/H6W;

    iget-object v1, v1, LX/H6W;->m:LX/H83;

    .line 2426847
    iget-object v2, v1, LX/H83;->b:LX/H70;

    move-object v1, v2

    .line 2426848
    const/4 v8, 0x1

    .line 2426849
    iget-object v2, v0, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->a:LX/H60;

    invoke-interface {v1}, LX/H6y;->me_()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget v5, v4, Lcom/facebook/offers/fragment/OffersWalletFragment;->l:I

    invoke-interface {v1}, LX/H6y;->A()Z

    move-result v4

    if-nez v4, :cond_6

    move v6, v8

    :goto_2
    iget-object v4, v0, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v4, v4, Lcom/facebook/offers/fragment/OffersWalletFragment;->f:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    move-object v4, v1

    .line 2426850
    const/4 v9, 0x0

    .line 2426851
    if-eqz v4, :cond_3

    .line 2426852
    invoke-static {v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->a(LX/H70;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v9

    invoke-static {v9}, LX/H78;->a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;)LX/H78;

    move-result-object v9

    .line 2426853
    iput-boolean v6, v9, LX/H78;->j:Z

    .line 2426854
    move-object v9, v9

    .line 2426855
    invoke-virtual {v9}, LX/H78;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v9

    .line 2426856
    :cond_3
    new-instance v10, LX/4HS;

    invoke-direct {v10}, LX/4HS;-><init>()V

    .line 2426857
    const-string v11, "offer_claim_id"

    invoke-virtual {v10, v11, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426858
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 2426859
    const-string v12, "enable"

    invoke-virtual {v10, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2426860
    const-string v11, "actor_id"

    invoke-virtual {v10, v11, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426861
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2426862
    const-string v12, "client_mutation_id"

    invoke-virtual {v10, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426863
    new-instance v11, LX/H6j;

    invoke-direct {v11}, LX/H6j;-><init>()V

    move-object v11, v11

    .line 2426864
    const-string v12, "input"

    invoke-virtual {v11, v12, v10}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v10

    const-string v12, "profile_pic_width"

    const/16 p0, 0x64

    div-int/lit8 p1, v5, 0x4

    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v10, v12, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v10

    const-string v12, "creative_img_size"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v10, v12, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2426865
    invoke-static {v11}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v10

    .line 2426866
    if-eqz v9, :cond_4

    .line 2426867
    invoke-virtual {v10, v9}, LX/399;->a(LX/0jT;)LX/399;

    .line 2426868
    :cond_4
    iget-object v9, v2, LX/H60;->a:LX/0tX;

    invoke-virtual {v9, v10}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    move-object v2, v9

    .line 2426869
    iget-object v3, v0, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    invoke-virtual {v3, v8}, Lcom/facebook/offers/fragment/OffersWalletFragment;->a(Z)V

    .line 2426870
    new-instance v3, LX/H6d;

    invoke-direct {v3, v0, v1}, LX/H6d;-><init>(LX/H6e;LX/H70;)V

    invoke-static {v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2426871
    goto/16 :goto_0

    .line 2426872
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 2426873
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_2
.end method
