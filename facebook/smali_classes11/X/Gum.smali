.class public final LX/Gum;
.super LX/Gui;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

.field private c:J


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 2403880
    iput-object p1, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403881
    invoke-direct {p0, p1, p2}, LX/Gui;-><init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V

    .line 2403882
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Gum;->c:J

    .line 2403883
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V
    .locals 2

    .prologue
    .line 2403875
    iget-object v0, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403876
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2403877
    iget-object v1, p0, LX/GuX;->g:LX/BWN;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 2403878
    :cond_0
    :goto_0
    return-void

    .line 2403879
    :cond_1
    invoke-super {p0, p1, p2}, LX/Gui;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;)V
    .locals 3

    .prologue
    .line 2403871
    iget-object v0, p0, LX/GuX;->g:LX/BWN;

    .line 2403872
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403873
    const-string v2, "callback"

    invoke-interface {v0, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gum;->a:Ljava/lang/String;

    .line 2403874
    return-void
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;Landroid/widget/TextView;)V
    .locals 6

    .prologue
    .line 2403839
    invoke-super {p0, p1, p2}, LX/Gui;->a(Lcom/facebook/webview/FacebookWebView;Landroid/widget/TextView;)V

    .line 2403840
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 2403841
    :try_start_0
    const-string v0, "text"

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2403842
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2403843
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2403844
    iget-object v1, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/4 v2, 0x4

    .line 2403845
    invoke-virtual {v1, v2}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->a(I)Landroid/support/v4/app/DialogFragment;

    move-result-object v3

    .line 2403846
    const-string v4, "Cannot create dialog for %s. Check onCreateDialogFragment(int) method"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2403847
    invoke-static {}, LX/0sL;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2403848
    invoke-static {v4, v3}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2403849
    :cond_0
    iget-object v4, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v4, v4

    .line 2403850
    invoke-virtual {v4}, LX/0gc;->a()LX/0hH;

    move-result-object v5

    .line 2403851
    invoke-static {v2}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->g(I)Ljava/lang/String;

    move-result-object p2

    .line 2403852
    invoke-virtual {v4, p2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    .line 2403853
    if-eqz v4, :cond_1

    .line 2403854
    invoke-virtual {v5, v4}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2403855
    :cond_1
    const/16 v4, 0x1001

    invoke-virtual {v5, v4}, LX/0hH;->a(I)LX/0hH;

    .line 2403856
    const/4 v4, 0x1

    invoke-virtual {v3, v5, p2, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 2403857
    iget-object v1, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/Gum;->c:J

    .line 2403858
    iget-object v1, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hx;->a(Z)V

    .line 2403859
    iget-object v1, p0, LX/Gum;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v0, p0}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;Ljava/util/List;LX/BWL;)V

    .line 2403860
    return-void

    .line 2403861
    :catch_0
    move-exception v0

    .line 2403862
    iget-object v2, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v2}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "inconceivable exception "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2403863
    iget-object v0, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e(I)V

    .line 2403864
    iget-wide v0, p0, LX/Gum;->c:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    .line 2403865
    iget-object v0, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    iget-object v1, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->R:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/Gum;->c:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, LX/0hx;->a(J)V

    .line 2403866
    iput-wide v6, p0, LX/Gum;->c:J

    .line 2403867
    :cond_0
    if-eqz p3, :cond_1

    .line 2403868
    iget-object v0, p0, LX/Gum;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083151

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    .line 2403869
    :goto_0
    return-void

    .line 2403870
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, LX/Gui;->a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method
