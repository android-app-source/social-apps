.class public LX/FK7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile m:LX/FK7;


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/0aG;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FK5;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/concurrent/ScheduledExecutorService;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6cq;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/1ML;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private i:Lcom/facebook/messaging/service/model/ModifyThreadParams;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public j:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public volatile l:LX/FK0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2224652
    const-class v0, LX/FK7;

    sput-object v0, LX/FK7;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "LX/FK5;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0Or",
            "<",
            "LX/6cq;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224646
    iput-object p1, p0, LX/FK7;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2224647
    iput-object p2, p0, LX/FK7;->c:LX/0aG;

    .line 2224648
    iput-object p3, p0, LX/FK7;->d:LX/0Or;

    .line 2224649
    iput-object p4, p0, LX/FK7;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2224650
    iput-object p5, p0, LX/FK7;->f:LX/0Or;

    .line 2224651
    return-void
.end method

.method public static a(LX/0QB;)LX/FK7;
    .locals 9

    .prologue
    .line 2224632
    sget-object v0, LX/FK7;->m:LX/FK7;

    if-nez v0, :cond_1

    .line 2224633
    const-class v1, LX/FK7;

    monitor-enter v1

    .line 2224634
    :try_start_0
    sget-object v0, LX/FK7;->m:LX/FK7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2224635
    if-eqz v2, :cond_0

    .line 2224636
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2224637
    new-instance v3, LX/FK7;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    const/16 v6, 0x28ea

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0Xi;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ScheduledExecutorService;

    const/16 v8, 0x273f

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/FK7;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0aG;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;LX/0Or;)V

    .line 2224638
    move-object v0, v3

    .line 2224639
    sput-object v0, LX/FK7;->m:LX/FK7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2224640
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2224641
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2224642
    :cond_1
    sget-object v0, LX/FK7;->m:LX/FK7;

    return-object v0

    .line 2224643
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2224644
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c$redex0(LX/FK7;)V
    .locals 5

    .prologue
    .line 2224630
    iget-object v0, p0, LX/FK7;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/prefs/notifications/ThreadNotificationPrefsSynchronizer$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/prefs/notifications/ThreadNotificationPrefsSynchronizer$1;-><init>(LX/FK7;)V

    iget-wide v2, p0, LX/FK7;->j:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2224631
    return-void
.end method

.method public static declared-synchronized d(LX/FK7;)V
    .locals 4

    .prologue
    .line 2224615
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FK7;->h:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 2224616
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2224617
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/FK7;->g:LX/1ML;

    if-nez v0, :cond_0

    .line 2224618
    iget-object v0, p0, LX/FK7;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2224619
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2224620
    const/4 v0, 0x0

    iput-object v0, p0, LX/FK7;->h:Ljava/util/Map;

    .line 2224621
    invoke-static {p0}, LX/FK7;->g(LX/FK7;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2224622
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2224623
    :cond_2
    :try_start_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2224624
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 2224625
    iput-object v0, p0, LX/FK7;->i:Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2224626
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2224627
    const-string v2, "modifyThreadParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2224628
    iget-object v0, p0, LX/FK7;->c:LX/0aG;

    const-string v2, "modify_thread"

    const v3, 0x56ea8b16

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, LX/FK7;->g:LX/1ML;

    .line 2224629
    iget-object v0, p0, LX/FK7;->g:LX/1ML;

    new-instance v1, LX/FK6;

    invoke-direct {v1, p0}, LX/FK6;-><init>(LX/FK7;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized e(LX/FK7;)V
    .locals 2

    .prologue
    .line 2224540
    monitor-enter p0

    .line 2224541
    :try_start_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/FK7;->g:LX/1ML;

    .line 2224542
    const/4 v0, 0x0

    iput-object v0, p0, LX/FK7;->i:Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2224543
    const-wide/16 v0, 0xfa0

    iput-wide v0, p0, LX/FK7;->j:J

    .line 2224544
    const/4 v0, 0x0

    iput v0, p0, LX/FK7;->k:I

    .line 2224545
    invoke-static {p0}, LX/FK7;->d(LX/FK7;)V

    .line 2224546
    invoke-static {p0}, LX/FK7;->g(LX/FK7;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2224547
    monitor-exit p0

    return-void

    .line 2224548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized f(LX/FK7;)V
    .locals 6

    .prologue
    .line 2224596
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/FK7;->g:LX/1ML;

    .line 2224597
    iget-object v0, p0, LX/FK7;->h:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 2224598
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/FK7;->h:Ljava/util/Map;

    .line 2224599
    :cond_0
    iget-object v0, p0, LX/FK7;->i:Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2224600
    iget-object v1, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v1

    .line 2224601
    iget-object v1, p0, LX/FK7;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2224602
    iget-object v1, p0, LX/FK7;->h:Ljava/util/Map;

    iget-object v2, p0, LX/FK7;->i:Lcom/facebook/messaging/service/model/ModifyThreadParams;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2224603
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, LX/FK7;->i:Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2224604
    iget v0, p0, LX/FK7;->k:I

    int-to-long v0, v0

    const-wide/16 v2, 0x5

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 2224605
    iget v0, p0, LX/FK7;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/FK7;->k:I

    .line 2224606
    const-wide/16 v0, 0x2

    iget-wide v2, p0, LX/FK7;->j:J

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/FK7;->j:J

    .line 2224607
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to update thread notification settings. Retrying in "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/FK7;->j:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2224608
    invoke-static {p0}, LX/FK7;->c$redex0(LX/FK7;)V

    .line 2224609
    :goto_0
    invoke-static {p0}, LX/FK7;->g(LX/FK7;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2224610
    monitor-exit p0

    return-void

    .line 2224611
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, LX/FK7;->k:I

    .line 2224612
    const-wide/16 v0, 0xfa0

    iput-wide v0, p0, LX/FK7;->j:J

    .line 2224613
    const/4 v0, 0x0

    iput-object v0, p0, LX/FK7;->h:Ljava/util/Map;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2224614
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static g(LX/FK7;)V
    .locals 1

    .prologue
    .line 2224592
    iget-object v0, p0, LX/FK7;->l:LX/FK0;

    .line 2224593
    if-eqz v0, :cond_0

    .line 2224594
    invoke-virtual {v0}, LX/FK0;->a()V

    .line 2224595
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2224565
    iget-object v0, p0, LX/FK7;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK5;

    invoke-virtual {v0, p1}, LX/FK5;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/FK4;

    move-result-object v3

    .line 2224566
    monitor-enter p0

    .line 2224567
    :try_start_0
    iget-object v0, p0, LX/FK7;->h:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FK7;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2224568
    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {v3}, LX/FK4;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2224569
    monitor-exit p0

    .line 2224570
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 2224571
    goto :goto_0

    .line 2224572
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2224573
    monitor-enter p0

    .line 2224574
    :try_start_1
    iget-object v0, p0, LX/FK7;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6cq;

    invoke-virtual {v0, p1}, LX/6cq;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2224575
    if-nez v0, :cond_2

    .line 2224576
    sget-object v0, LX/FK7;->a:Ljava/lang/Class;

    const-string v1, "Failed to fetch thread %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2224577
    monitor-exit p0

    goto :goto_1

    .line 2224578
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2224579
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 2224580
    :cond_2
    :try_start_3
    new-instance v0, LX/6ih;

    invoke-direct {v0}, LX/6ih;-><init>()V

    .line 2224581
    iput-object p1, v0, LX/6ih;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2224582
    move-object v0, v0

    .line 2224583
    iget-object v1, v3, LX/FK4;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v0, v1}, LX/6ih;->a(Lcom/facebook/messaging/model/threads/NotificationSetting;)LX/6ih;

    move-result-object v0

    invoke-virtual {v0}, LX/6ih;->u()Lcom/facebook/messaging/service/model/ModifyThreadParams;

    move-result-object v0

    .line 2224584
    iget-object v5, p0, LX/FK7;->h:Ljava/util/Map;

    if-nez v5, :cond_3

    .line 2224585
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    iput-object v5, p0, LX/FK7;->h:Ljava/util/Map;

    .line 2224586
    const-wide/16 v5, 0xfa0

    iput-wide v5, p0, LX/FK7;->j:J

    .line 2224587
    invoke-static {p0}, LX/FK7;->c$redex0(LX/FK7;)V

    .line 2224588
    :cond_3
    iget-object v5, p0, LX/FK7;->h:Ljava/util/Map;

    move-object v1, v5

    .line 2224589
    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2224590
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2224591
    invoke-static {p0}, LX/FK7;->g(LX/FK7;)V

    goto :goto_1
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 2224564
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FK7;->g:LX/1ML;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/FK7;->h:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FK7;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/FK7;->i:Lcom/facebook/messaging/service/model/ModifyThreadParams;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 6

    .prologue
    .line 2224549
    iget-object v0, p0, LX/FK7;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FK5;

    invoke-virtual {v0, p1}, LX/FK5;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/FK4;

    move-result-object v0

    .line 2224550
    invoke-virtual {v0}, LX/FK4;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2224551
    :goto_0
    return-void

    .line 2224552
    :cond_0
    monitor-enter p0

    .line 2224553
    :try_start_0
    iget-object v1, p0, LX/FK7;->i:Lcom/facebook/messaging/service/model/ModifyThreadParams;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/FK7;->i:Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2224554
    iget-object v2, v1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v2

    .line 2224555
    invoke-static {v1, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2224556
    :cond_1
    iget-object v1, p0, LX/FK7;->h:Ljava/util/Map;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/FK7;->h:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2224557
    :cond_2
    iget-object v1, p0, LX/FK7;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2224558
    invoke-static {p1}, LX/0db;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v2

    .line 2224559
    iget-object v0, v0, LX/FK4;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 2224560
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2224561
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2224562
    invoke-static {p0}, LX/FK7;->g(LX/FK7;)V

    goto :goto_0

    .line 2224563
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
