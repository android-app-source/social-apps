.class public LX/GEp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GJ3;

.field private b:LX/0ad;


# direct methods
.method public constructor <init>(LX/GJ3;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332111
    iput-object p1, p0, LX/GEp;->a:LX/GJ3;

    .line 2332112
    iput-object p2, p0, LX/GEp;->b:LX/0ad;

    .line 2332113
    return-void
.end method

.method public static a(LX/0QB;)LX/GEp;
    .locals 3

    .prologue
    .line 2332097
    new-instance v2, LX/GEp;

    .line 2332098
    new-instance v1, LX/GJ3;

    invoke-static {p0}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object v0

    check-cast v0, LX/GMm;

    invoke-direct {v1, v0}, LX/GJ3;-><init>(LX/GMm;)V

    .line 2332099
    move-object v0, v1

    .line 2332100
    check-cast v0, LX/GJ3;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/GEp;-><init>(LX/GJ3;LX/0ad;)V

    .line 2332101
    move-object v0, v2

    .line 2332102
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332109
    const v0, 0x7f030067

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2332105
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v2

    sget-object v3, LX/GGB;->PAUSED:LX/GGB;

    if-ne v2, v3, :cond_0

    .line 2332106
    iget-object v2, p0, LX/GEp;->b:LX/0ad;

    sget-short v3, LX/GDK;->A:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2332107
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2332108
    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332104
    iget-object v0, p0, LX/GEp;->a:LX/GJ3;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332103
    sget-object v0, LX/8wK;->INFO_CARD:LX/8wK;

    return-object v0
.end method
