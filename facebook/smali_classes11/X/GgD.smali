.class public LX/GgD;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GgD",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378710
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2378711
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/GgD;->b:LX/0Zi;

    .line 2378712
    iput-object p1, p0, LX/GgD;->a:LX/0Ot;

    .line 2378713
    return-void
.end method

.method public static a(LX/0QB;)LX/GgD;
    .locals 4

    .prologue
    .line 2378714
    const-class v1, LX/GgD;

    monitor-enter v1

    .line 2378715
    :try_start_0
    sget-object v0, LX/GgD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378716
    sput-object v2, LX/GgD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378717
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378718
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378719
    new-instance v3, LX/GgD;

    const/16 p0, 0x212e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GgD;-><init>(LX/0Ot;)V

    .line 2378720
    move-object v0, v3

    .line 2378721
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378722
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GgD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378723
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;Lcom/facebook/graphql/model/GraphQLNode;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2378725
    const v0, -0x67704e08

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2378726
    check-cast p2, LX/GgC;

    .line 2378727
    iget-object v0, p0, LX/GgD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;

    iget-object v1, p2, LX/GgC;->a:LX/GfY;

    iget-object v2, p2, LX/GgC;->b:LX/1Pp;

    const/4 p0, 0x2

    const/4 v6, 0x0

    const/4 p2, 0x1

    .line 2378728
    iget-object v3, v1, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    .line 2378729
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->p()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;

    move-result-object v4

    .line 2378730
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2378731
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;->a()LX/0Px;

    move-result-object v4

    .line 2378732
    :goto_0
    move-object v4, v4

    .line 2378733
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const v5, 0x7f0b22fa

    invoke-interface {v3, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x6

    invoke-interface {v3, v5, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v5

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->a(LX/1De;)LX/1Di;

    move-result-object v3

    :goto_1
    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a0118

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b22fe

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x7

    const p0, 0x7f0b22ff

    invoke-interface {v5, v6, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v3

    if-gt v3, p2, :cond_1

    invoke-static {p1}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->a(LX/1De;)LX/1Di;

    move-result-object v3

    :goto_2
    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2378734
    return-object v0

    :cond_0
    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v0, p1, v3, v2}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->a(Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pp;)LX/1Di;

    move-result-object v3

    const/4 v6, 0x3

    const p0, 0x7f0b22fd

    invoke-interface {v3, v6, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    goto :goto_1

    :cond_1
    invoke-virtual {v4, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v0, p1, v3, v2}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->a(Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pp;)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b22fd

    invoke-interface {v3, p2, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    goto :goto_2

    .line 2378735
    :cond_2
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2378736
    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2378737
    invoke-static {}, LX/1dS;->b()V

    .line 2378738
    iget v0, p1, LX/1dQ;->b:I

    .line 2378739
    packed-switch v0, :pswitch_data_0

    .line 2378740
    :goto_0
    return-object v3

    .line 2378741
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2378742
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 2378743
    check-cast v2, LX/GgC;

    .line 2378744
    iget-object v4, p0, LX/GgD;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;

    iget-object p1, v2, LX/GgC;->a:LX/GfY;

    .line 2378745
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_0

    .line 2378746
    iget-object p2, v4, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->c:LX/Gfl;

    iget-object p0, p1, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p0

    invoke-virtual {p2, v1, p0}, LX/Gfl;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPage;)V

    .line 2378747
    :goto_1
    goto :goto_0

    .line 2378748
    :cond_0
    new-instance p2, LX/89k;

    invoke-direct {p2}, LX/89k;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object p0

    .line 2378749
    iput-object p0, p2, LX/89k;->b:Ljava/lang/String;

    .line 2378750
    move-object p2, p2

    .line 2378751
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aC()Ljava/lang/String;

    move-result-object p0

    .line 2378752
    iput-object p0, p2, LX/89k;->c:Ljava/lang/String;

    .line 2378753
    move-object p2, p2

    .line 2378754
    invoke-virtual {p2}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object p2

    .line 2378755
    iget-object p0, v4, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->c:LX/Gfl;

    iget-object v2, v4, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlShareComponentSpec;->d:LX/0hy;

    invoke-interface {v2, p2}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object p2

    .line 2378756
    iget-object v2, p0, LX/Gfl;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v2, p2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2378757
    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x67704e08
        :pswitch_0
    .end packed-switch
.end method
