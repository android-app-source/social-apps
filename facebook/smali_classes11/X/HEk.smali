.class public LX/HEk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:LX/HEh;

.field public b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

.field public c:Landroid/view/View;

.field public final d:LX/HEU;


# direct methods
.method public constructor <init>(LX/HEU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2442698
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2442699
    iput-object p1, p0, LX/HEk;->d:LX/HEU;

    .line 2442700
    return-void
.end method

.method public static a(LX/0QB;)LX/HEk;
    .locals 4

    .prologue
    .line 2442701
    const-class v1, LX/HEk;

    monitor-enter v1

    .line 2442702
    :try_start_0
    sget-object v0, LX/HEk;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2442703
    sput-object v2, LX/HEk;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2442704
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2442705
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2442706
    new-instance p0, LX/HEk;

    invoke-static {v0}, LX/HEU;->a(LX/0QB;)LX/HEU;

    move-result-object v3

    check-cast v3, LX/HEU;

    invoke-direct {p0, v3}, LX/HEk;-><init>(LX/HEU;)V

    .line 2442707
    move-object v0, p0

    .line 2442708
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2442709
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HEk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2442710
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2442711
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2442695
    iget-object v0, p0, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;->setVisibility(I)V

    .line 2442696
    iget-object v0, p0, LX/HEk;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2442697
    return-void
.end method
