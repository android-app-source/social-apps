.class public LX/GL2;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/text/NumberFormat;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

.field public d:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field private e:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

.field private f:LX/0W9;

.field private g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field private h:LX/Hed;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;LX/0W9;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 2341978
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2341979
    const-string v0, "current_tab_key"

    iput-object v0, p0, LX/GL2;->a:Ljava/lang/String;

    .line 2341980
    iput-object p1, p0, LX/GL2;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2341981
    iput-object p2, p0, LX/GL2;->f:LX/0W9;

    .line 2341982
    iget-object v0, p0, LX/GL2;->f:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, LX/GL2;->b:Ljava/text/NumberFormat;

    .line 2341983
    iget-object v0, p0, LX/GL2;->b:Ljava/text/NumberFormat;

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 2341984
    iget-object v0, p0, LX/GL2;->b:Ljava/text/NumberFormat;

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 2341985
    return-void
.end method

.method public static b(LX/0QB;)LX/GL2;
    .locals 3

    .prologue
    .line 2341986
    new-instance v2, LX/GL2;

    invoke-static {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->b(LX/0QB;)Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-direct {v2, v0, v1}, LX/GL2;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;LX/0W9;)V

    .line 2341987
    return-object v2
.end method

.method public static b(LX/GL2;LX/Hed;)V
    .locals 3

    .prologue
    .line 2341993
    iput-object p1, p0, LX/GL2;->h:LX/Hed;

    .line 2341994
    iget-object v0, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->setVisibleTab(LX/Hed;)V

    .line 2341995
    sget-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a:LX/Hed;

    if-ne p1, v0, :cond_0

    sget-object v0, LX/GGG;->REGION:LX/GGG;

    .line 2341996
    :goto_0
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2341997
    new-instance v2, LX/GFt;

    invoke-direct {v2, v0}, LX/GFt;-><init>(LX/GGG;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2341998
    return-void

    .line 2341999
    :cond_0
    sget-object v0, LX/GGG;->ADDRESS:LX/GGG;

    goto :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2341988
    iget-object v0, p0, LX/GL2;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    iget-object v1, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    .line 2341989
    iget-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;

    move-object v1, v2

    .line 2341990
    iget-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    move-object v1, v2

    .line 2341991
    iget-object v2, p0, LX/GL2;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2341992
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2342000
    new-instance v0, LX/GL1;

    invoke-direct {v0, p0}, LX/GL1;-><init>(LX/GL2;)V

    .line 2342001
    iget-object v1, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->setRegionSelectorOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342002
    iget-object v0, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    iget-object v1, p0, LX/GL2;->d:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2342003
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v1, v2

    .line 2342004
    sget-object v2, LX/GIr;->n:LX/1jt;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 2342005
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2341972
    invoke-super {p0}, LX/GHg;->a()V

    .line 2341973
    const/4 v0, 0x0

    iput-object v0, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    .line 2341974
    return-void
.end method

.method public final a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2341975
    invoke-super {p0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2341976
    iget-object v0, p0, LX/GL2;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2341977
    return-void
.end method

.method public final a(LX/Hed;)V
    .locals 1

    .prologue
    .line 2341970
    iget-object v0, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->setVisibleTab(LX/Hed;)V

    .line 2341971
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2341965
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2341966
    iget-object v0, p0, LX/GL2;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2341967
    iget-object v0, p0, LX/GL2;->h:LX/Hed;

    if-eqz v0, :cond_0

    .line 2341968
    const-string v0, "current_tab_key"

    iget-object v1, p0, LX/GL2;->h:LX/Hed;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2341969
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341964
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    invoke-virtual {p0, p1, p2}, LX/GL2;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2341958
    iput-object p1, p0, LX/GL2;->d:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2341959
    instance-of v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    .line 2341960
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341961
    iget-object v0, p0, LX/GL2;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2341962
    iput-object p1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341963
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)V
    .locals 6
    .param p1    # Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341955
    if-nez p1, :cond_0

    .line 2341956
    :goto_0
    return-void

    .line 2341957
    :cond_0
    iget-object v0, p0, LX/GL2;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Landroid/location/Location;D)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341943
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2341944
    iput-object p1, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    .line 2341945
    iput-object p2, p0, LX/GL2;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2341946
    iget-object v0, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    new-instance v1, LX/GL0;

    invoke-direct {v1, p0}, LX/GL0;-><init>(LX/GL2;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->setSegmentedTabBarOnClickListener(LX/GKz;)V

    .line 2341947
    invoke-direct {p0}, LX/GL2;->e()V

    .line 2341948
    invoke-direct {p0}, LX/GL2;->d()V

    .line 2341949
    invoke-virtual {p0}, LX/GL2;->b()V

    .line 2341950
    iget-object v1, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    iget-object v0, p0, LX/GL2;->d:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2341951
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v0, v2

    .line 2341952
    sget-object v2, LX/GGG;->REGION:LX/GGG;

    if-ne v0, v2, :cond_0

    sget-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->a:LX/Hed;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->setVisibleTab(LX/Hed;)V

    .line 2341953
    return-void

    .line 2341954
    :cond_0
    sget-object v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->b:LX/Hed;

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2341931
    iget-object v0, p0, LX/GL2;->d:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2341932
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v1, v1

    .line 2341933
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080b36

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2341934
    :goto_0
    iget-object v2, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    .line 2341935
    iget-object v3, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;

    move-object v2, v3

    .line 2341936
    invoke-virtual {v2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;->setAddress(Ljava/lang/String;)V

    .line 2341937
    iget-object v0, p0, LX/GL2;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;

    .line 2341938
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRegionSelectorView;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;

    move-object v2, v2

    .line 2341939
    if-nez v1, :cond_2

    const-wide/16 v0, 0x0

    :goto_1
    invoke-virtual {v2, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressTabView;->setRadius(D)V

    .line 2341940
    return-void

    .line 2341941
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2341942
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v0

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341924
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2341925
    iget-object v0, p0, LX/GL2;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2341926
    if-eqz p1, :cond_0

    .line 2341927
    const-string v0, "current_tab_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Hed;

    iput-object v0, p0, LX/GL2;->h:LX/Hed;

    .line 2341928
    iget-object v0, p0, LX/GL2;->h:LX/Hed;

    if-eqz v0, :cond_0

    .line 2341929
    iget-object v0, p0, LX/GL2;->h:LX/Hed;

    invoke-static {p0, v0}, LX/GL2;->b(LX/GL2;LX/Hed;)V

    .line 2341930
    :cond_0
    return-void
.end method
