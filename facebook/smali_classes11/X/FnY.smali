.class public final LX/FnY;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2285963
    const-class v1, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    const v0, -0x3285b9d0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FundraiserPageHeaderQuery"

    const-string v6, "c067b2807fee7be749ec259f2509f3e5"

    const-string v7, "node"

    const-string v8, "10155207368101729"

    const-string v9, "10155259088851729"

    .line 2285964
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2285965
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2285966
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2285967
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2285968
    sparse-switch v0, :sswitch_data_0

    .line 2285969
    :goto_0
    return-object p1

    .line 2285970
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2285971
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2285972
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2285973
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_3
        0x3052e0ff -> :sswitch_0
        0x5f424068 -> :sswitch_1
        0x7c3416aa -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2285974
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2285975
    :goto_1
    return v0

    .line 2285976
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2285977
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
