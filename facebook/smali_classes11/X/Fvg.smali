.class public final LX/Fvg;
.super LX/Fv4;
.source ""

# interfaces
.implements LX/Fv6;


# instance fields
.field public final synthetic e:LX/Fvi;

.field private f:I

.field private final g:[I

.field public h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/Fvi;)V
    .locals 4

    .prologue
    .line 2301763
    iput-object p1, p0, LX/Fvg;->e:LX/Fvi;

    .line 2301764
    iget-object v0, p1, LX/Fv4;->a:Landroid/content/Context;

    iget-object v1, p1, LX/Fv4;->b:LX/0ad;

    iget-object v2, p1, LX/Fv4;->c:LX/BP0;

    iget-object v3, p1, LX/Fv4;->d:LX/BQ1;

    invoke-direct {p0, v0, v1, v2, v3}, LX/Fv4;-><init>(Landroid/content/Context;LX/0ad;LX/BP0;LX/BQ1;)V

    .line 2301765
    const/4 v0, -0x1

    iput v0, p0, LX/Fvg;->f:I

    .line 2301766
    invoke-static {}, LX/Fvc;->cachedValues()[LX/Fvc;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/Fvg;->g:[I

    .line 2301767
    return-void
.end method

.method public static e(LX/Fvg;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;
    .locals 2

    .prologue
    .line 2301760
    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301761
    iget-object v1, v0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v1

    .line 2301762
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2301707
    invoke-static {}, LX/Fvc;->cachedValues()[LX/Fvc;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2301752
    invoke-static {}, LX/Fvc;->cachedValues()[LX/Fvc;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2301753
    sget-object v1, LX/Fvb;->b:[I

    invoke-virtual {v0}, LX/Fvc;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2301754
    invoke-static {p1}, LX/Fv4;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2301755
    :pswitch_0
    new-instance v0, LX/Fvd;

    iget-object v1, p0, LX/Fv4;->a:Landroid/content/Context;

    iget-object v2, p0, LX/Fvg;->e:LX/Fvi;

    iget-object v2, v2, LX/Fvi;->j:LX/0Ot;

    iget-object v3, p0, LX/Fvg;->e:LX/Fvi;

    iget-object v3, v3, LX/Fvi;->f:LX/Fue;

    .line 2301756
    iget p0, v3, LX/Fue;->A:I

    move v3, p0

    .line 2301757
    invoke-direct {v0, v1, v2, v3}, LX/Fvd;-><init>(Landroid/content/Context;LX/0Ot;I)V

    .line 2301758
    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 2301759
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0314e8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2301751
    invoke-static {}, LX/Fvc;->cachedValues()[LX/Fvc;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a([Z)V
    .locals 5

    .prologue
    .line 2301768
    iget-object v0, p0, LX/Fvg;->e:LX/Fvi;

    invoke-virtual {v0}, LX/Fv4;->getCount()I

    .line 2301769
    iget-object v0, p0, LX/Fvg;->e:LX/Fvi;

    iget-object v0, v0, LX/Fvi;->k:LX/Fw7;

    iget-object v1, p0, LX/Fv4;->c:LX/BP0;

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0, v1, v2}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v0

    .line 2301770
    sget-object v1, LX/Fvc;->NON_SELF_PROFILE_INTRO_CTA:LX/Fvc;

    invoke-virtual {v1}, LX/Fvc;->ordinal()I

    move-result v1

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v2}, LX/BPy;->j()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, LX/Fve;->EXPANDED:LX/Fve;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x0

    .line 2301771
    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301772
    iget-object v3, v2, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v2, v3

    .line 2301773
    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301774
    iget-object v3, v2, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v2, v3

    .line 2301775
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301776
    iget-object v3, v2, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v2, v3

    .line 2301777
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2301778
    :cond_0
    :goto_0
    move v0, v0

    .line 2301779
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    aput-boolean v0, p1, v1

    .line 2301780
    sget-object v0, LX/Fvc;->BOTTOM_PADDING_AND_NUX_PART:LX/Fvc;

    invoke-virtual {v0}, LX/Fvc;->ordinal()I

    move-result v0

    iget-object v1, p0, LX/Fvg;->e:LX/Fvi;

    sget-object v2, LX/Fvh;->INTRO_CARD:LX/Fvh;

    invoke-virtual {v2}, LX/Fvh;->ordinal()I

    move-result v2

    .line 2301781
    iget-object v3, v1, LX/Fv4;->e:[Z

    aget-boolean v3, v3, v2

    move v1, v3

    .line 2301782
    aput-boolean v1, p1, v0

    .line 2301783
    return-void

    .line 2301784
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2301785
    :cond_2
    invoke-static {p0}, LX/Fvg;->e(LX/Fvg;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;

    move-result-object v2

    .line 2301786
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, -0x4b252c60

    if-ne v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2301713
    invoke-static {}, LX/Fvc;->cachedValues()[LX/Fvc;

    move-result-object v0

    aget-object v0, v0, p2

    .line 2301714
    iget-object v2, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    .line 2301715
    iget v3, p0, LX/Fvg;->f:I

    if-ne v3, v2, :cond_0

    iget-object v3, p0, LX/Fvg;->g:[I

    invoke-virtual {v0}, LX/Fvc;->ordinal()I

    move-result v4

    aget v3, v3, v4

    iget-object v4, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301716
    iget v5, v4, LX/BPy;->c:I

    move v4, v5

    .line 2301717
    if-ne v3, v4, :cond_0

    move v0, v1

    .line 2301718
    :goto_0
    return v0

    .line 2301719
    :cond_0
    iput v2, p0, LX/Fvg;->f:I

    .line 2301720
    iget-object v2, p0, LX/Fvg;->g:[I

    invoke-virtual {v0}, LX/Fvc;->ordinal()I

    move-result v3

    iget-object v4, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301721
    iget v5, v4, LX/BPy;->c:I

    move v4, v5

    .line 2301722
    aput v4, v2, v3

    .line 2301723
    sget-object v2, LX/Fvc;->BOTTOM_PADDING_AND_NUX_PART:LX/Fvc;

    if-ne v0, v2, :cond_3

    .line 2301724
    check-cast p1, LX/Fvd;

    .line 2301725
    iget-object v0, p0, LX/Fvg;->e:LX/Fvi;

    iget-object v0, v0, LX/Fvi;->f:LX/Fue;

    .line 2301726
    iget v2, v0, LX/Fue;->A:I

    move v0, v2

    .line 2301727
    iget v2, p1, LX/Fvd;->a:I

    move v2, v2

    .line 2301728
    if-eq v0, v2, :cond_1

    .line 2301729
    iput v0, p1, LX/Fvd;->a:I

    .line 2301730
    invoke-virtual {p1}, LX/Fvd;->requestLayout()V

    .line 2301731
    :cond_1
    iget-object v0, p0, LX/Fvg;->e:LX/Fvi;

    iget-boolean v0, v0, LX/Fvi;->l:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/Fvg;->e:LX/Fvi;

    iget-object v0, v0, LX/Fvi;->i:LX/Fw8;

    iget-object v2, p0, LX/Fv4;->c:LX/BP0;

    iget-object v3, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0, v2, v3}, LX/Fw8;->a(LX/5SB;LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2301732
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/Fvd;->d:Z

    .line 2301733
    invoke-virtual {p1}, LX/Fvd;->invalidate()V

    .line 2301734
    :goto_1
    move v0, v1

    .line 2301735
    goto :goto_0

    .line 2301736
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p1, LX/Fvd;->d:Z

    .line 2301737
    invoke-virtual {p1}, LX/Fvd;->invalidate()V

    .line 2301738
    goto :goto_1

    .line 2301739
    :cond_3
    sget-object v2, LX/Fvc;->NON_SELF_PROFILE_INTRO_CTA:LX/Fvc;

    if-ne v0, v2, :cond_5

    .line 2301740
    iget-object v0, p0, LX/Fvg;->e:LX/Fvi;

    iget-object v0, v0, LX/Fvi;->m:LX/Fv9;

    invoke-virtual {v0}, LX/Fv9;->f()V

    .line 2301741
    invoke-static {p0}, LX/Fvg;->e(LX/Fvg;)Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;

    move-result-object v2

    .line 2301742
    const v0, 0x7f0d2f58

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->d()Ljava/lang/String;

    move-result-object v3

    .line 2301743
    iget-object v4, p0, LX/Fvg;->h:Landroid/view/View$OnClickListener;

    if-nez v4, :cond_4

    .line 2301744
    new-instance v4, LX/Fvf;

    invoke-direct {v4, p0, v3}, LX/Fvf;-><init>(LX/Fvg;Ljava/lang/String;)V

    iput-object v4, p0, LX/Fvg;->h:Landroid/view/View$OnClickListener;

    .line 2301745
    :cond_4
    iget-object v4, p0, LX/Fvg;->h:Landroid/view/View$OnClickListener;

    move-object v3, v4

    .line 2301746
    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2301747
    const v0, 0x7f0d2f58

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2301748
    const v0, 0x7f0d2f57

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$ActionLinksModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 2301749
    goto/16 :goto_0

    .line 2301750
    :cond_5
    invoke-static {p2}, LX/Fv4;->c(I)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2301708
    const/4 v0, -0x1

    iput v0, p0, LX/Fvg;->f:I

    move v0, v1

    .line 2301709
    :goto_0
    iget-object v2, p0, LX/Fvg;->g:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2301710
    iget-object v2, p0, LX/Fvg;->g:[I

    aput v1, v2, v0

    .line 2301711
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2301712
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2301706
    invoke-virtual {p0, p1}, LX/Fv4;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fvc;

    invoke-virtual {v0}, LX/Fvc;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2301705
    invoke-static {}, LX/Fvc;->cachedValues()[LX/Fvc;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
