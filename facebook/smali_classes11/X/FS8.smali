.class public LX/FS8;
.super LX/0Tr;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:LX/FS8;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2241271
    const-string v0, "localphotofaceboxes"

    const-string v1, "detectedphotos"

    const-string v2, "stream_photos"

    const-string v3, "albums"

    const-string v4, "photos"

    const-string v5, "localcropdata"

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/FS8;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/FS6;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241272
    new-instance v0, LX/496;

    const/4 v1, 0x5

    sget-object v2, LX/FS8;->a:LX/0Px;

    invoke-direct {v0, v1, v2}, LX/496;-><init>(ILX/0Px;)V

    invoke-static {p3, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-string v1, "photos_db"

    invoke-direct {p0, p1, p2, v0, v1}, LX/0Tr;-><init>(Landroid/content/Context;LX/0Tt;LX/0Px;Ljava/lang/String;)V

    .line 2241273
    return-void
.end method

.method public static a(LX/0QB;)LX/FS8;
    .locals 6

    .prologue
    .line 2241274
    sget-object v0, LX/FS8;->b:LX/FS8;

    if-nez v0, :cond_1

    .line 2241275
    const-class v1, LX/FS8;

    monitor-enter v1

    .line 2241276
    :try_start_0
    sget-object v0, LX/FS8;->b:LX/FS8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2241277
    if-eqz v2, :cond_0

    .line 2241278
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2241279
    new-instance p0, LX/FS8;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/FS6;->a(LX/0QB;)LX/FS6;

    move-result-object v5

    check-cast v5, LX/FS6;

    invoke-direct {p0, v3, v4, v5}, LX/FS8;-><init>(Landroid/content/Context;LX/0Tt;LX/FS6;)V

    .line 2241280
    move-object v0, p0

    .line 2241281
    sput-object v0, LX/FS8;->b:LX/FS8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2241282
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2241283
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2241284
    :cond_1
    sget-object v0, LX/FS8;->b:LX/FS8;

    return-object v0

    .line 2241285
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2241286
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
