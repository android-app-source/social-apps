.class public LX/Ezb;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Eza;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/Ezl;

.field public b:LX/Ezh;

.field public c:LX/1DI;


# direct methods
.method public constructor <init>(LX/Ezl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2187345
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2187346
    iput-object p1, p0, LX/Ezb;->a:LX/Ezl;

    .line 2187347
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 6

    .prologue
    .line 2187348
    const/4 v4, 0x0

    .line 2187349
    packed-switch p2, :pswitch_data_0

    .line 2187350
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "In onCreateViewHolder, Unexpected view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2187351
    :pswitch_0
    new-instance v0, LX/Eza;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030bfd

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eza;-><init>(Landroid/view/View;)V

    .line 2187352
    :goto_0
    return-object v0

    .line 2187353
    :pswitch_1
    new-instance v0, LX/Eza;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030bfe

    invoke-virtual {v1, v2, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Eza;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2187354
    :pswitch_2
    new-instance v1, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    .line 2187355
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 2187356
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 p0, -0x1

    if-eq v5, p0, :cond_0

    .line 2187357
    const/4 v3, -0x2

    .line 2187358
    :goto_1
    move v3, v3

    .line 2187359
    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2187360
    const v0, 0x7f0a0097

    invoke-virtual {v1, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setBackgroundResource(I)V

    .line 2187361
    new-instance v0, LX/Eza;

    invoke-direct {v0, v1}, LX/Eza;-><init>(Landroid/view/View;)V

    .line 2187362
    invoke-virtual {v0, v4}, LX/1a1;->a(Z)V

    goto :goto_0

    .line 2187363
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p0

    move v5, v3

    :goto_2
    if-ge v3, p0, :cond_1

    .line 2187364
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    add-int/2addr v5, p2

    .line 2187365
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2187366
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result p0

    sub-int/2addr v3, p0

    sub-int/2addr v3, v5

    .line 2187367
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f0b224b

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2187368
    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2187369
    check-cast p1, LX/Eza;

    .line 2187370
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2187371
    :goto_0
    :pswitch_0
    return-void

    .line 2187372
    :pswitch_1
    iget-object v0, p0, LX/Ezb;->a:LX/Ezl;

    invoke-virtual {v0, p2}, LX/Ezl;->a(I)Lcom/facebook/friends/model/PersonYouMayKnow;

    move-result-object v1

    .line 2187373
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance v2, LX/EzZ;

    invoke-direct {v2, p0, p1}, LX/EzZ;-><init>(LX/Ezb;LX/Eza;)V

    .line 2187374
    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2187375
    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->d()Ljava/lang/String;

    move-result-object v3

    .line 2187376
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2187377
    :cond_0
    :goto_1
    const-string v3, ""

    .line 2187378
    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2187379
    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v5, p0, :cond_3

    .line 2187380
    const v3, 0x7f080f85

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2187381
    :cond_1
    :goto_2
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2187382
    const/4 p2, 0x0

    const/4 p1, 0x0

    .line 2187383
    iget-object v3, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v3, v3

    .line 2187384
    check-cast v3, Lcom/facebook/fig/button/FigButton;

    .line 2187385
    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2187386
    sget-object v5, LX/Ezr;->a:[I

    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result p0

    aget v5, v5, p0

    packed-switch v5, :pswitch_data_1

    .line 2187387
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2187388
    :goto_3
    invoke-virtual {v3, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2187389
    const-string v3, "%s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object p0

    aput-object p0, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object p0

    aput-object p0, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2187390
    goto :goto_0

    .line 2187391
    :pswitch_2
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    goto/16 :goto_0

    .line 2187392
    :pswitch_3
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2187393
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v2, p0, LX/Ezb;->c:LX/1DI;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto/16 :goto_0

    .line 2187394
    :cond_2
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTag(Ljava/lang/Object;)V

    .line 2187395
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto :goto_1

    .line 2187396
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v5, p0, :cond_4

    .line 2187397
    iget-object v5, v1, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v5, v5

    .line 2187398
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v5, p0, :cond_4

    .line 2187399
    const v3, 0x7f080f86

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 2187400
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->e()I

    move-result v5

    .line 2187401
    if-lez v5, :cond_1

    .line 2187402
    const v3, 0x7f0f005c

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p0, p1

    invoke-virtual {v4, v3, v5, p0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2187403
    :pswitch_4
    invoke-virtual {v3, p1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2187404
    const/16 v5, 0x12

    invoke-virtual {v3, v5}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2187405
    invoke-virtual {v3, p2}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 2187406
    const v5, 0x7f080f7b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const p0, 0x7f080f7c

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v5, p0}, Lcom/facebook/fig/button/FigButton;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2187407
    const v5, 0x7f080f9c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/button/FigButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2187408
    :pswitch_5
    invoke-virtual {v3, p1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2187409
    const/16 v5, 0x102

    invoke-virtual {v3, v5}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2187410
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 2187411
    const-string p0, "de"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    const-string p0, "ru"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    const-string p0, "id"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    const-string p0, "ja"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    const-string p0, "es"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_6

    const-string p0, "pt"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, 0x1

    :goto_4
    move v5, v5

    .line 2187412
    if-eqz v5, :cond_5

    .line 2187413
    const v5, 0x7f02089f

    invoke-virtual {v3, v5}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2187414
    :goto_5
    const v5, 0x7f080017

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2187415
    const v5, 0x7f080f9e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/button/FigButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2187416
    :cond_5
    invoke-virtual {v3, p2}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    goto :goto_5

    :cond_6
    const/4 v5, 0x0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 2
    .annotation build Lcom/facebook/friending/newuserpromotion/NewUserFriendingAdapter$ViewType;
    .end annotation

    .prologue
    .line 2187417
    iget-object v0, p0, LX/Ezb;->a:LX/Ezl;

    invoke-virtual {v0}, LX/Ezl;->b()LX/Ezj;

    move-result-object v0

    .line 2187418
    if-nez p1, :cond_0

    .line 2187419
    const/4 v1, 0x0

    .line 2187420
    :goto_0
    move v0, v1

    .line 2187421
    return v0

    .line 2187422
    :cond_0
    add-int/lit8 v1, p1, -0x1

    .line 2187423
    iget-object p0, v0, LX/Ezj;->a:LX/Ezl;

    iget-object p0, p0, LX/Ezl;->a:LX/Ezn;

    .line 2187424
    iget-object p1, p0, LX/Ezn;->a:Ljava/util/List;

    move-object p0, p1

    .line 2187425
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-ne v1, p0, :cond_2

    .line 2187426
    iget-object v1, v0, LX/Ezj;->a:LX/Ezl;

    iget-object v1, v1, LX/Ezl;->d:LX/Ezk;

    sget-object p0, LX/Ezk;->LOADING:LX/Ezk;

    if-ne v1, p0, :cond_1

    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    const/4 v1, 0x3

    goto :goto_0

    .line 2187427
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 5

    .prologue
    .line 2187428
    iget-object v0, p0, LX/Ezb;->a:LX/Ezl;

    invoke-virtual {v0}, LX/Ezl;->b()LX/Ezj;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2187429
    iget-object v1, v0, LX/Ezj;->a:LX/Ezl;

    iget-object v1, v1, LX/Ezl;->d:LX/Ezk;

    sget-object v4, LX/Ezk;->LOADING:LX/Ezk;

    if-ne v1, v4, :cond_0

    move v1, v2

    .line 2187430
    :goto_0
    iget-object v4, v0, LX/Ezj;->a:LX/Ezl;

    iget-object v4, v4, LX/Ezl;->d:LX/Ezk;

    sget-object p0, LX/Ezk;->ERROR:LX/Ezk;

    if-ne v4, p0, :cond_1

    .line 2187431
    :goto_1
    iget-object v3, v0, LX/Ezj;->a:LX/Ezl;

    iget-object v3, v3, LX/Ezl;->a:LX/Ezn;

    .line 2187432
    iget-object v4, v3, LX/Ezn;->a:Ljava/util/List;

    move-object v3, v4

    .line 2187433
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    move v0, v1

    .line 2187434
    return v0

    :cond_0
    move v1, v3

    .line 2187435
    goto :goto_0

    :cond_1
    move v2, v3

    .line 2187436
    goto :goto_1
.end method
