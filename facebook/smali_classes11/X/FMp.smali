.class public LX/FMp;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2uq;

.field private final d:LX/1rd;

.field private final e:LX/FN6;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2230866
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "ct_l"

    aput-object v2, v0, v1

    sput-object v0, LX/FMp;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2uq;LX/1rd;LX/FN6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2230867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2230868
    iput-object p1, p0, LX/FMp;->b:Landroid/content/Context;

    .line 2230869
    iput-object p2, p0, LX/FMp;->c:LX/2uq;

    .line 2230870
    iput-object p3, p0, LX/FMp;->d:LX/1rd;

    .line 2230871
    iput-object p4, p0, LX/FMp;->e:LX/FN6;

    .line 2230872
    return-void
.end method

.method private a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2230859
    iget-object v0, p0, LX/FMp;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, LX/FMp;->a:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2230860
    if-eqz v1, :cond_0

    .line 2230861
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2230862
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2230863
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2230864
    :cond_0
    :goto_0
    return-object v3

    .line 2230865
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static b(LX/0QB;)LX/FMp;
    .locals 5

    .prologue
    .line 2230857
    new-instance v4, LX/FMp;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/2uq;->a(LX/0QB;)LX/2uq;

    move-result-object v1

    check-cast v1, LX/2uq;

    invoke-static {p0}, LX/1rd;->b(LX/0QB;)LX/1rd;

    move-result-object v2

    check-cast v2, LX/1rd;

    invoke-static {p0}, LX/FN6;->b(LX/0QB;)LX/FN6;

    move-result-object v3

    check-cast v3, LX/FN6;

    invoke-direct {v4, v0, v1, v2, v3}, LX/FMp;-><init>(Landroid/content/Context;LX/2uq;LX/1rd;LX/FN6;)V

    .line 2230858
    return-object v4
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2230840
    const-string v0, "extra_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2230841
    const-string v1, "extra_repersist_on_error"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 2230842
    const-string v1, "location_url"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2230843
    iget-object v3, p0, LX/FMp;->d:LX/1rd;

    const-string v4, "subscription"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iget-object v5, p0, LX/FMp;->e:LX/FN6;

    invoke-virtual {v5}, LX/FN6;->a()I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/1rd;->a(II)I

    move-result v3

    .line 2230844
    invoke-static {}, LX/FMY;->d()Landroid/net/Uri;

    move-result-object v4

    .line 2230845
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.facebook.messaging.sms.MMS_DOWNLOADED"

    iget-object v7, p0, LX/FMp;->b:Landroid/content/Context;

    const-class v8, LX/FMm;

    invoke-direct {v5, v6, v0, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2230846
    const-string v6, "content_uri"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2230847
    const-string v6, "extra_uri"

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2230848
    const-string v6, "subscription"

    invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2230849
    if-eqz v2, :cond_0

    .line 2230850
    const-string v2, "extra_repersist_on_error"

    const/4 v6, 0x1

    invoke-virtual {v5, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2230851
    :cond_0
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2230852
    invoke-direct {p0, v0}, LX/FMp;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 2230853
    :goto_0
    iget-object v1, p0, LX/FMp;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v6, 0x8000000

    invoke-static {v1, v2, v5, v6}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2230854
    iget-object v2, p0, LX/FMp;->c:LX/2uq;

    invoke-virtual {v2}, LX/2uq;->a()V

    .line 2230855
    iget-object v2, p0, LX/FMp;->b:Landroid/content/Context;

    invoke-static {v3, v2, v0, v4, v1}, LX/EdD;->a(ILandroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;)V

    .line 2230856
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
