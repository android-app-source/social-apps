.class public LX/G4i;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/G4i;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2317936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317937
    return-void
.end method

.method public static a(LX/0QB;)LX/G4i;
    .locals 4

    .prologue
    .line 2317921
    sget-object v0, LX/G4i;->b:LX/G4i;

    if-nez v0, :cond_1

    .line 2317922
    const-class v1, LX/G4i;

    monitor-enter v1

    .line 2317923
    :try_start_0
    sget-object v0, LX/G4i;->b:LX/G4i;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2317924
    if-eqz v2, :cond_0

    .line 2317925
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2317926
    new-instance v3, LX/G4i;

    invoke-direct {v3}, LX/G4i;-><init>()V

    .line 2317927
    const/16 p0, 0xb0b

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2317928
    iput-object p0, v3, LX/G4i;->a:LX/0Or;

    .line 2317929
    move-object v0, v3

    .line 2317930
    sput-object v0, LX/G4i;->b:LX/G4i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2317931
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2317932
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2317933
    :cond_1
    sget-object v0, LX/G4i;->b:LX/G4i;

    return-object v0

    .line 2317934
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2317935
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2317918
    iget-object v0, p0, LX/G4i;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    .line 2317919
    const-string v1, "timeline_fetch_first_units_classic"

    const-string v2, "timeline_fetch_first_units_plutonium"

    const-string v3, "timeline_fetch_section"

    invoke-static {v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2317920
    return-void
.end method
