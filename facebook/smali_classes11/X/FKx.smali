.class public LX/FKx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/model/messages/Message;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/FKo;

.field private final c:LX/FKy;

.field private final d:LX/2Mv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2225742
    const-class v0, LX/FKx;

    sput-object v0, LX/FKx;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/FKo;LX/FKy;LX/2Mv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225738
    iput-object p1, p0, LX/FKx;->b:LX/FKo;

    .line 2225739
    iput-object p2, p0, LX/FKx;->c:LX/FKy;

    .line 2225740
    iput-object p3, p0, LX/FKx;->d:LX/2Mv;

    .line 2225741
    return-void
.end method

.method public static a(LX/0QB;)LX/FKx;
    .locals 1

    .prologue
    .line 2225704
    invoke-static {p0}, LX/FKx;->b(LX/0QB;)LX/FKx;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/FKx;
    .locals 4

    .prologue
    .line 2225735
    new-instance v3, LX/FKx;

    invoke-static {p0}, LX/FKo;->a(LX/0QB;)LX/FKo;

    move-result-object v0

    check-cast v0, LX/FKo;

    invoke-static {p0}, LX/FKy;->b(LX/0QB;)LX/FKy;

    move-result-object v1

    check-cast v1, LX/FKy;

    invoke-static {p0}, LX/2Mv;->b(LX/0QB;)LX/2Mv;

    move-result-object v2

    check-cast v2, LX/2Mv;

    invoke-direct {v3, v0, v1, v2}, LX/FKx;-><init>(LX/FKo;LX/FKy;LX/2Mv;)V

    .line 2225736
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2225707
    check-cast p1, Lcom/facebook/messaging/model/messages/Message;

    .line 2225708
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2225709
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2225710
    const-string v0, "/threads"

    .line 2225711
    :goto_0
    invoke-static {v1, p1}, LX/FKy;->a(Ljava/util/List;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2225712
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "sendMessage"

    .line 2225713
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2225714
    move-object v2, v2

    .line 2225715
    const-string v3, "POST"

    .line 2225716
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2225717
    move-object v2, v2

    .line 2225718
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2225719
    move-object v0, v2

    .line 2225720
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2225721
    move-object v0, v0

    .line 2225722
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2225723
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225724
    move-object v0, v0

    .line 2225725
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2225726
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2225727
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 2225728
    new-instance v0, LX/162;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v4}, LX/162;-><init>(LX/0mC;)V

    .line 2225729
    new-instance v4, Lcom/facebook/user/model/UserFbidIdentifier;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/user/model/UserFbidIdentifier;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, LX/FKo;->a(Lcom/facebook/user/model/UserIdentifier;)LX/0lF;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/162;->a(LX/0lF;)LX/162;

    .line 2225730
    move-object v0, v0

    .line 2225731
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "to"

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225732
    const-string v0, "me/threads"

    goto :goto_0

    .line 2225733
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "id"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v4, v5}, LX/DoA;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225734
    const-string v0, "/messages"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2225705
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2225706
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
