.class public final LX/G2p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/G2x;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;LX/G2x;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2314520
    iput-object p1, p0, LX/G2p;->c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    iput-object p2, p0, LX/G2p;->a:LX/G2x;

    iput-object p3, p0, LX/G2p;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x5ed7cf57

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2314521
    iget-object v1, p0, LX/G2p;->a:LX/G2x;

    .line 2314522
    iget-object v2, v1, LX/G2x;->a:LX/G2o;

    iget v3, v1, LX/G2x;->b:I

    .line 2314523
    iget-object v4, v2, LX/G2o;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2314524
    iget-object v2, v1, LX/G2x;->c:LX/1Pc;

    check-cast v2, LX/1Pq;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, LX/G2x;->a:LX/G2o;

    aput-object v5, v3, v4

    invoke-interface {v2, v3}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 2314525
    iget-object v1, p0, LX/G2p;->c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    iget-object v1, v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->c:LX/2dj;

    iget-object v2, p0, LX/G2p;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2hC;->SELF_PROFILE:LX/2hC;

    invoke-virtual {v1, v2, v3, v4}, LX/2dj;->a(JLX/2hC;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2314526
    iget-object v1, p0, LX/G2p;->c:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;

    iget-object v1, v1, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowBlacklistPartDefinition;->d:LX/2do;

    new-instance v2, LX/2iB;

    iget-object v3, p0, LX/G2p;->b:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, LX/2iB;-><init>(J)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2314527
    const v1, 0xe6f6427

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
