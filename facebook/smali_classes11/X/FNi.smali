.class public LX/FNi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FNi;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/telephony/TelephonyManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/FMl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2233263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2233264
    return-void
.end method

.method public static a(LX/0QB;)LX/FNi;
    .locals 6

    .prologue
    .line 2233265
    sget-object v0, LX/FNi;->d:LX/FNi;

    if-nez v0, :cond_1

    .line 2233266
    const-class v1, LX/FNi;

    monitor-enter v1

    .line 2233267
    :try_start_0
    sget-object v0, LX/FNi;->d:LX/FNi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2233268
    if-eqz v2, :cond_0

    .line 2233269
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2233270
    new-instance p0, LX/FNi;

    invoke-direct {p0}, LX/FNi;-><init>()V

    .line 2233271
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0e7;->b(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    invoke-static {v0}, LX/FMl;->a(LX/0QB;)LX/FMl;

    move-result-object v5

    check-cast v5, LX/FMl;

    .line 2233272
    iput-object v3, p0, LX/FNi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, p0, LX/FNi;->b:Landroid/telephony/TelephonyManager;

    iput-object v5, p0, LX/FNi;->c:LX/FMl;

    .line 2233273
    move-object v0, p0

    .line 2233274
    sput-object v0, LX/FNi;->d:LX/FNi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2233275
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2233276
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2233277
    :cond_1
    sget-object v0, LX/FNi;->d:LX/FNi;

    return-object v0

    .line 2233278
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2233279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2233280
    invoke-static {p0}, LX/FNi;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2233281
    :goto_0
    return v0

    .line 2233282
    :cond_0
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2233283
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 2233284
    const-string v3, "getMobileDataEnabled"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 2233285
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 2233286
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 2233287
    :catch_0
    move-exception v0

    .line 2233288
    const-string v2, "TelephonyUtils"

    const-string v3, "TelephonyManager.getMobileDataEnabled failed"

    invoke-static {v2, v3, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    .line 2233289
    goto :goto_0
.end method

.method private static b(LX/FNi;)Z
    .locals 2

    .prologue
    .line 2233290
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2233291
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FNi;->c:LX/FMl;

    invoke-virtual {v0}, LX/FMl;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2233292
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final c(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2233293
    invoke-static {p0}, LX/FNi;->b(LX/FNi;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2233294
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    .line 2233295
    invoke-static {}, Landroid/telephony/SmsManager;->getDefaultSmsSubscriptionId()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    .line 2233296
    if-eqz v0, :cond_2

    .line 2233297
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getNumber()Ljava/lang/String;

    move-result-object v0

    .line 2233298
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2233299
    iget-object v0, p0, LX/FNi;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 2233300
    :cond_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2233301
    iget-object v0, p0, LX/FNi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/3Kr;->af:LX/0Tn;

    invoke-interface {v0, v2, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2233302
    :cond_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final d(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .prologue
    .line 2233303
    :try_start_0
    invoke-static {p0}, LX/FNi;->b(LX/FNi;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2233304
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    .line 2233305
    invoke-static {}, Landroid/telephony/SmsManager;->getDefaultSmsSubscriptionId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfo(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    .line 2233306
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2233307
    const-string v0, "subscription_manager"

    .line 2233308
    :goto_0
    return-object v0

    .line 2233309
    :cond_0
    iget-object v0, p0, LX/FNi;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2233310
    const-string v0, "sim"

    goto :goto_0

    .line 2233311
    :cond_1
    iget-object v0, p0, LX/FNi;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Kr;->af:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2233312
    const-string v0, "pref_key"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2233313
    :cond_2
    const-string v0, "unknown"

    goto :goto_0

    .line 2233314
    :catch_0
    move-exception v0

    .line 2233315
    const-string v1, "TelephonyUtils"

    const-string v2, "Hit exception when trying to get number source."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2233316
    const-string v0, "error"

    goto :goto_0
.end method
