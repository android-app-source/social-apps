.class public LX/FQc;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FQc;


# instance fields
.field private final a:LX/FQb;

.field private final b:LX/47B;


# direct methods
.method public constructor <init>(LX/FQb;LX/47B;)V
    .locals 3
    .param p2    # LX/47B;
        .annotation runtime Lcom/facebook/pages/deeplinking/uri/PagesManagerRedirect;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2239484
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2239485
    iput-object p1, p0, LX/FQc;->a:LX/FQb;

    .line 2239486
    iput-object p2, p0, LX/FQc;->b:LX/47B;

    .line 2239487
    const-string v0, "pagesmanager/{#%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->NATIVE_PAGES_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2239488
    return-void
.end method

.method public static a(LX/0QB;)LX/FQc;
    .locals 5

    .prologue
    .line 2239489
    sget-object v0, LX/FQc;->c:LX/FQc;

    if-nez v0, :cond_1

    .line 2239490
    const-class v1, LX/FQc;

    monitor-enter v1

    .line 2239491
    :try_start_0
    sget-object v0, LX/FQc;->c:LX/FQc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2239492
    if-eqz v2, :cond_0

    .line 2239493
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2239494
    new-instance p0, LX/FQc;

    invoke-static {v0}, LX/FQb;->a(LX/0QB;)LX/FQb;

    move-result-object v3

    check-cast v3, LX/FQb;

    invoke-static {v0}, LX/FQX;->a(LX/0QB;)LX/47B;

    move-result-object v4

    check-cast v4, LX/47B;

    invoke-direct {p0, v3, v4}, LX/FQc;-><init>(LX/FQb;LX/47B;)V

    .line 2239495
    move-object v0, p0

    .line 2239496
    sput-object v0, LX/FQc;->c:LX/FQc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2239497
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2239498
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2239499
    :cond_1
    sget-object v0, LX/FQc;->c:LX/FQc;

    return-object v0

    .line 2239500
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2239501
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 10

    .prologue
    .line 2239502
    invoke-super {p0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2239503
    if-nez v0, :cond_1

    .line 2239504
    const/4 v0, 0x0

    .line 2239505
    :cond_0
    :goto_0
    return-object v0

    .line 2239506
    :cond_1
    iget-object v1, p0, LX/FQc;->a:LX/FQb;

    .line 2239507
    sget-object v2, LX/FQb;->a:LX/FQa;

    const/4 v4, 0x0

    .line 2239508
    iget-boolean v3, v2, LX/FQa;->a:Z

    if-nez v3, :cond_2

    move v3, v4

    .line 2239509
    :goto_1
    move v2, v3

    .line 2239510
    move v1, v2

    .line 2239511
    if-eqz v1, :cond_0

    .line 2239512
    iget-object v0, p0, LX/FQc;->b:LX/47B;

    invoke-virtual {v0, p1, p2}, LX/47B;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 2239513
    :cond_2
    iget-object v3, v1, LX/FQb;->b:LX/0s6;

    const-string v5, "com.facebook.pages.app"

    invoke-virtual {v3, v5}, LX/01H;->d(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2239514
    iget-object v3, v1, LX/FQb;->e:LX/0Zb;

    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "pma_not_installed"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "pma_deeplinking"

    .line 2239515
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2239516
    move-object v5, v5

    .line 2239517
    invoke-interface {v3, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2239518
    move v3, v4

    .line 2239519
    goto :goto_1

    .line 2239520
    :cond_3
    iget-object v3, v1, LX/FQb;->b:LX/0s6;

    const-string v5, "com.facebook.pages.app"

    .line 2239521
    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, LX/01H;->d(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 2239522
    if-eqz v6, :cond_9

    .line 2239523
    iget-object v6, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 2239524
    :goto_2
    move-object v3, v6

    .line 2239525
    if-eqz v3, :cond_4

    const-string v6, "2.0"

    invoke-static {v3, v6}, LX/14z;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-gez v5, :cond_5

    .line 2239526
    :cond_4
    iget-object v5, v1, LX/FQb;->e:LX/0Zb;

    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "pma_installed_old"

    invoke-direct {v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "pma_deeplinking"

    .line 2239527
    iput-object v2, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2239528
    move-object v6, v6

    .line 2239529
    const-string v2, "pma_version"

    invoke-virtual {v6, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2239530
    move v3, v4

    .line 2239531
    goto :goto_1

    .line 2239532
    :cond_5
    iget-object v5, v1, LX/FQb;->d:LX/8A2;

    iget-object v3, v1, LX/FQb;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, LX/151;->a(Ljava/lang/String;)LX/152;

    move-result-object v3

    .line 2239533
    iget-boolean v5, v2, LX/FQa;->b:Z

    .line 2239534
    iget-boolean v6, v3, LX/152;->a:Z

    if-nez v6, :cond_6

    sget-object v6, LX/03R;->YES:LX/03R;

    iget-object v7, v3, LX/152;->c:LX/03R;

    invoke-virtual {v6, v7}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    :cond_6
    const/4 v6, 0x1

    .line 2239535
    :goto_3
    iget-object v7, v1, LX/FQb;->e:LX/0Zb;

    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v9, "pma_installed"

    invoke-direct {v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v9, "pma_deeplinking"

    .line 2239536
    iput-object v9, v8, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2239537
    move-object v8, v8

    .line 2239538
    const-string v9, "logged_in"

    invoke-virtual {v8, v9, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v8, "same_logged_in_user"

    iget-boolean v9, v3, LX/152;->a:Z

    invoke-virtual {v6, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v8, "logout_okay"

    invoke-virtual {v6, v8, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v7, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2239539
    iget-boolean v5, v3, LX/152;->a:Z

    if-eqz v5, :cond_7

    .line 2239540
    const/4 v3, 0x1

    goto/16 :goto_1

    .line 2239541
    :cond_7
    sget-object v5, LX/03R;->NO:LX/03R;

    iget-object v3, v3, LX/152;->c:LX/03R;

    invoke-virtual {v5, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2239542
    iget-boolean v3, v2, LX/FQa;->b:Z

    goto/16 :goto_1

    :cond_8
    move v3, v4

    .line 2239543
    goto/16 :goto_1

    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 2239544
    :cond_a
    const/4 v6, 0x0

    goto :goto_3
.end method
