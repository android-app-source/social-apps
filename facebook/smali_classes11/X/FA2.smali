.class public LX/FA2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FA3;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0s5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2206583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p1, LX/FA2;

    const/16 v1, 0x250e

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0xb74

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v1, p1, LX/FA2;->a:LX/0Ot;

    iput-object v0, p1, LX/FA2;->b:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x1e17740e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2206584
    const-class v0, LX/FA2;

    invoke-static {v0, p0, p1}, LX/FA2;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 2206585
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 2206586
    const-string v2, "org.torproject.android"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2206587
    iget-object v0, p0, LX/FA2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0s5;

    invoke-virtual {v0}, LX/0s5;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2206588
    const/16 v0, 0x27

    const v2, 0x3fa58d85

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2206589
    :goto_0
    return-void

    .line 2206590
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2206591
    iget-object v0, p0, LX/FA2;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FA3;

    invoke-virtual {v0}, LX/FA3;->b()V

    .line 2206592
    :cond_1
    :goto_1
    const v0, 0x7fd0adec

    invoke-static {v0, v1}, LX/02F;->e(II)V

    goto :goto_0

    .line 2206593
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2206594
    iget-object v0, p0, LX/FA2;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FA3;

    invoke-virtual {v0}, LX/FA3;->a()V

    goto :goto_1
.end method
