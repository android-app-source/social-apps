.class public LX/G10;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/G10;


# instance fields
.field private final a:LX/1DN;

.field private final b:LX/1DM;

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/1DM;LX/1DN;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2310603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310604
    iput-object p1, p0, LX/G10;->b:LX/1DM;

    .line 2310605
    iput-object p2, p0, LX/G10;->a:LX/1DN;

    .line 2310606
    iput-object p3, p0, LX/G10;->c:LX/0ad;

    .line 2310607
    return-void
.end method

.method public static a(LX/0QB;)LX/G10;
    .locals 6

    .prologue
    .line 2310608
    sget-object v0, LX/G10;->d:LX/G10;

    if-nez v0, :cond_1

    .line 2310609
    const-class v1, LX/G10;

    monitor-enter v1

    .line 2310610
    :try_start_0
    sget-object v0, LX/G10;->d:LX/G10;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2310611
    if-eqz v2, :cond_0

    .line 2310612
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2310613
    new-instance p0, LX/G10;

    const-class v3, LX/1DM;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1DM;

    const-class v4, LX/1DN;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1DN;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/G10;-><init>(LX/1DM;LX/1DN;LX/0ad;)V

    .line 2310614
    move-object v0, p0

    .line 2310615
    sput-object v0, LX/G10;->d:LX/G10;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2310616
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2310617
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2310618
    :cond_1
    sget-object v0, LX/G10;->d:LX/G10;

    return-object v0

    .line 2310619
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2310620
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Jg;
    .locals 7

    .prologue
    const/16 v4, 0xa

    .line 2310621
    new-instance v0, LX/1JV;

    iget-object v1, p0, LX/G10;->c:LX/0ad;

    sget v2, LX/0wf;->ai:I

    invoke-interface {v1, v2, v4}, LX/0ad;->a(II)I

    move-result v1

    iget-object v2, p0, LX/G10;->c:LX/0ad;

    sget v3, LX/0wf;->ah:I

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    iget-object v3, p0, LX/G10;->c:LX/0ad;

    sget v4, LX/0wf;->ag:I

    const/16 v5, 0x1e

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    iget-object v4, p0, LX/G10;->c:LX/0ad;

    sget v5, LX/0wf;->af:I

    const/16 v6, 0x28

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/1JV;-><init>(IIII)V

    .line 2310622
    iget-object v1, p0, LX/G10;->a:LX/1DN;

    iget-object v2, p0, LX/G10;->b:LX/1DM;

    invoke-virtual {v2, v0}, LX/1DM;->a(LX/1JV;)LX/1JX;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1DN;->a(LX/1JX;)LX/1Jf;

    move-result-object v0

    return-object v0
.end method
