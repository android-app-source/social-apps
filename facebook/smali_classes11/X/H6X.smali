.class public final LX/H6X;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:LX/H6Y;


# direct methods
.method public constructor <init>(LX/H6Y;)V
    .locals 0

    .prologue
    .line 2427016
    iput-object p1, p0, LX/H6X;->a:LX/H6Y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2427017
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2427018
    const v3, 0x7f0d3238

    if-ne v0, v3, :cond_0

    .line 2427019
    iget-object v0, p0, LX/H6X;->a:LX/H6Y;

    iget-object v0, v0, LX/H6Y;->a:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f081ca6

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    move v0, v1

    .line 2427020
    :goto_0
    iget-object v3, p0, LX/H6X;->a:LX/H6Y;

    iget-object v3, v3, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-boolean v3, v3, Lcom/facebook/offers/fragment/OffersWalletFragment;->m:Z

    if-eq v3, v0, :cond_1

    .line 2427021
    iget-object v3, p0, LX/H6X;->a:LX/H6Y;

    iget-object v3, v3, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v3, v3, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 2427022
    iput-object v4, v3, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    .line 2427023
    iget-object v3, p0, LX/H6X;->a:LX/H6Y;

    iget-object v3, v3, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    .line 2427024
    iput-boolean v0, v3, Lcom/facebook/offers/fragment/OffersWalletFragment;->m:Z

    .line 2427025
    iget-object v0, p0, LX/H6X;->a:LX/H6Y;

    iget-object v0, v0, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v3, p0, LX/H6X;->a:LX/H6Y;

    iget-object v3, v3, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-boolean v3, v3, Lcom/facebook/offers/fragment/OffersWalletFragment;->p:Z

    invoke-static {v0, v3, v1}, Lcom/facebook/offers/fragment/OffersWalletFragment;->a$redex0(Lcom/facebook/offers/fragment/OffersWalletFragment;ZZ)V

    .line 2427026
    :goto_1
    iget-object v0, p0, LX/H6X;->a:LX/H6Y;

    iget-object v0, v0, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    .line 2427027
    iput-boolean v2, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->p:Z

    .line 2427028
    return v1

    .line 2427029
    :cond_0
    iget-object v0, p0, LX/H6X;->a:LX/H6Y;

    iget-object v0, v0, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 2427030
    iput-object v3, v0, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    .line 2427031
    iget-object v0, p0, LX/H6X;->a:LX/H6Y;

    iget-object v0, v0, LX/H6Y;->a:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f081ca7

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    move v0, v2

    goto :goto_0

    .line 2427032
    :cond_1
    iget-object v0, p0, LX/H6X;->a:LX/H6Y;

    iget-object v0, v0, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v3, p0, LX/H6X;->a:LX/H6Y;

    iget-object v3, v3, LX/H6Y;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-boolean v3, v3, Lcom/facebook/offers/fragment/OffersWalletFragment;->p:Z

    invoke-static {v0, v3, v2}, Lcom/facebook/offers/fragment/OffersWalletFragment;->a$redex0(Lcom/facebook/offers/fragment/OffersWalletFragment;ZZ)V

    goto :goto_1
.end method
