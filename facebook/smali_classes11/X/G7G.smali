.class public LX/G7G;
.super LX/121;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final h:Ljava/lang/Object;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:LX/12M;

.field private final e:LX/G7F;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/G7H;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2321195
    const-class v0, LX/G7G;

    sput-object v0, LX/G7G;->b:Ljava/lang/Class;

    .line 2321196
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/G7G;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/12M;LX/0Or;LX/0Ot;)V
    .locals 2
    .param p3    # LX/0Or;
        .annotation build Lcom/facebook/iorg/common/upsell/annotations/dialogprovider/SmsUpsellDialogProvider;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/12M;",
            "LX/0Or",
            "<",
            "LX/6YQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/G7H;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2321188
    invoke-direct {p0}, LX/121;-><init>()V

    .line 2321189
    iput-object p1, p0, LX/G7G;->c:Landroid/content/Context;

    .line 2321190
    iput-object p2, p0, LX/G7G;->d:LX/12M;

    .line 2321191
    new-instance v0, LX/G7F;

    invoke-direct {v0, p0}, LX/G7F;-><init>(LX/G7G;)V

    iput-object v0, p0, LX/G7G;->e:LX/G7F;

    .line 2321192
    iput-object p3, p0, LX/G7G;->f:LX/0Or;

    .line 2321193
    iput-object p4, p0, LX/G7G;->g:LX/0Ot;

    .line 2321194
    return-void
.end method

.method public static a(LX/0QB;)LX/G7G;
    .locals 10

    .prologue
    .line 2321157
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2321158
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2321159
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2321160
    if-nez v1, :cond_0

    .line 2321161
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321162
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2321163
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2321164
    sget-object v1, LX/G7G;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2321165
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2321166
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2321167
    :cond_1
    if-nez v1, :cond_4

    .line 2321168
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2321169
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2321170
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2321171
    new-instance v8, LX/G7G;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0}, LX/12M;->a(LX/0QB;)LX/12M;

    move-result-object v7

    check-cast v7, LX/12M;

    const/16 v9, 0x3939

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 p0, 0x390a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v8, v1, v7, v9, p0}, LX/G7G;-><init>(Landroid/content/Context;LX/12M;LX/0Or;LX/0Ot;)V

    .line 2321172
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2321173
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2321174
    if-nez v1, :cond_2

    .line 2321175
    sget-object v0, LX/G7G;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G7G;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2321176
    :goto_1
    if-eqz v0, :cond_3

    .line 2321177
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2321178
    :goto_3
    check-cast v0, LX/G7G;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2321179
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2321180
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2321181
    :catchall_1
    move-exception v0

    .line 2321182
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2321183
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2321184
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2321185
    :cond_2
    :try_start_8
    sget-object v0, LX/G7G;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G7G;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;
    .locals 1

    .prologue
    .line 2321187
    iget-object v0, p0, LX/G7G;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6YQ;

    invoke-interface {v0, p1, p2, p3}, LX/6YQ;->a(LX/3J6;Ljava/lang/Object;LX/0yY;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2321197
    iget-object v0, p0, LX/G7G;->d:LX/12M;

    iget-object v1, p0, LX/G7G;->e:LX/G7F;

    invoke-virtual {v1}, LX/G7F;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0b4;->a(Ljava/lang/Class;)V

    .line 2321198
    iget-object v0, p0, LX/G7G;->d:LX/12M;

    iget-object v1, p0, LX/G7G;->e:LX/G7F;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2321199
    return-void
.end method

.method public final a(LX/2nT;LX/0yY;)Z
    .locals 1

    .prologue
    .line 2321186
    sget-object v0, LX/0yY;->SMS_THREAD_INTERSTITIAL:LX/0yY;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2321156
    iget-object v0, p0, LX/G7G;->c:Landroid/content/Context;

    const v1, 0x7f080e52

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2321155
    sget-object v0, LX/G7G;->b:Ljava/lang/Class;

    return-object v0
.end method
