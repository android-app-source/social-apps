.class public LX/GeG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/17V;

.field public final b:LX/0Zb;

.field private final c:LX/17Q;

.field private final d:LX/961;


# direct methods
.method public constructor <init>(LX/17V;LX/0Zb;LX/17Q;LX/961;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2375394
    iput-object p1, p0, LX/GeG;->a:LX/17V;

    .line 2375395
    iput-object p2, p0, LX/GeG;->b:LX/0Zb;

    .line 2375396
    iput-object p3, p0, LX/GeG;->c:LX/17Q;

    .line 2375397
    iput-object p4, p0, LX/GeG;->d:LX/961;

    .line 2375398
    return-void
.end method

.method public static b(LX/0QB;)LX/GeG;
    .locals 5

    .prologue
    .line 2375399
    new-instance v4, LX/GeG;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v0

    check-cast v0, LX/17V;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v2

    check-cast v2, LX/17Q;

    invoke-static {p0}, LX/961;->b(LX/0QB;)LX/961;

    move-result-object v3

    check-cast v3, LX/961;

    invoke-direct {v4, v0, v1, v2, v3}, LX/GeG;-><init>(LX/17V;LX/0Zb;LX/17Q;LX/961;)V

    .line 2375400
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPage;LX/162;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2375401
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/GeG;->a(Lcom/facebook/graphql/model/GraphQLPage;ZLX/162;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2375402
    return-void

    .line 2375403
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPage;ZLX/162;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2375404
    iget-object v0, p0, LX/GeG;->d:LX/961;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    const-string v4, "native_newsfeed"

    const-string v5, "pyml_page_like"

    new-instance v9, LX/GeF;

    invoke-direct {v9, p0, p1}, LX/GeF;-><init>(LX/GeG;Lcom/facebook/graphql/model/GraphQLPage;)V

    move v2, p2

    move-object v6, p5

    move-object v7, p3

    invoke-virtual/range {v0 .. v9}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    .line 2375405
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v8, 0x1

    :cond_0
    invoke-static {v0, p3, v8}, LX/17Q;->a(ZLX/0lF;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2375406
    iget-object v1, p0, LX/GeG;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2375407
    return-void
.end method
