.class public LX/F4Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DKH;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0wM;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0Ot;LX/0wM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/DKH;",
            ">;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2196904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2196905
    iput-object p1, p0, LX/F4Q;->a:Landroid/content/res/Resources;

    .line 2196906
    iput-object p2, p0, LX/F4Q;->b:LX/0Ot;

    .line 2196907
    iput-object p3, p0, LX/F4Q;->c:LX/0wM;

    .line 2196908
    return-void
.end method

.method public static a(LX/F4Q;Ljava/lang/String;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2196909
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2196910
    new-instance v1, LX/DaP;

    sget-object v2, LX/F2y;->h:LX/DML;

    invoke-direct {v1, v2}, LX/DaP;-><init>(LX/DML;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2196911
    new-instance v1, LX/F4P;

    sget-object v2, LX/F2y;->a:LX/DML;

    invoke-direct {v1, p0, v2, p1}, LX/F4P;-><init>(LX/F4Q;LX/DML;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2196912
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/F4Q;
    .locals 4

    .prologue
    .line 2196913
    new-instance v2, LX/F4Q;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const/16 v1, 0x2390

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-direct {v2, v0, v3, v1}, LX/F4Q;-><init>(Landroid/content/res/Resources;LX/0Ot;LX/0wM;)V

    .line 2196914
    return-object v2
.end method
