.class public final LX/GL9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

.field public final synthetic b:LX/GLB;


# direct methods
.method public constructor <init>(LX/GLB;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V
    .locals 0

    .prologue
    .line 2342142
    iput-object p1, p0, LX/GL9;->b:LX/GLB;

    iput-object p2, p0, LX/GL9;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x2

    const v0, 0x207a0485

    invoke-static {v7, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2342143
    iget-object v0, p0, LX/GL9;->b:LX/GLB;

    iget-object v0, v0, LX/GLB;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2342144
    iget-object v0, p0, LX/GL9;->b:LX/GLB;

    iget-object v0, v0, LX/GLB;->d:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2342145
    iget-object v0, p0, LX/GL9;->b:LX/GLB;

    iget-object v2, v0, LX/GLB;->l:LX/1Ck;

    const-string v3, "fetch_saved_audiences_task_key"

    iget-object v0, p0, LX/GL9;->b:LX/GLB;

    iget-object v4, v0, LX/GLB;->k:LX/GEK;

    iget-object v0, p0, LX/GL9;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, LX/GL9;->b:LX/GLB;

    iget-object v0, v0, LX/GLB;->f:Ljava/util/List;

    iget-object v6, p0, LX/GL9;->b:LX/GLB;

    iget-object v6, v6, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    .line 2342146
    iget-object v6, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->a:Ljava/lang/String;

    move-object v0, v6

    .line 2342147
    const/4 v6, 0x5

    .line 2342148
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2342149
    new-instance v8, LX/ABs;

    invoke-direct {v8}, LX/ABs;-><init>()V

    move-object v8, v8

    .line 2342150
    const-string v9, "account_id"

    invoke-virtual {v8, v9, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    const-string v9, "fetch_saved_audiences"

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v8, v9, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v8

    const-string v9, "num_of_saved_audiences_to_fetch"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v8, v9, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v8

    const-string v9, "saved_audience_cursor"

    invoke-virtual {v8, v9, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    move-object v8, v8

    .line 2342151
    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    move-object v8, v8

    .line 2342152
    iget-object v9, v4, LX/GEK;->a:LX/0tX;

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    move-object v0, v8

    .line 2342153
    new-instance v4, LX/GL8;

    invoke-direct {v4, p0}, LX/GL8;-><init>(LX/GL9;)V

    invoke-virtual {v2, v3, v0, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2342154
    const v0, 0x1e7f38b4

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
