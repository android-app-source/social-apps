.class public LX/Fx3;
.super LX/Fwg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Fwg",
        "<",
        "LX/3l9;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static volatile b:LX/Fx3;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2304192
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE_INTRO_CAR_FAV_PHOTOS_WYSWIG_EDITOR_DRAG_AND_DROP:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Fx3;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304190
    invoke-direct {p0, p1}, LX/Fwg;-><init>(LX/0iA;)V

    .line 2304191
    return-void
.end method

.method public static a(LX/0QB;)LX/Fx3;
    .locals 4

    .prologue
    .line 2304193
    sget-object v0, LX/Fx3;->b:LX/Fx3;

    if-nez v0, :cond_1

    .line 2304194
    const-class v1, LX/Fx3;

    monitor-enter v1

    .line 2304195
    :try_start_0
    sget-object v0, LX/Fx3;->b:LX/Fx3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2304196
    if-eqz v2, :cond_0

    .line 2304197
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2304198
    new-instance p0, LX/Fx3;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-direct {p0, v3}, LX/Fx3;-><init>(LX/0iA;)V

    .line 2304199
    move-object v0, p0

    .line 2304200
    sput-object v0, LX/Fx3;->b:LX/Fx3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2304201
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2304202
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2304203
    :cond_1
    sget-object v0, LX/Fx3;->b:LX/Fx3;

    return-object v0

    .line 2304204
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2304205
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2304189
    const-string v0, "4270"

    return-object v0
.end method

.method public final b()Lcom/facebook/interstitial/manager/InterstitialTrigger;
    .locals 1

    .prologue
    .line 2304188
    sget-object v0, LX/Fx3;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-object v0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 2304187
    const-class v0, LX/3l9;

    return-object v0
.end method
