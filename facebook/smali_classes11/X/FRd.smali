.class public final LX/FRd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/settings/protocol/GetPayAccountResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FRt;

.field public final synthetic b:LX/6zj;

.field public final synthetic c:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

.field public final synthetic d:LX/FRg;


# direct methods
.method public constructor <init>(LX/FRg;LX/FRt;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V
    .locals 0

    .prologue
    .line 2240554
    iput-object p1, p0, LX/FRd;->d:LX/FRg;

    iput-object p2, p0, LX/FRd;->a:LX/FRt;

    iput-object p3, p0, LX/FRd;->b:LX/6zj;

    iput-object p4, p0, LX/FRd;->c:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2240555
    iget-object v0, p0, LX/FRd;->d:LX/FRg;

    iget-object v1, p0, LX/FRd;->b:LX/6zj;

    iget-object v2, p0, LX/FRd;->c:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-static {v0, v1, v2}, LX/FRg;->b(LX/FRg;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    .line 2240556
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2240557
    check-cast p1, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;

    .line 2240558
    iget-object v0, p0, LX/FRd;->a:LX/FRt;

    iget-object v1, p1, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2240559
    iput-object v1, v0, LX/FRt;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2240560
    iget-object v0, p0, LX/FRd;->a:LX/FRt;

    iget v1, p1, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;->b:I

    .line 2240561
    iput v1, v0, LX/FRt;->e:I

    .line 2240562
    return-void
.end method
