.class public LX/GPk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/GPk;


# instance fields
.field private final a:LX/GPc;

.field private final b:LX/GPW;

.field private final c:LX/GPa;

.field public final d:LX/GPp;

.field public final e:LX/GPe;

.field public final f:LX/GPm;

.field private final g:LX/GPj;


# direct methods
.method public constructor <init>(LX/GPc;LX/GPW;LX/GPa;LX/GPp;LX/GPe;LX/GPm;LX/GPj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349402
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2349403
    iput-object p1, p0, LX/GPk;->a:LX/GPc;

    .line 2349404
    iput-object p2, p0, LX/GPk;->b:LX/GPW;

    .line 2349405
    iput-object p3, p0, LX/GPk;->c:LX/GPa;

    .line 2349406
    iput-object p4, p0, LX/GPk;->d:LX/GPp;

    .line 2349407
    iput-object p5, p0, LX/GPk;->e:LX/GPe;

    .line 2349408
    iput-object p6, p0, LX/GPk;->f:LX/GPm;

    .line 2349409
    iput-object p7, p0, LX/GPk;->g:LX/GPj;

    .line 2349410
    return-void
.end method

.method public static a(LX/0QB;)LX/GPk;
    .locals 11

    .prologue
    .line 2349411
    sget-object v0, LX/GPk;->h:LX/GPk;

    if-nez v0, :cond_1

    .line 2349412
    const-class v1, LX/GPk;

    monitor-enter v1

    .line 2349413
    :try_start_0
    sget-object v0, LX/GPk;->h:LX/GPk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2349414
    if-eqz v2, :cond_0

    .line 2349415
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2349416
    new-instance v3, LX/GPk;

    invoke-static {v0}, LX/GPc;->a(LX/0QB;)LX/GPc;

    move-result-object v4

    check-cast v4, LX/GPc;

    invoke-static {v0}, LX/GPW;->b(LX/0QB;)LX/GPW;

    move-result-object v5

    check-cast v5, LX/GPW;

    invoke-static {v0}, LX/GPa;->b(LX/0QB;)LX/GPa;

    move-result-object v6

    check-cast v6, LX/GPa;

    invoke-static {v0}, LX/GPp;->b(LX/0QB;)LX/GPp;

    move-result-object v7

    check-cast v7, LX/GPp;

    invoke-static {v0}, LX/GPe;->a(LX/0QB;)LX/GPe;

    move-result-object v8

    check-cast v8, LX/GPe;

    invoke-static {v0}, LX/GPm;->b(LX/0QB;)LX/GPm;

    move-result-object v9

    check-cast v9, LX/GPm;

    invoke-static {v0}, LX/GPj;->a(LX/0QB;)LX/GPj;

    move-result-object v10

    check-cast v10, LX/GPj;

    invoke-direct/range {v3 .. v10}, LX/GPk;-><init>(LX/GPc;LX/GPW;LX/GPa;LX/GPp;LX/GPe;LX/GPm;LX/GPj;)V

    .line 2349417
    move-object v0, v3

    .line 2349418
    sput-object v0, LX/GPk;->h:LX/GPk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2349419
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2349420
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2349421
    :cond_1
    sget-object v0, LX/GPk;->h:LX/GPk;

    return-object v0

    .line 2349422
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2349423
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/6y8;Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/6xg;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6y8;",
            "Lcom/facebook/common/locale/Country;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/6xg;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2349424
    new-instance v0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;

    iget-object v1, p1, LX/6y8;->a:Ljava/lang/String;

    iget v2, p1, LX/6y8;->c:I

    iget v3, p1, LX/6y8;->d:I

    iget-object v4, p1, LX/6y8;->e:Ljava/lang/String;

    iget-object v5, p1, LX/6y8;->f:Ljava/lang/String;

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Lcom/facebook/common/locale/Country;Ljava/lang/String;Ljava/lang/String;LX/6xg;Z)V

    .line 2349425
    iget-object v1, p0, LX/GPk;->b:LX/GPW;

    invoke-virtual {v1, v0}, LX/6sU;->a(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/6xg;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/common/locale/Country;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/6xg;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Lcom/facebook/common/locale/Country;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2349426
    new-instance v0, Lcom/facebook/adspayments/protocol/EditPaymentCardParams;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/adspayments/protocol/EditPaymentCardParams;-><init>(Ljava/lang/String;LX/6xg;Ljava/lang/String;IILjava/lang/String;Lcom/facebook/common/locale/Country;)V

    .line 2349427
    iget-object v1, p0, LX/GPk;->c:LX/GPa;

    invoke-virtual {v1, v0}, LX/6sU;->a(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/PaymentOption;Lcom/facebook/payments/currency/CurrencyAmount;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/adspayments/protocol/CvvPrepayData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2349428
    iget-object v0, p0, LX/GPk;->g:LX/GPj;

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, p2, p3, v1}, Lcom/facebook/common/util/Quartet;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/common/util/Quartet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/adspayments/model/BusinessAddressDetails;",
            "Ljava/lang/String;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2349429
    new-instance v0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZ)V

    .line 2349430
    iget-object v1, p0, LX/GPk;->f:LX/GPm;

    invoke-virtual {v1, v0}, LX/6u5;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZLjava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/adspayments/model/BusinessAddressDetails;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/lang/String;",
            "Lcom/facebook/adspayments/model/BusinessAddressDetails;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2349431
    new-instance v0, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/facebook/adspayments/protocol/PostBusinessAddressParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;ZZLjava/lang/String;Lcom/facebook/adspayments/model/BusinessAddressDetails;Ljava/lang/String;Z)V

    .line 2349432
    iget-object v1, p0, LX/GPk;->f:LX/GPm;

    invoke-virtual {v1, v0}, LX/6u5;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
