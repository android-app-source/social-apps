.class public LX/Fau;
.super LX/0hs;
.source ""


# instance fields
.field public a:Landroid/widget/FrameLayout;

.field public l:Lcom/facebook/widget/text/BetterTextView;

.field public m:Lcom/facebook/widget/text/BetterTextView;

.field public n:Lcom/facebook/fbui/glyph/GlyphView;

.field public o:Lcom/facebook/fbui/glyph/GlyphView;

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2259303
    const/4 v0, 0x1

    const v1, 0x7f031276

    invoke-direct {p0, p1, v0, v1}, LX/Fau;-><init>(Landroid/content/Context;II)V

    .line 2259304
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .prologue
    .line 2259363
    invoke-direct {p0, p1, p2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2259364
    const/4 p2, 0x0

    .line 2259365
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2259366
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 2259367
    const v0, 0x7f0d2b60

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Fau;->a:Landroid/widget/FrameLayout;

    .line 2259368
    const v0, 0x7f0d2b62

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Fau;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 2259369
    const v0, 0x7f0d2b63

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Fau;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 2259370
    const v0, 0x7f0d2b64

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Fau;->n:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2259371
    const v0, 0x7f0d0632

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Fau;->o:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2259372
    invoke-virtual {p0, p1}, LX/0ht;->d(Landroid/view/View;)V

    .line 2259373
    iget-object v0, p0, LX/Fau;->o:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance p1, LX/Fat;

    invoke-direct {p1, p0}, LX/Fat;-><init>(LX/Fau;)V

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259374
    iget-object v0, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v0, p2}, LX/5OY;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259375
    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 2259362
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 2259358
    iget-object v0, p0, LX/Fau;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2259359
    iget-object v1, p0, LX/Fau;->l:Lcom/facebook/widget/text/BetterTextView;

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2259360
    return-void

    .line 2259361
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2259321
    invoke-direct {p0}, LX/Fau;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2259322
    iget-object v0, p0, LX/Fau;->a:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2259323
    :goto_0
    return-void

    .line 2259324
    :cond_0
    iget-object v0, p0, LX/Fau;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2259325
    iget-object v0, p0, LX/Fau;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2259326
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2259327
    iget v0, p0, LX/0ht;->A:I

    move v0, v0

    .line 2259328
    iget v1, p0, LX/0ht;->C:I

    move v2, v1

    .line 2259329
    iget v1, p0, LX/0ht;->B:I

    move v1, v1

    .line 2259330
    iget v3, p0, LX/0ht;->D:I

    move v3, v3

    .line 2259331
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 2259332
    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v5, v0

    sub-int/2addr v5, v2

    .line 2259333
    iget v6, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v1, v6, v1

    sub-int/2addr v1, v3

    .line 2259334
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 2259335
    const/high16 v5, -0x80000000

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 2259336
    iget-object v6, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v6, v3, v5}, LX/5OY;->measure(II)V

    .line 2259337
    iget-object v3, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v3}, LX/5OY;->getMeasuredWidth()I

    move-result v3

    .line 2259338
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    .line 2259339
    const/4 v6, 0x2

    new-array v6, v6, [I

    .line 2259340
    invoke-virtual {p1, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2259341
    aget v7, v6, v9

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    .line 2259342
    iput v3, p3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2259343
    const/4 v8, 0x1

    aget v6, v6, v8

    add-int/2addr v5, v6

    iput v5, p3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2259344
    iget v5, p3, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int v5, v1, v5

    iput v5, p3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2259345
    iget-object v5, p0, LX/Fau;->a:Landroid/widget/FrameLayout;

    iget v6, p3, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int/2addr v1, v6

    invoke-virtual {v5, v1}, Landroid/widget/FrameLayout;->setMinimumHeight(I)V

    .line 2259346
    const v1, 0x7f0e028b

    iput v1, p3, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 2259347
    const/16 v1, 0x33

    iput v1, p3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2259348
    iget-object v1, p0, LX/Fau;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v9}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2259349
    div-int/lit8 v1, v3, 0x2

    sub-int v1, v7, v1

    .line 2259350
    if-ge v1, v0, :cond_2

    move v1, v0

    .line 2259351
    :cond_1
    :goto_1
    iget v0, p3, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/2addr v0, v1

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2259352
    iget-object v0, p0, LX/Fau;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getMeasuredWidth()I

    move-result v2

    .line 2259353
    iget-object v0, p0, LX/Fau;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2259354
    div-int/lit8 v2, v2, 0x2

    sub-int v2, v7, v2

    sub-int v1, v2, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2259355
    iget-object v1, p0, LX/Fau;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 2259356
    :cond_2
    add-int v0, v1, v3

    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v5, v2

    if-le v0, v5, :cond_1

    .line 2259357
    iget v0, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v0, v2

    sub-int/2addr v0, v3

    move v1, v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2259317
    iget-object v0, p0, LX/Fau;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2259318
    iget-object v1, p0, LX/Fau;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2259319
    return-void

    .line 2259320
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2259313
    iget-object v0, p0, LX/Fau;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2259314
    iget-object v1, p0, LX/Fau;->m:Lcom/facebook/widget/text/BetterTextView;

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2259315
    return-void

    .line 2259316
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2259309
    iget-object v0, p0, LX/Fau;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2259310
    iget-object v1, p0, LX/Fau;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2259311
    return-void

    .line 2259312
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2259305
    invoke-direct {p0}, LX/Fau;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2259306
    invoke-super {p0, p1}, LX/0hs;->f(Landroid/view/View;)V

    .line 2259307
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fau;->p:Z

    .line 2259308
    :cond_0
    return-void
.end method
