.class public final LX/GeR;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/GeR;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GeP;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/GeW;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2375645
    const/4 v0, 0x0

    sput-object v0, LX/GeR;->a:LX/GeR;

    .line 2375646
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/GeR;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2375642
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2375643
    new-instance v0, LX/GeW;

    invoke-direct {v0}, LX/GeW;-><init>()V

    iput-object v0, p0, LX/GeR;->c:LX/GeW;

    .line 2375644
    return-void
.end method

.method public static declared-synchronized q()LX/GeR;
    .locals 2

    .prologue
    .line 2375638
    const-class v1, LX/GeR;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/GeR;->a:LX/GeR;

    if-nez v0, :cond_0

    .line 2375639
    new-instance v0, LX/GeR;

    invoke-direct {v0}, LX/GeR;-><init>()V

    sput-object v0, LX/GeR;->a:LX/GeR;

    .line 2375640
    :cond_0
    sget-object v0, LX/GeR;->a:LX/GeR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2375641
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2375631
    check-cast p2, LX/GeQ;

    .line 2375632
    iget-object v0, p2, LX/GeQ;->a:Lcom/facebook/java2js/JSValue;

    iget-object v1, p2, LX/GeQ;->b:LX/5KI;

    .line 2375633
    const-string v2, "contentComponent"

    invoke-virtual {v0, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object v2

    .line 2375634
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p2, 0x0

    invoke-interface {p0, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {p0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object p0

    const p2, 0x7f0a0460

    invoke-virtual {p0, p2}, LX/25Q;->i(I)LX/25Q;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f0b0033

    invoke-interface {p0, p2}, LX/1Di;->q(I)LX/1Di;

    move-result-object p0

    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2375635
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2375636
    invoke-static {}, LX/1dS;->b()V

    .line 2375637
    const/4 v0, 0x0

    return-object v0
.end method
