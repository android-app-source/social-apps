.class public LX/GF2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1nC;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/content/Context;

.field public final e:Ljava/util/concurrent/Executor;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/1nC;LX/0Ot;LX/0Ot;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/0ad;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nC;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/Executor;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332255
    iput-object p1, p0, LX/GF2;->a:LX/1nC;

    .line 2332256
    iput-object p2, p0, LX/GF2;->b:LX/0Ot;

    .line 2332257
    iput-object p3, p0, LX/GF2;->c:LX/0Ot;

    .line 2332258
    iput-object p4, p0, LX/GF2;->d:Landroid/content/Context;

    .line 2332259
    iput-object p5, p0, LX/GF2;->e:Ljava/util/concurrent/Executor;

    .line 2332260
    iput-object p6, p0, LX/GF2;->f:LX/0ad;

    .line 2332261
    return-void
.end method

.method public static a(LX/0QB;)LX/GF2;
    .locals 8

    .prologue
    .line 2332262
    new-instance v1, LX/GF2;

    invoke-static {p0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v2

    check-cast v2, LX/1nC;

    const/16 v3, 0x3be

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x12c4

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const-class v5, Landroid/content/Context;

    invoke-interface {p0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct/range {v1 .. v7}, LX/GF2;-><init>(LX/1nC;LX/0Ot;LX/0Ot;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/0ad;)V

    .line 2332263
    move-object v0, v1

    .line 2332264
    return-object v0
.end method
