.class public final enum LX/F9i;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F9i;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F9i;

.field public static final enum OFF:LX/F9i;

.field public static final enum ON:LX/F9i;

.field public static final enum UNKNOWN:LX/F9i;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2205810
    new-instance v0, LX/F9i;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/F9i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F9i;->UNKNOWN:LX/F9i;

    .line 2205811
    new-instance v0, LX/F9i;

    const-string v1, "ON"

    invoke-direct {v0, v1, v3}, LX/F9i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F9i;->ON:LX/F9i;

    .line 2205812
    new-instance v0, LX/F9i;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v4}, LX/F9i;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F9i;->OFF:LX/F9i;

    .line 2205813
    const/4 v0, 0x3

    new-array v0, v0, [LX/F9i;

    sget-object v1, LX/F9i;->UNKNOWN:LX/F9i;

    aput-object v1, v0, v2

    sget-object v1, LX/F9i;->ON:LX/F9i;

    aput-object v1, v0, v3

    sget-object v1, LX/F9i;->OFF:LX/F9i;

    aput-object v1, v0, v4

    sput-object v0, LX/F9i;->$VALUES:[LX/F9i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2205814
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F9i;
    .locals 1

    .prologue
    .line 2205815
    const-class v0, LX/F9i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F9i;

    return-object v0
.end method

.method public static values()[LX/F9i;
    .locals 1

    .prologue
    .line 2205816
    sget-object v0, LX/F9i;->$VALUES:[LX/F9i;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F9i;

    return-object v0
.end method
