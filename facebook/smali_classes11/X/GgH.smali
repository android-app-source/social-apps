.class public LX/GgH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/GgH;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378878
    return-void
.end method

.method public static a(LX/0QB;)LX/GgH;
    .locals 3

    .prologue
    .line 2378863
    sget-object v0, LX/GgH;->a:LX/GgH;

    if-nez v0, :cond_1

    .line 2378864
    const-class v1, LX/GgH;

    monitor-enter v1

    .line 2378865
    :try_start_0
    sget-object v0, LX/GgH;->a:LX/GgH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2378866
    if-eqz v2, :cond_0

    .line 2378867
    :try_start_1
    new-instance v0, LX/GgH;

    invoke-direct {v0}, LX/GgH;-><init>()V

    .line 2378868
    move-object v0, v0

    .line 2378869
    sput-object v0, LX/GgH;->a:LX/GgH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378870
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2378871
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2378872
    :cond_1
    sget-object v0, LX/GgH;->a:LX/GgH;

    return-object v0

    .line 2378873
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2378874
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 3

    .prologue
    .line 2378875
    const-class v0, LX/GgG;

    sget-object v1, LX/0ja;->a:LX/3AL;

    sget-object v2, LX/0ja;->e:LX/3AM;

    invoke-virtual {p1, v0, v1, v2}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    .line 2378876
    return-void
.end method
