.class public final LX/Fhn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Cwv;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic b:LX/Fho;


# direct methods
.method public constructor <init>(LX/Fho;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2273703
    iput-object p1, p0, LX/Fhn;->b:LX/Fho;

    iput-object p2, p0, LX/Fhn;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2273724
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    iget-object v0, v0, LX/Fho;->a:LX/03V;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_MODULES_FAIL:LX/3Ql;

    invoke-virtual {v1}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to fetch null state results"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2273725
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2273726
    move-object v1, v1

    .line 2273727
    const/4 v2, 0x0

    .line 2273728
    iput-boolean v2, v1, LX/0VK;->d:Z

    .line 2273729
    move-object v1, v1

    .line 2273730
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2273731
    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    const/4 v1, 0x0

    .line 2273732
    iput-object v1, v0, LX/Fho;->k:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2273733
    monitor-exit p0

    return-void

    .line 2273734
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2273704
    check-cast p1, LX/Cwv;

    .line 2273705
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    const/4 v1, 0x0

    .line 2273706
    iput-object v1, v0, LX/Fho;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2273707
    if-nez p1, :cond_0

    .line 2273708
    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    iget-object v0, v0, LX/Fho;->a:LX/03V;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_MODULES_FAIL:LX/3Ql;

    invoke-virtual {v1}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The null state result is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2273709
    :goto_0
    monitor-exit p0

    return-void

    .line 2273710
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    invoke-static {v0, p1}, LX/Fho;->a$redex0(LX/Fho;LX/Cwv;)V

    .line 2273711
    iget-object v0, p0, LX/Fhn;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    iget-wide v0, v0, LX/Fho;->h:J

    .line 2273712
    :goto_1
    iget-object v2, p0, LX/Fhn;->b:LX/Fho;

    iget-object v2, v2, LX/Fho;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2273713
    iget-wide v6, p1, LX/Cwv;->b:J

    move-wide v4, v6

    .line 2273714
    sub-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    .line 2273715
    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    iget-object v1, p0, LX/Fhn;->b:LX/Fho;

    iget-object v1, v1, LX/Fho;->f:Ljava/util/List;

    const/16 v2, 0xa

    const/4 v3, 0x0

    iget-object v4, p0, LX/Fhn;->a:Lcom/facebook/common/callercontext/CallerContext;

    sget-object v5, LX/0zS;->d:LX/0zS;

    .line 2273716
    invoke-static/range {v0 .. v5}, LX/Fho;->a$redex0(LX/Fho;Ljava/util/List;IZLcom/facebook/common/callercontext/CallerContext;LX/0zS;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273717
    goto :goto_0

    .line 2273718
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2273719
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    iget-wide v0, v0, LX/Fho;->i:J

    goto :goto_1

    .line 2273720
    :cond_2
    iget-object v0, p0, LX/Fhn;->b:LX/Fho;

    iget-object v1, p0, LX/Fhn;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2273721
    iget-object v6, v0, LX/Fho;->d:LX/0ad;

    sget-short v7, LX/100;->aQ:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2273722
    iget-object v6, v0, LX/Fho;->e:Landroid/os/Handler;

    new-instance v7, Lcom/facebook/search/suggestions/nullstate/ContentDiscoveryNullStateSupplier$2;

    invoke-direct {v7, v0, v1}, Lcom/facebook/search/suggestions/nullstate/ContentDiscoveryNullStateSupplier$2;-><init>(LX/Fho;Lcom/facebook/common/callercontext/CallerContext;)V

    iget-object v8, v0, LX/Fho;->d:LX/0ad;

    sget v9, LX/100;->aS:I

    const/16 v10, 0xa

    invoke-interface {v8, v9, v10}, LX/0ad;->a(II)I

    move-result v8

    mul-int/lit16 v8, v8, 0x3e8

    int-to-long v8, v8

    const v10, 0x557b2534

    invoke-static {v6, v7, v8, v9, v10}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2273723
    :cond_3
    goto :goto_0
.end method
