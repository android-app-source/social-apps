.class public final LX/FzW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;)V
    .locals 0

    .prologue
    .line 2308113
    iput-object p1, p0, LX/FzW;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2308120
    const-class v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    const-string v1, "Could not fetch data for FBID %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/FzW;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    iget-object v4, v4, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2308121
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2308114
    check-cast p1, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    .line 2308115
    iget-object v0, p0, LX/FzW;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->s:LX/0h5;

    iget-object v1, p0, LX/FzW;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    const v2, 0x7f083352

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->j()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2308116
    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2308117
    :cond_0
    iget-object v0, p0, LX/FzW;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    invoke-virtual {v0}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->finish()V

    .line 2308118
    :goto_0
    return-void

    .line 2308119
    :cond_1
    iget-object v0, p0, LX/FzW;->a:Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;)V

    goto :goto_0
.end method
