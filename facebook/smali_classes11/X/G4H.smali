.class public LX/G4H;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2317383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317384
    return-void
.end method

.method public static a(LX/0QB;)LX/G4H;
    .locals 6

    .prologue
    .line 2317385
    new-instance v0, LX/G4H;

    invoke-direct {v0}, LX/G4H;-><init>()V

    .line 2317386
    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    const/16 v3, 0xc49

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x369b

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    .line 2317387
    iput-object v1, v0, LX/G4H;->b:Landroid/content/Context;

    iput-object v2, v0, LX/G4H;->c:Landroid/content/res/Resources;

    iput-object v3, v0, LX/G4H;->d:LX/0Or;

    iput-object v4, v0, LX/G4H;->e:LX/0Or;

    iput-object v5, v0, LX/G4H;->a:LX/0Or;

    .line 2317388
    move-object v0, v0

    .line 2317389
    return-object v0
.end method

.method public static a(LX/G4H;JLjava/lang/String;IZLandroid/os/Bundle;)Landroid/app/Notification;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2317368
    new-instance v0, LX/2HB;

    iget-object v3, p0, LX/G4H;->b:Landroid/content/Context;

    invoke-direct {v0, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    const/4 v7, 0x1

    .line 2317369
    sget-object v3, LX/0ax;->bE:Ljava/lang/String;

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2317370
    iget-object v3, p0, LX/G4H;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17Y;

    iget-object v5, p0, LX/G4H;->b:Landroid/content/Context;

    invoke-interface {v3, v5, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2317371
    invoke-virtual {v3, p6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2317372
    if-nez p5, :cond_0

    .line 2317373
    const-string v4, "is_from_get_notified_notification"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2317374
    :cond_0
    iget-object v4, p0, LX/G4H;->b:Landroid/content/Context;

    invoke-static {p1, p2}, LX/G4H;->b(J)I

    move-result v5

    const/high16 v6, 0x8000000

    invoke-static {v4, v5, v3, v6}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object v3, v3

    .line 2317375
    iput-object v3, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2317376
    move-object v0, v0

    .line 2317377
    iget-object v3, p0, LX/G4H;->c:Landroid/content/res/Resources;

    const v4, 0x7f081627

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v3, p0, LX/G4H;->c:Landroid/content/res/Resources;

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p3, v4, v2

    invoke-virtual {v3, p4, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    const/4 v3, -0x1

    invoke-virtual {v0, v3}, LX/2HB;->c(I)LX/2HB;

    move-result-object v3

    if-nez p5, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    .line 2317378
    iput v1, v0, LX/2HB;->j:I

    .line 2317379
    move-object v0, v0

    .line 2317380
    invoke-virtual {v0, v2, v2, p5}, LX/2HB;->a(IIZ)LX/2HB;

    move-result-object v0

    const v1, 0x7f0218e4

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static a(LX/G4H;JLjava/lang/String;I)V
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2317358
    iget-object v0, p0, LX/G4H;->b:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const/high16 v2, 0x8000000

    invoke-static {v0, v3, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2317359
    new-instance v1, LX/2HB;

    iget-object v2, p0, LX/G4H;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 2317360
    iput-object v0, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2317361
    move-object v0, v1

    .line 2317362
    iget-object v1, p0, LX/G4H;->c:Landroid/content/res/Resources;

    const v2, 0x7f081627

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    iget-object v1, p0, LX/G4H;->c:Landroid/content/res/Resources;

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p3, v2, v3

    invoke-virtual {v1, p4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(I)LX/2HB;

    move-result-object v0

    .line 2317363
    iput v4, v0, LX/2HB;->j:I

    .line 2317364
    move-object v0, v0

    .line 2317365
    const v1, 0x7f0218e4

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v3, v3, v3}, LX/2HB;->a(IIZ)LX/2HB;

    move-result-object v0

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    .line 2317366
    invoke-static {p0, v0, p1, p2}, LX/G4H;->a(LX/G4H;Landroid/app/Notification;J)V

    .line 2317367
    return-void
.end method

.method public static a(LX/G4H;Landroid/app/Notification;J)V
    .locals 2

    .prologue
    .line 2317381
    iget-object v0, p0, LX/G4H;->b:Landroid/content/Context;

    invoke-static {v0}, LX/3RK;->a(Landroid/content/Context;)LX/3RK;

    move-result-object v0

    invoke-static {p2, p3}, LX/G4H;->b(J)I

    move-result v1

    invoke-virtual {v0, v1, p1}, LX/3RK;->a(ILandroid/app/Notification;)V

    .line 2317382
    return-void
.end method

.method public static a(LX/G4H;J)Z
    .locals 3

    .prologue
    .line 2317357
    iget-object v0, p0, LX/G4H;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G4H;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(J)I
    .locals 2

    .prologue
    .line 2317356
    const/16 v0, 0x20

    ushr-long v0, p0, v0

    xor-long/2addr v0, p0

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a(JLjava/lang/String;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2317351
    invoke-static {p0, p1, p2}, LX/G4H;->a(LX/G4H;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const v5, 0x7f08162b

    .line 2317352
    :goto_0
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v7, p4

    invoke-static/range {v1 .. v7}, LX/G4H;->a(LX/G4H;JLjava/lang/String;IZLandroid/os/Bundle;)Landroid/app/Notification;

    move-result-object v0

    .line 2317353
    invoke-static {p0, v0, p1, p2}, LX/G4H;->a(LX/G4H;Landroid/app/Notification;J)V

    .line 2317354
    return-void

    .line 2317355
    :cond_0
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v5, 0x7f08162c

    goto :goto_0

    :cond_1
    const v5, 0x7f08162a

    goto :goto_0
.end method
