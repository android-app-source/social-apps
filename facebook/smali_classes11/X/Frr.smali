.class public final LX/Frr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "LX/FsM;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FqO;

.field public final synthetic b:LX/Fso;

.field public final synthetic c:LX/FsJ;

.field public final synthetic d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic e:J

.field public final synthetic f:LX/G4x;

.field public final synthetic g:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

.field public final synthetic h:LX/Frv;


# direct methods
.method public constructor <init>(LX/Frv;LX/FqO;LX/Fso;LX/FsJ;Ljava/util/concurrent/atomic/AtomicBoolean;JLX/G4x;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 0

    .prologue
    .line 2296148
    iput-object p1, p0, LX/Frr;->h:LX/Frv;

    iput-object p2, p0, LX/Frr;->a:LX/FqO;

    iput-object p3, p0, LX/Frr;->b:LX/Fso;

    iput-object p4, p0, LX/Frr;->c:LX/FsJ;

    iput-object p5, p0, LX/Frr;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-wide p6, p0, LX/Frr;->e:J

    iput-object p8, p0, LX/Frr;->f:LX/G4x;

    iput-object p9, p0, LX/Frr;->g:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2296159
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2296151
    check-cast p1, LX/FsM;

    const/4 v10, 0x0

    .line 2296152
    iget-object v0, p0, LX/Frr;->c:LX/FsJ;

    iget-object v0, v0, LX/FsJ;->b:LX/0zX;

    if-eqz v0, :cond_1

    const/4 v6, 0x1

    .line 2296153
    :goto_0
    iget-object v0, p0, LX/Frr;->h:LX/Frv;

    iget-object v2, p0, LX/Frr;->b:LX/Fso;

    iget-object v3, p0, LX/Frr;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    iget-wide v4, p0, LX/Frr;->e:J

    iget-object v7, p0, LX/Frr;->f:LX/G4x;

    iget-object v8, p0, LX/Frr;->a:LX/FqO;

    iget-object v9, p0, LX/Frr;->g:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    move-object v1, p1

    .line 2296154
    invoke-static/range {v0 .. v9}, LX/Frv;->a$redex0(LX/Frv;LX/FsM;LX/Fso;Ljava/util/concurrent/atomic/AtomicBoolean;JZLX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    .line 2296155
    if-eqz v6, :cond_0

    .line 2296156
    iget-object v1, p0, LX/Frr;->h:LX/Frv;

    iget-object v0, p0, LX/Frr;->c:LX/FsJ;

    iget-object v2, v0, LX/FsJ;->b:LX/0zX;

    iget-object v3, p0, LX/Frr;->h:LX/Frv;

    iget-object v4, p0, LX/Frr;->b:LX/Fso;

    iget-object v0, p1, LX/FsM;->b:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSection;

    invoke-static {v3, v4, v0}, LX/Frv;->a(LX/Frv;LX/Fso;Lcom/facebook/graphql/model/GraphQLTimelineSection;)LX/Fso;

    move-result-object v0

    iget-object v3, p0, LX/Frr;->a:LX/FqO;

    iget-object v4, p0, LX/Frr;->g:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/Frv;->a(LX/0zX;LX/Fso;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)LX/0zi;

    .line 2296157
    :cond_0
    return-void

    :cond_1
    move v6, v10

    .line 2296158
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2296149
    iget-object v0, p0, LX/Frr;->a:LX/FqO;

    iget-object v1, p0, LX/Frr;->b:LX/Fso;

    invoke-interface {v0, v1}, LX/FqO;->b(LX/Fso;)V

    .line 2296150
    return-void
.end method
