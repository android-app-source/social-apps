.class public LX/G6Q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field public final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2320028
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2320029
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/G6Q;->b:Ljava/util/HashMap;

    .line 2320030
    iput-object p1, p0, LX/G6Q;->a:LX/0Zb;

    .line 2320031
    return-void
.end method

.method public static a(LX/G6Q;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2320038
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2320039
    const-string v1, "watermark"

    .line 2320040
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2320041
    invoke-virtual {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2320042
    iget-object v1, p0, LX/G6Q;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2320043
    return-void
.end method

.method public static b(LX/0QB;)LX/G6Q;
    .locals 2

    .prologue
    .line 2320044
    new-instance v1, LX/G6Q;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/G6Q;-><init>(LX/0Zb;)V

    .line 2320045
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2320035
    iget-object v0, p0, LX/G6Q;->b:Ljava/util/HashMap;

    const-string v1, "new_profile_picture_base"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2320036
    iget-object v0, p0, LX/G6Q;->b:Ljava/util/HashMap;

    const-string v1, "photo_selector"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2320037
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2320032
    iget-object v0, p0, LX/G6Q;->b:Ljava/util/HashMap;

    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2320033
    const-string v0, "fb4a_watermark_failure"

    iget-object v1, p0, LX/G6Q;->b:Ljava/util/HashMap;

    invoke-static {p0, v0, v1}, LX/G6Q;->a(LX/G6Q;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 2320034
    return-void
.end method
