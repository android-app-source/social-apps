.class public final LX/GxC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWL;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/GxD;


# direct methods
.method public constructor <init>(LX/GxD;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2407764
    iput-object p1, p0, LX/GxC;->b:LX/GxD;

    iput-object p2, p0, LX/GxC;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 2407765
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "1"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2407766
    :cond_0
    iget-object v0, p0, LX/GxC;->b:LX/GxD;

    iget-object v0, v0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    const-string v1, "%s.onPageFinished-Error"

    sget-object v2, LX/GxF;->z:Ljava/lang/Class;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "url: %s"

    iget-object v3, p0, LX/GxC;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407767
    iget-object v0, p0, LX/GxC;->b:LX/GxD;

    iget-object v0, v0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxE;->PAGE_STATE_ERROR:LX/GxE;

    iput-object v1, v0, LX/GxF;->x:LX/GxE;

    .line 2407768
    iget-object v0, p0, LX/GxC;->b:LX/GxD;

    iget-object v0, v0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxA;->INVALID_HTML_ERROR:LX/GxA;

    iput-object v1, v0, LX/GxF;->y:LX/GxA;

    .line 2407769
    iget-object v0, p0, LX/GxC;->b:LX/GxD;

    iget-object v0, v0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, LX/GxF;->m:LX/GxQ;

    iget-object v1, p0, LX/GxC;->b:LX/GxD;

    iget-object v1, v1, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, LX/GxF;->y:LX/GxA;

    invoke-virtual {v0, v1}, LX/GxQ;->a(LX/GxA;)V

    .line 2407770
    iget-object v0, p0, LX/GxC;->b:LX/GxD;

    iget-object v1, p0, LX/GxC;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/GxD;->a$redex0(LX/GxD;Ljava/lang/String;)V

    .line 2407771
    :goto_0
    return-void

    .line 2407772
    :cond_1
    iget-object v0, p0, LX/GxC;->b:LX/GxD;

    .line 2407773
    invoke-static {v0, p1}, LX/GxD;->a(LX/GxD;Landroid/webkit/WebView;)V

    .line 2407774
    goto :goto_0
.end method
