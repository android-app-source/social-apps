.class public final enum LX/GVd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GVd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GVd;

.field public static final enum CAPTCHA_FETCH_REQUEST_FAILED:LX/GVd;

.field public static final enum CAPTCHA_FETCH_REQUEST_SUCCEEDED:LX/GVd;

.field public static final enum CAPTCHA_SHOWN:LX/GVd;

.field public static final enum CAPTCHA_SOLVE_REQUEST_FAILED:LX/GVd;

.field public static final enum CAPTCHA_SOLVE_REQUEST_SUCCEEDED:LX/GVd;

.field public static final enum SUBMIT_CAPTCHA_INPUT_CLICKED:LX/GVd;

.field public static final enum TRY_ANOTHER_CAPTCHA_CLICKED:LX/GVd;


# instance fields
.field private final mEventType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2359683
    new-instance v0, LX/GVd;

    const-string v1, "CAPTCHA_SHOWN"

    const-string v2, "captcha_shown"

    invoke-direct {v0, v1, v4, v2}, LX/GVd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GVd;->CAPTCHA_SHOWN:LX/GVd;

    .line 2359684
    new-instance v0, LX/GVd;

    const-string v1, "CAPTCHA_FETCH_REQUEST_SUCCEEDED"

    const-string v2, "captcha_fetch_request_succeeded"

    invoke-direct {v0, v1, v5, v2}, LX/GVd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GVd;->CAPTCHA_FETCH_REQUEST_SUCCEEDED:LX/GVd;

    .line 2359685
    new-instance v0, LX/GVd;

    const-string v1, "CAPTCHA_FETCH_REQUEST_FAILED"

    const-string v2, "captcha_fetch_request_failed"

    invoke-direct {v0, v1, v6, v2}, LX/GVd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GVd;->CAPTCHA_FETCH_REQUEST_FAILED:LX/GVd;

    .line 2359686
    new-instance v0, LX/GVd;

    const-string v1, "TRY_ANOTHER_CAPTCHA_CLICKED"

    const-string v2, "try_another_captcha_clicked"

    invoke-direct {v0, v1, v7, v2}, LX/GVd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GVd;->TRY_ANOTHER_CAPTCHA_CLICKED:LX/GVd;

    .line 2359687
    new-instance v0, LX/GVd;

    const-string v1, "SUBMIT_CAPTCHA_INPUT_CLICKED"

    const-string v2, "submit_captcha_input_clicked"

    invoke-direct {v0, v1, v8, v2}, LX/GVd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GVd;->SUBMIT_CAPTCHA_INPUT_CLICKED:LX/GVd;

    .line 2359688
    new-instance v0, LX/GVd;

    const-string v1, "CAPTCHA_SOLVE_REQUEST_SUCCEEDED"

    const/4 v2, 0x5

    const-string v3, "captcha_solve_request_succeeded"

    invoke-direct {v0, v1, v2, v3}, LX/GVd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GVd;->CAPTCHA_SOLVE_REQUEST_SUCCEEDED:LX/GVd;

    .line 2359689
    new-instance v0, LX/GVd;

    const-string v1, "CAPTCHA_SOLVE_REQUEST_FAILED"

    const/4 v2, 0x6

    const-string v3, "captcha_solve_request_failed"

    invoke-direct {v0, v1, v2, v3}, LX/GVd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GVd;->CAPTCHA_SOLVE_REQUEST_FAILED:LX/GVd;

    .line 2359690
    const/4 v0, 0x7

    new-array v0, v0, [LX/GVd;

    sget-object v1, LX/GVd;->CAPTCHA_SHOWN:LX/GVd;

    aput-object v1, v0, v4

    sget-object v1, LX/GVd;->CAPTCHA_FETCH_REQUEST_SUCCEEDED:LX/GVd;

    aput-object v1, v0, v5

    sget-object v1, LX/GVd;->CAPTCHA_FETCH_REQUEST_FAILED:LX/GVd;

    aput-object v1, v0, v6

    sget-object v1, LX/GVd;->TRY_ANOTHER_CAPTCHA_CLICKED:LX/GVd;

    aput-object v1, v0, v7

    sget-object v1, LX/GVd;->SUBMIT_CAPTCHA_INPUT_CLICKED:LX/GVd;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/GVd;->CAPTCHA_SOLVE_REQUEST_SUCCEEDED:LX/GVd;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GVd;->CAPTCHA_SOLVE_REQUEST_FAILED:LX/GVd;

    aput-object v2, v0, v1

    sput-object v0, LX/GVd;->$VALUES:[LX/GVd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2359680
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2359681
    iput-object p3, p0, LX/GVd;->mEventType:Ljava/lang/String;

    .line 2359682
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GVd;
    .locals 1

    .prologue
    .line 2359679
    const-class v0, LX/GVd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GVd;

    return-object v0
.end method

.method public static values()[LX/GVd;
    .locals 1

    .prologue
    .line 2359678
    sget-object v0, LX/GVd;->$VALUES:[LX/GVd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GVd;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2359677
    iget-object v0, p0, LX/GVd;->mEventType:Ljava/lang/String;

    return-object v0
.end method
