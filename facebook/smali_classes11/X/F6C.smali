.class public final enum LX/F6C;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F6C;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F6C;

.field public static final enum CONFIRMED:LX/F6C;

.field public static final enum CREATED:LX/F6C;

.field public static final enum ERROR:LX/F6C;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2200194
    new-instance v0, LX/F6C;

    const-string v1, "CREATED"

    invoke-direct {v0, v1, v2}, LX/F6C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F6C;->CREATED:LX/F6C;

    .line 2200195
    new-instance v0, LX/F6C;

    const-string v1, "CONFIRMED"

    invoke-direct {v0, v1, v3}, LX/F6C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F6C;->CONFIRMED:LX/F6C;

    .line 2200196
    new-instance v0, LX/F6C;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/F6C;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F6C;->ERROR:LX/F6C;

    .line 2200197
    const/4 v0, 0x3

    new-array v0, v0, [LX/F6C;

    sget-object v1, LX/F6C;->CREATED:LX/F6C;

    aput-object v1, v0, v2

    sget-object v1, LX/F6C;->CONFIRMED:LX/F6C;

    aput-object v1, v0, v3

    sget-object v1, LX/F6C;->ERROR:LX/F6C;

    aput-object v1, v0, v4

    sput-object v0, LX/F6C;->$VALUES:[LX/F6C;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2200198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F6C;
    .locals 1

    .prologue
    .line 2200199
    const-class v0, LX/F6C;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F6C;

    return-object v0
.end method

.method public static values()[LX/F6C;
    .locals 1

    .prologue
    .line 2200200
    sget-object v0, LX/F6C;->$VALUES:[LX/F6C;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F6C;

    return-object v0
.end method
