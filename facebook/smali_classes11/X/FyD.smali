.class public LX/FyD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/BP0;

.field public final b:LX/Fy4;

.field public final c:LX/G1I;

.field public final d:Lcom/facebook/timeline/TimelineFragment;

.field public final e:LX/BQ1;

.field public final f:LX/Fsr;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0SG;

.field public i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;


# direct methods
.method public constructor <init>(LX/BP0;Lcom/facebook/timeline/TimelineFragment;LX/BQ1;LX/Fy4;LX/Fsr;LX/G1I;LX/0Or;LX/0SG;)V
    .locals 1
    .param p1    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/timeline/TimelineFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Fy4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BP0;",
            "Lcom/facebook/timeline/header/menus/TimelineFriendingClient$ViewCallback;",
            "LX/BQ1;",
            "LX/Fy4;",
            "LX/Fsr;",
            "LX/G1I;",
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2306406
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306407
    iput-object p1, p0, LX/FyD;->a:LX/BP0;

    .line 2306408
    iput-object p6, p0, LX/FyD;->c:LX/G1I;

    .line 2306409
    iput-object p2, p0, LX/FyD;->d:Lcom/facebook/timeline/TimelineFragment;

    .line 2306410
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ1;

    iput-object v0, p0, LX/FyD;->e:LX/BQ1;

    .line 2306411
    iput-object p5, p0, LX/FyD;->f:LX/Fsr;

    .line 2306412
    iput-object p4, p0, LX/FyD;->b:LX/Fy4;

    .line 2306413
    iput-object p7, p0, LX/FyD;->g:LX/0Or;

    .line 2306414
    iput-object p8, p0, LX/FyD;->h:LX/0SG;

    .line 2306415
    return-void
.end method


# virtual methods
.method public final a(LX/1B1;)V
    .locals 1

    .prologue
    .line 2306416
    new-instance v0, LX/Fy6;

    invoke-direct {v0, p0}, LX/Fy6;-><init>(LX/FyD;)V

    invoke-virtual {p1, v0}, LX/1B1;->a(LX/0b2;)Z

    .line 2306417
    new-instance v0, LX/Fy7;

    invoke-direct {v0, p0}, LX/Fy7;-><init>(LX/FyD;)V

    invoke-virtual {p1, v0}, LX/1B1;->a(LX/0b2;)Z

    .line 2306418
    new-instance v0, LX/Fy8;

    invoke-direct {v0, p0}, LX/Fy8;-><init>(LX/FyD;)V

    invoke-virtual {p1, v0}, LX/1B1;->a(LX/0b2;)Z

    .line 2306419
    new-instance v0, LX/Fy9;

    invoke-direct {v0, p0}, LX/Fy9;-><init>(LX/FyD;)V

    invoke-virtual {p1, v0}, LX/1B1;->a(LX/0b2;)Z

    .line 2306420
    new-instance v0, LX/FyA;

    invoke-direct {v0, p0}, LX/FyA;-><init>(LX/FyD;)V

    invoke-virtual {p1, v0}, LX/1B1;->a(LX/0b2;)Z

    .line 2306421
    new-instance v0, LX/FyB;

    invoke-direct {v0, p0}, LX/FyB;-><init>(LX/FyD;)V

    invoke-virtual {p1, v0}, LX/1B1;->a(LX/0b2;)Z

    .line 2306422
    return-void
.end method
