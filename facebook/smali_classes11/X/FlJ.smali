.class public LX/FlJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:I

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Z

.field private final m:Ljava/lang/String;

.field private final n:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

.field public final o:Z

.field public final p:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZZLjava/lang/String;Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;ZZ)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2280057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2280058
    iput-object p1, p0, LX/FlJ;->a:Ljava/lang/String;

    .line 2280059
    iput-object p2, p0, LX/FlJ;->b:Ljava/lang/String;

    .line 2280060
    iput-object p3, p0, LX/FlJ;->c:Ljava/lang/String;

    .line 2280061
    iput-boolean p4, p0, LX/FlJ;->d:Z

    .line 2280062
    iput-object p5, p0, LX/FlJ;->e:Ljava/lang/String;

    .line 2280063
    iput-object p6, p0, LX/FlJ;->f:Ljava/lang/String;

    .line 2280064
    iput-object p7, p0, LX/FlJ;->g:Ljava/lang/String;

    .line 2280065
    iput p8, p0, LX/FlJ;->h:I

    .line 2280066
    iput-boolean p9, p0, LX/FlJ;->i:Z

    .line 2280067
    iput-boolean p10, p0, LX/FlJ;->j:Z

    .line 2280068
    iput-boolean p11, p0, LX/FlJ;->k:Z

    .line 2280069
    iput-boolean p12, p0, LX/FlJ;->l:Z

    .line 2280070
    iput-object p13, p0, LX/FlJ;->m:Ljava/lang/String;

    .line 2280071
    iput-object p14, p0, LX/FlJ;->n:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    .line 2280072
    move/from16 v0, p15

    iput-boolean v0, p0, LX/FlJ;->o:Z

    .line 2280073
    move/from16 v0, p16

    iput-boolean v0, p0, LX/FlJ;->p:Z

    .line 2280074
    return-void
.end method
