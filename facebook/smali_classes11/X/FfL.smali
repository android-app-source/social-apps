.class public final LX/FfL;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FfI;

.field public final synthetic b:LX/FfN;


# direct methods
.method public constructor <init>(LX/FfN;LX/FfI;)V
    .locals 0

    .prologue
    .line 2268039
    iput-object p1, p0, LX/FfL;->b:LX/FfN;

    iput-object p2, p0, LX/FfL;->a:LX/FfI;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 2268005
    iget-object v0, p0, LX/FfL;->a:LX/FfI;

    invoke-virtual {v0}, LX/FfI;->b()V

    .line 2268006
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2268037
    iget-object v0, p0, LX/FfL;->a:LX/FfI;

    invoke-virtual {v0}, LX/FfI;->a()V

    .line 2268038
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2268007
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 2268008
    if-eqz p1, :cond_0

    .line 2268009
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268010
    if-eqz v0, :cond_0

    .line 2268011
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268012
    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;->a()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2268013
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268014
    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;->a()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2268015
    :cond_0
    iget-object v0, p0, LX/FfL;->b:LX/FfN;

    iget-object v0, v0, LX/FfN;->f:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_GRAPH_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    const-string v2, "See more remote fetch returned null data"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2268016
    iget-object v0, p0, LX/FfL;->a:LX/FfI;

    invoke-virtual {v0}, LX/FfI;->a()V

    .line 2268017
    :goto_0
    return-void

    .line 2268018
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268019
    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;->a()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_2

    .line 2268020
    iget-object v0, p0, LX/FfL;->b:LX/FfN;

    iget-object v0, v0, LX/FfN;->f:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_GRAPH_SEARCH_RESULT_DATA_FAIL:LX/3Ql;

    const-string v2, "Failed to grab page info"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2268021
    iget-object v0, p0, LX/FfL;->a:LX/FfI;

    invoke-virtual {v0}, LX/FfI;->a()V

    goto :goto_0

    .line 2268022
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268023
    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;->a()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v2

    .line 2268024
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2268025
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    move v1, v3

    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;

    .line 2268026
    :try_start_0
    invoke-static {v0}, LX/Cwo;->a(Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel$EdgesModel;)Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object v0

    .line 2268027
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2268028
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2268029
    :catch_0
    move-exception v0

    .line 2268030
    iget-object v6, p0, LX/FfL;->b:LX/FfN;

    iget-object v6, v6, LX/FfN;->f:LX/2Sc;

    invoke-virtual {v6, v0}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_2

    .line 2268031
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2268032
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2268033
    check-cast v0, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel;->a()Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchSimpleSearchEntitiesGraphQLModels$FetchSimpleSearchEntitiesQueryModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    .line 2268034
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268035
    iget-object v0, p0, LX/FfL;->a:LX/FfI;

    const/4 v2, 0x0

    invoke-virtual {v5, v6, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v5, v6, v4}, LX/15i;->h(II)Z

    move-result v4

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, LX/15i;->h(II)Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, LX/FfI;->a(LX/0Px;LX/0Px;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 2268036
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
