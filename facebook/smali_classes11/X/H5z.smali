.class public LX/H5z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2425665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2425666
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 2425669
    check-cast p1, Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;

    .line 2425670
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "offerResendEmail"

    .line 2425671
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2425672
    move-object v0, v0

    .line 2425673
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/facebook/offers/fetcher/OfferResendEmailApiMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/resend_email"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2425674
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2425675
    move-object v0, v0

    .line 2425676
    const-string v1, "POST"

    .line 2425677
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2425678
    move-object v0, v0

    .line 2425679
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2425680
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2425681
    move-object v0, v0

    .line 2425682
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2425683
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2425684
    move-object v0, v0

    .line 2425685
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2425667
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2425668
    const/4 v0, 0x0

    return-object v0
.end method
