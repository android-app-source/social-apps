.class public LX/GJf;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/FBStepperWithLabel;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public c:I

.field private d:I

.field private e:Ljava/lang/String;

.field public f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public g:Lcom/facebook/adinterfaces/FBStepperWithLabel;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2339384
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2339385
    const/16 v0, 0x16c

    iput v0, p0, LX/GJf;->d:I

    .line 2339386
    iput-object p1, p0, LX/GJf;->a:Landroid/content/res/Resources;

    .line 2339387
    return-void
.end method

.method public static c(LX/GJf;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2339372
    iget-object v0, p0, LX/GJf;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2339373
    if-nez v0, :cond_0

    .line 2339374
    iget-object v0, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v7}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2339375
    :goto_0
    return-void

    .line 2339376
    :cond_0
    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    iget v1, p0, LX/GJf;->c:I

    int-to-long v4, v1

    div-long/2addr v2, v4

    .line 2339377
    iget-object v1, p0, LX/GJf;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v4, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v1, v4, :cond_2

    iget-object v1, p0, LX/GJf;->g:Lcom/facebook/adinterfaces/FBStepperWithLabel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2339378
    iget v1, p0, LX/GJf;->c:I

    if-ne v1, v6, :cond_1

    .line 2339379
    iget-object v0, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GJf;->a:Landroid/content/res/Resources;

    const v2, 0x7f080b9c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    goto :goto_0

    .line 2339380
    :cond_1
    iget-object v1, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v4, p0, LX/GJf;->a:Landroid/content/res/Resources;

    const v5, 0x7f080b9d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    iget-object v5, p0, LX/GJf;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v5}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v5

    invoke-static {v0, v2, v3, v5}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2339381
    :cond_2
    iget v1, p0, LX/GJf;->c:I

    if-ne v1, v6, :cond_3

    .line 2339382
    iget-object v0, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v7}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2339383
    :cond_3
    iget-object v1, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v4, p0, LX/GJf;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    iget-object v5, p0, LX/GJf;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v5}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v5

    invoke-static {v0, v2, v3, v5}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2339368
    invoke-super {p0}, LX/GHg;->a()V

    .line 2339369
    iput-object v0, p0, LX/GJf;->g:Lcom/facebook/adinterfaces/FBStepperWithLabel;

    .line 2339370
    iput-object v0, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2339371
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2339366
    const-string v0, "duration"

    iget v1, p0, LX/GJf;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2339367
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 5

    .prologue
    .line 2339332
    check-cast p1, Lcom/facebook/adinterfaces/FBStepperWithLabel;

    .line 2339333
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2339334
    iput-object p2, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2339335
    iput-object p1, p0, LX/GJf;->g:Lcom/facebook/adinterfaces/FBStepperWithLabel;

    .line 2339336
    iget-object v0, p0, LX/GJf;->a:Landroid/content/res/Resources;

    const v1, 0x7f080a59

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GJf;->e:Ljava/lang/String;

    .line 2339337
    iget-object v0, p0, LX/GJf;->g:Lcom/facebook/adinterfaces/FBStepperWithLabel;

    iget v1, p0, LX/GJf;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->setStep(I)V

    .line 2339338
    iget-object v0, p0, LX/GJf;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_0

    .line 2339339
    iget v0, p0, LX/GJf;->c:I

    iget v1, p0, LX/GJf;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/GJf;->c:I

    .line 2339340
    iget-object v0, p0, LX/GJf;->g:Lcom/facebook/adinterfaces/FBStepperWithLabel;

    iget v1, p0, LX/GJf;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->setStep(I)V

    .line 2339341
    iget-object v0, p0, LX/GJf;->g:Lcom/facebook/adinterfaces/FBStepperWithLabel;

    iget v1, p0, LX/GJf;->d:I

    iget-object v2, p0, LX/GJf;->g:Lcom/facebook/adinterfaces/FBStepperWithLabel;

    .line 2339342
    iget v3, v2, Lcom/facebook/adinterfaces/FBStepperWithLabel;->g:I

    move v2, v3

    .line 2339343
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->setStepMax(I)V

    .line 2339344
    :cond_0
    new-instance v0, LX/GJc;

    invoke-direct {v0, p0}, LX/GJc;-><init>(LX/GJf;)V

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->setStepperCallback(LX/GCu;)V

    .line 2339345
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2339346
    new-instance v1, LX/GJd;

    invoke-direct {v1, p0}, LX/GJd;-><init>(LX/GJf;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2339347
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2339348
    new-instance v1, LX/GJe;

    invoke-direct {v1, p0}, LX/GJe;-><init>(LX/GJf;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2339349
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2339350
    iget-object v1, v0, LX/GCE;->e:LX/0ad;

    move-object v0, v1

    .line 2339351
    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/GDK;->E:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2339352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2339353
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d08a6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b5a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2339354
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d2d95

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b5b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2339355
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d2d96

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b5c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2339356
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d2d97

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b5d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2339357
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d08b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b5e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2339358
    new-instance v1, Landroid/util/Pair;

    const v2, 0x7f0d08a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f080b54

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2339359
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2339360
    const-string v2, "duration"

    invoke-virtual {p2, v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/util/List;Ljava/lang/String;)LX/8wQ;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/GCE;->a(LX/8wQ;)V

    .line 2339361
    :goto_0
    return-void

    .line 2339362
    :cond_1
    iget-object v0, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080a48

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2339363
    iget-object v1, p0, LX/GJf;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const v2, 0x7f021964

    invoke-virtual {v1, v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/String;I)V

    .line 2339364
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2339365
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->b()LX/8wQ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 8

    .prologue
    .line 2339318
    iput-object p1, p0, LX/GJf;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2339319
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v0

    iput v0, p0, LX/GJf;->c:I

    .line 2339320
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_0

    .line 2339321
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339322
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v0, v0

    .line 2339323
    iget-wide v4, v0, Lcom/facebook/adinterfaces/model/EventSpecModel;->e:J

    move-wide v0, v4

    .line 2339324
    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 2339325
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 2339326
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5, v0, v1}, LX/GG6;->c(JJ)I

    move-result v4

    int-to-long v4, v4

    .line 2339327
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-gtz v6, :cond_1

    .line 2339328
    const/4 v4, 0x1

    .line 2339329
    :goto_0
    move v0, v4

    .line 2339330
    iput v0, p0, LX/GJf;->d:I

    .line 2339331
    :cond_0
    return-void

    :cond_1
    long-to-int v4, v4

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339315
    if-nez p1, :cond_0

    .line 2339316
    :goto_0
    return-void

    .line 2339317
    :cond_0
    iget-object v0, p0, LX/GJf;->g:Lcom/facebook/adinterfaces/FBStepperWithLabel;

    const-string v1, "duration"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/FBStepperWithLabel;->setStep(I)V

    goto :goto_0
.end method
