.class public final enum LX/H3O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H3O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H3O;

.field public static final enum EXPERIMENTAL_PLACE_INFO:LX/H3O;

.field public static final enum PLACE_INFO:LX/H3O;

.field public static final enum SEARCH_SUGGESTION:LX/H3O;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2420885
    new-instance v0, LX/H3O;

    const-string v1, "SEARCH_SUGGESTION"

    invoke-direct {v0, v1, v2}, LX/H3O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H3O;->SEARCH_SUGGESTION:LX/H3O;

    .line 2420886
    new-instance v0, LX/H3O;

    const-string v1, "EXPERIMENTAL_PLACE_INFO"

    invoke-direct {v0, v1, v3}, LX/H3O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H3O;->EXPERIMENTAL_PLACE_INFO:LX/H3O;

    .line 2420887
    new-instance v0, LX/H3O;

    const-string v1, "PLACE_INFO"

    invoke-direct {v0, v1, v4}, LX/H3O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H3O;->PLACE_INFO:LX/H3O;

    .line 2420888
    const/4 v0, 0x3

    new-array v0, v0, [LX/H3O;

    sget-object v1, LX/H3O;->SEARCH_SUGGESTION:LX/H3O;

    aput-object v1, v0, v2

    sget-object v1, LX/H3O;->EXPERIMENTAL_PLACE_INFO:LX/H3O;

    aput-object v1, v0, v3

    sget-object v1, LX/H3O;->PLACE_INFO:LX/H3O;

    aput-object v1, v0, v4

    sput-object v0, LX/H3O;->$VALUES:[LX/H3O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2420889
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H3O;
    .locals 1

    .prologue
    .line 2420890
    const-class v0, LX/H3O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H3O;

    return-object v0
.end method

.method public static values()[LX/H3O;
    .locals 1

    .prologue
    .line 2420891
    sget-object v0, LX/H3O;->$VALUES:[LX/H3O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H3O;

    return-object v0
.end method
