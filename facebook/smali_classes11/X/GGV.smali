.class public final LX/GGV;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 2334315
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2334316
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/GGV;->setOrientation(I)V

    .line 2334317
    iput-boolean p2, p0, LX/GGV;->a:Z

    .line 2334318
    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2334319
    iget-boolean v1, p0, LX/GGV;->a:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 2334320
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
