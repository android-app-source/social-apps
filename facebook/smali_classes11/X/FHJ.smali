.class public LX/FHJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;
.implements LX/4B8;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/Object;


# instance fields
.field private final b:LX/0Xl;

.field private final c:LX/6ef;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FHO;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2220210
    const-string v0, "media_upload"

    const-string v1, "photo_upload"

    const-string v2, "photo_upload_parallel"

    const-string v3, "photo_upload_hires"

    const-string v4, "photo_upload_hires_parallel"

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/FHJ;->a:LX/0Rf;

    .line 2220211
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FHJ;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Xl;LX/6ef;)V
    .locals 1
    .param p1    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2220202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220203
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2220204
    iput-object v0, p0, LX/FHJ;->d:LX/0Ot;

    .line 2220205
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2220206
    iput-object v0, p0, LX/FHJ;->e:LX/0Ot;

    .line 2220207
    iput-object p1, p0, LX/FHJ;->b:LX/0Xl;

    .line 2220208
    iput-object p2, p0, LX/FHJ;->c:LX/6ef;

    .line 2220209
    return-void
.end method

.method public static a(LX/0QB;)LX/FHJ;
    .locals 8

    .prologue
    .line 2220171
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2220172
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2220173
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2220174
    if-nez v1, :cond_0

    .line 2220175
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2220176
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2220177
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2220178
    sget-object v1, LX/FHJ;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2220179
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2220180
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2220181
    :cond_1
    if-nez v1, :cond_4

    .line 2220182
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2220183
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2220184
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2220185
    new-instance p0, LX/FHJ;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v1

    check-cast v1, LX/0Xl;

    invoke-static {v0}, LX/6ef;->a(LX/0QB;)LX/6ef;

    move-result-object v7

    check-cast v7, LX/6ef;

    invoke-direct {p0, v1, v7}, LX/FHJ;-><init>(LX/0Xl;LX/6ef;)V

    .line 2220186
    const/16 v1, 0x27fc

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v7, 0xdf4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    .line 2220187
    iput-object v1, p0, LX/FHJ;->d:LX/0Ot;

    iput-object v7, p0, LX/FHJ;->e:LX/0Ot;

    .line 2220188
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2220189
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2220190
    if-nez v1, :cond_2

    .line 2220191
    sget-object v0, LX/FHJ;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHJ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2220192
    :goto_1
    if-eqz v0, :cond_3

    .line 2220193
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2220194
    :goto_3
    check-cast v0, LX/FHJ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2220195
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2220196
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2220197
    :catchall_1
    move-exception v0

    .line 2220198
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2220199
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2220200
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2220201
    :cond_2
    :try_start_8
    sget-object v0, LX/FHJ;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHJ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2220146
    iget-object v0, p1, LX/1qK;->mId:Ljava/lang/String;

    move-object v6, v0

    .line 2220147
    iget-object v0, p0, LX/FHJ;->c:LX/6ef;

    invoke-virtual {v0, v6}, LX/6ef;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2220148
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0

    .line 2220149
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2220150
    const-string v1, "mediaResource"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2220151
    const-string v1, "fullQualityImageUpload"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 2220152
    const-string v1, "originalFbid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2220153
    const-string v1, "videoFullDuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2220154
    const-string v1, "mediaUploadType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/FHf;

    .line 2220155
    const-string v7, "up/"

    .line 2220156
    iget-object v1, p0, LX/FHJ;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    sget-wide v8, LX/0X5;->kn:J

    invoke-interface {v1, v8, v9}, LX/0W4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2220157
    sget-object v1, LX/FHI;->a:[I

    iget-object v7, v3, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v7}, LX/2MK;->ordinal()I

    move-result v7

    aget v1, v1, v7

    packed-switch v1, :pswitch_data_0

    .line 2220158
    const-string v1, "up/"

    .line 2220159
    :goto_0
    new-instance v7, LX/FHg;

    const-string v8, "media_id"

    invoke-direct {v7, v0, v1, v8}, LX/FHg;-><init>(LX/FHf;Ljava/lang/String;Ljava/lang/String;)V

    .line 2220160
    iget-object v0, p0, LX/FHJ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHO;

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v7}, LX/FHO;->a(ZZLcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/FHg;)Lcom/facebook/ui/media/attachments/MediaUploadResult;

    move-result-object v0

    .line 2220161
    iget-object v1, p0, LX/FHJ;->c:LX/6ef;

    invoke-virtual {v1, v6}, LX/6ef;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2220162
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0

    .line 2220163
    :pswitch_0
    invoke-virtual {v3}, Lcom/facebook/ui/media/attachments/MediaResource;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2220164
    const-string v1, "messenger_gif/"

    goto :goto_0

    .line 2220165
    :cond_1
    const-string v1, "messenger_image/"

    goto :goto_0

    .line 2220166
    :pswitch_1
    const-string v1, "messenger_video/"

    goto :goto_0

    .line 2220167
    :pswitch_2
    const-string v1, "messenger_audio/"

    goto :goto_0

    .line 2220168
    :pswitch_3
    const-string v1, "messenger_file/"

    goto :goto_0

    .line 2220169
    :cond_2
    iget-object v1, p0, LX/FHJ;->b:LX/0Xl;

    invoke-static {v3}, LX/FGn;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2220170
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v1, v7

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized cancelOperation(Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 2220138
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FHJ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHO;

    .line 2220139
    iget-object v1, v0, LX/FHO;->m:LX/0Uh;

    const/16 v2, 0x17b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2220140
    iget-object v1, v0, LX/FHO;->h:LX/FGi;

    .line 2220141
    iget-object v2, v1, LX/FGi;->o:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7z0;

    .line 2220142
    if-eqz v2, :cond_0

    .line 2220143
    iget-object v3, v1, LX/FGi;->n:LX/7z2;

    invoke-virtual {v3, v2}, LX/7z2;->b(LX/7z0;)V

    .line 2220144
    :cond_0
    iget-object v0, p0, LX/FHJ;->c:LX/6ef;

    invoke-virtual {v0, p1}, LX/6ef;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 2220145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2220134
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2220135
    sget-object v1, LX/FHJ;->a:LX/0Rf;

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2220136
    invoke-direct {p0, p1}, LX/FHJ;->a(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2220137
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
