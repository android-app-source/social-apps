.class public abstract LX/FKK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2224978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224979
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/FKK;->a:Ljava/lang/String;

    .line 2224980
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/FKK;->b:Ljava/lang/String;

    .line 2224981
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/FKK;->c:Ljava/lang/String;

    .line 2224982
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 2224983
    check-cast p1, Ljava/lang/String;

    .line 2224984
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2224985
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "viewer_id"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2224986
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    iget-object v2, p0, LX/FKK;->a:Ljava/lang/String;

    .line 2224987
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2224988
    move-object v1, v1

    .line 2224989
    iget-object v2, p0, LX/FKK;->b:Ljava/lang/String;

    .line 2224990
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2224991
    move-object v1, v1

    .line 2224992
    iget-object v2, p0, LX/FKK;->c:Ljava/lang/String;

    .line 2224993
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2224994
    move-object v1, v1

    .line 2224995
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2224996
    move-object v0, v1

    .line 2224997
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2224998
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2224999
    move-object v0, v0

    .line 2225000
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225001
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225002
    const/4 v0, 0x0

    return-object v0
.end method
