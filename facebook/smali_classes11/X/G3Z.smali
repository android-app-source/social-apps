.class public LX/G3Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/BPp;

.field public e:LX/Fz2;

.field public f:LX/1B1;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;LX/0tX;Ljava/util/concurrent/Executor;LX/BPp;)V
    .locals 1
    .param p1    # Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2315803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2315804
    iput-object p1, p0, LX/G3Z;->a:Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    .line 2315805
    iput-object p2, p0, LX/G3Z;->b:LX/0tX;

    .line 2315806
    iput-object p3, p0, LX/G3Z;->c:Ljava/util/concurrent/Executor;

    .line 2315807
    iput-object p4, p0, LX/G3Z;->d:LX/BPp;

    .line 2315808
    new-instance v0, LX/Fz2;

    invoke-direct {v0}, LX/Fz2;-><init>()V

    iput-object v0, p0, LX/G3Z;->e:LX/Fz2;

    .line 2315809
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, LX/G3Z;->f:LX/1B1;

    .line 2315810
    return-void
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 2315811
    iget-object v0, p0, LX/G3Z;->a:Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;

    iget-object v1, p0, LX/G3Z;->e:LX/Fz2;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/refresher/ProfileRefresherInfoFragment;->a(LX/Fz2;)V

    .line 2315812
    return-void
.end method
