.class public LX/FMo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile x:LX/FMo;


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMd;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMb;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNH;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FM6;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ow;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FM9;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNL;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNX;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FML;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNQ;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FJW;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/2Oq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2230802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2230803
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230804
    iput-object v0, p0, LX/FMo;->c:LX/0Ot;

    .line 2230805
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230806
    iput-object v0, p0, LX/FMo;->d:LX/0Ot;

    .line 2230807
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230808
    iput-object v0, p0, LX/FMo;->e:LX/0Ot;

    .line 2230809
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230810
    iput-object v0, p0, LX/FMo;->f:LX/0Ot;

    .line 2230811
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230812
    iput-object v0, p0, LX/FMo;->g:LX/0Ot;

    .line 2230813
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230814
    iput-object v0, p0, LX/FMo;->h:LX/0Ot;

    .line 2230815
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230816
    iput-object v0, p0, LX/FMo;->i:LX/0Ot;

    .line 2230817
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230818
    iput-object v0, p0, LX/FMo;->j:LX/0Ot;

    .line 2230819
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230820
    iput-object v0, p0, LX/FMo;->l:LX/0Ot;

    .line 2230821
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230822
    iput-object v0, p0, LX/FMo;->m:LX/0Ot;

    .line 2230823
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230824
    iput-object v0, p0, LX/FMo;->n:LX/0Ot;

    .line 2230825
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230826
    iput-object v0, p0, LX/FMo;->o:LX/0Ot;

    .line 2230827
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230828
    iput-object v0, p0, LX/FMo;->p:LX/0Ot;

    .line 2230829
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230830
    iput-object v0, p0, LX/FMo;->q:LX/0Ot;

    .line 2230831
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230832
    iput-object v0, p0, LX/FMo;->r:LX/0Ot;

    .line 2230833
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230834
    iput-object v0, p0, LX/FMo;->s:LX/0Ot;

    .line 2230835
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230836
    iput-object v0, p0, LX/FMo;->t:LX/0Ot;

    .line 2230837
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230838
    iput-object v0, p0, LX/FMo;->w:LX/0Ot;

    .line 2230839
    return-void
.end method

.method public static a(LX/0QB;)LX/FMo;
    .locals 3

    .prologue
    .line 2230792
    sget-object v0, LX/FMo;->x:LX/FMo;

    if-nez v0, :cond_1

    .line 2230793
    const-class v1, LX/FMo;

    monitor-enter v1

    .line 2230794
    :try_start_0
    sget-object v0, LX/FMo;->x:LX/FMo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2230795
    if-eqz v2, :cond_0

    .line 2230796
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/FMo;->b(LX/0QB;)LX/FMo;

    move-result-object v0

    sput-object v0, LX/FMo;->x:LX/FMo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2230797
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2230798
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2230799
    :cond_1
    sget-object v0, LX/FMo;->x:LX/FMo;

    return-object v0

    .line 2230800
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2230801
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/FMo;LX/0Or;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/2Oq;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FMo;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Landroid/app/NotificationManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FM6;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Ow;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FM9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FML;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FJW;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;",
            "LX/2Oq;",
            "LX/0Ot",
            "<",
            "LX/FNh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2230791
    iput-object p1, p0, LX/FMo;->a:LX/0Or;

    iput-object p2, p0, LX/FMo;->b:Landroid/content/Context;

    iput-object p3, p0, LX/FMo;->c:LX/0Ot;

    iput-object p4, p0, LX/FMo;->d:LX/0Ot;

    iput-object p5, p0, LX/FMo;->e:LX/0Ot;

    iput-object p6, p0, LX/FMo;->f:LX/0Ot;

    iput-object p7, p0, LX/FMo;->g:LX/0Ot;

    iput-object p8, p0, LX/FMo;->h:LX/0Ot;

    iput-object p9, p0, LX/FMo;->i:LX/0Ot;

    iput-object p10, p0, LX/FMo;->j:LX/0Ot;

    iput-object p11, p0, LX/FMo;->k:LX/0Or;

    iput-object p12, p0, LX/FMo;->l:LX/0Ot;

    iput-object p13, p0, LX/FMo;->m:LX/0Ot;

    iput-object p14, p0, LX/FMo;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/FMo;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/FMo;->p:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/FMo;->q:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/FMo;->r:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/FMo;->s:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/FMo;->t:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/FMo;->u:LX/0Or;

    move-object/from16 v0, p22

    iput-object v0, p0, LX/FMo;->v:LX/2Oq;

    move-object/from16 v0, p23

    iput-object v0, p0, LX/FMo;->w:LX/0Ot;

    return-void
.end method

.method public static a(LX/FMo;LX/6el;)V
    .locals 1

    .prologue
    .line 2230787
    iget-object v0, p0, LX/FMo;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    .line 2230788
    iget-object p0, v0, LX/2Og;->e:LX/2Om;

    if-eqz p0, :cond_0

    .line 2230789
    iget-object p0, v0, LX/2Og;->e:LX/2Om;

    invoke-virtual {p0, p1}, LX/2Om;->e(LX/6el;)V

    .line 2230790
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)LX/FMo;
    .locals 26

    .prologue
    .line 2230643
    new-instance v2, LX/FMo;

    invoke-direct {v2}, LX/FMo;-><init>()V

    .line 2230644
    const/16 v3, 0x12cb

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const-class v4, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x6

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x542

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x298b

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2989

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xd9f

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x29ac

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xda0

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2979

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0xce8

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0xce9

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x297b

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x29b1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0xd9d

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x29b5

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x2984

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x245

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x29b3

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x2848

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x2847

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    invoke-static/range {p0 .. p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v24

    check-cast v24, LX/2Oq;

    const/16 v25, 0x29b7

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    invoke-static/range {v2 .. v25}, LX/FMo;->a(LX/FMo;LX/0Or;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/2Oq;LX/0Ot;)V

    .line 2230645
    return-object v2
.end method

.method private static c(LX/FMo;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2230776
    iget-object v0, p0, LX/FMo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FML;

    .line 2230777
    iget-object v1, v0, LX/FML;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6jK;

    .line 2230778
    iget-object p0, v1, LX/6jK;->d:Ljava/util/Set;

    if-nez p0, :cond_0

    .line 2230779
    sget-object p0, LX/3MP;->b:LX/0U1;

    .line 2230780
    iget-object v0, p0, LX/0U1;->d:Ljava/lang/String;

    move-object p0, v0

    .line 2230781
    const-string v0, "1"

    invoke-static {p0, v0}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object p0

    .line 2230782
    invoke-static {v1, p0}, LX/6jK;->a(LX/6jK;LX/0ux;)Ljava/util/Set;

    move-result-object p0

    .line 2230783
    iput-object p0, v1, LX/6jK;->d:Ljava/util/Set;

    .line 2230784
    :cond_0
    iget-object p0, v1, LX/6jK;->d:Ljava/util/Set;

    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p0

    move-object v1, p0

    .line 2230785
    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 2230786
    return v0
.end method

.method private e(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 4

    .prologue
    .line 2230764
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->i()Ljava/lang/String;

    move-result-object v2

    .line 2230765
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FMo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uq;

    invoke-virtual {v0}, LX/2Uq;->m()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2230766
    :cond_0
    :goto_0
    return-void

    .line 2230767
    :cond_1
    iget-object v0, p0, LX/FMo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FML;

    invoke-virtual {v0, v2}, LX/FML;->a(Ljava/lang/String;)Z

    move-result v0

    move v1, v0

    .line 2230768
    invoke-static {p0, v2}, LX/FMo;->c(LX/FMo;Ljava/lang/String;)Z

    move-result v0

    .line 2230769
    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FMo;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNh;

    invoke-virtual {v0, v2}, LX/FNh;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2230770
    iget-object v0, p0, LX/FMo;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FML;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2230771
    iget-object v2, v0, LX/FML;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/messaging/sms/business/SmsBusinessThreadManager$1;

    invoke-direct {v3, v0, v1}, Lcom/facebook/messaging/sms/business/SmsBusinessThreadManager$1;-><init>(LX/FML;Ljava/util/Collection;)V

    const p1, 0x3268dca7

    invoke-static {v2, v3, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2230772
    const/4 v0, 0x1

    .line 2230773
    :goto_1
    if-eqz v0, :cond_0

    .line 2230774
    sget-object v0, LX/6el;->BUSINESS:LX/6el;

    invoke-static {p0, v0}, LX/FMo;->a(LX/FMo;LX/6el;)V

    .line 2230775
    sget-object v0, LX/6el;->SMS:LX/6el;

    invoke-static {p0, v0}, LX/FMo;->a(LX/FMo;LX/6el;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private f(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 7

    .prologue
    .line 2230745
    iget-object v0, p0, LX/FMo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uq;

    const/4 v1, 0x0

    .line 2230746
    const-string v2, "sms_takeover_readonly_notifications_test"

    invoke-static {v0, v2}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2230747
    iget-object v2, v0, LX/2Uq;->a:LX/0ad;

    sget-short v3, LX/6jD;->K:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2230748
    :cond_0
    :goto_0
    move v0, v1

    .line 2230749
    if-nez v0, :cond_1

    .line 2230750
    const/4 v0, 0x0

    .line 2230751
    :goto_1
    return v0

    .line 2230752
    :cond_1
    iget-object v0, p0, LX/FMo;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNQ;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    iget-object v1, p0, LX/FMo;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Uq;

    const v4, 0x3f4ccccd    # 0.8f

    .line 2230753
    const-string v5, "sms_takeover_readonly_notifications_test"

    invoke-static {v1, v5}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2230754
    iget-object v5, v1, LX/2Uq;->a:LX/0ad;

    sget v6, LX/6jD;->O:F

    invoke-interface {v5, v6, v4}, LX/0ad;->a(FF)F

    move-result v4

    .line 2230755
    :cond_2
    :goto_2
    move v1, v4

    .line 2230756
    float-to-double v4, v1

    invoke-virtual {v0, v2, v3, v4, v5}, LX/FNQ;->a(JD)Z

    move-result v0

    .line 2230757
    if-eqz v0, :cond_3

    .line 2230758
    iget-object v1, p0, LX/FMo;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    goto :goto_1

    .line 2230759
    :cond_3
    iget-object v1, p0, LX/FMo;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    goto :goto_1

    .line 2230760
    :cond_4
    const-string v2, "sms_takeover_readonly_notifications_rollout"

    invoke-static {v0, v2}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2230761
    iget-object v2, v0, LX/2Uq;->a:LX/0ad;

    sget-short v3, LX/6jD;->S:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0

    .line 2230762
    :cond_5
    const-string v5, "sms_takeover_readonly_notifications_rollout"

    invoke-static {v1, v5}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2230763
    iget-object v5, v1, LX/2Uq;->a:LX/0ad;

    sget v6, LX/6jD;->W:F

    invoke-interface {v5, v6, v4}, LX/0ad;->a(FF)F

    move-result v4

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2230742
    iget-object v0, p0, LX/FMo;->k:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Og;

    sget-object v1, LX/6en;->SMS:LX/6en;

    invoke-virtual {v0, v1}, LX/2Og;->a(LX/6en;)V

    .line 2230743
    iget-object v0, p0, LX/FMo;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->a()V

    .line 2230744
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/model/messages/Message;LX/FMM;)V
    .locals 12

    .prologue
    .line 2230705
    iget-boolean v0, p2, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2230706
    iget-object v0, p0, LX/FMo;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMd;

    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/FMd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2230707
    if-nez v1, :cond_0

    if-eqz p3, :cond_3

    .line 2230708
    :cond_0
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v2

    .line 2230709
    if-eqz v1, :cond_1

    .line 2230710
    iget-object v0, p0, LX/FMo;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMd;

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/FMd;->c(Ljava/lang/String;)V

    .line 2230711
    iput-object v1, v2, LX/6f7;->n:Ljava/lang/String;

    .line 2230712
    :cond_1
    if-eqz p3, :cond_2

    sget-object v0, LX/FMM;->NO_ERROR:LX/FMM;

    if-eq p3, v0, :cond_2

    .line 2230713
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0}, LX/FMQ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2230714
    iget-object v0, p0, LX/FMo;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMb;

    invoke-virtual {v0, p3}, LX/FMb;->a(LX/FMM;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    .line 2230715
    iput-object v0, v2, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 2230716
    :goto_1
    iget-object v0, p0, LX/FMo;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FM6;

    iget-object v1, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    .line 2230717
    invoke-static {v0}, LX/FM6;->a(LX/FM6;)V

    .line 2230718
    new-instance v6, LX/FM5;

    iget-object v7, v0, LX/FM6;->e:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    invoke-direct {v6, p3, v8, v9}, LX/FM5;-><init>(LX/FMM;J)V

    .line 2230719
    iget-object v7, v0, LX/FM6;->b:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    const/16 v8, 0x28

    if-le v7, v8, :cond_7

    .line 2230720
    const/4 v7, 0x0

    iput-object v7, v0, LX/FM6;->b:Ljava/util/Map;

    .line 2230721
    :goto_2
    iget-object v7, v0, LX/FM6;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    iget v8, v0, LX/FM6;->c:I

    invoke-static {v8}, LX/FM6;->b(I)LX/0Tn;

    move-result-object v8

    invoke-interface {v7, v8, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v7

    iget v8, v0, LX/FM6;->c:I

    invoke-static {v8}, LX/FM6;->a(I)LX/0Tn;

    move-result-object v8

    invoke-virtual {p3}, LX/FMM;->ordinal()I

    move-result v9

    invoke-interface {v7, v8, v9}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v7

    iget v8, v0, LX/FM6;->c:I

    invoke-static {v8}, LX/FM6;->c(I)LX/0Tn;

    move-result-object v8

    iget-wide v10, v6, LX/FM5;->b:J

    invoke-interface {v7, v8, v10, v11}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v6

    sget-object v7, LX/FM6;->a:LX/0Tn;

    iget v8, v0, LX/FM6;->c:I

    add-int/lit8 v8, v8, 0x1

    rem-int/lit8 v8, v8, 0x14

    invoke-interface {v6, v7, v8}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v6

    invoke-interface {v6}, LX/0hN;->commit()V

    .line 2230722
    iget v6, v0, LX/FM6;->c:I

    add-int/lit8 v6, v6, 0x1

    rem-int/lit8 v6, v6, 0x14

    iput v6, v0, LX/FM6;->c:I

    .line 2230723
    :cond_2
    invoke-virtual {v2}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object p2

    .line 2230724
    :cond_3
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2230725
    const-string v0, "message"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2230726
    iget-object v0, p0, LX/FMo;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "sms_mms_sent"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v5, 0x1da07fe5

    move-object v4, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2230727
    iget-object v0, p0, LX/FMo;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Or;

    .line 2230728
    iget-object v1, v0, LX/2Or;->a:LX/0Uh;

    const/16 v2, 0x630

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2230729
    if-eqz v0, :cond_4

    iget-object v0, p0, LX/FMo;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->l()Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, LX/FMM;->NO_ERROR:LX/FMM;

    if-eq p3, v0, :cond_4

    .line 2230730
    iget-object v0, p0, LX/FMo;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Di5;

    new-instance v1, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v3, LX/Dhu;->SMS_MSS_ERROR:LX/Dhu;

    invoke-direct {v1, v2, v3}, Lcom/facebook/messaging/notify/FailedToSendMessageNotification;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dhu;)V

    invoke-virtual {v0, v1}, LX/Di5;->a(Lcom/facebook/messaging/notify/FailedToSendMessageNotification;)V

    .line 2230731
    :cond_4
    iget-object v0, p0, LX/FMo;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNL;

    .line 2230732
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2230733
    const-string v2, "SMS_ON_WEB.ACTION_MESSAGE_SENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2230734
    const-string v2, "message"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2230735
    invoke-static {v0, v1}, LX/FNL;->a(LX/FNL;Landroid/content/Intent;)V

    .line 2230736
    return-void

    .line 2230737
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2230738
    :cond_6
    iget-object v0, p0, LX/FMo;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMb;

    invoke-virtual {v0, p3}, LX/FMb;->b(LX/FMM;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    .line 2230739
    iput-object v0, v2, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 2230740
    goto/16 :goto_1

    .line 2230741
    :cond_7
    iget-object v7, v0, LX/FM6;->b:Ljava/util/Map;

    invoke-interface {v7, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/model/messages/Message;Landroid/net/Uri;Ljava/lang/Boolean;)V
    .locals 6
    .param p3    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2230655
    iget-boolean v0, p2, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2230656
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 2230657
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v2

    sget-object v4, LX/0XG;->EMAIL:LX/0XG;

    if-ne v2, v4, :cond_a

    .line 2230658
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->d:Ljava/lang/String;

    .line 2230659
    :goto_1
    invoke-virtual {p0, v0}, LX/FMo;->a(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 2230660
    if-eqz v0, :cond_1

    .line 2230661
    :goto_2
    return-void

    :cond_0
    move v0, v3

    .line 2230662
    goto :goto_0

    .line 2230663
    :cond_1
    iget-object v0, p0, LX/FMo;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/FMo;->v:LX/2Oq;

    invoke-virtual {v0}, LX/2Oq;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2230664
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2230665
    const-string v0, "message"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2230666
    if-eqz p3, :cond_2

    .line 2230667
    const-string v0, "delete_msg_id"

    invoke-static {p3}, LX/FMQ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2230668
    :cond_2
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->i()Ljava/lang/String;

    move-result-object v4

    .line 2230669
    iget-object v0, p0, LX/FMo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uq;

    invoke-virtual {v0}, LX/2Uq;->m()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, LX/FMo;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNX;

    invoke-virtual {v0, v4}, LX/FNX;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2230670
    if-eqz v0, :cond_3

    .line 2230671
    sget-object v4, LX/6el;->SPAM:LX/6el;

    invoke-static {p0, v4}, LX/FMo;->a(LX/FMo;LX/6el;)V

    .line 2230672
    sget-object v4, LX/6el;->SMS:LX/6el;

    invoke-static {p0, v4}, LX/FMo;->a(LX/FMo;LX/6el;)V

    .line 2230673
    :cond_3
    move v0, v0

    .line 2230674
    if-eqz v0, :cond_5

    .line 2230675
    const-string v0, "should_show_notification"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2230676
    :goto_4
    iget-object v0, p0, LX/FMo;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "received_sms"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v5, -0x1594b403

    move-object v4, p1

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2230677
    :cond_4
    :goto_5
    iget-object v0, p0, LX/FMo;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNL;

    .line 2230678
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2230679
    const-string v2, "SMS_ON_WEB.ACTION_MESSAGE_RECEIVED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2230680
    const-string v2, "message"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2230681
    invoke-static {v0, v1}, LX/FNL;->a(LX/FNL;Landroid/content/Intent;)V

    .line 2230682
    goto/16 :goto_2

    .line 2230683
    :cond_5
    invoke-direct {p0, p2}, LX/FMo;->e(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2230684
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2230685
    const-string v0, "is_class_zero"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2230686
    const-string v0, "should_show_notification"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_4

    .line 2230687
    :cond_6
    invoke-direct {p0, p2}, LX/FMo;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    .line 2230688
    const-string v0, "should_show_notification"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_4

    .line 2230689
    :cond_7
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2230690
    const/4 v4, 0x1

    .line 2230691
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2230692
    sget-object v1, LX/3RH;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2230693
    iget-object v1, p0, LX/FMo;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, LX/FMo;->v:LX/2Oq;

    invoke-virtual {v1}, LX/2Oq;->a()Z

    move-result v1

    if-nez v1, :cond_8

    .line 2230694
    const-string v1, "show_turn_on_show_sms"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2230695
    :cond_8
    iget-object v1, p0, LX/FMo;->b:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 2230696
    iget-object v0, p2, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 2230697
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v2}, LX/FMQ;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2230698
    iget-object v0, p0, LX/FMo;->b:Landroid/content/Context;

    const v2, 0x7f0808f0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2230699
    :cond_9
    new-instance v2, LX/2HB;

    iget-object v3, p0, LX/FMo;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v0

    const v2, 0x7f0212e2

    invoke-virtual {v0, v2}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    .line 2230700
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2230701
    move-object v1, v0

    .line 2230702
    iget-object v0, p0, LX/FMo;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v2, 0x272d

    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 2230703
    goto/16 :goto_5

    .line 2230704
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->g()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_3
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2230646
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/FMo;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FM9;

    .line 2230647
    invoke-static {p1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2230648
    iget-object p0, v0, LX/FM9;->f:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3Lx;

    invoke-virtual {p0, p1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2230649
    :cond_0
    sget-object p0, LX/FM9;->a:Ljava/util/HashMap;

    if-nez p0, :cond_1

    .line 2230650
    invoke-static {v0}, LX/FM9;->b(LX/FM9;)V

    .line 2230651
    :cond_1
    sget-object p0, LX/FM9;->a:Ljava/util/HashMap;

    if-eqz p0, :cond_3

    sget-object p0, LX/FM9;->a:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 2230652
    if-eqz v0, :cond_2

    .line 2230653
    const/4 v0, 0x1

    .line 2230654
    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 p0, 0x0

    goto :goto_0
.end method
