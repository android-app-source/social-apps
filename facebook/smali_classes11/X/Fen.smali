.class public final LX/Fen;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fee;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V
    .locals 0

    .prologue
    .line 2266640
    iput-object p1, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0am",
            "<",
            "LX/0us;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2266608
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    .line 2266609
    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266610
    move-object v1, v1

    .line 2266611
    invoke-virtual {v1, p3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d(Ljava/lang/String;)V

    .line 2266612
    invoke-virtual {v1, p4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c(Ljava/lang/String;)V

    .line 2266613
    invoke-virtual {v1, p5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e(Ljava/lang/String;)V

    .line 2266614
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->a$redex0(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2266615
    iget-object v2, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    .line 2266616
    new-instance v3, LX/2i4;

    invoke-direct {v3}, LX/2i4;-><init>()V

    iput-object v3, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->I:LX/2i4;

    .line 2266617
    iget-object v3, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    iget-object v4, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->I:LX/2i4;

    invoke-interface {v3, v4}, LX/0g8;->a(LX/2i4;)V

    .line 2266618
    iget-object v3, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->A:LX/1Qq;

    invoke-interface {v3}, LX/1Qq;->getCount()I

    move-result v3

    iput v3, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->J:I

    .line 2266619
    iget-object v2, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v2, v0}, LX/CzE;->a(LX/0Px;)I

    move-result v3

    .line 2266620
    iget-object v2, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-static {v2}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    .line 2266621
    iget-object v2, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    const/4 v5, 0x0

    .line 2266622
    iget-boolean v4, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->G:Z

    if-eqz v4, :cond_1

    .line 2266623
    iput-boolean v5, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->G:Z

    .line 2266624
    iget-object v4, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    .line 2266625
    iget-object v2, v4, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-object v4, v2

    .line 2266626
    invoke-virtual {v4, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2266627
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->p:LX/CvY;

    iget-object v2, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-static {v2}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->k(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)I

    move-result v2

    const/4 v4, 0x0

    sget-object v5, LX/8cf;->HEAD:LX/8cf;

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IILX/8cg;LX/8cf;)V

    .line 2266628
    return-void

    .line 2266629
    :cond_1
    if-nez v0, :cond_0

    .line 2266630
    iget-object v4, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->I:LX/2i4;

    if-nez v4, :cond_2

    .line 2266631
    :goto_1
    iget-object v4, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->K:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2266632
    :cond_2
    iget-object v4, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->A:LX/1Qq;

    invoke-interface {v4}, LX/1Qq;->getCount()I

    move-result v4

    iget p1, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->J:I

    sub-int/2addr v4, p1

    .line 2266633
    iget-object p1, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->I:LX/2i4;

    .line 2266634
    iget p2, p1, LX/2i4;->c:I

    move p1, p2

    .line 2266635
    iget-object p2, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->I:LX/2i4;

    .line 2266636
    iget v0, p2, LX/2i4;->a:I

    move p2, v0

    .line 2266637
    add-int/2addr v4, p2

    .line 2266638
    iget-object p2, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->F:LX/0g8;

    neg-int p1, p1

    invoke-interface {p2, v4, p1}, LX/0g8;->d(II)V

    .line 2266639
    const/4 p1, 0x0

    iput-object p1, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->I:LX/2i4;

    goto :goto_1
.end method

.method public final a(LX/7C4;)V
    .locals 2

    .prologue
    .line 2266602
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/EQG;->ERROR:LX/EQG;

    .line 2266603
    :goto_0
    iget-object v1, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    if-eqz v1, :cond_0

    .line 2266604
    iget-object v1, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    invoke-virtual {v1, v0}, LX/Fje;->setState(LX/EQG;)V

    .line 2266605
    :cond_0
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->o:LX/2Sc;

    invoke-virtual {v0, p1}, LX/2Sc;->a(LX/7C4;)V

    .line 2266606
    return-void

    .line 2266607
    :cond_1
    sget-object v0, LX/EQG;->ERROR_LOADING_MORE:LX/EQG;

    goto :goto_0
.end method

.method public final b(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0am",
            "<",
            "LX/0us;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2266575
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    .line 2266576
    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2266577
    move-object v1, v1

    .line 2266578
    invoke-virtual {v1, p3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d(Ljava/lang/String;)V

    .line 2266579
    invoke-virtual {v1, p5}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e(Ljava/lang/String;)V

    .line 2266580
    invoke-virtual {v1, p4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c(Ljava/lang/String;)V

    .line 2266581
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->a$redex0(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;LX/0Px;)LX/0Px;

    move-result-object v6

    .line 2266582
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {p2}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0us;

    .line 2266583
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    sget-object v5, LX/CzE;->a:LX/0us;

    invoke-virtual {v3, v5}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0us;

    iput-object v3, v2, LX/CzE;->n:LX/0us;

    .line 2266584
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, p1, :cond_0

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    .line 2266585
    invoke-static {v2, v3}, LX/CzE;->c(LX/CzE;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2266586
    iget-object p2, v2, LX/CzE;->f:Ljava/util/List;

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2266587
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 2266588
    :cond_0
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-static {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->B(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)V

    .line 2266589
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->p:LX/CvY;

    iget-object v2, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-static {v2}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->k(Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;)I

    move-result v2

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IILX/8cg;LX/8cf;)V

    .line 2266590
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    if-eqz v0, :cond_2

    .line 2266591
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2266592
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    sget-object v1, LX/EQG;->LOADING_MORE:LX/EQG;

    invoke-virtual {v0, v1}, LX/Fje;->setState(LX/EQG;)V

    .line 2266593
    :goto_1
    return-void

    .line 2266594
    :cond_1
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2266595
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    sget-object v1, LX/EQG;->EMPTY:LX/EQG;

    invoke-virtual {v0, v1}, LX/Fje;->setState(LX/EQG;)V

    .line 2266596
    :cond_2
    :goto_2
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    const/4 v1, 0x1

    .line 2266597
    iput-boolean v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->D:Z

    .line 2266598
    goto :goto_1

    .line 2266599
    :cond_3
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->z:LX/CzE;

    invoke-virtual {v0}, LX/CzE;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2266600
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->e()Ljava/lang/Class;

    .line 2266601
    iget-object v0, p0, LX/Fen;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;->E:LX/Fje;

    sget-object v1, LX/EQG;->LOADING_FINISHED:LX/EQG;

    invoke-virtual {v0, v1}, LX/Fje;->setState(LX/EQG;)V

    goto :goto_2
.end method
