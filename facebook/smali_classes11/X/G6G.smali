.class public LX/G6G;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/graphics/RectF;


# instance fields
.field public final b:LX/8GV;

.field public final c:LX/5vm;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/17Y;

.field public final f:LX/9fn;

.field public final g:LX/1Ck;

.field public h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 2319937
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, LX/G6G;->a:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(LX/8GV;LX/9fo;LX/5vm;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/1Ck;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2319938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319939
    iput-object p1, p0, LX/G6G;->b:LX/8GV;

    .line 2319940
    iput-object p3, p0, LX/G6G;->c:LX/5vm;

    .line 2319941
    iput-object p4, p0, LX/G6G;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2319942
    iput-object p5, p0, LX/G6G;->e:LX/17Y;

    .line 2319943
    iput-object p6, p0, LX/G6G;->g:LX/1Ck;

    .line 2319944
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/G6G;->h:Ljava/lang/String;

    .line 2319945
    iget-object v0, p0, LX/G6G;->h:Ljava/lang/String;

    invoke-virtual {p2, v0}, LX/9fo;->a(Ljava/lang/String;)LX/9fn;

    move-result-object v0

    iput-object v0, p0, LX/G6G;->f:LX/9fn;

    .line 2319946
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2319947
    iget-object v0, p0, LX/G6G;->e:LX/17Y;

    sget-object v1, LX/0ax;->bY:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2319948
    const-string v1, "extra_should_merge_camera_roll"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2319949
    const-string v1, "extra_should_show_suggested_photos"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2319950
    const-string v1, "pick_pic_lite"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2319951
    const-string v1, "disable_camera_roll"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2319952
    const-string v1, "disable_adding_photos_to_albums"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2319953
    const-string v1, "extra_photo_title_text"

    const v2, 0x7f081569

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2319954
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->PROFILEPIC:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->o()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->m()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->e()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->f()LX/8AA;

    move-result-object v1

    .line 2319955
    const-string v2, "extra_simple_picker_launcher_configuration"

    invoke-virtual {v1}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2319956
    iget-object v1, p0, LX/G6G;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2319957
    return-void
.end method
