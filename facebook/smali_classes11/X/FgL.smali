.class public final enum LX/FgL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FgL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FgL;

.field public static final enum ALL_POSTS:LX/FgL;

.field public static final enum ARCHIVED:LX/FgL;

.field public static final enum AVAILABLE:LX/FgL;

.field public static final enum SALE_POSTS:LX/FgL;

.field public static final enum SOLD:LX/FgL;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2270214
    new-instance v0, LX/FgL;

    const-string v1, "ALL_POSTS"

    invoke-direct {v0, v1, v2}, LX/FgL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FgL;->ALL_POSTS:LX/FgL;

    .line 2270215
    new-instance v0, LX/FgL;

    const-string v1, "SALE_POSTS"

    invoke-direct {v0, v1, v3}, LX/FgL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FgL;->SALE_POSTS:LX/FgL;

    .line 2270216
    new-instance v0, LX/FgL;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v4}, LX/FgL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FgL;->AVAILABLE:LX/FgL;

    .line 2270217
    new-instance v0, LX/FgL;

    const-string v1, "SOLD"

    invoke-direct {v0, v1, v5}, LX/FgL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FgL;->SOLD:LX/FgL;

    .line 2270218
    new-instance v0, LX/FgL;

    const-string v1, "ARCHIVED"

    invoke-direct {v0, v1, v6}, LX/FgL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FgL;->ARCHIVED:LX/FgL;

    .line 2270219
    const/4 v0, 0x5

    new-array v0, v0, [LX/FgL;

    sget-object v1, LX/FgL;->ALL_POSTS:LX/FgL;

    aput-object v1, v0, v2

    sget-object v1, LX/FgL;->SALE_POSTS:LX/FgL;

    aput-object v1, v0, v3

    sget-object v1, LX/FgL;->AVAILABLE:LX/FgL;

    aput-object v1, v0, v4

    sget-object v1, LX/FgL;->SOLD:LX/FgL;

    aput-object v1, v0, v5

    sget-object v1, LX/FgL;->ARCHIVED:LX/FgL;

    aput-object v1, v0, v6

    sput-object v0, LX/FgL;->$VALUES:[LX/FgL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2270220
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FgL;
    .locals 1

    .prologue
    .line 2270221
    const-class v0, LX/FgL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FgL;

    return-object v0
.end method

.method public static values()[LX/FgL;
    .locals 1

    .prologue
    .line 2270222
    sget-object v0, LX/FgL;->$VALUES:[LX/FgL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FgL;

    return-object v0
.end method
