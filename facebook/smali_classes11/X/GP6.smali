.class public abstract LX/GP6;
.super LX/GP4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GP4",
        "<",
        "LX/GQC;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/6yq;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public e:Lcom/facebook/resources/ui/FbEditText;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(LX/6yq;LX/GQC;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V
    .locals 0

    .prologue
    .line 2348419
    invoke-direct {p0, p2, p3, p5, p6}, LX/GP4;-><init>(LX/GQ7;Landroid/content/res/Resources;LX/GOy;LX/GNs;)V

    .line 2348420
    iput-object p1, p0, LX/GP6;->c:LX/6yq;

    .line 2348421
    iput-object p4, p0, LX/GP6;->d:Ljava/util/concurrent/ExecutorService;

    .line 2348422
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2348407
    const v0, 0x7f0d2488

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    .line 2348408
    const v0, 0x7f0d248a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/GP6;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2348409
    const v0, 0x7f0d2489

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/GP6;->g:Landroid/widget/ImageView;

    .line 2348410
    invoke-virtual {p0}, LX/GP6;->e()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    iget-object v1, p0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->hasFocus()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/GP6;->a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Z)V

    .line 2348411
    iget-object v0, p0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    const v1, 0x7f0827b1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setHint(I)V

    .line 2348412
    iget-object v0, p0, LX/GP4;->f:LX/GQ7;

    move-object v0, v0

    .line 2348413
    check-cast v0, LX/GQC;

    new-instance v1, LX/GPH;

    invoke-direct {v1, p0}, LX/GPH;-><init>(LX/GP6;)V

    .line 2348414
    iput-object v1, v0, LX/GQ7;->a:LX/GP0;

    .line 2348415
    iget-object v0, p0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/GPI;

    invoke-direct {v1, p0}, LX/GPI;-><init>(LX/GP6;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2348416
    iget-object v0, p0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, LX/GP6;->c:LX/6yq;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2348417
    iget-object v0, p0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/GPJ;

    invoke-direct {v1, p0}, LX/GPJ;-><init>(LX/GP6;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2348418
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;Z)V
    .locals 2

    .prologue
    .line 2348403
    iget-object v1, p0, LX/GP6;->g:Landroid/widget/ImageView;

    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-ne p1, v0, :cond_0

    const v0, 0x7f02141e

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2348404
    iget-object v0, p0, LX/GP6;->g:Landroid/widget/ImageView;

    invoke-static {v0, p2}, LX/GQ1;->a(Landroid/view/View;Z)V

    .line 2348405
    return-void

    .line 2348406
    :cond_0
    const v0, 0x7f02141d

    goto :goto_0
.end method

.method public final a()Z
    .locals 8

    .prologue
    .line 2348393
    iget-object v0, p0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LX/GP6;->e()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    const/4 p0, 0x4

    const/4 v7, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2348394
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2348395
    :cond_0
    :goto_0
    move v0, v2

    .line 2348396
    return v0

    .line 2348397
    :cond_1
    const-string v4, "\\d{3,4}"

    invoke-virtual {v0, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2348398
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    .line 2348399
    sget-object v5, LX/GQB;->a:[I

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2348400
    if-ne v4, v7, :cond_0

    move v2, v3

    goto :goto_0

    .line 2348401
    :pswitch_0
    if-ne v4, p0, :cond_0

    move v2, v3

    goto :goto_0

    .line 2348402
    :pswitch_1
    if-eq v4, p0, :cond_2

    if-ne v4, v7, :cond_0

    :cond_2
    move v2, v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348392
    const-string v0, "security_code"

    return-object v0
.end method

.method public final c()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2348391
    iget-object v0, p0, LX/GP6;->e:Lcom/facebook/resources/ui/FbEditText;

    return-object v0
.end method

.method public final d()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 2348385
    iget-object v0, p0, LX/GP6;->f:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public abstract e()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
.end method

.method public final lX_()V
    .locals 2

    .prologue
    .line 2348386
    invoke-super {p0}, LX/GP4;->lX_()V

    .line 2348387
    invoke-virtual {p0}, LX/GP6;->e()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    sget-object v1, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-ne v0, v1, :cond_0

    .line 2348388
    iget-object v0, p0, LX/GP6;->f:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0827bb

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2348389
    :goto_0
    return-void

    .line 2348390
    :cond_0
    iget-object v0, p0, LX/GP6;->f:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0827ba

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0
.end method
