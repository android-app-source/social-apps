.class public final LX/FGI;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/1bf;

.field public final synthetic c:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;ZLX/1bf;)V
    .locals 0

    .prologue
    .line 2218665
    iput-object p1, p0, LX/FGI;->c:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;

    iput-boolean p2, p0, LX/FGI;->a:Z

    iput-object p3, p0, LX/FGI;->b:LX/1bf;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2218666
    iget-boolean v0, p0, LX/FGI;->a:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1ca;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2218667
    iget-object v0, p0, LX/FGI;->c:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->n:Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;

    iget-object v1, p0, LX/FGI;->c:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;

    iget-object v1, v1, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->d:Ljava/lang/String;

    iget-object v2, p0, LX/FGI;->c:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;

    iget-object v2, v2, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/lowdatamode/DataSaverModeUtils;->a(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2218668
    :cond_0
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2218669
    iget-object v0, p0, LX/FGI;->c:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->i:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/FGJ;->b:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2218670
    iget-object v0, p0, LX/FGI;->c:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler$3;->e:Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;

    iget-object v0, v0, Lcom/facebook/messaging/media/prefetch/MediaPrefetchHandler;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    iget-object v1, p0, LX/FGI;->b:LX/1bf;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->a(LX/1bf;)V

    .line 2218671
    :cond_0
    return-void
.end method
