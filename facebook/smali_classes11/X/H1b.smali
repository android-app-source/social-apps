.class public LX/H1b;
.super LX/H0v;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/H0w;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H1U;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2415573
    invoke-direct {p0, p1}, LX/H0v;-><init>(Ljava/lang/String;)V

    .line 2415574
    const-string v0, ""

    iput-object v0, p0, LX/H1b;->g:Ljava/lang/String;

    .line 2415575
    const-string v0, ""

    iput-object v0, p0, LX/H1b;->h:Ljava/lang/String;

    .line 2415576
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/H1b;->i:Ljava/util/Set;

    .line 2415577
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/H1b;->a:Ljava/lang/String;

    .line 2415578
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/H1b;->f:Ljava/lang/String;

    .line 2415579
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/H1b;->j:Ljava/util/List;

    .line 2415580
    const-string v0, "Universe"

    iput-object v0, p0, LX/H1b;->c:Ljava/lang/CharSequence;

    .line 2415581
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2415564
    invoke-direct {p0, p1, p2, p3}, LX/H1b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2415565
    iput-object p4, p0, LX/H1b;->g:Ljava/lang/String;

    .line 2415566
    iput-object p5, p0, LX/H1b;->h:Ljava/lang/String;

    .line 2415567
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 2415582
    move-object v0, p1

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    .line 2415583
    invoke-virtual {v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030b29

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2415584
    const v1, 0x7f0d1c20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    .line 2415585
    invoke-virtual {p0}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415586
    const v1, 0x7f0d1c21

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2415587
    iget-object v2, p0, LX/H1b;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2415588
    invoke-virtual {v1, v4}, Lcom/facebook/fig/listitem/FigListItem;->setVisibility(I)V

    .line 2415589
    :goto_0
    const v1, 0x7f0d1c22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2415590
    iget-object v2, p0, LX/H1b;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2415591
    invoke-virtual {v1, v4}, Lcom/facebook/fig/listitem/FigListItem;->setVisibility(I)V

    .line 2415592
    :goto_1
    const v1, 0x7f0d1c23

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2415593
    new-instance v2, LX/H1a;

    iget-object v3, p0, LX/H1b;->j:Ljava/util/List;

    invoke-direct {v2, p1, v3}, LX/H1a;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 2415594
    new-instance v3, LX/1P1;

    invoke-direct {v3, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2415595
    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2415596
    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2415597
    return-object v0

    .line 2415598
    :cond_0
    iget-object v2, p0, LX/H1b;->a:Ljava/lang/String;

    invoke-static {v2}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415599
    iget-object v2, p0, LX/H1b;->f:Ljava/lang/String;

    invoke-static {v2}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2415600
    const-string v2, "Current Experiment"

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2415601
    :cond_1
    iget-object v2, p0, LX/H1b;->g:Ljava/lang/String;

    invoke-static {v2}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415602
    iget-object v2, p0, LX/H1b;->h:Ljava/lang/String;

    invoke-static {v2}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2415603
    const-string v2, "Override Experiment"

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(LX/H1U;)V
    .locals 3

    .prologue
    .line 2415569
    iget-object v0, p0, LX/H1b;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2415570
    iget-object v0, p1, LX/H1U;->f:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1T;

    iget-object v0, v0, LX/H1T;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2415571
    iget-object v2, p0, LX/H1b;->i:Ljava/util/Set;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2415572
    :cond_0
    return-void
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 2415568
    iget-object v0, p0, LX/H1b;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
