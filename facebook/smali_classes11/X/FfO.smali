.class public final LX/FfO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;)V
    .locals 0

    .prologue
    .line 2268128
    iput-object p1, p0, LX/FfO;->a:Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 12

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x2

    const/16 v0, 0x26

    const v1, 0x63c58530

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2268129
    iget-object v1, p0, LX/FfO;->a:Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    iget-wide v2, v1, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->F:J

    cmp-long v1, v2, v8

    if-lez v1, :cond_0

    .line 2268130
    iget-object v1, p0, LX/FfO;->a:Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    iget-object v2, p0, LX/FfO;->a:Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    .line 2268131
    invoke-virtual {v2}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->I()J

    move-result-wide v10

    move-wide v2, v10

    .line 2268132
    iget-object v4, p0, LX/FfO;->a:Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    iget-wide v4, v4, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->F:J

    sub-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->a(Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;J)J

    .line 2268133
    :cond_0
    iget-object v1, p0, LX/FfO;->a:Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    .line 2268134
    iput-wide v8, v1, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;->F:J

    .line 2268135
    const/16 v1, 0x27

    const v2, 0xf38bbc3

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
