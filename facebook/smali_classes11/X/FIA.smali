.class public LX/FIA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/FI9;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FIA;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/FIA;
    .locals 3

    .prologue
    .line 2221530
    sget-object v0, LX/FIA;->a:LX/FIA;

    if-nez v0, :cond_1

    .line 2221531
    const-class v1, LX/FIA;

    monitor-enter v1

    .line 2221532
    :try_start_0
    sget-object v0, LX/FIA;->a:LX/FIA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2221533
    if-eqz v2, :cond_0

    .line 2221534
    :try_start_1
    new-instance v0, LX/FIA;

    invoke-direct {v0}, LX/FIA;-><init>()V

    .line 2221535
    move-object v0, v0

    .line 2221536
    sput-object v0, LX/FIA;->a:LX/FIA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221537
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2221538
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2221539
    :cond_1
    sget-object v0, LX/FIA;->a:LX/FIA;

    return-object v0

    .line 2221540
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2221541
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2221549
    check-cast p1, LX/FI9;

    .line 2221550
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "start_stream_upload"

    .line 2221551
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2221552
    move-object v0, v0

    .line 2221553
    const-string v1, "POST"

    .line 2221554
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2221555
    move-object v0, v0

    .line 2221556
    new-instance v1, Ljava/lang/StringBuilder;

    const-string p0, "messenger_videos/"

    invoke-direct {v1, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, p1, LX/FI9;->b:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p0, "/?phase=start"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2221557
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2221558
    move-object v0, v0

    .line 2221559
    const/4 v1, 0x1

    .line 2221560
    iput-boolean v1, v0, LX/14O;->o:Z

    .line 2221561
    move-object v0, v0

    .line 2221562
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2221563
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "video_type"

    iget-object p0, p1, LX/FI9;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2221564
    invoke-static {p0}, LX/6eg;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5dX;

    move-result-object p1

    iget-object p1, p1, LX/5dX;->apiStringValue:Ljava/lang/String;

    move-object p0, p1

    .line 2221565
    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221566
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2221567
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2221568
    move-object v0, v0

    .line 2221569
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2221570
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2221571
    move-object v0, v0

    .line 2221572
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2221543
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2221544
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 2221545
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2221546
    new-instance v0, Lorg/apache/http/HttpException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Video segment transcode upload failed. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2221547
    :cond_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2221548
    const-string v1, "stream_id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
