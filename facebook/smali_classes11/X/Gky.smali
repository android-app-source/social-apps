.class public final LX/Gky;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gl6;


# direct methods
.method public constructor <init>(LX/Gl6;)V
    .locals 0

    .prologue
    .line 2389737
    iput-object p1, p0, LX/Gky;->a:LX/Gl6;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2389743
    sget-object v0, LX/Gl6;->a:Ljava/lang/String;

    const-string v1, "Failed to load groups for sort section on reorder"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2389744
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2389738
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2389739
    iget-object v0, p0, LX/Gky;->a:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->i:LX/GlB;

    invoke-static {v0, p1}, LX/Gl6;->b(LX/Gkp;Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389740
    iget-object v0, p0, LX/Gky;->a:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->g:Ljava/util/HashMap;

    iget-object v1, p0, LX/Gky;->a:LX/Gl6;

    iget-object v1, v1, LX/Gl6;->i:LX/GlB;

    invoke-virtual {v1}, LX/Gkp;->i()LX/Gkn;

    move-result-object v1

    iget-object v2, p0, LX/Gky;->a:LX/Gl6;

    iget-object v2, v2, LX/Gl6;->i:LX/GlB;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2389741
    iget-object v0, p0, LX/Gky;->a:LX/Gl6;

    invoke-static {v0}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389742
    :cond_0
    return-void
.end method
