.class public final enum LX/GG8;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GG8;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GG8;

.field public static final enum ADDRESS:LX/GG8;

.field public static final enum CALL_NOW_MIN_AGE:LX/GG8;

.field public static final enum INVALID_BID_AMOUNT:LX/GG8;

.field public static final enum INVALID_BUDGET:LX/GG8;

.field public static final enum INVALID_DURATION:LX/GG8;

.field public static final enum INVALID_LOCATION:LX/GG8;

.field public static final enum INVALID_URL:LX/GG8;

.field public static final enum PAGE_LIKE_BODY_TEXT:LX/GG8;

.field public static final enum PHONE_NUMBER:LX/GG8;

.field public static final enum PHOTO_NOT_UPLOADED:LX/GG8;

.field public static final enum SERVER_VALIDATION_ERROR:LX/GG8;

.field public static final enum UNEDITED_DATA:LX/GG8;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2333317
    new-instance v0, LX/GG8;

    const-string v1, "INVALID_BUDGET"

    invoke-direct {v0, v1, v3}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->INVALID_BUDGET:LX/GG8;

    .line 2333318
    new-instance v0, LX/GG8;

    const-string v1, "CALL_NOW_MIN_AGE"

    invoke-direct {v0, v1, v4}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->CALL_NOW_MIN_AGE:LX/GG8;

    .line 2333319
    new-instance v0, LX/GG8;

    const-string v1, "INVALID_URL"

    invoke-direct {v0, v1, v5}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->INVALID_URL:LX/GG8;

    .line 2333320
    new-instance v0, LX/GG8;

    const-string v1, "UNEDITED_DATA"

    invoke-direct {v0, v1, v6}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->UNEDITED_DATA:LX/GG8;

    .line 2333321
    new-instance v0, LX/GG8;

    const-string v1, "PHONE_NUMBER"

    invoke-direct {v0, v1, v7}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->PHONE_NUMBER:LX/GG8;

    .line 2333322
    new-instance v0, LX/GG8;

    const-string v1, "PAGE_LIKE_BODY_TEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->PAGE_LIKE_BODY_TEXT:LX/GG8;

    .line 2333323
    new-instance v0, LX/GG8;

    const-string v1, "ADDRESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->ADDRESS:LX/GG8;

    .line 2333324
    new-instance v0, LX/GG8;

    const-string v1, "PHOTO_NOT_UPLOADED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->PHOTO_NOT_UPLOADED:LX/GG8;

    .line 2333325
    new-instance v0, LX/GG8;

    const-string v1, "INVALID_DURATION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->INVALID_DURATION:LX/GG8;

    .line 2333326
    new-instance v0, LX/GG8;

    const-string v1, "SERVER_VALIDATION_ERROR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->SERVER_VALIDATION_ERROR:LX/GG8;

    .line 2333327
    new-instance v0, LX/GG8;

    const-string v1, "INVALID_BID_AMOUNT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->INVALID_BID_AMOUNT:LX/GG8;

    .line 2333328
    new-instance v0, LX/GG8;

    const-string v1, "INVALID_LOCATION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/GG8;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GG8;->INVALID_LOCATION:LX/GG8;

    .line 2333329
    const/16 v0, 0xc

    new-array v0, v0, [LX/GG8;

    sget-object v1, LX/GG8;->INVALID_BUDGET:LX/GG8;

    aput-object v1, v0, v3

    sget-object v1, LX/GG8;->CALL_NOW_MIN_AGE:LX/GG8;

    aput-object v1, v0, v4

    sget-object v1, LX/GG8;->INVALID_URL:LX/GG8;

    aput-object v1, v0, v5

    sget-object v1, LX/GG8;->UNEDITED_DATA:LX/GG8;

    aput-object v1, v0, v6

    sget-object v1, LX/GG8;->PHONE_NUMBER:LX/GG8;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GG8;->PAGE_LIKE_BODY_TEXT:LX/GG8;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GG8;->ADDRESS:LX/GG8;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GG8;->PHOTO_NOT_UPLOADED:LX/GG8;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/GG8;->INVALID_DURATION:LX/GG8;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/GG8;->SERVER_VALIDATION_ERROR:LX/GG8;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/GG8;->INVALID_BID_AMOUNT:LX/GG8;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/GG8;->INVALID_LOCATION:LX/GG8;

    aput-object v2, v0, v1

    sput-object v0, LX/GG8;->$VALUES:[LX/GG8;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2333332
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GG8;
    .locals 1

    .prologue
    .line 2333331
    const-class v0, LX/GG8;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GG8;

    return-object v0
.end method

.method public static values()[LX/GG8;
    .locals 1

    .prologue
    .line 2333330
    sget-object v0, LX/GG8;->$VALUES:[LX/GG8;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GG8;

    return-object v0
.end method
