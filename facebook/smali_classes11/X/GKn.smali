.class public LX/GKn;
.super LX/GIw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GIw",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/GDg;

.field public final b:LX/GDm;

.field public c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private d:LX/GNI;


# direct methods
.method public constructor <init>(LX/GK4;LX/GDg;LX/GDm;LX/GNI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2341445
    invoke-direct {p0, p1}, LX/GIw;-><init>(LX/GK4;)V

    .line 2341446
    iput-object p2, p0, LX/GKn;->a:LX/GDg;

    .line 2341447
    iput-object p3, p0, LX/GKn;->b:LX/GDm;

    .line 2341448
    iput-object p4, p0, LX/GKn;->d:LX/GNI;

    .line 2341449
    return-void
.end method

.method public static a(LX/0QB;)LX/GKn;
    .locals 1

    .prologue
    .line 2341382
    invoke-static {p0}, LX/GKn;->c(LX/0QB;)LX/GKn;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0QB;)LX/GKn;
    .locals 5

    .prologue
    .line 2341443
    new-instance v4, LX/GKn;

    invoke-static {p0}, LX/GK4;->a(LX/0QB;)LX/GK4;

    move-result-object v0

    check-cast v0, LX/GK4;

    invoke-static {p0}, LX/GDg;->a(LX/0QB;)LX/GDg;

    move-result-object v1

    check-cast v1, LX/GDg;

    invoke-static {p0}, LX/GDm;->a(LX/0QB;)LX/GDm;

    move-result-object v2

    check-cast v2, LX/GDm;

    invoke-static {p0}, LX/GNI;->a(LX/0QB;)LX/GNI;

    move-result-object v3

    check-cast v3, LX/GNI;

    invoke-direct {v4, v0, v1, v2, v3}, LX/GKn;-><init>(LX/GK4;LX/GDg;LX/GDm;LX/GNI;)V

    .line 2341444
    return-object v4
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2341442
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0, p1, p2}, LX/GIw;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2341438
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341439
    invoke-super {p0, p1}, LX/GIw;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2341440
    iput-object p1, p0, LX/GKn;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341441
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 1

    .prologue
    .line 2341435
    invoke-super {p0, p1, p2}, LX/GIw;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2341436
    const v0, 0x3f666666    # 0.9f

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAlpha(F)V

    .line 2341437
    return-void
.end method

.method public final b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2341434
    new-instance v0, LX/GKk;

    invoke-direct {v0, p0}, LX/GKk;-><init>(LX/GKn;)V

    return-object v0
.end method

.method public final c()Landroid/view/View$OnClickListener;
    .locals 5

    .prologue
    .line 2341430
    iget-object v0, p0, LX/GKn;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341431
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2341432
    iget-object v2, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v2, v2

    .line 2341433
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/GKn;->d:LX/GNI;

    iget-object v4, p0, LX/GKn;->b:LX/GDm;

    invoke-static {v0, v1, v2, v3, v4}, LX/GNI;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;Landroid/content/Context;LX/GNI;LX/GDm;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2341429
    new-instance v0, LX/GKl;

    invoke-direct {v0, p0}, LX/GKl;-><init>(LX/GKn;)V

    return-object v0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2341387
    sget-object v0, LX/GKm;->a:[I

    iget-object v1, p0, LX/GKn;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v1

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2341388
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341389
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2341390
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341391
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2341392
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341393
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2341394
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341395
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2341396
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341397
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    .line 2341398
    :goto_0
    return-void

    .line 2341399
    :pswitch_0
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341400
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2341401
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341402
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2341403
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341404
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2341405
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341406
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2341407
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341408
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto :goto_0

    .line 2341409
    :pswitch_1
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341410
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2341411
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341412
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2341413
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341414
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2341415
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341416
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2341417
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341418
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto :goto_0

    .line 2341419
    :pswitch_2
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341420
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2341421
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341422
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2341423
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341424
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2341425
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341426
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2341427
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341428
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2341383
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2341384
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v1, v1

    .line 2341385
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080abf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonText(Ljava/lang/String;)V

    .line 2341386
    return-void
.end method
