.class public LX/GA1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/SharedPreferences;

.field private final b:LX/GA0;

.field private c:LX/GAa;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2324514
    invoke-static {}, LX/GAK;->f()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.facebook.AccessTokenManager.SharedPreferences"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, LX/GA0;

    invoke-direct {v1}, LX/GA0;-><init>()V

    invoke-direct {p0, v0, v1}, LX/GA1;-><init>(Landroid/content/SharedPreferences;LX/GA0;)V

    .line 2324515
    return-void
.end method

.method private constructor <init>(Landroid/content/SharedPreferences;LX/GA0;)V
    .locals 0

    .prologue
    .line 2324516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2324517
    iput-object p1, p0, LX/GA1;->a:Landroid/content/SharedPreferences;

    .line 2324518
    iput-object p2, p0, LX/GA1;->b:LX/GA0;

    .line 2324519
    return-void
.end method

.method public static g(LX/GA1;)LX/GAa;
    .locals 2

    .prologue
    .line 2324520
    iget-object v0, p0, LX/GA1;->c:LX/GAa;

    if-nez v0, :cond_1

    .line 2324521
    monitor-enter p0

    .line 2324522
    :try_start_0
    iget-object v0, p0, LX/GA1;->c:LX/GAa;

    if-nez v0, :cond_0

    .line 2324523
    new-instance v0, LX/GAa;

    invoke-static {}, LX/GAK;->f()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GAa;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2324524
    iput-object v0, p0, LX/GA1;->c:LX/GAa;

    .line 2324525
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2324526
    :cond_1
    iget-object v0, p0, LX/GA1;->c:LX/GAa;

    return-object v0

    .line 2324527
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/AccessToken;
    .locals 14

    .prologue
    .line 2324528
    const/4 v0, 0x0

    .line 2324529
    iget-object v1, p0, LX/GA1;->a:Landroid/content/SharedPreferences;

    const-string v2, "com.facebook.AccessTokenManager.CachedAccessToken"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    move v1, v1

    .line 2324530
    if-eqz v1, :cond_2

    .line 2324531
    const/4 v0, 0x0

    .line 2324532
    iget-object v1, p0, LX/GA1;->a:Landroid/content/SharedPreferences;

    const-string v2, "com.facebook.AccessTokenManager.CachedAccessToken"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2324533
    if-eqz v1, :cond_0

    .line 2324534
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2324535
    invoke-static {v2}, Lcom/facebook/AccessToken;->a(Lorg/json/JSONObject;)Lcom/facebook/AccessToken;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2324536
    :cond_0
    :goto_0
    move-object v0, v0

    .line 2324537
    :cond_1
    :goto_1
    return-object v0

    .line 2324538
    :cond_2
    sget-boolean v1, LX/GAK;->k:Z

    move v1, v1

    .line 2324539
    if-eqz v1, :cond_1

    .line 2324540
    const/4 v2, 0x0

    .line 2324541
    invoke-static {p0}, LX/GA1;->g(LX/GA1;)LX/GAa;

    move-result-object v3

    invoke-virtual {v3}, LX/GAa;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 2324542
    if-eqz v3, :cond_5

    const-wide/16 v9, 0x0

    const/4 v5, 0x0

    .line 2324543
    if-nez v3, :cond_6

    .line 2324544
    :cond_3
    :goto_2
    move v4, v5

    .line 2324545
    if-eqz v4, :cond_5

    .line 2324546
    const-string v5, "com.facebook.TokenCachingStrategy.Permissions"

    invoke-static {v3, v5}, Lcom/facebook/AccessToken;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 2324547
    const-string v5, "com.facebook.TokenCachingStrategy.DeclinedPermissions"

    invoke-static {v3, v5}, Lcom/facebook/AccessToken;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 2324548
    const-string v5, "bundle"

    invoke-static {v3, v5}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2324549
    const-string v5, "com.facebook.TokenCachingStrategy.ApplicationId"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v7, v5

    .line 2324550
    invoke-static {v7}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2324551
    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v7

    .line 2324552
    :cond_4
    const-string v5, "bundle"

    invoke-static {v3, v5}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2324553
    const-string v5, "com.facebook.TokenCachingStrategy.Token"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .line 2324554
    invoke-static {v6}, LX/GsV;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 2324555
    if-eqz v5, :cond_7

    .line 2324556
    :goto_3
    move-object v5, v5

    .line 2324557
    :try_start_1
    const-string v8, "id"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v8

    .line 2324558
    new-instance v5, Lcom/facebook/AccessToken;

    .line 2324559
    const-string v11, "bundle"

    invoke-static {v3, v11}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2324560
    const-string v11, "com.facebook.TokenCachingStrategy.AccessTokenSource"

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 2324561
    const-string v11, "com.facebook.TokenCachingStrategy.AccessTokenSource"

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v11

    check-cast v11, LX/GA9;

    .line 2324562
    :goto_4
    move-object v11, v11

    .line 2324563
    const-string v12, "com.facebook.TokenCachingStrategy.ExpirationDate"

    invoke-static {v3, v12}, LX/GAa;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v12

    const-string v13, "com.facebook.TokenCachingStrategy.LastRefreshDate"

    invoke-static {v3, v13}, LX/GAa;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v13

    invoke-direct/range {v5 .. v13}, Lcom/facebook/AccessToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;LX/GA9;Ljava/util/Date;Ljava/util/Date;)V

    :goto_5
    move-object v2, v5

    .line 2324564
    :cond_5
    move-object v0, v2

    .line 2324565
    if-eqz v0, :cond_1

    .line 2324566
    invoke-virtual {p0, v0}, LX/GA1;->a(Lcom/facebook/AccessToken;)V

    .line 2324567
    invoke-static {p0}, LX/GA1;->g(LX/GA1;)LX/GAa;

    move-result-object v1

    invoke-virtual {v1}, LX/GAa;->b()V

    goto/16 :goto_1

    .line 2324568
    :catch_0
    goto/16 :goto_0

    .line 2324569
    :cond_6
    const-string v6, "com.facebook.TokenCachingStrategy.Token"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2324570
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    .line 2324571
    const-string v6, "com.facebook.TokenCachingStrategy.ExpirationDate"

    invoke-virtual {v3, v6, v9, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    .line 2324572
    cmp-long v6, v7, v9

    if-eqz v6, :cond_3

    .line 2324573
    const/4 v5, 0x1

    goto/16 :goto_2

    .line 2324574
    :catch_1
    const/4 v5, 0x0

    goto :goto_5

    .line 2324575
    :cond_7
    invoke-static {v6}, LX/Gsc;->g(Ljava/lang/String;)LX/GAU;

    move-result-object v5

    .line 2324576
    invoke-virtual {v5}, LX/GAU;->f()LX/GAY;

    move-result-object v5

    .line 2324577
    iget-object v8, v5, LX/GAY;->d:LX/GAF;

    move-object v8, v8

    .line 2324578
    if-eqz v8, :cond_8

    .line 2324579
    const/4 v5, 0x0

    goto :goto_3

    .line 2324580
    :cond_8
    iget-object v8, v5, LX/GAY;->b:Lorg/json/JSONObject;

    move-object v5, v8

    .line 2324581
    goto :goto_3

    .line 2324582
    :cond_9
    const-string v11, "com.facebook.TokenCachingStrategy.IsSSO"

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    .line 2324583
    if-eqz v11, :cond_a

    sget-object v11, LX/GA9;->FACEBOOK_APPLICATION_WEB:LX/GA9;

    goto :goto_4

    :cond_a
    sget-object v11, LX/GA9;->WEB_VIEW:LX/GA9;

    goto :goto_4
.end method

.method public final a(Lcom/facebook/AccessToken;)V
    .locals 7

    .prologue
    .line 2324584
    const-string v0, "accessToken"

    invoke-static {p1, v0}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2324585
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 2324586
    const-string v4, "version"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2324587
    const-string v4, "token"

    iget-object v5, p1, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2324588
    const-string v4, "expires_at"

    iget-object v5, p1, Lcom/facebook/AccessToken;->e:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 2324589
    new-instance v4, Lorg/json/JSONArray;

    iget-object v5, p1, Lcom/facebook/AccessToken;->f:Ljava/util/Set;

    invoke-direct {v4, v5}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 2324590
    const-string v5, "permissions"

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2324591
    new-instance v4, Lorg/json/JSONArray;

    iget-object v5, p1, Lcom/facebook/AccessToken;->g:Ljava/util/Set;

    invoke-direct {v4, v5}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 2324592
    const-string v5, "declined_permissions"

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2324593
    const-string v4, "last_refresh"

    iget-object v5, p1, Lcom/facebook/AccessToken;->j:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 2324594
    const-string v4, "source"

    iget-object v5, p1, Lcom/facebook/AccessToken;->i:LX/GA9;

    invoke-virtual {v5}, LX/GA9;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2324595
    const-string v4, "application_id"

    iget-object v5, p1, Lcom/facebook/AccessToken;->k:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2324596
    const-string v4, "user_id"

    iget-object v5, p1, Lcom/facebook/AccessToken;->l:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2324597
    move-object v0, v3

    .line 2324598
    iget-object v1, p0, LX/GA1;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "com.facebook.AccessTokenManager.CachedAccessToken"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2324599
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
