.class public final LX/HE3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/view/View;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:Z

.field public final g:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public final h:LX/HDw;

.field public i:Landroid/view/View;

.field public j:Lcom/facebook/resources/ui/FbTextView;

.field public final synthetic k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;Landroid/view/View;Landroid/view/View;Lcom/facebook/resources/ui/FbTextView;ZIIIILcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V
    .locals 1

    .prologue
    .line 2441400
    iput-object p1, p0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2441401
    iput-object p2, p0, LX/HE3;->a:Landroid/view/View;

    .line 2441402
    iput-object p3, p0, LX/HE3;->i:Landroid/view/View;

    .line 2441403
    iput-object p4, p0, LX/HE3;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2441404
    iput-boolean p5, p0, LX/HE3;->f:Z

    .line 2441405
    iput p8, p0, LX/HE3;->b:I

    .line 2441406
    iput p9, p0, LX/HE3;->c:I

    .line 2441407
    iput p6, p0, LX/HE3;->e:I

    .line 2441408
    iput p7, p0, LX/HE3;->d:I

    .line 2441409
    iput-object p10, p0, LX/HE3;->g:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2441410
    iget-object v0, p1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->c:LX/HDw;

    iput-object v0, p0, LX/HE3;->h:LX/HDw;

    .line 2441411
    new-instance v0, LX/HE2;

    invoke-direct {v0, p0, p1}, LX/HE2;-><init>(LX/HE3;Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2441412
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2441413
    iget-object v0, p0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->f:LX/HE3;

    if-ne v0, p0, :cond_0

    .line 2441414
    :goto_0
    return-void

    .line 2441415
    :cond_0
    iget-object v0, p0, LX/HE3;->i:Landroid/view/View;

    iget v1, p0, LX/HE3;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2441416
    iget-object v0, p0, LX/HE3;->j:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0217e4

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 2441417
    iget-object v0, p0, LX/HE3;->j:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a055d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2441418
    iget-object v0, p0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->f:LX/HE3;

    if-eqz v0, :cond_1

    .line 2441419
    iget-object v0, p0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->f:LX/HE3;

    .line 2441420
    iget-object v1, v0, LX/HE3;->i:Landroid/view/View;

    iget v2, v0, LX/HE3;->c:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2441421
    iget-object v1, v0, LX/HE3;->j:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    .line 2441422
    iget-object v1, v0, LX/HE3;->j:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, v0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2441423
    :cond_1
    iget-object v0, p0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    .line 2441424
    iput-object p0, v0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->f:LX/HE3;

    .line 2441425
    iget-object v0, p0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    iget v1, p0, LX/HE3;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2441426
    iget-object v0, p0, LX/HE3;->k:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->j:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method
