.class public LX/FhL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/FhL;


# instance fields
.field public final a:LX/8cT;

.field public final b:LX/3Qe;

.field public final c:LX/0ad;

.field public final d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/8cT;LX/3Qe;LX/0ad;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2272765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2272766
    iput-object p1, p0, LX/FhL;->a:LX/8cT;

    .line 2272767
    iput-object p2, p0, LX/FhL;->b:LX/3Qe;

    .line 2272768
    iput-object p3, p0, LX/FhL;->c:LX/0ad;

    .line 2272769
    iput-object p4, p0, LX/FhL;->d:LX/0Uh;

    .line 2272770
    return-void
.end method

.method public static a(LX/0QB;)LX/FhL;
    .locals 7

    .prologue
    .line 2272771
    sget-object v0, LX/FhL;->e:LX/FhL;

    if-nez v0, :cond_1

    .line 2272772
    const-class v1, LX/FhL;

    monitor-enter v1

    .line 2272773
    :try_start_0
    sget-object v0, LX/FhL;->e:LX/FhL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2272774
    if-eqz v2, :cond_0

    .line 2272775
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2272776
    new-instance p0, LX/FhL;

    invoke-static {v0}, LX/8cT;->b(LX/0QB;)LX/8cT;

    move-result-object v3

    check-cast v3, LX/8cT;

    invoke-static {v0}, LX/3Qe;->a(LX/0QB;)LX/3Qe;

    move-result-object v4

    check-cast v4, LX/3Qe;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FhL;-><init>(LX/8cT;LX/3Qe;LX/0ad;LX/0Uh;)V

    .line 2272777
    move-object v0, p0

    .line 2272778
    sput-object v0, LX/FhL;->e:LX/FhL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2272779
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2272780
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2272781
    :cond_1
    sget-object v0, LX/FhL;->e:LX/FhL;

    return-object v0

    .line 2272782
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2272783
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
