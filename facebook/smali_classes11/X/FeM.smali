.class public LX/FeM;
.super LX/2s5;
.source ""

# interfaces
.implements LX/6Ue;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field private final d:LX/7BH;

.field public final e:Lcom/facebook/search/model/KeywordTypeaheadUnit;

.field private final f:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

.field private final g:LX/8ci;

.field private final h:LX/FZb;

.field private final i:LX/CyE;

.field private final j:LX/0hL;

.field public final k:LX/Fff;

.field public final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/CwB;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0wM;


# direct methods
.method public constructor <init>(LX/0gc;LX/7BH;Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;LX/FZb;LX/CyE;LX/0Px;Landroid/content/Context;LX/0hL;LX/Fff;LX/0wM;)V
    .locals 3
    .param p1    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/7BH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/search/model/KeywordTypeaheadUnit;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/search/logging/api/SearchTypeaheadSession;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/8ci;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/FZb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/CyE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "LX/7BH;",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            "Lcom/facebook/search/logging/api/SearchTypeaheadSession;",
            "LX/8ci;",
            "Lcom/facebook/search/fragment/GraphSearchChildFragment$OnResultClickListener;",
            "LX/CyE;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;",
            "Landroid/content/Context;",
            "LX/0hL;",
            "LX/Fff;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2265737
    invoke-direct {p0, p1}, LX/2s5;-><init>(LX/0gc;)V

    .line 2265738
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FeM;->l:Ljava/util/Map;

    .line 2265739
    iput-object p2, p0, LX/FeM;->d:LX/7BH;

    .line 2265740
    iput-object p3, p0, LX/FeM;->e:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2265741
    iput-object p4, p0, LX/FeM;->f:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2265742
    iput-object p5, p0, LX/FeM;->g:LX/8ci;

    .line 2265743
    iput-object p6, p0, LX/FeM;->h:LX/FZb;

    .line 2265744
    iput-object p7, p0, LX/FeM;->i:LX/CyE;

    .line 2265745
    iput-object p8, p0, LX/FeM;->a:LX/0Px;

    .line 2265746
    iput-object p9, p0, LX/FeM;->c:Landroid/content/Context;

    .line 2265747
    iput-object p10, p0, LX/FeM;->j:LX/0hL;

    .line 2265748
    iput-object p11, p0, LX/FeM;->k:LX/Fff;

    .line 2265749
    iput-object p12, p0, LX/FeM;->m:LX/0wM;

    .line 2265750
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "post"

    const v2, 0x7f020984

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "star"

    const v2, 0x7f0209e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "info-solid"

    const v2, 0x7f0208ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "like"

    const v2, 0x7f0208fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "bulb"

    const v2, 0x7f0207a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "fork-knife"

    const v2, 0x7f020884

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "trending"

    const v2, 0x7f020a0f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "play-solid"

    const v2, 0x7f02096e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "app-mentions"

    const v2, 0x7f02073f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/FeM;->b:LX/0P1;

    .line 2265751
    return-void
.end method


# virtual methods
.method public A_(I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2265733
    iget-object v0, p0, LX/FeM;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2265734
    iget-object v1, p0, LX/FeM;->b:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2265735
    iget-object v1, p0, LX/FeM;->m:LX/0wM;

    iget-object v2, p0, LX/FeM;->b:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, LX/FeM;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2265736
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2265731
    invoke-virtual {p0, p1}, LX/FeM;->e(I)I

    move-result v0

    .line 2265732
    iget-object v1, p0, LX/FeM;->a:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->c()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel$QueryTitleModel;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2265721
    iget-object v0, p0, LX/FeM;->k:LX/Fff;

    invoke-virtual {v0}, LX/Fff;->c()Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    move-result-object v1

    .line 2265722
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2265723
    const-string v2, "search_theme"

    iget-object v3, p0, LX/FeM;->d:LX/7BH;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2265724
    const-string v2, "tab_bar_tap"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2265725
    invoke-interface {v1}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2265726
    iget-object v0, p0, LX/FeM;->h:LX/FZb;

    invoke-interface {v1, v0}, LX/FZU;->a(LX/FZb;)V

    .line 2265727
    instance-of v0, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2265728
    check-cast v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v2, p0, LX/FeM;->i:LX/CyE;

    .line 2265729
    iput-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aC:LX/CyE;

    .line 2265730
    :cond_0
    invoke-interface {v1}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2265698
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2265699
    iget-object v1, p0, LX/FeM;->e:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FeM;->e:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2265700
    iget-object v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j:Ljava/lang/String;

    move-object v1, v2

    .line 2265701
    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 2265702
    check-cast v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    new-instance v2, LX/CzK;

    iget-object v3, p0, LX/FeM;->e:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v3}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/FeM;->e:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2265703
    iget-object p1, v4, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j:Ljava/lang/String;

    move-object v4, p1

    .line 2265704
    invoke-direct {v2, v3, v4}, LX/CzK;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2265705
    iput-object v2, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ai:LX/CzK;

    .line 2265706
    move-object v1, v0

    .line 2265707
    check-cast v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v2, p0, LX/FeM;->e:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2265708
    iget-object v3, v2, Lcom/facebook/search/model/KeywordTypeaheadUnit;->u:Ljava/lang/String;

    move-object v2, v3

    .line 2265709
    iput-object v2, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aj:Ljava/lang/String;

    .line 2265710
    :cond_0
    iget-object v1, p0, LX/FeM;->k:LX/Fff;

    .line 2265711
    invoke-virtual {p0, p2}, LX/FeM;->e(I)I

    move-result v3

    .line 2265712
    iget-object v2, p0, LX/FeM;->l:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2265713
    iget-object v2, p0, LX/FeM;->k:LX/Fff;

    iget-object v4, p0, LX/FeM;->e:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    const/4 p1, 0x0

    invoke-virtual {v2, v4, p1}, LX/FfQ;->a(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v4

    iget-object v2, p0, LX/FeM;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModels$SearchResultsSerpTabsModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2265714
    iput-object v2, v4, LX/CwH;->c:Ljava/lang/String;

    .line 2265715
    move-object v2, v4

    .line 2265716
    invoke-virtual {v2}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v2

    .line 2265717
    iget-object v4, p0, LX/FeM;->l:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v4, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2265718
    :cond_1
    iget-object v2, p0, LX/FeM;->l:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CwB;

    move-object v2, v2

    .line 2265719
    iget-object v3, p0, LX/FeM;->f:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v4, p0, LX/FeM;->g:LX/8ci;

    invoke-virtual {v1, v0, v2, v3, v4}, LX/FfQ;->a(Landroid/support/v4/app/Fragment;LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2265720
    return-object v0
.end method

.method public final a(Landroid/widget/TextView;I)V
    .locals 0

    .prologue
    .line 2265697
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2265696
    iget-object v0, p0, LX/FeM;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2265694
    iget-object v0, p0, LX/FeM;->j:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    :cond_0
    return p1
.end method

.method public k_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2265695
    invoke-virtual {p0, p1}, LX/0gG;->G_(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
