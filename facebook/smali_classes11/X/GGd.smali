.class public LX/GGd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/8wL;",
            "LX/0Or",
            "<+",
            "LX/GGc;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/GGm;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHP;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GH3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHS;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHV;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHW;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHZ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHN;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHF;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHD;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GH5;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHB;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GGx;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GGn;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GGl;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GGp;",
            ">;",
            "LX/0Or",
            "<",
            "LX/GHH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334414
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    .line 2334415
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    move-object/from16 v0, p13

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334416
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOST_EVENT:LX/8wL;

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334417
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334418
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOST_POST_INSIGHTS:LX/8wL;

    invoke-interface {v1, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334419
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->PAGE_LIKE:LX/8wL;

    invoke-interface {v1, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334420
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    invoke-interface {v1, v2, p7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334421
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->PROMOTE_CTA:LX/8wL;

    invoke-interface {v1, v2, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334422
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    invoke-interface {v1, v2, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334423
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_TARGETING:LX/8wL;

    invoke-interface {v1, v2, p8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334424
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION:LX/8wL;

    invoke-interface {v1, v2, p9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334425
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    invoke-interface {v1, v2, p10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334426
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_BUDGET:LX/8wL;

    invoke-interface {v1, v2, p11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334427
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOST_EVENT_EDIT_CREATIVE:LX/8wL;

    invoke-interface {v1, v2, p12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334428
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->PAGE_LIKE_EDIT_CREATIVE:LX/8wL;

    invoke-interface {v1, v2, p12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334429
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->LOCAL_AWARENESS_EDIT_CREATIVE:LX/8wL;

    invoke-interface {v1, v2, p12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334430
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->PROMOTE_WEBSITE_EDIT_CREATIVE:LX/8wL;

    invoke-interface {v1, v2, p12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334431
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->PROMOTE_CTA_EDIT_CREATIVE:LX/8wL;

    invoke-interface {v1, v2, p12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334432
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->PAGE_LIKE_EDIT_RUNNING_CREATIVE:LX/8wL;

    invoke-interface {v1, v2, p12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334433
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOST_LIVE:LX/8wL;

    move-object/from16 v0, p14

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334434
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->AUDIENCE_MANAGEMENT:LX/8wL;

    move-object/from16 v0, p15

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334435
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOSTED_POST_ACTION_BUTTON:LX/8wL;

    move-object/from16 v0, p16

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334436
    iget-object v1, p0, LX/GGd;->a:Ljava/util/Map;

    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_PACING:LX/8wL;

    move-object/from16 v0, p17

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2334437
    return-void
.end method

.method public static a(LX/0QB;)LX/GGd;
    .locals 3

    .prologue
    .line 2334438
    const-class v1, LX/GGd;

    monitor-enter v1

    .line 2334439
    :try_start_0
    sget-object v0, LX/GGd;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2334440
    sput-object v2, LX/GGd;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2334441
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2334442
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/GGd;->b(LX/0QB;)LX/GGd;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2334443
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GGd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2334444
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2334445
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/GGd;
    .locals 20

    .prologue
    .line 2334446
    new-instance v2, LX/GGd;

    const/16 v3, 0x16c4

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x16d0

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x16c9

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x16d1

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x16d2

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x16d3

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v9, 0x16d4

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x16cf

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x16cd

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x16cc

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v13, 0x16ca

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v14, 0x16cb

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v15, 0x16c7

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    const/16 v16, 0x16c5

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x16c3

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    const/16 v18, 0x16c6

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    const/16 v19, 0x16ce

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    invoke-direct/range {v2 .. v19}, LX/GGd;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;)V

    .line 2334447
    return-object v2
.end method
