.class public LX/G5H;
.super LX/D2z;
.source ""


# instance fields
.field public g:LX/Fzo;

.field public h:LX/0Xl;

.field public i:LX/1Ck;


# direct methods
.method public constructor <init>(LX/5SB;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/BPq;LX/0Or;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0SG;LX/0bH;LX/0Sh;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0Ot;LX/1Pf;Ljava/lang/Boolean;LX/0qn;LX/1e4;LX/0W3;LX/0Ot;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/Fzo;LX/0Xl;LX/0Or;LX/0Ot;LX/0wL;)V
    .locals 47
    .param p1    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsFeedPrivacyEditingEnabled;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p24    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .param p30    # LX/1Pf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p31    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p43    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5SB;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/BPq;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0bH;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1e0;",
            ">;",
            "LX/1Pf;",
            "Ljava/lang/Boolean;",
            "LX/0qn;",
            "Lcom/facebook/privacy/ui/PrivacyScopeResourceResolver;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/Fzo;",
            "LX/0Xl;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318659
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move-object/from16 v17, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    move-object/from16 v20, p18

    move-object/from16 v21, p19

    move-object/from16 v22, p20

    move-object/from16 v23, p21

    move-object/from16 v24, p22

    move-object/from16 v25, p23

    move-object/from16 v26, p24

    move-object/from16 v27, p25

    move-object/from16 v28, p26

    move-object/from16 v29, p27

    move-object/from16 v30, p28

    move-object/from16 v31, p29

    move-object/from16 v32, p30

    move-object/from16 v33, p31

    move-object/from16 v34, p32

    move-object/from16 v35, p33

    move-object/from16 v36, p34

    move-object/from16 v37, p35

    move-object/from16 v38, p36

    move-object/from16 v39, p37

    move-object/from16 v40, p38

    move-object/from16 v41, p39

    move-object/from16 v42, p40

    move-object/from16 v43, p41

    move-object/from16 v44, p44

    move-object/from16 v45, p45

    move-object/from16 v46, p46

    invoke-direct/range {v2 .. v46}, LX/D2z;-><init>(LX/5SB;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/BPq;LX/0Or;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0SG;LX/0bH;LX/0Sh;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0Ot;LX/1Pf;Ljava/lang/Boolean;LX/0qn;LX/1e4;LX/0W3;LX/0Ot;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0wL;)V

    .line 2318660
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, LX/G5H;->i:LX/1Ck;

    .line 2318661
    move-object/from16 v0, p42

    move-object/from16 v1, p0

    iput-object v0, v1, LX/G5H;->g:LX/Fzo;

    .line 2318662
    move-object/from16 v0, p43

    move-object/from16 v1, p0

    iput-object v0, v1, LX/G5H;->h:LX/0Xl;

    .line 2318663
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)Z
    .locals 1

    .prologue
    .line 2318664
    sget-object v0, LX/D2z;->e:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 2318665
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 1

    .prologue
    .line 2318666
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 2318667
    new-instance v0, LX/G5G;

    invoke-direct {v0, p0}, LX/G5G;-><init>(LX/G5H;)V

    .line 2318668
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/0wD;
    .locals 1

    .prologue
    .line 2318669
    sget-object v0, LX/0wD;->TIMELINE_SOMEONE_ELSE:LX/0wD;

    return-object v0
.end method
