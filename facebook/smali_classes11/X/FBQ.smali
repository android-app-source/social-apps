.class public final LX/FBQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;)V
    .locals 0

    .prologue
    .line 2209330
    iput-object p1, p0, LX/FBQ;->a:Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x6af157aa

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209331
    iget-object v1, p0, LX/FBQ;->a:Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;

    iget-object v1, v1, Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/FBQ;->a:Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;

    iget-object v2, v2, Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;->d:LX/35f;

    invoke-virtual {v2}, LX/35f;->b()Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, LX/FBQ;->a:Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2209332
    iget-object v1, p0, LX/FBQ;->a:Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;

    const-string v2, "switch_messenger_account_button"

    .line 2209333
    iget-object v3, v1, Lcom/facebook/katana/orca/DiodeSwitchAccountFragment;->b:LX/0Zb;

    const-string p0, "click"

    const/4 p1, 0x0

    invoke-interface {v3, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2209334
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2209335
    const-string p0, "diode_qp_module"

    invoke-virtual {v3, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2209336
    const-string p0, "button"

    invoke-virtual {v3, p0}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2209337
    invoke-virtual {v3, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2209338
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2209339
    :cond_0
    const v1, 0x4fc267c3

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
