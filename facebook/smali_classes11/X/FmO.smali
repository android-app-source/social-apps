.class public final LX/FmO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2282343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282344
    return-void
.end method

.method public constructor <init>(Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;)V
    .locals 1

    .prologue
    .line 2282346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282347
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2282348
    instance-of v0, p1, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    if-eqz v0, :cond_0

    .line 2282349
    check-cast p1, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    .line 2282350
    iget-object v0, p1, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, LX/FmO;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2282351
    :goto_0
    return-void

    .line 2282352
    :cond_0
    iget-object v0, p1, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v0

    .line 2282353
    iput-object v0, p0, LX/FmO;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;
    .locals 2

    .prologue
    .line 2282345
    new-instance v0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    invoke-direct {v0, p0}, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;-><init>(LX/FmO;)V

    return-object v0
.end method
