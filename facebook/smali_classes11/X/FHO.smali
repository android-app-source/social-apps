.class public LX/FHO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final q:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Sh;

.field public final b:LX/18V;

.field public final c:LX/FHA;

.field public final d:LX/0SG;

.field public final e:LX/0Xl;

.field private final f:LX/0TD;

.field private final g:LX/6ef;

.field public final h:LX/FGi;

.field public final i:LX/2Mo;

.field private final j:LX/FHb;

.field private final k:LX/2Mn;

.field private final l:LX/FIc;

.field public final m:LX/0Uh;

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FGm;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2220312
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FHO;->q:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/18V;LX/FHA;LX/0SG;LX/0Xl;LX/0TD;LX/6ef;LX/FGi;LX/2Mo;LX/FHb;LX/2Mn;LX/FIc;LX/0Uh;)V
    .locals 1
    .param p5    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2220237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220238
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2220239
    iput-object v0, p0, LX/FHO;->n:LX/0Ot;

    .line 2220240
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2220241
    iput-object v0, p0, LX/FHO;->o:LX/0Ot;

    .line 2220242
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2220243
    iput-object v0, p0, LX/FHO;->p:LX/0Ot;

    .line 2220244
    iput-object p1, p0, LX/FHO;->a:LX/0Sh;

    .line 2220245
    iput-object p2, p0, LX/FHO;->b:LX/18V;

    .line 2220246
    iput-object p3, p0, LX/FHO;->c:LX/FHA;

    .line 2220247
    iput-object p4, p0, LX/FHO;->d:LX/0SG;

    .line 2220248
    iput-object p5, p0, LX/FHO;->e:LX/0Xl;

    .line 2220249
    iput-object p6, p0, LX/FHO;->f:LX/0TD;

    .line 2220250
    iput-object p7, p0, LX/FHO;->g:LX/6ef;

    .line 2220251
    iput-object p8, p0, LX/FHO;->h:LX/FGi;

    .line 2220252
    iput-object p9, p0, LX/FHO;->i:LX/2Mo;

    .line 2220253
    iput-object p10, p0, LX/FHO;->j:LX/FHb;

    .line 2220254
    iput-object p11, p0, LX/FHO;->k:LX/2Mn;

    .line 2220255
    iput-object p12, p0, LX/FHO;->l:LX/FIc;

    .line 2220256
    iput-object p13, p0, LX/FHO;->m:LX/0Uh;

    .line 2220257
    return-void
.end method

.method public static a(LX/0QB;)LX/FHO;
    .locals 7

    .prologue
    .line 2220285
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2220286
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2220287
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2220288
    if-nez v1, :cond_0

    .line 2220289
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2220290
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2220291
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2220292
    sget-object v1, LX/FHO;->q:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2220293
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2220294
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2220295
    :cond_1
    if-nez v1, :cond_4

    .line 2220296
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2220297
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2220298
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FHO;->b(LX/0QB;)LX/FHO;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2220299
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2220300
    if-nez v1, :cond_2

    .line 2220301
    sget-object v0, LX/FHO;->q:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHO;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2220302
    :goto_1
    if-eqz v0, :cond_3

    .line 2220303
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2220304
    :goto_3
    check-cast v0, LX/FHO;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2220305
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2220306
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2220307
    :catchall_1
    move-exception v0

    .line 2220308
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2220309
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2220310
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2220311
    :cond_2
    :try_start_8
    sget-object v0, LX/FHO;->q:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FHO;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(LX/FHO;Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/FHg;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/facebook/ui/media/attachments/MediaUploadResult;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/FHg;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;"
        }
    .end annotation

    .prologue
    .line 2220313
    sget-object v1, LX/FHN;->UN_SPECIFIED:LX/FHN;

    .line 2220314
    iget-object v0, p0, LX/FHO;->l:LX/FIc;

    invoke-virtual {v0, p1, p2}, LX/FIc;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2220315
    sget-object v1, LX/FHN;->ChunkedUDP:LX/FHN;

    .line 2220316
    :cond_0
    :goto_0
    if-eqz p3, :cond_4

    sget-object v0, LX/FHN;->Resumable:LX/FHN;

    if-eq v1, v0, :cond_4

    .line 2220317
    iget-object v0, p0, LX/FHO;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "MediaUploadServiceHelper_wrong_upload_method"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Upload method "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " does not support update with fbid"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2220318
    sget-object v0, LX/FHN;->Resumable:LX/FHN;

    .line 2220319
    :goto_1
    sget-object v1, LX/FHL;->a:[I

    invoke-virtual {v0}, LX/FHN;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2220320
    new-instance v2, LX/4d1;

    invoke-direct {v2}, LX/4d1;-><init>()V

    .line 2220321
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 2220322
    new-instance v1, LX/FHM;

    invoke-direct {v1, p0, p2}, LX/FHM;-><init>(LX/FHO;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2220323
    iput-object v1, v0, LX/14U;->a:LX/4ck;

    .line 2220324
    iput-object v2, v0, LX/14U;->c:LX/4d1;

    .line 2220325
    iget-object v1, p0, LX/FHO;->h:LX/FGi;

    invoke-virtual {v1, p2, v0}, LX/FGi;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/14U;)V

    .line 2220326
    new-instance v1, LX/FHK;

    invoke-direct {v1, p0, p2, v0}, LX/FHK;-><init>(LX/FHO;Lcom/facebook/ui/media/attachments/MediaResource;LX/14U;)V

    move-object v0, v1

    .line 2220327
    iget-object v1, p0, LX/FHO;->f:LX/0TD;

    invoke-interface {v1, v0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2220328
    :cond_1
    :try_start_0
    new-instance v1, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    const-wide/16 v4, 0x32

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v6, -0x7c1bcf81

    invoke-static {v3, v4, v5, v0, v6}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/facebook/ui/media/attachments/MediaUploadResult;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_2
    return-object v0

    .line 2220329
    :cond_2
    iget-object v0, p0, LX/FHO;->h:LX/FGi;

    invoke-virtual {v0, p2}, LX/FGi;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2220330
    sget-object v1, LX/FHN;->Resumable:LX/FHN;

    goto :goto_0

    .line 2220331
    :pswitch_0
    iget-object v0, p0, LX/FHO;->m:LX/0Uh;

    const/16 v1, 0x17b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2220332
    iget-object v0, p0, LX/FHO;->h:LX/FGi;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LX/FGi;->a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/FHg;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/facebook/ui/media/attachments/MediaUploadResult;

    move-result-object v0

    goto :goto_2

    .line 2220333
    :cond_3
    iget-object v0, p0, LX/FHO;->h:LX/FGi;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LX/FGi;->b(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/FHg;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/facebook/ui/media/attachments/MediaUploadResult;

    move-result-object v0

    goto :goto_2

    .line 2220334
    :pswitch_1
    new-instance v0, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    iget-object v1, p0, LX/FHO;->l:LX/FIc;

    invoke-virtual {v1, p1, p2}, LX/FIc;->b(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/ui/media/attachments/MediaUploadResult;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 2220335
    :catch_0
    iget-object v0, p0, LX/FHO;->g:LX/6ef;

    invoke-virtual {v0, p1}, LX/6ef;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2220336
    invoke-virtual {v2}, LX/4d1;->a()Z

    .line 2220337
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0

    :cond_4
    move-object v0, v1

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(LX/0QB;)LX/FHO;
    .locals 14

    .prologue
    .line 2220281
    new-instance v0, LX/FHO;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v2

    check-cast v2, LX/18V;

    invoke-static {p0}, LX/FHA;->b(LX/0QB;)LX/FHA;

    move-result-object v3

    check-cast v3, LX/FHA;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v5

    check-cast v5, LX/0Xl;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {p0}, LX/6ef;->a(LX/0QB;)LX/6ef;

    move-result-object v7

    check-cast v7, LX/6ef;

    invoke-static {p0}, LX/FGi;->a(LX/0QB;)LX/FGi;

    move-result-object v8

    check-cast v8, LX/FGi;

    invoke-static {p0}, LX/2Mo;->a(LX/0QB;)LX/2Mo;

    move-result-object v9

    check-cast v9, LX/2Mo;

    invoke-static {p0}, LX/FHb;->a(LX/0QB;)LX/FHb;

    move-result-object v10

    check-cast v10, LX/FHb;

    invoke-static {p0}, LX/2Mn;->a(LX/0QB;)LX/2Mn;

    move-result-object v11

    check-cast v11, LX/2Mn;

    invoke-static {p0}, LX/FIc;->a(LX/0QB;)LX/FIc;

    move-result-object v12

    check-cast v12, LX/FIc;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v13

    check-cast v13, LX/0Uh;

    invoke-direct/range {v0 .. v13}, LX/FHO;-><init>(LX/0Sh;LX/18V;LX/FHA;LX/0SG;LX/0Xl;LX/0TD;LX/6ef;LX/FGi;LX/2Mo;LX/FHb;LX/2Mn;LX/FIc;LX/0Uh;)V

    .line 2220282
    const/16 v1, 0x259

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x27f8

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1032

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2220283
    iput-object v1, v0, LX/FHO;->n:LX/0Ot;

    iput-object v2, v0, LX/FHO;->o:LX/0Ot;

    iput-object v3, v0, LX/FHO;->p:LX/0Ot;

    .line 2220284
    return-object v0
.end method


# virtual methods
.method public final a(ZZLcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/FHg;)Lcom/facebook/ui/media/attachments/MediaUploadResult;
    .locals 11

    .prologue
    .line 2220258
    new-instance v6, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v6}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 2220259
    iget-object v0, p0, LX/FHO;->j:LX/FHb;

    invoke-virtual {v0, p3, p2, p1}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;ZZ)V

    .line 2220260
    iget-object v0, p0, LX/FHO;->k:LX/2Mn;

    invoke-virtual {v0, p3, p4}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    .line 2220261
    const/4 v2, 0x0

    .line 2220262
    const/4 v1, 0x0

    .line 2220263
    iget-object v0, p0, LX/FHO;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/FGS;->m:S

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2220264
    iget-object v0, p0, LX/FHO;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/FGS;->n:I

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, LX/0ad;->a(II)I

    move-result v0

    move v7, v0

    .line 2220265
    :goto_0
    new-instance v9, LX/6eb;

    iget-object v0, p0, LX/FHO;->a:LX/0Sh;

    invoke-direct {v9, v0}, LX/6eb;-><init>(LX/0Sh;)V

    move v8, v2

    :goto_1
    move-object v0, p0

    move-object/from16 v1, p6

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    .line 2220266
    :try_start_0
    invoke-static/range {v0 .. v6}, LX/FHO;->a(LX/FHO;Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/FHg;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/facebook/ui/media/attachments/MediaUploadResult;

    move-result-object v10

    .line 2220267
    iget-object v0, p0, LX/FHO;->j:LX/FHb;

    invoke-virtual {v10}, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    move-object v1, p3

    move v4, p2

    move v5, p1

    invoke-virtual/range {v0 .. v5}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/util/Map;ZZ)V

    .line 2220268
    iget-object v1, p0, LX/FHO;->k:LX/2Mn;

    invoke-virtual {v10}, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, p3, v2, v8, v0}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;ILjava/util/Map;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2220269
    return-object v10

    .line 2220270
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2220271
    iget-object v2, p0, LX/FHO;->j:LX/FHb;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v2, p3, v0, p2, p1}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/util/Map;ZZ)V

    .line 2220272
    iget-object v2, p0, LX/FHO;->k:LX/2Mn;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v2, p3, v8, v0}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/util/Map;)V

    .line 2220273
    throw v1

    .line 2220274
    :catch_1
    move-exception v2

    .line 2220275
    if-ge v8, v7, :cond_0

    iget-object v0, p0, LX/FHO;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v2}, LX/FGm;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2220276
    add-int/lit8 v0, v8, 0x1

    .line 2220277
    invoke-virtual {v9}, LX/6eb;->b()V

    move v8, v0

    goto :goto_1

    .line 2220278
    :cond_0
    iget-object v0, p0, LX/FHO;->j:LX/FHb;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    move-object v1, p3

    move v4, p2

    move v5, p1

    invoke-virtual/range {v0 .. v5}, LX/FHb;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/Exception;Ljava/util/Map;ZZ)V

    .line 2220279
    iget-object v1, p0, LX/FHO;->k:LX/2Mn;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {v1, p3, v8, v2, v0}, LX/2Mn;->a(Lcom/facebook/ui/media/attachments/MediaResource;ILjava/lang/Throwable;Ljava/util/Map;)V

    .line 2220280
    throw v2

    :cond_1
    move v7, v1

    goto :goto_0
.end method
