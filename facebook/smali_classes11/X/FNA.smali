.class public LX/FNA;
.super LX/6LI;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FOd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/30m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/FNE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/messaging/model/threads/ThreadSummary;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/user/tiles/UserTileView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/widget/text/BetterTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/widget/text/BetterTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/widget/text/BetterTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/widget/text/BetterTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/6lI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2232295
    const-string v0, "IdentityMatchingNotification"

    invoke-direct {p0, v0}, LX/6LI;-><init>(Ljava/lang/String;)V

    .line 2232296
    return-void
.end method

.method public static synthetic f(LX/FNA;)V
    .locals 15

    .prologue
    .line 2232273
    iget-object v0, p0, LX/FNA;->q:LX/6lI;

    if-nez v0, :cond_0

    .line 2232274
    invoke-static {p0}, LX/FNA;->f$redex0(LX/FNA;)V

    .line 2232275
    :goto_0
    return-void

    .line 2232276
    :cond_0
    iget-object v0, p0, LX/FNA;->h:LX/FNE;

    iget-object v1, p0, LX/FNA;->a:Landroid/content/Context;

    iget-object v2, p0, LX/FNA;->q:LX/6lI;

    iget-object v2, v2, LX/6lI;->a:Ljava/lang/String;

    iget-object v3, p0, LX/FNA;->q:LX/6lI;

    invoke-static {v3}, LX/6lI;->a(LX/6lI;)Lcom/facebook/user/model/User;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, LX/FN9;

    invoke-direct {v5, p0}, LX/FN9;-><init>(LX/FNA;)V

    .line 2232277
    new-instance v6, LX/FND;

    invoke-static {v0}, LX/3Lx;->b(LX/0QB;)LX/3Lx;

    move-result-object v12

    check-cast v12, LX/3Lx;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v13

    check-cast v13, Landroid/view/LayoutInflater;

    invoke-static {v0}, LX/FOd;->b(LX/0QB;)LX/FOd;

    move-result-object v14

    check-cast v14, LX/FOd;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v14}, LX/FND;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/user/model/User;ZLX/FN9;LX/3Lx;Landroid/view/LayoutInflater;LX/FOd;)V

    .line 2232278
    move-object v0, v6

    .line 2232279
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2232280
    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    iget-object v1, v0, LX/FND;->d:Landroid/content/Context;

    const v2, 0x7f082ec1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    iget-object v1, v0, LX/FND;->d:Landroid/content/Context;

    const v2, 0x7f082ec2

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, v0, LX/FND;->f:Lcom/facebook/user/model/User;

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v7

    const/4 v1, 0x2

    iget-object v2, v0, LX/FND;->d:Landroid/content/Context;

    const v4, 0x7f082ec3

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v1

    .line 2232281
    iput v6, v0, LX/FND;->i:I

    .line 2232282
    iget-object v1, v0, LX/FND;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f03135a

    invoke-virtual {v1, v2, v8, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2232283
    const v2, 0x7f0d1602

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/tiles/UserTileView;

    .line 2232284
    iget-object v4, v0, LX/FND;->f:Lcom/facebook/user/model/User;

    .line 2232285
    iget-object v5, v4, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v5

    .line 2232286
    invoke-static {v4}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2232287
    const v2, 0x7f0d037b

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterTextView;

    .line 2232288
    iget-object v4, v0, LX/FND;->f:Lcom/facebook/user/model/User;

    invoke-virtual {v4}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2232289
    const v2, 0x7f0d2c17

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterTextView;

    .line 2232290
    iget-object v4, v0, LX/FND;->e:Ljava/lang/String;

    invoke-static {v4}, LX/3Lx;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2232291
    iget-object v4, v0, LX/FND;->a:LX/3Lx;

    iget-object v5, v0, LX/FND;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/3Lx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2232292
    :goto_1
    new-instance v2, LX/31Y;

    iget-object v4, v0, LX/FND;->d:Landroid/content/Context;

    invoke-direct {v2, v4}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, LX/0ju;->a(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x104000a

    new-instance v4, LX/FNC;

    invoke-direct {v4, v0}, LX/FNC;-><init>(LX/FND;)V

    invoke-virtual {v1, v2, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    new-instance v2, LX/FNB;

    invoke-direct {v2, v0}, LX/FNB;-><init>(LX/FND;)V

    invoke-virtual {v1, v3, v6, v2}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v8}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v7}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2232293
    goto/16 :goto_0

    .line 2232294
    :cond_1
    iget-object v4, v0, LX/FND;->e:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static f$redex0(LX/FNA;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2232246
    iput-object v0, p0, LX/FNA;->r:Ljava/lang/String;

    .line 2232247
    iput-object v0, p0, LX/FNA;->q:LX/6lI;

    .line 2232248
    iget-object v0, p0, LX/6LI;->a:LX/6LN;

    move-object v0, v0

    .line 2232249
    invoke-virtual {v0, p0}, LX/6LN;->b(LX/6LI;)V

    .line 2232250
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2232251
    iget-object v0, p0, LX/FNA;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f031359

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/FNA;->k:Landroid/view/View;

    .line 2232252
    iget-object v0, p0, LX/FNA;->k:Landroid/view/View;

    const v1, 0x7f0d1602

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/FNA;->l:Lcom/facebook/user/tiles/UserTileView;

    .line 2232253
    iget-object v0, p0, LX/FNA;->k:Landroid/view/View;

    const v1, 0x7f0d02c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/FNA;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 2232254
    iget-object v0, p0, LX/FNA;->k:Landroid/view/View;

    const v1, 0x7f0d02c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/FNA;->n:Lcom/facebook/widget/text/BetterTextView;

    .line 2232255
    iget-object v0, p0, LX/FNA;->k:Landroid/view/View;

    const v1, 0x7f0d2cbf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/FNA;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 2232256
    iget-object v0, p0, LX/FNA;->k:Landroid/view/View;

    const v1, 0x7f0d2cbe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/FNA;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 2232257
    iget-object v0, p0, LX/FNA;->o:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/FN7;

    invoke-direct {v1, p0}, LX/FN7;-><init>(LX/FNA;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2232258
    iget-object v0, p0, LX/FNA;->p:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/FN8;

    invoke-direct {v1, p0}, LX/FN8;-><init>(LX/FNA;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2232259
    iget-object v0, p0, LX/FNA;->q:LX/6lI;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2232260
    iget-object v0, p0, LX/FNA;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FNA;->o:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FNA;->p:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    .line 2232261
    iget-object v0, p0, LX/FNA;->j:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 2232262
    iget v1, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    move v0, v1

    .line 2232263
    iget-object v1, p0, LX/FNA;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2232264
    iget-object v1, p0, LX/FNA;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2232265
    :cond_0
    iget-object v0, p0, LX/FNA;->l:Lcom/facebook/user/tiles/UserTileView;

    if-eqz v0, :cond_1

    .line 2232266
    iget-object v0, p0, LX/FNA;->q:LX/6lI;

    iget-object v0, v0, LX/6lI;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v0

    .line 2232267
    iget-object v1, p0, LX/FNA;->l:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v1, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2232268
    :cond_1
    iget-object v0, p0, LX/FNA;->m:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FNA;->n:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_2

    .line 2232269
    iget-object v0, p0, LX/FNA;->m:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/FNA;->q:LX/6lI;

    iget-object v1, v1, LX/6lI;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2232270
    iget-object v1, p0, LX/FNA;->n:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, LX/FNA;->a:Landroid/content/Context;

    iget-boolean v0, p0, LX/FNA;->s:Z

    if-eqz v0, :cond_3

    const v0, 0x7f082ebf

    :goto_0
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object p1, p0, LX/FNA;->q:LX/6lI;

    iget-object p1, p1, LX/6lI;->d:Ljava/lang/String;

    aput-object p1, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2232271
    :cond_2
    iget-object v0, p0, LX/FNA;->k:Landroid/view/View;

    return-object v0

    .line 2232272
    :cond_3
    const v0, 0x7f082ec0

    goto :goto_0
.end method
