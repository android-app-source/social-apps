.class public LX/G4S;
.super LX/G4N;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/55H;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G15;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/55H;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G15;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317658
    invoke-direct {p0, p3, p4}, LX/G4N;-><init>(LX/0Or;LX/03V;)V

    .line 2317659
    iput-object p1, p0, LX/G4S;->a:LX/0Or;

    .line 2317660
    iput-object p2, p0, LX/G4S;->b:LX/0Or;

    .line 2317661
    return-void
.end method

.method public static b(LX/0QB;)LX/G4S;
    .locals 5

    .prologue
    .line 2317662
    new-instance v1, LX/G4S;

    const/16 v0, 0x1753

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v0, 0x36ad

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v0, 0xb83

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-direct {v1, v2, v3, v4, v0}, LX/G4S;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/03V;)V

    .line 2317663
    return-object v1
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2317664
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2317665
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2317666
    const-string v2, "timeline_delete_story"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2317667
    iget-object v0, p0, LX/G4S;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {p0, v1, v0}, LX/G4N;->a(Landroid/os/Bundle;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2317668
    :goto_0
    return-object v0

    .line 2317669
    :cond_0
    const-string v2, "timeline_hide_story"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2317670
    iget-object v0, p0, LX/G4S;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {p0, v1, v0}, LX/G4N;->a(Landroid/os/Bundle;LX/0e6;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2317671
    :cond_1
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1
.end method
