.class public final LX/H3e;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/H3f;


# direct methods
.method public constructor <init>(LX/H3f;)V
    .locals 0

    .prologue
    .line 2421494
    iput-object p1, p0, LX/H3e;->a:LX/H3f;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2421495
    iget-object v0, p0, LX/H3e;->a:LX/H3f;

    iget-object v0, v0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    sget-object v1, LX/FOw;->DEVICE_NON_OPTIMAL_LOCATION_SETTING:LX/FOw;

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a$redex0(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/FOw;)V

    .line 2421496
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2421497
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2421498
    iget-object v0, p0, LX/H3e;->a:LX/H3f;

    iget-object v0, v0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    const/4 p0, 0x0

    .line 2421499
    iput-boolean p0, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->o:Z

    .line 2421500
    iget-object v1, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421501
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v1, v2

    .line 2421502
    sget-object v2, LX/FOw;->OKAY:LX/FOw;

    .line 2421503
    iput-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    .line 2421504
    iget-object v1, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421505
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v1, v2

    .line 2421506
    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a(Landroid/location/Location;)V

    .line 2421507
    iget-object v1, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421508
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v1, v2

    .line 2421509
    iget-object v2, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2421510
    iget-object p1, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, p1

    .line 2421511
    iget-boolean p1, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v2, p1

    .line 2421512
    invoke-static {v0, v1, v2}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;Z)V

    .line 2421513
    iput-boolean p0, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->n:Z

    .line 2421514
    return-void
.end method
