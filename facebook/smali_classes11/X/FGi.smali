.class public LX/FGi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final r:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Sh;

.field private final b:LX/FHA;

.field private final c:LX/18V;

.field public final d:LX/2MM;

.field private final e:LX/FHn;

.field private final f:LX/FGl;

.field private final g:LX/7zS;

.field private final h:LX/2Mp;

.field private final i:LX/2ML;

.field public final j:LX/0Xl;

.field private final k:LX/0ad;

.field private final l:LX/0Uh;

.field private final m:LX/1tL;

.field public n:LX/7z2;

.field public o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/fbuploader/FbUploader$FbUploadJobHandle;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2219488
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FGi;->r:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/FHA;LX/18V;LX/2MM;LX/0Sh;LX/FHn;LX/FGl;LX/7zS;LX/2Mp;LX/2ML;LX/0Xl;LX/0ad;LX/0Uh;LX/1tL;)V
    .locals 6
    .param p10    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2219469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219470
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, LX/FGi;->p:LX/0Ot;

    .line 2219471
    iput-object p1, p0, LX/FGi;->b:LX/FHA;

    .line 2219472
    iput-object p4, p0, LX/FGi;->a:LX/0Sh;

    .line 2219473
    iput-object p2, p0, LX/FGi;->c:LX/18V;

    .line 2219474
    iput-object p3, p0, LX/FGi;->d:LX/2MM;

    .line 2219475
    iput-object p5, p0, LX/FGi;->e:LX/FHn;

    .line 2219476
    iput-object p6, p0, LX/FGi;->f:LX/FGl;

    .line 2219477
    iput-object p7, p0, LX/FGi;->g:LX/7zS;

    .line 2219478
    iput-object p8, p0, LX/FGi;->h:LX/2Mp;

    .line 2219479
    iput-object p9, p0, LX/FGi;->i:LX/2ML;

    .line 2219480
    move-object/from16 v0, p10

    iput-object v0, p0, LX/FGi;->j:LX/0Xl;

    .line 2219481
    iget-object v2, p0, LX/FGi;->g:LX/7zS;

    invoke-virtual {v2}, LX/7zS;->a()LX/7z2;

    move-result-object v2

    iput-object v2, p0, LX/FGi;->n:LX/7z2;

    .line 2219482
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, p0, LX/FGi;->o:Ljava/util/Map;

    .line 2219483
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v2

    const-wide/32 v4, 0x7b98a000

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v2

    invoke-virtual {v2}, LX/0QN;->q()LX/0QI;

    move-result-object v2

    iput-object v2, p0, LX/FGi;->q:LX/0QI;

    .line 2219484
    move-object/from16 v0, p11

    iput-object v0, p0, LX/FGi;->k:LX/0ad;

    .line 2219485
    move-object/from16 v0, p12

    iput-object v0, p0, LX/FGi;->l:LX/0Uh;

    .line 2219486
    move-object/from16 v0, p13

    iput-object v0, p0, LX/FGi;->m:LX/1tL;

    .line 2219487
    return-void
.end method

.method public static a(LX/0QB;)LX/FGi;
    .locals 7

    .prologue
    .line 2219442
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2219443
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2219444
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2219445
    if-nez v1, :cond_0

    .line 2219446
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2219447
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2219448
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2219449
    sget-object v1, LX/FGi;->r:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2219450
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2219451
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2219452
    :cond_1
    if-nez v1, :cond_4

    .line 2219453
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2219454
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2219455
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FGi;->b(LX/0QB;)LX/FGi;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2219456
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2219457
    if-nez v1, :cond_2

    .line 2219458
    sget-object v0, LX/FGi;->r:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGi;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2219459
    :goto_1
    if-eqz v0, :cond_3

    .line 2219460
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2219461
    :goto_3
    check-cast v0, LX/FGi;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2219462
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2219463
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2219464
    :catchall_1
    move-exception v0

    .line 2219465
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2219466
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2219467
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2219468
    :cond_2
    :try_start_8
    sget-object v0, LX/FGi;->r:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGi;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(LX/7zL;Ljava/io/File;)Lcom/facebook/ui/media/attachments/MediaUploadResult;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2219429
    sget-object v0, LX/51s;->a:LX/51l;

    move-object v0, v0

    .line 2219430
    invoke-static {p1, v0}, LX/1t3;->a(Ljava/io/File;LX/51l;)LX/51o;

    move-result-object v0

    invoke-virtual {v0}, LX/51o;->d()[B

    move-result-object v2

    .line 2219431
    iget-object v0, p0, LX/7zL;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2219432
    iget-object v0, p0, LX/7zL;->a:Ljava/lang/String;

    .line 2219433
    :goto_0
    iget-object v3, p0, LX/7zL;->b:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2219434
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    iget-object v4, p0, LX/7zL;->b:Ljava/lang/String;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2219435
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2219436
    :goto_1
    const-string v4, "mac"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v3, v1

    move-object v1, v0

    .line 2219437
    :goto_2
    new-instance v0, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/ui/media/attachments/MediaUploadResult;-><init>(Ljava/lang/String;[BLjava/lang/String;J)V

    return-object v0

    .line 2219438
    :cond_0
    :try_start_1
    const-string v4, "media_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 2219439
    :catch_0
    move-exception v3

    .line 2219440
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2219441
    throw v3

    :cond_1
    move-object v3, v1

    move-object v1, v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private static a(LX/FGi;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "LX/FHf;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2219393
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2219394
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v2, :cond_4

    .line 2219395
    const-string v0, "image_type"

    invoke-static {p1}, LX/FGi;->d(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219396
    :cond_0
    :goto_0
    sget-object v0, LX/FHf;->PRE_UPLOAD:LX/FHf;

    if-ne v0, p2, :cond_1

    .line 2219397
    const-string v0, "is_temporary"

    const-string v2, "1"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219398
    :cond_1
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v2, :cond_2

    if-eqz p4, :cond_2

    .line 2219399
    const-string v0, "video_content_mode"

    const-string v2, "preview"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219400
    const-string v0, "video_full_duration"

    invoke-interface {v1, v0, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219401
    :cond_2
    if-eqz p3, :cond_a

    .line 2219402
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v2, :cond_9

    .line 2219403
    const-string v0, "video_original_fbid"

    invoke-interface {v1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219404
    :cond_3
    :goto_1
    return-object v1

    .line 2219405
    :cond_4
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v2, :cond_5

    .line 2219406
    const-string v0, "video_type"

    .line 2219407
    invoke-static {p1}, LX/6eg;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5dX;

    move-result-object v2

    iget-object v2, v2, LX/5dX;->apiStringValue:Ljava/lang/String;

    move-object v2, v2

    .line 2219408
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2219409
    :cond_5
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    if-ne v0, v2, :cond_6

    .line 2219410
    const-string v0, "encrypted_blob"

    const-string v2, "FILE_ATTACHMENT"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2219411
    :cond_6
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->ENT_PHOTO:LX/2MK;

    if-ne v0, v2, :cond_7

    .line 2219412
    const-string v0, "image_type"

    invoke-static {p1}, LX/FGi;->d(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219413
    const-string v0, "use_ent_photo"

    const-string v2, "1"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2219414
    :cond_7
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v2, LX/2MK;->AUDIO:LX/2MK;

    if-ne v0, v2, :cond_0

    .line 2219415
    const-string v0, "audio_type"

    const-string v2, "VOICE_MESSAGE"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219416
    const-string v0, "duration"

    iget-wide v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219417
    const-string v2, "is_voicemail"

    iget-boolean v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    if-eqz v0, :cond_8

    const-string v0, "1"

    :goto_2
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219418
    const-string v0, "call_id"

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2219419
    :cond_8
    const-string v0, "0"

    goto :goto_2

    .line 2219420
    :cond_9
    const-string v0, "original_fbid"

    invoke-interface {v1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2219421
    :cond_a
    iget-object v0, p0, LX/FGi;->h:LX/2Mp;

    invoke-virtual {v0, p1}, LX/2Mp;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, LX/FGi;->i:LX/2ML;

    invoke-virtual {v0, p1}, LX/2ML;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2219422
    const-string v0, "media_hash"

    iget-object v2, p0, LX/FGi;->i:LX/2ML;

    invoke-virtual {v2, p1}, LX/2ML;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219423
    :cond_b
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 2219424
    if-eqz v0, :cond_3

    .line 2219425
    const-string v2, "attribution_app_id"

    iget-object v3, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219426
    const-string v2, "android_key_hash"

    iget-object v3, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219427
    iget-object v2, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 2219428
    const-string v2, "attribution_app_metadata"

    iget-object v0, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method private static b(Lcom/facebook/ui/media/attachments/MediaResource;)I
    .locals 2

    .prologue
    .line 2219390
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 2219391
    const/16 v0, 0x41

    .line 2219392
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x28

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/FGi;
    .locals 14

    .prologue
    .line 2219386
    new-instance v0, LX/FGi;

    invoke-static {p0}, LX/FHA;->b(LX/0QB;)LX/FHA;

    move-result-object v1

    check-cast v1, LX/FHA;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v2

    check-cast v2, LX/18V;

    invoke-static {p0}, LX/2MM;->a(LX/0QB;)LX/2MM;

    move-result-object v3

    check-cast v3, LX/2MM;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/FHn;->a(LX/0QB;)LX/FHn;

    move-result-object v5

    check-cast v5, LX/FHn;

    invoke-static {p0}, LX/FGl;->b(LX/0QB;)LX/FGl;

    move-result-object v6

    check-cast v6, LX/FGl;

    invoke-static {p0}, LX/7zS;->a(LX/0QB;)LX/7zS;

    move-result-object v7

    check-cast v7, LX/7zS;

    invoke-static {p0}, LX/2Mp;->a(LX/0QB;)LX/2Mp;

    move-result-object v8

    check-cast v8, LX/2Mp;

    invoke-static {p0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v9

    check-cast v9, LX/2ML;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-static {p0}, LX/1tL;->a(LX/0QB;)LX/1tL;

    move-result-object v13

    check-cast v13, LX/1tL;

    invoke-direct/range {v0 .. v13}, LX/FGi;-><init>(LX/FHA;LX/18V;LX/2MM;LX/0Sh;LX/FHn;LX/FGl;LX/7zS;LX/2Mp;LX/2ML;LX/0Xl;LX/0ad;LX/0Uh;LX/1tL;)V

    .line 2219387
    const/16 v1, 0xdf4

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2219388
    iput-object v1, v0, LX/FGi;->p:LX/0Ot;

    .line 2219389
    return-object v0
.end method

.method private static declared-synchronized b(LX/FGi;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2219380
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FGi;->q:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2219381
    if-eqz v0, :cond_0

    .line 2219382
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2219383
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/FGi;->q:LX/0QI;

    invoke-interface {v0, p1, p1}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, p1

    .line 2219384
    goto :goto_0

    .line 2219385
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static d(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2219277
    iget-object v0, p0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->isQuickCamSource()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/5dT;->QUICKCAM:LX/5dT;

    iget-object v0, v0, LX/5dT;->apiStringValue:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/5dT;->NONQUICKCAM:LX/5dT;

    iget-object v0, v0, LX/5dT;->apiStringValue:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/FHg;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/facebook/ui/media/attachments/MediaUploadResult;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/FHg;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;"
        }
    .end annotation

    .prologue
    .line 2219346
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FGi;->d:LX/2MM;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/FGi;->l:LX/0Uh;

    const/16 v7, 0x268

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_0
    invoke-virtual {v5, v6, v4}, LX/2MM;->a(Landroid/net/Uri;LX/46h;)LX/46f;

    move-result-object v7

    .line 2219347
    iget-object v8, v7, LX/46f;->a:Ljava/io/File;

    .line 2219348
    new-instance v9, LX/FHj;

    invoke-direct {v9}, LX/FHj;-><init>()V

    .line 2219349
    invoke-virtual {v9}, LX/FHj;->c()Ljava/util/Map;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 2219350
    sget-object v4, LX/FHi;->START_UPLOAD:LX/FHi;

    invoke-virtual {v9, v4}, LX/FHj;->a(LX/FHi;)V

    .line 2219351
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v9, v4, v5}, LX/FHj;->a(J)V

    .line 2219352
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FGi;->o:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/7z0;

    .line 2219353
    if-eqz v4, :cond_1

    .line 2219354
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FGi;->n:LX/7z2;

    invoke-virtual {v5, v4}, LX/7z2;->a(LX/7z0;)V

    .line 2219355
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FGi;->n:LX/7z2;

    invoke-virtual {v5, v4}, LX/7z2;->c(LX/7z0;)LX/7zL;

    move-result-object v4

    .line 2219356
    sget-object v5, LX/FHi;->VIDEO_POST_PROCESS_COMPLETED:LX/FHi;

    invoke-virtual {v9, v5}, LX/FHj;->a(LX/FHi;)V

    .line 2219357
    invoke-static {v4, v8}, LX/FGi;->a(LX/7zL;Ljava/io/File;)Lcom/facebook/ui/media/attachments/MediaUploadResult;

    move-result-object v4

    .line 2219358
    invoke-virtual {v7}, LX/46f;->a()V

    .line 2219359
    return-object v4

    .line 2219360
    :cond_0
    sget-object v4, LX/46h;->PREFER_SDCARD:LX/46h;

    goto :goto_0

    .line 2219361
    :cond_1
    new-instance v10, LX/7yy;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-direct {v10, v8, v4}, LX/7yy;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2219362
    invoke-virtual/range {p5 .. p5}, LX/FHg;->a()LX/FHf;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-static {v0, v1, v4, v2, v3}, LX/FGi;->a(LX/FGi;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHf;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v11

    .line 2219363
    new-instance v12, LX/7yu;

    invoke-static/range {p2 .. p2}, LX/FGi;->b(Lcom/facebook/ui/media/attachments/MediaResource;)I

    move-result v4

    const/16 v5, 0xa

    const/16 v6, 0x1388

    invoke-direct {v12, v4, v5, v6}, LX/7yu;-><init>(III)V

    .line 2219364
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v5, LX/2MK;->AUDIO:LX/2MK;

    if-eq v4, v5, :cond_2

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v5, LX/2MK;->PHOTO:LX/2MK;

    if-ne v4, v5, :cond_3

    .line 2219365
    :cond_2
    new-instance v4, LX/7ys;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/FGi;->k:LX/0ad;

    sget-short v6, LX/FGS;->c:S

    const/4 v13, 0x0

    invoke-interface {v5, v6, v13}, LX/0ad;->a(SZ)Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/FGi;->k:LX/0ad;

    sget v13, LX/FGS;->a:I

    const/16 v14, 0x400

    invoke-interface {v6, v13, v14}, LX/0ad;->a(II)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v13, v0, LX/FGi;->k:LX/0ad;

    sget-char v14, LX/FGS;->b:C

    const-string v15, "SHA256"

    invoke-interface {v13, v14, v15}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v4, v5, v6, v13}, LX/7ys;-><init>(ZILjava/lang/String;)V

    move-object v5, v4

    .line 2219366
    :goto_2
    sget-object v6, LX/7yt;->MESSENGER:LX/7yt;

    .line 2219367
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FGi;->p:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0W3;

    sget-wide v14, LX/0X5;->kn:J

    invoke-interface {v4, v14, v15}, LX/0W4;->a(J)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2219368
    sget-object v4, LX/FGh;->a:[I

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v6}, LX/2MK;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 2219369
    sget-object v4, LX/7yt;->MESSENGER:LX/7yt;

    .line 2219370
    :goto_3
    new-instance v6, LX/7yw;

    invoke-direct {v6, v4, v11, v12, v5}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;LX/7ys;)V

    .line 2219371
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FGi;->n:LX/7z2;

    new-instance v5, LX/FGg;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v5, v0, v9, v1, v2}, LX/FGg;-><init>(LX/FGi;LX/FHj;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    invoke-virtual {v4, v10, v6, v5}, LX/7z2;->a(LX/7yy;LX/7yw;LX/7yp;)LX/7z0;

    move-result-object v4

    .line 2219372
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FGi;->o:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 2219373
    :cond_3
    new-instance v4, LX/7ys;

    invoke-direct {v4}, LX/7ys;-><init>()V

    move-object v5, v4

    goto :goto_2

    .line 2219374
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/ui/media/attachments/MediaResource;->d()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2219375
    sget-object v4, LX/7yt;->MESSENGER_ANIMATED_IMAGE:LX/7yt;

    goto :goto_3

    .line 2219376
    :cond_4
    sget-object v4, LX/7yt;->MESSENGER_IMAGE:LX/7yt;

    goto :goto_3

    .line 2219377
    :pswitch_1
    sget-object v4, LX/7yt;->MESSENGER_VIDEO:LX/7yt;

    goto :goto_3

    .line 2219378
    :pswitch_2
    sget-object v4, LX/7yt;->MESSENGER_AUDIO:LX/7yt;

    goto :goto_3

    .line 2219379
    :pswitch_3
    sget-object v4, LX/7yt;->MESSENGER_FILE:LX/7yt;

    goto :goto_3

    :cond_5
    move-object v4, v6

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;LX/14U;)V
    .locals 3

    .prologue
    .line 2219339
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-eq v0, v1, :cond_1

    .line 2219340
    :cond_0
    :goto_0
    return-void

    .line 2219341
    :cond_1
    iget-object v0, p0, LX/FGi;->l:LX/0Uh;

    const/16 v1, 0x1b1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2219342
    iget-object v0, p0, LX/FGi;->m:LX/1tL;

    invoke-virtual {v0}, LX/1tL;->a()Ljava/lang/String;

    move-result-object v0

    .line 2219343
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2219344
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "X-MSGR-Region"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2219345
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/14U;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2219322
    iget-object v2, p0, LX/FGi;->f:LX/FGl;

    invoke-virtual {v2, p1}, LX/FGl;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2219323
    :cond_0
    :goto_0
    return v0

    .line 2219324
    :cond_1
    iget-wide v6, p1, Lcom/facebook/ui/media/attachments/MediaResource;->s:J

    .line 2219325
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-nez v8, :cond_2

    .line 2219326
    iget-object v6, p0, LX/FGi;->d:LX/2MM;

    iget-object v7, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v6, v7}, LX/2MM;->b(Landroid/net/Uri;)J

    move-result-wide v6

    .line 2219327
    :cond_2
    move-wide v2, v6

    .line 2219328
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2219329
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->VIDEO:LX/2MK;

    if-ne v2, v3, :cond_3

    .line 2219330
    iget-object v1, p0, LX/FGi;->l:LX/0Uh;

    const/16 v2, 0x533

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0

    .line 2219331
    :cond_3
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->PHOTO:LX/2MK;

    if-ne v2, v3, :cond_4

    .line 2219332
    iget-object v1, p0, LX/FGi;->l:LX/0Uh;

    const/16 v2, 0x532

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v0

    goto :goto_0

    .line 2219333
    :cond_4
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->ENCRYPTED_PHOTO:LX/2MK;

    if-ne v2, v3, :cond_5

    move v0, v1

    .line 2219334
    goto :goto_0

    .line 2219335
    :cond_5
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->ENT_PHOTO:LX/2MK;

    if-ne v2, v3, :cond_6

    move v0, v1

    .line 2219336
    goto :goto_0

    .line 2219337
    :cond_6
    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v3, LX/2MK;->AUDIO:LX/2MK;

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 2219338
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/FHg;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/facebook/ui/media/attachments/MediaUploadResult;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/FHg;",
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;"
        }
    .end annotation

    .prologue
    .line 2219278
    iget-object v2, p0, LX/FGi;->d:LX/2MM;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, LX/2MM;->a(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v5

    .line 2219279
    const-string v2, "Unable to retrieve the object file."

    invoke-static {v5, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219280
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "The file is not present!"

    invoke-static {v2, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219281
    new-instance v10, LX/FHj;

    invoke-direct {v10}, LX/FHj;-><init>()V

    .line 2219282
    invoke-virtual {v10}, LX/FHj;->c()Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 2219283
    sget-object v2, LX/FHi;->START_UPLOAD:LX/FHi;

    invoke-virtual {v10, v2}, LX/FHj;->a(LX/FHi;)V

    .line 2219284
    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v10, v2, v3}, LX/FHj;->a(J)V

    .line 2219285
    invoke-static {}, LX/51t;->a()LX/51l;

    move-result-object v2

    invoke-static {v5, v2}, LX/1t3;->a(Ljava/io/File;LX/51l;)LX/51o;

    move-result-object v2

    invoke-virtual {v2}, LX/51o;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2219286
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, LX/FGi;->b(LX/FGi;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2219287
    sget-object v2, LX/FHi;->FILE_KEY_COMPUTED:LX/FHi;

    invoke-virtual {v10, v2}, LX/FHj;->a(LX/FHi;)V

    .line 2219288
    invoke-static/range {p2 .. p2}, LX/FGi;->b(Lcom/facebook/ui/media/attachments/MediaResource;)I

    move-result v11

    .line 2219289
    iget-object v12, p0, LX/FGi;->e:LX/FHn;

    new-instance v2, LX/FHl;

    move-object v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    invoke-direct/range {v2 .. v10}, LX/FHl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/FHg;LX/FHj;)V

    invoke-virtual {v12, v2}, LX/FHn;->a(LX/FHl;)LX/FHm;

    move-result-object v6

    .line 2219290
    new-instance v4, LX/6eb;

    iget-object v2, p0, LX/FGi;->a:LX/0Sh;

    invoke-direct {v4, v2}, LX/6eb;-><init>(LX/0Sh;)V

    .line 2219291
    const/4 v2, 0x0

    .line 2219292
    new-instance v7, LX/14U;

    invoke-direct {v7}, LX/14U;-><init>()V

    .line 2219293
    move-object/from16 v0, p2

    invoke-virtual {p0, v0, v7}, LX/FGi;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/14U;)V

    .line 2219294
    sget-object v3, LX/FHi;->UPLOAD_STARTED:LX/FHi;

    invoke-virtual {v10, v3}, LX/FHj;->a(LX/FHi;)V

    .line 2219295
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v11, :cond_0

    .line 2219296
    invoke-virtual {v6, v7}, LX/FHm;->a(LX/14U;)Z

    .line 2219297
    invoke-virtual {v6}, LX/FHm;->e()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2219298
    const/4 v2, 0x1

    .line 2219299
    sget-object v3, LX/FHi;->VIDEO_UPLOAD_COMPLETED:LX/FHi;

    invoke-virtual {v10, v3}, LX/FHj;->a(LX/FHi;)V

    .line 2219300
    :cond_0
    if-nez v2, :cond_3

    .line 2219301
    sget-object v2, LX/FHi;->TIMED_OUT_UPLOAD:LX/FHi;

    invoke-virtual {v10, v2}, LX/FHj;->a(LX/FHi;)V

    .line 2219302
    new-instance v2, Ljava/util/concurrent/TimeoutException;

    const-string v3, "Uploading media failed due to time out"

    invoke-direct {v2, v3}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2219303
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2219304
    :cond_2
    invoke-virtual {v4}, LX/6eb;->b()V

    .line 2219305
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2219306
    :cond_3
    invoke-virtual {v4}, LX/6eb;->a()V

    .line 2219307
    invoke-virtual {v6}, LX/FHm;->d()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2219308
    const/4 v2, 0x0

    move v3, v2

    .line 2219309
    :goto_2
    :try_start_0
    new-instance v2, LX/14U;

    invoke-direct {v2}, LX/14U;-><init>()V

    .line 2219310
    move-object/from16 v0, p2

    invoke-virtual {p0, v0, v2}, LX/FGi;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/14U;)V

    .line 2219311
    iget-object v7, p0, LX/FGi;->c:LX/18V;

    iget-object v8, p0, LX/FGi;->b:LX/FHA;

    new-instance v9, LX/FH9;

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-virtual {v6}, LX/FHm;->a()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-direct {v9, v0, v11, v12, v13}, LX/FH9;-><init>(Lcom/facebook/ui/media/attachments/MediaResource;IZLjava/lang/String;)V

    invoke-virtual {v7, v8, v9, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v2

    .line 2219312
    :goto_3
    sget-object v2, LX/FHi;->VIDEO_POST_PROCESS_COMPLETED:LX/FHi;

    invoke-virtual {v10, v2}, LX/FHj;->a(LX/FHi;)V

    .line 2219313
    invoke-static {}, LX/51t;->c()LX/51l;

    move-result-object v2

    invoke-static {v5, v2}, LX/1t3;->a(Ljava/io/File;LX/51l;)LX/51o;

    move-result-object v2

    invoke-virtual {v2}, LX/51o;->d()[B

    move-result-object v4

    .line 2219314
    new-instance v2, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    invoke-virtual {v6}, LX/FHm;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    move-object v5, v8

    invoke-direct/range {v2 .. v7}, Lcom/facebook/ui/media/attachments/MediaUploadResult;-><init>(Ljava/lang/String;[BLjava/lang/String;J)V

    return-object v2

    .line 2219315
    :catch_0
    move-exception v2

    .line 2219316
    const/4 v7, 0x2

    if-ge v3, v7, :cond_4

    .line 2219317
    invoke-virtual {v4}, LX/6eb;->b()V

    .line 2219318
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2219319
    :cond_4
    sget-object v3, LX/FHi;->TIMED_OUT_POST_PROCESS:LX/FHi;

    invoke-virtual {v10, v3}, LX/FHj;->a(LX/FHi;)V

    .line 2219320
    throw v2

    .line 2219321
    :cond_5
    invoke-virtual {v6}, LX/FHm;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_3
.end method
