.class public final LX/GDL;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GKt;

.field public final synthetic b:LX/GDN;


# direct methods
.method public constructor <init>(LX/GDN;LX/GKt;)V
    .locals 0

    .prologue
    .line 2329746
    iput-object p1, p0, LX/GDL;->b:LX/GDN;

    iput-object p2, p0, LX/GDL;->a:LX/GKt;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2329747
    iget-object v0, p0, LX/GDL;->b:LX/GDN;

    invoke-static {v0, p1}, LX/GDN;->a$redex0(LX/GDN;Ljava/lang/Throwable;)V

    .line 2329748
    iget-object v0, p0, LX/GDL;->a:LX/GKt;

    invoke-virtual {v0, p1}, LX/GKt;->a(Ljava/lang/Throwable;)V

    .line 2329749
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2329750
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2329751
    if-eqz p1, :cond_0

    .line 2329752
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329753
    if-eqz v0, :cond_0

    .line 2329754
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329755
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesCallNowValidationQueryModels$AdinterfacesCallNowValidationQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2329756
    :goto_0
    iget-object v1, p0, LX/GDL;->a:LX/GKt;

    invoke-virtual {v1, v0}, LX/GKt;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2329757
    :goto_1
    return-void

    .line 2329758
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2329759
    :catch_0
    move-exception v0

    .line 2329760
    iget-object v1, p0, LX/GDL;->b:LX/GDN;

    invoke-static {v1, v0}, LX/GDN;->a$redex0(LX/GDN;Ljava/lang/Throwable;)V

    .line 2329761
    iget-object v1, p0, LX/GDL;->a:LX/GKt;

    invoke-virtual {v1, v0}, LX/GKt;->a(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
