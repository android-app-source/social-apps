.class public final LX/FxW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardFavPhotosMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;LX/4BY;)V
    .locals 0

    .prologue
    .line 2305026
    iput-object p1, p0, LX/FxW;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iput-object p2, p0, LX/FxW;->a:LX/4BY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2305027
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_0

    check-cast p1, LX/4Ua;

    .line 2305028
    iget-object v0, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v0

    .line 2305029
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    .line 2305030
    :goto_0
    iget-object v2, p0, LX/FxW;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x173d61

    if-ne v0, v3, :cond_1

    const v0, 0x7f081601

    :goto_1
    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 2305031
    iget-object v2, p0, LX/FxW;->a:LX/4BY;

    invoke-virtual {v2}, LX/4BY;->dismiss()V

    .line 2305032
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2305033
    iget-object v0, p0, LX/FxW;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2305034
    iget-object v0, p0, LX/FxW;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2305035
    return-void

    :cond_0
    move v0, v1

    .line 2305036
    goto :goto_0

    .line 2305037
    :cond_1
    const v0, 0x7f081600

    goto :goto_1
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2305038
    iget-object v0, p0, LX/FxW;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2305039
    if-eqz v0, :cond_1

    .line 2305040
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2305041
    iget-object v1, p0, LX/FxW;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-boolean v1, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->G:Z

    if-eqz v1, :cond_2

    .line 2305042
    iget-object v1, p0, LX/FxW;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v2, p0, LX/FxW;->a:LX/4BY;

    .line 2305043
    invoke-virtual {v2}, LX/4BY;->dismiss()V

    .line 2305044
    sget-object v3, LX/0ax;->bE:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v7, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->F:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2305045
    iget-object v3, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-interface {v3, v0, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2305046
    if-eqz v4, :cond_0

    .line 2305047
    iget-object v3, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2305048
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2305049
    :cond_1
    :goto_0
    return-void

    .line 2305050
    :cond_2
    iget-object v2, p0, LX/FxW;->a:LX/4BY;

    .line 2305051
    invoke-virtual {v2}, LX/4BY;->dismiss()V

    .line 2305052
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2305053
    goto :goto_0
.end method
