.class public LX/FbR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FbA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FbA",
        "<",
        "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchQuery$FilteredQuery$Modules$Edges;",
        "Lcom/facebook/search/results/model/SearchResultsBridge;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FbR;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2260227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2260228
    return-void
.end method

.method public static a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchQuery$FilteredQuery$Modules$Edges;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2260229
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v0

    .line 2260230
    if-nez v0, :cond_0

    .line 2260231
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2260232
    :goto_0
    return-object v0

    .line 2260233
    :cond_0
    new-instance v1, LX/8dO;

    invoke-direct {v1}, LX/8dO;-><init>()V

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    .line 2260234
    iput-object v0, v1, LX/8dO;->a:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    .line 2260235
    move-object v0, v1

    .line 2260236
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2260237
    iput-object v1, v0, LX/8dO;->c:Ljava/lang/String;

    .line 2260238
    move-object v0, v0

    .line 2260239
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->d()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 2260240
    iput-object v1, v0, LX/8dO;->j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2260241
    move-object v0, v0

    .line 2260242
    invoke-virtual {p0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;->c()LX/0Px;

    move-result-object v1

    .line 2260243
    iput-object v1, v0, LX/8dO;->i:LX/0Px;

    .line 2260244
    move-object v0, v0

    .line 2260245
    invoke-virtual {v0}, LX/8dO;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    .line 2260246
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/search/results/model/SearchResultsBridge;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Ljava/lang/String;)Lcom/facebook/search/results/model/SearchResultsBridge;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FbR;
    .locals 3

    .prologue
    .line 2260247
    sget-object v0, LX/FbR;->a:LX/FbR;

    if-nez v0, :cond_1

    .line 2260248
    const-class v1, LX/FbR;

    monitor-enter v1

    .line 2260249
    :try_start_0
    sget-object v0, LX/FbR;->a:LX/FbR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260250
    if-eqz v2, :cond_0

    .line 2260251
    :try_start_1
    new-instance v0, LX/FbR;

    invoke-direct {v0}, LX/FbR;-><init>()V

    .line 2260252
    move-object v0, v0

    .line 2260253
    sput-object v0, LX/FbR;->a:LX/FbR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260254
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260255
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260256
    :cond_1
    sget-object v0, LX/FbR;->a:LX/FbR;

    return-object v0

    .line 2260257
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260258
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)LX/0Px;
    .locals 1

    .prologue
    .line 2260259
    check-cast p1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;

    invoke-static {p1}, LX/FbR;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
