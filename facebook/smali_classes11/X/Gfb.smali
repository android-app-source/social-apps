.class public LX/Gfb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gfc;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gfb",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Gfc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377660
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2377661
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gfb;->b:LX/0Zi;

    .line 2377662
    iput-object p1, p0, LX/Gfb;->a:LX/0Ot;

    .line 2377663
    return-void
.end method

.method public static a(LX/0QB;)LX/Gfb;
    .locals 4

    .prologue
    .line 2377649
    const-class v1, LX/Gfb;

    monitor-enter v1

    .line 2377650
    :try_start_0
    sget-object v0, LX/Gfb;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377651
    sput-object v2, LX/Gfb;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377652
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377653
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377654
    new-instance v3, LX/Gfb;

    const/16 p0, 0x211a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gfb;-><init>(LX/0Ot;)V

    .line 2377655
    move-object v0, v3

    .line 2377656
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377657
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gfb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377658
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2377624
    const v0, 0x2a192908

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 2377644
    iget-object v0, p0, LX/Gfb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2377645
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const p0, 0x7f020afc

    invoke-virtual {v0, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/16 p0, 0x8

    const p2, 0x7f0b0a20

    invoke-interface {v0, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const p0, 0x7f020afb

    invoke-interface {v0, p0}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    .line 2377646
    const p0, 0x2a192908

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2377647
    invoke-interface {v0, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2377648
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2377625
    invoke-static {}, LX/1dS;->b()V

    .line 2377626
    iget v0, p1, LX/1dQ;->b:I

    .line 2377627
    packed-switch v0, :pswitch_data_0

    .line 2377628
    :goto_0
    return-object v1

    .line 2377629
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2377630
    check-cast v0, LX/GfZ;

    .line 2377631
    iget-object v2, p0, LX/Gfb;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gfc;

    iget-object v3, v0, LX/GfZ;->a:LX/GfY;

    .line 2377632
    iget-object p1, v2, LX/Gfc;->a:LX/Gfl;

    .line 2377633
    iget-object p2, p1, LX/Gfl;->b:LX/0bH;

    new-instance p0, LX/1Ni;

    iget-object v0, v3, LX/GfY;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v3, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, LX/1Ni;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, p0}, LX/0b4;->a(LX/0b7;)V

    .line 2377634
    iget-object p2, v3, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    iget-object p0, v3, LX/GfY;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-static {p2, p0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object p2

    .line 2377635
    invoke-static {p2}, LX/17Q;->F(LX/0lF;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2377636
    const/4 p0, 0x0

    .line 2377637
    :goto_1
    move-object p2, p0

    .line 2377638
    iget-object p0, p1, LX/Gfl;->d:LX/0Zb;

    invoke-interface {p0, p2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2377639
    goto :goto_0

    .line 2377640
    :cond_0
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "pyml_xout"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {p0, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v0, "native_newsfeed"

    .line 2377641
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2377642
    move-object p0, p0

    .line 2377643
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2a192908
        :pswitch_0
    .end packed-switch
.end method
