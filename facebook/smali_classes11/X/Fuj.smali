.class public LX/Fuj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/FwD;

.field public final c:LX/BP0;

.field public final d:LX/BQ1;

.field private final e:LX/BQB;

.field public final f:LX/Fv9;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/Fwh;

.field private final j:LX/FvY;

.field public final k:LX/Fw7;

.field public l:Z

.field private m:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Fsr;LX/0Or;LX/0Or;LX/FwD;LX/BQB;LX/Fwh;LX/Fw7;LX/FvY;LX/BQ1;LX/BP0;)V
    .locals 1
    .param p9    # LX/FvY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/Fsr;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/FwD;",
            "LX/BQB;",
            "LX/Fwh;",
            "LX/Fw7;",
            "Lcom/facebook/timeline/header/IntroCardBioBinder$OnEditBioClickListener;",
            "LX/BQ1;",
            "LX/BP0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300354
    iput-object p1, p0, LX/Fuj;->a:Landroid/content/Context;

    .line 2300355
    iput-object p5, p0, LX/Fuj;->b:LX/FwD;

    .line 2300356
    iput-object p11, p0, LX/Fuj;->c:LX/BP0;

    .line 2300357
    iput-object p10, p0, LX/Fuj;->d:LX/BQ1;

    .line 2300358
    iput-object p6, p0, LX/Fuj;->e:LX/BQB;

    .line 2300359
    invoke-virtual {p2}, LX/Fsr;->e()LX/Fv9;

    move-result-object v0

    iput-object v0, p0, LX/Fuj;->f:LX/Fv9;

    .line 2300360
    iput-object p3, p0, LX/Fuj;->g:LX/0Or;

    .line 2300361
    iput-object p4, p0, LX/Fuj;->h:LX/0Or;

    .line 2300362
    iput-object p7, p0, LX/Fuj;->i:LX/Fwh;

    .line 2300363
    iput-object p8, p0, LX/Fuj;->k:LX/Fw7;

    .line 2300364
    iput-object p9, p0, LX/Fuj;->j:LX/FvY;

    .line 2300365
    return-void
.end method

.method public static a(LX/Fuj;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2300366
    iget-object v0, p0, LX/Fuj;->m:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2300367
    new-instance v0, LX/Fuh;

    invoke-direct {v0, p0}, LX/Fuh;-><init>(LX/Fuj;)V

    iput-object v0, p0, LX/Fuj;->m:Landroid/view/View$OnClickListener;

    .line 2300368
    :cond_0
    iget-object v0, p0, LX/Fuj;->m:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static a(LX/Fuj;Ljava/lang/String;Z)V
    .locals 5
    .param p0    # LX/Fuj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2300369
    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, LX/Fuj;->a:Landroid/content/Context;

    const-class v1, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;

    invoke-direct {v2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2300370
    if-eqz p1, :cond_0

    .line 2300371
    const-string v0, "initial_bio_text"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2300372
    if-eqz p2, :cond_0

    .line 2300373
    const-string v0, "came_from_suggested_bio"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2300374
    :cond_0
    const-string v0, "bio_prompts"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, LX/Fuj;->d:LX/BQ1;

    .line 2300375
    iget-object v4, v3, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v3, v4

    .line 2300376
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->j()LX/0Px;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2300377
    const-string v0, "show_feed_sharing_switch_extra"

    iget-object v1, p0, LX/Fuj;->d:LX/BQ1;

    .line 2300378
    iget-object v3, v1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v1, v3

    .line 2300379
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->e()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2300380
    const-string v0, "initial_is_feed_sharing_switch_checked"

    iget-object v1, p0, LX/Fuj;->d:LX/BQ1;

    .line 2300381
    iget-object v3, v1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v1, v3

    .line 2300382
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->m()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2300383
    iget-object v0, p0, LX/Fuj;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x71d

    iget-object v1, p0, LX/Fuj;->a:Landroid/content/Context;

    const-class v4, Landroid/app/Activity;

    invoke-static {v1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v2, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2300384
    iget-object v0, p0, LX/Fuj;->j:LX/FvY;

    if-eqz v0, :cond_1

    .line 2300385
    iget-object v0, p0, LX/Fuj;->j:LX/FvY;

    .line 2300386
    iget-object v1, v0, LX/FvY;->a:LX/Fvi;

    iget-object v1, v1, LX/Fvi;->i:LX/Fw8;

    iget-object v2, v0, LX/FvY;->a:LX/Fvi;

    iget-object v2, v2, LX/Fv4;->c:LX/BP0;

    iget-object v3, v0, LX/FvY;->a:LX/Fvi;

    iget-object v3, v3, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v1, v2, v3}, LX/Fw8;->a(LX/5SB;LX/BQ1;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2300387
    iget-object v1, v0, LX/FvY;->a:LX/Fvi;

    invoke-static {v1}, LX/Fvi;->l(LX/Fvi;)V

    .line 2300388
    :cond_1
    return-void
.end method

.method private b(Lcom/facebook/timeline/header/TimelineIntroCardBioView;)Z
    .locals 14

    .prologue
    .line 2300389
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g()V

    .line 2300390
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a()V

    .line 2300391
    iget-object v0, p0, LX/Fuj;->d:LX/BQ1;

    .line 2300392
    iget-object v1, v0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v1

    .line 2300393
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->o()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    .line 2300394
    iget-object v1, p0, LX/Fuj;->n:Landroid/view/View$OnClickListener;

    if-nez v1, :cond_0

    .line 2300395
    new-instance v1, LX/Fui;

    invoke-direct {v1, p0}, LX/Fui;-><init>(LX/Fuj;)V

    iput-object v1, p0, LX/Fuj;->n:Landroid/view/View$OnClickListener;

    .line 2300396
    :cond_0
    iget-object v1, p0, LX/Fuj;->n:Landroid/view/View$OnClickListener;

    move-object v1, v1

    .line 2300397
    new-instance v2, LX/Fug;

    invoke-direct {v2, p0, p1}, LX/Fug;-><init>(LX/Fuj;Lcom/facebook/timeline/header/TimelineIntroCardBioView;)V

    move-object v2, v2

    .line 2300398
    invoke-virtual {p1, v0, v1, v2}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2300399
    iget-object v0, p0, LX/Fuj;->f:LX/Fv9;

    if-eqz v0, :cond_1

    .line 2300400
    iget-object v0, p0, LX/Fuj;->f:LX/Fv9;

    .line 2300401
    iget-boolean v3, v0, LX/Fv9;->o:Z

    if-eqz v3, :cond_2

    .line 2300402
    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2300403
    :cond_2
    iget-object v3, v0, LX/Fv9;->e:LX/BQ9;

    iget-wide v5, v0, LX/Fv9;->c:J

    iget-object v4, v0, LX/Fv9;->b:Ljava/lang/String;

    .line 2300404
    sget-object v12, LX/9lQ;->SELF:LX/9lQ;

    const-string v13, "bio_add_prompt_suggested_impression"

    move-object v8, v3

    move-wide v9, v5

    move-object v11, v4

    invoke-static/range {v8 .. v13}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 2300405
    if-eqz v7, :cond_3

    .line 2300406
    iget-object v8, v3, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300407
    :cond_3
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/Fv9;->o:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/header/TimelineIntroCardBioView;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2300408
    iput-boolean p2, p0, LX/Fuj;->l:Z

    .line 2300409
    iget-object v0, p0, LX/Fuj;->b:LX/FwD;

    iget-object v1, p0, LX/Fuj;->c:LX/BP0;

    invoke-virtual {v0, v1}, LX/FwD;->a(LX/5SB;)Z

    move-result v1

    .line 2300410
    iget-object v0, p0, LX/Fuj;->b:LX/FwD;

    iget-object v3, p0, LX/Fuj;->d:LX/BQ1;

    iget-object v4, p0, LX/Fuj;->c:LX/BP0;

    invoke-virtual {v0, v3, v4, v1}, LX/FwD;->a(LX/BQ1;LX/5SB;Z)Ljava/lang/Integer;

    move-result-object v3

    .line 2300411
    const/4 v0, 0x1

    .line 2300412
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2300413
    :goto_0
    iget-object v1, p0, LX/Fuj;->e:LX/BQB;

    invoke-static {v3}, LX/FwC;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v3

    .line 2300414
    iput-object v3, v1, LX/BQB;->F:Ljava/lang/String;

    .line 2300415
    iget-object v1, p0, LX/Fuj;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0df3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2300416
    iget-object v1, p0, LX/Fuj;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0df4

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2300417
    if-eqz v0, :cond_5

    .line 2300418
    iget-object v1, p0, LX/Fuj;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0b0df2

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 2300419
    const v5, 0x7f0a00d5

    invoke-virtual {p1, v5}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->setBackgroundResource(I)V

    .line 2300420
    :goto_1
    invoke-virtual {p1, v3, v1, v4, v2}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->setPadding(IIII)V

    .line 2300421
    iget-boolean v1, p0, LX/Fuj;->l:Z

    if-eqz v1, :cond_4

    .line 2300422
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->e()V

    .line 2300423
    :goto_2
    return v0

    .line 2300424
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a()V

    .line 2300425
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->b()V

    .line 2300426
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g()V

    .line 2300427
    const/4 v0, 0x0

    move v0, v0

    .line 2300428
    goto :goto_0

    .line 2300429
    :pswitch_1
    const/4 p2, 0x1

    .line 2300430
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2300431
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a()V

    .line 2300432
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->b()V

    .line 2300433
    invoke-static {p0}, LX/Fuj;->a(LX/Fuj;)Landroid/view/View$OnClickListener;

    move-result-object v4

    .line 2300434
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2300435
    iget-object v5, p0, LX/Fuj;->d:LX/BQ1;

    .line 2300436
    iget-object v1, v5, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v5, v1

    .line 2300437
    if-eqz v5, :cond_0

    .line 2300438
    iget-object v0, p0, LX/Fuj;->d:LX/BQ1;

    .line 2300439
    iget-object v5, v0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v5

    .line 2300440
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->b()LX/0Px;

    move-result-object v0

    .line 2300441
    :cond_0
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2300442
    invoke-virtual {p1, v4}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Landroid/view/View$OnClickListener;)V

    .line 2300443
    :goto_3
    iget-object v0, p0, LX/Fuj;->f:LX/Fv9;

    if-eqz v0, :cond_1

    .line 2300444
    iget-object v0, p0, LX/Fuj;->f:LX/Fv9;

    invoke-virtual {v0}, LX/Fv9;->b()V

    .line 2300445
    :cond_1
    move v0, p2

    .line 2300446
    goto/16 :goto_0

    .line 2300447
    :pswitch_2
    const/4 v4, 0x1

    .line 2300448
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->g()V

    .line 2300449
    iget-object v5, p0, LX/Fuj;->d:LX/BQ1;

    const/4 v0, 0x0

    .line 2300450
    invoke-static {v5}, LX/FwD;->b(LX/BQ1;)Z

    move-result p2

    if-nez p2, :cond_9

    .line 2300451
    :cond_2
    :goto_4
    move v0, v0

    .line 2300452
    if-eqz v0, :cond_8

    iget-object v0, p0, LX/Fuj;->k:LX/Fw7;

    iget-object v5, p0, LX/Fuj;->c:LX/BP0;

    iget-object p2, p0, LX/Fuj;->d:LX/BQ1;

    invoke-virtual {v0, v5, p2}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v0

    sget-object v5, LX/Fve;->EXPANDED:LX/Fve;

    if-eq v0, v5, :cond_8

    move v0, v4

    .line 2300453
    :goto_5
    iget-object v5, p0, LX/Fuj;->d:LX/BQ1;

    .line 2300454
    iget-object p2, v5, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v5, p2

    .line 2300455
    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->c()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, LX/Fuj;->a(LX/Fuj;)Landroid/view/View$OnClickListener;

    move-result-object p2

    invoke-virtual {p1, v5, v1, p2, v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Ljava/lang/CharSequence;ZLandroid/view/View$OnClickListener;Z)V

    .line 2300456
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->b()V

    .line 2300457
    iget-object v0, p0, LX/Fuj;->f:LX/Fv9;

    if-eqz v0, :cond_3

    .line 2300458
    iget-object v0, p0, LX/Fuj;->f:LX/Fv9;

    iget-object v5, p0, LX/Fuj;->d:LX/BQ1;

    invoke-virtual {v0, p1, v5}, LX/Fv9;->a(Landroid/view/View;LX/BQ1;)V

    .line 2300459
    :cond_3
    move v0, v4

    .line 2300460
    goto/16 :goto_0

    .line 2300461
    :pswitch_3
    invoke-direct {p0, p1}, LX/Fuj;->b(Lcom/facebook/timeline/header/TimelineIntroCardBioView;)Z

    move-result v0

    goto/16 :goto_0

    .line 2300462
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->f()V

    goto/16 :goto_2

    :cond_5
    move v1, v2

    goto/16 :goto_1

    .line 2300463
    :cond_6
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    if-ne v5, p2, :cond_7

    .line 2300464
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v4, v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    goto :goto_3

    .line 2300465
    :cond_7
    invoke-virtual {p1, v4, v0}, Lcom/facebook/timeline/header/TimelineIntroCardBioView;->a(Landroid/view/View$OnClickListener;LX/0Px;)V

    goto :goto_3

    .line 2300466
    :cond_8
    const/4 v0, 0x0

    goto :goto_5

    .line 2300467
    :cond_9
    iget-object p2, v5, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object p2, p2

    .line 2300468
    if-eqz p2, :cond_2

    .line 2300469
    iget-object p2, v5, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object p2, p2

    .line 2300470
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 2300471
    iget-object p2, v5, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object p2, p2

    .line 2300472
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object p2

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_2

    const/4 v0, 0x1

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
