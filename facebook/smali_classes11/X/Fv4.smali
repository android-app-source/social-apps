.class public abstract LX/Fv4;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/1Cw;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0ad;

.field public final c:LX/BP0;

.field public final d:LX/BQ1;

.field public e:[Z

.field private f:I

.field private g:[Ljava/lang/Object;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/BP0;LX/BQ1;)V
    .locals 1

    .prologue
    .line 2300788
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2300789
    const/4 v0, -0x1

    iput v0, p0, LX/Fv4;->f:I

    .line 2300790
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fv4;->h:Z

    .line 2300791
    iput-object p1, p0, LX/Fv4;->a:Landroid/content/Context;

    .line 2300792
    iput-object p2, p0, LX/Fv4;->b:LX/0ad;

    .line 2300793
    iput-object p3, p0, LX/Fv4;->c:LX/BP0;

    .line 2300794
    iput-object p4, p0, LX/Fv4;->d:LX/BQ1;

    .line 2300795
    return-void
.end method

.method public static b(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 2300787
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown item view type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static c(I)Z
    .locals 3

    .prologue
    .line 2300786
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown itemViewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(I)Ljava/lang/Object;
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2300763
    const/4 v0, 0x0

    .line 2300764
    instance-of v1, p5, LX/1a8;

    if-eqz v1, :cond_7

    .line 2300765
    check-cast p5, LX/1a8;

    invoke-interface {p5}, LX/1a8;->getViewDiagnostics()LX/1Rn;

    move-result-object v0

    .line 2300766
    if-eqz v0, :cond_6

    invoke-virtual {v0}, LX/1Rn;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/1Rn;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2300767
    :cond_0
    const/4 v1, 0x1

    .line 2300768
    const-string v2, "renderTimelineHeaderView"

    const v3, -0x5ebe31ce

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    move-object v2, v0

    move v3, v1

    .line 2300769
    :goto_0
    :try_start_0
    invoke-virtual {p0, p3, p4}, LX/Fv4;->a(Landroid/view/View;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    .line 2300770
    const-wide/16 v0, 0x0

    .line 2300771
    if-eqz v3, :cond_1

    .line 2300772
    const v0, -0x307098e1    # -4.8118001E9f

    invoke-static {v0}, LX/02m;->b(I)J

    move-result-wide v0

    .line 2300773
    :cond_1
    if-eqz v3, :cond_3

    if-eqz v5, :cond_3

    .line 2300774
    iput-wide v0, v2, LX/1Rn;->r:J

    .line 2300775
    invoke-virtual {v2, p3}, LX/1Rn;->a(Landroid/view/View;)V

    .line 2300776
    :cond_2
    :goto_1
    return-void

    .line 2300777
    :cond_3
    if-eqz v2, :cond_2

    .line 2300778
    iput-boolean v4, v2, LX/1Rn;->C:Z

    .line 2300779
    goto :goto_1

    .line 2300780
    :catchall_0
    move-exception v0

    .line 2300781
    if-eqz v3, :cond_4

    .line 2300782
    const v1, 0x7f6427fa

    invoke-static {v1}, LX/02m;->b(I)J

    .line 2300783
    :cond_4
    if-eqz v2, :cond_5

    .line 2300784
    iput-boolean v4, v2, LX/1Rn;->C:Z

    .line 2300785
    :cond_5
    throw v0

    :cond_6
    move-object v2, v0

    move v3, v4

    goto :goto_0

    :cond_7
    move-object v2, v0

    move v3, v4

    goto :goto_0
.end method

.method public abstract a([Z)V
.end method

.method public abstract a(Landroid/view/View;I)Z
.end method

.method public final getCount()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2300744
    iget v0, p0, LX/Fv4;->f:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_4

    .line 2300745
    invoke-virtual {p0}, LX/Fv4;->a()I

    move-result v0

    .line 2300746
    iget-object v2, p0, LX/Fv4;->e:[Z

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/Fv4;->e:[Z

    array-length v2, v2

    if-eq v0, v2, :cond_1

    .line 2300747
    :cond_0
    new-array v0, v0, [Z

    iput-object v0, p0, LX/Fv4;->e:[Z

    .line 2300748
    :cond_1
    iget-object v0, p0, LX/Fv4;->e:[Z

    invoke-virtual {p0, v0}, LX/Fv4;->a([Z)V

    .line 2300749
    iput v1, p0, LX/Fv4;->f:I

    move v0, v1

    .line 2300750
    :goto_0
    iget-object v2, p0, LX/Fv4;->e:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2300751
    iget-object v2, p0, LX/Fv4;->e:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_2

    .line 2300752
    iget v2, p0, LX/Fv4;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/Fv4;->f:I

    .line 2300753
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2300754
    :cond_3
    iget v0, p0, LX/Fv4;->f:I

    if-lez v0, :cond_4

    .line 2300755
    iget-object v0, p0, LX/Fv4;->g:[Ljava/lang/Object;

    if-nez v0, :cond_5

    .line 2300756
    iget-object v0, p0, LX/Fv4;->e:[Z

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/Fv4;->g:[Ljava/lang/Object;

    .line 2300757
    :cond_4
    :goto_1
    iget v0, p0, LX/Fv4;->f:I

    return v0

    .line 2300758
    :cond_5
    iget-object v0, p0, LX/Fv4;->g:[Ljava/lang/Object;

    array-length v0, v0

    iget-object v2, p0, LX/Fv4;->e:[Z

    array-length v2, v2

    if-eq v0, v2, :cond_6

    .line 2300759
    iget-object v0, p0, LX/Fv4;->e:[Z

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, LX/Fv4;->g:[Ljava/lang/Object;

    goto :goto_1

    .line 2300760
    :cond_6
    :goto_2
    iget-object v0, p0, LX/Fv4;->e:[Z

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 2300761
    iget-object v0, p0, LX/Fv4;->g:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 2300762
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2300734
    iget-object v0, p0, LX/Fv4;->g:[Ljava/lang/Object;

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    .line 2300735
    const/4 v1, -0x1

    .line 2300736
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LX/Fv4;->e:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2300737
    iget-object v2, p0, LX/Fv4;->e:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_0

    .line 2300738
    add-int/lit8 v1, v1, 0x1

    .line 2300739
    :cond_0
    if-ne p1, v1, :cond_2

    .line 2300740
    iget-object v1, p0, LX/Fv4;->g:[Ljava/lang/Object;

    invoke-virtual {p0, v0}, LX/Fv4;->a(I)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v1, p1

    .line 2300741
    :cond_1
    iget-object v0, p0, LX/Fv4;->g:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0

    .line 2300742
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2300743
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2300733
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 2300730
    const/4 v0, -0x1

    iput v0, p0, LX/Fv4;->f:I

    .line 2300731
    invoke-super {p0}, LX/1Cv;->notifyDataSetChanged()V

    .line 2300732
    return-void
.end method
