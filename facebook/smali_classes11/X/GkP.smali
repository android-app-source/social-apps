.class public final LX/GkP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Gkq;

.field public final synthetic b:Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;LX/Gkq;)V
    .locals 0

    .prologue
    .line 2389049
    iput-object p1, p0, LX/GkP;->b:Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    iput-object p2, p0, LX/GkP;->a:LX/Gkq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x34d7b795    # -1.1028587E7f

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2389050
    iget-object v2, p0, LX/GkP;->b:Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    iget-object v2, v2, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->g:LX/GkG;

    if-eqz v2, :cond_0

    .line 2389051
    iget-object v2, p0, LX/GkP;->b:Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    iget-object v2, v2, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->g:LX/GkG;

    iget-object v3, p0, LX/GkP;->a:LX/Gkq;

    .line 2389052
    iget-object v4, v3, LX/Gkq;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2389053
    iget-object v4, p0, LX/GkP;->a:LX/Gkq;

    .line 2389054
    iget-boolean p1, v4, LX/Gkq;->g:Z

    move v4, p1

    .line 2389055
    if-nez v4, :cond_1

    :goto_0
    iget-object v4, p0, LX/GkP;->a:LX/Gkq;

    .line 2389056
    iget-boolean p0, v4, LX/Gkq;->h:Z

    move v4, p0

    .line 2389057
    iget-object v5, v2, LX/GkG;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    iget-object v5, v5, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->c:LX/Gkk;

    invoke-interface {v5, v3, v0, v4}, LX/Gkk;->a(Ljava/lang/String;ZZ)V

    .line 2389058
    iget-object v5, v2, LX/GkG;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    iget-object v5, v5, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->e:LX/GkD;

    new-instance p0, LX/GkC;

    sget-object p1, LX/GkB;->STATUS_CHANGE:LX/GkB;

    invoke-direct {p0, p1}, LX/GkC;-><init>(LX/GkB;)V

    invoke-virtual {v5, p0}, LX/0b4;->a(LX/0b7;)V

    .line 2389059
    iget-object v5, v2, LX/GkG;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    const/4 p0, 0x1

    .line 2389060
    iput-boolean p0, v5, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->j:Z

    .line 2389061
    :cond_0
    const v0, 0x77734a72

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2389062
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
