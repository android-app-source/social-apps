.class public final LX/FyC;
.super LX/2f1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2f1",
        "<",
        "LX/2f2;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FyD;


# direct methods
.method public constructor <init>(LX/FyD;)V
    .locals 0

    .prologue
    .line 2306405
    iput-object p1, p0, LX/FyC;->a:LX/FyD;

    invoke-direct {p0}, LX/2f1;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2f2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2306404
    const-class v0, LX/2f2;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2306396
    check-cast p1, LX/2f2;

    .line 2306397
    iget-object v0, p0, LX/FyC;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->c:LX/G1I;

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v3, p0, LX/FyC;->a:LX/FyD;

    iget-object v3, v3, LX/FyD;->h:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 2306398
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    invoke-static {v0, v3}, LX/G1I;->b(LX/G1I;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)Lcom/facebook/timeline/protiles/model/ProtileModel;

    move-result-object v3

    .line 2306399
    if-eqz v3, :cond_0

    .line 2306400
    invoke-virtual {v3, v1, v2, v4, v5}, Lcom/facebook/timeline/protiles/model/ProtileModel;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;J)V

    .line 2306401
    :cond_0
    iget-object v0, p0, LX/FyC;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->d:Lcom/facebook/timeline/TimelineFragment;

    .line 2306402
    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->E()V

    .line 2306403
    return-void
.end method
