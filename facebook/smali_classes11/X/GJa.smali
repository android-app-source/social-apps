.class public final LX/GJa;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/GJb;


# direct methods
.method public constructor <init>(LX/GJb;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2339182
    iput-object p1, p0, LX/GJa;->c:LX/GJb;

    iput-boolean p2, p0, LX/GJa;->a:Z

    iput-object p3, p0, LX/GJa;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2339183
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2339184
    check-cast v0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;

    .line 2339185
    if-eqz v0, :cond_0

    .line 2339186
    iget-object v1, p0, LX/GJa;->c:LX/GJb;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel;->a()LX/0Px;

    move-result-object v0

    .line 2339187
    iput-object v0, v1, LX/GJb;->g:Ljava/util/List;

    .line 2339188
    iget-object v0, p0, LX/GJa;->c:LX/GJb;

    iget-boolean v1, p0, LX/GJa;->a:Z

    iget-object v2, p0, LX/GJa;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/GJb;->b(LX/GJb;ZLjava/lang/String;)V

    .line 2339189
    :cond_0
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2339179
    iget-object v0, p0, LX/GJa;->c:LX/GJb;

    iget-object v0, v0, LX/GJb;->b:LX/1Ck;

    const-string v1, "fetch_offer_discount_types_task_key"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2339180
    iget-object v0, p0, LX/GJa;->c:LX/GJb;

    iget-object v0, v0, LX/GJb;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, LX/GJb;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error Fetching Offer Discount Types"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2339181
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2339178
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/GJa;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
