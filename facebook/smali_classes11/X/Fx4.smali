.class public LX/Fx4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Fx4;


# instance fields
.field public final a:LX/0hB;

.field private final b:Landroid/content/res/Resources;

.field public final c:LX/G0r;

.field private final d:LX/0rq;

.field public final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/0hB;Landroid/content/res/Resources;LX/G0r;LX/0rq;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304207
    iput-object p1, p0, LX/Fx4;->a:LX/0hB;

    .line 2304208
    iput-object p2, p0, LX/Fx4;->b:Landroid/content/res/Resources;

    .line 2304209
    iput-object p3, p0, LX/Fx4;->c:LX/G0r;

    .line 2304210
    iput-object p4, p0, LX/Fx4;->d:LX/0rq;

    .line 2304211
    iput-object p5, p0, LX/Fx4;->e:LX/0ad;

    .line 2304212
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$SuggestedPhoto;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2304213
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2304214
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$SuggestedPhotoModel;

    .line 2304215
    new-instance v4, LX/5vQ;

    invoke-direct {v4}, LX/5vQ;-><init>()V

    const/4 v5, 0x0

    .line 2304216
    iput-object v5, v4, LX/5vQ;->a:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    .line 2304217
    move-object v4, v4

    .line 2304218
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$SuggestedPhotoModel;->a()LX/5vo;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;->a(LX/5vo;)Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    move-result-object v0

    .line 2304219
    iput-object v0, v4, LX/5vQ;->b:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    .line 2304220
    move-object v0, v4

    .line 2304221
    invoke-virtual {v0}, LX/5vQ;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-result-object v0

    .line 2304222
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2304223
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2304224
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Fx4;
    .locals 9

    .prologue
    .line 2304225
    sget-object v0, LX/Fx4;->f:LX/Fx4;

    if-nez v0, :cond_1

    .line 2304226
    const-class v1, LX/Fx4;

    monitor-enter v1

    .line 2304227
    :try_start_0
    sget-object v0, LX/Fx4;->f:LX/Fx4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2304228
    if-eqz v2, :cond_0

    .line 2304229
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2304230
    new-instance v3, LX/Fx4;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/G0r;->a(LX/0QB;)LX/G0r;

    move-result-object v6

    check-cast v6, LX/G0r;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v7

    check-cast v7, LX/0rq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/Fx4;-><init>(LX/0hB;Landroid/content/res/Resources;LX/G0r;LX/0rq;LX/0ad;)V

    .line 2304231
    move-object v0, v3

    .line 2304232
    sput-object v0, LX/Fx4;->f:LX/Fx4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2304233
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2304234
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2304235
    :cond_1
    sget-object v0, LX/Fx4;->f:LX/Fx4;

    return-object v0

    .line 2304236
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2304237
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;
    .locals 2

    .prologue
    .line 2304238
    new-instance v0, LX/4aM;

    invoke-direct {v0}, LX/4aM;-><init>()V

    .line 2304239
    iput-object p1, v0, LX/4aM;->b:Ljava/lang/String;

    .line 2304240
    move-object v0, v0

    .line 2304241
    invoke-virtual {v0}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2304242
    new-instance v1, LX/5vr;

    invoke-direct {v1}, LX/5vr;-><init>()V

    .line 2304243
    iput-object p0, v1, LX/5vr;->d:Ljava/lang/String;

    .line 2304244
    move-object v1, v1

    .line 2304245
    iput-object v0, v1, LX/5vr;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2304246
    move-object v0, v1

    .line 2304247
    invoke-virtual {v0}, LX/5vr;->a()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    move-result-object v0

    .line 2304248
    new-instance v1, LX/5vQ;

    invoke-direct {v1}, LX/5vQ;-><init>()V

    .line 2304249
    iput-object v0, v1, LX/5vQ;->b:Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$ProfileMediaGridPhotoFieldsModel;

    .line 2304250
    move-object v0, v1

    .line 2304251
    invoke-virtual {v0}, LX/5vQ;->a()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    move-result-object v0

    return-object v0
.end method
