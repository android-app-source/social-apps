.class public LX/Fbh;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbh;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260883
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2260884
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbh;
    .locals 3

    .prologue
    .line 2260885
    sget-object v0, LX/Fbh;->a:LX/Fbh;

    if-nez v0, :cond_1

    .line 2260886
    const-class v1, LX/Fbh;

    monitor-enter v1

    .line 2260887
    :try_start_0
    sget-object v0, LX/Fbh;->a:LX/Fbh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260888
    if-eqz v2, :cond_0

    .line 2260889
    :try_start_1
    new-instance v0, LX/Fbh;

    invoke-direct {v0}, LX/Fbh;-><init>()V

    .line 2260890
    move-object v0, v0

    .line 2260891
    sput-object v0, LX/Fbh;->a:LX/Fbh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260892
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260893
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260894
    :cond_1
    sget-object v0, LX/Fbh;->a:LX/Fbh;

    return-object v0

    .line 2260895
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260896
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260897
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2260898
    if-nez v0, :cond_0

    .line 2260899
    const/4 v0, 0x0

    .line 2260900
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    new-instance v2, LX/8dX;

    invoke-direct {v2}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jP()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    .line 2260901
    if-nez v3, :cond_1

    .line 2260902
    const/4 v4, 0x0

    .line 2260903
    :goto_1
    move-object v3, v4

    .line 2260904
    iput-object v3, v2, LX/8dX;->aR:Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    .line 2260905
    move-object v2, v2

    .line 2260906
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ki()Ljava/lang/String;

    move-result-object v3

    .line 2260907
    iput-object v3, v2, LX/8dX;->aU:Ljava/lang/String;

    .line 2260908
    move-object v2, v2

    .line 2260909
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kh()Lcom/facebook/graphql/model/GraphQLTrendingTopicData;

    move-result-object v3

    .line 2260910
    if-nez v3, :cond_2

    .line 2260911
    const/4 v4, 0x0

    .line 2260912
    :goto_2
    move-object v3, v4

    .line 2260913
    iput-object v3, v2, LX/8dX;->aT:Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    .line 2260914
    move-object v2, v2

    .line 2260915
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    .line 2260916
    iput-object v3, v2, LX/8dX;->aS:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2260917
    move-object v2, v2

    .line 2260918
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v0

    .line 2260919
    iput-object v0, v2, LX/8dX;->aV:Ljava/lang/String;

    .line 2260920
    move-object v0, v2

    .line 2260921
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2260922
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2260923
    move-object v0, v1

    .line 2260924
    goto :goto_0

    :cond_1
    new-instance v4, LX/8f1;

    invoke-direct {v4}, LX/8f1;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->bF()J

    move-result-wide v6

    .line 2260925
    iput-wide v6, v4, LX/8f1;->c:J

    .line 2260926
    move-object v4, v4

    .line 2260927
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 2260928
    iput-object v5, v4, LX/8f1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2260929
    move-object v4, v4

    .line 2260930
    invoke-virtual {v4}, LX/8f1;->a()Lcom/facebook/search/results/protocol/SearchResultsNewsContextModels$SearchResultsNewsContextModel$TopHeadlineObjectModel;

    move-result-object v4

    goto :goto_1

    :cond_2
    new-instance v4, LX/A21;

    invoke-direct {v4}, LX/A21;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->j()Ljava/lang/String;

    move-result-object v5

    .line 2260931
    iput-object v5, v4, LX/A21;->a:Ljava/lang/String;

    .line 2260932
    move-object v4, v4

    .line 2260933
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->k()Ljava/lang/String;

    move-result-object v5

    .line 2260934
    iput-object v5, v4, LX/A21;->b:Ljava/lang/String;

    .line 2260935
    move-object v4, v4

    .line 2260936
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    .line 2260937
    iput-object v5, v4, LX/A21;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2260938
    move-object v4, v4

    .line 2260939
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    .line 2260940
    iput-object v5, v4, LX/A21;->d:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2260941
    move-object v4, v4

    .line 2260942
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    .line 2260943
    iput-object v5, v4, LX/A21;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2260944
    move-object v4, v4

    .line 2260945
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->o()Ljava/lang/String;

    move-result-object v5

    .line 2260946
    iput-object v5, v4, LX/A21;->f:Ljava/lang/String;

    .line 2260947
    move-object v4, v4

    .line 2260948
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->p()Ljava/lang/String;

    move-result-object v5

    .line 2260949
    iput-object v5, v4, LX/A21;->g:Ljava/lang/String;

    .line 2260950
    move-object v4, v4

    .line 2260951
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTrendingTopicData;->q()Ljava/lang/String;

    move-result-object v5

    .line 2260952
    iput-object v5, v4, LX/A21;->h:Ljava/lang/String;

    .line 2260953
    move-object v4, v4

    .line 2260954
    invoke-virtual {v4}, LX/A21;->a()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v4

    goto/16 :goto_2
.end method
