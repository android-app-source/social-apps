.class public final LX/Gyx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GA2;


# instance fields
.field public final synthetic a:Lcom/facebook/login/DeviceAuthDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/login/DeviceAuthDialog;)V
    .locals 0

    .prologue
    .line 2410685
    iput-object p1, p0, LX/Gyx;->a:Lcom/facebook/login/DeviceAuthDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/GAY;)V
    .locals 13

    .prologue
    .line 2410686
    iget-object v0, p0, LX/Gyx;->a:Lcom/facebook/login/DeviceAuthDialog;

    iget-object v0, v0, Lcom/facebook/login/DeviceAuthDialog;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2410687
    :goto_0
    return-void

    .line 2410688
    :cond_0
    iget-object v0, p1, LX/GAY;->d:LX/GAF;

    move-object v0, v0

    .line 2410689
    if-eqz v0, :cond_5

    .line 2410690
    invoke-virtual {v0}, LX/GAF;->d()Ljava/lang/String;

    move-result-object v0

    .line 2410691
    const-string v1, "authorization_pending"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "slow_down"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2410692
    :cond_1
    iget-object v0, p0, LX/Gyx;->a:Lcom/facebook/login/DeviceAuthDialog;

    invoke-static {v0}, Lcom/facebook/login/DeviceAuthDialog;->b$redex0(Lcom/facebook/login/DeviceAuthDialog;)V

    goto :goto_0

    .line 2410693
    :cond_2
    const-string v1, "authorization_declined"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "code_expired"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2410694
    :cond_3
    iget-object v0, p0, LX/Gyx;->a:Lcom/facebook/login/DeviceAuthDialog;

    invoke-static {v0}, Lcom/facebook/login/DeviceAuthDialog;->i(Lcom/facebook/login/DeviceAuthDialog;)V

    goto :goto_0

    .line 2410695
    :cond_4
    iget-object v0, p0, LX/Gyx;->a:Lcom/facebook/login/DeviceAuthDialog;

    .line 2410696
    iget-object v1, p1, LX/GAY;->d:LX/GAF;

    move-object v1, v1

    .line 2410697
    iget-object v2, v1, LX/GAF;->o:LX/GAA;

    move-object v1, v2

    .line 2410698
    invoke-static {v0, v1}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;LX/GAA;)V

    goto :goto_0

    .line 2410699
    :cond_5
    :try_start_0
    iget-object v0, p1, LX/GAY;->b:Lorg/json/JSONObject;

    move-object v0, v0

    .line 2410700
    iget-object v1, p0, LX/Gyx;->a:Lcom/facebook/login/DeviceAuthDialog;

    const-string v2, "access_token"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x0

    .line 2410701
    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    .line 2410702
    const-string v3, "fields"

    const-string v4, "id,permissions"

    invoke-virtual {v12, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410703
    new-instance v3, Lcom/facebook/AccessToken;

    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v5

    const-string v6, "0"

    move-object v4, v0

    move-object v8, v7

    move-object v9, v7

    move-object v10, v7

    move-object v11, v7

    invoke-direct/range {v3 .. v11}, Lcom/facebook/AccessToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;LX/GA9;Ljava/util/Date;Ljava/util/Date;)V

    .line 2410704
    new-instance v4, LX/GAU;

    const-string v6, "me"

    sget-object v8, LX/GAZ;->GET:LX/GAZ;

    new-instance v9, LX/Gyy;

    invoke-direct {v9, v1, v0}, LX/Gyy;-><init>(Lcom/facebook/login/DeviceAuthDialog;Ljava/lang/String;)V

    move-object v5, v3

    move-object v7, v12

    invoke-direct/range {v4 .. v9}, LX/GAU;-><init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;)V

    .line 2410705
    invoke-virtual {v4}, LX/GAU;->g()LX/GAV;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2410706
    goto :goto_0

    .line 2410707
    :catch_0
    move-exception v0

    .line 2410708
    iget-object v1, p0, LX/Gyx;->a:Lcom/facebook/login/DeviceAuthDialog;

    new-instance v2, LX/GAA;

    invoke-direct {v2, v0}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/facebook/login/DeviceAuthDialog;->a$redex0(Lcom/facebook/login/DeviceAuthDialog;LX/GAA;)V

    goto/16 :goto_0
.end method
