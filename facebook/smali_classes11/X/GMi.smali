.class public final LX/GMi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;)V
    .locals 0

    .prologue
    .line 2345168
    iput-object p1, p0, LX/GMi;->a:Lcom/facebook/adinterfaces/ui/preview/AdInterfacesBoostedComponentPreviewFetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2345169
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2345170
    if-eqz p1, :cond_0

    .line 2345171
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2345172
    if-nez v0, :cond_1

    .line 2345173
    :cond_0
    const/4 v0, 0x0

    .line 2345174
    :goto_0
    return-object v0

    .line 2345175
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2345176
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesAdPreviewQueryModels$AdInterfacesAdPreviewFeedUnitQueryModel;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    goto :goto_0
.end method
