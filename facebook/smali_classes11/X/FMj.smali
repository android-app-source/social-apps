.class public LX/FMj;
.super Landroid/os/Handler;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field private static volatile y:LX/FMj;


# instance fields
.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/SmsThreadManager;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMc;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMr;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMq;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMp;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMs;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMw;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FN0;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMu;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNi;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rd;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public x:Landroid/app/Service;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2230587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/3RH;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "smsreply"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/FMj;->a:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/os/Looper;)V
    .locals 1
    .param p1    # Landroid/os/Looper;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2230545
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2230546
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230547
    iput-object v0, p0, LX/FMj;->d:LX/0Ot;

    .line 2230548
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230549
    iput-object v0, p0, LX/FMj;->e:LX/0Ot;

    .line 2230550
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230551
    iput-object v0, p0, LX/FMj;->f:LX/0Ot;

    .line 2230552
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230553
    iput-object v0, p0, LX/FMj;->g:LX/0Ot;

    .line 2230554
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230555
    iput-object v0, p0, LX/FMj;->h:LX/0Ot;

    .line 2230556
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230557
    iput-object v0, p0, LX/FMj;->i:LX/0Ot;

    .line 2230558
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230559
    iput-object v0, p0, LX/FMj;->j:LX/0Ot;

    .line 2230560
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230561
    iput-object v0, p0, LX/FMj;->k:LX/0Ot;

    .line 2230562
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230563
    iput-object v0, p0, LX/FMj;->l:LX/0Ot;

    .line 2230564
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230565
    iput-object v0, p0, LX/FMj;->m:LX/0Ot;

    .line 2230566
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230567
    iput-object v0, p0, LX/FMj;->n:LX/0Ot;

    .line 2230568
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230569
    iput-object v0, p0, LX/FMj;->o:LX/0Ot;

    .line 2230570
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230571
    iput-object v0, p0, LX/FMj;->p:LX/0Ot;

    .line 2230572
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230573
    iput-object v0, p0, LX/FMj;->q:LX/0Ot;

    .line 2230574
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230575
    iput-object v0, p0, LX/FMj;->r:LX/0Ot;

    .line 2230576
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230577
    iput-object v0, p0, LX/FMj;->s:LX/0Ot;

    .line 2230578
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230579
    iput-object v0, p0, LX/FMj;->t:LX/0Ot;

    .line 2230580
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230581
    iput-object v0, p0, LX/FMj;->u:LX/0Ot;

    .line 2230582
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230583
    iput-object v0, p0, LX/FMj;->v:LX/0Ot;

    .line 2230584
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2230585
    iput-object v0, p0, LX/FMj;->w:LX/0Ot;

    .line 2230586
    return-void
.end method

.method public static a(LX/0QB;)LX/FMj;
    .locals 3

    .prologue
    .line 2230535
    sget-object v0, LX/FMj;->y:LX/FMj;

    if-nez v0, :cond_1

    .line 2230536
    const-class v1, LX/FMj;

    monitor-enter v1

    .line 2230537
    :try_start_0
    sget-object v0, LX/FMj;->y:LX/FMj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2230538
    if-eqz v2, :cond_0

    .line 2230539
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/FMj;->b(LX/0QB;)LX/FMj;

    move-result-object v0

    sput-object v0, LX/FMj;->y:LX/FMj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2230540
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2230541
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2230542
    :cond_1
    sget-object v0, LX/FMj;->y:LX/FMj;

    return-object v0

    .line 2230543
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2230544
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 2230533
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.messaging.sms.DELETE_TEMP_FILE"

    const-class v2, LX/FMm;

    invoke-direct {v0, v1, p1, p0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2230534
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 2230532
    sget-object v0, LX/3RH;->H:Ljava/lang/String;

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/FMj;Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 2

    .prologue
    .line 2230527
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2230528
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2230529
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 2230530
    iget-object v1, p0, LX/FMj;->b:Landroid/content/Context;

    invoke-static {v1, v0}, LX/5e6;->a(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v0

    .line 2230531
    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    .line 2230512
    iget-object v0, p0, LX/FMj;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMw;

    invoke-virtual {v0}, LX/FMw;->b()Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    move-result-object v2

    .line 2230513
    if-nez v2, :cond_0

    .line 2230514
    :goto_0
    return-void

    .line 2230515
    :cond_0
    :try_start_0
    iget-object v0, v2, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2230516
    invoke-static {v0}, LX/FMQ;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2230517
    iget-object v0, p0, LX/FMj;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMr;

    invoke-virtual {v0, v2}, LX/FMr;->a(Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2230518
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2230519
    iget-object v0, p0, LX/FMj;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMw;

    .line 2230520
    iget-object v3, v2, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2230521
    iget-wide v6, v2, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    move-wide v4, v6

    .line 2230522
    invoke-virtual {v0, v3, v4, v5}, LX/FMw;->b(Ljava/lang/String;J)V

    .line 2230523
    throw v1

    .line 2230524
    :cond_1
    invoke-static {v0}, LX/FMQ;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2230525
    iget-object v0, p0, LX/FMj;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMq;

    invoke-virtual {v0, v2}, LX/FMq;->a(Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    goto :goto_0

    .line 2230526
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown message id type to send "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static a(LX/FMj;Landroid/content/Context;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FMj;",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/SmsThreadManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMc;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMq;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMp;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FN0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMu;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FNi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1rd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2230511
    iput-object p1, p0, LX/FMj;->b:Landroid/content/Context;

    iput-object p2, p0, LX/FMj;->c:LX/0Or;

    iput-object p3, p0, LX/FMj;->d:LX/0Ot;

    iput-object p4, p0, LX/FMj;->e:LX/0Ot;

    iput-object p5, p0, LX/FMj;->f:LX/0Ot;

    iput-object p6, p0, LX/FMj;->g:LX/0Ot;

    iput-object p7, p0, LX/FMj;->h:LX/0Ot;

    iput-object p8, p0, LX/FMj;->i:LX/0Ot;

    iput-object p9, p0, LX/FMj;->j:LX/0Ot;

    iput-object p10, p0, LX/FMj;->k:LX/0Ot;

    iput-object p11, p0, LX/FMj;->l:LX/0Ot;

    iput-object p12, p0, LX/FMj;->m:LX/0Ot;

    iput-object p13, p0, LX/FMj;->n:LX/0Ot;

    iput-object p14, p0, LX/FMj;->o:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/FMj;->p:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/FMj;->q:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/FMj;->r:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/FMj;->s:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/FMj;->t:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/FMj;->u:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/FMj;->v:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, LX/FMj;->w:LX/0Ot;

    return-void
.end method

.method private static a(LX/FMj;Lcom/facebook/messaging/model/messages/Message;IILX/FMM;Ljava/lang/String;ZZIZLX/Edm;LX/FMM;)V
    .locals 24
    .param p4    # LX/FMM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2230496
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMj;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/sms/SmsThreadManager;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(J)Ljava/util/List;

    move-result-object v11

    .line 2230497
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMj;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, LX/30m;->a(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v7

    .line 2230498
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v2}, LX/FMQ;->d(Ljava/lang/String;)Z

    move-result v3

    .line 2230499
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    sget-object v4, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    if-ne v2, v4, :cond_2

    :cond_0
    const/4 v4, 0x1

    .line 2230500
    :goto_0
    if-eqz v4, :cond_3

    const/4 v5, 0x0

    .line 2230501
    :goto_1
    const/16 v17, 0x0

    .line 2230502
    const/16 v18, 0x0

    .line 2230503
    if-eqz p10, :cond_1

    .line 2230504
    invoke-virtual/range {p10 .. p10}, LX/Edm;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    .line 2230505
    invoke-virtual/range {p10 .. p10}, LX/Edm;->e()Ljava/lang/String;

    move-result-object v18

    .line 2230506
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMj;->t:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FNi;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/FMj;->b:Landroid/content/Context;

    invoke-virtual {v2, v6}, LX/FNi;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    .line 2230507
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMj;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/30m;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v6, v6, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v6, v0, LX/FMj;->s:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/FMu;

    move-object/from16 v0, p1

    iget v8, v0, Lcom/facebook/messaging/model/messages/Message;->V:I

    invoke-virtual {v6, v8}, LX/FMu;->g(I)Z

    move-result v14

    invoke-virtual/range {p11 .. p11}, LX/FMM;->name()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v6, v0, LX/FMj;->u:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1rd;

    move-object/from16 v0, p1

    iget v8, v0, Lcom/facebook/messaging/model/messages/Message;->V:I

    invoke-virtual {v6, v8}, LX/1rd;->m(I)I

    move-result v21

    move-object/from16 v0, p0

    iget-object v6, v0, LX/FMj;->u:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1rd;

    invoke-virtual {v6}, LX/1rd;->a()I

    move-result v22

    move-object/from16 v0, p0

    iget-object v6, v0, LX/FMj;->u:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1rd;

    invoke-virtual {v6}, LX/1rd;->b()I

    move-result v23

    move-object/from16 v6, p5

    move/from16 v8, p2

    move/from16 v9, p3

    move/from16 v12, p6

    move/from16 v13, p7

    move/from16 v15, p8

    move/from16 v16, p9

    invoke-virtual/range {v2 .. v23}, LX/30m;->a(ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/util/List;ZZZIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    .line 2230508
    return-void

    .line 2230509
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 2230510
    :cond_3
    invoke-virtual/range {p4 .. p4}, LX/FMM;->name()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1
.end method

.method private static b(LX/0QB;)LX/FMj;
    .locals 25

    .prologue
    .line 2230493
    new-instance v2, LX/FMj;

    invoke-static/range {p0 .. p0}, LX/0Zp;->a(LX/0QB;)Landroid/os/Looper;

    move-result-object v3

    check-cast v3, Landroid/os/Looper;

    invoke-direct {v2, v3}, LX/FMj;-><init>(Landroid/os/Looper;)V

    .line 2230494
    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x2847

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xd9c

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x298a

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xda0

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2995

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2994

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2998

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2993

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x2997

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x2992

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x2990

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x2991

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x2996

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x299b

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x299e

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x2999

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x29b8

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x123e

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0xd9d

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0xf9a

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    invoke-static/range {v2 .. v24}, LX/FMj;->a(LX/FMj;Landroid/content/Context;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2230495
    return-object v2
.end method

.method private c(Landroid/content/Intent;)V
    .locals 17

    .prologue
    .line 2230260
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 2230261
    if-nez v2, :cond_12

    .line 2230262
    const-string v3, "uri"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2230263
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_12

    .line 2230264
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object v14, v2

    .line 2230265
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a(Landroid/os/Bundle;)Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;

    move-result-object v15

    .line 2230266
    const-string v2, "result_code"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 2230267
    const-string v2, "mmssms_quickfail_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, LX/FMM;

    .line 2230268
    if-eqz v15, :cond_0

    :try_start_0
    invoke-virtual {v15}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/FMQ;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    const/4 v3, 0x1

    move v8, v3

    .line 2230269
    :goto_1
    if-eqz v2, :cond_4

    move-object v6, v2

    .line 2230270
    :goto_2
    const-string v3, "mmssms_error_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2230271
    const-string v3, "number_of_parts"

    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 2230272
    const-string v3, "offline_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2230273
    sget-object v13, LX/FMM;->NO_ERROR:LX/FMM;

    .line 2230274
    if-eqz v15, :cond_1

    .line 2230275
    invoke-virtual {v15}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->i()LX/FMM;

    move-result-object v13

    .line 2230276
    invoke-virtual {v15, v6}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a(LX/FMM;)V

    .line 2230277
    :cond_1
    const/4 v3, 0x1

    if-ne v10, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, LX/FMj;->r:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FN0;

    invoke-virtual {v3, v6, v15}, LX/FN0;->a(LX/FMM;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2230278
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMj;->r:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FN0;

    invoke-virtual {v2, v6, v15}, LX/FN0;->b(LX/FMM;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2230279
    :cond_2
    :goto_3
    return-void

    .line 2230280
    :cond_3
    const/4 v3, 0x0

    move v8, v3

    goto :goto_1

    .line 2230281
    :cond_4
    if-eqz v8, :cond_5

    invoke-static {v5}, LX/FMb;->a(I)LX/FMM;

    move-result-object v6

    goto :goto_2

    :cond_5
    invoke-static {v5}, LX/FMb;->b(I)LX/FMM;

    move-result-object v6

    goto :goto_2

    .line 2230282
    :cond_6
    if-eqz v8, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, LX/FMj;->j:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v3, v14, v7, v4}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->a(Landroid/net/Uri;Landroid/os/Bundle;Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2230283
    :goto_4
    if-eqz v3, :cond_2

    .line 2230284
    const/4 v9, 0x0

    .line 2230285
    if-eqz v8, :cond_7

    .line 2230286
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FMj;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;

    invoke-virtual {v4, v14, v6}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->a(Landroid/net/Uri;LX/FMM;)LX/FMM;

    move-result-object v6

    .line 2230287
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FMj;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;

    invoke-virtual {v4, v14}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->a(Landroid/net/Uri;)Z

    move-result v9

    .line 2230288
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FMj;->j:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;

    invoke-virtual {v4, v14}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsSentAction;->b(Landroid/net/Uri;)V

    .line 2230289
    :cond_7
    const-string v4, "mmssms_quickfail_msg"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2230290
    if-eqz v2, :cond_b

    .line 2230291
    :goto_5
    const/4 v12, 0x0

    .line 2230292
    if-nez v8, :cond_8

    .line 2230293
    const-string v4, "android.telephony.extra.MMS_DATA"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    .line 2230294
    if-eqz v5, :cond_8

    .line 2230295
    new-instance v11, LX/Edf;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/FMj;->s:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FMu;

    iget v0, v3, Lcom/facebook/messaging/model/messages/Message;->V:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v4, v0}, LX/FMu;->a(I)Z

    move-result v4

    invoke-direct {v11, v5, v4}, LX/Edf;-><init>([BZ)V

    invoke-virtual {v11}, LX/Edf;->a()LX/EdM;

    move-result-object v4

    .line 2230296
    instance-of v5, v4, LX/Edm;

    if-eqz v5, :cond_8

    .line 2230297
    check-cast v4, LX/Edm;

    move-object v12, v4

    .line 2230298
    :cond_8
    if-eqz v15, :cond_d

    invoke-virtual {v15}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g()I

    move-result v4

    :goto_6
    if-eqz v15, :cond_e

    invoke-virtual {v15}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->d()I

    move-result v5

    :goto_7
    if-nez v8, :cond_f

    invoke-static {}, LX/EdD;->a()Z

    move-result v8

    if-nez v8, :cond_f

    const/4 v8, 0x1

    :goto_8
    if-eqz v2, :cond_10

    const/4 v11, 0x1

    :goto_9
    move-object/from16 v2, p0

    invoke-static/range {v2 .. v13}, LX/FMj;->a(LX/FMj;Lcom/facebook/messaging/model/messages/Message;IILX/FMM;Ljava/lang/String;ZZIZLX/Edm;LX/FMM;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    .line 2230299
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 2230300
    if-eqz v15, :cond_11

    .line 2230301
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMj;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FMw;

    invoke-virtual {v15}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, LX/FMw;->b(Ljava/lang/String;J)V

    .line 2230302
    :cond_9
    :goto_a
    throw v3

    .line 2230303
    :cond_a
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FMj;->k:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v14, v4}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsSentAction;->a(Landroid/net/Uri;Landroid/os/Bundle;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    goto/16 :goto_4

    .line 2230304
    :cond_b
    if-eqz v8, :cond_c

    const-string v4, "errorCode"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v5, v4}, LX/FMb;->a(II)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_5

    :cond_c
    const-string v4, "android.telephony.extra.MMS_HTTP_STATUS"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v7, "error_message"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v4, v7}, LX/FMb;->a(IILjava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    goto/16 :goto_5

    .line 2230305
    :cond_d
    const/4 v4, 0x0

    goto :goto_6

    :cond_e
    const/4 v5, 0x0

    goto :goto_7

    :cond_f
    const/4 v8, 0x0

    goto :goto_8

    :cond_10
    const/4 v11, 0x0

    goto :goto_9

    .line 2230306
    :cond_11
    if-eqz v14, :cond_9

    .line 2230307
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMj;->q:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FMw;

    invoke-static {v14}, LX/FMQ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v6, 0x0

    invoke-virtual {v2, v4, v6, v7}, LX/FMw;->b(Ljava/lang/String;J)V

    goto :goto_a

    :cond_12
    move-object v14, v2

    goto/16 :goto_0
.end method

.method private g(Landroid/content/Intent;)V
    .locals 14

    .prologue
    .line 2230422
    iget-object v0, p0, LX/FMj;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2230423
    const-string v2, "result_code"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 2230424
    const-string v2, "extra_uri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Landroid/net/Uri;

    .line 2230425
    const-string v2, "content_uri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/net/Uri;

    .line 2230426
    const-string v2, "extra_repersist_on_error"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    .line 2230427
    iget-object v2, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->h:LX/1rd;

    const-string v3, "subscription"

    const/4 v4, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iget-object v4, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->i:LX/FN6;

    invoke-virtual {v4}, LX/FN6;->a()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/1rd;->a(II)I

    move-result v2

    .line 2230428
    const/4 v4, 0x0

    .line 2230429
    const/4 v5, 0x0

    .line 2230430
    :try_start_0
    iget-object v3, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v3}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/Edh;->a(Landroid/net/Uri;)LX/EdM;

    move-result-object v3

    check-cast v3, LX/EdW;
    :try_end_0
    .catch LX/EdT; {:try_start_0 .. :try_end_0} :catch_0

    move-object v12, v3

    .line 2230431
    :goto_0
    const/4 v3, -0x1

    if-ne v11, v3, :cond_d

    .line 2230432
    :try_start_1
    invoke-static {v0, v8}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;Landroid/net/Uri;)[B

    move-result-object v3

    .line 2230433
    new-instance v5, LX/Edf;

    iget-object v6, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->e:LX/FMu;

    invoke-virtual {v6, v2}, LX/FMu;->a(I)Z

    move-result v6

    invoke-direct {v5, v3, v6}, LX/Edf;-><init>([BZ)V

    invoke-virtual {v5}, LX/Edf;->a()LX/EdM;

    move-result-object v3

    check-cast v3, LX/Edl;

    .line 2230434
    invoke-static {v0, v3}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;LX/Edl;)V

    .line 2230435
    invoke-virtual {v3}, LX/EdM;->c()LX/EdS;

    move-result-object v5

    if-nez v5, :cond_0

    if-eqz v12, :cond_0

    .line 2230436
    const/4 v4, 0x1

    .line 2230437
    invoke-virtual {v12}, LX/EdM;->c()LX/EdS;

    move-result-object v5

    .line 2230438
    if-eqz v5, :cond_0

    .line 2230439
    invoke-virtual {v3, v5}, LX/EdM;->a(LX/EdS;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    move v10, v4

    .line 2230440
    :try_start_2
    invoke-static {v0, v3, v2}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;LX/Edl;I)Landroid/net/Uri;

    move-result-object v3

    .line 2230441
    iget-object v4, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v9, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2230442
    invoke-static {v0, v3, v9}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 2230443
    if-eqz v12, :cond_1

    .line 2230444
    if-eqz v13, :cond_5

    .line 2230445
    new-instance v3, LX/EdX;

    const/16 v4, 0x12

    invoke-virtual {v12}, LX/EdW;->g()[B

    move-result-object v5

    const/16 v6, 0x81

    invoke-direct {v3, v4, v5, v6}, LX/EdX;-><init>(I[BI)V

    .line 2230446
    iget-object v4, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v4, v3}, LX/FMY;->a(Landroid/content/Context;LX/EdM;)Landroid/net/Uri;

    move-result-object v4

    .line 2230447
    :goto_1
    iget-object v3, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    iget-object v5, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->e:LX/FMu;

    invoke-virtual {v5}, LX/FMu;->b()Z

    move-result v5

    if-eqz v5, :cond_6

    new-instance v5, Ljava/lang/String;

    invoke-virtual {v12}, LX/EdW;->d()[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    :goto_2
    iget-object v6, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v6, v4}, LX/FMj;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, LX/EdD;->a(ILandroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;Z)V

    .line 2230448
    :cond_1
    iget-object v3, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->d:LX/FMX;

    invoke-static {v9}, LX/FMQ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 2230449
    iget-object v5, v3, LX/FMX;->a:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/common/util/concurrent/SettableFuture;

    .line 2230450
    if-eqz v5, :cond_2

    .line 2230451
    sget-object v6, LX/FMW;->NO_ERROR:LX/FMW;

    const v7, 0x4183b47b

    invoke-static {v5, v6, v7}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 2230452
    :cond_2
    move v8, v10

    move v10, v11

    .line 2230453
    :goto_3
    const/4 v3, -0x1

    if-eq v10, v3, :cond_3

    .line 2230454
    if-eqz v13, :cond_7

    .line 2230455
    :try_start_3
    invoke-static {v0, v12, v9, v2}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;LX/EdW;Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v9

    .line 2230456
    const/4 v3, 0x0

    invoke-static {v0, v9, v3}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->a(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;Landroid/net/Uri;Landroid/net/Uri;)V

    .line 2230457
    :goto_4
    new-instance v3, LX/EdX;

    const/16 v4, 0x12

    invoke-virtual {v12}, LX/EdW;->g()[B

    move-result-object v5

    const/16 v6, 0x83

    invoke-direct {v3, v4, v5, v6}, LX/EdX;-><init>(I[BI)V

    .line 2230458
    iget-object v4, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v4, v3}, LX/FMY;->a(Landroid/content/Context;LX/EdM;)Landroid/net/Uri;

    move-result-object v4

    .line 2230459
    iget-object v3, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    iget-object v5, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->e:LX/FMu;

    invoke-virtual {v5}, LX/FMu;->b()Z

    move-result v5

    if-eqz v5, :cond_8

    new-instance v5, Ljava/lang/String;

    invoke-virtual {v12}, LX/EdW;->d()[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V

    :goto_5
    iget-object v6, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v6, v4}, LX/FMj;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, LX/EdD;->a(ILandroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;Z)V

    .line 2230460
    iget-object v2, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v2, v9}, LX/FMn;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2230461
    iget-object v2, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->d:LX/FMX;

    invoke-static {v9}, LX/FMQ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/FMW;->OTHER:LX/FMW;

    .line 2230462
    iget-object v5, v2, LX/FMX;->a:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/common/util/concurrent/SettableFuture;

    .line 2230463
    if-eqz v5, :cond_3

    .line 2230464
    const v6, -0x1b37f5ff

    invoke-static {v5, v4, v6}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 2230465
    :cond_3
    :goto_6
    const/4 v2, -0x1

    if-ne v10, v2, :cond_9

    const/4 v2, 0x1

    .line 2230466
    :goto_7
    if-eqz v2, :cond_a

    const/4 v4, 0x0

    .line 2230467
    :goto_8
    iget-object v2, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/30m;

    const/4 v3, -0x1

    if-ne v10, v3, :cond_b

    const/4 v3, 0x1

    :goto_9
    const-string v5, "android.telephony.extra.MMS_HTTP_STATUS"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    const/4 v6, 0x0

    invoke-static {v10, v5, v6}, LX/FMb;->a(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, LX/EdD;->a()Z

    move-result v6

    if-nez v6, :cond_c

    const/4 v7, 0x1

    :goto_a
    move v6, v13

    .line 2230468
    const-string v9, "sms_takeover_message_downloaded"

    invoke-static {v2, v9}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "is_download_success"

    invoke-virtual {v9, v10, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    .line 2230469
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 2230470
    const-string v10, "error_type"

    invoke-virtual {v9, v10, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2230471
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 2230472
    const-string v10, "error_msg"

    invoke-virtual {v9, v10, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2230473
    :cond_4
    const-string v10, "auto_download"

    invoke-virtual {v9, v10, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2230474
    const-string v10, "legacy"

    invoke-virtual {v9, v10, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2230475
    const-string v10, "is_sender_missing"

    invoke-virtual {v9, v10, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2230476
    invoke-static {v2, v9}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2230477
    return-void

    :catch_0
    move-object v12, v5

    goto/16 :goto_0

    .line 2230478
    :cond_5
    :try_start_4
    new-instance v4, LX/EdN;

    const/16 v3, 0x12

    invoke-virtual {v12}, LX/EdW;->g()[B

    move-result-object v5

    invoke-direct {v4, v3, v5}, LX/EdN;-><init>(I[B)V

    .line 2230479
    new-instance v5, LX/EdS;

    iget-object v3, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->l:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FNi;

    iget-object v6, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-virtual {v3, v6}, LX/FNi;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, LX/EdS;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/EdM;->a(LX/EdS;)V

    .line 2230480
    iget-object v3, v0, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b:Landroid/content/Context;

    invoke-static {v3, v4}, LX/FMY;->a(Landroid/content/Context;LX/EdM;)Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v4

    goto/16 :goto_1

    .line 2230481
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2230482
    :catch_1
    move-exception v3

    .line 2230483
    :goto_b
    const-string v5, "ProcessMmsDownloadedAction"

    const-string v6, "Error processing downloaded MMS content: %s, msg: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v8, v7, v10

    const/4 v8, 0x1

    aput-object v9, v7, v8

    invoke-static {v5, v3, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2230484
    const/16 v3, 0x64

    move v8, v4

    move v10, v3

    goto/16 :goto_3

    .line 2230485
    :cond_7
    :try_start_5
    invoke-static {v0, v9}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;->b(Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsDownloadedAction;Landroid/net/Uri;)LX/FMW;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_4

    .line 2230486
    :catch_2
    move-exception v2

    .line 2230487
    const-string v3, "ProcessMmsDownloadedAction"

    const-string v4, "Failed handling failed mms download: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v9, v5, v6

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 2230488
    :cond_8
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 2230489
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 2230490
    :cond_a
    invoke-static {v10}, LX/FMb;->b(I)LX/FMM;

    move-result-object v2

    invoke-virtual {v2}, LX/FMM;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    .line 2230491
    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_9

    :cond_c
    const/4 v7, 0x0

    goto/16 :goto_a

    .line 2230492
    :catch_3
    move-exception v3

    move v4, v10

    goto :goto_b

    :cond_d
    move v8, v4

    move v10, v11

    goto/16 :goto_3
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    .line 2230308
    if-eqz p1, :cond_0

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v0, :cond_1

    .line 2230309
    :cond_0
    :goto_0
    return-void

    .line 2230310
    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    .line 2230311
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 2230312
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 2230313
    :try_start_0
    iget-object v1, p0, LX/FMj;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FMc;

    invoke-virtual {v1}, LX/FMc;->b()V

    .line 2230314
    const-string v1, "com.facebook.messaging.sms.REQUEST_SEND_MESSAGE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2230315
    invoke-direct {p0}, LX/FMj;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2230316
    :cond_2
    :goto_1
    iget-object v0, p0, LX/FMj;->x:Landroid/app/Service;

    invoke-static {v0, v2}, LX/FMg;->a(Landroid/app/Service;I)V

    goto :goto_0

    .line 2230317
    :cond_3
    :try_start_1
    const-string v1, "com.facebook.messaging.sms.MESSAGE_SENT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2230318
    invoke-direct {p0, v0}, LX/FMj;->c(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2230319
    :catch_0
    move-exception v0

    .line 2230320
    :try_start_2
    const-string v1, "SmsHandler"

    const-string v3, "Exception in sms handling."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2230321
    iget-object v0, p0, LX/FMj;->x:Landroid/app/Service;

    invoke-static {v0, v2}, LX/FMg;->a(Landroid/app/Service;I)V

    goto :goto_0

    .line 2230322
    :cond_4
    :try_start_3
    const-string v1, "android.provider.Telephony.SMS_DELIVER"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2230323
    iget-object v1, p0, LX/FMj;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a(Landroid/content/Intent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2230324
    goto :goto_1

    .line 2230325
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/FMj;->x:Landroid/app/Service;

    invoke-static {v1, v2}, LX/FMg;->a(Landroid/app/Service;I)V

    throw v0

    .line 2230326
    :cond_5
    :try_start_4
    const-string v1, "android.provider.Telephony.WAP_PUSH_DELIVER"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2230327
    iget-object v1, p0, LX/FMj;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessMmsReceivedAction;->a(Landroid/os/Bundle;)V

    .line 2230328
    goto :goto_1

    .line 2230329
    :cond_6
    const-string v1, "com.facebook.messaging.sms.DOWNLOAD_MMS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2230330
    iget-object v1, p0, LX/FMj;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FMp;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/FMp;->a(Landroid/os/Bundle;)V

    .line 2230331
    goto :goto_1

    .line 2230332
    :cond_7
    const-string v1, "com.facebook.messaging.sms.MMS_DOWNLOADED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2230333
    invoke-direct {p0, v0}, LX/FMj;->g(Landroid/content/Intent;)V

    goto :goto_1

    .line 2230334
    :cond_8
    const-string v1, "com.facebook.messaging.sms.COMPOSE_SMS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2230335
    const/4 v6, 0x1

    .line 2230336
    const-string v1, "addresses"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2230337
    const-string v3, "message"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2230338
    invoke-static {p0, v1}, LX/FMj;->a(LX/FMj;Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    invoke-static {v1}, LX/FMj;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v1

    .line 2230339
    new-instance v4, Landroid/content/Intent;

    sget-object v5, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2230340
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2230341
    const/high16 v1, 0x10000000

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2230342
    const-string v1, "focus_compose"

    invoke-virtual {v4, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2230343
    const-string v1, "show_composer"

    invoke-virtual {v4, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2230344
    const-string v1, "composer_initial_text"

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2230345
    const-string v1, "modify_backstack_override"

    const/4 v3, 0x0

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2230346
    iget-object v1, p0, LX/FMj;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/FMj;->b:Landroid/content/Context;

    invoke-interface {v1, v4, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2230347
    goto/16 :goto_1

    .line 2230348
    :cond_9
    const-string v1, "com.facebook.messaging.sms.HEADLESS_SEND"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2230349
    const-string v1, "addresses"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2230350
    const-string v3, "message"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2230351
    invoke-static {p0, v1}, LX/FMj;->a(LX/FMj;Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2230352
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.facebook.messaging.sms.HEADLESS_SEND"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2230353
    sget-object v5, LX/FMj;->a:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2230354
    const/high16 v5, 0x10000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2230355
    const-string v5, "thread_key"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2230356
    const-string v1, "text"

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2230357
    iget-object v1, p0, LX/FMj;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/FMj;->b:Landroid/content/Context;

    invoke-interface {v1, v4, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2230358
    goto/16 :goto_1

    .line 2230359
    :cond_a
    const-string v1, "com.facebook.messaging.sms.E2E_TEST_RECEIVING_SMS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2230360
    const/4 p1, 0x0

    .line 2230361
    const-string v1, "sms_in_base64"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2230362
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2230363
    :try_start_5
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2230364
    const/4 v4, 0x1

    new-array v4, v4, [[B

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v1, v6}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    aput-object v1, v4, v5

    .line 2230365
    const-string v1, "pdus"

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2230366
    iget-object v1, p0, LX/FMj;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;

    invoke-virtual {v1, v3}, Lcom/facebook/messaging/sms/defaultapp/action/ProcessSmsReceivedAction;->a(Landroid/content/Intent;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2230367
    :goto_2
    :try_start_6
    goto/16 :goto_1

    .line 2230368
    :cond_b
    const-string v1, "com.facebook.messaging.sms.MARK_PENDING_MMS"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2230369
    const-string v1, "com.facebook.messaging.sms.SHORTCODE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2230370
    iget-object v7, p0, LX/FMj;->p:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/FMs;

    const-wide/16 v10, -0x1

    .line 2230371
    const-string v8, "thread_id"

    invoke-virtual {v0, v8, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .line 2230372
    cmp-long v10, v8, v10

    if-lez v10, :cond_c

    .line 2230373
    invoke-static {v8, v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v9

    .line 2230374
    iget-object v8, v7, LX/FMs;->c:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Di5;

    invoke-virtual {v8, v9}, LX/Di5;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2230375
    iget-object v8, v7, LX/FMs;->b:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/cache/ReadThreadManager;

    invoke-virtual {v8, v9}, Lcom/facebook/messaging/cache/ReadThreadManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2230376
    const-string v8, "string_to_copy"

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2230377
    if-eqz v8, :cond_c

    .line 2230378
    iget-object v9, v7, LX/FMs;->a:Landroid/content/Context;

    invoke-static {v9, v8}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2230379
    iget-object v8, v7, LX/FMs;->d:LX/30m;

    const/4 v9, 0x1

    .line 2230380
    const-string v10, "sms_takeover_sms_code_copied"

    invoke-static {v8, v10}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "from_notification"

    invoke-virtual {v10, v11, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    .line 2230381
    invoke-static {v8, v10}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2230382
    iget-object v8, v7, LX/FMs;->e:LX/4nT;

    iget-object v9, v7, LX/FMs;->a:Landroid/content/Context;

    const v10, 0x7f082eb8

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/4nT;->a(Ljava/lang/String;)V

    .line 2230383
    :cond_c
    goto/16 :goto_1

    .line 2230384
    :cond_d
    const-string v1, "com.facebook.messaging.sms.LIKE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2230385
    const-string v1, "thread_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2230386
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2230387
    new-instance v3, Landroid/content/Intent;

    sget-object v4, LX/3GK;->b:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2230388
    invoke-static {v1}, LX/FMj;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2230389
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2230390
    const/high16 v1, 0x10000000

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2230391
    iget-object v1, p0, LX/FMj;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/FMj;->b:Landroid/content/Context;

    invoke-interface {v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2230392
    goto/16 :goto_1

    .line 2230393
    :cond_e
    const-string v1, "com.facebook.messaging.sms.DISMISS_NOTIFICATION"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 2230394
    const-string v1, "thread_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2230395
    iget-object v3, p0, LX/FMj;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Di5;

    invoke-virtual {v3, v1}, LX/Di5;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2230396
    iget-object v3, p0, LX/FMj;->v:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/2Or;

    invoke-virtual {v3}, LX/2Or;->p()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2230397
    invoke-static {v1}, LX/0db;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v4

    .line 2230398
    iget-object v3, p0, LX/FMj;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/30m;

    const/4 v6, 0x0

    .line 2230399
    invoke-static {v1}, LX/0db;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v5

    .line 2230400
    iget-object v7, v3, LX/30m;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 2230401
    iget-object v7, v3, LX/30m;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v7, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v5

    .line 2230402
    :goto_3
    invoke-static {v1}, LX/0db;->g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v7

    .line 2230403
    iget-object v0, v3, LX/30m;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2230404
    iget-object v0, v3, LX/30m;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0, v7, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v6

    .line 2230405
    :cond_f
    const-string v7, "sms_takeover_quickreply_notification_dismissed"

    invoke-static {v3, v7}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 2230406
    const-string v0, "replies_count"

    invoke-virtual {v7, v0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2230407
    const-string v5, "total_notifications_count"

    invoke-virtual {v7, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2230408
    invoke-static {v3, v7}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2230409
    iget-object v1, p0, LX/FMj;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2230410
    iget-object v1, p0, LX/FMj;->w:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, v4}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2230411
    :cond_10
    goto/16 :goto_1

    .line 2230412
    :cond_11
    const-string v1, "com.facebook.messaging.sms.DELETE_TEMP_FILE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2230413
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 2230414
    iget-object v3, p0, LX/FMj;->b:Landroid/content/Context;

    invoke-static {v3, v1}, LX/FMY;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v1

    .line 2230415
    if-eqz v1, :cond_12

    .line 2230416
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 2230417
    :cond_12
    goto/16 :goto_1

    .line 2230418
    :cond_13
    const-string v1, "SmsHandler"

    const-string v3, "Unknown action to handle: serviceId %d, intent %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v1, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 2230419
    :catch_1
    :try_start_7
    move-exception v1

    .line 2230420
    const-string v3, "SmsHandler"

    const-string v4, "Exception in handling full mode sms deliver for e2e test."

    new-array v5, p1, [Ljava/lang/Object;

    invoke-static {v3, v1, v4, v5}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 2230421
    :cond_14
    const-string v1, "SmsHandler"

    const-string v3, "Cannot get the sms message for full mode e2e test."

    invoke-static {v1, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_15
    move v5, v6

    goto/16 :goto_3
.end method
