.class public final LX/GY9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V
    .locals 0

    .prologue
    .line 2364817
    iput-object p1, p0, LX/GY9;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x5970df6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2364818
    iget-object v1, p0, LX/GY9;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    iget-boolean v1, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->i:Z

    if-eqz v1, :cond_0

    .line 2364819
    iget-object v1, p0, LX/GY9;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    invoke-static {v1}, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    .line 2364820
    :goto_0
    const v1, -0x58459336

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2364821
    :cond_0
    iget-object v1, p0, LX/GY9;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    const-wide/16 v7, 0x12c

    .line 2364822
    iget-object v3, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    if-nez v3, :cond_1

    .line 2364823
    iget-object v3, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->m:Landroid/view/ViewGroup;

    .line 2364824
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0300b4

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    .line 2364825
    iget-object v4, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    const v5, 0x7f0d04e4

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, LX/GYB;

    invoke-direct {v5, v1}, LX/GYB;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364826
    iget-object v4, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    const v5, 0x7f0d04e5

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, LX/GY3;

    invoke-direct {v5, v1}, LX/GY3;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364827
    iget-object v4, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2364828
    :cond_1
    iget-object v3, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->m:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    .line 2364829
    iget-object v4, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    int-to-float v5, v3

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setTranslationX(F)V

    .line 2364830
    iget-object v4, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->o:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 2364831
    iget-object v4, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v7, v8}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2364832
    iget-object v3, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n:Landroid/view/ViewGroup;

    new-instance v4, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment$4;

    invoke-direct {v4, v1}, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment$4;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v3, v4, v7, v8}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2364833
    sget-object v3, LX/GYC;->ANIMATING:LX/GYC;

    iput-object v3, v1, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->l:LX/GYC;

    .line 2364834
    goto :goto_0
.end method
