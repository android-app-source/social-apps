.class public abstract LX/FzJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:LX/9kE;

.field private b:LX/0tX;


# direct methods
.method public constructor <init>(LX/9kE;LX/0tX;)V
    .locals 0

    .prologue
    .line 2307814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2307815
    iput-object p1, p0, LX/FzJ;->a:LX/9kE;

    .line 2307816
    iput-object p2, p0, LX/FzJ;->b:LX/0tX;

    .line 2307817
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 2307806
    iget-object v0, p0, LX/FzJ;->a:LX/9kE;

    invoke-virtual {v0}, LX/9kE;->c()V

    .line 2307807
    return-void
.end method

.method public final a(LX/0gW;LX/0TF;LX/FzH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW",
            "<TModelType;>;",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLInterfaces$TypeaheadResultPage$;",
            ">;>;",
            "LX/FzH",
            "<TModelType;>;)V"
        }
    .end annotation

    .prologue
    .line 2307812
    sget-object v0, LX/0zT;->a:LX/0zT;

    invoke-virtual {p0, p1, p2, p3, v0}, LX/FzJ;->a(LX/0gW;LX/0TF;LX/FzH;LX/0zT;)V

    .line 2307813
    return-void
.end method

.method public final a(LX/0gW;LX/0TF;LX/FzH;LX/0zT;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW",
            "<TModelType;>;",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLInterfaces$TypeaheadResultPage$;",
            ">;>;",
            "LX/FzH",
            "<TModelType;>;",
            "LX/0zT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2307808
    invoke-static {p1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0xe10

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/0zO;->a(LX/0zT;)LX/0zO;

    move-result-object v0

    .line 2307809
    iget-object v1, p0, LX/FzJ;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {v0, p3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2307810
    iget-object v1, p0, LX/FzJ;->a:LX/9kE;

    invoke-virtual {v1, v0, p2}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2307811
    return-void
.end method
