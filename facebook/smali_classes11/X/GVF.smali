.class public LX/GVF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2358762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2358763
    iput-object p1, p0, LX/GVF;->a:Landroid/content/Context;

    .line 2358764
    iput-object p2, p0, LX/GVF;->b:Landroid/content/res/Resources;

    .line 2358765
    return-void
.end method

.method public static a(LX/GVF;Ljava/lang/String;I)LX/47s;
    .locals 4

    .prologue
    .line 2358761
    new-instance v0, LX/47s;

    new-instance v1, LX/GVE;

    const/4 v2, 0x0

    iget-object v3, p0, LX/GVF;->a:Landroid/content/Context;

    invoke-static {v3, p2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/GVE;-><init>(II)V

    const/16 v2, 0x21

    invoke-direct {v0, p1, v1, v2}, LX/47s;-><init>(Ljava/lang/Object;Ljava/lang/Object;I)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/GVF;
    .locals 5

    .prologue
    .line 2358750
    const-class v1, LX/GVF;

    monitor-enter v1

    .line 2358751
    :try_start_0
    sget-object v0, LX/GVF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2358752
    sput-object v2, LX/GVF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2358753
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2358754
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2358755
    new-instance p0, LX/GVF;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4}, LX/GVF;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 2358756
    move-object v0, p0

    .line 2358757
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2358758
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GVF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2358759
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2358760
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/GVF;I)Landroid/text/style/StyleSpan;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2358766
    if-nez p1, :cond_0

    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/GVE;

    iget-object v1, p0, LX/GVF;->a:Landroid/content/Context;

    invoke-static {v1, p1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v2, v1}, LX/GVE;-><init>(II)V

    goto :goto_0
.end method

.method public static b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;
    .locals 4

    .prologue
    .line 2358749
    new-instance v0, LX/47s;

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p2}, LX/GVF;->a(LX/GVF;I)Landroid/text/style/StyleSpan;

    move-result-object v2

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, LX/47s;-><init>(Ljava/lang/Object;Ljava/lang/Object;I)V

    return-object v0
.end method


# virtual methods
.method public final a(ILX/0Px;)Landroid/text/SpannableString;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriendsSharingText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;)",
            "Landroid/text/SpannableString;"
        }
    .end annotation

    .prologue
    .line 2358748
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/GVF;->a(ILX/0Px;I)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILX/0Px;I)Landroid/text/SpannableString;
    .locals 9
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriendsSharingText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<+",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLInterfaces$BackgroundLocationUpsellProfile$;",
            ">;I)",
            "Landroid/text/SpannableString;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2358726
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2358727
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2358728
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    .line 2358729
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-ne p1, v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2358730
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 2358731
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2358732
    iget-object v1, p0, LX/GVF;->b:Landroid/content/res/Resources;

    const v2, 0x7f08128f

    const/4 v0, 0x3

    new-array v3, v0, [LX/47s;

    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    invoke-static {p0, v0, p3}, LX/GVF;->b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    invoke-static {p0, v0, p3}, LX/GVF;->b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;

    move-result-object v0

    aput-object v0, v3, v5

    const/4 v0, 0x2

    add-int/lit8 v4, p1, -0x2

    .line 2358733
    new-instance v5, LX/47s;

    iget-object v6, p0, LX/GVF;->b:Landroid/content/res/Resources;

    const v7, 0x7f0f008e

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v8, p1

    invoke-virtual {v6, v7, v4, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, p3}, LX/GVF;->a(LX/GVF;I)Landroid/text/style/StyleSpan;

    move-result-object v7

    const/16 v8, 0x21

    invoke-direct {v5, v6, v7, v8}, LX/47s;-><init>(Ljava/lang/Object;Ljava/lang/Object;I)V

    move-object v4, v5

    .line 2358734
    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v0

    move-object v0, v0

    .line 2358735
    :goto_3
    return-object v0

    :cond_1
    move v0, v2

    .line 2358736
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2358737
    goto :goto_1

    :cond_3
    move v0, v2

    .line 2358738
    goto :goto_2

    .line 2358739
    :pswitch_0
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2358740
    iget-object v1, p0, LX/GVF;->b:Landroid/content/res/Resources;

    const v2, 0x7f081292    # 1.8087143E38f

    const/4 v3, 0x1

    new-array v3, v3, [LX/47s;

    const/4 v4, 0x0

    invoke-static {p0, v0, p3}, LX/GVF;->b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v1

    move-object v0, v1

    .line 2358741
    goto :goto_3

    .line 2358742
    :pswitch_1
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2358743
    iget-object v2, p0, LX/GVF;->b:Landroid/content/res/Resources;

    const v3, 0x7f081291

    const/4 v4, 0x2

    new-array v4, v4, [LX/47s;

    const/4 v5, 0x0

    invoke-static {p0, v0, p3}, LX/GVF;->b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p0, v1, p3}, LX/GVF;->b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v2

    move-object v0, v2

    .line 2358744
    goto :goto_3

    .line 2358745
    :pswitch_2
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    const/4 v2, 0x2

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2358746
    iget-object v3, p0, LX/GVF;->b:Landroid/content/res/Resources;

    const v4, 0x7f081290

    const/4 v5, 0x3

    new-array v5, v5, [LX/47s;

    const/4 v6, 0x0

    invoke-static {p0, v0, p3}, LX/GVF;->b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p0, v1, p3}, LX/GVF;->b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {p0, v2, p3}, LX/GVF;->b(LX/GVF;Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;I)LX/47s;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, LX/47r;->a(Landroid/content/res/Resources;I[LX/47s;)Landroid/text/SpannableString;

    move-result-object v3

    move-object v0, v3

    .line 2358747
    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
