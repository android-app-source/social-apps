.class public final LX/Fpk;
.super LX/BPU;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2291859
    iput-object p1, p0, LX/Fpk;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-direct {p0, p2}, LX/BPU;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2291860
    check-cast p1, LX/BPT;

    .line 2291861
    iget-object v0, p0, LX/Fpk;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->t()LX/Frd;

    move-result-object v0

    .line 2291862
    if-eqz v0, :cond_0

    .line 2291863
    iget-object v1, p1, LX/BPT;->a:Ljava/lang/String;

    iget-object v2, p1, LX/BPT;->b:Ljava/lang/String;

    iget-object v3, p1, LX/BPT;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/Frd;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2291864
    :cond_0
    iget-object v0, p0, LX/Fpk;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    .line 2291865
    new-instance v2, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    iget-object v1, p1, LX/BPT;->a:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, LX/BPT;->c:Ljava/lang/String;

    sget-object v5, LX/55G;->LOCAL_ONLY:LX/55G;

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/facebook/api/feed/DeleteStoryMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/55G;)V

    .line 2291866
    iget-object v1, v0, Lcom/facebook/timeline/BaseTimelineFragment;->r:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/99v;

    invoke-virtual {v1, v2}, LX/99v;->a(Lcom/facebook/api/feed/DeleteStoryMethod$Params;)V

    .line 2291867
    iget-object v0, p0, LX/Fpk;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lm_()LX/G4x;

    move-result-object v0

    iget-object v1, p1, LX/BPT;->b:Ljava/lang/String;

    iget-object v2, p1, LX/BPT;->a:Ljava/lang/String;

    sget-object v3, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v4, p1, LX/BPT;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    .line 2291868
    iget-object v0, p0, LX/Fpk;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2291869
    return-void
.end method
