.class public final LX/GUT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

.field public final synthetic b:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

.field public final synthetic c:Landroid/support/v4/app/DialogFragment;

.field public final synthetic d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 2357423
    iput-object p1, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iput-object p2, p0, LX/GUT;->a:Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    iput-object p3, p0, LX/GUT;->b:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    iput-object p4, p0, LX/GUT;->c:Landroid/support/v4/app/DialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2357409
    iget-object v0, p0, LX/GUT;->a:Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2357410
    iget-object v1, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, p0, LX/GUT;->a:Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2357411
    iput-boolean v0, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->P:Z

    .line 2357412
    :cond_0
    iget-object v0, p0, LX/GUT;->b:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->d(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2357413
    iget-object v0, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357414
    iput-object v1, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->M:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357415
    :goto_0
    iget-object v0, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, p0, LX/GUT;->b:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357416
    iput-object v1, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357417
    iget-object v0, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->b(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2357418
    iget-object v0, p0, LX/GUT;->c:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2357419
    return-void

    .line 2357420
    :cond_1
    iget-object v0, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    const/4 v1, 0x0

    .line 2357421
    iput-object v1, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->M:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2357422
    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2357404
    iget-object v0, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->b(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2357405
    iget-object v0, p0, LX/GUT;->c:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2357406
    iget-object v0, p0, LX/GUT;->d:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->K:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2357407
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2357408
    invoke-direct {p0}, LX/GUT;->a()V

    return-void
.end method
