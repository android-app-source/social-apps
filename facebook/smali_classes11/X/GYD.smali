.class public final enum LX/GYD;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GYD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GYD;

.field public static final enum CREATE_SHOP_MUTATION:LX/GYD;

.field public static final enum FETCH_ADD_SHOP_FIELDS:LX/GYD;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2364848
    new-instance v0, LX/GYD;

    const-string v1, "FETCH_ADD_SHOP_FIELDS"

    invoke-direct {v0, v1, v2}, LX/GYD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYD;->FETCH_ADD_SHOP_FIELDS:LX/GYD;

    .line 2364849
    new-instance v0, LX/GYD;

    const-string v1, "CREATE_SHOP_MUTATION"

    invoke-direct {v0, v1, v3}, LX/GYD;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYD;->CREATE_SHOP_MUTATION:LX/GYD;

    .line 2364850
    const/4 v0, 0x2

    new-array v0, v0, [LX/GYD;

    sget-object v1, LX/GYD;->FETCH_ADD_SHOP_FIELDS:LX/GYD;

    aput-object v1, v0, v2

    sget-object v1, LX/GYD;->CREATE_SHOP_MUTATION:LX/GYD;

    aput-object v1, v0, v3

    sput-object v0, LX/GYD;->$VALUES:[LX/GYD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2364851
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GYD;
    .locals 1

    .prologue
    .line 2364852
    const-class v0, LX/GYD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GYD;

    return-object v0
.end method

.method public static values()[LX/GYD;
    .locals 1

    .prologue
    .line 2364853
    sget-object v0, LX/GYD;->$VALUES:[LX/GYD;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GYD;

    return-object v0
.end method
