.class public final LX/Gdz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$FetchPaginatedPYMLWithLargeImageItemsQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/Ge0;


# direct methods
.method public constructor <init>(LX/Ge0;I)V
    .locals 0

    .prologue
    .line 2374860
    iput-object p1, p0, LX/Gdz;->b:LX/Ge0;

    iput p2, p0, LX/Gdz;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2374861
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2374862
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2374863
    check-cast v0, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$FetchPaginatedPYMLWithLargeImageItemsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$FetchPaginatedPYMLWithLargeImageItemsQueryModel;->a()Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;

    move-result-object v0

    .line 2374864
    if-nez v0, :cond_1

    .line 2374865
    const/4 v1, 0x0

    .line 2374866
    :goto_0
    move-object v0, v1

    .line 2374867
    iget-object v1, p0, LX/Gdz;->b:LX/Ge0;

    iget-object v1, v1, LX/Ge0;->m:LX/0ad;

    sget-short v2, LX/9Is;->d:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2374868
    iget v1, p0, LX/Gdz;->a:I

    const-class v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2374869
    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2374870
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "infinite_hscroll_fetch"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "inf_hscroll_fetch_offset"

    invoke-virtual {v4, v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "inf_hscroll_fetch_result"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "inf_hscroll_fetch_type"

    invoke-virtual {v4, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "native_newsfeed"

    .line 2374871
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2374872
    move-object v4, v4

    .line 2374873
    move-object v1, v4

    .line 2374874
    iget-object v2, p0, LX/Gdz;->b:LX/Ge0;

    iget-object v2, v2, LX/Ge0;->f:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2374875
    :cond_0
    return-object v0

    .line 2374876
    :cond_1
    new-instance v3, LX/4Yo;

    invoke-direct {v3}, LX/4Yo;-><init>()V

    .line 2374877
    invoke-virtual {v0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2374878
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2374879
    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 2374880
    invoke-virtual {v0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;

    .line 2374881
    if-nez v1, :cond_4

    .line 2374882
    const/4 v5, 0x0

    .line 2374883
    :goto_2
    move-object v1, v5

    .line 2374884
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2374885
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2374886
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2374887
    iput-object v1, v3, LX/4Yo;->b:LX/0Px;

    .line 2374888
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2374889
    if-nez v1, :cond_5

    .line 2374890
    const/4 v4, 0x0

    .line 2374891
    :goto_3
    move-object v1, v4

    .line 2374892
    iput-object v1, v3, LX/4Yo;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2374893
    new-instance v1, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    invoke-direct {v1, v3}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;-><init>(LX/4Yo;)V

    .line 2374894
    move-object v1, v1

    .line 2374895
    goto/16 :goto_0

    .line 2374896
    :cond_4
    new-instance v5, LX/4Yp;

    invoke-direct {v5}, LX/4Yp;-><init>()V

    .line 2374897
    invoke-virtual {v1}, Lcom/facebook/feedplugins/pyml/protocol/FetchPaginatedPYMLWithLargeImageItemsGraphQLModels$StatelessLargeImagePLAsConnectionModel$EdgesModel;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v6

    .line 2374898
    iput-object v6, v5, LX/4Yp;->b:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    .line 2374899
    new-instance v6, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsEdge;

    invoke-direct {v6, v5}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsEdge;-><init>(LX/4Yp;)V

    .line 2374900
    move-object v5, v6

    .line 2374901
    goto :goto_2

    .line 2374902
    :cond_5
    new-instance v4, LX/17L;

    invoke-direct {v4}, LX/17L;-><init>()V

    .line 2374903
    const/4 v5, 0x0

    invoke-virtual {v2, v1, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2374904
    iput-object v5, v4, LX/17L;->c:Ljava/lang/String;

    .line 2374905
    invoke-virtual {v4}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    goto :goto_3
.end method
