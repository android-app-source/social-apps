.class public LX/FRW;
.super LX/6vv;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6vv",
        "<",
        "Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;",
        "Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;",
        "Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;",
        "Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;",
        "LX/FRw;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240449
    invoke-direct {p0}, LX/6vv;-><init>()V

    .line 2240450
    return-void
.end method

.method public static a(LX/0QB;)LX/FRW;
    .locals 3

    .prologue
    .line 2240451
    const-class v1, LX/FRW;

    monitor-enter v1

    .line 2240452
    :try_start_0
    sget-object v0, LX/FRW;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2240453
    sput-object v2, LX/FRW;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2240454
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2240455
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2240456
    new-instance v0, LX/FRW;

    invoke-direct {v0}, LX/FRW;-><init>()V

    .line 2240457
    move-object v0, v0

    .line 2240458
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2240459
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FRW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2240460
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2240461
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 2240462
    check-cast p1, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    .line 2240463
    new-instance v0, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-direct {v0, p1}, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;-><init>(Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 2240464
    check-cast p1, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    check-cast p2, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;

    check-cast p3, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    .line 2240465
    new-instance v0, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;-><init>(Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;LX/0P1;)V

    return-object v0
.end method
