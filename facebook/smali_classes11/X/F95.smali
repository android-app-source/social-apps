.class public final LX/F95;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F96;


# direct methods
.method public constructor <init>(LX/F96;)V
    .locals 0

    .prologue
    .line 2205076
    iput-object p1, p0, LX/F95;->a:LX/F96;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2205077
    instance-of v0, p1, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 2205078
    check-cast p1, Lcom/facebook/fbservice/service/ServiceException;

    .line 2205079
    iget-object v0, p0, LX/F95;->a:LX/F96;

    iget-object v0, v0, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    iget-object v0, v0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;->a:LX/1CX;

    iget-object v1, p0, LX/F95;->a:LX/F96;

    iget-object v1, v1, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v1

    .line 2205080
    iput-object p1, v1, LX/4mn;->f:Lcom/facebook/fbservice/service/ServiceException;

    .line 2205081
    move-object v1, v1

    .line 2205082
    const v2, 0x7f083394

    invoke-virtual {v1, v2}, LX/4mn;->a(I)LX/4mn;

    move-result-object v1

    invoke-virtual {v1}, LX/4mn;->l()LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 2205083
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2205084
    iget-object v0, p0, LX/F95;->a:LX/F96;

    iget-object v0, v0, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, LX/8DG;

    if-eqz v0, :cond_1

    .line 2205085
    const-string v1, "native_name"

    .line 2205086
    iget-object v0, p0, LX/F95;->a:LX/F96;

    iget-object v0, v0, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, LX/8DG;

    invoke-interface {v0, v1}, LX/8DG;->b(Ljava/lang/String;)V

    .line 2205087
    :cond_0
    :goto_0
    return-void

    .line 2205088
    :cond_1
    iget-object v0, p0, LX/F95;->a:LX/F96;

    iget-object v0, v0, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/growth/promotion/NativeNameActivity;

    if-eqz v0, :cond_0

    .line 2205089
    iget-object v0, p0, LX/F95;->a:LX/F96;

    iget-object v0, v0, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method
