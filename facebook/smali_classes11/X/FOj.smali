.class public final LX/FOj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2235724
    const/4 v10, 0x0

    .line 2235725
    const/4 v9, 0x0

    .line 2235726
    const/4 v8, 0x0

    .line 2235727
    const/4 v7, 0x0

    .line 2235728
    const/4 v6, 0x0

    .line 2235729
    const/4 v5, 0x0

    .line 2235730
    const/4 v4, 0x0

    .line 2235731
    const/4 v3, 0x0

    .line 2235732
    const/4 v2, 0x0

    .line 2235733
    const/4 v1, 0x0

    .line 2235734
    const/4 v0, 0x0

    .line 2235735
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 2235736
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2235737
    const/4 v0, 0x0

    .line 2235738
    :goto_0
    return v0

    .line 2235739
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2235740
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 2235741
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2235742
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2235743
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2235744
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2235745
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 2235746
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2235747
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2235748
    :cond_4
    const-string v12, "is_messenger_user"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2235749
    const/4 v0, 0x1

    .line 2235750
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 2235751
    :cond_5
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2235752
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2235753
    :cond_6
    const-string v12, "profile_pic_large"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2235754
    invoke-static {p0, p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2235755
    :cond_7
    const-string v12, "profile_pic_medium"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2235756
    invoke-static {p0, p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2235757
    :cond_8
    const-string v12, "profile_pic_small"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2235758
    invoke-static {p0, p1}, LX/5bv;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2235759
    :cond_9
    const-string v12, "responsiveness_context"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 2235760
    invoke-static {p0, p1}, LX/FQq;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2235761
    :cond_a
    const-string v12, "structured_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 2235762
    invoke-static {p0, p1}, LX/5bs;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 2235763
    :cond_b
    const-string v12, "username"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2235764
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 2235765
    :cond_c
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2235766
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 2235767
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2235768
    if-eqz v0, :cond_d

    .line 2235769
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 2235770
    :cond_d
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2235771
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2235772
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2235773
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2235774
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2235775
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2235776
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2235777
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2235778
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2235779
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2235780
    if-eqz v0, :cond_0

    .line 2235781
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235782
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2235783
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2235784
    if-eqz v0, :cond_1

    .line 2235785
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235786
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2235787
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2235788
    if-eqz v0, :cond_2

    .line 2235789
    const-string v1, "is_messenger_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235790
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2235791
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2235792
    if-eqz v0, :cond_3

    .line 2235793
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235794
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2235795
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2235796
    if-eqz v0, :cond_4

    .line 2235797
    const-string v1, "profile_pic_large"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235798
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 2235799
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2235800
    if-eqz v0, :cond_5

    .line 2235801
    const-string v1, "profile_pic_medium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235802
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 2235803
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2235804
    if-eqz v0, :cond_6

    .line 2235805
    const-string v1, "profile_pic_small"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235806
    invoke-static {p0, v0, p2}, LX/5bv;->a(LX/15i;ILX/0nX;)V

    .line 2235807
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2235808
    if-eqz v0, :cond_7

    .line 2235809
    const-string v1, "responsiveness_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235810
    invoke-static {p0, v0, p2}, LX/FQq;->a(LX/15i;ILX/0nX;)V

    .line 2235811
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2235812
    if-eqz v0, :cond_8

    .line 2235813
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235814
    invoke-static {p0, v0, p2, p3}, LX/5bs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2235815
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2235816
    if-eqz v0, :cond_9

    .line 2235817
    const-string v1, "username"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2235818
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2235819
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2235820
    return-void
.end method
