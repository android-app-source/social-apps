.class public LX/GWv;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/GWw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:LX/GWu;

.field public g:LX/GWu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2362908
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2362909
    const-class v0, LX/GWv;

    invoke-static {v0, p0}, LX/GWv;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2362910
    const v0, 0x7f031037

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2362911
    const v0, 0x7f0d26e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GWv;->b:Landroid/widget/TextView;

    .line 2362912
    const v0, 0x7f0d26e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GWv;->c:Landroid/widget/TextView;

    .line 2362913
    const v0, 0x7f0d26e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GWv;->d:Landroid/widget/TextView;

    .line 2362914
    const v0, 0x7f0d26e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GWv;->e:Landroid/widget/TextView;

    .line 2362915
    new-instance v0, LX/GWu;

    iget-object v2, p0, LX/GWv;->b:Landroid/widget/TextView;

    iget-object v3, p0, LX/GWv;->c:Landroid/widget/TextView;

    const v4, 0x7f0218f9

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/GWu;-><init>(LX/GWv;Landroid/widget/TextView;Landroid/view/View;ILandroid/content/Context;)V

    iput-object v0, p0, LX/GWv;->f:LX/GWu;

    .line 2362916
    new-instance v0, LX/GWu;

    iget-object v2, p0, LX/GWv;->d:Landroid/widget/TextView;

    iget-object v3, p0, LX/GWv;->e:Landroid/widget/TextView;

    const v4, 0x7f02182b

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/GWu;-><init>(LX/GWv;Landroid/widget/TextView;Landroid/view/View;ILandroid/content/Context;)V

    iput-object v0, p0, LX/GWv;->g:LX/GWu;

    .line 2362917
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    check-cast p1, LX/GWv;

    new-instance v0, LX/GWw;

    invoke-direct {v0}, LX/GWw;-><init>()V

    move-object v0, v0

    move-object p0, v0

    check-cast p0, LX/GWw;

    iput-object p0, p1, LX/GWv;->a:LX/GWw;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 2362860
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2362861
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    move-object v2, v1

    .line 2362862
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    .line 2362863
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v1

    iget-object v8, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v8, v1, v6}, LX/15i;->o(II)LX/22e;

    move-result-object v1

    .line 2362864
    if-eqz v1, :cond_2

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_3

    move v1, v6

    :goto_2
    if-eqz v1, :cond_7

    .line 2362865
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v1

    iget-object v8, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v8, v1, v6}, LX/15i;->o(II)LX/22e;

    move-result-object v1

    .line 2362866
    if-eqz v1, :cond_5

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_3
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    move v1, v6

    :goto_4
    if-eqz v1, :cond_c

    .line 2362867
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 2362868
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v1

    iget-object v9, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v9, v1, v6}, LX/15i;->o(II)LX/22e;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v6, v1

    :goto_5
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    :goto_6
    if-ge v7, v9, :cond_9

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2362869
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-eqz v10, :cond_0

    .line 2362870
    sget-object v10, LX/GWw;->a:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2362871
    :cond_0
    const-string v10, "\u2022  "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2362872
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 2362873
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    .line 2362874
    :cond_2
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2362875
    goto :goto_1

    :cond_3
    move v1, v7

    goto :goto_2

    :cond_4
    move v1, v7

    goto :goto_2

    .line 2362876
    :cond_5
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2362877
    goto :goto_3

    :cond_6
    move v1, v7

    goto :goto_4

    :cond_7
    move v1, v7

    goto :goto_4

    .line 2362878
    :cond_8
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2362879
    move-object v6, v1

    goto :goto_5

    .line 2362880
    :cond_9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 2362881
    :goto_7
    new-instance v6, LX/GWx;

    invoke-direct {v6, v2, v1}, LX/GWx;-><init>(LX/0am;LX/0am;)V

    move-object v1, v6

    .line 2362882
    iget-object v0, v1, LX/GWx;->a:LX/0am;

    move-object v0, v0

    .line 2362883
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2362884
    iget-object v0, p0, LX/GWv;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2362885
    iget-object v0, p0, LX/GWv;->b:Landroid/widget/TextView;

    iget-object v2, p0, LX/GWv;->f:LX/GWu;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2362886
    iget-object v0, p0, LX/GWv;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2362887
    iget-object v2, p0, LX/GWv;->c:Landroid/widget/TextView;

    .line 2362888
    iget-object v0, v1, LX/GWx;->a:LX/0am;

    move-object v0, v0

    .line 2362889
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2362890
    :goto_8
    iget-object v0, v1, LX/GWx;->b:LX/0am;

    move-object v0, v0

    .line 2362891
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2362892
    iget-object v0, p0, LX/GWv;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2362893
    iget-object v0, p0, LX/GWv;->d:Landroid/widget/TextView;

    iget-object v2, p0, LX/GWv;->g:LX/GWu;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2362894
    iget-object v2, p0, LX/GWv;->e:Landroid/widget/TextView;

    .line 2362895
    iget-object v0, v1, LX/GWx;->b:LX/0am;

    move-object v0, v0

    .line 2362896
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2362897
    :goto_9
    iget-object v0, p0, LX/GWv;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2362898
    iget-object v0, p0, LX/GWv;->g:LX/GWu;

    invoke-virtual {v0}, LX/GWu;->a()V

    .line 2362899
    iget-object v0, p0, LX/GWv;->f:LX/GWu;

    invoke-virtual {v0}, LX/GWu;->a()V

    .line 2362900
    return-void

    .line 2362901
    :cond_a
    iget-object v0, p0, LX/GWv;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2362902
    iget-object v0, p0, LX/GWv;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2362903
    iget-object v0, p0, LX/GWv;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    .line 2362904
    :cond_b
    iget-object v0, p0, LX/GWv;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2362905
    iget-object v0, p0, LX/GWv;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2362906
    iget-object v0, p0, LX/GWv;->e:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 2362907
    :cond_c
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    goto/16 :goto_7
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2362859
    check-cast p1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-virtual {p0, p1}, LX/GWv;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    return-void
.end method
