.class public LX/GpI;
.super LX/Goq;
.source ""

# interfaces
.implements LX/GoG;
.implements LX/GoX;
.implements LX/Gob;
.implements LX/Cly;


# instance fields
.field private final a:LX/8Z4;

.field private final b:LX/Clb;

.field public final c:Z

.field private final d:LX/GoE;

.field private final e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;


# direct methods
.method public constructor <init>(LX/CHZ;IZ)V
    .locals 3

    .prologue
    .line 2394844
    invoke-interface {p1}, LX/CHY;->c()LX/CHa;

    move-result-object v0

    const/16 v1, 0x70

    invoke-direct {p0, v0, v1, p2}, LX/Goq;-><init>(LX/CHa;II)V

    .line 2394845
    invoke-interface {p1}, LX/CHY;->d()LX/8Z4;

    move-result-object v0

    iput-object v0, p0, LX/GpI;->a:LX/8Z4;

    .line 2394846
    iget-object v0, p0, LX/GpI;->a:LX/8Z4;

    invoke-interface {v0}, LX/8Z4;->a()Lcom/facebook/graphql/enums/GraphQLComposedBlockType;

    move-result-object v0

    invoke-static {v0}, LX/Clb;->from(Lcom/facebook/graphql/enums/GraphQLComposedBlockType;)LX/Clb;

    move-result-object v0

    iput-object v0, p0, LX/GpI;->b:LX/Clb;

    .line 2394847
    iput-boolean p3, p0, LX/GpI;->c:Z

    .line 2394848
    invoke-interface {p1}, LX/CHY;->c()LX/CHa;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2394849
    invoke-interface {p1}, LX/CHY;->c()LX/CHa;

    move-result-object v0

    invoke-interface {v0}, LX/CHa;->jv_()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v0

    iput-object v0, p0, LX/GpI;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    .line 2394850
    :goto_0
    new-instance v0, LX/GoE;

    invoke-interface {p1}, LX/CHY;->jt_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/CHY;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/GpI;->d:LX/GoE;

    .line 2394851
    return-void

    .line 2394852
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/GpI;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    goto :goto_0
.end method


# virtual methods
.method public final D()LX/GoE;
    .locals 1

    .prologue
    .line 2394853
    iget-object v0, p0, LX/GpI;->d:LX/GoE;

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 2394854
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RICH_TEXT:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public final e()LX/8Z4;
    .locals 1

    .prologue
    .line 2394855
    iget-object v0, p0, LX/GpI;->a:LX/8Z4;

    return-object v0
.end method

.method public final f()LX/Clb;
    .locals 1

    .prologue
    .line 2394856
    iget-object v0, p0, LX/GpI;->b:LX/Clb;

    return-object v0
.end method

.method public final g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394857
    iget-object v0, p0, LX/GpI;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    return-object v0
.end method
