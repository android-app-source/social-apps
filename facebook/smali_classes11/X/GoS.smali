.class public LX/GoS;
.super LX/Clm;
.source ""


# instance fields
.field public i:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/GoE;


# direct methods
.method public constructor <init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;LX/GoE;)V
    .locals 1

    .prologue
    .line 2394494
    invoke-direct {p0, p1, p2}, LX/Clm;-><init>(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;Landroid/content/Context;)V

    .line 2394495
    const-class v0, LX/GoS;

    invoke-static {v0, p0}, LX/GoS;->a(Ljava/lang/Class;LX/02k;)V

    .line 2394496
    iput-object p3, p0, LX/GoS;->j:LX/GoE;

    .line 2394497
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/GoS;

    invoke-static {p0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object p0

    check-cast p0, LX/Go0;

    iput-object p0, p1, LX/GoS;->i:LX/Go0;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2394499
    invoke-super {p0, p1}, LX/Clm;->onClick(Landroid/view/View;)V

    .line 2394500
    iget-object v0, p0, LX/GoS;->i:LX/Go0;

    const-string v1, "instant_shopping_element_click"

    new-instance v2, LX/GoR;

    invoke-direct {v2, p0}, LX/GoR;-><init>(LX/GoS;)V

    invoke-virtual {v0, v1, v2}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2394501
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 0

    .prologue
    .line 2394498
    return-void
.end method
