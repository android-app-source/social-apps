.class public final LX/GGy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEA;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/8wL;

.field public final synthetic e:LX/GCY;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:LX/GH0;


# direct methods
.method public constructor <init>(LX/GH0;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8wL;LX/GCY;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2334629
    iput-object p1, p0, LX/GGy;->g:LX/GH0;

    iput-object p2, p0, LX/GGy;->a:Ljava/lang/String;

    iput-object p3, p0, LX/GGy;->b:Ljava/lang/String;

    iput-object p4, p0, LX/GGy;->c:Ljava/lang/String;

    iput-object p5, p0, LX/GGy;->d:LX/8wL;

    iput-object p6, p0, LX/GGy;->e:LX/GCY;

    iput-object p7, p0, LX/GGy;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 8

    .prologue
    .line 2334630
    iget-object v0, p0, LX/GGy;->g:LX/GH0;

    iget-object v1, p0, LX/GGy;->a:Ljava/lang/String;

    iget-object v2, p0, LX/GGy;->b:Ljava/lang/String;

    iget-object v3, p0, LX/GGy;->c:Ljava/lang/String;

    iget-object v4, p0, LX/GGy;->d:LX/8wL;

    iget-object v5, p0, LX/GGy;->e:LX/GCY;

    iget-object v6, p0, LX/GGy;->f:Ljava/lang/String;

    move-object v7, p1

    .line 2334631
    iput-object v1, v7, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    .line 2334632
    invoke-virtual {v7, v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Ljava/lang/String;)V

    .line 2334633
    invoke-virtual {v7, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c(Ljava/lang/String;)V

    .line 2334634
    iput-object v6, v7, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->o:Ljava/lang/String;

    .line 2334635
    iput-object v4, v7, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    .line 2334636
    iget-object p0, v0, LX/GH0;->b:LX/GG6;

    invoke-virtual {p0, v7}, LX/GG6;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2334637
    invoke-interface {v5, v7}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334638
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2334639
    iget-object v0, p0, LX/GGy;->g:LX/GH0;

    iget-object v0, v0, LX/GH0;->c:LX/2U3;

    const-class v1, LX/GH0;

    const-string v2, "Failed fetching data"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2334640
    iget-object v0, p0, LX/GGy;->e:LX/GCY;

    iget-object v1, p0, LX/GGy;->f:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, LX/GCY;->a(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 2334641
    iget-object v0, p0, LX/GGy;->e:LX/GCY;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334642
    return-void
.end method
