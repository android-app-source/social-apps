.class public LX/FFz;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/1qM;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2218147
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/1qM;
    .locals 3

    .prologue
    .line 2218148
    const-class v1, LX/FFz;

    monitor-enter v1

    .line 2218149
    :try_start_0
    sget-object v0, LX/FFz;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2218150
    sput-object v2, LX/FFz;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2218151
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2218152
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2218153
    invoke-static {v0}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->b(LX/0QB;)Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;

    move-result-object p0

    check-cast p0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;

    .line 2218154
    move-object p0, p0

    .line 2218155
    move-object v0, p0

    .line 2218156
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2218157
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/1qM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2218158
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2218159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2218160
    invoke-static {p0}, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;->b(LX/0QB;)Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/download/MediaDownloadServiceHandler;

    .line 2218161
    move-object v0, v0

    .line 2218162
    return-object v0
.end method
