.class public LX/FV4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FV3;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FV3",
        "<",
        "LX/FVH;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0kL;

.field public final c:LX/FW1;

.field private final d:LX/5up;

.field public final e:LX/FVu;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0kL;LX/FW1;LX/5up;LX/FVu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250314
    iput-object p1, p0, LX/FV4;->a:Landroid/content/res/Resources;

    .line 2250315
    iput-object p2, p0, LX/FV4;->b:LX/0kL;

    .line 2250316
    iput-object p3, p0, LX/FV4;->c:LX/FW1;

    .line 2250317
    iput-object p4, p0, LX/FV4;->d:LX/5up;

    .line 2250318
    iput-object p5, p0, LX/FV4;->e:LX/FVu;

    .line 2250319
    return-void
.end method

.method public static a(LX/0QB;)LX/FV4;
    .locals 9

    .prologue
    .line 2250320
    const-class v1, LX/FV4;

    monitor-enter v1

    .line 2250321
    :try_start_0
    sget-object v0, LX/FV4;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2250322
    sput-object v2, LX/FV4;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2250323
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250324
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2250325
    new-instance v3, LX/FV4;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {v0}, LX/FW1;->a(LX/0QB;)LX/FW1;

    move-result-object v6

    check-cast v6, LX/FW1;

    invoke-static {v0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v7

    check-cast v7, LX/5up;

    invoke-static {v0}, LX/FVu;->a(LX/0QB;)LX/FVu;

    move-result-object v8

    check-cast v8, LX/FVu;

    invoke-direct/range {v3 .. v8}, LX/FV4;-><init>(Landroid/content/res/Resources;LX/0kL;LX/FW1;LX/5up;LX/FVu;)V

    .line 2250326
    move-object v0, v3

    .line 2250327
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2250328
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FV4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2250329
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2250330
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/FVH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2250331
    const-class v0, LX/FVH;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2250332
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Z
    .locals 6

    .prologue
    .line 2250333
    check-cast p1, LX/FVH;

    const/4 v5, 0x1

    .line 2250334
    new-instance v0, LX/FV2;

    invoke-direct {v0, p0, p1}, LX/FV2;-><init>(LX/FV4;LX/FVH;)V

    .line 2250335
    iget-object v1, p0, LX/FV4;->d:LX/5up;

    invoke-interface {p1}, LX/FVE;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "native_saved_dashboard"

    const-string v4, "archive_button"

    invoke-virtual {v1, v2, v3, v4, v0}, LX/5up;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2250336
    invoke-interface {p1}, LX/FVE;->b()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/FVt;

    if-eqz v0, :cond_0

    .line 2250337
    iget-object v1, p0, LX/FV4;->c:LX/FW1;

    new-instance v2, LX/FW2;

    invoke-interface {p1}, LX/FVE;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVt;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-static {v0, v4, v5}, LX/FVu;->a(LX/FVt;Lcom/facebook/graphql/enums/GraphQLSavedState;Z)LX/FVt;

    move-result-object v0

    invoke-direct {v2, v0}, LX/FW2;-><init>(LX/FVt;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2250338
    :cond_0
    return v5
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2250339
    iget-object v0, p0, LX/FV4;->a:Landroid/content/res/Resources;

    const v1, 0x7f081ac9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2250340
    check-cast p1, LX/FVH;

    .line 2250341
    invoke-interface {p1}, LX/FVE;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-interface {p1}, LX/FVH;->f()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2250342
    const-string v0, "archive_button"

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2250343
    const v0, 0x7f0217a0

    return v0
.end method
