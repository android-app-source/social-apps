.class public LX/FRR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/FRM;


# direct methods
.method public constructor <init>(LX/FRM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240395
    iput-object p1, p0, LX/FRR;->a:LX/FRM;

    .line 2240396
    return-void
.end method

.method public static b(LX/0QB;)LX/FRR;
    .locals 2

    .prologue
    .line 2240397
    new-instance v1, LX/FRR;

    invoke-static {p0}, LX/FRM;->b(LX/0QB;)LX/FRM;

    move-result-object v0

    check-cast v0, LX/FRM;

    invoke-direct {v1, v0}, LX/FRR;-><init>(LX/FRM;)V

    .line 2240398
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/history/model/PaymentTransactions;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2240399
    iget-object v0, p0, LX/FRR;->a:LX/FRM;

    invoke-virtual {v0, p1}, LX/6sU;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
