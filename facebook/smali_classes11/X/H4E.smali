.class public final enum LX/H4E;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H4E;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H4E;

.field public static final enum COUNT:LX/H4E;

.field public static final enum CURRENT_LOCATION_CELL:LX/H4E;

.field public static final enum LOCATION_CELL:LX/H4E;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2422432
    new-instance v0, LX/H4E;

    const-string v1, "CURRENT_LOCATION_CELL"

    invoke-direct {v0, v1, v2}, LX/H4E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H4E;->CURRENT_LOCATION_CELL:LX/H4E;

    .line 2422433
    new-instance v0, LX/H4E;

    const-string v1, "LOCATION_CELL"

    invoke-direct {v0, v1, v3}, LX/H4E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H4E;->LOCATION_CELL:LX/H4E;

    .line 2422434
    new-instance v0, LX/H4E;

    const-string v1, "COUNT"

    invoke-direct {v0, v1, v4}, LX/H4E;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H4E;->COUNT:LX/H4E;

    .line 2422435
    const/4 v0, 0x3

    new-array v0, v0, [LX/H4E;

    sget-object v1, LX/H4E;->CURRENT_LOCATION_CELL:LX/H4E;

    aput-object v1, v0, v2

    sget-object v1, LX/H4E;->LOCATION_CELL:LX/H4E;

    aput-object v1, v0, v3

    sget-object v1, LX/H4E;->COUNT:LX/H4E;

    aput-object v1, v0, v4

    sput-object v0, LX/H4E;->$VALUES:[LX/H4E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2422429
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H4E;
    .locals 1

    .prologue
    .line 2422431
    const-class v0, LX/H4E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H4E;

    return-object v0
.end method

.method public static values()[LX/H4E;
    .locals 1

    .prologue
    .line 2422430
    sget-object v0, LX/H4E;->$VALUES:[LX/H4E;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H4E;

    return-object v0
.end method
