.class public LX/Fqo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field private b:Landroid/view/View;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:LX/0ht;

.field public e:Landroid/content/res/Resources;

.field public f:LX/Fqn;

.field public g:LX/Fqn;

.field private h:LX/Fqn;

.field public i:LX/Fqn;

.field public j:LX/EpI;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Landroid/content/Context;LX/Fql;Ljava/lang/Boolean;)V
    .locals 12
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294529
    invoke-virtual/range {p7 .. p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/Fqo;->a:Z

    .line 2294530
    iput-object p1, p0, LX/Fqo;->b:Landroid/view/View;

    .line 2294531
    invoke-virtual/range {p5 .. p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, LX/Fqo;->e:Landroid/content/res/Resources;

    .line 2294532
    move-object/from16 v0, p6

    iput-object v0, p0, LX/Fqo;->d:LX/0ht;

    .line 2294533
    new-instance v1, Landroid/widget/FrameLayout;

    move-object/from16 v0, p5

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2294534
    invoke-static/range {p5 .. p5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0312e2

    invoke-virtual {v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 2294535
    iget-object v2, p0, LX/Fqo;->d:LX/0ht;

    invoke-virtual {v2, v1}, LX/0ht;->d(Landroid/view/View;)V

    .line 2294536
    iget-boolean v1, p0, LX/Fqo;->a:Z

    if-eqz v1, :cond_0

    .line 2294537
    const v1, 0x7f0d245b

    invoke-static {v11, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2294538
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2294539
    const/4 v2, -0x2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2294540
    :cond_0
    const v1, 0x7f0d245c

    invoke-static {v11, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, LX/Fqo;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2294541
    new-instance v1, LX/Fqn;

    const v2, 0x7f0d2c01

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    const v2, 0x7f0d2c02

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    const v2, 0x7f0d2c03

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/resources/ui/FbTextView;

    const/4 v6, 0x0

    const v7, 0x7f08157c

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const v8, 0x7f081584

    :goto_0
    const v9, 0x7f020099

    const v10, 0x7f020098

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, LX/Fqn;-><init>(LX/Fqo;Landroid/view/View;Landroid/view/View;Lcom/facebook/resources/ui/FbTextView;ZIIII)V

    iput-object v1, p0, LX/Fqo;->i:LX/Fqn;

    .line 2294542
    new-instance v1, LX/Fqn;

    const v2, 0x7f0d2bfe

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    const v2, 0x7f0d2bff

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    const v2, 0x7f0d2c00

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/resources/ui/FbTextView;

    const/4 v6, 0x1

    const v7, 0x7f081580

    const v8, 0x7f081585

    const v9, 0x7f020095

    const v10, 0x7f020094

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, LX/Fqn;-><init>(LX/Fqo;Landroid/view/View;Landroid/view/View;Lcom/facebook/resources/ui/FbTextView;ZIIII)V

    iput-object v1, p0, LX/Fqo;->h:LX/Fqn;

    .line 2294543
    new-instance v1, LX/Fqn;

    const v2, 0x7f0d2bfb

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    const v2, 0x7f0d2bfc

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    const v2, 0x7f0d2bfd

    invoke-static {v11, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/resources/ui/FbTextView;

    const/4 v6, 0x1

    const v7, 0x7f08157f

    const v8, 0x7f081587

    const v9, 0x7f020097

    const v10, 0x7f020096

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, LX/Fqn;-><init>(LX/Fqo;Landroid/view/View;Landroid/view/View;Lcom/facebook/resources/ui/FbTextView;ZIIII)V

    iput-object v1, p0, LX/Fqo;->g:LX/Fqn;

    .line 2294544
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2294545
    iget-object v1, p0, LX/Fqo;->g:LX/Fqn;

    invoke-virtual {v1}, LX/Fqn;->a()V

    .line 2294546
    iget-object v1, p0, LX/Fqo;->g:LX/Fqn;

    iput-object v1, p0, LX/Fqo;->f:LX/Fqn;

    .line 2294547
    :goto_1
    return-void

    .line 2294548
    :cond_1
    const v8, 0x7f081583

    goto :goto_0

    .line 2294549
    :cond_2
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2294550
    iget-object v1, p0, LX/Fqo;->h:LX/Fqn;

    invoke-virtual {v1}, LX/Fqn;->a()V

    .line 2294551
    iget-object v1, p0, LX/Fqo;->h:LX/Fqn;

    iput-object v1, p0, LX/Fqo;->f:LX/Fqn;

    goto :goto_1

    .line 2294552
    :cond_3
    iget-object v1, p0, LX/Fqo;->i:LX/Fqn;

    invoke-virtual {v1}, LX/Fqn;->a()V

    .line 2294553
    iget-object v1, p0, LX/Fqo;->i:LX/Fqn;

    iput-object v1, p0, LX/Fqo;->f:LX/Fqn;

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2294554
    iget-object v0, p0, LX/Fqo;->f:LX/Fqn;

    iget-object v1, p0, LX/Fqo;->i:LX/Fqn;

    if-ne v0, v1, :cond_0

    .line 2294555
    iget-object v0, p0, LX/Fqo;->h:LX/Fqn;

    invoke-virtual {v0}, LX/Fqn;->a()V

    .line 2294556
    :cond_0
    return-void
.end method

.method public final a(LX/EpI;)V
    .locals 0

    .prologue
    .line 2294557
    iput-object p1, p0, LX/Fqo;->j:LX/EpI;

    .line 2294558
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2294559
    iget-boolean v0, p0, LX/Fqo;->a:Z

    if-eqz v0, :cond_0

    .line 2294560
    iget-object v0, p0, LX/Fqo;->d:LX/0ht;

    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2294561
    iget-object v0, p0, LX/Fqo;->d:LX/0ht;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ht;->c(Z)V

    .line 2294562
    iget-object v0, p0, LX/Fqo;->d:LX/0ht;

    iget-object v1, p0, LX/Fqo;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2294563
    :goto_0
    return-void

    .line 2294564
    :cond_0
    iget-object v0, p0, LX/Fqo;->d:LX/0ht;

    iget-object v1, p0, LX/Fqo;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    goto :goto_0
.end method
