.class public LX/FyG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/17W;

.field public final b:Landroid/app/Activity;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/17W;Landroid/app/Activity;LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2306429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306430
    iput-object p1, p0, LX/FyG;->a:LX/17W;

    .line 2306431
    iput-object p2, p0, LX/FyG;->b:Landroid/app/Activity;

    .line 2306432
    iput-object p3, p0, LX/FyG;->c:LX/0Uh;

    .line 2306433
    return-void
.end method

.method private static a(LX/FyG;LX/5OG;IILjava/lang/String;J)V
    .locals 3

    .prologue
    .line 2306434
    invoke-virtual {p1, p3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    new-instance v1, LX/FyF;

    invoke-direct {v1, p0, p4, p5, p6}, LX/FyF;-><init>(LX/FyG;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2306435
    return-void
.end method


# virtual methods
.method public final a(JLandroid/view/View;)V
    .locals 9

    .prologue
    .line 2306436
    new-instance v0, LX/6WS;

    iget-object v1, p0, LX/FyG;->b:Landroid/app/Activity;

    invoke-direct {v0, v1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2306437
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 2306438
    iget-object v1, p0, LX/FyG;->c:LX/0Uh;

    const/16 v3, 0x40c

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 2306439
    const v3, 0x7f020892

    const v4, 0x7f08335b

    if-eqz v1, :cond_0

    sget-object v5, LX/0ax;->av:Ljava/lang/String;

    :goto_0
    move-object v1, p0

    move-wide v6, p1

    invoke-static/range {v1 .. v7}, LX/FyG;->a(LX/FyG;LX/5OG;IILjava/lang/String;J)V

    .line 2306440
    const v3, 0x7f0207b5

    const v4, 0x7f08335c

    sget-object v5, LX/0ax;->aw:Ljava/lang/String;

    move-object v1, p0

    move-wide v6, p1

    invoke-static/range {v1 .. v7}, LX/FyG;->a(LX/FyG;LX/5OG;IILjava/lang/String;J)V

    .line 2306441
    const v3, 0x7f02095f

    const v4, 0x7f08335d

    sget-object v5, LX/0ax;->ax:Ljava/lang/String;

    move-object v1, p0

    move-wide v6, p1

    invoke-static/range {v1 .. v7}, LX/FyG;->a(LX/FyG;LX/5OG;IILjava/lang/String;J)V

    .line 2306442
    invoke-virtual {v0, p3}, LX/0ht;->f(Landroid/view/View;)V

    .line 2306443
    return-void

    .line 2306444
    :cond_0
    sget-object v5, LX/0ax;->au:Ljava/lang/String;

    goto :goto_0
.end method
