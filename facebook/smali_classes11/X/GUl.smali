.class public LX/GUl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2357917
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2357918
    iput-object p1, p0, LX/GUl;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2357919
    return-void
.end method

.method public static a(LX/0QB;)LX/GUl;
    .locals 2

    .prologue
    .line 2357908
    new-instance v1, LX/GUl;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {v1, v0}, LX/GUl;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 2357909
    move-object v0, v1

    .line 2357910
    return-object v0
.end method


# virtual methods
.method public final a(LX/GUk;)V
    .locals 3

    .prologue
    .line 2357915
    iget-object v0, p0, LX/GUl;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/GUk;->eventId:I

    iget-object v2, p1, LX/GUk;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2357916
    return-void
.end method

.method public final b(LX/GUk;)V
    .locals 3

    .prologue
    .line 2357913
    iget-object v0, p0, LX/GUl;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/GUk;->eventId:I

    iget-object v2, p1, LX/GUk;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2357914
    return-void
.end method

.method public final c(LX/GUk;)V
    .locals 3

    .prologue
    .line 2357911
    iget-object v0, p0, LX/GUl;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/GUk;->eventId:I

    iget-object v2, p1, LX/GUk;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2357912
    return-void
.end method
