.class public LX/FrT;
.super LX/FrS;
.source ""


# direct methods
.method public constructor <init>(LX/0tX;LX/FsI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2295564
    invoke-direct {p0, p1, p2}, LX/FrS;-><init>(LX/0tX;LX/FsI;)V

    .line 2295565
    return-void
.end method


# virtual methods
.method public final a(LX/0v6;IZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;
    .locals 2

    .prologue
    .line 2295566
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "fetchFirstUnits() method should be used instead of this one for pages."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(ZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;
    .locals 12

    .prologue
    .line 2295567
    sget-object v0, LX/0zS;->c:LX/0zS;

    .line 2295568
    iget-object v2, p0, LX/FrS;->b:LX/FsI;

    .line 2295569
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    .line 2295570
    new-instance v5, LX/5xG;

    invoke-direct {v5}, LX/5xG;-><init>()V

    move-object v5, v5

    .line 2295571
    const-string v6, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "nodeId"

    .line 2295572
    iget-wide v10, p2, LX/G12;->a:J

    move-wide v8, v10

    .line 2295573
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "angora_attachment_cover_image_size"

    iget-object v8, v2, LX/FsI;->a:LX/0sa;

    invoke-virtual {v8}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "reading_attachment_profile_image_width"

    iget-object v8, v2, LX/FsI;->a:LX/0sa;

    invoke-virtual {v8}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "reading_attachment_profile_image_height"

    iget-object v8, v2, LX/FsI;->a:LX/0sa;

    invoke-virtual {v8}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "image_large_aspect_height"

    iget-object v8, v2, LX/FsI;->a:LX/0sa;

    invoke-virtual {v8}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "image_large_aspect_width"

    iget-object v8, v2, LX/FsI;->a:LX/0sa;

    invoke-virtual {v8}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "include_replies_in_total_count"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "default_image_scale"

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    const-string v7, "icon_scale"

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    const-string v6, "timeline_stories"

    const-string v7, "4"

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "action_location"

    sget-object v7, LX/0wD;->TIMELINE:LX/0wD;

    invoke-virtual {v7}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "enable_download"

    iget-object v7, v2, LX/FsI;->l:LX/0tQ;

    invoke-virtual {v7}, LX/0tQ;->c()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v6, "automatic_photo_captioning_enabled"

    iget-object v7, v2, LX/FsI;->j:LX/0sX;

    invoke-virtual {v7}, LX/0sX;->a()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2295574
    iget-object v4, p2, LX/G12;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2295575
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2295576
    const-string v4, "timeline_filter"

    .line 2295577
    iget-object v6, p2, LX/G12;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2295578
    invoke-virtual {v5, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2295579
    :cond_0
    iget-object v4, v2, LX/FsI;->c:LX/0se;

    iget-object v6, v2, LX/FsI;->b:LX/0rq;

    invoke-virtual {v6}, LX/0rq;->c()LX/0wF;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2295580
    iget-object v4, v2, LX/FsI;->e:LX/0tI;

    invoke-virtual {v4, v5}, LX/0tI;->a(LX/0gW;)V

    .line 2295581
    invoke-static {v5}, LX/0wo;->a(LX/0gW;)V

    .line 2295582
    iget-object v4, v2, LX/FsI;->f:LX/0Or;

    if-eqz v4, :cond_1

    iget-object v4, v2, LX/FsI;->f:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    .line 2295583
    const/4 v4, 0x1

    move v4, v4

    .line 2295584
    if-eqz v4, :cond_1

    .line 2295585
    const-string v4, "scrubbing"

    const-string v6, "MPEG_DASH"

    invoke-virtual {v5, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2295586
    :cond_1
    iget-object v4, v2, LX/FsI;->g:LX/0sU;

    const-string v6, "PAGE"

    invoke-virtual {v4, v5, v6}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 2295587
    move-object v2, v5

    .line 2295588
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v3}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    .line 2295589
    iput-object p3, v2, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2295590
    move-object v2, v2

    .line 2295591
    invoke-virtual {v2, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2295592
    iget-object v3, p0, LX/FrS;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2295593
    move-object v1, v2

    .line 2295594
    invoke-static {v1}, LX/FsA;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0zX;

    move-result-object v1

    invoke-static {v1}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v1

    .line 2295595
    const/4 v4, 0x0

    .line 2295596
    new-instance v2, LX/FrU;

    invoke-direct {v2, p0}, LX/FrU;-><init>(LX/FrS;)V

    invoke-virtual {v1, v2}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v3

    .line 2295597
    new-instance v2, LX/FsJ;

    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v6

    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v7

    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v8

    move-object v5, v4

    move-object v9, v4

    move-object v10, v4

    move-object v11, v4

    invoke-direct/range {v2 .. v11}, LX/FsJ;-><init>(LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;)V

    move-object v1, v2

    .line 2295598
    move-object v0, v1

    .line 2295599
    return-object v0
.end method
