.class public LX/Fy4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hZ;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fsr;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/5SB;

.field public final e:LX/BQ1;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/timeline/TimelineFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/2do;

.field public final i:Ljava/util/concurrent/Executor;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Landroid/content/Context;

.field public l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public n:Z


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/5SB;LX/BQ1;Lcom/facebook/timeline/TimelineFragment;LX/0Ot;LX/0Or;LX/0Ot;LX/2do;Ljava/util/concurrent/Executor;Landroid/content/Context;)V
    .locals 1
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/timeline/TimelineFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2hZ;",
            ">;",
            "LX/5SB;",
            "LX/BQ1;",
            "Lcom/facebook/timeline/header/menus/TimelineFriendingClient$ViewCallback;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fsr;",
            ">;",
            "LX/2do;",
            "Ljava/util/concurrent/Executor;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2306295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306296
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fy4;->n:Z

    .line 2306297
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    iput-object v0, p0, LX/Fy4;->a:LX/0Ot;

    .line 2306298
    iput-object p2, p0, LX/Fy4;->b:LX/0Ot;

    .line 2306299
    iput-object p3, p0, LX/Fy4;->d:LX/5SB;

    .line 2306300
    iput-object p5, p0, LX/Fy4;->g:Lcom/facebook/timeline/TimelineFragment;

    .line 2306301
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ1;

    iput-object v0, p0, LX/Fy4;->e:LX/BQ1;

    .line 2306302
    iput-object p6, p0, LX/Fy4;->j:LX/0Ot;

    .line 2306303
    iput-object p9, p0, LX/Fy4;->h:LX/2do;

    .line 2306304
    iput-object p7, p0, LX/Fy4;->f:LX/0Or;

    .line 2306305
    iput-object p8, p0, LX/Fy4;->c:LX/0Ot;

    .line 2306306
    iput-object p10, p0, LX/Fy4;->i:Ljava/util/concurrent/Executor;

    .line 2306307
    iput-object p11, p0, LX/Fy4;->k:Landroid/content/Context;

    .line 2306308
    return-void
.end method

.method public static synthetic a(LX/Fy4;JLX/2h8;LX/2hC;LX/5P2;)Landroid/content/DialogInterface$OnClickListener;
    .locals 8

    .prologue
    .line 2306293
    new-instance v1, LX/Fy3;

    move-object v2, p0

    move-wide v3, p1

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/Fy3;-><init>(LX/Fy4;JLX/2h8;LX/2hC;LX/5P2;)V

    move-object v0, v1

    .line 2306294
    return-object v0
.end method

.method public static a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V
    .locals 3

    .prologue
    .line 2306291
    iget-object v0, p0, LX/Fy4;->h:LX/2do;

    new-instance v1, LX/2f2;

    invoke-direct {v1, p1, p2, p3, p4}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2306292
    return-void
.end method

.method public static a$redex0(LX/Fy4;ZJLcom/google/common/util/concurrent/SettableFuture;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2306286
    if-eqz p1, :cond_0

    .line 2306287
    iget-object v0, p0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SEE_FIRST"

    const-string v3, "PROFILE"

    invoke-virtual {v0, v1, v2, v3}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2306288
    :goto_0
    new-instance v1, LX/Fy1;

    invoke-direct {v1, p0, p4}, LX/Fy1;-><init>(LX/Fy4;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v2, p0, LX/Fy4;->i:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2306289
    return-void

    .line 2306290
    :cond_0
    iget-object v0, p0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "REGULAR_FOLLOW"

    const-string v3, "PROFILE"

    invoke-virtual {v0, v1, v2, v3}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Throwable;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2306278
    if-eqz p0, :cond_0

    instance-of v1, p0, LX/4Ua;

    if-nez v1, :cond_1

    .line 2306279
    :cond_0
    :goto_0
    return v0

    .line 2306280
    :cond_1
    check-cast p0, LX/4Ua;

    .line 2306281
    iget-object v1, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2306282
    if-eqz v1, :cond_0

    .line 2306283
    iget-object v1, p0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2306284
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    const v2, 0x1754ad

    if-ne v1, v2, :cond_0

    .line 2306285
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c(LX/Fy4;)V
    .locals 1

    .prologue
    .line 2306235
    iget-object v0, p0, LX/Fy4;->g:Lcom/facebook/timeline/TimelineFragment;

    if-eqz v0, :cond_0

    .line 2306236
    iget-object v0, p0, LX/Fy4;->g:Lcom/facebook/timeline/TimelineFragment;

    .line 2306237
    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->E()V

    .line 2306238
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/2dj;
    .locals 1

    .prologue
    .line 2306277
    iget-object v0, p0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    return-object v0
.end method

.method public final a(ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2306257
    if-eqz p1, :cond_0

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2306258
    :goto_0
    if-eqz p2, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-eq v4, v5, :cond_1

    .line 2306259
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2306260
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->e:LX/BQ1;

    invoke-virtual {v4, v9}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;)V

    .line 2306261
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->e:LX/BQ1;

    invoke-virtual {v4, v8}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2306262
    invoke-static/range {p0 .. p0}, LX/Fy4;->c(LX/Fy4;)V

    .line 2306263
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->d:LX/5SB;

    invoke-virtual {v4}, LX/5SB;->g()J

    move-result-wide v6

    .line 2306264
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v18

    .line 2306265
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->h:LX/2do;

    new-instance v5, LX/84Z;

    const/4 v10, 0x1

    invoke-direct/range {v5 .. v10}, LX/84Z;-><init>(JLcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2306266
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v4, v8, :cond_3

    .line 2306267
    move-object/from16 v0, p0

    move/from16 v1, p2

    move-object/from16 v2, v18

    invoke-static {v0, v1, v6, v7, v2}, LX/Fy4;->a$redex0(LX/Fy4;ZJLcom/google/common/util/concurrent/SettableFuture;)V

    .line 2306268
    :goto_2
    return-object v18

    .line 2306269
    :cond_0
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0

    .line 2306270
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v4, v5, :cond_2

    if-nez p2, :cond_2

    .line 2306271
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->REGULAR_FOLLOW:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    goto :goto_1

    .line 2306272
    :cond_2
    move-object/from16 v0, p0

    iget-object v9, v0, LX/Fy4;->m:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    goto :goto_1

    .line 2306273
    :cond_3
    if-eqz p1, :cond_4

    .line 2306274
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    const-string v8, "PROFILE"

    invoke-virtual {v4, v5, v8}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2306275
    :goto_3
    new-instance v11, LX/Fy0;

    move-object/from16 v12, p0

    move/from16 v13, p1

    move-object v14, v9

    move/from16 v15, p2

    move-wide/from16 v16, v6

    invoke-direct/range {v11 .. v18}, LX/Fy0;-><init>(LX/Fy4;ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;ZJLcom/google/common/util/concurrent/SettableFuture;)V

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Fy4;->i:Ljava/util/concurrent/Executor;

    invoke-static {v4, v11, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    .line 2306276
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    const-string v8, "PROFILE"

    invoke-virtual {v4, v5, v8}, LX/2dj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto :goto_3
.end method

.method public final a(JLX/2h8;LX/2hC;LX/5P2;)V
    .locals 9

    .prologue
    .line 2306253
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x1

    invoke-static {p0, p1, p2, v0, v1}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306254
    iget-object v0, p0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2dj;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2306255
    new-instance v0, LX/Fxw;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/Fxw;-><init>(LX/Fy4;JLX/2h8;LX/2hC;LX/5P2;)V

    iget-object v1, p0, LX/Fy4;->i:Ljava/util/concurrent/Executor;

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2306256
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2306243
    invoke-static {p1}, LX/Fy4;->b(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2306244
    :cond_0
    :goto_0
    return-void

    .line 2306245
    :cond_1
    iget-object v0, p0, LX/Fy4;->k:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2306246
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2306247
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/Fy4;->k:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2306248
    const v1, 0x7f08158a

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    .line 2306249
    iget-object v1, p0, LX/Fy4;->k:Landroid/content/Context;

    const v2, 0x7f08158b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/Fy4;->e:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2306250
    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2306251
    const v1, 0x7f080016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2306252
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2306239
    iput-boolean p1, p0, LX/Fy4;->n:Z

    .line 2306240
    iget-boolean v0, p0, LX/Fy4;->n:Z

    if-eqz v0, :cond_0

    .line 2306241
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fy4;->g:Lcom/facebook/timeline/TimelineFragment;

    .line 2306242
    :cond_0
    return-void
.end method
