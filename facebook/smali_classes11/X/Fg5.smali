.class public LX/Fg5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/Fg7;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EIz;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/Fg7;LX/0Ot;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/Fg7;",
            "LX/0Ot",
            "<",
            "LX/EIz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2269306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2269307
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/Fg5;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2269308
    iput-object p1, p0, LX/Fg5;->a:Ljava/util/concurrent/ExecutorService;

    .line 2269309
    iput-object p2, p0, LX/Fg5;->b:LX/Fg7;

    .line 2269310
    iput-object p3, p0, LX/Fg5;->c:LX/0Ot;

    .line 2269311
    return-void
.end method
