.class public LX/G3Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/G3O;
.implements LX/G3P;


# instance fields
.field public final a:LX/G3K;

.field public final b:LX/G3c;

.field public final c:LX/G3C;

.field public final d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

.field public final e:LX/G3G;

.field public final f:LX/0tX;

.field public final g:Ljava/util/concurrent/Executor;

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1Mv",
            "<*>;>;"
        }
    .end annotation
.end field

.field private i:LX/G3f;

.field private final j:LX/0ad;


# direct methods
.method public constructor <init>(LX/G3C;LX/G3K;LX/G3c;Lcom/facebook/timeline/refresher/ProfileRefresherView;LX/0tX;Ljava/util/concurrent/Executor;LX/G3G;LX/G3f;LX/0ad;)V
    .locals 1
    .param p1    # LX/G3C;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/G3K;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/G3c;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/timeline/refresher/ProfileRefresherView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2315637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2315638
    iput-object p1, p0, LX/G3Q;->c:LX/G3C;

    .line 2315639
    iput-object p2, p0, LX/G3Q;->a:LX/G3K;

    .line 2315640
    iput-object p3, p0, LX/G3Q;->b:LX/G3c;

    .line 2315641
    iput-object p4, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315642
    iput-object p5, p0, LX/G3Q;->f:LX/0tX;

    .line 2315643
    iput-object p6, p0, LX/G3Q;->g:Ljava/util/concurrent/Executor;

    .line 2315644
    iput-object p7, p0, LX/G3Q;->e:LX/G3G;

    .line 2315645
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/G3Q;->h:Ljava/util/List;

    .line 2315646
    iput-object p8, p0, LX/G3Q;->i:LX/G3f;

    .line 2315647
    iput-object p9, p0, LX/G3Q;->j:LX/0ad;

    .line 2315648
    return-void
.end method

.method private a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const v5, -0x7f5f8fc3

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2315617
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    .line 2315618
    iget-boolean v3, v2, LX/G3c;->j:Z

    move v2, v3

    .line 2315619
    if-eqz v2, :cond_8

    .line 2315620
    check-cast p1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;

    invoke-virtual {p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineRefresherQueryModel;->a()Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;

    move-result-object v3

    .line 2315621
    if-nez v3, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_9

    .line 2315622
    :cond_0
    :goto_3
    return v0

    .line 2315623
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2315624
    if-nez v2, :cond_2

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_0

    .line 2315625
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2315626
    if-nez v2, :cond_4

    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_1

    .line 2315627
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$ProfileWizardRefresherFieldsModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v3, v2, v0, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2315628
    if-eqz v2, :cond_6

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_4
    if-nez v2, :cond_7

    move v2, v0

    goto :goto_2

    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_4

    :cond_7
    move v2, v1

    goto :goto_2

    .line 2315629
    :cond_8
    check-cast p1, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;

    invoke-virtual {p1}, Lcom/facebook/timeline/refresher/protocol/FetchProfileRefresherGraphQLModels$TimelineNuxQueryModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v4, v2, LX/1vs;->b:I

    .line 2315630
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2

    .line 2315631
    if-nez v4, :cond_a

    move v2, v0

    :goto_5
    if-eqz v2, :cond_c

    move v2, v0

    :goto_6
    if-eqz v2, :cond_e

    move v2, v0

    :goto_7
    if-nez v2, :cond_0

    :cond_9
    move v0, v1

    .line 2315632
    goto :goto_3

    .line 2315633
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2315634
    :cond_a
    invoke-virtual {v3, v4, v1}, LX/15i;->g(II)I

    move-result v2

    if-nez v2, :cond_b

    move v2, v0

    goto :goto_5

    :cond_b
    move v2, v1

    goto :goto_5

    :cond_c
    invoke-virtual {v3, v4, v0}, LX/15i;->g(II)I

    move-result v2

    if-nez v2, :cond_d

    move v2, v0

    goto :goto_6

    :cond_d
    move v2, v1

    goto :goto_6

    .line 2315635
    :cond_e
    invoke-virtual {v3, v4, v0}, LX/15i;->g(II)I

    move-result v2

    invoke-static {v3, v2, v0, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2315636
    if-eqz v2, :cond_f

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_8
    if-nez v2, :cond_10

    move v2, v0

    goto :goto_7

    :cond_f
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_8

    :cond_10
    move v2, v1

    goto :goto_7
.end method

.method public static c(LX/G3Q;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2315585
    if-eqz p1, :cond_0

    .line 2315586
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315587
    if-nez v0, :cond_1

    .line 2315588
    :cond_0
    :goto_0
    return-void

    .line 2315589
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315590
    invoke-direct {p0, v0}, LX/G3Q;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2315591
    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v1, v0}, LX/G3c;->a(Ljava/lang/Object;)V

    .line 2315592
    invoke-direct {p0}, LX/G3Q;->n()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, LX/G3Q;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2315593
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->a()V

    .line 2315594
    :cond_2
    :goto_1
    iget-object v0, p0, LX/G3Q;->c:LX/G3C;

    invoke-interface {v0}, LX/G3C;->n()V

    .line 2315595
    iget-object v0, p0, LX/G3Q;->a:LX/G3K;

    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    .line 2315596
    iget v2, v1, LX/G3c;->m:I

    move v2, v2

    .line 2315597
    const/4 p1, 0x2

    if-ge v2, p1, :cond_7

    .line 2315598
    :goto_2
    iget-object v0, p0, LX/G3Q;->a:LX/G3K;

    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0, v1}, LX/G3K;->b(LX/G3c;)V

    .line 2315599
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315600
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315601
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->c(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2315602
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315603
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315604
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    .line 2315605
    iput-object p0, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->o:LX/G3O;

    .line 2315606
    :cond_3
    invoke-static {p0}, LX/G3Q;->r(LX/G3Q;)V

    goto :goto_0

    .line 2315607
    :cond_4
    invoke-direct {p0}, LX/G3Q;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, LX/G3Q;->o()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2315608
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->b()V

    goto :goto_1

    .line 2315609
    :cond_5
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v1, :cond_6

    .line 2315610
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->c()V

    goto :goto_1

    .line 2315611
    :cond_6
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v1, :cond_2

    .line 2315612
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherView;->c()V

    goto :goto_1

    .line 2315613
    :cond_7
    iget-object v2, v0, LX/G3K;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315614
    iget-object p1, v2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->e:Landroid/view/ViewStub;

    move-object v2, p1

    .line 2315615
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    iput-object v2, v0, LX/G3K;->f:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    .line 2315616
    iget-object v2, v0, LX/G3K;->f:Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Lcom/facebook/timeline/refresher/ui/ProfileRefresherStepProgressBar;->setVisibility(I)V

    goto :goto_2
.end method

.method public static m(LX/G3Q;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2315492
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2315493
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2315494
    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    .line 2315495
    iget-boolean v2, v1, LX/G3c;->j:Z

    move v1, v2

    .line 2315496
    if-nez v1, :cond_1

    .line 2315497
    iget-object v1, p0, LX/G3Q;->j:LX/0ad;

    sget-short v2, LX/0wf;->ay:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2315498
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2315499
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2315500
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2315501
    :goto_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 2315502
    :cond_1
    iget-object v1, p0, LX/G3Q;->j:LX/0ad;

    sget-short v2, LX/0wf;->aA:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2315503
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2315504
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2315505
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 2315584
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Z
    .locals 3

    .prologue
    .line 2315583
    iget-object v0, p0, LX/G3Q;->j:LX/0ad;

    sget-short v1, LX/0wf;->aC:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static q(LX/G3Q;)V
    .locals 2

    .prologue
    .line 2315579
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->h()V

    .line 2315580
    iget-object v0, p0, LX/G3Q;->a:LX/G3K;

    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0, v1}, LX/G3K;->b(LX/G3c;)V

    .line 2315581
    invoke-static {p0}, LX/G3Q;->r(LX/G3Q;)V

    .line 2315582
    return-void
.end method

.method public static r(LX/G3Q;)V
    .locals 4

    .prologue
    .line 2315649
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2315650
    iget-object v1, p0, LX/G3Q;->e:LX/G3G;

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315651
    iget-boolean v2, v0, LX/G3c;->j:Z

    move v0, v2

    .line 2315652
    if-eqz v0, :cond_1

    const-string v0, "profile_refresher"

    :goto_0
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->f()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v3

    .line 2315653
    const-string p0, "profile_wizard_step_view"

    invoke-static {v1, v0, v2, v3, p0}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2315654
    :cond_0
    return-void

    .line 2315655
    :cond_1
    const-string v0, "profile_nux"

    goto :goto_0
.end method


# virtual methods
.method public final d()V
    .locals 6

    .prologue
    .line 2315552
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2315553
    iget-object v1, p0, LX/G3Q;->e:LX/G3G;

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315554
    iget-boolean v2, v0, LX/G3c;->j:Z

    move v0, v2

    .line 2315555
    if-eqz v0, :cond_1

    const-string v0, "profile_refresher"

    :goto_0
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->f()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v3

    .line 2315556
    const-string v4, "profile_wizard_skip_button_tap"

    invoke-static {v1, v0, v2, v3, v4}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2315557
    :cond_0
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315558
    iget v1, v0, LX/G3c;->l:I

    move v0, v1

    .line 2315559
    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    .line 2315560
    iget v2, v1, LX/G3c;->m:I

    move v1, v2

    .line 2315561
    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    .line 2315562
    iget-object v0, p0, LX/G3Q;->c:LX/G3C;

    invoke-interface {v0}, LX/G3C;->b()V

    .line 2315563
    :goto_1
    return-void

    .line 2315564
    :cond_1
    const-string v0, "profile_nux"

    goto :goto_0

    .line 2315565
    :cond_2
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315566
    iget-boolean v1, v0, LX/G3c;->j:Z

    move v0, v1

    .line 2315567
    if-eqz v0, :cond_3

    .line 2315568
    iget-object v0, p0, LX/G3Q;->i:LX/G3f;

    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    .line 2315569
    iget-object v2, v1, LX/G3c;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2315570
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2315571
    new-instance v3, LX/4Iq;

    invoke-direct {v3}, LX/4Iq;-><init>()V

    .line 2315572
    const-string v4, "actor_id"

    invoke-virtual {v3, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2315573
    const-string v4, "step_type"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2315574
    new-instance v4, LX/G41;

    invoke-direct {v4}, LX/G41;-><init>()V

    move-object v4, v4

    .line 2315575
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2315576
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2315577
    iget-object v4, v0, LX/G3f;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2315578
    :cond_3
    invoke-static {p0}, LX/G3Q;->q(LX/G3Q;)V

    goto :goto_1
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 2315544
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2315545
    iget-object v1, p0, LX/G3Q;->e:LX/G3G;

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315546
    iget-boolean v2, v0, LX/G3c;->j:Z

    move v0, v2

    .line 2315547
    if-eqz v0, :cond_1

    const-string v0, "profile_refresher"

    :goto_0
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->f()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v3

    .line 2315548
    const-string v4, "profile_wizard_cancel_button_tap"

    invoke-static {v1, v0, v2, v3, v4}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2315549
    :cond_0
    iget-object v0, p0, LX/G3Q;->c:LX/G3C;

    invoke-interface {v0}, LX/G3C;->b()V

    .line 2315550
    return-void

    .line 2315551
    :cond_1
    const-string v0, "profile_nux"

    goto :goto_0
.end method

.method public final f()V
    .locals 5

    .prologue
    .line 2315524
    invoke-static {}, LX/0wB;->c()I

    move-result v0

    .line 2315525
    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v1, v2, :cond_1

    .line 2315526
    invoke-static {}, LX/5wK;->b()LX/5wG;

    move-result-object v0

    const-string v1, "profile_id"

    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    .line 2315527
    iget-object v3, v2, LX/G3c;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2315528
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/5wG;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2315529
    iget-object v1, p0, LX/G3Q;->f:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2315530
    new-instance v1, LX/G3M;

    invoke-direct {v1, p0}, LX/G3M;-><init>(LX/G3Q;)V

    .line 2315531
    iget-object v2, p0, LX/G3Q;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2315532
    iget-object v2, p0, LX/G3Q;->h:Ljava/util/List;

    new-instance v3, LX/1Mv;

    invoke-direct {v3, v0, v1}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2315533
    :cond_0
    :goto_0
    return-void

    .line 2315534
    :cond_1
    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v1, v2, :cond_2

    .line 2315535
    invoke-static {}, LX/G3u;->c()LX/G3r;

    move-result-object v1

    const-string v2, "profile_id"

    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    .line 2315536
    iget-object v4, v3, LX/G3c;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2315537
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "cover_photo_size"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/G3r;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2315538
    iget-object v1, p0, LX/G3Q;->f:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2315539
    new-instance v1, LX/G3N;

    invoke-direct {v1, p0}, LX/G3N;-><init>(LX/G3Q;)V

    .line 2315540
    iget-object v2, p0, LX/G3Q;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2315541
    iget-object v2, p0, LX/G3Q;->h:Ljava/util/List;

    new-instance v3, LX/1Mv;

    invoke-direct {v3, v0, v1}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2315542
    :cond_2
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-ne v0, v1, :cond_0

    .line 2315543
    invoke-static {p0}, LX/G3Q;->q(LX/G3Q;)V

    goto :goto_0
.end method

.method public final i()V
    .locals 5

    .prologue
    .line 2315506
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315507
    iget-object v1, p0, LX/G3Q;->e:LX/G3G;

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315508
    iget-boolean v2, v0, LX/G3c;->j:Z

    move v0, v2

    .line 2315509
    if-eqz v0, :cond_2

    const-string v0, "profile_refresher"

    :goto_1
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->f()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v3

    .line 2315510
    const-string v4, "profile_wizard_upload_photo_from_albums_tap"

    invoke-static {v1, v0, v2, v3, v4}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2315511
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2315512
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315513
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315514
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->e()V

    .line 2315515
    :cond_0
    :goto_2
    return-void

    .line 2315516
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2315517
    :cond_2
    const-string v0, "profile_nux"

    goto :goto_1

    .line 2315518
    :cond_3
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315519
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315520
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315521
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    .line 2315522
    invoke-static {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->t(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)LX/FwT;

    move-result-object v1

    invoke-static {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->r(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)Z

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/FwT;->a(ZZ)V

    .line 2315523
    goto :goto_2
.end method

.method public final k()V
    .locals 6

    .prologue
    .line 2315474
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315475
    iget-object v1, p0, LX/G3Q;->e:LX/G3G;

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315476
    iget-boolean v2, v0, LX/G3c;->j:Z

    move v0, v2

    .line 2315477
    if-eqz v0, :cond_1

    const-string v0, "profile_refresher"

    :goto_1
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->f()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/G3G;->f(Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)V

    .line 2315478
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315479
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315480
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    .line 2315481
    iget-boolean v2, v1, LX/G3c;->e:Z

    move v1, v2

    .line 2315482
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    .line 2315483
    iget-boolean v3, v2, LX/G3c;->f:Z

    move v2, v3

    .line 2315484
    iget-object v3, v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    .line 2315485
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class p0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditActivity;

    invoke-direct {v4, v5, p0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2315486
    const-string v5, "show_feed_sharing_switch_extra"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2315487
    const-string v5, "initial_is_feed_sharing_switch_checked"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2315488
    const/16 v5, 0x71d

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object p0

    invoke-interface {v3, v4, v5, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2315489
    return-void

    .line 2315490
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2315491
    :cond_1
    const-string v0, "profile_nux"

    goto :goto_1
.end method

.method public final l()V
    .locals 8

    .prologue
    .line 2315456
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315457
    iget-object v1, p0, LX/G3Q;->e:LX/G3G;

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315458
    iget-boolean v2, v0, LX/G3c;->j:Z

    move v0, v2

    .line 2315459
    if-eqz v0, :cond_1

    const-string v0, "profile_refresher"

    :goto_1
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->f()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/G3G;->f(Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)V

    .line 2315460
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315461
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315462
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;

    iget-object v1, p0, LX/G3Q;->b:LX/G3c;

    .line 2315463
    iget-object v2, v1, LX/G3c;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2315464
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    .line 2315465
    iget-boolean v3, v2, LX/G3c;->g:Z

    move v2, v3

    .line 2315466
    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    .line 2315467
    iget-boolean p0, v3, LX/G3c;->h:Z

    move v3, p0

    .line 2315468
    const/4 p0, 0x0

    .line 2315469
    iget-object v4, v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, LX/0ax;->bX:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v1, v7, p0

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "show_feed_sharing_switch_extra"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "initial_is_feed_sharing_switch_checked"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "should_open_new_timeline_activity_on_save_success"

    invoke-virtual {v4, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    .line 2315470
    iget-object v4, v0, Lcom/facebook/timeline/refresher/ProfileNuxRefresherFeaturedPhotosFragment;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0x71e

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v7

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2315471
    return-void

    .line 2315472
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2315473
    :cond_1
    const-string v0, "profile_nux"

    goto :goto_1
.end method

.method public final li_()V
    .locals 5

    .prologue
    .line 2315438
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315439
    iget-object v1, p0, LX/G3Q;->e:LX/G3G;

    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    .line 2315440
    iget-boolean v2, v0, LX/G3c;->j:Z

    move v0, v2

    .line 2315441
    if-eqz v0, :cond_2

    const-string v0, "profile_refresher"

    :goto_1
    iget-object v2, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v2}, LX/G3c;->f()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v3

    .line 2315442
    const-string v4, "profile_wizard_upload_photo_from_camera_roll_tap"

    invoke-static {v1, v0, v2, v3, v4}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2315443
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2315444
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315445
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315446
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->l()V

    .line 2315447
    :cond_0
    :goto_2
    return-void

    .line 2315448
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2315449
    :cond_2
    const-string v0, "profile_nux"

    goto :goto_1

    .line 2315450
    :cond_3
    iget-object v0, p0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2315451
    iget-object v0, p0, LX/G3Q;->d:Lcom/facebook/timeline/refresher/ProfileRefresherView;

    .line 2315452
    iget-object v1, v0, Lcom/facebook/timeline/refresher/ProfileRefresherView;->m:LX/G3d;

    move-object v0, v1

    .line 2315453
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v0, v1}, LX/G3d;->b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    .line 2315454
    invoke-static {v0}, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->t(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)LX/FwT;

    move-result-object v1

    invoke-virtual {v1}, LX/FwT;->a()V

    .line 2315455
    goto :goto_2
.end method
