.class public final LX/GpJ;
.super LX/Gor;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Gor",
        "<",
        "Lcom/facebook/instantshopping/model/data/InstantShoppingTitleAndDateBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:LX/GoE;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V
    .locals 3

    .prologue
    .line 2394860
    invoke-direct {p0, p2, p3}, LX/Gor;-><init>(II)V

    .line 2394861
    new-instance v0, LX/GoE;

    invoke-interface {p1}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/GpJ;->f:LX/GoE;

    .line 2394862
    return-void
.end method


# virtual methods
.method public final synthetic b()LX/Clr;
    .locals 1

    .prologue
    .line 2394859
    invoke-virtual {p0}, LX/GpJ;->c()LX/GpK;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/GpK;
    .locals 2

    .prologue
    .line 2394858
    new-instance v0, LX/GpK;

    invoke-direct {v0, p0}, LX/GpK;-><init>(LX/GpJ;)V

    return-object v0
.end method
