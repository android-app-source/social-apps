.class public LX/GPG;
.super LX/GP4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GP4",
        "<",
        "LX/GQA;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/6yp;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field private e:Lcom/facebook/resources/ui/FbEditText;

.field private f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(LX/6yp;LX/GQA;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2348541
    invoke-direct {p0, p2, p3, p5, p6}, LX/GP4;-><init>(LX/GQ7;Landroid/content/res/Resources;LX/GOy;LX/GNs;)V

    .line 2348542
    iput-object p1, p0, LX/GPG;->c:LX/6yp;

    .line 2348543
    iput-object p4, p0, LX/GPG;->d:Ljava/util/concurrent/ExecutorService;

    .line 2348544
    return-void
.end method

.method public static b(LX/0QB;)LX/GPG;
    .locals 7

    .prologue
    .line 2348545
    new-instance v0, LX/GPG;

    invoke-static {p0}, LX/6yp;->a(LX/0QB;)LX/6yp;

    move-result-object v1

    check-cast v1, LX/6yp;

    .line 2348546
    new-instance v3, LX/GQA;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-direct {v3, v2}, LX/GQA;-><init>(LX/0SG;)V

    .line 2348547
    move-object v2, v3

    .line 2348548
    check-cast v2, LX/GQA;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v5

    check-cast v5, LX/GOy;

    invoke-static {p0}, LX/GNs;->a(LX/0QB;)LX/GNs;

    move-result-object v6

    check-cast v6, LX/GNs;

    invoke-direct/range {v0 .. v6}, LX/GPG;-><init>(LX/6yp;LX/GQA;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V

    .line 2348549
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2348550
    const v0, 0x7f0d0188

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/GPG;->e:Lcom/facebook/resources/ui/FbEditText;

    .line 2348551
    const v0, 0x7f0d0189

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/GPG;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2348552
    iget-object v0, p0, LX/GP4;->f:LX/GQ7;

    move-object v0, v0

    .line 2348553
    check-cast v0, LX/GQA;

    new-instance v1, LX/GPD;

    invoke-direct {v1, p0}, LX/GPD;-><init>(LX/GPG;)V

    .line 2348554
    iput-object v1, v0, LX/GQ7;->a:LX/GP0;

    .line 2348555
    iget-object v0, p0, LX/GPG;->e:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/GPE;

    invoke-direct {v1, p0}, LX/GPE;-><init>(LX/GPG;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2348556
    iget-object v0, p0, LX/GPG;->e:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, LX/GPG;->c:LX/6yp;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2348557
    iget-object v0, p0, LX/GPG;->e:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/GPF;

    invoke-direct {v1, p0}, LX/GPF;-><init>(LX/GPG;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2348558
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2348559
    iget-object v0, p0, LX/GP4;->f:LX/GQ7;

    move-object v0, v0

    .line 2348560
    check-cast v0, LX/GQA;

    iget-object v1, p0, LX/GPG;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GQA;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348561
    const-string v0, "expiration_date"

    return-object v0
.end method

.method public final c()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2348562
    iget-object v0, p0, LX/GPG;->e:Lcom/facebook/resources/ui/FbEditText;

    return-object v0
.end method

.method public final d()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 2348563
    iget-object v0, p0, LX/GPG;->f:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method
