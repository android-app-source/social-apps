.class public LX/FOa;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0PH;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/0Sh;

.field private final d:LX/3fr;

.field public final e:LX/3Lx;

.field private final f:LX/3MV;

.field public final g:LX/0tX;

.field public final h:LX/3MX;

.field private final i:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2235244
    const-class v0, LX/FOa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FOa;->a:Ljava/lang/String;

    .line 2235245
    sget-object v0, LX/0PH;->PHONE_VERIFIED:LX/0PH;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, LX/FOa;->b:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/3fr;LX/3Lx;LX/3MV;LX/0tX;LX/3MX;LX/2RQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2235246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235247
    iput-object p1, p0, LX/FOa;->c:LX/0Sh;

    .line 2235248
    iput-object p2, p0, LX/FOa;->d:LX/3fr;

    .line 2235249
    iput-object p3, p0, LX/FOa;->e:LX/3Lx;

    .line 2235250
    iput-object p4, p0, LX/FOa;->f:LX/3MV;

    .line 2235251
    iput-object p5, p0, LX/FOa;->g:LX/0tX;

    .line 2235252
    iput-object p6, p0, LX/FOa;->h:LX/3MX;

    .line 2235253
    iput-object p7, p0, LX/FOa;->i:LX/2RQ;

    .line 2235254
    return-void
.end method

.method public static b(LX/0QB;)LX/FOa;
    .locals 8

    .prologue
    .line 2235255
    new-instance v0, LX/FOa;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v2

    check-cast v2, LX/3fr;

    invoke-static {p0}, LX/3Lx;->b(LX/0QB;)LX/3Lx;

    move-result-object v3

    check-cast v3, LX/3Lx;

    invoke-static {p0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v4

    check-cast v4, LX/3MV;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/3MX;->a(LX/0QB;)LX/3MX;

    move-result-object v6

    check-cast v6, LX/3MX;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v7

    check-cast v7, LX/2RQ;

    invoke-direct/range {v0 .. v7}, LX/FOa;-><init>(LX/0Sh;LX/3fr;LX/3Lx;LX/3MV;LX/0tX;LX/3MX;LX/2RQ;)V

    .line 2235256
    return-object v0
.end method

.method public static b(LX/FOa;LX/FOb;)LX/FOe;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2235257
    iget-object v0, p1, LX/FOb;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2235258
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 2235259
    :goto_0
    return-object v0

    .line 2235260
    :cond_0
    iget-object v0, p0, LX/FOa;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 2235261
    iget-object v0, p0, LX/FOa;->e:LX/3Lx;

    .line 2235262
    iget-object v1, p1, LX/FOb;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2235263
    invoke-virtual {v0, v1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2235264
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    .line 2235265
    goto :goto_0

    .line 2235266
    :cond_1
    iget-object v0, p0, LX/FOa;->i:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    sget-object v1, LX/2RU;->USER:LX/2RU;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 2235267
    iput-object v1, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 2235268
    move-object v0, v0

    .line 2235269
    iput-object v4, v0, LX/2RR;->e:Ljava/lang/String;

    .line 2235270
    move-object v0, v0

    .line 2235271
    iput-boolean v3, v0, LX/2RR;->f:Z

    .line 2235272
    move-object v0, v0

    .line 2235273
    invoke-virtual {v0}, LX/2RR;->i()LX/2RR;

    move-result-object v0

    .line 2235274
    iput v3, v0, LX/2RR;->p:I

    .line 2235275
    move-object v0, v0

    .line 2235276
    iget-object v1, p0, LX/FOa;->d:LX/3fr;

    sget-object v3, LX/FOa;->b:Ljava/util/Set;

    invoke-virtual {v1, v0, v3}, LX/3fr;->a(LX/2RR;Ljava/util/Set;)LX/6N1;

    move-result-object v5

    .line 2235277
    :try_start_0
    invoke-interface {v5}, LX/6N1;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2235278
    invoke-interface {v5}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2235279
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2235280
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v7, :cond_4

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactPhone;

    .line 2235281
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactPhone;->d()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v4}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactPhone;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2235282
    invoke-static {v0}, LX/6Ok;->a(Lcom/facebook/contacts/graphql/Contact;)Lcom/facebook/user/model/User;

    move-result-object v1

    .line 2235283
    invoke-virtual {v1}, Lcom/facebook/user/model/User;->P()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2235284
    iget-boolean v0, p1, LX/FOb;->b:Z

    move v0, v0

    .line 2235285
    if-nez v0, :cond_4

    .line 2235286
    :cond_2
    new-instance v0, LX/FOe;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/FOe;-><init>(Lcom/facebook/user/model/User;LX/FQk;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2235287
    invoke-interface {v5}, LX/6N1;->close()V

    goto/16 :goto_0

    .line 2235288
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2235289
    :cond_4
    invoke-interface {v5}, LX/6N1;->close()V

    .line 2235290
    iget-boolean v0, p1, LX/FOb;->b:Z

    move v0, v0

    .line 2235291
    if-eqz v0, :cond_5

    invoke-static {p0, p1}, LX/FOa;->c(LX/FOa;LX/FOb;)LX/FOe;

    move-result-object v0

    goto/16 :goto_0

    .line 2235292
    :catchall_0
    move-exception v0

    invoke-interface {v5}, LX/6N1;->close()V

    throw v0

    :cond_5
    move-object v0, v2

    .line 2235293
    goto/16 :goto_0
.end method

.method private static c(LX/FOa;LX/FOb;)LX/FOe;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2235294
    iget-object v7, p0, LX/FOa;->e:LX/3Lx;

    .line 2235295
    iget-object v8, p1, LX/FOb;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2235296
    invoke-virtual {v7, v8}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2235297
    new-instance v8, LX/4Di;

    invoke-direct {v8}, LX/4Di;-><init>()V

    .line 2235298
    const-string v9, "key"

    invoke-virtual {v8, v9, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2235299
    move-object v8, v8

    .line 2235300
    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    .line 2235301
    const-string v9, "phones"

    invoke-virtual {v8, v9, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2235302
    move-object v7, v8

    .line 2235303
    iget-object v8, p1, LX/FOb;->e:Ljava/lang/String;

    move-object v8, v8

    .line 2235304
    const-string v9, "call_type"

    invoke-virtual {v7, v9, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2235305
    move-object v7, v7

    .line 2235306
    iget-object v8, p0, LX/FOa;->h:LX/3MX;

    sget-object v9, LX/3MY;->SMALL:LX/3MY;

    invoke-virtual {v8, v9}, LX/3MX;->a(LX/3MY;)I

    move-result v8

    .line 2235307
    iget-object v9, p0, LX/FOa;->h:LX/3MX;

    sget-object v10, LX/3MY;->BIG:LX/3MY;

    invoke-virtual {v9, v10}, LX/3MX;->a(LX/3MY;)I

    move-result v9

    .line 2235308
    iget-object v10, p0, LX/FOa;->h:LX/3MX;

    sget-object v11, LX/3MY;->HUGE:LX/3MY;

    invoke-virtual {v10, v11}, LX/3MX;->a(LX/3MY;)I

    move-result v10

    .line 2235309
    new-instance v11, LX/FOf;

    invoke-direct {v11}, LX/FOf;-><init>()V

    move-object v11, v11

    .line 2235310
    const-string v12, "include_full_user_info"

    sget-object v13, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v13}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v11

    const-string v12, "query"

    invoke-virtual {v11, v12, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v7

    const-string v11, "min_relationship"

    .line 2235311
    iget-object v12, p1, LX/FOb;->c:LX/FOZ;

    move-object v12, v12

    .line 2235312
    invoke-virtual {v12}, LX/FOZ;->toMinRelationshipInput()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v11

    const-string v12, "min_page_relationship"

    .line 2235313
    iget-object v7, p1, LX/FOb;->d:LX/FOZ;

    move-object v7, v7

    .line 2235314
    if-nez v7, :cond_2

    const/4 v7, 0x0

    :goto_0
    invoke-virtual {v11, v12, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v11, "max_page_response_time"

    .line 2235315
    iget-object v12, p1, LX/FOb;->f:Ljava/lang/String;

    move-object v12, v12

    .line 2235316
    invoke-virtual {v7, v11, v12}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v11, "profile_pic_small_size"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v11, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v8, "profile_pic_medium_size"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v8, "profile_pic_large_size"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/FOf;

    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    sget-object v8, LX/0zS;->a:LX/0zS;

    invoke-virtual {v7, v8}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-wide/32 v9, 0x15180

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    .line 2235317
    iget-object v8, p0, LX/FOa;->g:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    invoke-static {v7}, LX/0tX;->c(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v0, v7

    .line 2235318
    const v2, -0x68e30e5b

    :try_start_0
    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2235319
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 2235320
    :goto_1
    return-object v0

    .line 2235321
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2235322
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2235323
    :cond_1
    :goto_2
    move-object v0, v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2235324
    goto :goto_1

    .line 2235325
    :catch_0
    move-exception v0

    .line 2235326
    :goto_3
    iget-object v2, p0, LX/FOa;->e:LX/3Lx;

    .line 2235327
    iget-object v3, p1, LX/FOb;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2235328
    invoke-virtual {v2, v3}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2235329
    sget-object v3, LX/FOa;->a:Ljava/lang/String;

    const-string v4, "Failed to look up phone number %s on server"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v1

    .line 2235330
    goto :goto_1

    .line 2235331
    :catch_1
    move-exception v0

    goto :goto_3

    .line 2235332
    :cond_2
    iget-object v7, p1, LX/FOb;->d:LX/FOZ;

    move-object v7, v7

    .line 2235333
    invoke-virtual {v7}, LX/FOZ;->toMinRelationshipInput()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 2235334
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel;

    .line 2235335
    invoke-virtual {v2}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel;->a()Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;

    move-result-object v7

    .line 2235336
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->q()LX/1vs;

    move-result-object v8

    iget v8, v8, LX/1vs;->b:I

    if-eqz v8, :cond_4

    .line 2235337
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->q()LX/1vs;

    move-result-object v3

    iget-object v8, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2235338
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 2235339
    const-class v10, Lcom/facebook/graphql/enums/GraphQLTimespanCategory;

    invoke-virtual {v8, v3, v0, v10, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v10

    if-nez v10, :cond_9

    .line 2235340
    :goto_4
    move-object v3, v9

    .line 2235341
    :cond_4
    new-instance v8, LX/0XI;

    invoke-direct {v8}, LX/0XI;-><init>()V

    .line 2235342
    sget-object v9, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    .line 2235343
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    .line 2235344
    const-string v9, "user"

    .line 2235345
    iput-object v9, v8, LX/0XI;->y:Ljava/lang/String;

    .line 2235346
    :goto_5
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->r()Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;

    move-result-object v9

    if-eqz v9, :cond_6

    .line 2235347
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->r()Lcom/facebook/messaging/graphql/threads/UserInfoModels$NameFieldsModel;

    move-result-object v9

    invoke-static {v9}, LX/3MV;->a(LX/5bb;)Lcom/facebook/user/model/Name;

    move-result-object v9

    .line 2235348
    iput-object v9, v8, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2235349
    :goto_6
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->s()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 2235350
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->s()Ljava/lang/String;

    move-result-object v9

    .line 2235351
    iput-object v9, v8, LX/0XI;->l:Ljava/lang/String;

    .line 2235352
    :cond_5
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->p()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v9

    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->o()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v10

    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->n()Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    move-result-object v11

    invoke-static {v9, v10, v11}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquare;

    move-result-object v9

    .line 2235353
    iput-object v9, v8, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 2235354
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->l()Z

    move-result v7

    .line 2235355
    iput-boolean v7, v8, LX/0XI;->z:Z

    .line 2235356
    invoke-virtual {v2}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2235357
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2235358
    sget-object v7, LX/FOZ;->NONE:LX/FOZ;

    .line 2235359
    :goto_7
    move-object v7, v7

    .line 2235360
    sget-object v2, LX/FOZ;->FRIEND:LX/FOZ;

    if-ne v7, v2, :cond_7

    move v2, v4

    .line 2235361
    :goto_8
    iput-boolean v2, v8, LX/0XI;->F:Z

    .line 2235362
    sget-object v2, LX/FOZ;->FOF:LX/FOZ;

    if-ne v7, v2, :cond_8

    .line 2235363
    :goto_9
    iput-boolean v4, v8, LX/0XI;->G:Z

    .line 2235364
    new-instance v2, LX/FOe;

    invoke-virtual {v8}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v4

    invoke-direct {v2, v4, v3}, LX/FOe;-><init>(Lcom/facebook/user/model/User;LX/FQk;)V

    move-object v3, v2

    goto/16 :goto_2

    .line 2235365
    :sswitch_0
    const-string v9, "user"

    .line 2235366
    iput-object v9, v8, LX/0XI;->y:Ljava/lang/String;

    .line 2235367
    goto :goto_5

    .line 2235368
    :sswitch_1
    const-string v9, "page"

    .line 2235369
    iput-object v9, v8, LX/0XI;->y:Ljava/lang/String;

    .line 2235370
    goto :goto_5

    .line 2235371
    :cond_6
    invoke-virtual {v7}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel$ProfileMatchesModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v9

    .line 2235372
    iput-object v9, v8, LX/0XI;->h:Ljava/lang/String;

    .line 2235373
    goto :goto_6

    :cond_7
    move v2, v5

    .line 2235374
    goto :goto_8

    :cond_8
    move v4, v5

    .line 2235375
    goto :goto_9

    :cond_9
    new-instance v10, LX/FQk;

    const-class v11, Lcom/facebook/graphql/enums/GraphQLTimespanCategory;

    invoke-virtual {v8, v3, v0, v11, v9}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v9

    check-cast v9, Lcom/facebook/graphql/enums/GraphQLTimespanCategory;

    invoke-static {v9}, LX/FQi;->fromGraphQL(Lcom/facebook/graphql/enums/GraphQLTimespanCategory;)LX/FQi;

    move-result-object v9

    const/4 v11, 0x1

    invoke-virtual {v8, v3, v11}, LX/15i;->h(II)Z

    move-result v11

    invoke-direct {v10, v9, v11}, LX/FQk;-><init>(LX/FQi;Z)V

    move-object v9, v10

    goto/16 :goto_4
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2235376
    :cond_a
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v9}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/FOZ;->valueOf(Ljava/lang/String;)LX/FOZ;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v7

    goto :goto_7

    .line 2235377
    :catch_2
    move-exception v7

    .line 2235378
    sget-object v9, LX/FOa;->a:Ljava/lang/String;

    const-string v10, "Unable to parse relationship string %s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v2, v11, v0

    invoke-static {v9, v7, v10, v11}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2235379
    sget-object v7, LX/FOZ;->NONE:LX/FOZ;

    goto :goto_7

    nop

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
    .end sparse-switch
.end method
