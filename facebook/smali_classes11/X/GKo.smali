.class public LX/GKo;
.super LX/GIs;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GIs",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/GIN;LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2341460
    invoke-direct/range {p0 .. p6}, LX/GIs;-><init>(LX/GIN;LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V

    .line 2341461
    return-void
.end method

.method public static d(LX/0QB;)LX/GKo;
    .locals 7

    .prologue
    .line 2341458
    new-instance v0, LX/GKo;

    invoke-static {p0}, LX/GIN;->b(LX/0QB;)LX/GIN;

    move-result-object v1

    check-cast v1, LX/GIN;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v2

    check-cast v2, LX/GG6;

    invoke-static {p0}, LX/GL2;->b(LX/0QB;)LX/GL2;

    move-result-object v3

    check-cast v3, LX/GL2;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v4

    check-cast v4, LX/GKy;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v5

    check-cast v5, LX/2U3;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    invoke-direct/range {v0 .. v6}, LX/GKo;-><init>(LX/GIN;LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V

    .line 2341459
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2341462
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2341457
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2341450
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341451
    invoke-super {p0, p1}, LX/GIs;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2341452
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 1

    .prologue
    .line 2341453
    invoke-super {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2341454
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, LX/GIa;->setLocationSelectorDividerVisibility(I)V

    .line 2341455
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/GIr;->f(Z)V

    .line 2341456
    return-void
.end method
