.class public final enum LX/GZe;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GZe;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GZe;

.field public static final enum COLLECTION_FETCH:LX/GZe;

.field public static final enum STOREFRONT_FETCH:LX/GZe;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2366763
    new-instance v0, LX/GZe;

    const-string v1, "STOREFRONT_FETCH"

    invoke-direct {v0, v1, v2}, LX/GZe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZe;->STOREFRONT_FETCH:LX/GZe;

    .line 2366764
    new-instance v0, LX/GZe;

    const-string v1, "COLLECTION_FETCH"

    invoke-direct {v0, v1, v3}, LX/GZe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GZe;->COLLECTION_FETCH:LX/GZe;

    .line 2366765
    const/4 v0, 0x2

    new-array v0, v0, [LX/GZe;

    sget-object v1, LX/GZe;->STOREFRONT_FETCH:LX/GZe;

    aput-object v1, v0, v2

    sget-object v1, LX/GZe;->COLLECTION_FETCH:LX/GZe;

    aput-object v1, v0, v3

    sput-object v0, LX/GZe;->$VALUES:[LX/GZe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2366766
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GZe;
    .locals 1

    .prologue
    .line 2366767
    const-class v0, LX/GZe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GZe;

    return-object v0
.end method

.method public static values()[LX/GZe;
    .locals 1

    .prologue
    .line 2366768
    sget-object v0, LX/GZe;->$VALUES:[LX/GZe;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GZe;

    return-object v0
.end method
