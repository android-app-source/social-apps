.class public final LX/F7z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F80;


# direct methods
.method public constructor <init>(LX/F80;)V
    .locals 0

    .prologue
    .line 2202813
    iput-object p1, p0, LX/F7z;->a:LX/F80;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2202814
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2202815
    invoke-static {p1}, LX/F80;->b(Lcom/facebook/graphql/executor/GraphQLResult;)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2202816
    if-nez v4, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 2202817
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2202818
    :goto_2
    return-object v0

    .line 2202819
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2202820
    :cond_0
    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2202821
    :cond_2
    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    const-class v5, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;

    invoke-virtual {v3, v0, v2, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2202822
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    goto :goto_1

    .line 2202823
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2202824
    goto :goto_3

    .line 2202825
    :cond_4
    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v1}, LX/15i;->g(II)I

    move-result v0

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2202826
    if-eqz v0, :cond_5

    .line 2202827
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v5, p0, LX/F7z;->a:LX/F80;

    iput-object v3, v5, LX/F80;->a:LX/15i;

    iget-object v5, p0, LX/F7z;->a:LX/F80;

    iput v0, v5, LX/F80;->b:I

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2202828
    :cond_5
    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    const-class v1, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;

    invoke-virtual {v3, v0, v2, v1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 2202829
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2202830
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 2202831
    :cond_6
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2202832
    goto :goto_2
.end method
