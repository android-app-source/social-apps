.class public final LX/HAu;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;Z)V
    .locals 0

    .prologue
    .line 2436119
    iput-object p1, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iput-boolean p2, p0, LX/HAu;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2436120
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->e(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V

    .line 2436121
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2436122
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2436123
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->l:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2436124
    if-eqz p1, :cond_0

    .line 2436125
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436126
    if-eqz v0, :cond_0

    .line 2436127
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436128
    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 2436129
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->e(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V

    .line 2436130
    :cond_1
    :goto_3
    return-void

    .line 2436131
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436132
    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2436133
    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    .line 2436134
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436135
    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2436136
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    .line 2436137
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436138
    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2436139
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v1}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_2

    .line 2436140
    :cond_8
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436141
    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2436142
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v4

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2436143
    iget-boolean v0, p0, LX/HAu;->a:Z

    if-eqz v0, :cond_e

    .line 2436144
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->j:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2436145
    iget-object v5, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    .line 2436146
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436147
    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2436148
    iput-object v0, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->h:Ljava/lang/String;

    .line 2436149
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v3, v4, v2, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2436150
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    const v1, 0x7f083693

    invoke-static {v0, v1}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->a$redex0(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;I)V

    goto/16 :goto_3

    .line 2436151
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2436152
    :cond_9
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2436153
    goto :goto_4

    .line 2436154
    :cond_a
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v3, v4, v2, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    iget-object v5, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    if-eqz v0, :cond_d

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2436155
    :goto_5
    iget-object v6, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->m:Lcom/facebook/pages/common/ui/PagesEmptyView;

    const/16 p1, 0x8

    invoke-virtual {v6, p1}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setVisibility(I)V

    .line 2436156
    iget-object v6, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 p1, 0x0

    invoke-virtual {v6, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2436157
    iget-object v6, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    .line 2436158
    if-eqz v0, :cond_b

    .line 2436159
    iget-object p1, v6, LX/HB7;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 2436160
    iget-object p1, v6, LX/HB7;->c:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2436161
    invoke-virtual {v6}, LX/1OM;->notifyDataSetChanged()V

    .line 2436162
    :cond_b
    iget-object v6, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object p1, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->t:LX/1OX;

    invoke-virtual {v6, p1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2436163
    :goto_6
    invoke-virtual {v3, v4, v1}, LX/15i;->g(II)I

    move-result v0

    iget-object v5, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    invoke-virtual {v3, v0, v1}, LX/15i;->h(II)Z

    move-result v0

    .line 2436164
    iput-boolean v0, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->p:Z

    .line 2436165
    invoke-virtual {v3, v4, v1}, LX/15i;->g(II)I

    move-result v0

    iget-object v1, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2436166
    iput-object v0, v1, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->q:Ljava/lang/String;

    .line 2436167
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->p:Z

    if-nez v0, :cond_c

    .line 2436168
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->t:LX/1OX;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 2436169
    :cond_c
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->r:Z

    if-eqz v0, :cond_1

    .line 2436170
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    .line 2436171
    iput-boolean v2, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->r:Z

    .line 2436172
    goto/16 :goto_3

    .line 2436173
    :cond_d
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2436174
    goto :goto_5

    .line 2436175
    :cond_e
    const-class v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v3, v4, v2, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    iget-object v5, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    if-eqz v0, :cond_10

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2436176
    :goto_7
    iget-object v6, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    .line 2436177
    if-eqz v0, :cond_f

    .line 2436178
    iget-object v5, v6, LX/HB7;->c:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2436179
    invoke-virtual {v6}, LX/1OM;->notifyDataSetChanged()V

    .line 2436180
    :cond_f
    iget-object v0, p0, LX/HAu;->b:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    invoke-virtual {v0, v2}, LX/HB7;->b(Z)V

    goto :goto_6

    .line 2436181
    :cond_10
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2436182
    goto :goto_7
.end method
