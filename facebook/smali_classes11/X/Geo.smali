.class public final LX/Geo;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Gep;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:LX/1f9;

.field public e:LX/2dx;

.field public f:LX/1Pk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic g:LX/Gep;


# direct methods
.method public constructor <init>(LX/Gep;)V
    .locals 1

    .prologue
    .line 2376058
    iput-object p1, p0, LX/Geo;->g:LX/Gep;

    .line 2376059
    move-object v0, p1

    .line 2376060
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2376061
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2376062
    const-string v0, "CSPageYouMayLikeComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2376063
    if-ne p0, p1, :cond_1

    .line 2376064
    :cond_0
    :goto_0
    return v0

    .line 2376065
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2376066
    goto :goto_0

    .line 2376067
    :cond_3
    check-cast p1, LX/Geo;

    .line 2376068
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2376069
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2376070
    if-eq v2, v3, :cond_0

    .line 2376071
    iget-object v2, p0, LX/Geo;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Geo;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    iget-object v3, p1, LX/Geo;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2376072
    goto :goto_0

    .line 2376073
    :cond_5
    iget-object v2, p1, LX/Geo;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    if-nez v2, :cond_4

    .line 2376074
    :cond_6
    iget-object v2, p0, LX/Geo;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Geo;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Geo;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2376075
    goto :goto_0

    .line 2376076
    :cond_8
    iget-object v2, p1, LX/Geo;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2376077
    :cond_9
    iget v2, p0, LX/Geo;->c:I

    iget v3, p1, LX/Geo;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2376078
    goto :goto_0

    .line 2376079
    :cond_a
    iget-object v2, p0, LX/Geo;->d:LX/1f9;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Geo;->d:LX/1f9;

    iget-object v3, p1, LX/Geo;->d:LX/1f9;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2376080
    goto :goto_0

    .line 2376081
    :cond_c
    iget-object v2, p1, LX/Geo;->d:LX/1f9;

    if-nez v2, :cond_b

    .line 2376082
    :cond_d
    iget-object v2, p0, LX/Geo;->e:LX/2dx;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/Geo;->e:LX/2dx;

    iget-object v3, p1, LX/Geo;->e:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 2376083
    goto :goto_0

    .line 2376084
    :cond_f
    iget-object v2, p1, LX/Geo;->e:LX/2dx;

    if-nez v2, :cond_e

    .line 2376085
    :cond_10
    iget-object v2, p0, LX/Geo;->f:LX/1Pk;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/Geo;->f:LX/1Pk;

    iget-object v3, p1, LX/Geo;->f:LX/1Pk;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2376086
    goto :goto_0

    .line 2376087
    :cond_11
    iget-object v2, p1, LX/Geo;->f:LX/1Pk;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
