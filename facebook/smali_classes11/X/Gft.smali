.class public LX/Gft;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gfu;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gft",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Gfu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378115
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2378116
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gft;->b:LX/0Zi;

    .line 2378117
    iput-object p1, p0, LX/Gft;->a:LX/0Ot;

    .line 2378118
    return-void
.end method

.method public static a(LX/0QB;)LX/Gft;
    .locals 4

    .prologue
    .line 2378119
    const-class v1, LX/Gft;

    monitor-enter v1

    .line 2378120
    :try_start_0
    sget-object v0, LX/Gft;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378121
    sput-object v2, LX/Gft;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378122
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378123
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378124
    new-instance v3, LX/Gft;

    const/16 p0, 0x2124

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gft;-><init>(LX/0Ot;)V

    .line 2378125
    move-object v0, v3

    .line 2378126
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378127
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gft;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378128
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2378130
    check-cast p2, LX/Gfs;

    .line 2378131
    iget-object v0, p0, LX/Gft;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gfu;

    iget-object v1, p2, LX/Gfs;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Gfs;->b:LX/1Pn;

    const/4 v4, 0x0

    .line 2378132
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/Gfu;->a:LX/Gfj;

    const/4 v5, 0x0

    .line 2378133
    new-instance p0, LX/Gfi;

    invoke-direct {p0, v4}, LX/Gfi;-><init>(LX/Gfj;)V

    .line 2378134
    iget-object p2, v4, LX/Gfj;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Gfh;

    .line 2378135
    if-nez p2, :cond_0

    .line 2378136
    new-instance p2, LX/Gfh;

    invoke-direct {p2, v4}, LX/Gfh;-><init>(LX/Gfj;)V

    .line 2378137
    :cond_0
    invoke-static {p2, p1, v5, v5, p0}, LX/Gfh;->a$redex0(LX/Gfh;LX/1De;IILX/Gfi;)V

    .line 2378138
    move-object p0, p2

    .line 2378139
    move-object v5, p0

    .line 2378140
    move-object v4, v5

    .line 2378141
    iget-object v5, v4, LX/Gfh;->a:LX/Gfi;

    iput-object v2, v5, LX/Gfi;->b:LX/1Pn;

    .line 2378142
    iget-object v5, v4, LX/Gfh;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2378143
    move-object v4, v4

    .line 2378144
    iget-object v5, v4, LX/Gfh;->a:LX/Gfi;

    iput-object v1, v5, LX/Gfi;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2378145
    iget-object v5, v4, LX/Gfh;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2378146
    move-object v4, v4

    .line 2378147
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/Gfu;->b:LX/Gg1;

    const/4 v5, 0x0

    .line 2378148
    new-instance p0, LX/Gg0;

    invoke-direct {p0, v4}, LX/Gg0;-><init>(LX/Gg1;)V

    .line 2378149
    iget-object p2, v4, LX/Gg1;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Gfz;

    .line 2378150
    if-nez p2, :cond_1

    .line 2378151
    new-instance p2, LX/Gfz;

    invoke-direct {p2, v4}, LX/Gfz;-><init>(LX/Gg1;)V

    .line 2378152
    :cond_1
    invoke-static {p2, p1, v5, v5, p0}, LX/Gfz;->a$redex0(LX/Gfz;LX/1De;IILX/Gg0;)V

    .line 2378153
    move-object p0, p2

    .line 2378154
    move-object v5, p0

    .line 2378155
    move-object v4, v5

    .line 2378156
    iget-object v5, v4, LX/Gfz;->a:LX/Gg0;

    iput-object v1, v5, LX/Gg0;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2378157
    iget-object v5, v4, LX/Gfz;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2378158
    move-object v4, v4

    .line 2378159
    iget-object v5, v4, LX/Gfz;->a:LX/Gg0;

    iput-object v2, v5, LX/Gg0;->a:LX/1Pn;

    .line 2378160
    iget-object v5, v4, LX/Gfz;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2378161
    move-object v4, v4

    .line 2378162
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/Gfu;->c:LX/Gff;

    const/4 v5, 0x0

    .line 2378163
    new-instance p0, LX/Gfe;

    invoke-direct {p0, v4}, LX/Gfe;-><init>(LX/Gff;)V

    .line 2378164
    sget-object p2, LX/Gff;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Gfd;

    .line 2378165
    if-nez p2, :cond_2

    .line 2378166
    new-instance p2, LX/Gfd;

    invoke-direct {p2}, LX/Gfd;-><init>()V

    .line 2378167
    :cond_2
    invoke-static {p2, p1, v5, v5, p0}, LX/Gfd;->a$redex0(LX/Gfd;LX/1De;IILX/Gfe;)V

    .line 2378168
    move-object p0, p2

    .line 2378169
    move-object v5, p0

    .line 2378170
    move-object v4, v5

    .line 2378171
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2378172
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2378173
    invoke-static {}, LX/1dS;->b()V

    .line 2378174
    const/4 v0, 0x0

    return-object v0
.end method
