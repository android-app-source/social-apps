.class public LX/FWh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/11i;

.field public final b:LX/0So;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public d:J

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;


# direct methods
.method public constructor <init>(LX/11i;LX/0So;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2252451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252452
    iput-object v0, p0, LX/FWh;->e:Ljava/lang/Long;

    .line 2252453
    iput-object v0, p0, LX/FWh;->f:Ljava/lang/Long;

    .line 2252454
    iput-object p1, p0, LX/FWh;->a:LX/11i;

    .line 2252455
    iput-object p2, p0, LX/FWh;->b:LX/0So;

    .line 2252456
    iput-object p3, p0, LX/FWh;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2252457
    return-void
.end method

.method public static a(LX/0QB;)LX/FWh;
    .locals 6

    .prologue
    .line 2252440
    const-class v1, LX/FWh;

    monitor-enter v1

    .line 2252441
    :try_start_0
    sget-object v0, LX/FWh;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2252442
    sput-object v2, LX/FWh;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2252443
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2252444
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2252445
    new-instance p0, LX/FWh;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5}, LX/FWh;-><init>(LX/11i;LX/0So;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2252446
    move-object v0, p0

    .line 2252447
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2252448
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FWh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2252449
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2252450
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/FWh;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 2252438
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, LX/FWh;->a(Ljava/lang/String;LX/0P1;J)V

    .line 2252439
    return-void
.end method

.method public static a(LX/FWh;Ljava/lang/String;LX/0P1;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2252434
    invoke-static {p0}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v0

    .line 2252435
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2252436
    const/4 v1, 0x0

    const v2, 0x4322c33d

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2252437
    :cond_0
    return-void
.end method

.method public static a(LX/FWh;ZJ)V
    .locals 4

    .prologue
    .line 2252432
    iget-object v0, p0, LX/FWh;->a:LX/11i;

    sget-object v1, LX/FWg;->a:LX/FWf;

    const-string v2, "EARLY_START"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-interface {v0, v1, v2, p2, p3}, LX/11i;->a(LX/0Pq;LX/0P1;J)LX/11o;

    .line 2252433
    return-void
.end method

.method private a(Ljava/lang/String;LX/0P1;J)V
    .locals 7
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2252428
    invoke-static {p0}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v0

    .line 2252429
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2252430
    const/4 v2, 0x0

    const v6, 0x786f5ea8

    move-object v1, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 2252431
    :cond_0
    return-void
.end method

.method private b(ILX/FWA;)V
    .locals 10

    .prologue
    .line 2252426
    const-string v8, "SAVED_DASH_START_TO_INTERACT"

    const-string v0, "items_drawn"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "items_count"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "freshness"

    invoke-virtual {p2}, LX/FWA;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "has_loaded_before"

    iget-object v7, p0, LX/FWh;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/1vE;->d:LX/0Tn;

    invoke-interface {v7, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v7

    invoke-virtual {v7}, LX/03R;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    invoke-static {p0, v8, v0}, LX/FWh;->a(LX/FWh;Ljava/lang/String;LX/0P1;)V

    .line 2252427
    return-void
.end method

.method public static b(LX/FWh;J)V
    .locals 3

    .prologue
    .line 2252421
    const-string v0, "SAVED_DASH_START_TO_FRESH_ITEM_DRAWN"

    invoke-static {p0, v0, p1, p2}, LX/FWh;->a(LX/FWh;Ljava/lang/String;J)V

    .line 2252422
    const-string v0, "SAVED_DASH_START_TO_CACHE_ITEM_DRAWN"

    invoke-static {p0, v0, p1, p2}, LX/FWh;->a(LX/FWh;Ljava/lang/String;J)V

    .line 2252423
    const-string v1, "SAVED_DASH_START_TO_INTERACT"

    const-string v2, "EARLY_START"

    iget-object v0, p0, LX/FWh;->f:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-direct {p0, v1, v0, p1, p2}, LX/FWh;->a(Ljava/lang/String;LX/0P1;J)V

    .line 2252424
    return-void

    .line 2252425
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/FWh;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2252458
    invoke-static {p0}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v0

    .line 2252459
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2252460
    const v1, -0x5f526ea

    invoke-static {v0, p1, v1}, LX/096;->d(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2252461
    :cond_0
    return-void
.end method

.method public static c(LX/FWh;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2252417
    invoke-static {p0}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v0

    .line 2252418
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2252419
    const v1, -0x38655807

    invoke-static {v0, p1, v1}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2252420
    :cond_0
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2252415
    iget-object v0, p0, LX/FWh;->a:LX/11i;

    sget-object v1, LX/FWg;->a:LX/FWf;

    const-string v2, "RESULT_STATUS"

    invoke-static {v2, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/11i;->b(LX/0Pq;LX/0P1;)V

    .line 2252416
    return-void
.end method

.method public static n(LX/FWh;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<",
            "LX/FWf;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2252414
    iget-object v0, p0, LX/FWh;->a:LX/11i;

    sget-object v1, LX/FWg;->a:LX/FWf;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 2252409
    invoke-static {p0}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v0

    .line 2252410
    if-eqz v0, :cond_0

    const-string v1, "SAVED_DASH_START_TO_CACHE_ITEM_DRAWN"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2252411
    const-string v0, "SAVED_DASH_START_TO_CACHE_ITEM_DRAWN"

    const-string v1, "items_count"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/FWh;->a(LX/FWh;Ljava/lang/String;LX/0P1;)V

    .line 2252412
    sget-object v0, LX/FWA;->FROM_CACHE:LX/FWA;

    invoke-direct {p0, p1, v0}, LX/FWh;->b(ILX/FWA;)V

    .line 2252413
    :cond_0
    return-void
.end method

.method public final a(ILX/FWA;)V
    .locals 12

    .prologue
    .line 2252404
    const-string v0, "SAVED_ON_VIEW_DRAWN"

    const-string v1, "items_count"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "freshness"

    invoke-virtual {p2}, LX/FWA;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    .line 2252405
    invoke-static {p0}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v6

    .line 2252406
    if-eqz v6, :cond_0

    .line 2252407
    iget-object v5, p0, LX/FWh;->b:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v9

    const v11, 0x72ea0003

    move-object v7, v0

    move-object v8, v1

    invoke-static/range {v6 .. v11}, LX/096;->a(LX/11o;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 2252408
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2252393
    const-string v0, "SAVED_EARLY_FETCH_LOAD"

    invoke-static {p0, v0}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2252394
    const-string v0, "SAVED_CACHED_ITEM_LOAD"

    invoke-static {p0, v0}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2252395
    const-string v0, "SAVED_FRESH_ITEM_LOAD"

    invoke-static {p0, v0}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2252396
    const-string v0, "SAVED_DASH_START_TO_CACHE_ITEM_DRAWN"

    invoke-static {p0, v0}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2252397
    const-string v0, "SAVED_DASH_START_TO_FRESH_ITEM_DRAWN"

    invoke-static {p0, v0}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2252398
    const-string v0, "SAVED_DASH_START_TO_INTERACT"

    invoke-static {p0, v0}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2252399
    if-eqz p1, :cond_0

    .line 2252400
    const-string v0, "SUCCESS"

    invoke-direct {p0, v0}, LX/FWh;->d(Ljava/lang/String;)V

    .line 2252401
    iget-object v0, p0, LX/FWh;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1vE;->d:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2252402
    :goto_0
    return-void

    .line 2252403
    :cond_0
    const-string v0, "FAILURE"

    invoke-direct {p0, v0}, LX/FWh;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 2252388
    invoke-static {p0}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v0

    .line 2252389
    if-eqz v0, :cond_0

    const-string v1, "SAVED_DASH_START_TO_FRESH_ITEM_DRAWN"

    invoke-interface {v0, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2252390
    const-string v0, "SAVED_DASH_START_TO_FRESH_ITEM_DRAWN"

    const-string v1, "items_count"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/FWh;->a(LX/FWh;Ljava/lang/String;LX/0P1;)V

    .line 2252391
    sget-object v0, LX/FWA;->FROM_SERVER:LX/FWA;

    invoke-direct {p0, p1, v0}, LX/FWh;->b(ILX/FWA;)V

    .line 2252392
    :cond_0
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2252386
    const-string v0, "SAVED_DASH_START_TO_FRESH_ITEM_DRAWN"

    invoke-static {p0, v0}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2252387
    return-void
.end method
