.class public LX/Fw0;
.super LX/1Cd;
.source ""


# instance fields
.field public a:Z

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J

.field public final d:LX/G0n;

.field public final e:LX/BQ9;

.field public final f:LX/1dr;

.field public final g:LX/G4x;

.field private final h:LX/0SG;

.field public final i:LX/1BX;

.field private final j:LX/1BK;

.field public final k:LX/G2X;

.field public l:LX/9lQ;


# direct methods
.method public constructor <init>(Ljava/lang/Long;LX/G0n;LX/G4x;LX/BQ9;LX/1dr;LX/0SG;LX/1BX;LX/1BK;LX/G2X;)V
    .locals 2
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/G0n;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/G4x;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302452
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 2302453
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fw0;->a:Z

    .line 2302454
    sget-object v0, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    iput-object v0, p0, LX/Fw0;->l:LX/9lQ;

    .line 2302455
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/Fw0;->c:J

    .line 2302456
    iput-object p2, p0, LX/Fw0;->d:LX/G0n;

    .line 2302457
    iput-object p4, p0, LX/Fw0;->e:LX/BQ9;

    .line 2302458
    iput-object p3, p0, LX/Fw0;->g:LX/G4x;

    .line 2302459
    iput-object p5, p0, LX/Fw0;->f:LX/1dr;

    .line 2302460
    iput-object p6, p0, LX/Fw0;->h:LX/0SG;

    .line 2302461
    iput-object p7, p0, LX/Fw0;->i:LX/1BX;

    .line 2302462
    iput-object p8, p0, LX/Fw0;->j:LX/1BK;

    .line 2302463
    iput-object p9, p0, LX/Fw0;->k:LX/G2X;

    .line 2302464
    return-void
.end method

.method public static a(LX/Fw0;Lcom/facebook/timeline/protiles/model/ProtileModel;Z)V
    .locals 3

    .prologue
    .line 2302540
    if-eqz p2, :cond_1

    .line 2302541
    iget-boolean v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->f:Z

    move v0, v0

    .line 2302542
    if-nez v0, :cond_1

    .line 2302543
    iget-object v0, p0, LX/Fw0;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2302544
    iput-wide v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->e:J

    .line 2302545
    :cond_0
    :goto_0
    iput-boolean p2, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->f:Z

    .line 2302546
    return-void

    .line 2302547
    :cond_1
    if-nez p2, :cond_0

    .line 2302548
    iget-boolean v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->f:Z

    move v0, v0

    .line 2302549
    if-eqz v0, :cond_0

    .line 2302550
    invoke-direct {p0, p1}, LX/Fw0;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/timeline/protiles/model/ProtileModel;)V
    .locals 13

    .prologue
    .line 2302514
    iget-object v0, p0, LX/Fw0;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2302515
    iget-wide v11, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->e:J

    move-wide v2, v11

    .line 2302516
    sub-long v8, v0, v2

    .line 2302517
    const-wide/16 v0, 0x64

    cmp-long v0, v8, v0

    if-gez v0, :cond_1

    .line 2302518
    :cond_0
    :goto_0
    return-void

    .line 2302519
    :cond_1
    iget-object v0, p0, LX/Fw0;->k:LX/G2X;

    invoke-virtual {v0, p1}, LX/G2X;->c(Lcom/facebook/timeline/protiles/model/ProtileModel;)Z

    move-result v0

    move v0, v0

    .line 2302520
    if-nez v0, :cond_0

    .line 2302521
    iget-object v1, p0, LX/Fw0;->e:LX/BQ9;

    iget-wide v2, p0, LX/Fw0;->c:J

    iget-object v4, p0, LX/Fw0;->l:LX/9lQ;

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v5

    const/4 v6, 0x0

    .line 2302522
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-eq v0, v7, :cond_4

    .line 2302523
    :cond_2
    move v6, v6

    .line 2302524
    const/4 v7, 0x0

    .line 2302525
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v0

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-eq v0, v10, :cond_6

    .line 2302526
    :cond_3
    move v7, v7

    .line 2302527
    iget-object v0, p0, LX/Fw0;->j:LX/1BK;

    .line 2302528
    iget-object v10, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->a:Ljava/lang/String;

    move-object v10, v10

    .line 2302529
    invoke-virtual {v0, v10}, LX/1BL;->a(Ljava/lang/String;)I

    move-result v10

    invoke-virtual/range {v1 .. v10}, LX/BQ9;->a(JLX/9lQ;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;IIJI)V

    goto :goto_0

    .line 2302530
    :cond_4
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v10, v0

    .line 2302531
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v7, v6

    :goto_1
    if-ge v7, v11, :cond_2

    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2302532
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v12, :cond_5

    .line 2302533
    add-int/lit8 v0, v6, 0x1

    .line 2302534
    :goto_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v0

    goto :goto_1

    :cond_5
    move v0, v6

    goto :goto_2

    .line 2302535
    :cond_6
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v11, v0

    .line 2302536
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v10, v7

    :goto_3
    if-ge v10, v12, :cond_3

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2302537
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->j()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2302538
    add-int/lit8 v0, v7, 0x1

    .line 2302539
    :goto_4
    add-int/lit8 v7, v10, 0x1

    move v10, v7

    move v7, v0

    goto :goto_3

    :cond_7
    move v0, v7

    goto :goto_4
.end method

.method private static c(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2302551
    instance-of v0, p0, LX/1Rk;

    if-eqz v0, :cond_0

    check-cast p0, LX/1Rk;

    iget-object v0, p0, LX/1Rk;->a:LX/1RA;

    .line 2302552
    iget-object p0, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, p0

    .line 2302553
    instance-of v0, v0, Lcom/facebook/timeline/protiles/model/ProtileModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0g8;Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 2302502
    invoke-static {p2}, LX/Fw0;->c(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2302503
    :goto_0
    return-void

    .line 2302504
    :cond_0
    check-cast p2, LX/1Rk;

    iget-object v0, p2, LX/1Rk;->a:LX/1RA;

    .line 2302505
    iget-object p2, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, p2

    .line 2302506
    check-cast v0, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2302507
    invoke-interface {p1}, LX/0g8;->B()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2302508
    iget-boolean v1, v0, Lcom/facebook/timeline/protiles/model/ProtileModel;->g:Z

    move v1, v1

    .line 2302509
    if-eqz v1, :cond_2

    .line 2302510
    :cond_1
    :goto_1
    goto :goto_0

    .line 2302511
    :cond_2
    invoke-interface {p1, p3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v1

    .line 2302512
    iget-object v2, p0, LX/Fw0;->i:LX/1BX;

    iget-object p2, p0, LX/Fw0;->d:LX/G0n;

    invoke-virtual {v2, p2, p1, v1}, LX/1BX;->a(LX/1Qr;LX/0g8;Landroid/view/View;)Z

    move-result v1

    .line 2302513
    invoke-static {p0, v0, v1}, LX/Fw0;->a(LX/Fw0;Lcom/facebook/timeline/protiles/model/ProtileModel;Z)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 2302474
    const/4 v0, 0x0

    .line 2302475
    instance-of v1, p1, LX/1Rk;

    if-eqz v1, :cond_4

    .line 2302476
    check-cast p1, LX/1Rk;

    .line 2302477
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    .line 2302478
    iget-object v1, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v1

    .line 2302479
    :goto_0
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    .line 2302480
    if-eqz p1, :cond_1

    iget v1, p1, LX/1Rk;->b:I

    if-nez v1, :cond_1

    .line 2302481
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2302482
    iget-object v3, p0, LX/Fw0;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v3, :cond_0

    .line 2302483
    iget-object v3, p0, LX/Fw0;->f:LX/1dr;

    iget-object v4, p0, LX/Fw0;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2302484
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2302485
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v5, v6, :cond_5

    .line 2302486
    :cond_0
    :goto_1
    iput-object v0, p0, LX/Fw0;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2302487
    :cond_1
    :goto_2
    return-void

    .line 2302488
    :cond_2
    instance-of v1, v0, LX/G55;

    if-eqz v1, :cond_1

    .line 2302489
    check-cast v0, LX/G55;

    .line 2302490
    iget-boolean v3, p0, LX/Fw0;->a:Z

    if-nez v3, :cond_3

    iget-object v3, p0, LX/Fw0;->g:LX/G4x;

    iget-object v4, v0, LX/G52;->a:Ljava/lang/String;

    .line 2302491
    iget-object v5, v3, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2302492
    const/4 v5, 0x0

    .line 2302493
    :goto_3
    move v3, v5

    .line 2302494
    if-eqz v3, :cond_3

    .line 2302495
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/Fw0;->a:Z

    .line 2302496
    iget-object v3, p0, LX/Fw0;->e:LX/BQ9;

    iget-wide v5, p0, LX/Fw0;->c:J

    iget-object v4, p0, LX/Fw0;->l:LX/9lQ;

    .line 2302497
    iget-object v13, v3, LX/BQ9;->b:LX/0Zb;

    const-string v8, "profile_scroll_to_end"

    const/4 v11, 0x0

    move-object v7, v3

    move-wide v9, v5

    move-object v12, v4

    invoke-static/range {v7 .. v12}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v13, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2302498
    :cond_3
    goto :goto_2

    :cond_4
    move-object v2, v0

    move-object v0, p1

    move-object p1, v2

    goto :goto_0

    .line 2302499
    :cond_5
    iget-object v5, v3, LX/1dr;->c:Landroid/os/Handler;

    new-instance v6, Lcom/facebook/feed/viewstate/UnseenStoryManager$1;

    invoke-direct {v6, v3, v4}, Lcom/facebook/feed/viewstate/UnseenStoryManager$1;-><init>(LX/1dr;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    const-wide/16 v7, 0x1388

    const v9, -0x57e9d557

    invoke-static {v5, v6, v7, v8, v9}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1

    :cond_6
    iget-object v5, v3, LX/G4x;->c:Ljava/util/List;

    iget-object v6, v3, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/G59;

    .line 2302500
    iget-object v6, v5, LX/G59;->f:Ljava/lang/String;

    move-object v5, v6

    .line 2302501
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2302465
    instance-of v0, p1, LX/1Rk;

    if-eqz v0, :cond_2

    .line 2302466
    check-cast p1, LX/1Rk;

    .line 2302467
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    .line 2302468
    iget-object v2, v0, LX/1RA;->b:Ljava/lang/Object;

    move-object v0, v2

    .line 2302469
    :goto_0
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    if-eqz p1, :cond_0

    iget v2, p1, LX/1Rk;->b:I

    if-nez v2, :cond_0

    .line 2302470
    iput-object v1, p0, LX/Fw0;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2302471
    :cond_0
    instance-of v1, v0, Lcom/facebook/timeline/protiles/model/ProtileModel;

    if-eqz v1, :cond_1

    .line 2302472
    check-cast v0, Lcom/facebook/timeline/protiles/model/ProtileModel;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/Fw0;->a(LX/Fw0;Lcom/facebook/timeline/protiles/model/ProtileModel;Z)V

    .line 2302473
    :cond_1
    return-void

    :cond_2
    move-object v0, p1

    move-object p1, v1

    goto :goto_0
.end method
