.class public final enum LX/Gxu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gxu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gxu;

.field public static final enum FINISHED:LX/Gxu;

.field public static final enum NO_SUGGESTIONS:LX/Gxu;

.field public static final enum OTHER_LANGUAGES_CLICKED:LX/Gxu;

.field public static final enum OTHER_LANGUAGES_SELECTED:LX/Gxu;

.field public static final enum STARTED:LX/Gxu;

.field public static final enum SUGGESTIONS_FAILED:LX/Gxu;

.field public static final enum SUGGESTIONS_FETCHED:LX/Gxu;


# instance fields
.field private final mAnalyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2408677
    new-instance v0, LX/Gxu;

    const-string v1, "STARTED"

    const-string v2, "language_switcher_activity_started"

    invoke-direct {v0, v1, v4, v2}, LX/Gxu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxu;->STARTED:LX/Gxu;

    .line 2408678
    new-instance v0, LX/Gxu;

    const-string v1, "SUGGESTIONS_FETCHED"

    const-string v2, "language_switcher_activity_suggestions_fetched"

    invoke-direct {v0, v1, v5, v2}, LX/Gxu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxu;->SUGGESTIONS_FETCHED:LX/Gxu;

    .line 2408679
    new-instance v0, LX/Gxu;

    const-string v1, "NO_SUGGESTIONS"

    const-string v2, "language_switcher_activity_no_suggestions"

    invoke-direct {v0, v1, v6, v2}, LX/Gxu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxu;->NO_SUGGESTIONS:LX/Gxu;

    .line 2408680
    new-instance v0, LX/Gxu;

    const-string v1, "SUGGESTIONS_FAILED"

    const-string v2, "language_switcher_activity_suggestions_failed"

    invoke-direct {v0, v1, v7, v2}, LX/Gxu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxu;->SUGGESTIONS_FAILED:LX/Gxu;

    .line 2408681
    new-instance v0, LX/Gxu;

    const-string v1, "OTHER_LANGUAGES_CLICKED"

    const-string v2, "language_switcher_activity_other_languages_clicked"

    invoke-direct {v0, v1, v8, v2}, LX/Gxu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxu;->OTHER_LANGUAGES_CLICKED:LX/Gxu;

    .line 2408682
    new-instance v0, LX/Gxu;

    const-string v1, "OTHER_LANGUAGES_SELECTED"

    const/4 v2, 0x5

    const-string v3, "language_switcher_activity_other_languages_selected"

    invoke-direct {v0, v1, v2, v3}, LX/Gxu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxu;->OTHER_LANGUAGES_SELECTED:LX/Gxu;

    .line 2408683
    new-instance v0, LX/Gxu;

    const-string v1, "FINISHED"

    const/4 v2, 0x6

    const-string v3, "language_switcher_activity_finished"

    invoke-direct {v0, v1, v2, v3}, LX/Gxu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxu;->FINISHED:LX/Gxu;

    .line 2408684
    const/4 v0, 0x7

    new-array v0, v0, [LX/Gxu;

    sget-object v1, LX/Gxu;->STARTED:LX/Gxu;

    aput-object v1, v0, v4

    sget-object v1, LX/Gxu;->SUGGESTIONS_FETCHED:LX/Gxu;

    aput-object v1, v0, v5

    sget-object v1, LX/Gxu;->NO_SUGGESTIONS:LX/Gxu;

    aput-object v1, v0, v6

    sget-object v1, LX/Gxu;->SUGGESTIONS_FAILED:LX/Gxu;

    aput-object v1, v0, v7

    sget-object v1, LX/Gxu;->OTHER_LANGUAGES_CLICKED:LX/Gxu;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Gxu;->OTHER_LANGUAGES_SELECTED:LX/Gxu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Gxu;->FINISHED:LX/Gxu;

    aput-object v2, v0, v1

    sput-object v0, LX/Gxu;->$VALUES:[LX/Gxu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2408674
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2408675
    iput-object p3, p0, LX/Gxu;->mAnalyticsName:Ljava/lang/String;

    .line 2408676
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gxu;
    .locals 1

    .prologue
    .line 2408673
    const-class v0, LX/Gxu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gxu;

    return-object v0
.end method

.method public static values()[LX/Gxu;
    .locals 1

    .prologue
    .line 2408672
    sget-object v0, LX/Gxu;->$VALUES:[LX/Gxu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gxu;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2408670
    iget-object v0, p0, LX/Gxu;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2408671
    iget-object v0, p0, LX/Gxu;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method
