.class public LX/Fql;
.super LX/0ht;
.source ""


# instance fields
.field public final a:LX/0hB;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294471
    invoke-direct {p0, p1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 2294472
    iput-object p2, p0, LX/Fql;->a:LX/0hB;

    .line 2294473
    return-void
.end method

.method public static b(LX/0QB;)LX/Fql;
    .locals 3

    .prologue
    .line 2294474
    new-instance v2, LX/Fql;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-direct {v2, v0, v1}, LX/Fql;-><init>(Landroid/content/Context;LX/0hB;)V

    .line 2294475
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2294476
    if-eqz p2, :cond_0

    .line 2294477
    invoke-super {p0, p1, p2, p3}, LX/0ht;->a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V

    .line 2294478
    :goto_0
    return-void

    .line 2294479
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x106000b

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/0ht;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2294480
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2294481
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getMeasuredHeight()I

    move-result v1

    .line 2294482
    const v2, 0x7f0e0694

    iput v2, p3, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 2294483
    const/16 v2, 0x51

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iput v2, p3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2294484
    iget-object v0, p0, LX/Fql;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 2294485
    div-int/lit8 v1, v1, 0x2

    .line 2294486
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x15

    if-le v2, p1, :cond_1

    .line 2294487
    iget-object v2, p0, LX/Fql;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->e()I

    move-result v2

    iget-object p1, p0, LX/Fql;->a:LX/0hB;

    invoke-virtual {p1}, LX/0hB;->d()I

    move-result p1

    sub-int/2addr v2, p1

    .line 2294488
    :goto_1
    move v2, v2

    .line 2294489
    iput v2, p3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2294490
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v2, v3, v3, v3, v3}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setPadding(IIII)V

    .line 2294491
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sget-object v3, LX/5OS;->NONE:LX/5OS;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubShown(LX/5OS;)V

    .line 2294492
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->a(II)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
