.class public final enum LX/HBx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HBx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HBx;

.field public static final enum FETCH_EDIT_PAGE_QUERY:LX/HBx;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2438012
    new-instance v0, LX/HBx;

    const-string v1, "FETCH_EDIT_PAGE_QUERY"

    invoke-direct {v0, v1, v2}, LX/HBx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HBx;->FETCH_EDIT_PAGE_QUERY:LX/HBx;

    .line 2438013
    const/4 v0, 0x1

    new-array v0, v0, [LX/HBx;

    sget-object v1, LX/HBx;->FETCH_EDIT_PAGE_QUERY:LX/HBx;

    aput-object v1, v0, v2

    sput-object v0, LX/HBx;->$VALUES:[LX/HBx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2438016
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HBx;
    .locals 1

    .prologue
    .line 2438015
    const-class v0, LX/HBx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HBx;

    return-object v0
.end method

.method public static values()[LX/HBx;
    .locals 1

    .prologue
    .line 2438014
    sget-object v0, LX/HBx;->$VALUES:[LX/HBx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HBx;

    return-object v0
.end method
