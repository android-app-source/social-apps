.class public LX/Fdo;
.super LX/1Cv;
.source ""


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field public c:Lcom/facebook/search/results/protocol/filters/FilterValue;

.field public final d:Z

.field public e:Ljava/lang/String;

.field public f:Landroid/text/SpannableString;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2264206
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2264207
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2264208
    iput-object v0, p0, LX/Fdo;->g:LX/0Px;

    .line 2264209
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Fdo;->h:Ljava/util/HashSet;

    .line 2264210
    iput-object p2, p0, LX/Fdo;->e:Ljava/lang/String;

    .line 2264211
    iput-object p3, p0, LX/Fdo;->b:Landroid/content/Context;

    .line 2264212
    const-string v0, "friends"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/Fdo;->d:Z

    .line 2264213
    return-void

    .line 2264214
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2264205
    iget-object v0, p0, LX/Fdo;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312a2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/facebook/search/results/protocol/filters/FilterValue;
    .locals 1

    .prologue
    .line 2264204
    iget-object v0, p0, LX/Fdo;->g:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 2264215
    check-cast p2, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2264216
    const v0, 0x7f0d2b75

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 2264217
    const v0, 0x7f0d2b77

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2264218
    const v1, 0x7f0d2b93

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbCheckBox;

    .line 2264219
    const v2, 0x7f0d2b94

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbRadioButton;

    .line 2264220
    iget-object v4, p0, LX/Fdo;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    if-ne p2, v4, :cond_0

    .line 2264221
    const v4, -0xa76f01

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2264222
    iget-object v4, p0, LX/Fdo;->f:Landroid/text/SpannableString;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2264223
    :goto_0
    iget-object v0, p0, LX/Fdo;->a:LX/0ad;

    sget-short v4, LX/100;->I:S

    invoke-interface {v0, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2264224
    if-eqz v0, :cond_1

    .line 2264225
    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbCheckBox;->setVisibility(I)V

    .line 2264226
    new-instance v0, LX/Fdn;

    invoke-direct {v0, p0, v1, p2}, LX/Fdn;-><init>(LX/Fdo;Lcom/facebook/resources/ui/FbCheckBox;Lcom/facebook/search/results/protocol/filters/FilterValue;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2264227
    :goto_1
    return-void

    .line 2264228
    :cond_0
    const/high16 v4, -0x1000000

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2264229
    iget-object v4, p2, Lcom/facebook/search/results/protocol/filters/FilterValue;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2264230
    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2264231
    :cond_1
    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbCheckBox;->setVisibility(I)V

    .line 2264232
    invoke-virtual {v2, v6}, Lcom/facebook/resources/ui/FbRadioButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2264203
    iget-object v0, p0, LX/Fdo;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2264202
    invoke-virtual {p0, p1}, LX/Fdo;->a(I)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2264201
    const-wide/16 v0, 0x0

    return-wide v0
.end method
