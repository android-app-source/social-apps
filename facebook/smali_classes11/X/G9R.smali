.class public abstract LX/G9R;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[LX/G9R;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2322777
    const/16 v0, 0x8

    new-array v0, v0, [LX/G9R;

    new-instance v1, LX/G9S;

    invoke-direct {v1}, LX/G9S;-><init>()V

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v2, LX/G9T;

    invoke-direct {v2}, LX/G9T;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LX/G9U;

    invoke-direct {v2}, LX/G9U;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, LX/G9V;

    invoke-direct {v2}, LX/G9V;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, LX/G9W;

    invoke-direct {v2}, LX/G9W;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, LX/G9X;

    invoke-direct {v2}, LX/G9X;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, LX/G9Y;

    invoke-direct {v2}, LX/G9Y;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, LX/G9Z;

    invoke-direct {v2}, LX/G9Z;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, LX/G9R;->a:[LX/G9R;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2322778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322779
    return-void
.end method

.method public static a(I)LX/G9R;
    .locals 1

    .prologue
    .line 2322780
    if-ltz p0, :cond_0

    const/4 v0, 0x7

    if-le p0, v0, :cond_1

    .line 2322781
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2322782
    :cond_1
    sget-object v0, LX/G9R;->a:[LX/G9R;

    aget-object v0, v0, p0

    return-object v0
.end method


# virtual methods
.method public final a(LX/G96;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2322783
    move v2, v1

    :goto_0
    if-ge v2, p2, :cond_2

    move v0, v1

    .line 2322784
    :goto_1
    if-ge v0, p2, :cond_1

    .line 2322785
    invoke-virtual {p0, v2, v0}, LX/G9R;->a(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2322786
    invoke-virtual {p1, v0, v2}, LX/G96;->c(II)V

    .line 2322787
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322788
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2322789
    :cond_2
    return-void
.end method

.method public abstract a(II)Z
.end method
