.class public final LX/G8y;
.super LX/G8w;
.source ""


# instance fields
.field public final a:[B

.field public final b:I

.field private final c:I

.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>([BIIIIIIZ)V
    .locals 2

    .prologue
    .line 2321336
    invoke-direct {p0, p6, p7}, LX/G8w;-><init>(II)V

    .line 2321337
    add-int v0, p4, p6

    if-gt v0, p2, :cond_0

    add-int v0, p5, p7

    if-le v0, p3, :cond_1

    .line 2321338
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Crop rectangle does not fit within image data."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321339
    :cond_1
    iput-object p1, p0, LX/G8y;->a:[B

    .line 2321340
    iput p2, p0, LX/G8y;->b:I

    .line 2321341
    iput p3, p0, LX/G8y;->c:I

    .line 2321342
    iput p4, p0, LX/G8y;->d:I

    .line 2321343
    iput p5, p0, LX/G8y;->e:I

    .line 2321344
    if-eqz p8, :cond_3

    .line 2321345
    iget-object p3, p0, LX/G8y;->a:[B

    .line 2321346
    const/4 v0, 0x0

    iget v1, p0, LX/G8y;->e:I

    iget p1, p0, LX/G8y;->b:I

    mul-int/2addr v1, p1

    iget p1, p0, LX/G8y;->d:I

    add-int/2addr p1, v1

    move p2, v0

    :goto_0
    if-ge p2, p7, :cond_3

    .line 2321347
    div-int/lit8 v0, p6, 0x2

    add-int p4, p1, v0

    .line 2321348
    add-int v0, p1, p6

    add-int/lit8 v0, v0, -0x1

    move v1, p1

    :goto_1
    if-ge v1, p4, :cond_2

    .line 2321349
    aget-byte p5, p3, v1

    .line 2321350
    aget-byte p8, p3, v0

    aput-byte p8, p3, v1

    .line 2321351
    aput-byte p5, p3, v0

    .line 2321352
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 2321353
    :cond_2
    add-int/lit8 v0, p2, 0x1

    iget v1, p0, LX/G8y;->b:I

    add-int/2addr p1, v1

    move p2, v0

    goto :goto_0

    .line 2321354
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2321355
    iget v0, p0, LX/G8w;->a:I

    move v3, v0

    .line 2321356
    iget v0, p0, LX/G8w;->b:I

    move v4, v0

    .line 2321357
    iget v0, p0, LX/G8y;->b:I

    if-ne v3, v0, :cond_1

    iget v0, p0, LX/G8y;->c:I

    if-ne v4, v0, :cond_1

    .line 2321358
    iget-object v0, p0, LX/G8y;->a:[B

    .line 2321359
    :cond_0
    :goto_0
    return-object v0

    .line 2321360
    :cond_1
    mul-int v5, v3, v4

    .line 2321361
    new-array v0, v5, [B

    .line 2321362
    iget v2, p0, LX/G8y;->e:I

    iget v6, p0, LX/G8y;->b:I

    mul-int/2addr v2, v6

    iget v6, p0, LX/G8y;->d:I

    add-int/2addr v2, v6

    .line 2321363
    iget v6, p0, LX/G8y;->b:I

    if-ne v3, v6, :cond_2

    .line 2321364
    iget-object v3, p0, LX/G8y;->a:[B

    invoke-static {v3, v2, v0, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 2321365
    :cond_2
    iget-object v5, p0, LX/G8y;->a:[B

    .line 2321366
    :goto_1
    if-ge v1, v4, :cond_0

    .line 2321367
    mul-int v6, v1, v3

    .line 2321368
    invoke-static {v5, v2, v0, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2321369
    iget v6, p0, LX/G8y;->b:I

    add-int/2addr v2, v6

    .line 2321370
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final a(I[B)[B
    .locals 4

    .prologue
    .line 2321371
    if-ltz p1, :cond_0

    .line 2321372
    iget v0, p0, LX/G8w;->b:I

    move v0, v0

    .line 2321373
    if-lt p1, v0, :cond_1

    .line 2321374
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested row is outside the image: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321375
    :cond_1
    iget v0, p0, LX/G8w;->a:I

    move v0, v0

    .line 2321376
    if-eqz p2, :cond_2

    array-length v1, p2

    if-ge v1, v0, :cond_3

    .line 2321377
    :cond_2
    new-array p2, v0, [B

    .line 2321378
    :cond_3
    iget v1, p0, LX/G8y;->e:I

    add-int/2addr v1, p1

    iget v2, p0, LX/G8y;->b:I

    mul-int/2addr v1, v2

    iget v2, p0, LX/G8y;->d:I

    add-int/2addr v1, v2

    .line 2321379
    iget-object v2, p0, LX/G8y;->a:[B

    const/4 v3, 0x0

    invoke-static {v2, v1, p2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2321380
    return-object p2
.end method
