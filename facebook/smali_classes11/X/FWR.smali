.class public LX/FWR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/0iA;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/content/SecureContextHelper;LX/0iA;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252148
    iput-object p1, p0, LX/FWR;->a:Landroid/content/Context;

    .line 2252149
    iput-object p2, p0, LX/FWR;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2252150
    iput-object p3, p0, LX/FWR;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2252151
    iput-object p4, p0, LX/FWR;->d:LX/0iA;

    .line 2252152
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2252153
    iget-object v0, p0, LX/FWR;->d:LX/0iA;

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SAVED_DASHBOARD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v2, LX/13G;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13G;

    .line 2252154
    if-nez v0, :cond_1

    .line 2252155
    :cond_0
    :goto_0
    return-void

    .line 2252156
    :cond_1
    iget-object v1, p0, LX/FWR;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, LX/13G;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 2252157
    if-eqz v1, :cond_0

    .line 2252158
    iget-object v2, p0, LX/FWR;->d:LX/0iA;

    invoke-virtual {v2}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v2

    invoke-interface {v0}, LX/0i1;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2252159
    iget-object v2, p0, LX/FWR;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1vE;->b:LX/0Tn;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2252160
    iget-object v0, p0, LX/FWR;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/FWR;->a:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
