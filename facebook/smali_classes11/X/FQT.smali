.class public final enum LX/FQT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQT;

.field public static final enum OPEN_ID_CONNECT_FAILURE:LX/FQT;


# instance fields
.field private final mAnalyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2239376
    new-instance v0, LX/FQT;

    const-string v1, "OPEN_ID_CONNECT_FAILURE"

    const-string v2, "open_id_connect_failure"

    invoke-direct {v0, v1, v3, v2}, LX/FQT;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FQT;->OPEN_ID_CONNECT_FAILURE:LX/FQT;

    .line 2239377
    const/4 v0, 0x1

    new-array v0, v0, [LX/FQT;

    sget-object v1, LX/FQT;->OPEN_ID_CONNECT_FAILURE:LX/FQT;

    aput-object v1, v0, v3

    sput-object v0, LX/FQT;->$VALUES:[LX/FQT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2239382
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2239383
    iput-object p3, p0, LX/FQT;->mAnalyticsName:Ljava/lang/String;

    .line 2239384
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQT;
    .locals 1

    .prologue
    .line 2239381
    const-class v0, LX/FQT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQT;

    return-object v0
.end method

.method public static values()[LX/FQT;
    .locals 1

    .prologue
    .line 2239380
    sget-object v0, LX/FQT;->$VALUES:[LX/FQT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQT;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2239379
    iget-object v0, p0, LX/FQT;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2239378
    iget-object v0, p0, LX/FQT;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method
