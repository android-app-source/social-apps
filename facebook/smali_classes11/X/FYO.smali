.class public LX/FYO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FYN;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:[LX/FYM;

.field public c:LX/0tQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;JLX/0tQ;)V
    .locals 14

    .prologue
    const/4 v12, 0x0

    .line 2255978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2255979
    iput-object p1, p0, LX/FYO;->a:Landroid/content/Context;

    .line 2255980
    move-object/from16 v0, p4

    iput-object v0, p0, LX/FYO;->c:LX/0tQ;

    .line 2255981
    const/4 v2, 0x4

    new-array v2, v2, [LX/FYM;

    new-instance v3, LX/FYM;

    const v4, 0x7f08340f

    sget-object v5, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    sub-long v6, p2, v6

    invoke-direct {v3, v4, v6, v7}, LX/FYM;-><init>(IJ)V

    aput-object v3, v2, v12

    const/4 v3, 0x1

    new-instance v4, LX/FYM;

    const v5, 0x7f083410

    sget-object v6, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x7

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    sub-long v6, p2, v6

    invoke-direct {v4, v5, v6, v7}, LX/FYM;-><init>(IJ)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, LX/FYM;

    const v5, 0x7f083411

    sget-object v6, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x1e

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    sub-long v6, p2, v6

    sget-object v8, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0x9

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    sub-long/2addr v6, v8

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0x32

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    sub-long/2addr v6, v8

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0x18

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-direct {v4, v5, v6, v7}, LX/FYM;-><init>(IJ)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, LX/FYM;

    const v5, 0x7f083412

    const-wide v6, 0x7fffffffffffffffL

    invoke-direct {v4, v5, v6, v7}, LX/FYM;-><init>(IJ)V

    aput-object v4, v2, v3

    iput-object v2, p0, LX/FYO;->b:[LX/FYM;

    .line 2255982
    return-void
.end method


# virtual methods
.method public final a(LX/BO1;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BO1;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/FYd;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2255983
    invoke-interface {p1}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 2255984
    const/4 v1, 0x0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 2255985
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    sub-int/2addr v3, v1

    if-gt v2, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0Tp;->b(Z)V

    .line 2255986
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2255987
    invoke-interface {p1}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_1

    move-object v3, v7

    .line 2255988
    :goto_1
    move-object v0, v3

    .line 2255989
    return-object v0

    .line 2255990
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 2255991
    :cond_1
    iget-object v3, p0, LX/FYO;->b:[LX/FYM;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v8

    .line 2255992
    const/4 v6, -0x1

    .line 2255993
    const/4 v4, 0x0

    .line 2255994
    const/4 v5, 0x0

    .line 2255995
    const/4 v3, 0x0

    move v13, v3

    move v3, v5

    move v5, v6

    move-object v6, v8

    move v8, v13

    .line 2255996
    :goto_2
    if-nez v3, :cond_2

    iget-object v9, p0, LX/FYO;->b:[LX/FYM;

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    if-ge v5, v9, :cond_8

    :cond_2
    if-ge v8, v2, :cond_8

    .line 2255997
    add-int v9, v1, v8

    invoke-interface {v0, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 2255998
    if-nez v8, :cond_3

    invoke-interface {p1}, LX/BO1;->X()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    iget-object v9, p0, LX/FYO;->c:LX/0tQ;

    invoke-virtual {v9}, LX/0tQ;->c()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2255999
    new-instance v3, LX/FYd;

    .line 2256000
    sget-object v9, LX/FYL;->a:[I

    iget-object v10, p0, LX/FYO;->c:LX/0tQ;

    invoke-virtual {v10}, LX/0tQ;->m()LX/2qY;

    move-result-object v10

    invoke-virtual {v10}, LX/2qY;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 2256001
    iget-object v9, p0, LX/FYO;->a:Landroid/content/Context;

    const v10, 0x7f083414

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    :goto_3
    move-object v9, v9

    .line 2256002
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    add-int/2addr v11, v8

    invoke-direct {v3, v9, v10, v11}, LX/FYd;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2256003
    const/4 v3, 0x1

    .line 2256004
    :cond_3
    if-eqz v3, :cond_9

    invoke-interface {p1}, LX/BO1;->X()I

    move-result v9

    if-nez v9, :cond_9

    .line 2256005
    new-instance v3, LX/FYd;

    iget-object v4, p0, LX/FYO;->a:Landroid/content/Context;

    const v5, 0x7f083416

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/2addr v6, v8

    invoke-direct {v3, v4, v5, v6}, LX/FYd;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2256006
    const/4 v5, -0x1

    .line 2256007
    const/4 v4, 0x0

    .line 2256008
    const/4 v3, 0x0

    .line 2256009
    iget-object v6, p0, LX/FYO;->b:[LX/FYM;

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v6

    move v13, v3

    move-object v3, v4

    move v4, v5

    move v5, v13

    .line 2256010
    :goto_4
    invoke-interface {p1}, LX/BO1;->g()J

    move-result-wide v9

    .line 2256011
    if-eqz v3, :cond_4

    iget-wide v11, v3, LX/FYM;->b:J

    cmp-long v11, v9, v11

    if-gez v11, :cond_7

    .line 2256012
    :cond_4
    :goto_5
    if-eqz v3, :cond_5

    iget-wide v11, v3, LX/FYM;->b:J

    cmp-long v11, v9, v11

    if-gez v11, :cond_6

    iget-object v11, p0, LX/FYO;->b:[LX/FYM;

    array-length v11, v11

    add-int/lit8 v11, v11, -0x1

    if-ge v4, v11, :cond_6

    .line 2256013
    :cond_5
    invoke-interface {v6}, Ljava/util/ListIterator;->nextIndex()I

    move-result v4

    .line 2256014
    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FYM;

    goto :goto_5

    .line 2256015
    :cond_6
    new-instance v9, LX/FYd;

    iget-object v10, p0, LX/FYO;->a:Landroid/content/Context;

    iget v11, v3, LX/FYM;->a:I

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v12

    add-int/2addr v12, v8

    invoke-direct {v9, v10, v11, v12}, LX/FYd;-><init>(Ljava/lang/CharSequence;II)V

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2256016
    :cond_7
    add-int/lit8 v8, v8, 0x1

    move v13, v5

    move v5, v4

    move-object v4, v3

    move v3, v13

    goto/16 :goto_2

    :cond_8
    move-object v3, v7

    .line 2256017
    goto/16 :goto_1

    :cond_9
    move v13, v3

    move-object v3, v4

    move v4, v5

    move v5, v13

    goto :goto_4

    .line 2256018
    :pswitch_0
    iget-object v9, p0, LX/FYO;->a:Landroid/content/Context;

    const v10, 0x7f083413

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_3

    .line 2256019
    :pswitch_1
    iget-object v9, p0, LX/FYO;->a:Landroid/content/Context;

    const v10, 0x7f083415

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
