.class public final enum LX/GTf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GTf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GTf;

.field public static final enum COMPLETED:LX/GTf;

.field public static final enum SHARING:LX/GTf;

.field public static final enum SHARING_UPSELL:LX/GTf;

.field private static final mStepNameMap:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/GTf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final stepName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2355745
    new-instance v1, LX/GTf;

    const-string v2, "SHARING"

    const-string v3, "sharing"

    invoke-direct {v1, v2, v0, v3}, LX/GTf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/GTf;->SHARING:LX/GTf;

    .line 2355746
    new-instance v1, LX/GTf;

    const-string v2, "COMPLETED"

    const-string v3, "completed"

    invoke-direct {v1, v2, v4, v3}, LX/GTf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/GTf;->COMPLETED:LX/GTf;

    .line 2355747
    new-instance v1, LX/GTf;

    const-string v2, "SHARING_UPSELL"

    const-string v3, "sharing_upsell"

    invoke-direct {v1, v2, v5, v3}, LX/GTf;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, LX/GTf;->SHARING_UPSELL:LX/GTf;

    .line 2355748
    const/4 v1, 0x3

    new-array v1, v1, [LX/GTf;

    sget-object v2, LX/GTf;->SHARING:LX/GTf;

    aput-object v2, v1, v0

    sget-object v2, LX/GTf;->COMPLETED:LX/GTf;

    aput-object v2, v1, v4

    sget-object v2, LX/GTf;->SHARING_UPSELL:LX/GTf;

    aput-object v2, v1, v5

    sput-object v1, LX/GTf;->$VALUES:[LX/GTf;

    .line 2355749
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2355750
    invoke-static {}, LX/GTf;->values()[LX/GTf;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2355751
    iget-object v5, v4, LX/GTf;->stepName:Ljava/lang/String;

    invoke-static {v5}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2355752
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2355753
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/GTf;->mStepNameMap:LX/0P1;

    .line 2355754
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2355755
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2355756
    iput-object p3, p0, LX/GTf;->stepName:Ljava/lang/String;

    .line 2355757
    return-void
.end method

.method public static fromStepName(Ljava/lang/String;)LX/GTf;
    .locals 2

    .prologue
    .line 2355758
    sget-object v0, LX/GTf;->mStepNameMap:LX/0P1;

    invoke-static {p0}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GTf;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/GTf;
    .locals 1

    .prologue
    .line 2355759
    const-class v0, LX/GTf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GTf;

    return-object v0
.end method

.method public static values()[LX/GTf;
    .locals 1

    .prologue
    .line 2355760
    sget-object v0, LX/GTf;->$VALUES:[LX/GTf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GTf;

    return-object v0
.end method
