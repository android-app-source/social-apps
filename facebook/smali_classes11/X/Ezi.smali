.class public LX/Ezi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ezg;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/17W;

.field public final d:LX/Ezl;

.field public final e:LX/0SF;

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17W;LX/0Uh;LX/Ezl;LX/0SF;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2187560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187561
    iput-object p6, p0, LX/Ezi;->e:LX/0SF;

    .line 2187562
    iput-object p1, p0, LX/Ezi;->a:Landroid/content/Context;

    .line 2187563
    iput-object p2, p0, LX/Ezi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2187564
    iput-object p3, p0, LX/Ezi;->c:LX/17W;

    .line 2187565
    iput-object p5, p0, LX/Ezi;->d:LX/Ezl;

    .line 2187566
    const/16 v0, 0x435

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/Ezi;->f:Z

    .line 2187567
    iget-boolean v0, p0, LX/Ezi;->f:Z

    if-eqz v0, :cond_0

    .line 2187568
    iget-object v0, p0, LX/Ezi;->d:LX/Ezl;

    invoke-virtual {v0, p0}, LX/Ezl;->a(LX/Ezg;)V

    .line 2187569
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2187570
    iget-object v0, p0, LX/Ezi;->e:LX/0SF;

    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/Ezi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Ezc;->b:LX/0Tn;

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2710

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2187571
    :cond_0
    :goto_0
    return-void

    .line 2187572
    :cond_1
    iget-boolean v0, p0, LX/Ezi;->f:Z

    if-eqz v0, :cond_0

    .line 2187573
    iget-object v0, p0, LX/Ezi;->d:LX/Ezl;

    invoke-virtual {v0}, LX/Ezl;->a()V

    goto :goto_0
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 2187574
    iget-object v0, p0, LX/Ezi;->d:LX/Ezl;

    .line 2187575
    iget-object v1, v0, LX/Ezl;->a:LX/Ezn;

    .line 2187576
    iget-object v0, v1, LX/Ezn;->a:Ljava/util/List;

    move-object v1, v0

    .line 2187577
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    move v0, v1

    .line 2187578
    if-nez v0, :cond_0

    .line 2187579
    iget-object v3, p0, LX/Ezi;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/Ezc;->b:LX/0Tn;

    iget-object v5, p0, LX/Ezi;->e:LX/0SF;

    invoke-virtual {v5}, LX/0SF;->a()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2187580
    iget-object v0, p0, LX/Ezi;->c:LX/17W;

    iget-object v1, p0, LX/Ezi;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->db:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2187581
    :cond_0
    return-void
.end method
