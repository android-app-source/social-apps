.class public final LX/Ftr;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;)V
    .locals 0

    .prologue
    .line 2299275
    iput-object p1, p0, LX/Ftr;->a:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2299276
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2299277
    check-cast p1, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;

    .line 2299278
    if-eqz p1, :cond_0

    .line 2299279
    iget-object v0, p0, LX/Ftr;->a:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->k:LX/FtF;

    .line 2299280
    iget-object v1, v0, LX/FtF;->a:Ljava/util/List;

    invoke-virtual {p1}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;->a()LX/0Px;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2299281
    invoke-virtual {p1}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel;->j()Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;

    move-result-object v1

    iput-object v1, v0, LX/FtF;->c:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;

    .line 2299282
    iget-object v0, p0, LX/Ftr;->a:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->l:LX/1Rq;

    invoke-interface {v0}, LX/1OP;->notifyDataSetChanged()V

    .line 2299283
    :cond_0
    return-void
.end method
