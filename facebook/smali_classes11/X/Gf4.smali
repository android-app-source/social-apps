.class public final LX/Gf4;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Gf6;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Gf5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Gf6",
            "<TE;>.PageYouMay",
            "LikeHeaderComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Gf6;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Gf6;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 2376774
    iput-object p1, p0, LX/Gf4;->b:LX/Gf6;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2376775
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "feedProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "item"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "smallFormatFlag"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Gf4;->c:[Ljava/lang/String;

    .line 2376776
    iput v3, p0, LX/Gf4;->d:I

    .line 2376777
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Gf4;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Gf4;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Gf4;LX/1De;IILX/Gf5;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/Gf6",
            "<TE;>.PageYouMay",
            "LikeHeaderComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2376770
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2376771
    iput-object p4, p0, LX/Gf4;->a:LX/Gf5;

    .line 2376772
    iget-object v0, p0, LX/Gf4;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2376773
    return-void
.end method


# virtual methods
.method public final a(LX/1Pk;)LX/Gf4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/Gf6",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376767
    iget-object v0, p0, LX/Gf4;->a:LX/Gf5;

    iput-object p1, v0, LX/Gf5;->e:LX/1Pk;

    .line 2376768
    iget-object v0, p0, LX/Gf4;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376769
    return-object p0
.end method

.method public final a(LX/25E;)LX/Gf4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/25E;",
            ")",
            "LX/Gf6",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376764
    iget-object v0, p0, LX/Gf4;->a:LX/Gf5;

    iput-object p1, v0, LX/Gf5;->b:LX/25E;

    .line 2376765
    iget-object v0, p0, LX/Gf4;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376766
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Gf4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;)",
            "LX/Gf6",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376761
    iget-object v0, p0, LX/Gf4;->a:LX/Gf5;

    iput-object p1, v0, LX/Gf5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2376762
    iget-object v0, p0, LX/Gf4;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376763
    return-object p0
.end method

.method public final a(Z)LX/Gf4;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/Gf6",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376742
    iget-object v0, p0, LX/Gf4;->a:LX/Gf5;

    iput-boolean p1, v0, LX/Gf5;->c:Z

    .line 2376743
    iget-object v0, p0, LX/Gf4;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376744
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2376757
    invoke-super {p0}, LX/1X5;->a()V

    .line 2376758
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gf4;->a:LX/Gf5;

    .line 2376759
    iget-object v0, p0, LX/Gf4;->b:LX/Gf6;

    iget-object v0, v0, LX/Gf6;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2376760
    return-void
.end method

.method public final b(Z)LX/Gf4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/Gf6",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376755
    iget-object v0, p0, LX/Gf4;->a:LX/Gf5;

    iput-boolean p1, v0, LX/Gf5;->d:Z

    .line 2376756
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Gf6;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2376745
    iget-object v1, p0, LX/Gf4;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Gf4;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Gf4;->d:I

    if-ge v1, v2, :cond_2

    .line 2376746
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2376747
    :goto_0
    iget v2, p0, LX/Gf4;->d:I

    if-ge v0, v2, :cond_1

    .line 2376748
    iget-object v2, p0, LX/Gf4;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2376749
    iget-object v2, p0, LX/Gf4;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2376750
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2376751
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2376752
    :cond_2
    iget-object v0, p0, LX/Gf4;->a:LX/Gf5;

    .line 2376753
    invoke-virtual {p0}, LX/Gf4;->a()V

    .line 2376754
    return-object v0
.end method
