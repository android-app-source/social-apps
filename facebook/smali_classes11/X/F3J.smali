.class public final LX/F3J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V
    .locals 0

    .prologue
    .line 2193995
    iput-object p1, p0, LX/F3J;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0xbbced50

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193996
    iget-object v1, p0, LX/F3J;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    .line 2193997
    iget-object v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->p:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->r:Z

    if-nez v3, :cond_1

    iget-object v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokens()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    iget p0, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->s:I

    if-ne v3, p0, :cond_1

    .line 2193998
    :cond_0
    invoke-static {v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->r(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    .line 2193999
    :goto_0
    const v1, 0x63e5674

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2194000
    :cond_1
    new-instance v3, LX/0ju;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v3, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const p0, 0x7f08319d

    invoke-virtual {v3, p0}, LX/0ju;->b(I)LX/0ju;

    move-result-object v3

    const p0, 0x7f08319f

    const/4 p1, 0x0

    invoke-virtual {v3, p0, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const p0, 0x7f08319e

    new-instance p1, LX/F3P;

    invoke-direct {p1, v1}, LX/F3P;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    invoke-virtual {v3, p0, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    goto :goto_0
.end method
