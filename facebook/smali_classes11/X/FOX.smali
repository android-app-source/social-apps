.class public LX/FOX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/2CH;

.field private final b:LX/2Of;

.field private final c:LX/3Kx;

.field public final d:LX/13Q;

.field public final e:LX/1BA;

.field private final f:LX/0Zb;

.field private final g:LX/FQl;

.field private final h:Ljava/util/concurrent/Executor;

.field private final i:LX/2Oi;

.field private final j:LX/9l7;

.field public k:Z

.field public l:Lcom/facebook/messaging/model/messages/ParticipantInfo;

.field public m:Lcom/facebook/user/model/User;

.field public n:Lcom/facebook/user/model/UserKey;

.field public o:LX/3Ox;

.field public p:LX/FOW;


# direct methods
.method public constructor <init>(LX/2CH;LX/2Of;LX/3Kx;LX/13Q;LX/1BA;LX/0Zb;LX/FQl;Ljava/util/concurrent/Executor;LX/2Oi;)V
    .locals 2
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2235201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235202
    sget-object v0, LX/3Ox;->a:LX/3Ox;

    iput-object v0, p0, LX/FOX;->o:LX/3Ox;

    .line 2235203
    iput-object p1, p0, LX/FOX;->a:LX/2CH;

    .line 2235204
    iput-object p2, p0, LX/FOX;->b:LX/2Of;

    .line 2235205
    iput-object p3, p0, LX/FOX;->c:LX/3Kx;

    .line 2235206
    iput-object p4, p0, LX/FOX;->d:LX/13Q;

    .line 2235207
    iput-object p5, p0, LX/FOX;->e:LX/1BA;

    .line 2235208
    iput-object p6, p0, LX/FOX;->f:LX/0Zb;

    .line 2235209
    iput-object p7, p0, LX/FOX;->g:LX/FQl;

    .line 2235210
    iput-object p8, p0, LX/FOX;->h:Ljava/util/concurrent/Executor;

    .line 2235211
    iput-object p9, p0, LX/FOX;->i:LX/2Oi;

    .line 2235212
    new-instance v0, LX/FOV;

    invoke-direct {v0, p0}, LX/FOV;-><init>(LX/FOX;)V

    iput-object v0, p0, LX/FOX;->j:LX/9l7;

    .line 2235213
    return-void
.end method

.method public static a(LX/FOX;Lcom/facebook/user/model/UserKey;)V
    .locals 3
    .param p0    # LX/FOX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2235214
    iget-object v0, p0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    invoke-static {p1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2235215
    :cond_0
    :goto_0
    return-void

    .line 2235216
    :cond_1
    sget-object v0, LX/3Ox;->a:LX/3Ox;

    iput-object v0, p0, LX/FOX;->o:LX/3Ox;

    .line 2235217
    iget-object v0, p0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    if-eqz v0, :cond_2

    .line 2235218
    iget-object v0, p0, LX/FOX;->a:LX/2CH;

    iget-object v1, p0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    iget-object v2, p0, LX/FOX;->j:LX/9l7;

    invoke-virtual {v0, v1, v2}, LX/2CH;->b(Lcom/facebook/user/model/UserKey;LX/9l7;)V

    .line 2235219
    :cond_2
    iput-object p1, p0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    .line 2235220
    iget-object v0, p0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    if-eqz v0, :cond_0

    .line 2235221
    iget-object v0, p0, LX/FOX;->a:LX/2CH;

    iget-object v1, p0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    iget-object v2, p0, LX/FOX;->j:LX/9l7;

    invoke-virtual {v0, v1, v2}, LX/2CH;->a(Lcom/facebook/user/model/UserKey;LX/9l7;)V

    .line 2235222
    iget-object v0, p0, LX/FOX;->a:LX/2CH;

    iget-object v1, p0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v1}, LX/2CH;->d(Lcom/facebook/user/model/UserKey;)LX/3Ox;

    move-result-object v0

    iput-object v0, p0, LX/FOX;->o:LX/3Ox;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/FOX;
    .locals 11

    .prologue
    .line 2235223
    new-instance v0, LX/FOX;

    invoke-static {p0}, LX/2CH;->a(LX/0QB;)LX/2CH;

    move-result-object v1

    check-cast v1, LX/2CH;

    invoke-static {p0}, LX/2Of;->a(LX/0QB;)LX/2Of;

    move-result-object v2

    check-cast v2, LX/2Of;

    invoke-static {p0}, LX/3Kx;->a(LX/0QB;)LX/3Kx;

    move-result-object v3

    check-cast v3, LX/3Kx;

    invoke-static {p0}, LX/13Q;->a(LX/0QB;)LX/13Q;

    move-result-object v4

    check-cast v4, LX/13Q;

    invoke-static {p0}, LX/1B6;->a(LX/0QB;)LX/1BA;

    move-result-object v5

    check-cast v5, LX/1BA;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    .line 2235224
    new-instance v10, LX/FQl;

    invoke-static {p0}, LX/FQj;->a(LX/0QB;)LX/FQj;

    move-result-object v7

    check-cast v7, LX/FQj;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-direct {v10, v7, v8, v9}, LX/FQl;-><init>(LX/FQj;LX/0tX;LX/0Zb;)V

    .line 2235225
    move-object v7, v10

    .line 2235226
    check-cast v7, LX/FQl;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v9

    check-cast v9, LX/2Oi;

    invoke-direct/range {v0 .. v9}, LX/FOX;-><init>(LX/2CH;LX/2Of;LX/3Kx;LX/13Q;LX/1BA;LX/0Zb;LX/FQl;Ljava/util/concurrent/Executor;LX/2Oi;)V

    .line 2235227
    return-object v0
.end method
