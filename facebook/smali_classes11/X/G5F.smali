.class public final LX/G5F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/G5G;


# direct methods
.method public constructor <init>(LX/G5G;)V
    .locals 0

    .prologue
    .line 2318614
    iput-object p1, p0, LX/G5F;->a:LX/G5G;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    .line 2318615
    iget-object v0, p0, LX/G5F;->a:LX/G5G;

    iget-object v0, v0, LX/G5G;->c:LX/G5H;

    iget-object v0, v0, LX/G5H;->i:LX/1Ck;

    const-string v1, "TASK_UNPIN_POST"

    iget-object v2, p0, LX/G5F;->a:LX/G5G;

    iget-object v2, v2, LX/G5G;->c:LX/G5H;

    iget-object v2, v2, LX/G5H;->g:LX/Fzo;

    iget-object v3, p0, LX/G5F;->a:LX/G5G;

    iget-object v3, v3, LX/G5G;->c:LX/G5H;

    iget-object v3, v3, LX/D2z;->a:LX/5SB;

    .line 2318616
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2318617
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 2318618
    iget-object v4, v2, LX/Fzo;->b:LX/Fzy;

    .line 2318619
    new-instance v5, LX/4KD;

    invoke-direct {v5}, LX/4KD;-><init>()V

    .line 2318620
    const-string v6, "user_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2318621
    move-object v5, v5

    .line 2318622
    new-instance v6, LX/G0O;

    invoke-direct {v6}, LX/G0O;-><init>()V

    move-object v6, v6

    .line 2318623
    const-string v7, "input"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2318624
    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    invoke-static {v4, v5}, LX/Fzy;->a(LX/Fzy;LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v6, LX/Fzt;

    invoke-direct {v6, v4}, LX/Fzt;-><init>(LX/Fzy;)V

    iget-object v7, v4, LX/Fzy;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 2318625
    new-instance v5, LX/Fzj;

    invoke-direct {v5, v2}, LX/Fzj;-><init>(LX/Fzo;)V

    iget-object v6, v2, LX/Fzo;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2318626
    new-instance v3, LX/G5E;

    invoke-direct {v3, p0}, LX/G5E;-><init>(LX/G5F;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2318627
    const/4 v0, 0x0

    return v0
.end method
