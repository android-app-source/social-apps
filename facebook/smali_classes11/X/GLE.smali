.class public final LX/GLE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GLH;


# direct methods
.method public constructor <init>(LX/GLH;)V
    .locals 0

    .prologue
    .line 2342448
    iput-object p1, p0, LX/GLE;->a:LX/GLH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x1

    const/4 v7, 0x2

    const v0, -0x15a6308f

    invoke-static {v7, v10, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2342449
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 2342450
    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 2342451
    const/16 v0, 0xa

    invoke-virtual {v5, v11, v0}, Ljava/util/Calendar;->add(II)V

    .line 2342452
    iget-object v0, p0, LX/GLE;->a:LX/GLH;

    iget-object v0, v0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    .line 2342453
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c:Ljava/lang/Long;

    move-object v0, v1

    .line 2342454
    if-eqz v0, :cond_0

    .line 2342455
    iget-object v0, p0, LX/GLE;->a:LX/GLH;

    iget-object v0, v0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    .line 2342456
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c:Ljava/lang/Long;

    move-object v0, v1

    .line 2342457
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2342458
    :cond_0
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/GLD;

    invoke-direct {v2, p0, v8, v9}, LX/GLD;-><init>(LX/GLE;J)V

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v5, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v5, v11}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 2342459
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    .line 2342460
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 2342461
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2342462
    invoke-virtual {v2, v11, v10}, Ljava/util/Calendar;->add(II)V

    .line 2342463
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/32 v8, 0xea60

    sub-long/2addr v4, v8

    invoke-virtual {v1, v4, v5}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 2342464
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide v4, 0x757b12c00L

    add-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 2342465
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    .line 2342466
    const v0, -0x1cc7bd82

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
