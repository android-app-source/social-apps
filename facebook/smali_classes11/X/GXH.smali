.class public LX/GXH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/7j9;

.field public final b:LX/GW4;

.field public final c:LX/0TD;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/14x;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7ic;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HR7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/7j9;LX/GW4;LX/0TD;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7j9;",
            "LX/GW4;",
            "LX/0TD;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/14x;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7ic;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/01T;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HR7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2363906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2363907
    iput-object p1, p0, LX/GXH;->a:LX/7j9;

    .line 2363908
    iput-object p2, p0, LX/GXH;->b:LX/GW4;

    .line 2363909
    iput-object p3, p0, LX/GXH;->c:LX/0TD;

    .line 2363910
    iput-object p4, p0, LX/GXH;->d:LX/0Ot;

    .line 2363911
    iput-object p5, p0, LX/GXH;->e:LX/0Ot;

    .line 2363912
    iput-object p6, p0, LX/GXH;->f:LX/0Ot;

    .line 2363913
    iput-object p7, p0, LX/GXH;->g:LX/0Ot;

    .line 2363914
    iput-object p8, p0, LX/GXH;->h:LX/0Ot;

    .line 2363915
    iput-object p9, p0, LX/GXH;->i:LX/0Ot;

    .line 2363916
    iput-object p10, p0, LX/GXH;->j:LX/0Ot;

    .line 2363917
    iput-object p11, p0, LX/GXH;->k:LX/0Ot;

    .line 2363918
    iput-object p12, p0, LX/GXH;->l:LX/0Ot;

    .line 2363919
    iput-object p13, p0, LX/GXH;->m:LX/0Ot;

    .line 2363920
    iput-object p14, p0, LX/GXH;->n:LX/0Ot;

    .line 2363921
    move-object/from16 v0, p15

    iput-object v0, p0, LX/GXH;->o:LX/0Ot;

    .line 2363922
    move-object/from16 v0, p16

    iput-object v0, p0, LX/GXH;->p:LX/0Ot;

    .line 2363923
    move-object/from16 v0, p17

    iput-object v0, p0, LX/GXH;->q:LX/0Ot;

    .line 2363924
    return-void
.end method

.method public static a(LX/0Px;LX/0Px;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/GXM;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/GXM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2363869
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2363870
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 2363871
    const/4 v4, 0x0

    .line 2363872
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    new-array v7, v2, [I

    move v3, v4

    .line 2363873
    :goto_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 2363874
    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GXM;

    .line 2363875
    iget-object v5, v2, LX/GXM;->d:LX/0am;

    move-object v2, v5

    .line 2363876
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GXM;

    .line 2363877
    iget-object v5, v2, LX/GXM;->d:LX/0am;

    move-object v2, v5

    .line 2363878
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_2
    aput v2, v7, v3

    .line 2363879
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2363880
    :cond_0
    const/4 v2, -0x1

    goto :goto_2

    .line 2363881
    :cond_1
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    move v3, v4

    .line 2363882
    :goto_3
    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GXM;

    .line 2363883
    iget-object v5, v2, LX/GXM;->b:LX/0Px;

    move-object v2, v5

    .line 2363884
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_5

    .line 2363885
    aput v3, v7, v0

    .line 2363886
    invoke-static {v7, p0, p1}, LX/GXH;->a([ILX/0Px;LX/0Px;)LX/0Px;

    move-result-object v9

    .line 2363887
    invoke-virtual {v9}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2363888
    sget-object v2, LX/GXL;->NOT_AVAILABLE:LX/GXL;

    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2363889
    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 2363890
    :cond_2
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v5, v4

    move v6, v4

    :goto_5
    if-ge v5, v10, :cond_3

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    .line 2363891
    invoke-virtual {v2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->k()I

    move-result v2

    add-int/2addr v6, v2

    .line 2363892
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_5

    .line 2363893
    :cond_3
    if-nez v6, :cond_4

    .line 2363894
    sget-object v2, LX/GXL;->SOLD_OUT:LX/GXL;

    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2363895
    :cond_4
    sget-object v2, LX/GXL;->AVAILABLE:LX/GXL;

    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2363896
    :cond_5
    new-instance v3, LX/GXM;

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GXM;

    .line 2363897
    iget-object v4, v2, LX/GXM;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2363898
    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GXM;

    .line 2363899
    iget-object v5, v2, LX/GXM;->b:LX/0Px;

    move-object v5, v5

    .line 2363900
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GXM;

    .line 2363901
    iget-object v7, v2, LX/GXM;->d:LX/0am;

    move-object v2, v7

    .line 2363902
    invoke-direct {v3, v4, v5, v6, v2}, LX/GXM;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0am;)V

    move-object v2, v3

    .line 2363903
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2363904
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 2363905
    :cond_6
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a([ILX/0Px;LX/0Px;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "LX/0Px",
            "<",
            "LX/GXM;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2363852
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2363853
    array-length v8, p0

    .line 2363854
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v9

    move v6, v3

    :goto_0
    if-ge v6, v9, :cond_2

    invoke-virtual {p2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    .line 2363855
    const/4 v4, 0x1

    move v5, v3

    .line 2363856
    :goto_1
    if-ge v5, v8, :cond_3

    .line 2363857
    aget v10, p0, v5

    .line 2363858
    const/4 v1, -0x1

    if-eq v10, v1, :cond_1

    .line 2363859
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->s()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2363860
    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GXM;

    .line 2363861
    iget-object v11, v2, LX/GXM;->b:LX/0Px;

    move-object v2, v11

    .line 2363862
    invoke-virtual {v2, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2363863
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v3

    .line 2363864
    :goto_2
    if-eqz v1, :cond_0

    .line 2363865
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2363866
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2363867
    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 2363868
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move v1, v4

    goto :goto_2
.end method

.method public static a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;)LX/0am;
    .locals 7
    .param p0    # Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;",
            ")",
            "LX/0am",
            "<",
            "LX/GX0;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2363842
    if-nez p0, :cond_0

    .line 2363843
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2363844
    :goto_0
    return-object v0

    .line 2363845
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mz_()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v1, 0x1

    .line 2363846
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->o()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    .line 2363847
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->p()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    .line 2363848
    :goto_3
    new-instance v0, LX/GX0;

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->b()Z

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mA_()Z

    move-result v3

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, LX/GX0;-><init>(ZZZLX/0am;LX/0am;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0

    .line 2363849
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 2363850
    :cond_2
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    goto :goto_2

    .line 2363851
    :cond_3
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    goto :goto_3
.end method

.method public static a(ILcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;LX/0Px;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;)LX/GXM;
    .locals 6
    .param p3    # Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ">;",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ")",
            "LX/GXM;"
        }
    .end annotation

    .prologue
    .line 2363816
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2363817
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2363818
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 2363819
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    .line 2363820
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->s()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p1

    if-ge p0, p1, :cond_0

    .line 2363821
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->s()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2363822
    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 2363823
    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2363824
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2363825
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2363826
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v2, v1

    .line 2363827
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    new-array v3, v1, [LX/GXL;

    .line 2363828
    const/4 v1, 0x0

    :goto_1
    array-length v4, v3

    if-ge v1, v4, :cond_2

    .line 2363829
    sget-object v4, LX/GXL;->AVAILABLE:LX/GXL;

    aput-object v4, v3, v1

    .line 2363830
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2363831
    :cond_2
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2363832
    if-eqz p3, :cond_3

    .line 2363833
    invoke-virtual {p3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->s()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2363834
    const/4 v4, 0x0

    move v5, v4

    :goto_2
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_5

    .line 2363835
    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2363836
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    .line 2363837
    :goto_3
    move-object v1, v4

    .line 2363838
    :goto_4
    new-instance v4, LX/GXM;

    invoke-direct {v4, v0, v2, v3, v1}, LX/GXM;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0am;)V

    return-object v4

    .line 2363839
    :cond_3
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    goto :goto_4

    .line 2363840
    :cond_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 2363841
    :cond_5
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    goto :goto_3
.end method

.method public static a(LX/GXK;)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2363810
    iget-object v0, p0, LX/GXK;->o:LX/0Px;

    move-object v0, v0

    .line 2363811
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2363812
    const/4 v0, 0x0

    .line 2363813
    :goto_0
    return-object v0

    .line 2363814
    :cond_0
    iget-object v0, p0, LX/GXK;->o:LX/0Px;

    move-object v0, v0

    .line 2363815
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    goto :goto_0
.end method

.method public static a(LX/GXH;Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2363805
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2363806
    :goto_0
    return-void

    .line 2363807
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2363808
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2363809
    iget-object v0, p0, LX/GXH;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static a(LX/GXI;LX/0Px;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GXI;",
            "LX/0Px",
            "<",
            "LX/GXM;",
            ">;",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2363712
    iput-object p1, p0, LX/GXI;->l:LX/0Px;

    .line 2363713
    invoke-virtual {p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v0

    .line 2363714
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    new-array v3, v1, [I

    .line 2363715
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 2363716
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GXM;

    .line 2363717
    iget-object v4, v1, LX/GXM;->d:LX/0am;

    move-object v1, v4

    .line 2363718
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GXM;

    .line 2363719
    iget-object v4, v1, LX/GXM;->d:LX/0am;

    move-object v1, v4

    .line 2363720
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_1
    aput v1, v3, v2

    .line 2363721
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2363722
    :cond_0
    const/4 v1, -0x1

    goto :goto_1

    .line 2363723
    :cond_1
    invoke-static {v3, p1, v0}, LX/GXH;->a([ILX/0Px;LX/0Px;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2363724
    iput-object v0, p0, LX/GXI;->o:LX/0Px;

    .line 2363725
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2363726
    invoke-virtual {p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    .line 2363727
    invoke-virtual {p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2363728
    invoke-virtual {v4, v1, v2}, LX/15i;->j(II)I

    move-result v1

    invoke-static {v1}, LX/7j5;->a(I)Z

    move-result v1

    .line 2363729
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    move-result-object v4

    .line 2363730
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->OFFSITE_LINK:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    if-ne v4, v5, :cond_d

    const/4 v5, 0x1

    :goto_3
    move v4, v5

    .line 2363731
    if-eqz v4, :cond_5

    move v4, v3

    .line 2363732
    :goto_4
    if-eqz v1, :cond_6

    .line 2363733
    sget-object v5, LX/GXJ;->CONTACT_MERCHANT:LX/GXJ;

    .line 2363734
    iput-object v5, p0, LX/GXI;->p:LX/GXJ;

    .line 2363735
    :goto_5
    if-eqz v4, :cond_2

    .line 2363736
    invoke-virtual {p2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->p()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 2363737
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 2363738
    :goto_6
    move-object v5, v6

    .line 2363739
    iput-object v5, p0, LX/GXI;->q:Ljava/lang/String;

    .line 2363740
    :cond_2
    if-nez v1, :cond_3

    if-eqz v4, :cond_8

    .line 2363741
    :cond_3
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 2363742
    iput-object v1, p0, LX/GXI;->m:LX/0am;

    .line 2363743
    iput-boolean v2, p0, LX/GXI;->n:Z

    .line 2363744
    iput-boolean v3, p0, LX/GXI;->r:Z

    .line 2363745
    :goto_7
    const/4 v7, 0x0

    .line 2363746
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 2363747
    iput-object v1, p0, LX/GXI;->h:LX/0am;

    .line 2363748
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 2363749
    iput-object v1, p0, LX/GXI;->i:LX/0am;

    .line 2363750
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2363751
    const-string v1, "--"

    .line 2363752
    iput-object v1, p0, LX/GXI;->g:Ljava/lang/String;

    .line 2363753
    :goto_8
    return-void

    :cond_4
    move v1, v2

    .line 2363754
    goto :goto_2

    :cond_5
    move v4, v2

    .line 2363755
    goto :goto_4

    .line 2363756
    :cond_6
    if-eqz v4, :cond_7

    .line 2363757
    sget-object v5, LX/GXJ;->OFFSITE:LX/GXJ;

    .line 2363758
    iput-object v5, p0, LX/GXI;->p:LX/GXJ;

    .line 2363759
    goto :goto_5

    .line 2363760
    :cond_7
    sget-object v5, LX/GXJ;->ONSITE:LX/GXJ;

    .line 2363761
    iput-object v5, p0, LX/GXI;->p:LX/GXJ;

    .line 2363762
    goto :goto_5

    .line 2363763
    :cond_8
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v4, v2

    :goto_9
    if-ge v4, v5, :cond_c

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GXM;

    .line 2363764
    iget-object v6, v1, LX/GXM;->d:LX/0am;

    move-object v1, v6

    .line 2363765
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_a

    move v1, v2

    .line 2363766
    :goto_a
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-ne v4, v3, :cond_9

    if-eqz v1, :cond_9

    invoke-static {v0}, LX/GXH;->a(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2363767
    :cond_9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 2363768
    iput-object v1, p0, LX/GXI;->m:LX/0am;

    .line 2363769
    iput-boolean v2, p0, LX/GXI;->n:Z

    .line 2363770
    iput-boolean v2, p0, LX/GXI;->r:Z

    .line 2363771
    goto :goto_7

    .line 2363772
    :cond_a
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_9

    .line 2363773
    :cond_b
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->k()I

    move-result v1

    const/16 v2, 0x9

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2363774
    iput-boolean v3, p0, LX/GXI;->n:Z

    .line 2363775
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 2363776
    iput-object v1, p0, LX/GXI;->m:LX/0am;

    .line 2363777
    iput-boolean v3, p0, LX/GXI;->r:Z

    .line 2363778
    goto :goto_7

    :cond_c
    move v1, v3

    goto :goto_a

    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 2363779
    :cond_e
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2363780
    invoke-virtual {v7}, Ljava/net/URL;->getHost()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto/16 :goto_6

    .line 2363781
    :catch_0
    move-exception v7

    .line 2363782
    const-class v8, LX/GXH;

    const-string v9, "Could not parse URL: %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v5, v10, p2

    invoke-static {v8, v7, v9, v10}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 2363783
    :cond_f
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2363784
    new-instance v1, LX/GXG;

    invoke-direct {v1}, LX/GXG;-><init>()V

    invoke-static {v2, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2363785
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_11

    .line 2363786
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    .line 2363787
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->q()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2363788
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->l()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v3

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->r()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v4

    if-eq v3, v4, :cond_10

    .line 2363789
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->r()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v3

    invoke-static {v3}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    .line 2363790
    iput-object v3, p0, LX/GXI;->h:LX/0am;

    .line 2363791
    :cond_10
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->k()I

    move-result v3

    const/16 v4, 0xa

    if-ge v3, v4, :cond_11

    .line 2363792
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->k()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 2363793
    iput-object v1, p0, LX/GXI;->i:LX/0am;

    .line 2363794
    :cond_11
    invoke-static {v2}, LX/GXH;->a(Ljava/util/List;)Z

    move-result v1

    .line 2363795
    iput-boolean v1, p0, LX/GXI;->j:Z

    .line 2363796
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->l()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v1

    int-to-long v3, v1

    .line 2363797
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->l()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;->a()I

    move-result v1

    int-to-long v5, v1

    .line 2363798
    cmp-long v1, v3, v5

    if-nez v1, :cond_12

    .line 2363799
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->l()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v1

    .line 2363800
    iput-object v1, p0, LX/GXI;->g:Ljava/lang/String;

    .line 2363801
    goto/16 :goto_8

    .line 2363802
    :cond_12
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->l()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " - "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->l()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2363803
    iput-object v1, p0, LX/GXI;->g:Ljava/lang/String;

    .line 2363804
    goto/16 :goto_8
.end method

.method public static a(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2363581
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    .line 2363582
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->k()I

    move-result v0

    if-lez v0, :cond_0

    .line 2363583
    const/4 v0, 0x0

    .line 2363584
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/GXH;
    .locals 20

    .prologue
    .line 2363710
    new-instance v2, LX/GXH;

    invoke-static/range {p0 .. p0}, LX/7j9;->a(LX/0QB;)LX/7j9;

    move-result-object v3

    check-cast v3, LX/7j9;

    invoke-static/range {p0 .. p0}, LX/GW4;->a(LX/0QB;)LX/GW4;

    move-result-object v4

    check-cast v4, LX/GW4;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, LX/0TD;

    const/16 v6, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2978

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xdec

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2eb

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x18c2

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xac0

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xc49

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3be

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x7bc

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x1a1

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x12c4

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x259

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x3df

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x2c15

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-direct/range {v2 .. v19}, LX/GXH;-><init>(LX/7j9;LX/GW4;LX/0TD;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2363711
    return-object v2
.end method


# virtual methods
.method public final a(LX/GX0;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2363642
    iget-object v0, p1, LX/GX0;->f:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    move-object v0, v0

    .line 2363643
    if-nez v0, :cond_2

    .line 2363644
    const/4 v1, 0x0

    .line 2363645
    :goto_0
    move-object v1, v1

    .line 2363646
    if-nez v1, :cond_0

    .line 2363647
    :goto_1
    return-void

    .line 2363648
    :cond_0
    iget-object v0, p0, LX/GXH;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    .line 2363649
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    if-nez v2, :cond_5

    .line 2363650
    :cond_1
    :goto_2
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    .line 2363651
    iput-object v1, v0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2363652
    move-object v0, v0

    .line 2363653
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    .line 2363654
    iput-object v2, v0, LX/8qL;->d:Ljava/lang/String;

    .line 2363655
    move-object v0, v0

    .line 2363656
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 2363657
    iput-object v1, v0, LX/8qL;->e:Ljava/lang/String;

    .line 2363658
    move-object v0, v0

    .line 2363659
    const/4 v1, 0x1

    .line 2363660
    iput-boolean v1, v0, LX/8qL;->i:Z

    .line 2363661
    move-object v0, v0

    .line 2363662
    new-instance v1, LX/21A;

    invoke-direct {v1}, LX/21A;-><init>()V

    const-string v2, "commerce_product_details"

    .line 2363663
    iput-object v2, v1, LX/21A;->c:Ljava/lang/String;

    .line 2363664
    move-object v1, v1

    .line 2363665
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    .line 2363666
    iput-object v1, v0, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2363667
    move-object v0, v0

    .line 2363668
    invoke-virtual {v0}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v1

    .line 2363669
    iget-object v0, p0, LX/GXH;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nI;

    invoke-interface {v0, p2, v1}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    goto :goto_1

    .line 2363670
    :cond_2
    new-instance v1, LX/3dM;

    invoke-direct {v1}, LX/3dM;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->c(Z)LX/3dM;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->c()Z

    move-result v2

    .line 2363671
    iput-boolean v2, v1, LX/3dM;->g:Z

    .line 2363672
    move-object v1, v1

    .line 2363673
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->d()Z

    move-result v2

    .line 2363674
    iput-boolean v2, v1, LX/3dM;->h:Z

    .line 2363675
    move-object v1, v1

    .line 2363676
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->e()Z

    move-result v2

    .line 2363677
    iput-boolean v2, v1, LX/3dM;->i:Z

    .line 2363678
    move-object v1, v1

    .line 2363679
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mz_()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->g(Z)LX/3dM;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->mA_()Z

    move-result v2

    invoke-virtual {v1, v2}, LX/3dM;->j(Z)LX/3dM;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2363680
    iput-object v2, v1, LX/3dM;->y:Ljava/lang/String;

    .line 2363681
    move-object v1, v1

    .line 2363682
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2363683
    iput-object v2, v1, LX/3dM;->D:Ljava/lang/String;

    .line 2363684
    move-object v1, v1

    .line 2363685
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->n()Ljava/lang/String;

    move-result-object v2

    .line 2363686
    iput-object v2, v1, LX/3dM;->Q:Ljava/lang/String;

    .line 2363687
    move-object v1, v1

    .line 2363688
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->l()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2363689
    new-instance v2, LX/3dN;

    invoke-direct {v2}, LX/3dN;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->l()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$LikersModel;->a()I

    move-result p1

    .line 2363690
    iput p1, v2, LX/3dN;->b:I

    .line 2363691
    move-object v2, v2

    .line 2363692
    invoke-virtual {v2}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 2363693
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2363694
    new-instance v2, LX/4ZH;

    invoke-direct {v2}, LX/4ZH;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->a()I

    move-result p1

    .line 2363695
    iput p1, v2, LX/4ZH;->b:I

    .line 2363696
    move-object v2, v2

    .line 2363697
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;->m()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel$TopLevelCommentsModel;->b()I

    move-result p1

    .line 2363698
    iput p1, v2, LX/4ZH;->e:I

    .line 2363699
    move-object v2, v2

    .line 2363700
    invoke-virtual {v2}, LX/4ZH;->a()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 2363701
    :cond_4
    invoke-virtual {v1}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    goto/16 :goto_0

    .line 2363702
    :cond_5
    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 2363703
    if-eqz v2, :cond_1

    .line 2363704
    iget-boolean p1, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move p1, p1

    .line 2363705
    if-eqz p1, :cond_1

    .line 2363706
    iget-object p1, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object p1, p1

    .line 2363707
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 2363708
    iget-object p1, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v2, p1

    .line 2363709
    invoke-static {v1, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public final a(LX/GXK;Landroid/content/Context;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    .line 2363585
    iget-object v0, p1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v0, v0

    .line 2363586
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2363587
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    const-string v2, "productDetails"

    .line 2363588
    iget-object v3, p1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v3, v3

    .line 2363589
    invoke-virtual {v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v3

    const v4, 0xa7c5482

    invoke-static {v3, v4}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v3

    invoke-static {v3}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v3

    invoke-virtual {v3}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 2363590
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2363591
    iget-object v5, p0, LX/GXH;->p:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, LX/01T;->PAA:LX/01T;

    if-ne v5, v6, :cond_9

    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 2363592
    if-nez v5, :cond_2

    move v5, v9

    .line 2363593
    :goto_2
    move v0, v5

    .line 2363594
    if-nez v0, :cond_1

    .line 2363595
    :goto_3
    return-void

    .line 2363596
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2363597
    :cond_1
    iget-object v0, p0, LX/GXH;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-interface {v0, v2, v1, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    goto :goto_3

    .line 2363598
    :cond_2
    iget-object v5, p0, LX/GXH;->m:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0SI;

    .line 2363599
    invoke-interface {v5}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    .line 2363600
    if-eqz v6, :cond_3

    .line 2363601
    iget-boolean v7, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v7, v7

    .line 2363602
    if-nez v7, :cond_4

    .line 2363603
    :cond_3
    const/4 v6, 0x0

    .line 2363604
    :cond_4
    move-object v11, v6

    .line 2363605
    if-nez v11, :cond_5

    .line 2363606
    iget-object v5, p0, LX/GXH;->o:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    const-string v6, "ProductGroupUserInteractionsViewControllerImpl"

    const-string v7, "No overridden ViewerContext available to attach to Composer."

    invoke-virtual {v5, v6, v7}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v10

    .line 2363607
    goto :goto_2

    .line 2363608
    :cond_5
    iget-object v5, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2363609
    iget-object v6, p1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v6, v6

    .line 2363610
    invoke-virtual {v6}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2363611
    iget-object v5, p1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v5, v5

    .line 2363612
    invoke-virtual {v5}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 2363613
    iget-object v5, p1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v5, v5

    .line 2363614
    invoke-virtual {v5}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->m()Ljava/lang/String;

    move-result-object v6

    .line 2363615
    iget-object v5, p1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v5, v5

    .line 2363616
    invoke-virtual {v5}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->o()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_6

    .line 2363617
    iget-object v5, p1, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v5, v5

    .line 2363618
    invoke-virtual {v5}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->o()LX/1vs;

    move-result-object v5

    iget-object v12, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2363619
    invoke-virtual {v12, v5, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2363620
    :goto_4
    new-instance v10, LX/89I;

    sget-object v12, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v10, v7, v8, v12}, LX/89I;-><init>(JLX/2rw;)V

    .line 2363621
    iput-object v6, v10, LX/89I;->c:Ljava/lang/String;

    .line 2363622
    move-object v7, v10

    .line 2363623
    iput-object v5, v7, LX/89I;->d:Ljava/lang/String;

    .line 2363624
    move-object v7, v7

    .line 2363625
    invoke-virtual {v7}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v7

    .line 2363626
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v5

    invoke-virtual {v5, v11}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v5

    .line 2363627
    invoke-virtual {v1, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2363628
    invoke-virtual {v5}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move v5, v9

    .line 2363629
    goto/16 :goto_2

    .line 2363630
    :cond_6
    const-string v5, ""

    goto :goto_4

    .line 2363631
    :cond_7
    iget-object v5, p0, LX/GXH;->q:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/HR7;

    .line 2363632
    iget-object v6, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2363633
    invoke-virtual {v5, v6}, LX/HR7;->a(Ljava/lang/String;)Lcom/facebook/ipc/pages/PageInfo;

    move-result-object v5

    .line 2363634
    if-nez v5, :cond_8

    .line 2363635
    iget-object v5, p0, LX/GXH;->o:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    const-string v6, "ProductGroupUserInteractionsViewControllerImpl"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "No PageInfo available for Page (page_id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2363636
    iget-object v8, v11, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2363637
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v10

    .line 2363638
    goto/16 :goto_2

    .line 2363639
    :cond_8
    iget-wide v7, v5, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    .line 2363640
    iget-object v6, v5, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    .line 2363641
    iget-object v5, v5, Lcom/facebook/ipc/pages/PageInfo;->squareProfilePicUrl:Ljava/lang/String;

    goto :goto_4

    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_1
.end method
