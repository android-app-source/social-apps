.class public final LX/GMz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public final synthetic b:LX/GN1;


# direct methods
.method public constructor <init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 0

    .prologue
    .line 2345390
    iput-object p1, p0, LX/GMz;->b:LX/GN1;

    iput-object p2, p0, LX/GMz;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    .line 2345391
    iget-object v0, p0, LX/GMz;->b:LX/GN1;

    iget-object v1, v0, LX/GN1;->b:LX/GDm;

    iget-object v2, p0, LX/GMz;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz p2, :cond_0

    sget-object v0, LX/GGB;->ACTIVE:LX/GGB;

    :goto_0
    invoke-static {v2, v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GGB;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v2

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getContext()Landroid/content/Context;

    move-result-object v3

    if-eqz p2, :cond_1

    sget-object v0, LX/A8v;->RESUME:LX/A8v;

    :goto_1
    invoke-virtual {v1, v2, v3, v0}, LX/GDm;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;LX/A8v;)V

    .line 2345392
    return-void

    .line 2345393
    :cond_0
    sget-object v0, LX/GGB;->PAUSED:LX/GGB;

    goto :goto_0

    :cond_1
    sget-object v0, LX/A8v;->PAUSE:LX/A8v;

    goto :goto_1
.end method
