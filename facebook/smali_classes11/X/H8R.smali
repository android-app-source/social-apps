.class public final LX/H8R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

.field public final synthetic b:J

.field public final synthetic c:I

.field public final synthetic d:LX/H8U;


# direct methods
.method public constructor <init>(LX/H8U;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;JI)V
    .locals 1

    .prologue
    .line 2433391
    iput-object p1, p0, LX/H8R;->d:LX/H8U;

    iput-object p2, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    iput-wide p3, p0, LX/H8R;->b:J

    iput p5, p0, LX/H8R;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x2

    const v0, 0x1032ac30

    invoke-static {v1, v10, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2433392
    iget-object v0, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2433393
    :cond_0
    const v0, 0x2741a125

    invoke-static {v1, v1, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2433394
    :goto_0
    return-void

    .line 2433395
    :cond_1
    iget-object v0, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v0, v1, :cond_2

    .line 2433396
    iget-object v0, p0, LX/H8R;->d:LX/H8U;

    invoke-static {v0}, LX/H8U;->b(LX/H8U;)LX/6WJ;

    move-result-object v0

    invoke-virtual {v0}, LX/6WJ;->show()V

    .line 2433397
    :goto_1
    const v0, 0x1c0fb8be

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_0

    .line 2433398
    :cond_2
    sget-object v0, LX/H8U;->a:LX/0Rf;

    iget-object v1, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2433399
    iget-object v0, p0, LX/H8R;->d:LX/H8U;

    iget-object v0, v0, LX/H8U;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08368c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_1

    .line 2433400
    :cond_3
    iget-object v0, p0, LX/H8R;->d:LX/H8U;

    iget-object v0, v0, LX/H8U;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v1, LX/9X7;->EDIT_TAP_REPLACE_ACTION:LX/9X7;

    iget-object v2, p0, LX/H8R;->d:LX/H8U;

    iget-wide v2, v2, LX/H8U;->j:J

    iget-object v4, p0, LX/H8R;->d:LX/H8U;

    iget-object v4, v4, LX/H8U;->k:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    iget-object v5, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/9XE;->a(LX/9X7;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2433401
    iget-object v0, p0, LX/H8R;->d:LX/H8U;

    iget-object v0, v0, LX/H8U;->c:LX/17Y;

    iget-object v1, p0, LX/H8R;->d:LX/H8U;

    iget-object v1, v1, LX/H8U;->f:Landroid/content/Context;

    sget-object v2, LX/0ax;->be:Ljava/lang/String;

    iget-wide v4, p0, LX/H8R;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 2433402
    const-string v0, "com.facebook.katana.profile.id"

    iget-wide v2, p0, LX/H8R;->b:J

    invoke-virtual {v8, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2433403
    const-string v9, "extra_config_action_data"

    new-instance v0, LX/CYE;

    const-string v2, "ACTION_BAR"

    iget-object v1, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    iget-object v1, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->l()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, LX/H8R;->c:I

    iget-object v1, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v1

    invoke-static {v1}, LX/H8X;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Z

    move-result v6

    move v1, v10

    invoke-direct/range {v0 .. v6}, LX/CYE;-><init>(ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;IZ)V

    invoke-virtual {v8, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2433404
    const-string v0, "extra_action_channel_edit_action"

    iget-object v1, p0, LX/H8R;->a:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v1

    invoke-static {v8, v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2433405
    iget-object v0, p0, LX/H8R;->d:LX/H8U;

    iget-object v1, v0, LX/H8U;->d:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2785

    iget-object v0, p0, LX/H8R;->d:LX/H8U;

    iget-object v0, v0, LX/H8U;->f:Landroid/content/Context;

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v8, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto/16 :goto_1
.end method
