.class public LX/Gdi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Uf;


# direct methods
.method public constructor <init>(LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2374490
    iput-object p1, p0, LX/Gdi;->a:LX/1Uf;

    .line 2374491
    return-void
.end method

.method public static a(LX/25E;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 2374498
    invoke-interface {p0}, LX/25E;->l()Ljava/lang/String;

    move-result-object v0

    .line 2374499
    if-eqz v0, :cond_0

    const-string v1, "\\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;LX/25E;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2374494
    invoke-interface {p1}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/16z;->c(Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v0

    .line 2374495
    if-lez v0, :cond_0

    .line 2374496
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0810d0

    const v3, 0x7f0810d1

    invoke-static {v1, v2, v3, v0}, LX/1z0;->a(Landroid/content/res/Resources;III)Ljava/lang/String;

    move-result-object v0

    .line 2374497
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Gdi;
    .locals 2

    .prologue
    .line 2374492
    new-instance v1, LX/Gdi;

    invoke-static {p0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v0

    check-cast v0, LX/1Uf;

    invoke-direct {v1, v0}, LX/Gdi;-><init>(LX/1Uf;)V

    .line 2374493
    return-object v1
.end method

.method public static b(LX/25E;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2374500
    invoke-interface {p0}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2374501
    const-string v0, "/"

    invoke-interface {p0}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 2374502
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;LX/25E;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2374467
    invoke-interface {p1}, LX/25E;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v0

    .line 2374468
    if-eqz v0, :cond_0

    .line 2374469
    if-nez v0, :cond_1

    .line 2374470
    const/4 p1, 0x0

    .line 2374471
    :goto_0
    move-object v0, p1

    .line 2374472
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2374473
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->p()Z

    move-result p1

    if-nez p1, :cond_2

    .line 2374474
    const-string p1, ""

    goto :goto_0

    .line 2374475
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->c()Z

    move-result p1

    if-eqz p1, :cond_3

    const p1, 0x7f081015

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_3
    const p1, 0x7f081014

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static c(LX/25E;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1

    .prologue
    .line 2374476
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/16z;->d(Lcom/facebook/graphql/model/GraphQLPage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2374477
    invoke-interface {p0}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2374478
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/25E;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2374479
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2374480
    invoke-interface {p0}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    .line 2374481
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/25E;)Landroid/text/Spannable;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2374482
    if-nez p2, :cond_1

    .line 2374483
    :cond_0
    :goto_0
    return-object v0

    .line 2374484
    :cond_1
    invoke-interface {p2}, LX/25E;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2374485
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2374486
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    .line 2374487
    iget-object v2, p0, LX/Gdi;->a:LX/1Uf;

    invoke-static {v1}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v3

    sget-object v4, LX/1eK;->SUBTITLE:LX/1eK;

    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v0, v5}, LX/1Uf;->a(LX/1eE;LX/1eK;Landroid/text/Spannable;LX/0lF;)V

    .line 2374488
    iget-object v2, p0, LX/Gdi;->a:LX/1Uf;

    invoke-static {v1}, LX/1eD;->d(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1y8;

    move-result-object v1

    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v3

    invoke-virtual {v2, v1, v0, v3}, LX/1Uf;->a(LX/1y8;Landroid/text/Spannable;LX/0lF;)V

    goto :goto_0
.end method
