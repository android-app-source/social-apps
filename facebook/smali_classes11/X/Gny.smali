.class public LX/Gny;
.super LX/Gnx;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Gny;


# direct methods
.method public constructor <init>(LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393886
    invoke-direct {p0}, LX/Gnx;-><init>()V

    .line 2393887
    iput-object p1, p0, LX/Gny;->a:LX/11i;

    .line 2393888
    return-void
.end method

.method public static a(LX/0QB;)LX/Gny;
    .locals 4

    .prologue
    .line 2393889
    sget-object v0, LX/Gny;->c:LX/Gny;

    if-nez v0, :cond_1

    .line 2393890
    const-class v1, LX/Gny;

    monitor-enter v1

    .line 2393891
    :try_start_0
    sget-object v0, LX/Gny;->c:LX/Gny;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2393892
    if-eqz v2, :cond_0

    .line 2393893
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2393894
    new-instance p0, LX/Gny;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/Gny;-><init>(LX/11i;)V

    .line 2393895
    move-object v0, p0

    .line 2393896
    sput-object v0, LX/Gny;->c:LX/Gny;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393897
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2393898
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2393899
    :cond_1
    sget-object v0, LX/Gny;->c:LX/Gny;

    return-object v0

    .line 2393900
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2393901
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
