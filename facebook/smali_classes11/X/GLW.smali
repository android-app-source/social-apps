.class public final LX/GLW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

.field public final synthetic b:LX/GLb;


# direct methods
.method public constructor <init>(LX/GLb;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V
    .locals 0

    .prologue
    .line 2343032
    iput-object p1, p0, LX/GLW;->b:LX/GLb;

    iput-object p2, p0, LX/GLW;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2343033
    iget-object v0, p0, LX/GLW;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(LX/0Px;)V

    .line 2343034
    iget-object v0, p0, LX/GLW;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    if-eqz v0, :cond_0

    .line 2343035
    iget-object v0, p0, LX/GLW;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GLW;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;Z)V

    .line 2343036
    :cond_0
    iget-object v0, p0, LX/GLW;->b:LX/GLb;

    iget-object v1, p0, LX/GLW;->b:LX/GLb;

    iget-object v1, v1, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->v()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2343037
    iput v1, v0, LX/GLb;->j:I

    .line 2343038
    iget-object v0, p0, LX/GLW;->b:LX/GLb;

    invoke-virtual {v0}, LX/GLb;->b()V

    .line 2343039
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2343040
    iget-object v0, p0, LX/GLW;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->d:LX/2U3;

    const-class v1, LX/GLb;

    const-string v2, "Error fetching first batch of new audiences for Unified Audience"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2343041
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2343042
    check-cast p1, LX/0Px;

    invoke-direct {p0, p1}, LX/GLW;->a(LX/0Px;)V

    return-void
.end method
