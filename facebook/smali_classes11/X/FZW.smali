.class public LX/FZW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0gc;

.field public final b:I


# direct methods
.method public constructor <init>(LX/0gc;I)V
    .locals 0

    .prologue
    .line 2257237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2257238
    iput-object p1, p0, LX/FZW;->a:LX/0gc;

    .line 2257239
    iput p2, p0, LX/FZW;->b:I

    .line 2257240
    return-void
.end method

.method public static a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/FZU;",
            ">(",
            "LX/FZc;",
            "Landroid/os/Bundle;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2257229
    invoke-direct {p0, p1}, LX/FZW;->a(LX/FZc;)LX/FZU;

    move-result-object v0

    .line 2257230
    if-nez v0, :cond_0

    .line 2257231
    invoke-static {p0, p1, p2}, LX/FZW;->c(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v0

    .line 2257232
    invoke-interface {v0}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2257233
    iget-object v1, p0, LX/FZW;->a:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget v2, p0, LX/FZW;->b:I

    invoke-interface {v0}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-virtual {p1}, LX/FZc;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-interface {v0}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->c()I

    .line 2257234
    iget-object v1, p0, LX/FZW;->a:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->b()Z

    .line 2257235
    move-object v0, v0

    .line 2257236
    :cond_0
    return-object v0
.end method

.method private a(LX/FZc;)LX/FZU;
    .locals 2

    .prologue
    .line 2257258
    iget-object v0, p0, LX/FZW;->a:LX/0gc;

    invoke-virtual {p1}, LX/FZc;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, LX/FZU;

    return-object v0
.end method

.method public static c(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;
    .locals 4

    .prologue
    .line 2257260
    sget-object v0, LX/FZV;->a:[I

    invoke-virtual {p1}, LX/FZc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2257261
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown graph search fragment type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2257262
    :pswitch_0
    new-instance v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;-><init>()V

    .line 2257263
    :goto_0
    return-object v0

    .line 2257264
    :pswitch_1
    new-instance v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;-><init>()V

    goto :goto_0

    .line 2257265
    :pswitch_2
    new-instance v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;-><init>()V

    goto :goto_0

    .line 2257266
    :pswitch_3
    const-string v0, "graph_search_results_page_blended"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->b(Ljava/lang/String;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    move-result-object v0

    goto :goto_0

    .line 2257267
    :pswitch_4
    new-instance v0, Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-direct {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;-><init>()V

    goto :goto_0

    .line 2257268
    :pswitch_5
    new-instance v0, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;-><init>()V

    goto :goto_0

    .line 2257269
    :pswitch_6
    new-instance v0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;

    invoke-direct {v0}, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;-><init>()V

    goto :goto_0

    .line 2257270
    :pswitch_7
    iget-object v0, p0, LX/FZW;->a:LX/0gc;

    .line 2257271
    new-instance v1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;

    invoke-direct {v1}, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;-><init>()V

    .line 2257272
    const/4 v3, 0x1

    .line 2257273
    iget-object v2, v1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-nez v2, :cond_0

    move v2, v3

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2257274
    iput-object v0, v1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->n:LX/0gc;

    .line 2257275
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2257276
    const-string p0, "ReactRouteName"

    const-string p1, "marketplace_search_module"

    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2257277
    const-string p0, "ReactURI"

    const-string p1, "marketplace_search_uri"

    invoke-virtual {p2, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2257278
    const-string p0, "marketplace_search_typeahead_react_tag"

    invoke-virtual {p2, p0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p0

    iput p0, v1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->o:I

    .line 2257279
    const-string p0, "non_immersive"

    invoke-virtual {v2, p0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2257280
    const-string v3, "marketplace_search_module"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->p:Ljava/lang/String;

    .line 2257281
    const-string v3, "marketplace_search_module"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2257282
    const-string v3, "marketplace_search_uri"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2257283
    const-string v3, "marketplace_search_typeahead_react_tag"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2257284
    new-instance v3, LX/CIx;

    invoke-direct {v3}, LX/CIx;-><init>()V

    invoke-virtual {v3, v2}, LX/CIx;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    iput-object v2, v1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2257285
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    .line 2257286
    iget-object v3, v1, Lcom/facebook/search/results/fragment/marketplace/MarketplaceSearchResultsFragment;->k:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    const/4 p0, 0x0

    invoke-virtual {v2, v3, p0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 2257287
    invoke-virtual {v2}, LX/0hH;->b()I

    .line 2257288
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2257289
    move-object v0, v1

    .line 2257290
    goto/16 :goto_0

    .line 2257291
    :pswitch_8
    new-instance v0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;-><init>()V

    goto/16 :goto_0

    .line 2257292
    :pswitch_9
    const-string v0, "graph_search_results_page_blended"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsFragment;

    move-result-object v0

    goto/16 :goto_0

    .line 2257293
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;
    .locals 1

    .prologue
    .line 2257259
    sget-object v0, LX/FZc;->RESULTS_TEXT:LX/FZc;

    invoke-static {p0, v0, p1}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;Z)Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2257246
    if-eqz p2, :cond_0

    .line 2257247
    sget-object v0, LX/FZc;->RESULTS_KEYWORD_TABS:LX/FZc;

    invoke-direct {p0, v0}, LX/FZW;->a(LX/FZc;)LX/FZU;

    move-result-object v0

    .line 2257248
    if-eqz v0, :cond_0

    .line 2257249
    iget-object v1, p0, LX/FZW;->a:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-interface {v0}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2257250
    iget-object v0, p0, LX/FZW;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2257251
    :cond_0
    if-nez p1, :cond_1

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2257252
    :cond_1
    sget-object v0, LX/FZc;->RESULTS_KEYWORD_TABS:LX/FZc;

    invoke-static {p0, v0, p1}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/fragment/tabs/SearchResultsTabsFragment;

    return-object v0
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/suggestions/SuggestionsFragment;
    .locals 2

    .prologue
    .line 2257253
    const/4 v0, 0x0

    .line 2257254
    if-eqz p1, :cond_0

    .line 2257255
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2257256
    const-string v1, "initial_typeahead_query"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2257257
    :cond_0
    sget-object v1, LX/FZc;->SUGGESTIONS:LX/FZc;

    invoke-static {p0, v1, v0}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/suggestions/SuggestionsFragment;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;
    .locals 1

    .prologue
    .line 2257245
    sget-object v0, LX/FZc;->RESULTS_PHOTO:LX/FZc;

    invoke-static {p0, v0, p1}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    return-object v0
.end method

.method public final b()Lcom/facebook/search/suggestions/AwarenessNullStateFragment;
    .locals 2

    .prologue
    .line 2257244
    sget-object v0, LX/FZc;->AWARENESS_NULL_STATE:LX/FZc;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/suggestions/AwarenessNullStateFragment;

    return-object v0
.end method

.method public final c()Lcom/facebook/search/logging/api/SearchTypeaheadSession;
    .locals 1

    .prologue
    .line 2257242
    sget-object v0, LX/FZc;->SUGGESTIONS:LX/FZc;

    invoke-direct {p0, v0}, LX/FZW;->a(LX/FZc;)LX/FZU;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2257243
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->k()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    goto :goto_0
.end method

.method public final f(Landroid/os/Bundle;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;
    .locals 1

    .prologue
    .line 2257241
    sget-object v0, LX/FZc;->RESULTS_KEYWORD:LX/FZc;

    invoke-static {p0, v0, p1}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    return-object v0
.end method
