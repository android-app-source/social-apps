.class public final LX/F3I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V
    .locals 0

    .prologue
    .line 2193974
    iput-object p1, p0, LX/F3I;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x753d11c8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193975
    iget-object v1, p0, LX/F3I;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    const/4 v4, 0x0

    .line 2193976
    iget-object v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->p:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 2193977
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2193978
    iget-object v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokens()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_0
    if-ge v5, v8, :cond_0

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8QK;

    .line 2193979
    check-cast v3, LX/F3U;

    .line 2193980
    iget-object p1, v3, LX/F3U;->f:Ljava/lang/String;

    move-object v3, p1

    .line 2193981
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2193982
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 2193983
    :cond_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->s:I

    if-nez v3, :cond_2

    move v3, v4

    .line 2193984
    :goto_1
    move v1, v3

    .line 2193985
    if-eqz v1, :cond_1

    .line 2193986
    iget-object v1, p0, LX/F3I;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-static {v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->r(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    .line 2193987
    :cond_1
    const v1, -0xf59bec5

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2193988
    :cond_2
    iget-object v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->d:LX/F2L;

    iget-object v4, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->p:Ljava/lang/String;

    .line 2193989
    new-instance v5, LX/4Fk;

    invoke-direct {v5}, LX/4Fk;-><init>()V

    .line 2193990
    if-eqz v6, :cond_3

    .line 2193991
    const-string v7, "group_topic_tags"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2193992
    :cond_3
    new-instance v7, LX/F2E;

    invoke-direct {v7, v3}, LX/F2E;-><init>(LX/F2L;)V

    .line 2193993
    invoke-static {v3, v4, v5, v7}, LX/F2L;->a(LX/F2L;Ljava/lang/String;LX/4Fk;LX/0TF;)V

    .line 2193994
    :cond_4
    const/4 v3, 0x1

    goto :goto_1
.end method
