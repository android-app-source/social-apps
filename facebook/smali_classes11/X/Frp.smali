.class public final LX/Frp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "LX/FsK;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G1I;

.field public final synthetic b:LX/G4x;

.field public final synthetic c:LX/FqO;

.field public final synthetic d:LX/5SB;

.field public final synthetic e:LX/Frv;

.field private f:LX/0ta;


# direct methods
.method public constructor <init>(LX/Frv;LX/G1I;LX/G4x;LX/FqO;LX/5SB;)V
    .locals 1

    .prologue
    .line 2296097
    iput-object p1, p0, LX/Frp;->e:LX/Frv;

    iput-object p2, p0, LX/Frp;->a:LX/G1I;

    iput-object p3, p0, LX/Frp;->b:LX/G4x;

    iput-object p4, p0, LX/Frp;->c:LX/FqO;

    iput-object p5, p0, LX/Frp;->d:LX/5SB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296098
    sget-object v0, LX/0ta;->NO_DATA:LX/0ta;

    iput-object v0, p0, LX/Frp;->f:LX/0ta;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2296099
    iget-object v0, p0, LX/Frp;->a:LX/G1I;

    invoke-virtual {v0}, LX/G1I;->a()V

    .line 2296100
    iget-object v0, p0, LX/Frp;->b:LX/G4x;

    const/4 v1, 0x1

    .line 2296101
    iput-boolean v1, v0, LX/G4x;->f:Z

    .line 2296102
    iget-object v0, p0, LX/Frp;->c:LX/FqO;

    invoke-interface {v0}, LX/FqO;->lo_()V

    .line 2296103
    iget-object v0, p0, LX/Frp;->e:LX/Frv;

    iget-object v0, v0, LX/Frv;->a:LX/BQB;

    iget-object v1, p0, LX/Frp;->f:LX/0ta;

    const/4 p0, 0x1

    .line 2296104
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v2, v1}, LX/0ta;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2296105
    iget-object v2, v0, LX/BQB;->c:LX/BQD;

    const-string v3, "ProtilesNetworkFetch"

    invoke-virtual {v2, v3}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2296106
    :goto_0
    iget-boolean v2, v0, LX/BQB;->B:Z

    if-eqz v2, :cond_0

    .line 2296107
    iget-object v2, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x1a0028

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2296108
    iget-object v2, v0, LX/BQB;->c:LX/BQD;

    const-string v3, "ProtilesWaitTime"

    invoke-virtual {v2, v3}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2296109
    iput-boolean p0, v0, LX/BQB;->x:Z

    .line 2296110
    invoke-static {v0}, LX/BQB;->w(LX/BQB;)V

    .line 2296111
    :cond_0
    iput-boolean p0, v0, LX/BQB;->q:Z

    .line 2296112
    return-void

    .line 2296113
    :cond_1
    iget-object v2, v0, LX/BQB;->c:LX/BQD;

    const-string v3, "ProtilesNetworkFetch"

    invoke-virtual {v2, v3}, LX/BQD;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2296114
    check-cast p1, LX/FsK;

    .line 2296115
    iget-object v0, p1, LX/FsK;->b:LX/0ta;

    iput-object v0, p0, LX/Frp;->f:LX/0ta;

    .line 2296116
    iget-object v0, p0, LX/Frp;->a:LX/G1I;

    iget-object v1, p0, LX/Frp;->d:LX/5SB;

    .line 2296117
    iget-wide v6, v1, LX/5SB;->b:J

    move-wide v2, v6

    .line 2296118
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, LX/FsK;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Frp;->e:LX/Frv;

    iget-object v4, v4, LX/Frv;->e:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, LX/G1I;->a(Ljava/lang/String;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;ZJ)V

    .line 2296119
    iget-object v0, p0, LX/Frp;->c:LX/FqO;

    invoke-interface {v0}, LX/FqO;->lo_()V

    .line 2296120
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2296121
    iget-object v0, p0, LX/Frp;->a:LX/G1I;

    .line 2296122
    iget-object v1, v0, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2296123
    :goto_0
    iget-object v0, p0, LX/Frp;->b:LX/G4x;

    const/4 v1, 0x1

    .line 2296124
    iput-boolean v1, v0, LX/G4x;->f:Z

    .line 2296125
    iget-object v0, p0, LX/Frp;->c:LX/FqO;

    invoke-interface {v0}, LX/FqO;->lo_()V

    .line 2296126
    iget-object v0, p0, LX/Frp;->e:LX/Frv;

    iget-object v0, v0, LX/Frv;->a:LX/BQB;

    .line 2296127
    iget-object v1, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p0, 0x1a0028

    const/4 p1, 0x3

    invoke-interface {v1, p0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2296128
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string p0, "ProtilesNetworkFetch"

    invoke-virtual {v1, p0}, LX/BQD;->d(Ljava/lang/String;)V

    .line 2296129
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string p0, "ProtilesWaitTime"

    invoke-virtual {v1, p0}, LX/BQD;->d(Ljava/lang/String;)V

    .line 2296130
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BQB;->r:Z

    .line 2296131
    invoke-static {v0}, LX/BQB;->w(LX/BQB;)V

    .line 2296132
    return-void

    .line 2296133
    :cond_0
    iget-object v1, v0, LX/G1I;->b:LX/G1M;

    sget-object p1, LX/G1L;->FAILED:LX/G1L;

    .line 2296134
    iput-object p1, v1, LX/G1M;->a:LX/G1L;

    .line 2296135
    goto :goto_0
.end method
