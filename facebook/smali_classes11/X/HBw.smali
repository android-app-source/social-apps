.class public final LX/HBw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/editpage/EditPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/editpage/EditPageFragment;)V
    .locals 0

    .prologue
    .line 2438001
    iput-object p1, p0, LX/HBw;->a:Lcom/facebook/pages/common/editpage/EditPageFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2438002
    iget-object v0, p0, LX/HBw;->a:Lcom/facebook/pages/common/editpage/EditPageFragment;

    const/4 v1, 0x1

    .line 2438003
    iput-boolean v1, v0, Lcom/facebook/pages/common/editpage/EditPageFragment;->n:Z

    .line 2438004
    iget-object v0, p0, LX/HBw;->a:Lcom/facebook/pages/common/editpage/EditPageFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/common/editpage/EditPageFragment;->a$redex0(Lcom/facebook/pages/common/editpage/EditPageFragment;Z)V

    .line 2438005
    iget-object v0, p0, LX/HBw;->a:Lcom/facebook/pages/common/editpage/EditPageFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/editpage/EditPageFragment;->b:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/editpage/EditPageFragment;->e:Ljava/lang/String;

    const-string v2, "fail to load edit page data"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2438006
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2438007
    check-cast p1, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    .line 2438008
    iget-object v0, p0, LX/HBw;->a:Lcom/facebook/pages/common/editpage/EditPageFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/common/editpage/EditPageFragment;->a$redex0(Lcom/facebook/pages/common/editpage/EditPageFragment;Z)V

    .line 2438009
    iget-object v0, p0, LX/HBw;->a:Lcom/facebook/pages/common/editpage/EditPageFragment;

    .line 2438010
    iget-object v1, v0, Lcom/facebook/pages/common/editpage/EditPageFragment;->k:LX/HCC;

    invoke-virtual {v1, p1}, LX/HCC;->a(Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;)V

    .line 2438011
    return-void
.end method
