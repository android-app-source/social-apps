.class public LX/Fqd;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Fqc;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294295
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2294296
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/BQ9;LX/5SB;LX/Fy4;LX/BQ1;Ljava/lang/String;)LX/Fqc;
    .locals 30

    .prologue
    .line 2294297
    new-instance v1, LX/Fqc;

    const/16 v2, 0xa7c

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 v2, 0x3645

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v2, 0x97

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v2, 0x1430

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v2, 0xbc6

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    const/16 v2, 0x36f0

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    const/16 v2, 0x36e3

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v2, 0x36df

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    const/16 v2, 0x2eb

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v2, 0x2b2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v2, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    const-class v2, LX/Fqp;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/Fqp;

    const/16 v2, 0x3622

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v21

    check-cast v21, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x3688

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v2, 0x3689

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v2, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    invoke-static/range {p0 .. p0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v25

    check-cast v25, LX/Fsr;

    invoke-static/range {p0 .. p0}, Lcom/facebook/timeline/services/ProfileActionClient;->a(LX/0QB;)Lcom/facebook/timeline/services/ProfileActionClient;

    move-result-object v26

    check-cast v26, Lcom/facebook/timeline/services/ProfileActionClient;

    const/16 v2, 0x225c

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v28

    check-cast v28, Ljava/lang/Boolean;

    const/16 v2, 0xc49

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v29

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v29}, LX/Fqc;-><init>(Landroid/content/Context;LX/BQ9;LX/5SB;LX/Fy4;LX/BQ1;Ljava/lang/String;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/Fqp;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0Ot;LX/Fsr;Lcom/facebook/timeline/services/ProfileActionClient;LX/0Ot;Ljava/lang/Boolean;LX/0Ot;)V

    .line 2294298
    return-object v1
.end method
