.class public final LX/GzI;
.super LX/Gsi;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2411541
    const-string v0, "oauth"

    invoke-direct {p0, p1, p2, v0, p3}, LX/Gsi;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2411542
    return-void
.end method


# virtual methods
.method public final a()LX/GsI;
    .locals 6

    .prologue
    .line 2411543
    iget-object v0, p0, LX/Gsi;->f:Landroid/os/Bundle;

    move-object v3, v0

    .line 2411544
    const-string v0, "redirect_uri"

    const-string v1, "fbconnect://success"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411545
    const-string v0, "client_id"

    .line 2411546
    iget-object v1, p0, LX/Gsi;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2411547
    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411548
    const-string v0, "e2e"

    iget-object v1, p0, LX/GzI;->a:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411549
    const-string v0, "response_type"

    const-string v1, "token,signed_request"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411550
    const-string v0, "return_scopes"

    const-string v1, "true"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411551
    iget-boolean v0, p0, LX/GzI;->b:Z

    if-eqz v0, :cond_0

    .line 2411552
    const-string v0, "auth_type"

    const-string v1, "rerequest"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411553
    :cond_0
    new-instance v0, LX/GsI;

    .line 2411554
    iget-object v1, p0, LX/Gsi;->a:Landroid/content/Context;

    move-object v1, v1

    .line 2411555
    const-string v2, "oauth"

    .line 2411556
    iget v4, p0, LX/Gsi;->d:I

    move v4, v4

    .line 2411557
    iget-object v5, p0, LX/Gsi;->e:LX/GsB;

    move-object v5, v5

    .line 2411558
    invoke-direct/range {v0 .. v5}, LX/GsI;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILX/GsB;)V

    return-object v0
.end method
