.class public final LX/FF8;
.super LX/FF7;
.source ""


# instance fields
.field private final l:Lcom/facebook/widget/text/BetterTextView;

.field private final m:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2216915
    invoke-direct {p0, p1}, LX/FF7;-><init>(Landroid/view/View;)V

    .line 2216916
    const v0, 0x7f0d13c6

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/FF8;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 2216917
    const v0, 0x7f0d13c7

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/FF8;->m:Landroid/view/View;

    .line 2216918
    return-void
.end method


# virtual methods
.method public final a(ILX/FFD;LX/FF9;)V
    .locals 2

    .prologue
    .line 2216919
    instance-of v0, p2, LX/FFG;

    if-nez v0, :cond_0

    .line 2216920
    :goto_0
    return-void

    .line 2216921
    :cond_0
    check-cast p2, LX/FFG;

    .line 2216922
    iget-object v0, p0, LX/FF8;->l:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p2, LX/FFG;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216923
    iget-object v1, p0, LX/FF8;->m:Landroid/view/View;

    iget-boolean v0, p2, LX/FFG;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method
