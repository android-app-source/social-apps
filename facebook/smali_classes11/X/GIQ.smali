.class public final LX/GIQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GIT;


# direct methods
.method public constructor <init>(LX/GIT;)V
    .locals 0

    .prologue
    .line 2336612
    iput-object p1, p0, LX/GIQ;->a:LX/GIT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2336583
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 2336584
    :goto_0
    return-void

    .line 2336585
    :cond_0
    const-string v0, "audience_extra"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2336586
    const-string v1, "targeting_data_extra"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2336587
    if-eqz v1, :cond_1

    .line 2336588
    iget-object v3, p0, LX/GIQ;->a:LX/GIT;

    iget-object v3, v3, LX/GIT;->d:LX/GLd;

    invoke-virtual {v3, v1}, LX/GIr;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2336589
    :cond_1
    if-eqz v0, :cond_3

    .line 2336590
    iget-object v1, p0, LX/GIQ;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2336591
    iget-object v3, p0, LX/GIQ;->a:LX/GIT;

    iget-object v3, v3, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v4, p0, LX/GIQ;->a:LX/GIT;

    iget-boolean v4, v4, LX/GIT;->g:Z

    invoke-static {v3, v0, v4}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;Z)V

    .line 2336592
    iget-object v3, p0, LX/GIQ;->a:LX/GIT;

    iget-object v3, v3, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v1, v1, 0x1

    if-ne v3, v1, :cond_2

    .line 2336593
    iget-object v1, p0, LX/GIQ;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v3, p0, LX/GIQ;->a:LX/GIT;

    iget-object v3, v3, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d(I)V

    .line 2336594
    :cond_2
    iget-object v1, p0, LX/GIQ;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2336595
    iget-object v0, p0, LX/GIQ;->a:LX/GIT;

    iget-object v1, p0, LX/GIQ;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2336596
    iput v1, v0, LX/GIT;->i:I

    .line 2336597
    iget-object v0, p0, LX/GIQ;->a:LX/GIT;

    iget-object v0, v0, LX/GIT;->b:LX/GLb;

    iget-object v1, p0, LX/GIQ;->a:LX/GIT;

    iget v1, v1, LX/GIT;->i:I

    invoke-virtual {v0, v2, v1}, LX/GLb;->a(II)V

    .line 2336598
    :cond_3
    iget-object v0, p0, LX/GIQ;->a:LX/GIT;

    iget-object v1, v0, LX/GIT;->b:LX/GLb;

    iget-object v0, p0, LX/GIQ;->a:LX/GIT;

    iget-boolean v0, v0, LX/GIT;->h:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/GIQ;->a:LX/GIT;

    iget-object v0, v0, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2336599
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v0, v3

    .line 2336600
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v3, :cond_5

    :cond_4
    move v0, v2

    .line 2336601
    :goto_1
    iput-boolean v0, v1, LX/GLb;->n:Z

    .line 2336602
    iget-object v0, p0, LX/GIQ;->a:LX/GIT;

    iget-object v0, v0, LX/GIT;->b:LX/GLb;

    invoke-virtual {v0}, LX/GLb;->b()V

    .line 2336603
    iget-object v0, p0, LX/GIQ;->a:LX/GIT;

    .line 2336604
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2336605
    new-instance v1, LX/GFA;

    invoke-direct {v1}, LX/GFA;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2336606
    iget-object v0, p0, LX/GIQ;->a:LX/GIT;

    iget-object v0, v0, LX/GIT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    iget-object v1, p0, LX/GIQ;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2336607
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v2

    .line 2336608
    iget-object v2, p0, LX/GIQ;->a:LX/GIT;

    iget-object v2, v2, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2336609
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v2, v3

    .line 2336610
    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->b(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2336611
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method
