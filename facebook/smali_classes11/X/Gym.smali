.class public final enum LX/Gym;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gym;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gym;

.field public static final enum CONTENT:LX/Gym;

.field public static final enum ERROR:LX/Gym;

.field public static final enum LOADING:LX/Gym;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2410360
    new-instance v0, LX/Gym;

    const-string v1, "CONTENT"

    invoke-direct {v0, v1, v2}, LX/Gym;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gym;->CONTENT:LX/Gym;

    .line 2410361
    new-instance v0, LX/Gym;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/Gym;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gym;->LOADING:LX/Gym;

    .line 2410362
    new-instance v0, LX/Gym;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/Gym;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gym;->ERROR:LX/Gym;

    .line 2410363
    const/4 v0, 0x3

    new-array v0, v0, [LX/Gym;

    sget-object v1, LX/Gym;->CONTENT:LX/Gym;

    aput-object v1, v0, v2

    sget-object v1, LX/Gym;->LOADING:LX/Gym;

    aput-object v1, v0, v3

    sget-object v1, LX/Gym;->ERROR:LX/Gym;

    aput-object v1, v0, v4

    sput-object v0, LX/Gym;->$VALUES:[LX/Gym;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2410364
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gym;
    .locals 1

    .prologue
    .line 2410365
    const-class v0, LX/Gym;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gym;

    return-object v0
.end method

.method public static values()[LX/Gym;
    .locals 1

    .prologue
    .line 2410366
    sget-object v0, LX/Gym;->$VALUES:[LX/Gym;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gym;

    return-object v0
.end method
