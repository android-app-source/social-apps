.class public final LX/FQ6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

.field public final synthetic b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V
    .locals 0

    .prologue
    .line 2238452
    iput-object p1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iput-object p2, p0, LX/FQ6;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x4556bdb7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2238437
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v1, v2, :cond_1

    .line 2238438
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v2, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v2, v2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->L:Ljava/lang/String;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-static {v1, v2, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a$redex0(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 2238439
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238440
    iput-object v2, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238441
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    iget-object v2, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getUnsavedDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2238442
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->K:LX/FPC;

    if-eqz v1, :cond_0

    .line 2238443
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->K:LX/FPC;

    iget-object v2, p0, LX/FQ6;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-virtual {v1, v2}, LX/FPC;->b(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V

    .line 2238444
    :cond_0
    :goto_0
    const v1, -0x23cb7f14

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2238445
    :cond_1
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v1, v2, :cond_0

    .line 2238446
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v2, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v2, v2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->L:Ljava/lang/String;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-static {v1, v2, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a$redex0(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 2238447
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238448
    iput-object v2, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238449
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    iget-object v2, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-virtual {v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getSavedDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2238450
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->K:LX/FPC;

    if-eqz v1, :cond_0

    .line 2238451
    iget-object v1, p0, LX/FQ6;->b:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->K:LX/FPC;

    iget-object v2, p0, LX/FQ6;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-virtual {v1, v2}, LX/FPC;->a(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V

    goto :goto_0
.end method
