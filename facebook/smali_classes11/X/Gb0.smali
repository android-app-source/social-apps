.class public LX/Gb0;
.super LX/4ok;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2368777
    invoke-direct {p0, p1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2368778
    sget-object v0, LX/Gb1;->a:LX/0Tn;

    invoke-virtual {p0, v0}, LX/4oi;->a(LX/0Tn;)V

    .line 2368779
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Gb0;->setDefaultValue(Ljava/lang/Object;)V

    .line 2368780
    const-string v0, "Enable UIMonitor"

    invoke-virtual {p0, v0}, LX/Gb0;->setTitle(Ljava/lang/CharSequence;)V

    .line 2368781
    const-string v0, "Shows what Views request layout and get repainted. Requires restart."

    invoke-virtual {p0, v0}, LX/Gb0;->setSummary(Ljava/lang/CharSequence;)V

    .line 2368782
    return-void
.end method
