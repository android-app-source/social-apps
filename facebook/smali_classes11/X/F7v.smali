.class public final enum LX/F7v;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F7v;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F7v;

.field public static final enum FRIENDABLE_CONTACTS:LX/F7v;

.field public static final enum INVITABLE_CONTACTS:LX/F7v;

.field public static final enum LEGAL_SCREEN:LX/F7v;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2202761
    new-instance v0, LX/F7v;

    const-string v1, "LEGAL_SCREEN"

    invoke-direct {v0, v1, v2}, LX/F7v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7v;->LEGAL_SCREEN:LX/F7v;

    .line 2202762
    new-instance v0, LX/F7v;

    const-string v1, "FRIENDABLE_CONTACTS"

    invoke-direct {v0, v1, v3}, LX/F7v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7v;->FRIENDABLE_CONTACTS:LX/F7v;

    .line 2202763
    new-instance v0, LX/F7v;

    const-string v1, "INVITABLE_CONTACTS"

    invoke-direct {v0, v1, v4}, LX/F7v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F7v;->INVITABLE_CONTACTS:LX/F7v;

    .line 2202764
    const/4 v0, 0x3

    new-array v0, v0, [LX/F7v;

    sget-object v1, LX/F7v;->LEGAL_SCREEN:LX/F7v;

    aput-object v1, v0, v2

    sget-object v1, LX/F7v;->FRIENDABLE_CONTACTS:LX/F7v;

    aput-object v1, v0, v3

    sget-object v1, LX/F7v;->INVITABLE_CONTACTS:LX/F7v;

    aput-object v1, v0, v4

    sput-object v0, LX/F7v;->$VALUES:[LX/F7v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2202758
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F7v;
    .locals 1

    .prologue
    .line 2202759
    const-class v0, LX/F7v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F7v;

    return-object v0
.end method

.method public static values()[LX/F7v;
    .locals 1

    .prologue
    .line 2202760
    sget-object v0, LX/F7v;->$VALUES:[LX/F7v;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F7v;

    return-object v0
.end method
