.class public LX/FqU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FqT;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FqU;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2293915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2293916
    return-void
.end method

.method public static a(LX/0QB;)LX/FqU;
    .locals 3

    .prologue
    .line 2293917
    sget-object v0, LX/FqU;->a:LX/FqU;

    if-nez v0, :cond_1

    .line 2293918
    const-class v1, LX/FqU;

    monitor-enter v1

    .line 2293919
    :try_start_0
    sget-object v0, LX/FqU;->a:LX/FqU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2293920
    if-eqz v2, :cond_0

    .line 2293921
    :try_start_1
    invoke-static {}, LX/FqU;->b()LX/FqU;

    move-result-object v0

    sput-object v0, LX/FqU;->a:LX/FqU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2293922
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2293923
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2293924
    :cond_1
    sget-object v0, LX/FqU;->a:LX/FqU;

    return-object v0

    .line 2293925
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2293926
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b()LX/FqU;
    .locals 1

    .prologue
    .line 2293927
    new-instance v0, LX/FqU;

    invoke-direct {v0}, LX/FqU;-><init>()V

    .line 2293928
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2293929
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293934
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2293930
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293931
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2293932
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293933
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293913
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293914
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293902
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293903
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293904
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293905
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293906
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293907
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293908
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293909
    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293910
    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293911
    return-void
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2293912
    return-void
.end method
