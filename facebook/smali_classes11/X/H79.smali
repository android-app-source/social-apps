.class public final LX/H79;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2428283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;
    .locals 14

    .prologue
    .line 2428284
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2428285
    iget-object v1, p0, LX/H79;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2428286
    iget-object v2, p0, LX/H79;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2428287
    iget-object v2, p0, LX/H79;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2428288
    iget-object v2, p0, LX/H79;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2428289
    iget-object v2, p0, LX/H79;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2428290
    iget-object v2, p0, LX/H79;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2428291
    iget-object v2, p0, LX/H79;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2428292
    iget-object v2, p0, LX/H79;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2428293
    iget-object v2, p0, LX/H79;->l:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2428294
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2428295
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428296
    const/4 v1, 0x1

    iget-wide v2, p0, LX/H79;->b:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2428297
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2428298
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2428299
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2428300
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2428301
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2428302
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/H79;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2428303
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2428304
    const/16 v1, 0x9

    iget-boolean v2, p0, LX/H79;->j:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2428305
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 2428306
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 2428307
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2428308
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2428309
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2428310
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2428311
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2428312
    new-instance v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;

    invoke-direct {v1, v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataWithoutViewModel;-><init>(LX/15i;)V

    .line 2428313
    return-object v1
.end method
