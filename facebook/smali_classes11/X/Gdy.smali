.class public final LX/Gdy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

.field public final synthetic b:LX/Ge0;


# direct methods
.method public constructor <init>(LX/Ge0;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)V
    .locals 0

    .prologue
    .line 2374837
    iput-object p1, p0, LX/Gdy;->b:LX/Ge0;

    iput-object p2, p0, LX/Gdy;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2374838
    iget-object v0, p0, LX/Gdy;->b:LX/Ge0;

    iget-object v0, v0, LX/Ge0;->a:Ljava/util/Set;

    iget-object v1, p0, LX/Gdy;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2374839
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2374840
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;

    .line 2374841
    iget-object v0, p0, LX/Gdy;->b:LX/Ge0;

    iget-object v0, v0, LX/Ge0;->a:Ljava/util/Set;

    iget-object v1, p0, LX/Gdy;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2374842
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2374843
    :cond_0
    iget-object v0, p0, LX/Gdy;->b:LX/Ge0;

    iget-object v0, v0, LX/Ge0;->c:Ljava/util/Set;

    iget-object v1, p0, LX/Gdy;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2374844
    :goto_0
    return-void

    .line 2374845
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2374846
    iget-object v0, p0, LX/Gdy;->b:LX/Ge0;

    iget-object v0, v0, LX/Ge0;->k:LX/99Z;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/99Z;->a(Ljava/lang/String;)V

    .line 2374847
    :cond_2
    iget-object v0, p0, LX/Gdy;->b:LX/Ge0;

    iget-object v0, v0, LX/Ge0;->i:LX/189;

    iget-object v1, p0, LX/Gdy;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v0, p1, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    move-result-object v1

    .line 2374848
    const/4 v0, 0x0

    .line 2374849
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2374850
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStatelessLargeImagePLAsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2374851
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->u()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 2374852
    iget-object v3, p0, LX/Gdy;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-static {v3}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v3

    iget-object v4, p0, LX/Gdy;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->I_()I

    move-result v4

    .line 2374853
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "pyml_items_fetch"

    invoke-direct {v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {v5, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "inf_hscroll_user_pos"

    invoke-virtual {v5, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "inf_hscroll_fetch_size"

    invoke-virtual {v5, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "inf_hscroll_final_size"

    invoke-virtual {v5, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p1, "native_newsfeed"

    .line 2374854
    iput-object p1, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2374855
    move-object v5, v5

    .line 2374856
    move-object v0, v5

    .line 2374857
    iget-object v2, p0, LX/Gdy;->b:LX/Ge0;

    iget-object v2, v2, LX/Ge0;->f:LX/0Zb;

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2374858
    iget-object v0, p0, LX/Gdy;->b:LX/Ge0;

    iget-object v0, v0, LX/Ge0;->h:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v1}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2374859
    iget-object v0, p0, LX/Gdy;->b:LX/Ge0;

    iget-object v0, v0, LX/Ge0;->h:LX/0bH;

    new-instance v2, LX/1Nf;

    invoke-direct {v2, v1}, LX/1Nf;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0
.end method
