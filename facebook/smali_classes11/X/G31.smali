.class public final LX/G31;
.super LX/2Ip;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/2Ip;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/0jW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Pq;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/0jW;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "LX/0jW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2314749
    invoke-direct {p0}, LX/2Ip;-><init>()V

    .line 2314750
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/G31;->a:Ljava/lang/ref/WeakReference;

    .line 2314751
    iput-object p2, p0, LX/G31;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2314752
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/G31;->c:Ljava/lang/ref/WeakReference;

    .line 2314753
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 2314754
    check-cast p1, LX/2f2;

    const/4 v6, 0x0

    .line 2314755
    if-nez p1, :cond_1

    .line 2314756
    :cond_0
    :goto_0
    return-void

    .line 2314757
    :cond_1
    iget-object v0, p0, LX/G31;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Pq;

    .line 2314758
    iget-object v1, p0, LX/G31;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jW;

    .line 2314759
    new-instance v3, LX/2en;

    iget-wide v4, p1, LX/2f2;->a:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/G31;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v3, v2, v4}, LX/2en;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    move-object v2, v0

    .line 2314760
    check-cast v2, LX/1Pr;

    invoke-interface {v2, v3, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2ep;

    iget-object v2, v2, LX/2ep;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2314761
    iget-object v4, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v4, v2, :cond_0

    move-object v2, v0

    .line 2314762
    check-cast v2, LX/1Pr;

    new-instance v4, LX/2ep;

    iget-object v5, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {v4, v5, v6}, LX/2ep;-><init>(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-interface {v2, v3, v4}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2314763
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v6

    invoke-interface {v0, v2}, LX/1Pq;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method
