.class public final LX/Gmh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CqC;


# instance fields
.field public final synthetic a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V
    .locals 0

    .prologue
    .line 2392601
    iput-object p1, p0, LX/Gmh;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 2392602
    iget-object v0, p0, LX/Gmh;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gmh;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    if-nez v0, :cond_1

    .line 2392603
    :cond_0
    :goto_0
    return-void

    .line 2392604
    :cond_1
    iget-object v0, p0, LX/Gmh;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getFragmentCount()I

    move-result v0

    .line 2392605
    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 2392606
    iget-object v0, p0, LX/Gmh;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->setVisibility(I)V

    .line 2392607
    iget-object v0, p0, LX/Gmh;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->L:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2392608
    :cond_2
    iget-object v0, p0, LX/Gmh;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->K:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-virtual {v0, v3}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->setVisibility(I)V

    .line 2392609
    iget-object v0, p0, LX/Gmh;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->L:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
