.class public LX/Fkv;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2279230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static A(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2279228
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    move-result-object v0

    .line 2279229
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;->a()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)I
    .locals 1

    .prologue
    .line 2279226
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279227
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    return v0
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 2279225
    const v0, -0x4e6785e3

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z
    .locals 2

    .prologue
    .line 2279224
    invoke-static {p0}, LX/Fkv;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)I

    move-result v0

    const v1, 0x5e1f75b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z
    .locals 2

    .prologue
    .line 2279223
    invoke-static {p0}, LX/Fkv;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)I

    move-result v0

    const v1, -0x4e6785e3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z
    .locals 1

    .prologue
    .line 2279222
    invoke-static {p0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/Fkv;->z(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2279219
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279220
    invoke-static {p0}, LX/Fkv;->A(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v0

    .line 2279221
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel$PageModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2279209
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279210
    invoke-static {p0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2279211
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279212
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2279213
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2279214
    :goto_1
    return-object v0

    .line 2279215
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2279216
    :cond_1
    invoke-static {p0}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2279217
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->t()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2279218
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static i(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2279193
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279194
    invoke-static {p0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2279195
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279196
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    .line 2279197
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2279198
    const-class v4, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-virtual {v3, v2, v1, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    if-eqz v0, :cond_5

    .line 2279199
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    .line 2279200
    :goto_0
    return-object v0

    .line 2279201
    :cond_1
    invoke-static {p0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2279202
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2279203
    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_2

    .line 2279204
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2279205
    const-class v4, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-virtual {v3, v2, v1, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    if-eqz v2, :cond_2

    move v0, v1

    :cond_2
    if-eqz v0, :cond_5

    .line 2279206
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    goto :goto_0

    :cond_3
    move v2, v0

    .line 2279207
    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_1

    .line 2279208
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Landroid/graphics/PointF;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2279231
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279232
    const/4 v3, 0x0

    .line 2279233
    invoke-static {p0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2279234
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279235
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2279236
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2279237
    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_8

    .line 2279238
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v4

    .line 2279239
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v5, v0, v2}, LX/15i;->g(II)I

    move-result v6

    .line 2279240
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {v3, v4, v2}, LX/15i;->l(II)D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v5, v6, v1}, LX/15i;->l(II)D

    move-result-wide v4

    double-to-float v1, v4

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2279241
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 2279242
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2279243
    :cond_2
    invoke-static {p0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2279244
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2279245
    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    .line 2279246
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2279247
    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    if-eqz v0, :cond_7

    .line 2279248
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v4

    .line 2279249
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->q()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v5, v0, v2}, LX/15i;->g(II)I

    move-result v6

    .line 2279250
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {v3, v4, v2}, LX/15i;->l(II)D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v5, v6, v1}, LX/15i;->l(II)D

    move-result-wide v4

    double-to-float v1, v4

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2279251
    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move-object v0, v3

    goto :goto_1

    :cond_8
    move-object v0, v3

    goto :goto_1
.end method

.method public static n(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2279185
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279186
    invoke-static {p0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2279187
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279188
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2279189
    :goto_0
    return-object v0

    .line 2279190
    :cond_0
    invoke-static {p0}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2279191
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->A()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2279192
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z
    .locals 1

    .prologue
    .line 2279183
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279184
    invoke-static {p0}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z
    .locals 1

    .prologue
    .line 2279181
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279182
    invoke-static {p0}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2279175
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279176
    invoke-static {p0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 2279177
    const/4 v0, 0x0

    .line 2279178
    :goto_1
    return-object v0

    .line 2279179
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2279180
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static r(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z
    .locals 1

    .prologue
    .line 2279173
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279174
    invoke-static {p0}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z
    .locals 1

    .prologue
    .line 2279171
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279172
    invoke-static {p0}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static t(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2279167
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279168
    invoke-static {p0}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2279169
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2279170
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .locals 1

    .prologue
    .line 2279163
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279164
    invoke-static {p0}, LX/Fkv;->d(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2279165
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->z()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v0

    .line 2279166
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V
    .locals 1

    .prologue
    .line 2279155
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279156
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279157
    return-void
.end method

.method public static w(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)I
    .locals 1

    .prologue
    .line 2279159
    invoke-static {p0}, LX/Fkv;->v(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)V

    .line 2279160
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279161
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279162
    invoke-virtual {p0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    return v0
.end method

.method public static z(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z
    .locals 2

    .prologue
    .line 2279158
    invoke-static {p0}, LX/Fkv;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)I

    move-result v0

    const v1, 0x23637cfe

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
