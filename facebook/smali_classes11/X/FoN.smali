.class public final LX/FoN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V
    .locals 0

    .prologue
    .line 2289343
    iput-object p1, p0, LX/FoN;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x16b80ec3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2289344
    iget-object v1, p0, LX/FoN;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->c:LX/0Zb;

    iget-object v2, p0, LX/FoN;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    invoke-static {v2}, LX/BOe;->n(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2289345
    iget-object v1, p0, LX/FoN;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    const/4 v5, 0x0

    .line 2289346
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-static {v2}, LX/FoJ;->a(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;)I

    move-result v2

    .line 2289347
    const v4, 0x5e1f75b

    if-ne v2, v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    move v4, v4

    .line 2289348
    if-eqz v4, :cond_0

    .line 2289349
    const-string v2, "fundraiserCampaignShare"

    .line 2289350
    iget-object v4, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->o:Ljava/lang/String;

    const p0, 0x4462365a

    invoke-static {v4, p0}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    move-object p0, v4

    move-object v4, v2

    .line 2289351
    :goto_1
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Kf;

    sget-object p1, LX/21D;->FUNDRAISER_THANK_YOU_PAGE:LX/21D;

    invoke-static {p0}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object p0

    invoke-virtual {p0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object p0

    invoke-static {p1, v4, p0}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    const/16 p0, 0x2766

    invoke-interface {v2, v5, v4, p0, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2289352
    const v1, -0x23e0097b

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2289353
    :cond_0
    const v4, -0x4e6785e3

    if-ne v2, v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    move v2, v4

    .line 2289354
    if-eqz v2, :cond_1

    .line 2289355
    const-string v2, "fundraiserPersonToCharityShare"

    .line 2289356
    iget-object v4, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->o:Ljava/lang/String;

    const p0, -0x4e6785e3

    invoke-static {v4, p0}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    move-object p0, v4

    move-object v4, v2

    goto :goto_1

    :cond_1
    move-object v4, v5

    move-object p0, v5

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method
