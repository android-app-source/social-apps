.class public final LX/FH2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 2219726
    iput-object p1, p0, LX/FH2;->b:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FH2;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2219727
    iget-object v0, p0, LX/FH2;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219728
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2219729
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2219730
    iget-object v2, p0, LX/FH2;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2219731
    iget-object v1, v1, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    .line 2219732
    if-eqz v1, :cond_0

    iget-object v2, p0, LX/FH2;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2219733
    new-instance v0, LX/5zn;

    invoke-direct {v0}, LX/5zn;-><init>()V

    iget-object v2, p0, LX/FH2;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v2}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    .line 2219734
    iput-object v1, v0, LX/5zn;->q:Ljava/lang/String;

    .line 2219735
    move-object v0, v0

    .line 2219736
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2219737
    :cond_0
    return-object v0
.end method
