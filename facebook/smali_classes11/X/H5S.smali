.class public final LX/H5S;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/H5T;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public final synthetic b:LX/H5T;


# direct methods
.method public constructor <init>(LX/H5T;)V
    .locals 1

    .prologue
    .line 2424357
    iput-object p1, p0, LX/H5S;->b:LX/H5T;

    .line 2424358
    move-object v0, p1

    .line 2424359
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2424360
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2424361
    const-string v0, "NotificationsFriendingBucketFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2424362
    if-ne p0, p1, :cond_1

    .line 2424363
    :cond_0
    :goto_0
    return v0

    .line 2424364
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2424365
    goto :goto_0

    .line 2424366
    :cond_3
    check-cast p1, LX/H5S;

    .line 2424367
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2424368
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2424369
    if-eq v2, v3, :cond_0

    .line 2424370
    iget v2, p0, LX/H5S;->a:I

    iget v3, p1, LX/H5S;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2424371
    goto :goto_0
.end method
