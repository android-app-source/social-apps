.class public final LX/Ftb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2298904
    iput-object p1, p0, LX/Ftb;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    iput-object p2, p0, LX/Ftb;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x77d7f690

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2298888
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;

    .line 2298889
    if-eqz v0, :cond_0

    .line 2298890
    const/4 v2, -0x1

    iget-object v4, p0, LX/Ftb;->a:Landroid/net/Uri;

    .line 2298891
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 2298892
    new-instance v7, LX/74k;

    invoke-direct {v7}, LX/74k;-><init>()V

    new-instance p0, LX/4gN;

    invoke-direct {p0}, LX/4gN;-><init>()V

    new-instance p1, LX/4gP;

    invoke-direct {p1}, LX/4gP;-><init>()V

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object p1

    sget-object v3, LX/4gQ;->Photo:LX/4gQ;

    invoke-virtual {p1, v3}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object p1

    invoke-virtual {p1, v4}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object p1

    invoke-virtual {p1}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object p0

    invoke-virtual {p0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object p0

    .line 2298893
    iput-object p0, v7, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2298894
    move-object v7, v7

    .line 2298895
    invoke-virtual {v7}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v7

    .line 2298896
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 2298897
    invoke-virtual {p0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2298898
    const-string v7, "extra_media_items"

    invoke-virtual {v6, v7, p0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2298899
    move-object v6, v6

    .line 2298900
    move-object v3, v6

    .line 2298901
    invoke-virtual {v0, v2, v3}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2298902
    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->finish()V

    .line 2298903
    :cond_0
    const v0, 0x1f2ecf47

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
