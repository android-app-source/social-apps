.class public LX/FQg;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FQg;


# instance fields
.field private final a:LX/FQf;


# direct methods
.method public constructor <init>(LX/FQf;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2239590
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2239591
    iput-object p1, p0, LX/FQg;->a:LX/FQf;

    .line 2239592
    sget-object v0, LX/0ax;->bf:Ljava/lang/String;

    const-string v1, "com.facebook.katana.profile.id"

    .line 2239593
    new-instance v2, Ljava/lang/StringBuilder;

    const-string p1, "{#"

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string p1, "}"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2239594
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/FQg;->a:LX/FQf;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2239595
    return-void
.end method

.method public static a(LX/0QB;)LX/FQg;
    .locals 7

    .prologue
    .line 2239597
    sget-object v0, LX/FQg;->b:LX/FQg;

    if-nez v0, :cond_1

    .line 2239598
    const-class v1, LX/FQg;

    monitor-enter v1

    .line 2239599
    :try_start_0
    sget-object v0, LX/FQg;->b:LX/FQg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2239600
    if-eqz v2, :cond_0

    .line 2239601
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2239602
    new-instance v4, LX/FQg;

    .line 2239603
    new-instance p0, LX/FQf;

    invoke-static {v0}, LX/FQY;->a(LX/0QB;)LX/FQY;

    move-result-object v3

    check-cast v3, LX/FQY;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v6

    check-cast v6, LX/17T;

    invoke-direct {p0, v3, v5, v6}, LX/FQf;-><init>(LX/FQY;LX/0ad;LX/17T;)V

    .line 2239604
    move-object v3, p0

    .line 2239605
    check-cast v3, LX/FQf;

    invoke-direct {v4, v3}, LX/FQg;-><init>(LX/FQf;)V

    .line 2239606
    move-object v0, v4

    .line 2239607
    sput-object v0, LX/FQg;->b:LX/FQg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2239608
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2239609
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2239610
    :cond_1
    sget-object v0, LX/FQg;->b:LX/FQg;

    return-object v0

    .line 2239611
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2239612
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2239596
    const/4 v0, 0x1

    return v0
.end method
