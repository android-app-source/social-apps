.class public final LX/GSy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/appinvites/ui/AppInviteContentView;


# direct methods
.method public constructor <init>(Lcom/facebook/appinvites/ui/AppInviteContentView;)V
    .locals 0

    .prologue
    .line 2354981
    iput-object p1, p0, LX/GSy;->a:Lcom/facebook/appinvites/ui/AppInviteContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 6

    .prologue
    .line 2354982
    iget-object v0, p0, LX/GSy;->a:Lcom/facebook/appinvites/ui/AppInviteContentView;

    iget-object v0, v0, Lcom/facebook/appinvites/ui/AppInviteContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2354983
    iget-object v1, p0, LX/GSy;->a:Lcom/facebook/appinvites/ui/AppInviteContentView;

    iget-object v1, v1, Lcom/facebook/appinvites/ui/AppInviteContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getWidth()I

    move-result v1

    int-to-double v2, v1

    const-wide v4, 0x3fe0bf258bf258bfL    # 0.5233333333333333

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2354984
    iget-object v1, p0, LX/GSy;->a:Lcom/facebook/appinvites/ui/AppInviteContentView;

    iget-object v1, v1, Lcom/facebook/appinvites/ui/AppInviteContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2354985
    return-void
.end method
