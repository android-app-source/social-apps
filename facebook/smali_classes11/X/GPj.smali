.class public LX/GPj;
.super LX/6sU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sU",
        "<",
        "Lcom/facebook/common/util/Quartet",
        "<",
        "Ljava/lang/String;",
        "+",
        "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
        "Lcom/facebook/payments/currency/CurrencyAmount;",
        "Ljava/lang/Boolean;",
        ">;",
        "Lcom/facebook/adspayments/protocol/CvvPrepayData;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/GPj;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349399
    const-class v0, Lcom/facebook/adspayments/protocol/CvvPrepayData;

    invoke-direct {p0, p1, v0}, LX/6sU;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2349400
    return-void
.end method

.method public static a(LX/0QB;)LX/GPj;
    .locals 4

    .prologue
    .line 2349386
    sget-object v0, LX/GPj;->c:LX/GPj;

    if-nez v0, :cond_1

    .line 2349387
    const-class v1, LX/GPj;

    monitor-enter v1

    .line 2349388
    :try_start_0
    sget-object v0, LX/GPj;->c:LX/GPj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2349389
    if-eqz v2, :cond_0

    .line 2349390
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2349391
    new-instance p0, LX/GPj;

    invoke-static {v0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {p0, v3}, LX/GPj;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349392
    move-object v0, p0

    .line 2349393
    sput-object v0, LX/GPj;->c:LX/GPj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2349394
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2349395
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2349396
    :cond_1
    sget-object v0, LX/GPj;->c:LX/GPj;

    return-object v0

    .line 2349397
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2349398
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/0lF;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 4

    .prologue
    .line 2349401
    new-instance v0, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v1, "currency"

    invoke-static {p0, v1}, LX/6sU;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "offsetted_amount"

    invoke-static {p0, v2}, LX/6sU;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 11

    .prologue
    .line 2349359
    check-cast p1, Lcom/facebook/common/util/Quartet;

    .line 2349360
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v10, v0

    check-cast v10, Ljava/lang/String;

    .line 2349361
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v3, v0

    check-cast v3, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    .line 2349362
    iget-object v0, p1, Lcom/facebook/common/util/Triplet;->b:Ljava/lang/Object;

    move-object v7, v0

    check-cast v7, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2349363
    const-string v0, "credential_id"

    invoke-interface {v3}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "payment_method_type"

    invoke-interface {v3}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->e()LX/6zP;

    move-result-object v3

    invoke-interface {v3}, LX/6LU;->getValue()Ljava/lang/Object;

    move-result-object v3

    const-string v4, "budget_currency"

    .line 2349364
    iget-object v5, v7, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2349365
    const-string v6, "budget_amount"

    .line 2349366
    iget-object v8, v7, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v7, v8

    .line 2349367
    invoke-virtual {v7}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "budget_type"

    iget-object v9, p1, Lcom/facebook/common/util/Quartet;->a:Ljava/lang/Object;

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "daily_budget"

    :goto_0
    invoke-static/range {v0 .. v9}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 2349368
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "prepay_details"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2349369
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2349370
    const-string v1, "."

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2349371
    :cond_0
    const-string v9, "lifetime_budget"

    goto :goto_0

    .line 2349372
    :cond_1
    const-string v0, "{min_acceptable_amount, max_acceptable_amount, default_funding_amount, should_collect_business_details}"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2349373
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "/act_%s"

    invoke-static {v1, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2349374
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2349375
    move-object v0, v0

    .line 2349376
    invoke-virtual {p0}, LX/GPj;->d()Ljava/lang/String;

    move-result-object v1

    .line 2349377
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349378
    move-object v0, v0

    .line 2349379
    const-string v1, "GET"

    .line 2349380
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349381
    move-object v0, v0

    .line 2349382
    const-string v1, "fields"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/14O;->a(Ljava/util/Map;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2349383
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349384
    move-object v0, v0

    .line 2349385
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2349357
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2349358
    new-instance v1, Lcom/facebook/adspayments/protocol/CvvPrepayData;

    const-string v2, "should_collect_business_details"

    invoke-static {p2, v2}, LX/6sU;->a(LX/1pN;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const-string v3, "min_acceptable_amount"

    invoke-virtual {v0, v3}, LX/0lF;->f(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/GPj;->a(LX/0lF;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    const-string v4, "max_acceptable_amount"

    invoke-virtual {v0, v4}, LX/0lF;->f(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-static {v4}, LX/GPj;->a(LX/0lF;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v4

    const-string v5, "default_funding_amount"

    invoke-virtual {v0, v5}, LX/0lF;->f(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/GPj;->a(LX/0lF;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/facebook/adspayments/protocol/CvvPrepayData;-><init>(ZLcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V

    return-object v1
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349356
    const-string v0, "get_prepay_details"

    return-object v0
.end method
