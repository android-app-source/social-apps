.class public final enum LX/GlO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GlO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GlO;

.field public static final enum FAVORITES_NULL_STATE:LX/GlO;

.field public static final enum FAVORITES_SECTION_HEADER:LX/GlO;

.field public static final enum FILTERED_GROUPS_NULL_STATE:LX/GlO;

.field public static final enum FILTERED_GROUPS_SECTION_HEADER:LX/GlO;

.field public static final enum GENERAL_NULL_STATE:LX/GlO;

.field public static final enum HIDDEN_GROUPS_SECTION_HEADER:LX/GlO;

.field public static final enum INVALID:LX/GlO;

.field public static final enum LOADING:LX/GlO;

.field public static final enum POG:LX/GlO;

.field public static final enum RECENTLY_JOINED_SECTION_HEADER:LX/GlO;

.field public static final enum SECTION_LOADING:LX/GlO;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2390769
    new-instance v0, LX/GlO;

    const-string v1, "POG"

    invoke-direct {v0, v1, v3}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->POG:LX/GlO;

    .line 2390770
    new-instance v0, LX/GlO;

    const-string v1, "FAVORITES_SECTION_HEADER"

    invoke-direct {v0, v1, v4}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->FAVORITES_SECTION_HEADER:LX/GlO;

    .line 2390771
    new-instance v0, LX/GlO;

    const-string v1, "FILTERED_GROUPS_SECTION_HEADER"

    invoke-direct {v0, v1, v5}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->FILTERED_GROUPS_SECTION_HEADER:LX/GlO;

    .line 2390772
    new-instance v0, LX/GlO;

    const-string v1, "RECENTLY_JOINED_SECTION_HEADER"

    invoke-direct {v0, v1, v6}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->RECENTLY_JOINED_SECTION_HEADER:LX/GlO;

    .line 2390773
    new-instance v0, LX/GlO;

    const-string v1, "HIDDEN_GROUPS_SECTION_HEADER"

    invoke-direct {v0, v1, v7}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->HIDDEN_GROUPS_SECTION_HEADER:LX/GlO;

    .line 2390774
    new-instance v0, LX/GlO;

    const-string v1, "LOADING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->LOADING:LX/GlO;

    .line 2390775
    new-instance v0, LX/GlO;

    const-string v1, "SECTION_LOADING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->SECTION_LOADING:LX/GlO;

    .line 2390776
    new-instance v0, LX/GlO;

    const-string v1, "GENERAL_NULL_STATE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->GENERAL_NULL_STATE:LX/GlO;

    .line 2390777
    new-instance v0, LX/GlO;

    const-string v1, "FAVORITES_NULL_STATE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->FAVORITES_NULL_STATE:LX/GlO;

    .line 2390778
    new-instance v0, LX/GlO;

    const-string v1, "FILTERED_GROUPS_NULL_STATE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->FILTERED_GROUPS_NULL_STATE:LX/GlO;

    .line 2390779
    new-instance v0, LX/GlO;

    const-string v1, "INVALID"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/GlO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GlO;->INVALID:LX/GlO;

    .line 2390780
    const/16 v0, 0xb

    new-array v0, v0, [LX/GlO;

    sget-object v1, LX/GlO;->POG:LX/GlO;

    aput-object v1, v0, v3

    sget-object v1, LX/GlO;->FAVORITES_SECTION_HEADER:LX/GlO;

    aput-object v1, v0, v4

    sget-object v1, LX/GlO;->FILTERED_GROUPS_SECTION_HEADER:LX/GlO;

    aput-object v1, v0, v5

    sget-object v1, LX/GlO;->RECENTLY_JOINED_SECTION_HEADER:LX/GlO;

    aput-object v1, v0, v6

    sget-object v1, LX/GlO;->HIDDEN_GROUPS_SECTION_HEADER:LX/GlO;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GlO;->LOADING:LX/GlO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GlO;->SECTION_LOADING:LX/GlO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GlO;->GENERAL_NULL_STATE:LX/GlO;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/GlO;->FAVORITES_NULL_STATE:LX/GlO;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/GlO;->FILTERED_GROUPS_NULL_STATE:LX/GlO;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/GlO;->INVALID:LX/GlO;

    aput-object v2, v0, v1

    sput-object v0, LX/GlO;->$VALUES:[LX/GlO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2390781
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GlO;
    .locals 1

    .prologue
    .line 2390782
    const-class v0, LX/GlO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GlO;

    return-object v0
.end method

.method public static values()[LX/GlO;
    .locals 1

    .prologue
    .line 2390783
    sget-object v0, LX/GlO;->$VALUES:[LX/GlO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GlO;

    return-object v0
.end method
