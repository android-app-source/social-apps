.class public LX/GPo;
.super LX/6sU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sU",
        "<",
        "Lcom/facebook/common/util/Quartet",
        "<",
        "Ljava/lang/String;",
        "+",
        "Lcom/facebook/payments/paymentmethods/model/PaymentOption;",
        "Lcom/facebook/payments/currency/CurrencyAmount;",
        "Ljava/lang/String;",
        ">;",
        "Lcom/facebook/common/util/ParcelablePair",
        "<",
        "Ljava/lang/String;",
        "Landroid/net/Uri;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349536
    const-class v0, Lcom/facebook/common/util/ParcelablePair;

    .line 2349537
    move-object v0, v0

    .line 2349538
    invoke-direct {p0, p1, v0}, LX/6sU;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2349539
    return-void
.end method

.method public static b(LX/0QB;)LX/GPo;
    .locals 2

    .prologue
    .line 2349540
    new-instance v1, LX/GPo;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/GPo;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349541
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 9

    .prologue
    .line 2349542
    check-cast p1, Lcom/facebook/common/util/Quartet;

    .line 2349543
    iget-object v0, p1, Lcom/facebook/common/util/Triplet;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2349544
    move-object v7, v0

    .line 2349545
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "/act_%s/prepay_fund"

    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2349546
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2349547
    move-object v0, v0

    .line 2349548
    invoke-virtual {p0}, LX/GPo;->d()Ljava/lang/String;

    move-result-object v1

    .line 2349549
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349550
    move-object v0, v0

    .line 2349551
    const-string v1, "POST"

    .line 2349552
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349553
    move-object v8, v0

    .line 2349554
    const-string v0, "credential_id"

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "payment_method_type"

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/facebook/payments/paymentmethods/model/PaymentOption;

    invoke-interface {v3}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->e()LX/6zP;

    move-result-object v3

    invoke-interface {v3}, LX/6LU;->getValue()Ljava/lang/Object;

    move-result-object v3

    const-string v4, "currency"

    .line 2349555
    iget-object v5, v7, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2349556
    const-string v6, "amount"

    .line 2349557
    iget-object p0, v7, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v7, p0

    .line 2349558
    invoke-virtual {v7}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    const-string v1, "cached_csc_token"

    iget-object v2, p1, Lcom/facebook/common/util/Quartet;->a:Ljava/lang/Object;

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, LX/14O;->a(Ljava/util/Map;Ljava/util/Map;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2349559
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349560
    move-object v0, v0

    .line 2349561
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2349562
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "altpay_ref"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2349563
    const-string v1, "payment_id"

    invoke-static {p2, v1}, LX/6sU;->a(LX/1pN;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Lcom/facebook/common/util/ParcelablePair;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/common/util/ParcelablePair;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349564
    const-string v0, "prepay_fund"

    return-object v0
.end method
