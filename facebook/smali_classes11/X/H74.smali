.class public interface abstract LX/H74;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H73;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "OfferOwnerLocationDetails"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract j()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLocation"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract k()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPlaceCurrentOpenHours"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract l()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPlaceOpenStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract mi_()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
