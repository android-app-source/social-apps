.class public final LX/Fyj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/inforeview/InfoReviewItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/inforeview/InfoReviewItemView;)V
    .locals 0

    .prologue
    .line 2307088
    iput-object p1, p0, LX/Fyj;->a:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x729fbd60

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2307089
    iget-object v1, p0, LX/Fyj;->a:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->n:LX/Fyh;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Fyj;->a:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->o:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    if-eqz v1, :cond_0

    .line 2307090
    iget-object v1, p0, LX/Fyj;->a:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->n:LX/Fyh;

    iget-object v2, p0, LX/Fyj;->a:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iget-object v2, v2, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->o:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    iget-object v3, p0, LX/Fyj;->a:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iget-object v3, v3, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->p:LX/Fz2;

    iget-object v4, p0, LX/Fyj;->a:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iget-object v4, v4, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->q:LX/BPp;

    .line 2307091
    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->cn_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/Fz2;->a(Ljava/lang/String;)V

    .line 2307092
    new-instance v6, LX/BPI;

    invoke-direct {v6}, LX/BPI;-><init>()V

    invoke-virtual {v4, v6}, LX/0b4;->a(LX/0b7;)V

    .line 2307093
    new-instance v6, LX/4Jp;

    invoke-direct {v6}, LX/4Jp;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->d()Ljava/lang/String;

    move-result-object p0

    .line 2307094
    const-string p1, "timeline_info_review_item_id"

    invoke-virtual {v6, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307095
    move-object v6, v6

    .line 2307096
    new-instance p0, LX/5zD;

    invoke-direct {p0}, LX/5zD;-><init>()V

    move-object p0, p0

    .line 2307097
    const-string p1, "input"

    invoke-virtual {p0, p1, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/5zD;

    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    .line 2307098
    iget-object p0, v1, LX/Fyh;->b:LX/0tX;

    invoke-virtual {p0, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2307099
    iget-object p0, v1, LX/Fyh;->c:LX/0Sh;

    new-instance p1, LX/Fyg;

    invoke-direct {p1, v1, v2}, LX/Fyg;-><init>(LX/Fyh;Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;)V

    invoke-virtual {p0, v6, p1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2307100
    :cond_0
    const v1, -0x30f8cfea

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
