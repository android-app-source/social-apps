.class public LX/FyO;
.super LX/1Cd;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2306512
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 2306513
    return-void
.end method


# virtual methods
.method public final a(LX/0g8;Ljava/lang/Object;II)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2306514
    if-lez p4, :cond_1

    .line 2306515
    :cond_0
    :goto_0
    return-void

    .line 2306516
    :cond_1
    sget-object v0, LX/FvU;->COVER_PHOTO:LX/FvU;

    if-ne p2, v0, :cond_0

    .line 2306517
    invoke-interface {p1}, LX/0g8;->o()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, LX/G0n;

    .line 2306518
    iget-object v1, v0, LX/G0n;->c:LX/FvG;

    move-object v0, v1

    .line 2306519
    invoke-virtual {v0}, LX/FvG;->c()Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    move-result-object v0

    .line 2306520
    if-eqz v0, :cond_0

    .line 2306521
    invoke-static {v0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getProfileVideoController(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)LX/D2H;

    move-result-object v1

    invoke-virtual {v1}, LX/D2H;->b()Z

    move-result v1

    move v1, v1

    .line 2306522
    if-eqz v1, :cond_0

    .line 2306523
    invoke-virtual {v0}, LX/Ban;->getProfileVideoView()Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    move-result-object v0

    .line 2306524
    if-eqz v0, :cond_0

    .line 2306525
    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto :goto_0
.end method

.method public final b(LX/0g8;Ljava/lang/Object;I)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2306526
    if-lez p3, :cond_1

    .line 2306527
    :cond_0
    :goto_0
    return-void

    .line 2306528
    :cond_1
    sget-object v0, LX/FvU;->COVER_PHOTO:LX/FvU;

    if-ne p2, v0, :cond_0

    .line 2306529
    invoke-interface {p1}, LX/0g8;->o()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, LX/G0n;

    .line 2306530
    iget-object v1, v0, LX/G0n;->c:LX/FvG;

    move-object v0, v1

    .line 2306531
    invoke-virtual {v0}, LX/FvG;->c()Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    move-result-object v0

    .line 2306532
    if-eqz v0, :cond_0

    .line 2306533
    invoke-virtual {v0}, LX/Ban;->getProfileVideoView()Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    move-result-object v0

    .line 2306534
    if-eqz v0, :cond_0

    .line 2306535
    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    goto :goto_0
.end method
