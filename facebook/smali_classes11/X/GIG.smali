.class public LX/GIG;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

.field private d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2336094
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2336095
    iput-object p1, p0, LX/GIG;->d:LX/0Or;

    .line 2336096
    return-void
.end method

.method public static a(LX/0QB;)LX/GIG;
    .locals 1

    .prologue
    .line 2336140
    invoke-static {p0}, LX/GIG;->b(LX/0QB;)LX/GIG;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3

    .prologue
    .line 2336131
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336132
    iput-object p2, p0, LX/GIG;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2336133
    iput-object p1, p0, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    .line 2336134
    iget-object v0, p0, LX/GIG;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b7d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    .line 2336135
    invoke-static {p1}, LX/GMo;->b(Landroid/view/View;)V

    .line 2336136
    invoke-direct {p0}, LX/GIG;->c()V

    .line 2336137
    invoke-direct {p0}, LX/GIG;->d()V

    .line 2336138
    invoke-direct {p0}, LX/GIG;->b()V

    .line 2336139
    return-void
.end method

.method public static a$redex0(LX/GIG;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2336124
    iget-object v0, p0, LX/GIG;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336125
    sget-object v1, LX/8wL;->BOOSTED_POST_ACTION_BUTTON:LX/8wL;

    const v2, 0x7f080b7f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v1, v2, v3}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2336126
    const-string v2, "data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2336127
    move-object v0, v1

    .line 2336128
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2336129
    new-instance v2, LX/GFS;

    const/16 v3, 0xe

    const/4 v4, 0x1

    invoke-direct {v2, v0, v3, v4}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2336130
    return-void
.end method

.method private static b(LX/0QB;)LX/GIG;
    .locals 2

    .prologue
    .line 2336122
    new-instance v0, LX/GIG;

    const/16 v1, 0xbd2

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GIG;-><init>(LX/0Or;)V

    .line 2336123
    return-object v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 2336114
    iget-object v0, p0, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->getHandler()Landroid/os/Handler;

    move-result-object v2

    .line 2336115
    iget-object v0, p0, LX/GIG;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    .line 2336116
    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_ADD_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/3lM;

    invoke-virtual {v0, v1, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/3lM;

    .line 2336117
    if-nez v1, :cond_1

    .line 2336118
    :cond_0
    :goto_0
    return-void

    .line 2336119
    :cond_1
    new-instance v3, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonViewController$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonViewController$1;-><init>(LX/GIG;LX/3lM;LX/0iA;)V

    iput-object v3, p0, LX/GIG;->e:Ljava/lang/Runnable;

    .line 2336120
    if-eqz v2, :cond_0

    .line 2336121
    iget-object v0, p0, LX/GIG;->e:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    const v1, 0x71387a88

    invoke-static {v2, v0, v4, v5, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2336112
    iget-object v0, p0, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    new-instance v1, LX/GIE;

    invoke-direct {v1, p0}, LX/GIE;-><init>(LX/GIG;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2336113
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2336108
    new-instance v0, LX/GIF;

    invoke-direct {v0, p0}, LX/GIF;-><init>(LX/GIG;)V

    .line 2336109
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2336110
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0}, LX/GCE;->a(ILX/GFR;)V

    .line 2336111
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2336101
    invoke-super {p0}, LX/GHg;->a()V

    .line 2336102
    iget-object v0, p0, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 2336103
    if-eqz v0, :cond_0

    .line 2336104
    iget-object v1, p0, LX/GIG;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2336105
    :cond_0
    iput-object v2, p0, LX/GIG;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2336106
    iput-object v2, p0, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    .line 2336107
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2336100
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    invoke-direct {p0, p1, p2}, LX/GIG;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2336097
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336098
    iput-object p1, p0, LX/GIG;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336099
    return-void
.end method
