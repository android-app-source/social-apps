.class public final LX/HBz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HBy;


# instance fields
.field public final synthetic a:LX/HBj;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final synthetic c:Landroid/app/Activity;

.field public final synthetic d:LX/HC0;


# direct methods
.method public constructor <init>(LX/HC0;LX/HBj;Lcom/facebook/graphql/enums/GraphQLPageActionType;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 2438128
    iput-object p1, p0, LX/HBz;->d:LX/HC0;

    iput-object p2, p0, LX/HBz;->a:LX/HBj;

    iput-object p3, p0, LX/HBz;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object p4, p0, LX/HBz;->c:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 3

    .prologue
    .line 2438129
    iget-object v0, p0, LX/HBz;->a:LX/HBj;

    invoke-interface {v0}, LX/HBj;->b()V

    .line 2438130
    iget-object v0, p0, LX/HBz;->d:LX/HC0;

    iget-object v0, v0, LX/HC0;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817cc

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2438131
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3

    .prologue
    .line 2438132
    iget-object v0, p0, LX/HBz;->a:LX/HBj;

    invoke-interface {v0}, LX/HBj;->a()V

    .line 2438133
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2438134
    const-string v1, "extra_add_tab_type"

    iget-object v2, p0, LX/HBz;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2438135
    iget-object v1, p0, LX/HBz;->c:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2438136
    iget-object v0, p0, LX/HBz;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2438137
    return-void
.end method
