.class public LX/H8g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field private e:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433597
    const v0, 0x7f0208fd

    sput v0, LX/H8g;->a:I

    .line 2433598
    const v0, 0x7f0817a5

    sput v0, LX/H8g;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 0
    .param p2    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433593
    iput-object p1, p0, LX/H8g;->c:LX/0Ot;

    .line 2433594
    iput-object p2, p0, LX/H8g;->d:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433595
    iput-object p3, p0, LX/H8g;->e:Landroid/content/Context;

    .line 2433596
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433591
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8g;->b:I

    sget v3, LX/H8g;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433590
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8g;->b:I

    sget v3, LX/H8g;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2433586
    iget-object v0, p0, LX/H8g;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XI;->EVENT_TAPPED_COPY_LINK:LX/9XI;

    iget-object v2, p0, LX/H8g;->d:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433587
    iget-object v0, p0, LX/H8g;->e:Landroid/content/Context;

    iget-object v1, p0, LX/H8g;->d:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->D()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2433588
    iget-object v0, p0, LX/H8g;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    const v1, 0x7f0817d7

    invoke-virtual {v0, v1}, LX/H8W;->a(I)V

    .line 2433589
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433585
    const/4 v0, 0x0

    return-object v0
.end method
