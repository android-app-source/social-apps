.class public LX/FRr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6vm;


# instance fields
.field public final a:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/FRq;)V
    .locals 1

    .prologue
    .line 2241047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241048
    iget-object v0, p1, LX/FRq;->a:Landroid/content/Intent;

    move-object v0, v0

    .line 2241049
    iput-object v0, p0, LX/FRr;->a:Landroid/content/Intent;

    .line 2241050
    iget v0, p1, LX/FRq;->b:I

    move v0, v0

    .line 2241051
    iput v0, p0, LX/FRr;->b:I

    .line 2241052
    iget-object v0, p1, LX/FRq;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2241053
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/FRr;->c:Ljava/lang/String;

    .line 2241054
    iget-object v0, p1, LX/FRq;->d:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 2241055
    iput-object v0, p0, LX/FRr;->d:Landroid/graphics/drawable/Drawable;

    .line 2241056
    iget-object v0, p1, LX/FRq;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2241057
    iput-object v0, p0, LX/FRr;->e:Ljava/lang/String;

    .line 2241058
    return-void
.end method

.method public static newBuilder()LX/FRq;
    .locals 1

    .prologue
    .line 2241059
    new-instance v0, LX/FRq;

    invoke-direct {v0}, LX/FRq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()LX/71I;
    .locals 1

    .prologue
    .line 2241060
    sget-object v0, LX/71I;->PAYMENT_SETTINGS_ACTION:LX/71I;

    return-object v0
.end method
