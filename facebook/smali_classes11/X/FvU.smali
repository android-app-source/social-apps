.class public final enum LX/FvU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FvU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FvU;

.field public static final enum ACTION_BAR:LX/FvU;

.field public static final enum COVER_PHOTO:LX/FvU;

.field public static final enum FRIENDING_BUTTON:LX/FvU;

.field public static final enum HEADER_LOADING_VIEW:LX/FvU;

.field public static final enum NUX:LX/FvU;

.field private static mValues:[LX/FvU;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2301536
    new-instance v0, LX/FvU;

    const-string v1, "NUX"

    invoke-direct {v0, v1, v2}, LX/FvU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FvU;->NUX:LX/FvU;

    .line 2301537
    new-instance v0, LX/FvU;

    const-string v1, "COVER_PHOTO"

    invoke-direct {v0, v1, v3}, LX/FvU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FvU;->COVER_PHOTO:LX/FvU;

    .line 2301538
    new-instance v0, LX/FvU;

    const-string v1, "HEADER_LOADING_VIEW"

    invoke-direct {v0, v1, v4}, LX/FvU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FvU;->HEADER_LOADING_VIEW:LX/FvU;

    .line 2301539
    new-instance v0, LX/FvU;

    const-string v1, "FRIENDING_BUTTON"

    invoke-direct {v0, v1, v5}, LX/FvU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FvU;->FRIENDING_BUTTON:LX/FvU;

    .line 2301540
    new-instance v0, LX/FvU;

    const-string v1, "ACTION_BAR"

    invoke-direct {v0, v1, v6}, LX/FvU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FvU;->ACTION_BAR:LX/FvU;

    .line 2301541
    const/4 v0, 0x5

    new-array v0, v0, [LX/FvU;

    sget-object v1, LX/FvU;->NUX:LX/FvU;

    aput-object v1, v0, v2

    sget-object v1, LX/FvU;->COVER_PHOTO:LX/FvU;

    aput-object v1, v0, v3

    sget-object v1, LX/FvU;->HEADER_LOADING_VIEW:LX/FvU;

    aput-object v1, v0, v4

    sget-object v1, LX/FvU;->FRIENDING_BUTTON:LX/FvU;

    aput-object v1, v0, v5

    sget-object v1, LX/FvU;->ACTION_BAR:LX/FvU;

    aput-object v1, v0, v6

    sput-object v0, LX/FvU;->$VALUES:[LX/FvU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2301542
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static cachedValues()[LX/FvU;
    .locals 1

    .prologue
    .line 2301543
    sget-object v0, LX/FvU;->mValues:[LX/FvU;

    if-nez v0, :cond_0

    .line 2301544
    invoke-static {}, LX/FvU;->values()[LX/FvU;

    move-result-object v0

    sput-object v0, LX/FvU;->mValues:[LX/FvU;

    .line 2301545
    :cond_0
    sget-object v0, LX/FvU;->mValues:[LX/FvU;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FvU;
    .locals 1

    .prologue
    .line 2301546
    const-class v0, LX/FvU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FvU;

    return-object v0
.end method

.method public static values()[LX/FvU;
    .locals 1

    .prologue
    .line 2301547
    sget-object v0, LX/FvU;->$VALUES:[LX/FvU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FvU;

    return-object v0
.end method
