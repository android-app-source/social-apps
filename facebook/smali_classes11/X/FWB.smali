.class public final enum LX/FWB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FWB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FWB;

.field public static final enum IDLE:LX/FWB;

.field public static final enum LOADING_MORE:LX/FWB;

.field public static final enum LOAD_MORE_FAILED:LX/FWB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2251674
    new-instance v0, LX/FWB;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LX/FWB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FWB;->IDLE:LX/FWB;

    .line 2251675
    new-instance v0, LX/FWB;

    const-string v1, "LOADING_MORE"

    invoke-direct {v0, v1, v3}, LX/FWB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FWB;->LOADING_MORE:LX/FWB;

    .line 2251676
    new-instance v0, LX/FWB;

    const-string v1, "LOAD_MORE_FAILED"

    invoke-direct {v0, v1, v4}, LX/FWB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FWB;->LOAD_MORE_FAILED:LX/FWB;

    .line 2251677
    const/4 v0, 0x3

    new-array v0, v0, [LX/FWB;

    sget-object v1, LX/FWB;->IDLE:LX/FWB;

    aput-object v1, v0, v2

    sget-object v1, LX/FWB;->LOADING_MORE:LX/FWB;

    aput-object v1, v0, v3

    sget-object v1, LX/FWB;->LOAD_MORE_FAILED:LX/FWB;

    aput-object v1, v0, v4

    sput-object v0, LX/FWB;->$VALUES:[LX/FWB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2251678
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FWB;
    .locals 1

    .prologue
    .line 2251679
    const-class v0, LX/FWB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FWB;

    return-object v0
.end method

.method public static values()[LX/FWB;
    .locals 1

    .prologue
    .line 2251680
    sget-object v0, LX/FWB;->$VALUES:[LX/FWB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FWB;

    return-object v0
.end method
