.class public final LX/Gug;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2403666
    iput-object p1, p0, LX/Gug;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403667
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403668
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 8

    .prologue
    .line 2403669
    iget-object v0, p0, LX/Gug;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403670
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2403671
    if-nez v0, :cond_1

    .line 2403672
    :cond_0
    :goto_0
    return-void

    .line 2403673
    :cond_1
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403674
    const-string v1, "alertID"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403675
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2403676
    sget-object v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->af:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2403677
    iget-object v1, p0, LX/Gug;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ignored previously-seen alert "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2403678
    :cond_2
    sget-object v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->af:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2403679
    :cond_3
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403680
    const-string v1, "button0Url"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403681
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403682
    const-string v2, "button1Url"

    invoke-interface {p2, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403683
    iget-object v2, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v2, v2

    .line 2403684
    const-string v3, "message"

    invoke-interface {p2, v2, v3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2403685
    iget-object v3, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v3, v3

    .line 2403686
    const-string v4, "title"

    invoke-interface {p2, v3, v4}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2403687
    iget-object v4, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v4, v4

    .line 2403688
    const-string v5, "button0Title"

    invoke-interface {p2, v4, v5}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2403689
    iget-object v5, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v5, v5

    .line 2403690
    const-string v6, "button1Title"

    invoke-interface {p2, v5, v6}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2403691
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2403692
    new-instance v6, LX/0ju;

    iget-object v7, p0, LX/Gug;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v7}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    .line 2403693
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2403694
    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2403695
    :cond_4
    new-instance v3, LX/Gue;

    invoke-direct {v3, p0, v0}, LX/Gue;-><init>(LX/Gug;Ljava/lang/String;)V

    .line 2403696
    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2403697
    invoke-virtual {v2, v4, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2403698
    :goto_1
    new-instance v0, LX/Guf;

    invoke-direct {v0, p0, v1}, LX/Guf;-><init>(LX/Gug;Ljava/lang/String;)V

    .line 2403699
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2403700
    invoke-virtual {v2, v5, v0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2403701
    :cond_5
    :goto_2
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto/16 :goto_0

    .line 2403702
    :cond_6
    iget-object v0, p0, LX/Gug;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f080036

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_1

    .line 2403703
    :cond_7
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2403704
    iget-object v1, p0, LX/Gug;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f083205

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_2
.end method
