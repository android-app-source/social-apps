.class public LX/GXD;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
        ">;"
    }
.end annotation


# static fields
.field public static a:I

.field private static final h:Ljava/lang/String;


# instance fields
.field public A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public C:Landroid/widget/LinearLayout;

.field public D:LX/GWv;

.field public b:LX/GX2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/GXH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/GXN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/GX1;

.field public j:LX/7iP;

.field public k:Landroid/view/ViewGroup;

.field public l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field public m:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field public n:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field public o:Lcom/facebook/widget/text/BetterTextView;

.field public p:Lcom/facebook/commerce/core/ui/NoticeView;

.field public q:Landroid/widget/TextView;

.field public r:Lcom/facebook/widget/ListViewFriendlyViewPager;

.field public s:[Landroid/widget/LinearLayout;

.field public t:[Landroid/widget/TextView;

.field public u:[Landroid/widget/Spinner;

.field public v:Landroid/widget/TextView;

.field public w:Landroid/widget/TextView;

.field public x:Landroid/widget/TextView;

.field public y:Landroid/widget/LinearLayout;

.field public z:Landroid/widget/Spinner;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2363242
    const-class v0, LX/GXD;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GXD;->h:Ljava/lang/String;

    .line 2363243
    const/16 v0, 0x50

    sput v0, LX/GXD;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 12

    .prologue
    .line 2363244
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2363245
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2363246
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v6, p0

    check-cast v6, LX/GXD;

    const-class v7, LX/GX2;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/GX2;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v8

    check-cast v8, LX/0hB;

    const/16 v9, 0x13a4

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v10

    check-cast v10, LX/0wM;

    invoke-static {v0}, LX/GXH;->b(LX/0QB;)LX/GXH;

    move-result-object v11

    check-cast v11, LX/GXH;

    invoke-static {v0}, LX/GXN;->a(LX/0QB;)LX/GXN;

    move-result-object v0

    check-cast v0, LX/GXN;

    iput-object v7, v6, LX/GXD;->b:LX/GX2;

    iput-object v8, v6, LX/GXD;->c:LX/0hB;

    iput-object v9, v6, LX/GXD;->d:LX/0Or;

    iput-object v10, v6, LX/GXD;->e:LX/0wM;

    iput-object v11, v6, LX/GXD;->f:LX/GXH;

    iput-object v0, v6, LX/GXD;->g:LX/GXN;

    .line 2363247
    const v0, 0x7f031039

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2363248
    const v0, 0x7f0d26e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/ui/NoticeView;

    iput-object v0, p0, LX/GXD;->p:Lcom/facebook/commerce/core/ui/NoticeView;

    .line 2363249
    const v0, 0x7f0d26e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GXD;->q:Landroid/widget/TextView;

    .line 2363250
    new-instance v0, LX/GX1;

    invoke-direct {v0, p1}, LX/GX1;-><init>(Landroid/content/Context;)V

    .line 2363251
    move-object v0, v0

    .line 2363252
    iput-object v0, p0, LX/GXD;->i:LX/GX1;

    .line 2363253
    const v0, 0x7f0d26e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ListViewFriendlyViewPager;

    iput-object v0, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    .line 2363254
    iget-object v0, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    iget-object v1, p0, LX/GXD;->i:LX/GX1;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2363255
    new-array v0, v5, [Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/GXD;->s:[Landroid/widget/LinearLayout;

    .line 2363256
    new-array v0, v5, [Landroid/widget/TextView;

    iput-object v0, p0, LX/GXD;->t:[Landroid/widget/TextView;

    .line 2363257
    new-array v0, v5, [Landroid/widget/Spinner;

    iput-object v0, p0, LX/GXD;->u:[Landroid/widget/Spinner;

    .line 2363258
    iget-object v1, p0, LX/GXD;->s:[Landroid/widget/LinearLayout;

    const v0, 0x7f0d26ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    aput-object v0, v1, v2

    .line 2363259
    iget-object v1, p0, LX/GXD;->t:[Landroid/widget/TextView;

    const v0, 0x7f0d26ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    .line 2363260
    iget-object v1, p0, LX/GXD;->u:[Landroid/widget/Spinner;

    const v0, 0x7f0d26ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    aput-object v0, v1, v2

    .line 2363261
    iget-object v1, p0, LX/GXD;->s:[Landroid/widget/LinearLayout;

    const v0, 0x7f0d26ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    aput-object v0, v1, v3

    .line 2363262
    iget-object v1, p0, LX/GXD;->t:[Landroid/widget/TextView;

    const v0, 0x7f0d26f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    .line 2363263
    iget-object v1, p0, LX/GXD;->u:[Landroid/widget/Spinner;

    const v0, 0x7f0d26f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    aput-object v0, v1, v3

    .line 2363264
    iget-object v1, p0, LX/GXD;->s:[Landroid/widget/LinearLayout;

    const v0, 0x7f0d26f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    aput-object v0, v1, v4

    .line 2363265
    iget-object v1, p0, LX/GXD;->t:[Landroid/widget/TextView;

    const v0, 0x7f0d26f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v4

    .line 2363266
    iget-object v1, p0, LX/GXD;->u:[Landroid/widget/Spinner;

    const v0, 0x7f0d26f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    aput-object v0, v1, v4

    .line 2363267
    const v0, 0x7f0d26e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GXD;->v:Landroid/widget/TextView;

    .line 2363268
    const v0, 0x7f0d26e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GXD;->w:Landroid/widget/TextView;

    .line 2363269
    const v0, 0x7f0d26ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GXD;->x:Landroid/widget/TextView;

    .line 2363270
    const v0, 0x7f0d26f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/GXD;->y:Landroid/widget/LinearLayout;

    .line 2363271
    const v0, 0x7f0d26f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, LX/GXD;->z:Landroid/widget/Spinner;

    .line 2363272
    const v0, 0x7f0d26f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2363273
    const v0, 0x7f0d26f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2363274
    invoke-static {p0}, LX/GXD;->b(LX/GXD;)V

    .line 2363275
    iget-object v0, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2363276
    iget-object v0, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v0, v2, v2}, Lcom/facebook/widget/ListViewFriendlyViewPager;->scrollTo(II)V

    .line 2363277
    iget-object v0, p0, LX/GXD;->w:Landroid/widget/TextView;

    iget-object v1, p0, LX/GXD;->w:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 2363278
    const v0, 0x7f0d26eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/GXD;->C:Landroid/widget/LinearLayout;

    .line 2363279
    new-instance v0, LX/GWv;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, v2}, LX/GWv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, LX/GXD;->D:LX/GWv;

    .line 2363280
    iget-object v0, p0, LX/GXD;->C:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/GXD;->D:LX/GWv;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2363281
    const v3, -0x6e685d

    .line 2363282
    const v0, 0x7f0d26fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/GXD;->k:Landroid/view/ViewGroup;

    .line 2363283
    const v0, 0x7f0d26fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, LX/GXD;->l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2363284
    iget-object v1, p0, LX/GXD;->l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v0, p0, LX/GXD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 2363285
    const v0, 0x7f0d26fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, LX/GXD;->m:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2363286
    iget-object v1, p0, LX/GXD;->m:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v0, p0, LX/GXD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 2363287
    iget-object v0, p0, LX/GXD;->m:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, LX/GXD;->e:LX/0wM;

    const v2, 0x7f020809

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2363288
    const v0, 0x7f0d26fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, LX/GXD;->n:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2363289
    iget-object v1, p0, LX/GXD;->n:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v0, p0, LX/GXD;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 2363290
    iget-object v0, p0, LX/GXD;->n:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, LX/GXD;->e:LX/0wM;

    const v2, 0x7f0209c5

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2363291
    const v0, 0x7f0d26fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/GXD;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 2363292
    return-void
.end method

.method public static b(LX/GXD;)V
    .locals 8

    .prologue
    .line 2363293
    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2363294
    iget-object v1, p0, LX/GXD;->c:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    iget-object v2, p0, LX/GXD;->c:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->d()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    sget v2, LX/GXD;->a:I

    mul-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x64

    .line 2363295
    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b23a9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2363296
    sub-int v3, v0, v1

    iget-object v4, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v4}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v3, v2

    .line 2363297
    iget-object v4, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    iget-object v5, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v5}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getPaddingLeft()I

    move-result v5

    div-int/lit8 v6, v2, 0x2

    sub-int/2addr v5, v6

    iget-object v6, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v6}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getPaddingTop()I

    move-result v6

    add-int/2addr v3, v2

    div-int/lit8 v7, v2, 0x2

    add-int/2addr v3, v7

    iget-object v7, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v7}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getPaddingBottom()I

    move-result v7

    invoke-virtual {v4, v5, v6, v3, v7}, Lcom/facebook/widget/ListViewFriendlyViewPager;->setPadding(IIII)V

    .line 2363298
    iget-object v3, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v3, v2}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2363299
    iget-object v2, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2363300
    return-void
.end method

.method public static b(LX/GXD;LX/GXK;)V
    .locals 12

    .prologue
    .line 2363301
    const/4 v2, 0x0

    .line 2363302
    iget-object v0, p1, LX/GXK;->l:LX/0Px;

    move-object v0, v0

    .line 2363303
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    .line 2363304
    :goto_0
    if-ge v1, v3, :cond_3

    .line 2363305
    if-ge v1, v3, :cond_2

    .line 2363306
    iget-object v0, p0, LX/GXD;->s:[Landroid/widget/LinearLayout;

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2363307
    iget-object v0, p0, LX/GXD;->t:[Landroid/widget/TextView;

    aget-object v4, v0, v1

    .line 2363308
    iget-object v0, p1, LX/GXK;->l:LX/0Px;

    move-object v0, v0

    .line 2363309
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GXM;

    .line 2363310
    iget-object v5, v0, LX/GXM;->a:Ljava/lang/String;

    move-object v0, v5

    .line 2363311
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2363312
    new-instance v0, LX/GXC;

    invoke-direct {v0, p0, v1, p1}, LX/GXC;-><init>(LX/GXD;ILX/GXK;)V

    .line 2363313
    iget-object v4, p0, LX/GXD;->u:[Landroid/widget/Spinner;

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2363314
    iget-object v4, p0, LX/GXD;->u:[Landroid/widget/Spinner;

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2363315
    iget-object v0, p1, LX/GXK;->l:LX/0Px;

    move-object v0, v0

    .line 2363316
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GXM;

    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 2363317
    iget-object v4, v0, LX/GXM;->b:LX/0Px;

    move-object v4, v4

    .line 2363318
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .line 2363319
    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0837b7

    new-array v6, v11, [Ljava/lang/Object;

    .line 2363320
    iget-object v9, v0, LX/GXM;->a:Ljava/lang/String;

    move-object v9, v9

    .line 2363321
    aput-object v9, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v7

    move v6, v7

    .line 2363322
    :goto_1
    iget-object v4, v0, LX/GXM;->b:LX/0Px;

    move-object v4, v4

    .line 2363323
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v6, v4, :cond_0

    .line 2363324
    iget-object v4, v0, LX/GXM;->b:LX/0Px;

    move-object v4, v4

    .line 2363325
    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2363326
    sget-object v9, LX/GXB;->b:[I

    .line 2363327
    iget-object v5, v0, LX/GXM;->c:LX/0Px;

    move-object v5, v5

    .line 2363328
    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/GXL;

    invoke-virtual {v5}, LX/GXL;->ordinal()I

    move-result v5

    aget v5, v9, v5

    packed-switch v5, :pswitch_data_0

    .line 2363329
    :goto_2
    add-int/lit8 v5, v6, 0x1

    aput-object v4, v8, v5

    .line 2363330
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_1

    .line 2363331
    :pswitch_0
    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f0837b8

    new-array v10, v11, [Ljava/lang/Object;

    aput-object v4, v10, v7

    invoke-virtual {v5, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 2363332
    :pswitch_1
    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v9, 0x7f0837b9

    new-array v10, v11, [Ljava/lang/Object;

    aput-object v4, v10, v7

    invoke-virtual {v5, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 2363333
    :cond_0
    new-instance v4, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, LX/GXD;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x1090009

    invoke-direct {v4, v5, v6, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 2363334
    iget-object v5, p0, LX/GXD;->u:[Landroid/widget/Spinner;

    aget-object v5, v5, v1

    invoke-virtual {v5, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2363335
    iget-object v4, v0, LX/GXM;->d:LX/0am;

    move-object v4, v4

    .line 2363336
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2363337
    iget-object v4, p0, LX/GXD;->u:[Landroid/widget/Spinner;

    aget-object v5, v4, v1

    .line 2363338
    iget-object v4, v0, LX/GXM;->d:LX/0am;

    move-object v4, v4

    .line 2363339
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v5, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 2363340
    :cond_1
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2363341
    :cond_2
    iget-object v0, p0, LX/GXD;->s:[Landroid/widget/LinearLayout;

    aget-object v0, v0, v1

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 2363342
    :cond_3
    iget-object v0, p1, LX/GXK;->m:LX/0am;

    move-object v0, v0

    .line 2363343
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2363344
    iget-object v0, p1, LX/GXK;->m:LX/0am;

    move-object v0, v0

    .line 2363345
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_5

    .line 2363346
    iget-object v0, p0, LX/GXD;->y:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2363347
    iget-object v1, p0, LX/GXD;->z:Landroid/widget/Spinner;

    .line 2363348
    iget-object v0, p1, LX/GXK;->m:LX/0am;

    move-object v0, v0

    .line 2363349
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2363350
    new-array v3, v0, [Ljava/lang/String;

    .line 2363351
    const/4 v2, 0x1

    :goto_4
    if-gt v2, v0, :cond_4

    .line 2363352
    add-int/lit8 v4, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 2363353
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2363354
    :cond_4
    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, LX/GXD;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x1090009

    invoke-direct {v2, v4, v5, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    move-object v0, v2

    .line 2363355
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 2363356
    :goto_5
    iget-object v0, p0, LX/GXD;->z:Landroid/widget/Spinner;

    .line 2363357
    iget-boolean v1, p1, LX/GXK;->n:Z

    move v1, v1

    .line 2363358
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 2363359
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2363360
    iget-object v0, p0, LX/GXD;->v:Landroid/widget/TextView;

    .line 2363361
    iget-object v1, p1, LX/GXK;->g:Ljava/lang/String;

    move-object v1, v1

    .line 2363362
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2363363
    iget-object v0, p1, LX/GXK;->h:LX/0am;

    move-object v0, v0

    .line 2363364
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2363365
    iget-object v0, p0, LX/GXD;->w:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837bb

    new-array v3, v6, [Ljava/lang/Object;

    .line 2363366
    iget-object v4, p1, LX/GXK;->h:LX/0am;

    move-object v4, v4

    .line 2363367
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2363368
    :goto_6
    iget-boolean v0, p1, LX/GXK;->j:Z

    move v0, v0

    .line 2363369
    if-eqz v0, :cond_7

    .line 2363370
    iget-object v0, p0, LX/GXD;->x:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2363371
    :goto_7
    const/4 v4, -0x1

    const/16 v3, 0x8

    const/4 v5, 0x0

    .line 2363372
    iget-object v0, p1, LX/GXK;->p:LX/GXJ;

    move-object v0, v0

    .line 2363373
    if-nez v0, :cond_9

    .line 2363374
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2363375
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2363376
    :goto_8
    return-void

    .line 2363377
    :cond_5
    iget-object v0, p0, LX/GXD;->y:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_5

    .line 2363378
    :cond_6
    iget-object v0, p0, LX/GXD;->w:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 2363379
    :cond_7
    iget-object v0, p1, LX/GXK;->i:LX/0am;

    move-object v0, v0

    .line 2363380
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2363381
    iget-object v0, p0, LX/GXD;->x:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837bc

    new-array v3, v6, [Ljava/lang/Object;

    .line 2363382
    iget-object v4, p1, LX/GXK;->i:LX/0am;

    move-object v4, v4

    .line 2363383
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 2363384
    :cond_8
    iget-object v0, p0, LX/GXD;->x:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 2363385
    :cond_9
    iget-object v1, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2363386
    iget-object v1, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2363387
    iget-boolean v2, p1, LX/GXK;->r:Z

    move v2, v2

    .line 2363388
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setEnabled(Z)V

    .line 2363389
    iget-object v1, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    new-instance v2, LX/GXA;

    invoke-direct {v2, p0, p1}, LX/GXA;-><init>(LX/GXD;LX/GXK;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2363390
    sget-object v1, LX/GXB;->a:[I

    invoke-virtual {v0}, LX/GXJ;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    goto :goto_8

    .line 2363391
    :pswitch_2
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, LX/GXD;->e:LX/0wM;

    const v2, 0x7f0208f8

    invoke-virtual {v1, v2, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2363392
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOrientation(I)V

    .line 2363393
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const v1, 0x7f0837bf

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(I)V

    .line 2363394
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0065

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 2363395
    iget-object v0, p1, LX/GXK;->q:Ljava/lang/String;

    move-object v0, v0

    .line 2363396
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2363397
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 2363398
    :cond_a
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2363399
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 2363400
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0837c3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 2363401
    iget-object v4, p1, LX/GXK;->q:Ljava/lang/String;

    move-object v4, v4

    .line 2363402
    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 2363403
    :pswitch_3
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, LX/GXD;->e:LX/0wM;

    const v2, 0x7f020740

    invoke-virtual {v1, v2, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2363404
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOrientation(I)V

    .line 2363405
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageScale(F)V

    .line 2363406
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const v1, 0x7f0837be

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(I)V

    .line 2363407
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2363408
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const v1, 0x7f0837c4

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(I)V

    .line 2363409
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 2363410
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    goto/16 :goto_8

    .line 2363411
    :pswitch_4
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 2363412
    iget-object v0, p0, LX/GXD;->A:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const v1, 0x7f0837bd

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(I)V

    .line 2363413
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2363414
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    const v1, 0x7f0837c2

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(I)V

    .line 2363415
    iget-object v0, p0, LX/GXD;->B:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, LX/GXD;->e:LX/0wM;

    const v2, 0x7f020913

    const v3, -0x6e685d

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/0am;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/GX0;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 2363416
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2363417
    iget-object v0, p0, LX/GXD;->k:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2363418
    :goto_0
    return-void

    .line 2363419
    :cond_0
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GX0;

    .line 2363420
    iget-boolean v1, v0, LX/GX0;->a:Z

    move v1, v1

    .line 2363421
    if-eqz v1, :cond_3

    .line 2363422
    iget-object v1, p0, LX/GXD;->l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v1, v5}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 2363423
    iget-object v1, p0, LX/GXD;->l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    new-instance v2, LX/GX5;

    invoke-direct {v2, p0, v0}, LX/GX5;-><init>(LX/GXD;LX/GX0;)V

    invoke-virtual {v1, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2363424
    iget-object v2, p0, LX/GXD;->l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v3, p0, LX/GXD;->e:LX/0wM;

    const p1, 0x7f0208fa

    .line 2363425
    iget-boolean v1, v0, LX/GX0;->c:Z

    move v1, v1

    .line 2363426
    if-eqz v1, :cond_6

    const v1, -0xa76f01

    :goto_1
    invoke-virtual {v3, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2363427
    iget-object v2, p0, LX/GXD;->l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2363428
    iget-boolean v1, v0, LX/GX0;->c:Z

    move v1, v1

    .line 2363429
    if-eqz v1, :cond_7

    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00d2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_2
    invoke-virtual {v2, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTextColor(I)V

    .line 2363430
    iget-object v2, p0, LX/GXD;->l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2363431
    iget-boolean v1, v0, LX/GX0;->c:Z

    move v1, v1

    .line 2363432
    if-eqz v1, :cond_8

    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0837cd

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v2, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2363433
    :goto_4
    iget-boolean v1, v0, LX/GX0;->b:Z

    move v1, v1

    .line 2363434
    if-eqz v1, :cond_4

    .line 2363435
    iget-object v1, p0, LX/GXD;->m:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v1, v5}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 2363436
    iget-object v1, p0, LX/GXD;->m:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    new-instance v2, LX/GX6;

    invoke-direct {v2, p0, v0}, LX/GX6;-><init>(LX/GXD;LX/GX0;)V

    invoke-virtual {v1, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2363437
    :goto_5
    iget-object v1, p0, LX/GXD;->g:LX/GXN;

    .line 2363438
    iget-object v2, v0, LX/GX0;->d:LX/0am;

    move-object v2, v2

    .line 2363439
    iget-object v3, v0, LX/GX0;->e:LX/0am;

    move-object v3, v3

    .line 2363440
    const/4 p1, 0x1

    const/4 v12, 0x0

    .line 2363441
    new-instance v7, LX/3DI;

    const-string v6, "\u00a0"

    invoke-direct {v7, v6}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 2363442
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-lez v6, :cond_1

    .line 2363443
    iget-object v8, v1, LX/GXN;->a:Landroid/content/res/Resources;

    const v9, 0x7f0f0060

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    new-array v10, p1, [Ljava/lang/Object;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v6, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2363444
    :cond_1
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-lez v6, :cond_2

    .line 2363445
    iget-object v8, v1, LX/GXN;->a:Landroid/content/res/Resources;

    const v9, 0x7f0f0062

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    new-array v10, p1, [Ljava/lang/Object;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v6, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2363446
    :cond_2
    invoke-virtual {v7}, LX/3DI;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, v6

    .line 2363447
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2363448
    iget-object v2, p0, LX/GXD;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v5}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2363449
    iget-object v2, p0, LX/GXD;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2363450
    iget-object v1, p0, LX/GXD;->o:Lcom/facebook/widget/text/BetterTextView;

    new-instance v2, LX/GX7;

    invoke-direct {v2, p0, v0}, LX/GX7;-><init>(LX/GXD;LX/GX0;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2363451
    :cond_3
    iget-object v1, p0, LX/GXD;->l:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v1, v4}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto/16 :goto_4

    .line 2363452
    :cond_4
    iget-object v1, p0, LX/GXD;->m:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v1, v4}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 2363453
    :cond_5
    iget-object v0, p0, LX/GXD;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2363454
    :cond_6
    const v1, -0x6e685d

    goto/16 :goto_1

    .line 2363455
    :cond_7
    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00d5

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_2

    .line 2363456
    :cond_8
    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0837cc

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2363457
    check-cast p1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2363458
    iget-object v0, p0, LX/GXD;->f:LX/GXH;

    const/4 v2, 0x1

    .line 2363459
    new-instance v3, LX/GXI;

    invoke-direct {v3}, LX/GXI;-><init>()V

    .line 2363460
    iput-object p1, v3, LX/GXI;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    .line 2363461
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->s()Ljava/lang/String;

    move-result-object v1

    .line 2363462
    iput-object v1, v3, LX/GXI;->d:Ljava/lang/String;

    .line 2363463
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->m()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->VISIBLE:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne v1, v4, :cond_7

    move v1, v2

    .line 2363464
    :goto_0
    iput-boolean v1, v3, LX/GXI;->e:Z

    .line 2363465
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gt v1, v2, :cond_0

    .line 2363466
    iput-boolean v2, v3, LX/GXI;->k:Z

    .line 2363467
    :cond_0
    const/4 v1, 0x1

    .line 2363468
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_8

    :cond_1
    :goto_1
    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->m()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v1

    invoke-static {v1}, LX/7j9;->a(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)LX/7iu;

    move-result-object v1

    if-nez v1, :cond_9

    .line 2363469
    :cond_2
    :goto_2
    const/4 v4, 0x0

    .line 2363470
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v1

    if-nez v1, :cond_a

    .line 2363471
    :cond_3
    :goto_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    if-nez v1, :cond_e

    .line 2363472
    :cond_4
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 2363473
    iput-object v1, v3, LX/GXI;->s:LX/0am;

    .line 2363474
    :goto_4
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 2363475
    :cond_5
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2363476
    :goto_5
    move-object v1, v1

    .line 2363477
    invoke-static {v3, v1, p1}, LX/GXH;->a(LX/GXI;LX/0Px;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    .line 2363478
    invoke-virtual {v3}, LX/GXI;->a()LX/GXK;

    move-result-object v1

    move-object v0, v1

    .line 2363479
    iget-object v1, v0, LX/GXK;->a:LX/0am;

    move-object v1, v1

    .line 2363480
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 2363481
    iget-object v1, p0, LX/GXD;->p:Lcom/facebook/commerce/core/ui/NoticeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setVisibility(I)V

    .line 2363482
    iget-object v2, p0, LX/GXD;->p:Lcom/facebook/commerce/core/ui/NoticeView;

    .line 2363483
    iget-object v1, v0, LX/GXK;->a:LX/0am;

    move-object v1, v1

    .line 2363484
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7iu;

    invoke-virtual {v2, v1}, Lcom/facebook/commerce/core/ui/NoticeView;->setLevel(LX/7iu;)V

    .line 2363485
    iget-object v1, p0, LX/GXD;->p:Lcom/facebook/commerce/core/ui/NoticeView;

    .line 2363486
    iget-object v2, v0, LX/GXK;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2363487
    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setTitle(Ljava/lang/String;)V

    .line 2363488
    iget-object v1, p0, LX/GXD;->p:Lcom/facebook/commerce/core/ui/NoticeView;

    .line 2363489
    iget-object v2, v0, LX/GXK;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2363490
    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setMessage(Ljava/lang/String;)V

    .line 2363491
    iget-object v1, p0, LX/GXD;->p:Lcom/facebook/commerce/core/ui/NoticeView;

    new-instance v2, LX/GX9;

    invoke-direct {v2, p0, v0}, LX/GX9;-><init>(LX/GXD;LX/GXK;)V

    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2363492
    :goto_6
    iget-object v1, p0, LX/GXD;->q:Landroid/widget/TextView;

    .line 2363493
    iget-object v2, v0, LX/GXK;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2363494
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2363495
    iget-object v1, p0, LX/GXD;->i:LX/GX1;

    .line 2363496
    iget-object v2, v0, LX/GXK;->f:LX/0Px;

    move-object v2, v2

    .line 2363497
    iput-object v2, v1, LX/GX1;->b:LX/0Px;

    .line 2363498
    iget-boolean v1, v0, LX/GXK;->e:Z

    move v1, v1

    .line 2363499
    if-eqz v1, :cond_13

    .line 2363500
    iget-object v1, p0, LX/GXD;->i:LX/GX1;

    new-instance v2, LX/GX3;

    invoke-direct {v2, p0, v0}, LX/GX3;-><init>(LX/GXD;LX/GXK;)V

    .line 2363501
    iput-object v2, v1, LX/GX1;->c:Landroid/view/View$OnClickListener;

    .line 2363502
    iget-object v1, p0, LX/GXD;->n:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 2363503
    iget-object v1, p0, LX/GXD;->n:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    new-instance v2, LX/GX4;

    invoke-direct {v2, p0, v0}, LX/GX4;-><init>(LX/GXD;LX/GXK;)V

    invoke-virtual {v1, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2363504
    :goto_7
    invoke-static {p0}, LX/GXD;->b(LX/GXD;)V

    .line 2363505
    iget-object v1, p0, LX/GXD;->i:LX/GX1;

    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 2363506
    invoke-static {p0, v0}, LX/GXD;->b(LX/GXD;LX/GXK;)V

    .line 2363507
    iget-object v1, v0, LX/GXK;->s:LX/0am;

    move-object v1, v1

    .line 2363508
    invoke-virtual {p0, v1}, LX/GXD;->a(LX/0am;)V

    .line 2363509
    iget-object v1, p0, LX/GXD;->D:LX/GWv;

    .line 2363510
    iget-object v2, v0, LX/GXK;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object v2, v2

    .line 2363511
    invoke-virtual {v1, v2}, LX/GWv;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    .line 2363512
    iget-boolean v1, v0, LX/GXK;->k:Z

    move v1, v1

    .line 2363513
    if-eqz v1, :cond_6

    .line 2363514
    invoke-virtual {p0}, LX/GXD;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2363515
    iget-object v2, p0, LX/GXD;->c:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    iget-object v3, p0, LX/GXD;->c:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    sget v3, LX/GXD;->a:I

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    .line 2363516
    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 2363517
    iget-object v2, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    iget-object v3, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v3}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getPaddingTop()I

    move-result v3

    iget-object v0, p0, LX/GXD;->r:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->getPaddingBottom()I

    move-result v0

    invoke-virtual {v2, v1, v3, v1, v0}, Lcom/facebook/widget/ListViewFriendlyViewPager;->setPadding(IIII)V

    .line 2363518
    :cond_6
    return-void

    .line 2363519
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2363520
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2363521
    const/4 v5, 0x2

    invoke-virtual {v4, v2, v5}, LX/15i;->h(II)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto/16 :goto_1

    .line 2363522
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->m()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v1

    .line 2363523
    invoke-static {v1}, LX/7j9;->a(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)LX/7iu;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 2363524
    iput-object v2, v3, LX/GXI;->a:LX/0am;

    .line 2363525
    iget-object v2, v0, LX/GXH;->a:LX/7j9;

    invoke-virtual {v2, v1}, LX/7j9;->b(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)Ljava/lang/String;

    move-result-object v2

    .line 2363526
    iput-object v2, v3, LX/GXI;->b:Ljava/lang/String;

    .line 2363527
    iget-object v2, v0, LX/GXH;->a:LX/7j9;

    invoke-virtual {v2, v1}, LX/7j9;->c(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)Ljava/lang/String;

    move-result-object v1

    .line 2363528
    iput-object v1, v3, LX/GXI;->c:Ljava/lang/String;

    .line 2363529
    goto/16 :goto_2

    .line 2363530
    :cond_a
    new-instance v5, Ljava/util/LinkedHashSet;

    invoke-direct {v5}, Ljava/util/LinkedHashSet;-><init>()V

    .line 2363531
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v4

    :goto_8
    if-ge v2, v7, :cond_d

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    .line 2363532
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->p()LX/2uF;

    move-result-object v8

    if-eqz v8, :cond_c

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->p()LX/2uF;

    move-result-object v8

    invoke-virtual {v8}, LX/39O;->a()Z

    move-result v8

    if-nez v8, :cond_c

    .line 2363533
    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->p()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :cond_b
    :goto_9
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v8

    if-eqz v8, :cond_c

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2363534
    invoke-virtual {v9, v8, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 2363535
    invoke-virtual {v9, v8, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 2363536
    :cond_c
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_8

    .line 2363537
    :cond_d
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2363538
    iput-object v1, v3, LX/GXI;->f:LX/0Px;

    .line 2363539
    goto/16 :goto_3

    .line 2363540
    :cond_e
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->a()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;

    move-result-object v1

    invoke-static {v1}, LX/GXH;->a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$ProductGroupFeedbackModel;)LX/0am;

    move-result-object v1

    .line 2363541
    iput-object v1, v3, LX/GXI;->s:LX/0am;

    .line 2363542
    goto/16 :goto_4

    .line 2363543
    :cond_f
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2363544
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v1

    .line 2363545
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v4, 0x0

    move v5, v4

    :goto_a
    if-ge v5, v6, :cond_12

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    .line 2363546
    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 2363547
    :goto_b
    move-object v4, v4

    .line 2363548
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    .line 2363549
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2363550
    const/4 v1, 0x0

    :goto_c
    if-ge v1, v5, :cond_10

    .line 2363551
    invoke-static {v1, p1, v2, v4}, LX/GXH;->a(ILcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;LX/0Px;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;)LX/GXM;

    move-result-object v7

    .line 2363552
    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2363553
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 2363554
    :cond_10
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, v2}, LX/GXH;->a(LX/0Px;LX/0Px;)LX/0Px;

    move-result-object v1

    goto/16 :goto_5

    .line 2363555
    :cond_11
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_a

    .line 2363556
    :cond_12
    const/4 v4, 0x0

    goto :goto_b

    .line 2363557
    :cond_13
    iget-object v1, p0, LX/GXD;->i:LX/GX1;

    const/4 v2, 0x0

    .line 2363558
    iput-object v2, v1, LX/GX1;->c:Landroid/view/View$OnClickListener;

    .line 2363559
    iget-object v1, p0, LX/GXD;->n:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto/16 :goto_7

    .line 2363560
    :cond_14
    iget-object v1, p0, LX/GXD;->p:Lcom/facebook/commerce/core/ui/NoticeView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setVisibility(I)V

    goto/16 :goto_6
.end method
