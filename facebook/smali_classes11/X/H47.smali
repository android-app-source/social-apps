.class public abstract LX/H47;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2422318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2422319
    return-void
.end method

.method public static a(LX/H44;Landroid/content/Context;)LX/H45;
    .locals 6

    .prologue
    .line 2422320
    sget-object v0, LX/H46;->a:[I

    invoke-virtual {p0}, LX/H44;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2422321
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid category picker Icon view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2422322
    :pswitch_0
    const v2, 0x7f0a06b6

    .line 2422323
    const v1, 0x7f0820e0

    .line 2422324
    const v0, 0x7f0210af

    move v3, v2

    move v2, v0

    .line 2422325
    :goto_0
    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2422326
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2422327
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2422328
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2422329
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2422330
    new-instance v0, LX/H45;

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/H45;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ILX/H44;Landroid/content/Context;)V

    return-object v0

    .line 2422331
    :pswitch_1
    const v2, 0x7f0a06b7

    .line 2422332
    const v1, 0x7f0820e1

    .line 2422333
    const v0, 0x7f0210b5

    move v3, v2

    move v2, v0

    .line 2422334
    goto :goto_0

    .line 2422335
    :pswitch_2
    const v2, 0x7f0a06b8

    .line 2422336
    const v1, 0x7f0820e2

    .line 2422337
    const v0, 0x7f0210b1

    move v3, v2

    move v2, v0

    .line 2422338
    goto :goto_0

    .line 2422339
    :pswitch_3
    const v2, 0x7f0a06b9

    .line 2422340
    const v1, 0x7f0820e3

    .line 2422341
    const v0, 0x7f0210b3

    move v3, v2

    move v2, v0

    .line 2422342
    goto :goto_0

    .line 2422343
    :pswitch_4
    const v2, 0x7f0a06ba

    .line 2422344
    const v1, 0x7f0820e4

    .line 2422345
    const v0, 0x7f0210b4

    move v3, v2

    move v2, v0

    .line 2422346
    goto :goto_0

    .line 2422347
    :pswitch_5
    const v2, 0x7f0a06bb

    .line 2422348
    const v1, 0x7f0820e5

    .line 2422349
    const v0, 0x7f0210b0

    move v3, v2

    move v2, v0

    .line 2422350
    goto :goto_0

    .line 2422351
    :pswitch_6
    const v2, 0x7f0a06bc

    .line 2422352
    const v1, 0x7f0820e6

    .line 2422353
    const v0, 0x7f0210b2

    move v3, v2

    move v2, v0

    .line 2422354
    goto :goto_0

    .line 2422355
    :pswitch_7
    const v2, 0x7f0a06bd

    .line 2422356
    const v1, 0x7f0820e7

    .line 2422357
    const v0, 0x7f0210b6

    move v3, v2

    move v2, v0

    .line 2422358
    goto :goto_0

    .line 2422359
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
