.class public final LX/HAt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PagesContactInboxGraphQLModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V
    .locals 0

    .prologue
    .line 2436111
    iput-object p1, p0, LX/HAt;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2436112
    iget-object v0, p0, LX/HAt;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->b:LX/0tX;

    iget-object v1, p0, LX/HAt;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    .line 2436113
    new-instance v2, LX/HB9;

    invoke-direct {v2}, LX/HB9;-><init>()V

    move-object v2, v2

    .line 2436114
    const-string v3, "page_id"

    iget-wide v4, v1, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2436115
    const-string v3, "after_cursor"

    iget-object v4, v1, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->q:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2436116
    const-string v3, "first_count"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2436117
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    move-object v1, v2

    .line 2436118
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
