.class public LX/Fgu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fgb;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0gc;

.field private final c:LX/CIx;

.field private final d:LX/Fga;

.field public final e:LX/FiD;

.field private final f:LX/Bnj;

.field private g:LX/Fgt;

.field private h:Lcom/facebook/search/api/GraphSearchQuery;

.field public i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

.field private j:LX/FgZ;

.field private k:LX/Bno;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;LX/0gc;LX/CIx;LX/Fga;LX/FiD;LX/Bnj;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2271170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2271171
    iput-object p1, p0, LX/Fgu;->a:Landroid/content/Context;

    .line 2271172
    iput-object p2, p0, LX/Fgu;->h:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271173
    iput-object p4, p0, LX/Fgu;->c:LX/CIx;

    .line 2271174
    iput-object p3, p0, LX/Fgu;->b:LX/0gc;

    .line 2271175
    iput-object p5, p0, LX/Fgu;->d:LX/Fga;

    .line 2271176
    iput-object p6, p0, LX/Fgu;->e:LX/FiD;

    .line 2271177
    iput-object p7, p0, LX/Fgu;->f:LX/Bnj;

    .line 2271178
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2271179
    iget-object v0, p0, LX/Fgu;->h:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271180
    iget-object v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v1

    .line 2271181
    if-nez v0, :cond_0

    .line 2271182
    const-string v0, "MarketplaceSearch"

    .line 2271183
    :goto_0
    return-object v0

    .line 2271184
    :cond_0
    sget-object v0, LX/Fgs;->a:[I

    iget-object v1, p0, LX/Fgu;->h:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271185
    iget-object p0, v1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v1, p0

    .line 2271186
    invoke-virtual {v1}, LX/103;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2271187
    const-string v0, "MarketplaceSearch"

    goto :goto_0

    .line 2271188
    :pswitch_0
    const-string v0, "B2CSearch"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/7Hi;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2271189
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2271190
    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/Fid;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2271167
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2271191
    iget-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2271192
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2271193
    if-eqz v0, :cond_0

    .line 2271194
    iget-object v1, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 2271195
    iget-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2271196
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2271197
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2271198
    :cond_0
    return-void

    .line 2271199
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0zw;Landroid/view/View;Z)V
    .locals 0
    .param p1    # LX/0zw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;",
            "Landroid/view/View;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2271200
    return-void
.end method

.method public final a(LX/7HP;)V
    .locals 0

    .prologue
    .line 2271201
    return-void
.end method

.method public final a(LX/Cvp;)V
    .locals 0

    .prologue
    .line 2271202
    return-void
.end method

.method public final a(LX/Fh9;)V
    .locals 0

    .prologue
    .line 2271145
    return-void
.end method

.method public final a(Landroid/content/Context;LX/Fh8;)V
    .locals 0

    .prologue
    .line 2271203
    return-void
.end method

.method public final a(Landroid/view/View;LX/FhA;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2271204
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2271205
    const-string v1, "ReactRouteName"

    const-string v2, "SearchTypeaheadResultsRoute"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2271206
    const-string v1, "ReactURI"

    const-string v2, "search_typeahead_results/?module=%s"

    invoke-direct {p0}, LX/Fgu;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2271207
    const-string v1, "non_immersive"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2271208
    iget-object v1, p0, LX/Fgu;->c:LX/CIx;

    invoke-virtual {v1, v0}, LX/CIx;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    iput-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2271209
    iget-object v0, p0, LX/Fgu;->b:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2271210
    const v1, 0x7f0d1244

    iget-object v2, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 2271211
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2271212
    iget-object v0, p0, LX/Fgu;->d:LX/Fga;

    iget-object v1, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v0, v1}, LX/Fga;->a(Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;)LX/FgZ;

    move-result-object v0

    iput-object v0, p0, LX/Fgu;->j:LX/FgZ;

    .line 2271213
    new-instance v0, LX/Fgt;

    iget-object v1, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-direct {v0, p0, p2, v1}, LX/Fgt;-><init>(LX/Fgu;LX/FhA;Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;)V

    iput-object v0, p0, LX/Fgu;->g:LX/Fgt;

    .line 2271214
    new-instance v0, LX/Bno;

    new-instance v1, LX/Fgr;

    invoke-direct {v1, p0, p2}, LX/Fgr;-><init>(LX/Fgu;LX/FhA;)V

    invoke-direct {v0, v1}, LX/Bno;-><init>(LX/Bnp;)V

    iput-object v0, p0, LX/Fgu;->k:LX/Bno;

    .line 2271215
    iget-object v0, p0, LX/Fgu;->f:LX/Bnj;

    iget-object v1, p0, LX/Fgu;->g:LX/Fgt;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2271216
    iget-object v0, p0, LX/Fgu;->f:LX/Bnj;

    iget-object v1, p0, LX/Fgu;->k:LX/Bno;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2271217
    return-void
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 0

    .prologue
    .line 2271218
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 0

    .prologue
    .line 2271219
    iput-object p1, p0, LX/Fgu;->h:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271220
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2271221
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7BE;Z)V
    .locals 0

    .prologue
    .line 2271222
    return-void
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2271168
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2271169
    return-object v0
.end method

.method public final d()LX/1Qq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2271137
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/7HZ;
    .locals 1

    .prologue
    .line 2271138
    sget-object v0, LX/7HZ;->ACTIVE:LX/7HZ;

    return-object v0
.end method

.method public final f()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2271139
    iget-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2271140
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, p0

    .line 2271141
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2271142
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()LX/2SP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2271143
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 1

    .prologue
    .line 2271144
    iget-object v0, p0, LX/Fgu;->h:Lcom/facebook/search/api/GraphSearchQuery;

    return-object v0
.end method

.method public final j()LX/FgU;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2271146
    iget-object v0, p0, LX/Fgu;->j:LX/FgZ;

    return-object v0
.end method

.method public final k()LX/Fi7;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2271147
    iget-object v0, p0, LX/Fgu;->e:LX/FiD;

    return-object v0
.end method

.method public final l()LX/Fid;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2271148
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 2271149
    return-void
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 2271150
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2271151
    iget-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-eqz v0, :cond_0

    .line 2271152
    iget-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 2271153
    iget-object v0, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 2271154
    iput-object v2, p0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    .line 2271155
    :cond_0
    iget-object v0, p0, LX/Fgu;->g:LX/Fgt;

    if-eqz v0, :cond_1

    .line 2271156
    iget-object v0, p0, LX/Fgu;->f:LX/Bnj;

    iget-object v1, p0, LX/Fgu;->g:LX/Fgt;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2271157
    iput-object v2, p0, LX/Fgu;->g:LX/Fgt;

    .line 2271158
    :cond_1
    iget-object v0, p0, LX/Fgu;->k:LX/Bno;

    if-eqz v0, :cond_2

    .line 2271159
    iget-object v0, p0, LX/Fgu;->f:LX/Bnj;

    iget-object v1, p0, LX/Fgu;->k:LX/Bno;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2271160
    iput-object v2, p0, LX/Fgu;->k:LX/Bno;

    .line 2271161
    :cond_2
    iget-object v0, p0, LX/Fgu;->j:LX/FgZ;

    if-eqz v0, :cond_3

    .line 2271162
    iget-object v0, p0, LX/Fgu;->j:LX/FgZ;

    invoke-virtual {v0, v2}, LX/FgU;->b(LX/0P1;)V

    .line 2271163
    iput-object v2, p0, LX/Fgu;->j:LX/FgZ;

    .line 2271164
    :cond_3
    return-void
.end method

.method public final p()V
    .locals 0

    .prologue
    .line 2271165
    return-void
.end method

.method public final q()V
    .locals 0

    .prologue
    .line 2271166
    return-void
.end method
