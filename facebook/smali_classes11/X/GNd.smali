.class public final LX/GNd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GNc;


# instance fields
.field public final synthetic a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V
    .locals 0

    .prologue
    .line 2346308
    iput-object p1, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2346309
    iget-object v0, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    if-nez v0, :cond_0

    .line 2346310
    iget-object v0, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    invoke-static {v0}, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->k(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    .line 2346311
    :goto_0
    return-void

    .line 2346312
    :cond_0
    iget-object v0, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->h:LX/1e6;

    iget-object v1, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2346313
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346314
    new-instance v3, LX/4Cp;

    invoke-direct {v3}, LX/4Cp;-><init>()V

    iget-object v2, v0, LX/1e6;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2346315
    const-string v4, "actor_id"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346316
    move-object v2, v3

    .line 2346317
    const-string v3, "ads_experience_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346318
    move-object v2, v2

    .line 2346319
    new-instance v3, LX/AD0;

    invoke-direct {v3}, LX/AD0;-><init>()V

    move-object v3, v3

    .line 2346320
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2346321
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 2346322
    iget-object v3, v0, LX/1e6;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2346323
    new-instance v1, LX/GNb;

    invoke-direct {v1, p0}, LX/GNb;-><init>(LX/GNd;)V

    iget-object v2, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v2, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->j:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2346324
    iget-object v0, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    if-nez v0, :cond_0

    .line 2346325
    iget-object v0, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    invoke-static {v0}, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->k(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    .line 2346326
    :goto_0
    return-void

    .line 2346327
    :cond_0
    iget-object v0, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->h:LX/1e6;

    iget-object v1, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2346328
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346329
    new-instance v3, LX/4Co;

    invoke-direct {v3}, LX/4Co;-><init>()V

    iget-object v2, v0, LX/1e6;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2346330
    const-string v4, "actor_id"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346331
    move-object v2, v3

    .line 2346332
    const-string v3, "ads_experience_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346333
    move-object v2, v2

    .line 2346334
    new-instance v3, LX/ACz;

    invoke-direct {v3}, LX/ACz;-><init>()V

    move-object v3, v3

    .line 2346335
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2346336
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 2346337
    iget-object v3, v0, LX/1e6;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2346338
    iget-object v0, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->a$redex0(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;Z)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2346339
    iget-object v0, p0, LX/GNd;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->a$redex0(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;Z)V

    .line 2346340
    return-void
.end method
