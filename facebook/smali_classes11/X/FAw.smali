.class public LX/FAw;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FAw;


# direct methods
.method public constructor <init>()V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2208536
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2208537
    sget-object v0, LX/0ax;->bY:Ljava/lang/String;

    const-class v1, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2208538
    const-string v0, "albums/{#%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "owner_id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "tab_to_show"

    aput-object v3, v2, v6

    const-string v3, "albums"

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2208539
    sget-object v0, LX/0ax;->ca:Ljava/lang/String;

    const-class v1, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "tab_to_show"

    aput-object v3, v2, v6

    const-string v3, "albums"

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2208540
    const-string v0, "photosync"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "tab_to_show"

    aput-object v3, v2, v6

    const-string v3, "sync"

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2208541
    const-string v0, "syncnux"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "tab_to_show"

    aput-object v3, v2, v6

    const-string v3, "sync"

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2208542
    const-string v0, "mediaset/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "set_token"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/activity/media/photoset/PhotoSetActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2208543
    const-string v0, "native_album/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "extra_album_id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2208544
    sget-object v0, LX/0ax;->eJ:Ljava/lang/String;

    const-class v1, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "edit_profile_pic"

    aput-object v3, v2, v6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2208545
    sget-object v0, LX/0ax;->fi:Ljava/lang/String;

    const-string v1, "{set_token}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "pick_hc_pic"

    aput-object v3, v2, v6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    const-string v3, "show_suggested_tab"

    aput-object v3, v2, v7

    const/4 v3, 0x3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "hide_photos_of_tab"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "tab_to_show"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "campaign"

    aput-object v4, v2, v3

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2208546
    sget-object v0, LX/0ax;->fh:Ljava/lang/String;

    const-class v1, Lcom/facebook/katana/activity/photos/PhotosTabActivity;

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "pick_hc_pic"

    aput-object v3, v2, v6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v2}, LX/FBy;->a([Ljava/lang/Object;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2208547
    return-void
.end method

.method public static a(LX/0QB;)LX/FAw;
    .locals 3

    .prologue
    .line 2208548
    sget-object v0, LX/FAw;->a:LX/FAw;

    if-nez v0, :cond_1

    .line 2208549
    const-class v1, LX/FAw;

    monitor-enter v1

    .line 2208550
    :try_start_0
    sget-object v0, LX/FAw;->a:LX/FAw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2208551
    if-eqz v2, :cond_0

    .line 2208552
    :try_start_1
    new-instance v0, LX/FAw;

    invoke-direct {v0}, LX/FAw;-><init>()V

    .line 2208553
    move-object v0, v0

    .line 2208554
    sput-object v0, LX/FAw;->a:LX/FAw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2208555
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2208556
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2208557
    :cond_1
    sget-object v0, LX/FAw;->a:LX/FAw;

    return-object v0

    .line 2208558
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2208559
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
