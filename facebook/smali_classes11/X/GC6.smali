.class public final LX/GC6;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;)V
    .locals 0

    .prologue
    .line 2328032
    iput-object p1, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v1, 0x8

    .line 2328033
    iget-object v0, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->e:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2328034
    iget-object v0, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2328035
    iget-object v0, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2328036
    :goto_0
    return-void

    .line 2328037
    :cond_0
    iget-object v0, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2328038
    iget-object v0, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2328039
    iget-object v0, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    iget-object v1, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    const v2, 0x7f083543

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2328040
    iget-object v0, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    iget-object v1, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2328041
    iget-object v0, p0, LX/GC6;->a:Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/ResetPasswordFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
