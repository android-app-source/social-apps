.class public LX/FIW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;",
        "Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2222208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/FIW;
    .locals 1

    .prologue
    .line 2222209
    new-instance v0, LX/FIW;

    invoke-direct {v0}, LX/FIW;-><init>()V

    .line 2222210
    move-object v0, v0

    .line 2222211
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2222212
    check-cast p1, Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;

    .line 2222213
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "udp_upload"

    .line 2222214
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2222215
    move-object v0, v0

    .line 2222216
    const-string v1, "POST"

    .line 2222217
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2222218
    move-object v0, v0

    .line 2222219
    const-string v1, "/me/udp_uploads"

    .line 2222220
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2222221
    move-object v0, v0

    .line 2222222
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2222223
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "user_id"

    iget-wide v6, p1, Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2222224
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2222225
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2222226
    move-object v0, v0

    .line 2222227
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2222228
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2222229
    move-object v0, v0

    .line 2222230
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2222231
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;->a(LX/0lF;)Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;

    move-result-object v0

    return-object v0
.end method
