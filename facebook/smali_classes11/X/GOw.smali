.class public final enum LX/GOw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GOw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GOw;

.field public static final enum GET_ADDED_PAYPAL:LX/GOw;

.field public static final enum GET_PAYMENT_METHODS:LX/GOw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2348060
    new-instance v0, LX/GOw;

    const-string v1, "GET_PAYMENT_METHODS"

    invoke-direct {v0, v1, v2}, LX/GOw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOw;->GET_PAYMENT_METHODS:LX/GOw;

    .line 2348061
    new-instance v0, LX/GOw;

    const-string v1, "GET_ADDED_PAYPAL"

    invoke-direct {v0, v1, v3}, LX/GOw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOw;->GET_ADDED_PAYPAL:LX/GOw;

    .line 2348062
    const/4 v0, 0x2

    new-array v0, v0, [LX/GOw;

    sget-object v1, LX/GOw;->GET_PAYMENT_METHODS:LX/GOw;

    aput-object v1, v0, v2

    sget-object v1, LX/GOw;->GET_ADDED_PAYPAL:LX/GOw;

    aput-object v1, v0, v3

    sput-object v0, LX/GOw;->$VALUES:[LX/GOw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2348059
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GOw;
    .locals 1

    .prologue
    .line 2348063
    const-class v0, LX/GOw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GOw;

    return-object v0
.end method

.method public static values()[LX/GOw;
    .locals 1

    .prologue
    .line 2348058
    sget-object v0, LX/GOw;->$VALUES:[LX/GOw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GOw;

    return-object v0
.end method
