.class public LX/FwR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/FwT;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final c:LX/0SG;


# direct methods
.method public constructor <init>(LX/0QR;Landroid/os/Handler;LX/0SG;)V
    .locals 0
    .param p1    # LX/0QR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QR",
            "<",
            "LX/FwT;",
            ">;",
            "Landroid/os/Handler;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303137
    iput-object p1, p0, LX/FwR;->a:LX/0QR;

    .line 2303138
    iput-object p2, p0, LX/FwR;->b:Landroid/os/Handler;

    .line 2303139
    iput-object p3, p0, LX/FwR;->c:LX/0SG;

    .line 2303140
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;)V
    .locals 5

    .prologue
    .line 2303141
    iget-object v0, p0, LX/FwR;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/timeline/header/editphotohelper/ProfilePictureActionFlowLauncher$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/timeline/header/editphotohelper/ProfilePictureActionFlowLauncher$1;-><init>(LX/FwR;Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;)V

    const-wide/16 v2, 0x3e8

    const v4, -0x4071af04

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2303142
    return-void
.end method
