.class public LX/FkO;
.super LX/48b;
.source ""


# instance fields
.field private final a:LX/FkP;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/FkP;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278509
    invoke-direct {p0}, LX/48b;-><init>()V

    .line 2278510
    iput-object p1, p0, LX/FkO;->a:LX/FkP;

    .line 2278511
    iput-object p2, p0, LX/FkO;->b:Landroid/content/Context;

    .line 2278512
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2278513
    iget-object v0, p0, LX/FkO;->a:LX/FkP;

    .line 2278514
    iget-object v1, v0, LX/FkP;->a:LX/0Uh;

    const/16 v2, 0x4b8

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2278515
    if-eqz v0, :cond_1

    .line 2278516
    const-string v0, "fundraiser_charity_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "fundraiser_campaign_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2278517
    if-eqz v0, :cond_2

    .line 2278518
    :cond_1
    :goto_1
    return-object p1

    .line 2278519
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/FkO;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2278520
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object p1, v0

    .line 2278521
    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 2278522
    iget-object v0, p0, LX/FkO;->a:LX/FkP;

    .line 2278523
    iget-object v1, v0, LX/FkP;->a:LX/0Uh;

    const/16 v2, 0x4bd

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2278524
    if-nez v0, :cond_0

    iget-object v0, p0, LX/FkO;->a:LX/FkP;

    .line 2278525
    iget-object v1, v0, LX/FkP;->a:LX/0Uh;

    const/16 v2, 0x4bf

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2278526
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
