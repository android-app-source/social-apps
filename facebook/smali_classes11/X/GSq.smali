.class public final enum LX/GSq;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/GSp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GSq;",
        ">;",
        "LX/GSp;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GSq;

.field public static final enum APP_INVITES_DIALOG:LX/GSq;


# instance fields
.field private minVersion:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2354810
    new-instance v0, LX/GSq;

    const-string v1, "APP_INVITES_DIALOG"

    const v2, 0x133529d

    invoke-direct {v0, v1, v3, v2}, LX/GSq;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GSq;->APP_INVITES_DIALOG:LX/GSq;

    .line 2354811
    const/4 v0, 0x1

    new-array v0, v0, [LX/GSq;

    sget-object v1, LX/GSq;->APP_INVITES_DIALOG:LX/GSq;

    aput-object v1, v0, v3

    sput-object v0, LX/GSq;->$VALUES:[LX/GSq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2354807
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2354808
    iput p3, p0, LX/GSq;->minVersion:I

    .line 2354809
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GSq;
    .locals 1

    .prologue
    .line 2354814
    const-class v0, LX/GSq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GSq;

    return-object v0
.end method

.method public static values()[LX/GSq;
    .locals 1

    .prologue
    .line 2354815
    sget-object v0, LX/GSq;->$VALUES:[LX/GSq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GSq;

    return-object v0
.end method


# virtual methods
.method public final getAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2354813
    const-string v0, "com.facebook.platform.action.request.APPINVITES_DIALOG"

    return-object v0
.end method

.method public final getMinVersion()I
    .locals 1

    .prologue
    .line 2354812
    iget v0, p0, LX/GSq;->minVersion:I

    return v0
.end method
