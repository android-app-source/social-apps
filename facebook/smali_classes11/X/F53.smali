.class public LX/F53;
.super Ljava/util/Observable;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/GroupPurposes;
    .end annotation
.end field

.field private static t:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:LX/0ad;

.field public final d:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

.field public e:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

.field public f:LX/F52;

.field private g:Ljava/lang/Boolean;

.field public h:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/GroupVisibility;
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/GroupPurposes;
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public r:Ljava/lang/String;

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2197949
    const/4 v0, 0x0

    sput-object v0, LX/F53;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;)V
    .locals 11
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2197950
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 2197951
    sget-object v0, LX/F52;->Invite:LX/F52;

    iput-object v0, p0, LX/F53;->f:LX/F52;

    .line 2197952
    iput-object v1, p0, LX/F53;->g:Ljava/lang/Boolean;

    .line 2197953
    const-string v0, "CLOSED"

    iput-object v0, p0, LX/F53;->h:Ljava/lang/String;

    .line 2197954
    iput-object v1, p0, LX/F53;->i:Ljava/lang/String;

    .line 2197955
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/F53;->m:Ljava/util/List;

    .line 2197956
    const-string v0, ""

    iput-object v0, p0, LX/F53;->n:Ljava/lang/String;

    .line 2197957
    iput-object v1, p0, LX/F53;->o:Ljava/lang/String;

    .line 2197958
    iput-object p1, p0, LX/F53;->b:Landroid/content/res/Resources;

    .line 2197959
    iput-object p2, p0, LX/F53;->c:LX/0ad;

    .line 2197960
    new-instance v0, LX/F5B;

    invoke-direct {v0}, LX/F5B;-><init>()V

    iget-object v1, p0, LX/F53;->b:Landroid/content/res/Resources;

    const v2, 0x7f083185

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2197961
    iput-object v1, v0, LX/F5B;->b:Ljava/lang/String;

    .line 2197962
    move-object v0, v0

    .line 2197963
    const-string v1, "-1"

    .line 2197964
    iput-object v1, v0, LX/F5B;->a:Ljava/lang/String;

    .line 2197965
    move-object v0, v0

    .line 2197966
    const/4 v7, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 2197967
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2197968
    iget-object v4, v0, LX/F5B;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2197969
    iget-object v6, v0, LX/F5B;->b:Ljava/lang/String;

    invoke-virtual {v3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2197970
    iget-object v8, v0, LX/F5B;->c:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v3, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 2197971
    const/4 v9, 0x3

    invoke-virtual {v3, v9}, LX/186;->c(I)V

    .line 2197972
    invoke-virtual {v3, v10, v4}, LX/186;->b(II)V

    .line 2197973
    invoke-virtual {v3, v7, v6}, LX/186;->b(II)V

    .line 2197974
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 2197975
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 2197976
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 2197977
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2197978
    invoke-virtual {v4, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2197979
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2197980
    new-instance v4, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    invoke-direct {v4, v3}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;-><init>(LX/15i;)V

    .line 2197981
    move-object v0, v4

    .line 2197982
    iput-object v0, p0, LX/F53;->d:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    .line 2197983
    invoke-virtual {p0}, LX/F53;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2197984
    sget-object v0, LX/F52;->Type:LX/F52;

    iput-object v0, p0, LX/F53;->f:LX/F52;

    .line 2197985
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/F53;
    .locals 5

    .prologue
    .line 2197929
    const-class v1, LX/F53;

    monitor-enter v1

    .line 2197930
    :try_start_0
    sget-object v0, LX/F53;->t:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2197931
    sput-object v2, LX/F53;->t:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2197932
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2197933
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2197934
    new-instance p0, LX/F53;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/F53;-><init>(Landroid/content/res/Resources;LX/0ad;)V

    .line 2197935
    move-object v0, p0

    .line 2197936
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2197937
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/F53;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2197938
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2197939
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static s(LX/F53;)V
    .locals 0

    .prologue
    .line 2197992
    invoke-virtual {p0}, LX/F53;->setChanged()V

    .line 2197993
    invoke-virtual {p0}, LX/F53;->notifyObservers()V

    .line 2197994
    return-void
.end method


# virtual methods
.method public final a(LX/F52;)V
    .locals 0

    .prologue
    .line 2197986
    iput-object p1, p0, LX/F53;->f:LX/F52;

    .line 2197987
    invoke-static {p0}, LX/F53;->s(LX/F53;)V

    .line 2197988
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2197989
    iput-boolean p1, p0, LX/F53;->s:Z

    .line 2197990
    invoke-static {p0}, LX/F53;->s(LX/F53;)V

    .line 2197991
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2197940
    iput-object p1, p0, LX/F53;->o:Ljava/lang/String;

    .line 2197941
    invoke-virtual {p0}, LX/F53;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/F53;->f:LX/F52;

    sget-object v1, LX/F52;->Type:LX/F52;

    if-ne v0, v1, :cond_0

    .line 2197942
    sget-object v0, LX/F52;->Invite:LX/F52;

    invoke-virtual {p0, v0}, LX/F53;->a(LX/F52;)V

    .line 2197943
    :cond_0
    return-void
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2197944
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2197945
    iget-object v0, p0, LX/F53;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2197946
    iget-object p0, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, p0

    .line 2197947
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2197948
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupPurposes;
        .end annotation
    .end param

    .prologue
    .line 2197916
    iput-object p1, p0, LX/F53;->j:Ljava/lang/String;

    .line 2197917
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/F53;->k:Z

    .line 2197918
    invoke-static {p0}, LX/F53;->s(LX/F53;)V

    .line 2197919
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupVisibility;
        .end annotation
    .end param

    .prologue
    .line 2197906
    if-nez p1, :cond_0

    .line 2197907
    :goto_0
    return-void

    .line 2197908
    :cond_0
    iput-object p1, p0, LX/F53;->h:Ljava/lang/String;

    .line 2197909
    invoke-static {p0}, LX/F53;->s(LX/F53;)V

    goto :goto_0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2197910
    const-string v0, "GROUPS_YOU_SHOULD_CREATE_GROUPS_MALL"

    iget-object v1, p0, LX/F53;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GROUPS_YOU_SHOULD_CREATE_TAB"

    iget-object v1, p0, LX/F53;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2197911
    :cond_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2197912
    :goto_0
    return-object v0

    .line 2197913
    :cond_1
    iget-object v0, p0, LX/F53;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    .line 2197914
    iget-object v0, p0, LX/F53;->c:LX/0ad;

    sget-short v1, LX/F6S;->a:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/F53;->g:Ljava/lang/Boolean;

    .line 2197915
    :cond_2
    iget-object v0, p0, LX/F53;->g:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2197920
    iput-object p1, p0, LX/F53;->p:Ljava/lang/String;

    .line 2197921
    invoke-static {p0}, LX/F53;->s(LX/F53;)V

    .line 2197922
    return-void
.end method

.method public final k()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2197923
    iget-object v0, p0, LX/F53;->e:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    if-eqz v0, :cond_0

    .line 2197924
    iget-object v0, p0, LX/F53;->e:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2197925
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2197926
    iget-object v0, p0, LX/F53;->e:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F53;->e:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2197927
    iget-object v0, p0, LX/F53;->e:Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2197928
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "-1"

    goto :goto_0
.end method
