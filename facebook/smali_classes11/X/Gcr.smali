.class public final LX/Gcr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public final synthetic a:Lcom/facebook/events/carousel/EventCardViewBinder;


# direct methods
.method public constructor <init>(Lcom/facebook/events/carousel/EventCardViewBinder;)V
    .locals 0

    .prologue
    .line 2372640
    iput-object p1, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 7

    .prologue
    .line 2372654
    new-instance v0, LX/7vC;

    iget-object v1, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v1, v1, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    invoke-direct {v0, v1}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    .line 2372655
    iget-object v1, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372656
    iput-object p2, v0, LX/7vC;->C:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2372657
    move-object v0, v0

    .line 2372658
    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2372659
    iput-object v0, v1, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372660
    iget-object v0, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    invoke-static {v0}, Lcom/facebook/events/carousel/EventCardViewBinder;->c(Lcom/facebook/events/carousel/EventCardViewBinder;)V

    .line 2372661
    iget-object v0, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->h:LX/7vW;

    iget-object v1, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v1, v1, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372662
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2372663
    iget-object v2, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v2, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v2, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v2, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    iget-object v2, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v5, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->x:Lcom/facebook/events/common/ActionMechanism;

    iget-object v2, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v6, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->z:Ljava/lang/String;

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, LX/7vW;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2372664
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 2372641
    new-instance v0, LX/7vC;

    iget-object v1, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v1, v1, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    invoke-direct {v0, v1}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    .line 2372642
    iget-object v1, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    const/4 v2, 0x0

    .line 2372643
    iput-boolean v2, v0, LX/7vC;->H:Z

    .line 2372644
    move-object v0, v0

    .line 2372645
    iput-object p2, v0, LX/7vC;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 2372646
    move-object v0, v0

    .line 2372647
    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2372648
    iput-object v0, v1, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372649
    iget-object v0, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    invoke-static {v0}, Lcom/facebook/events/carousel/EventCardViewBinder;->c(Lcom/facebook/events/carousel/EventCardViewBinder;)V

    .line 2372650
    iget-object v0, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->i:LX/7vZ;

    iget-object v1, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v1, v1, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372651
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2372652
    iget-object v2, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v2, v2, Lcom/facebook/events/carousel/EventCardViewBinder;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, p0, LX/Gcr;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v3, v3, Lcom/facebook/events/carousel/EventCardViewBinder;->x:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, p2, v2, v3}, LX/7vZ;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2372653
    return-void
.end method
