.class public LX/FBq;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FBq;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2209788
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2209789
    const-string v0, "redirect?href={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "destination"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/FBp;

    invoke-direct {v1}, LX/FBp;-><init>()V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2209790
    return-void
.end method

.method public static a(LX/0QB;)LX/FBq;
    .locals 3

    .prologue
    .line 2209791
    sget-object v0, LX/FBq;->a:LX/FBq;

    if-nez v0, :cond_1

    .line 2209792
    const-class v1, LX/FBq;

    monitor-enter v1

    .line 2209793
    :try_start_0
    sget-object v0, LX/FBq;->a:LX/FBq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2209794
    if-eqz v2, :cond_0

    .line 2209795
    :try_start_1
    new-instance v0, LX/FBq;

    invoke-direct {v0}, LX/FBq;-><init>()V

    .line 2209796
    move-object v0, v0

    .line 2209797
    sput-object v0, LX/FBq;->a:LX/FBq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2209798
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2209799
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2209800
    :cond_1
    sget-object v0, LX/FBq;->a:LX/FBq;

    return-object v0

    .line 2209801
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2209802
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
