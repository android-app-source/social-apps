.class public LX/GJi;
.super LX/GIZ;
.source ""


# instance fields
.field public d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

.field public e:LX/GMm;


# direct methods
.method public constructor <init>(LX/GMm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2339458
    invoke-direct {p0}, LX/GIZ;-><init>()V

    .line 2339459
    iput-object p1, p0, LX/GJi;->e:LX/GMm;

    .line 2339460
    return-void
.end method

.method public static b(LX/0QB;)LX/GJi;
    .locals 1

    .prologue
    .line 2339461
    invoke-static {p0}, LX/GJi;->c(LX/0QB;)LX/GJi;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0QB;)LX/GJi;
    .locals 2

    .prologue
    .line 2339456
    new-instance v1, LX/GJi;

    invoke-static {p0}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object v0

    check-cast v0, LX/GMm;

    invoke-direct {v1, v0}, LX/GJi;-><init>(LX/GMm;)V

    .line 2339457
    return-object v1
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339406
    check-cast p1, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p0, p1, p2}, LX/GIZ;->a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2339453
    invoke-super {p0, p1}, LX/GIZ;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2339454
    iget-object v0, p0, LX/GIZ;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    iput-object v0, p0, LX/GJi;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    .line 2339455
    return-void
.end method

.method public final a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 9
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339407
    invoke-super {p0, p1, p2}, LX/GIZ;->a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2339408
    iget-object v0, p0, LX/GIZ;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    .line 2339409
    iget-object v1, p0, LX/GIZ;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v1}, LX/GG6;->h(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2339410
    iget-object v0, p0, LX/GIZ;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, LX/GIZ;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/GMm;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;I)V

    .line 2339411
    iget-object v0, p0, LX/GIZ;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, LX/GJi;->e:LX/GMm;

    const v2, 0x7f080a3b

    const-string v3, "https://m.facebook.com/ads/manage/accounts/?select"

    iget-object p1, p0, LX/GIZ;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2339412
    iget-object p2, p0, LX/GHg;->b:LX/GCE;

    move-object p2, p2

    .line 2339413
    invoke-virtual {v1, v2, v3, p1, p2}, LX/GMm;->a(ILjava/lang/String;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;LX/GCE;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2339414
    :cond_0
    :goto_0
    return-void

    .line 2339415
    :cond_1
    sget-object v1, LX/GJh;->a:[I

    invoke-virtual {v0}, LX/GGB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2339416
    :pswitch_0
    iget-object v0, p0, LX/GJi;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 2339417
    if-nez v0, :cond_5

    move v4, v3

    :goto_1
    if-eqz v4, :cond_7

    move v4, v3

    :goto_2
    if-eqz v4, :cond_9

    move v4, v3

    :goto_3
    if-eqz v4, :cond_b

    .line 2339418
    :cond_2
    :goto_4
    move v0, v2

    .line 2339419
    if-eqz v0, :cond_0

    .line 2339420
    iget-object v0, p0, LX/GIZ;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, LX/GIZ;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/GMm;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;I)V

    .line 2339421
    iget-object v0, p0, LX/GIZ;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, LX/GJi;->e:LX/GMm;

    const v2, 0x7f080a3a

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://m.facebook.com/ads/manage/billing?account_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/GJi;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/GIZ;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2339422
    iget-object v5, p0, LX/GHg;->b:LX/GCE;

    move-object v5, v5

    .line 2339423
    invoke-virtual {v1, v2, v3, v4, v5}, LX/GMm;->a(ILjava/lang/String;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;LX/GCE;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2339424
    goto :goto_0

    .line 2339425
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, LX/GMm;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;I)V

    .line 2339426
    iget-object v0, p0, LX/GIZ;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->p()LX/175;

    move-result-object v0

    .line 2339427
    if-eqz v0, :cond_c

    .line 2339428
    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    .line 2339429
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2339430
    invoke-interface {v0}, LX/175;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2339431
    invoke-interface {v0}, LX/175;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_5
    if-ge v4, v7, :cond_4

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1W5;

    .line 2339432
    const/4 v3, 0x0

    .line 2339433
    invoke-interface {v2}, LX/1W5;->a()LX/171;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-interface {v2}, LX/1W5;->a()LX/171;

    move-result-object v8

    invoke-interface {v8}, LX/171;->j()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 2339434
    invoke-interface {v2}, LX/1W5;->a()LX/171;

    move-result-object v3

    invoke-interface {v3}, LX/171;->j()Ljava/lang/String;

    move-result-object v3

    .line 2339435
    :cond_3
    new-instance v8, LX/479;

    new-instance p2, LX/1yL;

    invoke-interface {v2}, LX/1W5;->c()I

    move-result p0

    invoke-interface {v2}, LX/1W5;->b()I

    move-result v2

    invoke-direct {p2, p0, v2}, LX/1yL;-><init>(II)V

    invoke-direct {v8, p2, v3}, LX/479;-><init>(LX/1yL;Ljava/lang/Object;)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2339436
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    .line 2339437
    :cond_4
    move-object v0, v5

    .line 2339438
    invoke-virtual {p1, v1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2339439
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2339440
    :goto_6
    goto/16 :goto_0

    .line 2339441
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-nez v4, :cond_6

    move v4, v3

    goto/16 :goto_1

    :cond_6
    move v4, v2

    goto/16 :goto_1

    .line 2339442
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2339443
    invoke-virtual {v5, v4, v2}, LX/15i;->g(II)I

    move-result v4

    if-nez v4, :cond_8

    move v4, v3

    goto/16 :goto_2

    :cond_8
    move v4, v2

    goto/16 :goto_2

    .line 2339444
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2339445
    invoke-virtual {v5, v4, v3}, LX/15i;->g(II)I

    move-result v4

    if-nez v4, :cond_a

    move v4, v3

    goto/16 :goto_3

    :cond_a
    move v4, v2

    goto/16 :goto_3

    .line 2339446
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v5, v4, v2}, LX/15i;->g(II)I

    move-result v4

    .line 2339447
    invoke-virtual {v5, v4, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2339448
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {v6, v5, v3}, LX/15i;->g(II)I

    move-result v5

    .line 2339449
    invoke-virtual {v6, v5, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 2339450
    invoke-virtual {v4, v5}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v4

    if-nez v4, :cond_2

    move v2, v3

    .line 2339451
    goto/16 :goto_4

    .line 2339452
    :cond_c
    const v0, 0x7f080a46

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(I)V

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
