.class public LX/FRm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wI",
        "<",
        "Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;",
        "LX/FRw;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0wM;

.field public final c:LX/6zs;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wM;LX/6zs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240737
    iput-object p1, p0, LX/FRm;->a:Landroid/content/Context;

    .line 2240738
    iput-object p2, p0, LX/FRm;->b:LX/0wM;

    .line 2240739
    iput-object p3, p0, LX/FRm;->c:LX/6zs;

    .line 2240740
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2240741
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0Pz;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/6vm;",
            ">;",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
            "Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2240742
    new-instance v0, LX/716;

    iget-object v1, p0, LX/FRm;->a:Landroid/content/Context;

    const v2, 0x7f080c7a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240743
    iget-object v0, p2, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    move-object v2, v0

    .line 2240744
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 2240745
    invoke-static {}, LX/701;->newBuilder()LX/702;

    move-result-object v4

    .line 2240746
    iput-object v0, v4, LX/702;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 2240747
    move-object v4, v4

    .line 2240748
    iget-object v5, p2, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object v5, v5

    .line 2240749
    sget-object v6, LX/FRl;->b:[I

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v7

    invoke-virtual {v7}, LX/6zU;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 2240750
    const/4 v6, 0x0

    :goto_1
    move-object v0, v6

    .line 2240751
    iput-object v0, v4, LX/702;->c:Landroid/content/Intent;

    .line 2240752
    move-object v0, v4

    .line 2240753
    const/16 v4, 0x193

    .line 2240754
    iput v4, v0, LX/702;->d:I

    .line 2240755
    move-object v0, v0

    .line 2240756
    iget-object v4, p3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2240757
    iput-object v4, v0, LX/702;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2240758
    move-object v0, v0

    .line 2240759
    invoke-virtual {v0}, LX/702;->f()LX/701;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240760
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2240761
    :cond_0
    iget-object v0, p2, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->f:LX/0Px;

    move-object v2, v0

    .line 2240762
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;

    .line 2240763
    iget-object v4, p0, LX/FRm;->c:LX/6zs;

    invoke-virtual {v4, v0, p1, p2, p3}, LX/6zs;->a(Lcom/facebook/payments/paymentmethods/model/NewPaymentOption;LX/0Pz;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;)V

    .line 2240764
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2240765
    :cond_1
    return-void

    .line 2240766
    :pswitch_0
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 2240767
    iget-object v6, p3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v6, v6, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->c:Ljava/lang/String;

    iget-object v7, p3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v7, v7, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    invoke-static {v6, v7}, Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;->a(Ljava/lang/String;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/6xw;

    move-result-object v6

    sget-object v7, LX/6xZ;->UPDATE_CARD:LX/6xZ;

    .line 2240768
    iput-object v7, v6, LX/6xw;->c:LX/6xZ;

    .line 2240769
    move-object v6, v6

    .line 2240770
    invoke-virtual {v6}, LX/6xw;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;

    move-result-object v6

    .line 2240771
    invoke-static {}, Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;->newBuilder()LX/6yR;

    move-result-object v7

    invoke-static {}, Lcom/facebook/payments/decorator/PaymentsDecoratorParams;->b()Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    move-result-object v8

    .line 2240772
    iput-object v8, v7, LX/6yR;->c:Lcom/facebook/payments/decorator/PaymentsDecoratorParams;

    .line 2240773
    move-object v7, v7

    .line 2240774
    const/4 v8, 0x1

    .line 2240775
    iput-boolean v8, v7, LX/6yR;->d:Z

    .line 2240776
    move-object v7, v7

    .line 2240777
    invoke-virtual {v7}, LX/6yR;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    move-result-object v7

    .line 2240778
    sget-object v8, LX/6yO;->SIMPLE:LX/6yO;

    iget-object v9, p3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    invoke-static {v8, v6, v9}, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->a(LX/6yO;Lcom/facebook/payments/paymentmethods/cardform/CardFormAnalyticsParams;LX/6xg;)LX/6xy;

    move-result-object v6

    .line 2240779
    iput-object v0, v6, LX/6xy;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 2240780
    move-object v6, v6

    .line 2240781
    iput-object v7, v6, LX/6xy;->d:Lcom/facebook/payments/paymentmethods/cardform/CardFormStyleParams;

    .line 2240782
    move-object v6, v6

    .line 2240783
    iput-object v5, v6, LX/6xy;->g:Lcom/facebook/common/locale/Country;

    .line 2240784
    move-object v6, v6

    .line 2240785
    invoke-virtual {v6}, LX/6xy;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v6

    .line 2240786
    iget-object v7, p0, LX/FRm;->a:Landroid/content/Context;

    invoke-static {v7, v6}, Lcom/facebook/payments/paymentmethods/cardform/CardFormActivity;->a(Landroid/content/Context;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Landroid/content/Intent;

    move-result-object v6

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static c(Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)Lcom/facebook/payments/shipping/model/ShippingCommonParams;
    .locals 3

    .prologue
    .line 2240787
    invoke-static {}, Lcom/facebook/payments/shipping/model/ShippingCommonParams;->newBuilder()LX/72e;

    move-result-object v0

    sget-object v1, LX/72g;->SIMPLE:LX/72g;

    .line 2240788
    iput-object v1, v0, LX/72e;->a:LX/72g;

    .line 2240789
    move-object v1, v0

    .line 2240790
    iget-object v0, p0, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240791
    check-cast v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    iget-object v0, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 2240792
    iget-object v2, v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->b:Lcom/facebook/common/locale/Country;

    move-object v0, v2

    .line 2240793
    iput-object v0, v1, LX/72e;->b:Lcom/facebook/common/locale/Country;

    .line 2240794
    move-object v0, v1

    .line 2240795
    sget-object v1, LX/72f;->OTHERS:LX/72f;

    .line 2240796
    iput-object v1, v0, LX/72e;->e:LX/72f;

    .line 2240797
    move-object v0, v0

    .line 2240798
    invoke-virtual {p0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->f()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2240799
    iput-object v1, v0, LX/72e;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2240800
    move-object v1, v0

    .line 2240801
    invoke-virtual {p0}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    invoke-virtual {v0}, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 2240802
    iput-object v0, v1, LX/72e;->i:LX/6xg;

    .line 2240803
    move-object v0, v1

    .line 2240804
    invoke-virtual {v0}, LX/72e;->j()Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;LX/0Px;)LX/0Px;
    .locals 10

    .prologue
    .line 2240805
    check-cast p1, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    .line 2240806
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2240807
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FRw;

    .line 2240808
    sget-object v4, LX/FRl;->a:[I

    invoke-virtual {v0}, LX/FRw;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2240809
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled section type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2240810
    :pswitch_0
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240811
    check-cast v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    iget-object v4, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    invoke-virtual {v0}, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    invoke-direct {p0, v2, v4, v0}, LX/FRm;->a(LX/0Pz;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;)V

    .line 2240812
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2240813
    :pswitch_1
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240814
    check-cast v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    .line 2240815
    new-instance v4, LX/716;

    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    const v6, 0x7f081e02

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240816
    iget-object v4, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-interface {v4}, Lcom/facebook/payments/history/model/PaymentTransactions;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-interface {v4}, Lcom/facebook/payments/history/model/PaymentTransactions;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2240817
    :cond_0
    new-instance v4, LX/FR2;

    invoke-direct {v4}, LX/FR2;-><init>()V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240818
    :goto_2
    goto :goto_1

    .line 2240819
    :pswitch_2
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240820
    check-cast v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    .line 2240821
    new-instance v4, LX/716;

    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    const v6, 0x7f081e15

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240822
    new-instance v4, LX/FRp;

    iget-object v5, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->f:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-direct {v4, v5}, LX/FRp;-><init>(Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240823
    goto :goto_1

    .line 2240824
    :pswitch_3
    new-instance v0, LX/716;

    iget-object v4, p0, LX/FRm;->a:Landroid/content/Context;

    const v5, 0x7f081e0f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240825
    invoke-static {}, LX/FRr;->newBuilder()LX/FRq;

    move-result-object v0

    const-string v4, "https://m.facebook.com/ads/manager/billing/"

    invoke-static {v4}, LX/FRm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2240826
    iput-object v4, v0, LX/FRq;->a:Landroid/content/Intent;

    .line 2240827
    move-object v0, v0

    .line 2240828
    iget-object v4, p0, LX/FRm;->a:Landroid/content/Context;

    const v5, 0x7f081e10

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2240829
    iput-object v4, v0, LX/FRq;->c:Ljava/lang/String;

    .line 2240830
    move-object v0, v0

    .line 2240831
    iget-object v4, p0, LX/FRm;->b:LX/0wM;

    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0208f8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const v6, -0x4d4d4e

    invoke-virtual {v4, v5, v6}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 2240832
    iput-object v4, v0, LX/FRq;->d:Landroid/graphics/drawable/Drawable;

    .line 2240833
    move-object v0, v0

    .line 2240834
    invoke-virtual {v0}, LX/FRq;->f()LX/FRr;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240835
    goto/16 :goto_1

    .line 2240836
    :pswitch_4
    new-instance v0, LX/716;

    iget-object v4, p0, LX/FRm;->a:Landroid/content/Context;

    const v5, 0x7f081e0b    # 1.80931E38f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240837
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240838
    check-cast v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    .line 2240839
    iget-object v4, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->b:LX/0am;

    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2240840
    iget-object v0, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 2240841
    invoke-static {}, LX/72c;->newBuilder()LX/72b;

    move-result-object v4

    sget-object v5, LX/72f;->OTHERS:LX/72f;

    .line 2240842
    iput-object v5, v4, LX/72b;->a:LX/72f;

    .line 2240843
    move-object v4, v4

    .line 2240844
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v5

    check-cast v5, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    invoke-virtual {v5}, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v5

    .line 2240845
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v6

    iget-object v7, v5, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 2240846
    iput-object v7, v6, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 2240847
    move-object v6, v6

    .line 2240848
    iget-object v7, v5, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 2240849
    iput-object v7, v6, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 2240850
    move-object v6, v6

    .line 2240851
    sget-object v7, LX/71C;->SIMPLE_SHIPPING_ADDRESS:LX/71C;

    .line 2240852
    iput-object v7, v6, LX/71A;->b:LX/71C;

    .line 2240853
    move-object v6, v6

    .line 2240854
    iget-object v5, v5, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 2240855
    iput-object v5, v6, LX/71A;->c:LX/6xg;

    .line 2240856
    move-object v5, v6

    .line 2240857
    iget-object v6, p0, LX/FRm;->a:Landroid/content/Context;

    const v7, 0x7f081e66

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2240858
    iput-object v6, v5, LX/71A;->d:Ljava/lang/String;

    .line 2240859
    move-object v5, v5

    .line 2240860
    invoke-virtual {v5}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v5

    .line 2240861
    invoke-static {}, Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;->newBuilder()LX/722;

    move-result-object v6

    .line 2240862
    iput-object v5, v6, LX/722;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 2240863
    move-object v5, v6

    .line 2240864
    invoke-static {p1}, LX/FRm;->c(Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v6

    .line 2240865
    iput-object v6, v5, LX/722;->b:Lcom/facebook/payments/shipping/model/ShippingParams;

    .line 2240866
    move-object v5, v5

    .line 2240867
    invoke-virtual {v5}, LX/722;->c()Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    move-result-object v5

    .line 2240868
    iget-object v6, p0, LX/FRm;->a:Landroid/content/Context;

    invoke-static {v6, v5}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v5

    move-object v5, v5

    .line 2240869
    iput-object v5, v4, LX/72b;->b:Landroid/content/Intent;

    .line 2240870
    move-object v4, v4

    .line 2240871
    const/16 v5, 0x192

    .line 2240872
    iput v5, v4, LX/72b;->c:I

    .line 2240873
    move-object v4, v4

    .line 2240874
    iput-object v0, v4, LX/72b;->g:Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 2240875
    move-object v4, v4

    .line 2240876
    const-string v5, "%s (%s, %s, %s, %s, %s, %s)"

    invoke-interface {v0, v5}, Lcom/facebook/payments/shipping/model/MailingAddress;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2240877
    iput-object v0, v4, LX/72b;->d:Ljava/lang/String;

    .line 2240878
    move-object v0, v4

    .line 2240879
    iget-object v4, p0, LX/FRm;->a:Landroid/content/Context;

    const v5, 0x7f081e69

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2240880
    iput-object v4, v0, LX/72b;->e:Ljava/lang/String;

    .line 2240881
    move-object v0, v0

    .line 2240882
    const/4 v4, 0x0

    .line 2240883
    iput-boolean v4, v0, LX/72b;->f:Z

    .line 2240884
    move-object v0, v0

    .line 2240885
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->f()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2240886
    iput-object v4, v0, LX/72b;->h:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2240887
    move-object v0, v0

    .line 2240888
    invoke-virtual {v0}, LX/72b;->a()LX/72c;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240889
    :goto_3
    invoke-static {}, LX/FRr;->newBuilder()LX/FRq;

    move-result-object v0

    .line 2240890
    invoke-virtual {p1}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    invoke-virtual {v4}, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v4

    .line 2240891
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v5

    iget-object v6, v4, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->a:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 2240892
    iput-object v6, v5, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 2240893
    move-object v5, v5

    .line 2240894
    iget-object v6, v4, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 2240895
    iput-object v6, v5, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 2240896
    move-object v5, v5

    .line 2240897
    sget-object v6, LX/71C;->CONTACT_INFORMATION:LX/71C;

    .line 2240898
    iput-object v6, v5, LX/71A;->b:LX/71C;

    .line 2240899
    move-object v5, v5

    .line 2240900
    iget-object v4, v4, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    .line 2240901
    iput-object v4, v5, LX/71A;->c:LX/6xg;

    .line 2240902
    move-object v4, v5

    .line 2240903
    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    const v6, 0x7f081e3e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2240904
    iput-object v5, v4, LX/71A;->d:Ljava/lang/String;

    .line 2240905
    move-object v4, v4

    .line 2240906
    new-instance v5, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;-><init>(Z)V

    .line 2240907
    iput-object v5, v4, LX/71A;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 2240908
    move-object v4, v4

    .line 2240909
    invoke-virtual {v4}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v4

    .line 2240910
    invoke-static {}, Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;->newBuilder()LX/6w5;

    move-result-object v5

    .line 2240911
    iput-object v4, v5, LX/6w5;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 2240912
    move-object v4, v5

    .line 2240913
    sget-object v5, LX/6vb;->EMAIL:LX/6vb;

    sget-object v6, LX/6vb;->PHONE_NUMBER:LX/6vb;

    invoke-static {v5, v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v5

    .line 2240914
    iput-object v5, v4, LX/6w5;->b:LX/0Rf;

    .line 2240915
    move-object v4, v4

    .line 2240916
    sget-object v5, LX/71H;->OPENABLE:LX/71H;

    .line 2240917
    iput-object v5, v4, LX/6w5;->c:LX/71H;

    .line 2240918
    move-object v4, v4

    .line 2240919
    invoke-virtual {v4}, LX/6w5;->d()Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v4

    .line 2240920
    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v4

    move-object v4, v4

    .line 2240921
    iput-object v4, v0, LX/FRq;->a:Landroid/content/Intent;

    .line 2240922
    move-object v0, v0

    .line 2240923
    iget-object v4, p0, LX/FRm;->a:Landroid/content/Context;

    const v5, 0x7f081e3e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2240924
    iput-object v4, v0, LX/FRq;->c:Ljava/lang/String;

    .line 2240925
    move-object v0, v0

    .line 2240926
    invoke-virtual {v0}, LX/FRq;->f()LX/FRr;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240927
    goto/16 :goto_1

    .line 2240928
    :pswitch_5
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240929
    check-cast v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    .line 2240930
    new-instance v4, LX/716;

    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    const v6, 0x7f081e11

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240931
    invoke-static {}, LX/FRr;->newBuilder()LX/FRq;

    move-result-object v4

    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    const v6, 0x7f081e12

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2240932
    iput-object v5, v4, LX/FRq;->c:Ljava/lang/String;

    .line 2240933
    move-object v4, v4

    .line 2240934
    iget-object v5, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v5}, Lcom/facebook/payments/currency/CurrencyAmount;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2240935
    iput-object v5, v4, LX/FRq;->e:Ljava/lang/String;

    .line 2240936
    move-object v4, v4

    .line 2240937
    invoke-virtual {v4}, LX/FRq;->f()LX/FRr;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240938
    invoke-static {}, LX/FRr;->newBuilder()LX/FRq;

    move-result-object v4

    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    const v6, 0x7f081e13

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2240939
    iput-object v5, v4, LX/FRq;->c:Ljava/lang/String;

    .line 2240940
    move-object v4, v4

    .line 2240941
    iget v5, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->e:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 2240942
    iput-object v5, v4, LX/FRq;->e:Ljava/lang/String;

    .line 2240943
    move-object v4, v4

    .line 2240944
    invoke-virtual {v4}, LX/FRq;->f()LX/FRr;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240945
    new-instance v4, LX/716;

    iget-object v5, p0, LX/FRm;->a:Landroid/content/Context;

    const v6, 0x7f081e14

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240946
    goto/16 :goto_1

    .line 2240947
    :pswitch_6
    const-string v0, "https://m.facebook.com/help/414383411931263"

    const-string v4, "https://m.facebook.com/help/contact/614010102040957"

    .line 2240948
    new-instance v5, LX/716;

    iget-object v6, p0, LX/FRm;->a:Landroid/content/Context;

    const v7, 0x7f081e0c

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, LX/716;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240949
    invoke-static {}, LX/FRr;->newBuilder()LX/FRq;

    move-result-object v5

    invoke-static {v0}, LX/FRm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2240950
    iput-object v6, v5, LX/FRq;->a:Landroid/content/Intent;

    .line 2240951
    move-object v5, v5

    .line 2240952
    iget-object v6, p0, LX/FRm;->a:Landroid/content/Context;

    const v7, 0x7f081e0d    # 1.8093104E38f

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2240953
    iput-object v6, v5, LX/FRq;->c:Ljava/lang/String;

    .line 2240954
    move-object v5, v5

    .line 2240955
    invoke-virtual {v5}, LX/FRq;->f()LX/FRr;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240956
    invoke-static {}, LX/FRr;->newBuilder()LX/FRq;

    move-result-object v5

    invoke-static {v4}, LX/FRm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2240957
    iput-object v6, v5, LX/FRq;->a:Landroid/content/Intent;

    .line 2240958
    move-object v5, v5

    .line 2240959
    iget-object v6, p0, LX/FRm;->a:Landroid/content/Context;

    const v7, 0x7f081e0e

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2240960
    iput-object v6, v5, LX/FRq;->c:Ljava/lang/String;

    .line 2240961
    move-object v5, v5

    .line 2240962
    invoke-virtual {v5}, LX/FRq;->f()LX/FRr;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240963
    goto/16 :goto_1

    .line 2240964
    :pswitch_7
    new-instance v0, LX/FRk;

    invoke-direct {v0}, LX/FRk;-><init>()V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240965
    goto/16 :goto_1

    .line 2240966
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 2240967
    :cond_2
    iget-object v4, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-interface {v4}, Lcom/facebook/payments/history/model/PaymentTransactions;->a()LX/0Px;

    move-result-object v4

    .line 2240968
    const/4 v5, 0x0

    .line 2240969
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v5

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/payments/history/model/PaymentTransaction;

    .line 2240970
    add-int/lit8 v6, v6, 0x1

    const/4 v8, 0x2

    if-gt v6, v8, :cond_3

    .line 2240971
    new-instance v8, LX/FRD;

    const/4 v9, 0x0

    invoke-direct {v8, v5, v9}, LX/FRD;-><init>(Lcom/facebook/payments/history/model/PaymentTransaction;Landroid/content/Intent;)V

    invoke-virtual {v2, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2240972
    :cond_3
    iget-object v4, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240973
    invoke-interface {v4}, Lcom/facebook/payments/history/model/PaymentTransactions;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    const/4 v6, 0x2

    if-le v5, v6, :cond_4

    .line 2240974
    invoke-static {}, LX/FRr;->newBuilder()LX/FRq;

    move-result-object v5

    .line 2240975
    sget-object v6, LX/6xZ;->PAYMENTS_SETTINGS:LX/6xZ;

    sget-object v7, LX/6xY;->PAYMENTS_SETTINGS:LX/6xY;

    invoke-static {v7}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v7

    invoke-virtual {v7}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v6

    const-string v7, "payment_history"

    .line 2240976
    iput-object v7, v6, LX/718;->c:Ljava/lang/String;

    .line 2240977
    move-object v6, v6

    .line 2240978
    invoke-virtual {v6}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v6

    .line 2240979
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v7

    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->newBuilder()LX/71E;

    move-result-object v8

    invoke-virtual {v8}, LX/71E;->c()Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-result-object v8

    .line 2240980
    iput-object v8, v7, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 2240981
    move-object v7, v7

    .line 2240982
    iput-object v6, v7, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 2240983
    move-object v6, v7

    .line 2240984
    sget-object v7, LX/71C;->PAYMENT_HISTORY:LX/71C;

    .line 2240985
    iput-object v7, v6, LX/71A;->b:LX/71C;

    .line 2240986
    move-object v6, v6

    .line 2240987
    sget-object v7, LX/6xg;->MOR_NONE:LX/6xg;

    .line 2240988
    iput-object v7, v6, LX/71A;->c:LX/6xg;

    .line 2240989
    move-object v6, v6

    .line 2240990
    iget-object v7, p0, LX/FRm;->a:Landroid/content/Context;

    const v8, 0x7f081e02

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2240991
    iput-object v7, v6, LX/71A;->d:Ljava/lang/String;

    .line 2240992
    move-object v6, v6

    .line 2240993
    invoke-virtual {v6}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v6

    .line 2240994
    iget-object v7, p0, LX/FRm;->a:Landroid/content/Context;

    .line 2240995
    new-instance v8, LX/FR7;

    invoke-direct {v8}, LX/FR7;-><init>()V

    move-object v8, v8

    .line 2240996
    iput-object v4, v8, LX/FR7;->b:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240997
    move-object v8, v8

    .line 2240998
    iput-object v6, v8, LX/FR7;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 2240999
    move-object v6, v8

    .line 2241000
    new-instance v8, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;

    invoke-direct {v8, v6}, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;-><init>(LX/FR7;)V

    move-object v6, v8

    .line 2241001
    invoke-static {v7, v6}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v6

    move-object v6, v6

    .line 2241002
    iput-object v6, v5, LX/FRq;->a:Landroid/content/Intent;

    .line 2241003
    move-object v5, v5

    .line 2241004
    const/16 v6, 0x191

    .line 2241005
    iput v6, v5, LX/FRq;->b:I

    .line 2241006
    move-object v5, v5

    .line 2241007
    iget-object v6, p0, LX/FRm;->a:Landroid/content/Context;

    const v7, 0x7f081e03

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2241008
    iput-object v6, v5, LX/FRq;->c:Ljava/lang/String;

    .line 2241009
    move-object v5, v5

    .line 2241010
    invoke-virtual {v5}, LX/FRq;->f()LX/FRr;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241011
    :cond_4
    goto/16 :goto_2

    .line 2241012
    :cond_5
    new-instance v0, LX/71o;

    invoke-static {p1}, LX/FRm;->c(Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)Lcom/facebook/payments/shipping/model/ShippingCommonParams;

    move-result-object v4

    invoke-direct {v0, v4}, LX/71o;-><init>(Lcom/facebook/payments/shipping/model/ShippingParams;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
