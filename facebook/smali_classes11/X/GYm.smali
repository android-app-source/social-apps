.class public final LX/GYm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GYk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GYk",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GYo;


# direct methods
.method public constructor <init>(LX/GYo;)V
    .locals 0

    .prologue
    .line 2365622
    iput-object p1, p0, LX/GYm;->a:LX/GYo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365623
    check-cast p1, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;

    .line 2365624
    iget-object v0, p0, LX/GYm;->a:LX/GYo;

    .line 2365625
    if-eqz p1, :cond_1

    .line 2365626
    iget-object v1, v0, LX/GYo;->c:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 2365627
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2365628
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7ja;

    iput-object v3, v1, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    .line 2365629
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;->l()Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;

    move-result-object v1

    invoke-static {v0, v1}, LX/GYo;->a$redex0(LX/GYo;Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;)V

    .line 2365630
    :cond_1
    iget-object v0, p0, LX/GYm;->a:LX/GYo;

    invoke-static {v0}, LX/GYo;->c(LX/GYo;)V

    .line 2365631
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p2    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365632
    iget-object v0, p0, LX/GYm;->a:LX/GYo;

    invoke-static {v0, p1, p2}, LX/GYo;->a$redex0(LX/GYo;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2365633
    return-void
.end method
