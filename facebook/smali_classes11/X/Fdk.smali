.class public final LX/Fdk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/resources/ui/FbCheckBox;

.field public final synthetic c:Lcom/facebook/search/results/protocol/filters/FilterValue;

.field public final synthetic d:Lcom/facebook/resources/ui/FbRadioButton;

.field public final synthetic e:LX/Fdl;


# direct methods
.method public constructor <init>(LX/Fdl;ZLcom/facebook/resources/ui/FbCheckBox;Lcom/facebook/search/results/protocol/filters/FilterValue;Lcom/facebook/resources/ui/FbRadioButton;)V
    .locals 0

    .prologue
    .line 2263984
    iput-object p1, p0, LX/Fdk;->e:LX/Fdl;

    iput-boolean p2, p0, LX/Fdk;->a:Z

    iput-object p3, p0, LX/Fdk;->b:Lcom/facebook/resources/ui/FbCheckBox;

    iput-object p4, p0, LX/Fdk;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    iput-object p5, p0, LX/Fdk;->d:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, -0x606c5e11

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263985
    iget-boolean v1, p0, LX/Fdk;->a:Z

    if-eqz v1, :cond_1

    .line 2263986
    iget-object v1, p0, LX/Fdk;->b:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbCheckBox;->toggle()V

    .line 2263987
    iget-object v1, p0, LX/Fdk;->e:LX/Fdl;

    iget-object v1, v1, LX/Fdl;->c:Ljava/util/HashSet;

    iget-object v2, p0, LX/Fdk;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2263988
    iget-object v1, p0, LX/Fdk;->e:LX/Fdl;

    iget-object v1, v1, LX/Fdl;->c:Ljava/util/HashSet;

    iget-object v2, p0, LX/Fdk;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2263989
    :goto_0
    const v1, 0x13d0dce9

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2263990
    :cond_0
    iget-object v1, p0, LX/Fdk;->e:LX/Fdl;

    iget-object v1, v1, LX/Fdl;->c:Ljava/util/HashSet;

    iget-object v2, p0, LX/Fdk;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2263991
    :cond_1
    iget-object v1, p0, LX/Fdk;->d:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2263992
    iget-object v1, p0, LX/Fdk;->e:LX/Fdl;

    iget-object v1, v1, LX/Fdl;->c:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 2263993
    iget-object v1, p0, LX/Fdk;->e:LX/Fdl;

    iget-object v1, v1, LX/Fdl;->c:Ljava/util/HashSet;

    iget-object v2, p0, LX/Fdk;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2263994
    iget-object v1, p0, LX/Fdk;->e:LX/Fdl;

    const v2, -0x54acf1c2

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method
