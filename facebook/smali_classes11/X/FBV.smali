.class public final LX/FBV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;)V
    .locals 0

    .prologue
    .line 2209439
    iput-object p1, p0, LX/FBV;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x76fcf704

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2209440
    iget-object v1, p0, LX/FBV;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    const-string v2, "unread_prompt_get_messenger_button"

    .line 2209441
    iget-object v3, v1, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->b:LX/0Zb;

    const-string v5, "click"

    const/4 p1, 0x1

    invoke-interface {v3, v5, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2209442
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2209443
    const-string v5, "diode_qp_module"

    invoke-virtual {v3, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2209444
    const-string v5, "button"

    invoke-virtual {v3, v5}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2209445
    invoke-virtual {v3, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2209446
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2209447
    :cond_0
    iget-object v1, p0, LX/FBV;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget-object v1, v1, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->a:LX/17T;

    iget-object v2, p0, LX/FBV;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, LX/2g8;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2209448
    const v1, -0x109bd7af

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
