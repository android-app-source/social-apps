.class public LX/GVg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;


# direct methods
.method public constructor <init>(LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2359731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2359732
    iput-object p1, p0, LX/GVg;->a:LX/0dC;

    .line 2359733
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2359734
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2359735
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/GVg;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2359736
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "requestCaptcha"

    .line 2359737
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2359738
    move-object v1, v1

    .line 2359739
    const-string v2, "POST"

    .line 2359740
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2359741
    move-object v1, v1

    .line 2359742
    const-string v2, "captcha/challenges"

    .line 2359743
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2359744
    move-object v1, v1

    .line 2359745
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2359746
    move-object v0, v1

    .line 2359747
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2359748
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2359749
    move-object v0, v0

    .line 2359750
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2359751
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2359752
    const-string v1, "persist_data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 2359753
    const-string v2, "url"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    .line 2359754
    new-instance v2, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;

    invoke-direct {v2, v1, v0}, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method
