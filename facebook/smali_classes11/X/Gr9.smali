.class public LX/Gr9;
.super LX/Ci8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:Landroid/view/Display;

.field public final b:Landroid/graphics/Point;

.field private final c:LX/Chv;

.field public d:Landroid/widget/ImageView;

.field private final e:Landroid/content/Context;

.field public final f:I

.field public final g:LX/68u;

.field public h:I

.field private i:Z

.field public j:LX/1P1;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Chv;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2397565
    invoke-direct {p0}, LX/Ci8;-><init>()V

    .line 2397566
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2397567
    iput-object p2, p0, LX/Gr9;->c:LX/Chv;

    .line 2397568
    iget-object v1, p0, LX/Gr9;->c:LX/Chv;

    invoke-virtual {v1, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 2397569
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, LX/Gr9;->a:Landroid/view/Display;

    .line 2397570
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/Gr9;->b:Landroid/graphics/Point;

    .line 2397571
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, LX/68u;->a(FF)LX/68u;

    move-result-object v0

    iput-object v0, p0, LX/Gr9;->g:LX/68u;

    .line 2397572
    iput-object p1, p0, LX/Gr9;->e:Landroid/content/Context;

    .line 2397573
    const/4 v0, 0x1

    sget v1, LX/CoL;->p:I

    int-to-float v1, v1

    iget-object v2, p0, LX/Gr9;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, LX/Gr9;->f:I

    .line 2397574
    return-void
.end method

.method public static a(LX/0QB;)LX/Gr9;
    .locals 5

    .prologue
    .line 2397554
    const-class v1, LX/Gr9;

    monitor-enter v1

    .line 2397555
    :try_start_0
    sget-object v0, LX/Gr9;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2397556
    sput-object v2, LX/Gr9;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2397557
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2397558
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2397559
    new-instance p0, LX/Gr9;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    invoke-direct {p0, v3, v4}, LX/Gr9;-><init>(Landroid/content/Context;LX/Chv;)V

    .line 2397560
    move-object v0, p0

    .line 2397561
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2397562
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gr9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2397563
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2397564
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/Gr9;F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2397547
    iget-object v0, p0, LX/Gr9;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 2397548
    iget-object v0, p0, LX/Gr9;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestLayout()V

    .line 2397549
    iget-object v0, p0, LX/Gr9;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Gr9;->g:LX/68u;

    invoke-virtual {v0}, LX/68u;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2397550
    iget-object v0, p0, LX/Gr9;->g:LX/68u;

    invoke-virtual {v0}, LX/68u;->g()V

    .line 2397551
    :cond_0
    :goto_0
    return-void

    .line 2397552
    :cond_1
    iget-object v0, p0, LX/Gr9;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, LX/Gr9;->g:LX/68u;

    invoke-virtual {v0}, LX/68u;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2397553
    iget-object v0, p0, LX/Gr9;->g:LX/68u;

    invoke-virtual {v0}, LX/68u;->f()V

    goto :goto_0
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 2397529
    check-cast p1, LX/Cic;

    .line 2397530
    iget v0, p0, LX/Gr9;->h:I

    .line 2397531
    iget v1, p1, LX/Cic;->b:I

    move v1, v1

    .line 2397532
    add-int/2addr v0, v1

    iput v0, p0, LX/Gr9;->h:I

    .line 2397533
    iget-object v0, p0, LX/Gr9;->b:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v1

    .line 2397534
    iget v1, p0, LX/Gr9;->h:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 2397535
    sub-float v1, v0, v1

    div-float v0, v1, v0

    .line 2397536
    invoke-static {p0, v0}, LX/Gr9;->a(LX/Gr9;F)V

    .line 2397537
    iget-boolean v0, p0, LX/Gr9;->i:Z

    if-eqz v0, :cond_0

    .line 2397538
    iget-object v0, p0, LX/Gr9;->j:LX/1P1;

    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/Gr9;->j:LX/1P1;

    invoke-virtual {v1}, LX/1OR;->D()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2397539
    iget-object v0, p0, LX/Gr9;->j:LX/1P1;

    iget-object v1, p0, LX/Gr9;->j:LX/1P1;

    invoke-virtual {v1}, LX/1OR;->D()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2397540
    if-nez v0, :cond_1

    .line 2397541
    :cond_0
    :goto_0
    return-void

    .line 2397542
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v1, p0, LX/Gr9;->j:LX/1P1;

    invoke-virtual {v1}, LX/1OR;->x()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2397543
    iget-object v1, p0, LX/Gr9;->b:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    const p1, 0x3dcccccd    # 0.1f

    mul-float/2addr v1, p1

    .line 2397544
    int-to-float v0, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2397545
    sub-float v0, v1, v0

    div-float/2addr v0, v1

    .line 2397546
    invoke-static {p0, v0}, LX/Gr9;->a(LX/Gr9;F)V

    goto :goto_0
.end method
