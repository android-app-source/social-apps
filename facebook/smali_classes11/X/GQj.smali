.class public final LX/GQj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/aldrin/status/AldrinUserStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;)V
    .locals 0

    .prologue
    .line 2350374
    iput-object p1, p0, LX/GQj;->a:Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2350375
    iget-object v0, p0, LX/GQj;->a:Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;

    iget-object v0, v0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->f:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 2350376
    iget-object v0, p0, LX/GQj;->a:Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;

    iget-object v0, v0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083451

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2350377
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2350378
    iget-object v0, p0, LX/GQj;->a:Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2350379
    iget-object v0, p0, LX/GQj;->a:Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;

    iget-object v0, v0, Lcom/facebook/aldrin/transition/fragment/GeneralTosFragment;->c:LX/GQh;

    sget-object v1, LX/GQf;->AGREED_TO_GENERAL_TOS:LX/GQf;

    invoke-virtual {v0, v1}, LX/GQh;->a(LX/GQf;)V

    .line 2350380
    return-void
.end method
