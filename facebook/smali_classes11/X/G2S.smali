.class public LX/G2S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/2do;

.field public final c:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/2do;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314024
    iput-object p1, p0, LX/G2S;->a:Landroid/content/res/Resources;

    .line 2314025
    iput-object p2, p0, LX/G2S;->b:LX/2do;

    .line 2314026
    iput-object p3, p0, LX/G2S;->c:LX/0SG;

    .line 2314027
    return-void
.end method

.method public static a(LX/0QB;)LX/G2S;
    .locals 6

    .prologue
    .line 2314028
    const-class v1, LX/G2S;

    monitor-enter v1

    .line 2314029
    :try_start_0
    sget-object v0, LX/G2S;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314030
    sput-object v2, LX/G2S;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314031
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314032
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314033
    new-instance p0, LX/G2S;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v4

    check-cast v4, LX/2do;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/G2S;-><init>(Landroid/content/res/Resources;LX/2do;LX/0SG;)V

    .line 2314034
    move-object v0, p0

    .line 2314035
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314036
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G2S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314037
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314038
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/G2S;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V
    .locals 3

    .prologue
    .line 2314039
    iget-object v0, p0, LX/G2S;->b:LX/2do;

    new-instance v1, LX/2f2;

    invoke-direct {v1, p1, p2, p3, p4}, LX/2f2;-><init>(JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2314040
    return-void
.end method

.method public static b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)I
    .locals 2

    .prologue
    .line 2314041
    sget-object v0, LX/G2R;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2314042
    const/16 v0, 0x8

    :goto_0
    return v0

    .line 2314043
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2314044
    sget-object v0, LX/G2R;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2314045
    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    .line 2314046
    :pswitch_0
    iget-object v0, p0, LX/G2S;->a:Landroid/content/res/Resources;

    const v1, 0x7f020b69

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2314047
    :pswitch_1
    iget-object v0, p0, LX/G2S;->a:Landroid/content/res/Resources;

    const v1, 0x7f020b5a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2314048
    :pswitch_2
    iget-object v0, p0, LX/G2S;->a:Landroid/content/res/Resources;

    const v1, 0x7f020b6e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2314049
    if-eqz v0, :cond_0

    .line 2314050
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    iget-object v2, p0, LX/G2S;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/timeline/protiles/model/ProtileModel;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;LX/1Pc;)Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 2314051
    invoke-virtual {p2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    .line 2314052
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2314053
    :cond_0
    const/4 v0, 0x0

    .line 2314054
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/G2Q;

    invoke-direct {v0, p0, v1, p3, p1}, LX/G2Q;-><init>(LX/G2S;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;LX/1Pc;Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2314055
    sget-object v0, LX/G2R;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2314056
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2314057
    :pswitch_0
    iget-object v0, p0, LX/G2S;->a:Landroid/content/res/Resources;

    const v1, 0x7f081fed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2314058
    :pswitch_1
    iget-object v0, p0, LX/G2S;->a:Landroid/content/res/Resources;

    const v1, 0x7f081feb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2314059
    :pswitch_2
    iget-object v0, p0, LX/G2S;->a:Landroid/content/res/Resources;

    const v1, 0x7f081fec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
