.class public final LX/Gew;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Gex;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/1f9;

.field public c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

.field public d:LX/25E;

.field public e:Z

.field public f:Z

.field public g:LX/2dx;

.field public final synthetic h:LX/Gex;


# direct methods
.method public constructor <init>(LX/Gex;)V
    .locals 1

    .prologue
    .line 2376291
    iput-object p1, p0, LX/Gew;->h:LX/Gex;

    .line 2376292
    move-object v0, p1

    .line 2376293
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2376294
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gew;->f:Z

    .line 2376295
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2376296
    const-string v0, "PageYouMayLikeCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2376297
    if-ne p0, p1, :cond_1

    .line 2376298
    :cond_0
    :goto_0
    return v0

    .line 2376299
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2376300
    goto :goto_0

    .line 2376301
    :cond_3
    check-cast p1, LX/Gew;

    .line 2376302
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2376303
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2376304
    if-eq v2, v3, :cond_0

    .line 2376305
    iget-object v2, p0, LX/Gew;->a:LX/1Pp;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Gew;->a:LX/1Pp;

    iget-object v3, p1, LX/Gew;->a:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2376306
    goto :goto_0

    .line 2376307
    :cond_5
    iget-object v2, p1, LX/Gew;->a:LX/1Pp;

    if-nez v2, :cond_4

    .line 2376308
    :cond_6
    iget-object v2, p0, LX/Gew;->b:LX/1f9;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Gew;->b:LX/1f9;

    iget-object v3, p1, LX/Gew;->b:LX/1f9;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2376309
    goto :goto_0

    .line 2376310
    :cond_8
    iget-object v2, p1, LX/Gew;->b:LX/1f9;

    if-nez v2, :cond_7

    .line 2376311
    :cond_9
    iget-object v2, p0, LX/Gew;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Gew;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v3, p1, LX/Gew;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2376312
    goto :goto_0

    .line 2376313
    :cond_b
    iget-object v2, p1, LX/Gew;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-nez v2, :cond_a

    .line 2376314
    :cond_c
    iget-object v2, p0, LX/Gew;->d:LX/25E;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Gew;->d:LX/25E;

    iget-object v3, p1, LX/Gew;->d:LX/25E;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2376315
    goto :goto_0

    .line 2376316
    :cond_e
    iget-object v2, p1, LX/Gew;->d:LX/25E;

    if-nez v2, :cond_d

    .line 2376317
    :cond_f
    iget-boolean v2, p0, LX/Gew;->e:Z

    iget-boolean v3, p1, LX/Gew;->e:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2376318
    goto :goto_0

    .line 2376319
    :cond_10
    iget-boolean v2, p0, LX/Gew;->f:Z

    iget-boolean v3, p1, LX/Gew;->f:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 2376320
    goto :goto_0

    .line 2376321
    :cond_11
    iget-object v2, p0, LX/Gew;->g:LX/2dx;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/Gew;->g:LX/2dx;

    iget-object v3, p1, LX/Gew;->g:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2376322
    goto/16 :goto_0

    .line 2376323
    :cond_12
    iget-object v2, p1, LX/Gew;->g:LX/2dx;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
