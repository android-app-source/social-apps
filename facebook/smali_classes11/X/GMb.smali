.class public final LX/GMb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/GJJ;


# direct methods
.method public constructor <init>(LX/GJJ;)V
    .locals 0

    .prologue
    .line 2345041
    iput-object p1, p0, LX/GMb;->a:LX/GJJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 2345042
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/GMb;->a:LX/GJJ;

    iget-object v1, v1, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080b59

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/GMb;->a:LX/GJJ;

    .line 2345043
    iget-object v6, v5, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v6, v6

    .line 2345044
    invoke-static {v6}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v6

    .line 2345045
    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v7

    invoke-static {v6}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v8

    .line 2345046
    iget-object v6, v5, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v6, v6

    .line 2345047
    invoke-static {v6}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v6

    invoke-static {v7, v8, v9, v6}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 2345048
    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
