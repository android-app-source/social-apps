.class public final LX/Gvr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gvy;


# direct methods
.method public constructor <init>(LX/Gvy;)V
    .locals 0

    .prologue
    .line 2406137
    iput-object p1, p0, LX/Gvr;->a:LX/Gvy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2406138
    iget-object v0, p0, LX/Gvr;->a:LX/Gvy;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Gvy;->a(Ljava/lang/String;)V

    .line 2406139
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2406140
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    const/4 v0, 0x0

    .line 2406141
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2406142
    iget-object v1, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v1, v1, LX/Gvy;->i:LX/BM1;

    const-string v2, "on_return_to_main_app"

    iget-object v3, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v3, v3, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406143
    iget-object v4, v3, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2406144
    invoke-virtual {v1, v2, v3, v0}, LX/BM1;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2406145
    new-instance v1, Lcom/facebook/share/model/ComposerAppAttribution;

    iget-object v2, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v2, v2, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406146
    iget-object v3, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2406147
    iget-object v3, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v3, v3, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406148
    iget-object v4, v3, Lcom/facebook/platform/common/action/PlatformAppCall;->f:Ljava/lang/String;

    move-object v3, v4

    .line 2406149
    iget-object v4, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v4, v4, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406150
    iget-object v5, v4, Lcom/facebook/platform/common/action/PlatformAppCall;->g:Ljava/lang/String;

    move-object v4, v5

    .line 2406151
    iget-object v5, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v5, v5, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406152
    iget-object v6, v5, Lcom/facebook/platform/common/action/PlatformAppCall;->h:Ljava/lang/String;

    move-object v5, v6

    .line 2406153
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/share/model/ComposerAppAttribution;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAppAttribution(Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406154
    iget-object v1, p0, LX/Gvr;->a:LX/Gvy;

    .line 2406155
    iget-object v2, v1, LX/Gvy;->e:Landroid/app/Activity;

    move-object v1, v2

    .line 2406156
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.facebook.platform.protocol.METHOD_ARGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2406157
    if-eqz v1, :cond_0

    .line 2406158
    const-string v0, "com.facebook.platform.target.DATA"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2406159
    :cond_0
    if-eqz v0, :cond_1

    .line 2406160
    invoke-virtual {p1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406161
    :cond_1
    iget-object v0, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v0, v0, LX/Gvy;->m:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v1, v1, LX/Gvy;->e:Landroid/app/Activity;

    iget-object v2, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v2, v2, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406162
    iget-object v3, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->j:Ljava/lang/String;

    move-object v2, v3

    .line 2406163
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/platform/composer/composer/PlatformComposerActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Z)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, LX/Gvr;->a:LX/Gvy;

    iget v2, v2, LX/Gvy;->l:I

    iget-object v3, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v3, v3, LX/Gvy;->e:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2406164
    iget-object v0, p0, LX/Gvr;->a:LX/Gvy;

    iget-object v0, v0, LX/Gvy;->c:LX/0Zb;

    iget-object v1, p0, LX/Gvr;->a:LX/Gvy;

    const-string v2, "platform_share_show_dialog"

    invoke-virtual {v1, v2}, LX/Gvy;->b(Ljava/lang/String;)LX/8Or;

    move-result-object v1

    invoke-virtual {v1}, LX/8Or;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2406165
    return-void
.end method
