.class public LX/H8o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/shortcuts/InstallShortcutHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2LS;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433672
    const v0, 0x7f020971

    sput v0, LX/H8o;->a:I

    .line 2433673
    const v0, 0x7f081658

    sput v0, LX/H8o;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)V
    .locals 0
    .param p6    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/shortcuts/InstallShortcutHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2LS;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/os/Vibrator;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433675
    iput-object p1, p0, LX/H8o;->c:LX/0Ot;

    .line 2433676
    iput-object p2, p0, LX/H8o;->d:LX/0Ot;

    .line 2433677
    iput-object p3, p0, LX/H8o;->e:LX/0Ot;

    .line 2433678
    iput-object p4, p0, LX/H8o;->f:LX/0Ot;

    .line 2433679
    iput-object p5, p0, LX/H8o;->g:LX/0Ot;

    .line 2433680
    iput-object p6, p0, LX/H8o;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433681
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    .line 2433682
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8o;->b:I

    sget v3, LX/H8o;->a:I

    const/4 v4, 0x1

    .line 2433683
    iget-object v5, p0, LX/H8o;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/8A4;->a(LX/0Px;)Z

    move-result v5

    move v5, v5

    .line 2433684
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433685
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8o;->b:I

    sget v3, LX/H8o;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2433686
    iget-object v0, p0, LX/H8o;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XI;->EVENT_TAPPED_CREATE_SHORTCUT:LX/9XI;

    iget-object v2, p0, LX/H8o;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433687
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    iget-object v1, p0, LX/H8o;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2433688
    const/4 v0, 0x0

    .line 2433689
    iget-object v1, p0, LX/H8o;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->x()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2433690
    iget-object v0, p0, LX/H8o;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->x()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 2433691
    :goto_0
    iget-object v0, p0, LX/H8o;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    iget-object v3, p0, LX/H8o;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/46b;->ROUNDED:LX/46b;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/46b;)V

    .line 2433692
    iget-object v0, p0, LX/H8o;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2LS;

    .line 2433693
    invoke-static {v0}, LX/2LS;->i(LX/2LS;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.app.twlauncher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 2433694
    if-nez v1, :cond_0

    .line 2433695
    invoke-static {v0}, LX/2LS;->i(LX/2LS;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.app.launcher"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 2433696
    if-eqz v0, :cond_1

    .line 2433697
    :cond_0
    :goto_1
    return-void

    .line 2433698
    :cond_1
    iget-object v0, p0, LX/H8o;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 2433699
    iget-object v0, p0, LX/H8o;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08065f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433700
    const/4 v0, 0x0

    return-object v0
.end method
