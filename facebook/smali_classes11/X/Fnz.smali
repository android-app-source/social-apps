.class public final LX/Fnz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 31

    .prologue
    .line 2288075
    const/16 v27, 0x0

    .line 2288076
    const/16 v26, 0x0

    .line 2288077
    const/16 v25, 0x0

    .line 2288078
    const/16 v24, 0x0

    .line 2288079
    const/16 v23, 0x0

    .line 2288080
    const/16 v22, 0x0

    .line 2288081
    const/16 v21, 0x0

    .line 2288082
    const/16 v20, 0x0

    .line 2288083
    const/16 v19, 0x0

    .line 2288084
    const/16 v18, 0x0

    .line 2288085
    const/16 v17, 0x0

    .line 2288086
    const/16 v16, 0x0

    .line 2288087
    const/4 v15, 0x0

    .line 2288088
    const/4 v14, 0x0

    .line 2288089
    const/4 v13, 0x0

    .line 2288090
    const/4 v12, 0x0

    .line 2288091
    const/4 v11, 0x0

    .line 2288092
    const/4 v10, 0x0

    .line 2288093
    const/4 v9, 0x0

    .line 2288094
    const/4 v8, 0x0

    .line 2288095
    const/4 v7, 0x0

    .line 2288096
    const/4 v6, 0x0

    .line 2288097
    const/4 v5, 0x0

    .line 2288098
    const/4 v4, 0x0

    .line 2288099
    const/4 v3, 0x0

    .line 2288100
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    .line 2288101
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2288102
    const/4 v3, 0x0

    .line 2288103
    :goto_0
    return v3

    .line 2288104
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2288105
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_15

    .line 2288106
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v28

    .line 2288107
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2288108
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1

    if-eqz v28, :cond_1

    .line 2288109
    const-string v29, "__type__"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_2

    const-string v29, "__typename"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_3

    .line 2288110
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v27

    goto :goto_1

    .line 2288111
    :cond_3
    const-string v29, "campaign_title"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_4

    .line 2288112
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    goto :goto_1

    .line 2288113
    :cond_4
    const-string v29, "can_invite_to_campaign"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_5

    .line 2288114
    const/4 v8, 0x1

    .line 2288115
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v25

    goto :goto_1

    .line 2288116
    :cond_5
    const-string v29, "can_viewer_delete"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 2288117
    const/4 v7, 0x1

    .line 2288118
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v24

    goto :goto_1

    .line 2288119
    :cond_6
    const-string v29, "can_viewer_edit"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_7

    .line 2288120
    const/4 v6, 0x1

    .line 2288121
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v23

    goto :goto_1

    .line 2288122
    :cond_7
    const-string v29, "can_viewer_post"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_8

    .line 2288123
    const/4 v5, 0x1

    .line 2288124
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 2288125
    :cond_8
    const-string v29, "can_viewer_report"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_9

    .line 2288126
    const/4 v4, 0x1

    .line 2288127
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto/16 :goto_1

    .line 2288128
    :cond_9
    const-string v29, "charity_interface"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_a

    .line 2288129
    invoke-static/range {p0 .. p1}, LX/Fnv;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 2288130
    :cond_a
    const-string v29, "focused_cover_photo"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_b

    .line 2288131
    invoke-static/range {p0 .. p1}, LX/Fnx;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 2288132
    :cond_b
    const-string v29, "fundraiser_page"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_c

    .line 2288133
    invoke-static/range {p0 .. p1}, LX/Fns;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 2288134
    :cond_c
    const-string v29, "fundraiser_page_subtitle"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_d

    .line 2288135
    invoke-static/range {p0 .. p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 2288136
    :cond_d
    const-string v29, "id"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_e

    .line 2288137
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 2288138
    :cond_e
    const-string v29, "invited_you_to_donate_text"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_f

    .line 2288139
    invoke-static/range {p0 .. p1}, LX/Fnt;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 2288140
    :cond_f
    const-string v29, "is_viewer_following"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_10

    .line 2288141
    const/4 v3, 0x1

    .line 2288142
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 2288143
    :cond_10
    const-string v29, "logo_image"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_11

    .line 2288144
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 2288145
    :cond_11
    const-string v29, "mobile_donate_url"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_12

    .line 2288146
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 2288147
    :cond_12
    const-string v29, "owner"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_13

    .line 2288148
    invoke-static/range {p0 .. p1}, LX/Fnq;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 2288149
    :cond_13
    const-string v29, "privacy_scope"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_14

    .line 2288150
    invoke-static/range {p0 .. p1}, LX/5R2;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 2288151
    :cond_14
    const-string v29, "url"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    .line 2288152
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 2288153
    :cond_15
    const/16 v28, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2288154
    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2288155
    const/16 v27, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2288156
    if-eqz v8, :cond_16

    .line 2288157
    const/4 v8, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 2288158
    :cond_16
    if-eqz v7, :cond_17

    .line 2288159
    const/4 v7, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 2288160
    :cond_17
    if-eqz v6, :cond_18

    .line 2288161
    const/4 v6, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 2288162
    :cond_18
    if-eqz v5, :cond_19

    .line 2288163
    const/4 v5, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, LX/186;->a(IZ)V

    .line 2288164
    :cond_19
    if-eqz v4, :cond_1a

    .line 2288165
    const/4 v4, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 2288166
    :cond_1a
    const/4 v4, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2288167
    const/16 v4, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2288168
    const/16 v4, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2288169
    const/16 v4, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2288170
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2288171
    const/16 v4, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 2288172
    if-eqz v3, :cond_1b

    .line 2288173
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->a(IZ)V

    .line 2288174
    :cond_1b
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 2288175
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 2288176
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 2288177
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2288178
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2288179
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
