.class public final enum LX/Fr9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fr9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fr9;

.field public static final enum CONTEXTUAL_ITEM_BOTTOM:LX/Fr9;

.field public static final enum CONTEXTUAL_ITEM_MIDDLE:LX/Fr9;

.field public static final enum CONTEXTUAL_ITEM_TOP:LX/Fr9;

.field public static final enum INTRO_CARD_CONTEXTUAL_VIEW:LX/Fr9;

.field public static final enum LOAD_MORE_INDICATOR:LX/Fr9;

.field public static final enum UNKNOWN:LX/Fr9;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2295030
    new-instance v0, LX/Fr9;

    const-string v1, "CONTEXTUAL_ITEM_TOP"

    invoke-direct {v0, v1, v3}, LX/Fr9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fr9;->CONTEXTUAL_ITEM_TOP:LX/Fr9;

    .line 2295031
    new-instance v0, LX/Fr9;

    const-string v1, "CONTEXTUAL_ITEM_MIDDLE"

    invoke-direct {v0, v1, v4}, LX/Fr9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fr9;->CONTEXTUAL_ITEM_MIDDLE:LX/Fr9;

    .line 2295032
    new-instance v0, LX/Fr9;

    const-string v1, "CONTEXTUAL_ITEM_BOTTOM"

    invoke-direct {v0, v1, v5}, LX/Fr9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fr9;->CONTEXTUAL_ITEM_BOTTOM:LX/Fr9;

    .line 2295033
    new-instance v0, LX/Fr9;

    const-string v1, "INTRO_CARD_CONTEXTUAL_VIEW"

    invoke-direct {v0, v1, v6}, LX/Fr9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fr9;->INTRO_CARD_CONTEXTUAL_VIEW:LX/Fr9;

    .line 2295034
    new-instance v0, LX/Fr9;

    const-string v1, "LOAD_MORE_INDICATOR"

    invoke-direct {v0, v1, v7}, LX/Fr9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fr9;->LOAD_MORE_INDICATOR:LX/Fr9;

    .line 2295035
    new-instance v0, LX/Fr9;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Fr9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fr9;->UNKNOWN:LX/Fr9;

    .line 2295036
    const/4 v0, 0x6

    new-array v0, v0, [LX/Fr9;

    sget-object v1, LX/Fr9;->CONTEXTUAL_ITEM_TOP:LX/Fr9;

    aput-object v1, v0, v3

    sget-object v1, LX/Fr9;->CONTEXTUAL_ITEM_MIDDLE:LX/Fr9;

    aput-object v1, v0, v4

    sget-object v1, LX/Fr9;->CONTEXTUAL_ITEM_BOTTOM:LX/Fr9;

    aput-object v1, v0, v5

    sget-object v1, LX/Fr9;->INTRO_CARD_CONTEXTUAL_VIEW:LX/Fr9;

    aput-object v1, v0, v6

    sget-object v1, LX/Fr9;->LOAD_MORE_INDICATOR:LX/Fr9;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Fr9;->UNKNOWN:LX/Fr9;

    aput-object v2, v0, v1

    sput-object v0, LX/Fr9;->$VALUES:[LX/Fr9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2295037
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fr9;
    .locals 1

    .prologue
    .line 2295038
    const-class v0, LX/Fr9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fr9;

    return-object v0
.end method

.method public static values()[LX/Fr9;
    .locals 1

    .prologue
    .line 2295039
    sget-object v0, LX/Fr9;->$VALUES:[LX/Fr9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fr9;

    return-object v0
.end method
