.class public final LX/F18;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

.field public final synthetic c:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

.field public final synthetic d:LX/1Po;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;LX/1Po;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2191176
    iput-object p1, p0, LX/F18;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    iput-object p2, p0, LX/F18;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/F18;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    iput-object p4, p0, LX/F18;->c:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iput-object p5, p0, LX/F18;->d:LX/1Po;

    iput-object p6, p0, LX/F18;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x1198c01d

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2191177
    iget-object v0, p0, LX/F18;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191178
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 2191179
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->m()Ljava/lang/String;

    move-result-object v0

    .line 2191180
    sget-object v2, LX/F0r;->COLLAGE_V1:LX/F0r;

    invoke-virtual {v2}, LX/F0r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2191181
    const-string v2, "friendversary_card_collage_ipb"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2191182
    :goto_0
    move-object v2, v2

    .line 2191183
    iget-object v0, p0, LX/F18;->f:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryCampaignFooterPartDefinition;->j:LX/1Cn;

    iget-object v3, p0, LX/F18;->b:Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/CEz;->PERMALINK:LX/CEz;

    invoke-virtual {v0, v3, v2, v4}, LX/1Cn;->b(Ljava/lang/String;Ljava/lang/String;LX/CEz;)V

    move-object v0, p1

    .line 2191184
    check-cast v0, LX/3ZK;

    .line 2191185
    iget-object v3, v0, LX/3ZK;->d:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-object v0, v3

    .line 2191186
    new-instance v3, LX/6WS;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2191187
    const v4, 0x7f0314b7

    invoke-virtual {v3, v4}, LX/5OM;->b(I)V

    .line 2191188
    new-instance v4, LX/F17;

    invoke-direct {v4, p0, p1, v2}, LX/F17;-><init>(LX/F18;Landroid/view/View;Ljava/lang/String;)V

    .line 2191189
    iput-object v4, v3, LX/5OM;->p:LX/5OO;

    .line 2191190
    invoke-virtual {v3, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2191191
    const v0, -0x227b85c5

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2191192
    :cond_0
    sget-object v2, LX/F0r;->POLAROID_V1:LX/F0r;

    invoke-virtual {v2}, LX/F0r;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2191193
    const-string v2, "friendversary_polaroids_ipb"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 2191194
    :cond_1
    const-string v2, ""

    goto :goto_0
.end method
