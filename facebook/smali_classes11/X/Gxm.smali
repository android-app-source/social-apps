.class public final LX/Gxm;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/LayoutInflater;

.field public final synthetic b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 2408465
    iput-object p1, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iput-object p2, p0, LX/Gxm;->a:Landroid/view/LayoutInflater;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2408435
    iget-object v1, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    monitor-enter v1

    .line 2408436
    :try_start_0
    iget-object v0, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    const/4 v2, 0x1

    .line 2408437
    iput-boolean v2, v0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->r:Z

    .line 2408438
    iget-object v0, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    invoke-static {v0}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->d(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V

    .line 2408439
    iget-object v0, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->b:LX/Gxv;

    .line 2408440
    iget-object v2, v0, LX/Gxv;->a:LX/0Zb;

    sget-object p0, LX/Gxu;->SUGGESTIONS_FAILED:LX/Gxu;

    invoke-static {p0}, LX/Gxv;->a(LX/Gxu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v2, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408441
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2408442
    check-cast p1, LX/0Px;

    const/4 v0, 0x0

    .line 2408443
    iget-object v3, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    monitor-enter v3

    .line 2408444
    :try_start_0
    iget-object v1, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    const/4 v2, 0x1

    .line 2408445
    iput-boolean v2, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->r:Z

    .line 2408446
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2408447
    :cond_0
    iget-object v1, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->b:LX/Gxv;

    .line 2408448
    iget-object v2, v1, LX/Gxv;->a:LX/0Zb;

    sget-object v4, LX/Gxu;->NO_SUGGESTIONS:LX/Gxu;

    invoke-static {v4}, LX/Gxv;->a(LX/Gxu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408449
    :goto_0
    if-nez p1, :cond_2

    .line 2408450
    iget-object v0, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    invoke-static {v0}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->d(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V

    .line 2408451
    monitor-exit v3

    .line 2408452
    :goto_1
    return-void

    .line 2408453
    :cond_1
    iget-object v1, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->b:LX/Gxv;

    .line 2408454
    iget-object v2, v1, LX/Gxv;->a:LX/0Zb;

    sget-object v4, LX/Gxu;->SUGGESTIONS_FETCHED:LX/Gxu;

    invoke-static {v4}, LX/Gxv;->a(LX/Gxu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408455
    goto :goto_0

    .line 2408456
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2408457
    :cond_2
    :try_start_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_2
    if-ge v2, v4, :cond_3

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2408458
    iget-object v5, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v5, v5, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x4

    if-ge v5, v6, :cond_3

    .line 2408459
    iget-object v5, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v5, v5, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2408460
    iget-object v5, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v6, p0, LX/Gxm;->a:Landroid/view/LayoutInflater;

    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v5, v6, v0, v1}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->a$redex0(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/view/LayoutInflater;Ljava/util/Locale;I)Landroid/widget/RadioButton;

    .line 2408461
    add-int/lit8 v0, v1, 0x7d

    .line 2408462
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 2408463
    :cond_3
    iget-object v0, p0, LX/Gxm;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    invoke-static {v0}, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->d(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V

    .line 2408464
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3
.end method
