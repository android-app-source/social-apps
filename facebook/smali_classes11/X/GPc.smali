.class public LX/GPc;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/adspayments/analytics/PaymentsFlowContext;",
        "Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/GPc;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349165
    const-class v0, Lcom/facebook/adspayments/protocol/GetAccountDetailsResult;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2349166
    return-void
.end method

.method public static a(LX/0QB;)LX/GPc;
    .locals 4

    .prologue
    .line 2349167
    sget-object v0, LX/GPc;->c:LX/GPc;

    if-nez v0, :cond_1

    .line 2349168
    const-class v1, LX/GPc;

    monitor-enter v1

    .line 2349169
    :try_start_0
    sget-object v0, LX/GPc;->c:LX/GPc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2349170
    if-eqz v2, :cond_0

    .line 2349171
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2349172
    new-instance p0, LX/GPc;

    invoke-static {v0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {p0, v3}, LX/GPc;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349173
    move-object v0, p0

    .line 2349174
    sput-object v0, LX/GPc;->c:LX/GPc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2349175
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2349176
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2349177
    :cond_1
    sget-object v0, LX/GPc;->c:LX/GPc;

    return-object v0

    .line 2349178
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2349179
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 2349180
    check-cast p1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2349181
    iget-object v0, p1, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2349182
    const-string v1, "0"

    invoke-static {v1}, LX/0Rj;->equalTo(Ljava/lang/Object;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v1

    invoke-static {v0, v1}, LX/GPq;->a(Ljava/lang/Object;LX/0Rl;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2349183
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "/act_%s"

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2349184
    iput-object v0, v1, LX/14O;->d:Ljava/lang/String;

    .line 2349185
    move-object v0, v1

    .line 2349186
    invoke-virtual {p0}, LX/GPc;->d()Ljava/lang/String;

    move-result-object v1

    .line 2349187
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349188
    move-object v0, v0

    .line 2349189
    const-string v1, "GET"

    .line 2349190
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349191
    move-object v0, v0

    .line 2349192
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2349193
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349194
    move-object v0, v0

    .line 2349195
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349196
    const-string v0, "get_account_details"

    return-object v0
.end method
