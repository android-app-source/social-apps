.class public final LX/GQt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GA2;


# instance fields
.field public final synthetic a:LX/GQv;

.field public final synthetic b:LX/GAU;

.field public final synthetic c:LX/GR3;

.field public final synthetic d:LX/GR1;


# direct methods
.method public constructor <init>(LX/GQv;LX/GAU;LX/GR3;LX/GR1;)V
    .locals 0

    .prologue
    .line 2350870
    iput-object p1, p0, LX/GQt;->a:LX/GQv;

    iput-object p2, p0, LX/GQt;->b:LX/GAU;

    iput-object p3, p0, LX/GQt;->c:LX/GR3;

    iput-object p4, p0, LX/GQt;->d:LX/GR1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/GAY;)V
    .locals 13

    .prologue
    .line 2350871
    iget-object v0, p0, LX/GQt;->a:LX/GQv;

    iget-object v1, p0, LX/GQt;->b:LX/GAU;

    iget-object v2, p0, LX/GQt;->c:LX/GR3;

    iget-object v3, p0, LX/GQt;->d:LX/GR1;

    .line 2350872
    iget-object v4, p1, LX/GAY;->d:LX/GAF;

    move-object v7, v4

    .line 2350873
    const-string v5, "Success"

    .line 2350874
    sget-object v4, LX/GR0;->SUCCESS:LX/GR0;

    .line 2350875
    if-eqz v7, :cond_5

    .line 2350876
    iget v4, v7, LX/GAF;->d:I

    move v4, v4

    .line 2350877
    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 2350878
    const-string v5, "Failed: No Connectivity"

    .line 2350879
    sget-object v4, LX/GR0;->NO_CONNECTIVITY:LX/GR0;

    move-object v6, v5

    move-object v5, v4

    .line 2350880
    :goto_0
    sget-object v4, LX/GAb;->APP_EVENTS:LX/GAb;

    invoke-static {v4}, LX/GAK;->a(LX/GAb;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2350881
    iget-object v4, v1, LX/GAU;->n:Ljava/lang/Object;

    move-object v4, v4

    .line 2350882
    check-cast v4, Ljava/lang/String;

    .line 2350883
    :try_start_0
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 2350884
    const/4 v4, 0x2

    invoke-virtual {v8, v4}, Lorg/json/JSONArray;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 2350885
    :goto_1
    sget-object v8, LX/GAb;->APP_EVENTS:LX/GAb;

    sget-object v9, LX/GR4;->a:Ljava/lang/String;

    const-string v10, "Flush completed\nParams: %s\n  Result: %s\n  Events JSON: %s"

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    .line 2350886
    iget-object p0, v1, LX/GAU;->g:Lorg/json/JSONObject;

    move-object p0, p0

    .line 2350887
    invoke-virtual {p0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v11, v12

    const/4 v12, 0x1

    aput-object v6, v11, v12

    const/4 v6, 0x2

    aput-object v4, v11, v6

    invoke-static {v8, v9, v10, v11}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2350888
    :cond_0
    if-eqz v7, :cond_4

    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v2, v4}, LX/GR3;->a(Z)V

    .line 2350889
    sget-object v4, LX/GR0;->NO_CONNECTIVITY:LX/GR0;

    if-ne v5, v4, :cond_1

    .line 2350890
    sget-object v4, LX/GR4;->h:Landroid/content/Context;

    .line 2350891
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2350892
    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2350893
    invoke-static {v4, v6}, LX/GR2;->a(Landroid/content/Context;Ljava/util/Map;)V

    .line 2350894
    :cond_1
    sget-object v4, LX/GR0;->SUCCESS:LX/GR0;

    if-eq v5, v4, :cond_2

    .line 2350895
    iget-object v4, v3, LX/GR1;->b:LX/GR0;

    sget-object v6, LX/GR0;->NO_CONNECTIVITY:LX/GR0;

    if-eq v4, v6, :cond_2

    .line 2350896
    iput-object v5, v3, LX/GR1;->b:LX/GR0;

    .line 2350897
    :cond_2
    return-void

    .line 2350898
    :cond_3
    const-string v4, "Failed:\n  Response: %s\n  Error %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, LX/GAY;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v7}, LX/GAF;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2350899
    sget-object v4, LX/GR0;->SERVER_ERROR:LX/GR0;

    move-object v6, v5

    move-object v5, v4

    goto :goto_0

    .line 2350900
    :catch_0
    const-string v4, "<Can\'t encode events for debug logging>"

    goto :goto_1

    .line 2350901
    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    :cond_5
    move-object v6, v5

    move-object v5, v4

    goto :goto_0
.end method
