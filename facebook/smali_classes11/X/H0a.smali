.class public final enum LX/H0a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H0a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H0a;

.field public static final enum HEADER:LX/H0a;


# instance fields
.field private final viewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2414019
    new-instance v0, LX/H0a;

    const-string v1, "HEADER"

    const v2, 0x7f0d00e9

    invoke-direct {v0, v1, v3, v2}, LX/H0a;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0a;->HEADER:LX/H0a;

    .line 2414020
    const/4 v0, 0x1

    new-array v0, v0, [LX/H0a;

    sget-object v1, LX/H0a;->HEADER:LX/H0a;

    aput-object v1, v0, v3

    sput-object v0, LX/H0a;->$VALUES:[LX/H0a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2414021
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2414022
    iput p3, p0, LX/H0a;->viewType:I

    .line 2414023
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H0a;
    .locals 1

    .prologue
    .line 2414024
    const-class v0, LX/H0a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H0a;

    return-object v0
.end method

.method public static values()[LX/H0a;
    .locals 1

    .prologue
    .line 2414025
    sget-object v0, LX/H0a;->$VALUES:[LX/H0a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H0a;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2414026
    iget v0, p0, LX/H0a;->viewType:I

    return v0
.end method
