.class public LX/FDx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static a:I

.field public static b:I


# instance fields
.field public final c:I

.field public final d:J

.field public final e:J

.field public final f:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:LX/6ek;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2215941
    sput v0, LX/FDx;->a:I

    .line 2215942
    sput v0, LX/FDx;->b:I

    return-void
.end method

.method public constructor <init>(LX/FDw;)V
    .locals 2

    .prologue
    .line 2215943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2215944
    iget v0, p1, LX/FDw;->a:I

    iput v0, p0, LX/FDx;->c:I

    .line 2215945
    iget-wide v0, p1, LX/FDw;->b:J

    iput-wide v0, p0, LX/FDx;->d:J

    .line 2215946
    iget-wide v0, p1, LX/FDw;->c:J

    iput-wide v0, p0, LX/FDx;->e:J

    .line 2215947
    iget-object v0, p1, LX/FDw;->e:LX/6ek;

    iput-object v0, p0, LX/FDx;->g:LX/6ek;

    .line 2215948
    iget-object v0, p1, LX/FDw;->d:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, LX/FDx;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2215949
    return-void
.end method

.method public static newBuilder()LX/FDw;
    .locals 1

    .prologue
    .line 2215950
    new-instance v0, LX/FDw;

    invoke-direct {v0}, LX/FDw;-><init>()V

    return-object v0
.end method
