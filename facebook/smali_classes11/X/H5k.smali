.class public final LX/H5k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;I)V
    .locals 0

    .prologue
    .line 2424971
    iput-object p1, p0, LX/H5k;->b:Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;

    iput p2, p0, LX/H5k;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x3391be74

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2424972
    iget-object v0, p0, LX/H5k;->b:Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->cW:Ljava/lang/String;

    sget-object v4, LX/5Oz;->NOTIFICATIONS_FRIENDING_TAB_SEE_ALL_FRIEND_REQUESTS:LX/5Oz;

    invoke-virtual {v4}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/5P0;->REQUESTS:LX/5P0;

    invoke-virtual {v5}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2424973
    iget-object v0, p0, LX/H5k;->b:Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/notifications/multirow/partdefinition/NotificationsFriendingBucketHeaderComponentPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3TK;

    iget v2, p0, LX/H5k;->a:I

    invoke-virtual {v0, v2}, LX/3TK;->b(I)V

    .line 2424974
    const v0, -0x31675e5e

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
