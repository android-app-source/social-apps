.class public final LX/GNF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GCE;

.field public final synthetic b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public final synthetic c:LX/GDm;

.field public final synthetic d:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GDm;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2345583
    iput-object p1, p0, LX/GNF;->a:LX/GCE;

    iput-object p2, p0, LX/GNF;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object p3, p0, LX/GNF;->c:LX/GDm;

    iput-object p4, p0, LX/GNF;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2345584
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2345585
    const-string v0, "payments_flow_context_key"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2345586
    iget-object v1, p0, LX/GNF;->a:LX/GCE;

    .line 2345587
    iput-object v0, v1, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2345588
    iget-object v0, p0, LX/GNF;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const-string v1, "checkout_payment_ids"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2345589
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->q:[Ljava/lang/String;

    .line 2345590
    iget-object v0, p0, LX/GNF;->c:LX/GDm;

    iget-object v1, p0, LX/GNF;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v2, p0, LX/GNF;->d:Landroid/content/Context;

    sget-object v3, LX/A8v;->UPDATE_BUDGET:LX/A8v;

    invoke-virtual {v0, v1, v2, v3}, LX/GDm;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;LX/A8v;)V

    .line 2345591
    :cond_0
    return-void
.end method
