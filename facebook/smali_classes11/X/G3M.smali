.class public final LX/G3M;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G3Q;


# direct methods
.method public constructor <init>(LX/G3Q;)V
    .locals 0

    .prologue
    .line 2315394
    iput-object p1, p0, LX/G3M;->a:LX/G3Q;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2315395
    iget-object v0, p0, LX/G3M;->a:LX/G3Q;

    .line 2315396
    iget-object p1, v0, LX/G3Q;->c:LX/G3C;

    invoke-interface {p1}, LX/G3C;->m()V

    .line 2315397
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2315398
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2315399
    iget-object v0, p0, LX/G3M;->a:LX/G3Q;

    .line 2315400
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2315401
    check-cast v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 2315402
    :goto_0
    iget-object v2, v0, LX/G3Q;->b:LX/G3c;

    .line 2315403
    iput-object v1, v2, LX/G3c;->c:Ljava/lang/String;

    .line 2315404
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/G3c;->i:Z

    .line 2315405
    iget-object v2, v0, LX/G3Q;->e:LX/G3G;

    iget-object v1, v0, LX/G3Q;->b:LX/G3c;

    .line 2315406
    iget-boolean v3, v1, LX/G3c;->j:Z

    move v1, v3

    .line 2315407
    if-eqz v1, :cond_1

    const-string v1, "profile_refresher"

    :goto_1
    iget-object v3, v0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {v3}, LX/G3c;->f()LX/0Px;

    move-result-object v3

    iget-object p0, v0, LX/G3Q;->b:LX/G3c;

    invoke-virtual {p0}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object p0

    invoke-virtual {v2, v1, v3, p0}, LX/G3G;->g(Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)V

    .line 2315408
    invoke-static {v0}, LX/G3Q;->q(LX/G3Q;)V

    .line 2315409
    return-void

    .line 2315410
    :cond_0
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2315411
    check-cast v1, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2315412
    :cond_1
    const-string v1, "profile_nux"

    goto :goto_1
.end method
