.class public final LX/GeM;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/GeN;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2375560
    invoke-static {}, LX/GeN;->q()LX/GeN;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2375561
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2375562
    const-string v0, "CSFBFeedContentText"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2375563
    if-ne p0, p1, :cond_1

    .line 2375564
    :cond_0
    :goto_0
    return v0

    .line 2375565
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2375566
    goto :goto_0

    .line 2375567
    :cond_3
    check-cast p1, LX/GeM;

    .line 2375568
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2375569
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2375570
    if-eq v2, v3, :cond_0

    .line 2375571
    iget-object v2, p0, LX/GeM;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/GeM;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/GeM;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2375572
    goto :goto_0

    .line 2375573
    :cond_4
    iget-object v2, p1, LX/GeM;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
