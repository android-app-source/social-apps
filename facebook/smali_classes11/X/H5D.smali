.class public LX/H5D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Tt;


# instance fields
.field public final a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field private final b:LX/1rn;

.field public c:LX/H5C;

.field public d:LX/H5C;

.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H5B;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Z


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1rn;LX/0xW;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "LX/1rn;",
            "LX/0xW;",
            "LX/0Ot",
            "<",
            "LX/H5B;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2423786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423787
    iput-object p1, p0, LX/H5D;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 2423788
    iput-object p2, p0, LX/H5D;->b:LX/1rn;

    .line 2423789
    invoke-virtual {p3}, LX/0xW;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, LX/0xW;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/H5D;->h:Z

    .line 2423790
    iput-object p4, p0, LX/H5D;->g:LX/0Ot;

    .line 2423791
    new-instance v0, LX/H5C;

    invoke-direct {v0, p0}, LX/H5C;-><init>(LX/H5D;)V

    iput-object v0, p0, LX/H5D;->c:LX/H5C;

    .line 2423792
    new-instance v0, LX/H5C;

    invoke-direct {v0, p0}, LX/H5C;-><init>(LX/H5D;)V

    iput-object v0, p0, LX/H5D;->d:LX/H5C;

    .line 2423793
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2423794
    iput-object v0, p0, LX/H5D;->e:LX/0Px;

    .line 2423795
    iput-boolean v1, p0, LX/H5D;->f:Z

    .line 2423796
    return-void

    :cond_0
    move v0, v1

    .line 2423797
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/H5D;
    .locals 5

    .prologue
    .line 2423880
    new-instance v3, LX/H5D;

    invoke-static {p0}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static {p0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v1

    check-cast v1, LX/1rn;

    invoke-static {p0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v2

    check-cast v2, LX/0xW;

    const/16 v4, 0x2ad0

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v3, v0, v1, v2, v4}, LX/H5D;-><init>(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/1rn;LX/0xW;LX/0Ot;)V

    .line 2423881
    move-object v0, v3

    .line 2423882
    return-object v0
.end method

.method public static e(LX/H5D;)V
    .locals 12

    .prologue
    .line 2423804
    iget-boolean v0, p0, LX/H5D;->h:Z

    if-eqz v0, :cond_5

    .line 2423805
    iget-object v0, p0, LX/H5D;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5B;

    .line 2423806
    iget-object v1, v0, LX/H5B;->c:LX/DrB;

    iget-object v2, v0, LX/H5B;->e:Landroid/content/Context;

    .line 2423807
    iget-object v3, v1, LX/DrB;->e:LX/0Px;

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/DrB;->e:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2423808
    :cond_0
    iget-object v3, v1, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {}, LX/DrH;->a()LX/0Tn;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 2423809
    if-lez v3, :cond_6

    .line 2423810
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2423811
    new-instance v6, LX/BAk;

    invoke-direct {v6}, LX/BAk;-><init>()V

    .line 2423812
    new-instance v7, LX/BAm;

    invoke-direct {v7}, LX/BAm;-><init>()V

    .line 2423813
    new-instance v8, LX/BAl;

    invoke-direct {v8}, LX/BAl;-><init>()V

    .line 2423814
    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    .line 2423815
    iget-object v9, v1, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v4}, LX/DrH;->a(I)LX/0Tn;

    move-result-object v10

    const/4 v2, 0x0

    invoke-interface {v9, v10, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v9

    .line 2423816
    iput-object v9, v6, LX/BAk;->b:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 2423817
    iget-object v9, v1, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v4}, LX/DrH;->b(I)LX/0Tn;

    move-result-object v10

    const/4 v2, 0x0

    invoke-interface {v9, v10, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2423818
    iput-object v9, v7, LX/BAm;->a:Ljava/lang/String;

    .line 2423819
    invoke-virtual {v7}, LX/BAm;->a()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    move-result-object v9

    .line 2423820
    iput-object v9, v6, LX/BAk;->h:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    .line 2423821
    invoke-static {v1, v6, v8, v4}, LX/DrB;->a(LX/DrB;LX/BAk;LX/BAl;I)V

    .line 2423822
    iget-object v9, v1, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v4}, LX/DrH;->f(I)LX/0Tn;

    move-result-object v10

    const/4 v2, 0x0

    invoke-interface {v9, v10, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    move-result-object v9

    .line 2423823
    iput-object v9, v6, LX/BAk;->g:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    .line 2423824
    iget-object v9, v1, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v4}, LX/DrH;->g(I)LX/0Tn;

    move-result-object v10

    const/4 v2, -0x1

    invoke-interface {v9, v10, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v9

    .line 2423825
    iput v9, v6, LX/BAk;->d:I

    .line 2423826
    iget-object v9, v1, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v4}, LX/DrH;->h(I)LX/0Tn;

    move-result-object v10

    const/4 v2, -0x1

    invoke-interface {v9, v10, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v9

    .line 2423827
    iput v9, v6, LX/BAk;->e:I

    .line 2423828
    iget-object v9, v1, LX/DrB;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v4}, LX/DrH;->i(I)LX/0Tn;

    move-result-object v10

    const/4 v2, -0x1

    invoke-interface {v9, v10, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v9

    .line 2423829
    iput v9, v6, LX/BAk;->f:I

    .line 2423830
    invoke-virtual {v6}, LX/BAk;->a()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-result-object v9

    invoke-virtual {v5, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2423831
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2423832
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 2423833
    :goto_1
    move-object v3, v3

    .line 2423834
    iput-object v3, v1, LX/DrB;->e:LX/0Px;

    .line 2423835
    iget-object v3, v1, LX/DrB;->b:LX/DrF;

    .line 2423836
    new-instance v4, LX/DrA;

    invoke-direct {v4, v1}, LX/DrA;-><init>(LX/DrB;)V

    move-object v4, v4

    .line 2423837
    const/4 v5, 0x1

    .line 2423838
    if-nez v4, :cond_7

    .line 2423839
    :cond_2
    :goto_2
    iget-object v3, v1, LX/DrB;->e:LX/0Px;

    move-object v3, v3

    .line 2423840
    iget-object v1, v0, LX/H5B;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 2423841
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    .line 2423842
    iget-object v5, v0, LX/H5B;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->name()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, LX/H5B;->d:LX/H59;

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v8

    .line 2423843
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    if-ne v8, v9, :cond_8

    .line 2423844
    new-instance v9, LX/H5A;

    invoke-direct {v9, v0}, LX/H5A;-><init>(LX/H5B;)V

    .line 2423845
    :goto_4
    move-object v8, v9

    .line 2423846
    new-instance v10, LX/H58;

    invoke-static {v7}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v9

    check-cast v9, LX/0xW;

    const/16 v11, 0xe8b

    invoke-static {v7, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct {v10, v1, v9, v11, v8}, LX/H58;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;LX/0xW;LX/0Ot;LX/H55;)V

    .line 2423847
    invoke-static {v7}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    .line 2423848
    iput-object v9, v10, LX/H58;->b:LX/0SG;

    .line 2423849
    move-object v1, v10

    .line 2423850
    invoke-virtual {v5, v6, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2423851
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2423852
    :cond_3
    iget-object v0, p0, LX/H5D;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5B;

    .line 2423853
    const/4 v1, 0x0

    iput-object v1, v0, LX/H5B;->f:LX/0Px;

    .line 2423854
    iget-object v1, v0, LX/H5B;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H58;

    .line 2423855
    iget-object v3, v1, LX/H58;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2423856
    goto :goto_5

    .line 2423857
    :cond_4
    iget-object v1, v0, LX/H5B;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2423858
    iget-object v0, p0, LX/H5D;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5B;

    iget-object v1, p0, LX/H5D;->d:LX/H5C;

    iget-object v1, v1, LX/H5C;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/H5B;->a(Ljava/util/List;)V

    .line 2423859
    iget-object v0, p0, LX/H5D;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5B;

    iget-object v1, p0, LX/H5D;->c:LX/H5C;

    iget-object v1, v1, LX/H5C;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/H5B;->a(Ljava/util/List;)V

    .line 2423860
    :goto_6
    return-void

    .line 2423861
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2423862
    iget-object v1, p0, LX/H5D;->d:LX/H5C;

    iget-object v1, v1, LX/H5C;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2423863
    iget-object v1, p0, LX/H5D;->c:LX/H5C;

    iget-object v1, v1, LX/H5C;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2423864
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/H5D;->e:LX/0Px;

    goto :goto_6

    .line 2423865
    :cond_6
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2423866
    new-instance v4, LX/BAk;

    invoke-direct {v4}, LX/BAk;-><init>()V

    .line 2423867
    new-instance v5, LX/BAm;

    invoke-direct {v5}, LX/BAm;-><init>()V

    .line 2423868
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 2423869
    iput-object v6, v4, LX/BAk;->b:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 2423870
    const v6, 0x7f081184

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2423871
    iput-object v6, v5, LX/BAm;->a:Ljava/lang/String;

    .line 2423872
    invoke-virtual {v5}, LX/BAm;->a()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    move-result-object v5

    .line 2423873
    iput-object v5, v4, LX/BAk;->h:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    .line 2423874
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->ALL:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    .line 2423875
    iput-object v5, v4, LX/BAk;->g:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    .line 2423876
    invoke-virtual {v4}, LX/BAk;->a()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2423877
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 2423878
    goto/16 :goto_1

    .line 2423879
    :cond_7
    iget-object v6, v3, LX/DrF;->d:LX/0Sh;

    new-instance v7, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;

    invoke-direct {v7, v3, v5, v4}, Lcom/facebook/notifications/settings/data/NotificationsBucketSettingsFetcher$1;-><init>(LX/DrF;ZLX/0TF;)V

    invoke-virtual {v6, v7}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_2

    :cond_8
    sget-object v9, LX/H58;->a:LX/H55;

    goto/16 :goto_4
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2423803
    iget-object v0, p0, LX/H5D;->d:LX/H5C;

    invoke-virtual {v0}, LX/H5C;->a()I

    move-result v0

    iget-object v1, p0, LX/H5D;->c:LX/H5C;

    invoke-virtual {v1}, LX/H5C;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final a(Ljava/lang/String;)LX/2nq;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2423798
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2423799
    :cond_0
    :goto_0
    return-object v0

    .line 2423800
    :cond_1
    invoke-virtual {p0, p1}, LX/H5D;->b(Ljava/lang/String;)I

    move-result v1

    .line 2423801
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2423802
    invoke-virtual {p0, v1}, LX/H5D;->b(I)LX/2nq;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2423782
    iget-boolean v0, p0, LX/H5D;->h:Z

    if-eqz v0, :cond_0

    .line 2423783
    iget-object v0, p0, LX/H5D;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5B;

    .line 2423784
    invoke-static {v0}, LX/H5B;->d(LX/H5B;)LX/0Px;

    move-result-object p0

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    .line 2423785
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/H5D;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 2423883
    invoke-virtual {p0, p1}, LX/H5D;->b(I)LX/2nq;

    move-result-object v0

    .line 2423884
    invoke-static {v0, p2}, LX/BDX;->a(LX/2nq;Ljava/lang/String;)LX/2nq;

    move-result-object v1

    .line 2423885
    if-nez v1, :cond_0

    .line 2423886
    :goto_0
    return-void

    .line 2423887
    :cond_0
    iget-object v2, p0, LX/H5D;->a:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2423888
    iget-object v2, p0, LX/H5D;->b:LX/1rn;

    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, LX/2nq;->n()LX/BAy;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1rn;->a(Ljava/lang/String;LX/BAy;)V

    .line 2423889
    invoke-virtual {p0, v0, v1}, LX/H5D;->a(LX/2nq;LX/2nq;)V

    goto :goto_0
.end method

.method public final a(LX/2nq;LX/2nq;)V
    .locals 1

    .prologue
    .line 2423778
    iget-object v0, p0, LX/H5D;->d:LX/H5C;

    invoke-virtual {v0, p1, p2}, LX/H5C;->a(LX/2nq;LX/2nq;)V

    .line 2423779
    iget-object v0, p0, LX/H5D;->c:LX/H5C;

    invoke-virtual {v0, p1, p2}, LX/H5C;->a(LX/2nq;LX/2nq;)V

    .line 2423780
    invoke-static {p0}, LX/H5D;->e(LX/H5D;)V

    .line 2423781
    return-void
.end method

.method public final a(LX/2nq;)Z
    .locals 2

    .prologue
    .line 2423777
    iget-object v0, p0, LX/H5D;->c:LX/H5C;

    iget-object v0, v0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2423752
    iget-object v0, p0, LX/H5D;->d:LX/H5C;

    iget-object v0, v0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2423753
    iget-object v0, p0, LX/H5D;->d:LX/H5C;

    iget-object v0, v0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2423754
    :goto_0
    return v0

    .line 2423755
    :cond_0
    iget-object v0, p0, LX/H5D;->c:LX/H5C;

    iget-object v0, v0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2423756
    iget-object v0, p0, LX/H5D;->c:LX/H5C;

    iget-object v0, v0, LX/H5C;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/H5D;->d:LX/H5C;

    invoke-virtual {v1}, LX/H5C;->a()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 2423757
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final b(I)LX/2nq;
    .locals 3

    .prologue
    .line 2423769
    iget-object v0, p0, LX/H5D;->d:LX/H5C;

    invoke-virtual {v0}, LX/H5C;->a()I

    move-result v0

    .line 2423770
    iget-object v1, p0, LX/H5D;->c:LX/H5C;

    invoke-virtual {v1}, LX/H5C;->a()I

    move-result v1

    .line 2423771
    if-ge p1, v0, :cond_0

    .line 2423772
    iget-object v0, p0, LX/H5D;->d:LX/H5C;

    iget-object v0, v0, LX/H5C;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 2423773
    :goto_0
    return-object v0

    .line 2423774
    :cond_0
    add-int/2addr v1, v0

    if-ge p1, v1, :cond_1

    .line 2423775
    iget-object v1, p0, LX/H5D;->c:LX/H5C;

    iget-object v1, v1, LX/H5C;->b:Ljava/util/List;

    sub-int v0, p1, v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    goto :goto_0

    .line 2423776
    :cond_1
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error accessing position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " inside NotificationsFeedCollection"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(Ljava/lang/String;)LX/2nq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2423763
    iget-object v0, p0, LX/H5D;->d:LX/H5C;

    iget-object v0, v0, LX/H5C;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2423764
    iget-object v0, p0, LX/H5D;->d:LX/H5C;

    iget-object v0, v0, LX/H5C;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 2423765
    :goto_0
    return-object v0

    .line 2423766
    :cond_0
    iget-object v0, p0, LX/H5D;->c:LX/H5C;

    iget-object v0, v0, LX/H5C;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2423767
    iget-object v0, p0, LX/H5D;->c:LX/H5C;

    iget-object v0, v0, LX/H5C;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    goto :goto_0

    .line 2423768
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2423762
    invoke-virtual {p0}, LX/H5D;->a()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2423758
    iget-boolean v0, p0, LX/H5D;->h:Z

    if-eqz v0, :cond_0

    .line 2423759
    iget-object v0, p0, LX/H5D;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5B;

    .line 2423760
    invoke-static {v0}, LX/H5B;->d(LX/H5B;)LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p0

    move v0, p0

    .line 2423761
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/H5D;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
