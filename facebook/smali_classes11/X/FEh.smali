.class public final enum LX/FEh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FEh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FEh;

.field public static final enum ADMIN_MESSAGE:LX/FEh;

.field public static final enum COMPOSER:LX/FEh;

.field public static final enum GAME_EMOJI:LX/FEh;

.field public static final enum GAME_INBOX_UNIT:LX/FEh;

.field public static final enum GAME_SCORE_SHARE:LX/FEh;

.field public static final enum GAME_SHARE:LX/FEh;

.field public static final enum SEARCH:LX/FEh;

.field private static final TAG:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final enum UNKNOWN:LX/FEh;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2216590
    new-instance v0, LX/FEh;

    const-string v1, "COMPOSER"

    const-string v2, "composer"

    invoke-direct {v0, v1, v4, v2}, LX/FEh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEh;->COMPOSER:LX/FEh;

    .line 2216591
    new-instance v0, LX/FEh;

    const-string v1, "ADMIN_MESSAGE"

    const-string v2, "admin_message"

    invoke-direct {v0, v1, v5, v2}, LX/FEh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEh;->ADMIN_MESSAGE:LX/FEh;

    .line 2216592
    new-instance v0, LX/FEh;

    const-string v1, "GAME_EMOJI"

    const-string v2, "game_emoji"

    invoke-direct {v0, v1, v6, v2}, LX/FEh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEh;->GAME_EMOJI:LX/FEh;

    .line 2216593
    new-instance v0, LX/FEh;

    const-string v1, "GAME_SHARE"

    const-string v2, "game_share"

    invoke-direct {v0, v1, v7, v2}, LX/FEh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEh;->GAME_SHARE:LX/FEh;

    .line 2216594
    new-instance v0, LX/FEh;

    const-string v1, "GAME_SCORE_SHARE"

    const-string v2, "game_score_share"

    invoke-direct {v0, v1, v8, v2}, LX/FEh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEh;->GAME_SCORE_SHARE:LX/FEh;

    .line 2216595
    new-instance v0, LX/FEh;

    const-string v1, "GAME_INBOX_UNIT"

    const/4 v2, 0x5

    const-string v3, "game_inbox_unit"

    invoke-direct {v0, v1, v2, v3}, LX/FEh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEh;->GAME_INBOX_UNIT:LX/FEh;

    .line 2216596
    new-instance v0, LX/FEh;

    const-string v1, "SEARCH"

    const/4 v2, 0x6

    const-string v3, "search"

    invoke-direct {v0, v1, v2, v3}, LX/FEh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEh;->SEARCH:LX/FEh;

    .line 2216597
    new-instance v0, LX/FEh;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x7

    const-string v3, "unknown"

    invoke-direct {v0, v1, v2, v3}, LX/FEh;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEh;->UNKNOWN:LX/FEh;

    .line 2216598
    const/16 v0, 0x8

    new-array v0, v0, [LX/FEh;

    sget-object v1, LX/FEh;->COMPOSER:LX/FEh;

    aput-object v1, v0, v4

    sget-object v1, LX/FEh;->ADMIN_MESSAGE:LX/FEh;

    aput-object v1, v0, v5

    sget-object v1, LX/FEh;->GAME_EMOJI:LX/FEh;

    aput-object v1, v0, v6

    sget-object v1, LX/FEh;->GAME_SHARE:LX/FEh;

    aput-object v1, v0, v7

    sget-object v1, LX/FEh;->GAME_SCORE_SHARE:LX/FEh;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/FEh;->GAME_INBOX_UNIT:LX/FEh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FEh;->SEARCH:LX/FEh;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FEh;->UNKNOWN:LX/FEh;

    aput-object v2, v0, v1

    sput-object v0, LX/FEh;->$VALUES:[LX/FEh;

    .line 2216599
    const-class v0, LX/FEh;

    sput-object v0, LX/FEh;->TAG:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2216587
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2216588
    iput-object p3, p0, LX/FEh;->value:Ljava/lang/String;

    .line 2216589
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FEh;
    .locals 1

    .prologue
    .line 2216586
    const-class v0, LX/FEh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FEh;

    return-object v0
.end method

.method public static values()[LX/FEh;
    .locals 1

    .prologue
    .line 2216575
    sget-object v0, LX/FEh;->$VALUES:[LX/FEh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FEh;

    return-object v0
.end method


# virtual methods
.method public final toQuicksilverSource()LX/8TZ;
    .locals 5

    .prologue
    .line 2216576
    sget-object v0, LX/FEg;->a:[I

    invoke-virtual {p0}, LX/FEh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2216577
    sget-object v0, LX/FEh;->TAG:Ljava/lang/Class;

    const-string v1, "Unknown GamesEntryPoint %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/FEh;->value:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2216578
    sget-object v0, LX/8TZ;->UNKNOWN:LX/8TZ;

    :goto_0
    return-object v0

    .line 2216579
    :pswitch_0
    sget-object v0, LX/8TZ;->MESSENGER_COMPOSER:LX/8TZ;

    goto :goto_0

    .line 2216580
    :pswitch_1
    sget-object v0, LX/8TZ;->MESSENGER_ADMIN_MESSAGE:LX/8TZ;

    goto :goto_0

    .line 2216581
    :pswitch_2
    sget-object v0, LX/8TZ;->MESSENGER_GAME_EMOJI:LX/8TZ;

    goto :goto_0

    .line 2216582
    :pswitch_3
    sget-object v0, LX/8TZ;->MESSENGER_GAME_SHARE:LX/8TZ;

    goto :goto_0

    .line 2216583
    :pswitch_4
    sget-object v0, LX/8TZ;->MESSENGER_GAME_SCORE_SHARE:LX/8TZ;

    goto :goto_0

    .line 2216584
    :pswitch_5
    sget-object v0, LX/8TZ;->MESSENGER_GAME_INBOX_UNIT:LX/8TZ;

    goto :goto_0

    .line 2216585
    :pswitch_6
    sget-object v0, LX/8TZ;->MESSENGER_GAME_SEARCH:LX/8TZ;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
