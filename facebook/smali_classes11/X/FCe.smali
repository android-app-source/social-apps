.class public final LX/FCe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FCh;


# direct methods
.method public constructor <init>(LX/FCh;)V
    .locals 0

    .prologue
    .line 2210441
    iput-object p1, p0, LX/FCe;->a:LX/FCh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2210442
    iget-object v0, p0, LX/FCe;->a:LX/FCh;

    const/4 v1, 0x0

    .line 2210443
    iput-object v1, v0, LX/FCh;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2210444
    iget-object v0, p0, LX/FCe;->a:LX/FCh;

    invoke-static {v0}, LX/FCh;->f(LX/FCh;)V

    .line 2210445
    iget-object v0, p0, LX/FCe;->a:LX/FCh;

    sget-object v1, LX/FCf;->PLAYBACK_ERROR:LX/FCf;

    invoke-static {v0, v1}, LX/FCh;->a$redex0(LX/FCh;LX/FCf;)V

    .line 2210446
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2210436
    iget-object v0, p0, LX/FCe;->a:LX/FCh;

    const/4 v1, 0x0

    .line 2210437
    iput-object v1, v0, LX/FCh;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2210438
    iget-object v0, p0, LX/FCe;->a:LX/FCh;

    sget-object v1, LX/FCf;->PLAYBACK_STARTED:LX/FCf;

    invoke-static {v0, v1}, LX/FCh;->a$redex0(LX/FCh;LX/FCf;)V

    .line 2210439
    iget-object v0, p0, LX/FCe;->a:LX/FCh;

    iget-object v0, v0, LX/FCh;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/FCe;->a:LX/FCh;

    iget-object v1, v1, LX/FCh;->l:Ljava/lang/Runnable;

    const v2, -0x3019745

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2210440
    return-void
.end method
