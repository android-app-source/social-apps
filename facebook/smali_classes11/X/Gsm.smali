.class public LX/Gsm;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ps;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2400165
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2400166
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/Gsm;

    const/16 v0, 0x4c1

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object p1, p0, LX/Gsm;->a:LX/0Or;

    .line 2400167
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 4

    .prologue
    .line 2400168
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2400169
    invoke-virtual {p0}, LX/Gsm;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2400170
    const-string v1, "Data Usage - internal"

    invoke-virtual {p0, v1}, LX/Gsm;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400171
    new-instance v1, LX/4ok;

    invoke-direct {v1, v0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2400172
    sget-object v2, LX/49Q;->b:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 2400173
    const-string v2, "Limit Data Usage"

    invoke-virtual {v1, v2}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400174
    const-string v2, "Limit the amount of data that can be transferred per hour"

    invoke-virtual {v1, v2}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400175
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400176
    invoke-virtual {p0, v1}, LX/Gsm;->addPreference(Landroid/preference/Preference;)Z

    .line 2400177
    new-instance v1, LX/4om;

    invoke-direct {v1, v0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 2400178
    sget-object v2, LX/49Q;->c:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4om;->a(LX/0Tn;)V

    .line 2400179
    invoke-virtual {v1}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 2400180
    const-string v2, "Hourly Limit"

    invoke-virtual {v1, v2}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400181
    const-string v2, "Amount of data (in K) allowed per hour"

    invoke-virtual {v1, v2}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400182
    const-string v2, "1024"

    invoke-virtual {v1, v2}, LX/4om;->setDefaultValue(Ljava/lang/Object;)V

    .line 2400183
    invoke-virtual {p0, v1}, LX/Gsm;->addPreference(Landroid/preference/Preference;)Z

    .line 2400184
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2400185
    const-string v2, "Reset Data"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2400186
    const-string v2, "Reset Data Usage for session"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2400187
    new-instance v2, LX/Gsl;

    invoke-direct {v2, p0, v0}, LX/Gsl;-><init>(LX/Gsm;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2400188
    invoke-virtual {p0, v1}, LX/Gsm;->addPreference(Landroid/preference/Preference;)Z

    .line 2400189
    return-void
.end method
