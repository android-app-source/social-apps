.class public final enum LX/H3t;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H3t;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H3t;

.field public static final enum CELL_CATEGORY_PICKER:LX/H3t;

.field public static final enum CELL_TURN_ON_LOCATION_SERVICES:LX/H3t;

.field public static final enum HEADER_CATEGORY_PICKER:LX/H3t;

.field public static final enum HEADER_NEAR_YOUR_LOCATION:LX/H3t;

.field public static final enum LOADING_CELL:LX/H3t;

.field public static final enum PLACES_HUGE_CELL:LX/H3t;


# instance fields
.field private final mCellId:I

.field private final mCellViewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2421973
    new-instance v0, LX/H3t;

    const-string v1, "HEADER_CATEGORY_PICKER"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v4, v2}, LX/H3t;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/H3t;->HEADER_CATEGORY_PICKER:LX/H3t;

    .line 2421974
    new-instance v0, LX/H3t;

    const-string v1, "CELL_CATEGORY_PICKER"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v5, v5, v2}, LX/H3t;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/H3t;->CELL_CATEGORY_PICKER:LX/H3t;

    .line 2421975
    new-instance v0, LX/H3t;

    const-string v1, "HEADER_NEAR_YOUR_LOCATION"

    const/4 v2, -0x3

    invoke-direct {v0, v1, v6, v4, v2}, LX/H3t;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/H3t;->HEADER_NEAR_YOUR_LOCATION:LX/H3t;

    .line 2421976
    new-instance v0, LX/H3t;

    const-string v1, "CELL_TURN_ON_LOCATION_SERVICES"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v7, v6, v2}, LX/H3t;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/H3t;->CELL_TURN_ON_LOCATION_SERVICES:LX/H3t;

    .line 2421977
    new-instance v0, LX/H3t;

    const-string v1, "LOADING_CELL"

    const/4 v2, -0x5

    invoke-direct {v0, v1, v8, v7, v2}, LX/H3t;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/H3t;->LOADING_CELL:LX/H3t;

    .line 2421978
    new-instance v0, LX/H3t;

    const-string v1, "PLACES_HUGE_CELL"

    const/4 v2, 0x5

    const/4 v3, -0x6

    invoke-direct {v0, v1, v2, v8, v3}, LX/H3t;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/H3t;->PLACES_HUGE_CELL:LX/H3t;

    .line 2421979
    const/4 v0, 0x6

    new-array v0, v0, [LX/H3t;

    sget-object v1, LX/H3t;->HEADER_CATEGORY_PICKER:LX/H3t;

    aput-object v1, v0, v4

    sget-object v1, LX/H3t;->CELL_CATEGORY_PICKER:LX/H3t;

    aput-object v1, v0, v5

    sget-object v1, LX/H3t;->HEADER_NEAR_YOUR_LOCATION:LX/H3t;

    aput-object v1, v0, v6

    sget-object v1, LX/H3t;->CELL_TURN_ON_LOCATION_SERVICES:LX/H3t;

    aput-object v1, v0, v7

    sget-object v1, LX/H3t;->LOADING_CELL:LX/H3t;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/H3t;->PLACES_HUGE_CELL:LX/H3t;

    aput-object v2, v0, v1

    sput-object v0, LX/H3t;->$VALUES:[LX/H3t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2421980
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2421981
    iput p3, p0, LX/H3t;->mCellViewType:I

    .line 2421982
    iput p4, p0, LX/H3t;->mCellId:I

    .line 2421983
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H3t;
    .locals 1

    .prologue
    .line 2421984
    const-class v0, LX/H3t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H3t;

    return-object v0
.end method

.method public static values()[LX/H3t;
    .locals 1

    .prologue
    .line 2421985
    sget-object v0, LX/H3t;->$VALUES:[LX/H3t;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H3t;

    return-object v0
.end method


# virtual methods
.method public final getCellId()I
    .locals 1

    .prologue
    .line 2421986
    iget v0, p0, LX/H3t;->mCellId:I

    return v0
.end method

.method public final getCellViewType()I
    .locals 1

    .prologue
    .line 2421987
    iget v0, p0, LX/H3t;->mCellViewType:I

    return v0
.end method
