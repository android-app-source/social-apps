.class public LX/FNL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FNL;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2232472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232473
    iput-object p1, p0, LX/FNL;->a:Landroid/content/Context;

    .line 2232474
    return-void
.end method

.method public static a(LX/0QB;)LX/FNL;
    .locals 4

    .prologue
    .line 2232477
    sget-object v0, LX/FNL;->b:LX/FNL;

    if-nez v0, :cond_1

    .line 2232478
    const-class v1, LX/FNL;

    monitor-enter v1

    .line 2232479
    :try_start_0
    sget-object v0, LX/FNL;->b:LX/FNL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232480
    if-eqz v2, :cond_0

    .line 2232481
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232482
    new-instance p0, LX/FNL;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/FNL;-><init>(Landroid/content/Context;)V

    .line 2232483
    move-object v0, p0

    .line 2232484
    sput-object v0, LX/FNL;->b:LX/FNL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232485
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232486
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232487
    :cond_1
    sget-object v0, LX/FNL;->b:LX/FNL;

    return-object v0

    .line 2232488
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232489
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/FNL;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2232475
    iget-object v0, p0, LX/FNL;->a:Landroid/content/Context;

    invoke-static {v0}, LX/0Xp;->a(Landroid/content/Context;)LX/0Xp;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 2232476
    return-void
.end method
