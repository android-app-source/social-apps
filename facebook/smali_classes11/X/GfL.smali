.class public LX/GfL;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GfM;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GfL",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/GfM;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377196
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2377197
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/GfL;->b:LX/0Zi;

    .line 2377198
    iput-object p1, p0, LX/GfL;->a:LX/0Ot;

    .line 2377199
    return-void
.end method

.method public static a(LX/0QB;)LX/GfL;
    .locals 4

    .prologue
    .line 2377200
    const-class v1, LX/GfL;

    monitor-enter v1

    .line 2377201
    :try_start_0
    sget-object v0, LX/GfL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377202
    sput-object v2, LX/GfL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377203
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377204
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377205
    new-instance v3, LX/GfL;

    const/16 p0, 0x2113

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GfL;-><init>(LX/0Ot;)V

    .line 2377206
    move-object v0, v3

    .line 2377207
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377208
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GfL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377209
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377210
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2377211
    const v0, 0x5265e35b

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2377212
    check-cast p2, LX/GfK;

    .line 2377213
    iget-object v0, p0, LX/GfL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GfM;

    iget-object v2, p2, LX/GfK;->a:LX/1f9;

    iget-object v3, p2, LX/GfK;->b:LX/1Pn;

    iget-object v4, p2, LX/GfK;->c:LX/25E;

    iget-object v5, p2, LX/GfK;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v6, p2, LX/GfK;->e:I

    iget-object v7, p2, LX/GfK;->f:LX/2dx;

    iget-boolean v8, p2, LX/GfK;->g:Z

    iget-boolean v9, p2, LX/GfK;->h:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, LX/GfM;->a(LX/1De;LX/1f9;LX/1Pn;LX/25E;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/2dx;ZZ)LX/1Dg;

    move-result-object v0

    .line 2377214
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2377215
    invoke-static {}, LX/1dS;->b()V

    .line 2377216
    iget v0, p1, LX/1dQ;->b:I

    .line 2377217
    packed-switch v0, :pswitch_data_0

    .line 2377218
    :goto_0
    return-object v2

    .line 2377219
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2377220
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2377221
    check-cast v1, LX/GfK;

    .line 2377222
    iget-object v3, p0, LX/GfL;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GfM;

    iget-object v4, v1, LX/GfK;->c:LX/25E;

    iget-object p1, v1, LX/GfK;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377223
    const v1, 0x7f0d006a

    iget-object p2, v3, LX/GfM;->e:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-interface {v4}, LX/25E;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    move p0, p2

    .line 2377224
    :goto_1
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 2377225
    check-cast p2, LX/16h;

    invoke-static {v4, p2}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object p2

    invoke-static {p0, p2}, LX/17Q;->a(ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2377226
    invoke-interface {v4}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p0

    .line 2377227
    iget-object p2, v3, LX/GfM;->d:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/1nA;

    invoke-static {p0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p0, v1}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2377228
    goto :goto_0

    .line 2377229
    :cond_0
    const/4 p2, 0x0

    move p0, p2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x5265e35b
        :pswitch_0
    .end packed-switch
.end method
