.class public LX/GZO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2366461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2366462
    iput-object p1, p0, LX/GZO;->a:LX/0Or;

    .line 2366463
    iput-object p2, p0, LX/GZO;->b:LX/0Ot;

    .line 2366464
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2366465
    new-instance v0, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;

    invoke-direct {v0, p1, p2}, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;-><init>(Ljava/lang/String;Z)V

    .line 2366466
    const-string v1, "merchantSubscriptionParams"

    const-string v2, "update_merchant_subscription_status"

    .line 2366467
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2366468
    if-eqz v0, :cond_0

    .line 2366469
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2366470
    :cond_0
    const-string p2, "overridden_viewer_context"

    iget-object v3, p0, LX/GZO;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0SI;

    invoke-interface {v3}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    invoke-virtual {p1, p2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2366471
    iget-object v3, p0, LX/GZO;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0aG;

    const p2, 0x4bdfeae8    # 2.9349328E7f

    invoke-static {v3, v2, p1, p2}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    .line 2366472
    invoke-interface {v3}, LX/1MF;->startTryNotToUseMainThread()LX/1ML;

    move-result-object v3

    move-object v0, v3

    .line 2366473
    return-object v0
.end method
