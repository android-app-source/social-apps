.class public final LX/GAa;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field public c:Landroid/content/SharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2325930
    const-class v0, LX/GAa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GAa;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2325813
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/GAa;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2325814
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2325923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2325924
    const-string v0, "context"

    invoke-static {p1, v0}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2325925
    invoke-static {p2}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "com.facebook.SharedPreferencesTokenCachingStrategy.DEFAULT_KEY"

    :cond_0
    iput-object p2, p0, LX/GAa;->b:Ljava/lang/String;

    .line 2325926
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2325927
    if-eqz v0, :cond_1

    move-object p1, v0

    .line 2325928
    :cond_1
    iget-object v0, p0, LX/GAa;->b:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/GAa;->c:Landroid/content/SharedPreferences;

    .line 2325929
    return-void
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/Date;
    .locals 6

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v0, 0x0

    .line 2325931
    if-nez p0, :cond_1

    .line 2325932
    :cond_0
    :goto_0
    return-object v0

    .line 2325933
    :cond_1
    invoke-virtual {p0, p1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2325934
    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2325935
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 14

    .prologue
    .line 2325817
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2325818
    iget-object v0, p0, LX/GAa;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 2325819
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2325820
    :try_start_0
    const/4 v13, 0x1

    const/4 v7, 0x0

    .line 2325821
    iget-object v8, p0, LX/GAa;->c:Landroid/content/SharedPreferences;

    const-string v9, "{}"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2325822
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2325823
    const-string v8, "valueType"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2325824
    const-string v10, "bool"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 2325825
    const-string v7, "value"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2325826
    :cond_0
    :goto_1
    goto :goto_0

    .line 2325827
    :catch_0
    move-exception v1

    .line 2325828
    sget-object v2, LX/GAb;->CACHE:LX/GAb;

    const/4 v3, 0x5

    sget-object v4, LX/GAa;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error reading cached value for key: \'"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "\' -- "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v4, v0}, LX/GsN;->a(LX/GAb;ILjava/lang/String;Ljava/lang/String;)V

    .line 2325829
    const/4 v0, 0x0

    .line 2325830
    :goto_2
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_2

    .line 2325831
    :cond_2
    :try_start_1
    const-string v10, "bool[]"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2325832
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 2325833
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [Z

    .line 2325834
    :goto_3
    array-length v10, v9

    if-ge v7, v10, :cond_3

    .line 2325835
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->getBoolean(I)Z

    move-result v10

    aput-boolean v10, v9, v7

    .line 2325836
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 2325837
    :cond_3
    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    goto :goto_1

    .line 2325838
    :cond_4
    const-string v10, "byte"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 2325839
    const-string v7, "value"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    int-to-byte v7, v7

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    goto :goto_1

    .line 2325840
    :cond_5
    const-string v10, "byte[]"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 2325841
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 2325842
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [B

    .line 2325843
    :goto_4
    array-length v10, v9

    if-ge v7, v10, :cond_6

    .line 2325844
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v10

    int-to-byte v10, v10

    aput-byte v10, v9, v7

    .line 2325845
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 2325846
    :cond_6
    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto/16 :goto_1

    .line 2325847
    :cond_7
    const-string v10, "short"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2325848
    const-string v7, "value"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    int-to-short v7, v7

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putShort(Ljava/lang/String;S)V

    goto/16 :goto_1

    .line 2325849
    :cond_8
    const-string v10, "short[]"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2325850
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 2325851
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [S

    .line 2325852
    :goto_5
    array-length v10, v9

    if-ge v7, v10, :cond_9

    .line 2325853
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v10

    int-to-short v10, v10

    aput-short v10, v9, v7

    .line 2325854
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    .line 2325855
    :cond_9
    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putShortArray(Ljava/lang/String;[S)V

    goto/16 :goto_1

    .line 2325856
    :cond_a
    const-string v10, "int"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 2325857
    const-string v7, "value"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 2325858
    :cond_b
    const-string v10, "int[]"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 2325859
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 2325860
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [I

    .line 2325861
    :goto_6
    array-length v10, v9

    if-ge v7, v10, :cond_c

    .line 2325862
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v10

    aput v10, v9, v7

    .line 2325863
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 2325864
    :cond_c
    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto/16 :goto_1

    .line 2325865
    :cond_d
    const-string v10, "long"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 2325866
    const-string v7, "value"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {v1, v0, v7, v8}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 2325867
    :cond_e
    const-string v10, "long[]"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 2325868
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 2325869
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [J

    .line 2325870
    :goto_7
    array-length v10, v9

    if-ge v7, v10, :cond_f

    .line 2325871
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v11

    aput-wide v11, v9, v7

    .line 2325872
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 2325873
    :cond_f
    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    goto/16 :goto_1

    .line 2325874
    :cond_10
    const-string v10, "float"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 2325875
    const-string v7, "value"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v7

    double-to-float v7, v7

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    goto/16 :goto_1

    .line 2325876
    :cond_11
    const-string v10, "float[]"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_13

    .line 2325877
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 2325878
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [F

    .line 2325879
    :goto_8
    array-length v10, v9

    if-ge v7, v10, :cond_12

    .line 2325880
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v11

    double-to-float v10, v11

    aput v10, v9, v7

    .line 2325881
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 2325882
    :cond_12
    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    goto/16 :goto_1

    .line 2325883
    :cond_13
    const-string v10, "double"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 2325884
    const-string v7, "value"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v7

    invoke-virtual {v1, v0, v7, v8}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    goto/16 :goto_1

    .line 2325885
    :cond_14
    const-string v10, "double[]"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    .line 2325886
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 2325887
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v9

    new-array v9, v9, [D

    .line 2325888
    :goto_9
    array-length v10, v9

    if-ge v7, v10, :cond_15

    .line 2325889
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v11

    aput-wide v11, v9, v7

    .line 2325890
    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    .line 2325891
    :cond_15
    invoke-virtual {v1, v0, v9}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    goto/16 :goto_1

    .line 2325892
    :cond_16
    const-string v10, "char"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_17

    .line 2325893
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2325894
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v9, v13, :cond_0

    .line 2325895
    invoke-virtual {v8, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putChar(Ljava/lang/String;C)V

    goto/16 :goto_1

    .line 2325896
    :cond_17
    const-string v10, "char[]"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1a

    .line 2325897
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 2325898
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v8

    new-array v10, v8, [C

    move v8, v7

    .line 2325899
    :goto_a
    array-length v11, v10

    if-ge v8, v11, :cond_19

    .line 2325900
    invoke-virtual {v9, v8}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 2325901
    if-eqz v11, :cond_18

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-ne v12, v13, :cond_18

    .line 2325902
    invoke-virtual {v11, v7}, Ljava/lang/String;->charAt(I)C

    move-result v11

    aput-char v11, v10, v8

    .line 2325903
    :cond_18
    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    .line 2325904
    :cond_19
    invoke-virtual {v1, v0, v10}, Landroid/os/Bundle;->putCharArray(Ljava/lang/String;[C)V

    goto/16 :goto_1

    .line 2325905
    :cond_1a
    const-string v10, "string"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 2325906
    const-string v7, "value"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2325907
    :cond_1b
    const-string v10, "stringList"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1e

    .line 2325908
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 2325909
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v10

    .line 2325910
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(I)V

    move v8, v7

    .line 2325911
    :goto_b
    if-ge v8, v10, :cond_1d

    .line 2325912
    invoke-virtual {v9, v8}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    .line 2325913
    sget-object v12, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-ne v7, v12, :cond_1c

    const/4 v7, 0x0

    :goto_c
    invoke-virtual {v11, v8, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2325914
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_b

    .line 2325915
    :cond_1c
    check-cast v7, Ljava/lang/String;

    goto :goto_c

    .line 2325916
    :cond_1d
    invoke-virtual {v1, v0, v11}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_1

    .line 2325917
    :cond_1e
    const-string v7, "enum"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2325918
    :try_start_2
    const-string v7, "enumType"

    invoke-virtual {v9, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2325919
    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    .line 2325920
    const-string v8, "value"

    invoke-virtual {v9, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v7

    .line 2325921
    invoke-virtual {v1, v0, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 2325922
    :catch_1
    goto/16 :goto_1

    :catch_2
    goto/16 :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2325815
    iget-object v0, p0, LX/GAa;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2325816
    return-void
.end method
