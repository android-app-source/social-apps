.class public final LX/GlT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Gkm;

.field public final synthetic b:LX/GlX;


# direct methods
.method public constructor <init>(LX/GlX;LX/Gkm;)V
    .locals 0

    .prologue
    .line 2390800
    iput-object p1, p0, LX/GlT;->b:LX/GlX;

    iput-object p2, p0, LX/GlT;->a:LX/Gkm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2390801
    iget-object v0, p0, LX/GlT;->b:LX/GlX;

    iget-object v1, p0, LX/GlT;->a:LX/Gkm;

    .line 2390802
    invoke-virtual {v0}, LX/GlX;->d()LX/Gkm;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 2390803
    iget-object v2, v0, LX/GlX;->e:LX/DZB;

    sget-object p0, LX/DZA;->h:LX/0Tn;

    invoke-virtual {v1}, LX/Gkm;->name()Ljava/lang/String;

    move-result-object p1

    .line 2390804
    if-eqz p0, :cond_1

    .line 2390805
    if-eqz p1, :cond_0

    .line 2390806
    iget-object v1, v2, LX/DZB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    move v1, v1

    .line 2390807
    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v2, p0, v1}, LX/DZB;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2390808
    :cond_0
    iget-object v1, v2, LX/DZB;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    invoke-interface {v1, p0, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2390809
    :cond_1
    iget-object v2, v0, LX/GlX;->d:LX/Gld;

    .line 2390810
    iget-object p0, v2, LX/Gld;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    invoke-virtual {p0}, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->e()LX/Gkk;

    move-result-object p0

    iget-object p1, v2, LX/Gld;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    iget-object p1, p1, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-virtual {p1}, LX/GlX;->d()LX/Gkm;

    move-result-object p1

    invoke-interface {p0, p1}, LX/Gkk;->a(LX/Gkm;)V

    .line 2390811
    iget-object p0, v2, LX/Gld;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    iget-object p0, p0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->g:LX/Glj;

    iget-object p1, v2, LX/Gld;->a:Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;

    iget-object p1, p1, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-virtual {p1}, LX/GlX;->d()LX/Gkm;

    move-result-object p1

    .line 2390812
    iget-object v0, p0, LX/Glj;->c:LX/0Zb;

    const-string v1, "groups_grid_new_order"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2390813
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2390814
    const-string v1, "groups_grid"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    iget-object v1, p0, LX/Glj;->d:LX/0kv;

    iget-object v2, p0, LX/Glj;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "ordering"

    invoke-virtual {p1}, LX/Gkm;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2390815
    :cond_2
    const/4 v0, 0x1

    return v0
.end method
