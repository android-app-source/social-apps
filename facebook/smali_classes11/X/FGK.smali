.class public LX/FGK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:J

.field private final b:LX/0So;

.field private final c:Landroid/net/Uri;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;JILX/0So;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2218805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218806
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218807
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218808
    if-lez p4, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2218809
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-lez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2218810
    iput-object p1, p0, LX/FGK;->c:Landroid/net/Uri;

    .line 2218811
    iput-wide p2, p0, LX/FGK;->a:J

    .line 2218812
    iput p4, p0, LX/FGK;->d:I

    .line 2218813
    iput-object p5, p0, LX/FGK;->b:LX/0So;

    .line 2218814
    return-void

    :cond_0
    move v0, v2

    .line 2218815
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2218816
    goto :goto_1
.end method

.method public static declared-synchronized d(LX/FGK;)I
    .locals 1

    .prologue
    .line 2218817
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/FGK;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 2218802
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/FGK;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/FGK;->d:I

    .line 2218803
    iget v0, p0, LX/FGK;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 2218804
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 2218801
    iget-wide v0, p0, LX/FGK;->a:J

    iget-object v2, p0, LX/FGK;->b:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
