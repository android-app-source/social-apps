.class public final LX/GZJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

.field public final synthetic b:LX/GZK;


# direct methods
.method public constructor <init>(LX/GZK;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;)V
    .locals 0

    .prologue
    .line 2366360
    iput-object p1, p0, LX/GZJ;->b:LX/GZK;

    iput-object p2, p0, LX/GZJ;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x44ff9869

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2366361
    iget-object v1, p0, LX/GZJ;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GZJ;->b:LX/GZK;

    iget-wide v2, v2, LX/GZK;->h:J

    invoke-static {v1, v2, v3, v5}, LX/7iS;->a(Ljava/lang/String;JZ)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2366362
    iget-object v2, p0, LX/GZJ;->b:LX/GZK;

    iget-object v2, v2, LX/GZK;->d:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2366363
    iget-object v1, p0, LX/GZJ;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sget-object v3, LX/7iO;->COLLECTION_GRID:LX/7iO;

    invoke-static {v1, v2, v3}, LX/7iS;->b(Ljava/lang/String;Ljava/lang/Boolean;LX/7iO;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2366364
    iget-object v2, p0, LX/GZJ;->b:LX/GZK;

    iget-object v2, v2, LX/GZK;->d:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2366365
    iget-object v1, p0, LX/GZJ;->b:LX/GZK;

    iget-object v1, v1, LX/GZK;->a:LX/7j6;

    iget-object v2, p0, LX/GZJ;->a:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/GZJ;->b:LX/GZK;

    iget-object v3, v3, LX/GZK;->j:LX/7iP;

    invoke-virtual {v1, v2, v3}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2366366
    const v1, 0x4e107391    # 6.0587322E8f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
