.class public LX/FUu;
.super LX/5r0;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/5r0",
        "<",
        "LX/FUu;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/FUu;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:LX/FUv;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2250236
    new-instance v0, LX/0Zi;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/FUu;->a:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2250234
    invoke-direct {p0}, LX/5r0;-><init>()V

    .line 2250235
    return-void
.end method

.method public static a(ILX/FUv;IIIIII)LX/FUu;
    .locals 9

    .prologue
    .line 2250229
    sget-object v0, LX/FUu;->a:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FUu;

    .line 2250230
    if-nez v0, :cond_0

    .line 2250231
    new-instance v0, LX/FUu;

    invoke-direct {v0}, LX/FUu;-><init>()V

    :cond_0
    move v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    .line 2250232
    invoke-direct/range {v0 .. v8}, LX/FUu;->b(ILX/FUv;IIIIII)V

    .line 2250233
    return-object v0
.end method

.method private b(ILX/FUv;IIIIII)V
    .locals 0

    .prologue
    .line 2250220
    invoke-super {p0, p1}, LX/5r0;->a(I)V

    .line 2250221
    iput-object p2, p0, LX/FUu;->h:LX/FUv;

    .line 2250222
    iput p3, p0, LX/FUu;->b:I

    .line 2250223
    iput p4, p0, LX/FUu;->c:I

    .line 2250224
    iput p5, p0, LX/FUu;->d:I

    .line 2250225
    iput p6, p0, LX/FUu;->e:I

    .line 2250226
    iput p7, p0, LX/FUu;->f:I

    .line 2250227
    iput p8, p0, LX/FUu;->g:I

    .line 2250228
    return-void
.end method

.method private j()LX/5pH;
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 2250196
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v0

    .line 2250197
    const-string v1, "top"

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250198
    const-string v1, "bottom"

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250199
    const-string v1, "left"

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250200
    const-string v1, "right"

    invoke-interface {v0, v1, v2, v3}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250201
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2250202
    const-string v2, "x"

    iget v3, p0, LX/FUu;->b:I

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250203
    const-string v2, "y"

    iget v3, p0, LX/FUu;->c:I

    int-to-float v3, v3

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    float-to-double v4, v3

    invoke-interface {v1, v2, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250204
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v2

    .line 2250205
    const-string v3, "width"

    iget v4, p0, LX/FUu;->d:I

    int-to-float v4, v4

    invoke-static {v4}, LX/5r2;->c(F)F

    move-result v4

    float-to-double v4, v4

    invoke-interface {v2, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250206
    const-string v3, "height"

    iget v4, p0, LX/FUu;->e:I

    int-to-float v4, v4

    invoke-static {v4}, LX/5r2;->c(F)F

    move-result v4

    float-to-double v4, v4

    invoke-interface {v2, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250207
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v3

    .line 2250208
    const-string v4, "width"

    iget v5, p0, LX/FUu;->f:I

    int-to-float v5, v5

    invoke-static {v5}, LX/5r2;->c(F)F

    move-result v5

    float-to-double v6, v5

    invoke-interface {v3, v4, v6, v7}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250209
    const-string v4, "height"

    iget v5, p0, LX/FUu;->g:I

    int-to-float v5, v5

    invoke-static {v5}, LX/5r2;->c(F)F

    move-result v5

    float-to-double v6, v5

    invoke-interface {v3, v4, v6, v7}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2250210
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v4

    .line 2250211
    const-string v5, "contentInset"

    invoke-interface {v4, v5, v0}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2250212
    const-string v0, "contentOffset"

    invoke-interface {v4, v0, v1}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2250213
    const-string v0, "contentSize"

    invoke-interface {v4, v0, v2}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2250214
    const-string v0, "layoutMeasurement"

    invoke-interface {v4, v0, v3}, LX/5pH;->a(Ljava/lang/String;LX/5pH;)V

    .line 2250215
    const-string v0, "target"

    .line 2250216
    iget v1, p0, LX/5r0;->c:I

    move v1, v1

    .line 2250217
    invoke-interface {v4, v0, v1}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2250218
    const-string v0, "responderIgnoreScroll"

    const/4 v1, 0x1

    invoke-interface {v4, v0, v1}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    .line 2250219
    return-object v4
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2250194
    sget-object v0, LX/FUu;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2250195
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/events/RCTEventEmitter;)V
    .locals 3

    .prologue
    .line 2250191
    iget v0, p0, LX/5r0;->c:I

    move v0, v0

    .line 2250192
    invoke-virtual {p0}, LX/5r0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, LX/FUu;->j()LX/5pH;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2250193
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2250186
    iget-object v0, p0, LX/FUu;->h:LX/FUv;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FUv;

    invoke-virtual {v0}, LX/FUv;->getJSEventName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2250188
    iget-object v0, p0, LX/FUu;->h:LX/FUv;

    sget-object v1, LX/FUv;->SCROLL:LX/FUv;

    if-ne v0, v1, :cond_0

    .line 2250189
    const/4 v0, 0x1

    .line 2250190
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()S
    .locals 1

    .prologue
    .line 2250187
    const/4 v0, 0x0

    return v0
.end method
