.class public final enum LX/GCn;
.super LX/GCi;
.source ""


# direct methods
.method public constructor <init>(Ljava/lang/String;III)V
    .locals 6

    .prologue
    .line 2329192
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v4}, LX/GCi;-><init>(Ljava/lang/String;III)V

    return-void
.end method


# virtual methods
.method public final getUri(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2329193
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->o:Ljava/lang/String;

    move-object v0, v0

    .line 2329194
    return-object v0
.end method

.method public final isAvailable(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z
    .locals 2

    .prologue
    .line 2329195
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    move-object v0, v0

    .line 2329196
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2329197
    const/4 v0, 0x0

    .line 2329198
    :goto_0
    return v0

    .line 2329199
    :cond_0
    instance-of v0, p1, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    if-eqz v0, :cond_1

    .line 2329200
    check-cast p1, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2329201
    iget-boolean v0, p1, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;->b:Z

    move v0, v0

    .line 2329202
    goto :goto_0

    .line 2329203
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
