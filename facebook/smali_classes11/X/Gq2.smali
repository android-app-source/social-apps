.class public LX/Gq2;
.super LX/Cod;
.source ""

# interfaces
.implements LX/Gpv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/GpR;",
        ">;",
        "LX/Gpv;"
    }
.end annotation


# instance fields
.field public a:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/CIb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Lcom/facebook/widget/text/BetterTextView;

.field public final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/FrameLayout;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2396079
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396080
    const-class v0, LX/Gq2;

    invoke-static {v0, p0}, LX/Gq2;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396081
    const v0, 0x7f0d1887

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Gq2;->g:Landroid/widget/FrameLayout;

    .line 2396082
    const v0, 0x7f0d1889

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Gq2;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2396083
    const v0, 0x7f0d1888

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Gq2;->f:Landroid/widget/ImageView;

    .line 2396084
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gq2;

    invoke-static {p0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v1

    check-cast v1, LX/GnF;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {p0}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object p0

    check-cast p0, LX/CIb;

    iput-object v1, p1, LX/Gq2;->a:LX/GnF;

    iput-object v2, p1, LX/Gq2;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v3, p1, LX/Gq2;->c:LX/0ad;

    iput-object p0, p1, LX/Gq2;->d:LX/CIb;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2396085
    iget-object v0, p0, LX/Gq2;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2396086
    iget-object v0, p0, LX/Gq2;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2396087
    iget-object v0, p0, LX/Gq2;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2396088
    return-void
.end method

.method public final a(I)V
    .locals 5

    .prologue
    .line 2396089
    if-gtz p1, :cond_1

    .line 2396090
    iget-object v0, p0, LX/Gq2;->e:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2396091
    :cond_0
    :goto_0
    return-void

    .line 2396092
    :cond_1
    iget-object v0, p0, LX/Gq2;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2396093
    iget-object v1, p0, LX/Gq2;->e:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2396094
    iget-object v1, p0, LX/Gq2;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2396095
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2396096
    const/4 v4, 0x0

    const/high16 v3, 0x40400000    # 3.0f

    .line 2396097
    iget-object v0, p0, LX/Gq2;->c:LX/0ad;

    sget-short v1, LX/CHN;->a:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, LX/Gq2;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/Gn0;->b:LX/0Tn;

    const-string v2, "/"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/Gq2;->d:LX/CIb;

    .line 2396098
    iget-object p1, v2, LX/CIb;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2396099
    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v1, v0, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/Gq2;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2396100
    :cond_2
    :goto_1
    goto :goto_0

    .line 2396101
    :cond_3
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2396102
    iget-object v1, p0, LX/Gq2;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2396103
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2396104
    iput v1, v0, LX/0ht;->z:I

    .line 2396105
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2396106
    iput v1, v0, LX/0ht;->y:I

    .line 2396107
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2396108
    iget-object v1, p0, LX/Gq2;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2396109
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2396110
    iget-object v0, p0, LX/Gq2;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v0, LX/Gn0;->b:LX/0Tn;

    const-string v2, "/"

    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v2, p0, LX/Gq2;->d:LX/CIb;

    .line 2396111
    iget-object v3, v2, LX/CIb;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2396112
    invoke-virtual {v0, v2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_1
.end method

.method public final a(LX/CHX;)V
    .locals 2

    .prologue
    .line 2396113
    new-instance v0, LX/Gq1;

    invoke-direct {v0, p0, p1}, LX/Gq1;-><init>(LX/Gq2;LX/CHX;)V

    .line 2396114
    iget-object v1, p0, LX/Gq2;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396115
    iget-object v1, p0, LX/Gq2;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396116
    return-void
.end method
