.class public LX/H0x;
.super LX/H0w;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/H0w",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V
    .locals 1

    .prologue
    .line 2414604
    invoke-direct/range {p0 .. p6}, LX/H0w;-><init>(JLjava/lang/String;Ljava/lang/String;ILX/4gq;)V

    .line 2414605
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/H0x;->l:Z

    .line 2414606
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2414607
    const-string v0, "true"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414608
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2414609
    :goto_0
    return-object v0

    .line 2414610
    :cond_0
    const-string v0, "false"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2414611
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2414612
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/4gq;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2414613
    invoke-super {p0, p1}, LX/H0w;->a(LX/4gq;)V

    .line 2414614
    iget-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2414615
    iget-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-class v2, LX/H1O;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2414616
    :cond_0
    iget-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2414617
    iget-object v0, p0, LX/H0w;->m:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-class v2, LX/H1O;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2414618
    :cond_1
    return-void
.end method

.method public final b(LX/4gq;)Z
    .locals 2

    .prologue
    .line 2414619
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->c(J)Z

    move-result v0

    return v0
.end method

.method public final c(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414620
    iget-wide v0, p0, LX/H0w;->h:J

    .line 2414621
    invoke-virtual {p1, v0, v1}, LX/4gq;->c(J)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2414622
    invoke-virtual {p1, v0, v1}, LX/4gq;->d(J)Z

    move-result p0

    .line 2414623
    :goto_0
    move v0, p0

    .line 2414624
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p1, v0, v1}, LX/4gq;->e(J)Z

    move-result p0

    goto :goto_0
.end method

.method public final d(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414625
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->d(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414626
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-virtual {p1, v0, v1}, LX/4gq;->e(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414627
    iget-wide v0, p0, LX/H0w;->h:J

    .line 2414628
    iget-object p0, p1, LX/4gq;->c:LX/0W4;

    if-eqz p0, :cond_0

    .line 2414629
    iget-object p0, p1, LX/4gq;->c:LX/0W4;

    invoke-interface {p0, v0, v1}, LX/0W4;->b(J)Z

    move-result p0

    .line 2414630
    :goto_0
    move v0, p0

    .line 2414631
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {v0, v1}, LX/0ok;->a(J)Z

    move-result p0

    goto :goto_0
.end method

.method public final g(LX/4gq;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2414632
    iget-wide v0, p0, LX/H0w;->h:J

    invoke-static {v0, v1}, LX/0ok;->a(J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
