.class public final LX/Fkz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V
    .locals 0

    .prologue
    .line 2279284
    iput-object p1, p0, LX/Fkz;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x299ec4c5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2279285
    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2279286
    const/16 v1, 0x27

    const v2, 0x3ae16f6f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2279287
    :goto_0
    return-void

    .line 2279288
    :cond_0
    const-string v1, "extra_result"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7m7;->valueOf(Ljava/lang/String;)LX/7m7;

    move-result-object v1

    .line 2279289
    sget-object v2, LX/7m7;->SUCCESS:LX/7m7;

    if-ne v1, v2, :cond_1

    .line 2279290
    iget-object v1, p0, LX/Fkz;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    invoke-virtual {v1}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->e()V

    .line 2279291
    :cond_1
    const v1, 0x341dd21c

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
