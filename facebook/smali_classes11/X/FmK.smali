.class public LX/FmK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ds;


# instance fields
.field private final a:LX/6tK;


# direct methods
.method public constructor <init>(LX/6tK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2282314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282315
    iput-object p1, p0, LX/FmK;->a:LX/6tK;

    .line 2282316
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            ")",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2282326
    iget-object v0, p0, LX/FmK;->a:LX/6tK;

    invoke-virtual {v0, p1}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/payments/checkout/model/CheckoutData;",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/6E3;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2282325
    iget-object v0, p0, LX/FmK;->a:LX/6tK;

    invoke-virtual {v0, p1, p2}, LX/6tK;->a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2282317
    sget-object v0, LX/6so;->PRIVACY_SELECTOR:LX/6so;

    if-ne p1, v0, :cond_1

    .line 2282318
    invoke-interface {p2}, Lcom/facebook/payments/checkout/model/CheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    .line 2282319
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2282320
    :goto_0
    return-object v0

    .line 2282321
    :cond_0
    new-instance v1, LX/FmD;

    .line 2282322
    iget-object p0, v0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, p0

    .line 2282323
    invoke-direct {v1, v0}, LX/FmD;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    move-object v0, v1

    goto :goto_0

    .line 2282324
    :cond_1
    iget-object v0, p0, LX/FmK;->a:LX/6tK;

    invoke-virtual {v0, p1, p2}, LX/6tK;->a(LX/6so;Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6E3;

    move-result-object v0

    goto :goto_0
.end method
