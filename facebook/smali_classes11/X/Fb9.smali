.class public LX/Fb9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field private final b:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Rl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/0Rl",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2259641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259642
    iput-object p1, p0, LX/Fb9;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2259643
    iput-object p2, p0, LX/Fb9;->b:LX/0Rl;

    .line 2259644
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2259645
    if-ne p0, p1, :cond_1

    .line 2259646
    :cond_0
    :goto_0
    return v0

    .line 2259647
    :cond_1
    instance-of v2, p1, LX/Fb9;

    if-nez v2, :cond_2

    move v0, v1

    .line 2259648
    goto :goto_0

    .line 2259649
    :cond_2
    check-cast p1, LX/Fb9;

    .line 2259650
    iget-object v2, p0, LX/Fb9;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v3, p1, LX/Fb9;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LX/Fb9;->b:LX/0Rl;

    iget-object v3, p1, LX/Fb9;->b:LX/0Rl;

    invoke-interface {v2, v3}, LX/0Rl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2259651
    iget-object v0, p0, LX/Fb9;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->hashCode()I

    move-result v0

    iget-object v1, p0, LX/Fb9;->b:LX/0Rl;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method
