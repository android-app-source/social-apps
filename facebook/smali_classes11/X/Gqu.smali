.class public LX/Gqu;
.super LX/Cqx;
.source ""


# instance fields
.field public a:I

.field public b:Z


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 1

    .prologue
    .line 2397044
    invoke-direct {p0, p1, p2}, LX/Cqx;-><init>(LX/Ctg;LX/CrK;)V

    .line 2397045
    const/4 v0, 0x1

    iput v0, p0, LX/Gqu;->a:I

    .line 2397046
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gqu;->b:Z

    .line 2397047
    return-void
.end method


# virtual methods
.method public final k()Z
    .locals 1

    .prologue
    .line 2397043
    iget-boolean v0, p0, LX/Gqu;->b:Z

    return v0
.end method

.method public final l()V
    .locals 5

    .prologue
    .line 2397029
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2397030
    const/4 v1, 0x0

    .line 2397031
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, LX/Ctg;

    if-eqz v2, :cond_0

    .line 2397032
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/Ctg;

    invoke-interface {v1}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v1

    .line 2397033
    :cond_0
    instance-of v2, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_1

    .line 2397034
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2397035
    :cond_1
    iget v2, p0, LX/Gqu;->a:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 2397036
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 2397037
    if-eqz v1, :cond_3

    .line 2397038
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-interface {v1}, LX/Ct1;->getMediaAspectRatio()F

    move-result v1

    div-float v1, v4, v1

    int-to-float v3, v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    float-to-int v1, v1

    .line 2397039
    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    iget v4, p0, LX/Gqu;->a:I

    div-int/2addr v3, v4

    sub-int v2, v3, v2

    invoke-virtual {p0, v2, v1}, LX/Cqi;->a(II)V

    .line 2397040
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0, v1}, LX/Cqi;->b(II)V

    .line 2397041
    return-void

    .line 2397042
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1
.end method
