.class public abstract LX/FbI;
.super LX/FbH;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2259844
    invoke-direct {p0}, LX/FbH;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public final b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/8dO;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2259845
    invoke-static {p1}, LX/Fbf;->c(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;

    move-result-object v4

    .line 2259846
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2259847
    :goto_0
    return-object v0

    .line 2259848
    :cond_0
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2259849
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;

    .line 2259850
    invoke-virtual {p0, v0}, LX/FbI;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;

    move-result-object v7

    .line 2259851
    if-eqz v7, :cond_1

    .line 2259852
    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2259853
    iput-object v0, v7, LX/8dV;->a:Ljava/lang/String;

    .line 2259854
    move-object v0, v7

    .line 2259855
    invoke-virtual {v0}, LX/8dV;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2259856
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2259857
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2259858
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v0, v1

    .line 2259859
    goto :goto_0

    .line 2259860
    :cond_3
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->l()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 2259861
    new-instance v1, LX/8dO;

    invoke-direct {v1}, LX/8dO;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v2

    .line 2259862
    iput-object v2, v1, LX/8dO;->i:LX/0Px;

    .line 2259863
    move-object v1, v1

    .line 2259864
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2259865
    iput-object v2, v1, LX/8dO;->c:Ljava/lang/String;

    .line 2259866
    move-object v1, v1

    .line 2259867
    new-instance v2, LX/8dQ;

    invoke-direct {v2}, LX/8dQ;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    .line 2259868
    iput-object v3, v2, LX/8dQ;->Z:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2259869
    move-object v2, v2

    .line 2259870
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v3

    .line 2259871
    iput-object v3, v2, LX/8dQ;->q:LX/0Px;

    .line 2259872
    move-object v2, v2

    .line 2259873
    iput-object v0, v2, LX/8dQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2259874
    move-object v0, v2

    .line 2259875
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->q()Ljava/lang/String;

    move-result-object v2

    .line 2259876
    iput-object v2, v0, LX/8dQ;->ab:Ljava/lang/String;

    .line 2259877
    move-object v0, v0

    .line 2259878
    new-instance v2, LX/8dU;

    invoke-direct {v2}, LX/8dU;-><init>()V

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2259879
    iput-object v3, v2, LX/8dU;->a:LX/0Px;

    .line 2259880
    move-object v2, v2

    .line 2259881
    invoke-virtual {v2}, LX/8dU;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v2

    .line 2259882
    iput-object v2, v0, LX/8dQ;->Y:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 2259883
    move-object v0, v0

    .line 2259884
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->o()LX/8dH;

    move-result-object v2

    invoke-static {v2}, LX/A0T;->a(LX/8dH;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v2

    .line 2259885
    iput-object v2, v0, LX/8dQ;->av:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    .line 2259886
    move-object v0, v0

    .line 2259887
    invoke-virtual {v0}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 2259888
    iput-object v0, v1, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2259889
    move-object v0, v1

    .line 2259890
    goto/16 :goto_0
.end method
