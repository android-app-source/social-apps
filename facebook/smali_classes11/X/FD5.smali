.class public final LX/FD5;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessUserHasMessagedQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2210844
    const-class v1, Lcom/facebook/messaging/business/pages/graphql/BusinessMessagingQueriesModels$BusinessUserHasMessagedQueryModel;

    const v0, 0xe975a42

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "BusinessUserHasMessagedQuery"

    const-string v6, "5c248c4bdf2be4dfce7967b396cb958a"

    const-string v7, "viewer"

    const-string v8, "10155069963096729"

    const-string v9, "10155259086226729"

    .line 2210845
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2210846
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2210847
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2210848
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2210849
    sparse-switch v0, :sswitch_data_0

    .line 2210850
    :goto_0
    return-object p1

    .line 2210851
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2210852
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2210853
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2210854
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x352efdb4 -> :sswitch_0
        -0x132889c -> :sswitch_3
        0x2f1911b0 -> :sswitch_1
        0x3349e8c0 -> :sswitch_2
    .end sparse-switch
.end method
