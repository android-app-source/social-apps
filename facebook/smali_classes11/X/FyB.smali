.class public final LX/FyB;
.super LX/BPf;
.source ""


# instance fields
.field public final synthetic a:LX/FyD;


# direct methods
.method public constructor <init>(LX/FyD;)V
    .locals 0

    .prologue
    .line 2306388
    iput-object p1, p0, LX/FyB;->a:LX/FyD;

    invoke-direct {p0}, LX/BPf;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2306389
    check-cast p1, LX/BPe;

    .line 2306390
    iget-object v0, p0, LX/FyB;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->b:LX/Fy4;

    if-eqz v0, :cond_0

    .line 2306391
    iget-object v0, p0, LX/FyB;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->b:LX/Fy4;

    iget-wide v2, p1, LX/BPe;->a:J

    sget-object v1, LX/2h9;->PROFILE:LX/2h9;

    .line 2306392
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 p0, 0x1

    invoke-static {v0, v2, v3, v4, p0}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306393
    iget-object v4, v0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-virtual {v4, v2, v3, v1}, LX/2dj;->a(JLX/2h9;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2306394
    new-instance p0, LX/Fxz;

    invoke-direct {p0, v0, v2, v3}, LX/Fxz;-><init>(LX/Fy4;J)V

    iget-object p1, v0, LX/Fy4;->i:Ljava/util/concurrent/Executor;

    invoke-static {v4, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2306395
    :cond_0
    return-void
.end method
