.class public final LX/Fs7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "LX/FsL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G12;

.field public final synthetic b:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic c:LX/FrR;

.field public final synthetic d:LX/4VB;

.field public final synthetic e:LX/4VB;

.field public final synthetic f:LX/4VB;

.field public final synthetic g:LX/4VB;

.field public final synthetic h:LX/4VB;

.field public final synthetic i:LX/4VB;

.field public final synthetic j:LX/4VB;

.field public final synthetic k:LX/4VB;

.field public final synthetic l:LX/4VB;


# direct methods
.method public constructor <init>(LX/G12;Lcom/facebook/common/callercontext/CallerContext;LX/FrR;LX/4VB;LX/4VB;LX/4VB;LX/4VB;LX/4VB;LX/4VB;LX/4VB;LX/4VB;LX/4VB;)V
    .locals 0

    .prologue
    .line 2296618
    iput-object p1, p0, LX/Fs7;->a:LX/G12;

    iput-object p2, p0, LX/Fs7;->b:Lcom/facebook/common/callercontext/CallerContext;

    iput-object p3, p0, LX/Fs7;->c:LX/FrR;

    iput-object p4, p0, LX/Fs7;->d:LX/4VB;

    iput-object p5, p0, LX/Fs7;->e:LX/4VB;

    iput-object p6, p0, LX/Fs7;->f:LX/4VB;

    iput-object p7, p0, LX/Fs7;->g:LX/4VB;

    iput-object p8, p0, LX/Fs7;->h:LX/4VB;

    iput-object p9, p0, LX/Fs7;->i:LX/4VB;

    iput-object p10, p0, LX/Fs7;->j:LX/4VB;

    iput-object p11, p0, LX/Fs7;->k:LX/4VB;

    iput-object p12, p0, LX/Fs7;->l:LX/4VB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2296589
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 2296591
    check-cast p1, LX/FsL;

    .line 2296592
    iget-object v0, p1, LX/FsL;->b:LX/0ta;

    iget-object v1, p0, LX/Fs7;->a:LX/G12;

    iget-object v2, p0, LX/Fs7;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p0, LX/Fs7;->c:LX/FrR;

    iget-object v4, p0, LX/Fs7;->d:LX/4VB;

    iget-object v5, p0, LX/Fs7;->e:LX/4VB;

    iget-object v6, p0, LX/Fs7;->f:LX/4VB;

    iget-object v7, p0, LX/Fs7;->g:LX/4VB;

    iget-object v8, p0, LX/Fs7;->h:LX/4VB;

    iget-object v9, p0, LX/Fs7;->i:LX/4VB;

    iget-object v10, p0, LX/Fs7;->j:LX/4VB;

    iget-object v11, p0, LX/Fs7;->k:LX/4VB;

    iget-object v12, p0, LX/Fs7;->l:LX/4VB;

    .line 2296593
    sget-object p0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, p0, :cond_5

    .line 2296594
    const/4 p0, 0x1

    invoke-interface {v3, p0, v1, v2}, LX/FrR;->a(ZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object p0

    .line 2296595
    iget-object p1, p0, LX/FsJ;->a:LX/0zX;

    if-eqz p1, :cond_0

    .line 2296596
    iget-object p1, p0, LX/FsJ;->a:LX/0zX;

    invoke-virtual {p1, v4}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 2296597
    :cond_0
    iget-object p1, p0, LX/FsJ;->b:LX/0zX;

    if-eqz p1, :cond_1

    .line 2296598
    iget-object p1, p0, LX/FsJ;->b:LX/0zX;

    invoke-static {p1, v5}, LX/Fs8;->a(LX/0zX;LX/4VB;)V

    .line 2296599
    :cond_1
    iget-object p1, p0, LX/FsJ;->c:LX/0zX;

    if-eqz p1, :cond_2

    .line 2296600
    iget-object p1, p0, LX/FsJ;->c:LX/0zX;

    invoke-static {p1, v6}, LX/Fs8;->a(LX/0zX;LX/4VB;)V

    .line 2296601
    :cond_2
    iget-object p1, p0, LX/FsJ;->e:LX/0zX;

    invoke-static {p1, v7}, LX/Fs8;->a(LX/0zX;LX/4VB;)V

    .line 2296602
    iget-object p1, p0, LX/FsJ;->d:LX/0zX;

    invoke-static {p1, v8}, LX/Fs8;->a(LX/0zX;LX/4VB;)V

    .line 2296603
    iget-object p1, p0, LX/FsJ;->f:LX/0zX;

    invoke-static {p1, v9}, LX/Fs8;->a(LX/0zX;LX/4VB;)V

    .line 2296604
    iget-object p1, p0, LX/FsJ;->g:LX/0zX;

    if-eqz p1, :cond_3

    .line 2296605
    iget-object p1, p0, LX/FsJ;->g:LX/0zX;

    invoke-static {p1, v10}, LX/Fs8;->a(LX/0zX;LX/4VB;)V

    .line 2296606
    :cond_3
    iget-object p1, p0, LX/FsJ;->i:LX/0zX;

    if-eqz p1, :cond_4

    .line 2296607
    iget-object p0, p0, LX/FsJ;->i:LX/0zX;

    invoke-static {p0, v12}, LX/Fs8;->a(LX/0zX;LX/4VB;)V

    .line 2296608
    :cond_4
    :goto_0
    return-void

    .line 2296609
    :cond_5
    invoke-virtual {v4}, LX/4VB;->a()V

    .line 2296610
    invoke-virtual {v5}, LX/4VB;->a()V

    .line 2296611
    invoke-virtual {v6}, LX/4VB;->a()V

    .line 2296612
    invoke-virtual {v7}, LX/4VB;->a()V

    .line 2296613
    invoke-virtual {v8}, LX/4VB;->a()V

    .line 2296614
    invoke-virtual {v9}, LX/4VB;->a()V

    .line 2296615
    invoke-virtual {v10}, LX/4VB;->a()V

    .line 2296616
    invoke-virtual {v11}, LX/4VB;->a()V

    .line 2296617
    invoke-virtual {v12}, LX/4VB;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2296590
    return-void
.end method
