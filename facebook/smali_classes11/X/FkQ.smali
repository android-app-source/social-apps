.class public LX/FkQ;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FkQ;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0ad;LX/0Zb;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278558
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2278559
    iput-object p1, p0, LX/FkQ;->a:LX/0ad;

    .line 2278560
    iput-object p2, p0, LX/FkQ;->b:LX/0Zb;

    .line 2278561
    sget-object v0, LX/0ax;->gL:Ljava/lang/String;

    const-string v1, "{fundraiser_campaign_id}"

    const-string v2, "{post_id}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->FUNDRAISER_PAGE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2278562
    sget-object v0, LX/0ax;->gM:Ljava/lang/String;

    const-string v1, "{fundraiser_campaign_id}"

    const-string v2, "{source}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->FUNDRAISER_PAGE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2278563
    sget-object v0, LX/0ax;->gT:Ljava/lang/String;

    const-string v1, "{fundraiser_campaign_id}"

    const-string v2, "{action_type}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->FUNDRAISER_PAGE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2278564
    return-void
.end method

.method public static a(LX/0QB;)LX/FkQ;
    .locals 5

    .prologue
    .line 2278534
    sget-object v0, LX/FkQ;->c:LX/FkQ;

    if-nez v0, :cond_1

    .line 2278535
    const-class v1, LX/FkQ;

    monitor-enter v1

    .line 2278536
    :try_start_0
    sget-object v0, LX/FkQ;->c:LX/FkQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2278537
    if-eqz v2, :cond_0

    .line 2278538
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2278539
    new-instance p0, LX/FkQ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/FkQ;-><init>(LX/0ad;LX/0Zb;)V

    .line 2278540
    move-object v0, p0

    .line 2278541
    sput-object v0, LX/FkQ;->c:LX/FkQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2278542
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2278543
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2278544
    :cond_1
    sget-object v0, LX/FkQ;->c:LX/FkQ;

    return-object v0

    .line 2278545
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2278546
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2278548
    invoke-super {p0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2278549
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/FkQ;->a:LX/0ad;

    sget-short v2, LX/FkF;->c:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2278550
    :goto_0
    return-object v0

    .line 2278551
    :cond_0
    iget-object v0, p0, LX/FkQ;->b:LX/0Zb;

    .line 2278552
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "fundraiser_page_webview_fallback"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "fundraiser_page"

    .line 2278553
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2278554
    move-object v1, v1

    .line 2278555
    const-string v2, "uri"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    move-object v1, v1

    .line 2278556
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2278557
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2278547
    const/4 v0, 0x1

    return v0
.end method
