.class public LX/FCB;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/FC6;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/graphics/PointF;

.field private final c:Landroid/graphics/PointF;

.field private final d:Landroid/graphics/PointF;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FC6;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FC6;",
            ">;"
        }
    .end annotation
.end field

.field private final g:J

.field private final h:LX/FJ5;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/FC9;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/FCC;

.field public final k:Z

.field private l:J

.field private m:F

.field private n:F

.field private o:F

.field private p:F

.field private q:F

.field public r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2210124
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2210125
    sput-object v0, LX/FCB;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;JILX/FJ5;LX/FCC;FZLjava/util/List;)V
    .locals 13
    .param p10    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            "JI",
            "Lcom/facebook/kenburns/KenBurnsAnimationDelegate;",
            "Lcom/facebook/kenburns/ImageAnimationInterpolator;",
            "FZ",
            "Ljava/util/List",
            "<",
            "LX/FC6;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2210126
    const/4 v12, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide/from16 v4, p3

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v12}, LX/FCB;-><init>(Landroid/graphics/PointF;Landroid/graphics/PointF;JILX/FJ5;LX/FCC;FZLjava/util/List;Z)V

    .line 2210127
    return-void
.end method

.method private constructor <init>(Landroid/graphics/PointF;Landroid/graphics/PointF;JILX/FJ5;LX/FCC;FZLjava/util/List;Z)V
    .locals 7
    .param p10    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            "JI",
            "Lcom/facebook/kenburns/KenBurnsAnimationDelegate;",
            "Lcom/facebook/kenburns/ImageAnimationInterpolator;",
            "FZ",
            "Ljava/util/List",
            "<",
            "LX/FC6;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 2210128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210129
    move/from16 v0, p11

    iput-boolean v0, p0, LX/FCB;->r:Z

    .line 2210130
    iput-object p1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    .line 2210131
    iput-object p2, p0, LX/FCB;->c:Landroid/graphics/PointF;

    .line 2210132
    new-instance v2, Landroid/graphics/PointF;

    iget v3, p2, Landroid/graphics/PointF;->x:F

    div-float/2addr v3, v5

    iget v4, p2, Landroid/graphics/PointF;->y:F

    div-float/2addr v4, v5

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, LX/FCB;->d:Landroid/graphics/PointF;

    .line 2210133
    iput-wide p3, p0, LX/FCB;->g:J

    .line 2210134
    iput-object p6, p0, LX/FCB;->h:LX/FJ5;

    .line 2210135
    iput p8, p0, LX/FCB;->n:F

    .line 2210136
    if-nez p10, :cond_0

    sget-object p10, LX/FCB;->a:LX/0Px;

    :cond_0
    move-object/from16 v0, p10

    iput-object v0, p0, LX/FCB;->e:Ljava/util/List;

    .line 2210137
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LX/FCB;->f:Ljava/util/List;

    .line 2210138
    iput-object p7, p0, LX/FCB;->j:LX/FCC;

    .line 2210139
    int-to-float v2, p5

    iput v2, p0, LX/FCB;->m:F

    .line 2210140
    move/from16 v0, p9

    iput-boolean v0, p0, LX/FCB;->k:Z

    .line 2210141
    invoke-direct {p0}, LX/FCB;->a()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, LX/FCB;->i:Ljava/util/List;

    .line 2210142
    const-wide/16 v2, -0x1

    iput-wide v2, p0, LX/FCB;->l:J

    .line 2210143
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3}, LX/FCB;->a(J)V

    .line 2210144
    return-void
.end method

.method private static a(FFF)F
    .locals 1

    .prologue
    .line 2210157
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method public static a(LX/FCB;F)F
    .locals 2

    .prologue
    .line 2210145
    iget-object v0, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sub-float v0, p1, v0

    return v0
.end method

.method public static a(LX/FCB;FF)F
    .locals 8

    .prologue
    .line 2210146
    iget-object v0, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, LX/FCB;->c:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, LX/FCB;->c:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move v4, p1

    move v5, p2

    const/high16 p1, 0x40000000    # 2.0f

    .line 2210147
    div-float p0, v2, v3

    .line 2210148
    mul-float v6, v4, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    sub-float v7, v0, v6

    .line 2210149
    mul-float v6, v5, p1

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    sub-float v6, v1, v6

    .line 2210150
    div-float p1, v7, v6

    .line 2210151
    cmpg-float p2, p0, p1

    if-gez p2, :cond_0

    .line 2210152
    :goto_0
    cmpg-float p0, p0, p1

    if-gez p0, :cond_1

    .line 2210153
    :goto_1
    div-float v6, v3, v6

    move v0, v6

    .line 2210154
    return v0

    :cond_0
    move v3, v2

    .line 2210155
    goto :goto_0

    :cond_1
    move v6, v7

    .line 2210156
    goto :goto_1
.end method

.method private static a(LX/FCB;LX/FC6;)LX/FC9;
    .locals 4

    .prologue
    .line 2210056
    iget v0, p1, LX/FC6;->a:F

    invoke-direct {p0}, LX/FCB;->d()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-direct {p0}, LX/FCB;->c()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2210057
    iget v1, p1, LX/FC6;->b:F

    invoke-direct {p0}, LX/FCB;->f()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-direct {p0}, LX/FCB;->e()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 2210058
    invoke-static {p0, v0}, LX/FCB;->a(LX/FCB;F)F

    move-result v2

    invoke-static {p0, v1}, LX/FCB;->b(LX/FCB;F)F

    move-result v3

    invoke-static {p0, v2, v3}, LX/FCB;->a(LX/FCB;FF)F

    move-result v2

    .line 2210059
    iget v3, p1, LX/FC6;->c:F

    mul-float/2addr v2, v3

    .line 2210060
    new-instance v3, LX/FC9;

    invoke-direct {v3, p0, v0, v1, v2}, LX/FC9;-><init>(LX/FCB;FFF)V

    return-object v3
.end method

.method public static a(LX/FCB;LX/FCA;LX/FC9;F)LX/FC9;
    .locals 6

    .prologue
    .line 2210061
    sget-object v0, LX/FCA;->LEFT_OF:LX/FCA;

    if-ne p1, v0, :cond_0

    .line 2210062
    iget v0, p2, LX/FC9;->a:F

    sub-float/2addr v0, p3

    invoke-direct {p0}, LX/FCB;->d()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2210063
    :goto_0
    iget v1, p2, LX/FC9;->b:F

    sub-float/2addr v1, p3

    invoke-direct {p0}, LX/FCB;->f()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 2210064
    iget v2, p2, LX/FC9;->b:F

    add-float/2addr v2, p3

    invoke-direct {p0}, LX/FCB;->e()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 2210065
    iget-boolean v4, p0, LX/FCB;->r:Z

    if-eqz v4, :cond_1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    double-to-float v4, v4

    sub-float v5, v2, v1

    mul-float/2addr v4, v5

    :goto_1
    add-float/2addr v4, v1

    move v1, v4

    .line 2210066
    invoke-static {p0, v0}, LX/FCB;->a(LX/FCB;F)F

    move-result v2

    invoke-static {p0, v1}, LX/FCB;->b(LX/FCB;F)F

    move-result v3

    invoke-static {p0, v2, v3}, LX/FCB;->a(LX/FCB;FF)F

    move-result v2

    .line 2210067
    new-instance v3, LX/FC9;

    invoke-direct {v3, p0, v0, v1, v2}, LX/FC9;-><init>(LX/FCB;FFF)V

    return-object v3

    .line 2210068
    :cond_0
    iget v0, p2, LX/FC9;->a:F

    add-float/2addr v0, p3

    invoke-direct {p0}, LX/FCB;->c()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0

    :cond_1
    sub-float v4, v2, v1

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    goto :goto_1
.end method

.method private a()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/FC9;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x3fc00000    # 1.5f

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 2210069
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2210070
    iget-object v0, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    div-float/2addr v0, v1

    .line 2210071
    const v1, 0x3f8ccccd    # 1.1f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    const v1, 0x3f666666    # 0.9f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 2210072
    :goto_0
    iget-object v0, p0, LX/FCB;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2210073
    iget-object v0, p0, LX/FCB;->f:Ljava/util/List;

    iget-object v1, p0, LX/FCB;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2210074
    iget-object v0, p0, LX/FCB;->f:Ljava/util/List;

    new-instance v1, LX/FC7;

    invoke-direct {v1, p0}, LX/FC7;-><init>(LX/FCB;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2210075
    iget-object v0, p0, LX/FCB;->f:Ljava/util/List;

    iget-object v1, p0, LX/FCB;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    shr-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FC6;

    iget v4, v0, LX/FC6;->b:F

    .line 2210076
    iget-object v0, p0, LX/FCB;->f:Ljava/util/List;

    new-instance v1, LX/FC8;

    invoke-direct {v1, p0}, LX/FC8;-><init>(LX/FCB;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2210077
    iget-object v0, p0, LX/FCB;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FC6;

    .line 2210078
    new-instance v1, LX/FC6;

    iget v2, v0, LX/FC6;->a:F

    iget v5, v0, LX/FC6;->c:F

    invoke-direct {v1, v2, v4, v5}, LX/FC6;-><init>(FFF)V

    .line 2210079
    invoke-static {p0, v1}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v2

    .line 2210080
    iget-object v1, p0, LX/FCB;->f:Ljava/util/List;

    iget-object v5, p0, LX/FCB;->f:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FC6;

    .line 2210081
    new-instance v5, LX/FC6;

    iget v1, v1, LX/FC6;->a:F

    iget v0, v0, LX/FC6;->c:F

    invoke-direct {v5, v1, v4, v0}, LX/FC6;-><init>(FFF)V

    .line 2210082
    invoke-static {p0, v5}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v0

    .line 2210083
    iget-object v1, p0, LX/FCB;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    move-object v1, v2

    .line 2210084
    :goto_1
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2210085
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2210086
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_0
    move v1, v2

    .line 2210087
    goto :goto_0

    .line 2210088
    :pswitch_0
    if-eqz v1, :cond_1

    .line 2210089
    new-instance v0, LX/FC6;

    iget-object v1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    div-float/2addr v1, v7

    iget-object v2, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    div-float/2addr v2, v7

    invoke-direct {v0, v1, v2, v6}, LX/FC6;-><init>(FFF)V

    invoke-static {p0, v0}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v1

    .line 2210090
    invoke-static {p0}, LX/FCB;->b(LX/FCB;)LX/FC9;

    move-result-object v0

    goto :goto_1

    .line 2210091
    :cond_1
    new-instance v1, LX/FC9;

    invoke-direct {v1, p0, v4, v4, v5}, LX/FC9;-><init>(LX/FCB;FFF)V

    .line 2210092
    new-instance v0, LX/FC9;

    invoke-direct {v0, p0, v4, v4, v5}, LX/FC9;-><init>(LX/FCB;FFF)V

    .line 2210093
    iget v2, p0, LX/FCB;->m:F

    .line 2210094
    invoke-static {p0}, LX/FCB;->b(LX/FCB;)LX/FC9;

    move-result-object v5

    .line 2210095
    sget-object v4, LX/FCA;->LEFT_OF:LX/FCA;

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v2, v6

    invoke-static {p0, v4, v5, v6}, LX/FCB;->a(LX/FCB;LX/FCA;LX/FC9;F)LX/FC9;

    move-result-object v6

    .line 2210096
    iget v4, v6, LX/FC9;->a:F

    iput v4, v1, LX/FC9;->a:F

    .line 2210097
    iget v4, v6, LX/FC9;->b:F

    iput v4, v1, LX/FC9;->b:F

    .line 2210098
    iget-boolean v4, p0, LX/FCB;->k:Z

    if-eqz v4, :cond_4

    const/high16 v4, 0x3f800000    # 1.0f

    .line 2210099
    :goto_2
    iget v6, v6, LX/FC9;->c:F

    mul-float/2addr v4, v6

    iput v4, v1, LX/FC9;->c:F

    .line 2210100
    iget v4, v5, LX/FC9;->a:F

    iget v6, v1, LX/FC9;->a:F

    sub-float/2addr v4, v6

    .line 2210101
    iget v6, v5, LX/FC9;->b:F

    iget v7, v1, LX/FC9;->b:F

    sub-float/2addr v6, v7

    .line 2210102
    iget v7, v5, LX/FC9;->a:F

    add-float/2addr v4, v7

    .line 2210103
    iget v5, v5, LX/FC9;->b:F

    add-float/2addr v5, v6

    .line 2210104
    invoke-static {p0, v4}, LX/FCB;->a(LX/FCB;F)F

    move-result v6

    invoke-static {p0, v5}, LX/FCB;->b(LX/FCB;F)F

    move-result v7

    invoke-static {p0, v6, v7}, LX/FCB;->a(LX/FCB;FF)F

    move-result v6

    .line 2210105
    iput v4, v0, LX/FC9;->a:F

    .line 2210106
    iput v5, v0, LX/FC9;->b:F

    .line 2210107
    iput v6, v0, LX/FC9;->c:F

    .line 2210108
    goto :goto_1

    .line 2210109
    :pswitch_1
    iget-object v0, p0, LX/FCB;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FC6;

    .line 2210110
    if-eqz v1, :cond_2

    .line 2210111
    iget v1, v0, LX/FC6;->c:F

    invoke-static {v6, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, v0, LX/FC6;->c:F

    .line 2210112
    invoke-static {p0, v0}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v1

    .line 2210113
    invoke-static {p0}, LX/FCB;->b(LX/FCB;)LX/FC9;

    move-result-object v0

    goto/16 :goto_1

    .line 2210114
    :cond_2
    invoke-static {p0, v0}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v1

    .line 2210115
    sget-object v0, LX/FCA;->RIGHT_OF:LX/FCA;

    const/high16 v2, 0x42480000    # 50.0f

    invoke-static {p0, v0, v1, v2}, LX/FCB;->a(LX/FCB;LX/FCA;LX/FC9;F)LX/FC9;

    move-result-object v0

    goto/16 :goto_1

    .line 2210116
    :pswitch_2
    iget-object v0, p0, LX/FCB;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FC6;

    .line 2210117
    iget-object v1, p0, LX/FCB;->e:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FC6;

    .line 2210118
    iget v2, v0, LX/FC6;->a:F

    iget v4, v1, LX/FC6;->a:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_3

    .line 2210119
    invoke-static {p0, v0}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v2

    .line 2210120
    invoke-static {p0, v1}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v0

    move-object v1, v2

    goto/16 :goto_1

    .line 2210121
    :cond_3
    invoke-static {p0, v1}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v1

    .line 2210122
    invoke-static {p0, v0}, LX/FCB;->a(LX/FCB;LX/FC6;)LX/FC9;

    move-result-object v0

    goto/16 :goto_1

    .line 2210123
    :cond_4
    const v4, 0x3faccccd    # 1.35f

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/FCB;F)F
    .locals 2

    .prologue
    .line 2210031
    iget-object v0, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sub-float v0, p1, v0

    return v0
.end method

.method public static b(LX/FCB;)LX/FC9;
    .locals 4

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 2210032
    iget-object v0, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    div-float/2addr v0, v2

    .line 2210033
    iget-object v1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    div-float/2addr v1, v2

    .line 2210034
    invoke-static {p0, v0}, LX/FCB;->a(LX/FCB;F)F

    move-result v2

    invoke-static {p0, v1}, LX/FCB;->b(LX/FCB;F)F

    move-result v3

    invoke-static {p0, v2, v3}, LX/FCB;->a(LX/FCB;FF)F

    move-result v2

    .line 2210035
    new-instance v3, LX/FC9;

    invoke-direct {v3, p0, v0, v1, v2}, LX/FC9;-><init>(LX/FCB;FFF)V

    return-object v3
.end method

.method private c()F
    .locals 3

    .prologue
    .line 2210036
    iget-object v0, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-direct {p0}, LX/FCB;->d()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private d()F
    .locals 3

    .prologue
    .line 2210037
    iget-object v0, p0, LX/FCB;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget v1, p0, LX/FCB;->n:F

    div-float/2addr v0, v1

    iget-object v1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private e()F
    .locals 3

    .prologue
    .line 2210038
    iget-object v0, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {p0}, LX/FCB;->f()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private f()F
    .locals 3

    .prologue
    .line 2210039
    iget-object v0, p0, LX/FCB;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v1, p0, LX/FCB;->n:F

    div-float/2addr v0, v1

    iget-object v1, p0, LX/FCB;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(J)V
    .locals 11

    .prologue
    .line 2210040
    iget-wide v0, p0, LX/FCB;->l:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 2210041
    :goto_0
    return-void

    .line 2210042
    :cond_0
    iput-wide p1, p0, LX/FCB;->l:J

    .line 2210043
    iget-wide v2, p0, LX/FCB;->l:J

    long-to-float v1, v2

    iget-wide v2, p0, LX/FCB;->g:J

    long-to-float v2, v2

    div-float/2addr v1, v2

    .line 2210044
    float-to-double v7, v1

    invoke-static {v7, v8}, Ljava/lang/Math;->floor(D)D

    move-result-wide v7

    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    rem-double/2addr v7, v9

    const/high16 v9, 0x3f800000    # 1.0f

    rem-float v9, v1, v9

    float-to-double v9, v9

    sub-double/2addr v7, v9

    const-wide v9, 0x408f400000000000L    # 1000.0

    mul-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    long-to-float v7, v7

    const/high16 v8, 0x447a0000    # 1000.0f

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    move v4, v7

    .line 2210045
    iget-object v0, p0, LX/FCB;->i:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FC9;

    .line 2210046
    iget-object v1, p0, LX/FCB;->i:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, LX/FC9;

    .line 2210047
    iget v1, v0, LX/FC9;->a:F

    iget v2, v3, LX/FC9;->a:F

    invoke-static {v1, v2, v4}, LX/FCB;->a(FFF)F

    move-result v1

    .line 2210048
    iget v2, v0, LX/FC9;->b:F

    iget v5, v3, LX/FC9;->b:F

    invoke-static {v2, v5, v4}, LX/FCB;->a(FFF)F

    move-result v2

    .line 2210049
    iget v0, v0, LX/FC9;->c:F

    iget v3, v3, LX/FC9;->c:F

    invoke-static {v0, v3, v4}, LX/FCB;->a(FFF)F

    move-result v3

    .line 2210050
    iget-object v0, p0, LX/FCB;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v4, v0, v1

    .line 2210051
    iget-object v0, p0, LX/FCB;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v5, v0, v2

    .line 2210052
    iget-object v0, p0, LX/FCB;->h:LX/FJ5;

    invoke-virtual/range {v0 .. v5}, LX/FJ5;->a(FFFFF)V

    .line 2210053
    iput v1, p0, LX/FCB;->o:F

    .line 2210054
    iput v2, p0, LX/FCB;->p:F

    .line 2210055
    iput v3, p0, LX/FCB;->q:F

    goto :goto_0
.end method
