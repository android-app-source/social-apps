.class public final LX/FhA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/suggestions/SuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2271789
    iput-object p1, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Fid;LX/1Qq;LX/2SP;)Z
    .locals 12

    .prologue
    .line 2271727
    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->j:LX/Fhc;

    .line 2271728
    iget-object v1, p1, LX/Fid;->a:Ljava/util/List;

    move-object v1, v1

    .line 2271729
    iget-object v2, v0, LX/Fhc;->c:LX/Fhi;

    invoke-virtual {v2, v1}, LX/Fhd;->a(Ljava/util/List;)V

    .line 2271730
    iget-object v2, v0, LX/Fhc;->d:LX/Fhj;

    invoke-virtual {v2, v1}, LX/Fhd;->a(Ljava/util/List;)V

    .line 2271731
    iget-object v2, v0, LX/Fhc;->b:LX/Fhk;

    invoke-virtual {v2, v1}, LX/Fhd;->a(Ljava/util/List;)V

    .line 2271732
    iget-object v2, v0, LX/Fhc;->a:LX/Fhe;

    invoke-virtual {v2, v1}, LX/Fhd;->a(Ljava/util/List;)V

    .line 2271733
    iget-object v2, v0, LX/Fhc;->e:LX/Fhh;

    invoke-virtual {v2, v1}, LX/Fhd;->a(Ljava/util/List;)V

    .line 2271734
    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    iget-object v1, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v1

    .line 2271735
    const-string v2, "post_processing"

    invoke-static {v0, v2}, LX/2Sg;->d(LX/2Sg;Ljava/lang/String;)LX/11o;

    move-result-object v2

    const-string v3, "post_processing"

    const v4, -0x4c2ed82c

    invoke-static {v2, v3, v4}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2271736
    const-string v2, "end_to_end"

    invoke-static {v0, v2}, LX/2Sg;->d(LX/2Sg;Ljava/lang/String;)LX/11o;

    move-result-object v3

    const-string v4, "end_to_end"

    const/4 v5, 0x0

    const-string v6, "query_state"

    if-eqz v1, :cond_5

    invoke-virtual {v1}, LX/CwU;->name()Ljava/lang/String;

    move-result-object v2

    :goto_0
    const-string v7, "is_first_sequence"

    iget-boolean v8, v0, LX/2Sg;->d:Z

    invoke-static {v8}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v2, v7, v8}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v2

    const v6, -0x613c914f

    invoke-static {v3, v4, v5, v2, v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2271737
    iget-object v2, v0, LX/2Sg;->e:LX/11i;

    invoke-static {v0}, LX/2Sg;->l(LX/2Sg;)LX/2Sh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2271738
    iget-object v2, v0, LX/2Sg;->e:LX/11i;

    invoke-static {v0}, LX/2Sg;->l(LX/2Sg;)LX/2Sh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/11i;->b(LX/0Pq;)V

    .line 2271739
    iget-object v2, v0, LX/2Sg;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x70015

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2271740
    :cond_0
    sget-object v2, LX/2Si;->NONE:LX/2Si;

    iput-object v2, v0, LX/2Sg;->h:LX/2Si;

    .line 2271741
    iget-boolean v2, v0, LX/2Sg;->d:Z

    if-eqz v2, :cond_1

    .line 2271742
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/2Sg;->d:Z

    .line 2271743
    :cond_1
    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ac:LX/FgV;

    if-nez v0, :cond_2

    .line 2271744
    iget-object v1, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->S:LX/FgW;

    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    .line 2271745
    new-instance v3, LX/FgV;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v3, v4, v0}, LX/FgV;-><init>(LX/0ad;LX/Cvp;)V

    .line 2271746
    const/16 v4, 0x113c

    invoke-static {v2, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x113d

    invoke-static {v2, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32b7

    invoke-static {v2, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x32c2

    invoke-static {v2, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x34be

    invoke-static {v2, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2271747
    iput-object v4, v3, LX/FgV;->a:LX/0Ot;

    iput-object v5, v3, LX/FgV;->b:LX/0Ot;

    iput-object v6, v3, LX/FgV;->c:LX/0Ot;

    iput-object v7, v3, LX/FgV;->d:LX/0Ot;

    iput-object v8, v3, LX/FgV;->e:LX/0Ot;

    .line 2271748
    move-object v0, v3

    .line 2271749
    iput-object v0, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->ac:LX/FgV;

    .line 2271750
    :cond_2
    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->ac:LX/FgV;

    .line 2271751
    iget-boolean v3, v0, LX/FgV;->g:Z

    if-eqz v3, :cond_6

    .line 2271752
    :goto_1
    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v0

    sget-object v1, LX/CwU;->NULL_STATE:LX/CwU;

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, LX/Fid;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 2271753
    invoke-virtual {p3}, LX/2SP;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2271754
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {p1}, LX/Fid;->size()I

    move-result v2

    if-le v1, v2, :cond_3

    .line 2271755
    const/4 v2, 0x0

    invoke-virtual {p1}, LX/Fid;->size()I

    move-result v3

    add-int/lit8 v3, v3, 0x6

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    .line 2271756
    invoke-virtual {p1, v2}, LX/Fid;->a(Ljava/util/List;)V

    .line 2271757
    invoke-virtual {v2}, LX/0Px;->size()I

    .line 2271758
    invoke-interface {p2}, LX/1Cw;->notifyDataSetChanged()V

    .line 2271759
    :cond_3
    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2271760
    iget-object v1, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->d()LX/1Qq;

    move-result-object v1

    .line 2271761
    if-nez v1, :cond_b

    .line 2271762
    :cond_4
    :goto_2
    const/4 v0, 0x0

    return v0

    .line 2271763
    :cond_5
    const-string v2, "null"

    goto/16 :goto_0

    .line 2271764
    :cond_6
    iget-boolean v3, v0, LX/FgV;->h:Z

    if-eqz v3, :cond_7

    .line 2271765
    iget-object v3, v0, LX/FgV;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3Qc;

    invoke-virtual {v3}, LX/3Qc;->a()V

    .line 2271766
    :cond_7
    iget-boolean v3, v0, LX/FgV;->i:Z

    if-eqz v3, :cond_8

    .line 2271767
    iget-object v3, v0, LX/FgV;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3Qn;

    invoke-virtual {v3}, LX/3Qn;->a()V

    .line 2271768
    :cond_8
    iget-object v3, v0, LX/FgV;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FZS;

    .line 2271769
    iget-object v4, v3, LX/FZS;->a:LX/2do;

    iget-object v5, v3, LX/FZS;->b:LX/FZR;

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b2;)Z

    .line 2271770
    iget-object v3, v0, LX/FgV;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8cC;

    .line 2271771
    iget-object v5, v3, LX/8cC;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8cD;

    .line 2271772
    iget-object v8, v5, LX/8cD;->a:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    iget-object v10, v5, LX/8cD;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xdbba0

    cmp-long v8, v8, v10

    if-lez v8, :cond_a

    const/4 v8, 0x1

    :goto_3
    move v5, v8

    .line 2271773
    if-nez v5, :cond_9

    .line 2271774
    :goto_4
    iget-object v3, v0, LX/FgV;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FhP;

    iget-object v4, v0, LX/FgV;->f:LX/Cvp;

    invoke-virtual {v3, v4}, LX/FhH;->a(LX/Cvp;)V

    .line 2271775
    iget-object v3, v0, LX/FgV;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FhP;

    .line 2271776
    sget-object v4, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v4}, Lcom/facebook/search/api/GraphSearchQuery;->m()LX/7B3;

    move-result-object v4

    .line 2271777
    const-string v5, "FRIENDLY_NAME"

    const-string v6, "search_typeahead_pre_connect"

    invoke-static {v5, v6}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/7B2;->a(Ljava/util/Map;)LX/7B2;

    .line 2271778
    invoke-virtual {v4}, LX/7B3;->b()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/7HT;->a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2271779
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/FgV;->g:Z

    goto/16 :goto_1

    .line 2271780
    :cond_9
    iget-object v5, v3, LX/8cC;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    new-instance v6, Lcom/facebook/search/bootstrap/db/BootstrapDbCacheWarmer$1;

    invoke-direct {v6, v3}, Lcom/facebook/search/bootstrap/db/BootstrapDbCacheWarmer$1;-><init>(LX/8cC;)V

    const v7, -0x213ef425

    invoke-static {v5, v6, v7}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_4

    :cond_a
    const/4 v8, 0x0

    goto :goto_3

    .line 2271781
    :cond_b
    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->y(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0g7;

    move-result-object v2

    .line 2271782
    invoke-static {v2}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a(LX/0g7;)LX/1P0;

    move-result-object v3

    .line 2271783
    if-eqz v3, :cond_4

    .line 2271784
    iget v4, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aj:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_c

    .line 2271785
    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->z(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271786
    :cond_c
    invoke-virtual {v3}, LX/1P1;->n()I

    move-result v3

    iget v4, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aj:I

    sub-int/2addr v3, v4

    .line 2271787
    invoke-interface {v1}, LX/1Qq;->getCount()I

    move-result v4

    .line 2271788
    iget-object v1, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Q:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Kt;

    iget v5, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->aj:I

    invoke-interface {v1, v2, v5, v3, v4}, LX/0fx;->a(LX/0g8;III)V

    goto/16 :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2271725
    iget-object v0, p0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->K(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271726
    return-void
.end method
