.class public LX/GgP;
.super LX/1cr;
.source ""


# instance fields
.field private final b:Landroid/graphics/Rect;

.field public c:LX/GgO;

.field private d:Z


# direct methods
.method private b(I)Z
    .locals 1

    .prologue
    .line 2379287
    if-ltz p1, :cond_0

    .line 2379288
    const/4 p0, 0x5

    move v0, p0

    .line 2379289
    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)I
    .locals 7

    .prologue
    .line 2379275
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2379276
    invoke-virtual {p0, v3}, LX/GgP;->a(Ljava/util/List;)V

    .line 2379277
    const/high16 v2, -0x80000000

    .line 2379278
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2379279
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2379280
    if-ltz v0, :cond_0

    .line 2379281
    const/4 v5, 0x5

    move v4, v5

    .line 2379282
    if-ge v0, v4, :cond_0

    .line 2379283
    iget-object v4, p0, LX/GgP;->c:LX/GgO;

    invoke-virtual {v4, v0}, LX/GgO;->b(I)Landroid/graphics/Rect;

    move-result-object v4

    .line 2379284
    float-to-int v5, p1

    float-to-int v6, p2

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2379285
    :goto_1
    return v0

    .line 2379286
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final a(ILX/3sp;)V
    .locals 3

    .prologue
    .line 2379263
    invoke-direct {p0, p1}, LX/GgP;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2379264
    :cond_0
    :goto_0
    return-void

    .line 2379265
    :cond_1
    iget-object v0, p0, LX/GgP;->c:LX/GgO;

    invoke-virtual {v0, p1}, LX/GgO;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 2379266
    iget-object v1, p0, LX/GgP;->b:Landroid/graphics/Rect;

    .line 2379267
    iget-object v2, p0, LX/GgP;->c:LX/GgO;

    invoke-virtual {v2, p1}, LX/GgO;->b(I)Landroid/graphics/Rect;

    move-result-object v2

    .line 2379268
    if-eqz v1, :cond_2

    .line 2379269
    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2379270
    :goto_1
    iget-object v1, p0, LX/GgP;->b:Landroid/graphics/Rect;

    invoke-virtual {p2, v1}, LX/3sp;->b(Landroid/graphics/Rect;)V

    .line 2379271
    invoke-virtual {p2, v0}, LX/3sp;->d(Ljava/lang/CharSequence;)V

    .line 2379272
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 2379273
    iget-boolean v0, p0, LX/GgP;->d:Z

    if-eqz v0, :cond_0

    .line 2379274
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, LX/3sp;->f(Z)V

    goto :goto_0

    :cond_2
    goto :goto_1
.end method

.method public final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 2379290
    iget-object v0, p0, LX/GgP;->c:LX/GgO;

    invoke-virtual {v0, p1}, LX/GgO;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 2379291
    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2379292
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2379256
    const/4 v0, 0x0

    .line 2379257
    :goto_0
    const/4 v2, 0x5

    move v1, v2

    .line 2379258
    if-ge v0, v1, :cond_1

    .line 2379259
    iget-object v1, p0, LX/GgP;->c:LX/GgO;

    invoke-virtual {v1, v0}, LX/GgO;->a(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2379260
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2379261
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2379262
    :cond_1
    return-void
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 2379253
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/GgP;->c:LX/GgO;

    invoke-virtual {v0, p1}, LX/GgO;->a(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2379254
    :cond_0
    const/4 v0, 0x0

    .line 2379255
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, LX/1cr;->a(II)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2379252
    const/4 v0, 0x0

    return v0
.end method
