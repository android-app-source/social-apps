.class public final LX/G7P;
.super LX/2vq;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2vq",
        "<",
        "LX/G8C;",
        "Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LX/2vq;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;LX/1qf;LX/1qg;)LX/G8C;
    .locals 7
    .param p3    # Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    new-instance v0, LX/G8C;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/G8C;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;LX/1qf;LX/1qg;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Ljava/lang/Object;LX/1qf;LX/1qg;)LX/2wJ;
    .locals 6
    .param p4    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    move-object v3, p4

    check-cast v3, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v4, p5

    move-object v5, p6

    invoke-static/range {v0 .. v5}, LX/G7P;->a(Landroid/content/Context;Landroid/os/Looper;LX/2wA;Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;LX/1qf;LX/1qg;)LX/G8C;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    check-cast p1, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    if-nez p1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method
