.class public final LX/GVz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GVw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GVw",
        "<",
        "LX/GXD;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2360263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b(Landroid/view/ViewGroup;LX/7iP;)LX/GXD;
    .locals 2

    .prologue
    .line 2360264
    new-instance v0, LX/GXD;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GXD;-><init>(Landroid/content/Context;)V

    .line 2360265
    iput-object p1, v0, LX/GXD;->j:LX/7iP;

    .line 2360266
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360267
    sget-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    invoke-static {p1, v0}, LX/GVz;->b(Landroid/view/ViewGroup;LX/7iP;)LX/GXD;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;LX/7iP;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360268
    invoke-static {p1, p2}, LX/GVz;->b(Landroid/view/ViewGroup;LX/7iP;)LX/GXD;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
    .locals 1

    .prologue
    .line 2360269
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->USER_INTERACTIONS:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    return-object v0
.end method

.method public final a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2360270
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 2360271
    :goto_0
    return v0

    .line 2360272
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2360273
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2360274
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2360275
    goto :goto_0

    .line 2360276
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2360277
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->q()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;->j()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;

    .line 2360278
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;->l()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v1

    .line 2360279
    goto :goto_0

    .line 2360280
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2360281
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method
