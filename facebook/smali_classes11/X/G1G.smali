.class public LX/G1G;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2310881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310882
    return-void
.end method

.method public static a(LX/0QB;)LX/G1G;
    .locals 4

    .prologue
    .line 2310894
    const-class v1, LX/G1G;

    monitor-enter v1

    .line 2310895
    :try_start_0
    sget-object v0, LX/G1G;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2310896
    sput-object v2, LX/G1G;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2310897
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2310898
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2310899
    new-instance v3, LX/G1G;

    invoke-direct {v3}, LX/G1G;-><init>()V

    .line 2310900
    const/16 p0, 0x1b

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2310901
    iput-object p0, v3, LX/G1G;->a:LX/0Or;

    .line 2310902
    move-object v0, v3

    .line 2310903
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2310904
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G1G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2310905
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2310906
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(I)Landroid/graphics/drawable/LayerDrawable;
    .locals 4
    .param p1    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2310889
    iget-object v0, p0, LX/G1G;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 2310890
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 2310891
    const v2, 0x7f0d31e2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2310892
    const v3, 0x7f0a00d5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2310893
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310883
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne p1, v0, :cond_0

    .line 2310884
    const v0, 0x7f021463

    invoke-direct {p0, v0}, LX/G1G;->a(I)Landroid/graphics/drawable/LayerDrawable;

    move-result-object v0

    .line 2310885
    :goto_0
    return-object v0

    .line 2310886
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne p1, v0, :cond_1

    .line 2310887
    const v0, 0x7f020bab

    invoke-direct {p0, v0}, LX/G1G;->a(I)Landroid/graphics/drawable/LayerDrawable;

    move-result-object v0

    goto :goto_0

    .line 2310888
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
