.class public LX/FCh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0Sh;

.field public final c:LX/0TD;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:LX/FCn;

.field public final f:Landroid/os/Handler;

.field private final g:Landroid/media/AudioManager;

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/FCg;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/net/Uri;

.field public j:Landroid/media/MediaPlayer;

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2210468
    const-class v0, LX/FCh;

    sput-object v0, LX/FCh;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0TD;Ljava/util/concurrent/Executor;LX/FCn;Landroid/os/Handler;Landroid/media/AudioManager;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2210458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210459
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/FCh;->h:Ljava/util/Set;

    .line 2210460
    new-instance v0, Lcom/facebook/messaging/audio/playback/AudioClipPlayer$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/audio/playback/AudioClipPlayer$1;-><init>(LX/FCh;)V

    iput-object v0, p0, LX/FCh;->l:Ljava/lang/Runnable;

    .line 2210461
    iput-object p1, p0, LX/FCh;->b:LX/0Sh;

    .line 2210462
    iput-object p2, p0, LX/FCh;->c:LX/0TD;

    .line 2210463
    iput-object p3, p0, LX/FCh;->d:Ljava/util/concurrent/Executor;

    .line 2210464
    iput-object p4, p0, LX/FCh;->e:LX/FCn;

    .line 2210465
    iput-object p5, p0, LX/FCh;->f:Landroid/os/Handler;

    .line 2210466
    iput-object p6, p0, LX/FCh;->g:Landroid/media/AudioManager;

    .line 2210467
    return-void
.end method

.method public static a$redex0(LX/FCh;LX/FCf;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2210493
    iget-object v0, p0, LX/FCh;->h:Ljava/util/Set;

    new-array v2, v1, [LX/FCg;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FCg;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 2210494
    invoke-interface {v3, p1}, LX/FCg;->a(LX/FCf;)V

    .line 2210495
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2210496
    :cond_0
    return-void
.end method

.method public static b(LX/FCh;Z)V
    .locals 3

    .prologue
    .line 2210475
    iget-object v0, p0, LX/FCh;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 2210476
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Playing the audio clip: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/FCh;->i:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2210477
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/FCh;->i:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2210478
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2210479
    :try_start_0
    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 2210480
    if-eqz p1, :cond_0

    .line 2210481
    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 2210482
    :cond_0
    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2210483
    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 2210484
    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 2210485
    return-void

    .line 2210486
    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1md;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public static f(LX/FCh;)V
    .locals 2

    .prologue
    .line 2210487
    iget-object v0, p0, LX/FCh;->f:Landroid/os/Handler;

    iget-object v1, p0, LX/FCh;->l:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2210488
    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 2210489
    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 2210490
    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 2210491
    const/4 v0, 0x0

    iput-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    .line 2210492
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 2210470
    iget-object v0, p0, LX/FCh;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2210471
    iget-object v0, p0, LX/FCh;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2210472
    :cond_0
    invoke-static {p0}, LX/FCh;->f(LX/FCh;)V

    .line 2210473
    sget-object v0, LX/FCf;->PLAYBACK_STOPPED:LX/FCf;

    invoke-static {p0, v0}, LX/FCh;->a$redex0(LX/FCh;LX/FCf;)V

    .line 2210474
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2210469
    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
