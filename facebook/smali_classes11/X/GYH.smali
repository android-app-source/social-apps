.class public final LX/GYH;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V
    .locals 0

    .prologue
    .line 2364955
    iput-object p1, p0, LX/GYH;->a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2364956
    iget-object v0, p0, LX/GYH;->a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->m:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2364957
    iget-object v0, p0, LX/GYH;->a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814c5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2364958
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2364959
    iget-object v0, p0, LX/GYH;->a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->m:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2364960
    iget-object v0, p0, LX/GYH;->a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    iget-object v0, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0829ba

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2364961
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2364962
    const-string v1, "extra_deleted_tab_type"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2364963
    iget-object v1, p0, LX/GYH;->a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2364964
    if-eqz v1, :cond_0

    .line 2364965
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2364966
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2364967
    :cond_0
    return-void
.end method
