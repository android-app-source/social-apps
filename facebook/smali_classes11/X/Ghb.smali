.class public final enum LX/Ghb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ghb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ghb;

.field public static final enum SHOW_COMMENT:LX/Ghb;

.field public static final enum WITH_BORDER:LX/Ghb;

.field public static final enum WITH_INSET_BORDER:LX/Ghb;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2384439
    new-instance v0, LX/Ghb;

    const-string v1, "SHOW_COMMENT"

    invoke-direct {v0, v1, v2}, LX/Ghb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ghb;->SHOW_COMMENT:LX/Ghb;

    .line 2384440
    new-instance v0, LX/Ghb;

    const-string v1, "WITH_BORDER"

    invoke-direct {v0, v1, v3}, LX/Ghb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ghb;->WITH_BORDER:LX/Ghb;

    .line 2384441
    new-instance v0, LX/Ghb;

    const-string v1, "WITH_INSET_BORDER"

    invoke-direct {v0, v1, v4}, LX/Ghb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ghb;->WITH_INSET_BORDER:LX/Ghb;

    .line 2384442
    const/4 v0, 0x3

    new-array v0, v0, [LX/Ghb;

    sget-object v1, LX/Ghb;->SHOW_COMMENT:LX/Ghb;

    aput-object v1, v0, v2

    sget-object v1, LX/Ghb;->WITH_BORDER:LX/Ghb;

    aput-object v1, v0, v3

    sget-object v1, LX/Ghb;->WITH_INSET_BORDER:LX/Ghb;

    aput-object v1, v0, v4

    sput-object v0, LX/Ghb;->$VALUES:[LX/Ghb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2384436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ghb;
    .locals 1

    .prologue
    .line 2384438
    const-class v0, LX/Ghb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ghb;

    return-object v0
.end method

.method public static values()[LX/Ghb;
    .locals 1

    .prologue
    .line 2384437
    sget-object v0, LX/Ghb;->$VALUES:[LX/Ghb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ghb;

    return-object v0
.end method
