.class public LX/FsH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FsH;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fsf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FsU;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/Fsf;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FsU;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2296688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296689
    iput-object p1, p0, LX/FsH;->a:LX/0Or;

    .line 2296690
    iput-object p2, p0, LX/FsH;->b:LX/0Or;

    .line 2296691
    iput-object p3, p0, LX/FsH;->c:LX/0Or;

    .line 2296692
    return-void
.end method

.method public static a(LX/0QB;)LX/FsH;
    .locals 6

    .prologue
    .line 2296693
    sget-object v0, LX/FsH;->d:LX/FsH;

    if-nez v0, :cond_1

    .line 2296694
    const-class v1, LX/FsH;

    monitor-enter v1

    .line 2296695
    :try_start_0
    sget-object v0, LX/FsH;->d:LX/FsH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2296696
    if-eqz v2, :cond_0

    .line 2296697
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2296698
    new-instance v3, LX/FsH;

    const/16 v4, 0x363d

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x3639

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x2d2

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/FsH;-><init>(LX/0Or;LX/0Or;LX/0Or;)V

    .line 2296699
    move-object v0, v3

    .line 2296700
    sput-object v0, LX/FsH;->d:LX/FsH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2296701
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2296702
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2296703
    :cond_1
    sget-object v0, LX/FsH;->d:LX/FsH;

    return-object v0

    .line 2296704
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2296705
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0v6;JZZILcom/facebook/common/callercontext/CallerContext;)LX/FsJ;
    .locals 6
    .param p7    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2296706
    iget-object v0, p0, LX/FsH;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 2296707
    sget-boolean v0, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->f:Z

    move v3, v0

    .line 2296708
    new-instance v4, LX/G12;

    const/4 v0, 0x0

    invoke-direct {v4, p2, p3, v0, p5}, LX/G12;-><init>(JLjava/lang/String;Z)V

    .line 2296709
    if-eqz p4, :cond_0

    .line 2296710
    iget-object v0, p0, LX/FsH;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FrR;

    :goto_0
    move-object v1, p1

    move v2, p6

    move-object v5, p7

    .line 2296711
    invoke-interface/range {v0 .. v5}, LX/FrR;->a(LX/0v6;IZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v0

    return-object v0

    .line 2296712
    :cond_0
    iget-object v0, p0, LX/FsH;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FrR;

    goto :goto_0
.end method
