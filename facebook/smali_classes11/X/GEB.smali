.class public final LX/GEB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEA;


# instance fields
.field public final synthetic a:LX/GGz;

.field public final synthetic b:LX/GEF;


# direct methods
.method public constructor <init>(LX/GEF;LX/GGz;)V
    .locals 0

    .prologue
    .line 2331161
    iput-object p1, p0, LX/GEB;->b:LX/GEF;

    iput-object p2, p0, LX/GEB;->a:LX/GGz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 8

    .prologue
    .line 2331162
    iget-object v0, p0, LX/GEB;->a:LX/GGz;

    .line 2331163
    iget-object v1, v0, LX/GGz;->g:LX/GH0;

    iget-object v2, v0, LX/GGz;->a:Ljava/lang/String;

    iget-object v3, v0, LX/GGz;->b:Ljava/lang/String;

    iget-object v4, v0, LX/GGz;->c:Ljava/lang/String;

    iget-object v5, v0, LX/GGz;->d:LX/8wL;

    iget-object v6, v0, LX/GGz;->e:LX/GCY;

    iget-object v7, v0, LX/GGz;->f:Ljava/lang/String;

    move-object p0, p1

    .line 2331164
    iput-object v2, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    .line 2331165
    invoke-virtual {p0, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Ljava/lang/String;)V

    .line 2331166
    invoke-virtual {p0, v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c(Ljava/lang/String;)V

    .line 2331167
    iput-object v7, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->o:Ljava/lang/String;

    .line 2331168
    iput-object v5, p0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    .line 2331169
    iget-object v0, v1, LX/GH0;->b:LX/GG6;

    invoke-virtual {v0, p0}, LX/GG6;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2331170
    invoke-interface {v6, p0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2331171
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2331172
    iget-object v0, p0, LX/GEB;->a:LX/GGz;

    .line 2331173
    iget-object v1, v0, LX/GGz;->g:LX/GH0;

    iget-object v1, v1, LX/GH0;->c:LX/2U3;

    const-class v2, LX/GH0;

    const-string p0, "Failed refreshing data"

    invoke-virtual {v1, v2, p0, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2331174
    return-void
.end method
