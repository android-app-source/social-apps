.class public LX/GK0;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

.field public b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

.field private c:LX/GG6;


# direct methods
.method public constructor <init>(LX/GG6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340150
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2340151
    iput-object p1, p0, LX/GK0;->c:LX/GG6;

    .line 2340152
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2340147
    invoke-super {p0}, LX/GHg;->a()V

    .line 2340148
    const/4 v0, 0x0

    iput-object v0, p0, LX/GK0;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340149
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3

    .prologue
    .line 2340118
    check-cast p1, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340119
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340120
    const v0, 0x7f080aaa

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2340121
    iput-object p1, p0, LX/GK0;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    .line 2340122
    iget-object v0, p0, LX/GK0;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->b()I

    move-result v0

    .line 2340123
    iget-object v1, p0, LX/GK0;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    new-instance v2, LX/GG9;

    invoke-direct {v2}, LX/GG9;-><init>()V

    const/4 p1, 0x1

    .line 2340124
    iput p1, v2, LX/GG9;->g:I

    .line 2340125
    move-object v2, v2

    .line 2340126
    iget-object p1, p0, LX/GK0;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v0, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 2340127
    iput-object p1, v2, LX/GG9;->a:Ljava/lang/String;

    .line 2340128
    move-object v2, v2

    .line 2340129
    iget-object p1, p0, LX/GK0;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f080aab

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2340130
    iput-object p1, v2, LX/GG9;->b:Ljava/lang/String;

    .line 2340131
    move-object v2, v2

    .line 2340132
    iget-object p1, p0, LX/GK0;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a035a

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    .line 2340133
    iput-object p1, v2, LX/GG9;->f:LX/0Px;

    .line 2340134
    move-object v2, v2

    .line 2340135
    iget-object p1, p0, LX/GK0;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f080aaa

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    .line 2340136
    iput-object p1, v2, LX/GG9;->d:LX/0Px;

    .line 2340137
    move-object v2, v2

    .line 2340138
    iget-object p1, p0, LX/GK0;->b:Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {v0, p1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2340139
    iput-object v0, v2, LX/GG9;->c:LX/0Px;

    .line 2340140
    move-object v0, v2

    .line 2340141
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2340142
    iput-object v2, v0, LX/GG9;->e:LX/0Px;

    .line 2340143
    move-object v0, v0

    .line 2340144
    invoke-virtual {v0}, LX/GG9;->a()LX/GGA;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;->setViewModel(LX/GGA;)V

    .line 2340145
    const/4 v0, 0x0

    iput-object v0, p0, LX/GK0;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2340146
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2340116
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    iput-object v0, p0, LX/GK0;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2340117
    return-void
.end method
