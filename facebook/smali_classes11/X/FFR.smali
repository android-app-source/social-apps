.class public LX/FFR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0tX;


# direct methods
.method public constructor <init>(LX/0Or;LX/0tX;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0tX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2217441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2217442
    iput-object p1, p0, LX/FFR;->a:LX/0Or;

    .line 2217443
    iput-object p2, p0, LX/FFR;->b:LX/0tX;

    .line 2217444
    return-void
.end method

.method public static b(LX/0QB;)LX/FFR;
    .locals 3

    .prologue
    .line 2217445
    new-instance v1, LX/FFR;

    const/16 v0, 0x15e7

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, v2, v0}, LX/FFR;-><init>(LX/0Or;LX/0tX;)V

    .line 2217446
    return-object v1
.end method
