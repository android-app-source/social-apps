.class public final LX/FIJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public final synthetic e:LX/FIK;


# direct methods
.method public constructor <init>(LX/FIK;)V
    .locals 4

    .prologue
    .line 2221944
    iget-object v0, p1, LX/FIK;->d:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    iget-object v1, p1, LX/FIK;->d:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    invoke-direct {p0, p1, v0, v2, v3}, LX/FIJ;-><init>(LX/FIK;IJ)V

    .line 2221945
    return-void
.end method

.method private constructor <init>(LX/FIK;IJ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2221946
    iput-object p1, p0, LX/FIJ;->e:LX/FIK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221947
    iput p2, p0, LX/FIJ;->a:I

    .line 2221948
    iput-wide p3, p0, LX/FIJ;->b:J

    .line 2221949
    iput-object v0, p0, LX/FIJ;->c:Ljava/lang/String;

    .line 2221950
    iput-object v0, p0, LX/FIJ;->d:Ljava/lang/String;

    .line 2221951
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2221952
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, LX/FIJ;

    if-eq v1, v2, :cond_1

    .line 2221953
    :cond_0
    :goto_0
    return v0

    .line 2221954
    :cond_1
    check-cast p1, LX/FIJ;

    .line 2221955
    iget v1, p0, LX/FIJ;->a:I

    iget v2, p1, LX/FIJ;->a:I

    if-ne v1, v2, :cond_0

    iget-wide v2, p0, LX/FIJ;->b:J

    iget-wide v4, p1, LX/FIJ;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2221956
    iget v0, p0, LX/FIJ;->a:I

    iget-wide v2, p0, LX/FIJ;->b:J

    long-to-int v1, v2

    xor-int/2addr v0, v1

    iget-wide v2, p0, LX/FIJ;->b:J

    const/16 v1, 0x20

    shr-long/2addr v2, v1

    long-to-int v1, v2

    xor-int/2addr v0, v1

    return v0
.end method
