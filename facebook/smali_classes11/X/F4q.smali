.class public LX/F4q;
.super LX/BXZ;
.source ""


# instance fields
.field private final e:LX/333;

.field private final f:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;LX/8vM;LX/8tF;LX/F4W;Ljava/lang/Boolean;LX/0ad;)V
    .locals 8
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2197676
    invoke-direct {p0, p1, p2, p3, p4}, LX/BXZ;-><init>(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;LX/8vM;LX/8tF;)V

    .line 2197677
    iput-object p7, p0, LX/F4q;->f:LX/0ad;

    .line 2197678
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2197679
    invoke-virtual {p0}, LX/8tB;->e()LX/8vE;

    move-result-object v0

    invoke-virtual {p4, v0}, LX/8tF;->a(LX/8vE;)LX/8tE;

    move-result-object v0

    iput-object v0, p0, LX/F4q;->e:LX/333;

    .line 2197680
    :goto_0
    return-void

    .line 2197681
    :cond_0
    invoke-virtual {p0}, LX/8tB;->e()LX/8vE;

    move-result-object v0

    .line 2197682
    new-instance v1, LX/F4V;

    invoke-static {p5}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v2

    check-cast v2, LX/0Zr;

    invoke-static {p5}, LX/8RL;->b(LX/0QB;)LX/8RL;

    move-result-object v3

    check-cast v3, LX/8RL;

    invoke-static {p5}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    const/16 v4, 0x15e7

    invoke-static {p5, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p5}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    move-object v4, v0

    invoke-direct/range {v1 .. v7}, LX/F4V;-><init>(LX/0Zr;LX/8RL;LX/8vE;LX/0tX;LX/0Or;Landroid/content/res/Resources;)V

    .line 2197683
    move-object v0, v1

    .line 2197684
    iput-object v0, p0, LX/F4q;->e:LX/333;

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/333;
    .locals 1

    .prologue
    .line 2197675
    iget-object v0, p0, LX/F4q;->e:LX/333;

    return-object v0
.end method
