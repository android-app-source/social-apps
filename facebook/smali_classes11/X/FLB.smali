.class public LX/FLB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/FKp;


# direct methods
.method public constructor <init>(LX/FKp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2226161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226162
    iput-object p1, p0, LX/FLB;->a:LX/FKp;

    .line 2226163
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2226164
    check-cast p1, Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;

    .line 2226165
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2226166
    iget-object v0, p1, Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;->a:LX/0Px;

    invoke-static {v0}, LX/FKp;->a(Ljava/util/List;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2226167
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tids"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226168
    iget-object v0, p1, Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;->b:LX/0Px;

    invoke-static {v0}, LX/FKp;->a(Ljava/util/List;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2226169
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "removed_tids"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226170
    new-instance v0, LX/14N;

    const-string v1, "updatePinnedThreads"

    const-string v2, "POST"

    const-string v3, "me/pinned_threads"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2226171
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2226172
    const/4 v0, 0x0

    return-object v0
.end method
