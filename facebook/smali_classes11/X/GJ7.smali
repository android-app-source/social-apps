.class public LX/GJ7;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

.field public d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public e:LX/2U3;

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2337799
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->POST_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->LINK_CLICKS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/GJ7;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/2U3;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2U3;",
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337795
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2337796
    iput-object p1, p0, LX/GJ7;->e:LX/2U3;

    .line 2337797
    iput-object p2, p0, LX/GJ7;->f:LX/0Or;

    .line 2337798
    return-void
.end method

.method public static a(LX/0QB;)LX/GJ7;
    .locals 3

    .prologue
    .line 2337792
    new-instance v1, LX/GJ7;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v0

    check-cast v0, LX/2U3;

    const/16 v2, 0xbd2

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/GJ7;-><init>(LX/2U3;LX/0Or;)V

    .line 2337793
    move-object v0, v1

    .line 2337794
    return-object v0
.end method

.method public static a$redex0(LX/GJ7;Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;)V
    .locals 3

    .prologue
    .line 2337788
    sget-object v0, LX/GJ6;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2337789
    iget-object v0, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080ba0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    .line 2337790
    :goto_0
    return-void

    .line 2337791
    :pswitch_0
    iget-object v0, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080ba1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static d(LX/GJ7;Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;)V
    .locals 2

    .prologue
    .line 2337782
    iget-object v0, p0, LX/GJ7;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v0

    if-eq v0, p1, :cond_0

    .line 2337783
    iget-object v0, p0, LX/GJ7;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2337784
    iput-object p1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 2337785
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337786
    new-instance v1, LX/GFY;

    invoke-direct {v1}, LX/GFY;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2337787
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2337776
    invoke-super {p0}, LX/GHg;->a()V

    .line 2337777
    iget-object v0, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 2337778
    if-eqz v0, :cond_0

    .line 2337779
    iget-object v1, p0, LX/GJ7;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2337780
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    .line 2337781
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2337771
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2337772
    const-string v0, "LINK_OBJECTIVE"

    iget-object v1, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    .line 2337773
    iget p0, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->c:I

    move v1, p0

    .line 2337774
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2337775
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 10

    .prologue
    .line 2337721
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    const/4 v0, 0x0

    .line 2337722
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337723
    iput-object p2, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2337724
    iget-object v1, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v2, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080b9f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/String;I)V

    .line 2337725
    iput-object p1, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    .line 2337726
    iget-object v1, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const v2, 0x7f080b9e

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2337727
    iget-object v1, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    new-instance v2, LX/GJ5;

    invoke-direct {v2, p0}, LX/GJ5;-><init>(LX/GJ7;)V

    .line 2337728
    iput-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->b:LX/GJ4;

    .line 2337729
    move v1, v0

    .line 2337730
    :goto_0
    sget-object v0, LX/GJ7;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2337731
    sget-object v0, LX/GJ7;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 2337732
    iget-object v2, p0, LX/GJ7;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->aa()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2337733
    iget-object v2, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->d(I)Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    move-result-object v2

    .line 2337734
    sget-object v3, LX/GJ6;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->ordinal()I

    move-result p1

    aget v3, v3, p1

    packed-switch v3, :pswitch_data_0

    .line 2337735
    iget-object v3, p0, LX/GJ7;->e:LX/2U3;

    const-class p1, LX/GJ7;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    const-string p2, "Unsupported Objective type"

    invoke-virtual {v3, p1, p2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2337736
    const-string v3, ""

    :goto_1
    move-object v3, v3

    .line 2337737
    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2337738
    sget-object v3, LX/GJ6;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->ordinal()I

    move-result p1

    aget v3, v3, p1

    packed-switch v3, :pswitch_data_1

    .line 2337739
    iget-object v3, p0, LX/GJ7;->e:LX/2U3;

    const-class p1, LX/GJ7;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    const-string p2, "Unsupported Objective type"

    invoke-virtual {v3, p1, p2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2337740
    const-string v3, ""

    :goto_2
    move-object v0, v3

    .line 2337741
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2337742
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2337743
    :cond_1
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337744
    iget-object v1, v0, LX/GCE;->e:LX/0ad;

    move-object v0, v1

    .line 2337745
    sget-char v1, LX/GDK;->o:C

    const-string v2, "engagement"

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2337746
    const-string v1, "clicks"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->LINK_CLICKS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 2337747
    :goto_3
    iget-object v1, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    sget-object v2, LX/GJ7;->a:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->setSelected(I)V

    .line 2337748
    invoke-static {p0, v0}, LX/GJ7;->d(LX/GJ7;Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;)V

    .line 2337749
    invoke-static {p0, v0}, LX/GJ7;->a$redex0(LX/GJ7;Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;)V

    .line 2337750
    iget-object v4, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->getHandler()Landroid/os/Handler;

    move-result-object v6

    .line 2337751
    iget-object v4, p0, LX/GJ7;->f:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0iA;

    .line 2337752
    new-instance v5, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v7, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_LINK_OBJECTIVE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v5, v7}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v7, LX/3lN;

    invoke-virtual {v4, v5, v7}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v5

    check-cast v5, LX/3lN;

    .line 2337753
    if-nez v5, :cond_4

    .line 2337754
    :cond_2
    :goto_4
    return-void

    .line 2337755
    :pswitch_0
    iget-object v3, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f080ba3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 2337756
    :pswitch_1
    iget-object v3, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f080ba5

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 2337757
    :pswitch_2
    iget-object v3, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f080ba4

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 2337758
    :pswitch_3
    iget-object v3, p0, LX/GJ7;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f080ba6

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2337759
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->POST_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    goto :goto_3

    .line 2337760
    :cond_4
    new-instance v7, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostPostLinkObjectiveViewController$2;

    invoke-direct {v7, p0, v5, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostPostLinkObjectiveViewController$2;-><init>(LX/GJ7;LX/3lN;LX/0iA;)V

    iput-object v7, p0, LX/GJ7;->g:Ljava/lang/Runnable;

    .line 2337761
    if-eqz v6, :cond_2

    .line 2337762
    iget-object v4, p0, LX/GJ7;->g:Ljava/lang/Runnable;

    const-wide/16 v8, 0x3e8

    const v5, -0x77982550

    invoke-static {v6, v4, v8, v9, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2337768
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2337769
    iput-object p1, p0, LX/GJ7;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2337770
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2337763
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2337764
    if-eqz p1, :cond_0

    .line 2337765
    iget-object v0, p0, LX/GJ7;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    const-string v1, "LINK_OBJECTIVE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2337766
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->setSelected(I)V

    .line 2337767
    :cond_0
    return-void
.end method
