.class public final LX/F8e;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V
    .locals 0

    .prologue
    .line 2204206
    iput-object p1, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2204207
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v1, LX/F8L;->FAILURE:LX/F8L;

    invoke-virtual {v0, v1}, LX/F8M;->a(LX/F8L;)V

    .line 2204208
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2204209
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Z)V

    .line 2204210
    :cond_0
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    iget-object v1, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v2}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J

    move-result-wide v2

    iget-object v4, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v4, v4, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    sget-object v5, LX/9Tj;->INVITES_FETCH_FAILED:LX/9Tj;

    invoke-virtual/range {v0 .. v5}, LX/9Tk;->a(Ljava/lang/String;JILX/9Tj;)V

    .line 2204211
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2204212
    check-cast p1, LX/0Px;

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2204213
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v1, LX/F8L;->DEFAULT:LX/F8L;

    invoke-virtual {v0, v1}, LX/F8M;->a(LX/F8L;)V

    .line 2204214
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2204215
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2204216
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v0, v9}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Z)V

    .line 2204217
    :cond_0
    :goto_0
    return-void

    .line 2204218
    :cond_1
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    iget-object v1, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v2}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J

    move-result-wide v2

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    iget-object v5, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v5}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->t(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J

    move-result-wide v5

    sget-object v7, LX/9Tj;->INVITES_PAGE_FETCHED:LX/9Tj;

    invoke-virtual/range {v0 .. v7}, LX/9Tk;->a(Ljava/lang/String;JIJLX/9Tj;)V

    .line 2204219
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2204220
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v11

    move v7, v9

    :goto_1
    if-ge v7, v11, :cond_3

    invoke-virtual {p1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;

    .line 2204221
    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2204222
    iget-object v1, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2204223
    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 2204224
    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2204225
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    aput-object v2, v1, v9

    aput-object v4, v1, v8

    const/4 v3, 0x2

    aput-object v5, v1, v3

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2204226
    new-instance v1, LX/F8P;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v6, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v6}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->u(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)LX/5P4;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, LX/F8P;-><init>(JLjava/lang/String;Ljava/lang/String;LX/5P4;)V

    .line 2204227
    iget-object v2, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$InvitableContactsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2204228
    invoke-virtual {v10, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2204229
    :cond_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 2204230
    :cond_3
    iget-object v1, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v8

    :goto_2
    invoke-static {v1, v0}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Z)V

    .line 2204231
    iget-object v0, p0, LX/F8e;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/F8M;->a(Ljava/util/List;)V

    goto/16 :goto_0

    :cond_4
    move v0, v9

    .line 2204232
    goto :goto_2
.end method
