.class public LX/FQK;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;

.field private final b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;

.field private final c:Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;

.field public final d:Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;

.field private final e:Landroid/view/View$OnClickListener;

.field private final f:LX/FQE;

.field private final g:LX/FQG;

.field private final h:Landroid/view/View$OnClickListener;

.field public i:LX/FPE;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2238954
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FQK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2238955
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2238956
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/FQK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2238957
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2238958
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2238959
    new-instance v0, LX/FQD;

    invoke-direct {v0, p0}, LX/FQD;-><init>(LX/FQK;)V

    iput-object v0, p0, LX/FQK;->e:Landroid/view/View$OnClickListener;

    .line 2238960
    new-instance v0, LX/FQF;

    invoke-direct {v0, p0}, LX/FQF;-><init>(LX/FQK;)V

    iput-object v0, p0, LX/FQK;->f:LX/FQE;

    .line 2238961
    new-instance v0, LX/FQH;

    invoke-direct {v0, p0}, LX/FQH;-><init>(LX/FQK;)V

    iput-object v0, p0, LX/FQK;->g:LX/FQG;

    .line 2238962
    new-instance v0, LX/FQI;

    invoke-direct {v0, p0}, LX/FQI;-><init>(LX/FQK;)V

    iput-object v0, p0, LX/FQK;->h:Landroid/view/View$OnClickListener;

    .line 2238963
    const v0, 0x7f030bca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2238964
    const v0, 0x7f0d1d5f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;

    iput-object v0, p0, LX/FQK;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;

    .line 2238965
    iget-object v0, p0, LX/FQK;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;

    iget-object v1, p0, LX/FQK;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2238966
    const v0, 0x7f0d1d60

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;

    iput-object v0, p0, LX/FQK;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;

    .line 2238967
    iget-object v0, p0, LX/FQK;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;

    iget-object v1, p0, LX/FQK;->g:LX/FQG;

    .line 2238968
    iput-object v1, v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->g:LX/FQG;

    .line 2238969
    const v0, 0x7f0d1d61

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;

    iput-object v0, p0, LX/FQK;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;

    .line 2238970
    iget-object v0, p0, LX/FQK;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;

    iget-object v1, p0, LX/FQK;->f:LX/FQE;

    .line 2238971
    iput-object v1, v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->b:LX/FQE;

    .line 2238972
    const v0, 0x7f0d1d62

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;

    iput-object v0, p0, LX/FQK;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;

    .line 2238973
    iget-object v0, p0, LX/FQK;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;

    iget-object v1, p0, LX/FQK;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2238974
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2238975
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238976
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238977
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238978
    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->I()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->I()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 2238979
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v4, 0x3

    if-lt v0, v4, :cond_2

    const/4 v0, 0x1

    .line 2238980
    :goto_1
    if-eqz v0, :cond_3

    .line 2238981
    sget-object v0, LX/FQC;->LARGE:LX/FQC;

    .line 2238982
    :goto_2
    move-object v0, v0

    .line 2238983
    iget-object v1, p0, LX/FQK;->a:Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/NearbyPlacesResultListItemHeaderView;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;LX/FQC;)V

    .line 2238984
    sget-object v1, LX/FQC;->LARGE:LX/FQC;

    if-ne v0, v1, :cond_0

    .line 2238985
    iget-object v0, p0, LX/FQK;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->setVisibility(I)V

    .line 2238986
    iget-object v0, p0, LX/FQK;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;

    invoke-virtual {v0, p1}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    .line 2238987
    :goto_3
    invoke-static {p1, p2, p3}, LX/FQL;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Z)LX/FQL;

    move-result-object v0

    .line 2238988
    iget v0, v0, LX/FQL;->a:I

    if-lez v0, :cond_1

    .line 2238989
    iget-object v0, p0, LX/FQK;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->setVisibility(I)V

    .line 2238990
    iget-object v0, p0, LX/FQK;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Z)V

    .line 2238991
    :goto_4
    invoke-static {p1}, LX/FQP;->getComponentTypeForPlace(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)LX/FQP;

    move-result-object v0

    .line 2238992
    sget-object v1, LX/FQJ;->a:[I

    invoke-virtual {v0}, LX/FQP;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2238993
    iget-object v0, p0, LX/FQK;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->setVisibility(I)V

    .line 2238994
    :goto_5
    return-void

    .line 2238995
    :cond_0
    iget-object v0, p0, LX/FQK;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->a()V

    .line 2238996
    iget-object v0, p0, LX/FQK;->c:Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/photohscroll/NearbyPlacesPhotoHScrollView;->setVisibility(I)V

    goto :goto_3

    .line 2238997
    :cond_1
    iget-object v0, p0, LX/FQK;->b:Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/actionbar/NearbyPlacesActionBarView;->setVisibility(I)V

    goto :goto_4

    .line 2238998
    :pswitch_0
    iget-object v0, p0, LX/FQK;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->setVisibility(I)V

    goto :goto_5

    .line 2238999
    :pswitch_1
    iget-object v0, p0, LX/FQK;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;

    invoke-virtual {v0, v3}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->setVisibility(I)V

    .line 2239000
    iget-object v0, p0, LX/FQK;->d:Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->e()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/itemview/socialcontext/NearbyPlacesFriendsWhoVisitedView;->a(Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;Ljava/lang/String;)V

    goto :goto_5

    .line 2239001
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2239002
    :cond_3
    if-eqz v1, :cond_4

    .line 2239003
    sget-object v0, LX/FQC;->MEDIUM:LX/FQC;

    goto :goto_2

    .line 2239004
    :cond_4
    sget-object v0, LX/FQC;->SMALL:LX/FQC;

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
