.class public LX/Fsm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Fsm;


# instance fields
.field public final a:LX/0sa;

.field public final b:LX/0rq;

.field public final c:LX/0se;

.field public final d:LX/0tI;

.field public final e:LX/0ad;

.field private final f:LX/A5q;

.field private final g:LX/0wo;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0se;LX/0ad;LX/0tI;LX/A5q;LX/0wo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297518
    iput-object p1, p0, LX/Fsm;->a:LX/0sa;

    .line 2297519
    iput-object p2, p0, LX/Fsm;->b:LX/0rq;

    .line 2297520
    iput-object p3, p0, LX/Fsm;->c:LX/0se;

    .line 2297521
    iput-object p5, p0, LX/Fsm;->d:LX/0tI;

    .line 2297522
    iput-object p4, p0, LX/Fsm;->e:LX/0ad;

    .line 2297523
    iput-object p6, p0, LX/Fsm;->f:LX/A5q;

    .line 2297524
    iput-object p7, p0, LX/Fsm;->g:LX/0wo;

    .line 2297525
    return-void
.end method

.method public static a(LX/0QB;)LX/Fsm;
    .locals 11

    .prologue
    .line 2297526
    sget-object v0, LX/Fsm;->h:LX/Fsm;

    if-nez v0, :cond_1

    .line 2297527
    const-class v1, LX/Fsm;

    monitor-enter v1

    .line 2297528
    :try_start_0
    sget-object v0, LX/Fsm;->h:LX/Fsm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2297529
    if-eqz v2, :cond_0

    .line 2297530
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2297531
    new-instance v3, LX/Fsm;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v4

    check-cast v4, LX/0sa;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v6

    check-cast v6, LX/0se;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v8

    check-cast v8, LX/0tI;

    invoke-static {v0}, LX/A5q;->a(LX/0QB;)LX/A5q;

    move-result-object v9

    check-cast v9, LX/A5q;

    invoke-static {v0}, LX/0wo;->a(LX/0QB;)LX/0wo;

    move-result-object v10

    check-cast v10, LX/0wo;

    invoke-direct/range {v3 .. v10}, LX/Fsm;-><init>(LX/0sa;LX/0rq;LX/0se;LX/0ad;LX/0tI;LX/A5q;LX/0wo;)V

    .line 2297532
    move-object v0, v3

    .line 2297533
    sput-object v0, LX/Fsm;->h:LX/Fsm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297534
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2297535
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2297536
    :cond_1
    sget-object v0, LX/Fsm;->h:LX/Fsm;

    return-object v0

    .line 2297537
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2297538
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
