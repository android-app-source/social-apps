.class public LX/F9x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1hZ;
.implements LX/0dx;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/F9x;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile t:LX/F9x;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FA6;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/F9z;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4iM;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4iJ;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/onion/TorProxyListenersInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FA1;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1h1;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FAK;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0kr;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/4ce;

.field private o:LX/4cd;

.field private p:Lorg/apache/http/HttpHost;

.field private q:Ljava/net/Proxy;

.field private r:Z

.field public s:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2206423
    const-class v0, LX/F9x;

    sput-object v0, LX/F9x;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/FA6;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/F9z;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/4iM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/4iJ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/onion/TorProxyListenersInitializer;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FA1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1h1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FAK;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2206424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206425
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/F9x;->m:Ljava/util/List;

    .line 2206426
    sget-object v0, LX/4ce;->DISABLED:LX/4ce;

    iput-object v0, p0, LX/F9x;->n:LX/4ce;

    .line 2206427
    const/4 v0, 0x0

    iput-object v0, p0, LX/F9x;->o:LX/4cd;

    .line 2206428
    iput-boolean v1, p0, LX/F9x;->r:Z

    .line 2206429
    iput-boolean v1, p0, LX/F9x;->s:Z

    .line 2206430
    iput-object p1, p0, LX/F9x;->b:Landroid/content/Context;

    .line 2206431
    iput-object p2, p0, LX/F9x;->c:LX/0Ot;

    .line 2206432
    iput-object p3, p0, LX/F9x;->d:LX/0Ot;

    .line 2206433
    iput-object p4, p0, LX/F9x;->e:LX/0Ot;

    .line 2206434
    iput-object p5, p0, LX/F9x;->f:LX/0Ot;

    .line 2206435
    iput-object p6, p0, LX/F9x;->g:LX/0Ot;

    .line 2206436
    iput-object p7, p0, LX/F9x;->h:LX/0Ot;

    .line 2206437
    iput-object p8, p0, LX/F9x;->i:LX/0Ot;

    .line 2206438
    iput-object p9, p0, LX/F9x;->j:LX/0Ot;

    .line 2206439
    iput-object p10, p0, LX/F9x;->k:LX/0Ot;

    .line 2206440
    iput-object p11, p0, LX/F9x;->l:LX/0Ot;

    .line 2206441
    return-void
.end method

.method public static a(LX/0QB;)LX/F9x;
    .locals 15

    .prologue
    .line 2206442
    sget-object v0, LX/F9x;->t:LX/F9x;

    if-nez v0, :cond_1

    .line 2206443
    const-class v1, LX/F9x;

    monitor-enter v1

    .line 2206444
    :try_start_0
    sget-object v0, LX/F9x;->t:LX/F9x;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2206445
    if-eqz v2, :cond_0

    .line 2206446
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2206447
    new-instance v3, LX/F9x;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x250f

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1430

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x250b

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x301e

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x301d

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2512

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x250c

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0xb71

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x2519

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0xac0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, LX/F9x;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2206448
    move-object v0, v3

    .line 2206449
    sput-object v0, LX/F9x;->t:LX/F9x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206450
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2206451
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2206452
    :cond_1
    sget-object v0, LX/F9x;->t:LX/F9x;

    return-object v0

    .line 2206453
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2206454
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/4ce;LX/4cd;)V
    .locals 3

    .prologue
    .line 2206455
    iget-object v0, p0, LX/F9x;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kr;

    .line 2206456
    invoke-interface {v0, p1, p2}, LX/0kr;->a(LX/4ce;LX/4cd;)V

    goto :goto_0

    .line 2206457
    :cond_0
    iget-object v0, p0, LX/F9x;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F9z;

    .line 2206458
    iget-object v1, v0, LX/F9z;->b:Ljava/util/Map;

    const-string v2, "state"

    invoke-virtual {p1}, LX/4ce;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2206459
    if-eqz p2, :cond_1

    .line 2206460
    iget-object v1, v0, LX/F9z;->b:Ljava/util/Map;

    const-string v2, "connState"

    invoke-virtual {p2}, LX/4cd;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2206461
    :cond_1
    return-void
.end method

.method public static declared-synchronized a(LX/F9x;LX/4ce;)V
    .locals 2

    .prologue
    .line 2206462
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/F9x;->n:LX/4ce;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 2206463
    :goto_0
    monitor-exit p0

    return-void

    .line 2206464
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/F9x;->n:LX/4ce;

    .line 2206465
    invoke-direct {p0}, LX/F9x;->q()LX/4cd;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/F9x;->a(LX/4ce;LX/4cd;)V

    .line 2206466
    iget-object v0, p0, LX/F9x;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F9z;

    .line 2206467
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "onion_state_change"

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2206468
    invoke-static {v0, v1}, LX/F9z;->a(LX/F9z;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206469
    goto :goto_0

    .line 2206470
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(LX/F9x;Z)V
    .locals 3

    .prologue
    .line 2206471
    if-eqz p1, :cond_1

    .line 2206472
    iget-object v0, p0, LX/F9x;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x3ad

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2206473
    iget-object v0, p0, LX/F9x;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FAK;

    sget-object v1, LX/FAI;->REFUSE_TOR:LX/FAI;

    invoke-virtual {v0, v1}, LX/FAK;->a(LX/FAI;)V

    .line 2206474
    :cond_0
    :goto_0
    return-void

    .line 2206475
    :cond_1
    iget-object v0, p0, LX/F9x;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FAK;

    sget-object v1, LX/FAI;->DISABLED:LX/FAI;

    invoke-virtual {v0, v1}, LX/FAK;->a(LX/FAI;)V

    goto :goto_0
.end method

.method public static declared-synchronized a$redex0(LX/F9x;LX/4cd;)V
    .locals 1

    .prologue
    .line 2206476
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/F9x;->o:LX/4cd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    .line 2206477
    :goto_0
    monitor-exit p0

    return-void

    .line 2206478
    :cond_0
    :try_start_1
    iput-object p1, p0, LX/F9x;->o:LX/4cd;

    .line 2206479
    invoke-static {p0}, LX/F9x;->p(LX/F9x;)LX/4ce;

    move-result-object v0

    invoke-direct {p0, v0, p1}, LX/F9x;->a(LX/4ce;LX/4cd;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2206480
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized p(LX/F9x;)LX/4ce;
    .locals 1

    .prologue
    .line 2206481
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/F9x;->n:LX/4ce;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized q()LX/4cd;
    .locals 1

    .prologue
    .line 2206482
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/F9x;->p(LX/F9x;)LX/4ce;

    move-result-object v0

    invoke-virtual {v0}, LX/4ce;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F9x;->o:LX/4cd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized r()Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 2206483
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/F9x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2206484
    iget-object v0, p0, LX/F9x;->q:Ljava/net/Proxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206485
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2206486
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized s(LX/F9x;)V
    .locals 5

    .prologue
    .line 2206487
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/F9x;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4iJ;

    invoke-virtual {p0}, LX/F9x;->c()Lorg/apache/http/HttpHost;

    invoke-direct {p0}, LX/F9x;->r()Ljava/net/Proxy;

    move-result-object v1

    .line 2206488
    iget-object v2, v0, LX/4iJ;->a:LX/03V;

    invoke-virtual {v2, v1}, LX/03V;->a(Ljava/net/Proxy;)V

    .line 2206489
    iget-object v0, p0, LX/F9x;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kr;

    .line 2206490
    invoke-interface {v0}, LX/0kr;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2206491
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2206492
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/F9x;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4iM;

    .line 2206493
    iget-object v1, v0, LX/4iM;->c:LX/0dy;

    invoke-interface {v1}, LX/0dy;->c()Lorg/apache/http/HttpHost;

    move-result-object v1

    .line 2206494
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2206495
    iget-object v2, v0, LX/4iM;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 2206496
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    .line 2206497
    if-eqz v2, :cond_1

    .line 2206498
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2206499
    :cond_2
    move-object v2, v3

    .line 2206500
    iget-object v3, v0, LX/4iM;->c:LX/0dy;

    invoke-interface {v3}, LX/0dy;->e()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, LX/4iM;->a(LX/4iM;Lorg/apache/http/HttpHost;Ljava/util/List;Z)Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206501
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/0kr;)V
    .locals 1

    .prologue
    .line 2206502
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 2206503
    :try_start_0
    iget-object v0, p0, LX/F9x;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206504
    :cond_0
    monitor-exit p0

    return-void

    .line 2206505
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/webkit/WebView;)V
    .locals 3

    .prologue
    .line 2206401
    iget-object v0, p0, LX/F9x;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4iM;

    .line 2206402
    if-eqz p1, :cond_0

    .line 2206403
    iget-object v1, v0, LX/4iM;->e:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2206404
    iget-object v1, v0, LX/4iM;->c:LX/0dy;

    invoke-interface {v1}, LX/0dy;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2206405
    iget-object v1, v0, LX/4iM;->c:LX/0dy;

    invoke-interface {v1}, LX/0dy;->c()Lorg/apache/http/HttpHost;

    move-result-object v1

    iget-object v2, v0, LX/4iM;->c:LX/0dy;

    invoke-interface {v2}, LX/0dy;->e()Z

    move-result v2

    .line 2206406
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    invoke-static {v0, v1, p0, v2}, LX/4iM;->a(LX/4iM;Lorg/apache/http/HttpHost;Ljava/util/List;Z)Ljava/util/concurrent/Future;

    .line 2206407
    :cond_0
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 4

    .prologue
    .line 2206408
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/F9x;->p(LX/F9x;)LX/4ce;

    move-result-object v0

    invoke-virtual {v0}, LX/4ce;->isStarting()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2206409
    new-instance v0, Lorg/apache/http/HttpHost;

    const-string v1, "http"

    invoke-direct {v0, p1, p2, v1}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    iput-object v0, p0, LX/F9x;->p:Lorg/apache/http/HttpHost;

    .line 2206410
    new-instance v0, Ljava/net/Proxy;

    sget-object v1, Ljava/net/Proxy$Type;->SOCKS:Ljava/net/Proxy$Type;

    new-instance v2, Ljava/net/InetSocketAddress;

    invoke-direct {v2, p3, p4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-direct {v0, v1, v2}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    iput-object v0, p0, LX/F9x;->q:Ljava/net/Proxy;

    .line 2206411
    iget-object v0, p0, LX/F9x;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F9z;

    .line 2206412
    iget-object v1, v0, LX/F9z;->b:Ljava/util/Map;

    const-string v2, "httpHost"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2206413
    iget-object v1, v0, LX/F9z;->b:Ljava/util/Map;

    const-string v2, "httpPort"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2206414
    iget-object v1, v0, LX/F9z;->b:Ljava/util/Map;

    const-string v2, "socksHost"

    invoke-interface {v1, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2206415
    iget-object v1, v0, LX/F9z;->b:Ljava/util/Map;

    const-string v2, "socksPort"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2206416
    sget-object v0, LX/4ce;->ENABLED:LX/4ce;

    invoke-static {p0, v0}, LX/F9x;->a(LX/F9x;LX/4ce;)V

    .line 2206417
    sget-object v0, LX/4cd;->CHECKING_CONNECTION:LX/4cd;

    invoke-static {p0, v0}, LX/F9x;->a$redex0(LX/F9x;LX/4cd;)V

    .line 2206418
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/F9x;->a(LX/F9x;Z)V

    .line 2206419
    invoke-static {p0}, LX/F9x;->s(LX/F9x;)V

    .line 2206420
    invoke-virtual {p0}, LX/F9x;->n()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206421
    :cond_0
    monitor-exit p0

    return-void

    .line 2206422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2206400
    invoke-static {p0}, LX/F9x;->p(LX/F9x;)LX/4ce;

    move-result-object v0

    invoke-virtual {v0}, LX/4ce;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized b(LX/0kr;)V
    .locals 1

    .prologue
    .line 2206397
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/F9x;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206398
    monitor-exit p0

    return-void

    .line 2206399
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2206395
    invoke-direct {p0}, LX/F9x;->q()LX/4cd;

    move-result-object v0

    .line 2206396
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/4cd;->hasEncounteredError()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized c()Lorg/apache/http/HttpHost;
    .locals 1

    .prologue
    .line 2206391
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/F9x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2206392
    iget-object v0, p0, LX/F9x;->p:Lorg/apache/http/HttpHost;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206393
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2206394
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2206386
    invoke-virtual {p0}, LX/F9x;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2206387
    sget-object v0, LX/F9x;->a:Ljava/lang/Class;

    const-string v1, "Connection check requested when disabled"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2206388
    :goto_0
    return-void

    .line 2206389
    :cond_0
    new-instance v0, LX/F9u;

    invoke-virtual {p0}, LX/F9x;->c()Lorg/apache/http/HttpHost;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/F9u;-><init>(LX/F9x;Lorg/apache/http/HttpHost;)V

    .line 2206390
    iget-object v1, p0, LX/F9x;->b:Landroid/content/Context;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2206385
    const/4 v0, 0x1

    return v0
.end method

.method public final f()Landroid/webkit/WebViewClient;
    .locals 1

    .prologue
    .line 2206384
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 2206382
    invoke-virtual {p0}, LX/F9x;->n()V

    .line 2206383
    return-void
.end method

.method public final declared-synchronized i()V
    .locals 1

    .prologue
    .line 2206379
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/4ce;->STARTING:LX/4ce;

    invoke-static {p0, v0}, LX/F9x;->a(LX/F9x;LX/4ce;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206380
    monitor-exit p0

    return-void

    .line 2206381
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final init()V
    .locals 4

    .prologue
    .line 2206367
    iget-boolean v0, p0, LX/F9x;->r:Z

    if-nez v0, :cond_0

    .line 2206368
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/F9x;->r:Z

    .line 2206369
    iget-object v0, p0, LX/F9x;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FA1;

    .line 2206370
    iget-object v1, v0, LX/FA1;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0sF;->b:LX/0Tn;

    sget-object v3, LX/0sF;->c:LX/0Tn;

    invoke-static {v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/util/Set;LX/0dN;)V

    .line 2206371
    iget-object v1, v0, LX/FA1;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v2, Lcom/facebook/http/onion/impl/OnionPreferenceListener$1;

    invoke-direct {v2, v0}, Lcom/facebook/http/onion/impl/OnionPreferenceListener$1;-><init>(LX/FA1;)V

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(Ljava/lang/Runnable;)V

    .line 2206372
    iget-object v1, v0, LX/FA1;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/F9z;

    .line 2206373
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "onion_orbot_available"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2206374
    invoke-static {v1, v2}, LX/F9z;->a(LX/F9z;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2206375
    iget-object v0, p0, LX/F9x;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GmO;

    .line 2206376
    iget-object v1, v0, LX/GmO;->a:LX/GmQ;

    invoke-interface {p0, v1}, LX/0dx;->a(LX/0kr;)V

    .line 2206377
    iget-object v0, p0, LX/F9x;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1h1;

    invoke-interface {v0, p0}, LX/1h1;->a(LX/1hZ;)V

    .line 2206378
    :cond_0
    return-void
.end method

.method public final declared-synchronized j()V
    .locals 1

    .prologue
    .line 2206362
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, LX/F9x;->a(LX/F9x;Z)V

    .line 2206363
    sget-object v0, LX/4ce;->DISABLED:LX/4ce;

    invoke-static {p0, v0}, LX/F9x;->a(LX/F9x;LX/4ce;)V

    .line 2206364
    invoke-static {p0}, LX/F9x;->s(LX/F9x;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206365
    monitor-exit p0

    return-void

    .line 2206366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()V
    .locals 1

    .prologue
    .line 2206359
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/F9x;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2206360
    monitor-exit p0

    return-void

    .line 2206361
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n()V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2206336
    iget-boolean v0, p0, LX/F9x;->s:Z

    if-eqz v0, :cond_0

    .line 2206337
    :goto_0
    return-void

    .line 2206338
    :cond_0
    invoke-virtual {p0}, LX/F9x;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2206339
    sget-object v0, LX/F9x;->a:Ljava/lang/Class;

    const-string v1, "Integrity check requested when disabled"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2206340
    :cond_1
    iget-object v0, p0, LX/F9x;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FA6;

    .line 2206341
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    const-string v2, "https://www.facebook.com/si/proxy/"

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2206342
    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpGet;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 2206343
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v2

    .line 2206344
    iput-object v1, v2, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2206345
    move-object v1, v2

    .line 2206346
    const-string v2, "Tor integrity checker"

    .line 2206347
    iput-object v2, v1, LX/15E;->c:Ljava/lang/String;

    .line 2206348
    move-object v1, v1

    .line 2206349
    sget-object v2, LX/FA6;->b:Lorg/apache/http/client/ResponseHandler;

    .line 2206350
    iput-object v2, v1, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2206351
    move-object v1, v1

    .line 2206352
    invoke-virtual {v1}, LX/15E;->a()LX/15D;

    move-result-object v1

    .line 2206353
    iget-object v2, v0, LX/FA6;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v2, v1}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v1

    move-object v0, v1

    .line 2206354
    if-nez v0, :cond_2

    .line 2206355
    sget-object v0, LX/F9x;->a:Ljava/lang/Class;

    const-string v1, "Null future returned from integrity checker"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2206356
    :cond_2
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v1

    .line 2206357
    new-instance v2, LX/F9v;

    invoke-direct {v2, p0}, LX/F9v;-><init>(LX/F9x;)V

    iget-object v0, p0, LX/F9x;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/1v3;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2206358
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/F9x;->s:Z

    goto :goto_0
.end method
