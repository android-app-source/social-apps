.class public LX/H9i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:Landroid/content/Context;

.field private final g:LX/CK5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2434552
    const v0, 0x7f0207bb

    sput v0, LX/H9i;->a:I

    .line 2434553
    const v0, 0x7f08150a

    sput v0, LX/H9i;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;LX/0Ot;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/CK5;)V
    .locals 0
    .param p1    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/content/Context;",
            "LX/CK5;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434555
    iput-object p1, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434556
    iput-object p2, p0, LX/H9i;->d:LX/0Ot;

    .line 2434557
    iput-object p3, p0, LX/H9i;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2434558
    iput-object p4, p0, LX/H9i;->f:Landroid/content/Context;

    .line 2434559
    iput-object p5, p0, LX/H9i;->g:LX/CK5;

    .line 2434560
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2434561
    iget-object v0, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->b()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->q()LX/1k1;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    move v5, v4

    .line 2434562
    :goto_0
    new-instance v0, LX/HA7;

    sget v2, LX/H9i;->b:I

    sget v3, LX/H9i;->a:I

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_1
    move v5, v1

    .line 2434563
    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434564
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9i;->b:I

    sget v3, LX/H9i;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2434565
    iget-object v0, p0, LX/H9i;->g:LX/CK5;

    invoke-virtual {v0}, LX/CK5;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2434566
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->b()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->b()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->q()LX/1k1;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->q()LX/1k1;

    move-result-object v2

    invoke-interface {v2}, LX/1k1;->a()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    :goto_1
    iget-object v3, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->q()LX/1k1;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->q()LX/1k1;

    move-result-object v3

    invoke-interface {v3}, LX/1k1;->b()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    :goto_2
    const-string v5, "pages_action_bar_request_ride_action"

    invoke-static {v0, v2, v3, v1, v5}, LX/CK5;->a(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 2434567
    iget-object v0, p0, LX/H9i;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v2, LX/9XI;->EVENT_TAPPED_REQUEST_RIDE:LX/9XI;

    iget-object v3, p0, LX/H9i;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434568
    iget-object v0, p0, LX/H9i;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/H9i;->f:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2434569
    return-void

    :cond_0
    move-object v0, v1

    .line 2434570
    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1

    :cond_2
    move-object v3, v1

    goto :goto_2
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434571
    const/4 v0, 0x0

    return-object v0
.end method
