.class public final LX/GTn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 0

    .prologue
    .line 2355839
    iput-object p1, p0, LX/GTn;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2355840
    iget-object v1, p0, LX/GTn;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2355841
    :goto_0
    iput-object v0, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->V:Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2355842
    iget-object v0, p0, LX/GTn;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->n(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    .line 2355843
    iget-object v0, p0, LX/GTn;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->l(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    .line 2355844
    return-void

    .line 2355845
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2355846
    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2355847
    sget-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->B:Ljava/lang/Class;

    const-string v1, "failed to get inviter short name"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355848
    iget-object v0, p0, LX/GTn;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    const-string v1, "background_location_notification_nux_inviter_short_name_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355849
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2355850
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/GTn;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
