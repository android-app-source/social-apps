.class public final LX/FBj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/Object;

.field public final synthetic b:Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2209705
    iput-object p1, p0, LX/FBj;->b:Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    iput-object p2, p0, LX/FBj;->a:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2209706
    iget-object v0, p0, LX/FBj;->b:Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    iget-object v1, p0, LX/FBj;->a:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;->a$redex0(Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;Ljava/lang/Object;)V

    .line 2209707
    iget-object v0, p0, LX/FBj;->a:Ljava/lang/Object;

    instance-of v0, v0, Lcom/facebook/bookmark/model/Bookmark;

    if-eqz v0, :cond_0

    .line 2209708
    iget-object v1, p0, LX/FBj;->b:Lcom/facebook/katana/ui/bookmark/BookmarkMenuFragment;

    iget-object v0, p0, LX/FBj;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 2209709
    invoke-virtual {v1, v0, v2}, Lcom/facebook/bookmark/ui/BaseBookmarkMenuFragment;->a(Lcom/facebook/bookmark/model/Bookmark;Ljava/lang/String;)V

    .line 2209710
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
