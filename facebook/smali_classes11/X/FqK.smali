.class public final LX/FqK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1PH;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/TimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 0

    .prologue
    .line 2292669
    iput-object p1, p0, LX/FqK;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 2292670
    iget-object v0, p0, LX/FqK;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->aq:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2292671
    iget-object v0, p0, LX/FqK;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->mJ_()V

    .line 2292672
    iget-object v0, p0, LX/FqK;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->I:LX/BQ9;

    iget-object v1, p0, LX/FqK;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v1, v1, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2292673
    iget-wide v7, v1, LX/5SB;->b:J

    move-wide v2, v7

    .line 2292674
    const-string v1, "1"

    iget-object v4, p0, LX/FqK;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v4, v4, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    invoke-virtual {v4}, LX/5SB;->i()Z

    move-result v4

    iget-object v5, p0, LX/FqK;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v5, v5, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    iget-object v6, p0, LX/FqK;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v6, v6, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, LX/BQ9;->c(JLjava/lang/String;LX/9lQ;)V

    .line 2292675
    return-void
.end method
