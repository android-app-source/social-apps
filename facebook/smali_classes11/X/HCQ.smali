.class public LX/HCQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2438631
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2438632
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2438633
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v0, "profile_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "extra_addable_tabs_channel_data"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    .line 2438634
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 2438635
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2438636
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2438637
    const-string v5, "com.facebook.katana.profile.id"

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2438638
    const-string v5, "profile_name"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438639
    const-string v5, "extra_addable_tabs_channel_data"

    invoke-static {v4, v5, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2438640
    new-instance v5, Lcom/facebook/pages/common/editpage/AddTabFragment;

    invoke-direct {v5}, Lcom/facebook/pages/common/editpage/AddTabFragment;-><init>()V

    .line 2438641
    invoke-virtual {v5, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2438642
    move-object v0, v5

    .line 2438643
    return-object v0

    .line 2438644
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
