.class public final LX/F6z;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:LX/F73;


# direct methods
.method public constructor <init>(LX/F73;)V
    .locals 0

    .prologue
    .line 2201038
    iput-object p1, p0, LX/F6z;->a:LX/F73;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2201024
    check-cast p1, LX/2f2;

    .line 2201025
    if-nez p1, :cond_1

    .line 2201026
    :cond_0
    :goto_0
    return-void

    .line 2201027
    :cond_1
    iget-object v0, p0, LX/F6z;->a:LX/F73;

    iget-object v0, v0, LX/F73;->g:Ljava/util/Map;

    iget-wide v2, p1, LX/2f2;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7L;

    .line 2201028
    if-eqz v0, :cond_0

    .line 2201029
    iget-boolean v1, p1, LX/2f2;->c:Z

    if-nez v1, :cond_2

    .line 2201030
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0}, LX/F7L;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 2201031
    :cond_2
    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, LX/F7L;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2201032
    iget-object v1, p0, LX/F6z;->a:LX/F73;

    iget-object v1, v1, LX/F73;->v:LX/0Uh;

    sget v2, LX/Ezz;->a:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/F6z;->a:LX/F73;

    iget-object v1, v1, LX/F73;->h:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2201033
    invoke-virtual {v0}, LX/F7L;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v2, :cond_4

    .line 2201034
    iget-object v0, p0, LX/F6z;->a:LX/F73;

    iget-object v0, v0, LX/F73;->w:LX/2Ku;

    new-instance v1, LX/2zE;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, LX/2zE;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2201035
    :cond_3
    :goto_1
    iget-object v0, p0, LX/F6z;->a:LX/F73;

    const v1, 0x6d396514

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2201036
    :cond_4
    invoke-virtual {v0}, LX/F7L;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_3

    .line 2201037
    iget-object v0, p0, LX/F6z;->a:LX/F73;

    iget-object v0, v0, LX/F73;->w:LX/2Ku;

    new-instance v1, LX/2zE;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LX/2zE;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1
.end method
