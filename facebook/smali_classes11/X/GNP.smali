.class public LX/GNP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/widget/ImageView;

.field public b:I

.field public c:I

.field public d:Landroid/view/ViewGroup;

.field private e:Landroid/graphics/Rect;

.field public f:Landroid/graphics/Rect;

.field public g:Landroid/graphics/Rect;

.field public h:Lcom/facebook/adinterfaces/walkthrough/HoleView;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/widget/TextView;

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public l:I

.field private m:Landroid/view/View;

.field public n:Landroid/widget/TextView;

.field public o:Ljava/lang/String;

.field public p:Landroid/view/View;

.field public q:LX/GG3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GG3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345895
    iput-object p1, p0, LX/GNP;->q:LX/GG3;

    .line 2345896
    return-void
.end method

.method public static a$redex0(LX/GNP;Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v7, 0x33

    const/4 v1, 0x0

    .line 2345918
    const/4 v2, 0x0

    .line 2345919
    :try_start_0
    iget-object v3, p0, LX/GNP;->m:Landroid/view/View;

    iget-object v0, p0, LX/GNP;->k:Ljava/util/List;

    iget v4, p0, LX/GNP;->l:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v5, v0

    .line 2345920
    :goto_0
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2345921
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->callOnClick()Z

    .line 2345922
    :goto_1
    return-void

    :catch_0
    move-object v5, v2

    goto :goto_0

    .line 2345923
    :cond_1
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 2345924
    invoke-virtual {v5, v6}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    .line 2345925
    if-eqz v0, :cond_2

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, LX/GNP;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-gt v0, v2, :cond_2

    iget v0, v6, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/GNP;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-lt v0, v2, :cond_2

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, LX/GNP;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-gt v0, v2, :cond_2

    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, LX/GNP;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    if-le v0, v2, :cond_3

    .line 2345926
    :cond_2
    new-instance v0, LX/GNO;

    invoke-direct {v0, p0, p1}, LX/GNO;-><init>(LX/GNP;Landroid/view/View;)V

    invoke-static {v5, v0}, LX/GMo;->a(Landroid/view/View;LX/GJR;)V

    goto :goto_1

    .line 2345927
    :cond_3
    iget v0, v6, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/GNP;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    iput v0, v6, Landroid/graphics/Rect;->top:I

    .line 2345928
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, LX/GNP;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v2

    iput v0, v6, Landroid/graphics/Rect;->bottom:I

    .line 2345929
    iget-object v0, p0, LX/GNP;->h:Lcom/facebook/adinterfaces/walkthrough/HoleView;

    invoke-virtual {v0, v6}, Lcom/facebook/adinterfaces/walkthrough/HoleView;->a(Landroid/graphics/Rect;)V

    .line 2345930
    iget-object v0, p0, LX/GNP;->k:Ljava/util/List;

    iget v2, p0, LX/GNP;->l:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    .line 2345931
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 2345932
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2345933
    :goto_2
    invoke-virtual {v6}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    iget v2, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    .line 2345934
    iget v2, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    if-ge v2, v3, :cond_6

    .line 2345935
    iget-object v2, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_5

    const/4 v0, 0x1

    .line 2345936
    :goto_3
    move v2, v0

    .line 2345937
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v3, -0x40800000    # -1.0f

    .line 2345938
    packed-switch v2, :pswitch_data_0

    .line 2345939
    :goto_4
    iget-object v0, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2345940
    packed-switch v2, :pswitch_data_1

    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    .line 2345941
    :goto_5
    iget-object v7, p0, LX/GNP;->a:Landroid/widget/ImageView;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8, v6, v5, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v7, v8}, LX/GNR;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2345942
    iget-object v1, p0, LX/GNP;->n:Landroid/widget/TextView;

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v4, v3, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v1, v5}, LX/GNR;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto/16 :goto_1

    .line 2345943
    :cond_4
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2345944
    :pswitch_0
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLeft()I

    move-result v2

    iget-object v3, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTop()I

    move-result v3

    invoke-virtual {v0, v2, v3, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2345945
    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2345946
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2345947
    iget v0, v6, Landroid/graphics/Rect;->right:I

    iget v2, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    div-int/lit8 v2, v0, 0x2

    .line 2345948
    iget v3, v6, Landroid/graphics/Rect;->bottom:I

    .line 2345949
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, LX/GNP;->b:I

    add-int/2addr v0, v4

    move v4, v2

    move v5, v3

    move v6, v2

    move v2, v1

    move v3, v0

    move v0, v1

    .line 2345950
    goto :goto_5

    .line 2345951
    :pswitch_1
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLeft()I

    move-result v2

    iget-object v3, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTop()I

    move-result v3

    invoke-virtual {v0, v2, v3, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2345952
    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2345953
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2345954
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iget v2, p0, LX/GNP;->c:I

    sub-int v4, v0, v2

    .line 2345955
    iget v3, v6, Landroid/graphics/Rect;->bottom:I

    .line 2345956
    iget v0, v6, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, LX/GNP;->b:I

    add-int/2addr v2, v0

    .line 2345957
    iget-object v0, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v0, v6

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v0, v5

    move v5, v3

    move v6, v4

    move v4, v1

    move v3, v2

    move v2, v1

    .line 2345958
    goto/16 :goto_5

    .line 2345959
    :pswitch_2
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getRight()I

    move-result v2

    iget-object v3, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    iget-object v4, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2345960
    const/16 v2, 0x55

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2345961
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2345962
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iget v2, p0, LX/GNP;->c:I

    sub-int v4, v0, v2

    .line 2345963
    iget v0, v6, Landroid/graphics/Rect;->top:I

    iget v2, p0, LX/GNP;->b:I

    sub-int v3, v0, v2

    .line 2345964
    iget-object v0, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 2345965
    iget-object v2, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    iget v5, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v5

    iget v5, p0, LX/GNP;->b:I

    add-int/2addr v2, v5

    move v5, v3

    move v6, v4

    move v3, v1

    move v4, v1

    .line 2345966
    goto/16 :goto_5

    .line 2345967
    :pswitch_3
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLeft()I

    move-result v2

    iget-object v3, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    iget-object v4, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0, v2, v1, v1, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2345968
    const/16 v2, 0x53

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2345969
    iget-object v2, p0, LX/GNP;->n:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2345970
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int v4, v0, v2

    .line 2345971
    iget v0, v6, Landroid/graphics/Rect;->top:I

    iget v2, p0, LX/GNP;->b:I

    sub-int v3, v0, v2

    .line 2345972
    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    .line 2345973
    iget-object v0, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget v5, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v5

    iget v5, p0, LX/GNP;->b:I

    add-int/2addr v0, v5

    move v5, v3

    move v6, v4

    move v3, v1

    move v4, v2

    move v2, v0

    move v0, v1

    goto/16 :goto_5

    .line 2345974
    :cond_5
    const/4 v0, 0x2

    goto/16 :goto_3

    .line 2345975
    :cond_6
    iget-object v2, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-le v0, v2, :cond_7

    const/4 v0, 0x3

    goto/16 :goto_3

    :cond_7
    const/4 v0, 0x4

    goto/16 :goto_3

    .line 2345976
    :pswitch_4
    iget-object v0, p0, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 2345977
    iget-object v0, p0, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    goto/16 :goto_4

    .line 2345978
    :pswitch_5
    iget-object v0, p0, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 2345979
    iget-object v0, p0, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    goto/16 :goto_4

    .line 2345980
    :pswitch_6
    iget-object v0, p0, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 2345981
    iget-object v0, p0, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleY(F)V

    goto/16 :goto_4

    .line 2345982
    :pswitch_7
    iget-object v0, p0, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 2345983
    iget-object v0, p0, LX/GNP;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleY(F)V

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;Landroid/view/View;Ljava/util/List;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2345897
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2345898
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "One of the walkthrough parameters is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2345899
    :cond_1
    iput-object p1, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    .line 2345900
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/GNP;->e:Landroid/graphics/Rect;

    .line 2345901
    iget-object v0, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/GNP;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2345902
    iput-object p2, p0, LX/GNP;->m:Landroid/view/View;

    .line 2345903
    iput-object p3, p0, LX/GNP;->k:Ljava/util/List;

    .line 2345904
    const/4 v0, 0x0

    iput v0, p0, LX/GNP;->l:I

    .line 2345905
    iput-object p4, p0, LX/GNP;->o:Ljava/lang/String;

    .line 2345906
    iget-object v0, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0418

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GNP;->p:Landroid/view/View;

    .line 2345907
    iget-object v0, p0, LX/GNP;->p:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 2345908
    iget-object v0, p0, LX/GNP;->p:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2345909
    :cond_2
    iget-object v2, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0d3184

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2345910
    if-eqz v2, :cond_3

    .line 2345911
    iget-object v3, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2345912
    :cond_3
    iget-object v2, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f031607

    iget-object v4, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2345913
    iget-object v2, p0, LX/GNP;->q:LX/GG3;

    iget-object v3, p0, LX/GNP;->o:Ljava/lang/String;

    .line 2345914
    iget-object v6, v2, LX/GG3;->h:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const-string v7, "enter_flow"

    const-string v8, "walkthrough"

    const/4 v10, 0x0

    move-object v5, v2

    move-object v9, v3

    invoke-static/range {v5 .. v10}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2345915
    iget-object v2, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    const v3, 0x7f0d3184

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, LX/GNP;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2345916
    iget-object v2, p0, LX/GNP;->d:Landroid/view/ViewGroup;

    new-instance v3, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;

    invoke-direct {v3, p0}, Lcom/facebook/adinterfaces/walkthrough/AdInterfacesWalkThrough$1;-><init>(LX/GNP;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 2345917
    return-void
.end method
