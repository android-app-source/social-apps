.class public LX/Fey;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2266957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2266958
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Fey;->a:Ljava/util/List;

    .line 2266959
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;Z)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;Z)",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x6

    const/4 v2, 0x0

    .line 2266960
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v1, v2

    .line 2266961
    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2266962
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;->a()LX/Dv3;

    move-result-object v0

    sget-object v4, LX/Dv3;->SINGLE_MEDIA:LX/Dv3;

    if-ne v0, v4, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-virtual {v0}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2266963
    iget v0, p0, LX/Fey;->b:I

    if-lez v0, :cond_1

    .line 2266964
    iget-object v0, p0, LX/Fey;->a:Ljava/util/List;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2266965
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2266966
    :cond_1
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2266967
    iput v5, p0, LX/Fey;->b:I

    goto :goto_1

    .line 2266968
    :cond_2
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2266969
    iget v0, p0, LX/Fey;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/Fey;->b:I

    .line 2266970
    iget-object v0, p0, LX/Fey;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, LX/Fey;->b:I

    if-gtz v0, :cond_0

    .line 2266971
    iget-object v0, p0, LX/Fey;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2266972
    iput v5, p0, LX/Fey;->b:I

    goto :goto_1

    .line 2266973
    :cond_3
    if-nez p2, :cond_4

    .line 2266974
    iget-object v0, p0, LX/Fey;->a:Ljava/util/List;

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2266975
    iget-object v0, p0, LX/Fey;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2266976
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
