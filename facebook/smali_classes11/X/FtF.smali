.class public LX/FtF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/FtE;

.field public c:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;


# direct methods
.method public constructor <init>()V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2298068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2298069
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FtF;->a:Ljava/util/List;

    .line 2298070
    iget-object v0, p0, LX/FtF;->a:Ljava/util/List;

    new-instance v1, LX/FtE;

    invoke-direct {v1}, LX/FtE;-><init>()V

    iput-object v1, p0, LX/FtF;->b:LX/FtE;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2298071
    iget-object v0, p0, LX/FtF;->a:Ljava/util/List;

    new-instance v1, LX/FtG;

    invoke-direct {v1}, LX/FtG;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2298072
    new-instance v0, LX/FtK;

    invoke-direct {v0}, LX/FtK;-><init>()V

    const-string v1, "initial_client_cursor"

    .line 2298073
    iput-object v1, v0, LX/FtK;->a:Ljava/lang/String;

    .line 2298074
    move-object v0, v0

    .line 2298075
    const/4 v1, 0x1

    .line 2298076
    iput-boolean v1, v0, LX/FtK;->b:Z

    .line 2298077
    move-object v0, v0

    .line 2298078
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 2298079
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2298080
    iget-object v3, v0, LX/FtK;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2298081
    const/4 v5, 0x2

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 2298082
    invoke-virtual {v2, v7, v3}, LX/186;->b(II)V

    .line 2298083
    iget-boolean v3, v0, LX/FtK;->b:Z

    invoke-virtual {v2, v6, v3}, LX/186;->a(IZ)V

    .line 2298084
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2298085
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2298086
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2298087
    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2298088
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2298089
    new-instance v3, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;

    invoke-direct {v3, v2}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;-><init>(LX/15i;)V

    .line 2298090
    move-object v0, v3

    .line 2298091
    iput-object v0, p0, LX/FtF;->c:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;

    .line 2298092
    return-void
.end method

.method public static a(LX/0QB;)LX/FtF;
    .locals 3

    .prologue
    .line 2298093
    const-class v1, LX/FtF;

    monitor-enter v1

    .line 2298094
    :try_start_0
    sget-object v0, LX/FtF;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2298095
    sput-object v2, LX/FtF;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2298096
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2298097
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2298098
    new-instance v0, LX/FtF;

    invoke-direct {v0}, LX/FtF;-><init>()V

    .line 2298099
    move-object v0, v0

    .line 2298100
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2298101
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FtF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2298102
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2298103
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2298104
    iget-object v0, p0, LX/FtF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2298105
    iget-object v0, p0, LX/FtF;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2298106
    :goto_0
    return-object v0

    .line 2298107
    :cond_0
    iget-object v0, p0, LX/FtF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    .line 2298108
    if-nez v0, :cond_1

    .line 2298109
    iget-object v0, p0, LX/FtF;->c:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;

    goto :goto_0

    .line 2298110
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid index: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2298111
    iget-object v0, p0, LX/FtF;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
