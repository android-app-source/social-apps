.class public final LX/FZA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/debug/SearchDebugActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/search/debug/SearchDebugActivity;)V
    .locals 0

    .prologue
    .line 2256962
    iput-object p1, p0, LX/FZA;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2256963
    iget-object v0, p0, LX/FZA;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v0, v0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    .line 2256964
    invoke-static {}, LX/FaF;->values()[LX/FaF;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 2256965
    invoke-virtual {v5}, LX/FaF;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/7CP;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v5

    .line 2256966
    iget-object v6, p0, LX/FZA;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v6, v6, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v6, v5, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2256967
    invoke-interface {v2, v5}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 2256968
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2256969
    :cond_1
    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2256970
    iget-object v0, p0, LX/FZA;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-virtual {v0}, Lcom/facebook/search/debug/SearchDebugActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "Reset all awareness opt-out flags"

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2256971
    const/4 v0, 0x1

    return v0
.end method
