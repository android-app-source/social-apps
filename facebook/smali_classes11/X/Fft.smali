.class public final LX/Fft;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/livefeed/FetchLiveFeedGraphQLModels$FetchLiveFeedStoriesGraphQLModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:LX/Ffv;


# direct methods
.method public constructor <init>(LX/Ffv;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2269129
    iput-object p1, p0, LX/Fft;->c:LX/Ffv;

    iput-object p2, p0, LX/Fft;->a:Ljava/lang/String;

    iput p3, p0, LX/Fft;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2269130
    iget-object v0, p0, LX/Fft;->c:LX/Ffv;

    const-string v1, "android:live-feed:tail"

    iget-object v2, p0, LX/Fft;->c:LX/Ffv;

    iget-object v2, v2, LX/Ffv;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Fft;->a:Ljava/lang/String;

    iget v5, p0, LX/Fft;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/Ffv;->a(LX/Ffv;Ljava/lang/String;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
