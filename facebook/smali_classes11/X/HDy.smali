.class public final LX/HDy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/followpage/graphql/PageUnsubscribeNotificationMutationModels$PageUnsubscribeNotificationMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 0

    .prologue
    .line 2441363
    iput-object p1, p0, LX/HDy;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iput-boolean p2, p0, LX/HDy;->a:Z

    iput-object p3, p0, LX/HDy;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2441364
    iget-object v0, p0, LX/HDy;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-boolean v1, p0, LX/HDy;->a:Z

    iget-object v2, p0, LX/HDy;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v2, v2, LX/HE1;->b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iget-object v3, p0, LX/HDy;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a$redex0(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2441365
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2441358
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2441359
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2441360
    check-cast v0, Lcom/facebook/pages/common/followpage/graphql/PageUnsubscribeNotificationMutationModels$PageUnsubscribeNotificationMutationModel;

    .line 2441361
    iget-object v1, p0, LX/HDy;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/followpage/graphql/PageUnsubscribeNotificationMutationModels$PageUnsubscribeNotificationMutationModel;->a()Lcom/facebook/pages/common/followpage/graphql/PageUnsubscribeNotificationMutationModels$PageUnsubscribeNotificationMutationModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/followpage/graphql/PageUnsubscribeNotificationMutationModels$PageUnsubscribeNotificationMutationModel$PageModel;->a()Z

    move-result v2

    iget-object v3, p0, LX/HDy;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v3, v3, LX/HE1;->b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v0}, Lcom/facebook/pages/common/followpage/graphql/PageUnsubscribeNotificationMutationModels$PageUnsubscribeNotificationMutationModel;->a()Lcom/facebook/pages/common/followpage/graphql/PageUnsubscribeNotificationMutationModels$PageUnsubscribeNotificationMutationModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/followpage/graphql/PageUnsubscribeNotificationMutationModels$PageUnsubscribeNotificationMutationModel$PageModel;->j()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a$redex0(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2441362
    return-void
.end method
