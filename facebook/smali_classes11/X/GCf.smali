.class public LX/GCf;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/GCf;


# direct methods
.method public constructor <init>()V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2329050
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2329051
    sget-object v0, LX/0ax;->e:Ljava/lang/String;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "page_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "storyId"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "referral"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "coupon_promotion_group_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "ad_account_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "scroll_to_section"

    invoke-static {v3}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    .line 2329052
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2329053
    const-string v3, "objective"

    sget-object v9, LX/8wL;->BOOST_POST:LX/8wL;

    invoke-virtual {v2, v3, v9}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2329054
    const-string v3, "title"

    const v9, 0x7f080146

    invoke-virtual {v2, v3, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2329055
    move-object v2, v2

    .line 2329056
    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2329057
    sget-object v0, LX/0ax;->f:Ljava/lang/String;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "page_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "storyId"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "promotion_target_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "placement_extra"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "referral"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "coupon_promotion_group_id"

    invoke-static {v3}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "ad_account_id"

    invoke-static {v3}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "scroll_to_section"

    invoke-static {v3}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    .line 2329058
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2329059
    const-string v3, "objective"

    sget-object v9, LX/8wL;->BOOST_EVENT:LX/8wL;

    invoke-virtual {v2, v3, v9}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2329060
    const-string v3, "title"

    const v9, 0x7f080acc

    invoke-virtual {v2, v3, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2329061
    move-object v2, v2

    .line 2329062
    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2329063
    sget-object v0, LX/0ax;->l:Ljava/lang/String;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "page_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "promotion_target_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "referral"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "coupon_promotion_group_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "ad_account_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "scroll_to_section"

    invoke-static {v3}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    .line 2329064
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2329065
    const-string v3, "objective"

    sget-object v4, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2329066
    const-string v3, "title"

    const v4, 0x7f080b05

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2329067
    move-object v2, v2

    .line 2329068
    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2329069
    sget-object v0, LX/0ax;->g:Ljava/lang/String;

    .line 2329070
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2329071
    const-string v2, "objective"

    sget-object v3, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2329072
    const-string v2, "title"

    const v3, 0x7f080a64

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2329073
    move-object v1, v1

    .line 2329074
    invoke-direct {p0, v0, v1}, LX/GCf;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2329075
    sget-object v0, LX/0ax;->h:Ljava/lang/String;

    .line 2329076
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2329077
    const-string v2, "objective"

    sget-object v3, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2329078
    const-string v2, "title"

    const v3, 0x7f080b03

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2329079
    move-object v1, v1

    .line 2329080
    invoke-direct {p0, v0, v1}, LX/GCf;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2329081
    sget-object v0, LX/0ax;->i:Ljava/lang/String;

    .line 2329082
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2329083
    const-string v2, "objective"

    sget-object v3, LX/8wL;->PAGE_LIKE:LX/8wL;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2329084
    const-string v2, "title"

    const v3, 0x7f080b04

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2329085
    move-object v1, v1

    .line 2329086
    invoke-direct {p0, v0, v1}, LX/GCf;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2329087
    sget-object v0, LX/0ax;->j:Ljava/lang/String;

    .line 2329088
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2329089
    const-string v2, "objective"

    sget-object v3, LX/8wL;->PROMOTE_CTA:LX/8wL;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2329090
    const-string v2, "title"

    const v3, 0x7f080b06

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2329091
    move-object v1, v1

    .line 2329092
    invoke-direct {p0, v0, v1}, LX/GCf;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2329093
    return-void
.end method

.method public static a(LX/0QB;)LX/GCf;
    .locals 3

    .prologue
    .line 2329094
    sget-object v0, LX/GCf;->a:LX/GCf;

    if-nez v0, :cond_1

    .line 2329095
    const-class v1, LX/GCf;

    monitor-enter v1

    .line 2329096
    :try_start_0
    sget-object v0, LX/GCf;->a:LX/GCf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2329097
    if-eqz v2, :cond_0

    .line 2329098
    :try_start_1
    new-instance v0, LX/GCf;

    invoke-direct {v0}, LX/GCf;-><init>()V

    .line 2329099
    move-object v0, v0

    .line 2329100
    sput-object v0, LX/GCf;->a:LX/GCf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2329101
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2329102
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2329103
    :cond_1
    sget-object v0, LX/GCf;->a:LX/GCf;

    return-object v0

    .line 2329104
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2329105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2329106
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " UNKNOWN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2329107
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "page_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "source"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "referral"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "restore_saved_settings"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "coupon_promotion_group_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "ad_account_id"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "scroll_to_section"

    invoke-static {v2}, LX/GCf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2329108
    const-class v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-virtual {p0, v0, v1, p2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2329109
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2329110
    invoke-super {p0, p1, p2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2329111
    if-eqz v0, :cond_0

    .line 2329112
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2329113
    const-string v1, "uri_extra"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2329114
    :cond_0
    return-object v0
.end method
