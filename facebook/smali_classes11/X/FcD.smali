.class public LX/FcD;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/103;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:LX/2Sc;

.field private final d:LX/0wM;

.field public final e:LX/FcV;

.field private final f:LX/FcB;

.field public g:LX/Fje;

.field public h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/FcC;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2262106
    sget-object v0, LX/103;->GROUP:LX/103;

    const-string v1, "rp_group"

    sget-object v2, LX/103;->USER:LX/103;

    const-string v3, "rp_author"

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/FcD;->a:LX/0P1;

    .line 2262107
    const-class v0, LX/FcD;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FcD;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/2Sc;LX/0wM;LX/FcV;LX/FcB;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262195
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2262196
    iput-object v0, p0, LX/FcD;->j:LX/0Px;

    .line 2262197
    iput-object p1, p0, LX/FcD;->c:LX/2Sc;

    .line 2262198
    iput-object p2, p0, LX/FcD;->d:LX/0wM;

    .line 2262199
    iput-object p3, p0, LX/FcD;->e:LX/FcV;

    .line 2262200
    iput-object p4, p0, LX/FcD;->f:LX/FcB;

    .line 2262201
    return-void
.end method

.method public static a(Landroid/os/Bundle;LX/CwB;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/CwB;",
            ")",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2262155
    const-string v0, "filters"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "filters"

    invoke-static {p0, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2262156
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2262157
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_1

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;

    .line 2262158
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel;->a()LX/5uu;

    move-result-object v1

    .line 2262159
    invoke-interface {v1}, LX/5uu;->b()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-interface {v1}, LX/5uu;->b()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;->a()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_a

    new-instance v7, LX/4FP;

    invoke-direct {v7}, LX/4FP;-><init>()V

    invoke-interface {v1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4FP;->a(Ljava/lang/String;)LX/4FP;

    move-result-object v7

    const-string v8, "add"

    invoke-virtual {v7, v8}, LX/4FP;->c(Ljava/lang/String;)LX/4FP;

    move-result-object v7

    invoke-interface {v1}, LX/5uu;->b()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterCurrentValueFragmentModel$CurrentValueModel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4FP;->d(Ljava/lang/String;)LX/4FP;

    move-result-object v7

    invoke-interface {v1}, LX/5uu;->bk_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4FP;->b(Ljava/lang/String;)LX/4FP;

    move-result-object v7

    :goto_1
    move-object v1, v7

    .line 2262160
    if-eqz v1, :cond_0

    .line 2262161
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262162
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2262163
    :cond_1
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2262164
    move-object v1, v0

    .line 2262165
    :goto_2
    const-string v0, "initial_typeahead_query"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "initial_typeahead_query"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQuery;

    move-object v6, v0

    .line 2262166
    :goto_3
    if-eqz v6, :cond_6

    .line 2262167
    iget-boolean v0, v6, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    move v0, v0

    .line 2262168
    if-eqz v0, :cond_6

    sget-object v0, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v6, v0}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_6

    sget-object v0, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v6, v0}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    .line 2262169
    iget-boolean v2, v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->a:Z

    move v0, v2

    .line 2262170
    if-eqz v0, :cond_6

    move v2, v3

    .line 2262171
    :goto_4
    invoke-interface {p1}, LX/CwB;->k()LX/103;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, LX/CwB;->h()LX/0P1;

    move-result-object v0

    sget-object v5, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v5}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {p1}, LX/CwB;->h()LX/0P1;

    move-result-object v0

    sget-object v5, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v5}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    .line 2262172
    iget-boolean v5, v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->a:Z

    move v0, v5

    .line 2262173
    if-eqz v0, :cond_7

    move v5, v3

    .line 2262174
    :goto_5
    if-eqz v2, :cond_8

    .line 2262175
    iget-object v0, v6, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 2262176
    move-object v4, v0

    .line 2262177
    :goto_6
    if-eqz v2, :cond_9

    .line 2262178
    iget-object v0, v6, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2262179
    move-object v3, v0

    .line 2262180
    :goto_7
    if-nez v5, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    sget-object v0, LX/FcD;->a:LX/0P1;

    invoke-virtual {v0, v4}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2262181
    new-instance v2, LX/4FP;

    invoke-direct {v2}, LX/4FP;-><init>()V

    .line 2262182
    const-string v0, "add"

    invoke-virtual {v2, v0}, LX/4FP;->c(Ljava/lang/String;)LX/4FP;

    .line 2262183
    sget-object v0, LX/FcD;->a:LX/0P1;

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/4FP;->a(Ljava/lang/String;)LX/4FP;

    .line 2262184
    invoke-virtual {v2, v3}, LX/4FP;->d(Ljava/lang/String;)LX/4FP;

    .line 2262185
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2262186
    :cond_3
    return-object v1

    .line 2262187
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2262188
    move-object v1, v0

    goto/16 :goto_2

    .line 2262189
    :cond_5
    const/4 v0, 0x0

    move-object v6, v0

    goto/16 :goto_3

    :cond_6
    move v2, v4

    .line 2262190
    goto :goto_4

    :cond_7
    move v5, v4

    .line 2262191
    goto :goto_5

    .line 2262192
    :cond_8
    invoke-interface {p1}, LX/CwB;->k()LX/103;

    move-result-object v0

    move-object v4, v0

    goto :goto_6

    .line 2262193
    :cond_9
    invoke-interface {p1}, LX/CwB;->i()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_7

    :cond_a
    const/4 v7, 0x0

    goto/16 :goto_1
.end method

.method private i()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2262138
    iget-object v0, p0, LX/FcD;->h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    if-nez v0, :cond_0

    .line 2262139
    :goto_0
    return-void

    .line 2262140
    :cond_0
    iget-object v0, p0, LX/FcD;->h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->removeAllViews()V

    .line 2262141
    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2262142
    :cond_1
    iget-object v0, p0, LX/FcD;->h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 2262143
    :cond_2
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2262144
    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_3

    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyH;

    .line 2262145
    iget-object v6, p0, LX/FcD;->e:LX/FcV;

    .line 2262146
    iget-object v1, v0, LX/CyH;->c:LX/4FP;

    move-object v1, v1

    .line 2262147
    invoke-virtual {v1}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v1

    const-string v7, "name"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v6, v1}, LX/FcV;->b(Ljava/lang/String;)LX/FcI;

    move-result-object v1

    invoke-interface {v1, v0}, LX/FcI;->a(LX/CyH;)LX/FcT;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262148
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2262149
    :cond_3
    iget-object v0, p0, LX/FcD;->f:LX/FcB;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/FcD;->k:LX/FcC;

    .line 2262150
    iput-object v1, v0, LX/FcB;->a:LX/0Px;

    .line 2262151
    iput-object v2, v0, LX/FcB;->b:LX/FcC;

    .line 2262152
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2262153
    iget-object v0, p0, LX/FcD;->h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, LX/FcD;->f:LX/FcB;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2262154
    iget-object v0, p0, LX/FcD;->h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/4FP;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4FP;",
            ")",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262127
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2262128
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2262129
    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyH;

    .line 2262130
    iget-object v5, v0, LX/CyH;->c:LX/4FP;

    move-object v5, v5

    .line 2262131
    invoke-virtual {v5, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2262132
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262133
    iget-object v5, v0, LX/CyH;->c:LX/4FP;

    move-object v0, v5

    .line 2262134
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262135
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2262136
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FcD;->b(LX/0Px;)V

    .line 2262137
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2262125
    const/4 v0, 0x0

    iput-object v0, p0, LX/FcD;->g:LX/Fje;

    .line 2262126
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2262108
    iget-object v0, p0, LX/FcD;->g:LX/Fje;

    if-nez v0, :cond_1

    .line 2262109
    iget-object v0, p0, LX/FcD;->c:LX/2Sc;

    sget-object v1, LX/3Ql;->FILTER_CONTROLLER:LX/3Ql;

    const-string v2, "PageView not attached to controller"

    invoke-virtual {v0, v1, v2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2262110
    :cond_0
    :goto_0
    return-void

    .line 2262111
    :cond_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2262112
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    .line 2262113
    invoke-interface {v0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v4

    .line 2262114
    iget-object v5, p0, LX/FcD;->e:LX/FcV;

    invoke-virtual {v5, v4}, LX/FcV;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2262115
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262116
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2262117
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2262118
    iput-object v0, p0, LX/FcD;->i:LX/0Px;

    .line 2262119
    iget-object v0, p0, LX/FcD;->g:LX/Fje;

    .line 2262120
    iget-object v1, v0, LX/Fje;->v:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomLinearLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 2262121
    if-nez v0, :cond_4

    iget-object v0, p0, LX/FcD;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2262122
    iget-object v0, p0, LX/FcD;->g:LX/Fje;

    invoke-virtual {v0}, LX/Fje;->e()V

    goto :goto_0

    .line 2262123
    :cond_4
    iget-object v0, p0, LX/FcD;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262124
    iget-object v0, p0, LX/FcD;->g:LX/Fje;

    invoke-virtual {v0}, LX/Fje;->b()V

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(LX/FcC;)V
    .locals 0

    .prologue
    .line 2262202
    iput-object p1, p0, LX/FcD;->k:LX/FcC;

    .line 2262203
    return-void
.end method

.method public final a(LX/Fje;)V
    .locals 0

    .prologue
    .line 2262082
    iput-object p1, p0, LX/FcD;->g:LX/Fje;

    .line 2262083
    return-void
.end method

.method public final a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 0

    .prologue
    .line 2262084
    iput-object p1, p0, LX/FcD;->h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2262085
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2262086
    const/4 v0, 0x0

    iput-object v0, p0, LX/FcD;->h:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 2262087
    return-void
.end method

.method public final b(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2262088
    iput-object p1, p0, LX/FcD;->j:LX/0Px;

    .line 2262089
    invoke-direct {p0}, LX/FcD;->i()V

    .line 2262090
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2262091
    iget-object v0, p0, LX/FcD;->g:LX/Fje;

    if-nez v0, :cond_1

    .line 2262092
    :cond_0
    :goto_0
    return-void

    .line 2262093
    :cond_1
    iget-object v0, p0, LX/FcD;->i:LX/0Px;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FcD;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2262094
    iget-object v0, p0, LX/FcD;->g:LX/Fje;

    invoke-virtual {v0}, LX/Fje;->e()V

    .line 2262095
    :cond_2
    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    if-eqz v0, :cond_0

    .line 2262096
    invoke-direct {p0}, LX/FcD;->i()V

    goto :goto_0
.end method

.method public final g()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262097
    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    if-nez v0, :cond_0

    .line 2262098
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2262099
    :goto_0
    return-object v0

    .line 2262100
    :cond_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2262101
    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/FcD;->j:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyH;

    .line 2262102
    iget-object v4, v0, LX/CyH;->c:LX/4FP;

    move-object v0, v4

    .line 2262103
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2262104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2262105
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
