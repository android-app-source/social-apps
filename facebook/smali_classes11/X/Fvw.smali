.class public LX/Fvw;
.super LX/62C;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Fv6;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/Fv9;

.field private final c:LX/BQ1;

.field private final d:LX/FwD;

.field private final e:LX/5SB;


# direct methods
.method public constructor <init>(Ljava/util/List;LX/Fv9;LX/BQ1;LX/5SB;LX/FwD;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Fv9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Fv6;",
            ">;",
            "LX/Fv9;",
            "LX/BQ1;",
            "LX/5SB;",
            "LX/FwD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302315
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/62C;-><init>(ZLjava/util/List;)V

    .line 2302316
    iput-object p1, p0, LX/Fvw;->a:Ljava/util/List;

    .line 2302317
    iput-object p2, p0, LX/Fvw;->b:LX/Fv9;

    .line 2302318
    iput-object p3, p0, LX/Fvw;->c:LX/BQ1;

    .line 2302319
    iput-object p4, p0, LX/Fvw;->e:LX/5SB;

    .line 2302320
    iput-object p5, p0, LX/Fvw;->d:LX/FwD;

    .line 2302321
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 2302322
    iget-object v0, p0, LX/Fvw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fv6;

    .line 2302323
    invoke-interface {v0}, LX/Fv6;->b()V

    goto :goto_0

    .line 2302324
    :cond_0
    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2302325
    invoke-super {p0, p1, p2, p3}, LX/62C;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2302326
    iget-object v0, p0, LX/Fvw;->c:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Fvw;->b:LX/Fv9;

    if-eqz v0, :cond_1

    .line 2302327
    iget-object v0, p0, LX/Fvw;->c:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->aa()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fvw;->c:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ac()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fvw;->e:LX/5SB;

    iget-object v2, p0, LX/Fvw;->c:LX/BQ1;

    invoke-static {v0, v2}, LX/FwD;->b(LX/5SB;LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 2302328
    :goto_0
    iget-object v2, p0, LX/Fvw;->b:LX/Fv9;

    invoke-virtual {v2, v0}, LX/Fv9;->b(Z)V

    .line 2302329
    :cond_1
    return-object v1

    .line 2302330
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
