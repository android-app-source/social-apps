.class public final enum LX/Fal;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fal;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fal;

.field public static final enum HDPI:LX/Fal;

.field public static final enum MDPI:LX/Fal;

.field public static final enum XHDPI:LX/Fal;

.field public static final enum XXHDPI:LX/Fal;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2259103
    new-instance v0, LX/Fal;

    const-string v1, "MDPI"

    invoke-direct {v0, v1, v2}, LX/Fal;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fal;->MDPI:LX/Fal;

    .line 2259104
    new-instance v0, LX/Fal;

    const-string v1, "HDPI"

    invoke-direct {v0, v1, v3}, LX/Fal;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fal;->HDPI:LX/Fal;

    .line 2259105
    new-instance v0, LX/Fal;

    const-string v1, "XHDPI"

    invoke-direct {v0, v1, v4}, LX/Fal;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fal;->XHDPI:LX/Fal;

    .line 2259106
    new-instance v0, LX/Fal;

    const-string v1, "XXHDPI"

    invoke-direct {v0, v1, v5}, LX/Fal;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fal;->XXHDPI:LX/Fal;

    .line 2259107
    const/4 v0, 0x4

    new-array v0, v0, [LX/Fal;

    sget-object v1, LX/Fal;->MDPI:LX/Fal;

    aput-object v1, v0, v2

    sget-object v1, LX/Fal;->HDPI:LX/Fal;

    aput-object v1, v0, v3

    sget-object v1, LX/Fal;->XHDPI:LX/Fal;

    aput-object v1, v0, v4

    sget-object v1, LX/Fal;->XXHDPI:LX/Fal;

    aput-object v1, v0, v5

    sput-object v0, LX/Fal;->$VALUES:[LX/Fal;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2259108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fal;
    .locals 1

    .prologue
    .line 2259109
    const-class v0, LX/Fal;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fal;

    return-object v0
.end method

.method public static values()[LX/Fal;
    .locals 1

    .prologue
    .line 2259110
    sget-object v0, LX/Fal;->$VALUES:[LX/Fal;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fal;

    return-object v0
.end method
