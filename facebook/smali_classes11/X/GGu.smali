.class public final LX/GGu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGX;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2334595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2334596
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2334597
    :cond_0
    :goto_0
    return v0

    .line 2334598
    :cond_1
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334599
    sget-object v1, LX/GGw;->a:[I

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v2

    invoke-virtual {v2}, LX/GGB;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2334600
    :pswitch_0
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2334601
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->p()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
