.class public final LX/GBd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 0

    .prologue
    .line 2327129
    iput-object p1, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x7963240f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2327130
    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v2, LX/GBg;->SMS:LX/GBg;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2327131
    const v0, -0x4a42e6de

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2327132
    :goto_0
    return-void

    .line 2327133
    :cond_0
    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v2, LX/GBg;->EMAIL:LX/GBg;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2327134
    const v0, 0x2ecbbebf

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2327135
    :cond_1
    iget-object v2, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v3, LX/GBg;->SMS:LX/GBg;

    if-ne v0, v3, :cond_2

    sget-object v0, LX/GBg;->EMAIL:LX/GBg;

    .line 2327136
    :goto_1
    iput-object v0, v2, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    .line 2327137
    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->A(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327138
    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->B(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327139
    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327140
    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->s$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327141
    iget-object v0, p0, LX/GBd;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    sget-object v2, LX/GBh;->BACKGROUND_SEND:LX/GBh;

    invoke-static {v0, v2}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;LX/GBh;)V

    .line 2327142
    const v0, -0x7a88210f

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2327143
    :cond_2
    sget-object v0, LX/GBg;->SMS:LX/GBg;

    goto :goto_1
.end method
