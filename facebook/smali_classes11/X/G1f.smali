.class public final LX/G1f;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 2312567
    const/16 v18, 0x0

    .line 2312568
    const/16 v17, 0x0

    .line 2312569
    const/16 v16, 0x0

    .line 2312570
    const/4 v15, 0x0

    .line 2312571
    const/4 v14, 0x0

    .line 2312572
    const/4 v13, 0x0

    .line 2312573
    const/4 v12, 0x0

    .line 2312574
    const/4 v11, 0x0

    .line 2312575
    const/4 v10, 0x0

    .line 2312576
    const/4 v9, 0x0

    .line 2312577
    const/4 v8, 0x0

    .line 2312578
    const/4 v7, 0x0

    .line 2312579
    const/4 v6, 0x0

    .line 2312580
    const/4 v5, 0x0

    .line 2312581
    const/4 v4, 0x0

    .line 2312582
    const/4 v3, 0x0

    .line 2312583
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 2312584
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2312585
    const/4 v3, 0x0

    .line 2312586
    :goto_0
    return v3

    .line 2312587
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2312588
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_11

    .line 2312589
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v19

    .line 2312590
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2312591
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    if-eqz v19, :cond_1

    .line 2312592
    const-string v20, "__type__"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "__typename"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 2312593
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v18

    goto :goto_1

    .line 2312594
    :cond_3
    const-string v20, "accessibility_caption"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 2312595
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 2312596
    :cond_4
    const-string v20, "big_profile_image"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 2312597
    invoke-static/range {p0 .. p1}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 2312598
    :cond_5
    const-string v20, "creation_story"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 2312599
    invoke-static/range {p0 .. p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v15

    goto :goto_1

    .line 2312600
    :cond_6
    const-string v20, "dominant_color"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 2312601
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    goto :goto_1

    .line 2312602
    :cond_7
    const-string v20, "focus"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 2312603
    invoke-static/range {p0 .. p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 2312604
    :cond_8
    const-string v20, "friendship_status"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 2312605
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    goto/16 :goto_1

    .line 2312606
    :cond_9
    const-string v20, "id"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 2312607
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 2312608
    :cond_a
    const-string v20, "image"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 2312609
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 2312610
    :cond_b
    const-string v20, "imageHigh"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 2312611
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2312612
    :cond_c
    const-string v20, "imageLow"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 2312613
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 2312614
    :cond_d
    const-string v20, "imageMedium"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_e

    .line 2312615
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 2312616
    :cond_e
    const-string v20, "profile_photo"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_f

    .line 2312617
    invoke-static/range {p0 .. p1}, LX/G1e;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2312618
    :cond_f
    const-string v20, "tiny_profile_image"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_10

    .line 2312619
    invoke-static/range {p0 .. p1}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 2312620
    :cond_10
    const-string v20, "unread_count"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 2312621
    const/4 v3, 0x1

    .line 2312622
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v4

    goto/16 :goto_1

    .line 2312623
    :cond_11
    const/16 v19, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2312624
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2312625
    const/16 v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2312626
    const/16 v17, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2312627
    const/16 v16, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 2312628
    const/4 v15, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 2312629
    const/4 v14, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 2312630
    const/4 v13, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 2312631
    const/4 v12, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 2312632
    const/16 v11, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 2312633
    const/16 v10, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 2312634
    const/16 v9, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 2312635
    const/16 v8, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2312636
    const/16 v7, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 2312637
    const/16 v6, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 2312638
    if-eqz v3, :cond_12

    .line 2312639
    const/16 v3, 0xe

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 2312640
    :cond_12
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 2312641
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2312642
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2312643
    if-eqz v0, :cond_0

    .line 2312644
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312645
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2312646
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2312647
    if-eqz v0, :cond_1

    .line 2312648
    const-string v1, "accessibility_caption"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312649
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2312650
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312651
    if-eqz v0, :cond_2

    .line 2312652
    const-string v1, "big_profile_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312653
    invoke-static {p0, v0, p2}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 2312654
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312655
    if-eqz v0, :cond_3

    .line 2312656
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312657
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2312658
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2312659
    if-eqz v0, :cond_4

    .line 2312660
    const-string v1, "dominant_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312661
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2312662
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312663
    if-eqz v0, :cond_5

    .line 2312664
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312665
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 2312666
    :cond_5
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2312667
    if-eqz v0, :cond_6

    .line 2312668
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312669
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2312670
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2312671
    if-eqz v0, :cond_7

    .line 2312672
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312673
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2312674
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312675
    if-eqz v0, :cond_8

    .line 2312676
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312677
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2312678
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312679
    if-eqz v0, :cond_9

    .line 2312680
    const-string v1, "imageHigh"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312681
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2312682
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312683
    if-eqz v0, :cond_a

    .line 2312684
    const-string v1, "imageLow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312685
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2312686
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312687
    if-eqz v0, :cond_b

    .line 2312688
    const-string v1, "imageMedium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312689
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2312690
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312691
    if-eqz v0, :cond_c

    .line 2312692
    const-string v1, "profile_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312693
    invoke-static {p0, v0, p2}, LX/G1e;->a(LX/15i;ILX/0nX;)V

    .line 2312694
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2312695
    if-eqz v0, :cond_d

    .line 2312696
    const-string v1, "tiny_profile_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312697
    invoke-static {p0, v0, p2}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 2312698
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2312699
    if-eqz v0, :cond_e

    .line 2312700
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2312701
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2312702
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2312703
    return-void
.end method
