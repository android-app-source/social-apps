.class public final LX/GMO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GMR;


# direct methods
.method public constructor <init>(LX/GMR;)V
    .locals 0

    .prologue
    .line 2344461
    iput-object p1, p0, LX/GMO;->a:LX/GMR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, -0x600e29d0

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2344449
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/GMO;->a:LX/GMR;

    .line 2344450
    iget-object v3, v2, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-object v2, v3

    .line 2344451
    sget-object v3, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION:LX/8wL;

    const v4, 0x7f080a65

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v7, 0x0

    invoke-static {v1, v3, v4, v7}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    move-object v3, v2

    .line 2344452
    check-cast v3, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v3}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v3

    .line 2344453
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v7

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object p1

    invoke-virtual {v3, v7, p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2344454
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v7

    invoke-virtual {v3, v7}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2344455
    const-string v7, "data"

    invoke-virtual {v4, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2344456
    move-object v1, v4

    .line 2344457
    iget-object v2, p0, LX/GMO;->a:LX/GMR;

    .line 2344458
    iget-object v3, v2, LX/GHg;->b:LX/GCE;

    move-object v2, v3

    .line 2344459
    new-instance v3, LX/GFS;

    const/4 v4, 0x7

    invoke-direct {v3, v1, v4, v6}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v2, v3}, LX/GCE;->a(LX/8wN;)V

    .line 2344460
    const v1, 0x43f93fb0

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
