.class public final synthetic LX/GXB;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2363131
    invoke-static {}, LX/GXL;->values()[LX/GXL;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GXB;->b:[I

    :try_start_0
    sget-object v0, LX/GXB;->b:[I

    sget-object v1, LX/GXL;->SOLD_OUT:LX/GXL;

    invoke-virtual {v1}, LX/GXL;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, LX/GXB;->b:[I

    sget-object v1, LX/GXL;->NOT_AVAILABLE:LX/GXL;

    invoke-virtual {v1}, LX/GXL;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    .line 2363132
    :goto_1
    invoke-static {}, LX/GXJ;->values()[LX/GXJ;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/GXB;->a:[I

    :try_start_2
    sget-object v0, LX/GXB;->a:[I

    sget-object v1, LX/GXJ;->OFFSITE:LX/GXJ;

    invoke-virtual {v1}, LX/GXJ;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v0, LX/GXB;->a:[I

    sget-object v1, LX/GXJ;->CONTACT_MERCHANT:LX/GXJ;

    invoke-virtual {v1}, LX/GXJ;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, LX/GXB;->a:[I

    sget-object v1, LX/GXJ;->ONSITE:LX/GXJ;

    invoke-virtual {v1}, LX/GXJ;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    goto :goto_4

    :catch_1
    goto :goto_3

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_0
.end method
