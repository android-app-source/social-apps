.class public LX/Fm5;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fm5;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2282005
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2282006
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2282007
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->FUNDRAISER_PAGE_INVITER_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2282008
    const-string v1, "is_sticky_header_off"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2282009
    sget-object v1, LX/0ax;->gO:Ljava/lang/String;

    const-string v2, "{fundraiser_campaign_id}"

    const-string v3, "{source}"

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2282010
    sget-object v1, LX/0ax;->gP:Ljava/lang/String;

    const-string v2, "{fundraiser_campaign_id}"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2282011
    return-void
.end method

.method public static a(LX/0QB;)LX/Fm5;
    .locals 4

    .prologue
    .line 2281991
    sget-object v0, LX/Fm5;->a:LX/Fm5;

    if-nez v0, :cond_1

    .line 2281992
    const-class v1, LX/Fm5;

    monitor-enter v1

    .line 2281993
    :try_start_0
    sget-object v0, LX/Fm5;->a:LX/Fm5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2281994
    if-eqz v2, :cond_0

    .line 2281995
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2281996
    new-instance p0, LX/Fm5;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/Fm5;-><init>(LX/0ad;)V

    .line 2281997
    move-object v0, p0

    .line 2281998
    sput-object v0, LX/Fm5;->a:LX/Fm5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2281999
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2282000
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2282001
    :cond_1
    sget-object v0, LX/Fm5;->a:LX/Fm5;

    return-object v0

    .line 2282002
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2282003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2282004
    const/4 v0, 0x1

    return v0
.end method
