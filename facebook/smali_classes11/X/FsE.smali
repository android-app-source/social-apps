.class public LX/FsE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Zp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<+",
            "Landroid/os/Parcelable;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/1Zp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<+",
            "Landroid/os/Parcelable;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/1Zp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/1Zp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>(LX/1Zp;LX/1Zp;LX/1Zp;LX/1Zp;)V
    .locals 0
    .param p1    # LX/1Zp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/1Zp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1Zp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/1Zp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<+",
            "Landroid/os/Parcelable;",
            ">;>;",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<+",
            "Landroid/os/Parcelable;",
            ">;>;",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;",
            ">;>;",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2296665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296666
    iput-object p1, p0, LX/FsE;->a:LX/1Zp;

    .line 2296667
    iput-object p2, p0, LX/FsE;->b:LX/1Zp;

    .line 2296668
    iput-object p3, p0, LX/FsE;->c:LX/1Zp;

    .line 2296669
    iput-object p4, p0, LX/FsE;->d:LX/1Zp;

    .line 2296670
    return-void
.end method
