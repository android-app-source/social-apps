.class public final LX/FzO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FzH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FzH",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$ProfileInfoTypeaheadSearchQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FzP;


# direct methods
.method public constructor <init>(LX/FzP;)V
    .locals 0

    .prologue
    .line 2307858
    iput-object p1, p0, LX/FzO;->a:LX/FzP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2307859
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2307860
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2307861
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2307862
    check-cast v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$ProfileInfoTypeaheadSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$ProfileInfoTypeaheadSearchQueryModel;->a()Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$ProfileInfoTypeaheadSearchQueryModel$EligibleHubsModel;

    move-result-object v0

    .line 2307863
    if-eqz v0, :cond_0

    .line 2307864
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$ProfileInfoTypeaheadSearchQueryModel$EligibleHubsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2307865
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/ProfileInfoTypeaheadSearchGraphQLModels$TypeaheadResultPageModel;

    .line 2307866
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2307867
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2307868
    :cond_0
    return-object v2
.end method
