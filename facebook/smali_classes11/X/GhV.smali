.class public final LX/GhV;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/2km;

.field public final synthetic c:LX/E2b;

.field public final synthetic d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic e:Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;LX/0Px;LX/2km;LX/E2b;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2384270
    iput-object p1, p0, LX/GhV;->e:Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;

    iput-object p2, p0, LX/GhV;->a:LX/0Px;

    iput-object p3, p0, LX/GhV;->b:LX/2km;

    iput-object p4, p0, LX/GhV;->c:LX/E2b;

    iput-object p5, p0, LX/GhV;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2384271
    iget-object v0, p0, LX/GhV;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/GhV;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9o7;

    .line 2384272
    iget-object v3, p0, LX/GhV;->e:Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;

    iget-object v3, v3, Lcom/facebook/gametime/ui/components/partdefinition/GametimeRecentPlaysComponentPartDefinition;->d:Lcom/facebook/gametime/ui/components/partdefinition/GametimeSportsPlayUnitComponentPartDefinition;

    new-instance v4, LX/GhZ;

    sget-object v5, LX/Ghb;->WITH_BORDER:LX/Ghb;

    invoke-direct {v4, v0, v5}, LX/GhZ;-><init>(LX/9o7;LX/Ghb;)V

    invoke-virtual {p1, v3, v4}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2384273
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2384274
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 2384275
    iget-object v0, p0, LX/GhV;->b:LX/2km;

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/GhV;->c:LX/E2b;

    iget-object v2, p0, LX/GhV;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2c;

    .line 2384276
    iput p1, v0, LX/E2c;->d:I

    .line 2384277
    iget-object v0, p0, LX/GhV;->b:LX/2km;

    check-cast v0, LX/3U9;

    invoke-interface {v0}, LX/3U9;->n()LX/2ja;

    move-result-object v0

    iget-object v1, p0, LX/GhV;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2384278
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2384279
    iget-object v2, p0, LX/GhV;->d:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2384280
    iget-object p0, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, p0

    .line 2384281
    invoke-virtual {v0, v1, v2}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384282
    return-void
.end method
