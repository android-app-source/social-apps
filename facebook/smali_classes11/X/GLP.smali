.class public final LX/GLP;
.super LX/GF9;
.source ""


# instance fields
.field public final synthetic a:LX/GIs;


# direct methods
.method public constructor <init>(LX/GIs;)V
    .locals 0

    .prologue
    .line 2342777
    iput-object p1, p0, LX/GLP;->a:LX/GIs;

    invoke-direct {p0}, LX/GF9;-><init>()V

    return-void
.end method

.method private a(LX/GF8;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2342778
    iget-object v0, p1, LX/GF8;->a:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v3, v0

    .line 2342779
    iget-object v0, p0, LX/GLP;->a:LX/GIs;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2342780
    iget-object v4, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v0, v4

    .line 2342781
    if-eqz v0, :cond_0

    move v0, v1

    .line 2342782
    :goto_0
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v3, v4, :cond_1

    move v3, v1

    .line 2342783
    :goto_1
    iget-object v5, p0, LX/GLP;->a:LX/GIs;

    if-nez v0, :cond_2

    if-eqz v3, :cond_2

    move v4, v1

    :goto_2
    invoke-virtual {v5, v4}, LX/GIr;->e(Z)V

    .line 2342784
    iget-object v5, p0, LX/GLP;->a:LX/GIs;

    if-nez v0, :cond_3

    if-eqz v3, :cond_3

    move v4, v1

    :goto_3
    invoke-virtual {v5, v4}, LX/GIr;->a(Z)V

    .line 2342785
    iget-object v4, p0, LX/GLP;->a:LX/GIs;

    if-nez v0, :cond_4

    if-eqz v3, :cond_4

    move v3, v1

    :goto_4
    invoke-virtual {v4, v3}, LX/GIr;->c(Z)V

    .line 2342786
    iget-object v3, p0, LX/GLP;->a:LX/GIs;

    if-nez v0, :cond_5

    :goto_5
    invoke-virtual {v3, v1}, LX/GIr;->b(Z)V

    .line 2342787
    iget-object v0, p0, LX/GLP;->a:LX/GIs;

    invoke-virtual {v0}, LX/GIr;->b()V

    .line 2342788
    return-void

    :cond_0
    move v0, v2

    .line 2342789
    goto :goto_0

    :cond_1
    move v3, v2

    .line 2342790
    goto :goto_1

    :cond_2
    move v4, v2

    .line 2342791
    goto :goto_2

    :cond_3
    move v4, v2

    .line 2342792
    goto :goto_3

    :cond_4
    move v3, v2

    .line 2342793
    goto :goto_4

    :cond_5
    move v1, v2

    .line 2342794
    goto :goto_5
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2342795
    check-cast p1, LX/GF8;

    invoke-direct {p0, p1}, LX/GLP;->a(LX/GF8;)V

    return-void
.end method
