.class public final LX/Fuz;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/Fsr;

.field public final synthetic b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

.field public final synthetic c:Z


# direct methods
.method public constructor <init>(LX/Fsr;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;Z)V
    .locals 0

    .prologue
    .line 2300690
    iput-object p1, p0, LX/Fuz;->a:LX/Fsr;

    iput-object p2, p0, LX/Fuz;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    iput-boolean p3, p0, LX/Fuz;->c:Z

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2300691
    iget-object v0, p0, LX/Fuz;->a:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->r()LX/FrC;

    move-result-object v0

    iget-object v1, p0, LX/Fuz;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/FrC;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;Z)V

    .line 2300692
    iget-boolean v0, p0, LX/Fuz;->c:Z

    if-eqz v0, :cond_0

    .line 2300693
    iget-object v0, p0, LX/Fuz;->a:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->ll_()V

    .line 2300694
    :cond_0
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2300695
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2300696
    return-void
.end method
