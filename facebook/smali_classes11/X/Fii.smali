.class public final LX/Fii;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Fii;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Fig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Fij;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2275691
    const/4 v0, 0x0

    sput-object v0, LX/Fii;->a:LX/Fii;

    .line 2275692
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Fii;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2275672
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2275673
    new-instance v0, LX/Fij;

    invoke-direct {v0}, LX/Fij;-><init>()V

    iput-object v0, p0, LX/Fii;->c:LX/Fij;

    .line 2275674
    return-void
.end method

.method public static c(LX/1De;)LX/Fig;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2275675
    new-instance v1, LX/Fih;

    invoke-direct {v1}, LX/Fih;-><init>()V

    .line 2275676
    sget-object v2, LX/Fii;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fig;

    .line 2275677
    if-nez v2, :cond_0

    .line 2275678
    new-instance v2, LX/Fig;

    invoke-direct {v2}, LX/Fig;-><init>()V

    .line 2275679
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Fig;->a$redex0(LX/Fig;LX/1De;IILX/Fih;)V

    .line 2275680
    move-object v1, v2

    .line 2275681
    move-object v0, v1

    .line 2275682
    return-object v0
.end method

.method public static declared-synchronized q()LX/Fii;
    .locals 2

    .prologue
    .line 2275683
    const-class v1, LX/Fii;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Fii;->a:LX/Fii;

    if-nez v0, :cond_0

    .line 2275684
    new-instance v0, LX/Fii;

    invoke-direct {v0}, LX/Fii;-><init>()V

    sput-object v0, LX/Fii;->a:LX/Fii;

    .line 2275685
    :cond_0
    sget-object v0, LX/Fii;->a:LX/Fii;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2275686
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 2275687
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const p0, 0x7f0a06ff

    invoke-virtual {v0, p0}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const p0, 0x7f0b17d3

    invoke-interface {v0, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 p0, 0x4

    invoke-interface {v0, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2275688
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2275689
    invoke-static {}, LX/1dS;->b()V

    .line 2275690
    const/4 v0, 0x0

    return-object v0
.end method
