.class public final LX/H12;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/mobileconfig/ui/MainFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/mobileconfig/ui/MainFragment;)V
    .locals 0

    .prologue
    .line 2414706
    iput-object p1, p0, LX/H12;->a:Lcom/facebook/mobileconfig/ui/MainFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x286330be

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2414707
    iget-object v0, p0, LX/H12;->a:Lcom/facebook/mobileconfig/ui/MainFragment;

    iget-object v0, v0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    .line 2414708
    iget-object v2, p0, LX/H12;->a:Lcom/facebook/mobileconfig/ui/MainFragment;

    iget-object v2, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->b:LX/0Ww;

    invoke-virtual {v2}, LX/0Ww;->clearOverrides()V

    .line 2414709
    invoke-virtual {v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a()V

    .line 2414710
    const-string v2, "Deleted overrides. Restart the app for changes to take effect."

    invoke-virtual {v0, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->b(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 2414711
    const v0, -0x52ec7390

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
