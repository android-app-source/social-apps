.class public LX/Gm9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final g:Ljava/lang/String;

.field private static volatile i:LX/Gm9;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field public h:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2391594
    const-class v0, LX/Gm9;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Gm9;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2391595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2391596
    iput-object p1, p0, LX/Gm9;->h:LX/03V;

    .line 2391597
    return-void
.end method

.method public static a(LX/0QB;)LX/Gm9;
    .locals 4

    .prologue
    .line 2391598
    sget-object v0, LX/Gm9;->i:LX/Gm9;

    if-nez v0, :cond_1

    .line 2391599
    const-class v1, LX/Gm9;

    monitor-enter v1

    .line 2391600
    :try_start_0
    sget-object v0, LX/Gm9;->i:LX/Gm9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2391601
    if-eqz v2, :cond_0

    .line 2391602
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2391603
    new-instance p0, LX/Gm9;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-direct {p0, v3}, LX/Gm9;-><init>(LX/03V;)V

    .line 2391604
    move-object v0, p0

    .line 2391605
    sput-object v0, LX/Gm9;->i:LX/Gm9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2391606
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2391607
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2391608
    :cond_1
    sget-object v0, LX/Gm9;->i:LX/Gm9;

    return-object v0

    .line 2391609
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2391610
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
