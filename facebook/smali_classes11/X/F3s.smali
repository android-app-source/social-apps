.class public final LX/F3s;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:J

.field public g:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPurposes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "groupPurposes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possibleJoinApprovalSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possibleJoinApprovalSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possiblePostPermissionSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possiblePostPermissionSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possibleVisibilitySettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "possibleVisibilitySettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:I

.field public x:Z

.field public y:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2195229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2195230
    return-void
.end method

.method public static a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)LX/F3s;
    .locals 4

    .prologue
    .line 2195231
    new-instance v0, LX/F3s;

    invoke-direct {v0}, LX/F3s;-><init>()V

    .line 2195232
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->j()Z

    move-result v1

    iput-boolean v1, v0, LX/F3s;->a:Z

    .line 2195233
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->k()Z

    move-result v1

    iput-boolean v1, v0, LX/F3s;->b:Z

    .line 2195234
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->l()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/F3s;->c:LX/15i;

    iput v1, v0, LX/F3s;->d:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2195235
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/F3s;->e:Ljava/lang/String;

    .line 2195236
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->n()J

    move-result-wide v2

    iput-wide v2, v0, LX/F3s;->f:J

    .line 2195237
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->o()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/F3s;->g:LX/15i;

    iput v1, v0, LX/F3s;->h:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2195238
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iput-object v2, v0, LX/F3s;->i:LX/15i;

    iput v1, v0, LX/F3s;->j:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2195239
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/F3s;->k:Ljava/lang/String;

    .line 2195240
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v1

    iput-object v1, v0, LX/F3s;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 2195241
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->s()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/F3s;->m:Ljava/lang/String;

    .line 2195242
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->t()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;

    move-result-object v1

    iput-object v1, v0, LX/F3s;->n:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;

    .line 2195243
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->u()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    iput-object v2, v0, LX/F3s;->o:LX/15i;

    iput v1, v0, LX/F3s;->p:I

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2195244
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->v()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_4
    iput-object v2, v0, LX/F3s;->q:LX/15i;

    iput v1, v0, LX/F3s;->r:I

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2195245
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->w()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_5
    iput-object v2, v0, LX/F3s;->s:LX/15i;

    iput v1, v0, LX/F3s;->t:I

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 2195246
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v1

    iput-object v1, v0, LX/F3s;->u:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 2195247
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->y()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    iput-object v1, v0, LX/F3s;->v:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2195248
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->z()I

    move-result v1

    iput v1, v0, LX/F3s;->w:I

    .line 2195249
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->A()Z

    move-result v1

    iput-boolean v1, v0, LX/F3s;->x:Z

    .line 2195250
    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    iput-object v1, v0, LX/F3s;->y:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2195251
    return-object v0

    .line 2195252
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0

    .line 2195253
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v0

    .line 2195254
    :catchall_2
    move-exception v0

    :try_start_8
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v0

    .line 2195255
    :catchall_3
    move-exception v0

    :try_start_9
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v0

    .line 2195256
    :catchall_4
    move-exception v0

    :try_start_a
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v0

    .line 2195257
    :catchall_5
    move-exception v0

    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;
    .locals 20

    .prologue
    .line 2195258
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 2195259
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/F3s;->c:LX/15i;

    move-object/from16 v0, p0

    iget v5, v0, LX/F3s;->d:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v3, -0x12e352ce

    invoke-static {v4, v5, v3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    move-result-object v3

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2195260
    move-object/from16 v0, p0

    iget-object v4, v0, LX/F3s;->e:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2195261
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    move-object/from16 v0, p0

    iget-object v6, v0, LX/F3s;->g:LX/15i;

    move-object/from16 v0, p0

    iget v7, v0, LX/F3s;->h:I

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v5, 0x2b805edb

    invoke-static {v6, v7, v5}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2195262
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_2
    move-object/from16 v0, p0

    iget-object v6, v0, LX/F3s;->i:LX/15i;

    move-object/from16 v0, p0

    iget v7, v0, LX/F3s;->j:I

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const v5, -0x2030d109

    invoke-static {v6, v7, v5}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2195263
    move-object/from16 v0, p0

    iget-object v5, v0, LX/F3s;->k:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2195264
    move-object/from16 v0, p0

    iget-object v5, v0, LX/F3s;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 2195265
    move-object/from16 v0, p0

    iget-object v5, v0, LX/F3s;->m:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2195266
    move-object/from16 v0, p0

    iget-object v5, v0, LX/F3s;->n:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2195267
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_3
    move-object/from16 v0, p0

    iget-object v6, v0, LX/F3s;->o:LX/15i;

    move-object/from16 v0, p0

    iget v7, v0, LX/F3s;->p:I

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const v5, -0x31b8e1a6

    invoke-static {v6, v7, v5}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 2195268
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_4
    move-object/from16 v0, p0

    iget-object v6, v0, LX/F3s;->q:LX/15i;

    move-object/from16 v0, p0

    iget v7, v0, LX/F3s;->r:I

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    const v5, 0x5bce65ca

    invoke-static {v6, v7, v5}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 2195269
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_5
    move-object/from16 v0, p0

    iget-object v6, v0, LX/F3s;->s:LX/15i;

    move-object/from16 v0, p0

    iget v7, v0, LX/F3s;->t:I

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    const v5, -0x166dc189

    invoke-static {v6, v7, v5}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 2195270
    move-object/from16 v0, p0

    iget-object v5, v0, LX/F3s;->u:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    .line 2195271
    move-object/from16 v0, p0

    iget-object v5, v0, LX/F3s;->v:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 2195272
    move-object/from16 v0, p0

    iget-object v5, v0, LX/F3s;->y:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v2, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 2195273
    const/16 v5, 0x13

    invoke-virtual {v2, v5}, LX/186;->c(I)V

    .line 2195274
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/F3s;->a:Z

    invoke-virtual {v2, v5, v6}, LX/186;->a(IZ)V

    .line 2195275
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/F3s;->b:Z

    invoke-virtual {v2, v5, v6}, LX/186;->a(IZ)V

    .line 2195276
    const/4 v5, 0x2

    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 2195277
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 2195278
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-wide v4, v0, LX/F3s;->f:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2195279
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 2195280
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 2195281
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 2195282
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 2195283
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 2195284
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 2195285
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v14}, LX/186;->b(II)V

    .line 2195286
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v15}, LX/186;->b(II)V

    .line 2195287
    const/16 v3, 0xd

    move/from16 v0, v16

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 2195288
    const/16 v3, 0xe

    move/from16 v0, v17

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 2195289
    const/16 v3, 0xf

    move/from16 v0, v18

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 2195290
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget v4, v0, LX/F3s;->w:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 2195291
    const/16 v3, 0x11

    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/F3s;->x:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 2195292
    const/16 v3, 0x12

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, LX/186;->b(II)V

    .line 2195293
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 2195294
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 2195295
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 2195296
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2195297
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2195298
    new-instance v3, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-direct {v3, v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;-><init>(LX/15i;)V

    .line 2195299
    return-object v3

    .line 2195300
    :catchall_0
    move-exception v2

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v2

    .line 2195301
    :catchall_1
    move-exception v2

    :try_start_7
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v2

    .line 2195302
    :catchall_2
    move-exception v2

    :try_start_8
    monitor-exit v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v2

    .line 2195303
    :catchall_3
    move-exception v2

    :try_start_9
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v2

    .line 2195304
    :catchall_4
    move-exception v2

    :try_start_a
    monitor-exit v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    throw v2

    .line 2195305
    :catchall_5
    move-exception v2

    :try_start_b
    monitor-exit v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v2
.end method
