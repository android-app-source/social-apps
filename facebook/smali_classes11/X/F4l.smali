.class public final LX/F4l;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V
    .locals 0

    .prologue
    .line 2197211
    iput-object p1, p0, LX/F4l;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 2197212
    iget-object v0, p0, LX/F4l;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    .line 2197213
    const/4 v2, 0x0

    .line 2197214
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2197215
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2197216
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->b:LX/0kL;

    new-instance v3, LX/27k;

    const p0, 0x7f082f5f

    invoke-direct {v3, p0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    move v1, v2

    .line 2197217
    :goto_0
    move v1, v1

    .line 2197218
    if-eqz v1, :cond_0

    .line 2197219
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    sget-object v2, LX/F52;->Privacy:LX/F52;

    invoke-virtual {v1, v2}, LX/F53;->a(LX/F52;)V

    .line 2197220
    :cond_0
    return-void

    .line 2197221
    :cond_1
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    const/4 p0, 0x0

    .line 2197222
    invoke-virtual {v1}, LX/F53;->d()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, LX/F53;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_2
    move v3, p0

    .line 2197223
    :goto_1
    move v1, v3

    .line 2197224
    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197225
    iget-object v3, v1, LX/F53;->r:Ljava/lang/String;

    move-object v1, v3

    .line 2197226
    if-nez v1, :cond_4

    .line 2197227
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f082f5e

    .line 2197228
    :goto_2
    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->b:LX/0kL;

    new-instance p0, LX/27k;

    invoke-direct {p0, v1}, LX/27k;-><init>(I)V

    invoke-virtual {v3, p0}, LX/0kL;->b(LX/27k;)LX/27l;

    move v1, v2

    .line 2197229
    goto :goto_0

    .line 2197230
    :cond_3
    const v1, 0x7f082f5d

    goto :goto_2

    .line 2197231
    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    .line 2197232
    :cond_5
    iget-object v3, v1, LX/F53;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8QL;

    .line 2197233
    instance-of p2, v3, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    if-eqz p2, :cond_7

    check-cast v3, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    .line 2197234
    iget-boolean p2, v3, Lcom/facebook/groups/memberpicker/MemberPickerToken;->f:Z

    move v3, p2

    .line 2197235
    if-nez v3, :cond_6

    .line 2197236
    :cond_7
    const/4 v3, 0x1

    goto :goto_1

    :cond_8
    move v3, p0

    .line 2197237
    goto :goto_1
.end method
