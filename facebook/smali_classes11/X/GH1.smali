.class public final LX/GH1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGX;


# instance fields
.field public final synthetic a:LX/GH3;


# direct methods
.method public constructor <init>(LX/GH3;)V
    .locals 0

    .prologue
    .line 2334664
    iput-object p1, p0, LX/GH1;->a:LX/GH3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 4
    .param p1    # Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 2334665
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->D()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->D()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 2334666
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->REJECTED:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->FINISHED:LX/GGB;

    if-eq v1, v2, :cond_2

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->EXTENDABLE:LX/GGB;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
