.class public final LX/Fdm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;)V
    .locals 0

    .prologue
    .line 2264057
    iput-object p1, p0, LX/Fdm;->a:Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x1f6552b5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2264058
    iget-object v0, p0, LX/Fdm;->a:Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwL;

    .line 2264059
    iget-object v2, p0, LX/Fdm;->a:Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    iget-object v2, v2, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->l:LX/Fd6;

    .line 2264060
    iget-object v4, v2, LX/Fd6;->l:LX/Fc6;

    .line 2264061
    iput-object v0, v4, LX/Fc6;->h:LX/CwL;

    .line 2264062
    iget-object v4, v2, LX/Fd6;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2264063
    iget-object v4, v2, LX/Fd6;->x:LX/Fd7;

    const v5, -0x1228e8ed

    invoke-static {v4, v5}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2264064
    iget-object v4, v2, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2264065
    iget-object v4, v2, LX/Fd6;->m:LX/0P1;

    .line 2264066
    iget-object v5, v0, LX/CwL;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2264067
    invoke-virtual {v4, v5}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2264068
    iget-object v5, v2, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v2}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v4, v2, LX/Fd6;->m:LX/0P1;

    .line 2264069
    iget-object v7, v0, LX/CwL;->b:Ljava/lang/String;

    move-object v7, v7

    .line 2264070
    invoke-virtual {v4, v7}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v6, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2264071
    :goto_0
    iget-object v4, v2, LX/Fd6;->w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->smoothScrollToPosition(I)V

    .line 2264072
    iget-object v4, v2, LX/Fd6;->l:LX/Fc6;

    iget-object v5, v2, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v5}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/Fc6;->a(Ljava/lang/String;)V

    .line 2264073
    iget-object v0, p0, LX/Fdm;->a:Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->l:LX/Fd6;

    invoke-virtual {v0, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2264074
    const v0, -0x44a2f452

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2264075
    :cond_0
    iget-object v4, v2, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    .line 2264076
    iget-object v5, v0, LX/CwL;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2264077
    invoke-virtual {v4, v5}, Lcom/facebook/ui/search/SearchEditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
