.class public LX/FL6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2226067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226068
    return-void
.end method

.method private static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2226071
    if-nez p0, :cond_0

    const-string v0, "00000000"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2226072
    check-cast p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2226073
    const-string v0, "t_%s/threadcustomization"

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/ModifyThreadParams;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2226074
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2226075
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226076
    iget-object v2, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-object v2, v2

    .line 2226077
    iget-boolean v3, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->i:Z

    move v3, v3

    .line 2226078
    if-eqz v3, :cond_0

    .line 2226079
    iget v3, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    sget-object v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget v4, v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    if-ne v3, v4, :cond_7

    iget v3, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    sget-object v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget v4, v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    if-ne v3, v4, :cond_7

    iget v3, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    sget-object v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget v4, v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    if-ne v3, v4, :cond_7

    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 2226080
    if-eqz v3, :cond_2

    .line 2226081
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "clear_theme"

    const-string v5, "true"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226082
    :cond_0
    :goto_1
    iget-boolean v3, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->j:Z

    move v3, v3

    .line 2226083
    if-eqz v3, :cond_1

    .line 2226084
    iget-object v3, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    sget-object v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    move v3, v3

    .line 2226085
    if-eqz v3, :cond_6

    .line 2226086
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "clear_icon"

    const-string v4, "true"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226087
    :cond_1
    :goto_2
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "source"

    .line 2226088
    iget-object v4, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->m:Ljava/lang/String;

    move-object v4, v4

    .line 2226089
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226090
    new-instance v2, LX/14O;

    invoke-direct {v2}, LX/14O;-><init>()V

    const-string v3, "setThreadTheme"

    .line 2226091
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2226092
    move-object v2, v2

    .line 2226093
    const-string v3, "POST"

    .line 2226094
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2226095
    move-object v2, v2

    .line 2226096
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2226097
    move-object v0, v2

    .line 2226098
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2226099
    move-object v0, v0

    .line 2226100
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2226101
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2226102
    move-object v0, v0

    .line 2226103
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2226104
    :cond_2
    iget v3, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    if-eqz v3, :cond_3

    .line 2226105
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "background_color"

    iget v5, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    invoke-static {v5}, LX/FL6;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226106
    :cond_3
    iget v3, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    if-eqz v3, :cond_4

    .line 2226107
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "incoming_bubble_color"

    iget v5, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    invoke-static {v5}, LX/FL6;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226108
    :cond_4
    iget v3, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    if-eqz v3, :cond_5

    .line 2226109
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "outgoing_bubble_color"

    iget v5, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    invoke-static {v5}, LX/FL6;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226110
    :cond_5
    iget v3, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->e:I

    if-eqz v3, :cond_0

    .line 2226111
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "theme_color"

    iget v5, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->e:I

    invoke-static {v5}, LX/FL6;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 2226112
    :cond_6
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "emoji"

    .line 2226113
    iget-object v5, v2, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    move-object v2, v5

    .line 2226114
    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2226069
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2226070
    const/4 v0, 0x0

    return-object v0
.end method
