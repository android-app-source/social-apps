.class public final LX/Fw3;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

.field private final b:Z

.field private final c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/TimelineProfileImageFrameController;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 2302588
    iput-object p1, p0, LX/Fw3;->a:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    invoke-direct {p0}, LX/1cC;-><init>()V

    .line 2302589
    iput-boolean p2, p0, LX/Fw3;->b:Z

    .line 2302590
    iput-object p3, p0, LX/Fw3;->c:Landroid/view/View;

    .line 2302591
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2302577
    iget-object v0, p0, LX/Fw3;->a:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    iget-boolean v1, p0, LX/Fw3;->b:Z

    .line 2302578
    iget-object p0, v0, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->f:LX/BQB;

    .line 2302579
    if-eqz v1, :cond_1

    iget-object p1, p0, LX/BQB;->f:LX/BPC;

    .line 2302580
    iget-object v0, p1, LX/BPC;->d:LX/BPA;

    move-object p1, v0

    .line 2302581
    sget-object v0, LX/BPA;->PHOTO_NOT_LOADED:LX/BPA;

    if-ne p1, v0, :cond_1

    .line 2302582
    iget-object p1, p0, LX/BQB;->c:LX/BQD;

    const-string v0, "TimelineLoadProfilePicPreview"

    invoke-virtual {p1, v0}, LX/BQD;->a(Ljava/lang/String;)V

    .line 2302583
    :cond_0
    :goto_0
    return-void

    .line 2302584
    :cond_1
    iget-object p1, p0, LX/BQB;->f:LX/BPC;

    .line 2302585
    iget-object v0, p1, LX/BPC;->d:LX/BPA;

    move-object p1, v0

    .line 2302586
    sget-object v0, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-eq p1, v0, :cond_0

    .line 2302587
    iget-object p1, p0, LX/BQB;->c:LX/BQD;

    const-string v0, "TimelineLoadProfilePic"

    invoke-virtual {p1, v0}, LX/BQD;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2302562
    check-cast p2, LX/1ln;

    const/4 v0, 0x0

    .line 2302563
    if-eqz p2, :cond_1

    invoke-virtual {p2}, LX/1ln;->g()I

    move-result v1

    .line 2302564
    :goto_0
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/1ln;->h()I

    move-result v0

    .line 2302565
    :cond_0
    iget-object v2, p0, LX/Fw3;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 2302566
    iget-object v3, p0, LX/Fw3;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 2302567
    if-gt v2, v1, :cond_2

    if-gt v3, v0, :cond_2

    sget-object v0, LX/BQ6;->FULL:LX/BQ6;

    .line 2302568
    :goto_1
    iget-object v1, p0, LX/Fw3;->a:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    iget-object v1, v1, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    .line 2302569
    iput-object v0, v1, LX/BQ1;->i:LX/BQ6;

    .line 2302570
    iget-object v0, p0, LX/Fw3;->a:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    iget-boolean v1, p0, LX/Fw3;->b:Z

    .line 2302571
    if-eqz v1, :cond_3

    sget-object v2, LX/BPA;->PHOTO_LOW_RES:LX/BPA;

    .line 2302572
    :goto_2
    invoke-static {v0, v2}, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->a(Lcom/facebook/timeline/header/TimelineProfileImageFrameController;LX/BPA;)V

    .line 2302573
    return-void

    :cond_1
    move v1, v0

    .line 2302574
    goto :goto_0

    .line 2302575
    :cond_2
    sget-object v0, LX/BQ6;->SCALED_UP:LX/BQ6;

    goto :goto_1

    .line 2302576
    :cond_3
    sget-object v2, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2302557
    iget-object v0, p0, LX/Fw3;->a:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    iget-boolean v1, p0, LX/Fw3;->b:Z

    .line 2302558
    if-eqz v1, :cond_0

    sget-object p0, LX/BPA;->PHOTO_LOW_RES_FAILED:LX/BPA;

    .line 2302559
    :goto_0
    invoke-static {v0, p0}, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->a(Lcom/facebook/timeline/header/TimelineProfileImageFrameController;LX/BPA;)V

    .line 2302560
    return-void

    .line 2302561
    :cond_0
    sget-object p0, LX/BPA;->PHOTO_HIGH_RES_FAILED:LX/BPA;

    goto :goto_0
.end method
