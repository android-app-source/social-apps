.class public abstract enum LX/GQZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GQZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GQZ;

.field public static final enum AGREED_TO_ALDRIN_TOS:LX/GQZ;

.field public static final enum AGREED_TO_GENERAL_TOS:LX/GQZ;

.field public static final enum VIEW_ALDRIN_TOS:LX/GQZ;

.field public static final enum VIEW_BLOCK_INTERSTITIAL:LX/GQZ;

.field public static final enum VIEW_GENERAL_TOS:LX/GQZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2350243
    new-instance v0, LX/GQa;

    const-string v1, "VIEW_ALDRIN_TOS"

    invoke-direct {v0, v1, v2}, LX/GQa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQZ;->VIEW_ALDRIN_TOS:LX/GQZ;

    .line 2350244
    new-instance v0, LX/GQb;

    const-string v1, "AGREED_TO_ALDRIN_TOS"

    invoke-direct {v0, v1, v3}, LX/GQb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQZ;->AGREED_TO_ALDRIN_TOS:LX/GQZ;

    .line 2350245
    new-instance v0, LX/GQc;

    const-string v1, "VIEW_BLOCK_INTERSTITIAL"

    invoke-direct {v0, v1, v4}, LX/GQc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQZ;->VIEW_BLOCK_INTERSTITIAL:LX/GQZ;

    .line 2350246
    new-instance v0, LX/GQd;

    const-string v1, "VIEW_GENERAL_TOS"

    invoke-direct {v0, v1, v5}, LX/GQd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQZ;->VIEW_GENERAL_TOS:LX/GQZ;

    .line 2350247
    new-instance v0, LX/GQe;

    const-string v1, "AGREED_TO_GENERAL_TOS"

    invoke-direct {v0, v1, v6}, LX/GQe;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQZ;->AGREED_TO_GENERAL_TOS:LX/GQZ;

    .line 2350248
    const/4 v0, 0x5

    new-array v0, v0, [LX/GQZ;

    sget-object v1, LX/GQZ;->VIEW_ALDRIN_TOS:LX/GQZ;

    aput-object v1, v0, v2

    sget-object v1, LX/GQZ;->AGREED_TO_ALDRIN_TOS:LX/GQZ;

    aput-object v1, v0, v3

    sget-object v1, LX/GQZ;->VIEW_BLOCK_INTERSTITIAL:LX/GQZ;

    aput-object v1, v0, v4

    sget-object v1, LX/GQZ;->VIEW_GENERAL_TOS:LX/GQZ;

    aput-object v1, v0, v5

    sget-object v1, LX/GQZ;->AGREED_TO_GENERAL_TOS:LX/GQZ;

    aput-object v1, v0, v6

    sput-object v0, LX/GQZ;->$VALUES:[LX/GQZ;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2350240
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GQZ;
    .locals 1

    .prologue
    .line 2350242
    const-class v0, LX/GQZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GQZ;

    return-object v0
.end method

.method public static values()[LX/GQZ;
    .locals 1

    .prologue
    .line 2350241
    sget-object v0, LX/GQZ;->$VALUES:[LX/GQZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GQZ;

    return-object v0
.end method


# virtual methods
.method public abstract getIntent()Landroid/content/Intent;
.end method
