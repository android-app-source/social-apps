.class public final LX/G2E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

.field public final synthetic b:LX/1Fb;

.field public final synthetic c:Lcom/facebook/timeline/protiles/model/ProtileModel;

.field public final synthetic d:Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;LX/1Fb;Lcom/facebook/timeline/protiles/model/ProtileModel;)V
    .locals 0

    .prologue
    .line 2313707
    iput-object p1, p0, LX/G2E;->d:Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    iput-object p2, p0, LX/G2E;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    iput-object p3, p0, LX/G2E;->b:LX/1Fb;

    iput-object p4, p0, LX/G2E;->c:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x205da030

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2313708
    instance-of v0, p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2313709
    iget-object v0, p0, LX/G2E;->d:Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->b:LX/G2N;

    move-object v1, p1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/G2E;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/G2E;->b:LX/1Fb;

    iget-object v4, p0, LX/G2E;->c:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/model/ProtileModel;->q()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/G2E;->c:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-virtual/range {v0 .. v5}, LX/G2N;->a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;LX/1Fb;Ljava/lang/String;Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    .line 2313710
    iget-object v0, p0, LX/G2E;->d:Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/protiles/rows/ProtilesMosaicSinglePhotoPartDefinition;->c:LX/1K9;

    iget-object v1, p0, LX/G2E;->c:Lcom/facebook/timeline/protiles/model/ProtileModel;

    iget-object v2, p0, LX/G2E;->a:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    invoke-static {v1, v2}, LX/G1D;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)LX/G1D;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 2313711
    const v0, 0x4cfe1944    # 1.33220896E8f

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
