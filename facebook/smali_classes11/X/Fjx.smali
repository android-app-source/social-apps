.class public final enum LX/Fjx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fjx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fjx;

.field public static final enum DEFAULT:LX/Fjx;

.field public static final enum FORCE_UPDATE_NOTIFY_USER:LX/Fjx;

.field public static final enum FORCE_UPDATE_SILENT:LX/Fjx;

.field public static final enum NOTIFY_USER:LX/Fjx;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2278019
    new-instance v0, LX/Fjx;

    const-string v1, "NOTIFY_USER"

    invoke-direct {v0, v1, v6, v3}, LX/Fjx;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Fjx;->NOTIFY_USER:LX/Fjx;

    new-instance v0, LX/Fjx;

    const-string v1, "FORCE_UPDATE_NOTIFY_USER"

    invoke-direct {v0, v1, v3, v4}, LX/Fjx;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Fjx;->FORCE_UPDATE_NOTIFY_USER:LX/Fjx;

    .line 2278020
    new-instance v0, LX/Fjx;

    const-string v1, "FORCE_UPDATE_SILENT"

    invoke-direct {v0, v1, v4, v5}, LX/Fjx;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Fjx;->FORCE_UPDATE_SILENT:LX/Fjx;

    new-instance v0, LX/Fjx;

    const-string v1, "DEFAULT"

    sget-object v2, LX/Fjx;->NOTIFY_USER:LX/Fjx;

    iget v2, v2, LX/Fjx;->value:I

    invoke-direct {v0, v1, v5, v2}, LX/Fjx;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Fjx;->DEFAULT:LX/Fjx;

    .line 2278021
    const/4 v0, 0x4

    new-array v0, v0, [LX/Fjx;

    sget-object v1, LX/Fjx;->NOTIFY_USER:LX/Fjx;

    aput-object v1, v0, v6

    sget-object v1, LX/Fjx;->FORCE_UPDATE_NOTIFY_USER:LX/Fjx;

    aput-object v1, v0, v3

    sget-object v1, LX/Fjx;->FORCE_UPDATE_SILENT:LX/Fjx;

    aput-object v1, v0, v4

    sget-object v1, LX/Fjx;->DEFAULT:LX/Fjx;

    aput-object v1, v0, v5

    sput-object v0, LX/Fjx;->$VALUES:[LX/Fjx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2278022
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/Fjx;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fjx;
    .locals 1

    .prologue
    .line 2278023
    const-class v0, LX/Fjx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fjx;

    return-object v0
.end method

.method public static values()[LX/Fjx;
    .locals 1

    .prologue
    .line 2278024
    sget-object v0, LX/Fjx;->$VALUES:[LX/Fjx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fjx;

    return-object v0
.end method
