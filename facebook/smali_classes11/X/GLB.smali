.class public LX/GLB;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public b:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public d:Landroid/view/View;

.field public final e:LX/2U3;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/lang/String;

.field public k:LX/GEK;

.field public l:LX/1Ck;


# direct methods
.method public constructor <init>(LX/GEK;LX/1Ck;LX/2U3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2342173
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2342174
    const/4 v0, 0x0

    iput-object v0, p0, LX/GLB;->h:Ljava/lang/String;

    .line 2342175
    iput-object p1, p0, LX/GLB;->k:LX/GEK;

    .line 2342176
    iput-object p2, p0, LX/GLB;->l:LX/1Ck;

    .line 2342177
    iput-object p3, p0, LX/GLB;->e:LX/2U3;

    .line 2342178
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GLB;->f:Ljava/util/List;

    .line 2342179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GLB;->g:Ljava/util/ArrayList;

    .line 2342180
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2342260
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/GLB;->f:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2342261
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2342262
    iget-object v0, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    .line 2342263
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2342264
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2342265
    iget-object v2, p0, LX/GLB;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->getId()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->c(I)V

    .line 2342266
    const/4 v0, 0x0

    iput-object v0, p0, LX/GLB;->h:Ljava/lang/String;

    .line 2342267
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2342268
    :cond_1
    return-void
.end method

.method public static b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2342246
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2342247
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;->l()LX/2uF;

    move-result-object v0

    .line 2342248
    invoke-interface {v0}, LX/3Sb;->b()LX/2sN;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2342249
    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2342250
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2342251
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 2342252
    invoke-virtual {v1, v0, v5}, LX/15i;->o(II)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_1
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v9

    move v3, v4

    move v2, v5

    :goto_2
    if-ge v3, v9, :cond_2

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2342253
    if-eqz v2, :cond_1

    const-string v2, ""

    :goto_3
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2342254
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v4

    goto :goto_2

    .line 2342255
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2342256
    move-object v1, v0

    goto :goto_1

    .line 2342257
    :cond_1
    const-string v2, ", "

    goto :goto_3

    .line 2342258
    :cond_2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2342259
    :cond_3
    return-object v6
.end method

.method public static b(LX/GLB;)V
    .locals 3

    .prologue
    .line 2342242
    iget-object v0, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2342243
    iget-object v1, p0, LX/GLB;->d:Landroid/view/View;

    iget-object v2, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;->a()I

    move-result v0

    if-ge v2, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2342244
    return-void

    .line 2342245
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2342231
    iget-object v0, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2342232
    iget-object v1, p0, LX/GLB;->i:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/GLB;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2342233
    iget-object v0, p0, LX/GLB;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/GLB;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;

    .line 2342234
    iget-object v3, p0, LX/GLB;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2342235
    iget-object v3, p0, LX/GLB;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, LX/GLB;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    move-result-object v0

    .line 2342236
    iget-object v3, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2342237
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2342238
    :cond_0
    iput-object v6, p0, LX/GLB;->i:Ljava/util/ArrayList;

    .line 2342239
    iput-object v6, p0, LX/GLB;->j:Ljava/lang/String;

    .line 2342240
    invoke-static {p0}, LX/GLB;->b(LX/GLB;)V

    .line 2342241
    :cond_1
    return-void
.end method

.method public static d(LX/GLB;)V
    .locals 8

    .prologue
    .line 2342210
    iget-object v0, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    .line 2342211
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->a()V

    goto :goto_0

    .line 2342212
    :cond_0
    iget-object v0, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2342213
    iget-object v0, p0, LX/GLB;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2342214
    iget-object v0, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2342215
    :cond_1
    const/4 v0, 0x0

    .line 2342216
    :goto_1
    move v0, v0

    .line 2342217
    if-nez v0, :cond_2

    .line 2342218
    iget-object v0, p0, LX/GLB;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2342219
    :goto_2
    return-void

    .line 2342220
    :cond_2
    iget-object v0, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v2

    .line 2342221
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;

    .line 2342222
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v5

    .line 2342223
    invoke-static {v0}, LX/GLB;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;)Ljava/util/ArrayList;

    move-result-object v6

    .line 2342224
    iget-object v7, p0, LX/GLB;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0, v5, v6}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    move-result-object v0

    .line 2342225
    iget-object v5, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2342226
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2342227
    :cond_3
    invoke-static {p0}, LX/GLB;->b(LX/GLB;)V

    .line 2342228
    iget-object v0, p0, LX/GLB;->d:Landroid/view/View;

    new-instance v1, LX/GL9;

    invoke-direct {v1, p0, v2}, LX/GL9;-><init>(LX/GLB;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2342229
    invoke-direct {p0}, LX/GLB;->c()V

    .line 2342230
    iget-object v0, p0, LX/GLB;->h:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/GLB;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2342269
    invoke-super {p0}, LX/GHg;->a()V

    .line 2342270
    iget-object v0, p0, LX/GLB;->l:LX/1Ck;

    const-string v1, "fetch_saved_audiences_task_key"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2342271
    iput-object v2, p0, LX/GLB;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    .line 2342272
    iput-object v2, p0, LX/GLB;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2342273
    iput-object v2, p0, LX/GLB;->d:Landroid/view/View;

    .line 2342274
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2342199
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2342200
    iget-object v0, p0, LX/GLB;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2342201
    :cond_0
    :goto_0
    return-void

    .line 2342202
    :cond_1
    iget-object v0, p0, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    .line 2342203
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2342204
    const-string v1, "savedAudienceTargeting"

    .line 2342205
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2342206
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2342207
    :cond_3
    iget-object v0, p0, LX/GLB;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2342208
    const-string v0, "fetchedSavedAudienceAccount"

    iget-object v1, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v1}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2342209
    const-string v0, "fetchedSavedAudience"

    iget-object v1, p0, LX/GLB;->g:Ljava/util/ArrayList;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342198
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GLB;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2342196
    iput-object p1, p0, LX/GLB;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2342197
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 1
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342188
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2342189
    iput-object p2, p0, LX/GLB;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2342190
    iput-object p1, p0, LX/GLB;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    .line 2342191
    const v0, 0x7f0d03dc

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GLB;->d:Landroid/view/View;

    .line 2342192
    invoke-static {p0}, LX/GLB;->d(LX/GLB;)V

    .line 2342193
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2342194
    new-instance p1, LX/GLA;

    invoke-direct {p1, p0}, LX/GLA;-><init>(LX/GLB;)V

    invoke-virtual {v0, p1}, LX/GCE;->a(LX/8wQ;)V

    .line 2342195
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342181
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2342182
    const-string v0, "savedAudienceTargeting"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GLB;->h:Ljava/lang/String;

    .line 2342183
    const-string v0, "fetchedSavedAudience"

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, LX/GLB;->i:Ljava/util/ArrayList;

    .line 2342184
    const-string v0, "fetchedSavedAudienceAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GLB;->j:Ljava/lang/String;

    .line 2342185
    invoke-direct {p0}, LX/GLB;->c()V

    .line 2342186
    iget-object v0, p0, LX/GLB;->h:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/GLB;->a(Ljava/lang/String;)V

    .line 2342187
    return-void
.end method
