.class public final LX/FE9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FE8;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V
    .locals 0

    .prologue
    .line 2216032
    iput-object p1, p0, LX/FE9;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 2

    .prologue
    .line 2216033
    iget-object v0, p0, LX/FE9;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    .line 2216034
    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    .line 2216035
    iput-object p1, v1, Lcom/facebook/messaging/event/sending/EventMessageParams;->c:Ljava/util/Calendar;

    .line 2216036
    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->e:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->c(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216037
    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    if-eqz v1, :cond_0

    .line 2216038
    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    iget-object p0, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    invoke-virtual {v1, p0}, LX/FEF;->a(Lcom/facebook/messaging/event/sending/EventMessageParams;)V

    .line 2216039
    :cond_0
    return-void
.end method
