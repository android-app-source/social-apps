.class public LX/Gq0;
.super LX/Cow;
.source ""


# instance fields
.field public k:LX/Go0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2396058
    move-object v0, p1

    check-cast v0, LX/Ctg;

    invoke-direct {p0, v0, p1}, LX/Cow;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 2396059
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Gq0;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v0

    check-cast v0, LX/Go0;

    iput-object v0, p0, LX/Gq0;->k:LX/Go0;

    .line 2396060
    return-void
.end method


# virtual methods
.method public final a(LX/Ctg;LX/CrN;Z)LX/CqX;
    .locals 3

    .prologue
    .line 2396061
    new-instance v0, LX/Gqs;

    new-instance v1, LX/CrK;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CrK;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p1, v1}, LX/Gqs;-><init>(LX/Ctg;LX/CrK;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2396062
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 2396063
    invoke-interface {v0, p1}, LX/Ctg;->setOverlayBackgroundColor(I)V

    .line 2396064
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2396065
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Gq0;->a(I)V

    .line 2396066
    invoke-super {p0, p1}, LX/Cow;->a(Landroid/os/Bundle;)V

    .line 2396067
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2396068
    invoke-super/range {p0 .. p9}, LX/Cow;->a(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IILcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Ljava/lang/String;)V

    .line 2396069
    iget-object v0, p0, LX/Cos;->a:LX/Ctg;

    move-object v0, v0

    .line 2396070
    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    check-cast v0, LX/Gqs;

    .line 2396071
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, LX/Cow;->o:I

    int-to-float v2, v2

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    move v1, v1

    .line 2396072
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    iget p1, p0, LX/Cow;->p:I

    int-to-float p1, p1

    invoke-static {v2, p1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    move v2, v2

    .line 2396073
    iput v1, v0, LX/Gqs;->a:I

    .line 2396074
    iput v2, v0, LX/Gqs;->b:I

    .line 2396075
    return-void
.end method
