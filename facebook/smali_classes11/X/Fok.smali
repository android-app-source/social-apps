.class public final LX/Fok;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 0

    .prologue
    .line 2290107
    iput-object p1, p0, LX/Fok;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x7b68b748

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2290104
    iget-object v1, p0, LX/Fok;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Fok;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-nez v1, :cond_0

    .line 2290105
    iget-object v1, p0, LX/Fok;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-static {v1}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290106
    :cond_0
    const v1, 0x60dd3121

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
