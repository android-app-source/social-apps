.class public LX/Gex;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gex",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376376
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2376377
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gex;->b:LX/0Zi;

    .line 2376378
    iput-object p1, p0, LX/Gex;->a:LX/0Ot;

    .line 2376379
    return-void
.end method

.method public static a(LX/0QB;)LX/Gex;
    .locals 4

    .prologue
    .line 2376357
    const-class v1, LX/Gex;

    monitor-enter v1

    .line 2376358
    :try_start_0
    sget-object v0, LX/Gex;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376359
    sput-object v2, LX/Gex;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376360
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376361
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376362
    new-instance v3, LX/Gex;

    const/16 p0, 0x2108

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gex;-><init>(LX/0Ot;)V

    .line 2376363
    move-object v0, v3

    .line 2376364
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376365
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gex;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376366
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376367
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 9

    .prologue
    .line 2376368
    check-cast p1, LX/Gew;

    .line 2376369
    iget-object v0, p0, LX/Gex;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;

    iget-object v1, p1, LX/Gew;->d:LX/25E;

    iget-object v2, p1, LX/Gew;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v3, p1, LX/Gew;->g:LX/2dx;

    .line 2376370
    invoke-interface {v1}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    .line 2376371
    iget-object v4, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GeG;

    invoke-static {v1, v2}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v7

    invoke-interface {v1}, LX/25E;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v8, "feed_pyml"

    invoke-virtual {v4, v6, v7, v5, v8}, LX/GeG;->a(Lcom/facebook/graphql/model/GraphQLPage;LX/162;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2376372
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2376373
    iget-object v4, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec$1;

    invoke-direct {v5, v0, v3}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec$1;-><init>(Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;LX/2dx;)V

    const-wide/16 v6, 0x1f4

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v6, v7, v8}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2376374
    :cond_0
    return-void

    .line 2376375
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2376324
    check-cast p2, LX/Gew;

    .line 2376325
    iget-object v0, p0, LX/Gex;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;

    iget-object v2, p2, LX/Gew;->a:LX/1Pp;

    iget-object v3, p2, LX/Gew;->b:LX/1f9;

    iget-object v4, p2, LX/Gew;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v5, p2, LX/Gew;->d:LX/25E;

    iget-boolean v6, p2, LX/Gew;->e:Z

    iget-boolean v7, p2, LX/Gew;->f:Z

    move-object v1, p1

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2376326
    invoke-interface {v5}, LX/16p;->t()Lcom/facebook/graphql/model/SponsoredImpression;

    move-result-object v8

    .line 2376327
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/facebook/graphql/model/SponsoredImpression;->k()Z

    move-result v8

    if-eqz v8, :cond_0

    move v8, v9

    .line 2376328
    :goto_0
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p1, 0x2

    invoke-interface {p0, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p0

    const p1, 0x7f0a0438

    invoke-interface {p0, p1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object p1

    if-eqz v6, :cond_1

    invoke-static {v0, v1, v5, v3, v2}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->a(Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;LX/1De;LX/25E;LX/1f9;LX/1Pp;)LX/1Di;

    move-result-object p0

    :goto_1
    invoke-interface {p1, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    invoke-interface {p1, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    const/4 p1, 0x6

    const p2, 0x7f0b0917

    invoke-interface {v10, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v10

    const p1, 0x7f0b107f

    invoke-interface {v10, v9, p1}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v9

    const/4 v10, 0x3

    const p1, 0x7f0b1080

    invoke-interface {v9, v10, p1}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v9

    invoke-static {v0, v1, v4, v5, v8}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->a(Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;LX/1De;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/25E;Z)LX/1Di;

    move-result-object v8

    invoke-interface {v9, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    const/4 p2, 0x1

    .line 2376329
    invoke-static {v1, v5}, LX/Gdi;->a(Landroid/content/Context;LX/25E;)Ljava/lang/String;

    move-result-object v9

    .line 2376330
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    const p1, 0x7f0b004e

    invoke-virtual {v10, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v10

    const/4 p1, 0x0

    invoke-virtual {v10, p1}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v10

    sget-object p1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v10, p1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v10

    const p1, 0x7f0a043b

    invoke-virtual {v10, p1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const v10, 0x7f0b1082

    invoke-interface {v9, p2, v10}, LX/1Di;->g(II)LX/1Di;

    move-result-object v9

    move-object v9, v9

    .line 2376331
    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-interface {v8, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v8

    invoke-interface {p0, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    .line 2376332
    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v9

    const v10, 0x7f0a015a

    invoke-virtual {v9, v10}, LX/25Q;->i(I)LX/25Q;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const v10, 0x7f0b0033

    invoke-interface {v9, v10}, LX/1Di;->i(I)LX/1Di;

    move-result-object v9

    const/4 v10, 0x4

    invoke-interface {v9, v10}, LX/1Di;->b(I)LX/1Di;

    move-result-object v9

    const/4 v10, 0x7

    const p0, 0x7f0b0917

    invoke-interface {v9, v10, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v9

    move-object v9, v9

    .line 2376333
    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    .line 2376334
    invoke-interface {v5}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v9

    .line 2376335
    iget-object v10, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeCardComponentSpec;->c:LX/1vg;

    invoke-virtual {v10, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v10

    const p0, 0x7f020abc

    invoke-virtual {v10, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v10

    if-eqz v9, :cond_2

    const v9, 0x7f0a00d2

    :goto_2
    invoke-virtual {v10, v9}, LX/2xv;->j(I)LX/2xv;

    move-result-object v9

    invoke-virtual {v9}, LX/1n6;->b()LX/1dc;

    move-result-object v9

    .line 2376336
    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    const/16 v10, 0x8

    const p0, 0x7f0b107e

    invoke-interface {v9, v10, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v9

    const/4 v10, 0x2

    invoke-interface {v9, v10}, LX/1Di;->b(I)LX/1Di;

    move-result-object v10

    if-eqz v7, :cond_3

    const/4 v9, 0x0

    :goto_3
    invoke-interface {v10, v9}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v9

    move-object v9, v9

    .line 2376337
    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    move-object v0, v8

    .line 2376338
    return-object v0

    :cond_0
    move v8, v10

    .line 2376339
    goto/16 :goto_0

    .line 2376340
    :cond_1
    const/4 p0, 0x0

    goto/16 :goto_1

    .line 2376341
    :cond_2
    const v9, 0x7f0a00e7

    goto :goto_2

    .line 2376342
    :cond_3
    const v9, -0x4f969c8d

    const/4 p0, 0x0

    invoke-static {v1, v9, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 2376343
    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2376352
    invoke-static {}, LX/1dS;->b()V

    .line 2376353
    iget v0, p1, LX/1dQ;->b:I

    .line 2376354
    packed-switch v0, :pswitch_data_0

    .line 2376355
    :goto_0
    return-object v1

    .line 2376356
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/Gex;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x4f969c8d
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/Gev;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/Gex",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2376344
    new-instance v1, LX/Gew;

    invoke-direct {v1, p0}, LX/Gew;-><init>(LX/Gex;)V

    .line 2376345
    iget-object v2, p0, LX/Gex;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gev;

    .line 2376346
    if-nez v2, :cond_0

    .line 2376347
    new-instance v2, LX/Gev;

    invoke-direct {v2, p0}, LX/Gev;-><init>(LX/Gex;)V

    .line 2376348
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Gev;->a$redex0(LX/Gev;LX/1De;IILX/Gew;)V

    .line 2376349
    move-object v1, v2

    .line 2376350
    move-object v0, v1

    .line 2376351
    return-object v0
.end method
