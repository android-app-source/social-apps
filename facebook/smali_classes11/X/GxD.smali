.class public final LX/GxD;
.super LX/BWH;
.source ""


# instance fields
.field public final synthetic a:LX/GxF;


# direct methods
.method public constructor <init>(LX/GxF;Landroid/content/Context;LX/BWW;LX/BWV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/44G;LX/03R;LX/48V;)V
    .locals 8

    .prologue
    .line 2407868
    iput-object p1, p0, LX/GxD;->a:LX/GxF;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    move-object/from16 v7, p8

    .line 2407869
    invoke-direct/range {v0 .. v7}, LX/BWH;-><init>(Landroid/content/Context;LX/BWW;LX/BWV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/44G;LX/03R;LX/48V;)V

    .line 2407870
    return-void
.end method

.method public static a(LX/GxD;Landroid/webkit/WebView;)V
    .locals 2

    .prologue
    .line 2407871
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxE;->PAGE_STATE_SUCCESS:LX/GxE;

    iput-object v1, v0, LX/GxF;->x:LX/GxE;

    .line 2407872
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    const/4 v1, 0x0

    iput-object v1, v0, LX/GxF;->y:LX/GxA;

    .line 2407873
    check-cast p1, LX/GxF;

    invoke-static {p1}, LX/GxF;->a(LX/GxF;)V

    .line 2407874
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, LX/GxF;->m:LX/GxQ;

    .line 2407875
    iget-object v1, v0, LX/GxQ;->a:LX/GxZ;

    sget-object p0, LX/GxY;->CONTENT_STATE_WEBVIEW:LX/GxY;

    invoke-virtual {v1, p0}, LX/GxZ;->a(LX/GxY;)V

    .line 2407876
    iget-object v1, v0, LX/GxQ;->a:LX/GxZ;

    iget-object v1, v1, LX/GxZ;->q:LX/Gu8;

    .line 2407877
    iget-object p0, v1, LX/Gu8;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2407878
    iget-object p0, v1, LX/Gu8;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->b(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v1, LX/Gu8;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->B(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2407879
    iget-object p0, v1, LX/Gu8;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-static {p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V

    .line 2407880
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/GxD;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2407863
    invoke-static {p1}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2407864
    if-eqz v0, :cond_0

    .line 2407865
    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, Lcom/facebook/webview/FacebookWebView;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x240003

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FacewebPageNetworkLoad:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2407866
    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, Lcom/facebook/webview/FacebookWebView;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x240004

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FacewebPageRPCLoadCompleted:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2407867
    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2407859
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, LX/GxF;->x:LX/GxE;

    sget-object v1, LX/GxE;->PAGE_STATE_UINITIALIZED:LX/GxE;

    if-ne v0, v1, :cond_0

    .line 2407860
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    const/4 v1, 0x1

    .line 2407861
    iput-boolean v1, v0, LX/GxF;->C:Z

    .line 2407862
    :cond_0
    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2407881
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2407882
    if-eqz v0, :cond_0

    const-string v0, "http://"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2407883
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    const-string v1, "Webview"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Webview attempted to load a resource over http: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407884
    :cond_0
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 2407834
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    .line 2407835
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-boolean v0, v0, LX/GxF;->C:Z

    if-eqz v0, :cond_1

    .line 2407836
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 2407837
    check-cast v0, Lcom/facebook/webview/FacebookWebView;

    .line 2407838
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "function() {window.__fbNative = {};window.__fbNative.nativeReady = true;"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    .line 2407839
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 2407840
    const-string v5, "url"

    new-instance v6, LX/BWO;

    const-string v8, "url"

    invoke-direct {v6, v8}, LX/BWO;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2407841
    const-string v5, "fbrpc"

    const-string v6, "facebook"

    const-string v9, "openDialogWebview"

    move-object v8, v7

    invoke-static/range {v5 .. v10}, LX/BWP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v11

    .line 2407842
    const-string v5, "fbrpc"

    const-string v6, "facebook"

    const-string v9, "closeDialogWebview"

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    move-object v8, v7

    invoke-static/range {v5 .. v10}, LX/BWP;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v5

    .line 2407843
    const-string v6, "window.__fbNative.open = function(%1$s) {window.prompt(%2$s);var dialog = window.__fbNative.dialog = {close: function() {window.prompt(%3$s);},closed: false,postMessage: function(message, targetOrigin) {window.__fbNative.postMessage(message, targetOrigin);}};return dialog;};"

    const-string v7, "url"

    invoke-static {v6, v7, v11, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2407844
    move-object v2, v5

    .line 2407845
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}();"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2407846
    const/4 v2, 0x0

    .line 2407847
    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2407848
    invoke-super {p0, p1, p2}, LX/BWH;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2407849
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, LX/GxF;->x:LX/GxE;

    sget-object v1, LX/GxE;->PAGE_STATE_ERROR:LX/GxE;

    if-ne v0, v1, :cond_2

    .line 2407850
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, LX/GxF;->m:LX/GxQ;

    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, LX/GxF;->y:LX/GxA;

    invoke-virtual {v0, v1}, LX/GxQ;->a(LX/GxA;)V

    .line 2407851
    invoke-static {p0, p2}, LX/GxD;->a$redex0(LX/GxD;Ljava/lang/String;)V

    goto :goto_0

    .line 2407852
    :cond_2
    if-eqz p2, :cond_3

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/2yp;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2407853
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    const-string v1, "(function(){if (window.FW_ENABLED) { return \'1\'; }; return null;})()"

    new-instance v2, LX/GxC;

    invoke-direct {v2, p0, p2}, LX/GxC;-><init>(LX/GxD;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2407854
    :goto_1
    invoke-static {p2}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2407855
    if-eqz v0, :cond_0

    .line 2407856
    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, Lcom/facebook/webview/FacebookWebView;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x240003

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FacewebPageNetworkLoad:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2407857
    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, Lcom/facebook/webview/FacebookWebView;->h:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x240006

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FacewebPageSession:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 2407858
    :cond_3
    invoke-static {p0, p1}, LX/GxD;->a(LX/GxD;Landroid/webkit/WebView;)V

    goto :goto_1
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 2407830
    invoke-super {p0, p1, p2, p3}, LX/BWH;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 2407831
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, LX/GxF;->m:LX/GxQ;

    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, LX/GxF;->x:LX/GxE;

    invoke-virtual {v0, v1}, LX/GxQ;->a(LX/GxE;)V

    .line 2407832
    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    .line 2407833
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2407820
    invoke-super {p0, p1, p2, p3, p4}, LX/BWH;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 2407821
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxE;->PAGE_STATE_ERROR:LX/GxE;

    iput-object v1, v0, LX/GxF;->x:LX/GxE;

    .line 2407822
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    const-string v1, "%s.onReceivedError-%d"

    sget-object v2, LX/GxF;->z:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "url: %s description: %s"

    invoke-static {v2, p4, p3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407823
    packed-switch p2, :pswitch_data_0

    .line 2407824
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxA;->SITE_ERROR:LX/GxA;

    iput-object v1, v0, LX/GxF;->y:LX/GxA;

    .line 2407825
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FacewebWebViewClient: onReceivedError:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2407826
    return-void

    .line 2407827
    :pswitch_0
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxA;->SITE_ERROR:LX/GxA;

    iput-object v1, v0, LX/GxF;->y:LX/GxA;

    goto :goto_0

    .line 2407828
    :pswitch_1
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxA;->CONNECTION_ERROR:LX/GxA;

    iput-object v1, v0, LX/GxF;->y:LX/GxA;

    goto :goto_0

    .line 2407829
    :pswitch_2
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxA;->SSL_ERROR:LX/GxA;

    iput-object v1, v0, LX/GxF;->y:LX/GxA;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xf
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 5

    .prologue
    .line 2407775
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-boolean v0, v0, LX/GxF;->C:Z

    if-eqz v0, :cond_0

    .line 2407776
    :goto_0
    return-void

    .line 2407777
    :cond_0
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    const-string v1, "%s.onReceivedSSLError"

    sget-object v2, LX/GxF;->z:Ljava/lang/Class;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "url: %s"

    iget-object v3, p0, LX/GxD;->a:LX/GxF;

    .line 2407778
    iget-object v4, v3, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v3, v4

    .line 2407779
    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407780
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, Lcom/facebook/webview/FacebookWebView;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->j:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2407781
    if-nez v0, :cond_1

    .line 2407782
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    goto :goto_0

    .line 2407783
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/BWH;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 2407784
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxE;->PAGE_STATE_ERROR:LX/GxE;

    iput-object v1, v0, LX/GxF;->x:LX/GxE;

    .line 2407785
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    sget-object v1, LX/GxA;->SSL_ERROR:LX/GxA;

    iput-object v1, v0, LX/GxF;->y:LX/GxA;

    .line 2407786
    iget-object v0, p0, LX/GxD;->a:LX/GxF;

    iget-object v0, v0, LX/GxF;->m:LX/GxQ;

    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, LX/GxF;->y:LX/GxA;

    invoke-virtual {v0, v1}, LX/GxQ;->a(LX/GxA;)V

    .line 2407787
    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/GxD;->a$redex0(LX/GxD;Ljava/lang/String;)V

    .line 2407788
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FacewebWebViewClient: onReceivedSslError:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/net/http/SslError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 11

    .prologue
    const/4 v0, 0x1

    .line 2407789
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2407790
    iget-object v2, p0, LX/GxD;->a:LX/GxF;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .line 2407791
    iget-object v4, v2, Lcom/facebook/webview/BasicWebView;->b:LX/48V;

    .line 2407792
    iget-object v2, v4, LX/48V;->f:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    move v4, v2

    .line 2407793
    if-nez v4, :cond_0

    sget-object v4, LX/GxF;->j:LX/2QP;

    invoke-virtual {v4, v3}, LX/2QP;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_0
    const/4 v4, 0x1

    :goto_0
    move v2, v4

    .line 2407794
    if-nez v2, :cond_2

    .line 2407795
    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, Lcom/facebook/webview/BasicWebView;->c:LX/03V;

    sget-object v2, LX/GxF;->z:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Rejecting invalid URI scheme: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407796
    :cond_1
    :goto_1
    return v0

    .line 2407797
    :cond_2
    iget-object v2, p0, LX/GxD;->a:LX/GxF;

    iget-boolean v2, v2, LX/GxF;->C:Z

    if-nez v2, :cond_1

    .line 2407798
    invoke-super {p0, p1, p2}, LX/BWH;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2407799
    invoke-direct {p0}, LX/GxD;->b()V

    goto :goto_1

    .line 2407800
    :cond_3
    iget-object v2, p0, LX/GxD;->a:LX/GxF;

    iget-object v2, v2, LX/GxF;->x:LX/GxE;

    sget-object v3, LX/GxE;->PAGE_STATE_SUCCESS:LX/GxE;

    if-eq v2, v3, :cond_4

    invoke-static {v1}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v1}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2407801
    const/4 v0, 0x0

    goto :goto_1

    .line 2407802
    :cond_4
    iget-object v2, p0, LX/GxD;->a:LX/GxF;

    iget-object v2, v2, LX/GxF;->o:LX/17X;

    .line 2407803
    iget-object v3, p0, LX/BW6;->b:Landroid/content/Context;

    move-object v3, v3

    .line 2407804
    invoke-virtual {v2, v3, p2}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2407805
    if-eqz v2, :cond_5

    .line 2407806
    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, LX/GxF;->q:Lcom/facebook/katana/urimap/IntentHandlerUtil;

    .line 2407807
    iget-object v3, p0, LX/BW6;->b:Landroid/content/Context;

    move-object v3, v3

    .line 2407808
    const/4 v8, 0x0

    .line 2407809
    move-object v5, v1

    move-object v6, v3

    move-object v7, p2

    move-object v9, v8

    move-object v10, v2

    invoke-static/range {v5 .. v10}, Lcom/facebook/katana/urimap/IntentHandlerUtil;->a(Lcom/facebook/katana/urimap/IntentHandlerUtil;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;Landroid/content/Intent;)Z

    .line 2407810
    invoke-direct {p0}, LX/GxD;->b()V

    goto :goto_1

    .line 2407811
    :cond_5
    iget-object v2, p0, LX/GxD;->a:LX/GxF;

    iget-object v2, v2, LX/GxF;->D:LX/Gu2;

    if-eqz v2, :cond_6

    .line 2407812
    iget-object v2, p0, LX/GxD;->a:LX/GxF;

    iget-object v2, v2, LX/GxF;->D:LX/Gu2;

    .line 2407813
    iget-object v3, v2, LX/Gu2;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v3, v3, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    iget-object v4, v2, LX/Gu2;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v4, v4, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Q:Ljava/lang/String;

    invoke-virtual {v3, v4, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407814
    :cond_6
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2407815
    const/high16 v1, 0x80000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2407816
    :try_start_0
    iget-object v1, p0, LX/GxD;->a:LX/GxF;

    iget-object v1, v1, LX/GxF;->p:Lcom/facebook/content/SecureContextHelper;

    .line 2407817
    iget-object v3, p0, LX/BW6;->b:Landroid/content/Context;

    move-object v3, v3

    .line 2407818
    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2407819
    invoke-direct {p0}, LX/GxD;->b()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    goto :goto_1

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_0
.end method
