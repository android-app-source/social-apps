.class public final LX/H4C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/H4F;

.field public final synthetic b:LX/H4G;


# direct methods
.method public constructor <init>(LX/H4G;LX/H4F;)V
    .locals 0

    .prologue
    .line 2422411
    iput-object p1, p0, LX/H4C;->b:LX/H4G;

    iput-object p2, p0, LX/H4C;->a:LX/H4F;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2422412
    iget-object v0, p0, LX/H4C;->b:LX/H4G;

    iget-object v0, v0, LX/H4G;->f:LX/I57;

    iget-object v1, p0, LX/H4C;->a:LX/H4F;

    const/4 v4, 0x0

    .line 2422413
    iget-object v2, v0, LX/I57;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422414
    const/4 v3, 0x1

    .line 2422415
    iput-boolean v3, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2422416
    iget-object v3, v0, LX/I57;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v3, v3, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->j:Landroid/location/Location;

    if-eqz v3, :cond_0

    .line 2422417
    iget-object v3, v0, LX/I57;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v3, v3, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->j:Landroid/location/Location;

    invoke-virtual {v2, v3}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a(Landroid/location/Location;)V

    .line 2422418
    :cond_0
    iput-object v4, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2422419
    iput-object v4, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2422420
    iget-object v2, v0, LX/I57;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v3, v0, LX/I57;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v3, v3, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v3, v3, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-static {v2, v3, v1}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;LX/H4F;)V

    .line 2422421
    iget-object v2, v0, LX/I57;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->b:LX/1nQ;

    invoke-virtual {v1}, LX/H4F;->getValue()I

    move-result v3

    iget-object v4, v0, LX/I57;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v4, v4, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->l:Ljava/lang/String;

    .line 2422422
    iget-object p1, v2, LX/1nQ;->i:LX/0Zb;

    const-string v0, "selected_location_filter_current_location"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p1

    .line 2422423
    invoke-virtual {p1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2422424
    const-string v0, "event_discovery"

    invoke-virtual {p1, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string v0, "event_suggestion_token"

    invoke-virtual {p1, v0, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string v0, "range"

    invoke-virtual {p1, v0, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object p1

    invoke-virtual {p1}, LX/0oG;->d()V

    .line 2422425
    :cond_1
    iget-object v0, p0, LX/H4C;->b:LX/H4G;

    iget-object v1, p0, LX/H4C;->a:LX/H4F;

    .line 2422426
    iput-object v1, v0, LX/H4G;->d:LX/H4F;

    .line 2422427
    const/4 v0, 0x1

    return v0
.end method
