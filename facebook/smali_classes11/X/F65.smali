.class public final LX/F65;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F66;


# direct methods
.method public constructor <init>(LX/F66;)V
    .locals 0

    .prologue
    .line 2200134
    iput-object p1, p0, LX/F65;->a:LX/F66;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2200135
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2200136
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2200137
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2200138
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2200139
    iget-object v1, p0, LX/F65;->a:LX/F66;

    iget-object v1, v1, LX/F66;->b:LX/F67;

    iget-object v1, v1, LX/F67;->a:LX/5pX;

    invoke-virtual {v1}, LX/5pX;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2200140
    iget-object v1, p0, LX/F65;->a:LX/F66;

    iget-object v1, v1, LX/F66;->b:LX/F67;

    iget-object v1, v1, LX/F67;->a:LX/5pX;

    const-class v2, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v1, v2}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v1

    check-cast v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v2, "reloadFeedViewForGroup"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2200141
    iget-object v0, p0, LX/F65;->a:LX/F66;

    iget-object v0, v0, LX/F66;->b:LX/F67;

    iget-object v0, v0, LX/F67;->a:LX/5pX;

    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v1, "reloadInboxFeedAndPersistPosition"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2200142
    :cond_0
    return-void
.end method
