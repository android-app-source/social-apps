.class public final enum LX/GO7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GO7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GO7;

.field public static final enum SEND_BUSINESS_ADDRESS:LX/GO7;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2347138
    new-instance v0, LX/GO7;

    const-string v1, "SEND_BUSINESS_ADDRESS"

    invoke-direct {v0, v1, v2}, LX/GO7;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GO7;->SEND_BUSINESS_ADDRESS:LX/GO7;

    .line 2347139
    const/4 v0, 0x1

    new-array v0, v0, [LX/GO7;

    sget-object v1, LX/GO7;->SEND_BUSINESS_ADDRESS:LX/GO7;

    aput-object v1, v0, v2

    sput-object v0, LX/GO7;->$VALUES:[LX/GO7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2347140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GO7;
    .locals 1

    .prologue
    .line 2347141
    const-class v0, LX/GO7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GO7;

    return-object v0
.end method

.method public static values()[LX/GO7;
    .locals 1

    .prologue
    .line 2347142
    sget-object v0, LX/GO7;->$VALUES:[LX/GO7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GO7;

    return-object v0
.end method
