.class public final LX/GDD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68J;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;

.field public final synthetic b:Lcom/facebook/adinterfaces/MapAreaPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;)V
    .locals 0

    .prologue
    .line 2329468
    iput-object p1, p0, LX/GDD;->b:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iput-object p2, p0, LX/GDD;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/680;)V
    .locals 7

    .prologue
    .line 2329469
    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    iget-object v0, p0, LX/GDD;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;->a()D

    move-result-wide v0

    iget-object v2, p0, LX/GDD;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;->b()D

    move-result-wide v4

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2329470
    iget-object v0, p0, LX/GDD;->b:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v1, p0, LX/GDD;->b:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->q:LX/GNE;

    iget-object v2, p0, LX/GDD;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;->c()D

    move-result-wide v4

    iget-object v2, p0, LX/GDD;->b:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget v6, v2, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->R:I

    move-object v2, p1

    invoke-virtual/range {v1 .. v6}, LX/GNE;->a(LX/680;Lcom/facebook/android/maps/model/LatLng;DI)F

    move-result v1

    .line 2329471
    iput v1, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->F:F

    .line 2329472
    iget-object v0, p0, LX/GDD;->b:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->F:F

    invoke-static {v3, v0}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/67d;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/680;->b(LX/67d;)V

    .line 2329473
    return-void
.end method
