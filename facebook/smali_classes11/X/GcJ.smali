.class public final LX/GcJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;)V
    .locals 0

    .prologue
    .line 2371724
    iput-object p1, p0, LX/GcJ;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x119a4b66

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2371712
    iget-object v1, p0, LX/GcJ;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2371713
    iget-object v2, p0, LX/GcJ;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    const/4 p1, 0x1

    .line 2371714
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2371715
    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f083492

    invoke-virtual {v2, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 2371716
    iget-object v4, v2, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->a:Landroid/widget/EditText;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2371717
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2371718
    :goto_0
    move-object v2, v4

    .line 2371719
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2371720
    const v1, -0x5c9c90a3

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2371721
    :goto_1
    return-void

    .line 2371722
    :cond_0
    iget-object v2, p0, LX/GcJ;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->b:LX/Gbb;

    invoke-interface {v2, v1}, LX/Gbb;->c(Ljava/lang/String;)V

    .line 2371723
    const v1, 0x31ab5d16

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    :cond_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_0
.end method
