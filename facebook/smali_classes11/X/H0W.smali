.class public final enum LX/H0W;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H0W;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H0W;

.field public static final enum DESCRIPTION:LX/H0W;

.field public static final enum FIELD_CHECK_BOX:LX/H0W;

.field public static final enum TITLE:LX/H0W;


# instance fields
.field private final viewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2413942
    new-instance v0, LX/H0W;

    const-string v1, "TITLE"

    const v2, 0x7f0d00ea

    invoke-direct {v0, v1, v3, v2}, LX/H0W;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0W;->TITLE:LX/H0W;

    .line 2413943
    new-instance v0, LX/H0W;

    const-string v1, "DESCRIPTION"

    const v2, 0x7f0d00e7

    invoke-direct {v0, v1, v4, v2}, LX/H0W;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0W;->DESCRIPTION:LX/H0W;

    .line 2413944
    new-instance v0, LX/H0W;

    const-string v1, "FIELD_CHECK_BOX"

    const v2, 0x7f0d00e3

    invoke-direct {v0, v1, v5, v2}, LX/H0W;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0W;->FIELD_CHECK_BOX:LX/H0W;

    .line 2413945
    const/4 v0, 0x3

    new-array v0, v0, [LX/H0W;

    sget-object v1, LX/H0W;->TITLE:LX/H0W;

    aput-object v1, v0, v3

    sget-object v1, LX/H0W;->DESCRIPTION:LX/H0W;

    aput-object v1, v0, v4

    sget-object v1, LX/H0W;->FIELD_CHECK_BOX:LX/H0W;

    aput-object v1, v0, v5

    sput-object v0, LX/H0W;->$VALUES:[LX/H0W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2413946
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2413947
    iput p3, p0, LX/H0W;->viewType:I

    .line 2413948
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H0W;
    .locals 1

    .prologue
    .line 2413949
    const-class v0, LX/H0W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H0W;

    return-object v0
.end method

.method public static values()[LX/H0W;
    .locals 1

    .prologue
    .line 2413950
    sget-object v0, LX/H0W;->$VALUES:[LX/H0W;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H0W;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2413951
    iget v0, p0, LX/H0W;->viewType:I

    return v0
.end method
