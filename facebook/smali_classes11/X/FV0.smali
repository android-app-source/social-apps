.class public final enum LX/FV0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FV0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FV0;

.field public static final enum SAVED_DASHBOARD_LIST_SECTION_HEADER:LX/FV0;

.field public static final enum SAVED_DASHBOARD_LOAD_MORE:LX/FV0;

.field public static final enum SAVED_DASHBOARD_LOAD_MORE_FAILED:LX/FV0;

.field public static final enum SAVED_DASHBOARD_SAVED_ITEM:LX/FV0;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2250270
    new-instance v0, LX/FV0;

    const-string v1, "SAVED_DASHBOARD_LIST_SECTION_HEADER"

    invoke-direct {v0, v1, v2}, LX/FV0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FV0;->SAVED_DASHBOARD_LIST_SECTION_HEADER:LX/FV0;

    .line 2250271
    new-instance v0, LX/FV0;

    const-string v1, "SAVED_DASHBOARD_SAVED_ITEM"

    invoke-direct {v0, v1, v3}, LX/FV0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FV0;->SAVED_DASHBOARD_SAVED_ITEM:LX/FV0;

    .line 2250272
    new-instance v0, LX/FV0;

    const-string v1, "SAVED_DASHBOARD_LOAD_MORE"

    invoke-direct {v0, v1, v4}, LX/FV0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FV0;->SAVED_DASHBOARD_LOAD_MORE:LX/FV0;

    .line 2250273
    new-instance v0, LX/FV0;

    const-string v1, "SAVED_DASHBOARD_LOAD_MORE_FAILED"

    invoke-direct {v0, v1, v5}, LX/FV0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FV0;->SAVED_DASHBOARD_LOAD_MORE_FAILED:LX/FV0;

    .line 2250274
    const/4 v0, 0x4

    new-array v0, v0, [LX/FV0;

    sget-object v1, LX/FV0;->SAVED_DASHBOARD_LIST_SECTION_HEADER:LX/FV0;

    aput-object v1, v0, v2

    sget-object v1, LX/FV0;->SAVED_DASHBOARD_SAVED_ITEM:LX/FV0;

    aput-object v1, v0, v3

    sget-object v1, LX/FV0;->SAVED_DASHBOARD_LOAD_MORE:LX/FV0;

    aput-object v1, v0, v4

    sget-object v1, LX/FV0;->SAVED_DASHBOARD_LOAD_MORE_FAILED:LX/FV0;

    aput-object v1, v0, v5

    sput-object v0, LX/FV0;->$VALUES:[LX/FV0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2250275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromOrdinal(I)LX/FV0;
    .locals 2

    .prologue
    .line 2250276
    if-ltz p0, :cond_0

    invoke-static {}, LX/FV0;->values()[LX/FV0;

    move-result-object v0

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 2250277
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Ordinal does not match any SavedDashboardListItemType"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2250278
    :cond_1
    invoke-static {}, LX/FV0;->values()[LX/FV0;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FV0;
    .locals 1

    .prologue
    .line 2250279
    const-class v0, LX/FV0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FV0;

    return-object v0
.end method

.method public static values()[LX/FV0;
    .locals 1

    .prologue
    .line 2250280
    sget-object v0, LX/FV0;->$VALUES:[LX/FV0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FV0;

    return-object v0
.end method
