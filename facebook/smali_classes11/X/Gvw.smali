.class public final LX/Gvw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:LX/Gvy;


# direct methods
.method public constructor <init>(LX/Gvy;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 2406223
    iput-object p1, p0, LX/Gvw;->b:LX/Gvy;

    iput-object p2, p0, LX/Gvw;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2406224
    const/4 v4, 0x0

    .line 2406225
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2406226
    new-instance v0, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;

    iget-object v1, p0, LX/Gvw;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/facebook/platform/server/protocol/ResolveTaggableProfileIdsMethod$Params;-><init>(Ljava/util/ArrayList;)V

    .line 2406227
    const-string v1, "taggable_ids"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2406228
    iget-object v0, p0, LX/Gvw;->b:LX/Gvy;

    iget-object v0, v0, LX/Gvy;->b:LX/0aG;

    const-string v1, "platform_resolve_taggable_profile_ids"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v5, 0x5317284f

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2406229
    const v1, -0x11be1f2c    # -1.5000602E28f

    :try_start_0
    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    .line 2406230
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableMap()Ljava/util/HashMap;

    move-result-object v0

    .line 2406231
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2406232
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/server/handler/ParcelableString;

    .line 2406233
    iget-object v3, v0, Lcom/facebook/platform/server/handler/ParcelableString;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2406234
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2406235
    :catch_0
    move-exception v0

    .line 2406236
    iget-object v1, p0, LX/Gvw;->b:LX/Gvy;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Gvy;->a(Ljava/lang/String;)V

    .line 2406237
    :goto_1
    return-object v4

    :cond_0
    move-object v4, v1

    .line 2406238
    goto :goto_1
.end method
