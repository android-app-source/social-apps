.class public final LX/GOF;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "Lcom/facebook/adspayments/model/BusinessAddressDetails;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V
    .locals 0

    .prologue
    .line 2347332
    iput-object p1, p0, LX/GOF;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a(Lcom/facebook/adspayments/model/BusinessAddressDetails;)V
    .locals 2

    .prologue
    .line 2347333
    iget-object v0, p0, LX/GOF;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2347334
    iget-object v0, p0, LX/GOF;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->M:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {p1}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347335
    iget-object v0, p0, LX/GOF;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->P:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {p1}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347336
    iget-object v0, p0, LX/GOF;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->Q:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {p1}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347337
    iget-object v0, p0, LX/GOF;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->R:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {p1}, Lcom/facebook/adspayments/model/BusinessAddressDetails;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GQ1;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;Ljava/lang/String;)V

    .line 2347338
    iget-object v0, p0, LX/GOF;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->n(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    .line 2347339
    iget-object v0, p0, LX/GOF;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->p(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    .line 2347340
    return-void
.end method


# virtual methods
.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2347341
    check-cast p1, Lcom/facebook/adspayments/model/BusinessAddressDetails;

    invoke-direct {p0, p1}, LX/GOF;->a(Lcom/facebook/adspayments/model/BusinessAddressDetails;)V

    return-void
.end method
