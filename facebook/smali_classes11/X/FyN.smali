.class public LX/FyN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0W3;

.field private final b:LX/0ad;

.field public final c:LX/Fsr;

.field private final d:LX/2ua;


# direct methods
.method public constructor <init>(LX/0W3;LX/Fsr;LX/0ad;LX/2ua;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2306463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306464
    iput-object p2, p0, LX/FyN;->c:LX/Fsr;

    .line 2306465
    iput-object p3, p0, LX/FyN;->b:LX/0ad;

    .line 2306466
    iput-object p4, p0, LX/FyN;->d:LX/2ua;

    .line 2306467
    iput-object p1, p0, LX/FyN;->a:LX/0W3;

    .line 2306468
    return-void
.end method

.method public static a(LX/0QB;)LX/FyN;
    .locals 7

    .prologue
    .line 2306469
    const-class v1, LX/FyN;

    monitor-enter v1

    .line 2306470
    :try_start_0
    sget-object v0, LX/FyN;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2306471
    sput-object v2, LX/FyN;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2306472
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2306473
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2306474
    new-instance p0, LX/FyN;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v4

    check-cast v4, LX/Fsr;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/2ua;->b(LX/0QB;)LX/2ua;

    move-result-object v6

    check-cast v6, LX/2ua;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FyN;-><init>(LX/0W3;LX/Fsr;LX/0ad;LX/2ua;)V

    .line 2306475
    move-object v0, p0

    .line 2306476
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2306477
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FyN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2306478
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2306479
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/5SB;ZZZZLandroid/view/MenuItem$OnMenuItemClickListener;)LX/5OM;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2306480
    new-instance v0, LX/6WS;

    invoke-direct {v0, p1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2306481
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 2306482
    iget-object v2, p0, LX/FyN;->d:LX/2ua;

    invoke-virtual {v2}, LX/2ua;->a()Z

    move-result v2

    .line 2306483
    iget-object v3, p0, LX/FyN;->b:LX/0ad;

    sget-short v4, LX/0wf;->aE:S

    invoke-interface {v3, v4, v7}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 2306484
    iget-object v4, p0, LX/FyN;->b:LX/0ad;

    sget-short v5, LX/0wf;->aF:S

    invoke-interface {v4, v5, v7}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 2306485
    if-nez p3, :cond_0

    if-nez p5, :cond_0

    iget-object v5, p0, LX/FyN;->b:LX/0ad;

    sget-short v6, LX/0wf;->au:S

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2306486
    const v5, 0x7f0815d4

    invoke-virtual {v1, v5}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v5

    new-instance v6, LX/FyH;

    invoke-direct {v6, p0}, LX/FyH;-><init>(LX/FyN;)V

    invoke-virtual {v5, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306487
    :cond_0
    if-eqz v2, :cond_1

    .line 2306488
    const v5, 0x7f0815d5

    invoke-virtual {v1, v5}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v5

    new-instance v6, LX/FyI;

    invoke-direct {v6, p0}, LX/FyI;-><init>(LX/FyN;)V

    invoke-virtual {v5, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306489
    :cond_1
    if-nez v2, :cond_2

    if-nez v3, :cond_3

    .line 2306490
    :cond_2
    if-eqz v3, :cond_9

    if-eqz v4, :cond_8

    const v5, 0x7f0815cf

    .line 2306491
    :goto_0
    if-eqz v2, :cond_a

    .line 2306492
    :goto_1
    invoke-virtual {v1, v5}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v5

    new-instance v6, LX/FyJ;

    invoke-direct {v6, p0}, LX/FyJ;-><init>(LX/FyN;)V

    invoke-virtual {v5, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306493
    :cond_3
    if-eqz p2, :cond_4

    .line 2306494
    iget-object v2, p2, LX/5SB;->e:LX/5SA;

    move-object v2, v2

    .line 2306495
    sget-object v4, LX/5SA;->USER:LX/5SA;

    if-ne v2, v4, :cond_4

    .line 2306496
    if-eqz v3, :cond_b

    const v2, 0x7f0815d2

    .line 2306497
    :goto_2
    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    new-instance v4, LX/FyK;

    invoke-direct {v4, p0}, LX/FyK;-><init>(LX/FyN;)V

    invoke-virtual {v2, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306498
    :cond_4
    if-eqz p4, :cond_5

    .line 2306499
    if-eqz p5, :cond_c

    const v2, 0x7f0815c2

    .line 2306500
    :goto_3
    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    invoke-virtual {v2, p7}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306501
    :cond_5
    if-eqz p6, :cond_6

    .line 2306502
    iget-object v2, p0, LX/FyN;->a:LX/0W3;

    sget-wide v4, LX/0X5;->eq:J

    invoke-interface {v2, v4, v5}, LX/0W4;->a(J)Z

    move-result v2

    .line 2306503
    if-eqz v2, :cond_7

    .line 2306504
    const v2, 0x7f081620

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    new-instance v3, LX/FyM;

    invoke-direct {v3, p0}, LX/FyM;-><init>(LX/FyN;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306505
    :cond_6
    :goto_4
    return-object v0

    .line 2306506
    :cond_7
    const v2, 0x7f08161e

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    new-instance v3, LX/FyL;

    invoke-direct {v3, p0}, LX/FyL;-><init>(LX/FyN;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306507
    goto :goto_4

    .line 2306508
    :cond_8
    const v5, 0x7f0815cd

    goto :goto_0

    :cond_9
    const v5, 0x7f0815ce

    goto :goto_0

    .line 2306509
    :cond_a
    const v5, 0x7f0815cc

    goto :goto_1

    .line 2306510
    :cond_b
    const v2, 0x7f0815d1

    goto :goto_2

    .line 2306511
    :cond_c
    const v2, 0x7f0815c0

    goto :goto_3
.end method
