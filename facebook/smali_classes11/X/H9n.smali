.class public final LX/H9n;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/9XB;

.field public final synthetic b:LX/9XA;

.field public final synthetic c:LX/H9r;


# direct methods
.method public constructor <init>(LX/H9r;LX/9XB;LX/9XA;)V
    .locals 0

    .prologue
    .line 2434611
    iput-object p1, p0, LX/H9n;->c:LX/H9r;

    iput-object p2, p0, LX/H9n;->a:LX/9XB;

    iput-object p3, p0, LX/H9n;->b:LX/9XA;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2434606
    iget-object v0, p0, LX/H9n;->c:LX/H9r;

    iget-object v0, v0, LX/H9r;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "page_identity_save_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2434607
    iget-object v0, p0, LX/H9n;->c:LX/H9r;

    iget-object v0, v0, LX/H9r;->d:LX/H8W;

    iget-object v1, p0, LX/H9n;->b:LX/9XA;

    iget-object v2, p0, LX/H9n;->c:LX/H9r;

    iget-object v2, v2, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434608
    iget-object v0, p0, LX/H9n;->c:LX/H9r;

    iget-object v0, v0, LX/H9r;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08178c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2434609
    iget-object v0, p0, LX/H9n;->c:LX/H9r;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/H9r;->a$redex0(LX/H9r;Z)V

    .line 2434610
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2434604
    iget-object v0, p0, LX/H9n;->c:LX/H9r;

    iget-object v0, v0, LX/H9r;->d:LX/H8W;

    iget-object v1, p0, LX/H9n;->a:LX/9XB;

    iget-object v2, p0, LX/H9n;->c:LX/H9r;

    iget-object v2, v2, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434605
    return-void
.end method
