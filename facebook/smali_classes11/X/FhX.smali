.class public LX/FhX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8ht;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bj;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273127
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273128
    iput-object v0, p0, LX/FhX;->a:LX/0Ot;

    .line 2273129
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273130
    iput-object v0, p0, LX/FhX;->b:LX/0Ot;

    .line 2273131
    return-void
.end method

.method private a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/api/SearchTypeaheadResult;)Lcom/facebook/search/model/EntityTypeaheadUnit;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2273132
    new-instance v0, LX/Cw7;

    invoke-direct {v0}, LX/Cw7;-><init>()V

    iget-object v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 2273133
    iput-object v2, v0, LX/Cw7;->b:Ljava/lang/String;

    .line 2273134
    move-object v0, v0

    .line 2273135
    iget-object v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    .line 2273136
    iput-object v2, v0, LX/Cw7;->e:Landroid/net/Uri;

    .line 2273137
    move-object v0, v0

    .line 2273138
    iget-wide v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2273139
    iput-object v2, v0, LX/Cw7;->a:Ljava/lang/String;

    .line 2273140
    move-object v0, v0

    .line 2273141
    invoke-virtual {p2}, Lcom/facebook/search/api/SearchTypeaheadResult;->a()I

    move-result v2

    invoke-virtual {v0, v2}, LX/Cw7;->a(I)LX/Cw7;

    move-result-object v0

    iget-object v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    .line 2273142
    iput-object v2, v0, LX/Cw7;->f:Ljava/lang/String;

    .line 2273143
    move-object v0, v0

    .line 2273144
    iget-object v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    .line 2273145
    iput-object v2, v0, LX/Cw7;->g:Ljava/lang/String;

    .line 2273146
    move-object v2, v0

    .line 2273147
    iget-object v0, p0, LX/FhX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bj;

    invoke-virtual {v0, p2}, LX/8bj;->a(Lcom/facebook/search/api/SearchTypeaheadResult;)LX/0Px;

    move-result-object v0

    .line 2273148
    iput-object v0, v2, LX/Cw7;->o:LX/0Px;

    .line 2273149
    move-object v0, v2

    .line 2273150
    iget-boolean v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    .line 2273151
    iput-boolean v2, v0, LX/Cw7;->h:Z

    .line 2273152
    move-object v2, v0

    .line 2273153
    iget-object v0, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->y:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    .line 2273154
    :goto_0
    iput-object v0, v2, LX/Cw7;->t:LX/0P1;

    .line 2273155
    move-object v0, v2

    .line 2273156
    iget-object v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->p:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2273157
    iput-object v2, v0, LX/Cw7;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2273158
    move-object v2, v0

    .line 2273159
    iget-object v0, p0, LX/FhX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8ht;

    invoke-virtual {v0, p1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2273160
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 2273161
    sget-object v3, LX/103;->VIDEO:LX/103;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    .line 2273162
    :goto_1
    iput-boolean v0, v2, LX/Cw7;->q:Z

    .line 2273163
    move-object v0, v2

    .line 2273164
    iget-boolean v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    .line 2273165
    iput-boolean v2, v0, LX/Cw7;->s:Z

    .line 2273166
    move-object v0, v0

    .line 2273167
    iget-object v2, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    if-nez v2, :cond_2

    .line 2273168
    :goto_2
    iput-object v1, v0, LX/Cw7;->u:Ljava/lang/String;

    .line 2273169
    move-object v0, v0

    .line 2273170
    iget-boolean v1, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->B:Z

    .line 2273171
    iput-boolean v1, v0, LX/Cw7;->w:Z

    .line 2273172
    move-object v0, v0

    .line 2273173
    invoke-virtual {v0}, LX/Cw7;->x()Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v1, p2, Lcom/facebook/search/api/SearchTypeaheadResult;->d:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method private static a(Lcom/facebook/search/api/SearchTypeaheadResult;)Lcom/facebook/search/model/ShortcutTypeaheadUnit;
    .locals 4

    .prologue
    .line 2273174
    new-instance v0, LX/CwZ;

    invoke-direct {v0}, LX/CwZ;-><init>()V

    iget-wide v2, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->n:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2273175
    iput-object v1, v0, LX/CwZ;->a:Ljava/lang/String;

    .line 2273176
    move-object v0, v0

    .line 2273177
    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 2273178
    iput-object v1, v0, LX/CwZ;->b:Ljava/lang/String;

    .line 2273179
    move-object v0, v0

    .line 2273180
    invoke-virtual {p0}, Lcom/facebook/search/api/SearchTypeaheadResult;->a()I

    move-result v1

    .line 2273181
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v2, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2273182
    iput-object v2, v0, LX/CwZ;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2273183
    move-object v2, v0

    .line 2273184
    move-object v0, v2

    .line 2273185
    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    .line 2273186
    iput-object v1, v0, LX/CwZ;->d:Landroid/net/Uri;

    .line 2273187
    move-object v0, v0

    .line 2273188
    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    .line 2273189
    iput-object v1, v0, LX/CwZ;->e:Ljava/lang/String;

    .line 2273190
    move-object v0, v0

    .line 2273191
    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    .line 2273192
    iput-object v1, v0, LX/CwZ;->f:Ljava/lang/String;

    .line 2273193
    move-object v0, v0

    .line 2273194
    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->e:Landroid/net/Uri;

    .line 2273195
    iput-object v1, v0, LX/CwZ;->g:Landroid/net/Uri;

    .line 2273196
    move-object v0, v0

    .line 2273197
    iget-object v1, p0, Lcom/facebook/search/api/SearchTypeaheadResult;->b:Landroid/net/Uri;

    .line 2273198
    iput-object v1, v0, LX/CwZ;->h:Landroid/net/Uri;

    .line 2273199
    move-object v0, v0

    .line 2273200
    invoke-virtual {v0}, LX/CwZ;->i()Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/7BS;LX/0Uh;LX/0ad;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2273201
    sget-short v0, LX/100;->bN:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2273202
    sget-char v0, LX/100;->ca:C

    const-string v1, "kw_default_android"

    invoke-interface {p2, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2273203
    iput-object v0, p0, LX/7BS;->j:Ljava/lang/String;

    .line 2273204
    :cond_0
    :goto_0
    return-void

    .line 2273205
    :cond_1
    sget v0, LX/2SU;->n:I

    invoke-virtual {p1, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273206
    const-string v0, "kw_first_3andgrow_usergroup"

    .line 2273207
    iget-object v1, p0, LX/7BS;->i:LX/7BZ;

    move-object v1, v1

    .line 2273208
    sget-object v2, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    if-ne v1, v2, :cond_2

    .line 2273209
    sget-char v0, LX/100;->s:C

    const-string v1, "kw_first_3andgrow_usergroup"

    invoke-interface {p2, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2273210
    :cond_2
    iput-object v0, p0, LX/7BS;->j:Ljava/lang/String;

    .line 2273211
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7Hc;LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;)LX/7Hc;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;",
            "LX/7BZ;",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            ")",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2273212
    if-nez p1, :cond_0

    .line 2273213
    new-instance v0, LX/7Hc;

    .line 2273214
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2273215
    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    .line 2273216
    :goto_0
    return-object v0

    .line 2273217
    :cond_0
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v4

    .line 2273218
    iget-object v0, p1, LX/7Hc;->b:LX/0Px;

    move-object v5, v0

    .line 2273219
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_3

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 2273220
    invoke-virtual {v0}, Lcom/facebook/search/api/SearchTypeaheadResult;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2273221
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    .line 2273222
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2273223
    invoke-direct {p0, p3, v0}, LX/FhX;->a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/api/SearchTypeaheadResult;)Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    .line 2273224
    :goto_2
    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2273225
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    invoke-virtual {v4, v0, v7}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2273226
    :cond_1
    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2273227
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2273228
    :sswitch_0
    invoke-static {v0}, LX/FhX;->a(Lcom/facebook/search/api/SearchTypeaheadResult;)Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    move-result-object v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    .line 2273229
    goto :goto_2

    .line 2273230
    :sswitch_1
    new-instance v7, LX/CwH;

    invoke-direct {v7}, LX/CwH;-><init>()V

    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 2273231
    iput-object v8, v7, LX/CwH;->b:Ljava/lang/String;

    .line 2273232
    move-object v7, v7

    .line 2273233
    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->t:Ljava/lang/String;

    .line 2273234
    iput-object v8, v7, LX/CwH;->c:Ljava/lang/String;

    .line 2273235
    move-object v7, v7

    .line 2273236
    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    .line 2273237
    iput-object v8, v7, LX/CwH;->d:Ljava/lang/String;

    .line 2273238
    move-object v7, v7

    .line 2273239
    const-string v8, "content"

    .line 2273240
    iput-object v8, v7, LX/CwH;->e:Ljava/lang/String;

    .line 2273241
    move-object v7, v7

    .line 2273242
    sget-object v8, LX/CwI;->SUGGESTION:LX/CwI;

    .line 2273243
    iput-object v8, v7, LX/CwH;->l:LX/CwI;

    .line 2273244
    move-object v7, v7

    .line 2273245
    invoke-virtual {p2}, LX/7BZ;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 2273246
    iput-object v8, v7, LX/CwH;->m:Ljava/lang/String;

    .line 2273247
    move-object v7, v7

    .line 2273248
    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    .line 2273249
    iput-object v8, v7, LX/CwH;->t:Ljava/lang/String;

    .line 2273250
    move-object v7, v7

    .line 2273251
    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->h:Ljava/lang/String;

    .line 2273252
    iput-object v8, v7, LX/CwH;->u:Ljava/lang/String;

    .line 2273253
    move-object v7, v7

    .line 2273254
    iget-object v8, p0, LX/FhX;->a:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p3}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v8

    .line 2273255
    iput-object v8, v7, LX/CwH;->x:LX/0Px;

    .line 2273256
    move-object v7, v7

    .line 2273257
    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->j:Ljava/lang/String;

    .line 2273258
    iput-object v8, v7, LX/CwH;->h:Ljava/lang/String;

    .line 2273259
    move-object v7, v7

    .line 2273260
    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->k:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 2273261
    iput-object v8, v7, LX/CwH;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 2273262
    move-object v7, v7

    .line 2273263
    iget v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->v:I

    .line 2273264
    iput v8, v7, LX/CwH;->B:I

    .line 2273265
    move-object v7, v7

    .line 2273266
    iget v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->w:I

    .line 2273267
    iput v8, v7, LX/CwH;->C:I

    .line 2273268
    move-object v7, v7

    .line 2273269
    iget-object v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->z:Ljava/lang/String;

    .line 2273270
    iput-object v8, v7, LX/CwH;->F:Ljava/lang/String;

    .line 2273271
    move-object v7, v7

    .line 2273272
    iget-boolean v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->A:Z

    .line 2273273
    iput-boolean v8, v7, LX/CwH;->G:Z

    .line 2273274
    move-object v7, v7

    .line 2273275
    iget-boolean v8, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->x:Z

    .line 2273276
    iput-boolean v8, v7, LX/CwH;->D:Z

    .line 2273277
    move-object v8, v7

    .line 2273278
    iget-object v7, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    if-eqz v7, :cond_2

    .line 2273279
    iget-object v7, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->i:Ljava/lang/String;

    .line 2273280
    :try_start_0
    invoke-static {v7}, LX/CwF;->valueOf(Ljava/lang/String;)LX/CwF;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 2273281
    :goto_3
    move-object v7, v9

    .line 2273282
    iput-object v7, v8, LX/CwH;->g:LX/CwF;

    .line 2273283
    :cond_2
    iget-object v7, p0, LX/FhX;->a:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/8ht;

    invoke-virtual {v7, p3}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2273284
    invoke-virtual {v8}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v7

    .line 2273285
    :goto_4
    move-object v0, v7

    .line 2273286
    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    .line 2273287
    goto/16 :goto_2

    .line 2273288
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2273289
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2273290
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2273291
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto :goto_5

    .line 2273292
    :cond_4
    sget-object v0, LX/7BZ;->KEYWORD_ONLY_MODE:LX/7BZ;

    if-eq p2, v0, :cond_9

    .line 2273293
    iget v0, p1, LX/7Hc;->c:I

    move v0, v0

    .line 2273294
    :goto_6
    move v2, v0

    .line 2273295
    new-instance v0, LX/7Hc;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2273296
    iget v3, p1, LX/7Hc;->d:I

    move v3, v3

    .line 2273297
    invoke-direct {v0, v1, v2, v3}, LX/7Hc;-><init>(LX/0Px;II)V

    goto/16 :goto_0

    .line 2273298
    :cond_5
    iget-object v7, p3, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v7, v7

    .line 2273299
    sget-object v9, LX/103;->VIDEO:LX/103;

    if-ne v7, v9, :cond_7

    const/4 v7, 0x1

    .line 2273300
    :goto_7
    iget-boolean v9, v0, Lcom/facebook/search/api/SearchTypeaheadResult;->q:Z

    if-nez v9, :cond_6

    if-eqz v7, :cond_8

    .line 2273301
    :cond_6
    iget-object v7, p3, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v7, v7

    .line 2273302
    iget-object v9, p3, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v9, v9

    .line 2273303
    iget-object v10, p3, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v10, v10

    .line 2273304
    invoke-virtual {v8, v7, v9, v10}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/103;)LX/CwH;

    .line 2273305
    invoke-static {p3}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v7

    .line 2273306
    iput-object v7, v8, LX/CwH;->x:LX/0Px;

    .line 2273307
    iget-object v7, p3, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    move-object v7, v7

    .line 2273308
    iput-object v7, v8, LX/CwH;->y:LX/0P1;

    .line 2273309
    :goto_8
    invoke-virtual {v8}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v7

    goto :goto_4

    .line 2273310
    :cond_7
    const/4 v7, 0x0

    goto :goto_7

    .line 2273311
    :cond_8
    const/4 v7, 0x1

    .line 2273312
    iput-boolean v7, v8, LX/CwH;->q:Z

    .line 2273313
    goto :goto_8

    :catch_0
    sget-object v9, LX/CwF;->keyword:LX/CwF;

    goto/16 :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x30654a2e -> :sswitch_0
        0x361ab677 -> :sswitch_1
    .end sparse-switch
.end method
