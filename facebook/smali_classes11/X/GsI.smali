.class public LX/GsI;
.super Landroid/app/Dialog;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/GsB;

.field public d:Landroid/webkit/WebView;

.field public e:Landroid/app/ProgressDialog;

.field public f:Landroid/widget/ImageView;

.field public g:Landroid/widget/FrameLayout;

.field public h:Z

.field public i:Z

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2399334
    invoke-static {}, LX/GAK;->k()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, LX/GsI;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 2399335
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2399327
    if-nez p3, :cond_0

    invoke-static {}, LX/GAK;->k()I

    move-result p3

    :cond_0
    invoke-direct {p0, p1, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 2399328
    const-string v0, "fbconnect://success"

    iput-object v0, p0, LX/GsI;->b:Ljava/lang/String;

    .line 2399329
    iput-boolean v1, p0, LX/GsI;->h:Z

    .line 2399330
    iput-boolean v1, p0, LX/GsI;->i:Z

    .line 2399331
    iput-boolean v1, p0, LX/GsI;->j:Z

    .line 2399332
    iput-object p2, p0, LX/GsI;->a:Ljava/lang/String;

    .line 2399333
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;ILX/GsB;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2399296
    if-nez p4, :cond_0

    invoke-static {}, LX/GAK;->k()I

    move-result p4

    :cond_0
    invoke-direct {p0, p1, p4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 2399297
    const-string v0, "fbconnect://success"

    iput-object v0, p0, LX/GsI;->b:Ljava/lang/String;

    .line 2399298
    iput-boolean v5, p0, LX/GsI;->h:Z

    .line 2399299
    iput-boolean v5, p0, LX/GsI;->i:Z

    .line 2399300
    iput-boolean v5, p0, LX/GsI;->j:Z

    .line 2399301
    if-nez p3, :cond_1

    .line 2399302
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 2399303
    :cond_1
    const-string v0, "redirect_uri"

    const-string v1, "fbconnect://success"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2399304
    const-string v0, "display"

    const-string v1, "touch"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2399305
    const-string v0, "sdk"

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v2, "android-%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 2399306
    const-string v4, "4.9.0"

    move-object v4, v4

    .line 2399307
    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2399308
    const-string v0, "m.%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 2399309
    sget-object v3, LX/GAK;->h:Ljava/lang/String;

    move-object v3, v3

    .line 2399310
    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2399311
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2399312
    const-string v2, "v2.5"

    move-object v2, v2

    .line 2399313
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/dialog/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2399314
    new-instance v4, Landroid/net/Uri$Builder;

    invoke-direct {v4}, Landroid/net/Uri$Builder;-><init>()V

    .line 2399315
    const-string v2, "https"

    invoke-virtual {v4, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2399316
    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2399317
    invoke-virtual {v4, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2399318
    if-eqz p3, :cond_3

    .line 2399319
    invoke-virtual {p3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2399320
    invoke-virtual {p3, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 2399321
    instance-of p1, v3, Ljava/lang/String;

    if-eqz p1, :cond_2

    .line 2399322
    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 2399323
    :cond_3
    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    move-object v0, v2

    .line 2399324
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GsI;->a:Ljava/lang/String;

    .line 2399325
    iput-object p5, p0, LX/GsI;->c:LX/GsB;

    .line 2399326
    return-void
.end method

.method private static a(IFII)I
    .locals 6

    .prologue
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    .line 2399290
    int-to-float v2, p0

    div-float/2addr v2, p1

    float-to-int v2, v2

    .line 2399291
    if-gt v2, p2, :cond_1

    .line 2399292
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 2399293
    :cond_0
    :goto_0
    int-to-double v2, p0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0

    .line 2399294
    :cond_1
    if-ge v2, p3, :cond_0

    .line 2399295
    sub-int v2, p3, v2

    int-to-double v2, v2

    sub-int v4, p3, p2

    int-to-double v4, v4

    div-double/2addr v2, v4

    mul-double/2addr v2, v0

    add-double/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 2399286
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2399287
    invoke-virtual {v0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/Gsc;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2399288
    invoke-virtual {v0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Gsc;->b(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2399289
    return-object v1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2399278
    iget-object v0, p0, LX/GsI;->c:LX/GsB;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/GsI;->h:Z

    if-nez v0, :cond_0

    .line 2399279
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GsI;->h:Z

    .line 2399280
    instance-of v0, p1, LX/GAA;

    if-eqz v0, :cond_1

    .line 2399281
    check-cast p1, LX/GAA;

    .line 2399282
    :goto_0
    iget-object v0, p0, LX/GsI;->c:LX/GsB;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, LX/GsB;->a(Landroid/os/Bundle;LX/GAA;)V

    .line 2399283
    invoke-virtual {p0}, LX/GsI;->dismiss()V

    .line 2399284
    :cond_0
    return-void

    .line 2399285
    :cond_1
    new-instance v0, LX/GAA;

    invoke-direct {v0, p1}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 2399205
    iget-object v0, p0, LX/GsI;->c:LX/GsB;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/GsI;->h:Z

    if-nez v0, :cond_0

    .line 2399206
    new-instance v0, LX/GAC;

    invoke-direct {v0}, LX/GAC;-><init>()V

    invoke-virtual {p0, v0}, LX/GsI;->a(Ljava/lang/Throwable;)V

    .line 2399207
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    const/16 v5, 0x320

    .line 2399266
    invoke-virtual {p0}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2399267
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2399268
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2399269
    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2399270
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v0, v1, :cond_0

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2399271
    :goto_0
    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-ge v1, v3, :cond_1

    iget v1, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2399272
    :goto_1
    iget v3, v2, Landroid/util/DisplayMetrics;->density:F

    const/16 v4, 0x1e0

    invoke-static {v0, v3, v4, v5}, LX/GsI;->a(IFII)I

    move-result v0

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2399273
    iget v3, v2, Landroid/util/DisplayMetrics;->density:F

    const/16 v4, 0x500

    invoke-static {v1, v3, v5, v4}, LX/GsI;->a(IFII)I

    move-result v1

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2399274
    invoke-virtual {p0}, LX/GsI;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/Window;->setLayout(II)V

    .line 2399275
    return-void

    .line 2399276
    :cond_0
    iget v0, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    .line 2399277
    :cond_1
    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    goto :goto_1
.end method

.method public final dismiss()V
    .locals 1

    .prologue
    .line 2399259
    iget-object v0, p0, LX/GsI;->d:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 2399260
    iget-object v0, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 2399261
    :cond_0
    iget-boolean v0, p0, LX/GsI;->i:Z

    if-nez v0, :cond_1

    .line 2399262
    iget-object v0, p0, LX/GsI;->e:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GsI;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2399263
    iget-object v0, p0, LX/GsI;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2399264
    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    .line 2399265
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 2399256
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GsI;->i:Z

    .line 2399257
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 2399258
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x2

    .line 2399217
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 2399218
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/GsI;->e:Landroid/app/ProgressDialog;

    .line 2399219
    iget-object v0, p0, LX/GsI;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->requestWindowFeature(I)Z

    .line 2399220
    iget-object v0, p0, LX/GsI;->e:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082892

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2399221
    iget-object v0, p0, LX/GsI;->e:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 2399222
    iget-object v0, p0, LX/GsI;->e:Landroid/app/ProgressDialog;

    new-instance v1, LX/Gse;

    invoke-direct {v1, p0}, LX/Gse;-><init>(LX/GsI;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2399223
    invoke-virtual {p0, v4}, LX/GsI;->requestWindowFeature(I)Z

    .line 2399224
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/GsI;->g:Landroid/widget/FrameLayout;

    .line 2399225
    invoke-virtual {p0}, LX/GsI;->d()V

    .line 2399226
    invoke-virtual {p0}, LX/GsI;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 2399227
    invoke-virtual {p0}, LX/GsI;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2399228
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/GsI;->f:Landroid/widget/ImageView;

    .line 2399229
    iget-object v0, p0, LX/GsI;->f:Landroid/widget/ImageView;

    new-instance v1, LX/Gsf;

    invoke-direct {v1, p0}, LX/Gsf;-><init>(LX/GsI;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2399230
    invoke-virtual {p0}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02028d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2399231
    iget-object v1, p0, LX/GsI;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2399232
    iget-object v0, p0, LX/GsI;->f:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2399233
    iget-object v0, p0, LX/GsI;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 2399234
    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    const/4 p1, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2399235
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2399236
    new-instance v2, LX/Gsg;

    invoke-virtual {p0}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, p0, v4}, LX/Gsg;-><init>(LX/GsI;Landroid/content/Context;)V

    iput-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    .line 2399237
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 2399238
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->setHorizontalScrollBarEnabled(Z)V

    .line 2399239
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    new-instance v4, LX/Gsj;

    invoke-direct {v4, p0}, LX/Gsj;-><init>(LX/GsI;)V

    invoke-virtual {v2, v4}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2399240
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 2399241
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    iget-object v4, p0, LX/GsI;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 2399242
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, p1, p1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v4}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2399243
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 2399244
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 2399245
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 2399246
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v2, v6}, Landroid/webkit/WebView;->setFocusable(Z)V

    .line 2399247
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v2, v6}, Landroid/webkit/WebView;->setFocusableInTouchMode(Z)V

    .line 2399248
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    new-instance v4, LX/Gsh;

    invoke-direct {v4, p0}, LX/Gsh;-><init>(LX/GsI;)V

    invoke-virtual {v2, v4}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2399249
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2399250
    iget-object v2, p0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2399251
    const/high16 v2, -0x34000000    # -3.3554432E7f

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 2399252
    iget-object v2, p0, LX/GsI;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2399253
    iget-object v0, p0, LX/GsI;->g:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/GsI;->f:Landroid/widget/ImageView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2399254
    iget-object v0, p0, LX/GsI;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, LX/GsI;->setContentView(Landroid/view/View;)V

    .line 2399255
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 2399214
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GsI;->i:Z

    .line 2399215
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 2399216
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 2399211
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 2399212
    invoke-virtual {p0}, LX/GsI;->cancel()V

    .line 2399213
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onStart()V
    .locals 0

    .prologue
    .line 2399208
    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    .line 2399209
    invoke-virtual {p0}, LX/GsI;->d()V

    .line 2399210
    return-void
.end method
