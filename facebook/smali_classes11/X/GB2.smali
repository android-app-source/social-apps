.class public LX/GB2;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/2Du;

.field public final c:LX/0TD;

.field public final d:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2326305
    const-class v0, LX/GB2;

    sput-object v0, LX/GB2;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Du;LX/0TD;Lcom/facebook/account/recovery/common/model/AccountRecoveryData;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2326320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326321
    iput-object p1, p0, LX/GB2;->b:LX/2Du;

    .line 2326322
    iput-object p2, p0, LX/GB2;->c:LX/0TD;

    .line 2326323
    iput-object p3, p0, LX/GB2;->d:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    .line 2326324
    return-void
.end method

.method public static b(LX/0QB;)LX/GB2;
    .locals 4

    .prologue
    .line 2326318
    new-instance v3, LX/GB2;

    invoke-static {p0}, LX/2Du;->b(LX/0QB;)LX/2Du;

    move-result-object v0

    check-cast v0, LX/2Du;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static {p0}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a(LX/0QB;)Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    move-result-object v2

    check-cast v2, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    invoke-direct {v3, v0, v1, v2}, LX/GB2;-><init>(LX/2Du;LX/0TD;Lcom/facebook/account/recovery/common/model/AccountRecoveryData;)V

    .line 2326319
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2326307
    invoke-virtual {p0}, LX/GB2;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2326308
    :goto_0
    return-object v0

    .line 2326309
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2326310
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2326311
    iget-object v1, p0, LX/GB2;->d:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    invoke-virtual {v1}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move-object v5, v0

    :goto_1
    if-ge v1, v6, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/openidconnect/model/OpenIDCredential;

    .line 2326312
    if-nez v5, :cond_1

    .line 2326313
    iget-object v5, v0, Lcom/facebook/openidconnect/model/OpenIDCredential;->b:LX/4gy;

    .line 2326314
    :cond_1
    iget-object v7, v0, Lcom/facebook/openidconnect/model/OpenIDCredential;->a:Ljava/lang/String;

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2326315
    iget-object v0, v0, Lcom/facebook/openidconnect/model/OpenIDCredential;->c:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2326316
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2326317
    :cond_2
    new-instance v0, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;

    sget-object v4, LX/4gx;->ANDROID_ACCOUNT_RECOVERY:LX/4gx;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodParams;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;LX/4gx;LX/4gy;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2326306
    iget-object v0, p0, LX/GB2;->d:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
