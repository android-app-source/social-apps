.class public final LX/Fpm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G4x;

.field public final synthetic b:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;LX/G4x;)V
    .locals 0

    .prologue
    .line 2291875
    iput-object p1, p0, LX/Fpm;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    iput-object p2, p0, LX/Fpm;->a:LX/G4x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2291876
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 2291877
    iget-object v0, p0, LX/Fpm;->a:LX/G4x;

    invoke-virtual {v0, p1}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2291878
    iget-object v0, p0, LX/Fpm;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2291879
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2291880
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 2291881
    iget-object v0, p0, LX/Fpm;->a:LX/G4x;

    invoke-virtual {v0, p1}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2291882
    iget-object v0, p0, LX/Fpm;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->v()LX/1OP;

    move-result-object v0

    .line 2291883
    if-eqz v0, :cond_0

    .line 2291884
    iget-object v0, p0, LX/Fpm;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2291885
    :cond_0
    iget-object v0, p0, LX/Fpm;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "timeline_page_like_fail"

    invoke-virtual {v0, v1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2291886
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2291887
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2291888
    return-void
.end method
