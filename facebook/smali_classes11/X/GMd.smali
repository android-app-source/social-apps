.class public final LX/GMd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/GHv;

.field public final synthetic c:Lcom/facebook/adinterfaces/ui/UploadImageHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/UploadImageHelper;ZLX/GHv;)V
    .locals 0

    .prologue
    .line 2345050
    iput-object p1, p0, LX/GMd;->c:Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    iput-boolean p2, p0, LX/GMd;->a:Z

    iput-object p3, p0, LX/GMd;->b:LX/GHv;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2345051
    iget-object v0, p0, LX/GMd;->c:Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->a()V

    .line 2345052
    iget-object v0, p0, LX/GMd;->c:Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->e:LX/2U3;

    const-class v1, Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    const-string v2, "upload ad image failed"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2345053
    iget-object v0, p0, LX/GMd;->b:LX/GHv;

    .line 2345054
    new-instance v1, Ljava/io/File;

    iget-object v2, v0, LX/GHv;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2345055
    iget-object p0, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->m:Ljava/lang/String;

    move-object v2, p0

    .line 2345056
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 2345057
    iget-object v1, v0, LX/GHv;->a:LX/GHx;

    .line 2345058
    iget-object v2, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v2

    .line 2345059
    new-instance v2, LX/GFO;

    invoke-direct {v2}, LX/GFO;-><init>()V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2345060
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2345061
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2345062
    iget-boolean v0, p0, LX/GMd;->a:Z

    if-eqz v0, :cond_0

    .line 2345063
    iget-object v0, p0, LX/GMd;->c:Lcom/facebook/adinterfaces/ui/UploadImageHelper;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/UploadImageHelper;->a()V

    .line 2345064
    :cond_0
    iget-object v1, p0, LX/GMd;->b:LX/GHv;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;

    .line 2345065
    if-eqz v0, :cond_1

    .line 2345066
    iget-object v2, v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2345067
    if-nez v2, :cond_2

    .line 2345068
    :cond_1
    :goto_0
    return-void

    .line 2345069
    :cond_2
    new-instance v2, Ljava/io/File;

    iget-object v3, v1, LX/GHv;->a:LX/GHx;

    iget-object v3, v3, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2345070
    iget-object p0, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->m:Ljava/lang/String;

    move-object v3, p0

    .line 2345071
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 2345072
    iget-object v2, v1, LX/GHv;->a:LX/GHx;

    .line 2345073
    iget-object v3, v2, LX/GHg;->b:LX/GCE;

    move-object v2, v3

    .line 2345074
    sget-object v3, LX/GG8;->PHOTO_NOT_UPLOADED:LX/GG8;

    const/4 p0, 0x1

    invoke-virtual {v2, v3, p0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2345075
    iget-object v2, v1, LX/GHv;->a:LX/GHx;

    iget-boolean v2, v2, LX/GHx;->p:Z

    if-eqz v2, :cond_8

    .line 2345076
    iget-object v2, v1, LX/GHv;->a:LX/GHx;

    .line 2345077
    iget-object v3, v2, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2345078
    iget-object p0, v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->b:Ljava/lang/String;

    move-object p0, p0

    .line 2345079
    iget-object p1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    if-nez p1, :cond_3

    .line 2345080
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    .line 2345081
    :cond_3
    iget-object p1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->g:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2345082
    iget-object v3, v2, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    const-string p0, ""

    .line 2345083
    iget-object p1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    if-nez p1, :cond_4

    .line 2345084
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    .line 2345085
    :cond_4
    iget-object p1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->h:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2345086
    iget-object v3, v2, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    .line 2345087
    iget-object p0, v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->b:Ljava/lang/String;

    move-object p0, p0

    .line 2345088
    invoke-virtual {v3, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->a(Ljava/lang/String;)Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    move-result-object v3

    .line 2345089
    iget-object p0, v2, LX/GHx;->r:Ljava/util/List;

    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2345090
    iget-object p0, v2, LX/GHx;->r:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    const/4 p1, 0x1

    if-ne p0, p1, :cond_5

    .line 2345091
    iget-object p0, v2, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->g()V

    .line 2345092
    :cond_5
    iget-object p0, v2, LX/GHx;->r:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    const/4 p1, 0x5

    if-ne p0, p1, :cond_6

    .line 2345093
    iget-object p0, v2, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->m()V

    .line 2345094
    :cond_6
    iget-object p0, v2, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    if-eqz p0, :cond_7

    .line 2345095
    iget-object p0, v2, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->b()V

    .line 2345096
    :cond_7
    iget-object p0, v2, LX/GHx;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2345097
    iget-object p0, v2, LX/GHx;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;->setDeleteImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2345098
    iput-object v3, v2, LX/GHx;->s:Lcom/facebook/adinterfaces/ui/AdInterfacesImageAttachmentView;

    .line 2345099
    invoke-static {v2}, LX/GHx;->h(LX/GHx;)V

    .line 2345100
    :goto_1
    iget-object v2, v1, LX/GHv;->a:LX/GHx;

    .line 2345101
    iget-object v3, v2, LX/GHg;->b:LX/GCE;

    move-object v2, v3

    .line 2345102
    new-instance v3, LX/GFp;

    invoke-direct {v3}, LX/GFp;-><init>()V

    invoke-virtual {v2, v3}, LX/GCE;->a(LX/8wN;)V

    goto/16 :goto_0

    .line 2345103
    :cond_8
    iget-object v2, v1, LX/GHv;->a:LX/GHx;

    .line 2345104
    iget-object v3, v2, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2345105
    iget-object p0, v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->a:Ljava/lang/String;

    move-object p0, p0

    .line 2345106
    iput-object p0, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    .line 2345107
    iget-object v3, v0, Lcom/facebook/adinterfaces/protocol/UploadAdImageMethod$Result;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2345108
    iput-object v3, v2, LX/GHx;->m:Ljava/lang/String;

    .line 2345109
    iget-object v3, v2, LX/GHx;->q:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    iget-object p0, v2, LX/GHx;->m:Ljava/lang/String;

    .line 2345110
    iput-object p0, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    .line 2345111
    iget-object v3, v2, LX/GHx;->o:Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;

    iget-object p0, v2, LX/GHx;->m:Ljava/lang/String;

    invoke-virtual {v3, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAdCreativeView;->setAdImageThumbnail(Ljava/lang/String;)V

    .line 2345112
    goto :goto_1
.end method
