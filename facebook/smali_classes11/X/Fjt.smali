.class public final LX/Fjt;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field private final a:LX/1sf;

.field public final b:LX/Fjp;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Fjp;LX/1sf;)V
    .locals 0

    .prologue
    .line 2277958
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2277959
    iput-object p2, p0, LX/Fjt;->b:LX/Fjp;

    .line 2277960
    iput-object p3, p0, LX/Fjt;->a:LX/1sf;

    .line 2277961
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 3

    .prologue
    .line 2277962
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2277963
    const-string v0, "Update - Internal"

    invoke-virtual {p0, v0}, LX/Fjt;->setTitle(Ljava/lang/CharSequence;)V

    .line 2277964
    invoke-virtual {p0}, LX/Fjt;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2277965
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2277966
    const-string v2, "Force App Update"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2277967
    const-string v2, "Download and install the latest version right now (bypasses WiFi/version checks)"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2277968
    new-instance v2, LX/Fjs;

    invoke-direct {v2, p0, v0}, LX/Fjs;-><init>(LX/Fjt;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2277969
    invoke-virtual {p0, v1}, LX/Fjt;->addPreference(Landroid/preference/Preference;)Z

    .line 2277970
    return-void
.end method
