.class public LX/FF0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2216786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/FFH;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2216787
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2216788
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_2

    invoke-virtual {p0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel;

    .line 2216789
    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel;->a()LX/0Px;

    move-result-object v7

    .line 2216790
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2216791
    if-eqz v7, :cond_1

    .line 2216792
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    move v2, v3

    :goto_1
    if-ge v2, v9, :cond_1

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel$ActorsModel;

    .line 2216793
    if-eqz v1, :cond_0

    .line 2216794
    invoke-virtual {v1}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel$ActorsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 2216795
    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2216796
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2216797
    :cond_1
    new-instance v1, LX/FFH;

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel;->j()Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel$TextLineModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/games/graphql/MessengerGamesListQueryModels$InstantGameListUpdateLinesFragmentModel$UpdateLinesModel$TextLineModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/FFH;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2216798
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2216799
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 2216800
    :cond_2
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
