.class public final LX/Gja;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/39D;


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

.field private b:F

.field private c:LX/31M;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V
    .locals 0

    .prologue
    .line 2387872
    iput-object p1, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2387873
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    .line 2387874
    iget v1, v0, LX/GjY;->f:F

    move v0, v1

    .line 2387875
    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 2387876
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-static {v0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->k(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V

    .line 2387877
    :goto_0
    return-void

    .line 2387878
    :cond_0
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->b()V

    goto :goto_0
.end method

.method public final a(LX/31M;I)V
    .locals 4

    .prologue
    .line 2387879
    sget-object v0, LX/31M;->UP:LX/31M;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/Gja;->c:LX/31M;

    sget-object v1, LX/31M;->DOWN:LX/31M;

    if-ne v0, v1, :cond_0

    .line 2387880
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    neg-int v1, p2

    int-to-float v1, v1

    iget v2, p0, LX/Gja;->b:F

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v0, v2, v3}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->a$redex0(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;D)V

    .line 2387881
    :goto_0
    return-void

    .line 2387882
    :cond_0
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    neg-int v1, p2

    int-to-float v1, v1

    iget v2, p0, LX/Gja;->b:F

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v0, v2, v3}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->b$redex0(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;D)V

    goto :goto_0
.end method

.method public final a(FFLX/31M;)Z
    .locals 2

    .prologue
    .line 2387883
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->B:Lcom/facebook/greetingcards/render/RenderCardFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->B:Lcom/facebook/greetingcards/render/RenderCardFragment;

    .line 2387884
    iget-object v1, v0, Lcom/facebook/greetingcards/render/RenderCardFragment;->k:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    if-eqz v1, :cond_2

    .line 2387885
    iget-object v1, v0, Lcom/facebook/greetingcards/render/RenderCardFragment;->k:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    invoke-virtual {v1, p3}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->a(LX/31M;)Z

    move-result v1

    .line 2387886
    :goto_0
    move v0, v1

    .line 2387887
    if-eqz v0, :cond_0

    .line 2387888
    const/4 v0, 0x0

    .line 2387889
    :goto_1
    return v0

    .line 2387890
    :cond_0
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    .line 2387891
    iget v1, v0, LX/GjY;->f:F

    move v0, v1

    .line 2387892
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 2387893
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    invoke-virtual {v0}, LX/GjY;->a()V

    .line 2387894
    :cond_1
    const v0, 0x3f99999a    # 1.2f

    iget-object v1, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v1, v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    invoke-virtual {v1}, Lcom/facebook/greetingcards/render/DraggableView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, LX/Gja;->b:F

    .line 2387895
    iput-object p3, p0, LX/Gja;->c:LX/31M;

    .line 2387896
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2387897
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 8

    .prologue
    const-wide v6, 0x3feccccccccccccdL    # 0.9

    const-wide v4, 0x3fb999999999999aL    # 0.1

    .line 2387898
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    .line 2387899
    iget v1, v0, LX/GjY;->f:F

    move v0, v1

    .line 2387900
    iget-object v1, p0, LX/Gja;->c:LX/31M;

    sget-object v2, LX/31M;->UP:LX/31M;

    if-ne v1, v2, :cond_0

    .line 2387901
    neg-float p2, p2

    .line 2387902
    :cond_0
    iget v1, p0, LX/Gja;->b:F

    mul-float/2addr v1, p2

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    .line 2387903
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-lez v1, :cond_1

    sget-object v1, LX/31M;->UP:LX/31M;

    if-ne p3, v1, :cond_1

    iget-object v1, p0, LX/Gja;->c:LX/31M;

    sget-object v2, LX/31M;->DOWN:LX/31M;

    if-eq v1, v2, :cond_2

    :cond_1
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-lez v1, :cond_3

    sget-object v1, LX/31M;->DOWN:LX/31M;

    if-ne p3, v1, :cond_3

    iget-object v1, p0, LX/Gja;->c:LX/31M;

    sget-object v2, LX/31M;->UP:LX/31M;

    if-ne v1, v2, :cond_3

    .line 2387904
    :cond_2
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/render/DraggableView;->a()V

    .line 2387905
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-static {v0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->k(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V

    .line 2387906
    :goto_0
    return-void

    .line 2387907
    :cond_3
    float-to-double v2, v0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_4

    sget-object v1, LX/31M;->DOWN:LX/31M;

    if-ne p3, v1, :cond_4

    iget-object v1, p0, LX/Gja;->c:LX/31M;

    sget-object v2, LX/31M;->DOWN:LX/31M;

    if-eq v1, v2, :cond_5

    :cond_4
    float-to-double v2, v0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_6

    sget-object v1, LX/31M;->UP:LX/31M;

    if-ne p3, v1, :cond_6

    iget-object v1, p0, LX/Gja;->c:LX/31M;

    sget-object v2, LX/31M;->UP:LX/31M;

    if-ne v1, v2, :cond_6

    .line 2387908
    :cond_5
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->n:Lcom/facebook/greetingcards/render/DraggableView;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/render/DraggableView;->a()V

    .line 2387909
    iget-object v0, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->b()V

    goto :goto_0

    .line 2387910
    :cond_6
    iget-object v1, p0, LX/Gja;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v1, v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->w:LX/8YK;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/8YK;->a(D)LX/8YK;

    move-result-object v1

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/8YK;->b(D)LX/8YK;

    move-result-object v0

    invoke-virtual {v0}, LX/8YK;->f()LX/8YK;

    goto :goto_0
.end method
