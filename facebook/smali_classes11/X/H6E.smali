.class public final LX/H6E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 0

    .prologue
    .line 2425985
    iput-object p1, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2425986
    const-string v0, "OfferDetailPageFragment"

    const-string v1, "Error loading offers"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2425987
    iget-object v0, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    .line 2425988
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2425989
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2425990
    if-eqz p1, :cond_1

    .line 2425991
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425992
    if-eqz v0, :cond_1

    const v1, -0x14a5a4ff

    .line 2425993
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425994
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->L()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    if-ne v1, v0, :cond_1

    .line 2425995
    iget-object v1, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425996
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425997
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    invoke-static {v0}, LX/H83;->c(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;)LX/H83;

    move-result-object v0

    .line 2425998
    iput-object v0, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425999
    iget-object v0, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->u()Z

    move-result v1

    .line 2426000
    iput-boolean v1, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    .line 2426001
    iget-object v0, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2426002
    iget-object v0, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->n()Ljava/lang/String;

    move-result-object v1

    .line 2426003
    iput-object v1, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    .line 2426004
    :cond_0
    iget-object v0, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->Q(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    .line 2426005
    iget-object v0, p0, LX/H6E;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment$2$1;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment$2$1;-><init>(LX/H6E;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2426006
    :cond_1
    return-void
.end method
