.class public LX/GHh;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

.field public b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2335148
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2335149
    return-void
.end method

.method public static b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2335150
    invoke-static {}, Ljava/text/NumberFormat;->getCurrencyInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 2335151
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 2335152
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 2335153
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/GHh;)V
    .locals 11

    .prologue
    .line 2335154
    iget-object v0, p0, LX/GHh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    if-nez v0, :cond_0

    .line 2335155
    :goto_0
    return-void

    .line 2335156
    :cond_0
    iget-object v0, p0, LX/GHh;->a:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2335157
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->l()Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;->UNSETTLED:Lcom/facebook/graphql/enums/GraphQLAdAccountStatus;

    if-ne v1, v2, :cond_1

    .line 2335158
    iget-object v1, p0, LX/GHh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    const/16 v10, 0x21

    const/4 v9, 0x1

    .line 2335159
    iget-object v2, p0, LX/GHh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2335160
    iget-object v2, p0, LX/GHh;->a:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v4, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v2, v4, :cond_2

    const v2, 0x7f080b10

    .line 2335161
    :goto_1
    new-instance v4, LX/GHf;

    invoke-direct {v4, p0, v3}, LX/GHf;-><init>(LX/GHh;Landroid/content/res/Resources;)V

    .line 2335162
    new-instance v5, LX/47x;

    invoke-direct {v5, v3}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "{balance}"

    aput-object v8, v6, v7

    const-string v7, "{link}"

    aput-object v7, v6, v9

    invoke-virtual {v3, v2, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v2

    const-string v5, "{balance}"

    invoke-static {v0}, LX/GHh;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Landroid/text/style/StyleSpan;

    invoke-direct {v7, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2, v5, v6, v7, v10}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v2

    const-string v5, "{link}"

    const v6, 0x7f080b12

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v5, v3, v4, v10}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v2

    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v2

    move-object v0, v2

    .line 2335163
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2335164
    iget-object v0, p0, LX/GHh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2335165
    iget-object v0, p0, LX/GHh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_0

    .line 2335166
    :cond_1
    iget-object v0, p0, LX/GHh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2335167
    :cond_2
    const v2, 0x7f080b11

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2335168
    invoke-super {p0}, LX/GHg;->a()V

    .line 2335169
    const/4 v0, 0x0

    iput-object v0, p0, LX/GHh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2335170
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335171
    check-cast p1, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2335172
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2335173
    iput-object p1, p0, LX/GHh;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2335174
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335175
    new-instance v1, LX/GHd;

    invoke-direct {v1, p0}, LX/GHd;-><init>(LX/GHh;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2335176
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335177
    const/4 v1, 0x6

    new-instance v2, LX/GHe;

    invoke-direct {v2, p0}, LX/GHe;-><init>(LX/GHh;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2335178
    invoke-static {p0}, LX/GHh;->b(LX/GHh;)V

    .line 2335179
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2335180
    check-cast p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    .line 2335181
    iput-object p1, p0, LX/GHh;->a:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    .line 2335182
    return-void
.end method
