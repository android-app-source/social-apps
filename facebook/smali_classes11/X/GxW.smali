.class public final LX/GxW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:LX/GxZ;


# direct methods
.method public constructor <init>(LX/GxZ;)V
    .locals 0

    .prologue
    .line 2408149
    iput-object p1, p0, LX/GxW;->a:LX/GxZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 2408150
    iget-object v0, p0, LX/GxW;->a:LX/GxZ;

    .line 2408151
    iget-boolean v1, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->f:Z

    move v0, v1

    .line 2408152
    if-eqz v0, :cond_1

    .line 2408153
    :cond_0
    :goto_0
    return v3

    .line 2408154
    :cond_1
    check-cast p1, Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getHitTestResult()Landroid/webkit/WebView$HitTestResult;

    move-result-object v0

    .line 2408155
    invoke-virtual {v0}, Landroid/webkit/WebView$HitTestResult;->getType()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    .line 2408156
    invoke-virtual {v0}, Landroid/webkit/WebView$HitTestResult;->getExtra()Ljava/lang/String;

    move-result-object v0

    .line 2408157
    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2408158
    iget-object v1, p0, LX/GxW;->a:LX/GxZ;

    .line 2408159
    if-nez v0, :cond_2

    .line 2408160
    :goto_1
    goto :goto_0

    .line 2408161
    :cond_2
    invoke-virtual {v1}, LX/GxZ;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2408162
    :try_start_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2408163
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v5

    const-string v6, "m.facebook.com"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/l.php"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2408164
    const-string v5, "u"

    invoke-virtual {v2, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2408165
    :cond_3
    :goto_2
    move-object v5, v0

    .line 2408166
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2408167
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2408168
    const p0, 0x7f08355f

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408169
    const p0, 0x7f08355f

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v6, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408170
    const p0, 0x7f083560

    invoke-virtual {v4, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408171
    const p0, 0x7f083560

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v6, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2408172
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p0

    new-array p0, p0, [Ljava/lang/CharSequence;

    .line 2408173
    invoke-interface {v2, p0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/CharSequence;

    .line 2408174
    new-instance p0, LX/0ju;

    invoke-direct {p0, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    new-instance p1, LX/GxV;

    invoke-direct {p1, v1, v6, v4, v5}, LX/GxV;-><init>(LX/GxZ;Ljava/util/List;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v2, p1}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    new-instance v4, LX/GxU;

    invoke-direct {v4, v1}, LX/GxU;-><init>(LX/GxZ;)V

    invoke-virtual {v2, v4}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    move-result-object v2

    iput-object v2, v1, LX/GxZ;->c:LX/2EJ;

    goto/16 :goto_1

    .line 2408175
    :catch_0
    goto :goto_2
.end method
