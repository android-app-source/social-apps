.class public LX/Fqi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BS1;


# instance fields
.field public final a:LX/Fqj;

.field private final b:LX/0ad;

.field private final c:LX/BS5;

.field public final d:LX/Fsr;

.field public e:LX/BQ1;

.field private f:I

.field public g:Landroid/view/View;

.field private final h:LX/EtN;

.field private final i:LX/Fqh;

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Fqj;LX/0ad;LX/BS5;LX/Fsr;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2294408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294409
    iput v1, p0, LX/Fqi;->f:I

    .line 2294410
    new-instance v0, LX/Fqe;

    invoke-direct {v0, p0}, LX/Fqe;-><init>(LX/Fqi;)V

    iput-object v0, p0, LX/Fqi;->h:LX/EtN;

    .line 2294411
    new-instance v0, LX/Fqh;

    invoke-direct {v0, p0}, LX/Fqh;-><init>(LX/Fqi;)V

    iput-object v0, p0, LX/Fqi;->i:LX/Fqh;

    .line 2294412
    new-instance v0, LX/Fqg;

    invoke-direct {v0, p0}, LX/Fqg;-><init>(LX/Fqi;)V

    iput-object v0, p0, LX/Fqi;->j:LX/0Ot;

    .line 2294413
    iput-object p1, p0, LX/Fqi;->a:LX/Fqj;

    .line 2294414
    iput-object p2, p0, LX/Fqi;->b:LX/0ad;

    .line 2294415
    iput-object p3, p0, LX/Fqi;->c:LX/BS5;

    .line 2294416
    iput-object p4, p0, LX/Fqi;->d:LX/Fsr;

    .line 2294417
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2294418
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/fig/actionbar/FigActionBar;

    if-eqz v0, :cond_0

    .line 2294419
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->clear()V

    .line 2294420
    :goto_0
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2294421
    return-void

    .line 2294422
    :cond_0
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    invoke-virtual {v0}, LX/AhO;->clear()V

    goto :goto_0
.end method

.method private a(LX/BQ1;LX/5SB;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2294356
    if-eqz p2, :cond_1

    invoke-virtual {p2}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v2

    .line 2294357
    :goto_0
    if-eqz p1, :cond_0

    .line 2294358
    iget-object v0, p1, LX/BQ1;->d:LX/5wQ;

    move-object v0, v0

    .line 2294359
    if-eqz v0, :cond_0

    if-nez p2, :cond_2

    :cond_0
    if-nez v3, :cond_2

    .line 2294360
    invoke-direct {p0}, LX/Fqi;->a()V

    move v0, v1

    .line 2294361
    :goto_1
    return v0

    :cond_1
    move v3, v1

    .line 2294362
    goto :goto_0

    .line 2294363
    :cond_2
    iget-object v0, p0, LX/Fqi;->e:LX/BQ1;

    if-eqz v0, :cond_3

    iget v0, p0, LX/Fqi;->f:I

    iget-object v4, p0, LX/Fqi;->e:LX/BQ1;

    .line 2294364
    iget v5, v4, LX/BPy;->d:I

    move v4, v5

    .line 2294365
    if-lt v0, v4, :cond_3

    move v0, v1

    .line 2294366
    goto :goto_1

    .line 2294367
    :cond_3
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ1;

    iput-object v0, p0, LX/Fqi;->e:LX/BQ1;

    .line 2294368
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2294369
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/fig/actionbar/FigActionBar;

    if-eqz v0, :cond_4

    .line 2294370
    iget-object v0, p0, LX/Fqi;->b:LX/0ad;

    sget-short v4, LX/0wf;->W:S

    invoke-interface {v0, v4, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 2294371
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/actionbar/FigActionBar;

    iget-object v5, p0, LX/Fqi;->h:LX/EtN;

    .line 2294372
    iput-object v5, v0, LX/EtL;->b:LX/EtN;

    .line 2294373
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0, v4, v2}, LX/EtL;->a(ZZ)V

    .line 2294374
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->a()V

    .line 2294375
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->clear()V

    .line 2294376
    :goto_2
    if-eqz v3, :cond_5

    .line 2294377
    iget-object v0, p0, LX/Fqi;->c:LX/BS5;

    .line 2294378
    iget-object v1, p1, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v1}, LX/5wQ;->t()Z

    move-result v1

    move v1, v1

    .line 2294379
    invoke-virtual {v0, v1, p0}, LX/BS5;->a(ZLX/BS1;)V

    .line 2294380
    :goto_3
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/fig/actionbar/FigActionBar;

    if-eqz v0, :cond_7

    .line 2294381
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->b()V

    .line 2294382
    :goto_4
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->forceLayout()V

    .line 2294383
    iget-object v0, p0, LX/Fqi;->e:LX/BQ1;

    .line 2294384
    iget v1, v0, LX/BPy;->d:I

    move v0, v1

    .line 2294385
    iput v0, p0, LX/Fqi;->f:I

    move v0, v2

    .line 2294386
    goto :goto_1

    .line 2294387
    :cond_4
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    invoke-virtual {v0}, LX/AhO;->b()V

    .line 2294388
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    invoke-virtual {v0}, LX/AhO;->clear()V

    .line 2294389
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, LX/AhO;->setMaxNumOfVisibleButtons(I)V

    .line 2294390
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    iget-object v4, p0, LX/Fqi;->i:LX/Fqh;

    .line 2294391
    iput-object v4, v0, LX/AhO;->b:LX/AhN;

    .line 2294392
    goto :goto_2

    .line 2294393
    :cond_5
    iget v0, p1, LX/BPy;->d:I

    move v0, v0

    .line 2294394
    if-ne v0, v2, :cond_6

    .line 2294395
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, -0x1

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 2294396
    :cond_6
    iget-object v0, p0, LX/Fqi;->c:LX/BS5;

    .line 2294397
    iget-object v3, p1, LX/BQ1;->d:LX/5wQ;

    move-object v3, v3

    .line 2294398
    invoke-virtual {v0, v3, p0, v1}, LX/BS5;->a(LX/5wQ;LX/BS1;Z)V

    goto :goto_3

    .line 2294399
    :cond_7
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    invoke-virtual {v0}, LX/AhO;->d()V

    goto :goto_4
.end method

.method private b(IIIIZZZZ)LX/3qv;
    .locals 2
    .param p1    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2294400
    if-eqz p6, :cond_1

    .line 2294401
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/fig/actionbar/FigActionBar;

    if-eqz v0, :cond_0

    .line 2294402
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0, v1, p1, v1, p2}, LX/EtL;->a(IIII)LX/3qv;

    move-result-object v0

    .line 2294403
    :goto_0
    invoke-interface {v0, p4}, LX/3qv;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p7}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p8}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 2294404
    invoke-interface {v0, p3}, LX/3qv;->setIcon(I)Landroid/view/MenuItem;

    .line 2294405
    :goto_1
    return-object v0

    .line 2294406
    :cond_0
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    invoke-virtual {v0, v1, p1, v1, p2}, LX/AhO;->a(IIII)LX/3qv;

    move-result-object v0

    goto :goto_0

    .line 2294407
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/Fqi;
    .locals 5

    .prologue
    .line 2294354
    new-instance v4, LX/Fqi;

    invoke-static {p0}, LX/Fqj;->b(LX/0QB;)LX/Fqj;

    move-result-object v0

    check-cast v0, LX/Fqj;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/BS5;->a(LX/0QB;)LX/BS5;

    move-result-object v2

    check-cast v2, LX/BS5;

    invoke-static {p0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v3

    check-cast v3, LX/Fsr;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Fqi;-><init>(LX/Fqj;LX/0ad;LX/BS5;LX/Fsr;)V

    .line 2294355
    return-object v4
.end method


# virtual methods
.method public final a(IIIIZZ)V
    .locals 9
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 2294352
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v8, v7

    invoke-virtual/range {v0 .. v8}, LX/Fqi;->a(IIIIZZZZ)V

    .line 2294353
    return-void
.end method

.method public final a(IIIIZZZZ)V
    .locals 5
    .param p1    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2294326
    invoke-direct/range {p0 .. p8}, LX/Fqi;->b(IIIIZZZZ)LX/3qv;

    move-result-object v3

    .line 2294327
    if-nez v3, :cond_1

    .line 2294328
    :cond_0
    :goto_0
    return-void

    .line 2294329
    :cond_1
    const/16 v0, 0x9

    if-ne p1, v0, :cond_4

    .line 2294330
    iget-object v0, p0, LX/Fqi;->e:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ak()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2294331
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 2294332
    iget-object v0, p0, LX/Fqi;->e:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ak()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2294333
    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 2294334
    iget-object v0, p0, LX/Fqi;->e:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ak()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2294335
    invoke-virtual {v4, v0, v2}, LX/15i;->j(II)I

    move-result v0

    if-lez v0, :cond_7

    move v0, v1

    :goto_3
    if-eqz v0, :cond_0

    .line 2294336
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    instance-of v0, v0, Lcom/facebook/fig/actionbar/FigActionBar;

    if-eqz v0, :cond_9

    .line 2294337
    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/actionbar/FigActionBar;

    .line 2294338
    invoke-static {v0, p1}, LX/EtL;->a(LX/EtL;I)LX/EtO;

    move-result-object v2

    .line 2294339
    if-eqz v2, :cond_2

    .line 2294340
    iget-boolean v3, v2, LX/EtO;->j:Z

    if-ne v1, v3, :cond_a

    .line 2294341
    :cond_2
    :goto_4
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2294342
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_3

    .line 2294343
    :cond_9
    new-instance v1, LX/Fqk;

    iget-object v0, p0, LX/Fqi;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/Fqk;-><init>(Landroid/content/Context;)V

    .line 2294344
    iget-object v0, p0, LX/Fqi;->e:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ak()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v4, v0, v2}, LX/15i;->j(II)I

    move-result v0

    invoke-virtual {v1, v0}, LX/Fqk;->setCount(I)V

    .line 2294345
    iget-object v0, p0, LX/Fqi;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, LX/Fqk;->setClickListener(Landroid/view/View$OnClickListener;)V

    .line 2294346
    invoke-interface {v3, v1}, LX/3qv;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2294347
    :cond_a
    iput-boolean v1, v2, LX/EtO;->j:Z

    .line 2294348
    invoke-virtual {v2}, LX/EtO;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-nez v3, :cond_b

    const-string v3, ""

    .line 2294349
    :goto_5
    const-string v0, "\u25cf "

    const-string p1, ""

    invoke-virtual {v3, v0, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 2294350
    invoke-static {v2, v3}, LX/EtO;->a(LX/EtO;Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 2294351
    :cond_b
    invoke-virtual {v2}, LX/EtO;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_5
.end method

.method public final a(Landroid/view/View;LX/BQ1;LX/5SB;)Z
    .locals 2

    .prologue
    .line 2294322
    iput-object p1, p0, LX/Fqi;->g:Landroid/view/View;

    .line 2294323
    const-string v0, "TimelineActionBar.bindModel"

    const v1, -0x1e098b50

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2294324
    :try_start_0
    invoke-direct {p0, p2, p3}, LX/Fqi;->a(LX/BQ1;LX/5SB;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2294325
    const v1, 0x482decde

    invoke-static {v1}, LX/02m;->a(I)V

    return v0

    :catchall_0
    move-exception v0

    const v1, -0x70468bc0

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
