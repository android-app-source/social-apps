.class public LX/Fvu;
.super LX/Fv4;
.source ""

# interfaces
.implements LX/Fv6;


# instance fields
.field private final e:LX/FwD;

.field private final f:LX/Fw8;

.field private final g:LX/Fup;

.field private final h:LX/Fw7;

.field private final i:LX/Fv9;

.field private final j:LX/BQB;

.field private final k:Z

.field private final l:[I

.field private m:I


# direct methods
.method public constructor <init>(LX/0ad;LX/FwD;LX/Fw8;LX/BQB;LX/Fuq;LX/Fw7;Landroid/content/Context;LX/BQ1;LX/G4m;LX/BP0;ZLX/Fv9;)V
    .locals 1
    .param p7    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/Fv9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302237
    invoke-direct {p0, p7, p1, p10, p8}, LX/Fv4;-><init>(Landroid/content/Context;LX/0ad;LX/BP0;LX/BQ1;)V

    .line 2302238
    invoke-static {}, LX/Fvt;->cachedValues()[LX/Fvt;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/Fvu;->l:[I

    .line 2302239
    const/4 v0, -0x1

    iput v0, p0, LX/Fvu;->m:I

    .line 2302240
    iput-object p2, p0, LX/Fvu;->e:LX/FwD;

    .line 2302241
    iput-object p3, p0, LX/Fvu;->f:LX/Fw8;

    .line 2302242
    iput-object p6, p0, LX/Fvu;->h:LX/Fw7;

    .line 2302243
    iput-boolean p11, p0, LX/Fvu;->k:Z

    .line 2302244
    iput-object p12, p0, LX/Fvu;->i:LX/Fv9;

    .line 2302245
    iput-object p4, p0, LX/Fvu;->j:LX/BQB;

    .line 2302246
    invoke-virtual {p5, p8, p9, p10}, LX/Fuq;->a(LX/BQ1;LX/G4m;LX/BP0;)LX/Fup;

    move-result-object v0

    iput-object v0, p0, LX/Fvu;->g:LX/Fup;

    .line 2302247
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2302236
    invoke-static {}, LX/Fvt;->cachedValues()[LX/Fvt;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2302230
    invoke-static {}, LX/Fvt;->cachedValues()[LX/Fvt;

    move-result-object v0

    aget-object v1, v0, p1

    .line 2302231
    const v0, 0x7f0314e7

    .line 2302232
    iget-object v2, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    .line 2302233
    check-cast v0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    .line 2302234
    sget-object v2, LX/Fvs;->a:[I

    invoke-virtual {v1}, LX/Fvt;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2302235
    invoke-static {p1}, LX/Fv4;->b(I)Landroid/view/View;

    move-result-object v0

    :pswitch_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2302157
    invoke-static {}, LX/Fvt;->cachedValues()[LX/Fvt;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a([Z)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2302208
    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2302209
    sget-object v0, LX/Fvt;->EMPTY:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302210
    sget-object v0, LX/Fvt;->FAVORITE_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302211
    sget-object v0, LX/Fvt;->SUGGESTED_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302212
    :goto_0
    return-void

    .line 2302213
    :cond_0
    iget-object v0, p0, LX/Fvu;->e:LX/FwD;

    iget-object v1, p0, LX/Fv4;->d:LX/BQ1;

    iget-object v2, p0, LX/Fv4;->c:LX/BP0;

    iget-object v3, p0, LX/Fvu;->h:LX/Fw7;

    iget-object v4, p0, LX/Fv4;->c:LX/BP0;

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v3, v4, v5}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/FwD;->a(LX/BQ1;LX/5SB;LX/Fve;)Ljava/lang/Integer;

    move-result-object v0

    .line 2302214
    iget-object v1, p0, LX/Fvu;->j:LX/BQB;

    invoke-static {v0}, LX/FwC;->a(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v2

    .line 2302215
    iput-object v2, v1, LX/BQB;->H:Ljava/lang/String;

    .line 2302216
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2302217
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "not supported intro view type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2302218
    :pswitch_0
    sget-object v0, LX/Fvt;->EMPTY:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302219
    sget-object v0, LX/Fvt;->FAVORITE_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302220
    sget-object v0, LX/Fvt;->SUGGESTED_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    goto :goto_0

    .line 2302221
    :pswitch_1
    sget-object v0, LX/Fvt;->EMPTY:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v7, p1, v0

    .line 2302222
    sget-object v0, LX/Fvt;->FAVORITE_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302223
    sget-object v0, LX/Fvt;->SUGGESTED_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    goto :goto_0

    .line 2302224
    :pswitch_2
    sget-object v0, LX/Fvt;->EMPTY:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302225
    sget-object v0, LX/Fvt;->FAVORITE_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v7, p1, v0

    .line 2302226
    sget-object v0, LX/Fvt;->SUGGESTED_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    goto/16 :goto_0

    .line 2302227
    :pswitch_3
    sget-object v0, LX/Fvt;->EMPTY:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302228
    sget-object v0, LX/Fvt;->FAVORITE_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302229
    sget-object v0, LX/Fvt;->SUGGESTED_PHOTOS:LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    aput-boolean v7, p1, v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2302165
    invoke-static {}, LX/Fvt;->cachedValues()[LX/Fvt;

    move-result-object v2

    aget-object v2, v2, p2

    .line 2302166
    iget-object v3, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    .line 2302167
    iget v4, p0, LX/Fvu;->m:I

    if-ne v4, v3, :cond_0

    iget-object v4, p0, LX/Fvu;->l:[I

    invoke-virtual {v2}, LX/Fvt;->ordinal()I

    move-result v5

    aget v4, v4, v5

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    .line 2302168
    iget v6, v5, LX/BPy;->c:I

    move v5, v6

    .line 2302169
    if-ne v4, v5, :cond_0

    move v1, v0

    .line 2302170
    :goto_0
    return v1

    .line 2302171
    :cond_0
    iput v3, p0, LX/Fvu;->m:I

    .line 2302172
    iget-object v3, p0, LX/Fvu;->l:[I

    invoke-virtual {v2}, LX/Fvt;->ordinal()I

    move-result v4

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    .line 2302173
    iget v6, v5, LX/BPy;->c:I

    move v5, v6

    .line 2302174
    aput v5, v3, v4

    .line 2302175
    iget-boolean v3, p0, LX/Fvu;->k:Z

    if-nez v3, :cond_1

    iget-object v3, p0, LX/Fvu;->f:LX/Fw8;

    iget-object v4, p0, LX/Fv4;->c:LX/BP0;

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v3, v4, v5}, LX/Fw8;->a(LX/5SB;LX/BQ1;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 2302176
    :cond_1
    check-cast p1, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    .line 2302177
    iget-object v3, p0, LX/Fvu;->g:LX/Fup;

    const/4 v7, 0x0

    .line 2302178
    invoke-virtual {p1, v7}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->setVisibility(I)V

    .line 2302179
    iget-object v4, v3, LX/Fup;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0df0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2302180
    iget-object v5, v3, LX/Fup;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0df1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2302181
    invoke-virtual {p1, v4, v7, v5, v7}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->setPadding(IIII)V

    .line 2302182
    const v4, 0x7f0a00d5

    invoke-virtual {p1, v4}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->setBackgroundResource(I)V

    .line 2302183
    sget-object v3, LX/Fvs;->a:[I

    invoke-virtual {v2}, LX/Fvt;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2302184
    invoke-static {p2}, LX/Fv4;->c(I)Z

    move-result v1

    goto :goto_0

    .line 2302185
    :pswitch_0
    iget-object v2, p0, LX/Fvu;->g:LX/Fup;

    .line 2302186
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->b()V

    .line 2302187
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d()V

    .line 2302188
    invoke-static {v2}, LX/Fup;->a(LX/Fup;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Landroid/view/View$OnClickListener;)V

    .line 2302189
    invoke-static {p1, v0}, LX/Fup;->d(Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;Z)V

    .line 2302190
    iget-object v0, p0, LX/Fvu;->i:LX/Fv9;

    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getFavPhotosEmptyView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/Fv9;->b(Landroid/view/View;)V

    goto :goto_0

    .line 2302191
    :pswitch_1
    iget-object v2, p0, LX/Fvu;->g:LX/Fup;

    .line 2302192
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c()V

    .line 2302193
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->d()V

    .line 2302194
    iget-object v3, v2, LX/Fup;->e:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, LX/Fup;->g:LX/5SB;

    invoke-virtual {v4}, LX/5SB;->i()Z

    move-result v4

    iget-object v5, v2, LX/Fup;->e:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->ad()LX/0Px;

    move-result-object v5

    invoke-static {v2}, LX/Fup;->a(LX/Fup;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {p1, v3, v4, v5, v6}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Ljava/lang/String;ZLX/0Px;Landroid/view/View$OnClickListener;)V

    .line 2302195
    invoke-static {p1, v0}, LX/Fup;->d(Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;Z)V

    .line 2302196
    iget-object v0, p0, LX/Fvu;->i:LX/Fv9;

    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getFavPhotosView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/Fv9;->a(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2302197
    :pswitch_2
    iget-object v2, p0, LX/Fvu;->g:LX/Fup;

    .line 2302198
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->c()V

    .line 2302199
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->b()V

    .line 2302200
    iget-object v3, v2, LX/Fup;->e:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, LX/Fup;->e:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->af()LX/0Px;

    move-result-object v4

    .line 2302201
    iget-object v5, v2, LX/Fup;->i:Landroid/view/View$OnClickListener;

    if-nez v5, :cond_2

    .line 2302202
    new-instance v5, LX/Fun;

    invoke-direct {v5, v2}, LX/Fun;-><init>(LX/Fup;)V

    iput-object v5, v2, LX/Fup;->i:Landroid/view/View$OnClickListener;

    .line 2302203
    :cond_2
    iget-object v5, v2, LX/Fup;->i:Landroid/view/View$OnClickListener;

    move-object v5, v5

    .line 2302204
    new-instance v6, LX/Fuo;

    invoke-direct {v6, v2}, LX/Fuo;-><init>(LX/Fup;)V

    move-object v6, v6

    .line 2302205
    invoke-virtual {p1, v3, v4, v5, v6}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->a(Ljava/lang/String;LX/0Px;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 2302206
    invoke-static {p1, v0}, LX/Fup;->d(Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;Z)V

    .line 2302207
    iget-object v0, p0, LX/Fvu;->i:LX/Fv9;

    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->getSuggestedPhotosView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/Fv9;->a(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2302160
    const/4 v0, -0x1

    iput v0, p0, LX/Fvu;->m:I

    move v0, v1

    .line 2302161
    :goto_0
    iget-object v2, p0, LX/Fvu;->l:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2302162
    iget-object v2, p0, LX/Fvu;->l:[I

    aput v1, v2, v0

    .line 2302163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2302164
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2302159
    invoke-virtual {p0, p1}, LX/Fv4;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fvt;

    invoke-virtual {v0}, LX/Fvt;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2302158
    invoke-static {}, LX/Fvt;->cachedValues()[LX/Fvt;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
