.class public final LX/Fwl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EQO;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V
    .locals 1

    .prologue
    .line 2303614
    iput-object p1, p0, LX/Fwl;->a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303615
    const/4 v0, 0x0

    iput v0, p0, LX/Fwl;->b:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2303616
    iget v0, p0, LX/Fwl;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/Fwl;->a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    iget-object v1, v1, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->t:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, LX/Fwl;->b:I

    .line 2303617
    iget-object v0, p0, LX/Fwl;->a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    iget-object v1, v0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    iget-object v0, p0, LX/Fwl;->a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    iget-object v0, v0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->t:Ljava/util/List;

    iget v2, p0, LX/Fwl;->b:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2303618
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2303619
    iget-object v0, p0, LX/Fwl;->a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    iget-object v0, v0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setHintTextColor(I)V

    .line 2303620
    return-void
.end method
