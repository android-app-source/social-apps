.class public final LX/F5u;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2199716
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 2199717
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2199718
    :goto_0
    return v1

    .line 2199719
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 2199720
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2199721
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2199722
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 2199723
    const-string v9, "add_future_reports"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2199724
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 2199725
    :cond_1
    const-string v9, "can_edit_members"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2199726
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 2199727
    :cond_2
    const-string v9, "group_name_placeholder"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2199728
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2199729
    :cond_3
    const-string v9, "group_purpose"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2199730
    invoke-static {p0, p1}, LX/F5t;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2199731
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2199732
    :cond_5
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2199733
    if-eqz v3, :cond_6

    .line 2199734
    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 2199735
    :cond_6
    if-eqz v0, :cond_7

    .line 2199736
    invoke-virtual {p1, v2, v6}, LX/186;->a(IZ)V

    .line 2199737
    :cond_7
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2199738
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2199739
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2199740
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2199741
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2199742
    if-eqz v0, :cond_0

    .line 2199743
    const-string v1, "add_future_reports"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2199744
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2199745
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2199746
    if-eqz v0, :cond_1

    .line 2199747
    const-string v1, "can_edit_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2199748
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2199749
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2199750
    if-eqz v0, :cond_2

    .line 2199751
    const-string v1, "group_name_placeholder"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2199752
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2199753
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2199754
    if-eqz v0, :cond_3

    .line 2199755
    const-string v1, "group_purpose"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2199756
    invoke-static {p0, v0, p2}, LX/F5t;->a(LX/15i;ILX/0nX;)V

    .line 2199757
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2199758
    return-void
.end method
