.class public final LX/GT8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

.field public final synthetic c:LX/GTB;


# direct methods
.method public constructor <init>(LX/GTB;Landroid/content/Context;Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;)V
    .locals 0

    .prologue
    .line 2355181
    iput-object p1, p0, LX/GT8;->c:LX/GTB;

    iput-object p2, p0, LX/GT8;->a:Landroid/content/Context;

    iput-object p3, p0, LX/GT8;->b:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2355182
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2355183
    :goto_0
    return v0

    .line 2355184
    :pswitch_0
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    const v4, 0x7f083774

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    const v4, 0x7f083775

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, LX/GT8;->b:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    invoke-virtual {v6}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->mx_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v2, p0, LX/GT8;->a:Landroid/content/Context;

    const v3, 0x7f083776

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/GT5;

    invoke-direct {v3, p0}, LX/GT5;-><init>(LX/GT8;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080017

    invoke-virtual {v0, v2, v7}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    move v0, v1

    .line 2355185
    goto :goto_0

    .line 2355186
    :pswitch_1
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    const v4, 0x7f08377c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    const v4, 0x7f08377d

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, LX/GT8;->b:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    invoke-virtual {v6}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->mx_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v2, p0, LX/GT8;->a:Landroid/content/Context;

    const v3, 0x7f08377e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/GT6;

    invoke-direct {v3, p0}, LX/GT6;-><init>(LX/GT8;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080017

    invoke-virtual {v0, v2, v7}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    move v0, v1

    .line 2355187
    goto/16 :goto_0

    .line 2355188
    :pswitch_2
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    const v4, 0x7f083778

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    iget-object v3, p0, LX/GT8;->a:Landroid/content/Context;

    const v4, 0x7f083779

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, LX/GT8;->b:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    invoke-virtual {v6}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v2, p0, LX/GT8;->a:Landroid/content/Context;

    const v3, 0x7f08377a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/GT7;

    invoke-direct {v3, p0}, LX/GT7;-><init>(LX/GT8;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080017

    invoke-virtual {v0, v2, v7}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    move v0, v1

    .line 2355189
    goto/16 :goto_0

    .line 2355190
    :pswitch_3
    iget-object v0, p0, LX/GT8;->c:LX/GTB;

    iget-object v0, v0, LX/GTB;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/GT8;->a:Landroid/content/Context;

    sget-object v3, LX/0ax;->dv:Ljava/lang/String;

    iget-object v4, p0, LX/GT8;->b:Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->mv_()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->RESOLVE_PROBLEM:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v6, LX/0wD;->APP_INVITES_FEED:LX/0wD;

    invoke-virtual {v6}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move v0, v1

    .line 2355191
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
