.class public LX/FN6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/1rd;

.field private final d:LX/2Uq;

.field private final e:LX/2Oq;

.field private f:Landroid/telephony/SubscriptionManager;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/03R;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1rd;LX/2Uq;LX/2Oq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2232218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232219
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/FN6;->g:LX/03R;

    .line 2232220
    iput-object p1, p0, LX/FN6;->a:Landroid/content/Context;

    .line 2232221
    iput-object p2, p0, LX/FN6;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2232222
    iput-object p3, p0, LX/FN6;->c:LX/1rd;

    .line 2232223
    iput-object p4, p0, LX/FN6;->d:LX/2Uq;

    .line 2232224
    iput-object p5, p0, LX/FN6;->e:LX/2Oq;

    .line 2232225
    iget-object v0, p0, LX/FN6;->c:LX/1rd;

    invoke-virtual {v0}, LX/1rd;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2232226
    iget-object v0, p0, LX/FN6;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/telephony/SubscriptionManager;->from(Landroid/content/Context;)Landroid/telephony/SubscriptionManager;

    move-result-object v0

    iput-object v0, p0, LX/FN6;->f:Landroid/telephony/SubscriptionManager;

    .line 2232227
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/FN6;
    .locals 6

    .prologue
    .line 2232215
    new-instance v0, LX/FN6;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/1rd;->b(LX/0QB;)LX/1rd;

    move-result-object v3

    check-cast v3, LX/1rd;

    invoke-static {p0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v4

    check-cast v4, LX/2Uq;

    invoke-static {p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v5

    check-cast v5, LX/2Oq;

    invoke-direct/range {v0 .. v5}, LX/FN6;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1rd;LX/2Uq;LX/2Oq;)V

    .line 2232216
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x16
    .end annotation

    .prologue
    .line 2232228
    iget-object v0, p0, LX/FN6;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->aR:LX/0Tn;

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    move v0, v0

    .line 2232229
    iget-object v1, p0, LX/FN6;->f:Landroid/telephony/SubscriptionManager;

    if-eqz v1, :cond_0

    if-ltz v0, :cond_0

    .line 2232230
    iget-object v1, p0, LX/FN6;->f:Landroid/telephony/SubscriptionManager;

    invoke-virtual {v1, v0}, Landroid/telephony/SubscriptionManager;->getActiveSubscriptionInfoForSimSlotIndex(I)Landroid/telephony/SubscriptionInfo;

    move-result-object v0

    .line 2232231
    if-eqz v0, :cond_0

    .line 2232232
    invoke-virtual {v0}, Landroid/telephony/SubscriptionInfo;->getSubscriptionId()I

    move-result v0

    .line 2232233
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2232217
    iget-object v0, p0, LX/FN6;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->aU:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2232205
    iget-object v2, p0, LX/FN6;->e:LX/2Oq;

    invoke-virtual {v2}, LX/2Oq;->a()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2232206
    :goto_0
    return v0

    .line 2232207
    :cond_0
    iget-object v2, p0, LX/FN6;->g:LX/03R;

    invoke-virtual {v2}, LX/03R;->isSet()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2232208
    iget-object v2, p0, LX/FN6;->c:LX/1rd;

    invoke-virtual {v2}, LX/1rd;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/FN6;->c:LX/1rd;

    invoke-virtual {v2}, LX/1rd;->a()I

    move-result v2

    if-le v2, v0, :cond_2

    iget-object v2, p0, LX/FN6;->d:LX/2Uq;

    .line 2232209
    iget-object v3, v2, LX/2Uq;->a:LX/0ad;

    sget-short v4, LX/6jD;->x:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v2, v3

    .line 2232210
    if-eqz v2, :cond_2

    .line 2232211
    :goto_1
    if-eqz v0, :cond_3

    sget-object v0, LX/03R;->YES:LX/03R;

    :goto_2
    iput-object v0, p0, LX/FN6;->g:LX/03R;

    .line 2232212
    :cond_1
    iget-object v0, p0, LX/FN6;->g:LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2232213
    goto :goto_1

    .line 2232214
    :cond_3
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_2
.end method
