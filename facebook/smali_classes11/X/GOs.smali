.class public final LX/GOs;
.super LX/GOr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GOr",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
        "Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2348017
    iput-object p1, p0, LX/GOs;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-direct {p0, p2, p3}, LX/GOr;-><init>(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method private a(Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 1

    .prologue
    .line 2348018
    invoke-virtual {p1, p2}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setPaymentMethod(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 2348019
    new-instance v0, LX/GOq;

    invoke-direct {v0, p0, p2}, LX/GOq;-><init>(LX/GOs;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    invoke-virtual {p1, v0}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2348020
    return-void
.end method

.method private b()Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;
    .locals 3

    .prologue
    .line 2348021
    new-instance v0, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    iget-object v1, p0, LX/GOs;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-direct {v0, v1}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;-><init>(Landroid/content/Context;)V

    .line 2348022
    iget-object v1, p0, LX/GOs;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-virtual {v1}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0572

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2348023
    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;->setPadding(IIII)V

    .line 2348024
    return-object v0
.end method


# virtual methods
.method public final synthetic a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2348025
    invoke-direct {p0}, LX/GOs;->b()Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2348026
    check-cast p1, Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;

    check-cast p2, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-direct {p0, p1, p2}, LX/GOs;->a(Lcom/facebook/payments/paymentmethods/picker/SimplePaymentMethodView;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    return-void
.end method
