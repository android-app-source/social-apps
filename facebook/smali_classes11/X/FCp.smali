.class public final enum LX/FCp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FCp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FCp;

.field public static final enum OTHER_HIGHLIGHTED:LX/FCp;

.field public static final enum OTHER_NORMAL:LX/FCp;

.field public static final enum SELF_HIGHLIGHTED:LX/FCp;

.field public static final enum SELF_NORMAL:LX/FCp;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2210610
    new-instance v0, LX/FCp;

    const-string v1, "SELF_NORMAL"

    invoke-direct {v0, v1, v2}, LX/FCp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCp;->SELF_NORMAL:LX/FCp;

    .line 2210611
    new-instance v0, LX/FCp;

    const-string v1, "SELF_HIGHLIGHTED"

    invoke-direct {v0, v1, v3}, LX/FCp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCp;->SELF_HIGHLIGHTED:LX/FCp;

    .line 2210612
    new-instance v0, LX/FCp;

    const-string v1, "OTHER_NORMAL"

    invoke-direct {v0, v1, v4}, LX/FCp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCp;->OTHER_NORMAL:LX/FCp;

    .line 2210613
    new-instance v0, LX/FCp;

    const-string v1, "OTHER_HIGHLIGHTED"

    invoke-direct {v0, v1, v5}, LX/FCp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCp;->OTHER_HIGHLIGHTED:LX/FCp;

    .line 2210614
    const/4 v0, 0x4

    new-array v0, v0, [LX/FCp;

    sget-object v1, LX/FCp;->SELF_NORMAL:LX/FCp;

    aput-object v1, v0, v2

    sget-object v1, LX/FCp;->SELF_HIGHLIGHTED:LX/FCp;

    aput-object v1, v0, v3

    sget-object v1, LX/FCp;->OTHER_NORMAL:LX/FCp;

    aput-object v1, v0, v4

    sget-object v1, LX/FCp;->OTHER_HIGHLIGHTED:LX/FCp;

    aput-object v1, v0, v5

    sput-object v0, LX/FCp;->$VALUES:[LX/FCp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2210615
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FCp;
    .locals 1

    .prologue
    .line 2210616
    const-class v0, LX/FCp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FCp;

    return-object v0
.end method

.method public static values()[LX/FCp;
    .locals 1

    .prologue
    .line 2210617
    sget-object v0, LX/FCp;->$VALUES:[LX/FCp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FCp;

    return-object v0
.end method
