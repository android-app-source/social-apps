.class public final LX/G2g;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final h:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final i:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final j:Z

.field public final k:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final l:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final m:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final n:Z

.field public final o:Z

.field public final p:Ljava/lang/String;

.field public final q:Z

.field public final r:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/net/Uri;IIIIIIIIZIIIZIZZLjava/lang/String;)V
    .locals 1
    .param p7    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p15    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2314409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314410
    iput-object p1, p0, LX/G2g;->a:Landroid/net/Uri;

    .line 2314411
    iput p2, p0, LX/G2g;->c:I

    .line 2314412
    iput p3, p0, LX/G2g;->b:I

    .line 2314413
    iput p4, p0, LX/G2g;->d:I

    .line 2314414
    iput p5, p0, LX/G2g;->e:I

    .line 2314415
    iput p6, p0, LX/G2g;->f:I

    .line 2314416
    iput p7, p0, LX/G2g;->g:I

    .line 2314417
    iput p8, p0, LX/G2g;->h:I

    .line 2314418
    iput p9, p0, LX/G2g;->i:I

    .line 2314419
    iput-boolean p10, p0, LX/G2g;->j:Z

    .line 2314420
    iput p11, p0, LX/G2g;->k:I

    .line 2314421
    iput p12, p0, LX/G2g;->l:I

    .line 2314422
    iput p13, p0, LX/G2g;->m:I

    .line 2314423
    iput-boolean p14, p0, LX/G2g;->q:Z

    .line 2314424
    move/from16 v0, p15

    iput v0, p0, LX/G2g;->r:I

    .line 2314425
    move/from16 v0, p16

    iput-boolean v0, p0, LX/G2g;->n:Z

    .line 2314426
    move/from16 v0, p17

    iput-boolean v0, p0, LX/G2g;->o:Z

    .line 2314427
    move-object/from16 v0, p18

    iput-object v0, p0, LX/G2g;->p:Ljava/lang/String;

    .line 2314428
    return-void
.end method
