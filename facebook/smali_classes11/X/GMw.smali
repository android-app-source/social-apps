.class public final LX/GMw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public final synthetic b:LX/GN1;


# direct methods
.method public constructor <init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 0

    .prologue
    .line 2345369
    iput-object p1, p0, LX/GMw;->b:LX/GN1;

    iput-object p2, p0, LX/GMw;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x58312b8c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2345370
    iget-object v1, p0, LX/GMw;->b:LX/GN1;

    .line 2345371
    iget-object v2, v1, LX/GN1;->d:LX/0ad;

    sget-short v4, LX/GDK;->l:S

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    move v1, v2

    .line 2345372
    if-eqz v1, :cond_0

    .line 2345373
    iget-object v1, p0, LX/GMw;->b:LX/GN1;

    iget-object v2, p0, LX/GMw;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345374
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2345375
    new-instance v5, LX/31Y;

    invoke-direct {v5, v4}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2345376
    const v4, 0x7f080acd

    invoke-virtual {v5, v4}, LX/0ju;->a(I)LX/0ju;

    .line 2345377
    const v4, 0x7f080ace

    invoke-virtual {v5, v4}, LX/0ju;->b(I)LX/0ju;

    .line 2345378
    const v4, 0x7f080ac3

    new-instance p0, LX/GMx;

    invoke-direct {p0, v1, v2, p1}, LX/GMx;-><init>(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/view/View;)V

    invoke-virtual {v5, v4, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2345379
    const v4, 0x7f080ab4

    new-instance p0, LX/GMy;

    invoke-direct {p0, v1}, LX/GMy;-><init>(LX/GN1;)V

    invoke-virtual {v5, v4, p0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2345380
    invoke-virtual {v5}, LX/0ju;->a()LX/2EJ;

    move-result-object v4

    invoke-virtual {v4}, LX/2EJ;->show()V

    .line 2345381
    const v1, -0xf94e2e

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2345382
    :goto_0
    return-void

    .line 2345383
    :cond_0
    iget-object v1, p0, LX/GMw;->b:LX/GN1;

    iget-object v2, p0, LX/GMw;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1, v2, p1}, LX/GN1;->a$redex0(LX/GN1;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/view/View;)V

    .line 2345384
    const v1, -0x75c0773b

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
