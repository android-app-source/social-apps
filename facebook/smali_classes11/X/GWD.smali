.class public LX/GWD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2360580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2360581
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2360570
    const-string v0, "product_item_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2360571
    const-string v1, "product_ref_type"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7iP;->getFromValue(Ljava/lang/String;)LX/7iP;

    move-result-object v1

    .line 2360572
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2360573
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2360574
    const-string p0, "product_item_id"

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2360575
    const-string p0, "product_ref_type"

    invoke-virtual {v0, p0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2360576
    new-instance p0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-direct {p0}, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;-><init>()V

    .line 2360577
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2360578
    move-object v0, p0

    .line 2360579
    return-object v0
.end method
