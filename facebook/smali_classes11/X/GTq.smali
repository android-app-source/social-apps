.class public final LX/GTq;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Landroid/support/v4/app/DialogFragment;Z)V
    .locals 0

    .prologue
    .line 2355926
    iput-object p1, p0, LX/GTq;->c:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iput-object p2, p0, LX/GTq;->a:Landroid/support/v4/app/DialogFragment;

    iput-boolean p3, p0, LX/GTq;->b:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2355927
    iget-object v0, p0, LX/GTq;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2355928
    iget-object v0, p0, LX/GTq;->c:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-boolean v1, p0, LX/GTq;->b:Z

    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->e(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V

    .line 2355929
    iget-object v0, p0, LX/GTq;->c:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-boolean v1, p0, LX/GTq;->b:Z

    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->d(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V

    .line 2355930
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2355931
    iget-object v0, p0, LX/GTq;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2355932
    sget-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->B:Ljava/lang/Class;

    const-string v1, "Failed to save NUX status"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355933
    iget-object v0, p0, LX/GTq;->c:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->y:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2355934
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2355935
    invoke-direct {p0}, LX/GTq;->a()V

    return-void
.end method
