.class public final LX/FZB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/debug/SearchDebugActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/search/debug/SearchDebugActivity;)V
    .locals 0

    .prologue
    .line 2256972
    iput-object p1, p0, LX/FZB;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2256973
    iget-object v0, p0, LX/FZB;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v0, v0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/7CP;->e:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/7CP;->f:LX/0Tn;

    invoke-interface {v0, v1, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2256974
    iget-object v0, p0, LX/FZB;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-virtual {v0}, Lcom/facebook/search/debug/SearchDebugActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Reset stored client data for Learning Nux."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2256975
    return v3
.end method
