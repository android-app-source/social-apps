.class public final enum LX/FUv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FUv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FUv;

.field public static final enum ANIMATION_END:LX/FUv;

.field public static final enum BEGIN_DRAG:LX/FUv;

.field public static final enum END_DRAG:LX/FUv;

.field public static final enum MOMENTUM_BEGIN:LX/FUv;

.field public static final enum MOMENTUM_END:LX/FUv;

.field public static final enum SCROLL:LX/FUv;


# instance fields
.field private final mJSEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2250237
    new-instance v0, LX/FUv;

    const-string v1, "BEGIN_DRAG"

    const-string v2, "topScrollBeginDrag"

    invoke-direct {v0, v1, v4, v2}, LX/FUv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUv;->BEGIN_DRAG:LX/FUv;

    .line 2250238
    new-instance v0, LX/FUv;

    const-string v1, "END_DRAG"

    const-string v2, "topScrollEndDrag"

    invoke-direct {v0, v1, v5, v2}, LX/FUv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUv;->END_DRAG:LX/FUv;

    .line 2250239
    new-instance v0, LX/FUv;

    const-string v1, "SCROLL"

    const-string v2, "topScroll"

    invoke-direct {v0, v1, v6, v2}, LX/FUv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUv;->SCROLL:LX/FUv;

    .line 2250240
    new-instance v0, LX/FUv;

    const-string v1, "MOMENTUM_BEGIN"

    const-string v2, "topMomentumScrollBegin"

    invoke-direct {v0, v1, v7, v2}, LX/FUv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUv;->MOMENTUM_BEGIN:LX/FUv;

    .line 2250241
    new-instance v0, LX/FUv;

    const-string v1, "MOMENTUM_END"

    const-string v2, "topMomentumScrollEnd"

    invoke-direct {v0, v1, v8, v2}, LX/FUv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUv;->MOMENTUM_END:LX/FUv;

    .line 2250242
    new-instance v0, LX/FUv;

    const-string v1, "ANIMATION_END"

    const/4 v2, 0x5

    const-string v3, "topScrollAnimationEnd"

    invoke-direct {v0, v1, v2, v3}, LX/FUv;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUv;->ANIMATION_END:LX/FUv;

    .line 2250243
    const/4 v0, 0x6

    new-array v0, v0, [LX/FUv;

    sget-object v1, LX/FUv;->BEGIN_DRAG:LX/FUv;

    aput-object v1, v0, v4

    sget-object v1, LX/FUv;->END_DRAG:LX/FUv;

    aput-object v1, v0, v5

    sget-object v1, LX/FUv;->SCROLL:LX/FUv;

    aput-object v1, v0, v6

    sget-object v1, LX/FUv;->MOMENTUM_BEGIN:LX/FUv;

    aput-object v1, v0, v7

    sget-object v1, LX/FUv;->MOMENTUM_END:LX/FUv;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/FUv;->ANIMATION_END:LX/FUv;

    aput-object v2, v0, v1

    sput-object v0, LX/FUv;->$VALUES:[LX/FUv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2250244
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2250245
    iput-object p3, p0, LX/FUv;->mJSEventName:Ljava/lang/String;

    .line 2250246
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FUv;
    .locals 1

    .prologue
    .line 2250247
    const-class v0, LX/FUv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FUv;

    return-object v0
.end method

.method public static values()[LX/FUv;
    .locals 1

    .prologue
    .line 2250248
    sget-object v0, LX/FUv;->$VALUES:[LX/FUv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FUv;

    return-object v0
.end method


# virtual methods
.method public final getJSEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2250249
    iget-object v0, p0, LX/FUv;->mJSEventName:Ljava/lang/String;

    return-object v0
.end method
