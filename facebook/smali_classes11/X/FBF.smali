.class public final LX/FBF;
.super Ljava/io/InputStream;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private c:I

.field private d:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2208953
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 2208954
    iput-object p1, p0, LX/FBF;->d:Ljava/io/InputStream;

    .line 2208955
    iput-object p2, p0, LX/FBF;->a:Ljava/lang/String;

    .line 2208956
    invoke-static {p2}, LX/FBF;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/FBF;->b:I

    .line 2208957
    const/4 v0, 0x0

    iput v0, p0, LX/FBF;->c:I

    .line 2208958
    return-void
.end method

.method private static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 2208959
    const-string v0, "application/json"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "text/html"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "text/javascript"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2208960
    :cond_0
    const/high16 v0, 0x200000

    .line 2208961
    :goto_0
    return v0

    .line 2208962
    :cond_1
    const-string v0, "image/jpeg"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "image/gif"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "image/png"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "image/webp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2208963
    :cond_2
    const v0, 0x35b60

    goto :goto_0

    .line 2208964
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported content type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2208965
    iget v0, p0, LX/FBF;->c:I

    iget v1, p0, LX/FBF;->b:I

    if-lt v0, v1, :cond_0

    .line 2208966
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Content too large (length unknown): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LX/FBF;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/FBF;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2208967
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 2208968
    iget-object v0, p0, LX/FBF;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 2208969
    return-void
.end method

.method public final read()I
    .locals 2

    .prologue
    .line 2208970
    invoke-direct {p0}, LX/FBF;->a()V

    .line 2208971
    iget-object v0, p0, LX/FBF;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 2208972
    iget v1, p0, LX/FBF;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/FBF;->c:I

    .line 2208973
    return v0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 2208974
    invoke-direct {p0}, LX/FBF;->a()V

    .line 2208975
    iget v0, p0, LX/FBF;->c:I

    add-int/2addr v0, p3

    iget v1, p0, LX/FBF;->b:I

    if-le v0, v1, :cond_0

    .line 2208976
    iget v0, p0, LX/FBF;->c:I

    add-int/2addr v0, p3

    iget v1, p0, LX/FBF;->b:I

    sub-int/2addr v0, v1

    sub-int/2addr p3, v0

    .line 2208977
    :cond_0
    iget-object v0, p0, LX/FBF;->d:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 2208978
    iget v1, p0, LX/FBF;->c:I

    add-int/2addr v1, v0

    iput v1, p0, LX/FBF;->c:I

    .line 2208979
    return v0
.end method
