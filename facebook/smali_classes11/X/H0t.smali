.class public LX/H0t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2414444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2414445
    return-void
.end method

.method public static a(LX/0QB;)LX/H0t;
    .locals 3

    .prologue
    .line 2414446
    const-class v1, LX/H0t;

    monitor-enter v1

    .line 2414447
    :try_start_0
    sget-object v0, LX/H0t;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2414448
    sput-object v2, LX/H0t;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2414449
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414450
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    invoke-static {}, LX/H0t;->c()LX/H0t;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2414451
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H0t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2414452
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2414453
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static c()LX/H0t;
    .locals 1

    .prologue
    .line 2414454
    new-instance v0, LX/H0t;

    invoke-direct {v0}, LX/H0t;-><init>()V

    .line 2414455
    return-object v0
.end method
