.class public final LX/G6g;
.super Landroid/view/ViewGroup$LayoutParams;
.source ""


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0

    .prologue
    .line 2320628
    invoke-direct {p0, p3, p4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2320629
    iput p1, p0, LX/G6g;->a:I

    .line 2320630
    iput p2, p0, LX/G6g;->b:I

    .line 2320631
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2320632
    instance-of v1, p1, LX/G6g;

    if-nez v1, :cond_1

    .line 2320633
    :cond_0
    :goto_0
    return v0

    .line 2320634
    :cond_1
    check-cast p1, LX/G6g;

    .line 2320635
    iget v1, p0, LX/G6g;->a:I

    iget v2, p1, LX/G6g;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/G6g;->b:I

    iget v2, p1, LX/G6g;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
