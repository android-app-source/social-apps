.class public final LX/GNW;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 2346140
    const-class v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    const-string v5, "FetchAdPreviewFeedUnitQuery"

    const-string v6, "48cf511e08c9a0fb46efbc238d168361"

    const-string v7, "ad_preview_feed_unit"

    const-string v8, "10155261854936729"

    const/4 v9, 0x0

    const-string v0, "short_term_cache_key_pyml"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v10

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2346141
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2346142
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2346143
    sparse-switch v0, :sswitch_data_0

    .line 2346144
    :goto_0
    return-object p1

    .line 2346145
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2346146
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2346147
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2346148
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2346149
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2346150
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2346151
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2346152
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2346153
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2346154
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2346155
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2346156
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2346157
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2346158
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2346159
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2346160
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2346161
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2346162
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2346163
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 2346164
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 2346165
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 2346166
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 2346167
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    .line 2346168
    :sswitch_17
    const-string p1, "23"

    goto :goto_0

    .line 2346169
    :sswitch_18
    const-string p1, "24"

    goto :goto_0

    .line 2346170
    :sswitch_19
    const-string p1, "25"

    goto :goto_0

    .line 2346171
    :sswitch_1a
    const-string p1, "26"

    goto :goto_0

    .line 2346172
    :sswitch_1b
    const-string p1, "27"

    goto :goto_0

    .line 2346173
    :sswitch_1c
    const-string p1, "28"

    goto :goto_0

    .line 2346174
    :sswitch_1d
    const-string p1, "29"

    goto :goto_0

    .line 2346175
    :sswitch_1e
    const-string p1, "30"

    goto :goto_0

    .line 2346176
    :sswitch_1f
    const-string p1, "31"

    goto :goto_0

    .line 2346177
    :sswitch_20
    const-string p1, "32"

    goto :goto_0

    .line 2346178
    :sswitch_21
    const-string p1, "33"

    goto :goto_0

    .line 2346179
    :sswitch_22
    const-string p1, "34"

    goto :goto_0

    .line 2346180
    :sswitch_23
    const-string p1, "35"

    goto :goto_0

    .line 2346181
    :sswitch_24
    const-string p1, "36"

    goto :goto_0

    .line 2346182
    :sswitch_25
    const-string p1, "37"

    goto :goto_0

    .line 2346183
    :sswitch_26
    const-string p1, "38"

    goto :goto_0

    .line 2346184
    :sswitch_27
    const-string p1, "39"

    goto :goto_0

    .line 2346185
    :sswitch_28
    const-string p1, "40"

    goto :goto_0

    .line 2346186
    :sswitch_29
    const-string p1, "41"

    goto :goto_0

    .line 2346187
    :sswitch_2a
    const-string p1, "42"

    goto/16 :goto_0

    .line 2346188
    :sswitch_2b
    const-string p1, "43"

    goto/16 :goto_0

    .line 2346189
    :sswitch_2c
    const-string p1, "44"

    goto/16 :goto_0

    .line 2346190
    :sswitch_2d
    const-string p1, "45"

    goto/16 :goto_0

    .line 2346191
    :sswitch_2e
    const-string p1, "46"

    goto/16 :goto_0

    .line 2346192
    :sswitch_2f
    const-string p1, "47"

    goto/16 :goto_0

    .line 2346193
    :sswitch_30
    const-string p1, "48"

    goto/16 :goto_0

    .line 2346194
    :sswitch_31
    const-string p1, "49"

    goto/16 :goto_0

    .line 2346195
    :sswitch_32
    const-string p1, "50"

    goto/16 :goto_0

    .line 2346196
    :sswitch_33
    const-string p1, "51"

    goto/16 :goto_0

    .line 2346197
    :sswitch_34
    const-string p1, "52"

    goto/16 :goto_0

    .line 2346198
    :sswitch_35
    const-string p1, "53"

    goto/16 :goto_0

    .line 2346199
    :sswitch_36
    const-string p1, "54"

    goto/16 :goto_0

    .line 2346200
    :sswitch_37
    const-string p1, "55"

    goto/16 :goto_0

    .line 2346201
    :sswitch_38
    const-string p1, "56"

    goto/16 :goto_0

    .line 2346202
    :sswitch_39
    const-string p1, "57"

    goto/16 :goto_0

    .line 2346203
    :sswitch_3a
    const-string p1, "58"

    goto/16 :goto_0

    .line 2346204
    :sswitch_3b
    const-string p1, "59"

    goto/16 :goto_0

    .line 2346205
    :sswitch_3c
    const-string p1, "60"

    goto/16 :goto_0

    .line 2346206
    :sswitch_3d
    const-string p1, "61"

    goto/16 :goto_0

    .line 2346207
    :sswitch_3e
    const-string p1, "62"

    goto/16 :goto_0

    .line 2346208
    :sswitch_3f
    const-string p1, "63"

    goto/16 :goto_0

    .line 2346209
    :sswitch_40
    const-string p1, "64"

    goto/16 :goto_0

    .line 2346210
    :sswitch_41
    const-string p1, "65"

    goto/16 :goto_0

    .line 2346211
    :sswitch_42
    const-string p1, "66"

    goto/16 :goto_0

    .line 2346212
    :sswitch_43
    const-string p1, "67"

    goto/16 :goto_0

    .line 2346213
    :sswitch_44
    const-string p1, "68"

    goto/16 :goto_0

    .line 2346214
    :sswitch_45
    const-string p1, "69"

    goto/16 :goto_0

    .line 2346215
    :sswitch_46
    const-string p1, "70"

    goto/16 :goto_0

    .line 2346216
    :sswitch_47
    const-string p1, "71"

    goto/16 :goto_0

    .line 2346217
    :sswitch_48
    const-string p1, "72"

    goto/16 :goto_0

    .line 2346218
    :sswitch_49
    const-string p1, "73"

    goto/16 :goto_0

    .line 2346219
    :sswitch_4a
    const-string p1, "74"

    goto/16 :goto_0

    .line 2346220
    :sswitch_4b
    const-string p1, "75"

    goto/16 :goto_0

    .line 2346221
    :sswitch_4c
    const-string p1, "76"

    goto/16 :goto_0

    .line 2346222
    :sswitch_4d
    const-string p1, "77"

    goto/16 :goto_0

    .line 2346223
    :sswitch_4e
    const-string p1, "78"

    goto/16 :goto_0

    .line 2346224
    :sswitch_4f
    const-string p1, "79"

    goto/16 :goto_0

    .line 2346225
    :sswitch_50
    const-string p1, "80"

    goto/16 :goto_0

    .line 2346226
    :sswitch_51
    const-string p1, "81"

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7f566515 -> :sswitch_2f
        -0x7e998586 -> :sswitch_f
        -0x7b752021 -> :sswitch_3
        -0x7531a756 -> :sswitch_23
        -0x6e3ba572 -> :sswitch_14
        -0x6a24640d -> :sswitch_48
        -0x6a02a4f4 -> :sswitch_32
        -0x69f19a9a -> :sswitch_1
        -0x680de62a -> :sswitch_29
        -0x6326fdb3 -> :sswitch_26
        -0x626f1062 -> :sswitch_11
        -0x5e743804 -> :sswitch_c
        -0x57984ae8 -> :sswitch_3e
        -0x5709d77d -> :sswitch_4d
        -0x55ff6f9b -> :sswitch_2
        -0x5349037c -> :sswitch_30
        -0x51484e72 -> :sswitch_1d
        -0x513764de -> :sswitch_49
        -0x50cab1c8 -> :sswitch_8
        -0x4f1f32b4 -> :sswitch_3f
        -0x4eea3afb -> :sswitch_b
        -0x4aeac1fa -> :sswitch_40
        -0x4ae70342 -> :sswitch_9
        -0x48fcb87a -> :sswitch_51
        -0x4496acc9 -> :sswitch_2a
        -0x41a91745 -> :sswitch_3c
        -0x41143822 -> :sswitch_1c
        -0x3c54de38 -> :sswitch_2e
        -0x3b85b241 -> :sswitch_4c
        -0x39e54905 -> :sswitch_39
        -0x30b65c8f -> :sswitch_20
        -0x2fab0379 -> :sswitch_4b
        -0x2f1c601a -> :sswitch_24
        -0x25a646c8 -> :sswitch_1f
        -0x2511c384 -> :sswitch_2d
        -0x24e1906f -> :sswitch_4
        -0x2177e47b -> :sswitch_21
        -0x201d08e7 -> :sswitch_37
        -0x1d6ce0bf -> :sswitch_35
        -0x1b87b280 -> :sswitch_25
        -0x17e48248 -> :sswitch_5
        -0x15db59af -> :sswitch_50
        -0x14283bca -> :sswitch_36
        -0x12efdeb3 -> :sswitch_2b
        -0x8ca6426 -> :sswitch_7
        -0x6fe61e8 -> :sswitch_18
        -0x587d3fa -> :sswitch_27
        -0x3e446ed -> :sswitch_22
        -0x27b670e -> :sswitch_3a
        -0x12603b3 -> :sswitch_46
        0x180aba4 -> :sswitch_1b
        0xa1fa812 -> :sswitch_13
        0xc168ff8 -> :sswitch_a
        0xe50e2a0 -> :sswitch_19
        0x11850e88 -> :sswitch_42
        0x15888c51 -> :sswitch_12
        0x18ce3dbb -> :sswitch_e
        0x214100e0 -> :sswitch_2c
        0x2292beef -> :sswitch_47
        0x244e76e6 -> :sswitch_45
        0x26d0c0ff -> :sswitch_41
        0x27208b4a -> :sswitch_43
        0x291d8de0 -> :sswitch_3d
        0x2f8b060e -> :sswitch_4f
        0x3052e0ff -> :sswitch_16
        0x326dc744 -> :sswitch_3b
        0x34e16755 -> :sswitch_0
        0x410878b1 -> :sswitch_38
        0x420eb51c -> :sswitch_15
        0x43ee5105 -> :sswitch_4e
        0x44431ea4 -> :sswitch_10
        0x54ace343 -> :sswitch_44
        0x54df6484 -> :sswitch_d
        0x5aa53d79 -> :sswitch_34
        0x5e7957c4 -> :sswitch_1e
        0x5f424068 -> :sswitch_1a
        0x63c03b07 -> :sswitch_28
        0x670b906a -> :sswitch_17
        0x6771e9f5 -> :sswitch_31
        0x73a026b5 -> :sswitch_33
        0x7506f93c -> :sswitch_4a
        0x7c6b80b3 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 2346227
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 2346228
    :goto_1
    return v0

    .line 2346229
    :sswitch_0
    const-string v5, "3"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v0

    goto :goto_0

    :sswitch_1
    const-string v5, "22"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v2

    goto :goto_0

    :sswitch_2
    const-string v5, "30"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v3

    goto :goto_0

    :sswitch_3
    const-string v5, "15"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :sswitch_4
    const-string v5, "32"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v1, v4

    goto :goto_0

    :sswitch_5
    const-string v5, "14"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x5

    goto :goto_0

    :sswitch_6
    const-string v5, "10"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x6

    goto :goto_0

    :sswitch_7
    const-string v5, "1"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x7

    goto :goto_0

    :sswitch_8
    const-string v5, "2"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x8

    goto :goto_0

    :sswitch_9
    const-string v5, "45"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x9

    goto :goto_0

    :sswitch_a
    const-string v5, "59"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xa

    goto :goto_0

    :sswitch_b
    const-string v5, "47"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xb

    goto :goto_0

    :sswitch_c
    const-string v5, "0"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v5, "49"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v5, "13"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v5, "11"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v5, "5"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v5, "12"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v5, "4"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v5, "81"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v5, "73"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v5, "80"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v5, "9"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v5, "8"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v5, "7"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v5, "6"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v5, "75"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v5, "76"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v5, "62"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v1, 0x1c

    goto/16 :goto_0

    .line 2346230
    :pswitch_0
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346231
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346232
    :pswitch_2
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346233
    :pswitch_3
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346234
    :pswitch_4
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346235
    :pswitch_5
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346236
    :pswitch_6
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346237
    :pswitch_7
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346238
    :pswitch_8
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346239
    :pswitch_9
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346240
    :pswitch_a
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346241
    :pswitch_b
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346242
    :pswitch_c
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346243
    :pswitch_d
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346244
    :pswitch_e
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346245
    :pswitch_f
    const-string v0, "FUSE_BIG"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346246
    :pswitch_10
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346247
    :pswitch_11
    const-string v0, "%s"

    invoke-static {p2, v4, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346248
    :pswitch_12
    const-string v0, "%s"

    invoke-static {p2, v3, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346249
    :pswitch_13
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346250
    :pswitch_14
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346251
    :pswitch_15
    const-string v0, "mobile"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346252
    :pswitch_16
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346253
    :pswitch_17
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346254
    :pswitch_18
    const/16 v0, 0x78

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346255
    :pswitch_19
    const/16 v0, 0x5a

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346256
    :pswitch_1a
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346257
    :pswitch_1b
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    .line 2346258
    :pswitch_1c
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_c
        0x31 -> :sswitch_7
        0x32 -> :sswitch_8
        0x33 -> :sswitch_0
        0x34 -> :sswitch_12
        0x35 -> :sswitch_10
        0x36 -> :sswitch_19
        0x37 -> :sswitch_18
        0x38 -> :sswitch_17
        0x39 -> :sswitch_16
        0x61f -> :sswitch_6
        0x620 -> :sswitch_f
        0x621 -> :sswitch_11
        0x622 -> :sswitch_e
        0x623 -> :sswitch_5
        0x624 -> :sswitch_3
        0x640 -> :sswitch_1
        0x65d -> :sswitch_2
        0x65f -> :sswitch_4
        0x681 -> :sswitch_9
        0x683 -> :sswitch_b
        0x685 -> :sswitch_d
        0x6a4 -> :sswitch_a
        0x6bc -> :sswitch_1c
        0x6dc -> :sswitch_14
        0x6de -> :sswitch_1a
        0x6df -> :sswitch_1b
        0x6f8 -> :sswitch_15
        0x6f9 -> :sswitch_13
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method
