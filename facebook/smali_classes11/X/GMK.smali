.class public LX/GMK;
.super LX/GJI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/adinterfaces/ui/BudgetOptionsView;",
        ">",
        "LX/GJI",
        "<TT;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private g:LX/GG6;

.field private h:LX/2U3;


# direct methods
.method public constructor <init>(LX/GG6;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/2U3;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V
    .locals 12
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2344364
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p6

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v1 .. v11}, LX/GJI;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;LX/2U3;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2344365
    iput-object p1, p0, LX/GMK;->g:LX/GG6;

    .line 2344366
    move-object/from16 v0, p6

    iput-object v0, p0, LX/GMK;->h:LX/2U3;

    .line 2344367
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;)I
    .locals 2

    .prologue
    .line 2344359
    sget-object v0, LX/GMJ;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2344360
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2344361
    :pswitch_0
    const v0, 0x7f080a55

    goto :goto_0

    .line 2344362
    :pswitch_1
    const v0, 0x7f080a53

    goto :goto_0

    .line 2344363
    :pswitch_2
    const v0, 0x7f080a57

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/GMK;
    .locals 1

    .prologue
    .line 2344358
    invoke-static {p0}, LX/GMK;->b(LX/0QB;)LX/GMK;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;)I
    .locals 2

    .prologue
    .line 2344353
    sget-object v0, LX/GMJ;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2344354
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2344355
    :pswitch_0
    const v0, 0x7f080a56

    goto :goto_0

    .line 2344356
    :pswitch_1
    const v0, 0x7f080a54

    goto :goto_0

    .line 2344357
    :pswitch_2
    const v0, 0x7f080a58

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/GMK;
    .locals 12

    .prologue
    .line 2344351
    new-instance v0, LX/GMK;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v1

    check-cast v1, LX/GG6;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {p0}, LX/GMU;->a(LX/0QB;)LX/GMU;

    move-result-object v3

    check-cast v3, LX/GMU;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/GE2;->b(LX/0QB;)LX/GE2;

    move-result-object v5

    check-cast v5, LX/GE2;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v6

    check-cast v6, LX/2U3;

    const-class v7, LX/GLN;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/GLN;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v0 .. v11}, LX/GMK;-><init>(LX/GG6;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/2U3;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2344352
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2344319
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    .line 2344320
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v3

    .line 2344321
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v1

    .line 2344322
    const-string v0, ""

    .line 2344323
    if-eqz v3, :cond_2

    .line 2344324
    iget-object v4, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v4, v4

    .line 2344325
    invoke-interface {v4}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v4

    sget-object v5, LX/8wL;->PROMOTE_CTA:LX/8wL;

    if-eq v4, v5, :cond_0

    .line 2344326
    iget-object v4, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v4, v4

    .line 2344327
    invoke-interface {v4}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v4

    sget-object v5, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-eq v4, v5, :cond_0

    .line 2344328
    iget-object v4, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v4, v4

    .line 2344329
    invoke-interface {v4}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v4

    sget-object v5, LX/8wL;->PAGE_LIKE:LX/8wL;

    if-ne v4, v5, :cond_2

    .line 2344330
    :cond_0
    iget-object v4, p0, LX/GHg;->b:LX/GCE;

    move-object v4, v4

    .line 2344331
    iget-object v5, v4, LX/GCE;->e:LX/0ad;

    move-object v4, v5

    .line 2344332
    sget-short v5, LX/GDK;->s:S

    invoke-interface {v4, v5, v9}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 2344333
    iget-object v5, p0, LX/GHg;->b:LX/GCE;

    move-object v5, v5

    .line 2344334
    iget-object v6, v5, LX/GCE;->e:LX/0ad;

    move-object v5, v6

    .line 2344335
    sget-short v6, LX/GDK;->t:S

    invoke-interface {v5, v6, v9}, LX/0ad;->a(SZ)Z

    move-result v5

    .line 2344336
    if-nez v4, :cond_1

    if-eqz v5, :cond_1

    .line 2344337
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    .line 2344338
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v1

    .line 2344339
    :cond_1
    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    .line 2344340
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v4

    invoke-static {v4}, LX/GMK;->a(Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;)I

    move-result v4

    .line 2344341
    if-eqz v4, :cond_2

    .line 2344342
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "<br>"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/GJI;->b:Landroid/content/res/Resources;

    new-array v6, v11, [Ljava/lang/Object;

    iget-object v7, p0, LX/GMK;->g:LX/GG6;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v8

    invoke-virtual {v7, v8}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, LX/GMK;->g:LX/GG6;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v3

    invoke-virtual {v7, v3}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v10

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2344343
    :cond_2
    const-string v3, ""

    .line 2344344
    invoke-static {v1}, LX/GMK;->a(Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;)I

    move-result v1

    .line 2344345
    if-eqz v1, :cond_3

    .line 2344346
    iget-object v3, p0, LX/GJI;->b:Landroid/content/res/Resources;

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v5, p0, LX/GMK;->g:LX/GG6;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v6

    invoke-virtual {v5, v6}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    iget-object v5, p0, LX/GMK;->g:LX/GG6;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v2

    invoke-virtual {v5, v2}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v10

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2344347
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0

    .line 2344348
    :cond_3
    iget-object v1, p0, LX/GMK;->h:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to resolve reach string for objective "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2344349
    iget-object v5, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v5, v5

    .line 2344350
    invoke-interface {v5}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v5

    invoke-virtual {v5}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v1, v3

    goto :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2344316
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    invoke-static {p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 2344317
    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v1, v1

    .line 2344318
    invoke-static {v1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/GGB;)V
    .locals 2

    .prologue
    .line 2344310
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344311
    invoke-static {v0}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080ad6

    .line 2344312
    :goto_0
    iget-object v1, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    move-object v1, v1

    .line 2344313
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2344314
    return-void

    .line 2344315
    :cond_0
    const v0, 0x7f080ac1

    goto :goto_0
.end method

.method public final bridge synthetic a(LX/GJD;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2344309
    check-cast p1, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GJI;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2344221
    check-cast p1, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GJI;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2344306
    invoke-super {p0, p1, p2}, LX/GJI;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2344307
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080a4a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f021962

    invoke-virtual {p2, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/String;I)V

    .line 2344308
    return-void
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2344281
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    .line 2344282
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v3

    .line 2344283
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v1

    .line 2344284
    const-string v0, ""

    .line 2344285
    if-eqz v3, :cond_1

    .line 2344286
    iget-object v4, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v4, v4

    .line 2344287
    invoke-interface {v4}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v4

    sget-object v5, LX/8wL;->PROMOTE_CTA:LX/8wL;

    if-ne v4, v5, :cond_1

    .line 2344288
    iget-object v4, p0, LX/GHg;->b:LX/GCE;

    move-object v4, v4

    .line 2344289
    iget-object v5, v4, LX/GCE;->e:LX/0ad;

    move-object v4, v5

    .line 2344290
    sget-short v5, LX/GDK;->s:S

    invoke-interface {v4, v5, v9}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 2344291
    iget-object v5, p0, LX/GHg;->b:LX/GCE;

    move-object v5, v5

    .line 2344292
    iget-object v6, v5, LX/GCE;->e:LX/0ad;

    move-object v5, v6

    .line 2344293
    sget-short v6, LX/GDK;->t:S

    invoke-interface {v5, v6, v9}, LX/0ad;->a(SZ)Z

    move-result v5

    .line 2344294
    if-nez v4, :cond_0

    if-eqz v5, :cond_0

    .line 2344295
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    .line 2344296
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v1

    .line 2344297
    :cond_0
    if-eqz v4, :cond_1

    if-eqz v5, :cond_1

    .line 2344298
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->l()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v4

    invoke-static {v4}, LX/GMK;->b(Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;)I

    move-result v4

    .line 2344299
    if-eqz v4, :cond_1

    .line 2344300
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/GJI;->b:Landroid/content/res/Resources;

    new-array v6, v11, [Ljava/lang/Object;

    iget-object v7, p0, LX/GMK;->g:LX/GG6;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v8

    invoke-virtual {v7, v8}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    iget-object v7, p0, LX/GMK;->g:LX/GG6;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v3

    invoke-virtual {v7, v3}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v10

    invoke-virtual {v5, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2344301
    :cond_1
    const-string v3, ""

    .line 2344302
    invoke-static {v1}, LX/GMK;->b(Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;)I

    move-result v1

    .line 2344303
    if-eqz v1, :cond_2

    .line 2344304
    iget-object v3, p0, LX/GJI;->b:Landroid/content/res/Resources;

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v5, p0, LX/GMK;->g:LX/GG6;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v6

    invoke-virtual {v5, v6}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    iget-object v5, p0, LX/GMK;->g:LX/GG6;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v2

    invoke-virtual {v5, v2}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v10

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2344305
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v1, v3

    goto :goto_0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 2344276
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2344277
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    invoke-static {p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    invoke-static {}, LX/GMU;->a()Ljava/text/NumberFormat;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2344278
    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v1, v1

    .line 2344279
    invoke-static {v1}, LX/GMU;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2344280
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 0

    .prologue
    .line 2344275
    return-object p1
.end method

.method public final g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2344270
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344271
    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2344272
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 2344273
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344274
    invoke-static {v0}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()LX/GMT;
    .locals 6

    .prologue
    .line 2344247
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344248
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2344249
    sget-object v0, LX/GMT;->NONE:LX/GMT;

    .line 2344250
    :goto_0
    return-object v0

    .line 2344251
    :cond_0
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344252
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 2344253
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2344254
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344255
    invoke-static {v0}, LX/GMU;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2344256
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344257
    invoke-static {v0}, LX/GMU;->d(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2344258
    iget-object v3, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v3, v3

    .line 2344259
    invoke-static {v3}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2344260
    iget-object v3, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v3, v3

    .line 2344261
    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v4, v5}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v3

    .line 2344262
    invoke-virtual {v0, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2344263
    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2344264
    :cond_1
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2344265
    invoke-virtual {v2, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-lez v1, :cond_2

    .line 2344266
    sget-object v0, LX/GMT;->MAX:LX/GMT;

    goto :goto_0

    .line 2344267
    :cond_2
    invoke-virtual {v2, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_3

    .line 2344268
    sget-object v0, LX/GMT;->MIN:LX/GMT;

    goto :goto_0

    .line 2344269
    :cond_3
    sget-object v0, LX/GMT;->NONE:LX/GMT;

    goto :goto_0
.end method

.method public final i()Landroid/text/Spanned;
    .locals 5

    .prologue
    .line 2344233
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344234
    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    .line 2344235
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2344236
    :cond_0
    const/4 v0, 0x0

    .line 2344237
    :goto_0
    return-object v0

    .line 2344238
    :cond_1
    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2344239
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v2, v2

    .line 2344240
    invoke-static {v2}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2344241
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v2, v2

    .line 2344242
    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2344243
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    .line 2344244
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344245
    invoke-static {v0}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 2344246
    iget-object v1, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080a61

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public final o()Landroid/text/Spanned;
    .locals 8

    .prologue
    .line 2344222
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344223
    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->u()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2344224
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2344225
    :cond_0
    const/4 v0, 0x0

    .line 2344226
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v1, 0x7f080a5f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2344227
    iget-object v4, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v4, v4

    .line 2344228
    invoke-static {v4}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->u()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v4

    .line 2344229
    iget-object v5, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v5, v5

    .line 2344230
    invoke-static {v5}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->u()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v5

    invoke-static {v5}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v6

    .line 2344231
    iget-object v5, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v5, v5

    .line 2344232
    invoke-static {v5}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v5

    invoke-static {v4, v6, v7, v5}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method
