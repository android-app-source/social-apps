.class public final LX/FHq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/io/File;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:LX/FHg;

.field public final h:Ljava/lang/String;

.field public final i:LX/7yr;

.field public final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;ILX/FHg;Ljava/lang/String;LX/7yr;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2221114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221115
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Object size is invalid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2221116
    iput-object p1, p0, LX/FHq;->a:Ljava/io/File;

    .line 2221117
    iput-object p2, p0, LX/FHq;->b:Ljava/lang/String;

    .line 2221118
    iput-object p3, p0, LX/FHq;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2221119
    iput-object p4, p0, LX/FHq;->d:Ljava/lang/String;

    .line 2221120
    iput-object p5, p0, LX/FHq;->e:Ljava/lang/String;

    .line 2221121
    iput p6, p0, LX/FHq;->f:I

    .line 2221122
    iput-object p7, p0, LX/FHq;->g:LX/FHg;

    .line 2221123
    iput-object p9, p0, LX/FHq;->i:LX/7yr;

    .line 2221124
    iput-object p8, p0, LX/FHq;->h:Ljava/lang/String;

    .line 2221125
    iput-object p10, p0, LX/FHq;->j:Ljava/lang/String;

    .line 2221126
    return-void

    .line 2221127
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
