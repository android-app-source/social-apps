.class public LX/Fq7;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/BP8;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2cj;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Px;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/2cm;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BPq;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/G4x;

.field public final l:Lcom/facebook/timeline/TimelineFragment;

.field public final m:LX/5SB;

.field public final n:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/FwT;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/FrV;

.field public p:LX/BP7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2292495
    const-class v0, LX/Fq7;

    sput-object v0, LX/Fq7;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/timeline/TimelineFragment;LX/BP0;LX/0QR;LX/FrV;LX/0Or;LX/BP8;LX/0Or;LX/0Ot;LX/0Ot;LX/0Or;LX/2cm;LX/0Or;LX/0Or;LX/G4x;)V
    .locals 0
    .param p1    # Lcom/facebook/timeline/TimelineFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0QR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/FrV;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/0Or;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/TimelineFragment;",
            "LX/BP0;",
            "LX/0QR",
            "<",
            "LX/FwT;",
            ">;",
            "LX/FrV;",
            "LX/0Or",
            "<",
            "LX/1mR;",
            ">;",
            "LX/BP8;",
            "LX/0Or",
            "<",
            "LX/2cj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8Px;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2iz;",
            ">;",
            "LX/2cm;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BPq;",
            ">;",
            "LX/G4x;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2292479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2292480
    iput-object p1, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    .line 2292481
    iput-object p2, p0, LX/Fq7;->m:LX/5SB;

    .line 2292482
    iput-object p3, p0, LX/Fq7;->n:LX/0QR;

    .line 2292483
    iput-object p4, p0, LX/Fq7;->o:LX/FrV;

    .line 2292484
    iput-object p5, p0, LX/Fq7;->b:LX/0Or;

    .line 2292485
    iput-object p6, p0, LX/Fq7;->c:LX/BP8;

    .line 2292486
    iput-object p7, p0, LX/Fq7;->d:LX/0Or;

    .line 2292487
    iput-object p8, p0, LX/Fq7;->e:LX/0Ot;

    .line 2292488
    iput-object p9, p0, LX/Fq7;->f:LX/0Ot;

    .line 2292489
    iput-object p10, p0, LX/Fq7;->g:LX/0Or;

    .line 2292490
    iput-object p11, p0, LX/Fq7;->h:LX/2cm;

    .line 2292491
    iput-object p12, p0, LX/Fq7;->i:LX/0Or;

    .line 2292492
    iput-object p13, p0, LX/Fq7;->j:LX/0Or;

    .line 2292493
    iput-object p14, p0, LX/Fq7;->k:LX/G4x;

    .line 2292494
    return-void
.end method

.method public static a(LX/Fq7;Lcom/google/common/util/concurrent/ListenableFuture;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2292496
    new-instance v1, LX/Fq5;

    invoke-direct {v1, p0, p2}, LX/Fq5;-><init>(LX/Fq7;Z)V

    iget-object v0, p0, LX/Fq7;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {p1, v1, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2292497
    return-void
.end method

.method private static d(LX/Fq7;Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2292375
    iget-object v0, p0, LX/Fq7;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "CoverImageRequest"

    aput-object v2, v1, v3

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2292376
    iget-object v0, p0, LX/Fq7;->m:LX/5SB;

    if-eqz v0, :cond_1

    .line 2292377
    iget-object v4, p0, LX/Fq7;->m:LX/5SB;

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2292378
    iget-object v4, p0, LX/Fq7;->p:LX/BP7;

    if-nez v4, :cond_0

    .line 2292379
    iget-object v4, p0, LX/Fq7;->c:LX/BP8;

    iget-object v5, p0, LX/Fq7;->m:LX/5SB;

    .line 2292380
    iget-wide v8, v5, LX/5SB;->b:J

    move-wide v6, v8

    .line 2292381
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/BP8;->a(Ljava/lang/Long;)LX/BP7;

    move-result-object v4

    iput-object v4, p0, LX/Fq7;->p:LX/BP7;

    .line 2292382
    :cond_0
    iget-object v4, p0, LX/Fq7;->p:LX/BP7;

    move-object v0, v4

    .line 2292383
    iget-object v1, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0, v1, p1, v3}, LX/BP7;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;Z)V

    .line 2292384
    :cond_1
    return-void

    .line 2292385
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    .line 2292386
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2292387
    :goto_0
    return-void

    .line 2292388
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 2292389
    sget-object v0, LX/Fq7;->a:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected request code received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2292390
    :sswitch_0
    const/4 p1, 0x0

    .line 2292391
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->K()V

    .line 2292392
    const-string v0, "is_uploading_media"

    invoke-virtual {p3, v0, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2292393
    const-string v0, "extra_composer_has_published"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2292394
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->C()Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2292395
    :cond_1
    :goto_1
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2292396
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 2292397
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2292398
    :cond_2
    :goto_2
    goto :goto_0

    .line 2292399
    :sswitch_1
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    .line 2292400
    iget-object v1, v0, Lcom/facebook/timeline/TimelineFragment;->bL:LX/8Kc;

    if-eqz v1, :cond_10

    .line 2292401
    :goto_3
    const-string v0, "is_uploading_media"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2292402
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2292403
    iget-object v0, p0, LX/Fq7;->k:LX/G4x;

    sget-object v1, LX/G5A;->LOADING:LX/G5A;

    invoke-virtual {v0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2292404
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292405
    :cond_3
    iget-object v0, p0, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    invoke-virtual {v0}, LX/Frd;->f()V

    .line 2292406
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    iget-object v1, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    .line 2292407
    iget-object v2, v1, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    move-object v1, v2

    .line 2292408
    invoke-virtual {v1}, LX/G0n;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/TimelineFragment;->d(I)V

    .line 2292409
    :goto_4
    goto/16 :goto_0

    .line 2292410
    :sswitch_2
    invoke-static {p0, p3}, LX/Fq7;->d(LX/Fq7;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 2292411
    :sswitch_3
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->R()V

    goto/16 :goto_0

    .line 2292412
    :sswitch_4
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 2292413
    iget-object v0, p0, LX/Fq7;->n:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    invoke-virtual {v0, p3}, LX/FwT;->a(Landroid/content/Intent;)V

    .line 2292414
    :goto_5
    goto/16 :goto_0

    .line 2292415
    :sswitch_5
    const/4 p1, 0x0

    .line 2292416
    iget-object v0, p0, LX/Fq7;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "ProfileImageRequest"

    aput-object v2, v1, p1

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2292417
    const-string v0, "camera_roll"

    invoke-virtual {p3, v0, p1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2292418
    const-string v0, "profile_photo_method_extra"

    const-string v1, "existing"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2292419
    :cond_4
    const-string v0, "staging_ground_photo_caption"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2292420
    const-string v0, "extra_profile_pic_caption"

    const-string v1, "staging_ground_photo_caption"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2292421
    const-string v0, "staging_ground_photo_caption"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 2292422
    :cond_5
    iget-object v0, p0, LX/Fq7;->n:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FwT;->a(Landroid/os/Bundle;)V

    .line 2292423
    goto/16 :goto_0

    .line 2292424
    :sswitch_6
    const-string v0, "privacy_option_to_upsell"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2292425
    if-eqz v0, :cond_6

    .line 2292426
    iget-object v1, p0, LX/Fq7;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Px;

    iget-object v2, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v2, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    .line 2292427
    iget-object p2, v2, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, p2

    .line 2292428
    check-cast v2, Landroid/view/ViewGroup;

    sget-object p2, LX/1K2;->TIMELINE:LX/1K2;

    invoke-virtual {v1, p1, v2, v0, p2}, LX/8Px;->a(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/1K2;)V

    .line 2292429
    :cond_6
    goto/16 :goto_0

    .line 2292430
    :sswitch_7
    iget-object v0, p0, LX/Fq7;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cj;

    invoke-virtual {v0, p1, p2, p3}, LX/2cj;->a(IILandroid/content/Intent;)V

    .line 2292431
    goto/16 :goto_0

    .line 2292432
    :sswitch_8
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->lk_()V

    goto/16 :goto_0

    .line 2292433
    :sswitch_9
    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 2292434
    iget-object v0, p0, LX/Fq7;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    new-array v1, p2, [Ljava/lang/String;

    const-string v2, "ProfileImageRequest"

    aput-object v2, v1, p1

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2292435
    iget-object v0, p0, LX/Fq7;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    new-array v1, p2, [Ljava/lang/String;

    const-string v2, "CoverImageRequest"

    aput-object v2, v1, p1

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2292436
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->mJ_()V

    .line 2292437
    goto/16 :goto_0

    .line 2292438
    :sswitch_a
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->mJ_()V

    goto/16 :goto_0

    .line 2292439
    :sswitch_b
    iget-object v0, p0, LX/Fq7;->h:LX/2cm;

    invoke-virtual {v0, p3}, LX/2cm;->a(Landroid/content/Intent;)V

    .line 2292440
    goto/16 :goto_0

    .line 2292441
    :sswitch_c
    iget-object v3, p0, LX/Fq7;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BNP;

    const-string v6, "story_view"

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v7

    iget-object v4, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v8

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v3 .. v8}, LX/BNP;->a(ILandroid/content/Intent;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 2292442
    goto/16 :goto_0

    .line 2292443
    :cond_7
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2292444
    iget-object v0, p0, LX/Fq7;->k:LX/G4x;

    sget-object v1, LX/G5A;->LOADING:LX/G5A;

    invoke-virtual {v0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2292445
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292446
    :cond_8
    iget-object v0, p0, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    invoke-virtual {v0}, LX/Frd;->f()V

    .line 2292447
    const-string v0, "extra_composer_has_published"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2292448
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->C()Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2292449
    :goto_6
    invoke-static {p0, v0, p1}, LX/Fq7;->a(LX/Fq7;Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    goto/16 :goto_1

    .line 2292450
    :cond_9
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2292451
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_6

    .line 2292452
    :cond_a
    iget-object v1, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/TimelineFragment;->o()LX/G4x;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getLegacyStoryApiId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams;->getComposerSessionId()Ljava/lang/String;

    move-result-object v0

    sget-object p2, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v2, v1, p1, v0, p2}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;)V

    goto/16 :goto_2

    .line 2292453
    :cond_b
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2292454
    iget-object v1, p0, LX/Fq7;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iz;

    iget-object v2, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/2iz;->a(Ljava/lang/String;)V

    .line 2292455
    iget-object v1, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v1}, Lcom/facebook/timeline/TimelineFragment;->L()V

    .line 2292456
    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerType:LX/2rt;

    sget-object v1, LX/2rt;->SHARE:LX/2rt;

    if-eq v0, v1, :cond_2

    .line 2292457
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    iget-object v1, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    .line 2292458
    iget-object v2, v1, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    move-object v1, v2

    .line 2292459
    invoke-virtual {v1}, LX/G0n;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/TimelineFragment;->d(I)V

    goto/16 :goto_2

    .line 2292460
    :cond_c
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2292461
    iget-object v0, p0, LX/Fq7;->k:LX/G4x;

    sget-object v1, LX/G5A;->LOADING:LX/G5A;

    invoke-virtual {v0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2292462
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292463
    :cond_d
    iget-object v0, p0, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    invoke-virtual {v0}, LX/Frd;->f()V

    .line 2292464
    const-string v0, "extra_composer_has_published"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 2292465
    iget-object v0, p0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->C()Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->d(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v1, v0

    .line 2292466
    :goto_7
    const-string v0, "publishLifeEventParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;

    .line 2292467
    iget-object v2, v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->startDate:Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 2292468
    new-instance p3, LX/Fq6;

    invoke-direct {p3, p0}, LX/Fq6;-><init>(LX/Fq7;)V

    iget-object v2, p0, LX/Fq7;->i:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v1, p3, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2292469
    :goto_8
    iget-object v1, p0, LX/Fq7;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iz;

    iget-object v0, v0, Lcom/facebook/composer/lifeevent/protocol/ComposerLifeEventParam;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/2iz;->a(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2292470
    :cond_e
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2292471
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v1, v0

    goto :goto_7

    .line 2292472
    :cond_f
    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, LX/Fq7;->a(LX/Fq7;Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    goto :goto_8

    .line 2292473
    :cond_10
    new-instance v1, LX/1B1;

    invoke-direct {v1}, LX/1B1;-><init>()V

    iput-object v1, v0, Lcom/facebook/timeline/TimelineFragment;->aY:LX/1B1;

    .line 2292474
    new-instance v1, LX/FqG;

    invoke-direct {v1, v0}, LX/FqG;-><init>(Lcom/facebook/timeline/TimelineFragment;)V

    iput-object v1, v0, Lcom/facebook/timeline/TimelineFragment;->bL:LX/8Kc;

    .line 2292475
    iget-object v1, v0, Lcom/facebook/timeline/TimelineFragment;->aY:LX/1B1;

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->bL:LX/8Kc;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2292476
    iget-object v1, v0, Lcom/facebook/timeline/TimelineFragment;->aY:LX/1B1;

    iget-object v2, v0, Lcom/facebook/timeline/TimelineFragment;->C:LX/0b3;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    goto/16 :goto_3

    .line 2292477
    :cond_11
    iget-object v0, p0, LX/Fq7;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string p1, "ProfileImageRequest"

    aput-object p1, v1, v2

    invoke-static {v1}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2292478
    iget-object v0, p0, LX/Fq7;->n:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwT;

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FwT;->a(Landroid/os/Bundle;)V

    goto/16 :goto_5

    :sswitch_data_0
    .sparse-switch
        0x82 -> :sswitch_a
        0x3ec -> :sswitch_7
        0x6dc -> :sswitch_0
        0x6de -> :sswitch_0
        0x6df -> :sswitch_c
        0x6e0 -> :sswitch_1
        0x71d -> :sswitch_8
        0x71e -> :sswitch_8
        0x740 -> :sswitch_6
        0xc34 -> :sswitch_2
        0xc35 -> :sswitch_4
        0xc36 -> :sswitch_4
        0xc37 -> :sswitch_3
        0x138a -> :sswitch_b
        0x26bb -> :sswitch_5
        0x26bc -> :sswitch_3
        0x56e3 -> :sswitch_9
    .end sparse-switch
.end method
