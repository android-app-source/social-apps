.class public final LX/GDq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;",
        ">;",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GDs;


# direct methods
.method public constructor <init>(LX/GDs;)V
    .locals 0

    .prologue
    .line 2330585
    iput-object p1, p0, LX/GDq;->a:LX/GDs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2330586
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2330587
    if-eqz p1, :cond_0

    .line 2330588
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330589
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2330590
    const/4 v0, 0x0

    .line 2330591
    :goto_1
    return-object v0

    .line 2330592
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330593
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2330594
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2330595
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330596
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAccountsQueryModels$FetchAccountsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2330597
    const/4 v0, 0x0

    return v0
.end method
