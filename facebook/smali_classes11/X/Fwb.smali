.class public LX/Fwb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/FwY;

.field private final b:LX/0ad;

.field public final c:LX/Fsr;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/8p8;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FwD;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fw7;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/BQ1;

.field public final j:LX/5SB;

.field private k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$ExternalLink;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/0Or;LX/0ad;LX/Fsr;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/5SB;LX/BQ1;)V
    .locals 5
    .param p9    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/FwY;",
            ">;",
            "LX/0ad;",
            "LX/Fsr;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;",
            "LX/0Or",
            "<",
            "LX/8p8;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FwD;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fw7;",
            ">;",
            "LX/5SB;",
            "LX/BQ1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303348
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwY;

    iput-object v0, p0, LX/Fwb;->a:LX/FwY;

    .line 2303349
    iput-object p2, p0, LX/Fwb;->b:LX/0ad;

    .line 2303350
    iput-object p3, p0, LX/Fwb;->c:LX/Fsr;

    .line 2303351
    iput-object p4, p0, LX/Fwb;->d:LX/0Or;

    .line 2303352
    iput-object p5, p0, LX/Fwb;->e:LX/0Or;

    .line 2303353
    iput-object p6, p0, LX/Fwb;->f:LX/0Or;

    .line 2303354
    iput-object p7, p0, LX/Fwb;->g:LX/0Or;

    .line 2303355
    iput-object p8, p0, LX/Fwb;->h:LX/0Or;

    .line 2303356
    iput-object p9, p0, LX/Fwb;->j:LX/5SB;

    .line 2303357
    iput-object p10, p0, LX/Fwb;->i:LX/BQ1;

    .line 2303358
    iget-object v0, p0, LX/Fwb;->j:LX/5SB;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    invoke-virtual {p10}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p10}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v0

    .line 2303359
    iget-object v1, p0, LX/Fwb;->a:LX/FwY;

    iget-object v2, p0, LX/Fwb;->j:LX/5SB;

    invoke-virtual {v2}, LX/5SB;->g()J

    move-result-wide v2

    .line 2303360
    iput-wide v2, v1, LX/FwY;->f:J

    .line 2303361
    iput-object v0, v1, LX/FwY;->g:LX/9lQ;

    .line 2303362
    return-void
.end method

.method private a()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2303363
    iget-object v0, p0, LX/Fwb;->l:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2303364
    new-instance v0, LX/Fwa;

    invoke-direct {v0, p0}, LX/Fwa;-><init>(LX/Fwb;)V

    iput-object v0, p0, LX/Fwb;->l:Landroid/view/View$OnClickListener;

    .line 2303365
    :cond_0
    iget-object v0, p0, LX/Fwb;->l:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;LX/0Px;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$ExternalLink;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2303366
    iget-object v0, p0, LX/Fwb;->k:LX/0Px;

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2303367
    if-nez v0, :cond_7

    .line 2303368
    if-nez p2, :cond_0

    move v5, v6

    .line 2303369
    :cond_0
    :goto_0
    move v0, v5

    .line 2303370
    if-eqz v0, :cond_2

    .line 2303371
    :cond_1
    :goto_1
    return-void

    .line 2303372
    :cond_2
    iput-object p2, p0, LX/Fwb;->k:LX/0Px;

    .line 2303373
    invoke-virtual {p1}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->a()V

    .line 2303374
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_3

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "instagram"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Fwb;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8p8;

    invoke-virtual {v0}, LX/8p8;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2303375
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->setForceFirstItemSeparateLine(Z)V

    .line 2303376
    :cond_3
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_2
    if-ge v1, v3, :cond_5

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    .line 2303377
    new-instance v4, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;

    invoke-virtual {p1}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;-><init>(Landroid/content/Context;)V

    .line 2303378
    iget-object v5, p0, LX/Fwb;->a:LX/FwY;

    const/4 v7, 0x0

    .line 2303379
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;->a()Ljava/lang/String;

    move-result-object v6

    const-string v8, "instagram"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    .line 2303380
    iget-object v6, v5, LX/FwY;->e:LX/8p8;

    invoke-virtual {v6}, LX/8p8;->b()Z

    move-result v6

    .line 2303381
    if-eqz v6, :cond_4

    .line 2303382
    iget-object v9, v4, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a00e6

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2303383
    :cond_4
    if-eqz v8, :cond_e

    if-eqz v6, :cond_e

    .line 2303384
    invoke-virtual {v4}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f0815f3

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->d()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v6, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2303385
    :goto_3
    iget-object v9, v4, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v9, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2303386
    invoke-static {v0}, LX/FwY;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 2303387
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->b()LX/1Fb;

    move-result-object v6

    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2303388
    iget-object v9, v4, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v10, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v9, v6, v10}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2303389
    :goto_4
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->e()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_10

    move-object v11, v7

    .line 2303390
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->a()Ljava/lang/String;

    move-result-object v10

    .line 2303391
    if-nez v11, :cond_11

    .line 2303392
    invoke-virtual {v4, v7}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2303393
    invoke-virtual {v4}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f020692

    invoke-static {v6, v7}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-static {v4, v6}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2303394
    :goto_6
    invoke-virtual {p1, v4}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->a(Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;)V

    .line 2303395
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    .line 2303396
    :cond_5
    iget-object v0, p0, LX/Fwb;->b:LX/0ad;

    sget-short v1, LX/0wf;->y:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2303397
    iget-object v1, p0, LX/Fwb;->j:LX/5SB;

    invoke-virtual {v1}, LX/5SB;->i()Z

    move-result v1

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    .line 2303398
    invoke-direct {p0}, LX/Fwb;->a()Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 2303399
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->b()V

    .line 2303400
    iget-object v0, p0, LX/Fwb;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8p8;

    invoke-virtual {v0}, LX/8p8;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Fwb;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FwD;

    iget-object v2, p0, LX/Fwb;->i:LX/BQ1;

    iget-object v3, p0, LX/Fwb;->j:LX/5SB;

    iget-object v1, p0, LX/Fwb;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fw7;

    iget-object v4, p0, LX/Fwb;->j:LX/5SB;

    iget-object v5, p0, LX/Fwb;->i:LX/BQ1;

    invoke-virtual {v1, v4, v5}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/FwD;->a(LX/BQ1;LX/5SB;LX/Fve;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, LX/3CW;->c(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2303401
    invoke-virtual {p1}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;->c()V

    goto/16 :goto_1

    .line 2303402
    :cond_7
    if-eqz p2, :cond_0

    .line 2303403
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v3

    if-ne v1, v3, :cond_0

    move v4, v5

    .line 2303404
    :goto_7
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_8

    .line 2303405
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    .line 2303406
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->e()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v8

    .line 2303407
    if-nez v7, :cond_b

    if-nez v8, :cond_a

    const/4 v9, 0x1

    :goto_8
    move v7, v9

    .line 2303408
    if-eqz v7, :cond_9

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->b()LX/1Fb;

    move-result-object v7

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->b()LX/1Fb;

    move-result-object v8

    .line 2303409
    if-nez v7, :cond_d

    if-nez v8, :cond_c

    const/4 v9, 0x1

    :goto_9
    move v7, v9

    .line 2303410
    if-eqz v7, :cond_9

    const/4 v7, 0x1

    :goto_a
    move v1, v7

    .line 2303411
    if-eqz v1, :cond_0

    .line 2303412
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_7

    :cond_8
    move v5, v6

    .line 2303413
    goto/16 :goto_0

    :cond_9
    const/4 v7, 0x0

    goto :goto_a

    :cond_a
    const/4 v9, 0x0

    goto :goto_8

    :cond_b
    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;->a()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    goto :goto_8

    :cond_c
    const/4 v9, 0x0

    goto :goto_9

    :cond_d
    invoke-interface {v7}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    goto :goto_9

    .line 2303414
    :cond_e
    invoke-virtual {v4}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v5, v6, v0}, LX/FwY;->a(LX/FwY;Landroid/content/Context;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;)Ljava/lang/CharSequence;

    move-result-object v6

    goto/16 :goto_3

    .line 2303415
    :cond_f
    const/16 v6, 0x8

    .line 2303416
    iget-object v9, v4, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v9, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2303417
    goto/16 :goto_4

    .line 2303418
    :cond_10
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    goto/16 :goto_5

    .line 2303419
    :cond_11
    invoke-static {v10}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v9

    .line 2303420
    new-instance v6, LX/FwX;

    move-object v7, v5

    move-object v12, v0

    invoke-direct/range {v6 .. v12}, LX/FwX;-><init>(LX/FwY;ZZLjava/lang/String;Landroid/net/Uri;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;)V

    invoke-virtual {v4, v6}, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinkView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_6
.end method
