.class public final LX/Gx2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWC;


# instance fields
.field public a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/GxF;


# direct methods
.method public constructor <init>(LX/GxF;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2407665
    iput-object p1, p0, LX/Gx2;->c:LX/GxF;

    iput-object p2, p0, LX/Gx2;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407666
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Gx2;->a:Ljava/util/Queue;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 3

    .prologue
    .line 2407667
    monitor-enter p0

    .line 2407668
    :try_start_0
    iget-object v0, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2407669
    const-string v1, "clearHttpCache"

    invoke-interface {p3, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2407670
    iget-object v0, p0, LX/Gx2;->c:LX/GxF;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/GxF;->clearCache(Z)V

    .line 2407671
    iget-object v0, p0, LX/Gx2;->c:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/Gwx;->a(Landroid/content/Context;)V

    .line 2407672
    iget-object v0, p0, LX/Gx2;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 2407673
    :cond_0
    iget-object v0, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2407674
    const-string v1, "clearLocalStorage"

    invoke-interface {p3, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2407675
    invoke-static {p1}, LX/BWG;->a(Landroid/content/Context;)V

    .line 2407676
    iget-object v0, p0, LX/Gx2;->c:LX/GxF;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/GxF;->clearCache(Z)V

    .line 2407677
    :cond_1
    iget-object v0, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2407678
    const-string v1, "clearCookies"

    invoke-interface {p3, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2407679
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 2407680
    iget-object v0, p0, LX/Gx2;->c:LX/GxF;

    iget-object v0, v0, LX/GxF;->r:LX/BWW;

    iget-object v1, p0, LX/Gx2;->b:Landroid/content/Context;

    iget-object v2, p0, LX/Gx2;->c:LX/GxF;

    iget-object v2, v2, LX/GxF;->l:LX/BWV;

    invoke-interface {v0, v1, v2}, LX/BWW;->a(Landroid/content/Context;LX/BWV;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2407681
    :cond_2
    monitor-exit p0

    return-void

    .line 2407682
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
