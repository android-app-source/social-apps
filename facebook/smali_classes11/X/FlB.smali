.class public final enum LX/FlB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FlB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FlB;

.field public static final enum COPY_LINK:LX/FlB;

.field public static final enum DELETE_FUNDRAISER:LX/FlB;

.field public static final enum EDIT_FUNDRAISER:LX/FlB;

.field public static final enum FOLLOW:LX/FlB;

.field public static final enum FOLLOWING:LX/FlB;

.field public static final enum FOLLOW_FUNDRAISER:LX/FlB;

.field public static final enum GO_TO_PAGE:LX/FlB;

.field public static final enum INVITE:LX/FlB;

.field public static final enum REPORT_FUNDRAISER:LX/FlB;

.field public static final enum SHARE:LX/FlB;

.field public static final enum UNFOLLOW_FUNDRAISER:LX/FlB;


# instance fields
.field private final mIconResId:I

.field private final mTitleResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2279820
    new-instance v0, LX/FlB;

    const-string v1, "SHARE"

    const v2, 0x7f081f56

    const v3, 0x7f0209c5

    invoke-direct {v0, v1, v5, v2, v3}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->SHARE:LX/FlB;

    .line 2279821
    new-instance v0, LX/FlB;

    const-string v1, "INVITE"

    const v2, 0x7f082f34    # 1.810201E38f

    const v3, 0x7f020850

    invoke-direct {v0, v1, v6, v2, v3}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->INVITE:LX/FlB;

    .line 2279822
    new-instance v0, LX/FlB;

    const-string v1, "GO_TO_PAGE"

    const v2, 0x7f0832f6

    invoke-direct {v0, v1, v7, v2, v5}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->GO_TO_PAGE:LX/FlB;

    .line 2279823
    new-instance v0, LX/FlB;

    const-string v1, "REPORT_FUNDRAISER"

    const v2, 0x7f0832f7

    invoke-direct {v0, v1, v8, v2, v5}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->REPORT_FUNDRAISER:LX/FlB;

    .line 2279824
    new-instance v0, LX/FlB;

    const-string v1, "COPY_LINK"

    const v2, 0x7f081f5e

    invoke-direct {v0, v1, v9, v2, v5}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->COPY_LINK:LX/FlB;

    .line 2279825
    new-instance v0, LX/FlB;

    const-string v1, "FOLLOW"

    const/4 v2, 0x5

    const v3, 0x7f0832f9

    const v4, 0x7f02087f

    invoke-direct {v0, v1, v2, v3, v4}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->FOLLOW:LX/FlB;

    .line 2279826
    new-instance v0, LX/FlB;

    const-string v1, "FOLLOWING"

    const/4 v2, 0x6

    const v3, 0x7f0832fa

    const v4, 0x7f020881

    invoke-direct {v0, v1, v2, v3, v4}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->FOLLOWING:LX/FlB;

    .line 2279827
    new-instance v0, LX/FlB;

    const-string v1, "EDIT_FUNDRAISER"

    const/4 v2, 0x7

    const v3, 0x7f083327

    const v4, 0x7f02080f

    invoke-direct {v0, v1, v2, v3, v4}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->EDIT_FUNDRAISER:LX/FlB;

    .line 2279828
    new-instance v0, LX/FlB;

    const-string v1, "DELETE_FUNDRAISER"

    const/16 v2, 0x8

    const v3, 0x7f08332c

    invoke-direct {v0, v1, v2, v3, v5}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->DELETE_FUNDRAISER:LX/FlB;

    .line 2279829
    new-instance v0, LX/FlB;

    const-string v1, "FOLLOW_FUNDRAISER"

    const/16 v2, 0x9

    const v3, 0x7f0832fb

    invoke-direct {v0, v1, v2, v3, v5}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->FOLLOW_FUNDRAISER:LX/FlB;

    .line 2279830
    new-instance v0, LX/FlB;

    const-string v1, "UNFOLLOW_FUNDRAISER"

    const/16 v2, 0xa

    const v3, 0x7f0832fc

    invoke-direct {v0, v1, v2, v3, v5}, LX/FlB;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/FlB;->UNFOLLOW_FUNDRAISER:LX/FlB;

    .line 2279831
    const/16 v0, 0xb

    new-array v0, v0, [LX/FlB;

    sget-object v1, LX/FlB;->SHARE:LX/FlB;

    aput-object v1, v0, v5

    sget-object v1, LX/FlB;->INVITE:LX/FlB;

    aput-object v1, v0, v6

    sget-object v1, LX/FlB;->GO_TO_PAGE:LX/FlB;

    aput-object v1, v0, v7

    sget-object v1, LX/FlB;->REPORT_FUNDRAISER:LX/FlB;

    aput-object v1, v0, v8

    sget-object v1, LX/FlB;->COPY_LINK:LX/FlB;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/FlB;->FOLLOW:LX/FlB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FlB;->FOLLOWING:LX/FlB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FlB;->EDIT_FUNDRAISER:LX/FlB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FlB;->DELETE_FUNDRAISER:LX/FlB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/FlB;->FOLLOW_FUNDRAISER:LX/FlB;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/FlB;->UNFOLLOW_FUNDRAISER:LX/FlB;

    aput-object v2, v0, v1

    sput-object v0, LX/FlB;->$VALUES:[LX/FlB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2279811
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2279812
    iput p3, p0, LX/FlB;->mTitleResId:I

    .line 2279813
    iput p4, p0, LX/FlB;->mIconResId:I

    .line 2279814
    return-void
.end method

.method public static fromOrdinal(I)LX/FlB;
    .locals 1

    .prologue
    .line 2279819
    invoke-static {}, LX/FlB;->values()[LX/FlB;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FlB;
    .locals 1

    .prologue
    .line 2279818
    const-class v0, LX/FlB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FlB;

    return-object v0
.end method

.method public static values()[LX/FlB;
    .locals 1

    .prologue
    .line 2279832
    sget-object v0, LX/FlB;->$VALUES:[LX/FlB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FlB;

    return-object v0
.end method


# virtual methods
.method public final getIconResId()I
    .locals 1

    .prologue
    .line 2279817
    iget v0, p0, LX/FlB;->mIconResId:I

    return v0
.end method

.method public final getTitleResId()I
    .locals 1

    .prologue
    .line 2279816
    iget v0, p0, LX/FlB;->mTitleResId:I

    return v0
.end method

.method public final isOverflow()Z
    .locals 1

    .prologue
    .line 2279815
    iget v0, p0, LX/FlB;->mIconResId:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
