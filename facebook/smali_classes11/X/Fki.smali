.class public final LX/Fki;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fkk;

.field public final synthetic b:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;LX/Fkk;)V
    .locals 0

    .prologue
    .line 2278873
    iput-object p1, p0, LX/Fki;->b:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    iput-object p2, p0, LX/Fki;->a:LX/Fkk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x2dbe785c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2278874
    iget-object v1, p0, LX/Fki;->b:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->d:LX/17Y;

    iget-object v2, p0, LX/Fki;->b:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->hg:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2278875
    const-string v2, "beneficiary_type"

    iget-object v3, p0, LX/Fki;->a:LX/Fkk;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2278876
    iget-object v2, p0, LX/Fki;->b:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Fki;->b:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserCreationBeneficiarySelectorFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2278877
    const v1, 0x1d72934

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
