.class public final LX/FhJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/0Px",
        "<",
        "LX/8cI;",
        ">;",
        "LX/7Hc",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/7B6;

.field public final synthetic b:LX/FhL;


# direct methods
.method public constructor <init>(LX/FhL;LX/7B6;)V
    .locals 0

    .prologue
    .line 2272648
    iput-object p1, p0, LX/FhJ;->b:LX/FhL;

    iput-object p2, p0, LX/FhJ;->a:LX/7B6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2272649
    check-cast p1, LX/0Px;

    const/4 v8, 0x1

    .line 2272650
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2272651
    iget-object v0, p0, LX/FhJ;->a:LX/7B6;

    instance-of v0, v0, Lcom/facebook/search/api/GraphSearchQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FhJ;->a:LX/7B6;

    check-cast v0, Lcom/facebook/search/api/GraphSearchQuery;

    move-object v1, v0

    .line 2272652
    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cI;

    .line 2272653
    instance-of v2, v0, LX/8cM;

    if-eqz v2, :cond_2

    .line 2272654
    check-cast v0, LX/8cM;

    .line 2272655
    new-instance v2, LX/CwH;

    invoke-direct {v2}, LX/CwH;-><init>()V

    .line 2272656
    iget-object v6, v0, LX/8cI;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2272657
    iput-object v6, v2, LX/CwH;->b:Ljava/lang/String;

    .line 2272658
    move-object v2, v2

    .line 2272659
    iget-object v6, v0, LX/8cI;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2272660
    iput-object v6, v2, LX/CwH;->d:Ljava/lang/String;

    .line 2272661
    move-object v6, v2

    .line 2272662
    iget-object v2, v0, LX/8cM;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2272663
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2272664
    iget-object v2, v0, LX/8cI;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2272665
    invoke-static {v2}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2272666
    :goto_2
    iput-object v2, v6, LX/CwH;->c:Ljava/lang/String;

    .line 2272667
    move-object v2, v6

    .line 2272668
    const-string v6, "content"

    .line 2272669
    iput-object v6, v2, LX/CwH;->e:Ljava/lang/String;

    .line 2272670
    move-object v2, v2

    .line 2272671
    iget-object v6, v0, LX/8cI;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2272672
    :try_start_0
    invoke-static {v6}, LX/CwF;->valueOf(Ljava/lang/String;)LX/CwF;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 2272673
    :goto_3
    move-object v6, v7

    .line 2272674
    iput-object v6, v2, LX/CwH;->g:LX/CwF;

    .line 2272675
    move-object v2, v2

    .line 2272676
    sget-object v6, LX/CwI;->BOOTSTRAP:LX/CwI;

    .line 2272677
    iput-object v6, v2, LX/CwH;->l:LX/CwI;

    .line 2272678
    move-object v2, v2

    .line 2272679
    iput-boolean v8, v2, LX/CwH;->n:Z

    .line 2272680
    move-object v2, v2

    .line 2272681
    iget-wide v9, v0, LX/8cI;->e:D

    move-wide v6, v9

    .line 2272682
    iput-wide v6, v2, LX/CwH;->o:D

    .line 2272683
    move-object v2, v2

    .line 2272684
    iget-object v6, v0, LX/8cI;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2272685
    iput-object v6, v2, LX/CwH;->t:Ljava/lang/String;

    .line 2272686
    move-object v2, v2

    .line 2272687
    iget-object v6, v0, LX/8cM;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2272688
    iput-object v6, v2, LX/CwH;->u:Ljava/lang/String;

    .line 2272689
    move-object v2, v2

    .line 2272690
    iget-object v6, v0, LX/8cM;->c:Ljava/lang/String;

    move-object v0, v6

    .line 2272691
    iput-object v0, v2, LX/CwH;->h:Ljava/lang/String;

    .line 2272692
    move-object v0, v2

    .line 2272693
    invoke-static {v1}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v2

    .line 2272694
    iput-object v2, v0, LX/CwH;->x:LX/0Px;

    .line 2272695
    move-object v0, v0

    .line 2272696
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2272697
    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2272698
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 2272699
    :cond_1
    iget-object v2, v0, LX/8cM;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2272700
    goto :goto_2

    .line 2272701
    :cond_2
    check-cast v0, LX/8cK;

    .line 2272702
    new-instance v2, LX/Cw7;

    invoke-direct {v2}, LX/Cw7;-><init>()V

    .line 2272703
    iget-object v6, v0, LX/8cI;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2272704
    iput-object v6, v2, LX/Cw7;->a:Ljava/lang/String;

    .line 2272705
    move-object v2, v2

    .line 2272706
    iget-object v6, v0, LX/8cI;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2272707
    iput-object v6, v2, LX/Cw7;->b:Ljava/lang/String;

    .line 2272708
    move-object v2, v2

    .line 2272709
    iget-object v6, v0, LX/8cK;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2272710
    iput-object v6, v2, LX/Cw7;->c:Ljava/lang/String;

    .line 2272711
    move-object v2, v2

    .line 2272712
    iget-object v6, v0, LX/8cK;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2272713
    iput-object v6, v2, LX/Cw7;->g:Ljava/lang/String;

    .line 2272714
    move-object v2, v2

    .line 2272715
    iget-object v6, v0, LX/8cI;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2272716
    iput-object v6, v2, LX/Cw7;->f:Ljava/lang/String;

    .line 2272717
    move-object v2, v2

    .line 2272718
    iget-object v6, v0, LX/8cK;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2272719
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2272720
    iput-object v6, v2, LX/Cw7;->e:Landroid/net/Uri;

    .line 2272721
    move-object v2, v2

    .line 2272722
    new-instance v6, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2272723
    iget-object v7, v0, LX/8cI;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2272724
    invoke-direct {v6, v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    .line 2272725
    iput-object v6, v2, LX/Cw7;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2272726
    move-object v2, v2

    .line 2272727
    iget-object v6, v0, LX/8cK;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v6, v6

    .line 2272728
    iput-object v6, v2, LX/Cw7;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2272729
    move-object v2, v2

    .line 2272730
    iget-boolean v6, v0, LX/8cK;->g:Z

    move v6, v6

    .line 2272731
    iput-boolean v6, v2, LX/Cw7;->k:Z

    .line 2272732
    move-object v2, v2

    .line 2272733
    iget-object v6, v0, LX/8cK;->h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v6, v6

    .line 2272734
    iput-object v6, v2, LX/Cw7;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2272735
    move-object v2, v2

    .line 2272736
    iput-boolean v8, v2, LX/Cw7;->m:Z

    .line 2272737
    move-object v2, v2

    .line 2272738
    iget-wide v9, v0, LX/8cI;->e:D

    move-wide v6, v9

    .line 2272739
    iput-wide v6, v2, LX/Cw7;->n:D

    .line 2272740
    move-object v2, v2

    .line 2272741
    iget-boolean v6, v0, LX/8cK;->d:Z

    move v6, v6

    .line 2272742
    iput-boolean v6, v2, LX/Cw7;->h:Z

    .line 2272743
    move-object v2, v2

    .line 2272744
    iget-object v6, v0, LX/8cK;->e:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v6, v6

    .line 2272745
    iput-object v6, v2, LX/Cw7;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2272746
    move-object v2, v2

    .line 2272747
    iget-boolean v6, v0, LX/8cK;->i:Z

    move v6, v6

    .line 2272748
    iput-boolean v6, v2, LX/Cw7;->p:Z

    .line 2272749
    move-object v2, v2

    .line 2272750
    iget-object v6, v0, LX/8cK;->j:Ljava/lang/String;

    move-object v6, v6

    .line 2272751
    iput-object v6, v2, LX/Cw7;->u:Ljava/lang/String;

    .line 2272752
    move-object v2, v2

    .line 2272753
    iget-boolean v6, v0, LX/8cK;->k:Z

    move v0, v6

    .line 2272754
    iput-boolean v0, v2, LX/Cw7;->w:Z

    .line 2272755
    move-object v0, v2

    .line 2272756
    invoke-virtual {v0}, LX/Cw7;->x()Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_4

    .line 2272757
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2272758
    iget-object v1, p0, LX/FhJ;->b:LX/FhL;

    iget-object v1, v1, LX/FhL;->b:LX/3Qe;

    iget-object v2, p0, LX/FhJ;->a:LX/7B6;

    .line 2272759
    const-string v3, "result_parsing_time"

    const-string v4, "ui_thread_waiting_time"

    invoke-static {v1, v3, v4, v2}, LX/3Qe;->a(LX/3Qe;Ljava/lang/String;Ljava/lang/String;LX/7B6;)V

    .line 2272760
    new-instance v1, LX/7Hc;

    invoke-direct {v1, v0}, LX/7Hc;-><init>(LX/0Px;)V

    return-object v1

    :catch_0
    sget-object v7, LX/CwF;->keyword:LX/CwF;

    goto/16 :goto_3
.end method
