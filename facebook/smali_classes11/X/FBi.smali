.class public final enum LX/FBi;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/2lb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FBi;",
        ">;",
        "LX/2lb;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FBi;

.field public static final enum Bookmark:LX/FBi;

.field public static final enum FamilyBridgesProfile:LX/FBi;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2209650
    new-instance v0, LX/FBi;

    const-string v1, "Bookmark"

    invoke-direct {v0, v1, v2}, LX/FBi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FBi;->Bookmark:LX/FBi;

    .line 2209651
    new-instance v0, LX/FBi;

    const-string v1, "FamilyBridgesProfile"

    invoke-direct {v0, v1, v3}, LX/FBi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FBi;->FamilyBridgesProfile:LX/FBi;

    .line 2209652
    const/4 v0, 0x2

    new-array v0, v0, [LX/FBi;

    sget-object v1, LX/FBi;->Bookmark:LX/FBi;

    aput-object v1, v0, v2

    sget-object v1, LX/FBi;->FamilyBridgesProfile:LX/FBi;

    aput-object v1, v0, v3

    sput-object v0, LX/FBi;->$VALUES:[LX/FBi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2209649
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FBi;
    .locals 1

    .prologue
    .line 2209653
    const-class v0, LX/FBi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FBi;

    return-object v0
.end method

.method public static values()[LX/FBi;
    .locals 1

    .prologue
    .line 2209648
    sget-object v0, LX/FBi;->$VALUES:[LX/FBi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FBi;

    return-object v0
.end method
