.class public final LX/GLJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/GLK;


# direct methods
.method public constructor <init>(LX/GLK;)V
    .locals 0

    .prologue
    .line 2342591
    iput-object p1, p0, LX/GLJ;->a:LX/GLK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 2342592
    iget-object v0, p0, LX/GLJ;->a:LX/GLK;

    iget-object v0, v0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    .line 2342593
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    move-object v0, v1

    .line 2342594
    invoke-virtual {v0, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->setInputRowVisibility(Z)V

    .line 2342595
    iget-object v0, p0, LX/GLJ;->a:LX/GLK;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    if-eqz p2, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2342596
    return-void

    .line 2342597
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0
.end method
