.class public final LX/GOL;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GPR;

.field public final synthetic b:Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;LX/GPR;)V
    .locals 0

    .prologue
    .line 2347468
    iput-object p1, p0, LX/GOL;->b:Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;

    iput-object p2, p0, LX/GOL;->a:LX/GPR;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2347469
    iget-object v0, p0, LX/GOL;->b:Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2347470
    iget-object v0, p0, LX/GOL;->a:LX/GPR;

    sget-object v1, LX/GPR;->CNPJ:LX/GPR;

    if-ne v0, v1, :cond_0

    .line 2347471
    iget-object v0, p0, LX/GOL;->b:Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;

    iget-object v1, p0, LX/GOL;->b:Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;

    iget-object v1, v1, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->q:Lcom/facebook/common/locale/Country;

    iget-object v2, p0, LX/GOL;->b:Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;

    invoke-static {v2}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->q(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->a(Lcom/facebook/common/locale/Country;Ljava/lang/String;)V

    .line 2347472
    :goto_0
    return-void

    .line 2347473
    :cond_0
    iget-object v0, p0, LX/GOL;->b:Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->n(Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2347474
    invoke-direct {p0}, LX/GOL;->a()V

    return-void
.end method
