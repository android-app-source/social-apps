.class public final enum LX/FEm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FEm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FEm;

.field public static final enum GAME_ID:LX/FEm;

.field public static final enum GAME_LIST_INDEX:LX/FEm;

.field public static final enum GAME_LIST_IS_SEARCH_RESULT:LX/FEm;

.field public static final enum GAME_LIST_SEARCH_TERM:LX/FEm;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2216664
    new-instance v0, LX/FEm;

    const-string v1, "GAME_ID"

    const-string v2, "game_id"

    invoke-direct {v0, v1, v3, v2}, LX/FEm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEm;->GAME_ID:LX/FEm;

    .line 2216665
    new-instance v0, LX/FEm;

    const-string v1, "GAME_LIST_INDEX"

    const-string v2, "index"

    invoke-direct {v0, v1, v4, v2}, LX/FEm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEm;->GAME_LIST_INDEX:LX/FEm;

    .line 2216666
    new-instance v0, LX/FEm;

    const-string v1, "GAME_LIST_IS_SEARCH_RESULT"

    const-string v2, "game_list_is_search_result"

    invoke-direct {v0, v1, v5, v2}, LX/FEm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEm;->GAME_LIST_IS_SEARCH_RESULT:LX/FEm;

    .line 2216667
    new-instance v0, LX/FEm;

    const-string v1, "GAME_LIST_SEARCH_TERM"

    const-string v2, "game_list_search_term"

    invoke-direct {v0, v1, v6, v2}, LX/FEm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEm;->GAME_LIST_SEARCH_TERM:LX/FEm;

    .line 2216668
    const/4 v0, 0x4

    new-array v0, v0, [LX/FEm;

    sget-object v1, LX/FEm;->GAME_ID:LX/FEm;

    aput-object v1, v0, v3

    sget-object v1, LX/FEm;->GAME_LIST_INDEX:LX/FEm;

    aput-object v1, v0, v4

    sget-object v1, LX/FEm;->GAME_LIST_IS_SEARCH_RESULT:LX/FEm;

    aput-object v1, v0, v5

    sget-object v1, LX/FEm;->GAME_LIST_SEARCH_TERM:LX/FEm;

    aput-object v1, v0, v6

    sput-object v0, LX/FEm;->$VALUES:[LX/FEm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2216670
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2216671
    iput-object p3, p0, LX/FEm;->value:Ljava/lang/String;

    .line 2216672
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FEm;
    .locals 1

    .prologue
    .line 2216673
    const-class v0, LX/FEm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FEm;

    return-object v0
.end method

.method public static values()[LX/FEm;
    .locals 1

    .prologue
    .line 2216669
    sget-object v0, LX/FEm;->$VALUES:[LX/FEm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FEm;

    return-object v0
.end method
