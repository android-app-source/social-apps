.class public final enum LX/GBh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GBh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GBh;

.field public static final enum BACKGROUND_SEND:LX/GBh;

.field public static final enum CP_CHOOSE_SCREEN:LX/GBh;

.field public static final enum RESEND_BUTTON:LX/GBh;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2327160
    new-instance v0, LX/GBh;

    const-string v1, "RESEND_BUTTON"

    invoke-direct {v0, v1, v2}, LX/GBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GBh;->RESEND_BUTTON:LX/GBh;

    .line 2327161
    new-instance v0, LX/GBh;

    const-string v1, "CP_CHOOSE_SCREEN"

    invoke-direct {v0, v1, v3}, LX/GBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GBh;->CP_CHOOSE_SCREEN:LX/GBh;

    .line 2327162
    new-instance v0, LX/GBh;

    const-string v1, "BACKGROUND_SEND"

    invoke-direct {v0, v1, v4}, LX/GBh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GBh;->BACKGROUND_SEND:LX/GBh;

    .line 2327163
    const/4 v0, 0x3

    new-array v0, v0, [LX/GBh;

    sget-object v1, LX/GBh;->RESEND_BUTTON:LX/GBh;

    aput-object v1, v0, v2

    sget-object v1, LX/GBh;->CP_CHOOSE_SCREEN:LX/GBh;

    aput-object v1, v0, v3

    sget-object v1, LX/GBh;->BACKGROUND_SEND:LX/GBh;

    aput-object v1, v0, v4

    sput-object v0, LX/GBh;->$VALUES:[LX/GBh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2327164
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GBh;
    .locals 1

    .prologue
    .line 2327165
    const-class v0, LX/GBh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GBh;

    return-object v0
.end method

.method public static values()[LX/GBh;
    .locals 1

    .prologue
    .line 2327166
    sget-object v0, LX/GBh;->$VALUES:[LX/GBh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GBh;

    return-object v0
.end method
