.class public LX/GWq;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/GWp;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7j6;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/7iP;

.field private final d:LX/GWm;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/7j6;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2362766
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2362767
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2362768
    iput-object v0, p0, LX/GWq;->b:LX/0Px;

    .line 2362769
    new-instance v0, LX/GWm;

    invoke-direct {v0, p0}, LX/GWm;-><init>(LX/GWq;)V

    iput-object v0, p0, LX/GWq;->d:LX/GWm;

    .line 2362770
    iput-object p1, p0, LX/GWq;->a:LX/0Ot;

    .line 2362771
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2362772
    new-instance v0, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;-><init>(Landroid/content/Context;)V

    .line 2362773
    new-instance v1, LX/GWp;

    iget-object v2, p0, LX/GWq;->d:LX/GWm;

    invoke-direct {v1, p0, v0, v2}, LX/GWp;-><init>(LX/GWq;Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;LX/GWm;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2362774
    check-cast p1, LX/GWp;

    .line 2362775
    iget-object v0, p0, LX/GWq;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    move-object v0, v0

    .line 2362776
    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->l()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2362777
    iget-object p0, p1, LX/GWp;->m:Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;

    const/4 p2, 0x0

    invoke-virtual {v2, v1, p2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/facebook/commerce/core/ui/ProductItemWithPriceOverlayView;->a(Landroid/net/Uri;Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)V

    .line 2362778
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2362779
    iget-object v0, p0, LX/GWq;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
