.class public final LX/H3W;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/search/NearbySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/search/NearbySearchFragment;)V
    .locals 0

    .prologue
    .line 2421080
    iput-object p1, p0, LX/H3W;->a:Lcom/facebook/nearby/search/NearbySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/common/SearchSuggestion;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2421081
    iget-object v0, p0, LX/H3W;->a:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v0, v0, Lcom/facebook/nearby/search/NearbySearchFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1k;

    iget-object v1, p0, LX/H3W;->a:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v1, v1, Lcom/facebook/nearby/search/NearbySearchFragment;->m:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/H1j;->CATEGORY_PRESET:LX/H1j;

    iget-object v3, p1, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    iget-object v3, v3, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    invoke-virtual {v3}, LX/0Py;->asList()LX/0Px;

    move-result-object v3

    iget-object v5, p0, LX/H3W;->a:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v5, v5, Lcom/facebook/nearby/search/NearbySearchFragment;->q:Ljava/lang/String;

    iget-object v6, p0, LX/H3W;->a:Lcom/facebook/nearby/search/NearbySearchFragment;

    iget-object v7, v6, Lcom/facebook/nearby/search/NearbySearchFragment;->p:Landroid/location/Location;

    iget-object v8, p1, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-object v6, v4

    invoke-virtual/range {v0 .. v8}, LX/H1k;->a(Ljava/lang/String;LX/H1j;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Landroid/location/Location;Lcom/facebook/graphql/model/GraphQLGeoRectangle;)V

    .line 2421082
    iget-object v0, p0, LX/H3W;->a:Lcom/facebook/nearby/search/NearbySearchFragment;

    .line 2421083
    invoke-static {v0, p1}, Lcom/facebook/nearby/search/NearbySearchFragment;->a$redex0(Lcom/facebook/nearby/search/NearbySearchFragment;Lcom/facebook/nearby/common/SearchSuggestion;)V

    .line 2421084
    return-void
.end method
