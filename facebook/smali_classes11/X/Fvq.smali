.class public LX/Fvq;
.super LX/Fv4;
.source ""

# interfaces
.implements LX/Fv6;


# instance fields
.field private final e:LX/Fwc;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/FwD;

.field private final h:LX/Fw8;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/Fw7;

.field public final k:LX/Fsr;

.field public final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Z

.field private o:I

.field private final p:[I

.field public q:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/0ad;LX/Fwc;LX/0Or;LX/FwD;LX/Fw8;LX/0Ot;LX/Fw7;LX/Fsr;LX/0Or;LX/0Or;Landroid/content/Context;LX/BQ1;LX/BP0;Z)V
    .locals 1
    .param p11    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/Fwc;",
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;",
            "LX/FwD;",
            "LX/Fw8;",
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;",
            "LX/Fw7;",
            "LX/Fsr;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;",
            "Landroid/content/Context;",
            "LX/BQ1;",
            "LX/BP0;",
            "Z)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302111
    invoke-direct {p0, p11, p1, p13, p12}, LX/Fv4;-><init>(Landroid/content/Context;LX/0ad;LX/BP0;LX/BQ1;)V

    .line 2302112
    const/4 v0, -0x1

    iput v0, p0, LX/Fvq;->o:I

    .line 2302113
    invoke-static {}, LX/Fvp;->cachedValues()[LX/Fvp;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, LX/Fvq;->p:[I

    .line 2302114
    iput-object p2, p0, LX/Fvq;->e:LX/Fwc;

    .line 2302115
    iput-object p3, p0, LX/Fvq;->f:LX/0Or;

    .line 2302116
    iput-object p4, p0, LX/Fvq;->g:LX/FwD;

    .line 2302117
    iput-object p5, p0, LX/Fvq;->h:LX/Fw8;

    .line 2302118
    iput-object p6, p0, LX/Fvq;->i:LX/0Ot;

    .line 2302119
    iput-object p7, p0, LX/Fvq;->j:LX/Fw7;

    .line 2302120
    iput-object p8, p0, LX/Fvq;->k:LX/Fsr;

    .line 2302121
    iput-object p9, p0, LX/Fvq;->l:LX/0Or;

    .line 2302122
    iput-object p10, p0, LX/Fvq;->m:LX/0Or;

    .line 2302123
    iput-boolean p14, p0, LX/Fvq;->n:Z

    .line 2302124
    return-void
.end method

.method private a(LX/0Px;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$ExternalLink;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2302125
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    move v7, v0

    move v5, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    .line 2302126
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v0, 0x0

    .line 2302127
    :goto_1
    if-eqz v0, :cond_0

    const-string v3, "Website"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2302128
    add-int/lit8 v5, v5, 0x1

    .line 2302129
    :cond_0
    const-string v3, "instagram"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2302130
    add-int/lit8 v0, v7, 0x1

    .line 2302131
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v7, v0

    goto :goto_0

    .line 2302132
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2302133
    :cond_2
    iget-object v0, p0, LX/Fvq;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BQ9;

    iget-object v0, p0, LX/Fv4;->c:LX/BP0;

    .line 2302134
    iget-wide v8, v0, LX/5SB;->b:J

    move-wide v2, v8

    .line 2302135
    iget-object v0, p0, LX/Fv4;->c:LX/BP0;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    iget-object v4, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    iget-object v6, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v6}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    invoke-static {v0, v4, v6}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v4

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    sub-int v6, v0, v5

    invoke-virtual/range {v1 .. v6}, LX/BQ9;->a(JLX/9lQ;II)V

    .line 2302136
    if-lez v7, :cond_3

    .line 2302137
    iget-object v0, p0, LX/Fvq;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    iget-object v1, p0, LX/Fv4;->c:LX/BP0;

    .line 2302138
    iget-wide v8, v1, LX/5SB;->b:J

    move-wide v2, v8

    .line 2302139
    iget-object v1, p0, LX/Fv4;->c:LX/BP0;

    invoke-virtual {v1}, LX/5SB;->i()Z

    move-result v1

    iget-object v4, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v5

    invoke-static {v1, v4, v5}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1, v7}, LX/BQ9;->a(JLX/9lQ;I)V

    .line 2302140
    :cond_3
    return-void

    :cond_4
    move v0, v7

    goto :goto_2
.end method

.method public static b(LX/Fvq;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2302021
    iget-object v0, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/Ful;)V
    .locals 3

    .prologue
    .line 2302103
    iget-boolean v0, p0, LX/Fvq;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fvq;->h:LX/Fw8;

    iget-object v1, p0, LX/Fv4;->c:LX/BP0;

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0, v1, v2}, LX/Fw8;->a(LX/5SB;LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2302104
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/Ful;->d:Z

    .line 2302105
    iget-object v0, p1, LX/Ful;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FyY;

    sget-object v1, LX/Ful;->a:LX/Fwd;

    invoke-virtual {v0, p1, v1}, LX/FyY;->a(Landroid/view/View;LX/Fwd;)V

    .line 2302106
    invoke-virtual {p1}, LX/Ful;->invalidate()V

    .line 2302107
    :goto_0
    return-void

    .line 2302108
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p1, LX/Ful;->d:Z

    .line 2302109
    invoke-virtual {p1}, LX/Ful;->invalidate()V

    .line 2302110
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2302102
    invoke-static {}, LX/Fvp;->cachedValues()[LX/Fvp;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2302090
    invoke-static {}, LX/Fvp;->cachedValues()[LX/Fvp;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2302091
    sget-object v1, LX/Fvo;->a:[I

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2302092
    invoke-static {p1}, LX/Fv4;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2302093
    :pswitch_0
    new-instance v0, LX/Ful;

    iget-object v1, p0, LX/Fv4;->a:Landroid/content/Context;

    iget-object v2, p0, LX/Fvq;->i:LX/0Ot;

    invoke-direct {v0, v1, v2}, LX/Ful;-><init>(Landroid/content/Context;LX/0Ot;)V

    .line 2302094
    const v1, 0x7f0314e1

    invoke-static {p0, v1, v0}, LX/Fvq;->b(LX/Fvq;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    .line 2302095
    const v2, 0x7f020970

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2302096
    const v2, 0x7f0815f2

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2302097
    move-object v1, v1

    .line 2302098
    invoke-virtual {v0, v1}, LX/Ful;->a(Landroid/view/View;)V

    goto :goto_0

    .line 2302099
    :pswitch_1
    new-instance v0, LX/Ful;

    iget-object v1, p0, LX/Fv4;->a:Landroid/content/Context;

    iget-object v2, p0, LX/Fvq;->i:LX/0Ot;

    invoke-direct {v0, v1, v2}, LX/Ful;-><init>(Landroid/content/Context;LX/0Ot;)V

    .line 2302100
    const v1, 0x7f0314d2

    invoke-static {p0, v1, v0}, LX/Fvq;->b(LX/Fvq;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2302101
    invoke-virtual {v0, v1}, LX/Ful;->a(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2302141
    invoke-static {}, LX/Fvp;->cachedValues()[LX/Fvp;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a([Z)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2302063
    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2302064
    sget-object v0, LX/Fvp;->EMPTY:LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302065
    sget-object v0, LX/Fvp;->CONTENT:LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302066
    :goto_0
    return-void

    .line 2302067
    :cond_0
    iget-object v0, p0, LX/Fvq;->g:LX/FwD;

    iget-object v1, p0, LX/Fv4;->d:LX/BQ1;

    iget-object v2, p0, LX/Fv4;->c:LX/BP0;

    iget-object v3, p0, LX/Fvq;->j:LX/Fw7;

    iget-object v4, p0, LX/Fv4;->c:LX/BP0;

    iget-object v5, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v3, v4, v5}, LX/Fw7;->a(LX/5SB;LX/BQ1;)LX/Fve;

    move-result-object v3

    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 2302068
    iget-object v4, v0, LX/FwD;->a:LX/0ad;

    sget-short v5, LX/0wf;->y:S

    invoke-interface {v4, v5, v8}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 2302069
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 2302070
    invoke-virtual {v1}, LX/BPy;->j()Z

    move-result p0

    if-nez p0, :cond_1

    iget-object p0, v0, LX/FwD;->d:LX/8p8;

    invoke-virtual {p0}, LX/8p8;->a()Z

    move-result p0

    if-nez p0, :cond_6

    :cond_1
    move v5, v10

    .line 2302071
    :cond_2
    :goto_1
    move v5, v5

    .line 2302072
    if-eqz v5, :cond_3

    .line 2302073
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2302074
    :goto_2
    move-object v0, v4

    .line 2302075
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2302076
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "not supported intro view type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2302077
    :pswitch_1
    sget-object v0, LX/Fvp;->EMPTY:LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302078
    sget-object v0, LX/Fvp;->CONTENT:LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    goto :goto_0

    .line 2302079
    :pswitch_2
    sget-object v0, LX/Fvp;->EMPTY:LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aput-boolean v7, p1, v0

    .line 2302080
    sget-object v0, LX/Fvp;->CONTENT:LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    goto :goto_0

    .line 2302081
    :pswitch_3
    sget-object v0, LX/Fvp;->EMPTY:LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aput-boolean v6, p1, v0

    .line 2302082
    sget-object v0, LX/Fvp;->CONTENT:LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    aput-boolean v7, p1, v0

    goto/16 :goto_0

    .line 2302083
    :cond_3
    invoke-virtual {v1}, LX/BQ1;->ag()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 2302084
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_2

    .line 2302085
    :cond_4
    if-eqz v4, :cond_5

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2302086
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_2

    .line 2302087
    :cond_5
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_2

    .line 2302088
    :cond_6
    sget-object p0, LX/Fve;->EXPANDED:LX/Fve;

    if-eq v3, p0, :cond_2

    .line 2302089
    invoke-virtual {v1}, LX/BQ1;->aa()Z

    move-result p0

    if-nez p0, :cond_7

    invoke-virtual {v1}, LX/BQ1;->ac()Z

    move-result p0

    if-eqz p0, :cond_2

    :cond_7
    move v5, v10

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    .line 2302029
    invoke-static {}, LX/Fvp;->cachedValues()[LX/Fvp;

    move-result-object v0

    aget-object v0, v0, p2

    .line 2302030
    iget-object v2, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    .line 2302031
    iget v3, p0, LX/Fvq;->o:I

    if-ne v3, v2, :cond_0

    iget-object v3, p0, LX/Fvq;->p:[I

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v4

    aget v3, v3, v4

    iget-object v4, p0, LX/Fv4;->d:LX/BQ1;

    .line 2302032
    iget v5, v4, LX/BPy;->c:I

    move v4, v5

    .line 2302033
    if-ne v3, v4, :cond_0

    .line 2302034
    const/4 v0, 0x0

    .line 2302035
    :goto_0
    return v0

    .line 2302036
    :cond_0
    iput v2, p0, LX/Fvq;->o:I

    .line 2302037
    iget-object v2, p0, LX/Fvq;->p:[I

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v3

    iget-object v4, p0, LX/Fv4;->d:LX/BQ1;

    .line 2302038
    iget v5, v4, LX/BPy;->c:I

    move v4, v5

    .line 2302039
    aput v4, v2, v3

    .line 2302040
    sget-object v2, LX/Fvp;->EMPTY:LX/Fvp;

    if-ne v0, v2, :cond_3

    .line 2302041
    iget-object v0, p0, LX/Fvq;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    iget-object v2, p0, LX/Fv4;->c:LX/BP0;

    .line 2302042
    iget-wide v5, v2, LX/5SB;->b:J

    move-wide v2, v5

    .line 2302043
    const/4 v9, 0x0

    sget-object v10, LX/9lQ;->SELF:LX/9lQ;

    const-string v11, "links_add_prompt_impression"

    move-object v6, v0

    move-wide v7, v2

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2302044
    if-eqz v5, :cond_1

    .line 2302045
    iget-object v6, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2302046
    :cond_1
    check-cast p1, LX/Ful;

    .line 2302047
    iget-object v0, p1, LX/Ful;->c:Landroid/view/View;

    move-object v0, v0

    .line 2302048
    iget-object v2, p0, LX/Fvq;->q:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_2

    .line 2302049
    new-instance v2, LX/Fvn;

    invoke-direct {v2, p0}, LX/Fvn;-><init>(LX/Fvq;)V

    iput-object v2, p0, LX/Fvq;->q:Landroid/view/View$OnClickListener;

    .line 2302050
    :cond_2
    iget-object v2, p0, LX/Fvq;->q:Landroid/view/View$OnClickListener;

    move-object v2, v2

    .line 2302051
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2302052
    invoke-direct {p0, p1}, LX/Fvq;->b(LX/Ful;)V

    move v0, v1

    .line 2302053
    goto :goto_0

    .line 2302054
    :cond_3
    sget-object v2, LX/Fvp;->CONTENT:LX/Fvp;

    if-ne v0, v2, :cond_4

    .line 2302055
    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->ag()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Fvq;->a(LX/0Px;)V

    .line 2302056
    check-cast p1, LX/Ful;

    .line 2302057
    invoke-direct {p0, p1}, LX/Fvq;->b(LX/Ful;)V

    .line 2302058
    iget-object v0, p0, LX/Fvq;->e:LX/Fwc;

    iget-object v2, p0, LX/Fv4;->c:LX/BP0;

    iget-object v3, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0, v2, v3}, LX/Fwc;->a(LX/5SB;LX/BQ1;)LX/Fwb;

    move-result-object v2

    .line 2302059
    iget-object v0, p1, LX/Ful;->c:Landroid/view/View;

    move-object v0, v0

    .line 2302060
    check-cast v0, Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;

    iget-object v3, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->ag()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/Fwb;->a(Lcom/facebook/timeline/header/externalLinks/IntroCardExternalLinksView;LX/0Px;)V

    move v0, v1

    .line 2302061
    goto :goto_0

    .line 2302062
    :cond_4
    invoke-static {p2}, LX/Fv4;->c(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2302024
    const/4 v0, -0x1

    iput v0, p0, LX/Fvq;->o:I

    move v0, v1

    .line 2302025
    :goto_0
    iget-object v2, p0, LX/Fvq;->p:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2302026
    iget-object v2, p0, LX/Fvq;->p:[I

    aput v1, v2, v0

    .line 2302027
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2302028
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2302023
    invoke-virtual {p0, p1}, LX/Fv4;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fvp;

    invoke-virtual {v0}, LX/Fvp;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2302022
    invoke-static {}, LX/Fvp;->cachedValues()[LX/Fvp;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
