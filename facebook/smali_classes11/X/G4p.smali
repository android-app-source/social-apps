.class public LX/G4p;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Fsr;


# direct methods
.method public constructor <init>(LX/Fsr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317994
    iput-object p1, p0, LX/G4p;->a:LX/Fsr;

    .line 2317995
    return-void
.end method

.method public static a(LX/0QB;)LX/G4p;
    .locals 2

    .prologue
    .line 2317996
    new-instance v1, LX/G4p;

    invoke-static {p0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v0

    check-cast v0, LX/Fsr;

    invoke-direct {v1, v0}, LX/G4p;-><init>(LX/Fsr;)V

    .line 2317997
    move-object v0, v1

    .line 2317998
    return-object v0
.end method

.method public static a(LX/0zw;)V
    .locals 0
    .param p0    # LX/0zw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2317999
    if-eqz p0, :cond_0

    .line 2318000
    invoke-virtual {p0}, LX/0zw;->c()V

    .line 2318001
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/0zw;IZ)V
    .locals 5
    .param p1    # LX/0zw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2318002
    if-nez p1, :cond_0

    .line 2318003
    :goto_0
    return-void

    .line 2318004
    :cond_0
    invoke-virtual {p1}, LX/0zw;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2318005
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d1106

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2318006
    new-instance v1, LX/G4o;

    invoke-direct {v1, p0, p1}, LX/G4o;-><init>(LX/G4p;LX/0zw;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v1, v0

    .line 2318007
    :goto_1
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d1109

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2318008
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f0d1108

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2318009
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 2318010
    if-eqz p3, :cond_2

    .line 2318011
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 2318012
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2318013
    :goto_2
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2318014
    :cond_1
    invoke-virtual {p1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d1106

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 2318015
    :cond_2
    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 2318016
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method
