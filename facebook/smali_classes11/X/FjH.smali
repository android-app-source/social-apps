.class public final LX/FjH;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/FjI;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

.field public b:LX/FhE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/FjI;


# direct methods
.method public constructor <init>(LX/FjI;)V
    .locals 1

    .prologue
    .line 2276638
    iput-object p1, p0, LX/FjH;->c:LX/FjI;

    .line 2276639
    move-object v0, p1

    .line 2276640
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2276641
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2276642
    const-string v0, "SearchNullStateTrendingComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2276643
    if-ne p0, p1, :cond_1

    .line 2276644
    :cond_0
    :goto_0
    return v0

    .line 2276645
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2276646
    goto :goto_0

    .line 2276647
    :cond_3
    check-cast p1, LX/FjH;

    .line 2276648
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2276649
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2276650
    if-eq v2, v3, :cond_0

    .line 2276651
    iget-object v2, p0, LX/FjH;->a:Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/FjH;->a:Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    iget-object v3, p1, LX/FjH;->a:Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    invoke-virtual {v2, v3}, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2276652
    goto :goto_0

    .line 2276653
    :cond_5
    iget-object v2, p1, LX/FjH;->a:Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    if-nez v2, :cond_4

    .line 2276654
    :cond_6
    iget-object v2, p0, LX/FjH;->b:LX/FhE;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/FjH;->b:LX/FhE;

    iget-object v3, p1, LX/FjH;->b:LX/FhE;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2276655
    goto :goto_0

    .line 2276656
    :cond_7
    iget-object v2, p1, LX/FjH;->b:LX/FhE;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
