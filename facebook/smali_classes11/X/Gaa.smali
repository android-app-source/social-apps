.class public final LX/Gaa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gab;


# direct methods
.method public constructor <init>(LX/Gab;)V
    .locals 0

    .prologue
    .line 2368347
    iput-object p1, p0, LX/Gaa;->a:LX/Gab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/lang/Boolean;
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2368348
    iget-object v0, p0, LX/Gaa;->a:LX/Gab;

    iget-object v0, v0, LX/Gab;->b:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v1, p0, LX/Gaa;->a:LX/Gab;

    iget-object v1, v1, LX/Gab;->a:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2368349
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Gaa;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
