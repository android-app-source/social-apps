.class public final LX/FTG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2245098
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2245099
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2245100
    :goto_0
    return v1

    .line 2245101
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2245102
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2245103
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2245104
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2245105
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2245106
    const-string v6, "bio"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2245107
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2245108
    :cond_2
    const-string v6, "context_items"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2245109
    invoke-static {p0, p1}, LX/FTX;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2245110
    :cond_3
    const-string v6, "favorite_photos"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2245111
    invoke-static {p0, p1}, LX/5vT;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2245112
    :cond_4
    const-string v6, "suggested_bio"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2245113
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2245114
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2245115
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2245116
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2245117
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2245118
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2245119
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2245079
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2245080
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245081
    if-eqz v0, :cond_0

    .line 2245082
    const-string v1, "bio"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245083
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2245084
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245085
    if-eqz v0, :cond_1

    .line 2245086
    const-string v1, "context_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245087
    invoke-static {p0, v0, p2, p3}, LX/FTX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245088
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245089
    if-eqz v0, :cond_2

    .line 2245090
    const-string v1, "favorite_photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245091
    invoke-static {p0, v0, p2, p3}, LX/5vT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245092
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245093
    if-eqz v0, :cond_3

    .line 2245094
    const-string v1, "suggested_bio"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245095
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2245096
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2245097
    return-void
.end method
