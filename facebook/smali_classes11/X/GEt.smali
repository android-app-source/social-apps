.class public LX/GEt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GK1;


# direct methods
.method public constructor <init>(LX/GK1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332149
    iput-object p1, p0, LX/GEt;->a:LX/GK1;

    .line 2332150
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332147
    const v0, 0x7f030068

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 1

    .prologue
    .line 2332144
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2332145
    const/4 v0, 0x0

    .line 2332146
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/SegmentedBarInfoView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332142
    iget-object v0, p0, LX/GEt;->a:LX/GK1;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332143
    sget-object v0, LX/8wK;->INSIGHTS_REACH:LX/8wK;

    return-object v0
.end method
