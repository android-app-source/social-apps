.class public final LX/H1q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0

    .prologue
    .line 2415977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2415978
    iput-object p1, p0, LX/H1q;->a:LX/0lC;

    .line 2415979
    return-void
.end method

.method private static b(LX/0lF;)Lcom/facebook/graphql/model/GraphQLGeoRectangle;
    .locals 11

    .prologue
    .line 2415962
    const-string v0, "north"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->e(LX/0lF;)D

    move-result-wide v0

    double-to-float v0, v0

    const-string v1, "west"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->e(LX/0lF;)D

    move-result-wide v2

    double-to-float v1, v2

    const-string v2, "south"

    invoke-virtual {p0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/16N;->e(LX/0lF;)D

    move-result-wide v2

    double-to-float v2, v2

    const-string v3, "east"

    invoke-virtual {p0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-static {v3}, LX/16N;->e(LX/0lF;)D

    move-result-wide v4

    double-to-float v3, v4

    .line 2415963
    new-instance v6, LX/4WT;

    invoke-direct {v6}, LX/4WT;-><init>()V

    float-to-double v8, v0

    .line 2415964
    iput-wide v8, v6, LX/4WT;->c:D

    .line 2415965
    move-object v6, v6

    .line 2415966
    float-to-double v8, v1

    .line 2415967
    iput-wide v8, v6, LX/4WT;->e:D

    .line 2415968
    move-object v6, v6

    .line 2415969
    float-to-double v8, v2

    .line 2415970
    iput-wide v8, v6, LX/4WT;->d:D

    .line 2415971
    move-object v6, v6

    .line 2415972
    float-to-double v8, v3

    .line 2415973
    iput-wide v8, v6, LX/4WT;->b:D

    .line 2415974
    move-object v6, v6

    .line 2415975
    invoke-virtual {v6}, LX/4WT;->a()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v6

    move-object v0, v6

    .line 2415976
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/Iterable;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2415913
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2415914
    :try_start_0
    iget-object v0, p0, LX/H1q;->a:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2415915
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2415916
    const-string v2, "title"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2415917
    if-eqz v6, :cond_0

    .line 2415918
    const-string v2, "suggestionSearchBarText"

    invoke-virtual {v0, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2415919
    const-string v2, "suggestionSearchBarText"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2415920
    :goto_1
    const-string v2, "bounds"

    invoke-virtual {v0, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2415921
    const-string v2, "bounds"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-static {v2}, LX/H1q;->b(LX/0lF;)Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v2

    .line 2415922
    :goto_2
    const-string v7, "topic"

    invoke-virtual {v0, v7}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2415923
    const-string v7, "topic"

    invoke-virtual {v0, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2415924
    const-string v8, "name"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2415925
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v10

    .line 2415926
    const-string v8, "ids"

    invoke-virtual {v0, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    .line 2415927
    invoke-virtual {v8}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0lF;

    .line 2415928
    const-wide/16 v12, 0x0

    invoke-static {v8, v12, v13}, LX/16N;->a(LX/0lF;J)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v10, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2415929
    :cond_1
    new-instance v8, Lcom/facebook/nearby/common/NearbyTopic;

    invoke-direct {v8, v10, v9}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    move-object v0, v8

    .line 2415930
    :goto_4
    new-instance v7, Lcom/facebook/nearby/common/SearchSuggestion;

    invoke-direct {v7, v6, v3, v2, v0}, Lcom/facebook/nearby/common/SearchSuggestion;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGeoRectangle;Lcom/facebook/nearby/common/NearbyTopic;)V

    invoke-virtual {v4, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2415931
    :catch_0
    move-exception v0

    .line 2415932
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected serialization exception"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2415933
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v0, v1

    goto :goto_4

    :cond_4
    move-object v2, v1

    goto :goto_2

    :cond_5
    move-object v3, v1

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2415934
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2415935
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/common/SearchSuggestion;

    .line 2415936
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2415937
    const-string v4, "title"

    iget-object v5, v0, Lcom/facebook/nearby/common/SearchSuggestion;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2415938
    const-string v4, "suggestionSearchBarText"

    iget-object v5, v0, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2415939
    iget-object v4, v0, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    if-eqz v4, :cond_0

    .line 2415940
    const-string v4, "bounds"

    iget-object v5, v0, Lcom/facebook/nearby/common/SearchSuggestion;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2415941
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 2415942
    const-string v7, "north"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2415943
    const-string v7, "west"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2415944
    const-string v7, "south"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2415945
    const-string v7, "east"

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2415946
    move-object v5, v6

    .line 2415947
    invoke-virtual {v3, v4, v5}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2415948
    :cond_0
    iget-object v4, v0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    if-eqz v4, :cond_2

    .line 2415949
    const-string v4, "topic"

    iget-object v0, v0, Lcom/facebook/nearby/common/SearchSuggestion;->d:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2415950
    new-instance v5, LX/0m9;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v6}, LX/0m9;-><init>(LX/0mC;)V

    .line 2415951
    const-string v6, "ids"

    iget-object v7, v0, Lcom/facebook/nearby/common/NearbyTopic;->b:LX/0Rf;

    .line 2415952
    new-instance v9, LX/162;

    sget-object v8, LX/0mC;->a:LX/0mC;

    invoke-direct {v9, v8}, LX/162;-><init>(LX/0mC;)V

    .line 2415953
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    .line 2415954
    invoke-virtual {v9, v8}, LX/162;->a(Ljava/lang/Long;)LX/162;

    goto :goto_1

    .line 2415955
    :cond_1
    move-object v7, v9

    .line 2415956
    invoke-virtual {v5, v6, v7}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2415957
    const-string v6, "name"

    iget-object v7, v0, Lcom/facebook/nearby/common/NearbyTopic;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2415958
    move-object v0, v5

    .line 2415959
    invoke-virtual {v3, v4, v0}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2415960
    :cond_2
    invoke-virtual {v1, v3}, LX/162;->a(LX/0lF;)LX/162;

    goto/16 :goto_0

    .line 2415961
    :cond_3
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
