.class public LX/G2m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:LX/Fsr;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1Np;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/view/View$OnClickListener;

.field private f:Landroid/view/View$OnClickListener;

.field private g:Landroid/view/View$OnClickListener;

.field private h:Landroid/view/View$OnClickListener;

.field public i:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/Fsr;LX/0Or;LX/1Np;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fsr;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Np;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314470
    iput-object p1, p0, LX/G2m;->a:LX/Fsr;

    .line 2314471
    iput-object p2, p0, LX/G2m;->b:LX/0Or;

    .line 2314472
    iput-object p3, p0, LX/G2m;->c:LX/1Np;

    .line 2314473
    iput-object p4, p0, LX/G2m;->d:LX/0Ot;

    .line 2314474
    return-void
.end method

.method public static a(LX/0QB;)LX/G2m;
    .locals 7

    .prologue
    .line 2314487
    const-class v1, LX/G2m;

    monitor-enter v1

    .line 2314488
    :try_start_0
    sget-object v0, LX/G2m;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314489
    sput-object v2, LX/G2m;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314490
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314491
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314492
    new-instance v5, LX/G2m;

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v3

    check-cast v3, LX/Fsr;

    const/16 v4, 0xbc6

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/1Np;->a(LX/0QB;)LX/1Np;

    move-result-object v4

    check-cast v4, LX/1Np;

    const/16 p0, 0x455

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v6, v4, p0}, LX/G2m;-><init>(LX/Fsr;LX/0Or;LX/1Np;LX/0Ot;)V

    .line 2314493
    move-object v0, v5

    .line 2314494
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314495
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G2m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314496
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314497
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2314484
    iget-object v0, p0, LX/G2m;->e:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2314485
    new-instance v0, LX/G2h;

    invoke-direct {v0, p0}, LX/G2h;-><init>(LX/G2m;)V

    iput-object v0, p0, LX/G2m;->e:Landroid/view/View$OnClickListener;

    .line 2314486
    :cond_0
    iget-object v0, p0, LX/G2m;->e:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2314481
    iget-object v0, p0, LX/G2m;->h:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2314482
    new-instance v0, LX/G2k;

    invoke-direct {v0, p0, p1}, LX/G2k;-><init>(LX/G2m;Ljava/lang/String;)V

    iput-object v0, p0, LX/G2m;->h:Landroid/view/View$OnClickListener;

    .line 2314483
    :cond_0
    iget-object v0, p0, LX/G2m;->h:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2314478
    iget-object v0, p0, LX/G2m;->f:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2314479
    new-instance v0, LX/G2i;

    invoke-direct {v0, p0}, LX/G2i;-><init>(LX/G2m;)V

    iput-object v0, p0, LX/G2m;->f:Landroid/view/View$OnClickListener;

    .line 2314480
    :cond_0
    iget-object v0, p0, LX/G2m;->f:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final c()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2314475
    iget-object v0, p0, LX/G2m;->g:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2314476
    new-instance v0, LX/G2j;

    invoke-direct {v0, p0}, LX/G2j;-><init>(LX/G2m;)V

    iput-object v0, p0, LX/G2m;->g:Landroid/view/View$OnClickListener;

    .line 2314477
    :cond_0
    iget-object v0, p0, LX/G2m;->g:Landroid/view/View$OnClickListener;

    return-object v0
.end method
