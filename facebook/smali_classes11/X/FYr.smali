.class public LX/FYr;
.super LX/1OM;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:LX/FYp;

.field private f:LX/1OM;

.field public g:Z

.field public h:Z

.field public i:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2256586
    const-class v0, LX/FYr;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FYr;->a:Ljava/lang/String;

    .line 2256587
    const v0, 0x7f03125a

    sput v0, LX/FYr;->b:I

    .line 2256588
    const v0, 0x7f031259

    sput v0, LX/FYr;->c:I

    return-void
.end method

.method public constructor <init>(LX/1OM;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2256579
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2256580
    new-instance v0, LX/FYp;

    invoke-direct {v0, p0}, LX/FYp;-><init>(LX/FYr;)V

    iput-object v0, p0, LX/FYr;->e:LX/FYp;

    .line 2256581
    iput-object p1, p0, LX/FYr;->f:LX/1OM;

    .line 2256582
    iput-object p2, p0, LX/FYr;->d:Landroid/content/Context;

    .line 2256583
    iput-boolean v1, p0, LX/FYr;->g:Z

    .line 2256584
    iput-boolean v1, p0, LX/FYr;->h:Z

    .line 2256585
    return-void
.end method

.method private e(I)Z
    .locals 1

    .prologue
    .line 2256578
    iget-boolean v0, p0, LX/FYr;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(I)V
    .locals 5

    .prologue
    .line 2256576
    sget-object v0, LX/FYr;->a:Ljava/lang/String;

    const-string v1, "Invalid ViewHolder position = %d, child adapter count = %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2256577
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 2256571
    invoke-direct {p0, p1}, LX/FYr;->e(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2256572
    :goto_0
    return-wide v0

    .line 2256573
    :cond_0
    iget-object v2, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v2}, LX/1OM;->ij_()I

    move-result v2

    if-ge p1, v2, :cond_1

    .line 2256574
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->C_(I)J

    move-result-wide v0

    goto :goto_0

    .line 2256575
    :cond_1
    invoke-direct {p0, p1}, LX/FYr;->f(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2256567
    sget v0, LX/FYr;->b:I

    if-eq p2, v0, :cond_0

    sget v0, LX/FYr;->c:I

    if-ne p2, v0, :cond_1

    .line 2256568
    :cond_0
    iget-object v0, p0, LX/FYr;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2256569
    new-instance v0, LX/FYq;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/FYq;-><init>(Landroid/view/View;)V

    .line 2256570
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 2256564
    instance-of v0, p1, LX/FYq;

    if-nez v0, :cond_0

    .line 2256565
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 2256566
    :cond_0
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2256531
    invoke-direct {p0, p2}, LX/FYr;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2256532
    iget-boolean v0, p0, LX/FYr;->h:Z

    if-eqz v0, :cond_2

    .line 2256533
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance p2, LX/FYo;

    invoke-direct {p2, p0}, LX/FYo;-><init>(LX/FYr;)V

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2256534
    :goto_0
    return-void

    .line 2256535
    :cond_0
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-ge p2, v0, :cond_1

    .line 2256536
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(LX/1a1;I)V

    goto :goto_0

    .line 2256537
    :cond_1
    invoke-direct {p0, p2}, LX/FYr;->f(I)V

    goto :goto_0

    .line 2256538
    :cond_2
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2256562
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->a(Z)V

    .line 2256563
    return-void
.end method

.method public final a_(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 2256559
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    iget-object v1, p0, LX/FYr;->e:LX/FYp;

    invoke-virtual {v0, v1}, LX/1OM;->a(LX/1OD;)V

    .line 2256560
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->a_(Landroid/support/v7/widget/RecyclerView;)V

    .line 2256561
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 2256556
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 2256557
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    iget-object v1, p0, LX/FYr;->e:LX/FYp;

    invoke-virtual {v0, v1}, LX/1OM;->b(LX/1OD;)V

    .line 2256558
    return-void
.end method

.method public final b(LX/1a1;)Z
    .locals 1

    .prologue
    .line 2256555
    instance-of v0, p1, LX/FYq;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->b(LX/1a1;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(LX/1a1;)V
    .locals 1

    .prologue
    .line 2256552
    instance-of v0, p1, LX/FYq;

    if-nez v0, :cond_0

    .line 2256553
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->c(LX/1a1;)V

    .line 2256554
    :cond_0
    return-void
.end method

.method public final d(LX/1a1;)V
    .locals 1

    .prologue
    .line 2256549
    instance-of v0, p1, LX/FYq;

    if-nez v0, :cond_0

    .line 2256550
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->d(LX/1a1;)V

    .line 2256551
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2256541
    invoke-direct {p0, p1}, LX/FYr;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256542
    iget-boolean v0, p0, LX/FYr;->h:Z

    if-eqz v0, :cond_0

    sget v0, LX/FYr;->c:I

    .line 2256543
    :goto_0
    return v0

    .line 2256544
    :cond_0
    sget v0, LX/FYr;->b:I

    goto :goto_0

    .line 2256545
    :cond_1
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 2256546
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0, p1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    goto :goto_0

    .line 2256547
    :cond_2
    invoke-direct {p0, p1}, LX/FYr;->f(I)V

    .line 2256548
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2256539
    iget-object v0, p0, LX/FYr;->f:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    .line 2256540
    iget-boolean v1, p0, LX/FYr;->g:Z

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method
