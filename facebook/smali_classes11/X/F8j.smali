.class public LX/F8j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:LX/23P;

.field public final c:LX/F8S;


# direct methods
.method public constructor <init>(LX/23P;LX/F8T;LX/89v;LX/1OM;Landroid/content/res/Resources;)V
    .locals 1
    .param p3    # LX/89v;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1OM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/content/res/Resources;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2204470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204471
    iput-object p1, p0, LX/F8j;->b:LX/23P;

    .line 2204472
    invoke-virtual {p2, p3, p4}, LX/F8T;->a(LX/89v;LX/1OM;)LX/F8S;

    move-result-object v0

    iput-object v0, p0, LX/F8j;->c:LX/F8S;

    .line 2204473
    iput-object p5, p0, LX/F8j;->a:Landroid/content/res/Resources;

    .line 2204474
    return-void
.end method

.method public static a(LX/F8j;ILandroid/view/View;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2204475
    iget-object v0, p0, LX/F8j;->b:LX/23P;

    iget-object v1, p0, LX/F8j;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
