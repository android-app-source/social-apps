.class public LX/FjJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/search/suggestions/environment/CanVisitTypeaheadSuggestion;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2276705
    iput-object p1, p0, LX/FjJ;->a:LX/0ad;

    .line 2276706
    iput-object p2, p0, LX/FjJ;->b:LX/0Ot;

    .line 2276707
    return-void
.end method

.method public static a(LX/0QB;)LX/FjJ;
    .locals 5

    .prologue
    .line 2276708
    const-class v1, LX/FjJ;

    monitor-enter v1

    .line 2276709
    :try_start_0
    sget-object v0, LX/FjJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276710
    sput-object v2, LX/FjJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276711
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276712
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276713
    new-instance v4, LX/FjJ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    const/16 p0, 0x3b2

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/FjJ;-><init>(LX/0ad;LX/0Ot;)V

    .line 2276714
    move-object v0, v4

    .line 2276715
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276716
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FjJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276717
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276718
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
