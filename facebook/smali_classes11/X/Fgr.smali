.class public final LX/Fgr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bnp;


# instance fields
.field public final synthetic a:LX/FhA;

.field public final synthetic b:LX/Fgu;


# direct methods
.method public constructor <init>(LX/Fgu;LX/FhA;)V
    .locals 0

    .prologue
    .line 2271124
    iput-object p1, p0, LX/Fgr;->b:LX/Fgu;

    iput-object p2, p0, LX/Fgr;->a:LX/FhA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2271125
    iget-object v0, p0, LX/Fgr;->b:LX/Fgu;

    iget-object v0, v0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    if-nez v0, :cond_0

    .line 2271126
    const/4 v0, 0x0

    .line 2271127
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Fgr;->b:LX/Fgu;

    iget-object v0, v0, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v0}, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->m()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2271104
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2271105
    iget-object v0, p0, LX/Fgr;->b:LX/Fgu;

    iget-object v0, v0, LX/Fgu;->e:LX/FiD;

    iget-object v1, p0, LX/Fgr;->b:LX/Fgu;

    iget-object v1, v1, LX/Fgu;->i:Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    invoke-virtual {v1}, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->m()I

    move-result v1

    .line 2271106
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2271107
    iget-object v2, v0, LX/FiD;->b:Landroid/os/Bundle;

    const-string v3, "marketplace_search_uri"

    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2271108
    iget-object v2, v0, LX/FiD;->b:Landroid/os/Bundle;

    const-string v3, "marketplace_search_module"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2271109
    iget-object v2, v0, LX/FiD;->b:Landroid/os/Bundle;

    const-string v3, "marketplace_search_typeahead_react_tag"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2271110
    :cond_0
    iget-object v0, p0, LX/Fgr;->a:LX/FhA;

    .line 2271111
    iget-object v1, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->k()LX/Fi7;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2271112
    :goto_0
    return-void

    .line 2271113
    :cond_1
    iget-object v1, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    const/16 v2, 0x8

    invoke-interface {v1, v2}, LX/Fgb;->a(I)V

    .line 2271114
    iget-object v1, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271115
    iget-object v2, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, v2

    .line 2271116
    const/4 v2, 0x0

    .line 2271117
    iput-object v2, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2271118
    iget-object v1, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->k()LX/Fi7;

    move-result-object v1

    iget-object v2, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v2}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v2

    invoke-interface {v1, v2}, LX/Fi7;->a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;

    move-result-object v1

    iget-object v2, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->e:LX/Fh8;

    invoke-virtual {v1, v2}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwg;)V

    .line 2271119
    iget-object v1, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271120
    iget-object v2, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, v2

    .line 2271121
    iget-object v2, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->g:LX/F5P;

    .line 2271122
    iput-object v2, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2271123
    iget-object v1, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->K(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    goto :goto_0
.end method
