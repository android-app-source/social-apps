.class public final LX/GLu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

.field public final synthetic d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

.field public final synthetic e:LX/GJI;


# direct methods
.method public constructor <init>(LX/GJI;LX/0Px;Ljava/lang/String;Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V
    .locals 0

    .prologue
    .line 2343811
    iput-object p1, p0, LX/GLu;->e:LX/GJI;

    iput-object p2, p0, LX/GLu;->a:LX/0Px;

    iput-object p3, p0, LX/GLu;->b:Ljava/lang/String;

    iput-object p4, p0, LX/GLu;->c:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    iput-object p5, p0, LX/GLu;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2343812
    iget-object v0, p0, LX/GLu;->a:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2343813
    iget-object v1, p0, LX/GLu;->b:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2343814
    :goto_0
    return-void

    .line 2343815
    :cond_0
    iget-object v1, p0, LX/GLu;->c:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080adb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2343816
    new-instance v2, LX/0ju;

    iget-object v3, p0, LX/GLu;->c:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f080ada

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/GLt;

    invoke-direct {v3, p0, v0}, LX/GLt;-><init>(LX/GLu;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/GLs;

    invoke-direct {v2, p0}, LX/GLs;-><init>(LX/GLu;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2343817
    return-void
.end method
