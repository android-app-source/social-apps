.class public LX/FLG;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0tX;

.field private final c:LX/2Ov;

.field private final d:Landroid/content/res/Resources;

.field private final e:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2226267
    const-class v0, LX/FLG;

    sput-object v0, LX/FLG;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/2Ov;Landroid/content/res/Resources;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2226268
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226269
    iput-object p1, p0, LX/FLG;->b:LX/0tX;

    .line 2226270
    iput-object p2, p0, LX/FLG;->c:LX/2Ov;

    .line 2226271
    iput-object p3, p0, LX/FLG;->d:Landroid/content/res/Resources;

    .line 2226272
    iput-object p4, p0, LX/FLG;->e:LX/1Ck;

    .line 2226273
    return-void
.end method
