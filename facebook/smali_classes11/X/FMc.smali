.class public LX/FMc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static volatile i:LX/FMc;


# instance fields
.field private final c:LX/0Sh;

.field private final d:LX/2J0;

.field private final e:Landroid/content/Context;

.field private f:Z

.field public g:J

.field private final h:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2230059
    const-string v0, "content://sms/queued"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/FMc;->a:Landroid/net/Uri;

    .line 2230060
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, LX/FMc;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/2J0;Landroid/content/Context;LX/0SG;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2230125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2230126
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FMc;->f:Z

    .line 2230127
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/FMc;->h:Ljava/lang/Object;

    .line 2230128
    iput-object p1, p0, LX/FMc;->c:LX/0Sh;

    .line 2230129
    iput-object p2, p0, LX/FMc;->d:LX/2J0;

    .line 2230130
    iput-object p3, p0, LX/FMc;->e:Landroid/content/Context;

    .line 2230131
    invoke-interface {p4}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/FMc;->g:J

    .line 2230132
    return-void
.end method

.method public static a(LX/0QB;)LX/FMc;
    .locals 7

    .prologue
    .line 2230112
    sget-object v0, LX/FMc;->i:LX/FMc;

    if-nez v0, :cond_1

    .line 2230113
    const-class v1, LX/FMc;

    monitor-enter v1

    .line 2230114
    :try_start_0
    sget-object v0, LX/FMc;->i:LX/FMc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2230115
    if-eqz v2, :cond_0

    .line 2230116
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2230117
    new-instance p0, LX/FMc;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    const-class v4, LX/2J0;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2J0;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FMc;-><init>(LX/0Sh;LX/2J0;Landroid/content/Context;LX/0SG;)V

    .line 2230118
    move-object v0, p0

    .line 2230119
    sput-object v0, LX/FMc;->i:LX/FMc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2230120
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2230121
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2230122
    :cond_1
    sget-object v0, LX/FMc;->i:LX/FMc;

    return-object v0

    .line 2230123
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2230124
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c()V
    .locals 10

    .prologue
    .line 2230093
    const/4 v1, 0x0

    .line 2230094
    :try_start_0
    iget-object v0, p0, LX/FMc;->e:Landroid/content/Context;

    invoke-static {v0}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v0

    iget-wide v2, p0, LX/FMc;->g:J

    invoke-virtual {v0, v2, v3}, LX/Edh;->a(J)Landroid/database/Cursor;

    move-result-object v1

    .line 2230095
    if-eqz v1, :cond_2

    .line 2230096
    invoke-static {v1}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;

    move-result-object v1

    .line 2230097
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2230098
    const-string v0, "msg_id"

    invoke-static {v1, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 2230099
    const-string v0, "err_type"

    invoke-static {v1, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2230100
    const-string v0, "msg_type"

    invoke-static {v1, v0}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 2230101
    const-wide/16 v8, 0x80

    cmp-long v0, v6, v8

    if-nez v0, :cond_0

    const-wide/16 v6, 0xa

    cmp-long v0, v4, v6

    if-gez v0, :cond_0

    .line 2230102
    iget-object v0, p0, LX/FMc;->e:Landroid/content/Context;

    sget-object v4, LX/2UG;->a:Landroid/net/Uri;

    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, LX/FMn;->a(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2230103
    :catch_0
    move-exception v0

    .line 2230104
    :try_start_1
    const-string v2, "MmsSmsPendingMessagesMarker"

    const-string v3, "Exception when marking pending mms messages as failed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2230105
    if-eqz v1, :cond_1

    .line 2230106
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2230107
    :cond_1
    :goto_1
    return-void

    .line 2230108
    :cond_2
    if-eqz v1, :cond_1

    .line 2230109
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 2230110
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 2230111
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2230073
    iget-object v0, p0, LX/FMc;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2230074
    :try_start_0
    const-string v1, "date"

    iget-wide v2, p0, LX/FMc;->g:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0uu;->b(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 2230075
    sget-object v1, LX/FMc;->a:Landroid/net/Uri;

    sget-object v2, LX/FMc;->b:[Ljava/lang/String;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2230076
    :goto_0
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2230077
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 2230078
    sget-object v2, LX/554;->a:Landroid/net/Uri;

    int-to-long v4, v0

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 2230079
    :try_start_2
    iget-object v0, p0, LX/FMc;->e:Landroid/content/Context;

    const/4 v3, 0x5

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, LX/554;->a(Landroid/content/Context;Landroid/net/Uri;II)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2230080
    :catch_0
    move-exception v0

    .line 2230081
    :try_start_3
    const-string v3, "MmsSmsPendingMessagesMarker"

    const-string v4, "Error moving sms to failed folder: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v3, v0, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 2230082
    :catch_1
    move-exception v0

    .line 2230083
    :goto_1
    :try_start_4
    const-string v2, "MmsSmsPendingMessagesMarker"

    const-string v3, "Exception when marking pending sms messages as failed"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2230084
    if-eqz v1, :cond_0

    .line 2230085
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2230086
    :cond_0
    :goto_2
    return-void

    .line 2230087
    :cond_1
    if-eqz v1, :cond_0

    .line 2230088
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 2230089
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_2

    .line 2230090
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2230091
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 2230092
    :catch_2
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2230072
    iget-boolean v0, p0, LX/FMc;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2230061
    iget-object v0, p0, LX/FMc;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 2230062
    iget-boolean v0, p0, LX/FMc;->f:Z

    if-eqz v0, :cond_0

    .line 2230063
    :goto_0
    return-void

    .line 2230064
    :cond_0
    iget-object v1, p0, LX/FMc;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 2230065
    :try_start_0
    iget-boolean v0, p0, LX/FMc;->f:Z

    if-eqz v0, :cond_1

    .line 2230066
    monitor-exit v1

    goto :goto_0

    .line 2230067
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2230068
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/FMc;->f:Z

    .line 2230069
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2230070
    invoke-direct {p0}, LX/FMc;->c()V

    .line 2230071
    invoke-direct {p0}, LX/FMc;->d()V

    goto :goto_0
.end method
