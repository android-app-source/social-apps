.class public final LX/H1t;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

.field public final e:F

.field public final f:F


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGeoRectangle;FF)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLGeoRectangle;",
            "FF)V"
        }
    .end annotation

    .prologue
    .line 2416009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416010
    iput-object p1, p0, LX/H1t;->a:Ljava/util/Set;

    .line 2416011
    invoke-static {p2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/H1t;->b:Ljava/util/Set;

    .line 2416012
    iput-object p3, p0, LX/H1t;->c:Ljava/lang/String;

    .line 2416013
    iput-object p4, p0, LX/H1t;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2416014
    iput p6, p0, LX/H1t;->e:F

    .line 2416015
    iput p5, p0, LX/H1t;->f:F

    .line 2416016
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2416017
    instance-of v1, p1, LX/H1t;

    if-nez v1, :cond_1

    .line 2416018
    :cond_0
    :goto_0
    return v0

    .line 2416019
    :cond_1
    check-cast p1, LX/H1t;

    .line 2416020
    iget-object v1, p0, LX/H1t;->a:Ljava/util/Set;

    iget-object v2, p1, LX/H1t;->a:Ljava/util/Set;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/H1t;->b:Ljava/util/Set;

    iget-object v2, p1, LX/H1t;->b:Ljava/util/Set;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/H1t;->c:Ljava/lang/String;

    iget-object v2, p1, LX/H1t;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/H1t;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iget-object v2, p1, LX/H1t;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LX/H1t;->f:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iget v2, p1, LX/H1t;->f:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LX/H1t;->e:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iget v2, p1, LX/H1t;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2416021
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/H1t;->a:Ljava/util/Set;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/H1t;->b:Ljava/util/Set;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/H1t;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/H1t;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, LX/H1t;->f:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, LX/H1t;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
