.class public LX/FVv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FVF;
.implements LX/FVH;
.implements LX/FVI;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FVF",
        "<",
        "LX/FVt;",
        ">;",
        "LX/FVH",
        "<",
        "LX/FVt;",
        ">;",
        "LX/FVI",
        "<",
        "LX/FVt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/FVt;

.field private final b:LX/FWJ;


# direct methods
.method public constructor <init>(LX/FVt;LX/FWJ;)V
    .locals 0
    .param p1    # LX/FVt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2251483
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2251484
    iput-object p1, p0, LX/FVv;->a:LX/FVt;

    .line 2251485
    iput-object p2, p0, LX/FVv;->b:LX/FWJ;

    .line 2251486
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251478
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    .line 2251479
    iget-object v1, v0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v1

    .line 2251480
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    .line 2251481
    iget-object v1, v0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v1

    .line 2251482
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2251477
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2251472
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    .line 2251473
    iget-object v1, v0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v1

    .line 2251474
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    .line 2251475
    iget-object v1, v0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v1

    .line 2251476
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 2251464
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    const/4 v1, 0x0

    .line 2251465
    if-eqz v0, :cond_0

    .line 2251466
    iget-object v2, v0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v2, v2

    .line 2251467
    if-eqz v2, :cond_0

    .line 2251468
    iget-object v2, v0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v2, v2

    .line 2251469
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->I()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2251470
    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 2251471
    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251459
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    .line 2251460
    iget-object v1, v0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v1

    .line 2251461
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    .line 2251462
    iget-object v1, v0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v1

    .line 2251463
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->x()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251458
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    invoke-static {v0}, LX/FWJ;->d(LX/FVt;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251453
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    .line 2251454
    iget-object v1, v0, LX/FVt;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-object v0, v1

    .line 2251455
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FVv;->a:LX/FVt;

    .line 2251456
    iget-object v1, v0, LX/FVt;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-object v0, v1

    .line 2251457
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251437
    iget-object v1, p0, LX/FVv;->a:LX/FVt;

    .line 2251438
    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 2251439
    if-nez v1, :cond_3

    .line 2251440
    :cond_0
    :goto_0
    move v0, v0

    .line 2251441
    if-eqz v0, :cond_1

    .line 2251442
    iget-object v0, v1, LX/FVt;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-object v0, v0

    .line 2251443
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2251444
    :cond_1
    const/4 v0, 0x0

    .line 2251445
    :goto_1
    move-object v0, v0

    .line 2251446
    return-object v0

    .line 2251447
    :cond_2
    iget-object v0, v1, LX/FVt;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-object v0, v0

    .line 2251448
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;->j()LX/0Px;

    move-result-object v0

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel$ActorsModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2251449
    :cond_3
    iget-object p0, v1, LX/FVt;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-object p0, p0

    .line 2251450
    if-eqz p0, :cond_0

    .line 2251451
    iget-object p0, v1, LX/FVt;->h:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;

    move-object p0, p0

    .line 2251452
    invoke-virtual {p0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$SourceObjectModel;->k()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final i()Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251418
    iget-object v1, p0, LX/FVv;->a:LX/FVt;

    const/4 v0, 0x0

    .line 2251419
    if-eqz v1, :cond_0

    .line 2251420
    iget-object v2, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v2, v2

    .line 2251421
    if-nez v2, :cond_1

    .line 2251422
    :cond_0
    :goto_0
    move-object v0, v0

    .line 2251423
    return-object v0

    .line 2251424
    :cond_1
    iget-object v2, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v2, v2

    .line 2251425
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v1, 0x25d6af

    if-ne v3, v1, :cond_5

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 2251426
    if-nez v3, :cond_3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2251427
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    const v1, 0x403827a

    if-eq p0, v1, :cond_6

    :cond_2
    :goto_2
    if-eqz v4, :cond_7

    .line 2251428
    :goto_3
    move v3, v3

    .line 2251429
    if-eqz v3, :cond_4

    .line 2251430
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-static {v0, v2}, LX/16z;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    goto :goto_0

    .line 2251431
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;->f()I

    move-result v3

    const v4, 0x1eaef984

    if-ne v3, v4, :cond_8

    const/4 v3, 0x1

    :goto_4
    move v3, v3

    .line 2251432
    if-eqz v3, :cond_0

    .line 2251433
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->p()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$ShareableTargetExtraFieldsModel$GlobalShareModel;->j()Ljava/lang/String;

    move-result-object v0

    const v2, 0x1eaef984

    invoke-static {v0, v2}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1

    .line 2251434
    :cond_6
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n()LX/1vs;

    move-result-object p0

    iget p0, p0, LX/1vs;->b:I

    .line 2251435
    if-eqz p0, :cond_2

    move v4, v3

    goto :goto_2

    .line 2251436
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->n()LX/1vs;

    move-result-object v4

    iget-object p0, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {p0, v4, v3}, LX/15i;->h(II)Z

    move-result v3

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    goto :goto_4
.end method
