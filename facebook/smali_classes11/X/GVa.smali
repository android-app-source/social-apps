.class public final LX/GVa;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/captcha/fragment/CaptchaFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V
    .locals 0

    .prologue
    .line 2359558
    iput-object p1, p0, LX/GVa;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2359559
    const/4 v1, 0x0

    .line 2359560
    if-nez p1, :cond_1

    move v0, v1

    .line 2359561
    :goto_0
    move v0, v0

    .line 2359562
    if-eqz v0, :cond_0

    .line 2359563
    iget-object v0, p0, LX/GVa;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    iget-object v0, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->d:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083551

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2359564
    iget-object v0, p0, LX/GVa;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    iget-object v0, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2359565
    iget-object v0, p0, LX/GVa;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-static {v0}, Lcom/facebook/captcha/fragment/CaptchaFragment;->k(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    .line 2359566
    iget-object v0, p0, LX/GVa;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    iget-object v0, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GVe;->a(Ljava/lang/Boolean;)V

    .line 2359567
    :goto_1
    return-void

    .line 2359568
    :cond_0
    iget-object v0, p0, LX/GVa;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    .line 2359569
    invoke-static {v0}, Lcom/facebook/captcha/fragment/CaptchaFragment;->p(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    .line 2359570
    iget-object v1, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    .line 2359571
    iget-object v2, v1, LX/GVe;->a:LX/0Zb;

    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v0, LX/GVd;->CAPTCHA_SOLVE_REQUEST_FAILED:LX/GVd;

    invoke-virtual {v0}, LX/GVd;->getEventName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "captcha"

    .line 2359572
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2359573
    move-object p0, p0

    .line 2359574
    const/4 v0, 0x1

    invoke-interface {v2, p0, v0}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2359575
    goto :goto_1

    .line 2359576
    :cond_1
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2359577
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v0, v2, :cond_2

    move v0, v1

    .line 2359578
    goto :goto_0

    .line 2359579
    :cond_2
    invoke-static {p1}, LX/1Bz;->getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 2359580
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    .line 2359581
    iget-object v2, v0, Lcom/facebook/http/protocol/ApiErrorResult;->mJsonResponse:Ljava/lang/String;

    move-object v0, v2

    .line 2359582
    if-nez v0, :cond_3

    move v0, v1

    .line 2359583
    goto :goto_0

    .line 2359584
    :cond_3
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2359585
    const-string v0, "error"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2359586
    :try_start_1
    const-string v2, "code"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 2359587
    const/16 v2, 0xc2b

    if-ne v0, v2, :cond_4

    .line 2359588
    const/4 v0, 0x1

    goto :goto_0

    .line 2359589
    :catch_0
    move v0, v1

    goto :goto_0

    .line 2359590
    :catch_1
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 2359591
    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2359592
    iget-object v0, p0, LX/GVa;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    iget-object v0, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->g:LX/2Bt;

    invoke-interface {v0}, LX/2Bt;->a()V

    .line 2359593
    iget-object v0, p0, LX/GVa;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    iget-object v0, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GVe;->a(Ljava/lang/Boolean;)V

    .line 2359594
    return-void
.end method
