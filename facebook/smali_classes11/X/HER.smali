.class public final LX/HER;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/HES;


# direct methods
.method public constructor <init>(LX/HES;J)V
    .locals 0

    .prologue
    .line 2442037
    iput-object p1, p0, LX/HER;->b:LX/HES;

    iput-wide p2, p0, LX/HER;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2442038
    iget-object v0, p0, LX/HER;->b:LX/HES;

    invoke-static {v0}, LX/HES;->b$redex0(LX/HES;)V

    .line 2442039
    iget-object v0, p0, LX/HER;->b:LX/HES;

    iget-object v0, v0, LX/HES;->c:LX/03V;

    const-string v1, "page_admin_megaphone"

    const-string v2, "Error fetching page admin megaphone data"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2442040
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2442041
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2442042
    if-eqz p1, :cond_0

    .line 2442043
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2442044
    if-nez v0, :cond_1

    .line 2442045
    :cond_0
    iget-object v0, p0, LX/HER;->b:LX/HES;

    iget-object v0, v0, LX/HES;->c:LX/03V;

    const-string v1, "page_admin_megaphone"

    const-string v2, "Fetch page megaphone channel data success but result is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2442046
    :goto_0
    return-void

    .line 2442047
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2442048
    check-cast v0, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel;

    .line 2442049
    iget-object v1, p0, LX/HER;->b:LX/HES;

    invoke-static {v1}, LX/HES;->b$redex0(LX/HES;)V

    .line 2442050
    if-eqz v0, :cond_2

    .line 2442051
    invoke-virtual {v0}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel;->a()Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;

    move-result-object v0

    .line 2442052
    invoke-static {v0}, LX/HEU;->a(Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;)Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2442053
    iget-object v1, p0, LX/HER;->b:LX/HES;

    .line 2442054
    iput-object v0, v1, LX/HES;->e:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;

    .line 2442055
    :cond_2
    iget-object v0, p0, LX/HER;->b:LX/HES;

    iget-object v0, v0, LX/HES;->d:LX/8E2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pma_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/HER;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/8E7;->PAGE_ADMIN_MEGAHPHONE_DATA_FETCHED:LX/8E7;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8E2;->a(Ljava/lang/String;LX/0Rf;)V

    goto :goto_0
.end method
