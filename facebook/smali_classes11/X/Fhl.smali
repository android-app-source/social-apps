.class public final LX/Fhl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fhm;


# direct methods
.method public constructor <init>(LX/Fhm;)V
    .locals 0

    .prologue
    .line 2273680
    iput-object p1, p0, LX/Fhl;->a:LX/Fhm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3f862e4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2273681
    iget-object v1, p0, LX/Fhl;->a:LX/Fhm;

    iget-object v1, v1, LX/Fhm;->b:LX/Fh0;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Fhl;->a:LX/Fhm;

    iget-object v1, v1, LX/Fhm;->c:LX/0ht;

    if-eqz v1, :cond_0

    .line 2273682
    iget-object v1, p0, LX/Fhl;->a:LX/Fhm;

    iget-object v1, v1, LX/Fhm;->b:LX/Fh0;

    iget-object v2, p0, LX/Fhl;->a:LX/Fhm;

    iget-object v2, v2, LX/Fhm;->c:LX/0ht;

    .line 2273683
    invoke-virtual {v2}, LX/0ht;->l()V

    .line 2273684
    iget-object p0, v1, LX/Fh0;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2273685
    iget-object p1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->F:LX/EQ9;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/EQ9;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SZ;

    move-result-object p1

    invoke-interface {p1}, LX/2SZ;->f()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2273686
    iget-object p1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->L:LX/2Sd;

    sget-object v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->b:Ljava/lang/String;

    sget-object v2, LX/7CQ;->ACTIVITY_LOG_LAUNCHED:LX/7CQ;

    invoke-virtual {p1, v1, v2}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;)V

    .line 2273687
    iget-object p1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {p1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object p1

    invoke-static {p1}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, LX/0ax;->bN:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->p:Ljava/lang/String;

    sget-object v2, LX/7C8;->VIDEOS:LX/7C8;

    invoke-static {p1, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    .line 2273688
    :goto_0
    iget-object p1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->o:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2273689
    :cond_0
    const v1, -0xc52c2cd

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2273690
    :cond_1
    sget-object p1, LX/0ax;->bM:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;->p:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    move-object v1, p1

    goto :goto_0
.end method
