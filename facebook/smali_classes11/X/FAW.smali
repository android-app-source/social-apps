.class public final LX/FAW;
.super LX/GnP;
.source ""


# instance fields
.field public final synthetic a:LX/FAa;


# direct methods
.method public constructor <init>(LX/FAa;)V
    .locals 0

    .prologue
    .line 2207020
    iput-object p1, p0, LX/FAW;->a:LX/FAa;

    invoke-direct {p0}, LX/GnP;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2207021
    check-cast p1, LX/GnW;

    const/4 v3, 0x0

    .line 2207022
    iget-object v0, p1, LX/GnW;->a:LX/CHX;

    move-object v0, v0

    .line 2207023
    if-eqz v0, :cond_0

    .line 2207024
    iget-object v0, p0, LX/FAW;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->N:LX/GnF;

    iget-object v1, p0, LX/FAW;->a:LX/FAa;

    invoke-virtual {v1}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2207025
    iget-object v2, p1, LX/GnW;->a:LX/CHX;

    move-object v2, v2

    .line 2207026
    invoke-virtual {v0, v1, v2, v3, v3}, LX/GnF;->a(Landroid/content/Context;LX/CHX;LX/GoE;Ljava/util/Map;)V

    .line 2207027
    :goto_0
    return-void

    .line 2207028
    :cond_0
    iget-object v0, p0, LX/FAW;->a:LX/FAa;

    iget-object v0, v0, LX/FAa;->N:LX/GnF;

    iget-object v1, p0, LX/FAW;->a:LX/FAa;

    invoke-virtual {v1}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2207029
    iget-object v2, p1, LX/GnW;->b:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;

    move-object v2, v2

    .line 2207030
    sget-object v4, LX/GnD;->a:[I

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2207031
    :goto_1
    :pswitch_0
    goto :goto_0

    .line 2207032
    :pswitch_1
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, LX/1xP;->b(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2207033
    iget-object v4, v0, LX/GnF;->j:LX/Chv;

    new-instance v5, LX/GnX;

    iget-object v6, v0, LX/GnF;->m:LX/Gn7;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-direct {v5, v6}, LX/GnX;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2207034
    :goto_2
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5, v3}, LX/GnF;->a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V

    .line 2207035
    invoke-static {v0}, LX/GnF;->a(LX/GnF;)V

    goto :goto_1

    .line 2207036
    :cond_1
    iget-object v5, v0, LX/GnF;->k:LX/1xP;

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->d()Ljava/lang/String;

    move-result-object v6

    iget-object v4, v0, LX/GnF;->r:LX/CIb;

    .line 2207037
    iget-object v7, v4, LX/CIb;->c:LX/0lF;

    move-object v4, v7

    .line 2207038
    check-cast v4, LX/162;

    const-string v7, "canvas_ads"

    invoke-static {v4, v7}, LX/GnF;->a(LX/162;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v5, v1, v6, v4, v7}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    goto :goto_2

    .line 2207039
    :pswitch_2
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LX/GnF;->b(LX/GnF;Ljava/lang/String;)V

    .line 2207040
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5, v3}, LX/GnF;->a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1

    .line 2207041
    :pswitch_3
    iget-object v4, v0, LX/GnF;->a:Landroid/content/Context;

    iget-object v5, v0, LX/GnF;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0828c9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    .line 2207042
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 2207043
    iget-object v4, v0, LX/GnF;->j:LX/Chv;

    new-instance v5, LX/GnY;

    const-wide/16 v6, 0x1

    invoke-direct {v5, v6, v7}, LX/GnY;-><init>(J)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2207044
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, LX/GnF;->b(LX/GnF;Ljava/lang/String;)V

    .line 2207045
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5, v3}, LX/GnF;->a(LX/GnF;LX/GoE;Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;Ljava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
