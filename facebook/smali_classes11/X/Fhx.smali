.class public LX/Fhx;
.super LX/2SP;
.source ""

# interfaces
.implements LX/2Sp;
.implements LX/2Sq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2SP;",
        "LX/2Sp;",
        "LX/2Sq",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/2Sf;

.field private final b:Landroid/content/res/Resources;

.field private final c:LX/FhP;

.field public d:LX/7Hi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/103;

.field private f:Ljava/lang/String;

.field public g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/2SR;

.field private i:LX/2Sp;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/2Sf;LX/FhP;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2273997
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 2273998
    iput-object v0, p0, LX/Fhx;->d:LX/7Hi;

    .line 2273999
    iput-object v0, p0, LX/Fhx;->e:LX/103;

    .line 2274000
    iput-object v0, p0, LX/Fhx;->f:Ljava/lang/String;

    .line 2274001
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2274002
    iput-object v0, p0, LX/Fhx;->g:LX/0Px;

    .line 2274003
    iput-object p1, p0, LX/Fhx;->b:Landroid/content/res/Resources;

    .line 2274004
    iput-object p2, p0, LX/Fhx;->a:LX/2Sf;

    .line 2274005
    iput-object p3, p0, LX/Fhx;->c:LX/FhP;

    .line 2274006
    return-void
.end method

.method public static j(LX/Fhx;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2273989
    sget-object v0, LX/Fhw;->a:[I

    iget-object v1, p0, LX/Fhx;->e:LX/103;

    invoke-virtual {v1}, LX/103;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2273990
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Scoped search not supported for entity of type %s"

    iget-object v2, p0, LX/Fhx;->e:LX/103;

    invoke-virtual {v2}, LX/103;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2273991
    :pswitch_0
    iget-object v0, p0, LX/Fhx;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2273992
    iget-object v0, p0, LX/Fhx;->b:Landroid/content/res/Resources;

    const v1, 0x7f0822e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Fhx;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2273993
    :goto_0
    return-object v0

    .line 2273994
    :cond_0
    iget-object v0, p0, LX/Fhx;->b:Landroid/content/res/Resources;

    const v1, 0x7f0822ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2273995
    :pswitch_1
    iget-object v0, p0, LX/Fhx;->b:Landroid/content/res/Resources;

    const v1, 0x7f0822e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2273996
    :pswitch_2
    iget-object v0, p0, LX/Fhx;->b:Landroid/content/res/Resources;

    const v1, 0x7f0822e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/7BE;
    .locals 1

    .prologue
    .line 2273917
    iget-object v0, p0, LX/Fhx;->d:LX/7Hi;

    if-eqz v0, :cond_0

    sget-object v0, LX/7BE;->READY:LX/7BE;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7BE;->NOT_READY:LX/7BE;

    goto :goto_0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 0

    .prologue
    .line 2273986
    iput-object p1, p0, LX/Fhx;->h:LX/2SR;

    .line 2273987
    iput-object p2, p0, LX/Fhx;->i:LX/2Sp;

    .line 2273988
    return-void
.end method

.method public final a(LX/7HZ;)V
    .locals 2

    .prologue
    .line 2273981
    sget-object v0, LX/7HZ;->ERROR:LX/7HZ;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/Fhx;->h:LX/2SR;

    if-eqz v0, :cond_0

    .line 2273982
    iget-object v0, p0, LX/Fhx;->h:LX/2SR;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-interface {v0, v1}, LX/2SR;->a(LX/7BE;)V

    .line 2273983
    :cond_0
    iget-object v0, p0, LX/Fhx;->i:LX/2Sp;

    if-eqz v0, :cond_1

    .line 2273984
    iget-object v0, p0, LX/Fhx;->i:LX/2Sp;

    invoke-interface {v0, p1}, LX/2Sp;->a(LX/7HZ;)V

    .line 2273985
    :cond_1
    return-void
.end method

.method public final a(LX/7Hi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2273977
    iput-object p1, p0, LX/Fhx;->d:LX/7Hi;

    .line 2273978
    iget-object v0, p0, LX/Fhx;->h:LX/2SR;

    if-eqz v0, :cond_0

    .line 2273979
    iget-object v0, p0, LX/Fhx;->h:LX/2SR;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-interface {v0, v1}, LX/2SR;->a(LX/7BE;)V

    .line 2273980
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 2

    .prologue
    .line 2273958
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 2273959
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2273960
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v0

    .line 2273961
    iput-object v0, p0, LX/Fhx;->e:LX/103;

    .line 2273962
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2273963
    iput-object v0, p0, LX/Fhx;->f:Ljava/lang/String;

    .line 2273964
    new-instance v0, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;

    iget-object v1, p0, LX/Fhx;->e:LX/103;

    invoke-direct {v0, v1}, Lcom/facebook/search/model/EmptyScopedNullStateTypeaheadUnit;-><init>(LX/103;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Fhx;->g:LX/0Px;

    .line 2273965
    iget-object v0, p1, LX/7B6;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2273966
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2273967
    const-string v0, ""

    invoke-static {p1, v0}, Lcom/facebook/search/api/GraphSearchQuery;->a(Lcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object p1

    .line 2273968
    :cond_0
    iget-object v0, p0, LX/Fhx;->c:LX/FhP;

    invoke-virtual {v0, p0}, LX/7HT;->a(LX/2Sq;)V

    .line 2273969
    iget-object v0, p0, LX/Fhx;->c:LX/FhP;

    invoke-virtual {v0, p0}, LX/7HT;->a(LX/2Sp;)V

    .line 2273970
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fhx;->d:LX/7Hi;

    .line 2273971
    iget-object v0, p0, LX/Fhx;->c:LX/FhP;

    invoke-virtual {v0}, LX/7HT;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2273972
    iget-object v0, p0, LX/Fhx;->c:LX/FhP;

    .line 2273973
    sget-object v1, LX/0Rg;->a:LX/0Rg;

    move-object v1, v1

    .line 2273974
    invoke-virtual {v0, v1}, LX/7HT;->a(LX/0P1;)V

    .line 2273975
    :cond_1
    iget-object v0, p0, LX/Fhx;->c:LX/FhP;

    invoke-virtual {v0, p1}, LX/7HT;->b(LX/7B6;)V

    .line 2273976
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2273957
    const/4 v0, 0x1

    return v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 2273953
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/Fhx;->d:LX/7Hi;

    .line 2273954
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fhx;->g:LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2273955
    monitor-exit p0

    return-void

    .line 2273956
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2273919
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LX/2SP;->a()LX/7BE;

    move-result-object v0

    sget-object v1, LX/7BE;->NOT_READY:LX/7BE;

    invoke-virtual {v0, v1}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273920
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2273921
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2273922
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Fhx;->d:LX/7Hi;

    .line 2273923
    iget-object v1, v0, LX/7Hi;->b:LX/7Hc;

    move-object v0, v1

    .line 2273924
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v0, v1

    .line 2273925
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2273926
    iget-object v0, p0, LX/Fhx;->d:LX/7Hi;

    .line 2273927
    iget-object v1, v0, LX/7Hi;->b:LX/7Hc;

    move-object v0, v1

    .line 2273928
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v0, v1

    .line 2273929
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2273930
    new-instance v0, LX/Cwa;

    invoke-direct {v0}, LX/Cwa;-><init>()V

    iget-object v1, p0, LX/Fhx;->g:LX/0Px;

    .line 2273931
    iput-object v1, v0, LX/Cwa;->b:LX/0Px;

    .line 2273932
    move-object v0, v0

    .line 2273933
    sget-object v1, LX/Cwb;->NO_GROUP:LX/Cwb;

    .line 2273934
    iput-object v1, v0, LX/Cwa;->a:LX/Cwb;

    .line 2273935
    move-object v0, v0

    .line 2273936
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2273937
    :goto_1
    invoke-static {v0}, LX/2Sf;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2273938
    :cond_1
    new-instance v0, LX/Cwa;

    invoke-direct {v0}, LX/Cwa;-><init>()V

    iget-object v1, p0, LX/Fhx;->d:LX/7Hi;

    .line 2273939
    iget-object v2, v1, LX/7Hi;->b:LX/7Hc;

    move-object v1, v2

    .line 2273940
    iget-object v2, v1, LX/7Hc;->b:LX/0Px;

    move-object v1, v2

    .line 2273941
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2273942
    iput-object v1, v0, LX/Cwa;->b:LX/0Px;

    .line 2273943
    move-object v0, v0

    .line 2273944
    sget-object v1, LX/Cwb;->KEYWORD:LX/Cwb;

    .line 2273945
    iput-object v1, v0, LX/Cwa;->a:LX/Cwb;

    .line 2273946
    move-object v0, v0

    .line 2273947
    invoke-static {p0}, LX/Fhx;->j(LX/Fhx;)Ljava/lang/String;

    move-result-object v1

    .line 2273948
    iput-object v1, v0, LX/Cwa;->c:Ljava/lang/String;

    .line 2273949
    move-object v0, v0

    .line 2273950
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    move-object v0, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273951
    goto :goto_1

    .line 2273952
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final kE_()LX/CwU;
    .locals 1

    .prologue
    .line 2273918
    sget-object v0, LX/CwU;->SINGLE_STATE:LX/CwU;

    return-object v0
.end method
