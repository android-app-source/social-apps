.class public final LX/Fpj;
.super LX/BPS;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2291852
    iput-object p1, p0, LX/Fpj;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-direct {p0, p2}, LX/BPS;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2291853
    check-cast p1, LX/BPR;

    .line 2291854
    iget-object v0, p0, LX/Fpj;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lm_()LX/G4x;

    move-result-object v0

    iget-object v1, p1, LX/BPR;->c:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/graphql/enums/StoryVisibility;->DISAPPEARING:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v4, p1, LX/BPR;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    .line 2291855
    iget-object v0, p0, LX/Fpj;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2291856
    iget-object v0, p0, LX/Fpj;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->x()LX/2dj;

    move-result-object v0

    iget-wide v2, p1, LX/BPR;->a:J

    iget-wide v4, p1, LX/BPR;->b:J

    invoke-virtual {v0, v2, v3, v4, v5}, LX/2dj;->a(JJ)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2291857
    iget-object v0, p0, LX/Fpj;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fqs;

    const-string v1, "BanUser"

    invoke-virtual {v0, v1}, LX/Fqs;->a(Ljava/lang/String;)V

    .line 2291858
    return-void
.end method
