.class public LX/FRP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2240327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0lF;)Lcom/facebook/payments/history/model/PaymentTransactions;
    .locals 14

    .prologue
    .line 2240328
    const-string v0, "viewer"

    invoke-virtual {p0, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2240329
    const-string v1, "pay_account"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2240330
    const-string v1, "pay_transactions"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2240331
    const-string v1, "page_info"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 2240332
    new-instance v2, Lcom/facebook/payments/history/model/SimplePaymentTransactions;

    .line 2240333
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2240334
    const-string v3, "nodes"

    invoke-static {v0, v3}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lF;

    .line 2240335
    invoke-virtual {v3}, LX/0lF;->q()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x0

    :goto_1
    move-object v3, v6

    .line 2240336
    if-eqz v3, :cond_0

    .line 2240337
    invoke-virtual {v4, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2240338
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 2240339
    invoke-virtual {v1}, LX/0lF;->q()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    :goto_2
    move-object v1, v3

    .line 2240340
    invoke-direct {v2, v0, v1}, Lcom/facebook/payments/history/model/SimplePaymentTransactions;-><init>(LX/0Px;Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;)V

    return-object v2

    .line 2240341
    :cond_2
    new-instance v6, LX/FQz;

    invoke-direct {v6}, LX/FQz;-><init>()V

    move-object v6, v6

    .line 2240342
    const-string v7, "id"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    .line 2240343
    iput-object v7, v6, LX/FQz;->a:Ljava/lang/String;

    .line 2240344
    move-object v6, v6

    .line 2240345
    const-string v7, "uri"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    .line 2240346
    iput-object v7, v6, LX/FQz;->h:Ljava/lang/String;

    .line 2240347
    move-object v6, v6

    .line 2240348
    const-string v7, "creation_time"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->c(LX/0lF;)J

    move-result-wide v8

    .line 2240349
    iput-wide v8, v6, LX/FQz;->e:J

    .line 2240350
    move-object v6, v6

    .line 2240351
    const-string v7, "updated_time"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->c(LX/0lF;)J

    move-result-wide v8

    .line 2240352
    iput-wide v8, v6, LX/FQz;->f:J

    .line 2240353
    move-object v6, v6

    .line 2240354
    const-string v7, "sender_pay_profile"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/FRP;->e(LX/0lF;)Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v7

    .line 2240355
    iput-object v7, v6, LX/FQz;->b:Lcom/facebook/payments/history/model/PaymentProfile;

    .line 2240356
    move-object v6, v6

    .line 2240357
    const-string v7, "receiver_pay_profile"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/FRP;->e(LX/0lF;)Lcom/facebook/payments/history/model/PaymentProfile;

    move-result-object v7

    .line 2240358
    iput-object v7, v6, LX/FQz;->c:Lcom/facebook/payments/history/model/PaymentProfile;

    .line 2240359
    move-object v6, v6

    .line 2240360
    const-string v7, "pay_transaction_status"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 2240361
    invoke-virtual {v7}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/FQv;->forValue(Ljava/lang/String;)LX/FQv;

    move-result-object v8

    move-object v7, v8

    .line 2240362
    iput-object v7, v6, LX/FQz;->g:LX/FQv;

    .line 2240363
    move-object v6, v6

    .line 2240364
    const-string v7, "transaction_amount"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    .line 2240365
    invoke-virtual {v7}, LX/0lF;->q()Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v10, 0x0

    :goto_3
    move-object v7, v10

    .line 2240366
    iput-object v7, v6, LX/FQz;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2240367
    move-object v6, v6

    .line 2240368
    new-instance v7, Lcom/facebook/payments/history/model/SimplePaymentTransaction;

    invoke-direct {v7, v6}, Lcom/facebook/payments/history/model/SimplePaymentTransaction;-><init>(LX/FQz;)V

    move-object v6, v7

    .line 2240369
    goto/16 :goto_1

    :cond_3
    new-instance v10, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v11, "currency"

    invoke-virtual {v7, v11}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v11

    invoke-static {v11}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "amount_in_hundredths"

    invoke-virtual {v7, v12}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v12

    invoke-static {v12}, LX/16N;->c(LX/0lF;)J

    move-result-wide v12

    invoke-direct {v10, v11, v12, v13}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    goto :goto_3

    :cond_4
    new-instance v3, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;

    const-string v4, "has_next_page"

    invoke-virtual {v1, v4}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, LX/16N;->a(LX/0lF;Z)Z

    move-result v4

    invoke-direct {v3, v4}, Lcom/facebook/payments/history/model/PaymentHistoryPageInfo;-><init>(Z)V

    goto/16 :goto_2
.end method

.method public static e(LX/0lF;)Lcom/facebook/payments/history/model/PaymentProfile;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2240370
    invoke-virtual {p0}, LX/0lF;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2240371
    :cond_0
    new-instance v0, LX/FQu;

    invoke-direct {v0}, LX/FQu;-><init>()V

    move-object v0, v0

    .line 2240372
    const-string v1, "id"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 2240373
    iput-object v1, v0, LX/FQu;->a:Ljava/lang/String;

    .line 2240374
    move-object v0, v0

    .line 2240375
    const-string v1, "name"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    .line 2240376
    iput-object v1, v0, LX/FQu;->b:Ljava/lang/String;

    .line 2240377
    move-object v0, v0

    .line 2240378
    const-string v1, "profile_picture"

    invoke-virtual {p0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 2240379
    invoke-virtual {v1}, LX/0lF;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v1, v2

    .line 2240380
    iput-object v1, v0, LX/FQu;->c:Lcom/facebook/payments/history/model/ProfileImage;

    .line 2240381
    move-object v0, v0

    .line 2240382
    new-instance v1, Lcom/facebook/payments/history/model/PaymentProfile;

    invoke-direct {v1, v0}, Lcom/facebook/payments/history/model/PaymentProfile;-><init>(LX/FQu;)V

    move-object v0, v1

    .line 2240383
    goto :goto_0

    .line 2240384
    :cond_1
    new-instance v2, LX/FQx;

    invoke-direct {v2}, LX/FQx;-><init>()V

    move-object v2, v2

    .line 2240385
    const-string p0, "uri"

    invoke-virtual {v1, p0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p0

    invoke-static {p0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object p0

    .line 2240386
    iput-object p0, v2, LX/FQx;->a:Ljava/lang/String;

    .line 2240387
    move-object v2, v2

    .line 2240388
    new-instance p0, Lcom/facebook/payments/history/model/ProfileImage;

    invoke-direct {p0, v2}, Lcom/facebook/payments/history/model/ProfileImage;-><init>(LX/FQx;)V

    move-object v2, p0

    .line 2240389
    goto :goto_1
.end method
