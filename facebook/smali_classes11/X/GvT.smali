.class public final LX/GvT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:LX/0if;

.field public final synthetic d:LX/GvU;


# direct methods
.method public constructor <init>(LX/GvU;Ljava/util/List;Ljava/util/List;LX/0if;)V
    .locals 0

    .prologue
    .line 2405448
    iput-object p1, p0, LX/GvT;->d:LX/GvU;

    iput-object p2, p0, LX/GvT;->a:Ljava/util/List;

    iput-object p3, p0, LX/GvT;->b:Ljava/util/List;

    iput-object p4, p0, LX/GvT;->c:LX/0if;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 2405449
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2405450
    check-cast v0, LX/4Ua;

    .line 2405451
    iget-object v1, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget v1, v1, Lcom/facebook/graphql/error/GraphQLError;->code:I

    const v2, 0x1495ad

    if-eq v1, v2, :cond_0

    iget-object v0, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    const v1, 0x1495e3

    if-ne v0, v1, :cond_1

    .line 2405452
    :cond_0
    iget-object v0, p0, LX/GvT;->d:LX/GvU;

    .line 2405453
    iget-object v1, v0, LX/GvU;->i:LX/GvR;

    invoke-interface {v1}, LX/GvR;->m()V

    .line 2405454
    :goto_0
    iget-object v0, p0, LX/GvT;->c:LX/0if;

    sget-object v1, LX/0ig;->aQ:LX/0ih;

    const-string v2, "login_failure"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2405455
    iget-object v0, p0, LX/GvT;->d:LX/GvU;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GvU;->a$redex0(LX/GvU;Z)V

    .line 2405456
    return-void

    .line 2405457
    :cond_1
    iget-object v0, p0, LX/GvT;->d:LX/GvU;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SERVER_ERROR"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Gvi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GvU;->a(LX/3rL;)V

    goto :goto_0

    .line 2405458
    :cond_2
    instance-of v0, p1, Ljava/io/IOException;

    if-eqz v0, :cond_3

    .line 2405459
    iget-object v0, p0, LX/GvT;->d:LX/GvU;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "CONNECTION_FAILURE"

    const-string v3, "CONNECTION_FAILURE"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/Gvi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GvU;->a(LX/3rL;)V

    goto :goto_0

    .line 2405460
    :cond_3
    iget-object v0, p0, LX/GvT;->d:LX/GvU;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "SERVER_ERROR"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Gvi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GvU;->a(LX/3rL;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2405461
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const-wide/16 v8, 0x3e8

    const/4 v7, -0x1

    .line 2405462
    if-eqz p1, :cond_0

    .line 2405463
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2405464
    check-cast v0, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;

    .line 2405465
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 2405466
    invoke-virtual {v0}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->a()J

    move-result-wide v2

    mul-long/2addr v2, v8

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    div-long v4, v2, v8

    .line 2405467
    invoke-virtual {v0}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GvT;->a:Ljava/util/List;

    iget-object v3, p0, LX/GvT;->b:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/katana/gdp/ProxyAuthAppLoginModels$ProxyAuthAppLoginQueryModel;->j()Ljava/lang/String;

    move-result-object v6

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 2405468
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2405469
    const-string v8, "access_token"

    invoke-virtual {v0, v8, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405470
    const-string v8, "expires_in"

    invoke-virtual {v0, v8, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2405471
    const-string v8, "granted_scopes"

    const-string v9, ","

    new-array v10, v12, [Ljava/lang/Object;

    aput-object v2, v10, v11

    invoke-static {v9, v10}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405472
    const-string v8, "denied_scopes"

    const-string v9, ","

    new-array v10, v12, [Ljava/lang/Object;

    aput-object v3, v10, v11

    invoke-static {v9, v10}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405473
    const-string v8, "signed_request"

    invoke-virtual {v0, v8, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405474
    move-object v0, v0

    .line 2405475
    iget-object v1, p0, LX/GvT;->d:LX/GvU;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2, v0}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/GvU;->a(LX/3rL;)V

    .line 2405476
    iget-object v0, p0, LX/GvT;->c:LX/0if;

    sget-object v1, LX/0ig;->aQ:LX/0ih;

    const-string v2, "login_success"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2405477
    :goto_0
    iget-object v0, p0, LX/GvT;->d:LX/GvU;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/GvU;->a$redex0(LX/GvU;Z)V

    .line 2405478
    return-void

    .line 2405479
    :cond_0
    iget-object v0, p0, LX/GvT;->d:LX/GvU;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "SERVER_ERROR"

    const-string v4, "Server Error"

    invoke-static {v2, v3, v4}, LX/Gvi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GvU;->a(LX/3rL;)V

    .line 2405480
    iget-object v0, p0, LX/GvT;->c:LX/0if;

    sget-object v1, LX/0ig;->aQ:LX/0ih;

    const-string v2, "login_failure"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0
.end method
