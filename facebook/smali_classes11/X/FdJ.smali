.class public LX/FdJ;
.super Lcom/facebook/fig/button/FigButton;
.source ""


# instance fields
.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2263388
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FdJ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2263389
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263390
    invoke-direct {p0, p1, p2}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2263391
    const/16 p1, 0x82

    invoke-virtual {p0, p1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2263392
    return-void
.end method


# virtual methods
.method public final performClick()Z
    .locals 1

    .prologue
    .line 2263393
    iget-boolean v0, p0, LX/FdJ;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/FdJ;->setChecked(Z)V

    .line 2263394
    invoke-super {p0}, Lcom/facebook/fig/button/FigButton;->performClick()Z

    move-result v0

    return v0

    .line 2263395
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 2263396
    iget-boolean v0, p0, LX/FdJ;->j:Z

    if-eq v0, p1, :cond_0

    .line 2263397
    iput-boolean p1, p0, LX/FdJ;->j:Z

    .line 2263398
    iget-boolean v0, p0, LX/FdJ;->j:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x12

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2263399
    :cond_0
    return-void

    .line 2263400
    :cond_1
    const/16 v0, 0x82

    goto :goto_0
.end method
