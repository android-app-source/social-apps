.class public LX/H8U;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/H9D;

.field public final c:LX/17Y;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;

.field public final f:Landroid/content/Context;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/00H;",
            ">;"
        }
    .end annotation
.end field

.field public j:J

.field public k:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

.field private l:LX/6WJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2433419
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->LIKE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/H8U;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/H9E;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;)V
    .locals 1
    .param p7    # Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H9E;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/00H;",
            ">;",
            "Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433441
    iput-object p4, p0, LX/H8U;->g:LX/0Ot;

    .line 2433442
    invoke-virtual {p1, p7}, LX/H9E;->a(Landroid/view/View;)LX/H9D;

    move-result-object v0

    iput-object v0, p0, LX/H8U;->b:LX/H9D;

    .line 2433443
    iput-object p2, p0, LX/H8U;->c:LX/17Y;

    .line 2433444
    iput-object p3, p0, LX/H8U;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2433445
    iput-object p5, p0, LX/H8U;->h:LX/0Ot;

    .line 2433446
    iput-object p6, p0, LX/H8U;->i:LX/0Ot;

    .line 2433447
    iput-object p7, p0, LX/H8U;->e:Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;

    .line 2433448
    invoke-virtual {p7}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/H8U;->f:Landroid/content/Context;

    .line 2433449
    return-void
.end method

.method private a(JLcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;IZ)Landroid/view/View$OnClickListener;
    .locals 7

    .prologue
    .line 2433437
    if-nez p5, :cond_0

    .line 2433438
    const/4 v1, 0x0

    .line 2433439
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, LX/H8R;

    move-object v2, p0

    move-object v3, p3

    move-wide v4, p1

    move v6, p4

    invoke-direct/range {v1 .. v6}, LX/H8R;-><init>(LX/H8U;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;JI)V

    goto :goto_0
.end method

.method public static b(LX/H8U;)LX/6WJ;
    .locals 3

    .prologue
    .line 2433434
    iget-object v0, p0, LX/H8U;->l:LX/6WJ;

    if-nez v0, :cond_0

    .line 2433435
    new-instance v0, LX/6WI;

    iget-object v1, p0, LX/H8U;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/6WI;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08368d

    invoke-virtual {v0, v1}, LX/6WI;->b(I)LX/6WI;

    move-result-object v0

    const v1, 0x7f08368e

    new-instance v2, LX/H8T;

    invoke-direct {v2, p0}, LX/H8T;-><init>(LX/H8U;)V

    invoke-virtual {v0, v1, v2}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/H8S;

    invoke-direct {v2, p0}, LX/H8S;-><init>(LX/H8U;)V

    invoke-virtual {v0, v1, v2}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    invoke-virtual {v0}, LX/6WI;->a()LX/6WJ;

    move-result-object v0

    iput-object v0, p0, LX/H8U;->l:LX/6WJ;

    .line 2433436
    :cond_0
    iget-object v0, p0, LX/H8U;->l:LX/6WJ;

    return-object v0
.end method


# virtual methods
.method public final a(JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;LX/0Px;Z)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageOpenActionEditActionData;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2433420
    iput-wide p1, p0, LX/H8U;->j:J

    .line 2433421
    move-object/from16 v0, p3

    iput-object v0, p0, LX/H8U;->k:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    .line 2433422
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2433423
    invoke-virtual/range {p4 .. p4}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v10

    .line 2433424
    const/4 v7, 0x0

    .line 2433425
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/H8U;->e:Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;

    invoke-virtual {v2}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->getNumberItemViews()I

    move-result v2

    if-ge v7, v2, :cond_2

    .line 2433426
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2433427
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2433428
    iget-object v2, p0, LX/H8U;->b:LX/H9D;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v2

    check-cast v2, LX/H8Z;

    .line 2433429
    new-instance v11, LX/H8O;

    invoke-interface {v2}, LX/H8Z;->b()LX/HA7;

    move-result-object v12

    move-object v3, p0

    move-wide v4, p1

    move/from16 v8, p5

    invoke-direct/range {v3 .. v8}, LX/H8U;->a(JLcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;IZ)Landroid/view/View$OnClickListener;

    move-result-object v3

    sget-object v2, LX/H8U;->a:LX/0Rf;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, LX/HA6;->LOCKED:LX/HA6;

    :goto_1
    invoke-direct {v11, v12, v3, v2}, LX/H8O;-><init>(LX/HA7;Landroid/view/View$OnClickListener;LX/HA6;)V

    invoke-virtual {v9, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2433430
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 2433431
    :cond_1
    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v2

    move/from16 v0, p5

    invoke-static {v2, v0}, LX/H8X;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;Z)LX/HA6;

    move-result-object v2

    goto :goto_1

    .line 2433432
    :cond_2
    iget-object v2, p0, LX/H8U;->e:Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;->a(LX/0Px;)V

    .line 2433433
    return-void
.end method
