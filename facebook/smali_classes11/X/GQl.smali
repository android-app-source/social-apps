.class public final LX/GQl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/GQm;


# direct methods
.method public constructor <init>(LX/GQm;)V
    .locals 0

    .prologue
    .line 2350393
    iput-object p1, p0, LX/GQl;->a:LX/GQm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x57bc64f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2350394
    iget-object v1, p0, LX/GQl;->a:LX/GQm;

    const/4 v8, -0x1

    .line 2350395
    const-string v4, "level"

    invoke-virtual {p2, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 2350396
    if-ne v4, v8, :cond_1

    .line 2350397
    iget-object v4, v1, LX/GQm;->e:LX/03V;

    sget-object v5, LX/GQm;->a:Ljava/lang/String;

    const-string v6, "Could not read the battery level from the intent"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2350398
    :cond_0
    :goto_0
    const/16 v1, 0x27

    const v2, 0x165604dc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2350399
    :cond_1
    const-string v5, "status"

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 2350400
    iget v6, v1, LX/GQm;->k:I

    if-ne v6, v5, :cond_2

    iget v6, v1, LX/GQm;->j:I

    if-eq v6, v4, :cond_0

    .line 2350401
    :cond_2
    iput v4, v1, LX/GQm;->j:I

    .line 2350402
    iput v5, v1, LX/GQm;->k:I

    .line 2350403
    invoke-static {v5}, LX/GQm;->a(I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2350404
    invoke-static {v1}, LX/GQm;->a(LX/GQm;)V

    goto :goto_0

    .line 2350405
    :cond_3
    iget v5, v1, LX/GQm;->i:I

    if-ne v5, v8, :cond_5

    .line 2350406
    const/4 v5, -0x1

    .line 2350407
    if-nez p2, :cond_7

    .line 2350408
    iget-object v4, v1, LX/GQm;->e:LX/03V;

    sget-object v5, LX/GQm;->a:Ljava/lang/String;

    const-string v6, "Failed to start monitoring because battery intent is null"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2350409
    :cond_4
    :goto_1
    goto :goto_0

    .line 2350410
    :cond_5
    iget v5, v1, LX/GQm;->i:I

    if-eq v5, v4, :cond_0

    .line 2350411
    iget-object v5, v1, LX/GQm;->d:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    .line 2350412
    iget v5, v1, LX/GQm;->m:I

    if-ne v5, v8, :cond_6

    .line 2350413
    iput v4, v1, LX/GQm;->m:I

    .line 2350414
    iput-wide v6, v1, LX/GQm;->n:J

    goto :goto_0

    .line 2350415
    :cond_6
    iput v4, v1, LX/GQm;->o:I

    .line 2350416
    iput-wide v6, v1, LX/GQm;->p:J

    goto :goto_0

    .line 2350417
    :cond_7
    iget-boolean v4, v1, LX/GQm;->h:Z

    if-eqz v4, :cond_4

    .line 2350418
    const-string v4, "status"

    const/4 v6, 0x1

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 2350419
    invoke-static {v4}, LX/GQm;->a(I)Z

    move-result v4

    move v4, v4

    .line 2350420
    if-eqz v4, :cond_4

    .line 2350421
    const-string v4, "level"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, v1, LX/GQm;->i:I

    .line 2350422
    const-string v4, "scale"

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, v1, LX/GQm;->l:I

    .line 2350423
    iget v4, v1, LX/GQm;->i:I

    iput v4, v1, LX/GQm;->j:I

    .line 2350424
    const/4 v4, 0x3

    iput v4, v1, LX/GQm;->k:I

    .line 2350425
    iget v4, v1, LX/GQm;->i:I

    if-eq v4, v5, :cond_8

    iget v4, v1, LX/GQm;->l:I

    if-ne v4, v5, :cond_4

    .line 2350426
    :cond_8
    iget-object v4, v1, LX/GQm;->e:LX/03V;

    sget-object v5, LX/GQm;->a:Ljava/lang/String;

    const-string v6, "Could not read the current or max battery level from the intent"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
