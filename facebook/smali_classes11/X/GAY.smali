.class public LX/GAY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/net/HttpURLConnection;

.field public final b:Lorg/json/JSONObject;

.field private final c:Lorg/json/JSONArray;

.field public final d:LX/GAF;

.field private final e:Ljava/lang/String;

.field private final f:LX/GAU;


# direct methods
.method private constructor <init>(LX/GAU;Ljava/net/HttpURLConnection;LX/GAF;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 2325804
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONArray;LX/GAF;)V

    .line 2325805
    return-void
.end method

.method private constructor <init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONArray;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2325802
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONArray;LX/GAF;)V

    .line 2325803
    return-void
.end method

.method private constructor <init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2325800
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONArray;LX/GAF;)V

    .line 2325801
    return-void
.end method

.method private constructor <init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONObject;Lorg/json/JSONArray;LX/GAF;)V
    .locals 0

    .prologue
    .line 2325792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2325793
    iput-object p1, p0, LX/GAY;->f:LX/GAU;

    .line 2325794
    iput-object p2, p0, LX/GAY;->a:Ljava/net/HttpURLConnection;

    .line 2325795
    iput-object p3, p0, LX/GAY;->e:Ljava/lang/String;

    .line 2325796
    iput-object p4, p0, LX/GAY;->b:Lorg/json/JSONObject;

    .line 2325797
    iput-object p5, p0, LX/GAY;->c:Lorg/json/JSONArray;

    .line 2325798
    iput-object p6, p0, LX/GAY;->d:LX/GAF;

    .line 2325799
    return-void
.end method

.method private static a(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/Object;Ljava/lang/Object;)LX/GAY;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2325771
    instance-of v0, p2, Lorg/json/JSONObject;

    if-eqz v0, :cond_4

    .line 2325772
    check-cast p2, Lorg/json/JSONObject;

    .line 2325773
    invoke-static {p2, p3, p1}, LX/GAF;->a(Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;)LX/GAF;

    move-result-object v1

    .line 2325774
    if-eqz v1, :cond_1

    .line 2325775
    iget v0, v1, LX/GAF;->d:I

    move v0, v0

    .line 2325776
    const/16 v2, 0xbe

    if-ne v0, v2, :cond_0

    .line 2325777
    iget-object v0, p0, LX/GAU;->d:Lcom/facebook/AccessToken;

    move-object v0, v0

    .line 2325778
    if-eqz v0, :cond_6

    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/AccessToken;->equals(Ljava/lang/Object;)Z

    move-result v2

    :goto_0
    move v0, v2

    .line 2325779
    if-eqz v0, :cond_0

    .line 2325780
    invoke-static {}, LX/GA8;->a()LX/GA8;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/GA8;->a(Lcom/facebook/AccessToken;)V

    .line 2325781
    :cond_0
    new-instance v0, LX/GAY;

    invoke-direct {v0, p0, p1, v1}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;LX/GAF;)V

    .line 2325782
    :goto_1
    return-object v0

    .line 2325783
    :cond_1
    const-string v0, "body"

    const-string v1, "FACEBOOK_NON_JSON_RESULT"

    invoke-static {p2, v0, v1}, LX/Gsc;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2325784
    instance-of v1, v0, Lorg/json/JSONObject;

    if-eqz v1, :cond_2

    .line 2325785
    new-instance v1, LX/GAY;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v0, Lorg/json/JSONObject;

    invoke-direct {v1, p0, p1, v2, v0}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONObject;)V

    move-object v0, v1

    goto :goto_1

    .line 2325786
    :cond_2
    instance-of v1, v0, Lorg/json/JSONArray;

    if-eqz v1, :cond_3

    .line 2325787
    new-instance v1, LX/GAY;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v0, Lorg/json/JSONArray;

    invoke-direct {v1, p0, p1, v2, v0}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONArray;)V

    move-object v0, v1

    goto :goto_1

    .line 2325788
    :cond_3
    sget-object p2, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    .line 2325789
    :cond_4
    sget-object v0, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-ne p2, v0, :cond_5

    .line 2325790
    new-instance v0, LX/GAY;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1, v3}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/String;Lorg/json/JSONObject;)V

    goto :goto_1

    .line 2325791
    :cond_5
    new-instance v0, LX/GAA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got unexpected object type in response, class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/io/InputStream;Ljava/net/HttpURLConnection;LX/GAX;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/net/HttpURLConnection;",
            "LX/GAX;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/GAY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2325698
    invoke-static {p0}, LX/Gsc;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 2325699
    sget-object v1, LX/GAb;->INCLUDE_RAW_RESPONSES:LX/GAb;

    const-string v2, "Response"

    const-string v3, "Response (raw)\n  Size: %d\n  Response:\n%s\n"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v1, v2, v3, v4}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325700
    new-instance v1, Lorg/json/JSONTokener;

    invoke-direct {v1, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 2325701
    invoke-virtual {v1}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v1

    .line 2325702
    invoke-static {p1, p2, v1}, LX/GAY;->a(Ljava/net/HttpURLConnection;Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 2325703
    sget-object v2, LX/GAb;->REQUESTS:LX/GAb;

    const-string v3, "Response"

    const-string v4, "Response\n  Id: %s\n  Size: %d\n  Responses:\n%s\n"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 2325704
    iget-object p0, p2, LX/GAX;->e:Ljava/lang/String;

    move-object p0, p0

    .line 2325705
    aput-object p0, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v5, v6

    const/4 v6, 0x2

    aput-object v1, v5, v6

    invoke-static {v2, v3, v4, v5}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325706
    move-object v0, v1

    .line 2325707
    return-object v0
.end method

.method public static a(Ljava/net/HttpURLConnection;LX/GAX;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            "LX/GAX;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/GAY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2325749
    const/4 v1, 0x0

    .line 2325750
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v2, 0x190

    if-lt v0, v2, :cond_0

    .line 2325751
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v1

    .line 2325752
    :goto_0
    invoke-static {v1, p0, p1}, LX/GAY;->a(Ljava/io/InputStream;Ljava/net/HttpURLConnection;LX/GAX;)Ljava/util/List;
    :try_end_0
    .catch LX/GAA; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2325753
    invoke-static {v1}, LX/Gsc;->a(Ljava/io/Closeable;)V

    :goto_1
    return-object v0

    .line 2325754
    :cond_0
    const v0, 0x25d1e600

    :try_start_1
    invoke-static {p0, v0}, LX/04e;->b(Ljava/net/URLConnection;I)Ljava/io/InputStream;
    :try_end_1
    .catch LX/GAA; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 2325755
    :catch_0
    move-exception v0

    .line 2325756
    :try_start_2
    sget-object v2, LX/GAb;->REQUESTS:LX/GAb;

    const-string v3, "Response"

    const-string v4, "Response <Error>: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v4, v5}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325757
    invoke-static {p1, p0, v0}, LX/GAY;->a(Ljava/util/List;Ljava/net/HttpURLConnection;LX/GAA;)Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2325758
    invoke-static {v1}, LX/Gsc;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 2325759
    :catch_1
    move-exception v0

    .line 2325760
    :try_start_3
    sget-object v2, LX/GAb;->REQUESTS:LX/GAb;

    const-string v3, "Response"

    const-string v4, "Response <Error>: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v4, v5}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325761
    new-instance v2, LX/GAA;

    invoke-direct {v2, v0}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    invoke-static {p1, p0, v2}, LX/GAY;->a(Ljava/util/List;Ljava/net/HttpURLConnection;LX/GAA;)Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2325762
    invoke-static {v1}, LX/Gsc;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 2325763
    :catch_2
    move-exception v0

    .line 2325764
    :try_start_4
    sget-object v2, LX/GAb;->REQUESTS:LX/GAb;

    const-string v3, "Response"

    const-string v4, "Response <Error>: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v4, v5}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325765
    new-instance v2, LX/GAA;

    invoke-direct {v2, v0}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    invoke-static {p1, p0, v2}, LX/GAY;->a(Ljava/util/List;Ljava/net/HttpURLConnection;LX/GAA;)Ljava/util/List;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 2325766
    invoke-static {v1}, LX/Gsc;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 2325767
    :catch_3
    move-exception v0

    .line 2325768
    :try_start_5
    sget-object v2, LX/GAb;->REQUESTS:LX/GAb;

    const-string v3, "Response"

    const-string v4, "Response <Error>: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v2, v3, v4, v5}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325769
    new-instance v2, LX/GAA;

    invoke-direct {v2, v0}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    invoke-static {p1, p0, v2}, LX/GAY;->a(Ljava/util/List;Ljava/net/HttpURLConnection;LX/GAA;)Ljava/util/List;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 2325770
    invoke-static {v1}, LX/Gsc;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/Gsc;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public static a(Ljava/net/HttpURLConnection;Ljava/util/List;Ljava/lang/Object;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            "Ljava/util/List",
            "<",
            "LX/GAU;",
            ">;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/GAY;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2325719
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 2325720
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 2325721
    const/4 v0, 0x1

    if-ne v3, v0, :cond_2

    .line 2325722
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    .line 2325723
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 2325724
    const-string v1, "body"

    invoke-virtual {v5, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2325725
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 2325726
    :goto_0
    const-string v6, "code"

    invoke-virtual {v5, v6, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2325727
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 2325728
    invoke-virtual {v1, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2325729
    :goto_1
    instance-of v0, v1, Lorg/json/JSONArray;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 2325730
    :cond_0
    new-instance v0, LX/GAA;

    const-string v1, "Unexpected number of results"

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    .line 2325731
    throw v0

    .line 2325732
    :cond_1
    const/16 v1, 0xc8

    goto :goto_0

    .line 2325733
    :catch_0
    move-exception v1

    .line 2325734
    new-instance v5, LX/GAY;

    new-instance v6, LX/GAF;

    invoke-direct {v6, p0, v1}, LX/GAF;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v5, v0, p0, v6}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;LX/GAF;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, p2

    .line 2325735
    goto :goto_1

    .line 2325736
    :catch_1
    move-exception v1

    .line 2325737
    new-instance v5, LX/GAY;

    new-instance v6, LX/GAF;

    invoke-direct {v6, p0, v1}, LX/GAF;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v5, v0, p0, v6}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;LX/GAF;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object v1, p2

    goto :goto_1

    .line 2325738
    :cond_3
    check-cast v1, Lorg/json/JSONArray;

    .line 2325739
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2325740
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    .line 2325741
    :try_start_1
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 2325742
    invoke-static {v0, p0, v3, p2}, LX/GAY;->a(LX/GAU;Ljava/net/HttpURLConnection;Ljava/lang/Object;Ljava/lang/Object;)LX/GAY;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch LX/GAA; {:try_start_1 .. :try_end_1} :catch_3

    .line 2325743
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2325744
    :catch_2
    move-exception v3

    .line 2325745
    new-instance v5, LX/GAY;

    new-instance v6, LX/GAF;

    invoke-direct {v6, p0, v3}, LX/GAF;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v5, v0, p0, v6}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;LX/GAF;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2325746
    :catch_3
    move-exception v3

    .line 2325747
    new-instance v5, LX/GAY;

    new-instance v6, LX/GAF;

    invoke-direct {v6, p0, v3}, LX/GAF;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v5, v0, p0, v6}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;LX/GAF;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2325748
    :cond_4
    return-object v4
.end method

.method public static a(Ljava/util/List;Ljava/net/HttpURLConnection;LX/GAA;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/GAU;",
            ">;",
            "Ljava/net/HttpURLConnection;",
            "LX/GAA;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/GAY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2325712
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 2325713
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2325714
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2325715
    new-instance v4, LX/GAY;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    new-instance v5, LX/GAF;

    invoke-direct {v5, p1, p2}, LX/GAF;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V

    invoke-direct {v4, v0, p1, v5}, LX/GAY;-><init>(LX/GAU;Ljava/net/HttpURLConnection;LX/GAF;)V

    .line 2325716
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2325717
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2325718
    :cond_0
    return-object v3
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2325708
    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%d"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, LX/GAY;->a:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GAY;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2325709
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "{Response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " responseCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", graphObject: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAY;->b:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAY;->d:LX/GAF;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2325710
    :cond_0
    const/16 v0, 0xc8

    goto :goto_0

    .line 2325711
    :catch_0
    const-string v0, "unknown"

    goto :goto_1
.end method
