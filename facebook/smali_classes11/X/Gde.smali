.class public final LX/Gde;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:LX/GDA;

.field public final synthetic b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GDA;)V
    .locals 0

    .prologue
    .line 2374386
    iput-object p1, p0, LX/Gde;->b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    iput-object p2, p0, LX/Gde;->a:LX/GDA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 2374387
    iget-object v0, p0, LX/Gde;->b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    iget-object v1, p0, LX/Gde;->a:LX/GDA;

    .line 2374388
    iget-object v2, v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->i:LX/1Ck;

    sget-object v3, LX/Gdh;->FETCH_RADIUS:LX/Gdh;

    invoke-virtual {v2, v3}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2374389
    iget-object v2, v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1sS;

    .line 2374390
    sget-object v3, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->d:Lcom/facebook/location/FbLocationOperationParams;

    sget-object v4, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2374391
    invoke-virtual {v2}, LX/0SQ;->isDone()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2374392
    invoke-static {v2}, LX/1v3;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/location/ImmutableLocation;

    .line 2374393
    if-eqz v3, :cond_0

    .line 2374394
    invoke-virtual {v3}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-virtual {v3}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    move-object v3, v0

    move-object v8, v1

    invoke-static/range {v3 .. v8}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->a$redex0(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;DDLX/GDA;)V

    .line 2374395
    :goto_0
    return-void

    .line 2374396
    :cond_0
    iget-object v3, v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->i:LX/1Ck;

    sget-object v4, LX/Gdh;->FETCH_LOCATION:LX/Gdh;

    new-instance v5, LX/Gdf;

    invoke-direct {v5, v0, v1}, LX/Gdf;-><init>(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GDA;)V

    invoke-virtual {v3, v4, v2, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2374397
    iget-object v0, p0, LX/Gde;->a:LX/GDA;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/GDA;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;)V

    .line 2374398
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2374399
    iget-object v0, p0, LX/Gde;->a:LX/GDA;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/GDA;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;)V

    .line 2374400
    return-void
.end method
