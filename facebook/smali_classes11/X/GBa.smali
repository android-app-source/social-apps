.class public final LX/GBa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 0

    .prologue
    .line 2327108
    iput-object p1, p0, LX/GBa;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, 0x5972e62c

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327109
    iget-object v1, p0, LX/GBa;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    sget-object v2, LX/GBg;->EMAIL:LX/GBg;

    .line 2327110
    iput-object v2, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    .line 2327111
    iget-object v1, p0, LX/GBa;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2327112
    iget-object v1, p0, LX/GBa;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2327113
    iget-object v1, p0, LX/GBa;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->r:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const v2, 0x7f0209a0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkDrawable(I)V

    .line 2327114
    iget-object v1, p0, LX/GBa;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2327115
    :cond_0
    const v1, -0x3606ac01

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
