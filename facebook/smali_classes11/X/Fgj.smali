.class public final LX/Fgj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2ii;


# instance fields
.field public final synthetic a:LX/FhA;

.field public final synthetic b:LX/Fgp;


# direct methods
.method public constructor <init>(LX/Fgp;LX/FhA;)V
    .locals 0

    .prologue
    .line 2270811
    iput-object p1, p0, LX/Fgj;->b:LX/Fgp;

    iput-object p2, p0, LX/Fgj;->a:LX/FhA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 12

    .prologue
    .line 2270812
    iget-object v0, p0, LX/Fgj;->a:LX/FhA;

    iget-object v1, p0, LX/Fgj;->b:LX/Fgp;

    iget-object v1, v1, LX/Fgp;->p:LX/0g8;

    iget-object v2, p0, LX/Fgj;->b:LX/Fgp;

    iget-object v2, v2, LX/Fgp;->o:LX/1Qq;

    iget-object v3, p0, LX/Fgj;->b:LX/Fgp;

    iget-object v3, v3, LX/Fgp;->d:LX/Fid;

    const/4 v9, 0x0

    .line 2270813
    if-nez v1, :cond_1

    .line 2270814
    :cond_0
    :goto_0
    return-void

    .line 2270815
    :cond_1
    invoke-interface {v1}, LX/0g8;->t()I

    move-result v4

    sub-int v4, p1, v4

    .line 2270816
    if-ltz v4, :cond_0

    invoke-interface {v2}, LX/1Qq;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 2270817
    invoke-interface {v2, v4}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, LX/1Rk;

    if-eqz v5, :cond_2

    .line 2270818
    invoke-interface {v2, v4}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Rk;

    .line 2270819
    invoke-virtual {v4}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/model/TypeaheadUnit;

    move-object v6, v4

    .line 2270820
    :goto_1
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->L:LX/2Sd;

    sget-object v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->b:Ljava/lang/String;

    sget-object v7, LX/7CQ;->TYPEAHEAD_UNIT_CLICKED:LX/7CQ;

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v7, v8}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 2270821
    invoke-virtual {v6}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2270822
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Cvp;

    const-string v5, "keyword"

    iget-object v7, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v7, v7, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270823
    iget-object v8, v7, LX/7B6;->b:Ljava/lang/String;

    move-object v7, v8

    .line 2270824
    iget-object v8, v3, LX/Fid;->a:Ljava/util/List;

    move-object v8, v8

    .line 2270825
    invoke-static {v8}, Lcom/facebook/search/suggestions/SuggestionsFragment;->b(Ljava/util/List;)LX/0Px;

    move-result-object v8

    iget-object v9, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v9}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v9

    iget-object v10, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2270826
    iget-object v11, v10, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    move-object v10, v11

    .line 2270827
    iget-object v11, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v11, v11, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual/range {v4 .. v11}, LX/Cvp;->a(Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;LX/0Px;LX/CwU;LX/Cwd;Lcom/facebook/search/api/GraphSearchQuery;)V

    goto :goto_0

    .line 2270828
    :cond_2
    invoke-interface {v2, v4}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/model/TypeaheadUnit;

    move-object v6, v4

    goto :goto_1

    .line 2270829
    :cond_3
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->d:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v4, v5}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)Z

    move-result v4

    if-eqz v4, :cond_5

    instance-of v4, v6, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v4, :cond_5

    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v4}, Lcom/facebook/search/suggestions/SuggestionsFragment;->O(Lcom/facebook/search/suggestions/SuggestionsFragment;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2270830
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v4}, Lcom/facebook/search/suggestions/SuggestionsFragment;->K(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2270831
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->d:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    iget-object v6, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v6}, Lcom/facebook/search/suggestions/SuggestionsFragment;->O(Lcom/facebook/search/suggestions/SuggestionsFragment;)Landroid/view/View;

    move-result-object v6

    .line 2270832
    iget-object v7, v4, LX/0x9;->i:Ljava/util/Map;

    invoke-interface {v7, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/FaB;

    .line 2270833
    instance-of v8, v7, LX/Faw;

    if-eqz v8, :cond_4

    .line 2270834
    check-cast v7, LX/Faw;

    .line 2270835
    iput-object v6, v7, LX/Faw;->e:Landroid/view/View;

    .line 2270836
    :cond_4
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->d:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v4, v5}, LX/0x9;->b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    .line 2270837
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const/4 v5, 0x1

    .line 2270838
    iput-boolean v5, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->ag:Z

    .line 2270839
    goto/16 :goto_0

    .line 2270840
    :cond_5
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-boolean v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->af:Z

    if-eqz v4, :cond_6

    .line 2270841
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->c:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v4, v5, v9}, LX/0x9;->b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V

    .line 2270842
    :cond_6
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-boolean v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->ag:Z

    if-eqz v4, :cond_7

    .line 2270843
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->J:LX/0x9;

    sget-object v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->d:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v4, v5, v9}, LX/0x9;->b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V

    .line 2270844
    :cond_7
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->u:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0gh;

    const-string v5, "tap_search_result"

    invoke-virtual {v4, v5}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2270845
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2270846
    iget-object v5, v4, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v4, v5

    .line 2270847
    const/4 v5, 0x0

    .line 2270848
    iput-object v5, v4, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2270849
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->e:LX/Fh8;

    invoke-virtual {v6, v4}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwg;)V

    .line 2270850
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2270851
    iget-object v5, v4, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v4, v5

    .line 2270852
    iget-object v5, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v5, v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->g:LX/F5P;

    .line 2270853
    iput-object v5, v4, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2270854
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v4}, Lcom/facebook/search/suggestions/SuggestionsFragment;->K(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    goto/16 :goto_0
.end method
