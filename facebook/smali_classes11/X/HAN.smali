.class public final LX/HAN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/pages/common/actionchannel/primarybuttons/messagenux/PageNuxStateMutationModels$NotifyPageNuxShownMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3kf;


# direct methods
.method public constructor <init>(LX/3kf;)V
    .locals 0

    .prologue
    .line 2435166
    iput-object p1, p0, LX/HAN;->a:LX/3kf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2435167
    sget-object v0, LX/3kf;->a:Ljava/lang/String;

    const-string v1, "failed to mutate message button nux state"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2435168
    iget-object v0, p0, LX/HAN;->a:LX/3kf;

    iget-object v0, v0, LX/3kf;->d:LX/0Zb;

    const-string v1, "message_button_nux_mutation_failure"

    const-string v2, "user_Id"

    iget-object v3, p0, LX/HAN;->a:LX/3kf;

    iget-object v3, v3, LX/3kf;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2435169
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2435170
    iget-object v0, p0, LX/HAN;->a:LX/3kf;

    iget-object v0, v0, LX/3kf;->d:LX/0Zb;

    const-string v1, "message_button_nux_mutation_success"

    const-string v2, "user_Id"

    iget-object v3, p0, LX/HAN;->a:LX/3kf;

    iget-object v3, v3, LX/3kf;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2435171
    return-void
.end method
