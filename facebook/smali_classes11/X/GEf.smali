.class public LX/GEf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GIz;

.field private b:LX/0ad;


# direct methods
.method public constructor <init>(LX/GIz;LX/0ad;)V
    .locals 0

    .prologue
    .line 2331963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331964
    iput-object p1, p0, LX/GEf;->a:LX/GIz;

    .line 2331965
    iput-object p2, p0, LX/GEf;->b:LX/0ad;

    .line 2331966
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2331969
    const v0, 0x7f03005c

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2331970
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2331971
    :cond_0
    :goto_0
    return v0

    .line 2331972
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v2

    .line 2331973
    sget-object v3, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v2, v3, :cond_2

    sget-object v3, LX/GGB;->PENDING:LX/GGB;

    if-ne v2, v3, :cond_3

    .line 2331974
    :cond_2
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v2, v3, :cond_0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2331975
    goto :goto_0

    .line 2331976
    :cond_3
    sget-object v3, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v2, v3, :cond_4

    sget-object v3, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v2, v3, :cond_4

    sget-object v3, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v2, v3, :cond_4

    sget-object v3, LX/GGB;->PAUSED:LX/GGB;

    if-ne v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2331968
    iget-object v0, p0, LX/GEf;->a:LX/GIz;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2331967
    sget-object v0, LX/8wK;->FOOTER:LX/8wK;

    return-object v0
.end method
