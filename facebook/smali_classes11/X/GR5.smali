.class public LX/GR5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2351304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351305
    iput-object p1, p0, LX/GR5;->a:Landroid/net/Uri;

    .line 2351306
    return-void
.end method

.method public static a(Landroid/net/Uri;)LX/GR5;
    .locals 3

    .prologue
    .line 2351307
    new-instance v0, LX/GR5;

    invoke-direct {v0, p0}, LX/GR5;-><init>(Landroid/net/Uri;)V

    .line 2351308
    const-string v1, "android-app"

    iget-object v2, v0, LX/GR5;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2351309
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'android-app://\' schema is required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2351310
    :cond_0
    iget-object v1, v0, LX/GR5;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2351311
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "package name cannot be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2351312
    :cond_1
    return-object v0
.end method
