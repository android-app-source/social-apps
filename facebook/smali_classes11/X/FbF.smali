.class public abstract LX/FbF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FbA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/FbA",
        "<",
        "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2259713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 2259719
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v6

    .line 2259720
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v15

    .line 2259721
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2259722
    const/4 v3, 0x0

    .line 2259723
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->m()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v1, 0x0

    move v5, v1

    :goto_0
    if-ge v5, v8, :cond_4

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;

    .line 2259724
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1, v2}, LX/FbF;->b(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v2, 0x0

    move v4, v2

    move v2, v3

    :goto_1
    if-ge v4, v10, :cond_3

    invoke-virtual {v9, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    .line 2259725
    if-eqz v11, :cond_1

    .line 2259726
    add-int/lit8 v3, v2, 0x1

    .line 2259727
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, ""

    :goto_2
    invoke-virtual {v6, v11, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2259728
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2259729
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v2

    invoke-virtual {v15, v11, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    :cond_0
    move v2, v3

    .line 2259730
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2259731
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2259732
    :cond_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v3, v2

    goto :goto_0

    .line 2259733
    :cond_4
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v18

    .line 2259734
    invoke-virtual/range {v18 .. v18}, LX/0P1;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2259735
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v1

    .line 2259736
    :goto_3
    return-object v1

    .line 2259737
    :cond_5
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2259738
    invoke-virtual/range {v18 .. v18}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2259739
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/FbF;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2259740
    if-eqz v2, :cond_6

    .line 2259741
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2259742
    :cond_7
    invoke-virtual/range {v18 .. v18}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->asList()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/FbF;->b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 2259743
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->o()LX/8dH;

    move-result-object v13

    .line 2259744
    invoke-static {v13}, LX/D0M;->a(LX/8dH;)Ljava/lang/String;

    move-result-object v9

    .line 2259745
    invoke-static {v13}, LX/D0M;->b(LX/8dH;)Ljava/lang/String;

    move-result-object v10

    .line 2259746
    invoke-static {v13}, LX/D0M;->c(LX/8dH;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v11

    .line 2259747
    new-instance v1, LX/D0L;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->fD_()Z

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->fE_()Z

    move-result v6

    invoke-direct/range {v1 .. v6}, LX/D0L;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 2259748
    invoke-static {v13}, LX/D0M;->d(LX/8dH;)LX/0Px;

    move-result-object v12

    .line 2259749
    invoke-static {v13}, LX/D0M;->e(LX/8dH;)LX/0Px;

    move-result-object v16

    .line 2259750
    new-instance v2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-static/range {p1 .. p1}, LX/Fbf;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->b()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15}, LX/0P2;->b()LX/0P1;

    move-result-object v17

    move-object/from16 v6, v18

    move-object v15, v1

    invoke-direct/range {v2 .. v17}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;LX/0P1;LX/0Px;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;Ljava/lang/Integer;Ljava/lang/String;LX/D0L;LX/0Px;LX/0P1;)V

    .line 2259751
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->a(Ljava/lang/String;)V

    .line 2259752
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    goto/16 :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)LX/0Px;
    .locals 1

    .prologue
    .line 2259718
    check-cast p1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;

    invoke-virtual {p0, p1}, LX/FbF;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$ModuleResultEdge;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public b(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)LX/0Px;
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$ModuleResultEdge;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2259714
    invoke-virtual {p0, p1, p2, p3}, LX/FbF;->a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2259715
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2259716
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2259717
    goto :goto_0
.end method

.method public abstract b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
