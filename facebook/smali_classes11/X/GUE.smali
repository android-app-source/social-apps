.class public final LX/GUE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2357256
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2357257
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2357258
    :goto_0
    return v1

    .line 2357259
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 2357260
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2357261
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2357262
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 2357263
    const-string v8, "is_currently_selected"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2357264
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v6, v3

    move v3, v2

    goto :goto_1

    .line 2357265
    :cond_1
    const-string v8, "is_previous_selection"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2357266
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 2357267
    :cond_2
    const-string v8, "node"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2357268
    invoke-static {p0, p1}, LX/GUF;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2357269
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2357270
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2357271
    if-eqz v3, :cond_5

    .line 2357272
    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 2357273
    :cond_5
    if-eqz v0, :cond_6

    .line 2357274
    invoke-virtual {p1, v2, v5}, LX/186;->a(IZ)V

    .line 2357275
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2357276
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2357241
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2357242
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2357243
    if-eqz v0, :cond_0

    .line 2357244
    const-string v1, "is_currently_selected"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2357245
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2357246
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2357247
    if-eqz v0, :cond_1

    .line 2357248
    const-string v1, "is_previous_selection"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2357249
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2357250
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2357251
    if-eqz v0, :cond_2

    .line 2357252
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2357253
    invoke-static {p0, v0, p2, p3}, LX/GUF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2357254
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2357255
    return-void
.end method
