.class public LX/GpY;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/CompositeBlockView;",
        "Lcom/facebook/instantshopping/model/data/CompositeBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/CjL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Cs4;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/widget/LinearLayout;

.field private final h:LX/CjK;


# direct methods
.method public constructor <init>(LX/Gq7;)V
    .locals 2

    .prologue
    .line 2395079
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395080
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395081
    check-cast v0, LX/Gq7;

    invoke-interface {v0}, LX/CnG;->c()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d1827

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/GpY;->g:Landroid/widget/LinearLayout;

    .line 2395082
    const-class v0, LX/GpY;

    invoke-static {v0, p0}, LX/GpY;->a(Ljava/lang/Class;LX/02k;)V

    .line 2395083
    iget-object v0, p0, LX/GpY;->d:LX/CjL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CjL;->a(LX/0Pq;)LX/CjK;

    move-result-object v0

    iput-object v0, p0, LX/GpY;->h:LX/CjK;

    .line 2395084
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 2395146
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    .line 2395147
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2395148
    :goto_0
    move v1, v1

    .line 2395149
    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    return v0

    :catch_0
    goto :goto_0
.end method

.method private a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;LX/Cs4;)V
    .locals 7

    .prologue
    const/4 v1, -0x2

    .line 2395120
    invoke-interface {p1}, LX/CHb;->c()LX/CHa;

    move-result-object v5

    .line 2395121
    if-eqz v5, :cond_1

    .line 2395122
    iget-object v6, p2, LX/1a1;->a:Landroid/view/View;

    .line 2395123
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2395124
    if-nez v0, :cond_0

    .line 2395125
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 2395126
    :cond_0
    invoke-interface {v5}, LX/CHa;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2395127
    invoke-interface {v5}, LX/CHa;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LX/GpY;->a(Ljava/lang/String;)I

    move-result v1

    .line 2395128
    :goto_0
    invoke-interface {v5}, LX/CHa;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2395129
    invoke-interface {v5}, LX/CHa;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, LX/GpY;->a(Ljava/lang/String;)I

    move-result v2

    .line 2395130
    :goto_1
    invoke-interface {v5}, LX/CHa;->e()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 2395131
    invoke-interface {v5}, LX/CHa;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, LX/GpY;->a(Ljava/lang/String;)I

    move-result v3

    .line 2395132
    :goto_2
    invoke-interface {v5}, LX/CHa;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2395133
    invoke-interface {v5}, LX/CHa;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, LX/GpY;->a(Ljava/lang/String;)I

    move-result v4

    .line 2395134
    :goto_3
    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2395135
    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2395136
    invoke-interface {v5}, LX/CHa;->a()Ljava/lang/String;

    move-result-object v0

    .line 2395137
    if-nez v0, :cond_6

    .line 2395138
    :cond_1
    :goto_4
    return-void

    .line 2395139
    :cond_2
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_0

    .line 2395140
    :cond_3
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto :goto_1

    .line 2395141
    :cond_4
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_2

    .line 2395142
    :cond_5
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_3

    .line 2395143
    :cond_6
    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2395144
    :goto_5
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v6, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4

    .line 2395145
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/GpY;

    const-class p0, LX/CjL;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/CjL;

    iput-object v1, p1, LX/GpY;->d:LX/CjL;

    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 8

    .prologue
    .line 2395085
    check-cast p1, LX/Goy;

    const/4 v1, 0x0

    .line 2395086
    invoke-interface {p1}, LX/Goa;->C()I

    move-result v0

    iput v0, p0, LX/GpY;->e:I

    .line 2395087
    invoke-virtual {p1}, LX/Goy;->a()LX/0Px;

    move-result-object v3

    .line 2395088
    iget-object v0, p0, LX/GpY;->f:Ljava/util/List;

    if-nez v0, :cond_1

    .line 2395089
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GpY;->f:Ljava/util/List;

    .line 2395090
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;

    .line 2395091
    sget-object v5, LX/GpX;->a:[I

    invoke-interface {v0}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2395092
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2395093
    :pswitch_0
    iget-object v5, p0, LX/GpY;->h:LX/CjK;

    const/16 v6, 0x72

    iget-object v7, p0, LX/GpY;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6, v7}, LX/CjK;->a(ILandroid/view/ViewGroup;)LX/Cs4;

    move-result-object v5

    .line 2395094
    iget-object v6, p0, LX/GpY;->f:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2395095
    invoke-direct {p0, v0, v5}, LX/GpY;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;LX/Cs4;)V

    goto :goto_1

    .line 2395096
    :pswitch_1
    iget-object v5, p0, LX/GpY;->h:LX/CjK;

    const/16 v6, 0x70

    iget-object v7, p0, LX/GpY;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6, v7}, LX/CjK;->a(ILandroid/view/ViewGroup;)LX/Cs4;

    move-result-object v5

    .line 2395097
    invoke-direct {p0, v0, v5}, LX/GpY;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;LX/Cs4;)V

    .line 2395098
    iget-object v0, p0, LX/GpY;->f:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2395099
    :pswitch_2
    invoke-interface {v0}, LX/CHb;->e()I

    move-result v5

    if-lez v5, :cond_0

    invoke-interface {v0}, LX/CHb;->e()I

    move-result v5

    const/16 v6, 0x64

    if-ge v5, v6, :cond_0

    .line 2395100
    iget-object v5, p0, LX/GpY;->h:LX/CjK;

    const/16 v6, 0x6f

    iget-object v7, p0, LX/GpY;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v6, v7}, LX/CjK;->a(ILandroid/view/ViewGroup;)LX/Cs4;

    move-result-object v5

    .line 2395101
    invoke-direct {p0, v0, v5}, LX/GpY;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;LX/Cs4;)V

    .line 2395102
    iget-object v0, p0, LX/GpY;->f:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2395103
    :cond_1
    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2395104
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;

    iget v2, p0, LX/GpY;->e:I

    .line 2395105
    sget-object v4, LX/GpX;->a:[I

    invoke-interface {v0}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 2395106
    const/4 v4, 0x0

    :goto_3
    move-object v2, v4

    .line 2395107
    if-eqz v2, :cond_3

    .line 2395108
    iget-object v0, p0, LX/GpY;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2395109
    iget-object v0, p0, LX/GpY;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 2395110
    :cond_2
    iget-object v4, p0, LX/GpY;->g:Landroid/widget/LinearLayout;

    iget-object v0, p0, LX/GpY;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cs4;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v4, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 2395111
    iget-object v0, p0, LX/GpY;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cs4;

    invoke-virtual {v0, v2}, LX/Cs4;->a(LX/Clr;)V

    .line 2395112
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2395113
    :cond_4
    return-void

    .line 2395114
    :pswitch_3
    new-instance v4, LX/GpI;

    invoke-interface {v0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/GoK;->b(LX/0Px;)Z

    move-result v5

    invoke-direct {v4, v0, v2, v5}, LX/GpI;-><init>(LX/CHZ;IZ)V

    goto :goto_3

    .line 2395115
    :pswitch_4
    new-instance v4, LX/Gp8;

    const/16 v5, 0x72

    invoke-direct {v4, v0, v2, v5}, LX/Gp8;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;II)V

    const/4 v5, 0x0

    .line 2395116
    iput-boolean v5, v4, LX/Gos;->l:Z

    .line 2395117
    move-object v4, v4

    .line 2395118
    invoke-virtual {v4}, LX/Cm7;->b()LX/Clr;

    move-result-object v4

    goto :goto_3

    .line 2395119
    :pswitch_5
    new-instance v4, LX/Gox;

    const/16 v5, 0x6f

    invoke-interface {v0}, LX/CHb;->e()I

    move-result v6

    invoke-direct {v4, v0, v5, v6}, LX/Gox;-><init>(LX/CHe;II)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
