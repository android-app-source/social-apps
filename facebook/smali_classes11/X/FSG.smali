.class public final enum LX/FSG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FSG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FSG;

.field public static final enum APP_NOT_IN_BACKGROUND:LX/FSG;

.field public static final enum NOT_CONNECTED:LX/FSG;

.field public static final enum NOT_LOGGED_IN:LX/FSG;

.field public static final enum SUCCESS:LX/FSG;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2241719
    new-instance v0, LX/FSG;

    const-string v1, "SUCCESS"

    const-string v2, "success"

    invoke-direct {v0, v1, v3, v2}, LX/FSG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FSG;->SUCCESS:LX/FSG;

    .line 2241720
    new-instance v0, LX/FSG;

    const-string v1, "NOT_LOGGED_IN"

    const-string v2, "not_logged_in"

    invoke-direct {v0, v1, v4, v2}, LX/FSG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FSG;->NOT_LOGGED_IN:LX/FSG;

    .line 2241721
    new-instance v0, LX/FSG;

    const-string v1, "APP_NOT_IN_BACKGROUND"

    const-string v2, "app_not_in_background"

    invoke-direct {v0, v1, v5, v2}, LX/FSG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FSG;->APP_NOT_IN_BACKGROUND:LX/FSG;

    .line 2241722
    new-instance v0, LX/FSG;

    const-string v1, "NOT_CONNECTED"

    const-string v2, "network_not_connected"

    invoke-direct {v0, v1, v6, v2}, LX/FSG;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FSG;->NOT_CONNECTED:LX/FSG;

    .line 2241723
    const/4 v0, 0x4

    new-array v0, v0, [LX/FSG;

    sget-object v1, LX/FSG;->SUCCESS:LX/FSG;

    aput-object v1, v0, v3

    sget-object v1, LX/FSG;->NOT_LOGGED_IN:LX/FSG;

    aput-object v1, v0, v4

    sget-object v1, LX/FSG;->APP_NOT_IN_BACKGROUND:LX/FSG;

    aput-object v1, v0, v5

    sget-object v1, LX/FSG;->NOT_CONNECTED:LX/FSG;

    aput-object v1, v0, v6

    sput-object v0, LX/FSG;->$VALUES:[LX/FSG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2241715
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2241716
    iput-object p3, p0, LX/FSG;->mName:Ljava/lang/String;

    .line 2241717
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FSG;
    .locals 1

    .prologue
    .line 2241718
    const-class v0, LX/FSG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FSG;

    return-object v0
.end method

.method public static values()[LX/FSG;
    .locals 1

    .prologue
    .line 2241713
    sget-object v0, LX/FSG;->$VALUES:[LX/FSG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FSG;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2241714
    iget-object v0, p0, LX/FSG;->mName:Ljava/lang/String;

    return-object v0
.end method
