.class public LX/GIt;
.super LX/GIs;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GIs",
        "<",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/GIN;LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337352
    invoke-direct/range {p0 .. p6}, LX/GIs;-><init>(LX/GIN;LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V

    .line 2337353
    return-void
.end method

.method public static d(LX/0QB;)LX/GIt;
    .locals 7

    .prologue
    .line 2337354
    new-instance v0, LX/GIt;

    invoke-static {p0}, LX/GIN;->b(LX/0QB;)LX/GIN;

    move-result-object v1

    check-cast v1, LX/GIN;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v2

    check-cast v2, LX/GG6;

    invoke-static {p0}, LX/GL2;->b(LX/0QB;)LX/GL2;

    move-result-object v3

    check-cast v3, LX/GL2;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v4

    check-cast v4, LX/GKy;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v5

    check-cast v5, LX/2U3;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    invoke-direct/range {v0 .. v6}, LX/GIt;-><init>(LX/GIN;LX/GG6;LX/GL2;LX/GKy;LX/2U3;LX/0W9;)V

    .line 2337355
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/GIa;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337356
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337357
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    invoke-virtual {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 1

    .prologue
    .line 2337358
    invoke-super {p0, p1, p2}, LX/GIs;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337359
    const/4 v0, 0x0

    .line 2337360
    if-nez v0, :cond_0

    .line 2337361
    iget-object p1, p0, LX/GIr;->f:LX/GIa;

    invoke-virtual {p1}, LX/GIa;->getAudienceOptionsView()Lcom/facebook/widget/CustomLinearLayout;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2337362
    :goto_0
    return-void

    .line 2337363
    :cond_0
    iget-object p1, p0, LX/GIr;->f:LX/GIa;

    invoke-virtual {p1}, LX/GIa;->getAudienceOptionsView()Lcom/facebook/widget/CustomLinearLayout;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 2337364
    invoke-super {p0, p1}, LX/GIs;->b(Z)V

    .line 2337365
    iget-object v0, p0, LX/GIr;->f:LX/GIa;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/GIa;->setLocationSelectorDividerVisibility(I)V

    .line 2337366
    return-void
.end method
