.class public LX/Fho;
.super LX/2SP;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile o:LX/Fho;


# instance fields
.field public final a:LX/03V;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fhz;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0ad;

.field public final e:Landroid/os/Handler;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0SG;

.field public final h:J

.field public final i:J

.field private j:LX/Cwv;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cwv;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:LX/2SR;

.field private m:LX/0h9;

.field public n:Z


# direct methods
.method public constructor <init>(LX/03V;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0ad;Landroid/os/Handler;LX/0SG;)V
    .locals 3
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/Fhz;",
            ">;",
            "LX/0ad;",
            "Landroid/os/Handler;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0xe10

    .line 2273795
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 2273796
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fho;->n:Z

    .line 2273797
    iput-object p1, p0, LX/Fho;->a:LX/03V;

    .line 2273798
    iput-object p2, p0, LX/Fho;->b:Ljava/util/concurrent/ExecutorService;

    .line 2273799
    iput-object p3, p0, LX/Fho;->c:LX/0Ot;

    .line 2273800
    iput-object p4, p0, LX/Fho;->d:LX/0ad;

    .line 2273801
    iput-object p5, p0, LX/Fho;->e:Landroid/os/Handler;

    .line 2273802
    const-string v0, "dummy_source"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Fho;->f:Ljava/util/List;

    .line 2273803
    iput-object p6, p0, LX/Fho;->g:LX/0SG;

    .line 2273804
    iget-object v0, p0, LX/Fho;->d:LX/0ad;

    sget v1, LX/100;->au:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, LX/Fho;->h:J

    .line 2273805
    iget-object v0, p0, LX/Fho;->d:LX/0ad;

    sget v1, LX/100;->at:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, LX/Fho;->i:J

    .line 2273806
    return-void
.end method

.method public static a(LX/0QB;)LX/Fho;
    .locals 10

    .prologue
    .line 2273782
    sget-object v0, LX/Fho;->o:LX/Fho;

    if-nez v0, :cond_1

    .line 2273783
    const-class v1, LX/Fho;

    monitor-enter v1

    .line 2273784
    :try_start_0
    sget-object v0, LX/Fho;->o:LX/Fho;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2273785
    if-eqz v2, :cond_0

    .line 2273786
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2273787
    new-instance v3, LX/Fho;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    const/16 v6, 0x34ce

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-direct/range {v3 .. v9}, LX/Fho;-><init>(LX/03V;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0ad;Landroid/os/Handler;LX/0SG;)V

    .line 2273788
    move-object v0, v3

    .line 2273789
    sput-object v0, LX/Fho;->o:LX/Fho;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273790
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2273791
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2273792
    :cond_1
    sget-object v0, LX/Fho;->o:LX/Fho;

    return-object v0

    .line 2273793
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2273794
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/Fho;LX/Cwv;)V
    .locals 3

    .prologue
    .line 2273770
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, LX/Cwv;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2273771
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fho;->n:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2273772
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 2273773
    :cond_2
    :try_start_1
    iput-object p1, p0, LX/Fho;->j:LX/Cwv;

    .line 2273774
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fho;->n:Z

    .line 2273775
    iget-object v0, p0, LX/Fho;->d:LX/0ad;

    sget-short v1, LX/100;->aQ:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Fho;->m:LX/0h9;

    if-eqz v0, :cond_3

    .line 2273776
    iget-object v0, p0, LX/Fho;->m:LX/0h9;

    iget-object v1, p0, LX/Fho;->j:LX/Cwv;

    .line 2273777
    iget v2, v1, LX/Cwv;->c:I

    move v1, v2

    .line 2273778
    invoke-interface {v0, v1}, LX/0h9;->D_(I)V

    .line 2273779
    :cond_3
    iget-object v0, p0, LX/Fho;->l:LX/2SR;

    if-eqz v0, :cond_1

    .line 2273780
    iget-object v0, p0, LX/Fho;->l:LX/2SR;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-interface {v0, v1}, LX/2SR;->a(LX/7BE;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2273781
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/Fho;Ljava/util/List;IZLcom/facebook/common/callercontext/CallerContext;LX/0zS;)V
    .locals 7
    .param p3    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZ",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0zS;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2273760
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fho;->k:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 2273761
    :goto_0
    monitor-exit p0

    return-void

    .line 2273762
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Fho;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fhz;

    .line 2273763
    if-nez p4, :cond_1

    iget-object v1, p0, LX/Fho;->d:LX/0ad;

    sget-short v2, LX/100;->aQ:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2273764
    iget-object v1, p0, LX/Fho;->d:LX/0ad;

    sget-char v2, LX/100;->aP:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2273765
    :goto_1
    move-object v3, v1

    .line 2273766
    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, LX/Fhz;->a(Ljava/util/List;ILjava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Fho;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2273767
    new-instance v0, LX/Fhn;

    invoke-direct {v0, p0, p4}, LX/Fhn;-><init>(LX/Fho;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2273768
    iget-object v1, p0, LX/Fho;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/Fho;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2273769
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    const-string v1, ""

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()LX/7BE;
    .locals 6

    .prologue
    .line 2273752
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fho;->j:LX/Cwv;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Fho;->j:LX/Cwv;

    invoke-virtual {v0}, LX/Cwv;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2273753
    iget-object v0, p0, LX/Fho;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/Fho;->j:LX/Cwv;

    .line 2273754
    iget-wide v4, v2, LX/Cwv;->b:J

    move-wide v2, v4

    .line 2273755
    sub-long/2addr v0, v2

    iget-wide v2, p0, LX/Fho;->h:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    sget-object v0, LX/7BE;->READY:LX/7BE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2273756
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2273757
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Fho;->d:LX/0ad;

    sget-short v1, LX/100;->av:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/7BE;->STALE_READY:LX/7BE;

    goto :goto_0

    :cond_1
    sget-object v0, LX/7BE;->NOT_READY:LX/7BE;

    goto :goto_0

    .line 2273758
    :cond_2
    sget-object v0, LX/7BE;->NOT_READY:LX/7BE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2273759
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 0

    .prologue
    .line 2273750
    iput-object p1, p0, LX/Fho;->l:LX/2SR;

    .line 2273751
    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 6
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2273744
    monitor-enter p0

    :try_start_0
    invoke-static {p2}, LX/2SP;->a(LX/EPu;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v5, LX/0zS;->d:LX/0zS;

    .line 2273745
    :goto_0
    iget-object v1, p0, LX/Fho;->f:Ljava/util/List;

    const/16 v2, 0xa

    sget-object v0, LX/EPu;->NETWORK_BYPASS_CACHE_ONLY:LX/EPu;

    if-ne p2, v0, :cond_1

    const/4 v3, 0x1

    :goto_1
    move-object v0, p0

    move-object v4, p1

    invoke-static/range {v0 .. v5}, LX/Fho;->a$redex0(LX/Fho;Ljava/util/List;IZLcom/facebook/common/callercontext/CallerContext;LX/0zS;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2273746
    monitor-exit p0

    return-void

    .line 2273747
    :cond_0
    :try_start_1
    sget-object v5, LX/0zS;->a:LX/0zS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2273748
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 2273749
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2273739
    const/4 v0, 0x1

    return v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 2273741
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/Fho;->j:LX/Cwv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2273742
    monitor-exit p0

    return-void

    .line 2273743
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2273740
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fho;->j:LX/Cwv;

    invoke-virtual {v0}, LX/Cwv;->b()LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
