.class public LX/FmH;
.super LX/6E8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6E8",
        "<",
        "Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;",
        "LX/FmD;",
        ">;"
    }
.end annotation


# instance fields
.field private l:LX/6qh;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;)V
    .locals 0

    .prologue
    .line 2282296
    invoke-direct {p0, p1}, LX/6E8;-><init>(LX/6E6;)V

    .line 2282297
    return-void
.end method

.method private a(LX/FmD;)V
    .locals 2

    .prologue
    .line 2282298
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    .line 2282299
    iget-object v1, p0, LX/FmH;->l:LX/6qh;

    invoke-virtual {v0, v1}, LX/6E7;->setPaymentsComponentCallback(LX/6qh;)V

    .line 2282300
    iget-object v1, p1, LX/FmD;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v1

    .line 2282301
    invoke-virtual {v0, v1}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->setPrivacyData(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 2282302
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/6E2;)V
    .locals 0

    .prologue
    .line 2282303
    check-cast p1, LX/FmD;

    invoke-direct {p0, p1}, LX/FmH;->a(LX/FmD;)V

    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 2282304
    iput-object p1, p0, LX/FmH;->l:LX/6qh;

    .line 2282305
    return-void
.end method
