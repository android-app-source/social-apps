.class public LX/Ffi;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Ffi;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268806
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v1, 0x7f082319

    invoke-direct {p0, p1, v0, v1}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268807
    return-void
.end method

.method public static a(LX/0QB;)LX/Ffi;
    .locals 5

    .prologue
    .line 2268785
    sget-object v0, LX/Ffi;->a:LX/Ffi;

    if-nez v0, :cond_1

    .line 2268786
    const-class v1, LX/Ffi;

    monitor-enter v1

    .line 2268787
    :try_start_0
    sget-object v0, LX/Ffi;->a:LX/Ffi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268788
    if-eqz v2, :cond_0

    .line 2268789
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268790
    new-instance p0, LX/Ffi;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/Ffi;-><init>(Landroid/content/res/Resources;LX/0ad;)V

    .line 2268791
    move-object v0, p0

    .line 2268792
    sput-object v0, LX/Ffi;->a:LX/Ffi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268793
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268794
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268795
    :cond_1
    sget-object v0, LX/Ffi;->a:LX/Ffi;

    return-object v0

    .line 2268796
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CwB;Ljava/lang/String;)LX/CwH;
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268802
    invoke-virtual {p0, p1, p2}, LX/FfQ;->b(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v0

    sget-object v1, LX/CwF;->local:LX/CwF;

    .line 2268803
    iput-object v1, v0, LX/CwH;->g:LX/CwF;

    .line 2268804
    move-object v0, v0

    .line 2268805
    return-object v0
.end method

.method public final b()LX/Fdv;
    .locals 2

    .prologue
    .line 2268799
    new-instance v0, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/reaction/SearchResultsReactionFragment;-><init>()V

    .line 2268800
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2268801
    return-object v0
.end method

.method public final synthetic c(LX/CwB;Ljava/lang/String;)LX/CwA;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268798
    invoke-virtual {p0, p1, p2}, LX/FfQ;->a(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v0

    return-object v0
.end method
