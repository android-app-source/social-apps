.class public LX/FWX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/saved/launcher/SavedActivityLauncherWithResult",
        "<",
        "LX/FVF;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1Kf;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/1Ck;

.field public final d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field public final e:LX/0kL;

.field public final f:LX/BNE;

.field public final g:LX/FW1;

.field private final h:LX/1nC;


# direct methods
.method public constructor <init>(LX/1Kf;Landroid/content/res/Resources;LX/1Ck;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0kL;LX/BNE;LX/FW1;LX/1nC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252237
    iput-object p1, p0, LX/FWX;->a:LX/1Kf;

    .line 2252238
    iput-object p2, p0, LX/FWX;->b:Landroid/content/res/Resources;

    .line 2252239
    iput-object p3, p0, LX/FWX;->c:LX/1Ck;

    .line 2252240
    iput-object p4, p0, LX/FWX;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2252241
    iput-object p5, p0, LX/FWX;->e:LX/0kL;

    .line 2252242
    iput-object p6, p0, LX/FWX;->f:LX/BNE;

    .line 2252243
    iput-object p7, p0, LX/FWX;->g:LX/FW1;

    .line 2252244
    iput-object p8, p0, LX/FWX;->h:LX/1nC;

    .line 2252245
    return-void
.end method

.method public static a$redex0(LX/FWX;LX/FVF;Landroid/app/Activity;LX/55r;)V
    .locals 10
    .param p2    # Landroid/app/Activity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 2252246
    invoke-static {p3}, LX/BNJ;->a(LX/55r;)I

    move-result v9

    .line 2252247
    sget-object v1, LX/21D;->SAVED_STORIES_DASHBOARD:LX/21D;

    const-string v2, "saved_dashboard"

    if-eqz v9, :cond_1

    move v3, v0

    :goto_0
    invoke-interface {p1}, LX/FVE;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-interface {p1}, LX/FVF;->e()Ljava/lang/String;

    move-result-object v6

    const-string v7, "review_button"

    const-string v8, "native_saved_dashboard"

    invoke-static/range {v1 .. v8}, LX/1nC;->a(LX/21D;Ljava/lang/String;ZJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    invoke-static {}, LX/BN7;->c()LX/BN7;

    move-result-object v2

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setHideKeyboardIfReachedMinimumHeight(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialRating(I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {p3}, LX/BNJ;->c(LX/55r;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2252248
    invoke-static {p3}, LX/BNJ;->b(LX/55r;)Ljava/lang/String;

    move-result-object v1

    .line 2252249
    if-eqz v1, :cond_0

    .line 2252250
    invoke-static {v1}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2252251
    :cond_0
    iget-object v1, p0, LX/FWX;->a:LX/1Kf;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    sget-object v3, LX/79w;->REVIEW_ITEM:LX/79w;

    invoke-virtual {v3}, LX/79w;->ordinal()I

    move-result v3

    invoke-interface {v1, v2, v0, v3, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2252252
    return-void

    .line 2252253
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/FWX;
    .locals 9

    .prologue
    .line 2252254
    new-instance v0, LX/FWX;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {p0}, LX/BNE;->a(LX/0QB;)LX/BNE;

    move-result-object v6

    check-cast v6, LX/BNE;

    invoke-static {p0}, LX/FW1;->a(LX/0QB;)LX/FW1;

    move-result-object v7

    check-cast v7, LX/FW1;

    invoke-static {p0}, LX/1nC;->b(LX/0QB;)LX/1nC;

    move-result-object v8

    check-cast v8, LX/1nC;

    invoke-direct/range {v0 .. v8}, LX/FWX;-><init>(LX/1Kf;Landroid/content/res/Resources;LX/1Ck;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0kL;LX/BNE;LX/FW1;LX/1nC;)V

    .line 2252255
    return-object v0
.end method
