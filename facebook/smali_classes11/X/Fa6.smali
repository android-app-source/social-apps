.class public LX/Fa6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/Fa6;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CwV;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/CwV;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/FZz;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2258379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258380
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Fa6;->a:Ljava/util/List;

    .line 2258381
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Fa6;->b:Ljava/util/Set;

    .line 2258382
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Fa6;->c:Ljava/util/Set;

    .line 2258383
    return-void
.end method

.method public static a(LX/0QB;)LX/Fa6;
    .locals 3

    .prologue
    .line 2258384
    sget-object v0, LX/Fa6;->e:LX/Fa6;

    if-nez v0, :cond_1

    .line 2258385
    const-class v1, LX/Fa6;

    monitor-enter v1

    .line 2258386
    :try_start_0
    sget-object v0, LX/Fa6;->e:LX/Fa6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2258387
    if-eqz v2, :cond_0

    .line 2258388
    :try_start_1
    new-instance v0, LX/Fa6;

    invoke-direct {v0}, LX/Fa6;-><init>()V

    .line 2258389
    move-object v0, v0

    .line 2258390
    sput-object v0, LX/Fa6;->e:LX/Fa6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2258391
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2258392
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2258393
    :cond_1
    sget-object v0, LX/Fa6;->e:LX/Fa6;

    return-object v0

    .line 2258394
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2258395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 2258378
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)LX/CwV;
    .locals 1

    .prologue
    .line 2258377
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwV;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/CwV;)LX/CwV;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2258396
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 2258397
    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    .line 2258398
    const/4 v0, 0x0

    .line 2258399
    :goto_0
    monitor-exit p0

    return-object v0

    .line 2258400
    :cond_0
    :try_start_1
    new-instance v2, LX/CwV;

    iget-object v3, p1, LX/CwV;->a:Ljava/lang/String;

    iget-object v4, p1, LX/CwV;->b:Ljava/lang/String;

    iget-boolean v0, p1, LX/CwV;->d:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-boolean v5, p1, LX/CwV;->c:Z

    invoke-direct {v2, v3, v4, v0, v5}, LX/CwV;-><init>(Ljava/lang/String;Ljava/lang/String;ZZ)V

    move-object v0, v2

    .line 2258401
    iget-object v2, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v2, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2258402
    iget-object v1, p0, LX/Fa6;->c:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2258403
    iget-object v1, p0, LX/Fa6;->c:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2258404
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2258405
    :cond_1
    :try_start_2
    iget-object v1, p0, LX/Fa6;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CwV;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2258368
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 2258369
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwV;

    .line 2258370
    iget-object v4, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2258371
    iget-object v4, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2258372
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2258373
    :cond_1
    iget-object v0, p0, LX/Fa6;->d:LX/FZz;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2258374
    iget-object v0, p0, LX/Fa6;->d:LX/FZz;

    invoke-virtual {v0, v2}, LX/FZz;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2258375
    :cond_2
    monitor-exit p0

    return-void

    .line 2258376
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(LX/0Px;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/CwV;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2258364
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fa6;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2258365
    invoke-virtual {p0, p2}, LX/Fa6;->a(LX/0Px;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2258366
    monitor-exit p0

    return-void

    .line 2258367
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2258357
    iget-object v0, p0, LX/Fa6;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2258358
    return-void
.end method

.method public final declared-synchronized clearUserData()V
    .locals 1

    .prologue
    .line 2258359
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fa6;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2258360
    iget-object v0, p0, LX/Fa6;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2258361
    invoke-virtual {p0}, LX/Fa6;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2258362
    monitor-exit p0

    return-void

    .line 2258363
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
