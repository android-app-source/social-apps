.class public LX/GwD;
.super LX/4hY;
.source ""


# instance fields
.field public a:Z

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private final d:LX/1Kf;

.field private final e:LX/1nC;

.field public final f:Lcom/facebook/platform/common/action/PlatformAppCall;

.field public final g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/GwW;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/1Kf;LX/1nC;)V
    .locals 7
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/GwW;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2406747
    invoke-direct {p0}, LX/4hY;-><init>()V

    .line 2406748
    iput-object p1, p0, LX/GwD;->b:Landroid/app/Activity;

    .line 2406749
    iput-object p3, p0, LX/GwD;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2406750
    iput-object p4, p0, LX/GwD;->d:LX/1Kf;

    .line 2406751
    iput-object p5, p0, LX/GwD;->e:LX/1nC;

    .line 2406752
    iget-object v0, p2, LX/4hh;->a:Lcom/facebook/platform/common/action/PlatformAppCall;

    move-object v0, v0

    .line 2406753
    iput-object v0, p0, LX/GwD;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406754
    iget-object v0, p2, LX/GwW;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2406755
    :try_start_0
    new-instance p1, LX/5m9;

    invoke-direct {p1}, LX/5m9;-><init>()V

    .line 2406756
    iput-object v0, p1, LX/5m9;->f:Ljava/lang/String;

    .line 2406757
    move-object p1, p1

    .line 2406758
    invoke-virtual {p1}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object p1

    .line 2406759
    :goto_0
    move-object v0, p1

    .line 2406760
    iput-object v0, p0, LX/GwD;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2406761
    iget-object v0, p2, LX/GwW;->c:Ljava/util/ArrayList;

    move-object v0, v0

    .line 2406762
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2406763
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2406764
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v1

    invoke-virtual {v1}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2406765
    :catch_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2406766
    :goto_2
    move-object v0, v1

    .line 2406767
    iput-object v0, p0, LX/GwD;->h:LX/0Px;

    .line 2406768
    iget-object v0, p2, LX/GwW;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2406769
    iput-object v0, p0, LX/GwD;->i:Ljava/lang/String;

    .line 2406770
    return-void

    .line 2406771
    :catch_1
    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_2
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2406772
    const/16 v0, 0x33

    if-ne p1, v0, :cond_0

    .line 2406773
    if-nez p2, :cond_1

    .line 2406774
    iget-object v0, p0, LX/GwD;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    const-string v1, "User canceled message"

    invoke-static {v0, v1}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/4hY;->c(Landroid/os/Bundle;)V

    .line 2406775
    :cond_0
    :goto_0
    return-void

    .line 2406776
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GwD;->a:Z

    .line 2406777
    iget-object v0, p0, LX/GwD;->c:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406778
    new-instance v1, LX/4At;

    iget-object v2, p0, LX/GwD;->b:Landroid/app/Activity;

    const v3, 0x7f08360f

    invoke-direct {v1, v2, v3}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2406779
    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2406780
    new-instance v2, LX/GwC;

    invoke-direct {v2, p0, v1}, LX/GwC;-><init>(LX/GwD;LX/4At;)V

    invoke-static {v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2406781
    if-eqz p1, :cond_4

    const-string v0, "is_ui_showing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2406782
    iput-boolean v0, p0, LX/GwD;->a:Z

    .line 2406783
    iget-boolean v0, p0, LX/GwD;->a:Z

    if-nez v0, :cond_3

    .line 2406784
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GwD;->a:Z

    .line 2406785
    iget-object v0, p0, LX/GwD;->d:LX/1Kf;

    const/4 v1, 0x0

    .line 2406786
    sget-object v2, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    const-string v3, "legacyShareDialogActionExecutor"

    invoke-static {v2, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const-string v3, "platform_composer"

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    .line 2406787
    iget-object v3, p0, LX/GwD;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v3, :cond_0

    .line 2406788
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v3

    iget-object v4, p0, LX/GwD;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v3, v4}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v3

    invoke-virtual {v3}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406789
    :cond_0
    iget-object v3, p0, LX/GwD;->h:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2406790
    iget-object v3, p0, LX/GwD;->h:LX/0Px;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406791
    :cond_1
    iget-object v3, p0, LX/GwD;->i:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2406792
    iget-object v3, p0, LX/GwD;->i:Ljava/lang/String;

    invoke-static {v3}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v3

    invoke-virtual {v3}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406793
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    move-object v2, v2

    .line 2406794
    const/16 v3, 0x33

    iget-object v4, p0, LX/GwD;->b:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2406795
    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2406796
    const-string v0, "is_ui_showing"

    iget-boolean v1, p0, LX/GwD;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2406797
    return-void
.end method
