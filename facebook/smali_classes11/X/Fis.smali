.class public final LX/Fis;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Fit;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public final synthetic c:LX/Fit;


# direct methods
.method public constructor <init>(LX/Fit;)V
    .locals 1

    .prologue
    .line 2275902
    iput-object p1, p0, LX/Fis;->c:LX/Fit;

    .line 2275903
    move-object v0, p1

    .line 2275904
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2275905
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2275906
    const-string v0, "SearchTypeaheadNUXComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2275907
    if-ne p0, p1, :cond_1

    .line 2275908
    :cond_0
    :goto_0
    return v0

    .line 2275909
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2275910
    goto :goto_0

    .line 2275911
    :cond_3
    check-cast p1, LX/Fis;

    .line 2275912
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2275913
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2275914
    if-eq v2, v3, :cond_0

    .line 2275915
    iget-object v2, p0, LX/Fis;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Fis;->a:Ljava/lang/String;

    iget-object v3, p1, LX/Fis;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2275916
    goto :goto_0

    .line 2275917
    :cond_5
    iget-object v2, p1, LX/Fis;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2275918
    :cond_6
    iget-object v2, p0, LX/Fis;->b:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Fis;->b:Ljava/lang/String;

    iget-object v3, p1, LX/Fis;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2275919
    goto :goto_0

    .line 2275920
    :cond_7
    iget-object v2, p1, LX/Fis;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
