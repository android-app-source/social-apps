.class public LX/Gju;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2388618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2388619
    iput-object p1, p0, LX/Gju;->a:Landroid/content/res/Resources;

    .line 2388620
    return-void
.end method

.method private static a(IZ)LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/greetingcards/verve/model/VMAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2388612
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 2388613
    if-lez p0, :cond_0

    .line 2388614
    const-string v1, "swipe-down"

    new-instance v2, Lcom/facebook/greetingcards/verve/model/VMAction;

    const-string v3, "select-slide"

    add-int/lit8 v4, p0, -0x1

    invoke-static {v4}, LX/Gju;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/greetingcards/verve/model/VMAction;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2388615
    :cond_0
    if-nez p1, :cond_1

    .line 2388616
    const-string v1, "swipe-up"

    new-instance v2, Lcom/facebook/greetingcards/verve/model/VMAction;

    const-string v3, "select-slide"

    add-int/lit8 v4, p0, 0x1

    invoke-static {v4}, LX/Gju;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/greetingcards/verve/model/VMAction;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2388617
    :cond_1
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;ZLX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMSlideValue;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2388597
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2388598
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2388599
    const-string v0, "title"

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2388600
    :cond_0
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2388601
    const-string v0, "body"

    iget-object v1, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    :cond_1
    move v1, v2

    .line 2388602
    :goto_0
    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2388603
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "media"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/CardPhoto;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/model/CardPhoto;

    iget-object v0, v0, Lcom/facebook/greetingcards/model/CardPhoto;->e:Landroid/graphics/PointF;

    invoke-static {v4, v5, v0}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2388604
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2388605
    :cond_2
    if-eqz p2, :cond_3

    .line 2388606
    const-string v0, "swipe_up"

    iget-object v1, p0, LX/Gju;->a:Landroid/content/res/Resources;

    const v4, 0x7f0835f7

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2388607
    :cond_3
    if-eqz p3, :cond_4

    .line 2388608
    :goto_1
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2388609
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "button"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2388610
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2388611
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/Gju;Lcom/facebook/greetingcards/model/GreetingCard$Slide;IILjava/lang/String;LX/0Px;FF)Lcom/facebook/greetingcards/verve/model/VMSlide;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/model/GreetingCard$Slide;",
            "II",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;FF)",
            "Lcom/facebook/greetingcards/verve/model/VMSlide;"
        }
    .end annotation

    .prologue
    .line 2388580
    new-instance v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    invoke-static {p2}, LX/Gju;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static/range {p7 .. p7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    add-int/lit8 v4, p3, -0x1

    if-ne p2, v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {p2, v4}, LX/Gju;->a(IZ)LX/0P1;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "Front"

    invoke-virtual {v6, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    invoke-direct {p0, p1, v6, p5}, LX/Gju;->a(Lcom/facebook/greetingcards/model/GreetingCard$Slide;ZLX/0Px;)LX/0Px;

    move-result-object v7

    const/4 v8, 0x0

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/facebook/greetingcards/verve/model/VMSlide;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0P1;ZLjava/lang/String;LX/0Px;Lcom/facebook/greetingcards/verve/model/VMColor;)V

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2388596
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Slide "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v1, p0, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/verve/model/VMDeck;Lcom/facebook/greetingcards/model/GreetingCard;FFLX/0Px;)Lcom/facebook/greetingcards/verve/model/VMDeck;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/verve/model/VMDeck;",
            "Lcom/facebook/greetingcards/model/GreetingCard;",
            "FF",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/greetingcards/verve/model/VMDeck;"
        }
    .end annotation

    .prologue
    .line 2388581
    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p2, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    add-int v3, v1, v0

    .line 2388582
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2388583
    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    if-eqz v0, :cond_0

    .line 2388584
    iget-object v1, p2, Lcom/facebook/greetingcards/model/GreetingCard;->a:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    const/4 v2, 0x0

    const-string v4, "Front"

    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    if-nez v0, :cond_3

    move-object v5, p5

    :goto_2
    move-object v0, p0

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, LX/Gju;->a(LX/Gju;Lcom/facebook/greetingcards/model/GreetingCard$Slide;IILjava/lang/String;LX/0Px;FF)Lcom/facebook/greetingcards/verve/model/VMSlide;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2388585
    :cond_0
    const/4 v0, 0x0

    move v8, v0

    :goto_3
    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v8, v0, :cond_4

    .line 2388586
    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->b:LX/0Px;

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    add-int/lit8 v2, v8, 0x1

    const-string v4, "Moment"

    const/4 v5, 0x0

    move-object v0, p0

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, LX/Gju;->a(LX/Gju;Lcom/facebook/greetingcards/model/GreetingCard$Slide;IILjava/lang/String;LX/0Px;FF)Lcom/facebook/greetingcards/verve/model/VMSlide;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2388587
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_3

    .line 2388588
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2388589
    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    .line 2388590
    :cond_4
    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    if-eqz v0, :cond_5

    .line 2388591
    iget-object v1, p2, Lcom/facebook/greetingcards/model/GreetingCard;->c:Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    add-int/lit8 v2, v8, 0x1

    const-string v4, "Back"

    move-object v0, p0

    move-object v5, p5

    move v6, p3

    move v7, p4

    invoke-static/range {v0 .. v7}, LX/Gju;->a(LX/Gju;Lcom/facebook/greetingcards/model/GreetingCard$Slide;IILjava/lang/String;LX/0Px;FF)Lcom/facebook/greetingcards/verve/model/VMSlide;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2388592
    :cond_5
    iget-object v0, p1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v9, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2388593
    iget-object v0, p2, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v6, p2, Lcom/facebook/greetingcards/model/GreetingCard;->e:Ljava/lang/String;

    .line 2388594
    :goto_4
    new-instance v0, Lcom/facebook/greetingcards/verve/model/VMDeck;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/greetingcards/verve/model/VMDeck;->resources:LX/0P1;

    const/4 v4, 0x0

    invoke-static {v4}, LX/Gju;->a(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/greetingcards/verve/model/VMDeck;->styles:LX/0Px;

    iget-object v7, p1, Lcom/facebook/greetingcards/verve/model/VMDeck;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/greetingcards/verve/model/VMDeck;-><init>(LX/0Px;LX/0Px;LX/0P1;Ljava/lang/String;LX/0Px;Ljava/lang/String;Lcom/facebook/greetingcards/verve/model/VMColor;)V

    return-object v0

    .line 2388595
    :cond_6
    const-string v6, "Theme 1"

    goto :goto_4
.end method
