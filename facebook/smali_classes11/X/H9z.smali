.class public final LX/H9z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friends/protocol/FriendMutationsModels$ActorSubscribeCoreMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/HA0;


# direct methods
.method public constructor <init>(LX/HA0;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2434767
    iput-object p1, p0, LX/H9z;->c:LX/HA0;

    iput-object p2, p0, LX/H9z;->a:Ljava/lang/String;

    iput-object p3, p0, LX/H9z;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2434768
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 2434769
    :goto_0
    return-void

    .line 2434770
    :cond_0
    iget-object v0, p0, LX/H9z;->c:LX/HA0;

    sget-object v1, LX/9XA;->EVENT_PAGE_FOLLOW_ERROR:LX/9XA;

    iget-object v2, p0, LX/H9z;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, LX/HA0;->a$redex0(LX/HA0;LX/9X2;J)V

    .line 2434771
    iget-object v0, p0, LX/H9z;->c:LX/HA0;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2434772
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v2, v1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2434773
    const v3, 0x7f0817c9

    .line 2434774
    const-string v2, "page_identity_follow_fail"

    .line 2434775
    :goto_1
    iget-object v4, v0, LX/HA0;->c:LX/0kL;

    new-instance v5, LX/27k;

    invoke-direct {v5, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v4, v5}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2434776
    iget-object v3, v0, LX/HA0;->d:LX/03V;

    invoke-virtual {v3, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2434777
    iget-object v0, p0, LX/H9z;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0

    .line 2434778
    :cond_1
    const v3, 0x7f0817ca

    .line 2434779
    const-string v2, "page_identity_unfollow_fail"

    goto :goto_1
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2434780
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2434781
    iget-object v0, p0, LX/H9z;->c:LX/HA0;

    sget-object v1, LX/9XB;->EVENT_PAGE_FOLLOW_SUCCESS:LX/9XB;

    iget-object v2, p0, LX/H9z;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, LX/HA0;->a$redex0(LX/HA0;LX/9X2;J)V

    .line 2434782
    iget-object v0, p0, LX/H9z;->b:Lcom/google/common/util/concurrent/SettableFuture;

    .line 2434783
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2434784
    const v2, 0x701ca61d

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2434785
    return-void
.end method
