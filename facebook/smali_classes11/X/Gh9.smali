.class public final LX/Gh9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2383424
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 2383425
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2383426
    :goto_0
    return v1

    .line 2383427
    :cond_0
    const-string v10, "unread_count"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2383428
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 2383429
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_8

    .line 2383430
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2383431
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2383432
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 2383433
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2383434
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_1

    .line 2383435
    :cond_3
    const-string v10, "edges"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2383436
    invoke-static {p0, p1}, LX/9r8;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2383437
    :cond_4
    const-string v10, "next_update_condition"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 2383438
    invoke-static {p0, p1}, LX/9rJ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2383439
    :cond_5
    const-string v10, "page_info"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2383440
    invoke-static {p0, p1}, LX/3Bn;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2383441
    :cond_6
    const-string v10, "session_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2383442
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2383443
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2383444
    :cond_8
    const/4 v9, 0x6

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2383445
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2383446
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 2383447
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 2383448
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 2383449
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2383450
    if-eqz v0, :cond_9

    .line 2383451
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 2383452
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2383453
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2383454
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2383455
    if-eqz v0, :cond_0

    .line 2383456
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383457
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2383458
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383459
    if-eqz v0, :cond_1

    .line 2383460
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383461
    invoke-static {p0, v0, p2, p3}, LX/9r8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383462
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383463
    if-eqz v0, :cond_2

    .line 2383464
    const-string v1, "next_update_condition"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383465
    invoke-static {p0, v0, p2, p3}, LX/9rJ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383466
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383467
    if-eqz v0, :cond_3

    .line 2383468
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383469
    invoke-static {p0, v0, p2}, LX/3Bn;->a(LX/15i;ILX/0nX;)V

    .line 2383470
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383471
    if-eqz v0, :cond_4

    .line 2383472
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383473
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383474
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2383475
    if-eqz v0, :cond_5

    .line 2383476
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383477
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2383478
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2383479
    return-void
.end method
