.class public LX/FeD;
.super LX/0hD;
.source ""

# interfaces
.implements LX/0hk;


# annotations
.annotation build Lcom/facebook/controllercallbacks/api/ControllerConfig;
.end annotation


# instance fields
.field public a:LX/Cxk;

.field private b:LX/1B1;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0bH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FgJ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2265256
    invoke-direct {p0}, LX/0hD;-><init>()V

    .line 2265257
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2265258
    iput-object v0, p0, LX/FeD;->c:LX/0Ot;

    .line 2265259
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2265260
    iput-object v0, p0, LX/FeD;->d:LX/0Ot;

    .line 2265261
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2265262
    iput-object v0, p0, LX/FeD;->e:LX/0Ot;

    .line 2265263
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2265264
    iput-object v0, p0, LX/FeD;->f:LX/0Ot;

    .line 2265265
    return-void
.end method

.method private b()LX/1B1;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2265266
    iget-object v0, p0, LX/FeD;->b:LX/1B1;

    if-nez v0, :cond_0

    .line 2265267
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, LX/FeD;->b:LX/1B1;

    .line 2265268
    iget-object v0, p0, LX/FeD;->b:LX/1B1;

    const/4 v1, 0x3

    new-array v1, v1, [LX/0b2;

    new-instance v2, LX/FeA;

    invoke-direct {v2, p0}, LX/FeA;-><init>(LX/FeD;)V

    aput-object v2, v1, v4

    const/4 v2, 0x1

    new-instance v3, LX/FeB;

    invoke-direct {v3, p0}, LX/FeB;-><init>(LX/FeD;)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, LX/FeC;

    invoke-direct {v3, p0}, LX/FeC;-><init>(LX/FeD;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/1B1;->a([LX/0b2;)V

    .line 2265269
    :cond_0
    iget-object v0, p0, LX/FeD;->b:LX/1B1;

    return-object v0
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 2265270
    invoke-direct {p0}, LX/FeD;->b()LX/1B1;

    move-result-object v1

    iget-object v0, p0, LX/FeD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v1, v0}, LX/1B1;->a(LX/0b4;)V

    .line 2265271
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2265272
    invoke-direct {p0}, LX/FeD;->b()LX/1B1;

    move-result-object v1

    iget-object v0, p0, LX/FeD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0b4;

    invoke-virtual {v1, v0}, LX/1B1;->b(LX/0b4;)V

    .line 2265273
    return-void
.end method
