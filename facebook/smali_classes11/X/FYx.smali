.class public LX/FYx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3HM;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FYy;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1qa;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0W3;

.field public final j:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0W3;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p10    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3HM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FYy;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1qa;",
            ">;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2256678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2256679
    iput-object p1, p0, LX/FYx;->a:LX/0Ot;

    .line 2256680
    iput-object p2, p0, LX/FYx;->b:LX/0Ot;

    .line 2256681
    iput-object p5, p0, LX/FYx;->e:LX/0Ot;

    .line 2256682
    iput-object p6, p0, LX/FYx;->f:LX/0Ot;

    .line 2256683
    iput-object p7, p0, LX/FYx;->g:LX/0Ot;

    .line 2256684
    iput-object p8, p0, LX/FYx;->h:LX/0Ot;

    .line 2256685
    iput-object p4, p0, LX/FYx;->d:LX/0Ot;

    .line 2256686
    iput-object p3, p0, LX/FYx;->c:LX/0Ot;

    .line 2256687
    iput-object p9, p0, LX/FYx;->i:LX/0W3;

    .line 2256688
    iput-object p10, p0, LX/FYx;->j:Ljava/util/concurrent/ExecutorService;

    .line 2256689
    return-void
.end method

.method public static a(LX/FYx;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2256690
    new-instance v2, Lcom/facebook/api/story/FetchSingleStoryParams;

    sget-object v0, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v1, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    const/16 v3, 0x19

    invoke-direct {v2, p1, v0, v1, v3}, Lcom/facebook/api/story/FetchSingleStoryParams;-><init>(Ljava/lang/String;LX/0rS;LX/5Go;I)V

    .line 2256691
    iget-object v0, p0, LX/FYx;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    iget-object v1, p0, LX/FYx;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3HM;

    invoke-virtual {v1, v2, v4, v4}, LX/3HM;->a(Lcom/facebook/api/story/FetchSingleStoryParams;Ljava/lang/String;LX/3HP;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/FYx;LX/BO1;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BO1;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2256692
    invoke-interface {p1}, LX/AU0;->b()I

    move-result v1

    .line 2256693
    :goto_0
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 2256694
    invoke-interface {p1}, LX/AU0;->b()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {p1, v3}, LX/AU0;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, LX/BO1;->X()I

    move-result v3

    if-eq v3, v0, :cond_1

    :cond_0
    invoke-interface {p1, v2}, LX/AU0;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_1
    invoke-interface {p1}, LX/AU0;->b()I

    move-result v3

    if-eq v3, v1, :cond_3

    :goto_1
    move v0, v0

    .line 2256695
    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v0, p0, LX/FYx;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tQ;

    .line 2256696
    iget-object v3, v0, LX/0tQ;->a:LX/0ad;

    sget v4, LX/0wh;->D:I

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    move v0, v3

    .line 2256697
    if-ge v2, v0, :cond_2

    .line 2256698
    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2256699
    :cond_2
    return-object p2

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public static a$redex0(LX/FYx;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/395;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/395;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2256700
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2256701
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2256702
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    move-object v3, v1

    .line 2256703
    :goto_0
    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2256704
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2256705
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    move-object v3, v0

    move-object v0, v1

    goto :goto_0

    .line 2256706
    :cond_0
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256707
    invoke-virtual {p1, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    .line 2256708
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 2256709
    invoke-static {v5}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 2256710
    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 2256711
    new-instance v2, LX/0AW;

    invoke-direct {v2, v0}, LX/0AW;-><init>(LX/162;)V

    .line 2256712
    iput-boolean v1, v2, LX/0AW;->d:Z

    .line 2256713
    move-object v0, v2

    .line 2256714
    invoke-virtual {v0}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v2

    .line 2256715
    new-instance v0, LX/395;

    new-instance v1, LX/0AV;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 2256716
    iget-object v6, p0, LX/FYx;->h:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1qa;

    sget-object p1, LX/26P;->Video:LX/26P;

    invoke-virtual {v6, v3, p1}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v6

    move-object v3, v6

    .line 2256717
    invoke-direct/range {v0 .. v5}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2256718
    sget-object v1, LX/04D;->SAVED_DASHBOARD:LX/04D;

    invoke-virtual {v0, v1}, LX/395;->a(LX/04D;)LX/395;

    .line 2256719
    return-object v0
.end method

.method public static a$redex0(LX/FYx;JLandroid/content/Context;LX/3Qw;)V
    .locals 3

    .prologue
    .line 2256720
    const-class v0, LX/0f8;

    invoke-static {p3, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f8;

    .line 2256721
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256722
    invoke-interface {v0}, LX/0f8;->l()LX/0hE;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/channelfeed/ChannelFeedRootView;

    .line 2256723
    new-instance v1, LX/FYw;

    invoke-direct {v1, p0, p1, p2}, LX/FYw;-><init>(LX/FYx;J)V

    invoke-virtual {v0, v1}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(LX/394;)Ljava/lang/Object;

    .line 2256724
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256725
    invoke-virtual {v0, p4}, Lcom/facebook/video/channelfeed/ChannelFeedRootView;->a(LX/3Qw;)V

    .line 2256726
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/BO1;)V
    .locals 12

    .prologue
    .line 2256727
    iget-object v0, p0, LX/FYx;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/BO1;->X()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2256728
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2256729
    invoke-interface {p2}, LX/AUV;->d()J

    move-result-wide v8

    .line 2256730
    iget-object v4, p0, LX/FYx;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FYy;

    invoke-virtual {v4, p2}, LX/FYy;->a(LX/BO1;)I

    move-result v7

    .line 2256731
    const/4 v4, 0x0

    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2256732
    invoke-static {p0, p2, v5}, LX/FYx;->a(LX/FYx;LX/BO1;Ljava/util/List;)Ljava/util/List;

    .line 2256733
    iget-object v4, p0, LX/FYx;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/19w;

    invoke-virtual {v4, v5}, LX/19w;->a(Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2256734
    new-instance v4, LX/FYu;

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v4 .. v9}, LX/FYu;-><init>(LX/FYx;Landroid/content/Context;IJ)V

    iget-object v5, p0, LX/FYx;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v10, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2256735
    :goto_0
    return-void

    .line 2256736
    :cond_0
    invoke-interface {p2}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FYx;->i:LX/0W3;

    sget-wide v2, LX/0X5;->ee:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256737
    invoke-interface {p2}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, LX/FYx;->a(LX/FYx;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v11

    .line 2256738
    invoke-interface {p2}, LX/BO1;->Y()Ljava/lang/String;

    move-result-object v6

    .line 2256739
    invoke-interface {p2}, LX/AUV;->d()J

    move-result-wide v8

    .line 2256740
    iget-object v4, p0, LX/FYx;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FYy;

    invoke-virtual {v4, p2}, LX/FYy;->a(LX/BO1;)I

    move-result v7

    .line 2256741
    new-instance v4, LX/FYt;

    move-object v5, p0

    move-object v10, p1

    invoke-direct/range {v4 .. v10}, LX/FYt;-><init>(LX/FYx;Ljava/lang/String;IJLandroid/content/Context;)V

    iget-object v5, p0, LX/FYx;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v11, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2256742
    goto :goto_0

    .line 2256743
    :cond_1
    invoke-interface {p2}, LX/AUV;->d()J

    move-result-wide v8

    .line 2256744
    iget-object v4, p0, LX/FYx;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FYy;

    invoke-virtual {v4, p2}, LX/FYy;->a(LX/BO1;)I

    move-result v7

    .line 2256745
    invoke-interface {p2}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, LX/FYx;->a(LX/FYx;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2256746
    new-instance v5, LX/FYs;

    move-object v6, p0

    move-object v10, p1

    invoke-direct/range {v5 .. v10}, LX/FYs;-><init>(LX/FYx;IJLandroid/content/Context;)V

    iget-object v6, p0, LX/FYx;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2256747
    goto :goto_0
.end method
