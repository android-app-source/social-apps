.class public LX/Fbm;
.super LX/FbD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbD",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261049
    invoke-direct {p0}, LX/FbD;-><init>()V

    .line 2261050
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbm;
    .locals 3

    .prologue
    .line 2261051
    sget-object v0, LX/Fbm;->a:LX/Fbm;

    if-nez v0, :cond_1

    .line 2261052
    const-class v1, LX/Fbm;

    monitor-enter v1

    .line 2261053
    :try_start_0
    sget-object v0, LX/Fbm;->a:LX/Fbm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2261054
    if-eqz v2, :cond_0

    .line 2261055
    :try_start_1
    new-instance v0, LX/Fbm;

    invoke-direct {v0}, LX/Fbm;-><init>()V

    .line 2261056
    move-object v0, v0

    .line 2261057
    sput-object v0, LX/Fbm;->a:LX/Fbm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261058
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261059
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261060
    :cond_1
    sget-object v0, LX/Fbm;->a:LX/Fbm;

    return-object v0

    .line 2261061
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261062
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261063
    invoke-static {p1}, LX/Fbf;->b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261064
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->cn()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->H()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->H()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;->a()I

    move-result v0

    :goto_1
    invoke-direct {v1, v2, v3, v0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;-><init>(Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;Ljava/lang/String;I)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
