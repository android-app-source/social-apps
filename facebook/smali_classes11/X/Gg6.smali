.class public LX/Gg6;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gg7;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gg6",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Gg7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378515
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2378516
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gg6;->b:LX/0Zi;

    .line 2378517
    iput-object p1, p0, LX/Gg6;->a:LX/0Ot;

    .line 2378518
    return-void
.end method

.method public static a(LX/0QB;)LX/Gg6;
    .locals 4

    .prologue
    .line 2378504
    const-class v1, LX/Gg6;

    monitor-enter v1

    .line 2378505
    :try_start_0
    sget-object v0, LX/Gg6;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378506
    sput-object v2, LX/Gg6;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378507
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378508
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378509
    new-instance v3, LX/Gg6;

    const/16 p0, 0x212a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gg6;-><init>(LX/0Ot;)V

    .line 2378510
    move-object v0, v3

    .line 2378511
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378512
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gg6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378513
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378514
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2378519
    check-cast p2, LX/Gg5;

    .line 2378520
    iget-object v0, p0, LX/Gg6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gg7;

    iget-object v1, p2, LX/Gg5;->a:LX/GfY;

    iget-object v2, p2, LX/Gg5;->b:LX/1X1;

    iget-object v3, p2, LX/Gg5;->c:LX/1Pp;

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2378521
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/Gg7;->c:LX/Gfl;

    .line 2378522
    iget-object p0, v5, LX/Gfl;->g:LX/1DR;

    iget-object p2, v5, LX/Gfl;->h:Landroid/content/Context;

    invoke-virtual {p0, p2}, LX/1DR;->a(Landroid/content/Context;)I

    move-result p0

    move v5, p0

    .line 2378523
    invoke-interface {v4, v5}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    invoke-interface {v4, v5, v8}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3d

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/Gg7;->a:LX/Gfb;

    const/4 p0, 0x0

    .line 2378524
    new-instance p2, LX/GfZ;

    invoke-direct {p2, v5}, LX/GfZ;-><init>(LX/Gfb;)V

    .line 2378525
    iget-object v2, v5, LX/Gfb;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gfa;

    .line 2378526
    if-nez v2, :cond_0

    .line 2378527
    new-instance v2, LX/Gfa;

    invoke-direct {v2, v5}, LX/Gfa;-><init>(LX/Gfb;)V

    .line 2378528
    :cond_0
    invoke-static {v2, p1, p0, p0, p2}, LX/Gfa;->a$redex0(LX/Gfa;LX/1De;IILX/GfZ;)V

    .line 2378529
    move-object p2, v2

    .line 2378530
    move-object p0, p2

    .line 2378531
    move-object v5, p0

    .line 2378532
    iget-object p0, v5, LX/Gfa;->a:LX/GfZ;

    iput-object v1, p0, LX/GfZ;->a:LX/GfY;

    .line 2378533
    iget-object p0, v5, LX/Gfa;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 2378534
    move-object v5, v5

    .line 2378535
    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->c(I)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7, v6}, LX/1Di;->k(II)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v8, v6}, LX/1Di;->k(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/Gg7;->b:LX/Gfq;

    const/4 v6, 0x0

    .line 2378536
    new-instance v7, LX/Gfp;

    invoke-direct {v7, v5}, LX/Gfp;-><init>(LX/Gfq;)V

    .line 2378537
    iget-object v8, v5, LX/Gfq;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Gfo;

    .line 2378538
    if-nez v8, :cond_1

    .line 2378539
    new-instance v8, LX/Gfo;

    invoke-direct {v8, v5}, LX/Gfo;-><init>(LX/Gfq;)V

    .line 2378540
    :cond_1
    invoke-static {v8, p1, v6, v6, v7}, LX/Gfo;->a$redex0(LX/Gfo;LX/1De;IILX/Gfp;)V

    .line 2378541
    move-object v7, v8

    .line 2378542
    move-object v6, v7

    .line 2378543
    move-object v5, v6

    .line 2378544
    iget-object v6, v5, LX/Gfo;->a:LX/Gfp;

    iput-object v1, v6, LX/Gfp;->a:LX/GfY;

    .line 2378545
    iget-object v6, v5, LX/Gfo;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2378546
    move-object v5, v5

    .line 2378547
    iget-object v6, v5, LX/Gfo;->a:LX/Gfp;

    iput-object v3, v6, LX/Gfp;->b:LX/1Pp;

    .line 2378548
    iget-object v6, v5, LX/Gfo;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2378549
    move-object v5, v5

    .line 2378550
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2378551
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2378502
    invoke-static {}, LX/1dS;->b()V

    .line 2378503
    const/4 v0, 0x0

    return-object v0
.end method
