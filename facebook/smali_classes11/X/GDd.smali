.class public final LX/GDd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GDc;


# instance fields
.field public final synthetic a:LX/GCE;

.field public final synthetic b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public final synthetic c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:LX/GDg;


# direct methods
.method public constructor <init>(LX/GDg;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2330010
    iput-object p1, p0, LX/GDd;->e:LX/GDg;

    iput-object p2, p0, LX/GDd;->a:LX/GCE;

    iput-object p3, p0, LX/GDd;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object p4, p0, LX/GDd;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    iput-object p5, p0, LX/GDd;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 7

    .prologue
    .line 2330011
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 2330012
    iget-object v0, p0, LX/GDd;->a:LX/GCE;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2330013
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, LX/GCE;->c:Ljava/lang/Boolean;

    .line 2330014
    iget-object v0, p0, LX/GDd;->e:LX/GDg;

    iget-object v0, v0, LX/GDg;->c:LX/GNI;

    iget-object v1, p0, LX/GDd;->a:LX/GCE;

    iget-object v2, p0, LX/GDd;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v3, p0, LX/GDd;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    iget-object v4, p0, LX/GDd;->d:Landroid/content/Context;

    check-cast v4, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v4

    iget-object v5, p0, LX/GDd;->d:Landroid/content/Context;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, LX/GNI;->a(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;LX/0gc;Landroid/content/Context;Z)V

    .line 2330015
    return-void
.end method
