.class public LX/Fqk;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

.field private b:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2294453
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2294454
    invoke-direct {p0}, LX/Fqk;->a()V

    .line 2294455
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2294456
    const v0, 0x7f0314c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2294457
    const v0, 0x7f0d2f15

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    iput-object v0, p0, LX/Fqk;->a:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    .line 2294458
    const v0, 0x7f0d2f16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Fqk;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2294459
    iget-object v0, p0, LX/Fqk;->a:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    const v1, 0x7f081550

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setText(I)V

    .line 2294460
    iget-object v0, p0, LX/Fqk;->a:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    invoke-virtual {p0}, LX/Fqk;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f02088a

    invoke-static {v1, v2}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2294461
    iget-object v0, p0, LX/Fqk;->a:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    invoke-virtual {p0}, LX/Fqk;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0680

    invoke-virtual {v0, v1, v2}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 2294462
    iget-object v0, p0, LX/Fqk;->a:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    invoke-virtual {p0}, LX/Fqk;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00e5

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setGlyphColor(I)V

    .line 2294463
    return-void
.end method


# virtual methods
.method public setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2294464
    invoke-virtual {p0, p1}, LX/Fqk;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2294465
    iget-object v0, p0, LX/Fqk;->a:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2294466
    return-void
.end method

.method public setCount(I)V
    .locals 5

    .prologue
    .line 2294467
    iget-object v0, p0, LX/Fqk;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2294468
    invoke-virtual {p0}, LX/Fqk;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081552

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2294469
    iget-object v1, p0, LX/Fqk;->a:Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2294470
    return-void
.end method
