.class public final LX/FS5;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2241231
    sget-object v0, LX/6Yz;->b:LX/0U1;

    sget-object v1, LX/6Yz;->a:LX/0U1;

    sget-object v2, LX/6Yz;->c:LX/0U1;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/FS5;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2241232
    const-string v0, "removedprefilledtags"

    sget-object v1, LX/FS5;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2241233
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 2241234
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2241235
    const-string v0, "removedprefilledtags"

    const-string v1, "removed_prefilled_tag_idx"

    sget-object v2, LX/6Yz;->b:LX/0U1;

    sget-object v3, LX/6Yz;->a:LX/0U1;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x752cdd1a

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0xb800757

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2241236
    return-void
.end method
