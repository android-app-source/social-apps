.class public final enum LX/GuQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GuQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GuQ;

.field public static final enum NONE:LX/GuQ;

.field public static final enum OPTIONS_MENU:LX/GuQ;

.field public static final enum TITLE_BUTTON:LX/GuQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2403321
    new-instance v0, LX/GuQ;

    const-string v1, "TITLE_BUTTON"

    invoke-direct {v0, v1, v2}, LX/GuQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GuQ;->TITLE_BUTTON:LX/GuQ;

    .line 2403322
    new-instance v0, LX/GuQ;

    const-string v1, "OPTIONS_MENU"

    invoke-direct {v0, v1, v3}, LX/GuQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GuQ;->OPTIONS_MENU:LX/GuQ;

    .line 2403323
    new-instance v0, LX/GuQ;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/GuQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GuQ;->NONE:LX/GuQ;

    .line 2403324
    const/4 v0, 0x3

    new-array v0, v0, [LX/GuQ;

    sget-object v1, LX/GuQ;->TITLE_BUTTON:LX/GuQ;

    aput-object v1, v0, v2

    sget-object v1, LX/GuQ;->OPTIONS_MENU:LX/GuQ;

    aput-object v1, v0, v3

    sget-object v1, LX/GuQ;->NONE:LX/GuQ;

    aput-object v1, v0, v4

    sput-object v0, LX/GuQ;->$VALUES:[LX/GuQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2403325
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GuQ;
    .locals 1

    .prologue
    .line 2403326
    const-class v0, LX/GuQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GuQ;

    return-object v0
.end method

.method public static values()[LX/GuQ;
    .locals 1

    .prologue
    .line 2403327
    sget-object v0, LX/GuQ;->$VALUES:[LX/GuQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GuQ;

    return-object v0
.end method
