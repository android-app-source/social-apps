.class public LX/H82;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Y0;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Or;LX/0Y0;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Y0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2432642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2432643
    iput-object p1, p0, LX/H82;->a:LX/0SG;

    .line 2432644
    iput-object p2, p0, LX/H82;->b:LX/0Or;

    .line 2432645
    iput-object p3, p0, LX/H82;->c:LX/0Y0;

    .line 2432646
    return-void
.end method

.method public static a(LX/0QB;)LX/H82;
    .locals 1

    .prologue
    .line 2432641
    invoke-static {p0}, LX/H82;->b(LX/0QB;)LX/H82;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/H82;
    .locals 4

    .prologue
    .line 2432647
    new-instance v2, LX/H82;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    const/16 v1, 0x15e7

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0Y0;->a(LX/0QB;)LX/0Y0;

    move-result-object v1

    check-cast v1, LX/0Y0;

    invoke-direct {v2, v0, v3, v1}, LX/H82;-><init>(LX/0SG;LX/0Or;LX/0Y0;)V

    .line 2432648
    return-object v2
.end method


# virtual methods
.method public final a(LX/H83;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/HoneyAnalyticsEvent;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2432604
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "offer_notif_response"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2432605
    const-string v0, "offer_notifications"

    .line 2432606
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2432607
    const-string v2, "user_fbid"

    iget-object v0, p0, LX/H82;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432608
    const-string v0, "page_fbid"

    invoke-virtual {p1}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432609
    const-string v0, "offer_claim_fbid"

    invoke-virtual {p1}, LX/H83;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432610
    const-string v0, "share_fbid"

    invoke-static {p1}, LX/H6T;->b(LX/H83;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432611
    const-string v0, "offer_fbid"

    invoke-virtual {p1}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432612
    const-string v0, "notif_trigger"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432613
    const-string v0, "notif_medium"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432614
    const-string v0, "notif_rule"

    invoke-virtual {v1, v0, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432615
    const-string v0, "offer_view_fbid"

    invoke-virtual {p1}, LX/H83;->w()LX/H72;

    move-result-object v2

    invoke-interface {v2}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432616
    return-object v1
.end method

.method public final a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v6, 0x3e8

    .line 2432617
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2432618
    const-string v0, "offers"

    .line 2432619
    iput-object v0, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2432620
    invoke-virtual {p2}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432621
    const-string v0, "offer_claim_fbid"

    invoke-virtual {p2}, LX/H83;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432622
    const-string v0, "share_fbid"

    invoke-static {p2}, LX/H6T;->b(LX/H83;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432623
    const-string v0, "offer_fbid"

    invoke-virtual {p2}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432624
    const-string v0, "offer_view_fbid"

    invoke-virtual {p2}, LX/H83;->w()LX/H72;

    move-result-object v2

    invoke-interface {v2}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432625
    iget-object v0, p2, LX/H83;->b:LX/H70;

    move-object v0, v0

    .line 2432626
    invoke-interface {v0}, LX/H6y;->u()J

    move-result-wide v2

    mul-long/2addr v2, v6

    .line 2432627
    const-string v0, "seconds_since_claim"

    iget-object v4, p0, LX/H82;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    div-long/2addr v2, v6

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432628
    :goto_0
    const-string v2, "user_fbid"

    iget-object v0, p0, LX/H82;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432629
    const-string v0, "page_fbid"

    invoke-virtual {p2}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432630
    const-string v0, "network_down_bandwidth"

    iget-object v2, p0, LX/H82;->c:LX/0Y0;

    invoke-virtual {v2}, LX/0Y0;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432631
    if-eqz p3, :cond_0

    .line 2432632
    const-string v0, "offer_location"

    invoke-virtual {v1, v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432633
    :cond_0
    invoke-virtual {p2}, LX/H83;->h()J

    move-result-wide v2

    mul-long/2addr v2, v6

    .line 2432634
    const-string v0, "seconds_until_expires"

    iget-object v4, p0, LX/H82;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    div-long/2addr v2, v6

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432635
    return-object v1

    .line 2432636
    :cond_1
    invoke-virtual {p2}, LX/H83;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2432637
    const-string v0, "offer_view_fbid"

    invoke-virtual {p2}, LX/H83;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432638
    const-string v0, "share_fbid"

    invoke-static {p2}, LX/H6T;->b(LX/H83;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2432639
    const-string v0, "offer_fbid"

    invoke-virtual {p2}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 2432640
    :cond_2
    const-string v0, "coupon_fbid"

    invoke-virtual {p2}, LX/H83;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method
