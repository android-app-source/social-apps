.class public final LX/GF1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4At;

.field public final synthetic b:I

.field public final synthetic c:Landroid/support/v4/app/Fragment;

.field public final synthetic d:LX/GF2;


# direct methods
.method public constructor <init>(LX/GF2;LX/4At;ILandroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 2332244
    iput-object p1, p0, LX/GF1;->d:LX/GF2;

    iput-object p2, p0, LX/GF1;->a:LX/4At;

    iput p3, p0, LX/GF1;->b:I

    iput-object p4, p0, LX/GF1;->c:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2332245
    iget-object v0, p0, LX/GF1;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2332246
    iget-object v0, p0, LX/GF1;->d:LX/GF2;

    iget-object v0, v0, LX/GF2;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0836ac

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2332247
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2332248
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2332249
    iget-object v0, p0, LX/GF1;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2332250
    if-nez p1, :cond_0

    .line 2332251
    iget-object v0, p0, LX/GF1;->d:LX/GF2;

    iget-object v0, v0, LX/GF2;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0836ab

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2332252
    :cond_0
    iget-object v0, p0, LX/GF1;->d:LX/GF2;

    iget-object v0, v0, LX/GF2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget v3, p0, LX/GF1;->b:I

    iget-object v4, p0, LX/GF1;->c:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v1, v2, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2332253
    return-void
.end method
