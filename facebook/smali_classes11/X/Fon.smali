.class public final LX/Fon;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Foo;

.field private final b:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

.field public final c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

.field public final d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;


# direct methods
.method public constructor <init>(LX/Foo;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2290143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2290144
    iput-object p1, p0, LX/Fon;->a:LX/Foo;

    .line 2290145
    iput-object v0, p0, LX/Fon;->b:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    .line 2290146
    iput-object v0, p0, LX/Fon;->c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2290147
    iput-object v0, p0, LX/Fon;->d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2290148
    return-void
.end method

.method public constructor <init>(LX/Foo;Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2290137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2290138
    iput-object p1, p0, LX/Fon;->a:LX/Foo;

    .line 2290139
    iput-object p2, p0, LX/Fon;->b:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    .line 2290140
    iput-object v0, p0, LX/Fon;->c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2290141
    iput-object v0, p0, LX/Fon;->d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2290142
    return-void
.end method

.method public constructor <init>(LX/Foo;Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2290131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2290132
    iput-object p1, p0, LX/Fon;->a:LX/Foo;

    .line 2290133
    iput-object v0, p0, LX/Fon;->b:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    .line 2290134
    iput-object p2, p0, LX/Fon;->c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2290135
    iput-object v0, p0, LX/Fon;->d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2290136
    return-void
.end method

.method public constructor <init>(LX/Foo;Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2290125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2290126
    iput-object p1, p0, LX/Fon;->a:LX/Foo;

    .line 2290127
    iput-object p2, p0, LX/Fon;->d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2290128
    iput-object v0, p0, LX/Fon;->b:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    .line 2290129
    iput-object v0, p0, LX/Fon;->c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2290130
    return-void
.end method


# virtual methods
.method public final b()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;
    .locals 2

    .prologue
    .line 2290122
    iget-object v0, p0, LX/Fon;->a:LX/Foo;

    sget-object v1, LX/Foo;->CATEGORY:LX/Foo;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2290123
    iget-object v0, p0, LX/Fon;->b:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharityCategoriesModel$CategoryWrappersModel;

    return-object v0

    .line 2290124
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
