.class public LX/FVp;
.super LX/62n;
.source ""


# instance fields
.field public a:LX/0g8;

.field public b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

.field public c:Landroid/view/ViewStub;

.field private d:LX/FV1;


# direct methods
.method public constructor <init>(LX/FV1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2251280
    invoke-direct {p0}, LX/62n;-><init>()V

    .line 2251281
    iput-object p1, p0, LX/FVp;->d:LX/FV1;

    .line 2251282
    return-void
.end method

.method public static a(LX/0QB;)LX/FVp;
    .locals 2

    .prologue
    .line 2251295
    new-instance v1, LX/FVp;

    invoke-static {p0}, LX/FV1;->a(LX/0QB;)LX/FV1;

    move-result-object v0

    check-cast v0, LX/FV1;

    invoke-direct {v1, v0}, LX/FVp;-><init>(LX/FV1;)V

    .line 2251296
    move-object v0, v1

    .line 2251297
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2251298
    iget-object v0, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    if-eqz v0, :cond_0

    .line 2251299
    iget-object v0, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    invoke-virtual {v0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->b()V

    .line 2251300
    :cond_0
    return-void
.end method

.method public final a(LX/FVP;LX/0am;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FVP;",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2251286
    iget-object v0, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    if-nez v0, :cond_0

    .line 2251287
    iget-object v0, p0, LX/FVp;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/saved/views/SavedDashboardEmptyView;

    iput-object v0, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    .line 2251288
    iget-object v0, p0, LX/FVp;->a:LX/0g8;

    iget-object v1, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    invoke-interface {v0, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 2251289
    iget-object v0, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    invoke-virtual {v0, p1}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->setOnEmptyViewReadyListener(LX/FVP;)V

    .line 2251290
    iget-object v0, p0, LX/FVp;->d:LX/FV1;

    iget-object v1, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    invoke-virtual {v0, v1, p2}, LX/FV1;->a(Lcom/facebook/saved/views/SavedDashboardEmptyView;LX/0am;)V

    .line 2251291
    const/4 v0, 0x0

    .line 2251292
    :goto_0
    return v0

    .line 2251293
    :cond_0
    iget-object v0, p0, LX/FVp;->d:LX/FV1;

    iget-object v1, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    invoke-virtual {v0, v1, p2}, LX/FV1;->a(Lcom/facebook/saved/views/SavedDashboardEmptyView;LX/0am;)V

    .line 2251294
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2251283
    iget-object v0, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    if-eqz v0, :cond_0

    .line 2251284
    iget-object v0, p0, LX/FVp;->b:Lcom/facebook/saved/views/SavedDashboardEmptyView;

    invoke-virtual {v0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->a()V

    .line 2251285
    :cond_0
    return-void
.end method
