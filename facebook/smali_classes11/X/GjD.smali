.class public final LX/GjD;
.super Landroid/preference/PreferenceCategory;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2387288
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2387289
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 4

    .prologue
    .line 2387279
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2387280
    const-string v0, "Greeting Cards - Internal"

    invoke-virtual {p0, v0}, LX/GjD;->setTitle(Ljava/lang/CharSequence;)V

    .line 2387281
    invoke-virtual {p0}, LX/GjD;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2387282
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2387283
    const-string v2, "Prefilled card"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2387284
    const-string v2, "Go to your prefilled card."

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2387285
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/greetingcards/create/PreviewCardActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2387286
    invoke-virtual {p0, v1}, LX/GjD;->addPreference(Landroid/preference/Preference;)Z

    .line 2387287
    return-void
.end method
