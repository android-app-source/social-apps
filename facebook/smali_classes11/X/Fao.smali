.class public final LX/Fao;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fap;


# direct methods
.method public constructor <init>(LX/Fap;)V
    .locals 0

    .prologue
    .line 2259173
    iput-object p1, p0, LX/Fao;->a:LX/Fap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2259174
    iget-object v0, p0, LX/Fao;->a:LX/Fap;

    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->FETCH_AWARENESS_TUTORIAL_CONFIG_FAIL:LX/3Ql;

    invoke-direct {v1, v2, p1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2259175
    iget-object v2, v0, LX/Fap;->d:LX/2ST;

    if-eqz v2, :cond_0

    .line 2259176
    iget-object v2, v0, LX/Fap;->d:LX/2ST;

    invoke-interface {v2, v1}, LX/2ST;->a(LX/7C4;)V

    .line 2259177
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2259178
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2259179
    if-eqz p1, :cond_0

    .line 2259180
    iget-object v1, p0, LX/Fao;->a:LX/Fap;

    .line 2259181
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2259182
    check-cast v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    .line 2259183
    iget-object p0, v1, LX/Fap;->d:LX/2ST;

    if-eqz p0, :cond_0

    .line 2259184
    iget-object p0, v1, LX/Fap;->d:LX/2ST;

    invoke-interface {p0, v0}, LX/2ST;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)V

    .line 2259185
    :cond_0
    return-void
.end method
