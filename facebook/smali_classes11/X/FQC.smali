.class public final enum LX/FQC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQC;

.field public static final enum LARGE:LX/FQC;

.field public static final enum MEDIUM:LX/FQC;

.field public static final enum SMALL:LX/FQC;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2238862
    new-instance v0, LX/FQC;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v2}, LX/FQC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQC;->SMALL:LX/FQC;

    .line 2238863
    new-instance v0, LX/FQC;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, LX/FQC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQC;->MEDIUM:LX/FQC;

    .line 2238864
    new-instance v0, LX/FQC;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v4}, LX/FQC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQC;->LARGE:LX/FQC;

    .line 2238865
    const/4 v0, 0x3

    new-array v0, v0, [LX/FQC;

    sget-object v1, LX/FQC;->SMALL:LX/FQC;

    aput-object v1, v0, v2

    sget-object v1, LX/FQC;->MEDIUM:LX/FQC;

    aput-object v1, v0, v3

    sget-object v1, LX/FQC;->LARGE:LX/FQC;

    aput-object v1, v0, v4

    sput-object v0, LX/FQC;->$VALUES:[LX/FQC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2238866
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQC;
    .locals 1

    .prologue
    .line 2238867
    const-class v0, LX/FQC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQC;

    return-object v0
.end method

.method public static values()[LX/FQC;
    .locals 1

    .prologue
    .line 2238868
    sget-object v0, LX/FQC;->$VALUES:[LX/FQC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQC;

    return-object v0
.end method
