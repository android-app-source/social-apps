.class public LX/Gdb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0gh;


# direct methods
.method public constructor <init>(LX/0gh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2374353
    iput-object p1, p0, LX/Gdb;->a:LX/0gh;

    .line 2374354
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;Landroid/content/Context;ILjava/util/List;LX/Gda;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/Gda;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2374355
    iget-object v0, p0, LX/Gdb;->a:LX/0gh;

    .line 2374356
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2374357
    const-string v2, "index"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2374358
    const-string v2, "destination"

    invoke-interface {p4, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2374359
    const-string v2, "method"

    invoke-virtual {p5}, LX/Gda;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v1, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2374360
    const-string v2, "topic_feeds"

    invoke-interface {v1, v2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2374361
    move-object v1, v1

    .line 2374362
    const/4 v5, 0x0

    .line 2374363
    move-object v2, v0

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, v5

    move-object v8, v1

    invoke-static/range {v2 .. v8}, LX/0gh;->a(LX/0gh;Landroid/support/v4/app/Fragment;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2374364
    return-void
.end method
