.class public final LX/H7r;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2432041
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 2432042
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2432043
    :goto_0
    return v1

    .line 2432044
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2432045
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 2432046
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2432047
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2432048
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 2432049
    const-string v9, "app_data_for_offer"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2432050
    invoke-static {p0, p1}, LX/H7V;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2432051
    :cond_2
    const-string v9, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2432052
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2432053
    :cond_3
    const-string v9, "offer"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2432054
    invoke-static {p0, p1}, LX/H7c;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2432055
    :cond_4
    const-string v9, "photos"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2432056
    invoke-static {p0, p1}, LX/H7x;->b(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2432057
    :cond_5
    const-string v9, "root_share_story"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2432058
    invoke-static {p0, p1}, LX/H7q;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2432059
    :cond_6
    const-string v9, "videos"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2432060
    invoke-static {p0, p1}, LX/H80;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2432061
    :cond_7
    const-string v9, "viewer_claim"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2432062
    invoke-static {p0, p1}, LX/H7b;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2432063
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2432064
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2432065
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2432066
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2432067
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2432068
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2432069
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2432070
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2432071
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2432072
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2432073
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432074
    if-eqz v0, :cond_0

    .line 2432075
    const-string v1, "app_data_for_offer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432076
    invoke-static {p0, v0, p2, p3}, LX/H7V;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2432077
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2432078
    if-eqz v0, :cond_1

    .line 2432079
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432080
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2432081
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432082
    if-eqz v0, :cond_2

    .line 2432083
    const-string v1, "offer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432084
    invoke-static {p0, v0, p2, p3}, LX/H7c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2432085
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432086
    if-eqz v0, :cond_3

    .line 2432087
    const-string v1, "photos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432088
    invoke-static {p0, v0, p2, p3}, LX/H7x;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2432089
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432090
    if-eqz v0, :cond_4

    .line 2432091
    const-string v1, "root_share_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432092
    invoke-static {p0, v0, p2, p3}, LX/H7q;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2432093
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432094
    if-eqz v0, :cond_5

    .line 2432095
    const-string v1, "videos"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432096
    invoke-static {p0, v0, p2, p3}, LX/H80;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2432097
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432098
    if-eqz v0, :cond_6

    .line 2432099
    const-string v1, "viewer_claim"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2432100
    invoke-static {p0, v0, p2, p3}, LX/H7b;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2432101
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2432102
    return-void
.end method
