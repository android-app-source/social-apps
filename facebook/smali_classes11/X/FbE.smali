.class public LX/FbE;
.super LX/FbD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbD",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FbE;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2259696
    invoke-direct {p0}, LX/FbD;-><init>()V

    .line 2259697
    return-void
.end method

.method public static a(LX/0QB;)LX/FbE;
    .locals 3

    .prologue
    .line 2259698
    sget-object v0, LX/FbE;->a:LX/FbE;

    if-nez v0, :cond_1

    .line 2259699
    const-class v1, LX/FbE;

    monitor-enter v1

    .line 2259700
    :try_start_0
    sget-object v0, LX/FbE;->a:LX/FbE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2259701
    if-eqz v2, :cond_0

    .line 2259702
    :try_start_1
    new-instance v0, LX/FbE;

    invoke-direct {v0}, LX/FbE;-><init>()V

    .line 2259703
    move-object v0, v0

    .line 2259704
    sput-object v0, LX/FbE;->a:LX/FbE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2259705
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2259706
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2259707
    :cond_1
    sget-object v0, LX/FbE;->a:LX/FbE;

    return-object v0

    .line 2259708
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2259709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259710
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->q()Ljava/lang/String;

    move-result-object v1

    .line 2259711
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    .line 2259712
    if-eqz v1, :cond_0

    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->l()I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;-><init>(Ljava/lang/String;ILX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
