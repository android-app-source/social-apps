.class public final LX/FVk;
.super LX/FVj;
.source ""


# instance fields
.field public final synthetic a:LX/FVn;


# direct methods
.method public constructor <init>(LX/FVn;)V
    .locals 0

    .prologue
    .line 2251049
    iput-object p1, p0, LX/FVk;->a:LX/FVn;

    invoke-direct {p0}, LX/FVj;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2251031
    check-cast p1, LX/FW3;

    .line 2251032
    iget-object v0, p1, LX/FW3;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2251033
    iget-object v0, p0, LX/FVk;->a:LX/FVn;

    iget-object v0, v0, LX/FVn;->i:LX/03V;

    sget-object v1, LX/FVn;->a:Ljava/lang/String;

    const-string v2, "SavedItemReviewedEvent posted without page Id"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2251034
    :cond_0
    :goto_0
    return-void

    .line 2251035
    :cond_1
    iget-object v0, p0, LX/FVk;->a:LX/FVn;

    iget-object v0, v0, LX/FVn;->r:LX/FWC;

    iget-object v1, p1, LX/FW3;->b:Ljava/lang/String;

    .line 2251036
    iget-object v2, v0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_3

    iget-object v2, v0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FVs;

    .line 2251037
    instance-of v3, v2, LX/FVt;

    if-eqz v3, :cond_2

    move-object v3, v2

    check-cast v3, LX/FVt;

    .line 2251038
    iget-object p1, v3, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, p1

    .line 2251039
    if-eqz v3, :cond_2

    move-object v3, v2

    check-cast v3, LX/FVt;

    .line 2251040
    iget-object p1, v3, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, p1

    .line 2251041
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2251042
    check-cast v2, LX/FVt;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    .line 2251043
    :goto_2
    move-object v0, v2

    .line 2251044
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2251045
    iget-object v1, p0, LX/FVk;->a:LX/FVn;

    iget-object v1, v1, LX/FVn;->r:LX/FWC;

    .line 2251046
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVt;

    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/FVu;->a(LX/FVt;Z)LX/FVt;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/FWC;->a(LX/FVt;)V

    goto :goto_0

    .line 2251047
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 2251048
    :cond_3
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    goto :goto_2
.end method
