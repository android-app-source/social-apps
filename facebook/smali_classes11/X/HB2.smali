.class public final LX/HB2;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V
    .locals 0

    .prologue
    .line 2436328
    iput-object p1, p0, LX/HB2;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2436327
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2436320
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2436321
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436322
    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->a()Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2436323
    iget-object v0, p0, LX/HB2;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v1, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->i:LX/HBN;

    iget-object v0, p0, LX/HB2;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v2

    .line 2436324
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2436325
    check-cast v0, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel;->a()Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxLeadUpdateMutationsModels$PagesContactInboxLeadUpdateStateMutationModel$PageContactUsLeadModel;->j()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/HBN;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;)V

    .line 2436326
    :cond_0
    return-void
.end method
