.class public final enum LX/GOx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GOx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GOx;

.field public static final enum BRAZILIAN_TAX_ID:LX/GOx;

.field public static final enum DIALOG_US:LX/GOx;

.field public static final enum SCREEN_INTERNATIONAL:LX/GOx;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2348266
    new-instance v0, LX/GOx;

    const-string v1, "DIALOG_US"

    invoke-direct {v0, v1, v2}, LX/GOx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOx;->DIALOG_US:LX/GOx;

    .line 2348267
    new-instance v0, LX/GOx;

    const-string v1, "SCREEN_INTERNATIONAL"

    invoke-direct {v0, v1, v3}, LX/GOx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOx;->SCREEN_INTERNATIONAL:LX/GOx;

    .line 2348268
    new-instance v0, LX/GOx;

    const-string v1, "BRAZILIAN_TAX_ID"

    invoke-direct {v0, v1, v4}, LX/GOx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOx;->BRAZILIAN_TAX_ID:LX/GOx;

    .line 2348269
    const/4 v0, 0x3

    new-array v0, v0, [LX/GOx;

    sget-object v1, LX/GOx;->DIALOG_US:LX/GOx;

    aput-object v1, v0, v2

    sget-object v1, LX/GOx;->SCREEN_INTERNATIONAL:LX/GOx;

    aput-object v1, v0, v3

    sget-object v1, LX/GOx;->BRAZILIAN_TAX_ID:LX/GOx;

    aput-object v1, v0, v4

    sput-object v0, LX/GOx;->$VALUES:[LX/GOx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2348265
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GOx;
    .locals 1

    .prologue
    .line 2348263
    const-class v0, LX/GOx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GOx;

    return-object v0
.end method

.method public static values()[LX/GOx;
    .locals 1

    .prologue
    .line 2348264
    sget-object v0, LX/GOx;->$VALUES:[LX/GOx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GOx;

    return-object v0
.end method
