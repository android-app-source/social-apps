.class public LX/Ftz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:[Ljava/lang/String;

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;

.field private static h:LX/0Xm;


# instance fields
.field public final e:LX/0TD;

.field public final f:Landroid/content/Context;

.field public final g:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 2299421
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, LX/Ftz;->a:Landroid/net/Uri;

    .line 2299422
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "date_added"

    aput-object v1, v0, v5

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "media_type"

    aput-object v1, v0, v6

    sput-object v0, LX/Ftz;->b:[Ljava/lang/String;

    .line 2299423
    const-string v0, "%s = %d AND (LOWER(%s) LIKE \'%%dcim%%\' or LOWER(%s) LIKE \'%%camera%%\')"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "media_type"

    aput-object v2, v1, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "_data"

    aput-object v2, v1, v6

    const-string v2, "_data"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ftz;->c:Ljava/lang/String;

    .line 2299424
    const-string v0, "(%s = %d OR %s = %d) AND (LOWER(%s) LIKE \'%%dcim%%\' or LOWER(%s) LIKE \'%%camera%%\')"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "media_type"

    aput-object v2, v1, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "media_type"

    aput-object v2, v1, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "_data"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "_data"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ftz;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0TD;Landroid/content/Context;LX/0ad;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2299426
    iput-object p1, p0, LX/Ftz;->e:LX/0TD;

    .line 2299427
    iput-object p2, p0, LX/Ftz;->f:Landroid/content/Context;

    .line 2299428
    iput-object p3, p0, LX/Ftz;->g:LX/0ad;

    .line 2299429
    return-void
.end method

.method public static a(LX/0QB;)LX/Ftz;
    .locals 6

    .prologue
    .line 2299410
    const-class v1, LX/Ftz;

    monitor-enter v1

    .line 2299411
    :try_start_0
    sget-object v0, LX/Ftz;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299412
    sput-object v2, LX/Ftz;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299413
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299414
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299415
    new-instance p0, LX/Ftz;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/Ftz;-><init>(LX/0TD;Landroid/content/Context;LX/0ad;)V

    .line 2299416
    move-object v0, p0

    .line 2299417
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299418
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ftz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299419
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299420
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
