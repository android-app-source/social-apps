.class public LX/Gih;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0si;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2k0;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2386442
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2386443
    const-class v0, LX/Gih;

    invoke-static {v0, p0, p1}, LX/Gih;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 2386444
    const-string v0, "Trim GraphQL cache to minimum"

    invoke-virtual {p0, v0}, LX/Gih;->setTitle(Ljava/lang/CharSequence;)V

    .line 2386445
    const-string v0, "Call trimToMinimum on all GraphQL caches"

    invoke-virtual {p0, v0}, LX/Gih;->setSummary(Ljava/lang/CharSequence;)V

    .line 2386446
    new-instance v0, LX/Gig;

    invoke-direct {v0, p0}, LX/Gig;-><init>(LX/Gih;)V

    invoke-virtual {p0, v0}, LX/Gih;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2386447
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p1, LX/Gih;

    const/16 v1, 0xb0f

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xaee

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x12c4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v1, p1, LX/Gih;->a:LX/0Ot;

    iput-object v2, p1, LX/Gih;->b:LX/0Ot;

    iput-object v0, p1, LX/Gih;->c:LX/0Ot;

    return-void
.end method
