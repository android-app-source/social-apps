.class public final LX/HBg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final synthetic c:LX/34b;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:LX/HBh;


# direct methods
.method public constructor <init>(LX/HBh;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Lcom/facebook/graphql/enums/GraphQLPageActionType;LX/34b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2437780
    iput-object p1, p0, LX/HBg;->e:LX/HBh;

    iput-object p2, p0, LX/HBg;->a:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    iput-object p3, p0, LX/HBg;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object p4, p0, LX/HBg;->c:LX/34b;

    iput-object p5, p0, LX/HBg;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x694be165

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2437781
    iget-object v1, p0, LX/HBg;->e:LX/HBh;

    iget-object v1, v1, LX/HBh;->b:LX/HN9;

    iget-object v2, p0, LX/HBg;->a:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "pages_deeplink_simple_header_click_menu"

    iget-object v5, p0, LX/HBg;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/HN9;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 2437782
    iget-object v2, p0, LX/HBg;->c:LX/34b;

    .line 2437783
    invoke-static {v2}, LX/HBh;->a(LX/34b;)Z

    move-result v3

    move v1, v3

    .line 2437784
    if-nez v1, :cond_0

    .line 2437785
    const v1, 0x343d037

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2437786
    :goto_0
    return-void

    .line 2437787
    :cond_0
    new-instance v1, LX/3Af;

    iget-object v2, p0, LX/HBg;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2437788
    iget-object v2, p0, LX/HBg;->c:LX/34b;

    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2437789
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2437790
    const v1, 0x7cf3532

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
