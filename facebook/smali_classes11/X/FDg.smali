.class public LX/FDg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FDg;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2213075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2213076
    iput-object p1, p0, LX/FDg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2213077
    return-void
.end method

.method public static a(LX/0QB;)LX/FDg;
    .locals 4

    .prologue
    .line 2213062
    sget-object v0, LX/FDg;->b:LX/FDg;

    if-nez v0, :cond_1

    .line 2213063
    const-class v1, LX/FDg;

    monitor-enter v1

    .line 2213064
    :try_start_0
    sget-object v0, LX/FDg;->b:LX/FDg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2213065
    if-eqz v2, :cond_0

    .line 2213066
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2213067
    new-instance p0, LX/FDg;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/FDg;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2213068
    move-object v0, p0

    .line 2213069
    sput-object v0, LX/FDg;->b:LX/FDg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213070
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2213071
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213072
    :cond_1
    sget-object v0, LX/FDg;->b:LX/FDg;

    return-object v0

    .line 2213073
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2213074
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
