.class public LX/Fyr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BPp;

.field public final b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

.field public final c:LX/Fyq;

.field private final d:LX/Fz2;


# direct methods
.method public constructor <init>(LX/BPp;LX/Fz2;LX/Fyq;)V
    .locals 1
    .param p1    # LX/BPp;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Fz2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2307249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2307250
    iput-object p1, p0, LX/Fyr;->a:LX/BPp;

    .line 2307251
    iput-object p2, p0, LX/Fyr;->d:LX/Fz2;

    .line 2307252
    iget-object v0, p2, LX/Fz2;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    move-object v0, v0

    .line 2307253
    iput-object v0, p0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307254
    iput-object p3, p0, LX/Fyr;->c:LX/Fyq;

    .line 2307255
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2307227
    iget-object v0, p0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    invoke-virtual {v0, v1, v1}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307228
    iget-object v0, p0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307229
    iput-boolean v2, v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->c:Z

    .line 2307230
    iget-object v0, p0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Ljava/lang/String;)V

    .line 2307231
    iget-object v0, p0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307232
    iput-object v1, v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->e:Ljava/lang/String;

    .line 2307233
    iget-object v0, p0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307234
    iput-boolean v2, v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->g:Z

    .line 2307235
    iget-object v0, p0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307236
    iput-boolean v2, v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->h:Z

    .line 2307237
    iget-object v0, p0, LX/Fyr;->b:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2307238
    return-void
.end method

.method public static d(LX/Fyr;)V
    .locals 2

    .prologue
    .line 2307247
    iget-object v0, p0, LX/Fyr;->a:LX/BPp;

    new-instance v1, LX/BPI;

    invoke-direct {v1}, LX/BPI;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2307248
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;)V
    .locals 2

    .prologue
    .line 2307243
    invoke-direct {p0}, LX/Fyr;->c()V

    .line 2307244
    iget-object v0, p0, LX/Fyr;->d:LX/Fz2;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, LX/Fz2;->a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;I)V

    .line 2307245
    invoke-static {p0}, LX/Fyr;->d(LX/Fyr;)V

    .line 2307246
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2307239
    invoke-direct {p0}, LX/Fyr;->c()V

    .line 2307240
    iget-object v0, p0, LX/Fyr;->d:LX/Fz2;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, LX/Fz2;->a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;I)V

    .line 2307241
    invoke-static {p0}, LX/Fyr;->d(LX/Fyr;)V

    .line 2307242
    return-void
.end method
