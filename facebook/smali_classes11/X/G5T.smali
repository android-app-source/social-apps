.class public final LX/G5T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G5W;


# direct methods
.method public constructor <init>(LX/G5W;)V
    .locals 0

    .prologue
    .line 2318828
    iput-object p1, p0, LX/G5T;->a:LX/G5W;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    .line 2318836
    if-eqz p1, :cond_0

    .line 2318837
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    iget-object v0, v0, LX/G5W;->t:LX/G5P;

    .line 2318838
    sget-object v1, LX/G5O;->a:LX/G5N;

    const-string v2, "request_begin_to_response_end"

    invoke-static {v0, v1, v2}, LX/G5P;->b(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2318839
    :goto_0
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    iget-object v0, v0, LX/G5W;->t:LX/G5P;

    .line 2318840
    const-string v1, "results_were_received"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    .line 2318841
    sget-object v2, LX/G5O;->a:LX/G5N;

    .line 2318842
    iget-object v3, v0, LX/G5P;->a:LX/11i;

    iget-object v4, v0, LX/G5P;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v5

    invoke-interface {v3, v2, v1, v5, v6}, LX/11i;->b(LX/0Pq;LX/0P1;J)V

    .line 2318843
    return-void

    .line 2318844
    :cond_0
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    iget-object v0, v0, LX/G5W;->t:LX/G5P;

    .line 2318845
    sget-object v1, LX/G5O;->a:LX/G5N;

    const-string v2, "request_begin_to_response_end"

    invoke-static {v0, v1, v2}, LX/G5P;->c(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2318846
    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2318847
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    iget-boolean v0, v0, LX/G5W;->l:Z

    if-eqz v0, :cond_0

    .line 2318848
    :goto_0
    return-void

    .line 2318849
    :cond_0
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    sget-object v1, LX/G5W;->b:Ljava/util/List;

    .line 2318850
    iput-object v1, v0, LX/G5W;->r:Ljava/util/List;

    .line 2318851
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    invoke-static {v0}, LX/G5W;->g(LX/G5W;)V

    .line 2318852
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    iget-object v0, v0, LX/G5W;->v:LX/03V;

    sget-object v1, LX/G5W;->a:Ljava/lang/String;

    const-string v2, "Remote ubersearch failed: "

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2318853
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/G5T;->a(Z)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2318829
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2318830
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    iget-boolean v0, v0, LX/G5W;->l:Z

    if-eqz v0, :cond_0

    .line 2318831
    :goto_0
    return-void

    .line 2318832
    :cond_0
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableListNullOk()Ljava/util/ArrayList;

    move-result-object v1

    .line 2318833
    iput-object v1, v0, LX/G5W;->r:Ljava/util/List;

    .line 2318834
    iget-object v0, p0, LX/G5T;->a:LX/G5W;

    invoke-static {v0}, LX/G5W;->g(LX/G5W;)V

    .line 2318835
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/G5T;->a(Z)V

    goto :goto_0
.end method
