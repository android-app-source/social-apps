.class public LX/Fr1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Fr1;


# instance fields
.field private final a:LX/0Zm;

.field private final b:LX/0Zb;

.field private final c:LX/0SG;


# direct methods
.method public constructor <init>(LX/0Zm;LX/0Zb;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294919
    iput-object p1, p0, LX/Fr1;->a:LX/0Zm;

    .line 2294920
    iput-object p2, p0, LX/Fr1;->b:LX/0Zb;

    .line 2294921
    iput-object p3, p0, LX/Fr1;->c:LX/0SG;

    .line 2294922
    return-void
.end method

.method public static a(LX/0QB;)LX/Fr1;
    .locals 6

    .prologue
    .line 2294905
    sget-object v0, LX/Fr1;->d:LX/Fr1;

    if-nez v0, :cond_1

    .line 2294906
    const-class v1, LX/Fr1;

    monitor-enter v1

    .line 2294907
    :try_start_0
    sget-object v0, LX/Fr1;->d:LX/Fr1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2294908
    if-eqz v2, :cond_0

    .line 2294909
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2294910
    new-instance p0, LX/Fr1;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v3

    check-cast v3, LX/0Zm;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {p0, v3, v4, v5}, LX/Fr1;-><init>(LX/0Zm;LX/0Zb;LX/0SG;)V

    .line 2294911
    move-object v0, p0

    .line 2294912
    sput-object v0, LX/Fr1;->d:LX/Fr1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2294913
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2294914
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2294915
    :cond_1
    sget-object v0, LX/Fr1;->d:LX/Fr1;

    return-object v0

    .line 2294916
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2294917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/Fr1;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2294882
    iget-object v0, p0, LX/Fr1;->a:LX/0Zm;

    invoke-virtual {v0, p2}, LX/0Zm;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2294883
    :goto_0
    return-void

    .line 2294884
    :cond_0
    const-string v0, "deleted_rows_analytics"

    .line 2294885
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2294886
    invoke-static {p1, p3, p4}, LX/Fqx;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2294887
    invoke-virtual {v2, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2294888
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 2294889
    if-eqz v3, :cond_1

    .line 2294890
    const-string v4, "db_size"

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2294891
    :cond_1
    move-object v0, v2

    .line 2294892
    iget-object v1, p0, LX/Fr1;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    .line 2294899
    iget-object v0, p0, LX/Fr1;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2294900
    const-string v2, "fb4a_timeline_disk_cache_drop_table"

    .line 2294901
    const-string v3, "cache"

    invoke-static {v3}, LX/Fqy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2294902
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v0, v4

    .line 2294903
    invoke-static {p0, p1, v2, v3, v0}, LX/Fr1;->a(LX/Fr1;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 2294904
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 8

    .prologue
    .line 2294893
    iget-object v0, p0, LX/Fr1;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2294894
    const-string v2, "fb4a_timeline_disk_cache_trim_table"

    .line 2294895
    sget-object v3, LX/Fqy;->c:Ljava/lang/String;

    invoke-static {v3}, LX/Fqy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2294896
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v0, v4

    .line 2294897
    invoke-static {p0, p1, v2, v3, v0}, LX/Fr1;->a(LX/Fr1;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 2294898
    return-void
.end method
