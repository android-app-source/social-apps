.class public LX/GNm;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0gc;

.field private c:LX/0tX;

.field public final d:LX/ADW;

.field private e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2346549
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->CREDIT_CARD:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;->PAYPAL:Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/GNm;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0gc;LX/0tX;LX/ADW;)V
    .locals 0
    .param p1    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2346535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2346536
    iput-object p1, p0, LX/GNm;->b:LX/0gc;

    .line 2346537
    iput-object p2, p0, LX/GNm;->c:LX/0tX;

    .line 2346538
    iput-object p3, p0, LX/GNm;->d:LX/ADW;

    .line 2346539
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Ljava/lang/String;I)V
    .locals 8

    .prologue
    .line 2346540
    const v0, 0x7f080024

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v2

    .line 2346541
    iget-object v0, p0, LX/GNm;->b:LX/0gc;

    const-string v1, "show_payment_method_tag"

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2346542
    iget-object v0, p0, LX/GNm;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2346543
    :goto_0
    return-void

    .line 2346544
    :cond_0
    iget-object v0, p0, LX/GNm;->d:LX/ADW;

    const-string v1, "payments_select_new_payment_method_initiated"

    invoke-virtual {v0, v1, p1}, LX/ADW;->a(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2346545
    new-instance v0, LX/GPM;

    invoke-direct {v0}, LX/GPM;-><init>()V

    move-object v0, v0

    .line 2346546
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2346547
    iget-object v1, p0, LX/GNm;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/GNm;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2346548
    iget-object v7, p0, LX/GNm;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v0, LX/GNl;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, LX/GNl;-><init>(LX/GNm;Landroid/support/v4/app/DialogFragment;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Ljava/lang/String;I)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v1

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
