.class public LX/H5G;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/2kk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kl;",
        ":",
        "LX/1Pn;",
        ">",
        "LX/1S3;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/H5G;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H5I;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/H5G",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H5I;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2423941
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2423942
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/H5G;->b:LX/0Zi;

    .line 2423943
    iput-object p1, p0, LX/H5G;->a:LX/0Ot;

    .line 2423944
    return-void
.end method

.method public static a(LX/0QB;)LX/H5G;
    .locals 4

    .prologue
    .line 2423977
    sget-object v0, LX/H5G;->c:LX/H5G;

    if-nez v0, :cond_1

    .line 2423978
    const-class v1, LX/H5G;

    monitor-enter v1

    .line 2423979
    :try_start_0
    sget-object v0, LX/H5G;->c:LX/H5G;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2423980
    if-eqz v2, :cond_0

    .line 2423981
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2423982
    new-instance v3, LX/H5G;

    const/16 p0, 0x2ad5

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/H5G;-><init>(LX/0Ot;)V

    .line 2423983
    move-object v0, v3

    .line 2423984
    sput-object v0, LX/H5G;->c:LX/H5G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2423985
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2423986
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2423987
    :cond_1
    sget-object v0, LX/H5G;->c:LX/H5G;

    return-object v0

    .line 2423988
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2423989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/1X1;)V
    .locals 6

    .prologue
    .line 2423974
    check-cast p3, LX/H5F;

    .line 2423975
    iget-object v0, p0, LX/H5G;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5I;

    iget-object v3, p3, LX/H5F;->d:LX/2km;

    iget-object v4, p3, LX/H5F;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v5, p3, LX/H5F;->f:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/H5I;->onClick(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V

    .line 2423976
    return-void
.end method

.method public static onClick(LX/1X1;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2423973
    const v0, -0x517306c8

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2423951
    check-cast p2, LX/H5F;

    .line 2423952
    iget-object v0, p0, LX/H5G;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H5I;

    iget-object v2, p2, LX/H5F;->a:LX/0Px;

    iget-boolean v3, p2, LX/H5F;->b:Z

    iget-boolean v4, p2, LX/H5F;->c:Z

    iget-object v5, p2, LX/H5F;->d:LX/2km;

    iget-object v6, p2, LX/H5F;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    move-object v1, p1

    .line 2423953
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v7, 0x7f0a00d5

    invoke-interface {v0, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    const/4 v7, 0x7

    const v8, 0x7f0b163c

    invoke-interface {v0, v7, v8}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v0

    const/4 v7, 0x2

    invoke-interface {v0, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    .line 2423954
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v7, v0, :cond_3

    .line 2423955
    if-lez v7, :cond_0

    .line 2423956
    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const v8, 0x7f0a06e0

    invoke-virtual {v0, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v8, 0x7f0b0033

    invoke-interface {v0, v8}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    invoke-interface {v9, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    :cond_0
    move-object v0, v5

    .line 2423957
    check-cast v0, LX/1Pr;

    new-instance v8, LX/E2Q;

    .line 2423958
    iget-object v10, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v10, v10

    .line 2423959
    invoke-direct {v8, v10}, LX/E2Q;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v8, v6}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2R;

    .line 2423960
    iget-object v8, v0, LX/E2R;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2423961
    if-eqz v8, :cond_1

    .line 2423962
    iget-object v8, v0, LX/E2R;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2423963
    iget-object v0, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v10, v0

    .line 2423964
    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-static {v10, v0}, LX/H5I;->a(Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v8, v0

    .line 2423965
    :goto_1
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v10, 0x2

    invoke-interface {v0, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-interface {v0, v10}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    const/4 v10, 0x1

    invoke-interface {v0, v10}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const/4 v10, 0x7

    const p0, 0x7f0b163c

    invoke-interface {v0, v10, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v10

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    .line 2423966
    const p0, -0x517306c8

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v0, p1, p2

    invoke-static {v1, p0, p1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object v0, p0

    .line 2423967
    invoke-interface {v10, v0}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v10

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {v2, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    const p0, 0x7f0b0050

    invoke-virtual {v0, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    if-eqz v8, :cond_2

    const v0, 0x7f0a00d2

    :goto_2
    invoke-virtual {p0, v0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    invoke-interface {v10, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v9, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2423968
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0

    .line 2423969
    :cond_1
    const/4 v0, 0x0

    move v8, v0

    goto :goto_1

    .line 2423970
    :cond_2
    const v0, 0x7f0a010e

    goto :goto_2

    .line 2423971
    :cond_3
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v7, 0x6

    const v8, 0x7f010717

    invoke-interface {v0, v7, v8}, LX/1Dh;->p(II)LX/1Dh;

    move-result-object v0

    const v7, 0x7f0a00d5

    invoke-interface {v0, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    const/4 v7, 0x0

    invoke-interface {v0, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    if-eqz v3, :cond_4

    invoke-static {v1}, LX/H5I;->a(LX/1De;)LX/1Di;

    move-result-object v0

    :goto_3
    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    if-eqz v4, :cond_5

    invoke-static {v1}, LX/H5I;->a(LX/1De;)LX/1Di;

    move-result-object v0

    :goto_4
    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2423972
    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2423945
    invoke-static {}, LX/1dS;->b()V

    .line 2423946
    iget v0, p1, LX/1dQ;->b:I

    .line 2423947
    packed-switch v0, :pswitch_data_0

    .line 2423948
    :goto_0
    return-object v3

    .line 2423949
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2423950
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v1, v0, v2}, LX/H5G;->a(Landroid/view/View;Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/1X1;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x517306c8
        :pswitch_0
    .end packed-switch
.end method
