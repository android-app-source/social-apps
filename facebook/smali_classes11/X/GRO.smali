.class public final LX/GRO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GRR;

.field public final synthetic b:LX/GSs;

.field public final synthetic c:LX/GRP;


# direct methods
.method public constructor <init>(LX/GRP;LX/GRR;LX/GSs;)V
    .locals 0

    .prologue
    .line 2351521
    iput-object p1, p0, LX/GRO;->c:LX/GRP;

    iput-object p2, p0, LX/GRO;->a:LX/GRR;

    iput-object p3, p0, LX/GRO;->b:LX/GSs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x14df5592

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2351495
    iget-object v2, p0, LX/GRO;->c:LX/GRP;

    iget-object v2, v2, LX/GRP;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 2351496
    iget-object v2, p0, LX/GRO;->a:LX/GRR;

    iget-boolean v2, v2, LX/GRR;->d:Z

    if-eqz v2, :cond_0

    .line 2351497
    iget-object v2, p0, LX/GRO;->c:LX/GRP;

    iget-object v2, v2, LX/GRP;->a:LX/GRa;

    iget-object v3, p0, LX/GRO;->a:LX/GRR;

    iget-object v3, v3, LX/GRR;->a:Ljava/lang/String;

    .line 2351498
    new-instance v4, LX/4Cw;

    invoke-direct {v4}, LX/4Cw;-><init>()V

    .line 2351499
    const-string v5, "application_id"

    invoke-virtual {v4, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351500
    move-object v4, v4

    .line 2351501
    new-instance v5, LX/GS5;

    invoke-direct {v5}, LX/GS5;-><init>()V

    move-object v5, v5

    .line 2351502
    const-string p1, "input"

    invoke-virtual {v5, p1, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/GS5;

    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2351503
    iget-object v5, v2, LX/GRa;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2351504
    :goto_0
    iget-object v2, p0, LX/GRO;->a:LX/GRR;

    iget-object v3, p0, LX/GRO;->a:LX/GRR;

    iget-boolean v3, v3, LX/GRR;->d:Z

    if-nez v3, :cond_3

    :goto_1
    iput-boolean v0, v2, LX/GRR;->d:Z

    .line 2351505
    iget-object v0, p0, LX/GRO;->b:LX/GSs;

    iget-object v2, p0, LX/GRO;->a:LX/GRR;

    iget-boolean v2, v2, LX/GRR;->d:Z

    invoke-virtual {v0, v2}, LX/GSs;->setBlocked(Z)V

    .line 2351506
    const v0, 0x446231f3

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2351507
    :cond_0
    iget-object v2, p0, LX/GRO;->c:LX/GRP;

    iget-object v2, v2, LX/GRP;->a:LX/GRa;

    iget-object v3, p0, LX/GRO;->a:LX/GRR;

    iget-object v3, v3, LX/GRR;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/GRa;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2351508
    :cond_1
    iget-object v2, p0, LX/GRO;->a:LX/GRR;

    iget-boolean v2, v2, LX/GRR;->d:Z

    if-eqz v2, :cond_2

    .line 2351509
    iget-object v2, p0, LX/GRO;->c:LX/GRP;

    iget-object v2, v2, LX/GRP;->a:LX/GRa;

    iget-object v3, p0, LX/GRO;->a:LX/GRR;

    iget-object v3, v3, LX/GRR;->a:Ljava/lang/String;

    .line 2351510
    new-instance v4, LX/4Cx;

    invoke-direct {v4}, LX/4Cx;-><init>()V

    .line 2351511
    const-string v5, "user_id"

    invoke-virtual {v4, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351512
    move-object v4, v4

    .line 2351513
    new-instance v5, LX/GS6;

    invoke-direct {v5}, LX/GS6;-><init>()V

    move-object v5, v5

    .line 2351514
    const-string p1, "input"

    invoke-virtual {v5, p1, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/GS6;

    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2351515
    iget-object v5, v2, LX/GRa;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2351516
    goto :goto_0

    .line 2351517
    :cond_2
    iget-object v2, p0, LX/GRO;->c:LX/GRP;

    iget-object v2, v2, LX/GRP;->a:LX/GRa;

    iget-object v3, p0, LX/GRO;->a:LX/GRR;

    iget-object v3, v3, LX/GRR;->a:Ljava/lang/String;

    .line 2351518
    const-string v4, "630212950389510"

    invoke-virtual {v2, v3, v4}, LX/GRa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351519
    goto :goto_0

    .line 2351520
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
