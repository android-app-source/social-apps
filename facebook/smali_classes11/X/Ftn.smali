.class public final LX/Ftn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2299124
    iput-object p1, p0, LX/Ftn;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    iput-object p2, p0, LX/Ftn;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Ftn;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x58dade5a

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2299125
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;

    .line 2299126
    if-eqz v0, :cond_0

    .line 2299127
    const/4 v2, -0x1

    iget-object v4, p0, LX/Ftn;->a:Ljava/lang/String;

    iget-object v5, p0, LX/Ftn;->b:Ljava/lang/String;

    .line 2299128
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    .line 2299129
    const-string v3, "suggested_media_fb_id"

    invoke-virtual {p0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2299130
    const-string v3, "suggested_media_uri"

    invoke-virtual {p0, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2299131
    move-object p0, p0

    .line 2299132
    move-object v3, p0

    .line 2299133
    invoke-virtual {v0, v2, v3}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2299134
    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;->finish()V

    .line 2299135
    :cond_0
    const v0, 0x307de35e

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
