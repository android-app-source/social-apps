.class public final LX/F0p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "LX/0jQ;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2190812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2190813
    iput-object p1, p0, LX/F0p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190814
    iput-object p2, p0, LX/F0p;->b:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 2190815
    iput-boolean p3, p0, LX/F0p;->c:Z

    .line 2190816
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 2190817
    iget-object v0, p0, LX/F0p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2190818
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 2190819
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2190820
    iget-object v0, p0, LX/F0p;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
