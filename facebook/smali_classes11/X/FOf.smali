.class public final LX/FOf;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2235445
    const-class v1, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;

    const v0, 0x15360753

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "MessengerPhoneContactProfileMatch"

    const-string v6, "27070b89c02d31edb3e5cbcf650b8075"

    const-string v7, "reverse_contact_lookup"

    const-string v8, "10155252604376729"

    const/4 v9, 0x0

    .line 2235446
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2235447
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2235448
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2235450
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2235451
    sparse-switch v0, :sswitch_data_0

    .line 2235452
    :goto_0
    return-object p1

    .line 2235453
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2235454
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2235455
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2235456
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2235457
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2235458
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2235459
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2235460
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x12533aea -> :sswitch_4
        -0x132889c -> :sswitch_7
        0x66f18c8 -> :sswitch_0
        0x293cbd3b -> :sswitch_3
        0x2f1911b0 -> :sswitch_5
        0x3349e8c0 -> :sswitch_6
        0x666a8f05 -> :sswitch_2
        0x69308369 -> :sswitch_1
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 2235449
    new-instance v0, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatch$MessengerPhoneContactProfileMatchString$1;

    const-class v1, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatchModels$MessengerPhoneContactProfileMatchModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/messaging/users/phone/graphql/MessengerPhoneContactProfileMatch$MessengerPhoneContactProfileMatchString$1;-><init>(LX/FOf;Ljava/lang/Class;)V

    return-object v0
.end method
