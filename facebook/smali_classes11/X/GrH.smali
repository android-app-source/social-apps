.class public LX/GrH;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/widget/ImageView;

.field private b:LX/Ctg;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 2

    .prologue
    .line 2397836
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2397837
    iput-object p1, p0, LX/GrH;->b:LX/Ctg;

    .line 2397838
    iget-object v0, p0, LX/GrH;->b:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d1828

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/GrH;->a:Landroid/widget/ImageView;

    .line 2397839
    return-void
.end method


# virtual methods
.method public final a(LX/CrS;)V
    .locals 6

    .prologue
    .line 2397840
    iget-object v0, p0, LX/GrH;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 2397841
    :goto_0
    return-void

    .line 2397842
    :cond_0
    iget-object v0, p0, LX/GrH;->b:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 2397843
    iget-object v1, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, v1

    .line 2397844
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, LX/GrH;->a:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, LX/GrH;->a:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2397845
    iget-object v0, p0, LX/GrH;->b:LX/Ctg;

    iget-object v2, p0, LX/GrH;->a:Landroid/widget/ImageView;

    invoke-interface {v0, v2, v1}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0
.end method
