.class public final LX/Fg9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CyR;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/CyR",
        "<",
        "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final synthetic b:LX/8ci;

.field public final synthetic c:LX/FgF;


# direct methods
.method public constructor <init>(LX/FgF;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ci;)V
    .locals 0

    .prologue
    .line 2269672
    iput-object p1, p0, LX/Fg9;->c:LX/FgF;

    iput-object p2, p0, LX/Fg9;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iput-object p3, p0, LX/Fg9;->b:LX/8ci;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2269668
    check-cast p1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;

    .line 2269669
    iget-object v0, p0, LX/Fg9;->c:LX/FgF;

    const-string v1, "keyword_search_result_loader_key"

    iget-object v2, p0, LX/Fg9;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    const/4 v3, 0x0

    sget-object v5, LX/CwW;->BOOTSTRAP_ENTITIES:LX/CwW;

    iget-object v6, p0, LX/Fg9;->b:LX/8ci;

    const-string v7, "bootstrap_entities"

    move-object v4, p1

    .line 2269670
    invoke-static/range {v0 .. v7}, LX/FgF;->a$redex0(LX/FgF;Ljava/lang/String;LX/CwB;Ljava/lang/String;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;LX/CwW;LX/8ci;Ljava/lang/String;)V

    .line 2269671
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2269666
    iget-object v0, p0, LX/Fg9;->c:LX/FgF;

    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->RESULTS_DATA_LOADER_ERROR:LX/3Ql;

    invoke-direct {v1, v2, p1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/Throwable;)V

    invoke-static {v0, v1}, LX/FgF;->a$redex0(LX/FgF;LX/7C4;)V

    .line 2269667
    return-void
.end method
