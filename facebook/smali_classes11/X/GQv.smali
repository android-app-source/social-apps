.class public final LX/GQv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final accessTokenString:Ljava/lang/String;

.field public final applicationId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/AccessToken;)V
    .locals 2

    .prologue
    .line 2350907
    iget-object v0, p1, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    move-object v0, v0

    .line 2350908
    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/GQv;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2350909
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2350915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2350916
    invoke-static {p1}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    iput-object p1, p0, LX/GQv;->accessTokenString:Ljava/lang/String;

    .line 2350917
    iput-object p2, p0, LX/GQv;->applicationId:Ljava/lang/String;

    .line 2350918
    return-void
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2350919
    new-instance v0, LX/GQu;

    iget-object v1, p0, LX/GQv;->accessTokenString:Ljava/lang/String;

    iget-object v2, p0, LX/GQv;->applicationId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/GQu;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2350911
    instance-of v1, p1, LX/GQv;

    if-nez v1, :cond_1

    .line 2350912
    :cond_0
    :goto_0
    return v0

    .line 2350913
    :cond_1
    check-cast p1, LX/GQv;

    .line 2350914
    iget-object v1, p1, LX/GQv;->accessTokenString:Ljava/lang/String;

    iget-object v2, p0, LX/GQv;->accessTokenString:Ljava/lang/String;

    invoke-static {v1, v2}, LX/Gsc;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/GQv;->applicationId:Ljava/lang/String;

    iget-object v2, p0, LX/GQv;->applicationId:Ljava/lang/String;

    invoke-static {v1, v2}, LX/Gsc;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2350910
    iget-object v0, p0, LX/GQv;->accessTokenString:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v2, p0, LX/GQv;->applicationId:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, LX/GQv;->accessTokenString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, LX/GQv;->applicationId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
