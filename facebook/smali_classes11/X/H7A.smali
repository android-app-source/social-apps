.class public final LX/H7A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:J

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2428435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .locals 15

    .prologue
    .line 2428436
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2428437
    iget-object v1, p0, LX/H7A;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2428438
    iget-object v2, p0, LX/H7A;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2428439
    iget-object v3, p0, LX/H7A;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2428440
    iget-object v4, p0, LX/H7A;->f:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2428441
    iget-object v5, p0, LX/H7A;->g:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2428442
    iget-object v6, p0, LX/H7A;->h:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2428443
    iget-object v7, p0, LX/H7A;->i:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2428444
    iget-object v8, p0, LX/H7A;->k:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2428445
    iget-object v9, p0, LX/H7A;->m:Ljava/lang/String;

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2428446
    iget-object v10, p0, LX/H7A;->n:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    invoke-static {v0, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2428447
    iget-object v11, p0, LX/H7A;->o:Ljava/lang/String;

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2428448
    const/16 v12, 0xf

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 2428449
    const/4 v12, 0x0

    iget v13, p0, LX/H7A;->a:I

    const/4 v14, 0x0

    invoke-virtual {v0, v12, v13, v14}, LX/186;->a(III)V

    .line 2428450
    const/4 v12, 0x1

    invoke-virtual {v0, v12, v1}, LX/186;->b(II)V

    .line 2428451
    const/4 v1, 0x2

    iget-boolean v12, p0, LX/H7A;->c:Z

    invoke-virtual {v0, v1, v12}, LX/186;->a(IZ)V

    .line 2428452
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2428453
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2428454
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2428455
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2428456
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2428457
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2428458
    const/16 v1, 0x9

    iget-wide v2, p0, LX/H7A;->j:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2428459
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2428460
    const/16 v1, 0xb

    iget-boolean v2, p0, LX/H7A;->l:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2428461
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2428462
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2428463
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2428464
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2428465
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2428466
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2428467
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2428468
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2428469
    new-instance v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    invoke-direct {v1, v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;-><init>(LX/15i;)V

    .line 2428470
    return-object v1
.end method
