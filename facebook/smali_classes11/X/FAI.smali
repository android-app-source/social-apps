.class public final enum LX/FAI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FAI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FAI;

.field public static final enum DISABLED:LX/FAI;

.field public static final enum ENFORCE:LX/FAI;

.field public static final enum ENFORCE_TOR:LX/FAI;

.field public static final enum LOG:LX/FAI;

.field public static final enum LOG_TOR:LX/FAI;

.field public static final enum REFUSE:LX/FAI;

.field public static final enum REFUSE_TOR:LX/FAI;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2206882
    new-instance v0, LX/FAI;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3}, LX/FAI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FAI;->DISABLED:LX/FAI;

    .line 2206883
    new-instance v0, LX/FAI;

    const-string v1, "LOG"

    invoke-direct {v0, v1, v4}, LX/FAI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FAI;->LOG:LX/FAI;

    .line 2206884
    new-instance v0, LX/FAI;

    const-string v1, "ENFORCE"

    invoke-direct {v0, v1, v5}, LX/FAI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FAI;->ENFORCE:LX/FAI;

    .line 2206885
    new-instance v0, LX/FAI;

    const-string v1, "REFUSE"

    invoke-direct {v0, v1, v6}, LX/FAI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FAI;->REFUSE:LX/FAI;

    .line 2206886
    new-instance v0, LX/FAI;

    const-string v1, "LOG_TOR"

    invoke-direct {v0, v1, v7}, LX/FAI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FAI;->LOG_TOR:LX/FAI;

    .line 2206887
    new-instance v0, LX/FAI;

    const-string v1, "ENFORCE_TOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FAI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FAI;->ENFORCE_TOR:LX/FAI;

    .line 2206888
    new-instance v0, LX/FAI;

    const-string v1, "REFUSE_TOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FAI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FAI;->REFUSE_TOR:LX/FAI;

    .line 2206889
    const/4 v0, 0x7

    new-array v0, v0, [LX/FAI;

    sget-object v1, LX/FAI;->DISABLED:LX/FAI;

    aput-object v1, v0, v3

    sget-object v1, LX/FAI;->LOG:LX/FAI;

    aput-object v1, v0, v4

    sget-object v1, LX/FAI;->ENFORCE:LX/FAI;

    aput-object v1, v0, v5

    sget-object v1, LX/FAI;->REFUSE:LX/FAI;

    aput-object v1, v0, v6

    sget-object v1, LX/FAI;->LOG_TOR:LX/FAI;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FAI;->ENFORCE_TOR:LX/FAI;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FAI;->REFUSE_TOR:LX/FAI;

    aput-object v2, v0, v1

    sput-object v0, LX/FAI;->$VALUES:[LX/FAI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2206890
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FAI;
    .locals 1

    .prologue
    .line 2206891
    const-class v0, LX/FAI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FAI;

    return-object v0
.end method

.method public static values()[LX/FAI;
    .locals 1

    .prologue
    .line 2206892
    sget-object v0, LX/FAI;->$VALUES:[LX/FAI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FAI;

    return-object v0
.end method
