.class public LX/FiI;
.super LX/Fi8;
.source ""


# instance fields
.field private final a:LX/FgS;

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(LX/FgS;LX/0Uh;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2274930
    invoke-direct {p0}, LX/Fi8;-><init>()V

    .line 2274931
    iput-object p1, p0, LX/FiI;->a:LX/FgS;

    .line 2274932
    sget-short v0, LX/100;->K:S

    invoke-interface {p3, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/FiI;->b:Z

    .line 2274933
    sget v0, LX/2SU;->y:I

    invoke-virtual {p2, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/FiI;->c:Z

    .line 2274934
    return-void
.end method

.method private a(LX/CwI;)Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 3

    .prologue
    .line 2274909
    iget-object v0, p0, LX/FiI;->a:LX/FgS;

    invoke-virtual {v0}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v0

    .line 2274910
    new-instance v1, LX/CwH;

    invoke-direct {v1}, LX/CwH;-><init>()V

    .line 2274911
    iput-object v0, v1, LX/CwH;->b:Ljava/lang/String;

    .line 2274912
    move-object v1, v1

    .line 2274913
    iput-object v0, v1, LX/CwH;->d:Ljava/lang/String;

    .line 2274914
    move-object v1, v1

    .line 2274915
    const-string v2, "content"

    .line 2274916
    iput-object v2, v1, LX/CwH;->e:Ljava/lang/String;

    .line 2274917
    move-object v1, v1

    .line 2274918
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2274919
    iput-object v2, v1, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2274920
    move-object v1, v1

    .line 2274921
    iput-object p1, v1, LX/CwH;->l:LX/CwI;

    .line 2274922
    move-object v1, v1

    .line 2274923
    invoke-static {v0}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2274924
    iput-object v0, v1, LX/CwH;->c:Ljava/lang/String;

    .line 2274925
    move-object v1, v1

    .line 2274926
    iget-boolean v0, p0, LX/FiI;->c:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/CwF;->escape:LX/CwF;

    .line 2274927
    :goto_0
    iput-object v0, v1, LX/CwH;->g:LX/CwF;

    .line 2274928
    move-object v0, v1

    .line 2274929
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, LX/CwF;->escape_pps_style:LX/CwF;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2274935
    iget-object v0, p0, LX/FiI;->a:LX/FgS;

    invoke-virtual {v0}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2274936
    iget-object v0, p2, LX/7Hc;->b:LX/0Px;

    move-object v0, v0

    .line 2274937
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274938
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2274939
    :goto_0
    return-object v0

    .line 2274940
    :cond_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2274941
    iget-object v0, p2, LX/7Hc;->b:LX/0Px;

    move-object v3, v0

    .line 2274942
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274943
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v5

    sget-object v6, LX/Cwb;->KEYWORD:LX/Cwb;

    if-eq v5, v6, :cond_1

    .line 2274944
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274945
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2274946
    :cond_2
    iget-boolean v0, p0, LX/FiI;->c:Z

    if-eqz v0, :cond_3

    .line 2274947
    sget-object v0, LX/CwI;->ESCAPE:LX/CwI;

    invoke-direct {p0, v0}, LX/FiI;->a(LX/CwI;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    .line 2274948
    :goto_2
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274949
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2274950
    :cond_3
    iget-boolean v0, p0, LX/FiI;->b:Z

    if-eqz v0, :cond_4

    .line 2274951
    sget-object v0, LX/CwI;->SS_SEE_MORE_LINK:LX/CwI;

    invoke-direct {p0, v0}, LX/FiI;->a(LX/CwI;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    goto :goto_2

    .line 2274952
    :cond_4
    new-instance v0, Lcom/facebook/search/model/SeeMoreTypeaheadUnit;

    const-string v1, "see_more_link"

    invoke-direct {v0, v1}, Lcom/facebook/search/model/SeeMoreTypeaheadUnit;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 2

    .prologue
    .line 2274903
    iget-boolean v0, p0, LX/FiI;->c:Z

    if-eqz v0, :cond_0

    .line 2274904
    sget-object v0, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    invoke-direct {p0, v0}, LX/FiI;->a(LX/CwI;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    .line 2274905
    :goto_0
    return-object v0

    .line 2274906
    :cond_0
    iget-boolean v0, p0, LX/FiI;->b:Z

    if-eqz v0, :cond_1

    .line 2274907
    sget-object v0, LX/CwI;->SS_SEE_MORE_BUTTON:LX/CwI;

    invoke-direct {p0, v0}, LX/FiI;->a(LX/CwI;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    goto :goto_0

    .line 2274908
    :cond_1
    new-instance v0, Lcom/facebook/search/model/SeeMoreTypeaheadUnit;

    const-string v1, "search_button"

    invoke-direct {v0, v1}, Lcom/facebook/search/model/SeeMoreTypeaheadUnit;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
