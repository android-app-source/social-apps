.class public LX/HBS;
.super LX/5SB;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/HBQ;


# direct methods
.method private constructor <init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)V
    .locals 1

    .prologue
    .line 2437188
    invoke-direct/range {p0 .. p7}, LX/5SB;-><init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;)V

    .line 2437189
    iput-object p8, p0, LX/HBS;->a:Ljava/lang/String;

    .line 2437190
    iput-object p9, p0, LX/HBS;->b:LX/HBQ;

    .line 2437191
    return-void
.end method

.method public static a(JJLjava/lang/String;Landroid/os/ParcelUuid;Ljava/lang/String;LX/8A4;)LX/HBS;
    .locals 14

    .prologue
    .line 2437197
    new-instance v3, LX/HBS;

    sget-object v9, LX/5SA;->PAGE:LX/5SA;

    new-instance v12, LX/HBR;

    move-object/from16 v0, p7

    invoke-direct {v12, v0}, LX/HBR;-><init>(LX/8A4;)V

    move-wide v4, p0

    move-wide/from16 v6, p2

    move-object/from16 v8, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    invoke-direct/range {v3 .. v12}, LX/HBS;-><init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)V

    return-object v3
.end method

.method public static a(JJLjava/lang/String;Landroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)LX/HBS;
    .locals 12

    .prologue
    .line 2437196
    new-instance v1, LX/HBS;

    sget-object v7, LX/5SA;->PAGE_IDENTITY:LX/5SA;

    move-wide v2, p0

    move-wide v4, p2

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    invoke-direct/range {v1 .. v10}, LX/HBS;-><init>(JJLjava/lang/String;LX/5SA;Landroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)V

    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2437195
    iget-object v0, p0, LX/HBS;->b:LX/HBQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HBS;->b:LX/HBQ;

    invoke-interface {v0}, LX/HBQ;->a()LX/8A4;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HBS;->b:LX/HBQ;

    invoke-interface {v0}, LX/HBQ;->a()LX/8A4;

    move-result-object v0

    sget-object v1, LX/8A3;->MODERATE_CONTENT:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2437198
    iget-object v0, p0, LX/HBS;->b:LX/HBQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HBS;->b:LX/HBQ;

    invoke-interface {v0}, LX/HBQ;->a()LX/8A4;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HBS;->b:LX/HBQ;

    invoke-interface {v0}, LX/HBQ;->a()LX/8A4;

    move-result-object v0

    sget-object v1, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2437194
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2437193
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2437192
    iget-object v0, p0, LX/HBS;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method
