.class public LX/FaC;
.super LX/2s5;
.source ""


# instance fields
.field private final a:LX/Fam;

.field private final b:LX/Fas;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0hL;


# direct methods
.method public constructor <init>(LX/Fan;LX/0gc;LX/Fas;Landroid/content/Context;LX/0Uh;LX/0hL;)V
    .locals 2
    .param p2    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2258440
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2258441
    sget v0, LX/2SU;->q:I

    const/4 v1, 0x0

    invoke-virtual {p5, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2258442
    iget-object v0, p3, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-object v0, v0

    .line 2258443
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2258444
    iget-object v0, p3, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-object v0, v0

    .line 2258445
    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2258446
    iput-object p3, p0, LX/FaC;->b:LX/Fas;

    .line 2258447
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {p1, v0}, LX/Fan;->a(I)LX/Fam;

    move-result-object v0

    iput-object v0, p0, LX/FaC;->a:LX/Fam;

    .line 2258448
    iput-object p6, p0, LX/FaC;->d:LX/0hL;

    .line 2258449
    iget-object v0, p0, LX/FaC;->b:LX/Fas;

    .line 2258450
    iget-object v1, v0, LX/Fas;->l:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;

    move-object v0, v1

    .line 2258451
    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v0

    invoke-direct {p0, v0}, LX/FaC;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/FaC;->c:LX/0Px;

    .line 2258452
    iget-object v0, p0, LX/FaC;->d:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2258453
    iget-object v0, p0, LX/FaC;->c:LX/0Px;

    if-nez v0, :cond_1

    .line 2258454
    :cond_0
    :goto_0
    return-void

    .line 2258455
    :cond_1
    iget-object v0, p0, LX/FaC;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->reverse()LX/0Px;

    goto :goto_0
.end method

.method private a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)LX/0Px;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2258456
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2258457
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->j()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v8, v0, LX/1vs;->b:I

    .line 2258458
    new-instance v0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    const/4 v1, 0x2

    invoke-virtual {v5, v8, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v5, v8, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/FaC;->a:LX/Fam;

    invoke-virtual {v3, v5, v8}, LX/Fam;->a(LX/15i;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v5, v8, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "#"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x1

    invoke-virtual {v5, v8, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;I)V

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2258459
    :cond_0
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2258460
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/FaC;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2258461
    :cond_0
    const/4 v0, 0x0

    .line 2258462
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/FaC;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardConfiguration;

    .line 2258463
    new-instance v1, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;

    invoke-direct {v1}, Lcom/facebook/search/quickpromotion/AwarenessTutorialNuxCardFragment;-><init>()V

    .line 2258464
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2258465
    const-string p1, "configuration"

    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2258466
    invoke-virtual {v1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2258467
    move-object v0, v1

    .line 2258468
    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2258469
    iget-object v0, p0, LX/FaC;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
