.class public LX/FFv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final l:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Sh;

.field private final b:LX/0SG;

.field public final c:LX/3fX;

.field private final d:LX/2Og;

.field private final e:LX/FDp;

.field private final f:LX/03V;

.field private final g:Lcom/facebook/messaging/localfetch/FetchUserUtil;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/3fr;

.field public final j:LX/2Oi;

.field private final k:LX/2RQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2218059
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FFv;->l:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0SG;LX/3fX;LX/2Og;LX/FDp;LX/3fr;Lcom/facebook/messaging/localfetch/FetchUserUtil;LX/0Or;LX/03V;LX/2Oi;LX/2RQ;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/sync/annotations/IsCheckUserLocalEnabledGk;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0SG;",
            "LX/3fX;",
            "LX/2Og;",
            "LX/FDp;",
            "LX/3fr;",
            "Lcom/facebook/messaging/localfetch/FetchUserUtil;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2Oi;",
            "LX/2RQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2218046
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2218047
    iput-object p1, p0, LX/FFv;->a:LX/0Sh;

    .line 2218048
    iput-object p2, p0, LX/FFv;->b:LX/0SG;

    .line 2218049
    iput-object p6, p0, LX/FFv;->i:LX/3fr;

    .line 2218050
    iput-object p3, p0, LX/FFv;->c:LX/3fX;

    .line 2218051
    iput-object p4, p0, LX/FFv;->d:LX/2Og;

    .line 2218052
    iput-object p5, p0, LX/FFv;->e:LX/FDp;

    .line 2218053
    iput-object p7, p0, LX/FFv;->g:Lcom/facebook/messaging/localfetch/FetchUserUtil;

    .line 2218054
    iput-object p8, p0, LX/FFv;->h:LX/0Or;

    .line 2218055
    iput-object p9, p0, LX/FFv;->f:LX/03V;

    .line 2218056
    iput-object p10, p0, LX/FFv;->j:LX/2Oi;

    .line 2218057
    iput-object p11, p0, LX/FFv;->k:LX/2RQ;

    .line 2218058
    return-void
.end method

.method public static a(Lcom/facebook/user/model/User;LX/0Px;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/user/model/User;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2218042
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2218043
    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2218044
    invoke-virtual {v0, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2218045
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/FFv;
    .locals 7

    .prologue
    .line 2217960
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2217961
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2217962
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2217963
    if-nez v1, :cond_0

    .line 2217964
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2217965
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2217966
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2217967
    sget-object v1, LX/FFv;->l:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2217968
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2217969
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2217970
    :cond_1
    if-nez v1, :cond_4

    .line 2217971
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2217972
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2217973
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FFv;->b(LX/0QB;)LX/FFv;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2217974
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2217975
    if-nez v1, :cond_2

    .line 2217976
    sget-object v0, LX/FFv;->l:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FFv;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2217977
    :goto_1
    if-eqz v0, :cond_3

    .line 2217978
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2217979
    :goto_3
    check-cast v0, LX/FFv;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2217980
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2217981
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2217982
    :catchall_1
    move-exception v0

    .line 2217983
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2217984
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2217985
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2217986
    :cond_2
    :try_start_8
    sget-object v0, LX/FFv;->l:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FFv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/FFv;
    .locals 12

    .prologue
    .line 2218040
    new-instance v0, LX/FFv;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/3fX;->a(LX/0QB;)LX/3fX;

    move-result-object v3

    check-cast v3, LX/3fX;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v4

    check-cast v4, LX/2Og;

    invoke-static {p0}, LX/FDp;->a(LX/0QB;)LX/FDp;

    move-result-object v5

    check-cast v5, LX/FDp;

    invoke-static {p0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v6

    check-cast v6, LX/3fr;

    invoke-static {p0}, Lcom/facebook/messaging/localfetch/FetchUserUtil;->b(LX/0QB;)Lcom/facebook/messaging/localfetch/FetchUserUtil;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/localfetch/FetchUserUtil;

    const/16 v8, 0x152f

    invoke-static {p0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v10

    check-cast v10, LX/2Oi;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v11

    check-cast v11, LX/2RQ;

    invoke-direct/range {v0 .. v11}, LX/FFv;-><init>(LX/0Sh;LX/0SG;LX/3fX;LX/2Og;LX/FDp;LX/3fr;Lcom/facebook/messaging/localfetch/FetchUserUtil;LX/0Or;LX/03V;LX/2Oi;LX/2RQ;)V

    .line 2218041
    return-object v0
.end method

.method public static c(LX/FFv;Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2218016
    iget-object v0, p0, LX/FFv;->a:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 2218017
    iget-object v0, p0, LX/FFv;->e:LX/FDp;

    invoke-virtual {v0, p1}, LX/FDp;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v1

    .line 2218018
    if-eqz v1, :cond_0

    .line 2218019
    :goto_0
    return-object v1

    .line 2218020
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/FFv;->i:LX/3fr;

    iget-object v3, p0, LX/FFv;->k:LX/2RQ;

    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2RQ;->a(Ljava/lang/String;)LX/2RR;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/3fr;->a(LX/2RR;)LX/6N1;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 2218021
    :try_start_1
    invoke-interface {v3}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2218022
    if-eqz v0, :cond_6

    .line 2218023
    iget-object v1, p0, LX/FFv;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->F()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 2218024
    const-wide/32 v6, 0x5265c00

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 2218025
    invoke-virtual {p1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2218026
    if-eqz v3, :cond_1

    .line 2218027
    invoke-interface {v3}, LX/6N1;->close()V

    :cond_1
    move-object v1, v2

    goto :goto_0

    .line 2218028
    :cond_2
    :try_start_2
    invoke-static {v0}, LX/6Ok;->a(Lcom/facebook/contacts/graphql/Contact;)Lcom/facebook/user/model/User;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 2218029
    :goto_1
    if-eqz v3, :cond_3

    .line 2218030
    invoke-interface {v3}, LX/6N1;->close()V

    :cond_3
    move-object v1, v0

    goto :goto_0

    .line 2218031
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 2218032
    :goto_2
    :try_start_3
    iget-object v3, p0, LX/FFv;->f:LX/03V;

    const-string v4, "FetchUserHandler"

    const-string v5, "Exception raised while fetching contact for database."

    invoke-virtual {v3, v4, v5, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2218033
    if-eqz v1, :cond_4

    .line 2218034
    invoke-interface {v1}, LX/6N1;->close()V

    :cond_4
    move-object v1, v2

    .line 2218035
    goto :goto_0

    .line 2218036
    :catchall_0
    move-exception v0

    move-object v3, v2

    :goto_3
    if-eqz v3, :cond_5

    .line 2218037
    invoke-interface {v3}, LX/6N1;->close()V

    :cond_5
    throw v0

    .line 2218038
    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v3, v1

    goto :goto_3

    .line 2218039
    :catch_1
    move-exception v0

    move-object v1, v3

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/util/Set;)LX/0P1;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;)",
            "LX/0P1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2217987
    iget-object v0, p0, LX/FFv;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 2217988
    :goto_0
    return-object v0

    .line 2217989
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v3

    .line 2217990
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2217991
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/user/model/UserKey;

    move-result-object v1

    .line 2217992
    if-nez v1, :cond_1

    move-object v0, v2

    .line 2217993
    goto :goto_0

    .line 2217994
    :cond_1
    iget-object v5, p0, LX/FFv;->g:Lcom/facebook/messaging/localfetch/FetchUserUtil;

    .line 2217995
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2217996
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 2217997
    invoke-static {v5, v6}, Lcom/facebook/messaging/localfetch/FetchUserUtil;->b(Lcom/facebook/messaging/localfetch/FetchUserUtil;Ljava/util/List;)LX/1MF;

    move-result-object v6

    .line 2217998
    invoke-interface {v6}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v6

    new-instance v7, LX/FFw;

    invoke-direct {v7, v5}, LX/FFw;-><init>(Lcom/facebook/messaging/localfetch/FetchUserUtil;)V

    invoke-static {v6, v7}, LX/2Ck;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v1, v6

    .line 2217999
    if-nez v1, :cond_2

    move-object v0, v2

    .line 2218000
    goto :goto_0

    .line 2218001
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2218002
    const-wide/16 v6, 0x1

    :try_start_0
    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const v9, -0x445eb2be

    invoke-static {v1, v6, v7, v8, v9}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2

    .line 2218003
    :goto_2
    if-nez v1, :cond_3

    move-object v0, v2

    .line 2218004
    goto :goto_0

    .line 2218005
    :catch_0
    move-exception v1

    .line 2218006
    iget-object v6, p0, LX/FFv;->f:LX/03V;

    const-string v7, "FetchUserHandler"

    const-string v8, "InterruptedException raised while waiting for contact fetching futures to return."

    invoke-virtual {v6, v7, v8, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    .line 2218007
    goto :goto_2

    .line 2218008
    :catch_1
    move-exception v1

    .line 2218009
    iget-object v6, p0, LX/FFv;->f:LX/03V;

    const-string v7, "FetchUserHandler"

    const-string v8, "ExecutionException raised while waiting for contact fetching futures to return."

    invoke-virtual {v6, v7, v8, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    .line 2218010
    goto :goto_2

    .line 2218011
    :catch_2
    move-exception v1

    .line 2218012
    iget-object v6, p0, LX/FFv;->f:LX/03V;

    const-string v7, "FetchUserHandler"

    const-string v8, "TimeoutException raised while waiting for contact fetching futures to return."

    invoke-virtual {v6, v7, v8, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    goto :goto_2

    .line 2218013
    :cond_3
    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2218014
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 2218015
    :cond_4
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto/16 :goto_0
.end method
