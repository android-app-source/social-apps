.class public final LX/FpL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FpN;


# direct methods
.method public constructor <init>(LX/FpN;)V
    .locals 0

    .prologue
    .line 2291435
    iput-object p1, p0, LX/FpL;->a:LX/FpN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2291436
    iget-object v0, p0, LX/FpL;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-virtual {v0, v1, v1}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Ljava/lang/String;)V

    .line 2291437
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2291438
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2291439
    if-eqz p1, :cond_0

    .line 2291440
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2291441
    if-nez v0, :cond_1

    .line 2291442
    :cond_0
    iget-object v0, p0, LX/FpL;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-virtual {v0, v1, v1}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Ljava/lang/String;)V

    .line 2291443
    :goto_0
    return-void

    .line 2291444
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2291445
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_2

    .line 2291446
    :goto_1
    iget-object v0, p0, LX/FpL;->a:LX/FpN;

    iget-object v2, v0, LX/FpN;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    .line 2291447
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2291448
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;Ljava/lang/String;)V

    goto :goto_0

    .line 2291449
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2291450
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserNfgDialogModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2291451
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method
