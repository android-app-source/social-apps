.class public LX/Fha;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7HW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7HW",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273333
    return-void
.end method

.method public static a(LX/0QB;)LX/Fha;
    .locals 1

    .prologue
    .line 2273334
    new-instance v0, LX/Fha;

    invoke-direct {v0}, LX/Fha;-><init>()V

    .line 2273335
    move-object v0, v0

    .line 2273336
    return-object v0
.end method

.method private static a(Lcom/facebook/search/model/KeywordTypeaheadUnit;D)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 4

    .prologue
    .line 2273337
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2273338
    invoke-static {p0}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v0

    const/4 v1, 0x1

    .line 2273339
    iput-boolean v1, v0, LX/CwH;->n:Z

    .line 2273340
    move-object v0, v0

    .line 2273341
    iput-wide p1, v0, LX/CwH;->o:D

    .line 2273342
    move-object v0, v0

    .line 2273343
    sget-object v1, LX/CwI;->BOOTSTRAP:LX/CwI;

    .line 2273344
    iput-object v1, v0, LX/CwH;->l:LX/CwI;

    .line 2273345
    move-object v0, v0

    .line 2273346
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/model/KeywordTypeaheadUnit;LX/7HY;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 6

    .prologue
    .line 2273347
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v0

    .line 2273348
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v1

    .line 2273349
    sget-object v2, LX/CwF;->keyword:LX/CwF;

    if-ne v0, v2, :cond_2

    sget-object v2, LX/CwF;->local:LX/CwF;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/CwF;->local_category:LX/CwF;

    if-ne v1, v2, :cond_2

    .line 2273350
    :cond_0
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2273351
    invoke-static {p0}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v0

    .line 2273352
    iput-object v1, v0, LX/CwH;->g:LX/CwF;

    .line 2273353
    move-object v0, v0

    .line 2273354
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b()Ljava/lang/String;

    move-result-object v1

    .line 2273355
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2273356
    move-object v0, v0

    .line 2273357
    iget-object v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->t:Ljava/lang/String;

    move-object v1, v1

    .line 2273358
    iput-object v1, v0, LX/CwH;->u:Ljava/lang/String;

    .line 2273359
    move-object v0, v0

    .line 2273360
    iget-object v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->s:Ljava/lang/String;

    move-object v1, v1

    .line 2273361
    iput-object v1, v0, LX/CwH;->t:Ljava/lang/String;

    .line 2273362
    move-object v0, v0

    .line 2273363
    iget-boolean v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v1, v1

    .line 2273364
    iput-boolean v1, v0, LX/CwH;->n:Z

    .line 2273365
    move-object v0, v0

    .line 2273366
    iget-wide v4, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    move-wide v2, v4

    .line 2273367
    iput-wide v2, v0, LX/CwH;->o:D

    .line 2273368
    move-object v0, v0

    .line 2273369
    iget-object v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v1, v1

    .line 2273370
    iput-object v1, v0, LX/CwH;->l:LX/CwI;

    .line 2273371
    move-object v0, v0

    .line 2273372
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object p0

    .line 2273373
    :cond_1
    :goto_0
    return-object p0

    .line 2273374
    :cond_2
    sget-object v2, LX/CwF;->local:LX/CwF;

    if-eq v0, v2, :cond_3

    sget-object v2, LX/CwF;->local_category:LX/CwF;

    if-ne v0, v2, :cond_4

    :cond_3
    sget-object v0, LX/CwF;->keyword:LX/CwF;

    if-eq v1, v0, :cond_1

    .line 2273375
    :cond_4
    sget-object v0, LX/7HY;->MEMORY_CACHE:LX/7HY;

    if-ne p2, v0, :cond_7

    .line 2273376
    iget-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v0, v0

    .line 2273377
    if-nez v0, :cond_5

    .line 2273378
    iget-boolean v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v0, v0

    .line 2273379
    if-eqz v0, :cond_6

    .line 2273380
    :cond_5
    iget-wide v4, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    move-wide v0, v4

    .line 2273381
    invoke-static {p0, v0, v1}, LX/Fha;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;D)Lcom/facebook/search/model/TypeaheadUnit;

    move-result-object p0

    goto :goto_0

    .line 2273382
    :cond_6
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2273383
    invoke-static {p0}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v0

    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object p0

    goto :goto_0

    .line 2273384
    :cond_7
    iget-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v0, v0

    .line 2273385
    if-eqz v0, :cond_8

    .line 2273386
    iget-wide v4, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    move-wide v0, v4

    .line 2273387
    invoke-static {p1, v0, v1}, LX/Fha;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;D)Lcom/facebook/search/model/TypeaheadUnit;

    move-result-object p0

    goto :goto_0

    .line 2273388
    :cond_8
    iget-boolean v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v0, v0

    .line 2273389
    if-eqz v0, :cond_9

    .line 2273390
    iget-wide v4, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    move-wide v0, v4

    .line 2273391
    invoke-static {p0, v0, v1}, LX/Fha;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;D)Lcom/facebook/search/model/TypeaheadUnit;

    move-result-object p0

    goto :goto_0

    :cond_9
    move-object p0, p1

    .line 2273392
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/7HY;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2273393
    check-cast p1, Lcom/facebook/search/model/TypeaheadUnit;

    check-cast p2, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2273394
    instance-of v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v0, :cond_3

    instance-of v0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v0, :cond_3

    .line 2273395
    check-cast p1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    check-cast p2, Lcom/facebook/search/model/EntityTypeaheadUnit;

    const/4 v0, 0x1

    .line 2273396
    sget-object v1, LX/7HY;->MEMORY_CACHE:LX/7HY;

    if-ne p3, v1, :cond_5

    .line 2273397
    new-instance v1, LX/Cw7;

    invoke-direct {v1}, LX/Cw7;-><init>()V

    .line 2273398
    invoke-virtual {v1, p1}, LX/Cw7;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)LX/Cw7;

    move-result-object v1

    .line 2273399
    iget-boolean p0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move p0, p0

    .line 2273400
    if-nez p0, :cond_0

    .line 2273401
    iget-boolean p0, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move p0, p0

    .line 2273402
    if-eqz p0, :cond_4

    .line 2273403
    :cond_0
    :goto_0
    iput-boolean v0, v1, LX/Cw7;->m:Z

    .line 2273404
    move-object v0, v1

    .line 2273405
    invoke-virtual {v0}, LX/Cw7;->x()Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object p1

    .line 2273406
    :cond_1
    :goto_1
    move-object p1, p1

    .line 2273407
    :cond_2
    :goto_2
    return-object p1

    .line 2273408
    :cond_3
    instance-of v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v0, :cond_2

    instance-of v0, p2, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v0, :cond_2

    .line 2273409
    check-cast p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    check-cast p2, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-static {p1, p2, p3}, LX/Fha;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/model/KeywordTypeaheadUnit;LX/7HY;)Lcom/facebook/search/model/TypeaheadUnit;

    move-result-object p1

    goto :goto_2

    .line 2273410
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2273411
    :cond_5
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v1, v1

    .line 2273412
    if-nez v1, :cond_6

    .line 2273413
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    move v1, v1

    .line 2273414
    if-eqz v1, :cond_7

    .line 2273415
    :cond_6
    new-instance v0, LX/Cw7;

    invoke-direct {v0}, LX/Cw7;-><init>()V

    .line 2273416
    invoke-virtual {v0, p2}, LX/Cw7;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)LX/Cw7;

    move-result-object v0

    .line 2273417
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v1, v1

    .line 2273418
    iput-boolean v1, v0, LX/Cw7;->m:Z

    .line 2273419
    move-object v0, v0

    .line 2273420
    iget-boolean v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    move v1, v1

    .line 2273421
    iput-boolean v1, v0, LX/Cw7;->p:Z

    .line 2273422
    move-object v0, v0

    .line 2273423
    invoke-virtual {v0}, LX/Cw7;->x()Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object p1

    goto :goto_1

    .line 2273424
    :cond_7
    iget-boolean v1, p2, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v1, v1

    .line 2273425
    if-eqz v1, :cond_8

    .line 2273426
    new-instance v1, LX/Cw7;

    invoke-direct {v1}, LX/Cw7;-><init>()V

    .line 2273427
    invoke-virtual {v1, p1}, LX/Cw7;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)LX/Cw7;

    move-result-object v1

    .line 2273428
    iput-boolean v0, v1, LX/Cw7;->m:Z

    .line 2273429
    move-object v0, v1

    .line 2273430
    invoke-virtual {v0}, LX/Cw7;->x()Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-result-object p1

    goto :goto_1

    .line 2273431
    :cond_8
    iget-object v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2273432
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object p1, p2

    .line 2273433
    goto :goto_1
.end method
