.class public final LX/Fn2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2284886
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2284887
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2284888
    :goto_0
    return v1

    .line 2284889
    :cond_0
    const-string v8, "is_verified"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2284890
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v5, v3

    move v3, v2

    .line 2284891
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_4

    .line 2284892
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2284893
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2284894
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2284895
    const-string v8, "cover_photo"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2284896
    const/4 v7, 0x0

    .line 2284897
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_b

    .line 2284898
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2284899
    :goto_2
    move v6, v7

    .line 2284900
    goto :goto_1

    .line 2284901
    :cond_2
    const-string v8, "is_verified_page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2284902
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2284903
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2284904
    :cond_4
    const/4 v7, 0x3

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2284905
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2284906
    if-eqz v3, :cond_5

    .line 2284907
    invoke-virtual {p1, v2, v5}, LX/186;->a(IZ)V

    .line 2284908
    :cond_5
    if-eqz v0, :cond_6

    .line 2284909
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->a(IZ)V

    .line 2284910
    :cond_6
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1

    .line 2284911
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2284912
    :cond_9
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 2284913
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2284914
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2284915
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_9

    if-eqz v8, :cond_9

    .line 2284916
    const-string v9, "photo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2284917
    const/4 v8, 0x0

    .line 2284918
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_f

    .line 2284919
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2284920
    :goto_4
    move v6, v8

    .line 2284921
    goto :goto_3

    .line 2284922
    :cond_a
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2284923
    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 2284924
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_2

    :cond_b
    move v6, v7

    goto :goto_3

    .line 2284925
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2284926
    :cond_d
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_e

    .line 2284927
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2284928
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2284929
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_d

    if-eqz v9, :cond_d

    .line 2284930
    const-string v10, "image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 2284931
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_5

    .line 2284932
    :cond_e
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2284933
    invoke-virtual {p1, v8, v6}, LX/186;->b(II)V

    .line 2284934
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_4

    :cond_f
    move v6, v8

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2284935
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2284936
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2284937
    if-eqz v0, :cond_2

    .line 2284938
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284939
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2284940
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2284941
    if-eqz v1, :cond_1

    .line 2284942
    const-string v2, "photo"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284943
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2284944
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2284945
    if-eqz v2, :cond_0

    .line 2284946
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284947
    invoke-static {p0, v2, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2284948
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2284949
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2284950
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2284951
    if-eqz v0, :cond_3

    .line 2284952
    const-string v1, "is_verified"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284953
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2284954
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2284955
    if-eqz v0, :cond_4

    .line 2284956
    const-string v1, "is_verified_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2284957
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2284958
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2284959
    return-void
.end method
