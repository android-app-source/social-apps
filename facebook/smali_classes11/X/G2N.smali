.class public LX/G2N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/G2N;


# instance fields
.field public final a:LX/17W;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9hF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/17W;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17W;",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/9hF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2313944
    iput-object p1, p0, LX/G2N;->a:LX/17W;

    .line 2313945
    iput-object p2, p0, LX/G2N;->b:LX/0Or;

    .line 2313946
    iput-object p3, p0, LX/G2N;->c:LX/0Or;

    .line 2313947
    return-void
.end method

.method public static a(LX/0QB;)LX/G2N;
    .locals 6

    .prologue
    .line 2313948
    sget-object v0, LX/G2N;->d:LX/G2N;

    if-nez v0, :cond_1

    .line 2313949
    const-class v1, LX/G2N;

    monitor-enter v1

    .line 2313950
    :try_start_0
    sget-object v0, LX/G2N;->d:LX/G2N;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2313951
    if-eqz v2, :cond_0

    .line 2313952
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2313953
    new-instance v4, LX/G2N;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    const/16 v5, 0xf2f

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x2e6c

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, LX/G2N;-><init>(LX/17W;LX/0Or;LX/0Or;)V

    .line 2313954
    move-object v0, v4

    .line 2313955
    sput-object v0, LX/G2N;->d:LX/G2N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313956
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2313957
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2313958
    :cond_1
    sget-object v0, LX/G2N;->d:LX/G2N;

    return-object v0

    .line 2313959
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2313960
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/drawee/view/DraweeView;Ljava/lang/String;LX/1Fb;Ljava/lang/String;Lcom/facebook/timeline/protiles/model/ProtileModel;)V
    .locals 7
    .param p3    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/view/DraweeView",
            "<",
            "LX/1af;",
            ">;",
            "Ljava/lang/String;",
            "LX/1Fb;",
            "Ljava/lang/String;",
            "Lcom/facebook/timeline/protiles/model/ProtileModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2313961
    if-eqz p4, :cond_1

    .line 2313962
    iget-object v0, p0, LX/G2N;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    invoke-static {p4}, LX/9hF;->a(Ljava/lang/String;)LX/9hE;

    move-result-object v0

    move-object v2, v0

    .line 2313963
    :goto_0
    if-eqz p3, :cond_2

    invoke-interface {p3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    move-object v1, v0

    .line 2313964
    :goto_1
    sget-object v0, LX/74S;->TIMELINE_PHOTOS_OF_USER:LX/74S;

    invoke-virtual {v2, v0}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    .line 2313965
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2313966
    iget-object v2, p5, Lcom/facebook/timeline/protiles/model/ProtileModel;->c:LX/0Px;

    move-object v5, v2

    .line 2313967
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_0

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    .line 2313968
    new-instance p3, LX/5kN;

    invoke-direct {p3}, LX/5kN;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object p4

    invoke-virtual {p4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object p4

    .line 2313969
    iput-object p4, p3, LX/5kN;->D:Ljava/lang/String;

    .line 2313970
    move-object p3, p3

    .line 2313971
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object p4

    invoke-virtual {p4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p4

    invoke-static {p4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p4

    .line 2313972
    iput-object p4, p3, LX/5kN;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2313973
    move-object p3, p3

    .line 2313974
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object p4

    invoke-virtual {p4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p4

    invoke-static {p4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p4

    .line 2313975
    iput-object p4, p3, LX/5kN;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2313976
    move-object p3, p3

    .line 2313977
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object p4

    invoke-virtual {p4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p4

    invoke-static {p4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p4

    .line 2313978
    iput-object p4, p3, LX/5kN;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2313979
    move-object p3, p3

    .line 2313980
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    .line 2313981
    iput-object v2, p3, LX/5kN;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2313982
    move-object v2, p3

    .line 2313983
    invoke-virtual {v2}, LX/5kN;->a()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataModel;

    move-result-object v2

    .line 2313984
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2313985
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2313986
    :cond_0
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2313987
    invoke-virtual {v0, v2}, LX/9hD;->a(LX/0Px;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v2

    .line 2313988
    iget-object v0, p0, LX/G2N;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2313989
    new-instance v4, LX/G2M;

    invoke-direct {v4, p0, p2, p1, v1}, LX/G2M;-><init>(LX/G2N;Ljava/lang/String;Lcom/facebook/drawee/view/DraweeView;LX/1bf;)V

    move-object v1, v4

    .line 2313990
    invoke-interface {v0, v3, v2, v1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2313991
    return-void

    .line 2313992
    :cond_1
    iget-object v0, p0, LX/G2N;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_0

    .line 2313993
    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_1
.end method
