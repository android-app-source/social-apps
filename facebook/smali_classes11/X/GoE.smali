.class public LX/GoE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2394169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2394170
    iput-object p1, p0, LX/GoE;->b:Ljava/lang/String;

    .line 2394171
    iput-object p2, p0, LX/GoE;->a:Ljava/lang/String;

    .line 2394172
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2394173
    if-ne p0, p1, :cond_1

    .line 2394174
    :cond_0
    :goto_0
    return v0

    .line 2394175
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2394176
    goto :goto_0

    .line 2394177
    :cond_3
    check-cast p1, LX/GoE;

    .line 2394178
    iget-object v2, p0, LX/GoE;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/GoE;->a:Ljava/lang/String;

    iget-object v3, p1, LX/GoE;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2394179
    goto :goto_0

    .line 2394180
    :cond_5
    iget-object v2, p1, LX/GoE;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2394181
    :cond_6
    iget-object v2, p0, LX/GoE;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/GoE;->b:Ljava/lang/String;

    iget-object v3, p1, LX/GoE;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, LX/GoE;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2394182
    iget-object v0, p0, LX/GoE;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GoE;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2394183
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/GoE;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/GoE;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 2394184
    return v0

    :cond_1
    move v0, v1

    .line 2394185
    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2394186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/GoE;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GoE;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
