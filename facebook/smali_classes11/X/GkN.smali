.class public LX/GkN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DZ7;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/GkN;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2389022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/GkN;
    .locals 3

    .prologue
    .line 2389023
    sget-object v0, LX/GkN;->a:LX/GkN;

    if-nez v0, :cond_1

    .line 2389024
    const-class v1, LX/GkN;

    monitor-enter v1

    .line 2389025
    :try_start_0
    sget-object v0, LX/GkN;->a:LX/GkN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2389026
    if-eqz v2, :cond_0

    .line 2389027
    :try_start_1
    new-instance v0, LX/GkN;

    invoke-direct {v0}, LX/GkN;-><init>()V

    .line 2389028
    move-object v0, v0

    .line 2389029
    sput-object v0, LX/GkN;->a:LX/GkN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2389030
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2389031
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2389032
    :cond_1
    sget-object v0, LX/GkN;->a:LX/GkN;

    return-object v0

    .line 2389033
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2389034
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/FbFragment;LX/DLO;)V
    .locals 0

    .prologue
    .line 2389035
    return-void
.end method

.method public final a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V
    .locals 1
    .param p3    # LX/DLO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2389036
    const-class v0, LX/1ZF;

    invoke-virtual {p1, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2389037
    if-eqz v0, :cond_0

    .line 2389038
    invoke-interface {v0, p2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2389039
    :cond_0
    return-void
.end method
