.class public final LX/Fdr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2264247
    iput-object p1, p0, LX/Fdr;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x4cba233e    # 9.7589744E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264239
    iget-object v1, p0, LX/Fdr;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->q:Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    iget-object v2, p0, LX/Fdr;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    iget-object v2, v2, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->w:LX/Fdo;

    .line 2264240
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2264241
    iget-object v4, v2, LX/Fdo;->h:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2264242
    invoke-virtual {v5, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2264243
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v2, v4

    .line 2264244
    invoke-virtual {v1, v2}, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->a(LX/0Px;)V

    .line 2264245
    iget-object v1, p0, LX/Fdr;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2264246
    const v1, -0x796fbf60

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
