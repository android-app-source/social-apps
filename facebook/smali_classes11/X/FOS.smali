.class public final LX/FOS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2235142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235143
    iput-object p1, p0, LX/FOS;->a:Ljava/util/List;

    .line 2235144
    iput p2, p0, LX/FOS;->b:I

    .line 2235145
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2235146
    const-class v0, LX/3L0;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    const-string v1, "lines"

    iget-object v2, p0, LX/FOS;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "moreCount"

    iget v2, p0, LX/FOS;->b:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
