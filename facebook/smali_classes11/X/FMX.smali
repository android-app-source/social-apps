.class public LX/FMX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FMX;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/SettableFuture;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2229799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2229800
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/FMX;->a:Ljava/util/Map;

    .line 2229801
    iput-object p1, p0, LX/FMX;->b:Landroid/content/Context;

    .line 2229802
    return-void
.end method

.method public static a(LX/0QB;)LX/FMX;
    .locals 4

    .prologue
    .line 2229803
    sget-object v0, LX/FMX;->c:LX/FMX;

    if-nez v0, :cond_1

    .line 2229804
    const-class v1, LX/FMX;

    monitor-enter v1

    .line 2229805
    :try_start_0
    sget-object v0, LX/FMX;->c:LX/FMX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2229806
    if-eqz v2, :cond_0

    .line 2229807
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2229808
    new-instance p0, LX/FMX;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/FMX;-><init>(Landroid/content/Context;)V

    .line 2229809
    move-object v0, p0

    .line 2229810
    sput-object v0, LX/FMX;->c:LX/FMX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2229811
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2229812
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2229813
    :cond_1
    sget-object v0, LX/FMX;->c:LX/FMX;

    return-object v0

    .line 2229814
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2229815
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
