.class public final LX/F4K;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/F4L;


# direct methods
.method public constructor <init>(LX/F4L;)V
    .locals 0

    .prologue
    .line 2196824
    iput-object p1, p0, LX/F4K;->a:LX/F4L;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2196825
    iget-object v0, p0, LX/F4K;->a:LX/F4L;

    iget-object v0, v0, LX/F4L;->f:LX/01T;

    sget-object v1, LX/01T;->GROUPS:LX/01T;

    if-ne v0, v1, :cond_0

    .line 2196826
    const-string v0, "https://m.facebook.com/help/groups-app/565766570190970"

    move-object v0, v0

    .line 2196827
    :goto_0
    iget-object v1, p0, LX/F4K;->a:LX/F4L;

    iget-object v1, v1, LX/F4L;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/F4K;->a:LX/F4L;

    iget-object v2, v2, LX/F4L;->c:LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2196828
    return-void

    .line 2196829
    :cond_0
    const-string v0, "https://m.facebook.com/help/286027304749263"

    move-object v0, v0

    .line 2196830
    goto :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2196821
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2196822
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2196823
    return-void
.end method
