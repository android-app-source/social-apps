.class public final LX/HAU;
.super LX/CXx;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2435324
    iput-object p1, p0, LX/HAU;->b:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-direct {p0, p2}, LX/CXx;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method

.method private a(LX/CXw;)V
    .locals 2

    .prologue
    .line 2435318
    iget-object v0, p1, LX/CXw;->c:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    .line 2435319
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->j()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabCtaChannelModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2435320
    iget-object v1, p0, LX/HAU;->b:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->j()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabCtaChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$TabCtaChannelModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->setActionBarChannelItems(Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;LX/0Px;)V

    .line 2435321
    :goto_0
    iget-object v0, p0, LX/HAU;->b:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-virtual {v0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->a()V

    .line 2435322
    return-void

    .line 2435323
    :cond_0
    iget-object v0, p0, LX/HAU;->b:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->setActionBarChannelItems(Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;LX/0Px;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2435317
    check-cast p1, LX/CXw;

    invoke-direct {p0, p1}, LX/HAU;->a(LX/CXw;)V

    return-void
.end method
