.class public LX/FiE;
.super LX/Fi8;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/2Sf;

.field private final c:LX/FgS;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/2Sf;LX/FgS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274737
    invoke-direct {p0}, LX/Fi8;-><init>()V

    .line 2274738
    iput-object p1, p0, LX/FiE;->a:Landroid/content/res/Resources;

    .line 2274739
    iput-object p2, p0, LX/FiE;->b:LX/2Sf;

    .line 2274740
    iput-object p3, p0, LX/FiE;->c:LX/FgS;

    .line 2274741
    return-void
.end method

.method private static a(Lcom/facebook/search/api/GraphSearchQuery;LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 4

    .prologue
    .line 2274742
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2274743
    iput-object p2, v0, LX/CwH;->b:Ljava/lang/String;

    .line 2274744
    move-object v0, v0

    .line 2274745
    iput-object p2, v0, LX/CwH;->d:Ljava/lang/String;

    .line 2274746
    move-object v0, v0

    .line 2274747
    const-string v1, "scoped"

    .line 2274748
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2274749
    move-object v0, v0

    .line 2274750
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2274751
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2274752
    move-object v0, v0

    .line 2274753
    iput-object p1, v0, LX/CwH;->l:LX/CwI;

    .line 2274754
    move-object v0, v0

    .line 2274755
    invoke-static {p0}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v1

    .line 2274756
    iput-object v1, v0, LX/CwH;->x:LX/0Px;

    .line 2274757
    move-object v0, v0

    .line 2274758
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    move-object v1, v1

    .line 2274759
    iput-object v1, v0, LX/CwH;->y:LX/0P1;

    .line 2274760
    move-object v0, v0

    .line 2274761
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v1, v1

    .line 2274762
    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v2, v2

    .line 2274763
    iget-object v3, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v3, v3

    .line 2274764
    invoke-virtual {v0, v1, v2, v3}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/103;)LX/CwH;

    move-result-object v0

    .line 2274765
    iget-object v1, p0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v1, v1

    .line 2274766
    iget-object v2, p0, Lcom/facebook/search/api/GraphSearchQuery;->g:Ljava/lang/String;

    move-object v2, v2

    .line 2274767
    iget-object v3, p0, Lcom/facebook/search/api/GraphSearchQuery;->l:LX/0P1;

    move-object v3, v3

    .line 2274768
    invoke-static {v1, p2, v2, v3}, LX/7BG;->a(LX/103;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Ljava/lang/String;

    move-result-object v1

    .line 2274769
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2274770
    move-object v0, v0

    .line 2274771
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2274772
    iget-object v0, p0, LX/FiE;->c:LX/FgS;

    .line 2274773
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2274774
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FiE;->c:LX/FgS;

    .line 2274775
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2274776
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2274777
    if-nez v0, :cond_1

    .line 2274778
    :cond_0
    const-string v0, ""

    .line 2274779
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/FiE;->c:LX/FgS;

    .line 2274780
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2274781
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2274782
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2274783
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2274784
    invoke-direct {p0}, LX/FiE;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 2274785
    iget-object v1, p2, LX/7Hc;->b:LX/0Px;

    move-object v5, v1

    .line 2274786
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274787
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274788
    if-nez v1, :cond_3

    instance-of v7, v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v7, :cond_3

    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2274789
    const/4 v0, 0x1

    .line 2274790
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2274791
    :cond_0
    if-nez v1, :cond_1

    .line 2274792
    sget-object v0, LX/CwI;->ECHO:LX/CwI;

    invoke-static {p1, v0, v4}, LX/FiE;->a(Lcom/facebook/search/api/GraphSearchQuery;LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274793
    :cond_1
    new-instance v0, LX/Cwa;

    invoke-direct {v0}, LX/Cwa;-><init>()V

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2274794
    iput-object v1, v0, LX/Cwa;->b:LX/0Px;

    .line 2274795
    move-object v0, v0

    .line 2274796
    sget-object v1, LX/Cwb;->KEYWORD:LX/Cwb;

    .line 2274797
    iput-object v1, v0, LX/Cwa;->a:LX/Cwb;

    .line 2274798
    move-object v0, v0

    .line 2274799
    iget-object v1, p1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v1, v1

    .line 2274800
    sget-object v2, LX/103;->VIDEO:LX/103;

    if-eq v1, v2, :cond_2

    .line 2274801
    iget-object v1, p0, LX/FiE;->a:Landroid/content/res/Resources;

    const v2, 0x7f0822e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2274802
    iput-object v1, v0, LX/Cwa;->c:Ljava/lang/String;

    .line 2274803
    :cond_2
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/2Sf;->b(Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 2

    .prologue
    .line 2274804
    sget-object v0, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    invoke-direct {p0}, LX/FiE;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/FiE;->a(Lcom/facebook/search/api/GraphSearchQuery;LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method
