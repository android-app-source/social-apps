.class public LX/Gbi;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Gbk;",
        ">;",
        "Lcom/facebook/devicebasedlogin/settings/DBLSettingsPreferenceViewHolder$OnDBLSettingsClickListener;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Gbo;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field public final c:LX/GbV;

.field private final d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;LX/GbV;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Gbo;",
            ">;",
            "Landroid/content/Context;",
            "LX/GbV;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2370356
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2370357
    iput-object p1, p0, LX/Gbi;->a:Ljava/util/List;

    .line 2370358
    iput-object p2, p0, LX/Gbi;->b:Landroid/content/Context;

    .line 2370359
    iput-object p3, p0, LX/Gbi;->c:LX/GbV;

    .line 2370360
    iput-object p4, p0, LX/Gbi;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2370361
    return-void
.end method

.method public static a(LX/Gbi;Lcom/facebook/auth/credentials/DBLFacebookCredentials;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2370362
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, LX/Gbi;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2370363
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2370364
    const-string v2, "dbl_account_details"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2370365
    const-string v2, "operation_type"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370366
    const-string v2, "source"

    const-string v3, "logged_in_settings"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370367
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2370368
    iget-object v2, p0, LX/Gbi;->d:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0xc

    iget-object v0, p0, LX/Gbi;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2370369
    iget-object v0, p0, LX/Gbi;->b:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v2, 0x7f04003a

    const v3, 0x7f040040

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2370370
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2370371
    const/4 v2, 0x0

    .line 2370372
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 2370373
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0303ea

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2370374
    new-instance v0, LX/Gbq;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/Gbq;-><init>(Landroid/view/View;Landroid/content/Context;)V

    .line 2370375
    iput-object p0, v0, LX/Gbq;->q:LX/Gbi;

    .line 2370376
    :goto_0
    return-object v0

    .line 2370377
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0303e8

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2370378
    new-instance v0, LX/Gbl;

    invoke-direct {v0, v1}, LX/Gbl;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2370379
    check-cast p1, LX/Gbk;

    .line 2370380
    iget-object v0, p0, LX/Gbi;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gbo;

    .line 2370381
    invoke-virtual {p1, v0}, LX/Gbk;->a(LX/Gbo;)V

    .line 2370382
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2370383
    iget-object v0, p0, LX/Gbi;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gbo;

    .line 2370384
    iget-object v1, v0, LX/Gbo;->f:LX/Gbm;

    move-object v0, v1

    .line 2370385
    sget-object v1, LX/Gbm;->CATEGORY:LX/Gbm;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2370386
    iget-object v0, p0, LX/Gbi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
