.class public final LX/Fj5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/model/NullStateModuleCollectionUnit;

.field public final synthetic b:LX/FhE;

.field public final synthetic c:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;Lcom/facebook/search/model/NullStateModuleCollectionUnit;LX/FhE;)V
    .locals 0

    .prologue
    .line 2276249
    iput-object p1, p0, LX/Fj5;->c:Lcom/facebook/search/typeahead/rows/nullstate/NullStateModuleSeeMorePartDefinition;

    iput-object p2, p0, LX/Fj5;->a:Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    iput-object p3, p0, LX/Fj5;->b:LX/FhE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x79c9d2b8

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2276250
    iget-object v1, p0, LX/Fj5;->a:Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    .line 2276251
    new-instance v2, LX/CwN;

    invoke-direct {v2}, LX/CwN;-><init>()V

    .line 2276252
    iget-object v3, v1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->a:LX/3bj;

    move-object v3, v3

    .line 2276253
    invoke-virtual {v3}, LX/3bj;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/CwN;->a(Ljava/lang/String;)LX/CwN;

    move-result-object v2

    .line 2276254
    iget-object v3, v1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2276255
    iput-object v3, v2, LX/CwN;->b:Ljava/lang/String;

    .line 2276256
    move-object v2, v2

    .line 2276257
    iget-object v3, v1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2276258
    iput-object v3, v2, LX/CwN;->c:Ljava/lang/String;

    .line 2276259
    move-object v2, v2

    .line 2276260
    iget-object v3, v1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2276261
    iput-object v3, v2, LX/CwN;->d:Ljava/lang/String;

    .line 2276262
    move-object v2, v2

    .line 2276263
    invoke-virtual {v1}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v3

    .line 2276264
    iput-object v3, v2, LX/CwN;->e:LX/0Px;

    .line 2276265
    move-object v2, v2

    .line 2276266
    iget v3, v1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->f:I

    move v3, v3

    .line 2276267
    iput v3, v2, LX/CwN;->f:I

    .line 2276268
    move-object v2, v2

    .line 2276269
    const/4 v3, 0x1

    .line 2276270
    iput-boolean v3, v2, LX/CwN;->g:Z

    .line 2276271
    move-object v2, v2

    .line 2276272
    iget v3, v1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->h:I

    move v3, v3

    .line 2276273
    iput v3, v2, LX/CwN;->h:I

    .line 2276274
    move-object v2, v2

    .line 2276275
    iget-object v3, v1, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->i:LX/CwO;

    invoke-virtual {v3}, LX/CwO;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/CwN;->e(Ljava/lang/String;)LX/CwN;

    move-result-object v2

    invoke-virtual {v2}, LX/CwN;->a()Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    move-result-object v2

    move-object v1, v2

    .line 2276276
    iget-object v2, p0, LX/Fj5;->b:LX/FhE;

    new-instance v3, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;

    iget-object v4, p0, LX/Fj5;->a:Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    invoke-virtual {v4}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;-><init>(LX/Cwb;)V

    invoke-virtual {v2, v3}, LX/FhE;->a(Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2276277
    iget-object v2, p0, LX/Fj5;->b:LX/FhE;

    iget-object v3, p0, LX/Fj5;->a:Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    .line 2276278
    iget-object v4, v2, LX/FhE;->p:LX/Fid;

    .line 2276279
    iget-object p1, v4, LX/Fid;->a:Ljava/util/List;

    invoke-interface {p1, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    .line 2276280
    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    .line 2276281
    iget-object v2, v4, LX/Fid;->a:Ljava/util/List;

    invoke-interface {v2, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2276282
    :cond_0
    iget-object v1, p0, LX/Fj5;->b:LX/FhE;

    invoke-virtual {v1}, LX/1Qj;->iN_()V

    .line 2276283
    const v1, -0xdeb92dc

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
