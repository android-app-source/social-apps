.class public LX/FDh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static final n:[Ljava/lang/String;


# instance fields
.field private final b:LX/0ad;

.field public final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final d:LX/0Sh;

.field private final e:LX/FOa;

.field private final f:Landroid/content/ContentResolver;

.field private final g:LX/1Ml;

.field private final h:LX/3Lx;

.field private final i:LX/FDg;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0SG;

.field private final l:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

.field private final m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2213078
    const-class v0, LX/FDh;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FDh;->a:Ljava/lang/String;

    .line 2213079
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    sput-object v0, LX/FDh;->n:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/1Ml;LX/3Lx;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0SG;LX/FOa;LX/FDg;LX/0Sh;Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;)V
    .locals 6
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "LX/1Ml;",
            "LX/3Lx;",
            "LX/0ad;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0SG;",
            "LX/FOa;",
            "LX/FDg;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2213080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2213081
    iput-object p1, p0, LX/FDh;->f:Landroid/content/ContentResolver;

    .line 2213082
    iput-object p2, p0, LX/FDh;->g:LX/1Ml;

    .line 2213083
    iput-object p3, p0, LX/FDh;->h:LX/3Lx;

    .line 2213084
    iput-object p4, p0, LX/FDh;->b:LX/0ad;

    .line 2213085
    iput-object p5, p0, LX/FDh;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2213086
    iput-object p7, p0, LX/FDh;->k:LX/0SG;

    .line 2213087
    iput-object p6, p0, LX/FDh;->j:LX/0Or;

    .line 2213088
    iput-object p8, p0, LX/FDh;->e:LX/FOa;

    .line 2213089
    iput-object p9, p0, LX/FDh;->i:LX/FDg;

    .line 2213090
    move-object/from16 v0, p10

    iput-object v0, p0, LX/FDh;->d:LX/0Sh;

    .line 2213091
    move-object/from16 v0, p11

    iput-object v0, p0, LX/FDh;->l:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    .line 2213092
    iget-object v1, p0, LX/FDh;->b:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/3Dx;->o:I

    const/16 v5, 0x2760

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    iput v1, p0, LX/FDh;->m:I

    .line 2213093
    return-void
.end method
