.class public LX/FCv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2210793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210794
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2210795
    invoke-virtual {p1}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2210796
    iput-object p1, p0, LX/FCv;->a:Landroid/net/Uri;

    .line 2210797
    return-void
.end method


# virtual methods
.method public final b()LX/FCa;
    .locals 2

    .prologue
    .line 2210798
    new-instance v0, LX/FCa;

    iget-object v1, p0, LX/FCv;->a:Landroid/net/Uri;

    invoke-direct {v0, v1}, LX/FCa;-><init>(Landroid/net/Uri;)V

    return-object v0
.end method
