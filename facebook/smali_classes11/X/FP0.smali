.class public final LX/FP0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2236473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2236471
    new-instance v0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-direct {v0, p1}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2236472
    new-array v0, p1, [Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    return-object v0
.end method
