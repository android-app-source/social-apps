.class public final LX/Gcl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/ui/NumPadView;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/NumPadView;)V
    .locals 0

    .prologue
    .line 2372460
    iput-object p1, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x77e7e214

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2372449
    iget-object v1, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 2372450
    iget-object v2, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/NumPadView;->k:Landroid/widget/ImageView;

    if-ne p1, v2, :cond_1

    .line 2372451
    iget-object v2, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 2372452
    iget-object v2, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    iget-object v3, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    iget-object v3, v3, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4, v1}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2372453
    iput-object v1, v2, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    .line 2372454
    iget-object v1, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    invoke-static {v1}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->e(Lcom/facebook/devicebasedlogin/ui/NumPadView;)V

    .line 2372455
    :cond_0
    :goto_0
    const v1, 0x5a5a9dec

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2372456
    :cond_1
    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 2372457
    iget-object v1, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    iget-object v2, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2372458
    iput-object v2, v1, Lcom/facebook/devicebasedlogin/ui/NumPadView;->r:Ljava/lang/StringBuilder;

    .line 2372459
    iget-object v1, p0, LX/Gcl;->a:Lcom/facebook/devicebasedlogin/ui/NumPadView;

    invoke-static {v1}, Lcom/facebook/devicebasedlogin/ui/NumPadView;->e(Lcom/facebook/devicebasedlogin/ui/NumPadView;)V

    goto :goto_0
.end method
