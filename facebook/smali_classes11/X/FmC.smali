.class public LX/FmC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6qd",
        "<",
        "Lcom/facebook/payments/checkout/model/SimpleCheckoutData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/6r8;

.field private b:LX/6qc;

.field private c:LX/6qh;


# direct methods
.method public constructor <init>(LX/6r8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2282219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282220
    iput-object p1, p0, LX/FmC;->a:LX/6r8;

    .line 2282221
    return-void
.end method


# virtual methods
.method public final a(LX/6qc;)V
    .locals 2

    .prologue
    .line 2282234
    iput-object p1, p0, LX/FmC;->b:LX/6qc;

    .line 2282235
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    iget-object v1, p0, LX/FmC;->b:LX/6qc;

    invoke-virtual {v0, v1}, LX/6r8;->a(LX/6qc;)V

    .line 2282236
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 2

    .prologue
    .line 2282231
    iput-object p1, p0, LX/FmC;->c:LX/6qh;

    .line 2282232
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    iget-object v1, p0, LX/FmC;->c:LX/6qh;

    invoke-virtual {v0, v1}, LX/6r8;->a(LX/6qh;)V

    .line 2282233
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;I)V
    .locals 1

    .prologue
    .line 2282228
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282229
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;I)V

    .line 2282230
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .locals 1

    .prologue
    .line 2282225
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282226
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V

    .line 2282227
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Rf;)V
    .locals 1

    .prologue
    .line 2282222
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282223
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Rf;)V

    .line 2282224
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/6tr;)V
    .locals 1

    .prologue
    .line 2282216
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282217
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/6tr;)V

    .line 2282218
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;LX/73T;)V
    .locals 2

    .prologue
    .line 2282205
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282206
    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->p()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    invoke-static {v0}, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a(Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;)LX/FmO;

    move-result-object v1

    .line 2282207
    const-string v0, "privacy_option"

    invoke-virtual {p2, v0}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2282208
    if-eqz v0, :cond_0

    .line 2282209
    iput-object v0, v1, LX/FmO;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2282210
    :cond_0
    invoke-static {}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->newBuilder()LX/6sR;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6sR;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)LX/6sR;

    move-result-object v0

    invoke-virtual {v1}, LX/FmO;->a()Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    move-result-object v1

    .line 2282211
    iput-object v1, v0, LX/6sR;->n:Landroid/os/Parcelable;

    .line 2282212
    move-object v0, v0

    .line 2282213
    invoke-virtual {v0}, LX/6sR;->z()Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    move-result-object v0

    .line 2282214
    iget-object v1, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v1, v0, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/73T;)V

    .line 2282215
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2282202
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282203
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Landroid/os/Parcelable;)V

    .line 2282204
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .locals 1

    .prologue
    .line 2282199
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282200
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 2282201
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V
    .locals 1

    .prologue
    .line 2282196
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282197
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)V

    .line 2282198
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
    .locals 1

    .prologue
    .line 2282193
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282194
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V

    .line 2282195
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/model/PaymentsPin;)V
    .locals 1

    .prologue
    .line 2282190
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282191
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/model/PaymentsPin;)V

    .line 2282192
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 1

    .prologue
    .line 2282237
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282238
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 2282239
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V
    .locals 1

    .prologue
    .line 2282187
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282188
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;)V

    .line 2282189
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/shipping/model/MailingAddress;)V
    .locals 1

    .prologue
    .line 2282184
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282185
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/shipping/model/MailingAddress;)V

    .line 2282186
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/shipping/model/ShippingOption;)V
    .locals 1

    .prologue
    .line 2282181
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282182
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Lcom/facebook/payments/shipping/model/ShippingOption;)V

    .line 2282183
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 1

    .prologue
    .line 2282178
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282179
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2, p3}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 2282180
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2282175
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282176
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V

    .line 2282177
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;LX/0Px;)V
    .locals 1

    .prologue
    .line 2282172
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282173
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2, p3}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;LX/0Px;)V

    .line 2282174
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 2282169
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282170
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/util/List;)V

    .line 2282171
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Z)V
    .locals 1

    .prologue
    .line 2282166
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282167
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Z)V

    .line 2282168
    return-void
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 2282149
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282150
    invoke-static {p1}, LX/6r8;->a(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 1

    .prologue
    .line 2282163
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282164
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1}, LX/6r8;->b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;)V

    .line 2282165
    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .locals 1

    .prologue
    .line 2282160
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282161
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V

    .line 2282162
    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2282157
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282158
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->b(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V

    .line 2282159
    return-void
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;LX/0Px;)V
    .locals 1

    .prologue
    .line 2282154
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282155
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->c(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;LX/0Px;)V

    .line 2282156
    return-void
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2282151
    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    .line 2282152
    iget-object v0, p0, LX/FmC;->a:LX/6r8;

    invoke-virtual {v0, p1, p2}, LX/6r8;->c(Lcom/facebook/payments/checkout/model/SimpleCheckoutData;Ljava/lang/String;)V

    .line 2282153
    return-void
.end method
