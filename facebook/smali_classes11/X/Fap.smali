.class public LX/Fap;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/0tX;

.field public d:LX/2ST;

.field public e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2259186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259187
    iput-object p1, p0, LX/Fap;->b:Ljava/util/concurrent/ExecutorService;

    .line 2259188
    iput-object p2, p0, LX/Fap;->c:LX/0tX;

    .line 2259189
    return-void
.end method

.method public static a(LX/Fap;LX/0zO;)V
    .locals 3

    .prologue
    .line 2259190
    iget-object v0, p0, LX/Fap;->c:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/Fap;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2259191
    new-instance v0, LX/Fao;

    invoke-direct {v0, p0}, LX/Fao;-><init>(LX/Fap;)V

    .line 2259192
    iget-object v1, p0, LX/Fap;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/Fap;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2259193
    return-void
.end method
