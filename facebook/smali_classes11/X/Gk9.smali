.class public final enum LX/Gk9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gk9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gk9;

.field public static final enum FAVORITE_ROW:LX/Gk9;

.field public static final enum SECTION_HEADER:LX/Gk9;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2388811
    new-instance v0, LX/Gk9;

    const-string v1, "FAVORITE_ROW"

    invoke-direct {v0, v1, v2}, LX/Gk9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gk9;->FAVORITE_ROW:LX/Gk9;

    .line 2388812
    new-instance v0, LX/Gk9;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v3}, LX/Gk9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gk9;->SECTION_HEADER:LX/Gk9;

    .line 2388813
    const/4 v0, 0x2

    new-array v0, v0, [LX/Gk9;

    sget-object v1, LX/Gk9;->FAVORITE_ROW:LX/Gk9;

    aput-object v1, v0, v2

    sget-object v1, LX/Gk9;->SECTION_HEADER:LX/Gk9;

    aput-object v1, v0, v3

    sput-object v0, LX/Gk9;->$VALUES:[LX/Gk9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2388816
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gk9;
    .locals 1

    .prologue
    .line 2388815
    const-class v0, LX/Gk9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gk9;

    return-object v0
.end method

.method public static values()[LX/Gk9;
    .locals 1

    .prologue
    .line 2388814
    sget-object v0, LX/Gk9;->$VALUES:[LX/Gk9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gk9;

    return-object v0
.end method
