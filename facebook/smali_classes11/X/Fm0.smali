.class public LX/Fm0;
.super Lcom/facebook/fbui/widget/contentview/CheckedContentView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2281895
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;-><init>(Landroid/content/Context;)V

    .line 2281896
    invoke-virtual {p0}, LX/Fm0;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b21f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2281897
    sget-object v1, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2281898
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2281899
    invoke-virtual {p0, v3, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 2281900
    invoke-virtual {p0, v0, v0, v2, v0}, LX/Fm0;->setPadding(IIII)V

    .line 2281901
    sget-object v0, LX/6V9;->START:LX/6V9;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkPosition(LX/6V9;)V

    .line 2281902
    return-void
.end method
