.class public final enum LX/G9c;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G9c;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G9c;

.field private static final FOR_BITS:[LX/G9c;

.field public static final enum H:LX/G9c;

.field public static final enum L:LX/G9c;

.field public static final enum M:LX/G9c;

.field public static final enum Q:LX/G9c;


# instance fields
.field private final bits:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2323055
    new-instance v0, LX/G9c;

    const-string v1, "L"

    invoke-direct {v0, v1, v2, v3}, LX/G9c;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G9c;->L:LX/G9c;

    .line 2323056
    new-instance v0, LX/G9c;

    const-string v1, "M"

    invoke-direct {v0, v1, v3, v2}, LX/G9c;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G9c;->M:LX/G9c;

    .line 2323057
    new-instance v0, LX/G9c;

    const-string v1, "Q"

    invoke-direct {v0, v1, v4, v5}, LX/G9c;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G9c;->Q:LX/G9c;

    .line 2323058
    new-instance v0, LX/G9c;

    const-string v1, "H"

    invoke-direct {v0, v1, v5, v4}, LX/G9c;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G9c;->H:LX/G9c;

    .line 2323059
    new-array v0, v6, [LX/G9c;

    sget-object v1, LX/G9c;->L:LX/G9c;

    aput-object v1, v0, v2

    sget-object v1, LX/G9c;->M:LX/G9c;

    aput-object v1, v0, v3

    sget-object v1, LX/G9c;->Q:LX/G9c;

    aput-object v1, v0, v4

    sget-object v1, LX/G9c;->H:LX/G9c;

    aput-object v1, v0, v5

    sput-object v0, LX/G9c;->$VALUES:[LX/G9c;

    .line 2323060
    new-array v0, v6, [LX/G9c;

    sget-object v1, LX/G9c;->M:LX/G9c;

    aput-object v1, v0, v2

    sget-object v1, LX/G9c;->L:LX/G9c;

    aput-object v1, v0, v3

    sget-object v1, LX/G9c;->H:LX/G9c;

    aput-object v1, v0, v4

    sget-object v1, LX/G9c;->Q:LX/G9c;

    aput-object v1, v0, v5

    sput-object v0, LX/G9c;->FOR_BITS:[LX/G9c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2323052
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2323053
    iput p3, p0, LX/G9c;->bits:I

    .line 2323054
    return-void
.end method

.method public static forBits(I)LX/G9c;
    .locals 1

    .prologue
    .line 2323049
    if-ltz p0, :cond_0

    sget-object v0, LX/G9c;->FOR_BITS:[LX/G9c;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 2323050
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2323051
    :cond_1
    sget-object v0, LX/G9c;->FOR_BITS:[LX/G9c;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/G9c;
    .locals 1

    .prologue
    .line 2323046
    const-class v0, LX/G9c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G9c;

    return-object v0
.end method

.method public static values()[LX/G9c;
    .locals 1

    .prologue
    .line 2323048
    sget-object v0, LX/G9c;->$VALUES:[LX/G9c;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G9c;

    return-object v0
.end method


# virtual methods
.method public final getBits()I
    .locals 1

    .prologue
    .line 2323047
    iget v0, p0, LX/G9c;->bits:I

    return v0
.end method
