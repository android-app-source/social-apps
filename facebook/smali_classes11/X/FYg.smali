.class public LX/FYg;
.super LX/1OM;
.source ""

# interfaces
.implements LX/FYf;


# static fields
.field private static final a:I


# instance fields
.field private final b:Landroid/content/Context;

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/FYd;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2256420
    const v0, 0x7f031251

    sput v0, LX/FYg;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2256396
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2256397
    iput-object p1, p0, LX/FYg;->b:Landroid/content/Context;

    .line 2256398
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2256399
    return-void
.end method

.method public static e(LX/FYg;)V
    .locals 2

    .prologue
    .line 2256417
    iget-object v0, p0, LX/FYg;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2256418
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Non-zero count reported when no headers were defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2256419
    :cond_0
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2256415
    invoke-static {p0}, LX/FYg;->e(LX/FYg;)V

    .line 2256416
    iget-object v0, p0, LX/FYg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FYd;

    iget v0, v0, LX/FYd;->b:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2256413
    iget-object v0, p0, LX/FYg;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2256414
    new-instance v1, LX/FYe;

    const/4 v2, 0x0

    invoke-virtual {v0, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v0}, LX/FYe;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2256410
    invoke-static {p0}, LX/FYg;->e(LX/FYg;)V

    .line 2256411
    check-cast p1, LX/FYe;

    iget-object v1, p1, LX/FYe;->l:Landroid/widget/TextView;

    iget-object v0, p0, LX/FYg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FYd;

    iget-object v0, v0, LX/FYd;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2256412
    return-void
.end method

.method public final b(I)Z
    .locals 4

    .prologue
    .line 2256402
    invoke-static {p0}, LX/FYg;->e(LX/FYg;)V

    .line 2256403
    iget-object v0, p0, LX/FYg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/FYg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FYd;

    .line 2256404
    iget v3, v0, LX/FYd;->c:I

    move v3, v3

    .line 2256405
    if-ne v3, p1, :cond_1

    .line 2256406
    :goto_1
    move-object v0, v0

    .line 2256407
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 2256408
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2256409
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2256401
    sget v0, LX/FYg;->a:I

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2256400
    iget-object v0, p0, LX/FYg;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/FYg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method
