.class public final enum LX/Gbn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gbn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gbn;

.field public static final enum ADD_PASSCODE:LX/Gbn;

.field public static final enum CHANGE_PASSCODE:LX/Gbn;

.field public static final enum OTHER_SESSION:LX/Gbn;

.field public static final enum REMEMBER_PASSWORD:LX/Gbn;

.field public static final enum REMOVE_ACCOUNT:LX/Gbn;

.field public static final enum REMOVE_PASSCODE:LX/Gbn;

.field public static final enum RESET_SETTINGS:LX/Gbn;

.field public static final enum USE_PASSCODE:LX/Gbn;

.field public static final enum USE_PASSWORD:LX/Gbn;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2370410
    new-instance v0, LX/Gbn;

    const-string v1, "ADD_PASSCODE"

    invoke-direct {v0, v1, v3}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->ADD_PASSCODE:LX/Gbn;

    .line 2370411
    new-instance v0, LX/Gbn;

    const-string v1, "CHANGE_PASSCODE"

    invoke-direct {v0, v1, v4}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->CHANGE_PASSCODE:LX/Gbn;

    .line 2370412
    new-instance v0, LX/Gbn;

    const-string v1, "REMOVE_PASSCODE"

    invoke-direct {v0, v1, v5}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->REMOVE_PASSCODE:LX/Gbn;

    .line 2370413
    new-instance v0, LX/Gbn;

    const-string v1, "REMOVE_ACCOUNT"

    invoke-direct {v0, v1, v6}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->REMOVE_ACCOUNT:LX/Gbn;

    .line 2370414
    new-instance v0, LX/Gbn;

    const-string v1, "REMEMBER_PASSWORD"

    invoke-direct {v0, v1, v7}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->REMEMBER_PASSWORD:LX/Gbn;

    .line 2370415
    new-instance v0, LX/Gbn;

    const-string v1, "USE_PASSCODE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->USE_PASSCODE:LX/Gbn;

    .line 2370416
    new-instance v0, LX/Gbn;

    const-string v1, "USE_PASSWORD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->USE_PASSWORD:LX/Gbn;

    .line 2370417
    new-instance v0, LX/Gbn;

    const-string v1, "RESET_SETTINGS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->RESET_SETTINGS:LX/Gbn;

    .line 2370418
    new-instance v0, LX/Gbn;

    const-string v1, "OTHER_SESSION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Gbn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbn;->OTHER_SESSION:LX/Gbn;

    .line 2370419
    const/16 v0, 0x9

    new-array v0, v0, [LX/Gbn;

    sget-object v1, LX/Gbn;->ADD_PASSCODE:LX/Gbn;

    aput-object v1, v0, v3

    sget-object v1, LX/Gbn;->CHANGE_PASSCODE:LX/Gbn;

    aput-object v1, v0, v4

    sget-object v1, LX/Gbn;->REMOVE_PASSCODE:LX/Gbn;

    aput-object v1, v0, v5

    sget-object v1, LX/Gbn;->REMOVE_ACCOUNT:LX/Gbn;

    aput-object v1, v0, v6

    sget-object v1, LX/Gbn;->REMEMBER_PASSWORD:LX/Gbn;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Gbn;->USE_PASSCODE:LX/Gbn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Gbn;->USE_PASSWORD:LX/Gbn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Gbn;->RESET_SETTINGS:LX/Gbn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Gbn;->OTHER_SESSION:LX/Gbn;

    aput-object v2, v0, v1

    sput-object v0, LX/Gbn;->$VALUES:[LX/Gbn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2370409
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gbn;
    .locals 1

    .prologue
    .line 2370407
    const-class v0, LX/Gbn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gbn;

    return-object v0
.end method

.method public static values()[LX/Gbn;
    .locals 1

    .prologue
    .line 2370408
    sget-object v0, LX/Gbn;->$VALUES:[LX/Gbn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gbn;

    return-object v0
.end method
