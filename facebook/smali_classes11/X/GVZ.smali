.class public final LX/GVZ;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/captcha/fragment/CaptchaFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V
    .locals 0

    .prologue
    .line 2359557
    iput-object p1, p0, LX/GVZ;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 2359555
    iget-object v0, p0, LX/GVZ;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-virtual {v0}, Lcom/facebook/captcha/fragment/CaptchaFragment;->b()V

    .line 2359556
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2359540
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2359541
    if-nez p1, :cond_0

    .line 2359542
    iget-object v0, p0, LX/GVZ;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-virtual {v0}, Lcom/facebook/captcha/fragment/CaptchaFragment;->b()V

    .line 2359543
    :goto_0
    return-void

    .line 2359544
    :cond_0
    iget-object v1, p0, LX/GVZ;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;

    .line 2359545
    invoke-virtual {v0}, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/captcha/fragment/CaptchaFragment;->n:Ljava/lang/String;

    .line 2359546
    invoke-virtual {v0}, Lcom/facebook/captcha/protocol/RequestCaptchaMethod$Result;->b()Ljava/lang/String;

    move-result-object v2

    .line 2359547
    iget-object v3, v1, Lcom/facebook/captcha/fragment/CaptchaFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object p1, Lcom/facebook/captcha/fragment/CaptchaFragment;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2359548
    invoke-static {v1}, Lcom/facebook/captcha/fragment/CaptchaFragment;->n(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    .line 2359549
    iget-object v0, p0, LX/GVZ;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    iget-object v0, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    .line 2359550
    iget-object v1, v0, LX/GVe;->a:LX/0Zb;

    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/GVd;->CAPTCHA_FETCH_REQUEST_SUCCEEDED:LX/GVd;

    invoke-virtual {v3}, LX/GVd;->getEventName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "captcha"

    .line 2359551
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2359552
    move-object v2, v2

    .line 2359553
    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2359554
    goto :goto_0
.end method
