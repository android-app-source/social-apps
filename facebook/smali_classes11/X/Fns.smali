.class public final LX/Fns;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2287817
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2287818
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2287819
    :goto_0
    return v1

    .line 2287820
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2287821
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2287822
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2287823
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2287824
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2287825
    const-string v5, "cover_photo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2287826
    const/4 v4, 0x0

    .line 2287827
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 2287828
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2287829
    :goto_2
    move v3, v4

    .line 2287830
    goto :goto_1

    .line 2287831
    :cond_2
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2287832
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 2287833
    :cond_3
    const-string v5, "url"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2287834
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2287835
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2287836
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2287837
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2287838
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2287839
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 2287840
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2287841
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 2287842
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2287843
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2287844
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_7

    if-eqz v6, :cond_7

    .line 2287845
    const-string v7, "focus"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2287846
    invoke-static {p0, p1}, LX/Fnr;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_3

    .line 2287847
    :cond_8
    const-string v7, "photo"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2287848
    invoke-static {p0, p1}, LX/Fnl;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_3

    .line 2287849
    :cond_9
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2287850
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 2287851
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 2287852
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_a
    move v3, v4

    move v5, v4

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 2287853
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2287854
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2287855
    if-eqz v0, :cond_4

    .line 2287856
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287857
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2287858
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287859
    if-eqz v2, :cond_2

    .line 2287860
    const-string v3, "focus"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287861
    const-wide/16 v8, 0x0

    .line 2287862
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2287863
    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 2287864
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_0

    .line 2287865
    const-string v6, "x"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287866
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 2287867
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 2287868
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_1

    .line 2287869
    const-string v6, "y"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287870
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 2287871
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2287872
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2287873
    if-eqz v2, :cond_3

    .line 2287874
    const-string v3, "photo"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287875
    invoke-static {p0, v2, p2, p3}, LX/Fnl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2287876
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2287877
    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2287878
    if-eqz v0, :cond_5

    .line 2287879
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287880
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2287881
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2287882
    if-eqz v0, :cond_6

    .line 2287883
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2287884
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2287885
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2287886
    return-void
.end method
