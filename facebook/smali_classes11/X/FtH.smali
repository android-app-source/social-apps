.class public final LX/FtH;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2298113
    const-class v1, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel;

    const v0, 0x4c4124a6    # 5.063132E7f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FavoriteMediaSuggestionsQuery"

    const-string v6, "39c111d4d9b559311e6ce5861aa45b39"

    const-string v7, "me"

    const-string v8, "10155069964336729"

    const-string v9, "10155259086996729"

    .line 2298114
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2298115
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2298116
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2298121
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2298122
    sparse-switch v0, :sswitch_data_0

    .line 2298123
    :goto_0
    return-object p1

    .line 2298124
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2298125
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2298126
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2298127
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5069ecaa -> :sswitch_0
        -0x1e419612 -> :sswitch_3
        0x1ed14f8e -> :sswitch_1
        0x6b4e17d1 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2298117
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2298118
    :goto_1
    return v0

    .line 2298119
    :pswitch_0
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2298120
    :pswitch_1
    const/4 v0, 0x6

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
