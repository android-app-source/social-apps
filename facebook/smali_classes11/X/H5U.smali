.class public LX/H5U;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3TK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3TK;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2424418
    iput-object p1, p0, LX/H5U;->a:LX/0Ot;

    .line 2424419
    iput-object p2, p0, LX/H5U;->b:LX/0Ot;

    .line 2424420
    return-void
.end method

.method public static a(LX/0QB;)LX/H5U;
    .locals 5

    .prologue
    .line 2424421
    const-class v1, LX/H5U;

    monitor-enter v1

    .line 2424422
    :try_start_0
    sget-object v0, LX/H5U;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424423
    sput-object v2, LX/H5U;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424426
    new-instance v3, LX/H5U;

    const/16 v4, 0x2eb

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xe59

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/H5U;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2424427
    move-object v0, v3

    .line 2424428
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424429
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5U;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424430
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/text/Spannable;LX/1De;I)V
    .locals 7
    .param p2    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    const/16 v6, 0x21

    const/4 v5, 0x0

    .line 2424432
    new-instance v0, LX/7Gs;

    const-string v1, "roboto"

    sget-object v2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v3, LX/0xr;->REGULAR:LX/0xr;

    const/4 v4, 0x0

    invoke-static {p1, v2, v3, v4}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7Gs;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v1

    invoke-interface {p0, v0, v5, v1, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2424433
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v1

    invoke-interface {p0, v0, v5, v1, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2424434
    return-void
.end method
