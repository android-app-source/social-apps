.class public LX/GEF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2U5;

.field private final b:LX/0tX;

.field public final c:LX/2U4;

.field private final d:LX/GG6;

.field private final e:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final f:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final g:LX/0W9;

.field private final h:LX/0sh;

.field private final i:LX/16I;


# direct methods
.method public constructor <init>(LX/0tX;LX/2U5;LX/2U4;LX/GG6;Ljava/util/concurrent/Executor;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0W9;LX/0sh;LX/16I;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331315
    iput-object p1, p0, LX/GEF;->b:LX/0tX;

    .line 2331316
    iput-object p2, p0, LX/GEF;->a:LX/2U5;

    .line 2331317
    iput-object p3, p0, LX/GEF;->c:LX/2U4;

    .line 2331318
    iput-object p4, p0, LX/GEF;->d:LX/GG6;

    .line 2331319
    iput-object p5, p0, LX/GEF;->e:Ljava/util/concurrent/Executor;

    .line 2331320
    iput-object p6, p0, LX/GEF;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2331321
    iput-object p7, p0, LX/GEF;->g:LX/0W9;

    .line 2331322
    iput-object p8, p0, LX/GEF;->h:LX/0sh;

    .line 2331323
    iput-object p9, p0, LX/GEF;->i:LX/16I;

    .line 2331324
    return-void
.end method

.method private static a(LX/GEF;LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GGz;ZLX/0zS;)V
    .locals 7

    .prologue
    .line 2331325
    new-instance v4, LX/GEB;

    invoke-direct {v4, p0, p4}, LX/GEB;-><init>(LX/GEF;LX/GGz;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LX/GEF;->a(LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GEA;ZLX/0zS;)V

    .line 2331326
    return-void
.end method

.method public static a$redex0(LX/GEF;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 2331327
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$EventInfoModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3

    .line 2331328
    if-nez v5, :cond_0

    .line 2331329
    const/4 v0, 0x0

    .line 2331330
    :goto_0
    return-object v0

    .line 2331331
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2331332
    :cond_0
    const-string v3, ""

    .line 2331333
    invoke-virtual {v4, v5, v6}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    .line 2331334
    invoke-virtual {v4, v5, v6}, LX/15i;->g(II)I

    move-result v0

    .line 2331335
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 2331336
    invoke-virtual {v4, v5, v6}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2331337
    :goto_2
    new-instance v3, LX/A9E;

    invoke-direct {v3}, LX/A9E;-><init>()V

    invoke-virtual {v4, v5, v2}, LX/15i;->l(II)D

    move-result-wide v6

    .line 2331338
    iput-wide v6, v3, LX/A9E;->f:D

    .line 2331339
    move-object v2, v3

    .line 2331340
    invoke-virtual {v4, v5, v1}, LX/15i;->l(II)D

    move-result-wide v4

    .line 2331341
    iput-wide v4, v2, LX/A9E;->h:D

    .line 2331342
    move-object v1, v2

    .line 2331343
    iput-object v0, v1, LX/A9E;->a:Ljava/lang/String;

    .line 2331344
    move-object v0, v1

    .line 2331345
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 2331346
    iput-wide v2, v0, LX/A9E;->j:D

    .line 2331347
    move-object v0, v0

    .line 2331348
    iget-object v1, p0, LX/GEF;->g:LX/0W9;

    invoke-static {v1}, LX/GNL;->a(LX/0W9;)Ljava/lang/String;

    move-result-object v1

    .line 2331349
    iput-object v1, v0, LX/A9E;->d:Ljava/lang/String;

    .line 2331350
    move-object v0, v0

    .line 2331351
    invoke-virtual {v0}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2331352
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move-object v0, v3

    goto :goto_2
.end method

.method public static a$redex0(LX/GEF;Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2331353
    if-nez p1, :cond_1

    .line 2331354
    :cond_0
    :goto_0
    return-object v0

    .line 2331355
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v1

    .line 2331356
    if-eqz v1, :cond_0

    .line 2331357
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    .line 2331358
    :goto_1
    new-instance v2, LX/A9E;

    invoke-direct {v2}, LX/A9E;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->a()D

    move-result-wide v4

    .line 2331359
    iput-wide v4, v2, LX/A9E;->f:D

    .line 2331360
    move-object v2, v2

    .line 2331361
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->b()D

    move-result-wide v4

    .line 2331362
    iput-wide v4, v2, LX/A9E;->h:D

    .line 2331363
    move-object v1, v2

    .line 2331364
    iput-object v0, v1, LX/A9E;->a:Ljava/lang/String;

    .line 2331365
    move-object v0, v1

    .line 2331366
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 2331367
    iput-wide v2, v0, LX/A9E;->j:D

    .line 2331368
    move-object v0, v0

    .line 2331369
    iget-object v1, p0, LX/GEF;->g:LX/0W9;

    invoke-static {v1}, LX/GNL;->a(LX/0W9;)Ljava/lang/String;

    move-result-object v1

    .line 2331370
    iput-object v1, v0, LX/A9E;->d:Ljava/lang/String;

    .line 2331371
    move-object v0, v0

    .line 2331372
    invoke-virtual {v0}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    goto :goto_0

    .line 2331373
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GEA;LX/GGz;Z)V
    .locals 7

    .prologue
    .line 2331374
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2331375
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2331376
    const-string v0, "no callback"

    invoke-static {p4, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2331377
    iget-object v0, p0, LX/GEF;->h:LX/0sh;

    iget-object v1, p0, LX/GEF;->c:LX/2U4;

    invoke-virtual {v1, p2, p3, p1}, LX/2U4;->a(Ljava/lang/String;Ljava/lang/String;LX/8wL;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0sh;->d(LX/0zO;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GEF;->h:LX/0sh;

    iget-object v1, p0, LX/GEF;->c:LX/2U4;

    invoke-virtual {v1, p3, p1, p6}, LX/2U4;->a(Ljava/lang/String;LX/8wL;Z)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0sh;->d(LX/0zO;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2331378
    sget-object v6, LX/0zS;->a:LX/0zS;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p6

    invoke-virtual/range {v0 .. v6}, LX/GEF;->a(LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GEA;ZLX/0zS;)V

    .line 2331379
    iget-object v0, p0, LX/GEF;->i:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2331380
    sget-object v6, LX/0zS;->d:LX/0zS;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move v5, p6

    invoke-static/range {v0 .. v6}, LX/GEF;->a(LX/GEF;LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GGz;ZLX/0zS;)V

    .line 2331381
    :cond_0
    :goto_0
    return-void

    .line 2331382
    :cond_1
    sget-object v6, LX/0zS;->d:LX/0zS;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p6

    invoke-virtual/range {v0 .. v6}, LX/GEF;->a(LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GEA;ZLX/0zS;)V

    goto :goto_0
.end method

.method public final a(LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GEA;ZLX/0zS;)V
    .locals 5

    .prologue
    const v3, 0x970001

    .line 2331383
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2331384
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2331385
    const-string v0, "no callback"

    invoke-static {p4, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2331386
    iget-boolean v0, p6, LX/0zS;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2331387
    :goto_0
    if-eqz v0, :cond_0

    .line 2331388
    iget-object v1, p0, LX/GEF;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2331389
    iget-object v1, p0, LX/GEF;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p1}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2331390
    :cond_0
    const-string v1, "FetchPromotion"

    invoke-static {v1}, LX/2U5;->a(Ljava/lang/String;)LX/0v6;

    move-result-object v1

    .line 2331391
    iget-object v2, p0, LX/GEF;->c:LX/2U4;

    invoke-virtual {v2, p2, p3, p1}, LX/2U4;->a(Ljava/lang/String;Ljava/lang/String;LX/8wL;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v2

    iget-object v3, p0, LX/GEF;->e:Ljava/util/concurrent/Executor;

    invoke-virtual {v2, v3}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v2

    new-instance v3, LX/GEC;

    invoke-direct {v3, p0}, LX/GEC;-><init>(LX/GEF;)V

    invoke-virtual {v2, v3}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v2

    .line 2331392
    iget-object v3, p0, LX/GEF;->c:LX/2U4;

    invoke-virtual {v3, p3, p1, p5}, LX/2U4;->a(Ljava/lang/String;LX/8wL;Z)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, p6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v3

    iget-object v4, p0, LX/GEF;->e:Ljava/util/concurrent/Executor;

    invoke-virtual {v3, v4}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v3

    new-instance v4, LX/GED;

    invoke-direct {v4, p0, p2, p3}, LX/GED;-><init>(LX/GEF;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v3

    .line 2331393
    new-instance v4, LX/0zX;

    iget-object p2, v2, LX/0zX;->a:LX/0v9;

    iget-object p3, v3, LX/0zX;->a:LX/0v9;

    invoke-static {p2, p3}, LX/0v9;->a(LX/0v9;LX/0v9;)LX/0v9;

    move-result-object p2

    invoke-direct {v4, p2}, LX/0zX;-><init>(LX/0v9;)V

    move-object v2, v4

    .line 2331394
    iget-object v3, p0, LX/GEF;->e:Ljava/util/concurrent/Executor;

    invoke-virtual {v2, v3}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v2

    new-instance v3, LX/GEE;

    invoke-direct {v3, p0, p1, p4, v0}, LX/GEE;-><init>(LX/GEF;LX/8wL;LX/GEA;Z)V

    invoke-virtual {v2, v3}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 2331395
    iget-object v0, p0, LX/GEF;->b:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0v6;)V

    .line 2331396
    return-void

    .line 2331397
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
