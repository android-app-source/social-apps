.class public final LX/F8B;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2203509
    const-class v1, Lcom/facebook/growth/friendfinder/graphql/FetchInvitableContactsQueryGraphQLModels$InvitableContactsQueryModel;

    const v0, -0x6e1b8f63

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "InvitableContactsQuery"

    const-string v6, "e36a05eed12a015255acd9fa56a5d326"

    const-string v7, "viewer"

    const-string v8, "10155069967276729"

    const-string v9, "10155259089061729"

    .line 2203510
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2203511
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2203512
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2203517
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2203518
    sparse-switch v0, :sswitch_data_0

    .line 2203519
    :goto_0
    return-object p1

    .line 2203520
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2203521
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2203522
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2203523
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2203524
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4c4cea74 -> :sswitch_2
        -0x49323afe -> :sswitch_4
        0x58705dc -> :sswitch_3
        0x4f66c4a1 -> :sswitch_0
        0x5371764e -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2203513
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2203514
    :goto_1
    return v0

    .line 2203515
    :pswitch_0
    const-string v2, "0"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2203516
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
