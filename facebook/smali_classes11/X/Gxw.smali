.class public final enum LX/Gxw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gxw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gxw;

.field public static final enum SWITCH:LX/Gxw;

.field public static final enum SWITCH_PROMO:LX/Gxw;

.field public static final enum VISIT:LX/Gxw;


# instance fields
.field private final mAnalyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2408695
    new-instance v0, LX/Gxw;

    const-string v1, "VISIT"

    const-string v2, "language_switcher_visit"

    invoke-direct {v0, v1, v3, v2}, LX/Gxw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxw;->VISIT:LX/Gxw;

    .line 2408696
    new-instance v0, LX/Gxw;

    const-string v1, "SWITCH"

    const-string v2, "language_switcher_switch"

    invoke-direct {v0, v1, v4, v2}, LX/Gxw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxw;->SWITCH:LX/Gxw;

    .line 2408697
    new-instance v0, LX/Gxw;

    const-string v1, "SWITCH_PROMO"

    const-string v2, "language_switcher_auto_uri"

    invoke-direct {v0, v1, v5, v2}, LX/Gxw;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gxw;->SWITCH_PROMO:LX/Gxw;

    .line 2408698
    const/4 v0, 0x3

    new-array v0, v0, [LX/Gxw;

    sget-object v1, LX/Gxw;->VISIT:LX/Gxw;

    aput-object v1, v0, v3

    sget-object v1, LX/Gxw;->SWITCH:LX/Gxw;

    aput-object v1, v0, v4

    sget-object v1, LX/Gxw;->SWITCH_PROMO:LX/Gxw;

    aput-object v1, v0, v5

    sput-object v0, LX/Gxw;->$VALUES:[LX/Gxw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2408699
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2408700
    iput-object p3, p0, LX/Gxw;->mAnalyticsName:Ljava/lang/String;

    .line 2408701
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gxw;
    .locals 1

    .prologue
    .line 2408702
    const-class v0, LX/Gxw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gxw;

    return-object v0
.end method

.method public static values()[LX/Gxw;
    .locals 1

    .prologue
    .line 2408703
    sget-object v0, LX/Gxw;->$VALUES:[LX/Gxw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gxw;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2408704
    iget-object v0, p0, LX/Gxw;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2408705
    iget-object v0, p0, LX/Gxw;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method
