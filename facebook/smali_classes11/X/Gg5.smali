.class public final LX/Gg5;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Gg6;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/GfY;

.field public b:LX/1X1;

.field public c:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/Gg6;


# direct methods
.method public constructor <init>(LX/Gg6;)V
    .locals 1

    .prologue
    .line 2378476
    iput-object p1, p0, LX/Gg5;->d:LX/Gg6;

    .line 2378477
    move-object v0, p1

    .line 2378478
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2378479
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2378480
    const-string v0, "PaginatedPymlPageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2378481
    if-ne p0, p1, :cond_1

    .line 2378482
    :cond_0
    :goto_0
    return v0

    .line 2378483
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2378484
    goto :goto_0

    .line 2378485
    :cond_3
    check-cast p1, LX/Gg5;

    .line 2378486
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2378487
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2378488
    if-eq v2, v3, :cond_0

    .line 2378489
    iget-object v2, p0, LX/Gg5;->a:LX/GfY;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Gg5;->a:LX/GfY;

    iget-object v3, p1, LX/Gg5;->a:LX/GfY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2378490
    goto :goto_0

    .line 2378491
    :cond_5
    iget-object v2, p1, LX/Gg5;->a:LX/GfY;

    if-nez v2, :cond_4

    .line 2378492
    :cond_6
    iget-object v2, p0, LX/Gg5;->b:LX/1X1;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Gg5;->b:LX/1X1;

    iget-object v3, p1, LX/Gg5;->b:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2378493
    goto :goto_0

    .line 2378494
    :cond_8
    iget-object v2, p1, LX/Gg5;->b:LX/1X1;

    if-nez v2, :cond_7

    .line 2378495
    :cond_9
    iget-object v2, p0, LX/Gg5;->c:LX/1Pp;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Gg5;->c:LX/1Pp;

    iget-object v3, p1, LX/Gg5;->c:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2378496
    goto :goto_0

    .line 2378497
    :cond_a
    iget-object v2, p1, LX/Gg5;->c:LX/1Pp;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2378498
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Gg5;

    .line 2378499
    iget-object v1, v0, LX/Gg5;->b:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Gg5;->b:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/Gg5;->b:LX/1X1;

    .line 2378500
    return-object v0

    .line 2378501
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
