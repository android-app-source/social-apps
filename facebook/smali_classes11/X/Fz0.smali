.class public LX/Fz0;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/03V;

.field private final c:LX/BPp;

.field private final d:LX/Fyh;

.field private final e:LX/Fz2;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Fz2;LX/03V;LX/BPp;LX/Fyh;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Fz2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2307420
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2307421
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/Fz0;->a:Landroid/content/Context;

    .line 2307422
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fz2;

    iput-object v0, p0, LX/Fz0;->e:LX/Fz2;

    .line 2307423
    iput-object p3, p0, LX/Fz0;->b:LX/03V;

    .line 2307424
    iput-object p4, p0, LX/Fz0;->c:LX/BPp;

    .line 2307425
    iput-object p5, p0, LX/Fz0;->d:LX/Fyh;

    .line 2307426
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2307413
    invoke-static {}, LX/Fyz;->values()[LX/Fyz;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2307414
    sget-object v1, LX/Fyy;->a:[I

    invoke-virtual {v0}, LX/Fyz;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2307415
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown item type for TimelineInfoReviewAdapter of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2307416
    :pswitch_0
    new-instance v0, Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iget-object v1, p0, LX/Fz0;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;-><init>(Landroid/content/Context;)V

    .line 2307417
    :goto_0
    return-object v0

    .line 2307418
    :pswitch_1
    new-instance v0, LX/Fyx;

    iget-object v1, p0, LX/Fz0;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Fyx;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2307419
    :pswitch_2
    new-instance v0, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    iget-object v1, p0, LX/Fz0;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    .line 2307349
    instance-of v0, p3, LX/Fyx;

    if-eqz v0, :cond_1

    instance-of v0, p2, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    if-eqz v0, :cond_1

    .line 2307350
    check-cast p3, LX/Fyx;

    iget-object v0, p0, LX/Fz0;->c:LX/BPp;

    check-cast p2, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    iget-object v1, p0, LX/Fz0;->e:LX/Fz2;

    iget-object v2, p0, LX/Fz0;->d:LX/Fyh;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2307351
    iget-object v5, v1, LX/Fz2;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    move-object v5, v5

    .line 2307352
    iput-object v5, p3, LX/Fyx;->d:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307353
    iget-object v5, p3, LX/Fyx;->c:LX/Fys;

    .line 2307354
    new-instance p0, LX/Fyr;

    .line 2307355
    new-instance p5, LX/Fyq;

    invoke-static {v5}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v5}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object p1

    check-cast p1, LX/0Sh;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p4

    check-cast p4, LX/03V;

    invoke-direct {p5, v6, p1, p4}, LX/Fyq;-><init>(LX/0tX;LX/0Sh;LX/03V;)V

    .line 2307356
    move-object v6, p5

    .line 2307357
    check-cast v6, LX/Fyq;

    invoke-direct {p0, v0, v1, v6}, LX/Fyr;-><init>(LX/BPp;LX/Fz2;LX/Fyq;)V

    .line 2307358
    move-object v5, p0

    .line 2307359
    iput-object v5, p3, LX/Fyx;->e:LX/Fyr;

    .line 2307360
    iput-object p2, p3, LX/Fyx;->f:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    .line 2307361
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->n()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v5

    iput-object v5, p3, LX/Fyx;->g:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    .line 2307362
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->j()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p3, LX/Fyx;->h:Ljava/lang/String;

    .line 2307363
    iget-object v5, p3, LX/Fyx;->d:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307364
    iget-boolean v6, v5, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->g:Z

    move v5, v6

    .line 2307365
    if-eqz v5, :cond_4

    .line 2307366
    invoke-virtual {p3}, LX/Fyx;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0055

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 2307367
    new-instance v6, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v6, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2307368
    iput-object v6, p3, LX/4oC;->a:Landroid/graphics/drawable/Drawable;

    .line 2307369
    iget-object v5, p3, LX/Fyx;->n:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2307370
    :goto_0
    iget-object v5, p3, LX/Fyx;->g:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    .line 2307371
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->cm_()LX/174;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->cl_()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->c()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    const/4 v6, 0x1

    :goto_1
    move v5, v6

    .line 2307372
    if-nez v5, :cond_5

    .line 2307373
    iget-object v4, p3, LX/Fyx;->a:LX/03V;

    const-string v5, "identitygrowth"

    const-string v6, "The profileQuestions argument of this function should not be null or empty, please check hasValidData or manually check it before passing in"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307374
    iget-object v4, p3, LX/Fyx;->e:LX/Fyr;

    invoke-virtual {v4}, LX/Fyr;->b()V

    .line 2307375
    :cond_0
    :goto_2
    return-void

    .line 2307376
    :cond_1
    instance-of v0, p3, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    if-eqz v0, :cond_3

    instance-of v0, p2, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    if-eqz v0, :cond_3

    .line 2307377
    check-cast p3, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    check-cast p2, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    iget-object v0, p0, LX/Fz0;->d:LX/Fyh;

    .line 2307378
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;->b()LX/174;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;->a()LX/1Fb;

    move-result-object v1

    if-nez v1, :cond_8

    .line 2307379
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {p3, v1}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->setVisibility(I)V

    .line 2307380
    :goto_3
    goto :goto_2

    .line 2307381
    :cond_3
    instance-of v0, p3, Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    if-eqz v0, :cond_0

    instance-of v0, p2, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    if-eqz v0, :cond_0

    .line 2307382
    check-cast p3, Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    check-cast p2, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    iget-object v0, p0, LX/Fz0;->d:LX/Fyh;

    iget-object v1, p0, LX/Fz0;->e:LX/Fz2;

    iget-object v2, p0, LX/Fz0;->c:LX/BPp;

    invoke-virtual {p3, p2, v0, v1, v2}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;LX/Fyh;LX/Fz2;LX/BPp;)V

    goto :goto_2

    .line 2307383
    :cond_4
    const/4 v5, 0x0

    .line 2307384
    iput-object v5, p3, LX/4oC;->a:Landroid/graphics/drawable/Drawable;

    .line 2307385
    iget-object v5, p3, LX/Fyx;->n:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2307386
    :cond_5
    invoke-virtual {p3, v3}, LX/Fyx;->setVisibility(I)V

    .line 2307387
    iget-object v5, p3, LX/Fyx;->j:Lcom/facebook/timeline/inforeview/InfoReviewItemView;

    iget-object v6, p3, LX/Fyx;->f:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    invoke-virtual {v5, v6, v2, v1, v0}, Lcom/facebook/timeline/inforeview/InfoReviewItemView;->a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;LX/Fyh;LX/Fz2;LX/BPp;)V

    .line 2307388
    const v5, 0x7f0d264c    # 1.8762E38f

    invoke-virtual {p3, v5}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2307389
    iget-object v5, p3, LX/Fyx;->o:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    invoke-virtual {v5, v3}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->setVisibility(I)V

    .line 2307390
    iget-object v5, p3, LX/Fyx;->o:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    iget-object v6, p3, LX/Fyx;->d:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    iget-object p0, p3, LX/Fyx;->g:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;->cl_()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->a(LX/Fym;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2307391
    iget-object v5, p3, LX/Fyx;->k:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2307392
    iget-object v5, p3, LX/Fyx;->m:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    iget-object v6, p3, LX/Fyx;->g:Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    iget-object p0, p3, LX/Fyx;->d:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    invoke-virtual {v5, v6, p0, v3, v3}, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->a(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;ZZ)V

    .line 2307393
    iget-object v5, p3, LX/Fyx;->m:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;

    new-instance v6, LX/Fyw;

    invoke-direct {v6, p3}, LX/Fyw;-><init>(LX/Fyx;)V

    .line 2307394
    iput-object v6, v5, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionOptionListView;->g:LX/Fyv;

    .line 2307395
    iget-object v5, p3, LX/Fyx;->k:Lcom/facebook/resources/ui/FbButton;

    const v6, 0x7f081533

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2307396
    iget-object v5, p3, LX/Fyx;->k:Lcom/facebook/resources/ui/FbButton;

    iget-object v6, p3, LX/Fyx;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2307397
    iget-object v5, p3, LX/Fyx;->l:Lcom/facebook/resources/ui/FbButton;

    const v6, 0x7f080019

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2307398
    iget-object v5, p3, LX/Fyx;->l:Lcom/facebook/resources/ui/FbButton;

    iget-object v6, p3, LX/Fyx;->d:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    .line 2307399
    iget-object p0, v6, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;->a:Ljava/lang/String;

    move-object v6, p0

    .line 2307400
    if-eqz v6, :cond_6

    move v3, v4

    :cond_6
    invoke-virtual {v5, v3}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2307401
    goto/16 :goto_2

    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 2307402
    :cond_8
    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->setVisibility(I)V

    .line 2307403
    iput-object v0, p3, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->l:LX/Fyh;

    .line 2307404
    iput-object p2, p3, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->m:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    .line 2307405
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;->b()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2307406
    iget-object v1, p3, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;->a()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_3
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2307407
    iget-object v0, p0, LX/Fz0;->e:LX/Fz2;

    invoke-virtual {v0}, LX/BPB;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2307408
    const/4 v0, 0x0

    .line 2307409
    :goto_0
    return v0

    .line 2307410
    :cond_0
    iget-object v0, p0, LX/Fz0;->e:LX/Fz2;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v1, v0, 0x0

    .line 2307411
    iget-object v0, p0, LX/Fz0;->e:LX/Fz2;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2307412
    add-int/lit8 v0, v1, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2307427
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2307428
    invoke-virtual {p0}, LX/Fz0;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2307429
    iget-object v0, p0, LX/Fz0;->e:LX/Fz2;

    invoke-virtual {v0}, LX/BPB;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2307430
    iget-object v0, p0, LX/Fz0;->b:LX/03V;

    const-string v1, "TimelineInfoReviewAdapter.no_valid_data"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No valid info review item for position "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307431
    const/4 v0, 0x0

    .line 2307432
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 2307433
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2307434
    goto :goto_1

    .line 2307435
    :cond_2
    iget-object v0, p0, LX/Fz0;->e:LX/Fz2;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2307436
    iget-object v1, p0, LX/Fz0;->e:LX/Fz2;

    invoke-virtual {v1}, LX/BPB;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    if-lt p1, v0, :cond_3

    .line 2307437
    iget-object v0, p0, LX/Fz0;->e:LX/Fz2;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    move-result-object v0

    goto :goto_2

    .line 2307438
    :cond_3
    iget-object v0, p0, LX/Fz0;->e:LX/Fz2;

    invoke-virtual {v0}, LX/BPB;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2307334
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 5

    .prologue
    .line 2307335
    invoke-virtual {p0, p1}, LX/Fz0;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2307336
    instance-of v1, v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    if-eqz v1, :cond_0

    .line 2307337
    sget-object v0, LX/Fyz;->OVERFLOW_LINK:LX/Fyz;

    .line 2307338
    :goto_0
    move-object v0, v0

    .line 2307339
    invoke-virtual {v0}, LX/Fyz;->ordinal()I

    move-result v0

    return v0

    .line 2307340
    :cond_0
    instance-of v1, v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    if-eqz v1, :cond_3

    .line 2307341
    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    .line 2307342
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->n()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$ProfileQuestionFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2307343
    :cond_1
    sget-object v0, LX/Fyz;->COLLAPSED_ITEM:LX/Fyz;

    goto :goto_0

    .line 2307344
    :cond_2
    sget-object v0, LX/Fyz;->EXPANDED_PROFILE_QUESTION_ITEM:LX/Fyz;

    goto :goto_0

    .line 2307345
    :cond_3
    iget-object v1, p0, LX/Fz0;->b:LX/03V;

    const-string v2, "TimelineInfoReviewAdapter.unknown_viewtype"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown view type for position: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2307346
    sget-object v0, LX/Fyz;->UNKNOWN:LX/Fyz;

    goto :goto_0

    .line 2307347
    :cond_4
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2307348
    invoke-static {}, LX/Fyz;->values()[LX/Fyz;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
