.class public LX/Gbg;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/auth/credentials/DBLFacebookCredentials;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/10M;

.field public d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/10M;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2370350
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2370351
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Gbg;->b:Ljava/util/List;

    .line 2370352
    iput-object p1, p0, LX/Gbg;->a:Landroid/content/Context;

    .line 2370353
    iput-object p2, p0, LX/Gbg;->c:LX/10M;

    .line 2370354
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2370349
    iget-object v0, p0, LX/Gbg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2370348
    iget-object v0, p0, LX/Gbg;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2370339
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2370340
    if-nez p2, :cond_0

    .line 2370341
    iget-object v0, p0, LX/Gbg;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, LX/Gbg;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2370342
    :cond_0
    invoke-virtual {p0, p1}, LX/Gbg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2370343
    const v1, 0x7f0d0c10

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;

    .line 2370344
    iget-object v2, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/devicebasedlogin/ui/DBLProfilePhotoView;->setImage(Ljava/lang/String;)V

    .line 2370345
    const v1, 0x7f0d0bf2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2370346
    iget-object v0, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2370347
    return-object p2
.end method
