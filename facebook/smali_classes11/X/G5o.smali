.class public LX/G5o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/G5o;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G5n;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Sj;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Sj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/G5n;",
            ">;",
            "LX/0Sj;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319395
    iput-object p1, p0, LX/G5o;->a:LX/0Or;

    .line 2319396
    iput-object p2, p0, LX/G5o;->b:LX/0Sj;

    .line 2319397
    return-void
.end method

.method public static a(LX/0QB;)LX/G5o;
    .locals 5

    .prologue
    .line 2319398
    sget-object v0, LX/G5o;->c:LX/G5o;

    if-nez v0, :cond_1

    .line 2319399
    const-class v1, LX/G5o;

    monitor-enter v1

    .line 2319400
    :try_start_0
    sget-object v0, LX/G5o;->c:LX/G5o;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2319401
    if-eqz v2, :cond_0

    .line 2319402
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2319403
    new-instance v4, LX/G5o;

    const/16 v3, 0x387e

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Si;->a(LX/0QB;)LX/0Si;

    move-result-object v3

    check-cast v3, LX/0Sj;

    invoke-direct {v4, p0, v3}, LX/G5o;-><init>(LX/0Or;LX/0Sj;)V

    .line 2319404
    move-object v0, v4

    .line 2319405
    sput-object v0, LX/G5o;->c:LX/G5o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2319406
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2319407
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2319408
    :cond_1
    sget-object v0, LX/G5o;->c:LX/G5o;

    return-object v0

    .line 2319409
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2319410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/G5p;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G5p;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2319390
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2319391
    new-instance v1, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/facebook/videocodec/trimmer/VideoTrimmer$1;-><init>(LX/G5o;LX/G5p;Lcom/google/common/util/concurrent/SettableFuture;)V

    const-string v2, "Video Trimmer"

    const v3, -0x36a9b591

    invoke-static {v1, v2, v3}, LX/00l;->a(Ljava/lang/Runnable;Ljava/lang/String;I)Ljava/lang/Thread;

    move-result-object v1

    .line 2319392
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 2319393
    return-object v0
.end method
