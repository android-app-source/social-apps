.class public final LX/GJr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field public final synthetic a:LX/GJs;


# direct methods
.method public constructor <init>(LX/GJs;)V
    .locals 0

    .prologue
    .line 2339840
    iput-object p1, p0, LX/GJr;->a:LX/GJs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x3e8

    .line 2339841
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2339842
    iget-object v1, p0, LX/GJr;->a:LX/GJs;

    iget-object v1, v1, LX/GJs;->a:LX/GJt;

    iget-object v1, v1, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339843
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v2

    .line 2339844
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v2

    mul-long/2addr v2, v6

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2339845
    invoke-virtual {v0, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 2339846
    iget-object v1, p0, LX/GJr;->a:LX/GJs;

    iget-object v1, v1, LX/GJs;->a:LX/GJt;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 2339847
    iput-wide v2, v1, LX/GJt;->f:J

    .line 2339848
    invoke-virtual {p1}, Landroid/widget/DatePicker;->getMinDate()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Landroid/widget/DatePicker;->getMaxDate()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-gez v0, :cond_1

    .line 2339849
    :cond_0
    iget-object v0, p0, LX/GJr;->a:LX/GJs;

    invoke-virtual {v0, p1}, LX/GJs;->onClick(Landroid/view/View;)V

    .line 2339850
    :goto_0
    return-void

    .line 2339851
    :cond_1
    iget-object v0, p0, LX/GJr;->a:LX/GJs;

    iget-object v0, v0, LX/GJs;->a:LX/GJt;

    iget-object v0, v0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339852
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2339853
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v0

    mul-long/2addr v0, v6

    iget-object v2, p0, LX/GJr;->a:LX/GJs;

    iget-object v2, v2, LX/GJs;->a:LX/GJt;

    iget-wide v2, v2, LX/GJt;->f:J

    invoke-static {v0, v1, v2, v3}, LX/GG6;->a(JJ)I

    move-result v0

    .line 2339854
    iget-object v1, p0, LX/GJr;->a:LX/GJs;

    iget-object v1, v1, LX/GJs;->a:LX/GJt;

    iget-object v1, v1, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    iget-object v2, p0, LX/GJr;->a:LX/GJs;

    iget-object v2, v2, LX/GJs;->a:LX/GJt;

    iget-wide v2, v2, LX/GJt;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->setCustomDurationDate(Ljava/lang/Long;)V

    .line 2339855
    iget-object v1, p0, LX/GJr;->a:LX/GJs;

    iget-object v1, v1, LX/GJs;->a:LX/GJt;

    .line 2339856
    invoke-static {v1, v0}, LX/GJt;->a$redex0(LX/GJt;I)V

    .line 2339857
    goto :goto_0
.end method
