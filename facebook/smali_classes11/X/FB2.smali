.class public LX/FB2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Gl;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/FB2;


# instance fields
.field private final a:LX/0V8;

.field public final b:Landroid/content/pm/PackageManager;

.field private final c:LX/0l0;

.field private final d:LX/0kb;

.field private final e:LX/0Yi;


# direct methods
.method public constructor <init>(LX/0V8;Landroid/content/pm/PackageManager;LX/0l0;LX/0kb;LX/0Yi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2208678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208679
    iput-object p1, p0, LX/FB2;->a:LX/0V8;

    .line 2208680
    iput-object p2, p0, LX/FB2;->b:Landroid/content/pm/PackageManager;

    .line 2208681
    iput-object p3, p0, LX/FB2;->c:LX/0l0;

    .line 2208682
    iput-object p4, p0, LX/FB2;->d:LX/0kb;

    .line 2208683
    iput-object p5, p0, LX/FB2;->e:LX/0Yi;

    .line 2208684
    return-void
.end method

.method public static a(LX/0QB;)LX/FB2;
    .locals 9

    .prologue
    .line 2208665
    sget-object v0, LX/FB2;->f:LX/FB2;

    if-nez v0, :cond_1

    .line 2208666
    const-class v1, LX/FB2;

    monitor-enter v1

    .line 2208667
    :try_start_0
    sget-object v0, LX/FB2;->f:LX/FB2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2208668
    if-eqz v2, :cond_0

    .line 2208669
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2208670
    new-instance v3, LX/FB2;

    invoke-static {v0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v4

    check-cast v4, LX/0V8;

    invoke-static {v0}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v5

    check-cast v5, Landroid/content/pm/PackageManager;

    invoke-static {v0}, LX/0l0;->a(LX/0QB;)LX/0l0;

    move-result-object v6

    check-cast v6, LX/0l0;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v7

    check-cast v7, LX/0kb;

    invoke-static {v0}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v8

    check-cast v8, LX/0Yi;

    invoke-direct/range {v3 .. v8}, LX/FB2;-><init>(LX/0V8;Landroid/content/pm/PackageManager;LX/0l0;LX/0kb;LX/0Yi;)V

    .line 2208671
    move-object v0, v3

    .line 2208672
    sput-object v0, LX/FB2;->f:LX/FB2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2208673
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2208674
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2208675
    :cond_1
    sget-object v0, LX/FB2;->f:LX/FB2;

    return-object v0

    .line 2208676
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2208677
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/util/Map;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x3e8

    .line 2208632
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 2208633
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v1

    .line 2208634
    const-string v2, "image_file_bytes"

    invoke-virtual {v1, v2}, LX/009;->getCustomData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2208635
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2208636
    const-string v1, "image_cache_size_bytes"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2208637
    :goto_0
    iget-object v1, p0, LX/FB2;->e:LX/0Yi;

    .line 2208638
    iget-object v8, v1, LX/0Yi;->k:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    iget-wide v10, v1, LX/0Yi;->D:J

    sub-long/2addr v8, v10

    move-wide v2, v8

    .line 2208639
    div-long/2addr v2, v6

    .line 2208640
    iget-object v1, p0, LX/FB2;->e:LX/0Yi;

    .line 2208641
    iget-object v8, v1, LX/0Yi;->k:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    iget-wide v10, v1, LX/0Yi;->E:J

    sub-long/2addr v8, v10

    move-wide v4, v8

    .line 2208642
    div-long/2addr v4, v6

    .line 2208643
    const-string v1, "seconds_since_cold_start"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2208644
    const-string v1, "seconds_since_warm_start"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2208645
    instance-of v1, p1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 2208646
    const-string v1, "current_activity"

    check-cast p1, Landroid/app/Activity;

    invoke-static {p1}, LX/0l0;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2208647
    :cond_0
    iget-object v1, p0, LX/FB2;->a:LX/0V8;

    sget-object v2, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v1, v2}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v2

    .line 2208648
    const-string v1, "free_internal_storage_bytes"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2208649
    const/4 v1, 0x0

    .line 2208650
    :try_start_1
    iget-object v2, p0, LX/FB2;->b:Landroid/content/pm/PackageManager;

    invoke-static {}, LX/007;->p()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 2208651
    :goto_1
    move-object v1, v1

    .line 2208652
    const-string v2, "first_install_time"

    iget-wide v4, v1, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2208653
    const-string v2, "last_upgrade_time"

    iget-wide v4, v1, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2208654
    iget-object v1, p0, LX/FB2;->d:LX/0kb;

    .line 2208655
    iget-object v2, v1, LX/0kb;->t:LX/0kh;

    move-object v1, v2

    .line 2208656
    if-eqz v1, :cond_2

    .line 2208657
    iget v2, v1, LX/0kh;->c:I

    move v2, v2

    .line 2208658
    if-ltz v2, :cond_1

    const/16 v3, 0x64

    if-gt v2, v3, :cond_1

    .line 2208659
    const-string v3, "inet_cond"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2208660
    :cond_1
    invoke-virtual {v1}, LX/0kh;->c()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    .line 2208661
    if-eqz v1, :cond_2

    .line 2208662
    const-string v2, "connection_state"

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2208663
    :cond_2
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    :catch_0
    goto/16 :goto_0

    :catch_1
    goto :goto_1

    .line 2208664
    :catch_2
    goto :goto_1
.end method
