.class public final LX/FJE;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FJ6;

.field public final synthetic b:Landroid/net/Uri;

.field public final synthetic c:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/FJ6;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2223319
    iput-object p1, p0, LX/FJE;->c:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    iput-object p2, p0, LX/FJE;->a:LX/FJ6;

    iput-object p3, p0, LX/FJE;->b:Landroid/net/Uri;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2223320
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 2223321
    if-eqz v0, :cond_1

    .line 2223322
    :try_start_0
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ln;

    .line 2223323
    iget-object v2, p0, LX/FJE;->a:LX/FJ6;

    iget-object v3, p0, LX/FJE;->b:Landroid/net/Uri;

    invoke-virtual {v1}, LX/1ln;->g()I

    move-result v4

    invoke-virtual {v1}, LX/1ln;->h()I

    move-result v1

    .line 2223324
    iget-object p0, v2, LX/FJ6;->d:Landroid/net/Uri;

    invoke-static {p0, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2223325
    new-instance p0, Landroid/graphics/PointF;

    int-to-float p1, v4

    int-to-float v3, v1

    invoke-direct {p0, p1, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object p0, v2, LX/FJ6;->e:Landroid/graphics/PointF;

    .line 2223326
    invoke-static {v2}, LX/FJ6;->d(LX/FJ6;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2223327
    :cond_0
    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 2223328
    :cond_1
    return-void

    .line 2223329
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1FJ;->close()V

    throw v1
.end method

.method public final f(LX/1ca;)V
    .locals 3

    .prologue
    .line 2223330
    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v0

    .line 2223331
    iget-object v1, p0, LX/FJE;->c:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    iget-object v1, v1, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->d:LX/FJF;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FJE;->b:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 2223332
    iget-object v1, p0, LX/FJE;->c:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    iget-object v1, v1, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->d:LX/FJF;

    iget-object v2, p0, LX/FJE;->b:Landroid/net/Uri;

    invoke-interface {v1, v2, v0}, LX/FJF;->a(Landroid/net/Uri;Ljava/lang/Throwable;)V

    .line 2223333
    :cond_0
    return-void
.end method
