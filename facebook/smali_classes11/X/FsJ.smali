.class public final LX/FsJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "LX/FsM;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "LX/FsL;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "LX/G1P;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLInterfaces$TimelineTaggedMediaSetFields;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "LX/G1P;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "LX/FsK;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:LX/0zX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zX",
            "<",
            "LX/3Fw;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<",
            "LX/FsM;",
            ">;",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;",
            "LX/0zX",
            "<",
            "LX/FsL;",
            ">;",
            "LX/0zX",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLInterfaces$TimelineTaggedMediaSetFields;",
            ">;",
            "LX/0zX",
            "<",
            "LX/G1P;",
            ">;",
            "LX/0zX",
            "<",
            "LX/G1P;",
            ">;",
            "LX/0zX",
            "<",
            "LX/FsK;",
            ">;",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;",
            ">;",
            "LX/0zX",
            "<",
            "LX/3Fw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2296764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296765
    iput-object p1, p0, LX/FsJ;->a:LX/0zX;

    .line 2296766
    iput-object p2, p0, LX/FsJ;->b:LX/0zX;

    .line 2296767
    iput-object p3, p0, LX/FsJ;->c:LX/0zX;

    .line 2296768
    iput-object p4, p0, LX/FsJ;->e:LX/0zX;

    .line 2296769
    iput-object p5, p0, LX/FsJ;->d:LX/0zX;

    .line 2296770
    iput-object p6, p0, LX/FsJ;->f:LX/0zX;

    .line 2296771
    iput-object p7, p0, LX/FsJ;->g:LX/0zX;

    .line 2296772
    iput-object p8, p0, LX/FsJ;->h:LX/0zX;

    .line 2296773
    iput-object p9, p0, LX/FsJ;->i:LX/0zX;

    .line 2296774
    return-void
.end method
