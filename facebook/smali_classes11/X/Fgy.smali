.class public final LX/Fgy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7HK;


# instance fields
.field public final synthetic a:Lcom/facebook/search/suggestions/SuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2271235
    iput-object p1, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2271236
    iget-object v0, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->j()LX/FgU;

    move-result-object v0

    invoke-virtual {v0}, LX/7HQ;->e()V

    .line 2271237
    iget-object v0, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->k()LX/Fi7;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-static {v0}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2271238
    :cond_0
    :goto_0
    return-void

    .line 2271239
    :cond_1
    iget-object v0, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271240
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2271241
    const/4 v1, 0x0

    .line 2271242
    iput-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2271243
    iget-object v0, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->k()LX/Fi7;

    move-result-object v0

    iget-object v1, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Fi7;->a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;

    move-result-object v0

    iget-object v1, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->e:LX/Fh8;

    invoke-virtual {v0, v1}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwg;)V

    .line 2271244
    iget-object v0, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271245
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2271246
    iget-object v1, p0, LX/Fgy;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->g:LX/F5P;

    .line 2271247
    iput-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->o:LX/F5P;

    .line 2271248
    goto :goto_0
.end method
