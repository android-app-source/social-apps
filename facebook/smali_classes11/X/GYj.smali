.class public final enum LX/GYj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GYj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GYj;

.field public static final enum DELETE_PRODUCT_ITEM:LX/GYj;

.field public static final enum LOAD_PRODUCT_ITEM:LX/GYj;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2365292
    new-instance v0, LX/GYj;

    const-string v1, "LOAD_PRODUCT_ITEM"

    invoke-direct {v0, v1, v2}, LX/GYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYj;->LOAD_PRODUCT_ITEM:LX/GYj;

    .line 2365293
    new-instance v0, LX/GYj;

    const-string v1, "DELETE_PRODUCT_ITEM"

    invoke-direct {v0, v1, v3}, LX/GYj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYj;->DELETE_PRODUCT_ITEM:LX/GYj;

    .line 2365294
    const/4 v0, 0x2

    new-array v0, v0, [LX/GYj;

    sget-object v1, LX/GYj;->LOAD_PRODUCT_ITEM:LX/GYj;

    aput-object v1, v0, v2

    sget-object v1, LX/GYj;->DELETE_PRODUCT_ITEM:LX/GYj;

    aput-object v1, v0, v3

    sput-object v0, LX/GYj;->$VALUES:[LX/GYj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2365295
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GYj;
    .locals 1

    .prologue
    .line 2365296
    const-class v0, LX/GYj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GYj;

    return-object v0
.end method

.method public static values()[LX/GYj;
    .locals 1

    .prologue
    .line 2365297
    sget-object v0, LX/GYj;->$VALUES:[LX/GYj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GYj;

    return-object v0
.end method
