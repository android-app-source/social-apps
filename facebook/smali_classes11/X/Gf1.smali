.class public final LX/Gf1;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Gf3;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Gf2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Gf3",
            "<TE;>.PageYouMay",
            "LikeComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Gf3;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Gf3;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 2376628
    iput-object p1, p0, LX/Gf1;->b:LX/Gf3;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2376629
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "asyncPrefetchProps"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "item"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "unitProps"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "pageWidth"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "goToNextController"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Gf1;->c:[Ljava/lang/String;

    .line 2376630
    iput v3, p0, LX/Gf1;->d:I

    .line 2376631
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Gf1;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Gf1;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Gf1;LX/1De;IILX/Gf2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/Gf3",
            "<TE;>.PageYouMay",
            "LikeComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2376624
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2376625
    iput-object p4, p0, LX/Gf1;->a:LX/Gf2;

    .line 2376626
    iget-object v0, p0, LX/Gf1;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2376627
    return-void
.end method


# virtual methods
.method public final a(LX/1Pp;)LX/Gf1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/Gf3",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376621
    iget-object v0, p0, LX/Gf1;->a:LX/Gf2;

    iput-object p1, v0, LX/Gf2;->a:LX/1Pp;

    .line 2376622
    iget-object v0, p0, LX/Gf1;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376623
    return-object p0
.end method

.method public final a(LX/1f9;)LX/Gf1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1f9;",
            ")",
            "LX/Gf3",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376618
    iget-object v0, p0, LX/Gf1;->a:LX/Gf2;

    iput-object p1, v0, LX/Gf2;->b:LX/1f9;

    .line 2376619
    iget-object v0, p0, LX/Gf1;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376620
    return-object p0
.end method

.method public final a(LX/25E;)LX/Gf1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/25E;",
            ")",
            "LX/Gf3",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376615
    iget-object v0, p0, LX/Gf1;->a:LX/Gf2;

    iput-object p1, v0, LX/Gf2;->c:LX/25E;

    .line 2376616
    iget-object v0, p0, LX/Gf1;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376617
    return-object p0
.end method

.method public final a(LX/2dx;)LX/Gf1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2dx;",
            ")",
            "LX/Gf3",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376612
    iget-object v0, p0, LX/Gf1;->a:LX/Gf2;

    iput-object p1, v0, LX/Gf2;->f:LX/2dx;

    .line 2376613
    iget-object v0, p0, LX/Gf1;->e:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376614
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Gf1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;)",
            "LX/Gf3",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376590
    iget-object v0, p0, LX/Gf1;->a:LX/Gf2;

    iput-object p1, v0, LX/Gf2;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2376591
    iget-object v0, p0, LX/Gf1;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376592
    return-object p0
.end method

.method public final a(Z)LX/Gf1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/Gf3",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376610
    iget-object v0, p0, LX/Gf1;->a:LX/Gf2;

    iput-boolean p1, v0, LX/Gf2;->g:Z

    .line 2376611
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2376606
    invoke-super {p0}, LX/1X5;->a()V

    .line 2376607
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gf1;->a:LX/Gf2;

    .line 2376608
    iget-object v0, p0, LX/Gf1;->b:LX/Gf3;

    iget-object v0, v0, LX/Gf3;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2376609
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Gf3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2376596
    iget-object v1, p0, LX/Gf1;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Gf1;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Gf1;->d:I

    if-ge v1, v2, :cond_2

    .line 2376597
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2376598
    :goto_0
    iget v2, p0, LX/Gf1;->d:I

    if-ge v0, v2, :cond_1

    .line 2376599
    iget-object v2, p0, LX/Gf1;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2376600
    iget-object v2, p0, LX/Gf1;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2376601
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2376602
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2376603
    :cond_2
    iget-object v0, p0, LX/Gf1;->a:LX/Gf2;

    .line 2376604
    invoke-virtual {p0}, LX/Gf1;->a()V

    .line 2376605
    return-object v0
.end method

.method public final h(I)LX/Gf1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/Gf3",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376593
    iget-object v0, p0, LX/Gf1;->a:LX/Gf2;

    iput p1, v0, LX/Gf2;->e:I

    .line 2376594
    iget-object v0, p0, LX/Gf1;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376595
    return-object p0
.end method
