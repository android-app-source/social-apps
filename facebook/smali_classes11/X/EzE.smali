.class public final LX/EzE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5P5;


# instance fields
.field public final synthetic a:Lcom/facebook/friends/model/PersonYouMayKnow;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:LX/2iS;


# direct methods
.method public constructor <init>(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 2186953
    iput-object p1, p0, LX/EzE;->d:LX/2iS;

    iput-object p2, p0, LX/EzE;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iput-object p3, p0, LX/EzE;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p4, p0, LX/EzE;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2186954
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2186955
    iget-object v0, p0, LX/EzE;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iget-object v1, p0, LX/EzE;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2186956
    iput-object v1, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2186957
    iget-object v0, p0, LX/EzE;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iget-object v1, p0, LX/EzE;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2186958
    iget-object v0, p0, LX/EzE;->d:LX/2iS;

    iget-object v1, p0, LX/EzE;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/2iS;->b(LX/2iS;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2186959
    iget-object v0, p0, LX/EzE;->d:LX/2iS;

    iget-object v0, v0, LX/2iS;->n:LX/2ia;

    iget-object v1, p0, LX/EzE;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/2ia;->c(J)V

    .line 2186960
    :cond_0
    return-void
.end method
