.class public final enum LX/GEQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GEQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GEQ;

.field public static final enum SHOW_CHECKOUT:LX/GEQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2331679
    new-instance v0, LX/GEQ;

    const-string v1, "SHOW_CHECKOUT"

    invoke-direct {v0, v1, v2}, LX/GEQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GEQ;->SHOW_CHECKOUT:LX/GEQ;

    .line 2331680
    const/4 v0, 0x1

    new-array v0, v0, [LX/GEQ;

    sget-object v1, LX/GEQ;->SHOW_CHECKOUT:LX/GEQ;

    aput-object v1, v0, v2

    sput-object v0, LX/GEQ;->$VALUES:[LX/GEQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2331678
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GEQ;
    .locals 1

    .prologue
    .line 2331677
    const-class v0, LX/GEQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GEQ;

    return-object v0
.end method

.method public static values()[LX/GEQ;
    .locals 1

    .prologue
    .line 2331676
    sget-object v0, LX/GEQ;->$VALUES:[LX/GEQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GEQ;

    return-object v0
.end method
