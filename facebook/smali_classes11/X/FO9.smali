.class public final enum LX/FO9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FO9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FO9;

.field public static final enum ANIMATED_IMAGE:LX/FO9;

.field public static final enum REGULAR_IMAGE:LX/FO9;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2234360
    new-instance v0, LX/FO9;

    const-string v1, "ANIMATED_IMAGE"

    invoke-direct {v0, v1, v2}, LX/FO9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FO9;->ANIMATED_IMAGE:LX/FO9;

    .line 2234361
    new-instance v0, LX/FO9;

    const-string v1, "REGULAR_IMAGE"

    invoke-direct {v0, v1, v3}, LX/FO9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FO9;->REGULAR_IMAGE:LX/FO9;

    .line 2234362
    const/4 v0, 0x2

    new-array v0, v0, [LX/FO9;

    sget-object v1, LX/FO9;->ANIMATED_IMAGE:LX/FO9;

    aput-object v1, v0, v2

    sget-object v1, LX/FO9;->REGULAR_IMAGE:LX/FO9;

    aput-object v1, v0, v3

    sput-object v0, LX/FO9;->$VALUES:[LX/FO9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2234363
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FO9;
    .locals 1

    .prologue
    .line 2234364
    const-class v0, LX/FO9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FO9;

    return-object v0
.end method

.method public static values()[LX/FO9;
    .locals 1

    .prologue
    .line 2234365
    sget-object v0, LX/FO9;->$VALUES:[LX/FO9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FO9;

    return-object v0
.end method
