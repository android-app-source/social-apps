.class public final enum LX/GR6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GR6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GR6;

.field public static final enum APP_INDEXING_EVENT_TYPE:LX/GR6;


# instance fields
.field private final mEventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2351313
    new-instance v0, LX/GR6;

    const-string v1, "APP_INDEXING_EVENT_TYPE"

    const-string v2, "app_indexing_referrer"

    invoke-direct {v0, v1, v3, v2}, LX/GR6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GR6;->APP_INDEXING_EVENT_TYPE:LX/GR6;

    .line 2351314
    const/4 v0, 0x1

    new-array v0, v0, [LX/GR6;

    sget-object v1, LX/GR6;->APP_INDEXING_EVENT_TYPE:LX/GR6;

    aput-object v1, v0, v3

    sput-object v0, LX/GR6;->$VALUES:[LX/GR6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2351318
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2351319
    iput-object p3, p0, LX/GR6;->mEventName:Ljava/lang/String;

    .line 2351320
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GR6;
    .locals 1

    .prologue
    .line 2351317
    const-class v0, LX/GR6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GR6;

    return-object v0
.end method

.method public static values()[LX/GR6;
    .locals 1

    .prologue
    .line 2351316
    sget-object v0, LX/GR6;->$VALUES:[LX/GR6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GR6;

    return-object v0
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2351315
    iget-object v0, p0, LX/GR6;->mEventName:Ljava/lang/String;

    return-object v0
.end method
