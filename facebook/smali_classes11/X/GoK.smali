.class public LX/GoK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2394465
    const/4 v0, 0x0

    sput-boolean v0, LX/GoK;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2394463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2394464
    return-void
.end method

.method public static a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;I)LX/Clr;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2394276
    sget-object v0, LX/GoI;->a:[I

    invoke-interface {p0}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v0, v3

    .line 2394277
    :goto_0
    return-object v0

    .line 2394278
    :pswitch_0
    new-instance v0, LX/GpI;

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v1

    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/GoK;->b(LX/0Px;)Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/GpI;-><init>(LX/CHZ;IZ)V

    goto :goto_0

    .line 2394279
    :pswitch_1
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    .line 2394280
    if-nez v0, :cond_10

    .line 2394281
    const/4 v1, 0x0

    .line 2394282
    :goto_1
    move v0, v1

    .line 2394283
    if-eqz v0, :cond_1

    .line 2394284
    invoke-interface {p0}, LX/CHb;->e()I

    move-result v0

    .line 2394285
    new-instance v1, LX/Gov;

    const/16 v2, 0x79

    invoke-direct {v1, p0, v0, v2}, LX/Gov;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V

    .line 2394286
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-virtual {v1, v2}, LX/Gos;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Gos;

    .line 2394287
    iput p1, v1, LX/Gov;->b:I

    .line 2394288
    invoke-virtual {v1}, LX/Gov;->d()LX/Gow;

    move-result-object v1

    move-object v0, v1

    .line 2394289
    goto :goto_0

    .line 2394290
    :cond_1
    invoke-interface {p0}, LX/CHb;->e()I

    move-result v0

    .line 2394291
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v1

    .line 2394292
    if-nez v1, :cond_13

    .line 2394293
    const/4 v2, 0x0

    .line 2394294
    :goto_2
    move v2, v2

    .line 2394295
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/Gp7;->f(LX/0Px;)Z

    move-result v1

    .line 2394296
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_11

    if-nez v1, :cond_2

    if-eqz v2, :cond_11

    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394297
    :goto_3
    new-instance v3, LX/Gp8;

    const/16 v4, 0x72

    invoke-direct {v3, p0, v0, v4}, LX/Gp8;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V

    .line 2394298
    invoke-virtual {v3, v1}, LX/Gos;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Gos;

    .line 2394299
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2394300
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/Gp7;->h(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_12

    const/4 v1, 0x1

    .line 2394301
    :goto_4
    iput-boolean v1, v3, LX/Gos;->i:Z

    .line 2394302
    move-object v1, v3

    .line 2394303
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/Gp7;->d(LX/0Px;)Z

    move-result v4

    .line 2394304
    iput-boolean v4, v1, LX/Gos;->l:Z

    .line 2394305
    move-object v1, v1

    .line 2394306
    iput-boolean v2, v1, LX/Gos;->p:Z

    .line 2394307
    move-object v1, v1

    .line 2394308
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->c(LX/0Px;)Z

    move-result v2

    .line 2394309
    iput-boolean v2, v1, LX/Gos;->j:Z

    .line 2394310
    move-object v1, v1

    .line 2394311
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->a(LX/0Px;)Z

    move-result v2

    .line 2394312
    iput-boolean v2, v1, LX/Gos;->m:Z

    .line 2394313
    move-object v1, v1

    .line 2394314
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->m()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->b(LX/0Px;)Z

    move-result v2

    .line 2394315
    iput-boolean v2, v1, LX/Gos;->n:Z

    .line 2394316
    :cond_3
    invoke-virtual {v3}, LX/Gp8;->d()LX/Goo;

    move-result-object v1

    move-object v0, v1

    .line 2394317
    goto/16 :goto_0

    .line 2394318
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->N()LX/8Ys;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v3

    .line 2394319
    goto/16 :goto_0

    .line 2394320
    :cond_4
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SCRUBBABLE_GIF:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 2394321
    new-instance v1, LX/GpC;

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v0

    const/16 v2, 0x75

    invoke-direct {v1, p0, v0, v2}, LX/GpC;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V

    .line 2394322
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2394323
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Gp7;->f(LX/0Px;)Z

    move-result v0

    .line 2394324
    if-eqz v0, :cond_5

    .line 2394325
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-virtual {v1, v0}, LX/Gos;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Gos;

    .line 2394326
    :cond_5
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Gp7;->i(LX/0Px;)Z

    move-result v0

    .line 2394327
    iput-boolean v0, v1, LX/Gos;->k:Z

    .line 2394328
    move-object v2, v1

    .line 2394329
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Gp7;->h(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_17

    const/4 v0, 0x1

    .line 2394330
    :goto_5
    iput-boolean v0, v2, LX/Gos;->i:Z

    .line 2394331
    move-object v0, v2

    .line 2394332
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->c(LX/0Px;)Z

    move-result v2

    .line 2394333
    iput-boolean v2, v0, LX/Gos;->j:Z

    .line 2394334
    move-object v0, v0

    .line 2394335
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->d(LX/0Px;)Z

    move-result v2

    .line 2394336
    iput-boolean v2, v0, LX/Gos;->l:Z

    .line 2394337
    :cond_6
    invoke-virtual {v1}, LX/GpC;->d()LX/GpD;

    move-result-object v0

    move-object v0, v0

    .line 2394338
    :goto_6
    move-object v0, v0

    .line 2394339
    goto/16 :goto_0

    .line 2394340
    :pswitch_3
    new-instance v0, LX/Gou;

    invoke-direct {v0, p0}, LX/Gou;-><init>(LX/CHc;)V

    goto/16 :goto_0

    .line 2394341
    :pswitch_4
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->D()LX/0Px;

    move-result-object v0

    sget v1, LX/CIY;->a:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2394342
    invoke-static {v0}, LX/GoK;->a(LX/0Px;)LX/GoH;

    move-result-object v2

    .line 2394343
    new-instance v5, LX/GpE;

    const/16 v6, 0x74

    invoke-direct {v5, v2, p0, v1, v6}, LX/GpE;-><init>(LX/Clo;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V

    .line 2394344
    iput p1, v5, LX/GpE;->b:I

    .line 2394345
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 2394346
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->f(LX/0Px;)Z

    move-result v2

    .line 2394347
    if-eqz v2, :cond_7

    .line 2394348
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-virtual {v5, v2}, LX/Gos;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Gos;

    .line 2394349
    :cond_7
    invoke-virtual {v5}, LX/Gos;->c()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v2

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    if-eq v2, v6, :cond_8

    .line 2394350
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    .line 2394351
    if-nez v2, :cond_1c

    .line 2394352
    const/4 v6, 0x0

    .line 2394353
    :goto_7
    move v2, v6

    .line 2394354
    if-nez v2, :cond_1a

    move v2, v3

    .line 2394355
    :goto_8
    iput-boolean v2, v5, LX/Gos;->q:Z

    .line 2394356
    :cond_8
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    .line 2394357
    if-nez v2, :cond_1d

    .line 2394358
    const/4 v6, 0x0

    .line 2394359
    :goto_9
    move v2, v6

    .line 2394360
    iput-boolean v2, v5, LX/Gos;->j:Z

    .line 2394361
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    .line 2394362
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->DARK_GRADIENT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    move v2, v6

    .line 2394363
    iput-boolean v2, v5, LX/GpE;->c:Z

    .line 2394364
    move-object v2, v5

    .line 2394365
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v6

    invoke-static {v6}, LX/Gp7;->h(LX/0Px;)Z

    move-result v6

    if-nez v6, :cond_1b

    .line 2394366
    :goto_a
    iput-boolean v3, v2, LX/Gos;->i:Z

    .line 2394367
    move-object v2, v2

    .line 2394368
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/Gp7;->d(LX/0Px;)Z

    move-result v3

    .line 2394369
    iput-boolean v3, v2, LX/Gos;->l:Z

    .line 2394370
    :cond_9
    invoke-virtual {v5}, LX/GpE;->d()LX/GpF;

    move-result-object v2

    move-object v0, v2

    .line 2394371
    goto/16 :goto_0

    .line 2394372
    :pswitch_5
    invoke-interface {p0}, LX/CHb;->e()I

    .line 2394373
    new-instance v0, LX/GpA;

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v1

    const/16 v2, 0x76

    invoke-direct {v0, p0, v1, v2}, LX/GpA;-><init>(LX/CHs;II)V

    .line 2394374
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394375
    iput-object v1, v0, LX/GpA;->a:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394376
    invoke-virtual {v0}, LX/GpA;->d()LX/GpB;

    move-result-object v0

    move-object v0, v0

    .line 2394377
    goto/16 :goto_0

    .line 2394378
    :pswitch_6
    new-instance v0, LX/Gp1;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->M()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;

    move-result-object v1

    sget v2, LX/CIY;->a:I

    invoke-direct {v0, v1, v2}, LX/Gp1;-><init>(LX/CHa;I)V

    goto/16 :goto_0

    .line 2394379
    :pswitch_7
    invoke-interface {p0}, LX/CHb;->e()I

    move-result v0

    if-lez v0, :cond_a

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v0

    const/16 v1, 0x64

    if-ge v0, v1, :cond_a

    .line 2394380
    new-instance v0, LX/Gox;

    const/16 v1, 0x6f

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, LX/Gox;-><init>(LX/CHe;II)V

    goto/16 :goto_0

    .line 2394381
    :cond_a
    new-instance v0, LX/Gox;

    const/16 v1, 0x68

    sget v2, LX/CIY;->a:I

    invoke-direct {v0, p0, v1, v2}, LX/Gox;-><init>(LX/CHe;II)V

    goto/16 :goto_0

    .line 2394382
    :pswitch_8
    new-instance v0, LX/Gp2;

    sget v1, LX/CIY;->a:I

    invoke-direct {v0, p0, v1}, LX/Gp2;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;I)V

    goto/16 :goto_0

    .line 2394383
    :pswitch_9
    new-instance v0, LX/Goy;

    invoke-direct {v0, p0}, LX/Goy;-><init>(LX/CHk;)V

    goto/16 :goto_0

    .line 2394384
    :pswitch_a
    new-instance v0, LX/GpP;

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v1

    invoke-direct {v0, p0, v1}, LX/GpP;-><init>(LX/CHm;I)V

    goto/16 :goto_0

    .line 2394385
    :pswitch_b
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->H()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_b

    .line 2394386
    new-instance v0, LX/Goz;

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v1

    const/16 v2, 0x7a

    invoke-direct {v0, p0, v1, v2}, LX/Goz;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->Q()LX/8Z4;

    move-result-object v1

    invoke-interface {v1}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v1

    .line 2394387
    iput-object v1, v0, LX/Goz;->a:Ljava/lang/String;

    .line 2394388
    move-object v0, v0

    .line 2394389
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->H()LX/0Px;

    move-result-object v1

    .line 2394390
    iput-object v1, v0, LX/Goz;->d:Ljava/util/List;

    .line 2394391
    move-object v0, v0

    .line 2394392
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->J()LX/8Z4;

    move-result-object v1

    invoke-interface {v1}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v1

    .line 2394393
    iput-object v1, v0, LX/Goz;->b:Ljava/lang/String;

    .line 2394394
    move-object v0, v0

    .line 2394395
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->C()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ActionModel;

    move-result-object v1

    .line 2394396
    iput-object v1, v0, LX/Goz;->c:LX/CHX;

    .line 2394397
    move-object v0, v0

    .line 2394398
    invoke-virtual {v0}, LX/Goz;->c()LX/Gp0;

    move-result-object v0

    move-object v0, v0

    .line 2394399
    goto/16 :goto_0

    :cond_b
    move-object v0, v3

    .line 2394400
    goto/16 :goto_0

    .line 2394401
    :pswitch_c
    if-eqz p0, :cond_d

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->Q()LX/8Z4;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 2394402
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->I()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2394403
    if-eqz v0, :cond_c

    move v0, v1

    :goto_b
    if-eqz v0, :cond_f

    .line 2394404
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->E()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2394405
    if-eqz v0, :cond_e

    :goto_c
    if-eqz v1, :cond_0

    .line 2394406
    const/4 p1, 0x1

    const/4 v10, 0x0

    .line 2394407
    new-instance v0, LX/GpJ;

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v1

    const/16 v2, 0x78

    invoke-direct {v0, p0, v1, v2}, LX/GpJ;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V

    .line 2394408
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->I()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2394409
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->I()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2394410
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->E()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2394411
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->E()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 2394412
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->Q()LX/8Z4;

    move-result-object v9

    invoke-interface {v9}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v9

    .line 2394413
    iput-object v9, v0, LX/GpJ;->a:Ljava/lang/String;

    .line 2394414
    move-object v9, v0

    .line 2394415
    invoke-virtual {v2, v1, v10}, LX/15i;->j(II)I

    move-result v1

    .line 2394416
    iput v1, v9, LX/GpJ;->b:I

    .line 2394417
    move-object v1, v9

    .line 2394418
    invoke-virtual {v4, v3, p1}, LX/15i;->j(II)I

    move-result v2

    .line 2394419
    iput v2, v1, LX/GpJ;->c:I

    .line 2394420
    move-object v1, v1

    .line 2394421
    invoke-virtual {v6, v5, v10}, LX/15i;->j(II)I

    move-result v2

    .line 2394422
    iput v2, v1, LX/GpJ;->d:I

    .line 2394423
    move-object v1, v1

    .line 2394424
    invoke-virtual {v8, v7, p1}, LX/15i;->j(II)I

    move-result v2

    .line 2394425
    iput v2, v1, LX/GpJ;->e:I

    .line 2394426
    invoke-virtual {v0}, LX/GpJ;->c()LX/GpK;

    move-result-object v0

    move-object v0, v0

    .line 2394427
    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 2394428
    goto :goto_b

    :cond_d
    move v0, v2

    goto :goto_b

    :cond_e
    move v1, v2

    goto :goto_c

    :cond_f
    move v1, v2

    goto :goto_c

    :cond_10
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->CENTER_ALIGNED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto/16 :goto_1

    .line 2394429
    :cond_11
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->ASPECT_FIT:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    goto/16 :goto_3

    .line 2394430
    :cond_12
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_13
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->TILT_TO_PAN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/16 :goto_2

    .line 2394431
    :cond_14
    const/4 v0, 0x1

    sput-boolean v0, LX/GoK;->a:Z

    .line 2394432
    new-instance v1, LX/GpL;

    invoke-interface {p0}, LX/CHb;->e()I

    move-result v0

    const/16 v2, 0x73

    invoke-direct {v1, p0, v0, v2}, LX/GpL;-><init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;II)V

    .line 2394433
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 2394434
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Gp7;->f(LX/0Px;)Z

    move-result v0

    .line 2394435
    if-eqz v0, :cond_15

    .line 2394436
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-virtual {v1, v0}, LX/Gos;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Gos;

    .line 2394437
    :cond_15
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    .line 2394438
    if-nez v0, :cond_19

    .line 2394439
    const/4 v2, 0x0

    .line 2394440
    :goto_d
    move v0, v2

    .line 2394441
    iput-boolean v0, v1, LX/GpL;->b:Z

    .line 2394442
    move-object v0, v1

    .line 2394443
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->i(LX/0Px;)Z

    move-result v2

    .line 2394444
    iput-boolean v2, v0, LX/Gos;->k:Z

    .line 2394445
    move-object v2, v0

    .line 2394446
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/Gp7;->h(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    .line 2394447
    :goto_e
    iput-boolean v0, v2, LX/Gos;->i:Z

    .line 2394448
    move-object v0, v2

    .line 2394449
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->c(LX/0Px;)Z

    move-result v2

    .line 2394450
    iput-boolean v2, v0, LX/Gos;->j:Z

    .line 2394451
    move-object v0, v0

    .line 2394452
    invoke-interface {p0}, LX/CHb;->jw_()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->d(LX/0Px;)Z

    move-result v2

    .line 2394453
    iput-boolean v2, v0, LX/Gos;->l:Z

    .line 2394454
    move-object v0, v0

    .line 2394455
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->m()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Gp7;->b(LX/0Px;)Z

    move-result v2

    .line 2394456
    iput-boolean v2, v0, LX/Gos;->n:Z

    .line 2394457
    :cond_16
    invoke-virtual {v1}, LX/GpL;->d()LX/GpM;

    move-result-object v0

    move-object v0, v0

    .line 2394458
    goto/16 :goto_6

    .line 2394459
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 2394460
    :cond_18
    const/4 v0, 0x0

    goto :goto_e

    :cond_19
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->VIDEO_PLAY_PAUSE_DISABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_d

    :cond_1a
    move v2, v4

    .line 2394461
    goto/16 :goto_8

    :cond_1b
    move v3, v4

    .line 2394462
    goto/16 :goto_a

    :cond_1c
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->NON_ADJUSTED_FIT_TO_WIDTH:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    goto/16 :goto_7

    :cond_1d
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SHOW_INTERACTION_HINT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    goto/16 :goto_9

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static a(LX/0Px;)LX/GoH;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CHi;",
            ">;)",
            "LX/GoH;"
        }
    .end annotation

    .prologue
    .line 2394243
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2394244
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_7

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CHi;

    .line 2394245
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2394246
    const/16 v5, 0x6b

    .line 2394247
    invoke-interface {v0}, LX/CHg;->jw_()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-interface {v0}, LX/CHg;->jw_()LX/0Px;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v4, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move v6, v7

    .line 2394248
    :goto_1
    const/4 v4, 0x0

    .line 2394249
    if-eqz v6, :cond_0

    .line 2394250
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->FULL_SCREEN:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2394251
    const/16 v5, 0x6c

    .line 2394252
    :cond_0
    invoke-interface {v0}, LX/CHg;->jw_()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-interface {v0}, LX/CHg;->jw_()LX/0Px;

    move-result-object v6

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {v6, v9}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    move v6, v7

    .line 2394253
    :goto_2
    invoke-interface {v0}, LX/CHi;->m()LX/0Px;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-interface {v0}, LX/CHi;->m()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    invoke-interface {v0}, LX/CHi;->m()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_2

    .line 2394254
    :cond_1
    const/16 v5, 0x6e

    .line 2394255
    :cond_2
    new-instance v9, LX/Gp8;

    sget v10, LX/CIY;->a:I

    invoke-direct {v9, v0, v10, v5}, LX/Gp8;-><init>(LX/CHi;II)V

    .line 2394256
    iput-boolean v7, v9, LX/Gos;->o:Z

    .line 2394257
    if-eqz v4, :cond_3

    .line 2394258
    invoke-virtual {v9, v4}, LX/Gos;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Gos;

    .line 2394259
    :cond_3
    if-eqz v6, :cond_a

    .line 2394260
    iput-boolean v7, v9, LX/Gos;->l:Z

    .line 2394261
    :goto_3
    invoke-interface {v0}, LX/CHg;->jw_()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/Gp7;->a(LX/0Px;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2394262
    iput-boolean v7, v9, LX/Gos;->m:Z

    .line 2394263
    :cond_4
    invoke-interface {v0}, LX/CHi;->m()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/Gp7;->b(LX/0Px;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2394264
    iput-boolean v7, v9, LX/Gos;->n:Z

    .line 2394265
    :cond_5
    invoke-virtual {v9}, LX/Gp8;->d()LX/Goo;

    move-result-object v4

    move-object v0, v4

    .line 2394266
    if-eqz v0, :cond_6

    .line 2394267
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2394268
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2394269
    :cond_7
    new-instance v0, LX/GoH;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/GoH;-><init>(Ljava/lang/String;)V

    .line 2394270
    invoke-virtual {v0, v2}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2394271
    return-object v0

    :cond_8
    move v6, v8

    .line 2394272
    goto :goto_1

    :cond_9
    move v6, v8

    .line 2394273
    goto :goto_2

    .line 2394274
    :cond_a
    iput-boolean v8, v9, LX/Gos;->l:Z

    .line 2394275
    goto :goto_3
.end method

.method public static a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;)LX/GoH;
    .locals 7

    .prologue
    .line 2394224
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->b()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v1

    .line 2394225
    const/4 v0, -0x1

    .line 2394226
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2394227
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 2394228
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 2394229
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2394230
    sput-boolean v2, LX/GoK;->a:Z

    .line 2394231
    if-eqz v1, :cond_2

    .line 2394232
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;

    .line 2394233
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 2394234
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v2

    invoke-static {v2, v0}, LX/GoK;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;I)LX/Clr;

    move-result-object v2

    .line 2394235
    if-eqz v2, :cond_1

    .line 2394236
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2394237
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2394238
    :cond_2
    move-object v0, v4

    .line 2394239
    new-instance v1, LX/GoH;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/GoH;-><init>(Ljava/lang/String;)V

    .line 2394240
    invoke-virtual {v1, v0}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2394241
    return-object v1

    .line 2394242
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a()LX/Gp5;
    .locals 2

    .prologue
    .line 2394223
    new-instance v0, LX/Gp5;

    sget v1, LX/CIY;->a:I

    invoke-direct {v0, v1}, LX/Gp5;-><init>(I)V

    return-object v0
.end method

.method public static a(LX/Clr;)LX/GpO;
    .locals 3

    .prologue
    .line 2394213
    new-instance v1, LX/GpN;

    invoke-direct {v1}, LX/GpN;-><init>()V

    .line 2394214
    instance-of v0, p0, LX/GoL;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 2394215
    check-cast v0, LX/GoL;

    invoke-interface {v0}, LX/GoL;->getAction()LX/CHX;

    move-result-object v0

    .line 2394216
    invoke-static {v0}, LX/Gn5;->a(LX/CHX;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2394217
    iput-object v0, v1, LX/GpN;->b:LX/CHX;

    .line 2394218
    :cond_0
    instance-of v0, p0, LX/GoY;

    if-eqz v0, :cond_1

    .line 2394219
    check-cast p0, LX/GoY;

    invoke-interface {p0}, LX/GoY;->D()LX/GoE;

    move-result-object v0

    .line 2394220
    iput-object v0, v1, LX/GpN;->a:LX/GoE;

    .line 2394221
    :cond_1
    invoke-virtual {v1}, LX/GpN;->a()LX/GpO;

    move-result-object v0

    .line 2394222
    invoke-interface {v0}, LX/GoL;->getAction()LX/CHX;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    return-object v0
.end method

.method public static a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;
    .locals 7

    .prologue
    .line 2394207
    const/4 v1, 0x0

    .line 2394208
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;

    .line 2394209
    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v5

    invoke-interface {v5}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->FOOTER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    if-ne v5, v6, :cond_0

    .line 2394210
    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v0

    .line 2394211
    :goto_1
    return-object v0

    .line 2394212
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static b()LX/Gp6;
    .locals 2

    .prologue
    .line 2394203
    new-instance v0, LX/Gp6;

    sget v1, LX/CIY;->a:I

    invoke-direct {v0, v1}, LX/Gp6;-><init>(I)V

    return-object v0
.end method

.method public static b(LX/0Px;)Z
    .locals 1
    .param p0    # LX/0Px;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2394204
    if-nez p0, :cond_0

    .line 2394205
    const/4 v0, 0x0

    .line 2394206
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->TEXT_NO_CUSTOM_MEASURE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p0, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
