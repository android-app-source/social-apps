.class public LX/FaJ;
.super LX/1Ur;
.source ""


# static fields
.field public static final i:LX/1Up;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2258617
    new-instance v0, LX/FaJ;

    invoke-direct {v0}, LX/FaJ;-><init>()V

    sput-object v0, LX/FaJ;->i:LX/1Up;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2258618
    invoke-direct {p0}, LX/1Ur;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFFFF)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 2258619
    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, p3

    mul-float/2addr v2, p7

    sub-float/2addr v1, v2

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    .line 2258620
    iget v1, p2, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    .line 2258621
    invoke-virtual {p1, p7, p7}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 2258622
    float-to-int v0, v0

    int-to-float v0, v0

    add-float/2addr v0, v3

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2258623
    return-void
.end method
