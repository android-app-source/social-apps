.class public final LX/F35;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V
    .locals 0

    .prologue
    .line 2193642
    iput-object p1, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2193643
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2193644
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2193645
    iget-object v1, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    .line 2193646
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2193647
    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    .line 2193648
    iput-object v0, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    .line 2193649
    iget-object v0, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->s()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->m()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2193650
    :cond_0
    iget-object v0, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v1, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v2, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2193651
    :cond_1
    :goto_0
    return-void

    .line 2193652
    :cond_2
    iget-object v0, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    if-eqz v0, :cond_1

    .line 2193653
    iget-object v0, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v1, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->l()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/F35;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    iget-object v2, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
