.class public final LX/FCj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# instance fields
.field public final synthetic a:LX/FCl;


# direct methods
.method public constructor <init>(LX/FCl;)V
    .locals 0

    .prologue
    .line 2210531
    iput-object p1, p0, LX/FCj;->a:LX/FCl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAudioFocusChange(I)V
    .locals 7

    .prologue
    .line 2210532
    packed-switch p1, :pswitch_data_0

    .line 2210533
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2210534
    :pswitch_1
    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-object v0, v0, LX/FCl;->g:LX/FCh;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-object v0, v0, LX/FCl;->g:LX/FCh;

    invoke-virtual {v0}, LX/FCh;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2210535
    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    const/4 v1, 0x1

    .line 2210536
    iput-boolean v1, v0, LX/FCl;->h:Z

    .line 2210537
    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-object v0, v0, LX/FCl;->g:LX/FCh;

    .line 2210538
    :try_start_0
    iget-object v1, v0, LX/FCh;->j:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2210539
    iget-object v1, v0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V

    .line 2210540
    sget-object v1, LX/FCf;->PLAYBACK_PAUSED:LX/FCf;

    invoke-static {v0, v1}, LX/FCh;->a$redex0(LX/FCh;LX/FCf;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2210541
    :cond_1
    :goto_1
    iget-object v1, v0, LX/FCh;->f:Landroid/os/Handler;

    iget-object p0, v0, LX/FCh;->l:Ljava/lang/Runnable;

    invoke-static {v1, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2210542
    goto :goto_0

    .line 2210543
    :pswitch_2
    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-object v0, v0, LX/FCl;->g:LX/FCh;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-object v0, v0, LX/FCl;->g:LX/FCh;

    invoke-virtual {v0}, LX/FCh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-boolean v0, v0, LX/FCl;->h:Z

    if-eqz v0, :cond_0

    .line 2210544
    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    const/4 v1, 0x0

    .line 2210545
    iput-boolean v1, v0, LX/FCl;->h:Z

    .line 2210546
    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-object v0, v0, LX/FCl;->g:LX/FCh;

    .line 2210547
    iget-object v2, v0, LX/FCh;->j:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    .line 2210548
    iget-object v2, v0, LX/FCh;->e:LX/FCn;

    .line 2210549
    iget v5, v2, LX/FCn;->f:I

    iput v5, v2, LX/FCn;->e:I

    .line 2210550
    iget-object v5, v2, LX/FCn;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    iput-wide v5, v2, LX/FCn;->d:J

    .line 2210551
    sget-object v2, LX/FCf;->PLAYBACK_RESUMED:LX/FCf;

    invoke-static {v0, v2}, LX/FCh;->a$redex0(LX/FCh;LX/FCf;)V

    .line 2210552
    iget-object v2, v0, LX/FCh;->f:Landroid/os/Handler;

    iget-object v3, v0, LX/FCh;->l:Ljava/lang/Runnable;

    const v4, 0x1ca7b259

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2210553
    goto :goto_0

    .line 2210554
    :pswitch_3
    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-object v0, v0, LX/FCl;->g:LX/FCh;

    if-eqz v0, :cond_0

    .line 2210555
    iget-object v0, p0, LX/FCj;->a:LX/FCl;

    iget-object v0, v0, LX/FCl;->g:LX/FCh;

    invoke-virtual {v0}, LX/FCh;->b()V

    goto/16 :goto_0

    .line 2210556
    :catch_0
    sget-object v1, LX/FCh;->a:Ljava/lang/Class;

    const-string p0, "The player finished playing before pause() was called"

    invoke-static {v1, p0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
