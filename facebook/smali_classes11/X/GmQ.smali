.class public LX/GmQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0kr;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/GmQ;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/2c4;

.field public final c:LX/BAa;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0s5;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

.field public f:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2c4;LX/BAa;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2c4;",
            "LX/BAa;",
            "LX/0Ot",
            "<",
            "LX/0s5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2392013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2392014
    iput-object p1, p0, LX/GmQ;->a:Landroid/content/Context;

    .line 2392015
    iput-object p2, p0, LX/GmQ;->b:LX/2c4;

    .line 2392016
    iput-object p3, p0, LX/GmQ;->c:LX/BAa;

    .line 2392017
    iput-object p4, p0, LX/GmQ;->d:LX/0Ot;

    .line 2392018
    return-void
.end method

.method public static a(LX/0QB;)LX/GmQ;
    .locals 7

    .prologue
    .line 2391968
    sget-object v0, LX/GmQ;->g:LX/GmQ;

    if-nez v0, :cond_1

    .line 2391969
    const-class v1, LX/GmQ;

    monitor-enter v1

    .line 2391970
    :try_start_0
    sget-object v0, LX/GmQ;->g:LX/GmQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2391971
    if-eqz v2, :cond_0

    .line 2391972
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2391973
    new-instance v6, LX/GmQ;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v4

    check-cast v4, LX/2c4;

    invoke-static {v0}, LX/BAa;->b(LX/0QB;)LX/BAa;

    move-result-object v5

    check-cast v5, LX/BAa;

    const/16 p0, 0xb74

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/GmQ;-><init>(Landroid/content/Context;LX/2c4;LX/BAa;LX/0Ot;)V

    .line 2391974
    move-object v0, v6

    .line 2391975
    sput-object v0, LX/GmQ;->g:LX/GmQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2391976
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2391977
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2391978
    :cond_1
    sget-object v0, LX/GmQ;->g:LX/GmQ;

    return-object v0

    .line 2391979
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2391980
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2391981
    return-void
.end method

.method public final a(LX/4ce;LX/4cd;)V
    .locals 6

    .prologue
    .line 2391982
    sget-object v0, LX/4ce;->DISABLED:LX/4ce;

    if-ne p1, v0, :cond_0

    .line 2391983
    iget-object v0, p0, LX/GmQ;->b:LX/2c4;

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->TOR_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v0, v1}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;)V

    .line 2391984
    :goto_0
    return-void

    .line 2391985
    :cond_0
    iget-object v0, p0, LX/GmQ;->f:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GmQ;->e:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    if-nez v0, :cond_2

    .line 2391986
    :cond_1
    const/4 v3, 0x1

    .line 2391987
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "settings/tor"

    invoke-static {v2}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v0, p0, LX/GmQ;->f:Landroid/content/Intent;

    .line 2391988
    new-instance v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v0}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    .line 2391989
    iput-boolean v3, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    .line 2391990
    move-object v0, v0

    .line 2391991
    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->TOR_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    .line 2391992
    iput-object v1, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->b:Lcom/facebook/notifications/constants/NotificationType;

    .line 2391993
    move-object v0, v0

    .line 2391994
    iput v3, v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->d:I

    .line 2391995
    move-object v0, v0

    .line 2391996
    iput-object v0, p0, LX/GmQ;->e:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 2391997
    iget-object v0, p0, LX/GmQ;->c:LX/BAa;

    iget-object v1, p0, LX/GmQ;->a:Landroid/content/Context;

    const v2, 0x7f080084

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BAa;->d(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/BAa;->b(Z)LX/BAa;

    move-result-object v0

    .line 2391998
    iget-object v1, v0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v1, v3}, LX/2HB;->b(Z)LX/2HB;

    .line 2391999
    move-object v0, v0

    .line 2392000
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2392001
    iget-object v2, v0, LX/BAa;->k:LX/2HB;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Z)LX/2HB;

    .line 2392002
    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual {p2}, LX/4cd;->isCheckedConnection()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f020d44

    move v2, v0

    .line 2392003
    :goto_1
    if-eqz p2, :cond_5

    iget v0, p2, LX/4cd;->resId:I

    move v1, v0

    .line 2392004
    :goto_2
    sget-object v0, LX/4cd;->CHECKED_CONNECTION_TOR:LX/4cd;

    if-ne p2, v0, :cond_3

    .line 2392005
    iget-object v0, p0, LX/GmQ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0s5;

    invoke-virtual {v0}, LX/0s5;->d()Z

    move-result v0

    .line 2392006
    if-eqz v0, :cond_3

    .line 2392007
    const v1, 0x7f080082

    .line 2392008
    :cond_3
    iget-object v0, p0, LX/GmQ;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2392009
    iget-object v1, p0, LX/GmQ;->c:LX/BAa;

    invoke-virtual {v1, v0}, LX/BAa;->a(Ljava/lang/CharSequence;)LX/BAa;

    move-result-object v1

    new-instance v3, LX/3pe;

    invoke-direct {v3}, LX/3pe;-><init>()V

    invoke-virtual {v3, v0}, LX/3pe;->b(Ljava/lang/CharSequence;)LX/3pe;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/BAa;->a(LX/3pc;)LX/BAa;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/BAa;->a(I)LX/BAa;

    .line 2392010
    iget-object v0, p0, LX/GmQ;->b:LX/2c4;

    sget-object v1, Lcom/facebook/notifications/constants/NotificationType;->TOR_STATUS:Lcom/facebook/notifications/constants/NotificationType;

    iget-object v2, p0, LX/GmQ;->c:LX/BAa;

    iget-object v3, p0, LX/GmQ;->f:Landroid/content/Intent;

    sget-object v4, LX/8D4;->ACTIVITY:LX/8D4;

    iget-object v5, p0, LX/GmQ;->e:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-virtual/range {v0 .. v5}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;LX/BAa;Landroid/content/Intent;LX/8D4;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    goto/16 :goto_0

    .line 2392011
    :cond_4
    const v0, 0x7f020d43

    move v2, v0

    goto :goto_1

    .line 2392012
    :cond_5
    sget-object v0, LX/4cd;->CHECKING_CONNECTION:LX/4cd;

    iget v0, v0, LX/4cd;->resId:I

    move v1, v0

    goto :goto_2
.end method
