.class public final LX/GKW;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/GCE;

.field public final synthetic b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/GKX;


# direct methods
.method public constructor <init>(LX/GKX;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2340992
    iput-object p1, p0, LX/GKW;->d:LX/GKX;

    iput-object p2, p0, LX/GKW;->a:LX/GCE;

    iput-object p3, p0, LX/GKW;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iput-object p4, p0, LX/GKW;->c:Landroid/content/Context;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2340993
    iget-object v0, p0, LX/GKW;->d:LX/GKX;

    iget-object v0, v0, LX/GKX;->b:LX/GN0;

    iget-object v1, p0, LX/GKW;->a:LX/GCE;

    iget-object v2, p0, LX/GKW;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, p1, v1, v2}, LX/GN0;->a(Landroid/view/View;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2340994
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2340995
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2340996
    iget-object v0, p0, LX/GKW;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0124

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2340997
    return-void
.end method
