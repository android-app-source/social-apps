.class public final LX/H4W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2422835
    iput-object p1, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2422836
    iget-object v0, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2422837
    iget-object v0, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082125

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 2422838
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2422839
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2422840
    check-cast p1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2422841
    iget-object v0, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2422842
    if-eqz p1, :cond_0

    .line 2422843
    iget-object v0, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iput-object p1, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2422844
    iget-object v0, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    sget-object v1, LX/H4Z;->LOCATIONS:LX/H4Z;

    invoke-virtual {v0, v1}, LX/H4a;->a(LX/H4Z;)V

    .line 2422845
    iget-object v0, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    sget-object v1, LX/H4Z;->LOCATIONS:LX/H4Z;

    invoke-virtual {v0, v1}, LX/H4a;->b(LX/H4Z;)V

    .line 2422846
    iget-object v0, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    if-ne v0, v1, :cond_0

    .line 2422847
    iget-object v0, p0, LX/H4W;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    const v1, 0xeeb7ed3    # 5.80541E-30f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2422848
    :cond_0
    return-void
.end method
