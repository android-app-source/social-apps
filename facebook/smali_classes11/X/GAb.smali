.class public final enum LX/GAb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GAb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GAb;

.field public static final enum APP_EVENTS:LX/GAb;

.field public static final enum CACHE:LX/GAb;

.field public static final enum DEVELOPER_ERRORS:LX/GAb;

.field public static final enum GRAPH_API_DEBUG_INFO:LX/GAb;

.field public static final enum GRAPH_API_DEBUG_WARNING:LX/GAb;

.field public static final enum INCLUDE_ACCESS_TOKENS:LX/GAb;

.field public static final enum INCLUDE_RAW_RESPONSES:LX/GAb;

.field public static final enum REQUESTS:LX/GAb;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2325936
    new-instance v0, LX/GAb;

    const-string v1, "REQUESTS"

    invoke-direct {v0, v1, v3}, LX/GAb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAb;->REQUESTS:LX/GAb;

    .line 2325937
    new-instance v0, LX/GAb;

    const-string v1, "INCLUDE_ACCESS_TOKENS"

    invoke-direct {v0, v1, v4}, LX/GAb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAb;->INCLUDE_ACCESS_TOKENS:LX/GAb;

    .line 2325938
    new-instance v0, LX/GAb;

    const-string v1, "INCLUDE_RAW_RESPONSES"

    invoke-direct {v0, v1, v5}, LX/GAb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAb;->INCLUDE_RAW_RESPONSES:LX/GAb;

    .line 2325939
    new-instance v0, LX/GAb;

    const-string v1, "CACHE"

    invoke-direct {v0, v1, v6}, LX/GAb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAb;->CACHE:LX/GAb;

    .line 2325940
    new-instance v0, LX/GAb;

    const-string v1, "APP_EVENTS"

    invoke-direct {v0, v1, v7}, LX/GAb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAb;->APP_EVENTS:LX/GAb;

    .line 2325941
    new-instance v0, LX/GAb;

    const-string v1, "DEVELOPER_ERRORS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GAb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAb;->DEVELOPER_ERRORS:LX/GAb;

    .line 2325942
    new-instance v0, LX/GAb;

    const-string v1, "GRAPH_API_DEBUG_WARNING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/GAb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAb;->GRAPH_API_DEBUG_WARNING:LX/GAb;

    .line 2325943
    new-instance v0, LX/GAb;

    const-string v1, "GRAPH_API_DEBUG_INFO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/GAb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GAb;->GRAPH_API_DEBUG_INFO:LX/GAb;

    .line 2325944
    const/16 v0, 0x8

    new-array v0, v0, [LX/GAb;

    sget-object v1, LX/GAb;->REQUESTS:LX/GAb;

    aput-object v1, v0, v3

    sget-object v1, LX/GAb;->INCLUDE_ACCESS_TOKENS:LX/GAb;

    aput-object v1, v0, v4

    sget-object v1, LX/GAb;->INCLUDE_RAW_RESPONSES:LX/GAb;

    aput-object v1, v0, v5

    sget-object v1, LX/GAb;->CACHE:LX/GAb;

    aput-object v1, v0, v6

    sget-object v1, LX/GAb;->APP_EVENTS:LX/GAb;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GAb;->DEVELOPER_ERRORS:LX/GAb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GAb;->GRAPH_API_DEBUG_WARNING:LX/GAb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GAb;->GRAPH_API_DEBUG_INFO:LX/GAb;

    aput-object v2, v0, v1

    sput-object v0, LX/GAb;->$VALUES:[LX/GAb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2325945
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GAb;
    .locals 1

    .prologue
    .line 2325946
    const-class v0, LX/GAb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GAb;

    return-object v0
.end method

.method public static values()[LX/GAb;
    .locals 1

    .prologue
    .line 2325947
    sget-object v0, LX/GAb;->$VALUES:[LX/GAb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GAb;

    return-object v0
.end method
