.class public final LX/HCW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;)V
    .locals 0

    .prologue
    .line 2438711
    iput-object p1, p0, LX/HCW;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 2438712
    iget-object v0, p0, LX/HCW;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    sget-object v1, LX/9X7;->TEMPLATE_CONFIRM_APPLICATION:LX/9X7;

    invoke-static {v0, v1}, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->a$redex0(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;LX/9X7;)V

    .line 2438713
    iget-object v0, p0, LX/HCW;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    .line 2438714
    new-instance v2, LX/4JN;

    invoke-direct {v2}, LX/4JN;-><init>()V

    iget-wide v4, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->f:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2438715
    const-string v4, "page_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438716
    move-object v2, v2

    .line 2438717
    iget-object v3, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->g:Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->name()Ljava/lang/String;

    move-result-object v3

    .line 2438718
    const-string v4, "template_type"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438719
    move-object v2, v2

    .line 2438720
    new-instance v3, LX/HD8;

    invoke-direct {v3}, LX/HD8;-><init>()V

    move-object v3, v3

    .line 2438721
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/HD8;

    .line 2438722
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2438723
    iget-object v2, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ck;

    const-string v5, "set_page_template_mutation"

    iget-object v3, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-virtual {v3, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, LX/HCX;

    invoke-direct {v4, v0}, LX/HCX;-><init>(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;)V

    invoke-virtual {v2, v5, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2438724
    return-void
.end method
