.class public final LX/HCs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2440133
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2440134
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440135
    :goto_0
    return v1

    .line 2440136
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440137
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2440138
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2440139
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2440140
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2440141
    const-string v3, "commerce_merchant_settings"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2440142
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2440143
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 2440144
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2440145
    :goto_2
    move v0, v2

    .line 2440146
    goto :goto_1

    .line 2440147
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2440148
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2440149
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2440150
    :cond_4
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 2440151
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2440152
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2440153
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_4

    if-eqz v5, :cond_4

    .line 2440154
    const-string v6, "show_edit_interface"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2440155
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v4, v0

    move v0, v3

    goto :goto_3

    .line 2440156
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2440157
    :cond_6
    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2440158
    if-eqz v0, :cond_7

    .line 2440159
    invoke-virtual {p1, v2, v4}, LX/186;->a(IZ)V

    .line 2440160
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_8
    move v0, v2

    move v4, v2

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2440161
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2440162
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2440163
    if-eqz v0, :cond_1

    .line 2440164
    const-string v1, "commerce_merchant_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440165
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2440166
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2440167
    if-eqz v1, :cond_0

    .line 2440168
    const-string p1, "show_edit_interface"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2440169
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2440170
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2440171
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2440172
    return-void
.end method
