.class public final LX/FdG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/FdH;


# direct methods
.method public constructor <init>(LX/FdH;)V
    .locals 0

    .prologue
    .line 2263333
    iput-object p1, p0, LX/FdG;->a:LX/FdH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x7dad83c7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2263334
    iget-object v0, p0, LX/FdG;->a:LX/FdH;

    const/4 v5, 0x0

    .line 2263335
    iget-object v2, v0, LX/FdH;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_1

    iget-object v2, v0, LX/FdH;->a:LX/0Px;

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FdE;

    .line 2263336
    invoke-virtual {v2}, LX/FdE;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    if-ne v4, v8, :cond_0

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v2, v4}, LX/FdE;->setChecked(Z)V

    .line 2263337
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_0

    :cond_0
    move v4, v5

    .line 2263338
    goto :goto_1

    .line 2263339
    :cond_1
    iget-object v0, p0, LX/FdG;->a:LX/FdH;

    iget-object v2, v0, LX/FdH;->c:LX/Fcf;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2263340
    iget-object v4, v2, LX/Fcf;->a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2263341
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v5

    if-nez v5, :cond_2

    .line 2263342
    :goto_2
    const v0, -0xfbecd2a

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2263343
    :cond_2
    iget-object v5, v2, LX/Fcf;->b:LX/FdQ;

    new-instance v6, LX/CyH;

    iget-object v7, v2, LX/Fcf;->c:LX/CyH;

    .line 2263344
    iget-object v8, v7, LX/CyH;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2263345
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v7, v8, v4}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, LX/FdQ;->a(LX/CyH;)V

    goto :goto_2
.end method
