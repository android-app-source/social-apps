.class public final LX/FoM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V
    .locals 0

    .prologue
    .line 2289333
    iput-object p1, p0, LX/FoM;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2cd80e5e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2289334
    iget-object v1, p0, LX/FoM;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->c:LX/0Zb;

    iget-object v2, p0, LX/FoM;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    .line 2289335
    const-string p1, "invite"

    invoke-static {v2, p1}, LX/BOe;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    move-object v2, p1

    .line 2289336
    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2289337
    iget-object v1, p0, LX/FoM;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    .line 2289338
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->l:LX/BOb;

    iget-object v4, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    const-string p0, "native_thank_you_page"

    const/4 p1, 0x0

    invoke-virtual {v2, v4, p0, p1}, LX/BOb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2289339
    if-eqz v4, :cond_0

    .line 2289340
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/17Y;

    iget-object p0, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->j:Landroid/content/Context;

    invoke-interface {v2, p0, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2289341
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x2767

    invoke-interface {v2, v4, p0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2289342
    :cond_0
    const v1, -0x39865032

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
