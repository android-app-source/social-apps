.class public final enum LX/Fjd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fjd;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fjd;

.field public static final enum GENERAL:LX/Fjd;

.field public static final enum SPECIFIC:LX/Fjd;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2277010
    new-instance v0, LX/Fjd;

    const-string v1, "GENERAL"

    invoke-direct {v0, v1, v2}, LX/Fjd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fjd;->GENERAL:LX/Fjd;

    .line 2277011
    new-instance v0, LX/Fjd;

    const-string v1, "SPECIFIC"

    invoke-direct {v0, v1, v3}, LX/Fjd;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fjd;->SPECIFIC:LX/Fjd;

    .line 2277012
    const/4 v0, 0x2

    new-array v0, v0, [LX/Fjd;

    sget-object v1, LX/Fjd;->GENERAL:LX/Fjd;

    aput-object v1, v0, v2

    sget-object v1, LX/Fjd;->SPECIFIC:LX/Fjd;

    aput-object v1, v0, v3

    sput-object v0, LX/Fjd;->$VALUES:[LX/Fjd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2277013
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fjd;
    .locals 1

    .prologue
    .line 2277009
    const-class v0, LX/Fjd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fjd;

    return-object v0
.end method

.method public static values()[LX/Fjd;
    .locals 1

    .prologue
    .line 2277008
    sget-object v0, LX/Fjd;->$VALUES:[LX/Fjd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fjd;

    return-object v0
.end method
