.class public LX/FKd;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/messaging/service/model/FetchMessageParams;",
        "Lcom/facebook/messaging/service/model/FetchMessageResult;",
        ">;"
    }
.end annotation


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3MX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2225354
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2225355
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2225356
    iput-object v0, p0, LX/FKd;->b:LX/0Ot;

    .line 2225357
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2225358
    iput-object v0, p0, LX/FKd;->c:LX/0Ot;

    .line 2225359
    return-void
.end method

.method public static a(LX/0QB;)LX/FKd;
    .locals 3

    .prologue
    .line 2225349
    new-instance v1, LX/FKd;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-direct {v1, v0}, LX/FKd;-><init>(LX/0sO;)V

    .line 2225350
    const/16 v0, 0x2a01

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    const/16 v2, 0x435

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 2225351
    iput-object v0, v1, LX/FKd;->b:LX/0Ot;

    iput-object v2, v1, LX/FKd;->c:LX/0Ot;

    .line 2225352
    move-object v0, v1

    .line 2225353
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2225362
    check-cast p1, Lcom/facebook/messaging/service/model/FetchMessageParams;

    .line 2225363
    iget-object v0, p0, LX/0ro;->a:LX/0sO;

    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;

    invoke-static {v1}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/0sO;->a(LX/0w5;LX/15w;)Ljava/util/List;

    move-result-object v1

    .line 2225364
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 2225365
    const/4 v0, 0x0

    .line 2225366
    :goto_0
    return-object v0

    .line 2225367
    :cond_0
    new-instance v2, Lcom/facebook/messaging/service/model/FetchMessageParams;

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MessageInfoModel;->w()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchMessageParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v2, v0, v3}, Lcom/facebook/messaging/service/model/FetchMessageParams;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2225368
    iget-object v0, p0, LX/FKd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Collection;Ljava/util/Set;)LX/0P1;

    move-result-object v0

    iget-object v1, v2, Lcom/facebook/messaging/service/model/FetchMessageParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMessageResult;

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2225369
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2225360
    check-cast p1, Lcom/facebook/messaging/service/model/FetchMessageParams;

    .line 2225361
    invoke-static {}, LX/5ZC;->g()LX/5Z3;

    move-result-object v0

    const-string v1, "thread_msg_ids"

    iget-object v2, p0, LX/FKd;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Ljava/util/Set;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_pic_medium_size"

    iget-object v0, p0, LX/FKd;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MX;

    sget-object v3, LX/3MY;->BIG:LX/3MY;

    invoke-virtual {v0, v3}, LX/3MX;->a(LX/3MY;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
