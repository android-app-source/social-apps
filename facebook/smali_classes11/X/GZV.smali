.class public final LX/GZV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;J)V
    .locals 0

    .prologue
    .line 2366565
    iput-object p1, p0, LX/GZV;->b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    iput-wide p2, p0, LX/GZV;->a:J

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2366566
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2366567
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2366568
    if-eqz p1, :cond_0

    .line 2366569
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2366570
    if-nez v0, :cond_1

    .line 2366571
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Null or empty collection query response from server"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/GZV;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2366572
    :goto_0
    return-void

    .line 2366573
    :cond_1
    iget-object v1, p0, LX/GZV;->b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    .line 2366574
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2366575
    check-cast v0, Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;

    .line 2366576
    iput-object v0, v1, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->u:Lcom/facebook/commerce/storefront/graphql/FetchStorefrontCollectionQueryModels$FetchStorefrontCollectionQueryModel;

    .line 2366577
    iget-object v0, p0, LX/GZV;->b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    iget-wide v2, p0, LX/GZV;->a:J

    invoke-static {v0, v2, v3}, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->a$redex0(Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;J)V

    .line 2366578
    iget-object v0, p0, LX/GZV;->b:Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    iget-object v0, v0, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;->a:LX/7iU;

    const v3, 0x6c0001

    .line 2366579
    iget-object v1, v0, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2366580
    iget-object v1, v0, LX/7iU;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v1, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2366581
    :cond_2
    goto :goto_0
.end method
