.class public abstract LX/GVq;
.super LX/42j;
.source ""

# interfaces
.implements LX/0f1;
.implements LX/0f5;
.implements LX/0f8;
.implements LX/1ZF;


# instance fields
.field private a:LX/0hE;

.field private b:LX/0hE;

.field public c:LX/0hE;

.field public d:LX/0iI;

.field public e:LX/0iL;

.field public f:LX/0iM;

.field public g:LX/0iQ;

.field public h:LX/0h5;

.field private i:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2360088
    invoke-direct {p0}, LX/42j;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract B()Landroid/support/v4/app/Fragment;
.end method

.method public abstract C()V
.end method

.method public abstract D()V
.end method

.method public final E()Z
    .locals 1

    .prologue
    .line 2360089
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2360090
    invoke-virtual {p0}, LX/GVq;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2360091
    instance-of v1, v0, LX/0fh;

    if-eqz v1, :cond_0

    .line 2360092
    check-cast v0, LX/0fh;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    .line 2360093
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method public final a(LX/0hE;)V
    .locals 0

    .prologue
    .line 2360094
    iput-object p1, p0, LX/GVq;->a:LX/0hE;

    .line 2360095
    return-void
.end method

.method public final a(LX/63W;)V
    .locals 1
    .param p1    # LX/63W;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2360009
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2360010
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2
    .param p1    # Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2360096
    if-nez p1, :cond_0

    .line 2360097
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2360098
    :goto_0
    iget-object v1, p0, LX/GVq;->h:LX/0h5;

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2360099
    return-void

    .line 2360100
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2360101
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2360102
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2360103
    invoke-virtual {p0}, LX/GVq;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2360104
    instance-of v1, v0, LX/0hF;

    if-eqz v1, :cond_0

    .line 2360105
    check-cast v0, LX/0hF;

    invoke-interface {v0}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v0

    .line 2360106
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 2360107
    if-nez p1, :cond_0

    .line 2360108
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2360109
    move-object v1, v0

    .line 2360110
    :goto_0
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    instance-of v0, v0, LX/0hA;

    if-eqz v0, :cond_1

    .line 2360111
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    check-cast v0, LX/0hA;

    invoke-interface {v0, v1}, LX/0hA;->setButtonSpecsWithAnimation(Ljava/util/List;)V

    .line 2360112
    :goto_1
    return-void

    .line 2360113
    :cond_0
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2360114
    :cond_1
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    goto :goto_1
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 2360115
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    if-eqz v0, :cond_0

    .line 2360116
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    invoke-interface {v0, p1}, LX/0hE;->t_(I)V

    .line 2360117
    :cond_0
    invoke-super {p0, p1, p2}, LX/42j;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final d(Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2360078
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 2360079
    :goto_0
    return v0

    .line 2360080
    :cond_0
    invoke-virtual {p0}, LX/GVq;->B()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2360081
    if-eqz p1, :cond_1

    instance-of v0, v1, Lcom/facebook/search/fragment/GraphSearchFragment;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/facebook/search/fragment/GraphSearchFragment;

    .line 2360082
    iget-object p0, v0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/FZe;->a(Z)Z

    move-result p0

    move v0, p0

    .line 2360083
    if-eqz v0, :cond_1

    move v0, v2

    .line 2360084
    goto :goto_0

    .line 2360085
    :cond_1
    instance-of v0, v1, LX/0fj;

    if-eqz v0, :cond_2

    check-cast v1, LX/0fj;

    invoke-interface {v1}, LX/0fj;->S_()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 2360086
    goto :goto_0

    .line 2360087
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 2360118
    invoke-super {p0, p1}, LX/42j;->e(I)V

    .line 2360119
    invoke-virtual {p0}, LX/GVq;->C()V

    .line 2360120
    invoke-virtual {p0}, LX/GVq;->D()V

    .line 2360121
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1

    .prologue
    .line 2360077
    iget-object v0, p0, LX/GVq;->i:Landroid/view/View;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2360076
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2360071
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2360072
    invoke-virtual {p0}, LX/GVq;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2360073
    instance-of v2, v0, LX/0fk;

    if-eqz v2, :cond_0

    .line 2360074
    check-cast v0, LX/0fk;

    invoke-interface {v0}, LX/0f6;->getDebugInfo()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2360075
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/0hE;
    .locals 2

    .prologue
    .line 2360065
    iget-object v0, p0, LX/GVq;->a:LX/0hE;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/GVq;->d:LX/0iI;

    if-eqz v0, :cond_0

    .line 2360066
    iget-object v0, p0, LX/GVq;->d:LX/0iI;

    .line 2360067
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2360068
    invoke-virtual {v0, v1}, LX/0iI;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, LX/GVq;->a:LX/0hE;

    .line 2360069
    :cond_0
    iget-object v0, p0, LX/GVq;->a:LX/0hE;

    iput-object v0, p0, LX/GVq;->c:LX/0hE;

    .line 2360070
    iget-object v0, p0, LX/GVq;->a:LX/0hE;

    return-object v0
.end method

.method public final k_(Z)V
    .locals 2

    .prologue
    .line 2360061
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    instance-of v0, v0, LX/0h4;

    if-eqz v0, :cond_0

    .line 2360062
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    check-cast v0, LX/0h4;

    if-nez p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v0, v1}, LX/0h4;->setSearchButtonVisible(Z)V

    .line 2360063
    :cond_0
    return-void

    .line 2360064
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final l()LX/0hE;
    .locals 2

    .prologue
    .line 2360056
    iget-object v0, p0, LX/GVq;->e:LX/0iL;

    .line 2360057
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2360058
    invoke-virtual {v0, v1}, LX/0iL;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, LX/GVq;->b:LX/0hE;

    .line 2360059
    iget-object v0, p0, LX/GVq;->b:LX/0hE;

    iput-object v0, p0, LX/GVq;->c:LX/0hE;

    .line 2360060
    iget-object v0, p0, LX/GVq;->b:LX/0hE;

    return-object v0
.end method

.method public final lH_()V
    .locals 2

    .prologue
    .line 2360051
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    .line 2360052
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2360053
    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2360054
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2360055
    return-void
.end method

.method public final m()LX/0hE;
    .locals 2

    .prologue
    .line 2360043
    iget-object v0, p0, LX/GVq;->f:LX/0iM;

    if-nez v0, :cond_0

    .line 2360044
    const/4 v0, 0x0

    .line 2360045
    :goto_0
    return-object v0

    .line 2360046
    :cond_0
    iget-object v0, p0, LX/GVq;->f:LX/0iM;

    .line 2360047
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2360048
    invoke-virtual {v0, v1}, LX/0iM;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, LX/GVq;->a:LX/0hE;

    .line 2360049
    iget-object v0, p0, LX/GVq;->a:LX/0hE;

    iput-object v0, p0, LX/GVq;->c:LX/0hE;

    .line 2360050
    iget-object v0, p0, LX/GVq;->a:LX/0hE;

    goto :goto_0
.end method

.method public n()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2360024
    iput-object v1, p0, LX/GVq;->a:LX/0hE;

    .line 2360025
    iput-object v1, p0, LX/GVq;->b:LX/0hE;

    .line 2360026
    iput-object v1, p0, LX/GVq;->c:LX/0hE;

    .line 2360027
    iget-object v0, p0, LX/GVq;->d:LX/0iI;

    if-eqz v0, :cond_0

    .line 2360028
    iget-object v0, p0, LX/GVq;->d:LX/0iI;

    .line 2360029
    const/4 v2, 0x0

    iput-object v2, v0, LX/0iI;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    .line 2360030
    iput-object v1, p0, LX/GVq;->d:LX/0iI;

    .line 2360031
    :cond_0
    iget-object v0, p0, LX/GVq;->e:LX/0iL;

    if-eqz v0, :cond_1

    .line 2360032
    iget-object v0, p0, LX/GVq;->e:LX/0iL;

    invoke-virtual {v0}, LX/0iL;->a()V

    .line 2360033
    iput-object v1, p0, LX/GVq;->e:LX/0iL;

    .line 2360034
    :cond_1
    iget-object v0, p0, LX/GVq;->f:LX/0iM;

    if-eqz v0, :cond_2

    .line 2360035
    iget-object v0, p0, LX/GVq;->f:LX/0iM;

    .line 2360036
    const/4 v2, 0x0

    iput-object v2, v0, LX/0iM;->a:LX/D8W;

    .line 2360037
    iput-object v1, p0, LX/GVq;->f:LX/0iM;

    .line 2360038
    :cond_2
    iget-object v0, p0, LX/GVq;->g:LX/0iQ;

    if-eqz v0, :cond_3

    .line 2360039
    iget-object v0, p0, LX/GVq;->g:LX/0iQ;

    invoke-virtual {v0}, LX/0iQ;->a()V

    .line 2360040
    iput-object v1, p0, LX/GVq;->g:LX/0iQ;

    .line 2360041
    :cond_3
    invoke-super {p0}, LX/42j;->n()V

    .line 2360042
    return-void
.end method

.method public final o()LX/0hE;
    .locals 2

    .prologue
    .line 2360019
    iget-object v0, p0, LX/GVq;->g:LX/0iQ;

    .line 2360020
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2360021
    invoke-virtual {v0, v1}, LX/0iQ;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, LX/GVq;->a:LX/0hE;

    .line 2360022
    iget-object v0, p0, LX/GVq;->a:LX/0hE;

    iput-object v0, p0, LX/GVq;->c:LX/0hE;

    .line 2360023
    iget-object v0, p0, LX/GVq;->a:LX/0hE;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2360016
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2360017
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    move-result v0

    .line 2360018
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setCustomTitle(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2360013
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 2360014
    iput-object p1, p0, LX/GVq;->i:Landroid/view/View;

    .line 2360015
    return-void
.end method

.method public final x_(I)V
    .locals 1

    .prologue
    .line 2360011
    iget-object v0, p0, LX/GVq;->h:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(I)V

    .line 2360012
    return-void
.end method
