.class public LX/G2L;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/G2L;


# instance fields
.field public final e:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2313935
    const-string v0, "PHOTOS"

    const-string v1, "FRIENDS"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G2L;->a:LX/0Px;

    .line 2313936
    const-string v0, "VIDEOS"

    const-string v1, "PHOTOS"

    const-string v2, "FRIENDS"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G2L;->b:LX/0Px;

    .line 2313937
    const-string v0, "PHOTO_GRID"

    const-string v1, "FRIEND_GRID"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G2L;->c:LX/0Px;

    .line 2313938
    const-string v0, "VIDEO_GRID"

    const-string v1, "PHOTO_GRID"

    const-string v2, "FRIEND_GRID"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G2L;->d:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2313933
    iput-object p1, p0, LX/G2L;->e:LX/0Uh;

    .line 2313934
    return-void
.end method

.method public static a(LX/0QB;)LX/G2L;
    .locals 4

    .prologue
    .line 2313919
    sget-object v0, LX/G2L;->f:LX/G2L;

    if-nez v0, :cond_1

    .line 2313920
    const-class v1, LX/G2L;

    monitor-enter v1

    .line 2313921
    :try_start_0
    sget-object v0, LX/G2L;->f:LX/G2L;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2313922
    if-eqz v2, :cond_0

    .line 2313923
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2313924
    new-instance p0, LX/G2L;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p0, v3}, LX/G2L;-><init>(LX/0Uh;)V

    .line 2313925
    move-object v0, p0

    .line 2313926
    sput-object v0, LX/G2L;->f:LX/G2L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313927
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2313928
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2313929
    :cond_1
    sget-object v0, LX/G2L;->f:LX/G2L;

    return-object v0

    .line 2313930
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2313931
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2313918
    iget-object v0, p0, LX/G2L;->e:LX/0Uh;

    const/16 v1, 0x464

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/G2L;->b:LX/0Px;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/G2L;->a:LX/0Px;

    goto :goto_0
.end method
