.class public interface abstract LX/GGc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/GGX;

.field public static final b:LX/GGX;

.field public static final c:LX/GGX;

.field public static final d:LX/GGX;

.field public static final e:LX/GGX;

.field public static final f:LX/GGX;

.field public static final g:LX/GGX;

.field public static final h:LX/GGX;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 2334387
    invoke-static {}, LX/GGZ;->newBuilder()LX/GGZ;

    move-result-object v9

    sget-object v0, LX/GGB;->INACTIVE:LX/GGB;

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    sget-object v3, LX/GGB;->PAUSED:LX/GGB;

    sget-object v4, LX/GGB;->EXTENDABLE:LX/GGB;

    sget-object v5, LX/GGB;->CREATING:LX/GGB;

    sget-object v6, LX/GGB;->FINISHED:LX/GGB;

    sget-object v7, LX/GGB;->PENDING:LX/GGB;

    sget-object v8, LX/GGB;->REJECTED:LX/GGB;

    invoke-static/range {v0 .. v8}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2334388
    iput-object v0, v9, LX/GGZ;->a:LX/0Px;

    .line 2334389
    move-object v0, v9

    .line 2334390
    invoke-virtual {v0}, LX/GGZ;->a()LX/GGX;

    move-result-object v0

    sput-object v0, LX/GGc;->a:LX/GGX;

    .line 2334391
    invoke-static {}, LX/GGZ;->newBuilder()LX/GGZ;

    move-result-object v10

    sget-object v0, LX/GGB;->INACTIVE:LX/GGB;

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    sget-object v3, LX/GGB;->PAUSED:LX/GGB;

    sget-object v4, LX/GGB;->EXTENDABLE:LX/GGB;

    sget-object v5, LX/GGB;->CREATING:LX/GGB;

    sget-object v6, LX/GGB;->FINISHED:LX/GGB;

    sget-object v7, LX/GGB;->PENDING:LX/GGB;

    sget-object v8, LX/GGB;->REJECTED:LX/GGB;

    sget-object v9, LX/GGB;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/GGB;

    invoke-static/range {v0 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2334392
    iput-object v0, v10, LX/GGZ;->a:LX/0Px;

    .line 2334393
    move-object v0, v10

    .line 2334394
    invoke-virtual {v0}, LX/GGZ;->a()LX/GGX;

    move-result-object v0

    sput-object v0, LX/GGc;->b:LX/GGX;

    .line 2334395
    invoke-static {}, LX/GGZ;->newBuilder()LX/GGZ;

    move-result-object v0

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    sget-object v3, LX/GGB;->REJECTED:LX/GGB;

    sget-object v4, LX/GGB;->FINISHED:LX/GGB;

    sget-object v5, LX/GGB;->PAUSED:LX/GGB;

    invoke-static {v1, v2, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2334396
    iput-object v1, v0, LX/GGZ;->a:LX/0Px;

    .line 2334397
    move-object v0, v0

    .line 2334398
    invoke-virtual {v0}, LX/GGZ;->a()LX/GGX;

    move-result-object v0

    sput-object v0, LX/GGc;->c:LX/GGX;

    .line 2334399
    invoke-static {}, LX/GGZ;->newBuilder()LX/GGZ;

    move-result-object v0

    sget-object v1, LX/GGB;->CREATING:LX/GGB;

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    sget-object v3, LX/GGB;->PENDING:LX/GGB;

    sget-object v4, LX/GGB;->PAUSED:LX/GGB;

    invoke-static {v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2334400
    iput-object v1, v0, LX/GGZ;->a:LX/0Px;

    .line 2334401
    move-object v0, v0

    .line 2334402
    invoke-virtual {v0}, LX/GGZ;->a()LX/GGX;

    move-result-object v0

    sput-object v0, LX/GGc;->d:LX/GGX;

    .line 2334403
    invoke-static {}, LX/GGZ;->newBuilder()LX/GGZ;

    move-result-object v8

    sget-object v0, LX/GGB;->INACTIVE:LX/GGB;

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    sget-object v3, LX/GGB;->PAUSED:LX/GGB;

    sget-object v4, LX/GGB;->EXTENDABLE:LX/GGB;

    sget-object v5, LX/GGB;->FINISHED:LX/GGB;

    sget-object v6, LX/GGB;->PENDING:LX/GGB;

    sget-object v7, LX/GGB;->REJECTED:LX/GGB;

    invoke-static/range {v0 .. v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2334404
    iput-object v0, v8, LX/GGZ;->a:LX/0Px;

    .line 2334405
    move-object v0, v8

    .line 2334406
    invoke-virtual {v0}, LX/GGZ;->a()LX/GGX;

    move-result-object v0

    sput-object v0, LX/GGc;->e:LX/GGX;

    .line 2334407
    invoke-static {}, LX/GGZ;->newBuilder()LX/GGZ;

    move-result-object v0

    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2334408
    iput-object v1, v0, LX/GGZ;->a:LX/0Px;

    .line 2334409
    move-object v0, v0

    .line 2334410
    invoke-virtual {v0}, LX/GGZ;->a()LX/GGX;

    move-result-object v0

    sput-object v0, LX/GGc;->f:LX/GGX;

    .line 2334411
    new-instance v0, LX/GGa;

    invoke-direct {v0}, LX/GGa;-><init>()V

    sput-object v0, LX/GGc;->g:LX/GGX;

    .line 2334412
    new-instance v0, LX/GGb;

    invoke-direct {v0}, LX/GGb;-><init>()V

    sput-object v0, LX/GGc;->h:LX/GGX;

    return-void
.end method


# virtual methods
.method public abstract a()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW",
            "<*-TE;>;>;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/content/Intent;LX/GCY;)V
.end method
