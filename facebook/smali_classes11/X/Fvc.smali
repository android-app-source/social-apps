.class public final enum LX/Fvc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fvc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fvc;

.field public static final enum BOTTOM_PADDING_AND_NUX_PART:LX/Fvc;

.field public static final enum NON_SELF_PROFILE_INTRO_CTA:LX/Fvc;

.field private static mValues:[LX/Fvc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2301672
    new-instance v0, LX/Fvc;

    const-string v1, "BOTTOM_PADDING_AND_NUX_PART"

    invoke-direct {v0, v1, v2}, LX/Fvc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvc;->BOTTOM_PADDING_AND_NUX_PART:LX/Fvc;

    .line 2301673
    new-instance v0, LX/Fvc;

    const-string v1, "NON_SELF_PROFILE_INTRO_CTA"

    invoke-direct {v0, v1, v3}, LX/Fvc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fvc;->NON_SELF_PROFILE_INTRO_CTA:LX/Fvc;

    .line 2301674
    const/4 v0, 0x2

    new-array v0, v0, [LX/Fvc;

    sget-object v1, LX/Fvc;->BOTTOM_PADDING_AND_NUX_PART:LX/Fvc;

    aput-object v1, v0, v2

    sget-object v1, LX/Fvc;->NON_SELF_PROFILE_INTRO_CTA:LX/Fvc;

    aput-object v1, v0, v3

    sput-object v0, LX/Fvc;->$VALUES:[LX/Fvc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2301675
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static cachedValues()[LX/Fvc;
    .locals 1

    .prologue
    .line 2301676
    sget-object v0, LX/Fvc;->mValues:[LX/Fvc;

    if-nez v0, :cond_0

    .line 2301677
    invoke-static {}, LX/Fvc;->values()[LX/Fvc;

    move-result-object v0

    sput-object v0, LX/Fvc;->mValues:[LX/Fvc;

    .line 2301678
    :cond_0
    sget-object v0, LX/Fvc;->mValues:[LX/Fvc;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fvc;
    .locals 1

    .prologue
    .line 2301679
    const-class v0, LX/Fvc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fvc;

    return-object v0
.end method

.method public static values()[LX/Fvc;
    .locals 1

    .prologue
    .line 2301680
    sget-object v0, LX/Fvc;->$VALUES:[LX/Fvc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fvc;

    return-object v0
.end method
