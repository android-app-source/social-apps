.class public LX/GpW;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "LX/Gpt;",
        "Lcom/facebook/instantshopping/model/data/ColorPickerBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/Gpt;)V
    .locals 0

    .prologue
    .line 2395050
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395051
    return-void
.end method

.method private b(LX/Gox;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2395052
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395053
    check-cast v0, LX/Gq6;

    .line 2395054
    iget-object v1, v0, LX/Gq6;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2395055
    invoke-interface {p1}, LX/Goa;->C()I

    move-result v0

    div-int/lit8 v4, v0, 0xa

    move v2, v3

    .line 2395056
    :goto_0
    if-ge v2, v4, :cond_0

    .line 2395057
    invoke-virtual {p1}, LX/Gox;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 2395058
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395059
    check-cast v0, LX/Gpt;

    invoke-virtual {p1}, LX/Gox;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;

    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v5

    invoke-interface {v0, v1, v3, v5}, LX/Gpt;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;ZLX/GoE;)V

    .line 2395060
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2395061
    :cond_0
    invoke-virtual {p1}, LX/Gox;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    .line 2395062
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395063
    check-cast v0, LX/Gq6;

    invoke-virtual {p1}, LX/Gox;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    sub-int/2addr v1, v4

    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v2

    const/4 p1, 0x0

    .line 2395064
    iput-object v2, v0, LX/Gq6;->d:LX/GoE;

    .line 2395065
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 2395066
    const v4, 0x7f030965

    iget-object v5, v0, LX/Gq6;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4, v5, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/instantshopping/view/widget/ColorPickerPileTextItemLayout;

    .line 2395067
    iget-object v4, v0, LX/Gq6;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2395068
    invoke-virtual {v3}, Lcom/facebook/instantshopping/view/widget/ColorPickerPileTextItemLayout;->a()V

    .line 2395069
    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0828ce

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v5, p1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/instantshopping/view/widget/ColorPickerPileTextItemLayout;->setMoreText(Ljava/lang/String;)V

    .line 2395070
    iget-object v4, v0, LX/Gq6;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Lcom/facebook/instantshopping/view/widget/ColorPickerPileTextItemLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395071
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 2

    .prologue
    .line 2395072
    check-cast p1, LX/Gox;

    .line 2395073
    invoke-direct {p0, p1}, LX/GpW;->b(LX/Gox;)V

    .line 2395074
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395075
    check-cast v0, LX/Gq6;

    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    .line 2395076
    iget-object p0, v0, LX/Gq6;->a:LX/Go0;

    invoke-virtual {p0, v1}, LX/Go0;->a(LX/GoE;)V

    .line 2395077
    return-void
.end method
