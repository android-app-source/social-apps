.class public LX/GqX;
.super LX/Cot;
.source ""

# interfaces
.implements LX/Cob;


# instance fields
.field public a:LX/Gqx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/Cu3;

.field public l:LX/Grs;

.field public m:Z


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 2396639
    invoke-direct {p0, p1, p2, p3}, LX/Cot;-><init>(LX/Ctg;Landroid/view/View;Landroid/widget/ImageView;)V

    .line 2396640
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/GqX;

    invoke-static {v0}, LX/Gqx;->b(LX/0QB;)LX/Gqx;

    move-result-object p2

    check-cast p2, LX/Gqx;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object p2, p0, LX/GqX;->a:LX/Gqx;

    iput-object v0, p0, LX/GqX;->e:LX/0ad;

    .line 2396641
    return-void
.end method


# virtual methods
.method public final a(LX/Ctg;LX/CrN;Z)LX/CqX;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2396642
    iget-object v0, p0, LX/GqX;->a:LX/Gqx;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v1, p0, LX/GqX;->m:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/GqX;->e:LX/0ad;

    sget-short v3, LX/CHN;->t:S

    invoke-interface {v1, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v1, p2

    move-object v3, p1

    move v4, p3

    invoke-virtual/range {v0 .. v6}, LX/Gqx;->a(LX/CrN;Landroid/content/Context;LX/Ctg;ZZZ)LX/CqX;

    move-result-object v0

    return-object v0

    :cond_0
    move v5, v6

    goto :goto_0
.end method

.method public final a(LX/Ctg;)V
    .locals 1

    .prologue
    .line 2396643
    new-instance v0, LX/Cu3;

    invoke-direct {v0, p1}, LX/Cu3;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/GqX;->k:LX/Cu3;

    .line 2396644
    iget-object v0, p0, LX/GqX;->k:LX/Cu3;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396645
    new-instance v0, LX/Grs;

    invoke-direct {v0, p1}, LX/Grs;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/GqX;->l:LX/Grs;

    .line 2396646
    iget-object v0, p0, LX/GqX;->l:LX/Grs;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396647
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2396648
    invoke-super {p0, p1}, LX/Cot;->a(Landroid/os/Bundle;)V

    .line 2396649
    iget-object v0, p0, LX/GqX;->k:LX/Cu3;

    invoke-virtual {v0}, LX/Cts;->c()V

    .line 2396650
    iget-object v0, p0, LX/GqX;->l:LX/Grs;

    invoke-virtual {v0}, LX/Cts;->c()V

    .line 2396651
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2396652
    if-eqz p1, :cond_0

    .line 2396653
    iget-object v0, p0, LX/GqX;->k:LX/Cu3;

    .line 2396654
    iget-object p0, v0, LX/Cu2;->c:Landroid/widget/ImageView;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2396655
    :goto_0
    return-void

    .line 2396656
    :cond_0
    iget-object v0, p0, LX/GqX;->k:LX/Cu3;

    .line 2396657
    iget-object p0, v0, LX/Cu2;->c:Landroid/widget/ImageView;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2396658
    goto :goto_0
.end method
