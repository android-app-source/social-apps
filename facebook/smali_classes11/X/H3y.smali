.class public final LX/H3y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;)V
    .locals 0

    .prologue
    .line 2422132
    iput-object p1, p0, LX/H3y;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2422133
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2422134
    check-cast p1, Ljava/util/ArrayList;

    .line 2422135
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2422136
    iget-object v0, p0, LX/H3y;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->g:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    .line 2422137
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    .line 2422138
    iget-object v0, p0, LX/H3y;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->j:LX/H3u;

    const v1, 0x1bea6d71

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2422139
    :cond_0
    return-void
.end method
