.class public LX/G2Y;
.super Lcom/facebook/fbui/widget/text/BadgeTextView;
.source ""


# instance fields
.field public a:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2314198
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/G2Y;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2314199
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2314200
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2314201
    const-class v0, LX/G2Y;

    invoke-static {v0, p0}, LX/G2Y;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2314202
    const v0, 0x7f021953

    invoke-virtual {p0, v0}, LX/G2Y;->setBackgroundResource(I)V

    .line 2314203
    invoke-virtual {p0}, LX/G2Y;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0db3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2314204
    invoke-virtual {p0}, LX/G2Y;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0db4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2314205
    invoke-virtual {p0}, LX/G2Y;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0db5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2314206
    invoke-virtual {p0, v1, v0, v2, v0}, LX/G2Y;->setPadding(IIII)V

    .line 2314207
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/G2Y;->setGravity(I)V

    .line 2314208
    invoke-virtual {p0}, LX/G2Y;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0db6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2314209
    invoke-virtual {p0, v0}, LX/G2Y;->setCompoundDrawablePadding(I)V

    .line 2314210
    iget-object v0, p0, LX/G2Y;->a:LX/0hL;

    const v1, 0x7f0207ed

    invoke-virtual {v0, v1}, LX/0hL;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2314211
    iget-object v1, p0, LX/G2Y;->a:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2314212
    invoke-virtual {p0, v0, v4, v4, v4}, LX/G2Y;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2314213
    :goto_0
    const v0, 0x7f0e0689

    invoke-virtual {p0, p1, v0}, LX/G2Y;->setTextAppearance(Landroid/content/Context;I)V

    .line 2314214
    const v0, 0x7f0e068a

    invoke-virtual {p0, p1, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    .line 2314215
    const v0, 0x7f021956

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeBackground(I)V

    .line 2314216
    sget-object v0, LX/5Oe;->AWAY_FROM_TEXT:LX/5Oe;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgePlacement(LX/5Oe;)V

    .line 2314217
    return-void

    .line 2314218
    :cond_0
    invoke-virtual {p0, v4, v4, v0, v4}, LX/G2Y;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/G2Y;

    invoke-static {p0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object p0

    check-cast p0, LX/0hL;

    iput-object p0, p1, LX/G2Y;->a:LX/0hL;

    return-void
.end method
