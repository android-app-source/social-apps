.class public LX/FY8;
.super LX/FXy;
.source ""


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field public final b:LX/BUA;

.field private final c:LX/0tQ;

.field private final d:LX/19w;

.field private final e:LX/BYZ;

.field private f:D


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;LX/BUA;LX/0tQ;LX/19w;LX/BYZ;)V
    .locals 2
    .param p1    # Landroid/support/v4/app/FragmentActivity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255813
    invoke-direct {p0}, LX/FXy;-><init>()V

    .line 2255814
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/FY8;->f:D

    .line 2255815
    iput-object p2, p0, LX/FY8;->b:LX/BUA;

    .line 2255816
    iput-object p3, p0, LX/FY8;->c:LX/0tQ;

    .line 2255817
    iput-object p4, p0, LX/FY8;->d:LX/19w;

    .line 2255818
    iput-object p5, p0, LX/FY8;->e:LX/BYZ;

    .line 2255819
    iput-object p1, p0, LX/FY8;->a:Landroid/support/v4/app/FragmentActivity;

    .line 2255820
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2255812
    const v0, 0x7f020841

    return v0
.end method

.method public final a(LX/BO1;)LX/FXy;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    .line 2255801
    invoke-interface {p1}, LX/BO1;->F()Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/FY8;->c:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, LX/BO1;->V()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    invoke-interface {p1}, LX/BO1;->U()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2255802
    iget-object v1, p0, LX/FY8;->d:LX/19w;

    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v1

    .line 2255803
    iget-object v2, v1, LX/2fs;->c:LX/1A0;

    .line 2255804
    sget-object v3, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-eq v2, v3, :cond_0

    sget-object v3, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    if-ne v2, v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2255805
    :cond_1
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255806
    sget-object v0, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-ne v2, v0, :cond_2

    invoke-interface {p1}, LX/BO1;->V()J

    move-result-wide v0

    long-to-double v0, v0

    .line 2255807
    :goto_0
    mul-double/2addr v0, v6

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    div-double/2addr v0, v6

    iput-wide v0, p0, LX/FY8;->f:D

    .line 2255808
    :goto_1
    return-object p0

    .line 2255809
    :cond_2
    iget-wide v0, v1, LX/2fs;->a:J

    long-to-double v0, v0

    goto :goto_0

    .line 2255810
    :cond_3
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255811
    goto :goto_1
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2255797
    sget-object v0, LX/FY7;->a:[I

    iget-object v1, p0, LX/FY8;->c:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2255798
    const v0, 0x7f081ad3

    new-array v1, v2, [Ljava/lang/Object;

    iget-wide v2, p0, LX/FY8;->f:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2255799
    :pswitch_0
    const v0, 0x7f081ad2

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2255800
    :pswitch_1
    const v0, 0x7f081ad4

    new-array v1, v2, [Ljava/lang/Object;

    iget-wide v2, p0, LX/FY8;->f:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2255796
    const-string v0, "video_download_button"

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2255795
    const v0, 0x7f081ad5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/BO1;)Z
    .locals 15

    .prologue
    .line 2255787
    invoke-interface/range {p1 .. p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v3

    .line 2255788
    invoke-interface/range {p1 .. p1}, LX/BO1;->G()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2255789
    new-instance v1, LX/BUL;

    const-string v4, ""

    const-string v5, "saved_dashboard"

    invoke-interface/range {p1 .. p1}, LX/BO1;->V()J

    move-result-wide v6

    invoke-interface/range {p1 .. p1}, LX/BO1;->j()Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, LX/BO1;->k()Ljava/lang/String;

    move-result-object v9

    invoke-interface/range {p1 .. p1}, LX/BO1;->m()Ljava/lang/String;

    move-result-object v10

    invoke-interface/range {p1 .. p1}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v11

    invoke-interface/range {p1 .. p1}, LX/BO1;->C()Ljava/lang/String;

    move-result-object v12

    invoke-interface/range {p1 .. p1}, LX/BO1;->D()Ljava/lang/String;

    move-result-object v13

    invoke-interface/range {p1 .. p1}, LX/BO1;->E()Ljava/lang/String;

    move-result-object v14

    invoke-direct/range {v1 .. v14}, LX/BUL;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2255790
    new-instance v4, LX/FY6;

    move-object v5, p0

    move-object v6, v2

    move-object v7, v3

    move-object/from16 v8, p1

    move-object v9, v1

    invoke-direct/range {v4 .. v9}, LX/FY6;-><init>(LX/FY8;Landroid/net/Uri;Ljava/lang/String;LX/BO1;LX/BUL;)V

    .line 2255791
    iget-object v0, p0, LX/FY8;->e:LX/BYZ;

    iget-object v2, p0, LX/FY8;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v2, v4}, LX/BYZ;->a(Landroid/app/Activity;LX/BYY;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2255792
    iget-object v0, p0, LX/FY8;->b:LX/BUA;

    invoke-virtual {v0, v1}, LX/BUA;->a(LX/BUL;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2255793
    iget-object v0, p0, LX/FY8;->b:LX/BUA;

    invoke-virtual {v0, v3}, LX/BUA;->a(Ljava/lang/String;)V

    .line 2255794
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
