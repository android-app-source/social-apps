.class public LX/FuI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/17W;

.field public final b:LX/1K9;

.field public final c:Landroid/content/Context;

.field public final d:LX/BQ1;

.field public final e:LX/G4m;

.field public final f:LX/5SB;

.field public final g:LX/BQ9;

.field public final h:LX/0gh;

.field public final i:LX/G2X;

.field public j:LX/6Vi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/6Vi",
            "<",
            "LX/G1D;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/BQ1;LX/G4m;LX/5SB;LX/BQ9;LX/0gh;LX/17W;LX/1K9;Landroid/content/Context;LX/G2X;)V
    .locals 1
    .param p1    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/BQ9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299930
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2299931
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ1;

    iput-object v0, p0, LX/FuI;->d:LX/BQ1;

    .line 2299932
    iput-object p2, p0, LX/FuI;->e:LX/G4m;

    .line 2299933
    iput-object p3, p0, LX/FuI;->f:LX/5SB;

    .line 2299934
    iput-object p4, p0, LX/FuI;->g:LX/BQ9;

    .line 2299935
    iput-object p5, p0, LX/FuI;->h:LX/0gh;

    .line 2299936
    iput-object p9, p0, LX/FuI;->i:LX/G2X;

    .line 2299937
    iput-object p6, p0, LX/FuI;->a:LX/17W;

    .line 2299938
    iput-object p7, p0, LX/FuI;->b:LX/1K9;

    .line 2299939
    iput-object p8, p0, LX/FuI;->c:Landroid/content/Context;

    .line 2299940
    return-void
.end method
