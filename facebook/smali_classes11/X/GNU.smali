.class public final LX/GNU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adpreview/activity/AdPreviewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adpreview/activity/AdPreviewActivity;)V
    .locals 0

    .prologue
    .line 2346052
    iput-object p1, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2346053
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2346054
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2346055
    new-instance v1, Lcom/facebook/ipc/feed/ViewPermalinkParams;

    invoke-direct {v1, v0}, Lcom/facebook/ipc/feed/ViewPermalinkParams;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2346056
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/facebook/ipc/feed/ViewPermalinkParams;->c:Z

    .line 2346057
    iget-object v2, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v2, v2, Lcom/facebook/adpreview/activity/AdPreviewActivity;->x:LX/0pf;

    invoke-virtual {v2, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v2

    const/4 v3, 0x1

    .line 2346058
    iput-boolean v3, v2, LX/1g0;->o:Z

    .line 2346059
    iget-object v2, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v2, v2, Lcom/facebook/adpreview/activity/AdPreviewActivity;->w:LX/0hy;

    invoke-interface {v2, v1}, LX/0hz;->a(Lcom/facebook/ipc/intent/FacebookOnlyIntentParams;)Landroid/content/Intent;

    move-result-object v1

    .line 2346060
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2346061
    iget-object v2, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v2, v2, Lcom/facebook/adpreview/activity/AdPreviewActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2346062
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 2346063
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2346064
    iget-object v1, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-boolean v1, v1, Lcom/facebook/adpreview/activity/AdPreviewActivity;->z:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2346065
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2346066
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2346067
    iget-object v1, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v1, v1, Lcom/facebook/adpreview/activity/AdPreviewActivity;->v:LX/17Y;

    iget-object v2, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2346068
    iget-object v1, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v1, v1, Lcom/facebook/adpreview/activity/AdPreviewActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2346069
    :cond_0
    iget-object v0, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v0, v0, Lcom/facebook/adpreview/activity/AdPreviewActivity;->p:LX/0Zb;

    const-string v1, "adpreview_graphql_success"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2346070
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2346071
    const-string v1, "preview_id"

    iget-object v2, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v2, v2, Lcom/facebook/adpreview/activity/AdPreviewActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2346072
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2346073
    :cond_1
    iget-object v0, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-virtual {v0}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->finish()V

    .line 2346074
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2346075
    const-string v0, "adpreview_graphql_error"

    const-string v1, "Error fetching ad preview."

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    const/4 v1, 0x1

    .line 2346076
    iput v1, v0, LX/0VK;->e:I

    .line 2346077
    move-object v0, v0

    .line 2346078
    iput-object p1, v0, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2346079
    move-object v0, v0

    .line 2346080
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 2346081
    iget-object v1, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    iget-object v1, v1, Lcom/facebook/adpreview/activity/AdPreviewActivity;->q:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 2346082
    iget-object v0, p0, LX/GNU;->a:Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-virtual {v0}, Lcom/facebook/adpreview/activity/AdPreviewActivity;->finish()V

    .line 2346083
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2346084
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/GNU;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
