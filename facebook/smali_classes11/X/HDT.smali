.class public LX/HDT;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/H8E;",
        "LX/HDS;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/HDT;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2441263
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2441264
    return-void
.end method

.method public static a(LX/0QB;)LX/HDT;
    .locals 3

    .prologue
    .line 2441251
    sget-object v0, LX/HDT;->a:LX/HDT;

    if-nez v0, :cond_1

    .line 2441252
    const-class v1, LX/HDT;

    monitor-enter v1

    .line 2441253
    :try_start_0
    sget-object v0, LX/HDT;->a:LX/HDT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2441254
    if-eqz v2, :cond_0

    .line 2441255
    :try_start_1
    new-instance v0, LX/HDT;

    invoke-direct {v0}, LX/HDT;-><init>()V

    .line 2441256
    move-object v0, v0

    .line 2441257
    sput-object v0, LX/HDT;->a:LX/HDT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2441258
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2441259
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2441260
    :cond_1
    sget-object v0, LX/HDT;->a:LX/HDT;

    return-object v0

    .line 2441261
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2441262
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
