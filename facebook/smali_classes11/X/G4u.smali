.class public final LX/G4u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G4v;


# direct methods
.method public constructor <init>(LX/G4v;)V
    .locals 0

    .prologue
    .line 2318145
    iput-object p1, p0, LX/G4u;->a:LX/G4v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2318146
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2318147
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2318148
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2318149
    if-eqz v0, :cond_0

    .line 2318150
    iget-object v1, p0, LX/G4u;->a:LX/G4v;

    .line 2318151
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2318152
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2318153
    iget-object v2, v1, LX/G4v;->a:LX/G4x;

    const/4 p0, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p0, p1}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;)LX/16n;

    move-result-object v2

    .line 2318154
    if-eqz v2, :cond_0

    .line 2318155
    iget-object p0, v1, LX/G4v;->a:LX/G4x;

    invoke-virtual {p0, v2, v0}, LX/G4x;->a(LX/16n;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 2318156
    invoke-static {v1}, LX/G4v;->e(LX/G4v;)V

    .line 2318157
    :cond_0
    return-void
.end method
