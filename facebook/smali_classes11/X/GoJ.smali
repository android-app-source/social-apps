.class public final enum LX/GoJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GoJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GoJ;

.field public static final enum SWIPE_TO_OPEN_INDICATOR:LX/GoJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2394201
    new-instance v0, LX/GoJ;

    const-string v1, "SWIPE_TO_OPEN_INDICATOR"

    invoke-direct {v0, v1, v2}, LX/GoJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GoJ;->SWIPE_TO_OPEN_INDICATOR:LX/GoJ;

    .line 2394202
    const/4 v0, 0x1

    new-array v0, v0, [LX/GoJ;

    sget-object v1, LX/GoJ;->SWIPE_TO_OPEN_INDICATOR:LX/GoJ;

    aput-object v1, v0, v2

    sput-object v0, LX/GoJ;->$VALUES:[LX/GoJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2394198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GoJ;
    .locals 1

    .prologue
    .line 2394199
    const-class v0, LX/GoJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GoJ;

    return-object v0
.end method

.method public static values()[LX/GoJ;
    .locals 1

    .prologue
    .line 2394200
    sget-object v0, LX/GoJ;->$VALUES:[LX/GoJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GoJ;

    return-object v0
.end method
