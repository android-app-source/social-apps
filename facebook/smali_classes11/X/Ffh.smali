.class public LX/Ffh;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Ffh;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268764
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-short v0, LX/100;->aM:S

    const/4 v2, 0x0

    invoke-interface {p2, v0, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f08231a

    :goto_0
    invoke-direct {p0, p1, v1, v0}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268765
    iput-object p2, p0, LX/Ffh;->a:LX/0ad;

    .line 2268766
    return-void

    .line 2268767
    :cond_0
    const v0, 0x7f082319

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Ffh;
    .locals 5

    .prologue
    .line 2268768
    sget-object v0, LX/Ffh;->b:LX/Ffh;

    if-nez v0, :cond_1

    .line 2268769
    const-class v1, LX/Ffh;

    monitor-enter v1

    .line 2268770
    :try_start_0
    sget-object v0, LX/Ffh;->b:LX/Ffh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268771
    if-eqz v2, :cond_0

    .line 2268772
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268773
    new-instance p0, LX/Ffh;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/Ffh;-><init>(Landroid/content/res/Resources;LX/0ad;)V

    .line 2268774
    move-object v0, p0

    .line 2268775
    sput-object v0, LX/Ffh;->b:LX/Ffh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268776
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268777
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268778
    :cond_1
    sget-object v0, LX/Ffh;->b:LX/Ffh;

    return-object v0

    .line 2268779
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;
    .locals 1

    .prologue
    .line 2268781
    iget-object v0, p0, LX/Ffh;->a:LX/0ad;

    invoke-static {v0, p1}, LX/1nE;->a(LX/0ad;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "graph_search_results_page_blended"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsFragment;

    move-result-object v0

    .line 2268782
    :goto_0
    return-object v0

    .line 2268783
    :cond_0
    const-string v0, "graph_search_results_page_blended"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->b(Ljava/lang/String;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/Fdv;
    .locals 1

    .prologue
    .line 2268784
    const-string v0, ""

    invoke-virtual {p0, v0}, LX/Ffh;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    move-result-object v0

    return-object v0
.end method
