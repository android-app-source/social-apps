.class public final LX/GzX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/GzZ;

.field public final synthetic c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Px;LX/GzZ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2412110
    iput-object p1, p0, LX/GzX;->a:LX/0Px;

    iput-object p2, p0, LX/GzX;->b:LX/GzZ;

    iput-object p3, p0, LX/GzX;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 2412111
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2412112
    iget-object v0, p0, LX/GzX;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 2412113
    iget-object v0, p0, LX/GzX;->b:LX/GzZ;

    invoke-interface {v0, p2}, LX/GzZ;->a(I)V

    .line 2412114
    :goto_0
    return-void

    .line 2412115
    :cond_0
    iget-object v0, p0, LX/GzX;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2412116
    iget-object v0, p0, LX/GzX;->b:LX/GzZ;

    invoke-interface {v0}, LX/GzZ;->a()V

    goto :goto_0

    .line 2412117
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
