.class public LX/H1u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/H1u;


# instance fields
.field private a:LX/0re;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0re",
            "<",
            "LX/H1t;",
            "LX/H29;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0rd;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2416060
    const/16 v0, 0x64

    invoke-direct {p0, p1, v0}, LX/H1u;-><init>(LX/0rd;I)V

    .line 2416061
    return-void
.end method

.method private constructor <init>(LX/0rd;I)V
    .locals 1

    .prologue
    .line 2416057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416058
    const-string v0, "tilesCache"

    invoke-virtual {p1, p2, v0}, LX/0rd;->b(ILjava/lang/String;)LX/0re;

    move-result-object v0

    iput-object v0, p0, LX/H1u;->a:LX/0re;

    .line 2416059
    return-void
.end method

.method public static a(LX/0QB;)LX/H1u;
    .locals 4

    .prologue
    .line 2416044
    sget-object v0, LX/H1u;->c:LX/H1u;

    if-nez v0, :cond_1

    .line 2416045
    const-class v1, LX/H1u;

    monitor-enter v1

    .line 2416046
    :try_start_0
    sget-object v0, LX/H1u;->c:LX/H1u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2416047
    if-eqz v2, :cond_0

    .line 2416048
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2416049
    new-instance p0, LX/H1u;

    invoke-static {v0}, LX/0rZ;->a(LX/0QB;)LX/0rd;

    move-result-object v3

    check-cast v3, LX/0rd;

    invoke-direct {p0, v3}, LX/H1u;-><init>(LX/0rd;)V

    .line 2416050
    move-object v0, p0

    .line 2416051
    sput-object v0, LX/H1u;->c:LX/H1u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2416052
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2416053
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2416054
    :cond_1
    sget-object v0, LX/H1u;->c:LX/H1u;

    return-object v0

    .line 2416055
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2416056
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 2

    .prologue
    .line 2416022
    iget v0, p0, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 2416041
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/H1u;->a:LX/0re;

    invoke-virtual {v0}, LX/0re;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2416042
    monitor-exit p0

    return-void

    .line 2416043
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(DLjava/util/Set;Ljava/util/Set;Landroid/graphics/RectF;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/graphics/RectF;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/MapTile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2416032
    monitor-enter p0

    :try_start_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2416033
    iget-object v2, p0, LX/H1u;->a:LX/0re;

    invoke-virtual {v2}, LX/0re;->c()Ljava/util/Map;

    move-result-object v2

    .line 2416034
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/H1t;

    .line 2416035
    iget-object v5, v2, LX/H1t;->b:Ljava/util/Set;

    move-object/from16 v0, p4

    invoke-interface {v5, v0}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v2, LX/H1t;->a:Ljava/util/Set;

    move-object/from16 v0, p3

    invoke-interface {v5, v0}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2416036
    new-instance v5, Landroid/graphics/RectF;

    iget-object v6, v2, LX/H1t;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v6

    double-to-float v6, v6

    iget-object v7, v2, LX/H1t;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v8

    double-to-float v7, v8

    iget-object v8, v2, LX/H1t;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v8

    double-to-float v8, v8

    iget-object v9, v2, LX/H1t;->d:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v10

    double-to-float v9, v10

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2416037
    iget v6, v2, LX/H1t;->f:F

    float-to-double v6, v6

    cmpg-double v6, v6, p1

    if-gez v6, :cond_0

    iget v6, v2, LX/H1t;->e:F

    float-to-double v6, v6

    cmpl-double v6, v6, p1

    if-lez v6, :cond_0

    move-object/from16 v0, p5

    invoke-static {v0, v5}, LX/H1u;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2416038
    iget-object v5, p0, LX/H1u;->a:LX/0re;

    invoke-virtual {v5, v2}, LX/0re;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/H29;

    invoke-virtual {v2}, LX/H29;->a()Lcom/facebook/nearby/model/MapTile;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2416039
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 2416040
    :cond_1
    monitor-exit p0

    return-object v3
.end method

.method public final declared-synchronized a(Ljava/util/Set;Ljava/util/Set;Ljava/util/List;Ljava/lang/String;J)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/MapTile;",
            ">;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 2416025
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LX/H1u;->b:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2416026
    invoke-direct {p0}, LX/H1u;->c()V

    .line 2416027
    move-object/from16 v0, p4

    iput-object v0, p0, LX/H1u;->b:Ljava/lang/String;

    .line 2416028
    :cond_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/facebook/nearby/model/MapTile;

    move-object v9, v0

    .line 2416029
    iget-object v11, p0, LX/H1u;->a:LX/0re;

    new-instance v2, LX/H1t;

    iget-object v5, v9, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    iget-object v6, v9, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iget v7, v9, Lcom/facebook/nearby/model/MapTile;->minZoom:F

    iget v8, v9, Lcom/facebook/nearby/model/MapTile;->maxZoom:F

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v2 .. v8}, LX/H1t;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGeoRectangle;FF)V

    new-instance v4, LX/H29;

    move-object v5, v9

    move-wide/from16 v6, p5

    move-object v8, p1

    move-object v9, p2

    invoke-direct/range {v4 .. v9}, LX/H29;-><init>(Lcom/facebook/nearby/model/MapTile;JLjava/util/Set;Ljava/util/Set;)V

    invoke-virtual {v11, v2, v4}, LX/0re;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2416030
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 2416031
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 2416024
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/H1u;->a:LX/0re;

    invoke-virtual {v0}, LX/0re;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2416023
    iget-object v0, p0, LX/H1u;->b:Ljava/lang/String;

    return-object v0
.end method
