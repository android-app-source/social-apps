.class public final LX/F2i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DZN;


# instance fields
.field public final synthetic a:LX/F2r;


# direct methods
.method public constructor <init>(LX/F2r;)V
    .locals 0

    .prologue
    .line 2193271
    iput-object p1, p0, LX/F2i;->a:LX/F2r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2193270
    iget-object v0, p0, LX/F2i;->a:LX/F2r;

    iget-object v0, v0, LX/F2r;->b:Landroid/content/res/Resources;

    const v1, 0x7f0831ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 2193261
    iget-object v0, p0, LX/F2i;->a:LX/F2r;

    iget-object v0, v0, LX/F2r;->a:LX/F2L;

    iget-object v1, p0, LX/F2i;->a:LX/F2r;

    iget-object v1, v1, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/F2i;->a:LX/F2r;

    iget-object v2, v2, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->A()Z

    move-result v2

    .line 2193262
    invoke-static {v0, p1}, LX/F2L;->a$redex0(LX/F2L;Z)V

    .line 2193263
    new-instance p0, LX/4Fk;

    invoke-direct {p0}, LX/4Fk;-><init>()V

    if-eqz p1, :cond_0

    const-string v3, "ADMIN_ONLY"

    .line 2193264
    :goto_0
    const-string p1, "post_approval_setting"

    invoke-virtual {p0, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2193265
    move-object v3, p0

    .line 2193266
    new-instance p0, LX/F2J;

    invoke-direct {p0, v0, v2}, LX/F2J;-><init>(LX/F2L;Z)V

    .line 2193267
    invoke-static {v0, v1, v3, p0}, LX/F2L;->a(LX/F2L;Ljava/lang/String;LX/4Fk;LX/0TF;)V

    .line 2193268
    return-void

    .line 2193269
    :cond_0
    const-string v3, "NONE"

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2193260
    iget-object v0, p0, LX/F2i;->a:LX/F2r;

    iget-object v0, v0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->A()Z

    move-result v0

    return v0
.end method
