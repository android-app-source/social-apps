.class public LX/GW6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/commerce/productdetails/api/CheckoutParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2360378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2360379
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2360385
    check-cast p1, Lcom/facebook/commerce/productdetails/api/CheckoutParams;

    .line 2360386
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2360387
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "item_id"

    iget-object v2, p1, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2360388
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "quantity"

    iget v2, p1, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2360389
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "ref_id"

    iget-wide v2, p1, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2360390
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "ref_type"

    iget-object v2, p1, Lcom/facebook/commerce/productdetails/api/CheckoutParams;->d:LX/7iP;

    invoke-static {v2}, LX/7iR;->a(LX/7iP;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2360391
    new-instance v0, LX/14N;

    const-string v1, "checkout_item"

    const-string v2, "POST"

    const-string v3, "me/commerce_cart_items"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2360380
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2360381
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2360382
    const-string v1, "checkout_path"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2360383
    const-string v1, "checkout_path"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2360384
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
