.class public LX/FKr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FKr;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225617
    return-void
.end method

.method public static a(LX/0QB;)LX/FKr;
    .locals 3

    .prologue
    .line 2225618
    sget-object v0, LX/FKr;->a:LX/FKr;

    if-nez v0, :cond_1

    .line 2225619
    const-class v1, LX/FKr;

    monitor-enter v1

    .line 2225620
    :try_start_0
    sget-object v0, LX/FKr;->a:LX/FKr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2225621
    if-eqz v2, :cond_0

    .line 2225622
    :try_start_1
    new-instance v0, LX/FKr;

    invoke-direct {v0}, LX/FKr;-><init>()V

    .line 2225623
    move-object v0, v0

    .line 2225624
    sput-object v0, LX/FKr;->a:LX/FKr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2225625
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2225626
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2225627
    :cond_1
    sget-object v0, LX/FKr;->a:LX/FKr;

    return-object v0

    .line 2225628
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2225629
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2225630
    check-cast p1, Ljava/lang/String;

    .line 2225631
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2225632
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225633
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "event_info"

    const-string v2, "device_received"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225634
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "event_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225635
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "trace_info"

    invoke-direct {v0, v1, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225636
    new-instance v0, LX/14N;

    const-string v1, "pushTraceInfoConfirmation"

    const-string v2, "POST"

    const-string v3, "method/user.tracePush"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225614
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225615
    const/4 v0, 0x0

    return-object v0
.end method
