.class public final LX/GI6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GCE;

.field public final synthetic b:LX/GIA;


# direct methods
.method public constructor <init>(LX/GIA;LX/GCE;)V
    .locals 0

    .prologue
    .line 2335726
    iput-object p1, p0, LX/GI6;->b:LX/GIA;

    iput-object p2, p0, LX/GI6;->a:LX/GCE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 2335727
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2335728
    iget-object v0, p0, LX/GI6;->b:LX/GIA;

    iget-object v0, v0, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v1, p0, LX/GI6;->b:LX/GIA;

    iget-object v1, v1, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v2

    .line 2335729
    const-string v0, "payments_flow_context_key"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2335730
    iget-object v1, p0, LX/GI6;->a:LX/GCE;

    .line 2335731
    iput-object v0, v1, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2335732
    const-string v1, "country"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/locale/Country;

    .line 2335733
    sget-object v1, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    if-ne v4, v1, :cond_1

    move v1, v3

    :goto_0
    const-string v6, "This handler should never get called unless country is BR. But it is %s"

    new-array v7, v3, [Ljava/lang/Object;

    if-nez v4, :cond_2

    const/4 v3, 0x0

    :goto_1
    aput-object v3, v7, v5

    invoke-static {v1, v6, v7}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2335734
    iget-object v1, p0, LX/GI6;->a:LX/GCE;

    iget-object v3, p0, LX/GI6;->b:LX/GIA;

    iget-object v3, v3, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static/range {v0 .. v5}, LX/GNI;->a(Lcom/facebook/adspayments/analytics/PaymentsFlowContext;LX/GCE;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Landroid/content/Context;Lcom/facebook/common/locale/Country;Z)V

    .line 2335735
    :cond_0
    return-void

    :cond_1
    move v1, v5

    .line 2335736
    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method
