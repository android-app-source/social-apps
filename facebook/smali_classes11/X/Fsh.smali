.class public final LX/Fsh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLUser;",
        ">;",
        "LX/FsL;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fsj;


# direct methods
.method public constructor <init>(LX/Fsj;)V
    .locals 0

    .prologue
    .line 2297468
    iput-object p1, p0, LX/Fsh;->a:LX/Fsj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2297457
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2297458
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297459
    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2297460
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297461
    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2297462
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected a non-null and non-empty result"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2297463
    :cond_1
    new-instance v1, LX/FsL;

    .line 2297464
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297465
    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v0

    .line 2297466
    iget-object v2, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 2297467
    invoke-direct {v1, v0, v2}, LX/FsL;-><init>(Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;LX/0ta;)V

    return-object v1
.end method
