.class public final LX/FGg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7yp;


# instance fields
.field public final synthetic a:LX/FHj;

.field public final synthetic b:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/FGi;


# direct methods
.method public constructor <init>(LX/FGi;LX/FHj;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2219273
    iput-object p1, p0, LX/FGg;->d:LX/FGi;

    iput-object p2, p0, LX/FGg;->a:LX/FHj;

    iput-object p3, p0, LX/FGg;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p4, p0, LX/FGg;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2219271
    iget-object v0, p0, LX/FGg;->a:LX/FHj;

    sget-object v1, LX/FHi;->UPLOAD_STARTED:LX/FHi;

    invoke-virtual {v0, v1}, LX/FHj;->a(LX/FHi;)V

    .line 2219272
    return-void
.end method

.method public final a(F)V
    .locals 4

    .prologue
    .line 2219274
    iget-object v0, p0, LX/FGg;->d:LX/FGi;

    iget-object v0, v0, LX/FGi;->j:LX/0Xl;

    iget-object v1, p0, LX/FGg;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    float-to-double v2, p1

    invoke-static {v1, v2, v3}, LX/FGn;->b(Lcom/facebook/ui/media/attachments/MediaResource;D)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2219275
    return-void
.end method

.method public final a(LX/7zB;)V
    .locals 3

    .prologue
    .line 2219268
    iget-object v1, p0, LX/FGg;->a:LX/FHj;

    invoke-virtual {p1}, LX/7zB;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, LX/7zB;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/7zB;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/FHj;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2219269
    return-void

    .line 2219270
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(LX/7zL;)V
    .locals 2

    .prologue
    .line 2219263
    iget-object v0, p0, LX/FGg;->a:LX/FHj;

    sget-object v1, LX/FHi;->VIDEO_UPLOAD_COMPLETED:LX/FHi;

    invoke-virtual {v0, v1}, LX/FHj;->a(LX/FHi;)V

    .line 2219264
    iget-object v0, p0, LX/FGg;->d:LX/FGi;

    iget-object v0, v0, LX/FGi;->o:Ljava/util/Map;

    iget-object v1, p0, LX/FGg;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219265
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2219266
    iget-object v0, p0, LX/FGg;->a:LX/FHj;

    sget-object v1, LX/FHi;->USER_CANCELLED:LX/FHi;

    invoke-virtual {v0, v1}, LX/FHj;->a(LX/FHi;)V

    .line 2219267
    return-void
.end method
