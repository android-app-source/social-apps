.class public LX/G72;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/63p;


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2320979
    const-string v0, "com.facebook.browser.lite.BrowserLiteActivity"

    const-string v1, "com.facebook.browser.lite.BrowserLiteFallbackActivity"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G72;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2320980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2320981
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/G72;->b:Ljava/lang/String;

    .line 2320982
    iput-object p2, p0, LX/G72;->c:LX/03V;

    .line 2320983
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)LX/03R;
    .locals 3

    .prologue
    .line 2320984
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 2320985
    if-eqz v0, :cond_0

    sget-object v1, LX/G72;->a:LX/0Px;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2320986
    sget-object v0, LX/03R;->NO:LX/03R;

    .line 2320987
    :goto_0
    return-object v0

    .line 2320988
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, LX/G72;->b:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2320989
    iget-object v0, p0, LX/G72;->c:LX/03V;

    const-string v1, "fix:shall_start_internal_activity_instead"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2320990
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 2320991
    :cond_1
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method
