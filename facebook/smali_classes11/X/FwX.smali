.class public final LX/FwX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Landroid/net/Uri;

.field public final synthetic e:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

.field public final synthetic f:LX/FwY;


# direct methods
.method public constructor <init>(LX/FwY;ZZLjava/lang/String;Landroid/net/Uri;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;)V
    .locals 0

    .prologue
    .line 2303253
    iput-object p1, p0, LX/FwX;->f:LX/FwY;

    iput-boolean p2, p0, LX/FwX;->a:Z

    iput-boolean p3, p0, LX/FwX;->b:Z

    iput-object p4, p0, LX/FwX;->c:Ljava/lang/String;

    iput-object p5, p0, LX/FwX;->d:Landroid/net/Uri;

    iput-object p6, p0, LX/FwX;->e:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x17ba7a39

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2303254
    iget-boolean v0, p0, LX/FwX;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/FwX;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FwX;->f:LX/FwY;

    iget-object v0, v0, LX/FwY;->b:LX/0Uh;

    const/16 v1, 0x42e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2303255
    const-string v6, "app"

    .line 2303256
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v0

    iget-object v1, p0, LX/FwX;->c:Ljava/lang/String;

    .line 2303257
    iput-object v1, v0, LX/47H;->a:Ljava/lang/String;

    .line 2303258
    move-object v0, v0

    .line 2303259
    const/4 v1, 0x0

    .line 2303260
    iput-object v1, v0, LX/47H;->d:LX/47G;

    .line 2303261
    move-object v0, v0

    .line 2303262
    invoke-virtual {v0}, LX/47H;->a()LX/47I;

    move-result-object v0

    .line 2303263
    iget-object v1, p0, LX/FwX;->f:LX/FwY;

    iget-object v1, v1, LX/FwY;->a:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/17W;->a(Landroid/content/Context;LX/47I;)Z

    .line 2303264
    :goto_0
    iget-object v0, p0, LX/FwX;->f:LX/FwY;

    iget-object v0, v0, LX/FwY;->g:LX/9lQ;

    if-nez v0, :cond_1

    .line 2303265
    const v0, 0x6fff60a6

    invoke-static {v0, v7}, LX/02F;->a(II)V

    .line 2303266
    :goto_1
    return-void

    .line 2303267
    :cond_0
    const-string v6, "web"

    .line 2303268
    iget-object v0, p0, LX/FwX;->f:LX/FwY;

    iget-object v0, v0, LX/FwY;->d:Lcom/facebook/content/SecureContextHelper;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, LX/FwX;->d:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2303269
    :cond_1
    iget-boolean v0, p0, LX/FwX;->a:Z

    if-eqz v0, :cond_2

    .line 2303270
    iget-object v0, p0, LX/FwX;->f:LX/FwY;

    iget-object v0, v0, LX/FwY;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BQ9;

    iget-object v0, p0, LX/FwX;->f:LX/FwY;

    iget-wide v2, v0, LX/FwY;->f:J

    iget-object v0, p0, LX/FwX;->f:LX/FwY;

    iget-object v4, v0, LX/FwY;->g:LX/9lQ;

    iget-object v0, p0, LX/FwX;->e:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v1 .. v6}, LX/BQ9;->a(JLX/9lQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 2303271
    :cond_2
    iget-object v0, p0, LX/FwX;->f:LX/FwY;

    iget-object v0, v0, LX/FwY;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    iget-object v1, p0, LX/FwX;->f:LX/FwY;

    iget-wide v1, v1, LX/FwY;->f:J

    iget-object v3, p0, LX/FwX;->f:LX/FwY;

    iget-object v3, v3, LX/FwY;->g:LX/9lQ;

    iget-object v4, p0, LX/FwX;->e:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/FwX;->e:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, LX/BQ9;->a(JLX/9lQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2303272
    const v0, 0x237dff49

    invoke-static {v0, v7}, LX/02F;->a(II)V

    goto :goto_1
.end method
