.class public final LX/H4Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2422744
    iput-object p1, p0, LX/H4Q;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2422745
    iget-object v0, p0, LX/H4Q;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    iget-object v1, p0, LX/H4Q;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->setTextColor(I)V

    .line 2422746
    iget-object v0, p0, LX/H4Q;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    const/4 v1, 0x0

    .line 2422747
    iput-boolean v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2422748
    iget-object v0, p0, LX/H4Q;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422749
    iput-object v3, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2422750
    iget-object v0, p0, LX/H4Q;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->f:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2422751
    iput-object v3, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2422752
    iget-object v0, p0, LX/H4Q;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    .line 2422753
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2422743
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2422742
    return-void
.end method
