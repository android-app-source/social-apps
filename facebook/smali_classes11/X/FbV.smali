.class public LX/FbV;
.super LX/FbF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbF",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FbV;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260365
    invoke-direct {p0}, LX/FbF;-><init>()V

    .line 2260366
    return-void
.end method

.method public static a(LX/0QB;)LX/FbV;
    .locals 3

    .prologue
    .line 2260367
    sget-object v0, LX/FbV;->a:LX/FbV;

    if-nez v0, :cond_1

    .line 2260368
    const-class v1, LX/FbV;

    monitor-enter v1

    .line 2260369
    :try_start_0
    sget-object v0, LX/FbV;->a:LX/FbV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260370
    if-eqz v2, :cond_0

    .line 2260371
    :try_start_1
    new-instance v0, LX/FbV;

    invoke-direct {v0}, LX/FbV;-><init>()V

    .line 2260372
    move-object v0, v0

    .line 2260373
    sput-object v0, LX/FbV;->a:LX/FbV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260374
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260375
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260376
    :cond_1
    sget-object v0, LX/FbV;->a:LX/FbV;

    return-object v0

    .line 2260377
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260378
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260379
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2260380
    if-eqz v1, :cond_0

    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    invoke-direct {v0, v1}, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;-><init>(Lcom/facebook/graphql/model/GraphQLNode;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260381
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    .line 2260382
    if-eqz p1, :cond_0

    .line 2260383
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->g:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 2260384
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260385
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    .line 2260386
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2260387
    return-object v0
.end method
