.class public LX/HAj;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/CSL;

.field private final b:LX/HAp;

.field public final c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;LX/CSL;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .param p2    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;",
            ">;",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2435856
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2435857
    iput-object p3, p0, LX/HAj;->a:LX/CSL;

    .line 2435858
    new-instance v0, LX/HAp;

    invoke-direct {v0}, LX/HAp;-><init>()V

    iput-object v0, p0, LX/HAj;->b:LX/HAp;

    .line 2435859
    iput-object p4, p0, LX/HAj;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2435860
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2435861
    if-eqz p2, :cond_2

    .line 2435862
    check-cast p2, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;

    .line 2435863
    :goto_0
    invoke-virtual {p0, p1}, LX/HAj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    .line 2435864
    iget-object v1, p0, LX/HAj;->b:LX/HAp;

    .line 2435865
    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/HAp;->a:Ljava/lang/String;

    .line 2435866
    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 p1, 0x1

    const/4 v5, 0x0

    .line 2435867
    if-nez v2, :cond_3

    .line 2435868
    const-string v4, ""

    .line 2435869
    :goto_1
    move-object v2, v4

    .line 2435870
    iput-object v2, v1, LX/HAp;->b:Ljava/lang/String;

    .line 2435871
    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2435872
    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, v1, LX/HAp;->c:Landroid/net/Uri;

    .line 2435873
    :cond_0
    iget-object v1, p0, LX/HAj;->b:LX/HAp;

    .line 2435874
    iget-object v2, v1, LX/HAp;->c:Landroid/net/Uri;

    move-object v2, v2

    .line 2435875
    iget-object v3, p2, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v4, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2435876
    iget-object v2, v1, LX/HAp;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2435877
    iget-object v3, p2, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2435878
    iget-object v2, v1, LX/HAp;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2435879
    if-eqz v2, :cond_1

    .line 2435880
    iget-object v3, p2, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->d:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2435881
    :cond_1
    new-instance v1, LX/HAi;

    invoke-direct {v1, p0, v0, p3}, LX/HAi;-><init>(LX/HAj;Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;Landroid/view/ViewGroup;)V

    invoke-virtual {p2, v1}, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2435882
    return-object p2

    .line 2435883
    :cond_2
    new-instance p2, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;

    invoke-virtual {p0}, LX/HAj;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/facebook/pages/common/childlocations/PageChildLocationsRowView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2435884
    :cond_3
    invoke-virtual {v3, v2, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2435885
    invoke-virtual {v3, v2, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 2435886
    :cond_4
    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2435887
    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 2435888
    :cond_5
    const-string v4, ""

    goto :goto_1
.end method
