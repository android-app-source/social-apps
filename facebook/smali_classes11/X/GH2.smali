.class public final LX/GH2;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:LX/GCY;

.field public final synthetic c:LX/GH3;


# direct methods
.method public constructor <init>(LX/GH3;Landroid/content/Intent;LX/GCY;)V
    .locals 0

    .prologue
    .line 2334667
    iput-object p1, p0, LX/GH2;->c:LX/GH3;

    iput-object p2, p0, LX/GH2;->a:Landroid/content/Intent;

    iput-object p3, p0, LX/GH2;->b:LX/GCY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2334668
    iget-object v0, p0, LX/GH2;->c:LX/GH3;

    iget-object v0, v0, LX/GH3;->m:LX/2U3;

    const-class v1, LX/GH3;

    const-string v2, "Fetch Insights failed"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2334669
    iget-object v0, p0, LX/GH2;->b:LX/GCY;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334670
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2334671
    check-cast p1, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2334672
    iget-object v0, p0, LX/GH2;->a:Landroid/content/Intent;

    invoke-interface {p1, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Landroid/content/Intent;)V

    .line 2334673
    iget-object v0, p0, LX/GH2;->b:LX/GCY;

    invoke-interface {v0, p1}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334674
    return-void
.end method
