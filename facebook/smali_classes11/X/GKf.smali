.class public final LX/GKf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/GKj;


# direct methods
.method public constructor <init>(LX/GKj;)V
    .locals 0

    .prologue
    .line 2341230
    iput-object p1, p0, LX/GKf;->a:LX/GKj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 2341231
    if-nez p2, :cond_1

    .line 2341232
    iget-object v0, p0, LX/GKf;->a:LX/GKj;

    invoke-static {v0}, LX/GKj;->h(LX/GKj;)V

    .line 2341233
    iget-object v0, p0, LX/GKf;->a:LX/GKj;

    invoke-static {v0}, LX/GKj;->j(LX/GKj;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/GKf;->a:LX/GKj;

    invoke-static {v0}, LX/GKj;->j(LX/GKj;)I

    move-result v0

    .line 2341234
    :goto_0
    iget-object v1, p0, LX/GKf;->a:LX/GKj;

    iget-object v1, v1, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    iget-object v2, p0, LX/GKf;->a:LX/GKj;

    invoke-static {v2, v0}, LX/GKj;->a$redex0(LX/GKj;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setBidAmount(Ljava/lang/CharSequence;)V

    .line 2341235
    iget-object v1, p0, LX/GKf;->a:LX/GKj;

    iget-object v1, v1, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341236
    iput v0, v1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    .line 2341237
    :goto_1
    return-void

    .line 2341238
    :cond_0
    iget-object v0, p0, LX/GKf;->a:LX/GKj;

    iget v0, v0, LX/GKj;->h:I

    goto :goto_0

    .line 2341239
    :cond_1
    iget-object v0, p0, LX/GKf;->a:LX/GKj;

    const/4 p0, 0x1

    .line 2341240
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 2341241
    iget-object v1, v0, LX/GKj;->b:Landroid/view/inputmethod/InputMethodManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p0}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 2341242
    iget-object v1, v0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    invoke-virtual {v1, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setCursorVisible(Z)V

    .line 2341243
    goto :goto_1
.end method
