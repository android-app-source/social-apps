.class public LX/F2r;
.super LX/1Cv;
.source ""


# instance fields
.field public a:LX/F2L;

.field public b:Landroid/content/res/Resources;

.field public c:LX/F2N;

.field public d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/11T;

.field public final f:LX/F4L;

.field public final g:LX/F4Q;

.field public h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

.field private i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/DZN;

.field public k:LX/DZN;

.field public l:LX/DZN;

.field public m:Landroid/view/View$OnClickListener;

.field public n:Landroid/view/View$OnClickListener;

.field public o:LX/F3E;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public final r:LX/F2f;


# direct methods
.method public constructor <init>(LX/F3E;LX/F2L;Landroid/content/res/Resources;LX/F2N;Lcom/facebook/content/SecureContextHelper;LX/11T;LX/F4L;LX/F4Q;)V
    .locals 1
    .param p1    # LX/F3E;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2193326
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2193327
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193328
    iput-object v0, p0, LX/F2r;->i:LX/0Px;

    .line 2193329
    new-instance v0, LX/F2f;

    invoke-direct {v0, p0}, LX/F2f;-><init>(LX/F2r;)V

    iput-object v0, p0, LX/F2r;->r:LX/F2f;

    .line 2193330
    iput-object p1, p0, LX/F2r;->o:LX/F3E;

    .line 2193331
    iput-object p2, p0, LX/F2r;->a:LX/F2L;

    .line 2193332
    iput-object p3, p0, LX/F2r;->b:Landroid/content/res/Resources;

    .line 2193333
    iput-object p4, p0, LX/F2r;->c:LX/F2N;

    .line 2193334
    iput-object p5, p0, LX/F2r;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2193335
    iput-object p6, p0, LX/F2r;->e:LX/11T;

    .line 2193336
    iput-object p7, p0, LX/F2r;->f:LX/F4L;

    .line 2193337
    iput-object p8, p0, LX/F2r;->g:LX/F4Q;

    .line 2193338
    new-instance v0, LX/F2g;

    invoke-direct {v0, p0}, LX/F2g;-><init>(LX/F2r;)V

    iput-object v0, p0, LX/F2r;->j:LX/DZN;

    .line 2193339
    new-instance v0, LX/F2h;

    invoke-direct {v0, p0}, LX/F2h;-><init>(LX/F2r;)V

    iput-object v0, p0, LX/F2r;->k:LX/DZN;

    .line 2193340
    new-instance v0, LX/F2i;

    invoke-direct {v0, p0}, LX/F2i;-><init>(LX/F2r;)V

    iput-object v0, p0, LX/F2r;->l:LX/DZN;

    .line 2193341
    new-instance v0, LX/F2j;

    invoke-direct {v0, p0}, LX/F2j;-><init>(LX/F2r;)V

    iput-object v0, p0, LX/F2r;->m:Landroid/view/View$OnClickListener;

    .line 2193342
    new-instance v0, LX/F2k;

    invoke-direct {v0, p0}, LX/F2k;-><init>(LX/F2r;)V

    iput-object v0, p0, LX/F2r;->n:Landroid/view/View$OnClickListener;

    .line 2193343
    return-void
.end method

.method public static a(LX/F2r;LX/0Pz;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2193483
    iget-object v0, p0, LX/F2r;->g:LX/F4Q;

    const/4 v1, 0x0

    .line 2193484
    packed-switch p2, :pswitch_data_0

    .line 2193485
    :goto_0
    :pswitch_0
    move-object v0, v1

    .line 2193486
    if-eqz v0, :cond_0

    .line 2193487
    invoke-virtual {p1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2193488
    :cond_0
    return-void

    .line 2193489
    :pswitch_1
    iget-object v1, v0, LX/F4Q;->a:Landroid/content/res/Resources;

    const p0, 0x7f083190

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/F4Q;->a(LX/F4Q;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    goto :goto_0

    .line 2193490
    :pswitch_2
    iget-object v1, v0, LX/F4Q;->a:Landroid/content/res/Resources;

    const p0, 0x7f08318f

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/F4Q;->a(LX/F4Q;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    goto :goto_0

    .line 2193491
    :pswitch_3
    iget-object v1, v0, LX/F4Q;->a:Landroid/content/res/Resources;

    const p0, 0x7f083191

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/F4Q;->a(LX/F4Q;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/F2r;Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)V
    .locals 1

    .prologue
    .line 2193478
    iput-object p1, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193479
    iget-object v0, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/F2r;->q:Ljava/lang/String;

    .line 2193480
    iget-object v0, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/F2r;->p:Ljava/lang/String;

    .line 2193481
    invoke-direct {p0}, LX/F2r;->e()V

    .line 2193482
    return-void
.end method

.method private static c(LX/F2r;LX/0Pz;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 2193456
    invoke-static {p0, p1, v3}, LX/F2r;->a(LX/F2r;LX/0Pz;I)V

    .line 2193457
    invoke-static {p0, p1}, LX/F2r;->e(LX/F2r;LX/0Pz;)V

    .line 2193458
    iget-object v0, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    if-gt v0, v3, :cond_2

    .line 2193459
    iget-object v0, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->ADMIN_ONLY:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/F2r;->b:Landroid/content/res/Resources;

    const v1, 0x7f0831ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2193460
    :goto_0
    iget-object v1, p0, LX/F2r;->g:LX/F4Q;

    iget-object v2, p0, LX/F2r;->j:LX/DZN;

    .line 2193461
    new-instance v3, LX/F4O;

    sget-object v4, LX/F2y;->c:LX/DML;

    invoke-direct {v3, v1, v4, v2, v0}, LX/F4O;-><init>(LX/F4Q;LX/DML;LX/DZN;Ljava/lang/String;)V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    move-object v0, v3

    .line 2193462
    if-eqz v0, :cond_0

    .line 2193463
    invoke-virtual {p1, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2193464
    :cond_0
    :goto_1
    return-void

    .line 2193465
    :cond_1
    iget-object v0, p0, LX/F2r;->b:Landroid/content/res/Resources;

    const v1, 0x7f0831a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2193466
    :cond_2
    iget-object v0, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x1

    const v4, 0x264ce754

    invoke-static {v2, v0, v3, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2193467
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2193468
    :goto_2
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v2

    .line 2193469
    :goto_3
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 2193470
    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2193471
    new-instance v5, LX/F2a;

    sget-object v6, LX/F2y;->d:LX/DML;

    invoke-direct {v5, p0, v6, v4, v3}, LX/F2a;-><init>(LX/F2r;LX/DML;LX/15i;I)V

    invoke-virtual {p1, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193472
    add-int/lit8 v3, v2, -0x1

    if-eq v1, v3, :cond_3

    .line 2193473
    invoke-static {p0, p1}, LX/F2r;->f(LX/F2r;LX/0Pz;)V

    .line 2193474
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2193475
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2

    .line 2193476
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2193477
    :cond_5
    invoke-static {p0, p1}, LX/F2r;->e(LX/F2r;LX/0Pz;)V

    goto :goto_1
.end method

.method private static d(LX/F2r;LX/0Pz;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2193437
    const/4 v0, 0x3

    invoke-static {p0, p1, v0}, LX/F2r;->a(LX/F2r;LX/0Pz;I)V

    .line 2193438
    iget-object v0, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->v()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    const/4 v2, 0x2

    if-gt v0, v2, :cond_0

    .line 2193439
    new-instance v0, LX/F2b;

    sget-object v1, LX/F2y;->b:LX/DML;

    invoke-direct {v0, p0, v1}, LX/F2b;-><init>(LX/F2r;LX/DML;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193440
    :goto_0
    invoke-static {p0, p1}, LX/F2r;->e(LX/F2r;LX/0Pz;)V

    .line 2193441
    new-instance v0, LX/F2d;

    sget-object v1, LX/F2y;->b:LX/DML;

    invoke-direct {v0, p0, v1}, LX/F2d;-><init>(LX/F2r;LX/DML;)V

    invoke-virtual {p1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193442
    invoke-static {p0, p1}, LX/F2r;->e(LX/F2r;LX/0Pz;)V

    .line 2193443
    return-void

    .line 2193444
    :cond_0
    iget-object v0, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->v()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x1

    const v4, -0x184212d0

    invoke-static {v2, v0, v3, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2193445
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2193446
    :goto_1
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v2

    .line 2193447
    :goto_2
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 2193448
    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2193449
    new-instance v5, LX/F2c;

    sget-object v6, LX/F2y;->d:LX/DML;

    invoke-direct {v5, p0, v6, v4, v3}, LX/F2c;-><init>(LX/F2r;LX/DML;LX/15i;I)V

    invoke-virtual {p1, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193450
    add-int/lit8 v3, v2, -0x1

    if-eq v1, v3, :cond_1

    .line 2193451
    invoke-static {p0, p1}, LX/F2r;->f(LX/F2r;LX/0Pz;)V

    .line 2193452
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2193453
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 2193454
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2193455
    :cond_3
    invoke-static {p0, p1}, LX/F2r;->e(LX/F2r;LX/0Pz;)V

    goto :goto_0
.end method

.method private e()V
    .locals 15

    .prologue
    .line 2193375
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193376
    iput-object v0, p0, LX/F2r;->i:LX/0Px;

    .line 2193377
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2193378
    const/4 v5, 0x0

    .line 2193379
    invoke-static {p0, v0, v5}, LX/F2r;->a(LX/F2r;LX/0Pz;I)V

    .line 2193380
    new-instance v1, LX/F2m;

    sget-object v2, LX/F2y;->e:LX/DML;

    invoke-direct {v1, p0, v2}, LX/F2m;-><init>(LX/F2r;LX/DML;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193381
    invoke-static {p0, v0}, LX/F2r;->f(LX/F2r;LX/0Pz;)V

    .line 2193382
    new-instance v1, LX/F2o;

    sget-object v2, LX/F2y;->e:LX/DML;

    invoke-direct {v1, p0, v2}, LX/F2o;-><init>(LX/F2r;LX/DML;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193383
    invoke-static {p0, v0}, LX/F2r;->f(LX/F2r;LX/0Pz;)V

    .line 2193384
    new-instance v1, LX/F2q;

    sget-object v2, LX/F2y;->e:LX/DML;

    invoke-direct {v1, p0, v2}, LX/F2q;-><init>(LX/F2r;LX/DML;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193385
    invoke-static {p0, v0}, LX/F2r;->f(LX/F2r;LX/0Pz;)V

    .line 2193386
    new-instance v2, LX/F2Y;

    invoke-direct {v2, p0}, LX/F2Y;-><init>(LX/F2r;)V

    .line 2193387
    iget-object v1, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->w()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v4, 0x37a88ba0

    invoke-static {v3, v1, v5, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    .line 2193388
    if-eqz v1, :cond_3

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    .line 2193389
    :goto_0
    iget-object v3, p0, LX/F2r;->g:LX/F4Q;

    iget-object v4, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v4}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v4

    .line 2193390
    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, LX/2sN;->a()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, LX/2sN;->b()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2193391
    const/4 v8, 0x2

    const-class v9, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v7, v6, v8, v9, v10}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v8

    if-ne v4, v8, :cond_0

    .line 2193392
    const/4 v5, 0x1

    invoke-virtual {v7, v6, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2193393
    :goto_1
    move-object v1, v5

    .line 2193394
    const/4 v6, 0x0

    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 2193395
    iget-object v7, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->o()LX/1vs;

    move-result-object v7

    iget v7, v7, LX/1vs;->b:I

    if-eqz v7, :cond_1

    iget-object v7, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v7

    if-eqz v7, :cond_1

    iget-object v7, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v7, v8, :cond_1

    iget-object v7, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-static {v7}, LX/F4L;->b(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2193396
    :cond_1
    :goto_2
    move-object v4, v6

    .line 2193397
    new-instance v6, LX/F4M;

    sget-object v8, LX/F2y;->f:LX/DML;

    move-object v7, v3

    move-object v9, v1

    move-object v10, v2

    move-object v11, v4

    invoke-direct/range {v6 .. v11}, LX/F4M;-><init>(LX/F4Q;LX/DML;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/CharSequence;)V

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    move-object v1, v6

    .line 2193398
    if-eqz v1, :cond_2

    .line 2193399
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2193400
    :cond_2
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2193401
    invoke-static {p0}, LX/F2r;->g(LX/F2r;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2193402
    :goto_3
    invoke-static {p0, v0}, LX/F2r;->c(LX/F2r;LX/0Pz;)V

    .line 2193403
    invoke-static {p0, v0}, LX/F2r;->d(LX/F2r;LX/0Pz;)V

    .line 2193404
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/F2r;->i:LX/0Px;

    .line 2193405
    const v0, 0x158c1ecf

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2193406
    return-void

    .line 2193407
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto/16 :goto_0

    :cond_4
    const-string v5, ""

    goto :goto_1

    .line 2193408
    :cond_5
    iget-object v7, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->o()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    invoke-virtual {v8, v7, v13}, LX/15i;->j(II)I

    move-result v7

    iget-object v8, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v8}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->z()I

    move-result v8

    if-ge v7, v8, :cond_7

    .line 2193409
    iget-object v6, p0, LX/F2r;->b:Landroid/content/res/Resources;

    const v7, 0x7f0831c7

    new-array v8, v14, [Ljava/lang/Object;

    iget-object v9, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v9}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->z()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v13

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2193410
    :cond_6
    :goto_4
    if-eqz v6, :cond_1

    iget-object v7, p0, LX/F2r;->f:LX/F4L;

    invoke-virtual {v7, v6}, LX/F4L;->a(Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    goto :goto_2

    .line 2193411
    :cond_7
    iget-object v7, p0, LX/F2r;->f:LX/F4L;

    iget-object v8, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7, v8}, LX/F4L;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 2193412
    iget-object v6, p0, LX/F2r;->b:Landroid/content/res/Resources;

    const v7, 0x7f0831c9

    new-array v8, v14, [Ljava/lang/Object;

    iget-object v9, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v9}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->z()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v13

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 2193413
    :cond_8
    iget-object v7, p0, LX/F2r;->f:LX/F4L;

    iget-object v8, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7, v8}, LX/F4L;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v7, v8, :cond_6

    .line 2193414
    new-instance v6, Ljava/util/Date;

    iget-object v7, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->n()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 2193415
    iget-object v7, p0, LX/F2r;->b:Landroid/content/res/Resources;

    const v8, 0x7f0831c8

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, LX/F2r;->f:LX/F4L;

    iget-object v11, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    iget-object v12, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v12}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, LX/F4L;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v13

    iget-object v10, p0, LX/F2r;->e:LX/11T;

    invoke-virtual {v10}, LX/11T;->a()Ljava/text/DateFormat;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v14

    const/4 v10, 0x2

    iget-object v11, p0, LX/F2r;->e:LX/11T;

    invoke-virtual {v11}, LX/11T;->g()Ljava/text/SimpleDateFormat;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_4

    .line 2193416
    :cond_9
    invoke-static {p0, v0, v2}, LX/F2r;->a(LX/F2r;LX/0Pz;I)V

    .line 2193417
    invoke-static {p0, v0}, LX/F2r;->e(LX/F2r;LX/0Pz;)V

    .line 2193418
    iget-object v1, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_c

    .line 2193419
    iget-object v1, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v6, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v5, v1, v3, v6}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v1

    .line 2193420
    if-eqz v1, :cond_a

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_5
    if-eqz v1, :cond_b

    move v1, v2

    :goto_6
    if-eqz v1, :cond_f

    .line 2193421
    iget-object v1, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v6, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v5, v1, v3, v6}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v1

    .line 2193422
    if-eqz v1, :cond_d

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_7
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    :goto_8
    if-eqz v2, :cond_12

    .line 2193423
    iget-object v1, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v2, v1, v3, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v1

    .line 2193424
    if-eqz v1, :cond_10

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_9
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    .line 2193425
    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->a()LX/1Fb;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 2193426
    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->a()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 2193427
    :goto_a
    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2193428
    :goto_b
    iget-object v1, p0, LX/F2r;->g:LX/F4Q;

    new-instance v3, LX/F2Z;

    invoke-direct {v3, p0}, LX/F2Z;-><init>(LX/F2r;)V

    .line 2193429
    new-instance v7, LX/F4N;

    sget-object v9, LX/F2y;->g:LX/DML;

    move-object v8, v1

    move-object v10, v2

    move-object v11, v4

    move-object v12, v3

    invoke-direct/range {v7 .. v12}, LX/F4N;-><init>(LX/F4Q;LX/DML;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    move-object v1, v7

    .line 2193430
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    goto/16 :goto_3

    .line 2193431
    :cond_a
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2193432
    goto :goto_5

    :cond_b
    move v1, v3

    goto :goto_6

    :cond_c
    move v1, v3

    goto :goto_6

    .line 2193433
    :cond_d
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2193434
    goto :goto_7

    :cond_e
    move v2, v3

    goto :goto_8

    :cond_f
    move v2, v3

    goto :goto_8

    .line 2193435
    :cond_10
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2193436
    goto :goto_9

    :cond_11
    move-object v2, v4

    goto :goto_a

    :cond_12
    move-object v2, v4

    goto :goto_b
.end method

.method public static e(LX/F2r;LX/0Pz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2193373
    goto :goto_0

    .line 2193374
    :goto_0
    return-void
.end method

.method public static f(LX/F2r;LX/0Pz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2193371
    goto :goto_0

    .line 2193372
    :goto_0
    return-void
.end method

.method public static g(LX/F2r;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2193356
    iget-object v2, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-nez v2, :cond_0

    move v0, v1

    .line 2193357
    :goto_0
    return v0

    .line 2193358
    :cond_0
    iget-object v2, p0, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v4, v2, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2

    .line 2193359
    if-nez v4, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    :goto_2
    if-eqz v0, :cond_6

    move v0, v1

    .line 2193360
    goto :goto_0

    .line 2193361
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2193362
    :cond_1
    const-class v2, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v3, v4, v1, v2}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    :goto_3
    if-nez v2, :cond_3

    move v2, v0

    goto :goto_1

    .line 2193363
    :cond_2
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2193364
    goto :goto_3

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    const-class v0, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    goto :goto_2

    .line 2193365
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193366
    goto :goto_4

    .line 2193367
    :cond_6
    const-class v0, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_5
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->name()Ljava/lang/String;

    move-result-object v0

    .line 2193368
    const-string v1, "REAL_WORLD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 2193369
    :cond_7
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193370
    goto :goto_5
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2193354
    sget-object p0, LX/F2y;->i:LX/0Px;

    move-object v0, p0

    .line 2193355
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2193351
    check-cast p2, LX/DMB;

    .line 2193352
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2193353
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2193350
    iget-object v0, p0, LX/F2r;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2193349
    iget-object v0, p0, LX/F2r;->i:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2193348
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2193346
    sget-object v1, LX/F2y;->i:LX/0Px;

    move-object v1, v1

    .line 2193347
    iget-object v0, p0, LX/F2r;->i:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2193344
    sget-object p0, LX/F2y;->i:LX/0Px;

    move-object v0, p0

    .line 2193345
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
