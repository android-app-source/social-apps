.class public final LX/FZM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/debug/SearchDebugActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/search/debug/SearchDebugActivity;)V
    .locals 0

    .prologue
    .line 2257032
    iput-object p1, p0, LX/FZM;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 2257029
    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2257030
    iget-object v0, p0, LX/FZM;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v1, p0, LX/FZM;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v1, v1, Lcom/facebook/search/debug/SearchDebugActivity;->e:LX/1nD;

    sget-object v2, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    const-string v3, "Injected Pulse Title"

    check-cast p2, Ljava/lang/String;

    sget-object v4, LX/8ci;->h:LX/8ci;

    invoke-virtual {v1, v2, v3, p2, v4}, LX/1nD;->b(Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;LX/8ci;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/search/debug/SearchDebugActivity;->startActivity(Landroid/content/Intent;)V

    .line 2257031
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
