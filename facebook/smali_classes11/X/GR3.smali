.class public final LX/GR3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GQx;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GQx;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:LX/Gry;

.field private e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field private final g:I


# direct methods
.method public constructor <init>(LX/Gry;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2351136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GR3;->a:Ljava/util/List;

    .line 2351138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GR3;->b:Ljava/util/List;

    .line 2351139
    const/16 v0, 0x3e8

    iput v0, p0, LX/GR3;->g:I

    .line 2351140
    iput-object p1, p0, LX/GR3;->d:LX/Gry;

    .line 2351141
    iput-object p2, p0, LX/GR3;->e:Ljava/lang/String;

    .line 2351142
    iput-object p3, p0, LX/GR3;->f:Ljava/lang/String;

    .line 2351143
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 1

    .prologue
    .line 2351135
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GR3;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(LX/GAU;ZZ)I
    .locals 12

    .prologue
    .line 2351048
    monitor-enter p0

    .line 2351049
    :try_start_0
    iget v1, p0, LX/GR3;->c:I

    .line 2351050
    iget-object v0, p0, LX/GR3;->b:Ljava/util/List;

    iget-object v2, p0, LX/GR3;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2351051
    iget-object v0, p0, LX/GR3;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2351052
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 2351053
    iget-object v0, p0, LX/GR3;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GQx;

    .line 2351054
    if-nez p2, :cond_1

    .line 2351055
    iget-boolean v4, v0, LX/GQx;->isImplicit:Z

    move v4, v4

    .line 2351056
    if-nez v4, :cond_0

    .line 2351057
    :cond_1
    iget-object v4, v0, LX/GQx;->jsonObject:Lorg/json/JSONObject;

    move-object v0, v4

    .line 2351058
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 2351059
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2351060
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 2351061
    const/4 v0, 0x0

    monitor-exit p0

    .line 2351062
    :goto_1
    return v0

    .line 2351063
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2351064
    :try_start_2
    sget-object v0, LX/Gru;->CUSTOM_APP_EVENTS:LX/Gru;

    iget-object v3, p0, LX/GR3;->d:LX/Gry;

    iget-object v4, p0, LX/GR3;->f:Ljava/lang/String;

    sget-object v5, LX/GR4;->h:Landroid/content/Context;

    .line 2351065
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 2351066
    const-string v7, "event"

    sget-object v8, LX/Grv;->a:Ljava/util/Map;

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2351067
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2351068
    if-eqz v3, :cond_4

    .line 2351069
    iget-object v7, v3, LX/Gry;->b:Ljava/lang/String;

    move-object v7, v7

    .line 2351070
    if-eqz v7, :cond_4

    .line 2351071
    const-string v7, "attribution"

    .line 2351072
    iget-object v10, v3, LX/Gry;->b:Ljava/lang/String;

    move-object v10, v10

    .line 2351073
    invoke-virtual {v6, v7, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2351074
    :cond_4
    if-eqz v3, :cond_5

    .line 2351075
    iget-object v7, v3, LX/Gry;->c:Ljava/lang/String;

    move-object v7, v7

    .line 2351076
    if-eqz v7, :cond_5

    .line 2351077
    const-string v7, "advertiser_id"

    .line 2351078
    iget-object v10, v3, LX/Gry;->c:Ljava/lang/String;

    move-object v10, v10

    .line 2351079
    invoke-virtual {v6, v7, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2351080
    const-string v10, "advertiser_tracking_enabled"

    .line 2351081
    iget-boolean v7, v3, LX/Gry;->e:Z

    move v7, v7

    .line 2351082
    if-nez v7, :cond_a

    move v7, v8

    :goto_2
    invoke-virtual {v6, v10, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2351083
    :cond_5
    if-eqz v3, :cond_6

    .line 2351084
    iget-object v7, v3, LX/Gry;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2351085
    if-eqz v7, :cond_6

    .line 2351086
    const-string v7, "installer_package"

    .line 2351087
    iget-object v10, v3, LX/Gry;->d:Ljava/lang/String;

    move-object v10, v10

    .line 2351088
    invoke-virtual {v6, v7, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2351089
    :cond_6
    const-string v7, "anon_id"

    invoke-virtual {v6, v7, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2351090
    const-string v7, "application_tracking_enabled"

    if-nez p3, :cond_b

    :goto_3
    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2351091
    :try_start_3
    invoke-static {v6, v5}, LX/Gsc;->a(Lorg/json/JSONObject;Landroid/content/Context;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 2351092
    :goto_4
    :try_start_4
    const-string v7, "application_package_name"

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2351093
    move-object v0, v6

    .line 2351094
    iget v3, p0, LX/GR3;->c:I

    if-lez v3, :cond_7

    .line 2351095
    const-string v3, "num_skipped_events"

    invoke-virtual {v0, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    .line 2351096
    :cond_7
    :goto_5
    iput-object v0, p1, LX/GAU;->g:Lorg/json/JSONObject;

    .line 2351097
    iget-object v0, p1, LX/GAU;->k:Landroid/os/Bundle;

    move-object v0, v0

    .line 2351098
    if-nez v0, :cond_8

    .line 2351099
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2351100
    :cond_8
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2351101
    if-eqz v3, :cond_9

    .line 2351102
    const-string v4, "custom_events_file"

    .line 2351103
    const/4 v5, 0x0

    .line 2351104
    :try_start_5
    const-string v6, "UTF-8"

    invoke-virtual {v3, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v5

    .line 2351105
    :goto_6
    move-object v5, v5

    .line 2351106
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 2351107
    iput-object v3, p1, LX/GAU;->n:Ljava/lang/Object;

    .line 2351108
    :cond_9
    iput-object v0, p1, LX/GAU;->k:Landroid/os/Bundle;

    .line 2351109
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    goto/16 :goto_1

    .line 2351110
    :catch_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    goto :goto_5

    .line 2351111
    :catch_1
    :try_start_6
    move-exception v7

    .line 2351112
    sget-object v8, LX/GAb;->APP_EVENTS:LX/GAb;

    const-string v9, "AppEvents"

    const-string v10, "Fetching extended device info parameters failed: \'%s\'"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v11, p2

    invoke-static {v8, v9, v10, v11}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_a
    move v7, v9

    .line 2351113
    goto :goto_2

    :cond_b
    move v8, v9

    .line 2351114
    goto :goto_3
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0

    .line 2351115
    :catch_2
    move-exception v6

    .line 2351116
    const-string v7, "Encoding exception: "

    invoke-static {v7, v6}, LX/Gsc;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_6
.end method

.method public final declared-synchronized a(LX/GQx;)V
    .locals 2

    .prologue
    .line 2351130
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GR3;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/GR3;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_0

    .line 2351131
    iget v0, p0, LX/GR3;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/GR3;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351132
    :goto_0
    monitor-exit p0

    return-void

    .line 2351133
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/GR3;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2351134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/GQx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2351127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GR3;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351128
    monitor-exit p0

    return-void

    .line 2351129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 2351121
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 2351122
    :try_start_0
    iget-object v0, p0, LX/GR3;->a:Ljava/util/List;

    iget-object v1, p0, LX/GR3;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2351123
    :cond_0
    iget-object v0, p0, LX/GR3;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2351124
    const/4 v0, 0x0

    iput v0, p0, LX/GR3;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351125
    monitor-exit p0

    return-void

    .line 2351126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/GQx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2351117
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GR3;->a:Ljava/util/List;

    .line 2351118
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/GR3;->a:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351119
    monitor-exit p0

    return-object v0

    .line 2351120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
