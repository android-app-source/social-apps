.class public final LX/Gxk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;)V
    .locals 0

    .prologue
    .line 2408418
    iput-object p1, p0, LX/Gxk;->a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x4a578c57    # 3531541.8f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2408419
    iget-object v1, p0, LX/Gxk;->a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->c:LX/GvB;

    iget-object v2, p0, LX/Gxk;->a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/GvB;->a(Landroid/app/Activity;)V

    .line 2408420
    iget-object v1, p0, LX/Gxk;->a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->b:LX/Gxv;

    iget-object v2, p0, LX/Gxk;->a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v2, v2, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->w:Ljava/util/List;

    iget-object v3, p0, LX/Gxk;->a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget v3, v3, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->q:I

    iget-object v4, p0, LX/Gxk;->a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget v4, v4, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->t:I

    iget-object v5, p0, LX/Gxk;->a:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget v5, v5, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->s:I

    .line 2408421
    sget-object v7, LX/Gxu;->FINISHED:LX/Gxu;

    invoke-static {v7}, LX/Gxv;->a(LX/Gxu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 2408422
    const-string v7, "device_locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "network_country"

    iget-object p1, v1, LX/Gxv;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "sim_country"

    iget-object p1, v1, LX/Gxv;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "selected_index"

    invoke-virtual {v7, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "num_selected"

    invoke-virtual {v7, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "num_manual_selected"

    invoke-virtual {v7, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2408423
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 2408424
    new-instance p0, LX/162;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {p0, v7}, LX/162;-><init>(LX/0mC;)V

    .line 2408425
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2408426
    invoke-virtual {p0, v7}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 2408427
    :cond_0
    const-string v7, "locale_list"

    invoke-virtual {v8, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "locale_list_count"

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2408428
    const-string p0, "current_app_locale"

    const/4 v7, 0x0

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2408429
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 2408430
    const-string p0, "selected_locale"

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2408431
    :cond_1
    iget-object v7, v1, LX/Gxv;->a:LX/0Zb;

    invoke-interface {v7, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408432
    const v1, -0x3718700b

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
