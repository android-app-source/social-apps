.class public LX/FIy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/Random;

.field private final b:Landroid/content/res/Resources;

.field public final c:LX/FIs;

.field public final d:LX/FIs;

.field public final e:LX/FIs;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 5
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 2222826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222827
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/FIy;->a:Ljava/util/Random;

    .line 2222828
    iput-object p1, p0, LX/FIy;->b:Landroid/content/res/Resources;

    .line 2222829
    invoke-static {}, LX/FIs;->a()LX/FIr;

    move-result-object v0

    const v1, 0x7f020f56

    invoke-direct {p0, v1}, LX/FIy;->a(I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 2222830
    iput-object v1, v0, LX/FIr;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 2222831
    move-object v0, v0

    .line 2222832
    iput v2, v0, LX/FIr;->b:F

    .line 2222833
    move-object v0, v0

    .line 2222834
    invoke-virtual {v0, v3, v3}, LX/FIr;->a(FF)LX/FIr;

    move-result-object v0

    invoke-virtual {v0}, LX/FIr;->a()LX/FIs;

    move-result-object v0

    iput-object v0, p0, LX/FIy;->c:LX/FIs;

    .line 2222835
    invoke-static {}, LX/FIs;->a()LX/FIr;

    move-result-object v0

    const v1, 0x7f020f57

    invoke-direct {p0, v1}, LX/FIy;->a(I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 2222836
    iput-object v1, v0, LX/FIr;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 2222837
    move-object v0, v0

    .line 2222838
    iput v3, v0, LX/FIr;->b:F

    .line 2222839
    move-object v0, v0

    .line 2222840
    invoke-virtual {v0, v2, v2}, LX/FIr;->a(FF)LX/FIr;

    move-result-object v0

    invoke-virtual {v0}, LX/FIr;->a()LX/FIs;

    move-result-object v0

    iput-object v0, p0, LX/FIy;->d:LX/FIs;

    .line 2222841
    invoke-static {}, LX/FIs;->a()LX/FIr;

    move-result-object v0

    const v1, 0x7f020f58

    invoke-direct {p0, v1}, LX/FIy;->a(I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 2222842
    iput-object v1, v0, LX/FIr;->a:Landroid/graphics/drawable/BitmapDrawable;

    .line 2222843
    move-object v0, v0

    .line 2222844
    iput v3, v0, LX/FIr;->b:F

    .line 2222845
    move-object v0, v0

    .line 2222846
    invoke-virtual {v0, v2, v2}, LX/FIr;->a(FF)LX/FIr;

    move-result-object v0

    invoke-virtual {v0}, LX/FIr;->a()LX/FIs;

    move-result-object v0

    iput-object v0, p0, LX/FIy;->e:LX/FIs;

    .line 2222847
    const v0, -0xa06c

    const/16 v1, -0x6c19

    invoke-static {v0, v1}, LX/FIy;->a(II)Landroid/graphics/Paint;

    move-result-object v0

    .line 2222848
    const v1, -0x44630d

    const v2, -0x893109

    invoke-static {v1, v2}, LX/FIy;->a(II)Landroid/graphics/Paint;

    move-result-object v1

    .line 2222849
    const/16 v2, -0x3ee8

    const v3, -0x200e9

    invoke-static {v2, v3}, LX/FIy;->a(II)Landroid/graphics/Paint;

    move-result-object v2

    .line 2222850
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/graphics/Paint;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    aput-object v2, v3, v0

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/FIy;->f:Ljava/util/List;

    .line 2222851
    return-void
.end method

.method public static a(LX/FIp;I)LX/FIq;
    .locals 5

    .prologue
    .line 2222957
    int-to-long v0, p1

    const-wide/16 v2, 0x50

    mul-long/2addr v0, v2

    .line 2222958
    const-wide/16 v2, 0x4b0

    add-long/2addr v2, v0

    .line 2222959
    iput-wide v0, p0, LX/FIp;->h:J

    .line 2222960
    iput-wide v2, p0, LX/FIp;->i:J

    .line 2222961
    invoke-virtual {p0}, LX/FIp;->a()LX/FIq;

    move-result-object v0

    return-object v0
.end method

.method private static a(II)Landroid/graphics/Paint;
    .locals 13

    .prologue
    .line 2222946
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 2222947
    const/high16 v12, 0x437f0000    # 255.0f

    const/4 v11, 0x0

    .line 2222948
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    .line 2222949
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 2222950
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    .line 2222951
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v4

    .line 2222952
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v5

    .line 2222953
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    .line 2222954
    new-instance v7, Landroid/graphics/ColorMatrixColorFilter;

    new-instance v8, Landroid/graphics/ColorMatrix;

    const/16 v9, 0x14

    new-array v9, v9, [F

    const/4 v10, 0x0

    sub-int/2addr v4, v1

    int-to-float v4, v4

    div-float/2addr v4, v12

    aput v4, v9, v10

    const/4 v4, 0x1

    aput v11, v9, v4

    const/4 v4, 0x2

    aput v11, v9, v4

    const/4 v4, 0x3

    aput v11, v9, v4

    const/4 v4, 0x4

    int-to-float v1, v1

    aput v1, v9, v4

    const/4 v1, 0x5

    sub-int v4, v5, v2

    int-to-float v4, v4

    div-float/2addr v4, v12

    aput v4, v9, v1

    const/4 v1, 0x6

    aput v11, v9, v1

    const/4 v1, 0x7

    aput v11, v9, v1

    const/16 v1, 0x8

    aput v11, v9, v1

    const/16 v1, 0x9

    int-to-float v2, v2

    aput v2, v9, v1

    const/16 v1, 0xa

    sub-int v2, v6, v3

    int-to-float v2, v2

    div-float/2addr v2, v12

    aput v2, v9, v1

    const/16 v1, 0xb

    aput v11, v9, v1

    const/16 v1, 0xc

    aput v11, v9, v1

    const/16 v1, 0xd

    aput v11, v9, v1

    const/16 v1, 0xe

    int-to-float v2, v3

    aput v2, v9, v1

    const/16 v1, 0xf

    aput v11, v9, v1

    const/16 v1, 0x10

    aput v11, v9, v1

    const/16 v1, 0x11

    aput v11, v9, v1

    const/16 v1, 0x12

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v9, v1

    const/16 v1, 0x13

    aput v11, v9, v1

    invoke-direct {v8, v9}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-direct {v7, v8}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    move-object v1, v7

    .line 2222955
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 2222956
    return-object v0
.end method

.method private a(I)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 2222945
    iget-object v0, p0, LX/FIy;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method public static a$redex0(LX/FIy;F)F
    .locals 1

    .prologue
    .line 2222944
    iget-object v0, p0, LX/FIy;->b:Landroid/content/res/Resources;

    invoke-static {v0, p1}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/FIq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2222852
    new-instance v0, LX/FIx;

    invoke-direct {v0, p0}, LX/FIx;-><init>(LX/FIy;)V

    .line 2222853
    iget-object v1, p0, LX/FIy;->a:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextBoolean()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2222854
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x42480000    # 50.0f

    const/high16 v4, 0x41400000    # 12.0f

    const/high16 v5, -0x3d740000    # -70.0f

    .line 2222855
    const/4 v1, 0x5

    new-array v1, v1, [LX/FIq;

    const/high16 v2, 0x41bc0000    # 23.5f

    invoke-virtual {v0, v2, v4}, LX/FIx;->a(FF)LX/FIp;

    move-result-object v2

    const v3, 0x3f19999a    # 0.6f

    .line 2222856
    iput v3, v2, LX/FIp;->c:F

    .line 2222857
    move-object v2, v2

    .line 2222858
    iput v5, v2, LX/FIp;->e:F

    .line 2222859
    move-object v2, v2

    .line 2222860
    const/4 v3, 0x2

    invoke-static {v2, v3}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v2

    aput-object v2, v1, v7

    const/high16 v2, 0x41200000    # 10.0f

    const/high16 v3, 0x41e80000    # 29.0f

    invoke-virtual {v0, v2, v3}, LX/FIx;->b(FF)LX/FIp;

    move-result-object v2

    const v3, 0x3ee66666    # 0.45f

    .line 2222861
    iput v3, v2, LX/FIp;->c:F

    .line 2222862
    move-object v2, v2

    .line 2222863
    const/high16 v3, -0x3d600000    # -80.0f

    .line 2222864
    iput v3, v2, LX/FIp;->d:F

    .line 2222865
    move-object v2, v2

    .line 2222866
    const/high16 v3, -0x3d900000    # -60.0f

    .line 2222867
    iput v3, v2, LX/FIp;->e:F

    .line 2222868
    move-object v2, v2

    .line 2222869
    const/4 v3, 0x4

    invoke-static {v2, v3}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x2

    const/high16 v3, 0x41e40000    # 28.5f

    invoke-virtual {v0, v4, v3}, LX/FIx;->a(FF)LX/FIp;

    move-result-object v3

    const v4, 0x3f266666    # 0.65f

    .line 2222870
    iput v4, v3, LX/FIp;->c:F

    .line 2222871
    move-object v3, v3

    .line 2222872
    iput v5, v3, LX/FIp;->e:F

    .line 2222873
    move-object v3, v3

    .line 2222874
    const/4 v4, 0x3

    invoke-static {v3, v4}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/high16 v3, 0x41900000    # 18.0f

    const/high16 v4, 0x41580000    # 13.5f

    invoke-virtual {v0, v3, v4}, LX/FIx;->b(FF)LX/FIp;

    move-result-object v3

    const v4, 0x3f333333    # 0.7f

    .line 2222875
    iput v4, v3, LX/FIp;->c:F

    .line 2222876
    move-object v3, v3

    .line 2222877
    const/high16 v4, 0x420c0000    # 35.0f

    .line 2222878
    iput v4, v3, LX/FIp;->d:F

    .line 2222879
    move-object v3, v3

    .line 2222880
    iput v6, v3, LX/FIp;->e:F

    .line 2222881
    move-object v3, v3

    .line 2222882
    invoke-static {v3, v8}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const/high16 v3, 0x41680000    # 14.5f

    const/high16 v4, 0x41840000    # 16.5f

    invoke-virtual {v0, v3, v4}, LX/FIx;->a(FF)LX/FIp;

    move-result-object v3

    .line 2222883
    iput v6, v3, LX/FIp;->e:F

    .line 2222884
    move-object v3, v3

    .line 2222885
    invoke-static {v3, v7}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 2222886
    :goto_0
    iget-object v1, p0, LX/FIy;->a:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextBoolean()Z

    move-result v1

    .line 2222887
    if-eqz v1, :cond_2

    .line 2222888
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2222889
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIq;

    .line 2222890
    invoke-static {}, LX/FIq;->a()LX/FIp;

    move-result-object v3

    .line 2222891
    iget v6, v0, LX/FIq;->a:F

    iput v6, v3, LX/FIp;->a:F

    .line 2222892
    iget v6, v0, LX/FIq;->b:F

    iput v6, v3, LX/FIp;->b:F

    .line 2222893
    iget v6, v0, LX/FIq;->c:F

    iput v6, v3, LX/FIp;->c:F

    .line 2222894
    iget v6, v0, LX/FIq;->d:F

    iput v6, v3, LX/FIp;->d:F

    .line 2222895
    iget v6, v0, LX/FIq;->e:F

    iput v6, v3, LX/FIp;->e:F

    .line 2222896
    iget-object v6, v0, LX/FIq;->f:LX/FIs;

    iput-object v6, v3, LX/FIp;->f:LX/FIs;

    .line 2222897
    iget-object v6, v0, LX/FIq;->g:Landroid/graphics/Paint;

    iput-object v6, v3, LX/FIp;->g:Landroid/graphics/Paint;

    .line 2222898
    iget-wide v6, v0, LX/FIq;->h:J

    iput-wide v6, v3, LX/FIp;->h:J

    .line 2222899
    iget-wide v6, v0, LX/FIq;->i:J

    iput-wide v6, v3, LX/FIp;->i:J

    .line 2222900
    move-object v3, v3

    .line 2222901
    iget v4, v0, LX/FIq;->b:F

    iget v5, v0, LX/FIq;->a:F

    invoke-virtual {v3, v4, v5}, LX/FIp;->a(FF)LX/FIp;

    move-result-object v3

    iget v4, v0, LX/FIq;->d:F

    neg-float v4, v4

    .line 2222902
    iput v4, v3, LX/FIp;->d:F

    .line 2222903
    move-object v3, v3

    .line 2222904
    iget v0, v0, LX/FIq;->e:F

    neg-float v0, v0

    .line 2222905
    iput v0, v3, LX/FIp;->e:F

    .line 2222906
    move-object v0, v3

    .line 2222907
    invoke-virtual {v0}, LX/FIp;->a()LX/FIq;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2222908
    :cond_0
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v7, 0x42480000    # 50.0f

    const/high16 v6, 0x41300000    # 11.0f

    const/high16 v5, -0x3d740000    # -70.0f

    .line 2222909
    const/4 v1, 0x5

    new-array v1, v1, [LX/FIq;

    const/high16 v2, 0x41280000    # 10.5f

    const/high16 v3, 0x41c40000    # 24.5f

    invoke-virtual {v0, v2, v3}, LX/FIx;->b(FF)LX/FIp;

    move-result-object v2

    const v3, 0x3ee66666    # 0.45f

    .line 2222910
    iput v3, v2, LX/FIp;->c:F

    .line 2222911
    move-object v2, v2

    .line 2222912
    const/high16 v3, -0x3d1a0000    # -115.0f

    .line 2222913
    iput v3, v2, LX/FIp;->d:F

    .line 2222914
    move-object v2, v2

    .line 2222915
    iput v5, v2, LX/FIp;->e:F

    .line 2222916
    move-object v2, v2

    .line 2222917
    const/4 v3, 0x2

    invoke-static {v2, v3}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v2

    aput-object v2, v1, v8

    const/high16 v2, 0x41200000    # 10.0f

    const/high16 v3, 0x41880000    # 17.0f

    invoke-virtual {v0, v2, v3}, LX/FIx;->a(FF)LX/FIp;

    move-result-object v2

    const v3, 0x3f2e147b    # 0.68f

    .line 2222918
    iput v3, v2, LX/FIp;->c:F

    .line 2222919
    move-object v2, v2

    .line 2222920
    const/high16 v3, -0x3d900000    # -60.0f

    .line 2222921
    iput v3, v2, LX/FIp;->e:F

    .line 2222922
    move-object v2, v2

    .line 2222923
    const/4 v3, 0x3

    invoke-static {v2, v3}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v2

    aput-object v2, v1, v9

    const/4 v2, 0x2

    const/high16 v3, 0x41600000    # 14.0f

    const/high16 v4, 0x41b80000    # 23.0f

    invoke-virtual {v0, v3, v4}, LX/FIx;->a(FF)LX/FIp;

    move-result-object v3

    const v4, 0x3f2b851f    # 0.67f

    .line 2222924
    iput v4, v3, LX/FIp;->c:F

    .line 2222925
    move-object v3, v3

    .line 2222926
    iput v5, v3, LX/FIp;->e:F

    .line 2222927
    move-object v3, v3

    .line 2222928
    const/4 v4, 0x4

    invoke-static {v3, v4}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/high16 v3, 0x41a40000    # 20.5f

    invoke-virtual {v0, v3, v6}, LX/FIx;->b(FF)LX/FIp;

    move-result-object v3

    const v4, 0x3f59999a    # 0.85f

    .line 2222929
    iput v4, v3, LX/FIp;->c:F

    .line 2222930
    move-object v3, v3

    .line 2222931
    const/high16 v4, 0x42dc0000    # 110.0f

    .line 2222932
    iput v4, v3, LX/FIp;->d:F

    .line 2222933
    move-object v3, v3

    .line 2222934
    iput v7, v3, LX/FIp;->e:F

    .line 2222935
    move-object v3, v3

    .line 2222936
    invoke-static {v3, v9}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const/high16 v3, 0x41980000    # 19.0f

    invoke-virtual {v0, v3, v6}, LX/FIx;->a(FF)LX/FIp;

    move-result-object v3

    const v4, 0x3f733333    # 0.95f

    .line 2222937
    iput v4, v3, LX/FIp;->c:F

    .line 2222938
    move-object v3, v3

    .line 2222939
    iput v7, v3, LX/FIp;->e:F

    .line 2222940
    move-object v3, v3

    .line 2222941
    invoke-static {v3, v8}, LX/FIy;->a(LX/FIp;I)LX/FIq;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 2222942
    goto/16 :goto_0

    :cond_1
    move-object v0, v1

    .line 2222943
    :cond_2
    return-object v0
.end method
