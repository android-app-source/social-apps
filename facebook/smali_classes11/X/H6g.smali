.class public final LX/H6g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/offers/fragment/OffersWalletFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OffersWalletFragment;Z)V
    .locals 0

    .prologue
    .line 2427126
    iput-object p1, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iput-boolean p2, p0, LX/H6g;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2427127
    const-string v0, "OffersWalletFragment"

    const-string v1, "Error loading offers"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2427128
    iget-object v0, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OffersWalletFragment$6$5;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OffersWalletFragment$6$5;-><init>(LX/H6g;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2427129
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2427130
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2427131
    if-eqz p1, :cond_4

    .line 2427132
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2427133
    if-eqz v0, :cond_4

    .line 2427134
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2427135
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2427136
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2427137
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2427138
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2427139
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2427140
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2427141
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2427142
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    .line 2427143
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2427144
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel;->a()LX/0Px;

    move-result-object v0

    .line 2427145
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {p1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 2427146
    const/4 v4, 0x0

    move v5, v4

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_0

    .line 2427147
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel$EdgesModel;

    invoke-virtual {v4}, Lcom/facebook/offers/graphql/OfferQueriesModels$OffersWalletQueryModel$ActorModel$ClaimedOffersModel$EdgesModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v4

    .line 2427148
    invoke-static {v4}, LX/H83;->a(LX/H70;)LX/H83;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2427149
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2427150
    :cond_0
    move-object v0, p1

    .line 2427151
    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move v0, v1

    .line 2427152
    :goto_1
    new-instance v1, LX/H6f;

    invoke-direct {v1, p0}, LX/H6f;-><init>(LX/H6g;)V

    invoke-static {v3, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2427153
    iget-object v1, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletFragment;->c:Lcom/facebook/offers/fragment/OffersWalletAdapter;

    .line 2427154
    iput-object v3, v1, Lcom/facebook/offers/fragment/OffersWalletAdapter;->b:Ljava/util/List;

    .line 2427155
    if-lez v0, :cond_1

    iget-boolean v1, p0, LX/H6g;->a:Z

    if-eqz v1, :cond_1

    .line 2427156
    iget-object v1, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletFragment;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2427157
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Results: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2427158
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2427159
    iget-object v1, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletFragment;->b:LX/0Sh;

    new-instance v3, Lcom/facebook/offers/fragment/OffersWalletFragment$6$2;

    invoke-direct {v3, p0}, Lcom/facebook/offers/fragment/OffersWalletFragment$6$2;-><init>(LX/H6g;)V

    invoke-virtual {v1, v3}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2427160
    :cond_2
    iget-object v1, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletFragment;->d:LX/0Zb;

    iget-object v3, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v3, v3, Lcom/facebook/offers/fragment/OffersWalletFragment;->e:LX/H82;

    iget-object v4, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-boolean v4, v4, Lcom/facebook/offers/fragment/OffersWalletFragment;->m:Z

    iget-object v5, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v5, v5, Lcom/facebook/offers/fragment/OffersWalletFragment;->q:Ljava/lang/String;

    .line 2427161
    if-eqz v4, :cond_6

    .line 2427162
    const-string v6, "viewed_wallet_active"

    .line 2427163
    :goto_2
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v7, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2427164
    const-string v6, "offers"

    .line 2427165
    iput-object v6, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2427166
    const-string p1, "user_fbid"

    iget-object v6, v3, LX/H82;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2427167
    const-string v6, "coupons_shown"

    invoke-virtual {v7, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2427168
    const-string v6, "offers_shown"

    invoke-virtual {v7, v6, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2427169
    const-string v6, "total_shown"

    add-int p1, v2, v0

    invoke-virtual {v7, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2427170
    const-string v6, "network_down_bandwidth"

    iget-object p1, v3, LX/H82;->c:LX/0Y0;

    invoke-virtual {p1}, LX/0Y0;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2427171
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 2427172
    const-string v6, "offer_location"

    invoke-virtual {v7, v6, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2427173
    :cond_3
    move-object v0, v7

    .line 2427174
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2427175
    :cond_4
    iget-object v0, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OffersWalletFragment$6$3;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OffersWalletFragment$6$3;-><init>(LX/H6g;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2427176
    iget-object v0, p0, LX/H6g;->b:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OffersWalletFragment$6$4;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OffersWalletFragment$6$4;-><init>(LX/H6g;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2427177
    return-void

    :cond_5
    move v0, v2

    goto/16 :goto_1

    .line 2427178
    :cond_6
    const-string v6, "viewed_wallet_inactive"

    goto :goto_2
.end method
