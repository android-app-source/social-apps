.class public abstract LX/H0v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2414501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2414502
    const-string v0, ""

    iput-object v0, p0, LX/H0v;->c:Ljava/lang/CharSequence;

    .line 2414503
    const-string v0, ""

    iput-object v0, p0, LX/H0v;->d:Ljava/lang/CharSequence;

    .line 2414504
    const-string v0, ""

    iput-object v0, p0, LX/H0v;->e:Ljava/lang/CharSequence;

    .line 2414505
    iput-object p1, p0, LX/H0v;->b:Ljava/lang/String;

    .line 2414506
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2414459
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<b>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2414460
    invoke-virtual {p0, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2414500
    const/16 v0, 0x5f

    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/view/View;
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2414499
    const-string v0, ""

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2414498
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/H0v;->c(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2414497
    iget-object v0, p0, LX/H0v;->b:Ljava/lang/String;

    invoke-static {v0}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2414490
    iget-object v0, p0, LX/H0v;->c:Ljava/lang/CharSequence;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H0v;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414491
    const-string v0, ""

    .line 2414492
    :goto_0
    return-object v0

    .line 2414493
    :cond_0
    if-nez p1, :cond_1

    .line 2414494
    invoke-virtual {p0}, LX/H0v;->c()Ljava/lang/String;

    move-result-object v0

    .line 2414495
    :goto_1
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, LX/H0v;->c:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ": "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 2414496
    :cond_1
    invoke-virtual {p0}, LX/H0v;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/H0v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2414489
    iget-object v0, p0, LX/H0v;->b:Ljava/lang/String;

    invoke-static {v0}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2414488
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/H0v;->d(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2414481
    iget-object v0, p0, LX/H0v;->d:Ljava/lang/CharSequence;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H0v;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414482
    const-string v0, ""

    .line 2414483
    :goto_0
    return-object v0

    .line 2414484
    :cond_0
    if-nez p1, :cond_1

    .line 2414485
    invoke-virtual {p0}, LX/H0v;->e()Ljava/lang/String;

    move-result-object v0

    .line 2414486
    :goto_1
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, LX/H0v;->d:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ": "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 2414487
    :cond_1
    invoke-virtual {p0}, LX/H0v;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/H0v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2414474
    iget-object v0, p0, LX/H0v;->e:Ljava/lang/CharSequence;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H0v;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414475
    const-string v0, ""

    .line 2414476
    :goto_0
    return-object v0

    .line 2414477
    :cond_0
    if-nez p1, :cond_1

    .line 2414478
    invoke-virtual {p0}, LX/H0v;->a()Ljava/lang/String;

    move-result-object v0

    .line 2414479
    :goto_1
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, LX/H0v;->e:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, ": "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 2414480
    :cond_1
    invoke-virtual {p0}, LX/H0v;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/H0v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2414473
    const-string v0, ""

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2414472
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/H0v;->e(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/H17;
    .locals 2

    .prologue
    .line 2414461
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 2414462
    const-class v1, LX/H1b;

    if-ne v0, v1, :cond_0

    .line 2414463
    sget-object v0, LX/H17;->UNIVERSE:LX/H17;

    .line 2414464
    :goto_0
    return-object v0

    .line 2414465
    :cond_0
    const-class v1, LX/H1U;

    if-ne v0, v1, :cond_1

    .line 2414466
    sget-object v0, LX/H17;->QE:LX/H17;

    goto :goto_0

    .line 2414467
    :cond_1
    const-class v1, LX/H0z;

    if-ne v0, v1, :cond_2

    .line 2414468
    sget-object v0, LX/H17;->GK:LX/H17;

    goto :goto_0

    .line 2414469
    :cond_2
    instance-of v0, p0, LX/H0w;

    if-eqz v0, :cond_3

    .line 2414470
    sget-object v0, LX/H17;->PARAM:LX/H17;

    goto :goto_0

    .line 2414471
    :cond_3
    sget-object v0, LX/H17;->UNKNOWN:LX/H17;

    goto :goto_0
.end method
