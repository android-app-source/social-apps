.class public LX/FSa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FSa;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:LX/12x;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/12x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2242214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2242215
    iput-object p1, p0, LX/FSa;->a:Landroid/content/Context;

    .line 2242216
    iput-object p2, p0, LX/FSa;->b:LX/12x;

    .line 2242217
    return-void
.end method

.method public static a(LX/0QB;)LX/FSa;
    .locals 5

    .prologue
    .line 2242218
    sget-object v0, LX/FSa;->c:LX/FSa;

    if-nez v0, :cond_1

    .line 2242219
    const-class v1, LX/FSa;

    monitor-enter v1

    .line 2242220
    :try_start_0
    sget-object v0, LX/FSa;->c:LX/FSa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2242221
    if-eqz v2, :cond_0

    .line 2242222
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2242223
    new-instance p0, LX/FSa;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/12x;->a(LX/0QB;)LX/12x;

    move-result-object v4

    check-cast v4, LX/12x;

    invoke-direct {p0, v3, v4}, LX/FSa;-><init>(Landroid/content/Context;LX/12x;)V

    .line 2242224
    move-object v0, p0

    .line 2242225
    sput-object v0, LX/FSa;->c:LX/FSa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2242226
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2242227
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2242228
    :cond_1
    sget-object v0, LX/FSa;->c:LX/FSa;

    return-object v0

    .line 2242229
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2242230
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2242231
    iget-object v0, p0, LX/FSa;->a:Landroid/content/Context;

    invoke-static {v0}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v0

    .line 2242232
    const-class v1, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v0, LX/2E6;->b:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/2E6;->b(LX/2E6;Ljava/lang/String;)V

    invoke-static {v0}, LX/2E6;->a(LX/2E6;)Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2242233
    :goto_0
    return-void

    :cond_0
    const-string p0, "scheduler_action"

    const-string v1, "CANCEL_ALL"

    invoke-virtual {v3, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "component"

    invoke-virtual {v3, p0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object p0, v0, LX/2E6;->b:Landroid/content/Context;

    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private static e(LX/FSa;)Z
    .locals 2

    .prologue
    .line 2242234
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 2242235
    iget-object v1, p0, LX/FSa;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 2242236
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f()Z
    .locals 2

    .prologue
    .line 2242237
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2242238
    invoke-static {}, LX/FSa;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2242239
    iget-object v0, p0, LX/FSa;->a:Landroid/content/Context;

    const-string v1, "jobscheduler"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/job/JobScheduler;

    .line 2242240
    const-string v1, "NewsFeedPrefetchScheduler"

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/job/JobScheduler;->cancel(I)V

    .line 2242241
    :cond_0
    invoke-static {p0}, LX/FSa;->e(LX/FSa;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2242242
    invoke-direct {p0}, LX/FSa;->c()V

    .line 2242243
    :cond_1
    const/4 v3, 0x0

    .line 2242244
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/FSa;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchBroadcastReceiver;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2242245
    const-string v1, "com.facebook.prefetch.feed.scheduler.NewsFeedPrefetchBroadcastReceiver.INITIATE_BACKGROUND_FETCH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2242246
    iget-object v1, p0, LX/FSa;->a:Landroid/content/Context;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 2242247
    iget-object v1, p0, LX/FSa;->b:LX/12x;

    invoke-virtual {v1, v0}, LX/12x;->a(Landroid/app/PendingIntent;)V

    .line 2242248
    return-void
.end method

.method public final a(J)Z
    .locals 12

    .prologue
    .line 2242249
    const/4 v0, 0x0

    .line 2242250
    invoke-static {}, LX/FSa;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2242251
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2242252
    new-instance v6, Landroid/content/ComponentName;

    iget-object v3, p0, LX/FSa;->a:Landroid/content/Context;

    const-class v7, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;

    invoke-direct {v6, v3, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2242253
    iget-object v3, p0, LX/FSa;->a:Landroid/content/Context;

    const-string v7, "jobscheduler"

    invoke-virtual {v3, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/job/JobScheduler;

    .line 2242254
    new-instance v7, Landroid/app/job/JobInfo$Builder;

    const v8, 0x7f0d01fb

    invoke-direct {v7, v8, v6}, Landroid/app/job/JobInfo$Builder;-><init>(ILandroid/content/ComponentName;)V

    invoke-virtual {v7, v4}, Landroid/app/job/JobInfo$Builder;->setRequiredNetworkType(I)Landroid/app/job/JobInfo$Builder;

    move-result-object v7

    const-wide/32 v9, 0xea60

    mul-long/2addr v9, p1

    invoke-virtual {v7, v9, v10}, Landroid/app/job/JobInfo$Builder;->setMinimumLatency(J)Landroid/app/job/JobInfo$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/app/job/JobInfo$Builder;->setRequiresDeviceIdle(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/app/job/JobInfo$Builder;->setPersisted(Z)Landroid/app/job/JobInfo$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/job/JobInfo$Builder;->build()Landroid/app/job/JobInfo;

    move-result-object v7

    .line 2242255
    :try_start_0
    invoke-virtual {v3, v7}, Landroid/app/job/JobScheduler;->schedule(Landroid/app/job/JobInfo;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-lez v3, :cond_3

    move v3, v4

    .line 2242256
    :goto_0
    move v0, v3

    .line 2242257
    :cond_0
    if-nez v0, :cond_1

    invoke-static {p0}, LX/FSa;->e(LX/FSa;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2242258
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 2242259
    new-instance v3, LX/2E9;

    invoke-direct {v3}, LX/2E9;-><init>()V

    .line 2242260
    const-class v4, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchGCMService;

    invoke-virtual {v3, v4}, LX/2E9;->a(Ljava/lang/Class;)LX/2E9;

    .line 2242261
    const-string v4, "NewsFeedPrefetchScheduler"

    iput-object v4, v3, LX/2EA;->c:Ljava/lang/String;

    .line 2242262
    const-wide/16 v5, 0x3c

    mul-long/2addr v5, p1

    .line 2242263
    const-wide/16 v7, 0x1e

    add-long/2addr v7, v5

    .line 2242264
    invoke-virtual {v3, v5, v6, v7, v8}, LX/2E9;->a(JJ)LX/2E9;

    .line 2242265
    iput-boolean v9, v3, LX/2EA;->d:Z

    .line 2242266
    iput v10, v3, LX/2EA;->a:I

    .line 2242267
    iput-boolean v10, v3, LX/2EA;->f:Z

    .line 2242268
    iput-boolean v9, v3, LX/2EA;->e:Z

    .line 2242269
    iget-object v4, p0, LX/FSa;->a:Landroid/content/Context;

    invoke-static {v4}, LX/2E6;->a(Landroid/content/Context;)LX/2E6;

    move-result-object v4

    .line 2242270
    invoke-virtual {v3}, LX/2E9;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v3

    .line 2242271
    :try_start_1
    invoke-virtual {v4, v3}, LX/2E6;->a(Lcom/google/android/gms/gcm/Task;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2242272
    :goto_1
    move v0, v9

    .line 2242273
    :cond_1
    if-nez v0, :cond_2

    .line 2242274
    const/4 v6, 0x0

    .line 2242275
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/FSa;->a:Landroid/content/Context;

    const-class v5, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchBroadcastReceiver;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2242276
    const-string v4, "com.facebook.prefetch.feed.scheduler.NewsFeedPrefetchBroadcastReceiver.INITIATE_BACKGROUND_FETCH"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2242277
    iget-object v4, p0, LX/FSa;->a:Landroid/content/Context;

    invoke-static {v4, v6, v3, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 2242278
    iget-object v4, p0, LX/FSa;->b:LX/12x;

    const/4 v5, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    const-wide/32 v9, 0xea60

    mul-long/2addr v9, p1

    add-long/2addr v7, v9

    invoke-virtual {v4, v5, v7, v8, v3}, LX/12x;->b(IJLandroid/app/PendingIntent;)V

    .line 2242279
    const/4 v3, 0x1

    move v0, v3

    .line 2242280
    :cond_2
    return v0

    :cond_3
    move v3, v5

    .line 2242281
    goto :goto_0

    .line 2242282
    :catch_0
    move-exception v3

    .line 2242283
    iget-object v4, p0, LX/FSa;->a:Landroid/content/Context;

    invoke-static {v4, v6, v3}, LX/2EB;->a(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/IllegalArgumentException;)V

    move v3, v5

    goto :goto_0

    .line 2242284
    :catch_1
    move-exception v4

    .line 2242285
    iget-object v5, p0, LX/FSa;->a:Landroid/content/Context;

    new-instance v6, Landroid/content/ComponentName;

    iget-object v7, p0, LX/FSa;->a:Landroid/content/Context;

    iget-object v8, v3, Lcom/google/android/gms/gcm/Task;->a:Ljava/lang/String;

    move-object v3, v8

    .line 2242286
    invoke-direct {v6, v7, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v5, v6, v4}, LX/2EB;->a(Landroid/content/Context;Landroid/content/ComponentName;Ljava/lang/IllegalArgumentException;)V

    goto :goto_1
.end method
