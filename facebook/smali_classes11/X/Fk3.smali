.class public LX/Fk3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278082
    iput-object p1, p0, LX/Fk3;->a:Landroid/content/Context;

    .line 2278083
    return-void
.end method

.method public static b(LX/0QB;)LX/Fk3;
    .locals 2

    .prologue
    .line 2278084
    new-instance v1, LX/Fk3;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/Fk3;-><init>(Landroid/content/Context;)V

    .line 2278085
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2278086
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 2278087
    iget-object v1, p0, LX/Fk3;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 2278088
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2278089
    if-eqz v0, :cond_0

    .line 2278090
    iget-object v0, p0, LX/Fk3;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 2278091
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 2278092
    array-length v0, v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2278093
    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
