.class public final LX/Ghh;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ghi;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public final synthetic g:LX/Ghi;


# direct methods
.method public constructor <init>(LX/Ghi;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2384583
    iput-object p1, p0, LX/Ghh;->g:LX/Ghi;

    .line 2384584
    move-object v0, p1

    .line 2384585
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2384586
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->a:Ljava/lang/String;

    iput-object v0, p0, LX/Ghh;->a:Ljava/lang/String;

    .line 2384587
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->b:Ljava/lang/String;

    iput-object v0, p0, LX/Ghh;->b:Ljava/lang/String;

    .line 2384588
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->c:Ljava/lang/String;

    iput-object v0, p0, LX/Ghh;->c:Ljava/lang/String;

    .line 2384589
    sget-object v0, Lcom/facebook/gametime/ui/components/partdefinition/iconmessage/GametimeIconMessageComponentSpec;->d:Ljava/lang/String;

    iput-object v0, p0, LX/Ghh;->d:Ljava/lang/String;

    .line 2384590
    iput-boolean v1, p0, LX/Ghh;->e:Z

    .line 2384591
    iput-boolean v1, p0, LX/Ghh;->f:Z

    .line 2384592
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2384582
    const-string v0, "GametimeIconMessageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2384558
    if-ne p0, p1, :cond_1

    .line 2384559
    :cond_0
    :goto_0
    return v0

    .line 2384560
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2384561
    goto :goto_0

    .line 2384562
    :cond_3
    check-cast p1, LX/Ghh;

    .line 2384563
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2384564
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2384565
    if-eq v2, v3, :cond_0

    .line 2384566
    iget-object v2, p0, LX/Ghh;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ghh;->a:Ljava/lang/String;

    iget-object v3, p1, LX/Ghh;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2384567
    goto :goto_0

    .line 2384568
    :cond_5
    iget-object v2, p1, LX/Ghh;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2384569
    :cond_6
    iget-object v2, p0, LX/Ghh;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Ghh;->b:Ljava/lang/String;

    iget-object v3, p1, LX/Ghh;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2384570
    goto :goto_0

    .line 2384571
    :cond_8
    iget-object v2, p1, LX/Ghh;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2384572
    :cond_9
    iget-object v2, p0, LX/Ghh;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Ghh;->c:Ljava/lang/String;

    iget-object v3, p1, LX/Ghh;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2384573
    goto :goto_0

    .line 2384574
    :cond_b
    iget-object v2, p1, LX/Ghh;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2384575
    :cond_c
    iget-object v2, p0, LX/Ghh;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Ghh;->d:Ljava/lang/String;

    iget-object v3, p1, LX/Ghh;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2384576
    goto :goto_0

    .line 2384577
    :cond_e
    iget-object v2, p1, LX/Ghh;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2384578
    :cond_f
    iget-boolean v2, p0, LX/Ghh;->e:Z

    iget-boolean v3, p1, LX/Ghh;->e:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2384579
    goto :goto_0

    .line 2384580
    :cond_10
    iget-boolean v2, p0, LX/Ghh;->f:Z

    iget-boolean v3, p1, LX/Ghh;->f:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2384581
    goto :goto_0
.end method
