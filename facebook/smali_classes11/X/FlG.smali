.class public final LX/FlG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FlI;


# direct methods
.method public constructor <init>(LX/FlI;)V
    .locals 0

    .prologue
    .line 2279857
    iput-object p1, p0, LX/FlG;->a:LX/FlI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/FlI;B)V
    .locals 0

    .prologue
    .line 2279858
    invoke-direct {p0, p1}, LX/FlG;-><init>(LX/FlI;)V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2279859
    if-eqz p1, :cond_0

    .line 2279860
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2279861
    if-nez v0, :cond_1

    .line 2279862
    :cond_0
    :goto_0
    return-void

    .line 2279863
    :cond_1
    sget-object v1, LX/0ax;->gZ:Ljava/lang/String;

    .line 2279864
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2279865
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->o()Ljava/lang/String;

    move-result-object v0

    const-string v2, "fundraiser"

    invoke-static {v1, v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2279866
    iget-object v0, p0, LX/FlG;->a:LX/FlI;

    iget-object v0, v0, LX/FlI;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, LX/FlG;->a:LX/FlI;

    iget-object v2, v2, LX/FlI;->c:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2279867
    iget-object v2, p0, LX/FlG;->a:LX/FlI;

    .line 2279868
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2279869
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;

    invoke-virtual {v2, v0}, LX/FlI;->a(Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;)Lcom/facebook/socialgood/model/Fundraiser;

    move-result-object v0

    .line 2279870
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2279871
    const-string v2, "fundraiser_model"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2279872
    iget-object v0, p0, LX/FlG;->a:LX/FlI;

    iget-object v0, v0, LX/FlI;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x1bc

    iget-object v3, p0, LX/FlG;->a:LX/FlI;

    iget-object v3, v3, LX/FlI;->a:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2279873
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279874
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/FlG;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
