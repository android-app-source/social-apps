.class public final LX/GW7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;)V
    .locals 0

    .prologue
    .line 2360392
    iput-object p1, p0, LX/GW7;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2360393
    new-instance v0, LX/GWE;

    invoke-direct {v0}, LX/GWE;-><init>()V

    move-object v0, v0

    .line 2360394
    const-string v1, "product_item_id"

    iget-object v2, p0, LX/GW7;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-wide v2, v2, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->m:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_pic_size"

    iget-object v3, p0, LX/GW7;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b008b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "recommended_product_image_size"

    iget-object v3, p0, LX/GW7;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0cec

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "product_item_image_size"

    iget-object v3, p0, LX/GW7;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v3, v3, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->h:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->c()I

    move-result v3

    iget-object v4, p0, LX/GW7;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v4, v4, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->h:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->d()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    sget v4, LX/GXD;->a:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x64

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "supported_section_types"

    .line 2360395
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2360396
    sget-object v6, LX/GW2;->f:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GVw;

    .line 2360397
    invoke-interface {v3}, LX/GVw;->a()Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2360398
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 2360399
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 2360400
    invoke-virtual {v1, v2, v3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2360401
    iget-object v1, p0, LX/GW7;->a:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v1, v1, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->e:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
