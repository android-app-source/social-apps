.class public final LX/G9K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/G9I;


# direct methods
.method public constructor <init>(LX/G9I;)V
    .locals 0

    .prologue
    .line 2322335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322336
    iput-object p1, p0, LX/G9K;->a:LX/G9I;

    .line 2322337
    return-void
.end method

.method private static a(LX/G9K;LX/G9J;)[I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2322292
    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v3

    .line 2322293
    if-ne v3, v0, :cond_0

    .line 2322294
    new-array v2, v0, [I

    invoke-virtual {p1, v0}, LX/G9J;->a(I)I

    move-result v0

    aput v0, v2, v1

    move-object v0, v2

    .line 2322295
    :goto_0
    return-object v0

    .line 2322296
    :cond_0
    new-array v2, v3, [I

    .line 2322297
    :goto_1
    iget-object v4, p0, LX/G9K;->a:LX/G9I;

    .line 2322298
    iget v5, v4, LX/G9I;->m:I

    move v4, v5

    .line 2322299
    if-ge v0, v4, :cond_2

    if-ge v1, v3, :cond_2

    .line 2322300
    invoke-virtual {p1, v0}, LX/G9J;->b(I)I

    move-result v4

    if-nez v4, :cond_1

    .line 2322301
    iget-object v4, p0, LX/G9K;->a:LX/G9I;

    invoke-virtual {v4, v0}, LX/G9I;->c(I)I

    move-result v4

    aput v4, v2, v1

    .line 2322302
    add-int/lit8 v1, v1, 0x1

    .line 2322303
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322304
    :cond_2
    if-eq v1, v3, :cond_3

    .line 2322305
    new-instance v0, LX/G9M;

    const-string v1, "Error locator degree does not match number of roots"

    invoke-direct {v0, v1}, LX/G9M;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v2

    .line 2322306
    goto :goto_0
.end method

.method private static a(LX/G9K;LX/G9J;LX/G9J;I)[LX/G9J;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2322307
    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v0

    invoke-virtual {p2}, LX/G9J;->b()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 2322308
    :goto_0
    iget-object v0, p0, LX/G9K;->a:LX/G9I;

    .line 2322309
    iget-object v1, v0, LX/G9I;->k:LX/G9J;

    move-object v1, v1

    .line 2322310
    iget-object v0, p0, LX/G9K;->a:LX/G9I;

    .line 2322311
    iget-object v2, v0, LX/G9I;->l:LX/G9J;

    move-object v0, v2

    .line 2322312
    :goto_1
    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v2

    div-int/lit8 v3, p3, 0x2

    if-lt v2, v3, :cond_3

    .line 2322313
    invoke-virtual {p1}, LX/G9J;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2322314
    new-instance v0, LX/G9M;

    const-string v1, "r_{i-1} was zero"

    invoke-direct {v0, v1}, LX/G9M;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322315
    :cond_0
    iget-object v2, p0, LX/G9K;->a:LX/G9I;

    .line 2322316
    iget-object v3, v2, LX/G9I;->k:LX/G9J;

    move-object v2, v3

    .line 2322317
    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v3

    invoke-virtual {p1, v3}, LX/G9J;->a(I)I

    move-result v3

    .line 2322318
    iget-object v4, p0, LX/G9K;->a:LX/G9I;

    invoke-virtual {v4, v3}, LX/G9I;->c(I)I

    move-result v4

    move-object v3, v2

    move-object v2, p2

    .line 2322319
    :goto_2
    invoke-virtual {v2}, LX/G9J;->b()I

    move-result v5

    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v6

    if-lt v5, v6, :cond_1

    invoke-virtual {v2}, LX/G9J;->c()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2322320
    invoke-virtual {v2}, LX/G9J;->b()I

    move-result v5

    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v6

    sub-int/2addr v5, v6

    .line 2322321
    iget-object v6, p0, LX/G9K;->a:LX/G9I;

    invoke-virtual {v2}, LX/G9J;->b()I

    move-result v7

    invoke-virtual {v2, v7}, LX/G9J;->a(I)I

    move-result v7

    invoke-virtual {v6, v7, v4}, LX/G9I;->c(II)I

    move-result v6

    .line 2322322
    iget-object v7, p0, LX/G9K;->a:LX/G9I;

    invoke-virtual {v7, v5, v6}, LX/G9I;->a(II)LX/G9J;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/G9J;->a(LX/G9J;)LX/G9J;

    move-result-object v3

    .line 2322323
    invoke-virtual {p1, v5, v6}, LX/G9J;->a(II)LX/G9J;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/G9J;->a(LX/G9J;)LX/G9J;

    move-result-object v2

    goto :goto_2

    .line 2322324
    :cond_1
    invoke-virtual {v3, v0}, LX/G9J;->b(LX/G9J;)LX/G9J;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/G9J;->a(LX/G9J;)LX/G9J;

    move-result-object v1

    .line 2322325
    invoke-virtual {v2}, LX/G9J;->b()I

    move-result v3

    invoke-virtual {p1}, LX/G9J;->b()I

    move-result v4

    if-lt v3, v4, :cond_2

    .line 2322326
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Division algorithm failed to reduce polynomial?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move-object p2, p1

    move-object p1, v2

    move-object v9, v0

    move-object v0, v1

    move-object v1, v9

    .line 2322327
    goto/16 :goto_1

    .line 2322328
    :cond_3
    invoke-virtual {v0, v8}, LX/G9J;->a(I)I

    move-result v1

    .line 2322329
    if-nez v1, :cond_4

    .line 2322330
    new-instance v0, LX/G9M;

    const-string v1, "sigmaTilde(0) was zero"

    invoke-direct {v0, v1}, LX/G9M;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322331
    :cond_4
    iget-object v2, p0, LX/G9K;->a:LX/G9I;

    invoke-virtual {v2, v1}, LX/G9I;->c(I)I

    move-result v1

    .line 2322332
    invoke-virtual {v0, v1}, LX/G9J;->c(I)LX/G9J;

    move-result-object v0

    .line 2322333
    invoke-virtual {p1, v1}, LX/G9J;->c(I)LX/G9J;

    move-result-object v1

    .line 2322334
    const/4 v2, 0x2

    new-array v2, v2, [LX/G9J;

    aput-object v0, v2, v8

    const/4 v0, 0x1

    aput-object v1, v2, v0

    return-object v2

    :cond_5
    move-object v9, p2

    move-object p2, p1

    move-object p1, v9

    goto/16 :goto_0
.end method


# virtual methods
.method public final a([II)V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2322245
    new-instance v4, LX/G9J;

    iget-object v0, p0, LX/G9K;->a:LX/G9I;

    invoke-direct {v4, v0, p1}, LX/G9J;-><init>(LX/G9I;[I)V

    .line 2322246
    new-array v5, p2, [I

    move v2, v1

    move v0, v3

    .line 2322247
    :goto_0
    if-ge v2, p2, :cond_1

    .line 2322248
    iget-object v6, p0, LX/G9K;->a:LX/G9I;

    iget-object v7, p0, LX/G9K;->a:LX/G9I;

    .line 2322249
    iget v8, v7, LX/G9I;->o:I

    move v7, v8

    .line 2322250
    add-int/2addr v7, v2

    invoke-virtual {v6, v7}, LX/G9I;->a(I)I

    move-result v6

    invoke-virtual {v4, v6}, LX/G9J;->b(I)I

    move-result v6

    .line 2322251
    add-int/lit8 v7, p2, -0x1

    sub-int/2addr v7, v2

    aput v6, v5, v7

    .line 2322252
    if-eqz v6, :cond_0

    move v0, v1

    .line 2322253
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2322254
    :cond_1
    if-eqz v0, :cond_3

    .line 2322255
    :cond_2
    return-void

    .line 2322256
    :cond_3
    new-instance v0, LX/G9J;

    iget-object v2, p0, LX/G9K;->a:LX/G9I;

    invoke-direct {v0, v2, v5}, LX/G9J;-><init>(LX/G9I;[I)V

    .line 2322257
    iget-object v2, p0, LX/G9K;->a:LX/G9I;

    .line 2322258
    invoke-virtual {v2, p2, v3}, LX/G9I;->a(II)LX/G9J;

    move-result-object v2

    invoke-static {p0, v2, v0, p2}, LX/G9K;->a(LX/G9K;LX/G9J;LX/G9J;I)[LX/G9J;

    move-result-object v0

    .line 2322259
    aget-object v2, v0, v1

    .line 2322260
    aget-object v0, v0, v3

    .line 2322261
    invoke-static {p0, v2}, LX/G9K;->a(LX/G9K;LX/G9J;)[I

    move-result-object v2

    .line 2322262
    const/4 v6, 0x0

    .line 2322263
    array-length v8, v2

    .line 2322264
    new-array v9, v8, [I

    move v7, v6

    .line 2322265
    :goto_1
    if-ge v7, v8, :cond_7

    .line 2322266
    iget-object v3, p0, LX/G9K;->a:LX/G9I;

    aget v4, v2, v7

    invoke-virtual {v3, v4}, LX/G9I;->c(I)I

    move-result v10

    .line 2322267
    const/4 v4, 0x1

    move v5, v6

    .line 2322268
    :goto_2
    if-ge v5, v8, :cond_5

    .line 2322269
    if-eq v7, v5, :cond_9

    .line 2322270
    iget-object v3, p0, LX/G9K;->a:LX/G9I;

    aget p2, v2, v5

    invoke-virtual {v3, p2, v10}, LX/G9I;->c(II)I

    move-result v3

    .line 2322271
    and-int/lit8 p2, v3, 0x1

    if-nez p2, :cond_4

    or-int/lit8 v3, v3, 0x1

    .line 2322272
    :goto_3
    iget-object p2, p0, LX/G9K;->a:LX/G9I;

    invoke-virtual {p2, v4, v3}, LX/G9I;->c(II)I

    move-result v3

    .line 2322273
    :goto_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_2

    .line 2322274
    :cond_4
    and-int/lit8 v3, v3, -0x2

    goto :goto_3

    .line 2322275
    :cond_5
    iget-object v3, p0, LX/G9K;->a:LX/G9I;

    invoke-virtual {v0, v10}, LX/G9J;->b(I)I

    move-result v5

    iget-object p2, p0, LX/G9K;->a:LX/G9I;

    .line 2322276
    invoke-virtual {p2, v4}, LX/G9I;->c(I)I

    move-result v4

    .line 2322277
    invoke-virtual {v3, v5, v4}, LX/G9I;->c(II)I

    move-result v3

    aput v3, v9, v7

    .line 2322278
    iget-object v3, p0, LX/G9K;->a:LX/G9I;

    .line 2322279
    iget v4, v3, LX/G9I;->o:I

    move v3, v4

    .line 2322280
    if-eqz v3, :cond_6

    .line 2322281
    iget-object v3, p0, LX/G9K;->a:LX/G9I;

    aget v4, v9, v7

    invoke-virtual {v3, v4, v10}, LX/G9I;->c(II)I

    move-result v3

    aput v3, v9, v7

    .line 2322282
    :cond_6
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_1

    .line 2322283
    :cond_7
    move-object v0, v9

    .line 2322284
    :goto_5
    array-length v3, v2

    if-ge v1, v3, :cond_2

    .line 2322285
    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, LX/G9K;->a:LX/G9I;

    aget v5, v2, v1

    invoke-virtual {v4, v5}, LX/G9I;->b(I)I

    move-result v4

    sub-int/2addr v3, v4

    .line 2322286
    if-gez v3, :cond_8

    .line 2322287
    new-instance v0, LX/G9M;

    const-string v1, "Bad error location"

    invoke-direct {v0, v1}, LX/G9M;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322288
    :cond_8
    aget v4, p1, v3

    aget v5, v0, v1

    .line 2322289
    xor-int v6, v4, v5

    move v4, v6

    .line 2322290
    aput v4, p1, v3

    .line 2322291
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_9
    move v3, v4

    goto :goto_4
.end method
