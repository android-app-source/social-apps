.class public final LX/FUE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2247992
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2247993
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2247994
    :goto_0
    return v1

    .line 2247995
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2247996
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2247997
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2247998
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2247999
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2248000
    const-string v6, "footer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2248001
    invoke-static {p0, p1}, LX/G1p;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2248002
    :cond_2
    const-string v6, "profile_tile_views"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2248003
    invoke-static {p0, p1}, LX/FUD;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2248004
    :cond_3
    const-string v6, "subtitle"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2248005
    invoke-static {p0, p1}, LX/G1q;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2248006
    :cond_4
    const-string v6, "title"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2248007
    invoke-static {p0, p1}, LX/G1r;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2248008
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2248009
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2248010
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2248011
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2248012
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2248013
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2248014
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2248015
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2248016
    if-eqz v0, :cond_0

    .line 2248017
    const-string v1, "footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2248018
    invoke-static {p0, v0, p2}, LX/G1p;->a(LX/15i;ILX/0nX;)V

    .line 2248019
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2248020
    if-eqz v0, :cond_1

    .line 2248021
    const-string v1, "profile_tile_views"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2248022
    invoke-static {p0, v0, p2, p3}, LX/FUD;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2248023
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2248024
    if-eqz v0, :cond_2

    .line 2248025
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2248026
    invoke-static {p0, v0, p2}, LX/G1q;->a(LX/15i;ILX/0nX;)V

    .line 2248027
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2248028
    if-eqz v0, :cond_3

    .line 2248029
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2248030
    invoke-static {p0, v0, p2}, LX/G1r;->a(LX/15i;ILX/0nX;)V

    .line 2248031
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2248032
    return-void
.end method
