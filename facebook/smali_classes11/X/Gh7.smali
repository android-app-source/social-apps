.class public final LX/Gh7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 2383333
    const/16 v20, 0x0

    .line 2383334
    const/16 v19, 0x0

    .line 2383335
    const/16 v18, 0x0

    .line 2383336
    const/16 v17, 0x0

    .line 2383337
    const/16 v16, 0x0

    .line 2383338
    const/4 v15, 0x0

    .line 2383339
    const/4 v14, 0x0

    .line 2383340
    const/4 v13, 0x0

    .line 2383341
    const/4 v12, 0x0

    .line 2383342
    const/4 v11, 0x0

    .line 2383343
    const/4 v10, 0x0

    .line 2383344
    const-wide/16 v8, 0x0

    .line 2383345
    const/4 v7, 0x0

    .line 2383346
    const/4 v6, 0x0

    .line 2383347
    const/4 v5, 0x0

    .line 2383348
    const/4 v4, 0x0

    .line 2383349
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_12

    .line 2383350
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2383351
    const/4 v4, 0x0

    .line 2383352
    :goto_0
    return v4

    .line 2383353
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2383354
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_d

    .line 2383355
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v21

    .line 2383356
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2383357
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    if-eqz v21, :cond_1

    .line 2383358
    const-string v22, "active_team_with_ball"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 2383359
    invoke-static/range {p0 .. p1}, LX/Gh4;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 2383360
    :cond_2
    const-string v22, "first_team_object"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 2383361
    invoke-static/range {p0 .. p1}, LX/Gh0;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 2383362
    :cond_3
    const-string v22, "first_team_primary_color"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 2383363
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto :goto_1

    .line 2383364
    :cond_4
    const-string v22, "first_team_score"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 2383365
    const/4 v9, 0x1

    .line 2383366
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto :goto_1

    .line 2383367
    :cond_5
    const-string v22, "has_match_started"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 2383368
    const/4 v8, 0x1

    .line 2383369
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 2383370
    :cond_6
    const-string v22, "id"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 2383371
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 2383372
    :cond_7
    const-string v22, "second_team_object"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 2383373
    invoke-static/range {p0 .. p1}, LX/Gh2;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 2383374
    :cond_8
    const-string v22, "second_team_primary_color"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 2383375
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 2383376
    :cond_9
    const-string v22, "second_team_score"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 2383377
    const/4 v5, 0x1

    .line 2383378
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto/16 :goto_1

    .line 2383379
    :cond_a
    const-string v22, "status"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 2383380
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 2383381
    :cond_b
    const-string v22, "status_text"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 2383382
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 2383383
    :cond_c
    const-string v22, "updated_time"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 2383384
    const/4 v4, 0x1

    .line 2383385
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    goto/16 :goto_1

    .line 2383386
    :cond_d
    const/16 v21, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2383387
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2383388
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2383389
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2383390
    if-eqz v9, :cond_e

    .line 2383391
    const/4 v9, 0x3

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v9, v1, v2}, LX/186;->a(III)V

    .line 2383392
    :cond_e
    if-eqz v8, :cond_f

    .line 2383393
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 2383394
    :cond_f
    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 2383395
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v14}, LX/186;->b(II)V

    .line 2383396
    const/4 v8, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v13}, LX/186;->b(II)V

    .line 2383397
    if-eqz v5, :cond_10

    .line 2383398
    const/16 v5, 0x8

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12, v8}, LX/186;->a(III)V

    .line 2383399
    :cond_10
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 2383400
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->b(II)V

    .line 2383401
    if-eqz v4, :cond_11

    .line 2383402
    const/16 v5, 0xb

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 2383403
    :cond_11
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_12
    move/from16 v24, v6

    move/from16 v25, v7

    move-wide v6, v8

    move/from16 v8, v24

    move/from16 v9, v25

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 2383282
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2383283
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2383284
    if-eqz v0, :cond_0

    .line 2383285
    const-string v1, "active_team_with_ball"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383286
    invoke-static {p0, v0, p2}, LX/Gh4;->a(LX/15i;ILX/0nX;)V

    .line 2383287
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383288
    if-eqz v0, :cond_1

    .line 2383289
    const-string v1, "first_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383290
    invoke-static {p0, v0, p2, p3}, LX/Gh0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383291
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383292
    if-eqz v0, :cond_2

    .line 2383293
    const-string v1, "first_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383294
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383295
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2383296
    if-eqz v0, :cond_3

    .line 2383297
    const-string v1, "first_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383298
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2383299
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2383300
    if-eqz v0, :cond_4

    .line 2383301
    const-string v1, "has_match_started"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383302
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2383303
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383304
    if-eqz v0, :cond_5

    .line 2383305
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383306
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383307
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383308
    if-eqz v0, :cond_6

    .line 2383309
    const-string v1, "second_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383310
    invoke-static {p0, v0, p2, p3}, LX/Gh2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383311
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383312
    if-eqz v0, :cond_7

    .line 2383313
    const-string v1, "second_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383314
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383315
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2383316
    if-eqz v0, :cond_8

    .line 2383317
    const-string v1, "second_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383318
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2383319
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383320
    if-eqz v0, :cond_9

    .line 2383321
    const-string v1, "status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383322
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383323
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383324
    if-eqz v0, :cond_a

    .line 2383325
    const-string v1, "status_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383326
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383327
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2383328
    cmp-long v2, v0, v4

    if-eqz v2, :cond_b

    .line 2383329
    const-string v2, "updated_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383330
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2383331
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2383332
    return-void
.end method
