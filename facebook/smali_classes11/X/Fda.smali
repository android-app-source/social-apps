.class public final LX/Fda;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;)V
    .locals 0

    .prologue
    .line 2263805
    iput-object p1, p0, LX/Fda;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2263806
    iget-object v0, p0, LX/Fda;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    invoke-virtual {v0, p3}, LX/FdT;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    .line 2263807
    iget-object v1, p0, LX/Fda;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    .line 2263808
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v2

    .line 2263809
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object v2, p0, LX/Fda;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    invoke-static {v0, v2}, Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;->a(LX/5uu;LX/FdZ;)Lcom/facebook/search/results/filters/ui/SearchResultPageSpecificFilterFragment;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2263810
    return-void
.end method
