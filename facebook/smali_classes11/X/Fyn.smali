.class public final LX/Fyn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSaveMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4Ij;

.field public final synthetic b:LX/Fyq;


# direct methods
.method public constructor <init>(LX/Fyq;LX/4Ij;)V
    .locals 0

    .prologue
    .line 2307187
    iput-object p1, p0, LX/Fyn;->b:LX/Fyq;

    iput-object p2, p0, LX/Fyn;->a:LX/4Ij;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2307188
    iget-object v0, p0, LX/Fyn;->b:LX/Fyq;

    iget-object v0, v0, LX/Fyq;->c:LX/03V;

    const-string v1, "PlutoniumProfileQuestionActionController.save_question_failed"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failure saving profile question. Input: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Fyn;->a:LX/4Ij;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2307189
    iget-object v0, p0, LX/Fyn;->b:LX/Fyq;

    iget-object v0, v0, LX/Fyq;->d:LX/Fyr;

    .line 2307190
    invoke-virtual {v0}, LX/Fyr;->b()V

    .line 2307191
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2307192
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2307193
    if-eqz p1, :cond_0

    .line 2307194
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2307195
    if-eqz v0, :cond_0

    .line 2307196
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2307197
    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSaveMutationModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSaveMutationModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSaveMutationModel$ViewerModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2307198
    :cond_0
    iget-object v0, p0, LX/Fyn;->b:LX/Fyq;

    iget-object v0, v0, LX/Fyq;->d:LX/Fyr;

    .line 2307199
    invoke-virtual {v0}, LX/Fyr;->b()V

    .line 2307200
    :goto_0
    return-void

    .line 2307201
    :cond_1
    iget-object v0, p0, LX/Fyn;->b:LX/Fyq;

    iget-object v1, v0, LX/Fyq;->d:LX/Fyr;

    .line 2307202
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2307203
    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSaveMutationModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSaveMutationModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSaveMutationModel$ViewerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewMutationModels$TimelineInfoReviewQuestionSaveMutationModel$ViewerModel;->a()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Fyr;->a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;)V

    goto :goto_0
.end method
