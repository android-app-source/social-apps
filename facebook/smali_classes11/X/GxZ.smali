.class public LX/GxZ;
.super Landroid/support/v4/widget/SwipeRefreshLayout;
.source ""


# static fields
.field public static final l:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/3zI;

.field public final B:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field public c:LX/2EJ;

.field public d:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0hx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0kv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/performancelogger/PerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public m:LX/GxF;

.field public n:Landroid/view/View;

.field public o:Landroid/view/View;

.field public p:Landroid/widget/FrameLayout;

.field public q:LX/Gu8;

.field public r:LX/GxQ;

.field public s:Landroid/os/Handler;

.field public t:I

.field private u:J

.field public v:Z

.field public w:Z

.field public x:Ljava/lang/String;

.field public y:LX/GxY;

.field private z:LX/GxY;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2408298
    const-class v0, LX/GxZ;

    sput-object v0, LX/GxZ;->l:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2408187
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;)V

    .line 2408188
    iput v2, p0, LX/GxZ;->t:I

    .line 2408189
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/GxZ;->u:J

    .line 2408190
    iput-boolean v2, p0, LX/GxZ;->j:Z

    .line 2408191
    iput-boolean v2, p0, LX/GxZ;->k:Z

    .line 2408192
    iput-boolean v2, p0, LX/GxZ;->v:Z

    .line 2408193
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GxZ;->w:Z

    .line 2408194
    sget-object v0, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    iput-object v0, p0, LX/GxZ;->y:LX/GxY;

    .line 2408195
    sget-object v0, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    iput-object v0, p0, LX/GxZ;->z:LX/GxY;

    .line 2408196
    sget-object v0, LX/3zI;->UNDEFINED:LX/3zI;

    iput-object v0, p0, LX/GxZ;->A:LX/3zI;

    .line 2408197
    new-instance v0, LX/GxO;

    invoke-direct {v0, p0}, LX/GxO;-><init>(LX/GxZ;)V

    iput-object v0, p0, LX/GxZ;->B:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    .line 2408198
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 2408199
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/GxZ;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v7

    check-cast v7, LX/0hx;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v8

    check-cast v8, LX/0So;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/0kv;->a(LX/0QB;)LX/0kv;

    move-result-object p1

    check-cast p1, LX/0kv;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    iput-object v6, v2, LX/GxZ;->d:LX/0Zb;

    iput-object v7, v2, LX/GxZ;->e:LX/0hx;

    iput-object v8, v2, LX/GxZ;->f:LX/0So;

    iput-object v9, v2, LX/GxZ;->g:LX/03V;

    iput-object p1, v2, LX/GxZ;->h:LX/0kv;

    iput-object v0, v2, LX/GxZ;->i:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2408200
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/GxZ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0168

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2408201
    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2408202
    new-array v0, v4, [I

    const v1, 0x7f0a00d1

    aput v1, v0, v5

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 2408203
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/GxZ;->s:Landroid/os/Handler;

    .line 2408204
    new-instance v0, LX/GxP;

    invoke-direct {v0, p0}, LX/GxP;-><init>(LX/GxZ;)V

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2408205
    new-instance v0, LX/GxQ;

    invoke-direct {v0, p0}, LX/GxQ;-><init>(LX/GxZ;)V

    iput-object v0, p0, LX/GxZ;->r:LX/GxQ;

    .line 2408206
    invoke-virtual {p0}, LX/GxZ;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/GxZ;->r:LX/GxQ;

    .line 2408207
    new-instance v2, LX/GxF;

    invoke-direct {v2, v0, v1}, LX/GxF;-><init>(Landroid/content/Context;LX/GxQ;)V

    move-object v0, v2

    .line 2408208
    iput-object v0, p0, LX/GxZ;->m:LX/GxF;

    .line 2408209
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v0, v4}, LX/GxF;->setFocusable(Z)V

    .line 2408210
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v0, v4}, LX/GxF;->setFocusableInTouchMode(Z)V

    .line 2408211
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/GxZ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0168

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2408212
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-le v1, v2, :cond_1

    .line 2408213
    iget-object v1, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v1, v0}, LX/GxF;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2408214
    :goto_0
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v0, v5}, LX/GxF;->setScrollContainer(Z)V

    .line 2408215
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/GxF;->setLongClickable(Z)V

    .line 2408216
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    new-instance v1, LX/GxW;

    invoke-direct {v1, p0}, LX/GxW;-><init>(LX/GxZ;)V

    invoke-virtual {v0, v1}, LX/GxF;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2408217
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    const-string v1, "enablePullToRefresh"

    new-instance v2, LX/GxR;

    invoke-direct {v2, p0}, LX/GxR;-><init>(LX/GxZ;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2408218
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    const-string v1, "disablePullToRefresh"

    new-instance v2, LX/GxS;

    invoke-direct {v2, p0}, LX/GxS;-><init>(LX/GxZ;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2408219
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    const-string v1, "loadCompleted"

    new-instance v2, LX/GxT;

    invoke-direct {v2, p0}, LX/GxT;-><init>(LX/GxZ;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWC;)V

    .line 2408220
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2408221
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, LX/GxZ;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/GxZ;->p:Landroid/widget/FrameLayout;

    .line 2408222
    iget-object v1, p0, LX/GxZ;->p:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1, v0}, LX/GxZ;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2408223
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2408224
    iget-object v1, p0, LX/GxZ;->p:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2408225
    invoke-virtual {p0}, LX/GxZ;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2408226
    const/4 v4, -0x1

    .line 2408227
    const v1, 0x7f0305f2

    iget-object v2, p0, LX/GxZ;->p:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/GxZ;->n:Landroid/view/View;

    .line 2408228
    iget-object v1, p0, LX/GxZ;->n:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2408229
    iget-object v1, p0, LX/GxZ;->p:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/GxZ;->n:Landroid/view/View;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2408230
    const/4 v4, -0x1

    .line 2408231
    const v1, 0x7f0305f1

    iget-object v2, p0, LX/GxZ;->p:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/GxZ;->o:Landroid/view/View;

    .line 2408232
    iget-object v1, p0, LX/GxZ;->o:Landroid/view/View;

    new-instance v2, LX/GxX;

    invoke-direct {v2, p0}, LX/GxX;-><init>(LX/GxZ;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2408233
    iget-object v1, p0, LX/GxZ;->p:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/GxZ;->o:Landroid/view/View;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2408234
    iget-boolean v0, p0, LX/GxZ;->w:Z

    if-nez v0, :cond_0

    .line 2408235
    invoke-static {p0}, LX/GxZ;->i(LX/GxZ;)V

    .line 2408236
    :cond_0
    return-void

    .line 2408237
    :cond_1
    iget-object v1, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v1, v0}, LX/GxF;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method public static i(LX/GxZ;)V
    .locals 1

    .prologue
    .line 2408238
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GxZ;->k:Z

    .line 2408239
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2408240
    return-void
.end method

.method public static setEnable(LX/GxZ;Z)V
    .locals 3

    .prologue
    .line 2408241
    new-instance v0, Lcom/facebook/katana/webview/RefreshableFacewebWebViewContainer$10;

    invoke-direct {v0, p0, p1}, Lcom/facebook/katana/webview/RefreshableFacewebWebViewContainer$10;-><init>(LX/GxZ;Z)V

    .line 2408242
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 2408243
    invoke-virtual {p0, v0}, LX/GxZ;->post(Ljava/lang/Runnable;)Z

    .line 2408244
    :goto_0
    return-void

    .line 2408245
    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/GxY;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 2408246
    iget-object v0, p0, LX/GxZ;->y:LX/GxY;

    if-eq v0, p1, :cond_0

    .line 2408247
    iget-object v0, p0, LX/GxZ;->y:LX/GxY;

    iput-object v0, p0, LX/GxZ;->z:LX/GxY;

    .line 2408248
    :cond_0
    iput-object p1, p0, LX/GxZ;->y:LX/GxY;

    .line 2408249
    sget-object v0, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    if-eq p1, v0, :cond_2

    .line 2408250
    iget-wide v0, p0, LX/GxZ;->u:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GxZ;->e:LX/0hx;

    iget-object v1, p0, LX/GxZ;->f:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, LX/GxZ;->u:J

    sub-long/2addr v2, v4

    iget-object v1, p0, LX/GxZ;->n:Landroid/view/View;

    invoke-virtual {v0, v2, v3, v1}, LX/0hx;->a(JLandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2408251
    iput-wide v8, p0, LX/GxZ;->u:J

    .line 2408252
    :cond_1
    iget-object v0, p0, LX/GxZ;->n:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2408253
    :cond_2
    sget-object v0, LX/GxY;->CONTENT_STATE_WEBVIEW:LX/GxY;

    if-eq p1, v0, :cond_3

    sget-object v0, LX/GxY;->CONTENT_STATE_LOAD_COMPLETE:LX/GxY;

    if-eq p1, v0, :cond_3

    .line 2408254
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v0, v7}, LX/GxF;->setVisibility(I)V

    .line 2408255
    :cond_3
    sget-object v0, LX/GxY;->CONTENT_STATE_ERROR:LX/GxY;

    if-eq p1, v0, :cond_4

    .line 2408256
    iget-object v0, p0, LX/GxZ;->o:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2408257
    :cond_4
    sget-object v0, LX/3zI;->UNDEFINED:LX/3zI;

    .line 2408258
    sget-object v1, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    if-ne p1, v1, :cond_d

    .line 2408259
    iget-object v0, p0, LX/GxZ;->n:Landroid/view/View;

    invoke-static {v0}, LX/0hx;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2408260
    iget-object v0, p0, LX/GxZ;->f:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/GxZ;->u:J

    .line 2408261
    :cond_5
    iget-object v0, p0, LX/GxZ;->n:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2408262
    iget-object v0, p0, LX/GxZ;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 2408263
    sget-object v0, LX/3zI;->LOCAL_DATA:LX/3zI;

    .line 2408264
    :cond_6
    :goto_0
    sget-object v1, LX/3zI;->UNDEFINED:LX/3zI;

    if-eq v0, v1, :cond_a

    .line 2408265
    invoke-virtual {p0}, LX/GxZ;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, p0, LX/GxZ;->A:LX/3zI;

    if-eq v0, v1, :cond_9

    .line 2408266
    iget-object v1, p0, LX/GxZ;->e:LX/0hx;

    invoke-virtual {p0}, LX/GxZ;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2408267
    iget-object v3, p0, LX/GxZ;->x:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 2408268
    check-cast v2, Landroid/app/Activity;

    .line 2408269
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2408270
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2408271
    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-static {v4}, LX/Ehu;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    .line 2408272
    if-eqz v4, :cond_7

    .line 2408273
    const/16 v5, 0x40

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2408274
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2408275
    :cond_7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, LX/GxZ;->x:Ljava/lang/String;

    .line 2408276
    :cond_8
    iget-object v3, p0, LX/GxZ;->x:Ljava/lang/String;

    move-object v2, v3

    .line 2408277
    iget-object v3, p0, LX/GxZ;->h:LX/0kv;

    invoke-virtual {p0}, LX/GxZ;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/0hx;->a(LX/3zI;Ljava/lang/String;Ljava/lang/String;)V

    .line 2408278
    :cond_9
    iput-object v0, p0, LX/GxZ;->A:LX/3zI;

    .line 2408279
    :cond_a
    sget-object v0, LX/GxY;->CONTENT_STATE_WEBVIEW:LX/GxY;

    if-eq p1, v0, :cond_b

    sget-object v0, LX/GxY;->CONTENT_STATE_ERROR:LX/GxY;

    if-eq p1, v0, :cond_b

    sget-object v0, LX/GxY;->CONTENT_STATE_LOAD_COMPLETE:LX/GxY;

    if-ne p1, v0, :cond_c

    .line 2408280
    :cond_b
    invoke-virtual {p0, v6}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2408281
    :cond_c
    return-void

    .line 2408282
    :cond_d
    sget-object v1, LX/GxY;->CONTENT_STATE_ERROR:LX/GxY;

    if-ne p1, v1, :cond_f

    .line 2408283
    iget-object v0, p0, LX/GxZ;->o:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2408284
    iget-object v0, p0, LX/GxZ;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 2408285
    iget-object v0, p0, LX/GxZ;->d:LX/0Zb;

    sget-object v1, LX/CIr;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2408286
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 2408287
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2408288
    :cond_e
    sget-object v0, LX/3zI;->NETWORK_DATA:LX/3zI;

    goto/16 :goto_0

    .line 2408289
    :cond_f
    iget-object v1, p0, LX/GxZ;->z:LX/GxY;

    sget-object v2, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    if-ne v1, v2, :cond_6

    sget-object v1, LX/GxY;->CONTENT_STATE_WEBVIEW:LX/GxY;

    if-eq p1, v1, :cond_10

    sget-object v1, LX/GxY;->CONTENT_STATE_LOAD_COMPLETE:LX/GxY;

    if-ne p1, v1, :cond_6

    .line 2408290
    :cond_10
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v0, v6}, LX/GxF;->setVisibility(I)V

    .line 2408291
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->bringToFront()V

    .line 2408292
    sget-object v0, LX/3zI;->NETWORK_DATA:LX/3zI;

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2408293
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v0, v1, v1}, LX/GxF;->scrollTo(II)V

    .line 2408294
    invoke-static {p0}, LX/GxZ;->i(LX/GxZ;)V

    .line 2408295
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    invoke-virtual {v0}, LX/GxF;->c()V

    .line 2408296
    iget-object v0, p0, LX/GxZ;->m:LX/GxF;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/GxF;->setVerticalScrollBarEnabled(Z)V

    .line 2408297
    return-void
.end method
