.class public final LX/GcX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/resources/ui/FbEditText;

.field public final synthetic b:LX/GcZ;


# direct methods
.method public constructor <init>(LX/GcZ;Lcom/facebook/resources/ui/FbEditText;)V
    .locals 0

    .prologue
    .line 2372013
    iput-object p1, p0, LX/GcX;->b:LX/GcZ;

    iput-object p2, p0, LX/GcX;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2372014
    iget-object v0, p0, LX/GcX;->b:LX/GcZ;

    iget-object v0, v0, LX/GcZ;->b:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ReportIssue:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/GcX;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2372015
    iget-object v0, p0, LX/GcX;->b:LX/GcZ;

    iget-object v0, v0, LX/GcZ;->b:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    invoke-static {v0}, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->m(Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;)V

    .line 2372016
    return-void
.end method
