.class public final LX/Gyl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GyR;

.field public final synthetic b:Lcom/facebook/location/ui/LocationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/location/ui/LocationSettingsFragment;LX/GyR;)V
    .locals 0

    .prologue
    .line 2410347
    iput-object p1, p0, LX/Gyl;->b:Lcom/facebook/location/ui/LocationSettingsFragment;

    iput-object p2, p0, LX/Gyl;->a:LX/GyR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x2ff43aae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2410348
    iget-object v1, p0, LX/Gyl;->b:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v2, p0, LX/Gyl;->a:LX/GyR;

    .line 2410349
    :try_start_0
    invoke-interface {v2}, LX/GyR;->e()Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/PendingIntent;->send()V

    .line 2410350
    iget-object v4, v1, Lcom/facebook/location/ui/LocationSettingsFragment;->o:LX/GyW;

    invoke-interface {v2}, LX/GyR;->d()Ljava/lang/String;

    move-result-object p0

    .line 2410351
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "location_settings_device_settings_click"

    invoke-direct {p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "background_location"

    .line 2410352
    iput-object v2, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2410353
    move-object p1, p1

    .line 2410354
    const-string v2, "setting_id"

    invoke-virtual {p1, v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2410355
    iget-object v2, v4, LX/GyW;->a:LX/0Zb;

    invoke-interface {v2, p1}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2410356
    :goto_0
    const v1, 0x7b60ec18

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2410357
    :catch_0
    move-exception v4

    .line 2410358
    sget-object p0, Lcom/facebook/location/ui/LocationSettingsFragment;->a:Ljava/lang/Class;

    const-string p1, "Tip returned a PendingIntent that was cancelled"

    invoke-static {p0, p1, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2410359
    sget-object v4, LX/Gym;->ERROR:LX/Gym;

    invoke-static {v1, v4}, Lcom/facebook/location/ui/LocationSettingsFragment;->a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;LX/Gym;)V

    goto :goto_0
.end method
