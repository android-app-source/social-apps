.class public LX/FWt;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/11i;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>(LX/11i;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 2252827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252828
    iput v0, p0, LX/FWt;->b:I

    .line 2252829
    iput v0, p0, LX/FWt;->c:I

    .line 2252830
    iput-object p1, p0, LX/FWt;->a:LX/11i;

    .line 2252831
    return-void
.end method

.method public static a(LX/0QB;)LX/FWt;
    .locals 4

    .prologue
    .line 2252816
    const-class v1, LX/FWt;

    monitor-enter v1

    .line 2252817
    :try_start_0
    sget-object v0, LX/FWt;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2252818
    sput-object v2, LX/FWt;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2252819
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2252820
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2252821
    new-instance p0, LX/FWt;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-direct {p0, v3}, LX/FWt;-><init>(LX/11i;)V

    .line 2252822
    move-object v0, p0

    .line 2252823
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2252824
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FWt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2252825
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2252826
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/FWt;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2252811
    const/4 v0, 0x0

    .line 2252812
    invoke-static {p0}, LX/FWt;->l(LX/FWt;)LX/11o;

    move-result-object v1

    .line 2252813
    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2252814
    const/4 v2, 0x0

    const v3, 0x50342d35

    invoke-static {v1, p1, v2, v0, v3}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2252815
    :cond_0
    return-void
.end method

.method public static b(LX/FWt;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2252809
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FWt;->b(Ljava/lang/String;LX/0P1;)V

    .line 2252810
    return-void
.end method

.method private b(Ljava/lang/String;LX/0P1;)V
    .locals 3
    .param p2    # LX/0P1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2252832
    invoke-static {p0}, LX/FWt;->l(LX/FWt;)LX/11o;

    move-result-object v0

    .line 2252833
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2252834
    const/4 v1, 0x0

    const v2, 0x2bb6186a

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2252835
    :cond_0
    return-void
.end method

.method public static l(LX/FWt;)LX/11o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/11o",
            "<",
            "LX/FWr;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2252808
    iget-object v0, p0, LX/FWt;->a:LX/11i;

    sget-object v1, LX/FWs;->a:LX/FWr;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 2252806
    const-string v0, "DASH_ITEMS_LOAD_FROM_NETWORK"

    invoke-static {p0, v0}, LX/FWt;->b(LX/FWt;Ljava/lang/String;)V

    .line 2252807
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 2252794
    const-string v1, "DASH_START"

    const-string v2, "item_count"

    .line 2252795
    iget v0, p0, LX/FWt;->c:I

    if-ltz v0, :cond_1

    iget v0, p0, LX/FWt;->c:I

    :goto_0
    move v0, v0

    .line 2252796
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "freshness"

    .line 2252797
    iget v0, p0, LX/FWt;->b:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2252798
    if-eqz v0, :cond_0

    const-string v0, "FROM_CACHE"

    :goto_2
    invoke-static {v2, v3, v4, v0}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    invoke-direct {p0, v1, v0}, LX/FWt;->b(Ljava/lang/String;LX/0P1;)V

    .line 2252799
    iget-object v0, p0, LX/FWt;->a:LX/11i;

    sget-object v1, LX/FWs;->a:LX/FWr;

    invoke-interface {v0, v1}, LX/11i;->b(LX/0Pq;)V

    .line 2252800
    return-void

    .line 2252801
    :cond_0
    const-string v0, "FROM_NETWORK"

    goto :goto_2

    :cond_1
    iget v0, p0, LX/FWt;->b:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2252804
    const-string v0, "DASH_TABS_LOAD_FROM_CACHE"

    invoke-static {p0, v0}, LX/FWt;->a(LX/FWt;Ljava/lang/String;)V

    .line 2252805
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 2252802
    const-string v0, "DASH_TABS_LOAD_FROM_NETWORK"

    invoke-static {p0, v0}, LX/FWt;->b(LX/FWt;Ljava/lang/String;)V

    .line 2252803
    return-void
.end method
