.class public LX/Ffp;
.super LX/2s5;
.source ""


# instance fields
.field private final a:LX/7BH;

.field public final b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

.field private final c:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

.field private final d:LX/8ci;

.field private final e:LX/FfW;

.field private final f:LX/FZb;

.field private final g:LX/CyE;

.field private final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0hL;

.field private final j:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;LX/7BH;Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;LX/FZb;LX/CyE;LX/0Px;LX/FfW;LX/0hL;)V
    .locals 1
    .param p1    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/7BH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/search/model/KeywordTypeaheadUnit;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/search/logging/api/SearchTypeaheadSession;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/8ci;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/FZb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/CyE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "LX/7BH;",
            "Lcom/facebook/search/model/KeywordTypeaheadUnit;",
            "Lcom/facebook/search/logging/api/SearchTypeaheadSession;",
            "LX/8ci;",
            "Lcom/facebook/search/fragment/GraphSearchChildFragment$OnResultClickListener;",
            "LX/CyE;",
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;",
            "LX/FfW;",
            "LX/0hL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2269100
    invoke-direct {p0, p1}, LX/2s5;-><init>(LX/0gc;)V

    .line 2269101
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/Ffp;->j:LX/0YU;

    .line 2269102
    iput-object p2, p0, LX/Ffp;->a:LX/7BH;

    .line 2269103
    iput-object p3, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2269104
    iput-object p4, p0, LX/Ffp;->c:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2269105
    iput-object p5, p0, LX/Ffp;->d:LX/8ci;

    .line 2269106
    iput-object p9, p0, LX/Ffp;->e:LX/FfW;

    .line 2269107
    iput-object p6, p0, LX/Ffp;->f:LX/FZb;

    .line 2269108
    iput-object p7, p0, LX/Ffp;->g:LX/CyE;

    .line 2269109
    iput-object p8, p0, LX/Ffp;->h:LX/0Px;

    .line 2269110
    iput-object p10, p0, LX/Ffp;->i:LX/0hL;

    .line 2269111
    return-void
.end method

.method public static g(LX/Ffp;I)LX/FfQ;
    .locals 2

    .prologue
    .line 2269112
    iget-object v1, p0, LX/Ffp;->e:LX/FfW;

    iget-object v0, p0, LX/Ffp;->h:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyI;

    invoke-virtual {v1, v0}, LX/FfW;->a(LX/CyI;)LX/FfQ;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/Ffp;I)I
    .locals 1

    .prologue
    .line 2269095
    iget-object v0, p0, LX/Ffp;->i:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    :cond_0
    return p1
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2269096
    invoke-static {p0, p1}, LX/Ffp;->h(LX/Ffp;I)I

    move-result v0

    .line 2269097
    invoke-static {p0, v0}, LX/Ffp;->g(LX/Ffp;I)LX/FfQ;

    move-result-object v0

    .line 2269098
    iget-object p0, v0, LX/FfQ;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2269099
    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 5

    .prologue
    .line 2269051
    invoke-static {p0, p1}, LX/Ffp;->h(LX/Ffp;I)I

    move-result v2

    .line 2269052
    iget-object v0, p0, LX/Ffp;->j:LX/0YU;

    invoke-virtual {v0, v2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 2269053
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2269054
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2269055
    :goto_0
    return-object v0

    .line 2269056
    :cond_0
    iget-object v0, p0, LX/Ffp;->h:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyI;

    .line 2269057
    iget-object v1, p0, LX/Ffp;->e:LX/FfW;

    invoke-virtual {v1, v0}, LX/FfW;->a(LX/CyI;)LX/FfQ;

    move-result-object v0

    .line 2269058
    instance-of v1, v0, LX/Ffh;

    if-eqz v1, :cond_2

    .line 2269059
    check-cast v0, LX/Ffh;

    iget-object v1, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jA_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ffh;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    move-result-object v1

    .line 2269060
    :goto_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2269061
    const-string v3, "search_theme"

    iget-object v4, p0, LX/Ffp;->a:LX/7BH;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2269062
    const-string v3, "tab_bar_tap"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2269063
    invoke-interface {v1}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2269064
    iget-object v0, p0, LX/Ffp;->f:LX/FZb;

    invoke-interface {v1, v0}, LX/FZU;->a(LX/FZb;)V

    .line 2269065
    instance-of v0, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2269066
    check-cast v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v3, p0, LX/Ffp;->g:LX/CyE;

    .line 2269067
    iput-object v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aC:LX/CyE;

    .line 2269068
    :cond_1
    :goto_2
    iget-object v0, p0, LX/Ffp;->j:LX/0YU;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-interface {v1}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 2269069
    invoke-interface {v1}, LX/FZU;->d()Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0

    .line 2269070
    :cond_2
    instance-of v1, v0, LX/Fff;

    if-eqz v1, :cond_3

    .line 2269071
    check-cast v0, LX/Fff;

    iget-object v1, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jA_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fff;->a(Ljava/lang/String;)Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;

    move-result-object v1

    goto :goto_1

    .line 2269072
    :cond_3
    invoke-virtual {v0}, LX/FfQ;->b()LX/Fdv;

    move-result-object v1

    goto :goto_1

    .line 2269073
    :cond_4
    instance-of v0, v1, Lcom/facebook/search/results/fragment/SearchResultsFragment;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2269074
    check-cast v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v3, p0, LX/Ffp;->g:LX/CyE;

    .line 2269075
    iput-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->P:LX/CyE;

    .line 2269076
    goto :goto_2
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2269077
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 2269078
    iget-object v1, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2269079
    iget-object v2, v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j:Ljava/lang/String;

    move-object v1, v2

    .line 2269080
    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 2269081
    check-cast v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    new-instance v2, LX/CzK;

    iget-object v3, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v3}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2269082
    iget-object p1, v4, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j:Ljava/lang/String;

    move-object v4, p1

    .line 2269083
    invoke-direct {v2, v3, v4}, LX/CzK;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2269084
    iput-object v2, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ai:LX/CzK;

    .line 2269085
    move-object v1, v0

    .line 2269086
    check-cast v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v2, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2269087
    iget-object v3, v2, Lcom/facebook/search/model/KeywordTypeaheadUnit;->u:Ljava/lang/String;

    move-object v2, v3

    .line 2269088
    iput-object v2, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aj:Ljava/lang/String;

    .line 2269089
    :cond_0
    invoke-static {p0, p2}, LX/Ffp;->g(LX/Ffp;I)LX/FfQ;

    move-result-object v1

    .line 2269090
    invoke-static {p0, p2}, LX/Ffp;->h(LX/Ffp;I)I

    move-result v2

    .line 2269091
    invoke-static {p0, v2}, LX/Ffp;->g(LX/Ffp;I)LX/FfQ;

    move-result-object v2

    iget-object v3, p0, LX/Ffp;->b:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/FfQ;->c(LX/CwB;Ljava/lang/String;)LX/CwA;

    move-result-object v2

    invoke-interface {v2}, LX/CwA;->a()LX/CwB;

    move-result-object v2

    move-object v2, v2

    .line 2269092
    iget-object v3, p0, LX/Ffp;->c:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iget-object v4, p0, LX/Ffp;->d:LX/8ci;

    invoke-virtual {v1, v0, v2, v3, v4}, LX/FfQ;->a(Landroid/support/v4/app/Fragment;LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2269093
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2269094
    iget-object v0, p0, LX/Ffp;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
