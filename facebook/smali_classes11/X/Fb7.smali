.class public LX/Fb7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field public b:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(LX/03V;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2259579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259580
    iput-object p1, p0, LX/Fb7;->a:LX/03V;

    .line 2259581
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/Fb7;->b:Ljava/lang/StringBuilder;

    .line 2259582
    return-void
.end method

.method public static a(LX/Fb7;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2259583
    iget-object v0, p0, LX/Fb7;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2259584
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;)Z
    .locals 10

    .prologue
    .line 2259585
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2259586
    const/4 v0, 0x0

    .line 2259587
    :goto_0
    return v0

    .line 2259588
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2259589
    const/4 v0, 0x1

    goto :goto_0

    .line 2259590
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxConfigurationModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2259591
    if-nez v0, :cond_4

    .line 2259592
    const-string v2, "Carousel configuration is null.\n"

    invoke-static {p0, v2}, LX/Fb7;->a(LX/Fb7;Ljava/lang/String;)V

    .line 2259593
    :cond_2
    :goto_1
    move v0, v1

    .line 2259594
    if-nez v0, :cond_3

    .line 2259595
    iget-object v1, p0, LX/Fb7;->a:LX/03V;

    const-string v2, "SearchAwareness"

    iget-object v3, p0, LX/Fb7;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2259596
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, LX/Fb7;->b:Ljava/lang/StringBuilder;

    .line 2259597
    goto :goto_0

    .line 2259598
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->j()LX/2uF;

    move-result-object v3

    invoke-virtual {v3}, LX/39O;->a()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2259599
    const-string v3, "Carousel must have at least one card/\n"

    invoke-static {p0, v3}, LX/Fb7;->a(LX/Fb7;Ljava/lang/String;)V

    move v3, v1

    .line 2259600
    :goto_2
    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->j()LX/2uF;

    move-result-object v3

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2259601
    invoke-virtual {v3}, LX/39O;->a()Z

    move-result v4

    if-nez v4, :cond_6

    move v4, v5

    .line 2259602
    :goto_3
    if-nez v4, :cond_5

    .line 2259603
    const-string v7, "Carousel must have at least one card.\n"

    invoke-static {p0, v7}, LX/Fb7;->a(LX/Fb7;Ljava/lang/String;)V

    .line 2259604
    :cond_5
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    .line 2259605
    if-eqz v4, :cond_7

    .line 2259606
    sget-object p1, LX/Fb6;->a:[I

    const/16 v4, 0x8

    const-class v0, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-virtual {v9, v8, v4, v0, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTutorialNUXTemplate;->ordinal()I

    move-result v4

    aget v4, p1, v4

    packed-switch v4, :pswitch_data_0

    .line 2259607
    const-string v4, "Unsupported template.\n"

    invoke-static {p0, v4}, LX/Fb7;->a(LX/Fb7;Ljava/lang/String;)V

    .line 2259608
    const/4 v4, 0x0

    :goto_5
    move v4, v4

    .line 2259609
    if-eqz v4, :cond_7

    move v4, v5

    goto :goto_4

    :cond_6
    move v4, v6

    .line 2259610
    goto :goto_3

    :cond_7
    move v4, v6

    .line 2259611
    goto :goto_4

    .line 2259612
    :cond_8
    move v3, v4

    .line 2259613
    if-eqz v3, :cond_2

    move v1, v2

    goto :goto_1

    :cond_9
    move v3, v2

    goto :goto_2

    .line 2259614
    :pswitch_0
    const/4 v0, 0x1

    const/4 p1, 0x0

    .line 2259615
    invoke-virtual {v9, v8, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2259616
    const-string v4, "CARD spotlight template is missing required body text."

    invoke-static {p0, v4}, LX/Fb7;->a(LX/Fb7;Ljava/lang/String;)V

    move v4, p1

    .line 2259617
    :goto_6
    invoke-virtual {v9, v8, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2259618
    const-string v4, "CARD spotlight template is missing required background color value.\n"

    invoke-static {p0, v4}, LX/Fb7;->a(LX/Fb7;Ljava/lang/String;)V

    .line 2259619
    :goto_7
    move v4, p1

    .line 2259620
    goto :goto_5

    .line 2259621
    :pswitch_1
    const/4 v4, 0x0

    .line 2259622
    invoke-virtual {v9, v8, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_c

    .line 2259623
    const-string p1, "INTRO spotlight template is missing required body text."

    invoke-static {p0, p1}, LX/Fb7;->a(LX/Fb7;Ljava/lang/String;)V

    .line 2259624
    :goto_8
    move v4, v4

    .line 2259625
    goto :goto_5

    :cond_a
    move p1, v4

    goto :goto_7

    :cond_b
    move v4, v0

    goto :goto_6

    :cond_c
    const/4 v4, 0x1

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
