.class public LX/Fer;
.super LX/1LN;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LX/CzE;

.field private final b:LX/1K9;


# direct methods
.method public constructor <init>(LX/CzE;LX/1K9;)V
    .locals 0
    .param p1    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2266784
    invoke-direct {p0}, LX/1LN;-><init>()V

    .line 2266785
    iput-object p1, p0, LX/Fer;->a:LX/CzE;

    .line 2266786
    iput-object p2, p0, LX/Fer;->b:LX/1K9;

    .line 2266787
    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2266788
    check-cast p1, LX/1Zf;

    .line 2266789
    iget-object v0, p0, LX/Fer;->a:LX/CzE;

    iget-object v1, p1, LX/1Zf;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CzE;->a(Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2266790
    if-nez v0, :cond_0

    .line 2266791
    :goto_0
    return-void

    .line 2266792
    :cond_0
    iget-object v1, p0, LX/Fer;->b:LX/1K9;

    new-instance v2, LX/EJ3;

    iget-object v3, p1, LX/1Zf;->a:Ljava/lang/String;

    iget-object v4, p1, LX/1Zf;->c:LX/1zt;

    invoke-direct {v2, v0, v3, v4}, LX/EJ3;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/1zt;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_0
.end method
