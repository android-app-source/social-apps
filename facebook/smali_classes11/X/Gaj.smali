.class public final enum LX/Gaj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gaj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gaj;

.field public static final enum FETCH_ALL:LX/Gaj;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2368619
    new-instance v0, LX/Gaj;

    const-string v1, "FETCH_ALL"

    invoke-direct {v0, v1, v2}, LX/Gaj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gaj;->FETCH_ALL:LX/Gaj;

    .line 2368620
    const/4 v0, 0x1

    new-array v0, v0, [LX/Gaj;

    sget-object v1, LX/Gaj;->FETCH_ALL:LX/Gaj;

    aput-object v1, v0, v2

    sput-object v0, LX/Gaj;->$VALUES:[LX/Gaj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2368621
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gaj;
    .locals 1

    .prologue
    .line 2368618
    const-class v0, LX/Gaj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gaj;

    return-object v0
.end method

.method public static values()[LX/Gaj;
    .locals 1

    .prologue
    .line 2368617
    sget-object v0, LX/Gaj;->$VALUES:[LX/Gaj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gaj;

    return-object v0
.end method
