.class public final LX/HDz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 0

    .prologue
    .line 2441368
    iput-object p1, p0, LX/HDz;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iput-object p2, p0, LX/HDz;->a:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iput-object p3, p0, LX/HDz;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2441366
    iget-object v0, p0, LX/HDz;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v1, p0, LX/HDz;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-boolean v1, v1, LX/HE1;->a:Z

    iget-object v2, p0, LX/HDz;->a:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    iget-object v3, p0, LX/HDz;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a$redex0(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2441367
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2441369
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2441370
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2441371
    check-cast v0, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel;

    .line 2441372
    iget-object v1, p0, LX/HDz;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v2, p0, LX/HDz;->c:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-boolean v2, v2, LX/HE1;->a:Z

    invoke-virtual {v0}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel;->a()Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->a()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel;->a()Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/followpage/graphql/PageNewsFeedStatusMutationModels$PageNewsFeedStatusMutationModel$PageModel;->j()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a$redex0(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2441373
    return-void
.end method
