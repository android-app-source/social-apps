.class public final LX/F4k;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V
    .locals 0

    .prologue
    .line 2197154
    iput-object p1, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2197155
    sget-object v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->a:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2197156
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    invoke-static {v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->t(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    .line 2197157
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->i:LX/1g8;

    .line 2197158
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "creation_create_failed"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "group_creation"

    .line 2197159
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2197160
    move-object v1, v1

    .line 2197161
    iget-object v2, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2197162
    invoke-static {v0, v1}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2197163
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, LX/4Ua;

    if-eqz v0, :cond_0

    .line 2197164
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/4Ua;

    .line 2197165
    new-instance v1, LX/27k;

    .line 2197166
    iget-object v2, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v2

    .line 2197167
    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 2197168
    :goto_0
    iget-object v1, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->b:LX/0kL;

    invoke-virtual {v1, v0}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2197169
    return-void

    .line 2197170
    :cond_0
    new-instance v0, LX/27k;

    const v1, 0x7f082f57

    invoke-direct {v0, v1}, LX/27k;-><init>(I)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2197171
    check-cast p1, Ljava/lang/String;

    .line 2197172
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082f5c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2197173
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->H:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2197174
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->m:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v1, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->H:Ljava/lang/String;

    iget-object v2, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->E:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197175
    :cond_0
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->i:LX/1g8;

    .line 2197176
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "creation_create_succeeded"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "group_creation"

    .line 2197177
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2197178
    move-object v1, v1

    .line 2197179
    iget-object v2, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2197180
    invoke-static {v0, v1}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2197181
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->F:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->G:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2197182
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->i:LX/1g8;

    iget-object v1, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->F:Ljava/lang/String;

    iget-object v2, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->G:Ljava/lang/String;

    iget-object v3, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v3, v3, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->D:Ljava/lang/String;

    iget-object v4, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v4, v4, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->E:Ljava/lang/String;

    iget-object v5, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v5, v5, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197183
    iget-object v6, v5, LX/F53;->o:Ljava/lang/String;

    move-object v5, v6

    .line 2197184
    if-eqz v1, :cond_1

    if-nez v2, :cond_3

    .line 2197185
    :cond_1
    :goto_0
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->E:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->D:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2197186
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->l:LX/DKO;

    new-instance v1, LX/DKN;

    iget-object v2, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->E:Ljava/lang/String;

    iget-object v3, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    iget-object v3, v3, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->D:Ljava/lang/String;

    invoke-direct {v1, p1, v2, v3}, LX/DKN;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2197187
    :cond_2
    iget-object v0, p0, LX/F4k;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    .line 2197188
    invoke-static {v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->t(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    .line 2197189
    iget-boolean v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->J:Z

    if-eqz v1, :cond_4

    .line 2197190
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2197191
    const-string v2, "group_name"

    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v3}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2197192
    const-string v2, "group_feed_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2197193
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 2197194
    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2197195
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2197196
    :goto_1
    return-void

    .line 2197197
    :cond_3
    new-instance v6, LX/162;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/162;-><init>(LX/0mC;)V

    .line 2197198
    invoke-virtual {v6, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2197199
    invoke-virtual {v6, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2197200
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "gysc_create"

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "tracking"

    invoke-virtual {v7, v8, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "suggestion_type"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "suggestion_id"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "ref"

    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "native_newsfeed"

    .line 2197201
    iput-object v7, v6, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2197202
    move-object v6, v6

    .line 2197203
    iget-object v7, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2197204
    invoke-static {v0, v6}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_0

    .line 2197205
    :cond_4
    sget-object v1, LX/0cQ;->GROUPS_MALL_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    .line 2197206
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2197207
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2197208
    const-string v3, "group_feed_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2197209
    const-string v3, "target_fragment"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2197210
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method
