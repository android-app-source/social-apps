.class public final LX/F12;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLUser;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;Lcom/facebook/graphql/model/GraphQLUser;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2191073
    iput-object p1, p0, LX/F12;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

    iput-object p2, p0, LX/F12;->a:Lcom/facebook/graphql/model/GraphQLUser;

    iput-object p3, p0, LX/F12;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x43b3de6a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2191074
    iget-object v1, p0, LX/F12;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;

    iget-object v2, p0, LX/F12;->a:Lcom/facebook/graphql/model/GraphQLUser;

    iget-object v3, p0, LX/F12;->b:Ljava/lang/String;

    const/4 v9, 0x0

    .line 2191075
    sget-object v5, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 2191076
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2191077
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v8

    move-object v7, v3

    move-object v10, v9

    invoke-static/range {v5 .. v10}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 2191078
    iget-object v6, v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendversaryAggregatedFriendListItemPartDefinition;->c:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v6, v7, v11, v5}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2191079
    const v1, 0xf14dac2

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
