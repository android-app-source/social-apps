.class public LX/F9t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/F9t;


# instance fields
.field public final a:LX/F9s;


# direct methods
.method public constructor <init>(LX/F9s;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206296
    iput-object p1, p0, LX/F9t;->a:LX/F9s;

    .line 2206297
    return-void
.end method

.method public static a(LX/0QB;)LX/F9t;
    .locals 4

    .prologue
    .line 2206298
    sget-object v0, LX/F9t;->b:LX/F9t;

    if-nez v0, :cond_1

    .line 2206299
    const-class v1, LX/F9t;

    monitor-enter v1

    .line 2206300
    :try_start_0
    sget-object v0, LX/F9t;->b:LX/F9t;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2206301
    if-eqz v2, :cond_0

    .line 2206302
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2206303
    new-instance p0, LX/F9t;

    invoke-static {v0}, LX/F9s;->a(LX/0QB;)LX/F9s;

    move-result-object v3

    check-cast v3, LX/F9s;

    invoke-direct {p0, v3}, LX/F9t;-><init>(LX/F9s;)V

    .line 2206304
    move-object v0, p0

    .line 2206305
    sput-object v0, LX/F9t;->b:LX/F9t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206306
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2206307
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2206308
    :cond_1
    sget-object v0, LX/F9t;->b:LX/F9t;

    return-object v0

    .line 2206309
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2206310
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
