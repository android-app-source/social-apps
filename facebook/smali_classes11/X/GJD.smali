.class public abstract LX/GJD;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

.field public b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2337934
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2337935
    invoke-virtual {p0}, LX/GJD;->a()V

    .line 2337936
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2337937
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2337938
    invoke-virtual {p0}, LX/GJD;->a()V

    .line 2337939
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2337943
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2337944
    invoke-virtual {p0}, LX/GJD;->a()V

    .line 2337945
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 2337940
    if-ltz p2, :cond_0

    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 2337941
    :cond_0
    :goto_0
    return-void

    .line 2337942
    :cond_1
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setContentDescriptionTextViewEnd(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V
    .locals 2

    .prologue
    .line 2337925
    if-ltz p3, :cond_0

    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p3, v0, :cond_1

    .line 2337926
    :cond_0
    :goto_0
    return-void

    .line 2337927
    :cond_1
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2337928
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2337929
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0, p2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewEnd(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 2337922
    if-gez p1, :cond_0

    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2337923
    :cond_0
    :goto_0
    return-void

    .line 2337924
    :cond_1
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2337930
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2337931
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    .line 2337932
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2337933
    :cond_0
    return-void
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 2337919
    if-gez p1, :cond_0

    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2337920
    :cond_0
    :goto_0
    return-void

    .line 2337921
    :cond_1
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2337915
    iget-object v0, p0, LX/GJD;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextEditText(Ljava/lang/CharSequence;)V

    .line 2337916
    iget-object v0, p0, LX/GJD;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setChecked(Z)V

    .line 2337917
    iget-object v0, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a()V

    .line 2337918
    return-void
.end method

.method public final f(I)V
    .locals 2

    .prologue
    .line 2337912
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2337913
    :cond_0
    :goto_0
    return-void

    .line 2337914
    :cond_1
    iget-object v0, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v1, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v1, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    goto :goto_0
.end method

.method public getSelectedIndex()I
    .locals 2

    .prologue
    .line 2337906
    iget-object v0, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iget-object v1, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2337907
    iget p0, v1, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    move v1, p0

    .line 2337908
    invoke-virtual {v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2337909
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2337910
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2337911
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setOnCheckChangedListener(LX/Bc9;)V
    .locals 1

    .prologue
    .line 2337903
    iget-object v0, p0, LX/GJD;->a:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 2337904
    iput-object p1, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 2337905
    return-void
.end method

.method public setRadioButtonClickListeners(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2337899
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2337900
    iget-object v0, p0, LX/GJD;->c:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2337901
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2337902
    :cond_0
    return-void
.end method
