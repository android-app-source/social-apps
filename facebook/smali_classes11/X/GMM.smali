.class public LX/GMM;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/GKy;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/GG6;

.field private final d:Landroid/content/res/Resources;

.field public e:I

.field public f:I

.field private g:Ljava/lang/String;

.field public h:Ljava/lang/Runnable;

.field private i:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

.field public j:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;


# direct methods
.method public constructor <init>(LX/GKy;LX/0Or;LX/GG6;Landroid/content/res/Resources;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GKy;",
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;",
            "LX/GG6;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2344398
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2344399
    iput v0, p0, LX/GMM;->e:I

    .line 2344400
    iput v0, p0, LX/GMM;->f:I

    .line 2344401
    iput-object p1, p0, LX/GMM;->a:LX/GKy;

    .line 2344402
    iput-object p2, p0, LX/GMM;->b:LX/0Or;

    .line 2344403
    iput-object p3, p0, LX/GMM;->c:LX/GG6;

    .line 2344404
    iput-object p4, p0, LX/GMM;->d:Landroid/content/res/Resources;

    .line 2344405
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2344406
    invoke-super {p0}, LX/GHg;->a()V

    .line 2344407
    iget-object v0, p0, LX/GMM;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 2344408
    if-eqz v0, :cond_0

    .line 2344409
    iget-object v1, p0, LX/GMM;->h:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2344410
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/GMM;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    .line 2344411
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 8
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2344412
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    const/4 v3, 0x0

    .line 2344413
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2344414
    iput-object p1, p0, LX/GMM;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    .line 2344415
    if-nez p2, :cond_1

    .line 2344416
    :cond_0
    :goto_0
    return-void

    .line 2344417
    :cond_1
    iget v0, p0, LX/GMM;->e:I

    if-nez v0, :cond_2

    iget v0, p0, LX/GMM;->f:I

    if-nez v0, :cond_2

    .line 2344418
    invoke-virtual {p1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setColumnsActive(Z)V

    .line 2344419
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2344420
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionText(Ljava/lang/String;)V

    .line 2344421
    const v1, 0x7f080a3f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    goto :goto_0

    .line 2344422
    :cond_2
    iget v0, p0, LX/GMM;->f:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/GMM;->d:Landroid/content/res/Resources;

    const v2, 0x7f080aa8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344423
    iget v0, p0, LX/GMM;->e:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/GMM;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2344424
    iget-object v0, p0, LX/GMM;->i:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 2344425
    iget v4, p0, LX/GMM;->e:I

    if-eqz v4, :cond_3

    iget v4, p0, LX/GMM;->f:I

    if-nez v4, :cond_4

    .line 2344426
    :cond_3
    :goto_1
    new-instance v0, LX/GML;

    invoke-direct {v0, p0}, LX/GML;-><init>(LX/GMM;)V

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2344427
    iget-object v0, p0, LX/GMM;->j:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->LINK_CLICKS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    if-ne v0, v1, :cond_0

    .line 2344428
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080ba2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 2344429
    :cond_4
    iget-object v4, p0, LX/GMM;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0iA;

    .line 2344430
    new-instance v5, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v6, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->AD_INTERFACES_VIEW_RESULTS:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v5, v6}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v6, LX/2tl;

    invoke-virtual {v4, v5, v6}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v5

    check-cast v5, LX/2tl;

    .line 2344431
    if-eqz v5, :cond_3

    .line 2344432
    new-instance v6, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;

    invoke-direct {v6, p0, v5, p2, v4}, Lcom/facebook/adinterfaces/ui/BoostedComponentResultsViewController$2;-><init>(LX/GMM;LX/2tl;Landroid/view/View;LX/0iA;)V

    iput-object v6, p0, LX/GMM;->h:Ljava/lang/Runnable;

    .line 2344433
    if-eqz v0, :cond_3

    .line 2344434
    iget-object v4, p0, LX/GMM;->h:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    const v5, 0x5bd5cbf3

    invoke-static {v0, v4, v6, v7, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 2

    .prologue
    .line 2344435
    iput-object p1, p0, LX/GMM;->j:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2344436
    invoke-static {p1}, LX/GG7;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/util/List;

    move-result-object v1

    .line 2344437
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/GMM;->e:I

    .line 2344438
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/GMM;->f:I

    .line 2344439
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/GMM;->g:Ljava/lang/String;

    .line 2344440
    return-void
.end method
