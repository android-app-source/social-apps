.class public final LX/HCX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/editpage/graphql/SetPageTemplateTypeMutationModels$SetPageTemplateTypeMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;)V
    .locals 0

    .prologue
    .line 2438725
    iput-object p1, p0, LX/HCX;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2438726
    iget-object v0, p0, LX/HCX;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "failed to apply template"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2438727
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2438728
    iget-object v0, p0, LX/HCX;->a:Lcom/facebook/pages/common/editpage/TemplateDetailsFragment;

    .line 2438729
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2438730
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2438731
    const-string p0, "extra_updated_page_template"

    const/4 p1, 0x1

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2438732
    const/4 p0, -0x1

    invoke-virtual {v1, p0, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2438733
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2438734
    return-void
.end method
