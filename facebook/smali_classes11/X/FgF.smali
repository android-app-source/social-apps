.class public LX/FgF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field private static z:LX/0Xm;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:LX/0tX;

.field private final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/Fg7;

.field private final f:LX/Cve;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:LX/0ad;

.field private final i:LX/0Uh;

.field private final j:Ljava/util/concurrent/Executor;

.field private final k:LX/Fcs;

.field private final l:LX/Cya;

.field private final m:LX/FgD;

.field private final n:LX/7B0;

.field private final o:LX/CvK;

.field public p:LX/CyS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final q:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LX/FgE;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CyY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cvk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final t:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

.field public v:LX/CwB;

.field private w:LX/0zi;

.field private x:LX/CyU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/CyU",
            "<",
            "LX/FgE;",
            ">;"
        }
    .end annotation
.end field

.field public y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2269962
    const-class v0, LX/FgF;

    sput-object v0, LX/FgF;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0tX;LX/1Ck;LX/Fg7;LX/Cve;LX/0ad;LX/0Uh;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;LX/Fcs;LX/Cya;LX/7B0;LX/CvK;)V
    .locals 2
    .param p8    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270008
    new-instance v0, LX/FgD;

    invoke-direct {v0, p0}, LX/FgD;-><init>(LX/FgF;)V

    iput-object v0, p0, LX/FgF;->m:LX/FgD;

    .line 2270009
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LX/FgF;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 2270010
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270011
    iput-object v0, p0, LX/FgF;->r:LX/0Ot;

    .line 2270012
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270013
    iput-object v0, p0, LX/FgF;->s:LX/0Ot;

    .line 2270014
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/FgF;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2270015
    iput-object p1, p0, LX/FgF;->b:Landroid/content/res/Resources;

    .line 2270016
    iput-object p2, p0, LX/FgF;->c:LX/0tX;

    .line 2270017
    iput-object p3, p0, LX/FgF;->d:LX/1Ck;

    .line 2270018
    iput-object p4, p0, LX/FgF;->e:LX/Fg7;

    .line 2270019
    iput-object p5, p0, LX/FgF;->f:LX/Cve;

    .line 2270020
    iput-object p6, p0, LX/FgF;->h:LX/0ad;

    .line 2270021
    iput-object p7, p0, LX/FgF;->i:LX/0Uh;

    .line 2270022
    iput-object p8, p0, LX/FgF;->g:Ljava/util/concurrent/ExecutorService;

    .line 2270023
    iput-object p9, p0, LX/FgF;->j:Ljava/util/concurrent/Executor;

    .line 2270024
    iput-object p10, p0, LX/FgF;->k:LX/Fcs;

    .line 2270025
    iput-object p11, p0, LX/FgF;->l:LX/Cya;

    .line 2270026
    iput-object p12, p0, LX/FgF;->n:LX/7B0;

    .line 2270027
    iput-object p13, p0, LX/FgF;->o:LX/CvK;

    .line 2270028
    return-void
.end method

.method private static a(LX/FgF;ILX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;ZLjava/lang/String;LX/0Px;LX/0Px;Z)LX/0gW;
    .locals 10
    .param p2    # LX/CwB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/CwB;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "Z",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;Z)",
            "LX/0gW",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270002
    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move/from16 v8, p6

    move/from16 v9, p10

    invoke-static/range {v1 .. v9}, LX/FgF;->a(LX/FgF;LX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;LX/0Px;LX/0Px;ZZ)LX/0gW;

    move-result-object v1

    .line 2270003
    if-lez p1, :cond_0

    .line 2270004
    const-string v2, "modules_count"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2270005
    :cond_0
    const-string v2, "callsite"

    move-object/from16 v0, p7

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2270006
    return-object v1
.end method

.method private static a(LX/FgF;LX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;LX/0Px;LX/0Px;ZZ)LX/0gW;
    .locals 13
    .param p1    # LX/CwB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/8ci;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;ZZ)",
            "LX/0gW",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2269999
    invoke-static {}, LX/9zD;->a()LX/9zC;

    move-result-object v2

    .line 2270000
    iget-object v0, p0, LX/FgF;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyY;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v1, p1

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    invoke-virtual/range {v0 .. v12}, LX/CyY;->a(LX/CwB;LX/0gW;ZLjava/lang/String;LX/0Px;LX/0Px;Ljava/lang/Integer;ZZLjava/lang/String;Ljava/lang/String;LX/8ci;)V

    .line 2270001
    return-object v2
.end method

.method private static a(LX/FgF;LX/CwB;ILX/8ci;)LX/0rl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "I",
            "LX/8ci;",
            ")",
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2269998
    new-instance v0, LX/FgB;

    invoke-direct {v0, p0, p2, p1, p3}, LX/FgB;-><init>(LX/FgF;ILX/CwB;LX/8ci;)V

    return-object v0
.end method

.method private static a(LX/FgF;[ILX/CwB;LX/0zO;LX/0rl;Ljava/lang/String;Ljava/lang/String;LX/8ci;LX/0Px;LX/0Px;Ljava/util/Set;)LX/0v6;
    .locals 16
    .param p4    # LX/0rl;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/8ci;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "LX/CwB;",
            "LX/0zO",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
            ">;",
            "LX/0rl",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0v6;"
        }
    .end annotation

    .prologue
    .line 2269977
    new-instance v15, LX/0v6;

    invoke-virtual/range {p3 .. p3}, LX/0zO;->b()LX/0gW;

    move-result-object v1

    invoke-virtual {v1}, LX/0gW;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v15, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2269978
    const/4 v2, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {p3 .. p3}, LX/0zO;->b()LX/0gW;

    move-result-object v1

    invoke-virtual {v1}, LX/0gW;->h()Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x1

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v1 .. v11}, LX/FgF;->a(LX/FgF;ILX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;ZLjava/lang/String;LX/0Px;LX/0Px;Z)LX/0gW;

    move-result-object v1

    .line 2269979
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2269980
    invoke-virtual {v15, v1}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v2

    .line 2269981
    invoke-virtual {v1}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2269982
    new-instance v1, LX/CyU;

    new-instance v3, LX/FgC;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/FgC;-><init>(LX/FgF;)V

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4}, LX/CyU;-><init>(LX/CyT;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/FgF;->x:LX/CyU;

    .line 2269983
    const/4 v3, 0x0

    .line 2269984
    const/4 v1, 0x0

    move v12, v1

    move-object v13, v2

    move-object v14, v3

    :goto_0
    move-object/from16 v0, p1

    array-length v1, v0

    if-ge v12, v1, :cond_2

    .line 2269985
    aget v2, p1, v12

    .line 2269986
    const/4 v7, 0x0

    invoke-virtual/range {p3 .. p3}, LX/0zO;->b()LX/0gW;

    move-result-object v1

    invoke-virtual {v1}, LX/0gW;->h()Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x1

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-static/range {v1 .. v11}, LX/FgF;->a(LX/FgF;ILX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;ZLjava/lang/String;LX/0Px;LX/0Px;Z)LX/0gW;

    move-result-object v1

    .line 2269987
    if-lez v12, :cond_0

    .line 2269988
    const-string v2, "after_cursor"

    const-string v3, "end_cursor"

    sget-object v4, LX/4Zz;->FIRST:LX/4Zz;

    sget-object v5, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {v14, v3, v4, v5}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 2269989
    :cond_0
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2269990
    invoke-virtual {v15, v3}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v2

    .line 2269991
    invoke-virtual {v3}, LX/0zO;->a()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2269992
    if-eqz v13, :cond_1

    .line 2269993
    invoke-virtual {v13, v2}, LX/0zX;->a(LX/0zX;)LX/0zX;

    move-result-object v2

    .line 2269994
    :cond_1
    add-int/lit8 v1, v12, 0x1

    move v12, v1

    move-object v13, v2

    move-object v14, v3

    goto :goto_0

    .line 2269995
    :cond_2
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, LX/FgF;->y:Z

    .line 2269996
    move-object/from16 v0, p0

    iget-object v1, v0, LX/FgF;->j:Ljava/util/concurrent/Executor;

    invoke-virtual {v13, v1}, LX/0zX;->a(Ljava/util/concurrent/Executor;)LX/0zX;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, LX/0zX;->a(LX/0rl;)LX/0zi;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LX/FgF;->w:LX/0zi;

    .line 2269997
    return-object v15
.end method

.method private static a(LX/FgF;LX/0gW;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
            ">;)",
            "LX/0zO",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2269973
    invoke-static {p1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2269974
    iget-object v1, p0, LX/FgF;->n:LX/7B0;

    sget-object v2, LX/7Az;->SERP:LX/7Az;

    invoke-virtual {v1, v2, v0}, LX/7B0;->a(LX/7Az;LX/0zO;)V

    .line 2269975
    iget-object v1, p0, LX/FgF;->o:LX/CvK;

    invoke-virtual {v1, v0}, LX/CvK;->a(LX/0zO;)V

    .line 2269976
    return-object v0
.end method

.method public static a(LX/0QB;)LX/FgF;
    .locals 3

    .prologue
    .line 2269965
    const-class v1, LX/FgF;

    monitor-enter v1

    .line 2269966
    :try_start_0
    sget-object v0, LX/FgF;->z:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2269967
    sput-object v2, LX/FgF;->z:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2269968
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2269969
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FgF;->b(LX/0QB;)LX/FgF;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2269970
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FgF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2269971
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2269972
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/FgF;LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 2269963
    iget-object v0, p0, LX/FgF;->c:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2269964
    return-object v0
.end method

.method private a(LX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;ZLX/0Px;LX/0Px;)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/8ci;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2269948
    invoke-static {}, LX/Cyf;->a()[I

    move-result-object v9

    .line 2269949
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-static/range {v0 .. v8}, LX/FgF;->a(LX/FgF;LX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;LX/0Px;LX/0Px;ZZ)LX/0gW;

    move-result-object v0

    .line 2269950
    if-eqz p5, :cond_0

    .line 2269951
    const-string v1, "update_activity_log"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2269952
    :cond_0
    invoke-static {p0, v0}, LX/FgF;->a(LX/FgF;LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2269953
    iget-object v0, p0, LX/FgF;->m:LX/FgD;

    invoke-static {v0}, LX/FgD;->a$redex0(LX/FgD;)V

    .line 2269954
    array-length v0, v9

    invoke-static {p0, p1, v0, p4}, LX/FgF;->a(LX/FgF;LX/CwB;ILX/8ci;)LX/0rl;

    move-result-object v4

    .line 2269955
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    move-object v0, p0

    move-object v1, v9

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    .line 2269956
    invoke-static/range {v0 .. v10}, LX/FgF;->a(LX/FgF;[ILX/CwB;LX/0zO;LX/0rl;Ljava/lang/String;Ljava/lang/String;LX/8ci;LX/0Px;LX/0Px;Ljava/util/Set;)LX/0v6;

    move-result-object v0

    .line 2269957
    iget-object v1, p0, LX/FgF;->n:LX/7B0;

    sget-object v2, LX/7Az;->SERP:LX/7Az;

    invoke-virtual {v1, v2, v0}, LX/7B0;->a(LX/7Az;LX/0v6;)V

    .line 2269958
    iget-object v1, p0, LX/FgF;->o:LX/CvK;

    invoke-virtual {v1, v0}, LX/CvK;->a(LX/0v6;)V

    .line 2269959
    iget-object v1, p0, LX/FgF;->f:LX/Cve;

    const/4 v2, 0x1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v4, "1"

    invoke-virtual {v1, p1, v2, v3, v4}, LX/Cve;->a(LX/CwB;ZLjava/lang/Iterable;Ljava/lang/String;)V

    .line 2269960
    iget-object v1, p0, LX/FgF;->c:LX/0tX;

    iget-object v2, p0, LX/FgF;->g:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v1, v0, v2}, LX/0tX;->b(LX/0v6;Ljava/util/concurrent/ExecutorService;)V

    .line 2269961
    return-void
.end method

.method private static a(LX/FgF;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;LX/8ci;ZLX/0Px;LX/0Px;)V
    .locals 5
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2269937
    iget-object v0, p0, LX/FgF;->v:LX/CwB;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FgF;->v:LX/CwB;

    invoke-interface {v0}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2269938
    if-eqz v0, :cond_0

    .line 2269939
    :goto_1
    return-void

    .line 2269940
    :cond_0
    iput-object p1, p0, LX/FgF;->v:LX/CwB;

    .line 2269941
    invoke-virtual {p0}, LX/FgF;->d()V

    .line 2269942
    iget-object v0, p0, LX/FgF;->e:LX/Fg7;

    const/4 v1, 0x0

    .line 2269943
    iput-boolean v1, v0, LX/Fg7;->m:Z

    .line 2269944
    iput-boolean v1, v0, LX/Fg7;->n:Z

    .line 2269945
    iput-boolean v1, v0, LX/Fg7;->o:Z

    .line 2269946
    iget-object v1, p0, LX/FgF;->p:LX/CyS;

    new-instance v2, LX/Fg9;

    invoke-direct {v2, p0, p1, p4}, LX/Fg9;-><init>(LX/FgF;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ci;)V

    new-instance v3, LX/Fg6;

    iget-object v4, p0, LX/FgF;->b:Landroid/content/res/Resources;

    iget-object v0, p0, LX/FgF;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvk;

    invoke-direct {v3, p1, v4, v0}, LX/Fg6;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;Landroid/content/res/Resources;LX/Cvk;)V

    invoke-virtual {v1, p1, p7, v2, v3}, LX/CyS;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/CyR;LX/CyQ;)Z

    .line 2269947
    invoke-direct/range {p0 .. p7}, LX/FgF;->a(LX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;ZLX/0Px;LX/0Px;)V

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/FgF;Ljava/lang/String;LX/0zO;LX/CwB;Ljava/lang/String;LX/8ci;LX/CwW;LX/Fg8;)V
    .locals 9
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/CwW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0zO",
            "<",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
            ">;",
            "LX/CwB;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "LX/CwW;",
            "LX/Fg8;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2269928
    if-nez p7, :cond_0

    .line 2269929
    iget-object v0, p0, LX/FgF;->m:LX/FgD;

    invoke-static {v0}, LX/FgD;->a$redex0(LX/FgD;)V

    .line 2269930
    :cond_0
    invoke-virtual {p6}, LX/CwW;->name()Ljava/lang/String;

    move-result-object v7

    .line 2269931
    iget-object v0, p0, LX/FgF;->f:LX/Cve;

    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "1"

    invoke-virtual {v0, p3, v1, v7, v2}, LX/Cve;->a(LX/CwB;ZLjava/lang/String;Ljava/lang/String;)V

    .line 2269932
    invoke-static {p0, p2}, LX/FgF;->a(LX/FgF;LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 2269933
    new-instance v0, LX/FgA;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, LX/FgA;-><init>(LX/FgF;Ljava/lang/String;LX/CwB;Ljava/lang/String;LX/CwW;LX/8ci;Ljava/lang/String;)V

    .line 2269934
    iget-object v1, p0, LX/FgF;->d:LX/1Ck;

    invoke-virtual {v1, p1, v8, v0}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2269935
    invoke-static {p0}, LX/FgF;->f(LX/FgF;)V

    .line 2269936
    return-void
.end method

.method private static a(LX/FgF;Ljava/lang/String;LX/CwB;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;LX/CwW;LX/Fg8;ZLX/0Px;LX/0Px;)V
    .locals 9
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/CwW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/CwB;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8ci;",
            "LX/CwW;",
            "LX/Fg8;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270029
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object/from16 v5, p10

    move-object/from16 v6, p11

    invoke-static/range {v0 .. v8}, LX/FgF;->a(LX/FgF;LX/CwB;Ljava/lang/String;Ljava/lang/String;LX/8ci;LX/0Px;LX/0Px;ZZ)LX/0gW;

    move-result-object v0

    .line 2270030
    if-eqz p3, :cond_0

    .line 2270031
    const-string v1, "after_cursor"

    invoke-virtual {v0, v1, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2270032
    const-string v1, "independent_module_or_first_unit"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2270033
    :cond_0
    if-eqz p9, :cond_1

    .line 2270034
    const-string v1, "update_activity_log"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2270035
    :cond_1
    const-string v1, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2270036
    invoke-static {p0, v0}, LX/FgF;->a(LX/FgF;LX/0gW;)LX/0zO;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    invoke-static/range {v0 .. v7}, LX/FgF;->a(LX/FgF;Ljava/lang/String;LX/0zO;LX/CwB;Ljava/lang/String;LX/8ci;LX/CwW;LX/Fg8;)V

    .line 2270037
    return-void
.end method

.method public static a$redex0(LX/FgF;LX/7C4;)V
    .locals 5

    .prologue
    .line 2269748
    invoke-static {p0}, LX/FgF;->h(LX/FgF;)V

    .line 2269749
    iget-object v0, p0, LX/FgF;->f:LX/Cve;

    invoke-virtual {v0}, LX/Cve;->c()V

    .line 2269750
    iget-object v0, p0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    if-eqz v0, :cond_1

    .line 2269751
    iget-object v0, p0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    const/4 v2, 0x0

    .line 2269752
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->W:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2269753
    iput-boolean v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ap:Z

    .line 2269754
    iput-boolean v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aq:Z

    .line 2269755
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/EQG;->ERROR:LX/EQG;

    .line 2269756
    :goto_0
    invoke-static {v0, v1}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V

    .line 2269757
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    .line 2269758
    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2269759
    iget v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    iget v4, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->as:I

    invoke-virtual {p1}, LX/7C4;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, v2, v3, v4, p0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IILjava/lang/String;)V

    .line 2269760
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->P:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Sc;

    invoke-virtual {v1, p1}, LX/2Sc;->a(LX/7C4;)V

    .line 2269761
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 2269762
    if-eqz v1, :cond_0

    .line 2269763
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->O:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0kL;

    new-instance v2, LX/27k;

    invoke-virtual {p1}, LX/7C4;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2269764
    :cond_0
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->T:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Sd;

    sget-object v2, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->M:Ljava/lang/String;

    sget-object v3, LX/7CQ;->RESULTS_LOAD_FAILURE:LX/7CQ;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "Exception: "

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 2269765
    :cond_1
    return-void

    .line 2269766
    :cond_2
    sget-object v1, LX/EQG;->ERROR_LOADING_MORE:LX/EQG;

    goto :goto_0
.end method

.method public static a$redex0(LX/FgF;LX/FgE;)V
    .locals 13

    .prologue
    .line 2269770
    iget-object v0, p0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    if-nez v0, :cond_0

    .line 2269771
    iget-object v0, p0, LX/FgF;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 2269772
    :goto_0
    return-void

    .line 2269773
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/FgF;->u:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    const/4 v2, 0x1

    const/4 v9, 0x0

    .line 2269774
    iget-object v1, p1, LX/FgE;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2269775
    if-eqz v1, :cond_2

    .line 2269776
    iget-object v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->W:Ljava/util/Map;

    .line 2269777
    iget-object v1, p1, LX/FgE;->f:Ljava/lang/String;

    move-object v4, v1

    .line 2269778
    iget-boolean v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->an:Z

    if-nez v1, :cond_1

    .line 2269779
    iget-object v1, p1, LX/FgE;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2269780
    const-string v5, "FetchKeywordSearchResults"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_d

    :cond_1
    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2269781
    :cond_2
    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2269782
    iget-object v3, p1, LX/FgE;->a:LX/Cz4;

    move-object v8, v3

    .line 2269783
    iget-object v3, p1, LX/FgE;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2269784
    invoke-virtual {v1, v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c(Ljava/lang/String;)V

    .line 2269785
    iget-object v3, p1, LX/FgE;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2269786
    invoke-static {v1, v3}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;)V

    .line 2269787
    iget-object v3, p1, LX/FgE;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2269788
    invoke-virtual {v1, v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e(Ljava/lang/String;)V

    .line 2269789
    iget-object v3, v8, LX/Cz4;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2269790
    iput-object v3, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->v:Ljava/lang/String;

    .line 2269791
    iget-object v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->F:LX/CzG;

    .line 2269792
    iput-object v1, v3, LX/CzG;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2269793
    iget-object v1, p1, LX/FgE;->h:LX/0Px;

    move-object v1, v1

    .line 2269794
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2269795
    iget-object v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v3}, LX/CzE;->h()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-virtual {v8}, LX/Cz4;->c()I

    move-result v3

    if-nez v3, :cond_11

    iget-object v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FcD;

    invoke-virtual {v3}, LX/FcD;->g()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_11

    const/4 v3, 0x1

    :goto_2
    move v3, v3

    .line 2269796
    if-nez v3, :cond_3

    .line 2269797
    iget-object v3, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->D:LX/0Sh;

    new-instance v4, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment$6;

    invoke-direct {v4, v0, v1}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment$6;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/0Px;)V

    invoke-virtual {v3, v4}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2269798
    :cond_3
    iget-boolean v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ao:Z

    if-eqz v1, :cond_4

    .line 2269799
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->d()V

    .line 2269800
    iput-boolean v9, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ao:Z

    .line 2269801
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ad:LX/Fje;

    .line 2269802
    iget-object v3, v1, LX/Fje;->u:Lcom/facebook/widget/FbSwipeRefreshLayout;

    move-object v1, v3

    .line 2269803
    invoke-virtual {v1, v9}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2269804
    :cond_4
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->size()I

    move-result v10

    .line 2269805
    const/4 v3, 0x0

    .line 2269806
    iget-object v1, p1, LX/FgE;->b:LX/CwW;

    move-object v1, v1

    .line 2269807
    sget-object v4, LX/CwW;->BOOTSTRAP_ENTITIES:LX/CwW;

    if-ne v1, v4, :cond_5

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->size()I

    move-result v1

    if-nez v1, :cond_9

    .line 2269808
    :cond_5
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    .line 2269809
    iget-object v3, p1, LX/FgE;->b:LX/CwW;

    move-object v3, v3

    .line 2269810
    sget-object v4, LX/CwW;->BOOTSTRAP_ENTITIES:LX/CwW;

    if-ne v3, v4, :cond_e

    :goto_3
    invoke-virtual {v1, v8, v2}, LX/CzE;->a(LX/Cz4;Z)LX/0Px;

    move-result-object v3

    .line 2269811
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    .line 2269812
    iget-object v2, v8, LX/Cz4;->c:LX/0P1;

    move-object v2, v2

    .line 2269813
    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    .line 2269814
    iget-object v4, v8, LX/Cz4;->d:LX/0P1;

    move-object v4, v4

    .line 2269815
    invoke-virtual {v4}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v4

    .line 2269816
    iget-object v5, v8, LX/Cz4;->e:LX/0P1;

    move-object v5, v5

    .line 2269817
    invoke-virtual {v5}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v5

    .line 2269818
    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 2269819
    iget-object v11, v1, LX/CzE;->h:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v11, v12, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 2269820
    :cond_6
    invoke-virtual {v4}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 2269821
    iget-object v11, v1, LX/CzE;->i:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v11, v12, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 2269822
    :cond_7
    invoke-virtual {v5}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 2269823
    iget-object v11, v1, LX/CzE;->j:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v11, v12, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 2269824
    :cond_8
    iget-boolean v1, p1, LX/FgE;->g:Z

    move v1, v1

    .line 2269825
    iput-boolean v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->aB:Z

    .line 2269826
    :cond_9
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    invoke-virtual {v1}, LX/FgF;->c()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2269827
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->a()I

    move-result v1

    if-nez v1, :cond_f

    sget-object v1, LX/EQG;->LOADING:LX/EQG;

    :goto_7
    invoke-static {v0, v1}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;LX/EQG;)V

    .line 2269828
    :goto_8
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->a()I

    invoke-virtual {v8}, LX/Cz4;->c()I

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->p:LX/FgF;

    invoke-virtual {v1}, LX/FgF;->c()Z

    .line 2269829
    if-eqz v3, :cond_b

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 2269830
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-interface {v1}, LX/0g8;->q()I

    move-result v1

    .line 2269831
    invoke-static {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->y(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    .line 2269832
    if-gtz v1, :cond_a

    .line 2269833
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->af:LX/0g8;

    invoke-interface {v1, v9}, LX/0g8;->g(I)V

    .line 2269834
    :cond_a
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->l:LX/FgH;

    .line 2269835
    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2269836
    iget-object v4, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    iget-object v5, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->U:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FcD;

    invoke-virtual {v5}, LX/FcD;->g()LX/0Px;

    move-result-object v5

    iget v6, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    iget v7, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->as:I

    .line 2269837
    iget-object v11, v8, LX/Cz4;->f:Ljava/lang/String;

    move-object v8, v11

    .line 2269838
    invoke-virtual/range {v1 .. v8}, LX/FgH;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0Px;LX/CzE;LX/0Px;IILjava/lang/String;)V

    .line 2269839
    iget v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ar:I

    .line 2269840
    :cond_b
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ab:LX/CzE;

    invoke-virtual {v1}, LX/CzE;->size()I

    move-result v1

    if-ne v10, v1, :cond_c

    .line 2269841
    iget-object v1, p1, LX/FgE;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2269842
    if-eqz v1, :cond_c

    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->W:Ljava/util/Map;

    .line 2269843
    iget-object v2, p1, LX/FgE;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2269844
    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2269845
    iget-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->W:Ljava/util/Map;

    .line 2269846
    iget-object v2, p1, LX/FgE;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2269847
    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2269848
    iget-object v2, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->m:LX/Cve;

    .line 2269849
    iget-object v3, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v3

    .line 2269850
    iget-object v4, p1, LX/FgE;->f:Ljava/lang/String;

    move-object v4, v4

    .line 2269851
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v4, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v1

    invoke-virtual {v2, v3, v1, v9}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/util/Map;Z)Z
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2269852
    :cond_c
    goto/16 :goto_0

    .line 2269853
    :catch_0
    move-exception v0

    .line 2269854
    invoke-static {p0, v0}, LX/FgF;->a$redex0(LX/FgF;LX/7C4;)V

    goto/16 :goto_0

    :cond_d
    move v1, v9

    .line 2269855
    goto/16 :goto_1

    :cond_e
    move v2, v9

    .line 2269856
    goto/16 :goto_3

    .line 2269857
    :cond_f
    sget-object v1, LX/EQG;->LOADING_MORE:LX/EQG;

    goto/16 :goto_7

    .line 2269858
    :cond_10
    invoke-virtual {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->c()V

    goto/16 :goto_8

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public static a$redex0(LX/FgF;Ljava/lang/String;LX/CwB;Ljava/lang/String;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;LX/CwW;LX/8ci;Ljava/lang/String;)V
    .locals 16
    .param p6    # LX/8ci;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2269859
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgF;->d:LX/1Ck;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2269860
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgF;->e:LX/Fg7;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    move-object/from16 v3, p2

    invoke-virtual {v4, v0, v1, v2, v3}, LX/Fg7;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;LX/CwW;LX/8ci;LX/CwB;)LX/Cz4;

    move-result-object v15

    .line 2269861
    invoke-virtual/range {p0 .. p0}, LX/FgF;->c()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2269862
    invoke-static/range {p0 .. p0}, LX/FgF;->h(LX/FgF;)V

    .line 2269863
    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;->j()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;

    move-result-object v11

    .line 2269864
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->d()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    move-result-object v4

    move-object v13, v4

    .line 2269865
    :goto_0
    if-eqz v13, :cond_2

    invoke-virtual {v13}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->e()Ljava/lang/String;

    move-result-object v9

    .line 2269866
    :goto_1
    if-eqz v11, :cond_3

    invoke-virtual {v11}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v11}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->b()LX/0Px;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v4

    move-object v12, v4

    .line 2269867
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgF;->f:LX/Cve;

    invoke-static/range {p3 .. p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    move-object/from16 v5, p2

    move-object/from16 v7, p4

    move-object/from16 v8, p7

    invoke-virtual/range {v4 .. v9}, LX/Cve;->a(LX/CwB;ZLcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 2269868
    new-instance v6, LX/FgE;

    if-eqz v13, :cond_4

    invoke-virtual {v13}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->fI_()Ljava/lang/String;

    move-result-object v10

    :goto_3
    if-eqz v11, :cond_5

    invoke-virtual {v11}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;->e()Ljava/lang/String;

    move-result-object v11

    :goto_4
    if-eqz v13, :cond_6

    invoke-virtual {v13}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;->b()Z

    move-result v13

    :goto_5
    invoke-static {v12}, LX/Fcs;->a(LX/0Px;)LX/0Px;

    move-result-object v14

    move-object v7, v15

    move-object/from16 v8, p5

    move-object/from16 v12, p7

    invoke-direct/range {v6 .. v14}, LX/FgE;-><init>(LX/Cz4;LX/CwW;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0Px;)V

    .line 2269869
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgF;->x:LX/CyU;

    .line 2269870
    if-eqz v4, :cond_7

    if-eqz p7, :cond_7

    const-string v4, "bootstrap_entities"

    move-object/from16 v0, p7

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 2269871
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgF;->x:LX/CyU;

    move-object/from16 v0, p7

    invoke-virtual {v4, v0, v6}, LX/CyU;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2269872
    :goto_6
    invoke-virtual {v15}, LX/Cz4;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    .line 2269873
    :goto_7
    return-void

    .line 2269874
    :cond_1
    const/4 v4, 0x0

    move-object v13, v4

    goto :goto_0

    .line 2269875
    :cond_2
    const/4 v9, 0x0

    goto :goto_1

    .line 2269876
    :cond_3
    const/4 v4, 0x0

    move-object v12, v4

    goto :goto_2

    .line 2269877
    :cond_4
    const/4 v10, 0x0

    goto :goto_3

    :cond_5
    const/4 v11, 0x0

    goto :goto_4

    :cond_6
    const/4 v13, 0x0

    goto :goto_5

    .line 2269878
    :cond_7
    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/FgF;->a$redex0(LX/FgF;LX/FgE;)V
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    .line 2269879
    :catch_0
    move-exception v4

    .line 2269880
    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/FgF;->a$redex0(LX/FgF;LX/7C4;)V

    goto :goto_7
.end method

.method private static b(LX/0QB;)LX/FgF;
    .locals 14

    .prologue
    .line 2269881
    new-instance v0, LX/FgF;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/Fg7;->b(LX/0QB;)LX/Fg7;

    move-result-object v4

    check-cast v4, LX/Fg7;

    invoke-static {p0}, LX/Cve;->a(LX/0QB;)LX/Cve;

    move-result-object v5

    check-cast v5, LX/Cve;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/Fcs;->a(LX/0QB;)LX/Fcs;

    move-result-object v10

    check-cast v10, LX/Fcs;

    invoke-static {p0}, LX/Cya;->a(LX/0QB;)LX/Cya;

    move-result-object v11

    check-cast v11, LX/Cya;

    invoke-static {p0}, LX/7B0;->b(LX/0QB;)LX/7B0;

    move-result-object v12

    check-cast v12, LX/7B0;

    invoke-static {p0}, LX/CvK;->b(LX/0QB;)LX/CvK;

    move-result-object v13

    check-cast v13, LX/CvK;

    invoke-direct/range {v0 .. v13}, LX/FgF;-><init>(Landroid/content/res/Resources;LX/0tX;LX/1Ck;LX/Fg7;LX/Cve;LX/0ad;LX/0Uh;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;LX/Fcs;LX/Cya;LX/7B0;LX/CvK;)V

    .line 2269882
    invoke-static {p0}, LX/CyS;->b(LX/0QB;)LX/CyS;

    move-result-object v1

    check-cast v1, LX/CyS;

    const/16 v2, 0x335f

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x32d6

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2269883
    iput-object v1, v0, LX/FgF;->p:LX/CyS;

    iput-object v2, v0, LX/FgF;->r:LX/0Ot;

    iput-object v3, v0, LX/FgF;->s:LX/0Ot;

    .line 2269884
    return-object v0
.end method

.method private static f(LX/FgF;)V
    .locals 3

    .prologue
    .line 2269767
    iget-object v0, p0, LX/FgF;->i:LX/0Uh;

    sget v1, LX/2SU;->D:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FgF;->t:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2269768
    iget-object v0, p0, LX/FgF;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$3;

    invoke-direct {v1, p0}, Lcom/facebook/search/results/loader/modules/SearchResultsFeedModulesDataLoader$3;-><init>(LX/FgF;)V

    const v2, -0x66d2a97f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2269769
    :cond_0
    return-void
.end method

.method public static h(LX/FgF;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2269885
    iput-object v0, p0, LX/FgF;->v:LX/CwB;

    .line 2269886
    iput-object v0, p0, LX/FgF;->x:LX/CyU;

    .line 2269887
    iget-object v0, p0, LX/FgF;->m:LX/FgD;

    .line 2269888
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/FgD;->d:Z

    .line 2269889
    iget-object v1, v0, LX/FgD;->b:Landroid/os/Handler;

    iget-object v2, v0, LX/FgD;->c:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2269890
    iget-object v0, p0, LX/FgF;->p:LX/CyS;

    .line 2269891
    iget-object v1, v0, LX/CyS;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    const-string v2, "keyword_search_result_bootstrap_loader_key"

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2269892
    iget-object v0, p0, LX/FgF;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 2269893
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;LX/Fg8;LX/0Px;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "Ljava/lang/String;",
            "LX/Fg8;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2269894
    invoke-virtual {p0}, LX/FgF;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2269895
    :goto_0
    return-void

    .line 2269896
    :cond_0
    const-string v1, "keyword_search_result_loader_more_key"

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v0

    iget-object v4, v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->q()LX/8ci;

    move-result-object v6

    sget-object v7, LX/CwW;->SINGLE:LX/CwW;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v8, p3

    move-object/from16 v11, p4

    invoke-static/range {v0 .. v11}, LX/FgF;->a(LX/FgF;Ljava/lang/String;LX/CwB;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;LX/CwW;LX/Fg8;ZLX/0Px;LX/0Px;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;LX/8ci;Z)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/8ci;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2269897
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v7, v0

    .line 2269898
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h()LX/0P1;

    move-result-object v0

    const-string v1, "SCOPED_TAB"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h()LX/0P1;

    move-result-object v0

    sget-object v1, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v1}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    .line 2269899
    iget-boolean v1, v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->a:Z

    move v0, v1

    .line 2269900
    if-eqz v0, :cond_0

    .line 2269901
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->k()LX/103;

    move-result-object v0

    .line 2269902
    new-instance v1, LX/4FP;

    invoke-direct {v1}, LX/4FP;-><init>()V

    const-string v2, "add"

    invoke-virtual {v1, v2}, LX/4FP;->c(Ljava/lang/String;)LX/4FP;

    move-result-object v1

    sget-object v2, LX/FcD;->a:LX/0P1;

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/4FP;->a(Ljava/lang/String;)LX/4FP;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4FP;->d(Ljava/lang/String;)LX/4FP;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    .line 2269903
    :cond_0
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-static/range {v0 .. v7}, LX/FgF;->a(LX/FgF;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;LX/8ci;ZLX/0Px;LX/0Px;)V

    .line 2269904
    return-void
.end method

.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ZLX/0Px;LX/0Px;)V
    .locals 8
    .param p3    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2269905
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v0, v0

    .line 2269906
    iget-object v2, v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v3

    .line 2269907
    iget-object v0, p1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v4, v0

    .line 2269908
    move-object v0, p0

    move-object v1, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-static/range {v0 .. v7}, LX/FgF;->a(LX/FgF;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;LX/8ci;ZLX/0Px;LX/0Px;)V

    .line 2269909
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2269910
    invoke-static {p0}, LX/FgF;->h(LX/FgF;)V

    .line 2269911
    iget-object v0, p0, LX/FgF;->d:LX/1Ck;

    const-string v1, "keyword_search_result_loader_key"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2269912
    iget-object v0, p0, LX/FgF;->d:LX/1Ck;

    const-string v1, "keyword_search_result_loader_more_key"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2269913
    invoke-virtual {p0}, LX/FgF;->d()V

    .line 2269914
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2269915
    iget-boolean v0, p0, LX/FgF;->y:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FgF;->d:LX/1Ck;

    const-string v1, "keyword_search_result_loader_key"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FgF;->d:LX/1Ck;

    const-string v1, "keyword_search_result_loader_more_key"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FgF;->p:LX/CyS;

    .line 2269916
    iget-object v1, v0, LX/CyS;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    const-string p0, "keyword_search_result_bootstrap_loader_key"

    invoke-virtual {v1, p0}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 2269917
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2269918
    iget-object v0, p0, LX/FgF;->w:LX/0zi;

    .line 2269919
    if-eqz v0, :cond_0

    .line 2269920
    invoke-virtual {v0}, LX/0zi;->a()V

    .line 2269921
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/FgF;->w:LX/0zi;

    .line 2269922
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FgF;->y:Z

    .line 2269923
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2269924
    iget-object v1, p0, LX/FgF;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    monitor-enter v1

    .line 2269925
    :goto_0
    :try_start_0
    iget-object v0, p0, LX/FgF;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2269926
    iget-object v0, p0, LX/FgF;->q:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FgE;

    invoke-static {p0, v0}, LX/FgF;->a$redex0(LX/FgF;LX/FgE;)V

    goto :goto_0

    .line 2269927
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
