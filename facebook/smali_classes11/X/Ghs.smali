.class public final LX/Ghs;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ght;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/Ghu;

.field public final synthetic b:LX/Ght;


# direct methods
.method public constructor <init>(LX/Ght;)V
    .locals 1

    .prologue
    .line 2384939
    iput-object p1, p0, LX/Ghs;->b:LX/Ght;

    .line 2384940
    move-object v0, p1

    .line 2384941
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2384942
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2384927
    const-string v0, "GametimeMatchTeamComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2384928
    if-ne p0, p1, :cond_1

    .line 2384929
    :cond_0
    :goto_0
    return v0

    .line 2384930
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2384931
    goto :goto_0

    .line 2384932
    :cond_3
    check-cast p1, LX/Ghs;

    .line 2384933
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2384934
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2384935
    if-eq v2, v3, :cond_0

    .line 2384936
    iget-object v2, p0, LX/Ghs;->a:LX/Ghu;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Ghs;->a:LX/Ghu;

    iget-object v3, p1, LX/Ghs;->a:LX/Ghu;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2384937
    goto :goto_0

    .line 2384938
    :cond_4
    iget-object v2, p1, LX/Ghs;->a:LX/Ghu;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
