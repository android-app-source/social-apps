.class public LX/GGm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;


# instance fields
.field private i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/GH0;


# direct methods
.method public constructor <init>(LX/GH0;LX/GEk;LX/GEw;LX/GEl;LX/GEe;LX/GEz;LX/GEi;LX/GEg;LX/GEX;LX/GEy;LX/GKQ;LX/GEf;LX/GEx;LX/GEu;LX/GEY;LX/GMS;LX/GEb;LX/GEp;)V
    .locals 6
    .param p3    # LX/GEw;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostEvent;
        .end annotation
    .end param
    .param p11    # LX/GKQ;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostStory;
        .end annotation
    .end param
    .param p12    # LX/GEf;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostEvent;
        .end annotation
    .end param
    .param p13    # LX/GEx;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostEvent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334539
    iput-object p1, p0, LX/GGm;->j:LX/GH0;

    .line 2334540
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p18

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03006e

    sget-object v4, LX/GGm;->e:LX/GGX;

    sget-object v5, LX/8wK;->AD_PREVIEW:LX/8wK;

    move-object/from16 v0, p11

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p14

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p17

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03008d

    sget-object v4, LX/GGm;->d:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING_DESCRIPTION:LX/8wK;

    move-object/from16 v0, p16

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p15

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/GGm;->i:LX/0Px;

    .line 2334541
    return-void
.end method

.method public static b(LX/0QB;)LX/GGm;
    .locals 19

    .prologue
    .line 2334542
    new-instance v0, LX/GGm;

    invoke-static/range {p0 .. p0}, LX/GH0;->a(LX/0QB;)LX/GH0;

    move-result-object v1

    check-cast v1, LX/GH0;

    invoke-static/range {p0 .. p0}, LX/GEk;->a(LX/0QB;)LX/GEk;

    move-result-object v2

    check-cast v2, LX/GEk;

    invoke-static/range {p0 .. p0}, LX/GDF;->a(LX/0QB;)LX/GEw;

    move-result-object v3

    check-cast v3, LX/GEw;

    invoke-static/range {p0 .. p0}, LX/GEl;->a(LX/0QB;)LX/GEl;

    move-result-object v4

    check-cast v4, LX/GEl;

    invoke-static/range {p0 .. p0}, LX/GEe;->a(LX/0QB;)LX/GEe;

    move-result-object v5

    check-cast v5, LX/GEe;

    invoke-static/range {p0 .. p0}, LX/GEz;->a(LX/0QB;)LX/GEz;

    move-result-object v6

    check-cast v6, LX/GEz;

    invoke-static/range {p0 .. p0}, LX/GEi;->a(LX/0QB;)LX/GEi;

    move-result-object v7

    check-cast v7, LX/GEi;

    invoke-static/range {p0 .. p0}, LX/GEg;->a(LX/0QB;)LX/GEg;

    move-result-object v8

    check-cast v8, LX/GEg;

    invoke-static/range {p0 .. p0}, LX/GEX;->a(LX/0QB;)LX/GEX;

    move-result-object v9

    check-cast v9, LX/GEX;

    invoke-static/range {p0 .. p0}, LX/GEy;->a(LX/0QB;)LX/GEy;

    move-result-object v10

    check-cast v10, LX/GEy;

    invoke-static/range {p0 .. p0}, LX/GCI;->a(LX/0QB;)LX/GKQ;

    move-result-object v11

    check-cast v11, LX/GKQ;

    invoke-static/range {p0 .. p0}, LX/GCg;->a(LX/0QB;)LX/GEf;

    move-result-object v12

    check-cast v12, LX/GEf;

    invoke-static/range {p0 .. p0}, LX/GDI;->a(LX/0QB;)LX/GEx;

    move-result-object v13

    check-cast v13, LX/GEx;

    invoke-static/range {p0 .. p0}, LX/GEu;->a(LX/0QB;)LX/GEu;

    move-result-object v14

    check-cast v14, LX/GEu;

    invoke-static/range {p0 .. p0}, LX/GEY;->a(LX/0QB;)LX/GEY;

    move-result-object v15

    check-cast v15, LX/GEY;

    invoke-static/range {p0 .. p0}, LX/GMS;->a(LX/0QB;)LX/GMS;

    move-result-object v16

    check-cast v16, LX/GMS;

    invoke-static/range {p0 .. p0}, LX/GEb;->a(LX/0QB;)LX/GEb;

    move-result-object v17

    check-cast v17, LX/GEb;

    invoke-static/range {p0 .. p0}, LX/GEp;->a(LX/0QB;)LX/GEp;

    move-result-object v18

    check-cast v18, LX/GEp;

    invoke-direct/range {v0 .. v18}, LX/GGm;-><init>(LX/GH0;LX/GEk;LX/GEw;LX/GEl;LX/GEe;LX/GEz;LX/GEi;LX/GEg;LX/GEX;LX/GEy;LX/GKQ;LX/GEf;LX/GEx;LX/GEu;LX/GEY;LX/GMS;LX/GEb;LX/GEp;)V

    .line 2334543
    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334544
    iget-object v0, p0, LX/GGm;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 6

    .prologue
    .line 2334545
    const-string v0, "storyId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2334546
    const-string v0, "page_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2334547
    invoke-static {p1}, LX/8wJ;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 2334548
    iget-object v0, p0, LX/GGm;->j:LX/GH0;

    sget-object v4, LX/8wL;->BOOST_EVENT:LX/8wL;

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, LX/GH0;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8wL;LX/GCY;)V

    .line 2334549
    return-void
.end method
