.class public LX/Frv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/BQB;

.field public final b:LX/4V5;

.field public final c:LX/G4s;

.field public final d:LX/G4r;

.field public final e:LX/0SG;

.field public final f:LX/03V;


# direct methods
.method public constructor <init>(LX/0SG;LX/4V5;LX/03V;LX/BQB;LX/G4s;LX/G4r;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2296381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296382
    iput-object p2, p0, LX/Frv;->b:LX/4V5;

    .line 2296383
    iput-object p4, p0, LX/Frv;->a:LX/BQB;

    .line 2296384
    iput-object p5, p0, LX/Frv;->c:LX/G4s;

    .line 2296385
    iput-object p6, p0, LX/Frv;->d:LX/G4r;

    .line 2296386
    iput-object p1, p0, LX/Frv;->e:LX/0SG;

    .line 2296387
    iput-object p3, p0, LX/Frv;->f:LX/03V;

    .line 2296388
    return-void
.end method

.method private static a(LX/Frv;LX/0zX;JLX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/0zi;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<",
            "LX/FsL;",
            ">;J",
            "LX/G4x;",
            "LX/FqO;",
            "Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 2296378
    invoke-static {p0}, LX/Frv;->f(LX/Frv;)LX/0QK;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    invoke-static {p0}, LX/Frv;->h(LX/Frv;)LX/0QK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    .line 2296379
    iget-object v9, p0, LX/Frv;->b:LX/4V5;

    new-instance v1, LX/Frq;

    move-object v2, p0

    move-object v3, p4

    move-object/from16 v4, p6

    move-object v5, p5

    move-wide v6, p2

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, LX/Frq;-><init>(LX/Frv;LX/G4x;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;LX/FqO;JLjava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {v9, v0, v1}, LX/4V5;->a(LX/0zX;LX/0rl;)LX/0zi;

    move-result-object v0

    .line 2296380
    return-object v0
.end method

.method private static a(LX/Frv;LX/0zX;LX/G1I;LX/G4x;LX/5SB;LX/FqO;)LX/0zi;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<",
            "LX/FsK;",
            ">;",
            "LX/G1I;",
            "LX/G4x;",
            "LX/5SB;",
            "LX/FqO;",
            ")",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 2296366
    iget-object v0, p0, LX/Frv;->a:LX/BQB;

    .line 2296367
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "ProtilesNetworkFetch"

    invoke-virtual {v1, v2}, LX/BQD;->a(Ljava/lang/String;)V

    .line 2296368
    iget-object v0, p2, LX/G1I;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2296369
    :goto_0
    invoke-virtual {p2}, LX/G1I;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2296370
    const/4 v0, 0x0

    .line 2296371
    iput-boolean v0, p3, LX/G4x;->f:Z

    .line 2296372
    :cond_0
    invoke-interface {p5}, LX/FqO;->lo_()V

    .line 2296373
    iget-object v6, p0, LX/Frv;->b:LX/4V5;

    new-instance v0, LX/Frp;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/Frp;-><init>(LX/Frv;LX/G1I;LX/G4x;LX/FqO;LX/5SB;)V

    invoke-virtual {v6, p1, v0}, LX/4V5;->a(LX/0zX;LX/0rl;)LX/0zi;

    move-result-object v0

    .line 2296374
    return-object v0

    .line 2296375
    :cond_1
    iget-object v0, p2, LX/G1I;->b:LX/G1M;

    sget-object v1, LX/G1L;->LOADING:LX/G1L;

    .line 2296376
    iput-object v1, v0, LX/G1M;->a:LX/G1L;

    .line 2296377
    goto :goto_0
.end method

.method private static a(LX/Frv;LX/0zX;LX/G2n;LX/FqO;)LX/0zi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<",
            "LX/3Fw;",
            ">;",
            "LX/G2n;",
            "LX/FqO;",
            ")",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 2296364
    iget-object v0, p0, LX/Frv;->b:LX/4V5;

    new-instance v1, LX/Frn;

    invoke-direct {v1, p0, p2, p3}, LX/Frn;-><init>(LX/Frv;LX/G2n;LX/FqO;)V

    invoke-virtual {v0, p1, v1}, LX/4V5;->a(LX/0zX;LX/0rl;)LX/0zi;

    move-result-object v0

    .line 2296365
    return-object v0
.end method

.method private static a(LX/Frv;LX/0zX;LX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/0zi;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;",
            ">;",
            "LX/G4x;",
            "LX/FqO;",
            "Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ")",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 2296362
    iget-object v6, p0, LX/Frv;->b:LX/4V5;

    new-instance v0, LX/Fro;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/Fro;-><init>(LX/Frv;Ljava/util/concurrent/atomic/AtomicBoolean;LX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    invoke-virtual {v6, p1, v0}, LX/4V5;->a(LX/0zX;LX/0rl;)LX/0zi;

    move-result-object v0

    .line 2296363
    return-object v0
.end method

.method private static a(LX/Frv;LX/FsJ;JLX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;LX/Fso;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/0zi;
    .locals 12

    .prologue
    .line 2296359
    iget-object v0, p1, LX/FsJ;->a:LX/0zX;

    invoke-static {p0}, LX/Frv;->e(LX/Frv;)LX/0QK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    invoke-static {p0}, LX/Frv;->g(LX/Frv;)LX/0QK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v10

    .line 2296360
    iget-object v11, p0, LX/Frv;->b:LX/4V5;

    new-instance v0, LX/Frr;

    move-object v1, p0

    move-object/from16 v2, p5

    move-object/from16 v3, p7

    move-object v4, p1

    move-object/from16 v5, p8

    move-wide v6, p2

    move-object/from16 v8, p4

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, LX/Frr;-><init>(LX/Frv;LX/FqO;LX/Fso;LX/FsJ;Ljava/util/concurrent/atomic/AtomicBoolean;JLX/G4x;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    invoke-virtual {v11, v10, v0}, LX/4V5;->a(LX/0zX;LX/0rl;)LX/0zi;

    move-result-object v0

    .line 2296361
    return-object v0
.end method

.method private static a(LX/Frv;LX/Fso;LX/0zX;LX/BPB;ZLX/FqO;)LX/0zi;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Fso;",
            "LX/0zX",
            "<TT;>;",
            "LX/BPB",
            "<TT;>;Z",
            "LX/FqO;",
            ")",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 2296358
    iget-object v6, p0, LX/Frv;->b:LX/4V5;

    new-instance v0, LX/Frh;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/Frh;-><init>(LX/Frv;LX/FqO;LX/Fso;LX/BPB;Z)V

    invoke-virtual {v6, p2, v0}, LX/4V5;->a(LX/0zX;LX/0rl;)LX/0zi;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Frv;
    .locals 10

    .prologue
    .line 2296347
    const-class v1, LX/Frv;

    monitor-enter v1

    .line 2296348
    :try_start_0
    sget-object v0, LX/Frv;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2296349
    sput-object v2, LX/Frv;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2296350
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2296351
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2296352
    new-instance v3, LX/Frv;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/4V5;->a(LX/0QB;)LX/4V5;

    move-result-object v5

    check-cast v5, LX/4V5;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v7

    check-cast v7, LX/BQB;

    invoke-static {v0}, LX/G4s;->a(LX/0QB;)LX/G4s;

    move-result-object v8

    check-cast v8, LX/G4s;

    invoke-static {v0}, LX/G4r;->a(LX/0QB;)LX/G4r;

    move-result-object v9

    check-cast v9, LX/G4r;

    invoke-direct/range {v3 .. v9}, LX/Frv;-><init>(LX/0SG;LX/4V5;LX/03V;LX/BQB;LX/G4s;LX/G4r;)V

    .line 2296353
    move-object v0, v3

    .line 2296354
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2296355
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Frv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2296356
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2296357
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic a(LX/Frv;LX/Fso;Lcom/facebook/graphql/model/GraphQLTimelineSection;)LX/Fso;
    .locals 1

    .prologue
    .line 2296343
    invoke-static {p2}, LX/Frv;->a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2296344
    invoke-virtual {p1}, LX/Fso;->a()LX/Fsn;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/Fsn;->a(Ljava/lang/String;)LX/Fsn;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/Fsn;->b(Ljava/lang/String;)LX/Fsn;

    move-result-object v0

    invoke-virtual {v0}, LX/Fsn;->b()LX/Fso;

    move-result-object p1

    .line 2296345
    :cond_0
    move-object v0, p1

    .line 2296346
    return-object v0
.end method

.method private static a(LX/Fso;JLcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;LX/0ta;Lcom/facebook/timeline/protocol/ResultSource;ZLX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 10

    .prologue
    .line 2296336
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;->a()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLTimelineSection;

    .line 2296337
    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-static {p0, v2, v0, v1}, LX/Frv;->a(LX/Fso;Lcom/facebook/graphql/model/GraphQLTimelineSection;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    .line 2296338
    move-object/from16 v0, p7

    move-object/from16 v1, p5

    invoke-interface {v0, p4, v1, p1, p2}, LX/FqO;->a(LX/0ta;Lcom/facebook/timeline/protocol/ResultSource;J)V

    .line 2296339
    if-eqz p6, :cond_0

    invoke-static {v2}, LX/Frv;->a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move-object v3, p0

    move-wide v4, p1

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 2296340
    invoke-static/range {v2 .. v9}, LX/Frv;->a(Lcom/facebook/graphql/model/GraphQLTimelineSection;LX/Fso;JLX/0ta;Lcom/facebook/timeline/protocol/ResultSource;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    .line 2296341
    :goto_0
    return-void

    .line 2296342
    :cond_1
    invoke-interface/range {p7 .. p7}, LX/FqO;->lo_()V

    goto :goto_0
.end method

.method public static a(LX/Fso;Lcom/facebook/graphql/model/GraphQLTimelineSection;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 6

    .prologue
    .line 2296302
    invoke-interface {p2}, LX/FqO;->K()V

    .line 2296303
    iget-object v0, p3, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    invoke-virtual {v0, p0, v1}, LX/G4x;->a(LX/Fso;LX/G5A;)V

    .line 2296304
    iget-object v0, p3, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    const/4 v1, 0x0

    .line 2296305
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/G4x;->a(Ljava/lang/String;)LX/G59;

    move-result-object v3

    .line 2296306
    if-nez v3, :cond_3

    .line 2296307
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2296308
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;

    .line 2296309
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 2296310
    instance-of v4, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_0

    .line 2296311
    invoke-static {p3}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b(Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)LX/1ly;

    move-result-object v4

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4, v0}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2296312
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2296313
    :cond_1
    iget-object v0, p3, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    sget-object v1, LX/G5A;->LOADING:LX/G5A;

    invoke-virtual {v0, p0, v1}, LX/G4x;->a(LX/Fso;LX/G5A;)V

    .line 2296314
    iget-boolean v0, p0, LX/Fso;->f:Z

    if-eqz v0, :cond_2

    .line 2296315
    iget-object v0, p3, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    invoke-virtual {v0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2296316
    :cond_2
    iget-object v0, p3, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v0}, LX/FqO;->lo_()V

    .line 2296317
    invoke-interface {p2}, LX/FqO;->L()V

    .line 2296318
    return-void

    .line 2296319
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v4

    .line 2296320
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v3}, LX/G59;->g()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2296321
    iget-object v2, v3, LX/G59;->a:LX/G58;

    move-object v2, v2

    .line 2296322
    sget-object v5, LX/G58;->RECENT_SECTION:LX/G58;

    if-eq v2, v5, :cond_8

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 2296323
    if-eqz v2, :cond_4

    .line 2296324
    iget-object v2, v3, LX/G59;->d:Ljava/util/List;

    new-instance v4, LX/G50;

    invoke-direct {v4}, LX/G50;-><init>()V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2296325
    const/4 v2, 0x1

    iput-boolean v2, v3, LX/G59;->h:Z

    .line 2296326
    goto/16 :goto_0

    .line 2296327
    :cond_4
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2296328
    invoke-static {v3}, LX/G4x;->a(LX/G59;)V

    .line 2296329
    :cond_5
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_6

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;

    .line 2296330
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/G59;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2296331
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2296332
    :cond_6
    iget-object v1, v0, LX/G4x;->g:LX/G4v;

    if-eqz v1, :cond_7

    .line 2296333
    iget-object v1, v0, LX/G4x;->g:LX/G4v;

    invoke-virtual {v1, v3}, LX/G4v;->a(Ljava/lang/Iterable;)V

    .line 2296334
    :cond_7
    invoke-static {v0}, LX/G4x;->e(LX/G4x;)V

    .line 2296335
    invoke-virtual {v4}, LX/0Px;->size()I

    goto/16 :goto_0

    :cond_8
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTimelineSection;LX/Fso;JLX/0ta;Lcom/facebook/timeline/protocol/ResultSource;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 8

    .prologue
    .line 2296179
    invoke-interface {p6}, LX/FqO;->K()V

    move-object v1, p7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p4

    move-wide v6, p2

    .line 2296180
    iget-object v0, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    .line 2296181
    iget-object p0, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    move v0, p0

    .line 2296182
    if-eqz v0, :cond_1

    .line 2296183
    :cond_0
    :goto_0
    invoke-interface {p6}, LX/FqO;->L()V

    .line 2296184
    return-void

    .line 2296185
    :cond_1
    iget-object v0, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/G4x;->a(Ljava/lang/String;)LX/G59;

    move-result-object p0

    .line 2296186
    if-nez p0, :cond_2

    .line 2296187
    const-string v0, "onSectionUnitsLoaded: Expected ID %s to be in section list %s"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object p0

    iget-object p1, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    invoke-virtual {p1}, LX/G4x;->d()LX/0Px;

    move-result-object p1

    invoke-static {v0, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2296188
    iget-object p0, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->d:LX/03V;

    const-string p1, "timeline_unknown_section_id"

    invoke-virtual {p0, p1, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2296189
    :cond_2
    iget-object v0, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    sget-object p1, LX/G5A;->COMPLETED:LX/G5A;

    invoke-virtual {v0, v3, p1}, LX/G4x;->a(LX/Fso;LX/G5A;)V

    .line 2296190
    const-string v0, "SectionResult should not be absent unless server side error."

    invoke-static {v2, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2296191
    invoke-static {v2}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2296192
    iget v0, v3, LX/Fso;->h:I

    .line 2296193
    iget-object p1, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    invoke-static {p1}, LX/G4j;->a(LX/5SB;)Z

    move-result p1

    .line 2296194
    if-gtz v0, :cond_9

    iget-object p2, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    invoke-virtual {p2}, LX/5SB;->c()Z

    move-result p2

    if-eqz p2, :cond_9

    if-nez p1, :cond_9

    const/4 p1, 0x1

    :goto_1
    move v0, p1

    .line 2296195
    if-eqz v0, :cond_6

    .line 2296196
    new-instance v0, LX/G53;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, LX/G53;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2296197
    :goto_2
    move-object v0, v0

    .line 2296198
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2296199
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4y;

    invoke-virtual {p0, v0}, LX/G59;->b(LX/G4y;)V

    .line 2296200
    :cond_3
    iget-object v0, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    iget-object p0, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/G4x;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v2}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)Z

    move-result p1

    .line 2296201
    if-eqz p1, :cond_a

    .line 2296202
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p2

    .line 2296203
    :goto_3
    move-object p1, p2

    .line 2296204
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result p2

    if-nez p2, :cond_d

    .line 2296205
    :cond_4
    :goto_4
    iget-object v0, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v0, v3, v4, v6, v7}, LX/FqO;->a(LX/Fso;LX/0ta;J)V

    .line 2296206
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2296207
    :cond_5
    iget-object v0, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v0}, LX/FqO;->M()V

    goto/16 :goto_0

    .line 2296208
    :cond_6
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v4, v0, :cond_7

    .line 2296209
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object p1

    iget p2, v3, LX/Fso;->h:I

    iget-object v5, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    invoke-static {v0, p1, p2, v5}, LX/Fsq;->a(Ljava/lang/String;Ljava/lang/String;ILX/5SB;)LX/Fso;

    move-result-object v0

    .line 2296210
    new-instance p1, LX/G51;

    invoke-direct {p1, v0}, LX/G51;-><init>(LX/Fso;)V

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_2

    .line 2296211
    :cond_7
    new-instance v0, LX/G4z;

    invoke-direct {v0, v3}, LX/G4z;-><init>(LX/Fso;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto/16 :goto_2

    .line 2296212
    :cond_8
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto/16 :goto_2

    :cond_9
    const/4 p1, 0x0

    goto/16 :goto_1

    .line 2296213
    :cond_a
    iget-object p2, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, LX/G4x;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 2296214
    if-eqz p2, :cond_c

    .line 2296215
    iget-object p3, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    invoke-virtual {p3, p2}, LX/G4x;->a(Ljava/lang/String;)LX/G59;

    move-result-object p3

    .line 2296216
    if-nez p3, :cond_b

    .line 2296217
    const-string p3, "getScrollLoadTrigger: Expected ID %s to be in section list %s"

    iget-object p4, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    invoke-virtual {p4}, LX/G4x;->d()LX/0Px;

    move-result-object p4

    invoke-static {p3, p2, p4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 2296218
    iget-object p3, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->d:LX/03V;

    const-string p4, "timeline_next_section_not_found"

    invoke-virtual {p3, p4, p2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2296219
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p2

    goto/16 :goto_3

    .line 2296220
    :cond_b
    iget-object p4, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    invoke-static {p4}, LX/G4j;->a(LX/5SB;)Z

    move-result p4

    if-eqz p4, :cond_c

    .line 2296221
    iget-boolean p4, p3, LX/G59;->h:Z

    move p4, p4

    .line 2296222
    if-nez p4, :cond_c

    .line 2296223
    iget-boolean p4, p3, LX/G59;->g:Z

    move p4, p4

    .line 2296224
    if-nez p4, :cond_c

    .line 2296225
    const/4 p4, 0x1

    iput-boolean p4, p3, LX/G59;->i:Z

    .line 2296226
    const/4 p4, 0x0

    const/4 p5, 0x0

    iget-object v5, v1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    invoke-static {p2, p4, p5, v5}, LX/Fsq;->a(Ljava/lang/String;Ljava/lang/String;ILX/5SB;)LX/Fso;

    move-result-object p2

    .line 2296227
    new-instance p4, LX/G51;

    invoke-direct {p4, p2}, LX/G51;-><init>(LX/Fso;)V

    .line 2296228
    sget-object p5, LX/G5A;->LOADING:LX/G5A;

    invoke-virtual {p3, p5, p2}, LX/G59;->a(LX/G5A;LX/Fso;)V

    .line 2296229
    invoke-static {p4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p2

    goto/16 :goto_3

    .line 2296230
    :cond_c
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p2

    goto/16 :goto_3

    .line 2296231
    :cond_d
    invoke-virtual {v0, p0}, LX/G4x;->a(Ljava/lang/String;)LX/G59;

    move-result-object p3

    .line 2296232
    if-eqz p3, :cond_4

    .line 2296233
    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/G4y;

    invoke-virtual {p3, p2}, LX/G59;->b(LX/G4y;)V

    goto/16 :goto_4
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 13

    .prologue
    .line 2296291
    if-eqz p0, :cond_3

    .line 2296292
    iget-object v0, p1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    iget-object v1, p1, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a:LX/5SB;

    invoke-static {v1}, LX/G4j;->a(LX/5SB;)Z

    move-result v1

    const/4 v10, 0x0

    .line 2296293
    const/4 v2, 0x1

    .line 2296294
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v10

    move v6, v2

    :goto_0
    if-ge v9, v12, :cond_3

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/facebook/graphql/model/GraphQLTimelineSection;

    .line 2296295
    new-instance v2, LX/G59;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->k()Ljava/lang/String;

    move-result-object v4

    if-eqz v6, :cond_1

    sget-object v5, LX/G58;->RECENT_SECTION:LX/G58;

    :goto_1
    iget-object v7, v0, LX/G4x;->b:LX/0So;

    if-eqz v6, :cond_2

    iget-object v8, v0, LX/G4x;->e:LX/0qm;

    :goto_2
    move v6, v1

    invoke-direct/range {v2 .. v8}, LX/G59;-><init>(Ljava/lang/String;Ljava/lang/String;LX/G58;ZLX/0So;LX/0qm;)V

    .line 2296296
    iget-object v3, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2296297
    iget-object v3, v0, LX/G4x;->g:LX/G4v;

    if-eqz v3, :cond_0

    .line 2296298
    iget-object v3, v0, LX/G4x;->g:LX/G4v;

    invoke-virtual {v3, v2}, LX/G4v;->a(Ljava/lang/Iterable;)V

    .line 2296299
    :cond_0
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move v6, v10

    goto :goto_0

    .line 2296300
    :cond_1
    sget-object v5, LX/G58;->YEAR_SECTION:LX/G58;

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    .line 2296301
    :cond_3
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;Ljava/util/concurrent/atomic/AtomicBoolean;LX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2296280
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/G5A;->COMPLETED:LX/G5A;

    .line 2296281
    :goto_0
    const/4 p1, 0x0

    .line 2296282
    iget-object v1, p2, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p2, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G59;

    .line 2296283
    iget-object v2, v1, LX/G59;->a:LX/G58;

    move-object v1, v2

    .line 2296284
    sget-object v2, LX/G58;->UNSEEN_SECTION:LX/G58;

    if-ne v1, v2, :cond_0

    .line 2296285
    iget-object v1, p2, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2296286
    :cond_0
    iget-object v1, p4, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    invoke-virtual {v1, p0, v0}, LX/G4x;->a(Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;LX/G5A;)V

    .line 2296287
    sget-object v0, LX/G5A;->COMPLETED:LX/G5A;

    invoke-virtual {p2, v0}, LX/G4x;->a(LX/G5A;)V

    .line 2296288
    invoke-interface {p3}, LX/FqO;->lo_()V

    .line 2296289
    return-void

    .line 2296290
    :cond_1
    sget-object v0, LX/G5A;->LOADING:LX/G5A;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLTimelineSection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2296279
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/Frv;LX/FsM;LX/Fso;Ljava/util/concurrent/atomic/AtomicBoolean;JZLX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V
    .locals 12

    .prologue
    .line 2296274
    const/4 v2, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, LX/G4x;->a(Z)V

    .line 2296275
    iget-object v2, p1, LX/FsM;->a:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-object/from16 v0, p9

    invoke-static {v2, v0}, LX/Frv;->a(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    .line 2296276
    iget-object v6, p1, LX/FsM;->b:Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    iget-object v7, p1, LX/FsM;->c:LX/0ta;

    iget-object v2, p1, LX/FsM;->c:LX/0ta;

    invoke-static {v2}, Lcom/facebook/timeline/protocol/ResultSource;->fromGraphQLResultDataFreshness(LX/0ta;)Lcom/facebook/timeline/protocol/ResultSource;

    move-result-object v8

    move-object v3, p2

    move-wide/from16 v4, p4

    move/from16 v9, p6

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-static/range {v3 .. v11}, LX/Frv;->a(LX/Fso;JLcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;LX/0ta;Lcom/facebook/timeline/protocol/ResultSource;ZLX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    .line 2296277
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2296278
    return-void
.end method

.method private static e(LX/Frv;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "LX/FsM;",
            "LX/FsM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2296273
    new-instance v0, LX/Fri;

    invoke-direct {v0, p0}, LX/Fri;-><init>(LX/Frv;)V

    return-object v0
.end method

.method private static f(LX/Frv;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "LX/FsL;",
            "LX/FsL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2296272
    new-instance v0, LX/Frj;

    invoke-direct {v0, p0}, LX/Frj;-><init>(LX/Frv;)V

    return-object v0
.end method

.method private static g(LX/Frv;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "LX/FsM;",
            "LX/FsM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2296271
    new-instance v0, LX/Frk;

    invoke-direct {v0, p0}, LX/Frk;-><init>(LX/Frv;)V

    return-object v0
.end method

.method private static h(LX/Frv;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "LX/FsL;",
            "LX/FsL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2296270
    new-instance v0, LX/Frl;

    invoke-direct {v0, p0}, LX/Frl;-><init>(LX/Frv;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/FsJ;LX/G4m;LX/G1N;LX/G1I;JLX/G4x;LX/5SB;LX/G2n;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)LX/0Px;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FsJ;",
            "LX/G4m;",
            "LX/G1N;",
            "LX/G1I;",
            "J",
            "LX/G4x;",
            "LX/5SB;",
            "LX/G2n;",
            "LX/FqO;",
            "Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;",
            ")",
            "LX/0Px",
            "<",
            "LX/0zi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2296242
    invoke-static {}, LX/Fsq;->a()LX/Fso;

    move-result-object v3

    .line 2296243
    new-instance v13, LX/0Pz;

    invoke-direct {v13}, LX/0Pz;-><init>()V

    .line 2296244
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->e:LX/0zX;

    if-eqz v2, :cond_0

    .line 2296245
    move-object/from16 v0, p1

    iget-object v4, v0, LX/FsJ;->e:LX/0zX;

    const/4 v6, 0x0

    move-object v2, p0

    move-object/from16 v5, p2

    move-object/from16 v7, p10

    invoke-static/range {v2 .. v7}, LX/Frv;->a(LX/Frv;LX/Fso;LX/0zX;LX/BPB;ZLX/FqO;)LX/0zi;

    move-result-object v2

    .line 2296246
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296247
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->d:LX/0zX;

    if-eqz v2, :cond_1

    .line 2296248
    move-object/from16 v0, p1

    iget-object v4, v0, LX/FsJ;->d:LX/0zX;

    const/4 v6, 0x1

    move-object v2, p0

    move-object/from16 v5, p3

    move-object/from16 v7, p10

    invoke-static/range {v2 .. v7}, LX/Frv;->a(LX/Frv;LX/Fso;LX/0zX;LX/BPB;ZLX/FqO;)LX/0zi;

    move-result-object v2

    .line 2296249
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296250
    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->f:LX/0zX;

    if-eqz v2, :cond_2

    .line 2296251
    move-object/from16 v0, p1

    iget-object v4, v0, LX/FsJ;->f:LX/0zX;

    invoke-virtual/range {p4 .. p4}, LX/G1I;->b()LX/G1N;

    move-result-object v5

    const/4 v6, 0x1

    move-object v2, p0

    move-object/from16 v7, p10

    invoke-static/range {v2 .. v7}, LX/Frv;->a(LX/Frv;LX/Fso;LX/0zX;LX/BPB;ZLX/FqO;)LX/0zi;

    move-result-object v2

    .line 2296252
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296253
    :cond_2
    new-instance v12, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v12}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    .line 2296254
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->a:LX/0zX;

    if-eqz v2, :cond_3

    move-object v4, p0

    move-object/from16 v5, p1

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object v11, v3

    .line 2296255
    invoke-static/range {v4 .. v12}, LX/Frv;->a(LX/Frv;LX/FsJ;JLX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;LX/Fso;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/0zi;

    move-result-object v2

    .line 2296256
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296257
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->c:LX/0zX;

    if-eqz v2, :cond_4

    .line 2296258
    move-object/from16 v0, p1

    iget-object v6, v0, LX/FsJ;->c:LX/0zX;

    move-object v5, p0

    move-wide/from16 v7, p5

    move-object/from16 v9, p7

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-static/range {v5 .. v12}, LX/Frv;->a(LX/Frv;LX/0zX;JLX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/0zi;

    move-result-object v2

    .line 2296259
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296260
    :cond_4
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->g:LX/0zX;

    if-eqz v2, :cond_5

    .line 2296261
    move-object/from16 v0, p1

    iget-object v3, v0, LX/FsJ;->g:LX/0zX;

    move-object v2, p0

    move-object/from16 v4, p4

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p10

    invoke-static/range {v2 .. v7}, LX/Frv;->a(LX/Frv;LX/0zX;LX/G1I;LX/G4x;LX/5SB;LX/FqO;)LX/0zi;

    move-result-object v2

    .line 2296262
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296263
    :cond_5
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->h:LX/0zX;

    if-eqz v2, :cond_6

    .line 2296264
    move-object/from16 v0, p1

    iget-object v8, v0, LX/FsJ;->h:LX/0zX;

    move-object v7, p0

    move-object/from16 v9, p7

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-static/range {v7 .. v12}, LX/Frv;->a(LX/Frv;LX/0zX;LX/G4x;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;Ljava/util/concurrent/atomic/AtomicBoolean;)LX/0zi;

    move-result-object v2

    .line 2296265
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296266
    :cond_6
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->i:LX/0zX;

    if-eqz v2, :cond_7

    .line 2296267
    move-object/from16 v0, p1

    iget-object v2, v0, LX/FsJ;->i:LX/0zX;

    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-static {p0, v2, v0, v1}, LX/Frv;->a(LX/Frv;LX/0zX;LX/G2n;LX/FqO;)LX/0zi;

    move-result-object v2

    .line 2296268
    invoke-virtual {v13, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296269
    :cond_7
    invoke-virtual {v13}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    return-object v2
.end method

.method public final a(LX/0zX;LX/Fso;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)LX/0zi;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;",
            "LX/Fso;",
            "LX/FqO;",
            "Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;",
            ")",
            "LX/0zi;"
        }
    .end annotation

    .prologue
    .line 2296234
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2296235
    iget-object v0, p0, LX/Frv;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 2296236
    new-instance v0, LX/Frf;

    invoke-direct {v0, p0}, LX/Frf;-><init>(LX/Frv;)V

    move-object v0, v0

    .line 2296237
    invoke-virtual {p1, v0}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    .line 2296238
    new-instance v1, LX/Frg;

    invoke-direct {v1, p0}, LX/Frg;-><init>(LX/Frv;)V

    move-object v1, v1

    .line 2296239
    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    .line 2296240
    iget-object v8, p0, LX/Frv;->b:LX/4V5;

    new-instance v1, LX/Frm;

    move-object v2, p0

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, LX/Frm;-><init>(LX/Frv;LX/Fso;JLX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    invoke-virtual {v8, v0, v1}, LX/4V5;->a(LX/0zX;LX/0rl;)LX/0zi;

    move-result-object v0

    .line 2296241
    return-object v0
.end method
