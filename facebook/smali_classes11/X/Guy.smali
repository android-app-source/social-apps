.class public LX/Guy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/A7s;
.implements LX/BWC;


# static fields
.field private static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/A7t;

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field public f:LX/63W;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Z

.field public j:LX/0gc;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2404664
    const-string v0, "REPORT_IP_VIOLATION"

    const-string v1, "REPORT_CONTENT"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Guy;->a:LX/0Rf;

    .line 2404665
    const-string v0, "REDIRECT"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Guy;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/A7t;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2404666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2404667
    iput-object p1, p0, LX/Guy;->c:LX/A7t;

    .line 2404668
    return-void
.end method

.method public static a(LX/0QB;)LX/Guy;
    .locals 2

    .prologue
    .line 2404669
    new-instance v1, LX/Guy;

    invoke-static {p0}, LX/A7t;->b(LX/0QB;)LX/A7t;

    move-result-object v0

    check-cast v0, LX/A7t;

    invoke-direct {v1, v0}, LX/Guy;-><init>(LX/A7t;)V

    .line 2404670
    move-object v0, v1

    .line 2404671
    return-object v0
.end method

.method public static c$redex0(LX/Guy;)V
    .locals 3

    .prologue
    .line 2404672
    iget-object v0, p0, LX/Guy;->f:LX/63W;

    iget-object v1, p0, LX/Guy;->d:Landroid/view/View;

    iget-object v2, p0, LX/Guy;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v0, v1, v2}, LX/63W;->a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2404673
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2404674
    invoke-static {p0}, LX/Guy;->c$redex0(LX/Guy;)V

    .line 2404675
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 3

    .prologue
    .line 2404676
    iget-object v0, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2404677
    const-string v1, "action"

    invoke-interface {p3, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2404678
    iget-object v1, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2404679
    const-string v2, "node_token"

    invoke-interface {p3, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/Guy;->g:Ljava/lang/String;

    .line 2404680
    sget-object v1, LX/Guy;->a:LX/0Rf;

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, LX/Guy;->i:Z

    .line 2404681
    sget-object v1, LX/Guy;->b:LX/0Rf;

    invoke-virtual {v1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, LX/Guy;->h:Z

    .line 2404682
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2404683
    invoke-static {p0}, LX/Guy;->c$redex0(LX/Guy;)V

    .line 2404684
    return-void
.end method
