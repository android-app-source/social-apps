.class public LX/F9o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:[Ljava/lang/String;

.field public final b:LX/F9m;

.field public final c:LX/F9p;

.field private final d:LX/0TD;

.field public final e:Ljava/lang/String;

.field private final f:LX/3Lz;

.field public volatile g:Lcom/facebook/growth/model/DeviceOwnerData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I


# direct methods
.method public constructor <init>(LX/F9m;LX/F9p;LX/0TD;Ljava/lang/String;LX/3Lz;)V
    .locals 3
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2205985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205986
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "My Info"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/F9o;->a:[Ljava/lang/String;

    .line 2205987
    const/4 v0, -0x1

    iput v0, p0, LX/F9o;->h:I

    .line 2205988
    iput-object p1, p0, LX/F9o;->b:LX/F9m;

    .line 2205989
    iput-object p2, p0, LX/F9o;->c:LX/F9p;

    .line 2205990
    iput-object p3, p0, LX/F9o;->d:LX/0TD;

    .line 2205991
    iput-object p4, p0, LX/F9o;->e:Ljava/lang/String;

    .line 2205992
    iput-object p5, p0, LX/F9o;->f:LX/3Lz;

    .line 2205993
    return-void
.end method

.method public static a(LX/0QB;)LX/F9o;
    .locals 9

    .prologue
    .line 2205994
    const-class v1, LX/F9o;

    monitor-enter v1

    .line 2205995
    :try_start_0
    sget-object v0, LX/F9o;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2205996
    sput-object v2, LX/F9o;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2205997
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2205998
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2205999
    new-instance v3, LX/F9o;

    invoke-static {v0}, LX/F9m;->b(LX/0QB;)LX/F9m;

    move-result-object v4

    check-cast v4, LX/F9m;

    .line 2206000
    new-instance v7, LX/F9p;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v6

    check-cast v6, Landroid/content/ContentResolver;

    invoke-direct {v7, v5, v6}, LX/F9p;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;)V

    .line 2206001
    move-object v5, v7

    .line 2206002
    check-cast v5, LX/F9p;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, LX/0TD;

    invoke-static {v0}, LX/14b;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v8

    check-cast v8, LX/3Lz;

    invoke-direct/range {v3 .. v8}, LX/F9o;-><init>(LX/F9m;LX/F9p;LX/0TD;Ljava/lang/String;LX/3Lz;)V

    .line 2206003
    move-object v0, v3

    .line 2206004
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2206005
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/F9o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206006
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2206007
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/F9o;Lcom/facebook/growth/model/DeviceOwnerData;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2206008
    if-nez p1, :cond_1

    .line 2206009
    :cond_0
    :goto_0
    return-void

    .line 2206010
    :cond_1
    iget-object v0, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v0}, Lcom/facebook/growth/model/DeviceOwnerData;->a()Lcom/facebook/growth/model/Birthday;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2206011
    iget-object v0, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->a()Lcom/facebook/growth/model/Birthday;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/growth/model/DeviceOwnerData;->a(Lcom/facebook/growth/model/Birthday;)V

    .line 2206012
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->c()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2206013
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2206014
    :cond_3
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2206015
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/model/FullName;

    .line 2206016
    if-nez v0, :cond_9

    .line 2206017
    :cond_5
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2206018
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->d()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_5
    if-ge v1, v3, :cond_7

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2206019
    invoke-static {p0, v0}, LX/F9o;->a(LX/F9o;Ljava/lang/String;)V

    .line 2206020
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 2206021
    :cond_7
    iget-object v0, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v0}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2206022
    iget-object v0, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {p1}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/growth/model/DeviceOwnerData;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 2206023
    :cond_8
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 2206024
    sget-object v6, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2206025
    iget-object v6, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v6, v5}, Lcom/facebook/growth/model/DeviceOwnerData;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 2206026
    :cond_9
    iget-object v5, v0, Lcom/facebook/growth/model/FullName;->b:Ljava/lang/String;

    invoke-static {p0, v5}, LX/F9o;->c(LX/F9o;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2206027
    iget-object v6, v0, Lcom/facebook/growth/model/FullName;->d:Ljava/lang/String;

    invoke-static {p0, v6}, LX/F9o;->c(LX/F9o;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2206028
    iget-object v7, v0, Lcom/facebook/growth/model/FullName;->c:Ljava/lang/String;

    invoke-static {p0, v7}, LX/F9o;->c(LX/F9o;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2206029
    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-static {v6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_a

    invoke-static {v7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 2206030
    :cond_a
    iget-object v8, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    new-instance v9, Lcom/facebook/growth/model/FullName;

    iget-object v10, v0, Lcom/facebook/growth/model/FullName;->a:Ljava/lang/String;

    invoke-direct {v9, v5, v6, v7, v10}, Lcom/facebook/growth/model/FullName;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/facebook/growth/model/DeviceOwnerData;->a(Lcom/facebook/growth/model/FullName;)V

    goto :goto_4
.end method

.method public static a(LX/F9o;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2206031
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2206032
    :cond_0
    :goto_0
    return-void

    .line 2206033
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/F9o;->f:LX/3Lz;

    iget-object v2, p0, LX/F9o;->e:Ljava/lang/String;

    invoke-virtual {v0, p1, v2}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v0

    .line 2206034
    iget-wide v6, v0, LX/4hT;->nationalNumber_:J

    move-wide v2, v6

    .line 2206035
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 2206036
    :try_start_1
    iget v3, p0, LX/F9o;->h:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    .line 2206037
    iget-object v3, p0, LX/F9o;->f:LX/3Lz;

    iget-object v4, p0, LX/F9o;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, LX/F9o;->h:I

    .line 2206038
    :cond_2
    iget v3, p0, LX/F9o;->h:I

    .line 2206039
    iget v4, v0, LX/4hT;->countryCode_:I

    move v4, v4

    .line 2206040
    if-eq v3, v4, :cond_4

    .line 2206041
    iget-object v3, p0, LX/F9o;->f:LX/3Lz;

    invoke-virtual {v3, v0}, LX/3Lz;->getRegionCodeForNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v0

    .line 2206042
    if-eqz v0, :cond_5

    const-string v3, "ZZ"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch LX/4hE; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    if-nez v3, :cond_5

    :goto_1
    move-object v1, v2

    .line 2206043
    :goto_2
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2206044
    iget-object v2, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v2, v1}, Lcom/facebook/growth/model/DeviceOwnerData;->b(Ljava/lang/String;)V

    .line 2206045
    :cond_3
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v1}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2206046
    iget-object v1, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v1, v0}, Lcom/facebook/growth/model/DeviceOwnerData;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 2206047
    :cond_4
    :try_start_2
    iget-object v0, p0, LX/F9o;->e:Ljava/lang/String;
    :try_end_2
    .catch LX/4hE; {:try_start_2 .. :try_end_2} :catch_1

    move-object v1, v2

    .line 2206048
    goto :goto_2

    :catch_0
    move-object v0, v1

    :goto_3
    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_2

    :catch_1
    move-object v0, v2

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public static c(LX/F9o;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2206049
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2206050
    :cond_0
    :goto_0
    return-object v0

    .line 2206051
    :cond_1
    sget-object v1, LX/1IA;->WHITESPACE:LX/1IA;

    const-string v2, ","

    invoke-static {v2}, LX/1IA;->anyOf(Ljava/lang/CharSequence;)LX/1IA;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1IA;->or(LX/1IA;)LX/1IA;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/1IA;->trimFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 2206052
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2206053
    sget-object v2, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2206054
    iget-object v3, p0, LX/F9o;->a:[Ljava/lang/String;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2206055
    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2206056
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2206057
    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/growth/model/DeviceOwnerData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2206058
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/F9o;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Z)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/growth/model/DeviceOwnerData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2206059
    iget-object v0, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 2206060
    iget-object v0, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2206061
    :goto_0
    return-object v0

    .line 2206062
    :cond_0
    new-instance v0, Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-direct {v0}, Lcom/facebook/growth/model/DeviceOwnerData;-><init>()V

    iput-object v0, p0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2206063
    iget-object v0, p0, LX/F9o;->d:LX/0TD;

    new-instance v1, LX/F9n;

    invoke-direct {v1, p0}, LX/F9n;-><init>(LX/F9o;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
