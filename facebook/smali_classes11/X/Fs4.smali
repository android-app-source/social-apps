.class public LX/Fs4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2296553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;)V
    .locals 2

    .prologue
    .line 2296546
    if-nez p0, :cond_0

    .line 2296547
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected a non-null first section"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2296548
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2296549
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected a non-null node list"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2296550
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2296551
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected a non-empty node list"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2296552
    :cond_2
    return-void
.end method
