.class public LX/GZN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2366459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2366460
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2366440
    check-cast p1, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;

    .line 2366441
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2366442
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "settings_id"

    iget-object v3, p1, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2366443
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "status"

    iget-object v3, p1, Lcom/facebook/commerce/storefront/api/MerchantSubscriptionParams;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2366444
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "update_merchant_subscription_status"

    .line 2366445
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2366446
    move-object v1, v1

    .line 2366447
    const-string v2, "POST"

    .line 2366448
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2366449
    move-object v1, v1

    .line 2366450
    const-string v2, "me/commerce_merchant_subscriptions"

    .line 2366451
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2366452
    move-object v1, v1

    .line 2366453
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2366454
    move-object v0, v1

    .line 2366455
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2366456
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2366457
    move-object v0, v0

    .line 2366458
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2366435
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2366436
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2366437
    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2366438
    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2366439
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
