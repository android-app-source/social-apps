.class public final LX/GYy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GYz;


# direct methods
.method public constructor <init>(LX/GYz;)V
    .locals 0

    .prologue
    .line 2365854
    iput-object p1, p0, LX/GYy;->a:LX/GYz;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2365855
    iget-object v0, p0, LX/GYy;->a:LX/GYz;

    iget-object v0, v0, LX/GYz;->d:LX/GYk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ProductWithEditFields failure. productItemId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/GYy;->a:LX/GYz;

    iget-object v2, v2, LX/GYz;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/GYk;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2365856
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365857
    check-cast p1, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;

    .line 2365858
    iget-object v0, p0, LX/GYy;->a:LX/GYz;

    iget-object v0, v0, LX/GYz;->d:LX/GYk;

    invoke-interface {v0, p1}, LX/GYk;->a(Ljava/lang/Object;)V

    .line 2365859
    return-void
.end method
