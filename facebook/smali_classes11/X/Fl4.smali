.class public final LX/Fl4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V
    .locals 0

    .prologue
    .line 2279315
    iput-object p1, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2279316
    iget-object v0, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-boolean v0, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->M:Z

    if-eqz v0, :cond_0

    .line 2279317
    iget-object v0, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fundraiser_page_fragment_error"

    const-string v2, "Failed initial fundraiser data fetch"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2279318
    iget-object v0, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    const/4 v1, 0x0

    .line 2279319
    iput-boolean v1, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->M:Z

    .line 2279320
    iget-object v0, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    invoke-static {v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->R(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    .line 2279321
    :goto_0
    return-void

    .line 2279322
    :cond_0
    iget-object v0, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    const-string v1, "Failed retried fundraiser data fetch"

    invoke-static {v0, v1, p1}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->a$redex0(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279323
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2279324
    if-eqz p1, :cond_12

    .line 2279325
    iget-object v1, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    .line 2279326
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2279327
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2279328
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2279329
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2279330
    if-nez v0, :cond_1

    .line 2279331
    const-string v2, "no_model"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279332
    :cond_0
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_13

    .line 2279333
    const/4 v2, 0x0

    invoke-static {v2}, LX/BOe;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object v7, v2

    .line 2279334
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result p1

    move v5, v3

    :goto_1
    if-ge v5, p1, :cond_f

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2279335
    invoke-virtual {v7, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2279336
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 2279337
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2279338
    const-string v2, "no_campaign_id"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279339
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2279340
    const-string v2, "no_fundraiser_title"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279341
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-nez v2, :cond_4

    .line 2279342
    const-string v2, "no_profile_picture"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279343
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->s()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2279344
    :cond_5
    const-string v2, "no_charity_text"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279345
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v2

    if-nez v2, :cond_d

    .line 2279346
    const-string v2, "no_owner"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279347
    :cond_7
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 2279348
    invoke-static {v0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {v0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-static {v0}, LX/Fkv;->z(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-eqz v2, :cond_14

    :cond_8
    const/4 v2, 0x1

    :goto_3
    move v2, v2

    .line 2279349
    if-nez v2, :cond_a

    .line 2279350
    :cond_9
    const-string v2, "unrecognized_graphql_type"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279351
    :cond_a
    invoke-static {v0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-static {v0}, LX/Fkv;->c(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2279352
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->p()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserToCharityFragmentModel$CharityInterfaceModel;

    move-result-object v2

    if-nez v2, :cond_c

    .line 2279353
    const-string v2, "no_charity"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2279354
    :cond_c
    invoke-static {v0}, LX/Fkv;->b(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2279355
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v2

    if-nez v2, :cond_e

    .line 2279356
    const-string v2, "no_fundraiser_page"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2279357
    :cond_d
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->y()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserFragmentModel$OwnerModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2279358
    const-string v2, "no_owner_id"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2279359
    :cond_e
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->r()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderFundraiserPageFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2279360
    const-string v2, "no_fundraiser_page_id"

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2279361
    :cond_f
    iget-object v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 2279362
    const-string v2, "fundraiser_campaign_id"

    iget-object v4, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    invoke-virtual {v7, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2279363
    :cond_10
    iget-object v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->l:LX/0Zb;

    invoke-interface {v2, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2279364
    const-string v2, ", "

    invoke-static {v2, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->d(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;Ljava/lang/String;)V

    move v2, v3

    .line 2279365
    :goto_4
    move v2, v2

    .line 2279366
    if-eqz v2, :cond_11

    .line 2279367
    iput-object v0, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2279368
    iget-object v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    if-eqz v2, :cond_11

    .line 2279369
    iget-object v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->E:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;

    iget-object v3, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    iget-object v4, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->J:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageHeaderView;->a(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;Ljava/lang/String;)V

    .line 2279370
    invoke-static {v1}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->V(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V

    .line 2279371
    iget-object v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    .line 2279372
    invoke-static {v2}, LX/Fkv;->q(Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;)Ljava/lang/String;

    move-result-object v3

    .line 2279373
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 2279374
    iget-object v3, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->F:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2279375
    iget-object v3, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->F:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2279376
    :goto_5
    iget-object v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-boolean v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->N:Z

    if-nez v2, :cond_11

    .line 2279377
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->N:Z

    .line 2279378
    iget-object v2, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->l:LX/0Zb;

    iget-object v3, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    iget-object v4, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->J:Ljava/lang/String;

    .line 2279379
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "fundraiser_page_view"

    invoke-direct {v5, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "fundraiser_page"

    .line 2279380
    iput-object v1, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2279381
    move-object v5, v5

    .line 2279382
    const-string v1, "fundraiser_campaign_id"

    invoke-virtual {v5, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v1, "source"

    invoke-virtual {v5, v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v3, v5

    .line 2279383
    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2279384
    :cond_11
    iget-object v0, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->X:LX/0h5;

    if-eqz v0, :cond_12

    iget-object v0, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    if-eqz v0, :cond_12

    .line 2279385
    iget-object v0, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->X:LX/0h5;

    iget-object v1, p0, LX/Fl4;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->L:Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserPageHeaderQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2279386
    :cond_12
    return-void

    :cond_13
    move v2, v4

    goto :goto_4

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 2279387
    :cond_15
    iget-object v4, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->F:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2279388
    iget-object v4, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->F:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method
