.class public LX/FdR;
.super Landroid/widget/BaseExpandableListAdapter;
.source ""


# instance fields
.field private final a:LX/FdU;

.field public final b:LX/FcV;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/CyH;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/FdQ;


# direct methods
.method public constructor <init>(LX/FdU;LX/FcV;)V
    .locals 1
    .param p1    # LX/FdU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2263575
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 2263576
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FdR;->d:Ljava/util/HashMap;

    .line 2263577
    new-instance v0, LX/FdQ;

    invoke-direct {v0, p0}, LX/FdQ;-><init>(LX/FdR;)V

    iput-object v0, p0, LX/FdR;->e:LX/FdQ;

    .line 2263578
    iput-object p1, p0, LX/FdR;->a:LX/FdU;

    .line 2263579
    iput-object p2, p0, LX/FdR;->b:LX/FcV;

    .line 2263580
    return-void
.end method

.method public static a(LX/FdR;LX/FcI;Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)V
    .locals 2

    .prologue
    .line 2263581
    iget-object v0, p0, LX/FdR;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2263582
    :cond_0
    :goto_0
    return-void

    .line 2263583
    :cond_1
    invoke-interface {p1, p2, p3}, LX/FcI;->a(Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)LX/CyH;

    move-result-object v0

    .line 2263584
    if-eqz v0, :cond_0

    .line 2263585
    iget-object v1, p0, LX/FdR;->d:Ljava/util/HashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2263588
    iput-object p1, p0, LX/FdR;->c:LX/0Px;

    .line 2263589
    iget-object v0, p0, LX/FdR;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2263590
    invoke-virtual {p0}, LX/FdR;->notifyDataSetChanged()V

    .line 2263591
    return-void
.end method

.method public final getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2263586
    iget-object v0, p0, LX/FdR;->c:LX/0Px;

    return-object v0
.end method

.method public final getChildId(II)J
    .locals 2

    .prologue
    .line 2263587
    iget-object v0, p0, LX/FdR;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    invoke-interface {v0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2263568
    iget-object v0, p0, LX/FdR;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5uu;

    .line 2263569
    iget-object v0, p0, LX/FdR;->b:LX/FcV;

    invoke-interface {v1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/FcV;->b(Ljava/lang/String;)LX/FcI;

    move-result-object v0

    .line 2263570
    invoke-interface {v0}, LX/FcI;->b()LX/FcE;

    move-result-object v2

    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, LX/FcE;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    .line 2263571
    invoke-interface {v1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v4

    invoke-static {p0, v0, v3, v4}, LX/FdR;->a(LX/FdR;LX/FcI;Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)V

    .line 2263572
    iget-object v3, p0, LX/FdR;->d:Ljava/util/HashMap;

    invoke-interface {v1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CyH;

    iget-object v4, p0, LX/FdR;->a:LX/FdU;

    iget-object v5, p0, LX/FdR;->e:LX/FdQ;

    invoke-interface/range {v0 .. v5}, LX/FcI;->a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V

    .line 2263573
    return-object v2
.end method

.method public final getChildrenCount(I)I
    .locals 1

    .prologue
    .line 2263574
    iget-object v0, p0, LX/FdR;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    invoke-interface {v0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2263557
    iget-object v0, p0, LX/FdR;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getGroupCount()I
    .locals 1

    .prologue
    .line 2263558
    iget-object v0, p0, LX/FdR;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getGroupId(I)J
    .locals 2

    .prologue
    .line 2263559
    iget-object v0, p0, LX/FdR;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    invoke-interface {v0}, LX/5uu;->bl_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2263560
    iget-object v0, p0, LX/FdR;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    .line 2263561
    iget-object v1, p0, LX/FdR;->b:LX/FcV;

    invoke-interface {v0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/FcV;->b(Ljava/lang/String;)LX/FcI;

    move-result-object v2

    .line 2263562
    invoke-interface {v2}, LX/FcI;->a()LX/FcE;

    move-result-object v1

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v3}, LX/FcE;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v3

    .line 2263563
    invoke-interface {v0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v4

    invoke-static {p0, v2, v1, v4}, LX/FdR;->a(LX/FdR;LX/FcI;Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)V

    .line 2263564
    iget-object v1, p0, LX/FdR;->d:Ljava/util/HashMap;

    invoke-interface {v0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CyH;

    iget-object v4, p0, LX/FdR;->e:LX/FdQ;

    invoke-interface {v2, v0, v3, v1, v4}, LX/FcI;->a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V

    .line 2263565
    return-object v3
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2263566
    const/4 v0, 0x1

    return v0
.end method

.method public final isChildSelectable(II)Z
    .locals 1

    .prologue
    .line 2263567
    const/4 v0, 0x0

    return v0
.end method
