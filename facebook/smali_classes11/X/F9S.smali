.class public LX/F9S;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/F9Q;

.field public c:LX/F9R;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2205482
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2205483
    const-class v0, LX/F9S;

    invoke-static {v0, p0}, LX/F9S;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2205484
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/F9S;->setOrientation(I)V

    .line 2205485
    new-instance v0, LX/F9Q;

    invoke-direct {v0, p1, p2}, LX/F9Q;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, LX/F9S;->b:LX/F9Q;

    .line 2205486
    iget-object v0, p0, LX/F9S;->b:LX/F9Q;

    invoke-virtual {p0, v0}, LX/F9S;->addView(Landroid/view/View;)V

    .line 2205487
    iget-object v0, p0, LX/F9S;->a:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 2205488
    sget-object v1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2205489
    new-instance v0, LX/F9R;

    invoke-direct {v0, p1, p2}, LX/F9R;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, LX/F9S;->c:LX/F9R;

    .line 2205490
    iget-object v0, p0, LX/F9S;->c:LX/F9R;

    invoke-virtual {p0, v0}, LX/F9S;->addView(Landroid/view/View;)V

    .line 2205491
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/F9S;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object p0

    check-cast p0, LX/0W9;

    iput-object p0, p1, LX/F9S;->a:LX/0W9;

    return-void
.end method
