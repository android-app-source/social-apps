.class public LX/Gi3;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public final a:Landroid/widget/LinearLayout;

.field public final b:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

.field private final c:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public final d:Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2385229
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2385230
    const v0, 0x7f03078e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2385231
    const v0, 0x7f0d1430

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iput-object v0, p0, LX/Gi3;->b:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 2385232
    const v0, 0x7f0d142f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/Gi3;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2385233
    const v0, 0x7f0d1432

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    iput-object v0, p0, LX/Gi3;->d:Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    .line 2385234
    const v0, 0x7f0d142e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Gi3;->a:Landroid/widget/LinearLayout;

    .line 2385235
    return-void
.end method


# virtual methods
.method public getContentView()Lcom/facebook/fbui/widget/contentview/ContentView;
    .locals 1

    .prologue
    .line 2385236
    iget-object v0, p0, LX/Gi3;->c:Lcom/facebook/fbui/widget/contentview/ContentView;

    return-object v0
.end method
