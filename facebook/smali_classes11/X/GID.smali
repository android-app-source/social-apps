.class public LX/GID;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCallToActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

.field public e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private f:LX/0Uh;

.field public g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 2336035
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->BOOK_TRAVEL:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SHOP_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SIGN_UP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-static/range {v0 .. v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/GID;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2336032
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2336033
    iput-object p1, p0, LX/GID;->f:LX/0Uh;

    .line 2336034
    return-void
.end method

.method public static a$redex0(LX/GID;Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V
    .locals 3

    .prologue
    .line 2336020
    invoke-static {p1}, LX/GCi;->fromGraphQLTypeCallToAction(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)LX/GCi;

    move-result-object v0

    .line 2336021
    iget-object v1, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1}, LX/GCi;->getUri(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Ljava/lang/String;

    move-result-object v0

    .line 2336022
    iget-object v1, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336023
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v2

    .line 2336024
    iput-object p1, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2336025
    iget-object v1, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336026
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v2

    .line 2336027
    iput-object v0, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    .line 2336028
    iget-object v0, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336029
    iput-object p1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2336030
    invoke-static {p0}, LX/GID;->d(LX/GID;)V

    .line 2336031
    return-void
.end method

.method public static b(LX/GID;Z)V
    .locals 7

    .prologue
    .line 2336009
    iget-object v0, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336010
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->l:LX/0Px;

    move-object v2, v1

    .line 2336011
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2336012
    sget-object v4, LX/GID;->a:LX/0Px;

    invoke-virtual {v4, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2336013
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v4, :cond_1

    .line 2336014
    if-eqz p1, :cond_0

    .line 2336015
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iget-object v5, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080a73

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a(Lcom/facebook/graphql/enums/GraphQLCallToActionType;Ljava/lang/String;)V

    .line 2336016
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2336017
    :cond_1
    iget-object v4, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v4, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    goto :goto_1

    .line 2336018
    :cond_2
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    new-instance v1, LX/GIC;

    invoke-direct {v1, p0}, LX/GIC;-><init>(LX/GID;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setOnCheckedChangeListener(LX/Bc9;)V

    .line 2336019
    return-void
.end method

.method public static c(LX/GID;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2336008
    iget-object v1, p0, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/GID;->f:LX/0Uh;

    const/16 v2, 0x2f4

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static d(LX/GID;)V
    .locals 12

    .prologue
    .line 2335953
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335954
    new-instance v1, LX/GFE;

    iget-object v2, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GFE;-><init>(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2335955
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335956
    new-instance v1, LX/GFz;

    iget-object v2, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->R()Z

    move-result v2

    invoke-direct {v1, v2}, LX/GFz;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2335957
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335958
    new-instance v1, LX/GFG;

    invoke-direct {v1}, LX/GFG;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2335959
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335960
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2335961
    iget-object v1, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v8, 0x0

    .line 2335962
    const-string v5, "change_flow_option"

    invoke-static {v1}, LX/GG3;->Q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "call_to_action"

    const/4 v11, 0x1

    move-object v3, v0

    move-object v4, v1

    move-object v9, v8

    move-object v10, v8

    invoke-static/range {v3 .. v11}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2335963
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2336003
    invoke-super {p0}, LX/GHg;->a()V

    .line 2336004
    iput-object v0, p0, LX/GID;->b:Landroid/content/Context;

    .line 2336005
    iput-object v0, p0, LX/GID;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2336006
    iput-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    .line 2336007
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 12

    .prologue
    .line 2335969
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    .line 2335970
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2335971
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/GID;->b:Landroid/content/Context;

    .line 2335972
    iput-object p2, p0, LX/GID;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2335973
    iput-object p1, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    .line 2335974
    iget-object v0, p0, LX/GID;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b7d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    .line 2335975
    invoke-static {p0}, LX/GID;->c(LX/GID;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2335976
    const/4 p2, 0x0

    .line 2335977
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v0, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setInstantWorkflowVisibility(I)V

    .line 2335978
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    new-instance v1, LX/GIB;

    invoke-direct {v1, p0}, LX/GIB;-><init>(LX/GID;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setOnCheckedChangeListenerForIW(LX/Bc9;)V

    .line 2335979
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2335980
    iget-object v1, p0, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;->j()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    invoke-static {v1}, LX/GCi;->fromGraphQLTypeCallToAction(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)LX/GCi;

    move-result-object v1

    .line 2335981
    if-eqz v1, :cond_3

    .line 2335982
    iget-object v2, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    const v3, 0x7f080ba9

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object p1, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v1, p1}, LX/GCi;->getText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f080baa

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2335983
    :goto_0
    invoke-static {p0, p2}, LX/GID;->b(LX/GID;Z)V

    .line 2335984
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a()V

    .line 2335985
    :goto_1
    iget-object v0, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    .line 2335986
    invoke-static {p0}, LX/GID;->c(LX/GID;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2335987
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v1, :cond_4

    .line 2335988
    :cond_0
    iget-object v1, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setCallToActionTypeIndex(I)V

    .line 2335989
    :goto_2
    if-eqz v0, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v1, :cond_5

    .line 2335990
    :cond_1
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    const-string v1, "no_button_tag"

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setInstantWorkflowOption(Ljava/lang/String;)V

    .line 2335991
    :goto_3
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335992
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2335993
    iget-object v1, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const-string v2, "call_to_action"

    const/4 v8, 0x0

    .line 2335994
    const-string v5, "toggle_flow_option"

    invoke-static {v1}, LX/GG3;->Q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v6

    const/4 v11, 0x1

    move-object v3, v0

    move-object v4, v1

    move-object v7, v2

    move-object v9, v8

    move-object v10, v8

    invoke-static/range {v3 .. v11}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2335995
    return-void

    .line 2335996
    :cond_2
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/GID;->b(LX/GID;Z)V

    goto :goto_1

    .line 2335997
    :cond_3
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setInstantWorkflowRadioVisibility(I)V

    goto :goto_0

    .line 2335998
    :cond_4
    iget-object v1, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    iget-object v2, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setCallToActionType(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    goto :goto_2

    .line 2335999
    :cond_5
    if-eqz v0, :cond_6

    iget-object v1, p0, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;->j()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2336000
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    const-string v1, "instant_workflow_tag"

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setInstantWorkflowOption(Ljava/lang/String;)V

    goto :goto_3

    .line 2336001
    :cond_6
    iget-object v0, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    const-string v1, "other_button_tag"

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setInstantWorkflowOption(Ljava/lang/String;)V

    goto :goto_3

    .line 2336002
    :cond_7
    iget-object v1, p0, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setCallToActionType(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    goto :goto_3

    :cond_8
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    goto :goto_4
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2335964
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335965
    iput-object p1, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335966
    iget-object v0, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->F()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    move-result-object v0

    iput-object v0, p0, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    .line 2335967
    iget-object v0, p0, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2335968
    return-void
.end method
