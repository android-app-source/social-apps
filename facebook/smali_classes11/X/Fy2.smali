.class public final LX/Fy2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fy3;


# direct methods
.method public constructor <init>(LX/Fy3;)V
    .locals 0

    .prologue
    .line 2306224
    iput-object p1, p0, LX/Fy2;->a:LX/Fy3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2306227
    iget-object v0, p0, LX/Fy2;->a:LX/Fy3;

    iget-object v0, v0, LX/Fy3;->e:LX/Fy4;

    iget-object v1, p0, LX/Fy2;->a:LX/Fy3;

    iget-wide v2, v1, LX/Fy3;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306228
    iget-object v0, p0, LX/Fy2;->a:LX/Fy3;

    iget-object v0, v0, LX/Fy3;->e:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hZ;

    invoke-virtual {v0, p1}, LX/2hZ;->a(Ljava/lang/Throwable;)V

    .line 2306229
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2306225
    iget-object v0, p0, LX/Fy2;->a:LX/Fy3;

    iget-object v0, v0, LX/Fy3;->e:LX/Fy4;

    iget-object v1, p0, LX/Fy2;->a:LX/Fy3;

    iget-wide v2, v1, LX/Fy3;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306226
    return-void
.end method
