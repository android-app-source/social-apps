.class public LX/Fz2;
.super LX/BPB;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BPB",
        "<",
        "Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLInterfaces$InfoReviewItemsFragment$;",
        ">;"
    }
.end annotation


# instance fields
.field public f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2307441
    invoke-direct {p0}, LX/BPB;-><init>()V

    .line 2307442
    new-instance v0, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    invoke-direct {v0}, Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;-><init>()V

    iput-object v0, p0, LX/Fz2;->f:Lcom/facebook/timeline/inforeview/InfoReviewProfileQuestionStatusData;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;I)V
    .locals 1
    .param p1    # Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2307443
    invoke-super {p0, p1, p2}, LX/BPB;->a(Ljava/lang/Object;I)V

    .line 2307444
    invoke-virtual {p0}, LX/BPB;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2307445
    const/4 v0, 0x2

    iput v0, p0, LX/Fz2;->d:I

    .line 2307446
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2307447
    check-cast p1, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    invoke-virtual {p0, p1, p2}, LX/Fz2;->a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 2307448
    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    .line 2307449
    new-instance v1, LX/5z7;

    invoke-direct {v1}, LX/5z7;-><init>()V

    .line 2307450
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->b()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/5z7;->a:LX/0Px;

    .line 2307451
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->j()Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    move-result-object v2

    iput-object v2, v1, LX/5z7;->b:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    .line 2307452
    move-object v2, v1

    .line 2307453
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2307454
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    .line 2307455
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->cn_()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->cn_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2307456
    invoke-static {v0}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;->a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;)Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemFragmentModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2307457
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2307458
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2307459
    iput-object v0, v2, LX/5z7;->a:LX/0Px;

    .line 2307460
    const/4 v11, 0x1

    const/4 v13, 0x0

    const/4 v9, 0x0

    .line 2307461
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 2307462
    iget-object v8, v2, LX/5z7;->a:LX/0Px;

    invoke-static {v7, v8}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 2307463
    iget-object v10, v2, LX/5z7;->b:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    invoke-static {v7, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2307464
    const/4 v12, 0x2

    invoke-virtual {v7, v12}, LX/186;->c(I)V

    .line 2307465
    invoke-virtual {v7, v13, v8}, LX/186;->b(II)V

    .line 2307466
    invoke-virtual {v7, v11, v10}, LX/186;->b(II)V

    .line 2307467
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 2307468
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 2307469
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 2307470
    invoke-virtual {v8, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2307471
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2307472
    new-instance v8, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;

    invoke-direct {v8, v7}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;-><init>(LX/15i;)V

    .line 2307473
    move-object v0, v8

    .line 2307474
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/Fz2;->a(Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewItemsFragmentModel;I)V

    .line 2307475
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2307476
    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
