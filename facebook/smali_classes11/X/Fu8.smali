.class public final LX/Fu8;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Fu8;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Fu6;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Fu9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2299694
    const/4 v0, 0x0

    sput-object v0, LX/Fu8;->a:LX/Fu8;

    .line 2299695
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Fu8;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2299696
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2299697
    new-instance v0, LX/Fu9;

    invoke-direct {v0}, LX/Fu9;-><init>()V

    iput-object v0, p0, LX/Fu8;->c:LX/Fu9;

    .line 2299698
    return-void
.end method

.method public static declared-synchronized q()LX/Fu8;
    .locals 2

    .prologue
    .line 2299699
    const-class v1, LX/Fu8;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Fu8;->a:LX/Fu8;

    if-nez v0, :cond_0

    .line 2299700
    new-instance v0, LX/Fu8;

    invoke-direct {v0}, LX/Fu8;-><init>()V

    sput-object v0, LX/Fu8;->a:LX/Fu8;

    .line 2299701
    :cond_0
    sget-object v0, LX/Fu8;->a:LX/Fu8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2299702
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2299703
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p0, 0x7f0a0168

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-interface {v0, v1}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    const p0, 0x7f0815b4

    invoke-virtual {v1, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0b0051

    invoke-virtual {v1, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0a0552

    invoke-virtual {v1, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const p0, 0x3c23d70a    # 0.01f

    invoke-virtual {v1, p0}, LX/1ne;->c(F)LX/1ne;

    move-result-object v1

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-virtual {v1, p0}, LX/1ne;->e(F)LX/1ne;

    move-result-object v1

    const p0, 0x7f0a0553

    invoke-virtual {v1, p0}, LX/1ne;->l(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 p0, 0x2

    invoke-interface {v1, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    const/4 p0, 0x3

    const p2, 0x7f0b0dc5

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2299704
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2299705
    invoke-static {}, LX/1dS;->b()V

    .line 2299706
    const/4 v0, 0x0

    return-object v0
.end method
