.class public LX/G60;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/G62;

.field public final b:LX/G63;


# direct methods
.method public constructor <init>(LX/G62;LX/G63;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319600
    iput-object p1, p0, LX/G60;->a:LX/G62;

    .line 2319601
    iput-object p2, p0, LX/G60;->b:LX/G63;

    .line 2319602
    return-void
.end method

.method public static a(LX/G60;Landroid/widget/TextView;Ljava/lang/String;J)Landroid/text/style/ClickableSpan;
    .locals 7

    .prologue
    .line 2319618
    new-instance v0, LX/G5z;

    move-object v1, p0

    move-wide v2, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/G5z;-><init>(LX/G60;JLandroid/widget/TextView;Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2319619
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2319620
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2319621
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2319622
    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2319623
    const-string v1, "+"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 2319624
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;Landroid/view/ViewStub;)V
    .locals 11

    .prologue
    .line 2319603
    const-string v0, "footer_text"

    invoke-static {p1, v0}, LX/G60;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2319604
    const-string v0, "short_name"

    invoke-static {p1, v0}, LX/G60;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2319605
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2319606
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-gez v0, :cond_1

    .line 2319607
    :cond_0
    :goto_0
    return-void

    .line 2319608
    :cond_1
    invoke-virtual {p2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 2319609
    const-string v1, "footer_link_text"

    invoke-static {p1, v1}, LX/G60;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2319610
    const v1, 0x7f0d2b03

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2319611
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2319612
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2319613
    :goto_1
    const v1, 0x7f0d2b04

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, LX/G5x;

    invoke-direct {v2, p0, v0}, LX/G5x;-><init>(LX/G60;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2319614
    :cond_2
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    move-object v1, p0

    .line 2319615
    new-instance v8, LX/47x;

    invoke-virtual {v2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-direct {v8, v9}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v8, v3}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v8

    const-string v9, "link_stop_suggestions"

    invoke-static {v1, v2, v5, v6, v7}, LX/G60;->a(LX/G60;Landroid/widget/TextView;Ljava/lang/String;J)Landroid/text/style/ClickableSpan;

    move-result-object v10

    const/16 p1, 0x21

    invoke-virtual {v8, v9, v4, v10, p1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v8

    .line 2319616
    invoke-virtual {v8}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v8

    move-object v1, v8

    .line 2319617
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
