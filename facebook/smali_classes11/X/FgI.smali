.class public LX/FgI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FgI;


# instance fields
.field private final a:LX/CvY;

.field private final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/CvY;LX/0Uh;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2270205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270206
    iput-object p1, p0, LX/FgI;->a:LX/CvY;

    .line 2270207
    iput-object p2, p0, LX/FgI;->b:LX/0Uh;

    .line 2270208
    return-void
.end method

.method private static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270200
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 2270201
    invoke-static {p0}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2270202
    :goto_0
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0

    .line 2270203
    :sswitch_0
    const-string v1, "media_combined_module_number_of_annotated_results"

    invoke-static {p0}, LX/Cvf;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 2270204
    :sswitch_1
    const-string v1, "thumbnail_decorations"

    invoke-static {p0}, LX/Cvh;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5852ec13 -> :sswitch_0
        -0x5389cb28 -> :sswitch_1
        -0x30cf2d19 -> :sswitch_1
        0x4ed245b -> :sswitch_1
        0xd5fee8a -> :sswitch_1
        0x735086f1 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/0QB;)LX/FgI;
    .locals 5

    .prologue
    .line 2270141
    sget-object v0, LX/FgI;->c:LX/FgI;

    if-nez v0, :cond_1

    .line 2270142
    const-class v1, LX/FgI;

    monitor-enter v1

    .line 2270143
    :try_start_0
    sget-object v0, LX/FgI;->c:LX/FgI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2270144
    if-eqz v2, :cond_0

    .line 2270145
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2270146
    new-instance p0, LX/FgI;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v3

    check-cast v3, LX/CvY;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/FgI;-><init>(LX/CvY;LX/0Uh;)V

    .line 2270147
    move-object v0, p0

    .line 2270148
    sput-object v0, LX/FgI;->c:LX/FgI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2270149
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2270150
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2270151
    :cond_1
    sget-object v0, LX/FgI;->c:LX/FgI;

    return-object v0

    .line 2270152
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2270153
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;ILcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2270187
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    .line 2270188
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 2270189
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x4c808d5

    if-eq v0, v2, :cond_2

    .line 2270190
    :cond_0
    :goto_1
    return-void

    .line 2270191
    :cond_1
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v0, v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    goto :goto_0

    .line 2270192
    :cond_2
    invoke-static {p1}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 2270193
    if-eqz v6, :cond_0

    .line 2270194
    invoke-static {v6}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 2270195
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ae()Ljava/lang/String;

    move-result-object v5

    move-object v0, p3

    move v1, p2

    move-object v3, p1

    invoke-static/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2270196
    invoke-static {v7}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2270197
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FgI;->b:LX/0Uh;

    sget v2, LX/2SU;->Y:I

    invoke-virtual {v1, v2, v8}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2270198
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2270199
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1
.end method

.method private a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;ILcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;LX/0Px;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V
    .locals 18
    .param p3    # Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            "I",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;",
            "Lcom/facebook/search/results/model/contract/SearchResultsContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2270171
    invoke-static/range {p1 .. p1}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    .line 2270172
    invoke-static/range {p1 .. p1}, LX/8eM;->e(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v8

    .line 2270173
    invoke-static/range {p1 .. p1}, LX/CvY;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CvV;

    move-result-object v17

    .line 2270174
    invoke-static/range {p1 .. p1}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 2270175
    invoke-static/range {p1 .. p1}, LX/8eM;->j(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v11

    .line 2270176
    if-lez v11, :cond_2

    .line 2270177
    move-object/from16 v0, p0

    iget-object v13, v0, LX/FgI;->a:LX/CvY;

    const/4 v7, 0x0

    invoke-static/range {p1 .. p1}, LX/8eM;->d(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aK()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aK()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;->c()Ljava/lang/String;

    move-result-object v10

    :goto_0
    invoke-static/range {p1 .. p1}, LX/FgI;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0P1;

    move-result-object v12

    move-object/from16 v3, p5

    move/from16 v4, p2

    move-object/from16 v5, p4

    invoke-static/range {v3 .. v12}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;ILX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object/from16 v0, p5

    move/from16 v1, p2

    move-object/from16 v2, v17

    invoke-virtual {v13, v0, v1, v2, v3}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2270178
    :cond_0
    :goto_1
    return-void

    .line 2270179
    :cond_1
    const/4 v10, 0x0

    goto :goto_0

    .line 2270180
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, -0x59a2740a

    if-ne v4, v5, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgI;->b:LX/0Uh;

    sget v5, LX/2SU;->af:I

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-nez v4, :cond_3

    if-eqz p3, :cond_3

    invoke-static/range {p3 .. p3}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, 0x4c808d5

    if-ne v4, v5, :cond_3

    .line 2270181
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->k()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    .line 2270182
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v7

    .line 2270183
    move-object/from16 v0, p0

    iget-object v9, v0, LX/FgI;->a:LX/CvY;

    const/4 v8, 0x0

    move-object/from16 v4, p5

    move/from16 v5, p2

    invoke-static/range {v4 .. v8}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object/from16 v0, p5

    move/from16 v1, p2

    move-object/from16 v2, v17

    invoke-virtual {v9, v0, v1, v2, v3}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_1

    .line 2270184
    :cond_3
    invoke-static/range {p1 .. p1}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, 0x4c808d5

    if-ne v4, v5, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgI;->b:LX/0Uh;

    sget v5, LX/2SU;->af:I

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2270185
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/FgI;->b:LX/0Uh;

    invoke-static {v3, v4}, LX/FgI;->a(ILX/0Uh;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2270186
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FgI;->a:LX/CvY;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static/range {p1 .. p1}, LX/8eM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v15

    invoke-static/range {p1 .. p1}, LX/FgI;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0P1;

    move-result-object v16

    move-object/from16 v9, p5

    move/from16 v10, p2

    move-object v11, v6

    move-object v14, v8

    invoke-static/range {v9 .. v16}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    move-object/from16 v0, p5

    move/from16 v1, p2

    move-object/from16 v2, v17

    invoke-virtual {v3, v0, v1, v2, v4}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_1
.end method

.method public static a(ILX/0Uh;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2270167
    const v1, -0x59a2740a

    if-ne p0, v1, :cond_1

    sget v1, LX/2SU;->af:I

    invoke-virtual {p1, v1, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2270168
    :cond_0
    :goto_0
    return v0

    .line 2270169
    :cond_1
    const v1, -0x49f04592

    if-eq p0, v1, :cond_0

    .line 2270170
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;LX/0Px;LX/0Px;ILjava/lang/String;)V
    .locals 9
    .param p4    # LX/0Px;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/contract/SearchResultsContext;",
            "LX/CzA;",
            "LX/0Px",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            ">;>;",
            "LX/0Px",
            "<",
            "LX/4FP;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2270154
    iget-object v8, p0, LX/FgI;->a:LX/CvY;

    iget-object v0, p0, LX/FgI;->a:LX/CvY;

    .line 2270155
    iget v1, p2, LX/CzA;->c:I

    move v2, v1

    .line 2270156
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move v4, p5

    move-object v5, p4

    invoke-virtual/range {v0 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IIILX/0Px;LX/8cg;LX/8cf;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v8, p1, p6, p5, v0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;ILcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2270157
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v0

    if-ge v6, v0, :cond_2

    .line 2270158
    invoke-virtual {p3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2270159
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v6, v1, :cond_1

    add-int/lit8 v1, v6, 0x1

    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v3, v1

    .line 2270160
    :goto_1
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-virtual {p2, v1}, LX/CzA;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v2

    .line 2270161
    iget-object v1, p0, LX/FgI;->b:LX/0Uh;

    sget v4, LX/2SU;->Z:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2270162
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-direct {p0, v1, v2, p1}, LX/FgI;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;ILcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2270163
    :cond_0
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, p0

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/FgI;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;ILcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;LX/0Px;Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2270164
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2270165
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 2270166
    :cond_2
    return-void
.end method
