.class public final LX/H1a;
.super LX/1OM;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H1U;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/H1U;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2415553
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2415554
    iput-object p1, p0, LX/H1a;->a:Landroid/content/Context;

    .line 2415555
    iput-object p2, p0, LX/H1a;->b:Ljava/util/List;

    .line 2415556
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2415557
    new-instance v0, LX/62U;

    new-instance v1, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v2, p0, LX/H1a;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/62U;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2415558
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2415559
    iget-object v1, p0, LX/H1a;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H0v;

    .line 2415560
    invoke-virtual {v1}, LX/H0v;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415561
    new-instance v2, LX/H1Z;

    invoke-direct {v2, p0, v1}, LX/H1Z;-><init>(LX/H1a;LX/H0v;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2415562
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2415563
    iget-object v0, p0, LX/H1a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
