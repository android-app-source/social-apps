.class public LX/FbM;
.super LX/FbH;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2260081
    invoke-direct {p0}, LX/FbH;-><init>()V

    .line 2260082
    return-void
.end method

.method public static a(LX/0QB;)LX/FbM;
    .locals 3

    .prologue
    .line 2260070
    const-class v1, LX/FbM;

    monitor-enter v1

    .line 2260071
    :try_start_0
    sget-object v0, LX/FbM;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2260072
    sput-object v2, LX/FbM;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2260073
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2260074
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2260075
    new-instance v0, LX/FbM;

    invoke-direct {v0}, LX/FbM;-><init>()V

    .line 2260076
    move-object v0, v0

    .line 2260077
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2260078
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FbM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260079
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2260080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/8dO;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260054
    new-instance v0, LX/8dO;

    invoke-direct {v0}, LX/8dO;-><init>()V

    new-instance v1, LX/8dQ;

    invoke-direct {v1}, LX/8dQ;-><init>()V

    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, 0x72e46842

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2260055
    iput-object v2, v1, LX/8dQ;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2260056
    move-object v1, v1

    .line 2260057
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    .line 2260058
    iput-object v2, v1, LX/8dQ;->Z:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2260059
    move-object v1, v1

    .line 2260060
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->q()Ljava/lang/String;

    move-result-object v2

    .line 2260061
    iput-object v2, v1, LX/8dQ;->ab:Ljava/lang/String;

    .line 2260062
    move-object v1, v1

    .line 2260063
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v2

    .line 2260064
    iput-object v2, v1, LX/8dQ;->q:LX/0Px;

    .line 2260065
    move-object v1, v1

    .line 2260066
    invoke-virtual {v1}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    .line 2260067
    iput-object v1, v0, LX/8dO;->f:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2260068
    move-object v0, v0

    .line 2260069
    return-object v0
.end method
