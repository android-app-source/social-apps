.class public final LX/FkJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCreationModels$FundraiserCreateMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:LX/FkL;


# direct methods
.method public constructor <init>(LX/FkL;)V
    .locals 0

    .prologue
    .line 2278470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278471
    iput-object p1, p0, LX/FkJ;->a:LX/FkL;

    .line 2278472
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2278473
    iget-object v0, p0, LX/FkJ;->a:LX/FkL;

    if-eqz v0, :cond_0

    .line 2278474
    iget-object v0, p0, LX/FkJ;->a:LX/FkL;

    invoke-interface {v0, p1}, LX/FkL;->a(Ljava/lang/Throwable;)V

    .line 2278475
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2278476
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2278477
    iget-object v0, p0, LX/FkJ;->a:LX/FkL;

    if-eqz v0, :cond_0

    .line 2278478
    iget-object v0, p0, LX/FkJ;->a:LX/FkL;

    invoke-interface {v0, p1}, LX/FkL;->b(Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2278479
    :cond_0
    return-void
.end method
