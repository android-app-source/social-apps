.class public final LX/Fy3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/2h8;

.field public final synthetic c:LX/2hC;

.field public final synthetic d:LX/5P2;

.field public final synthetic e:LX/Fy4;


# direct methods
.method public constructor <init>(LX/Fy4;JLX/2h8;LX/2hC;LX/5P2;)V
    .locals 0

    .prologue
    .line 2306230
    iput-object p1, p0, LX/Fy3;->e:LX/Fy4;

    iput-wide p2, p0, LX/Fy3;->a:J

    iput-object p4, p0, LX/Fy3;->b:LX/2h8;

    iput-object p5, p0, LX/Fy3;->c:LX/2hC;

    iput-object p6, p0, LX/Fy3;->d:LX/5P2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 2306231
    iget-object v0, p0, LX/Fy3;->e:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2dj;

    iget-wide v2, p0, LX/Fy3;->a:J

    iget-object v4, p0, LX/Fy3;->b:LX/2h8;

    iget-object v5, p0, LX/Fy3;->c:LX/2hC;

    iget-object v6, p0, LX/Fy3;->d:LX/5P2;

    invoke-virtual/range {v1 .. v6}, LX/2dj;->a(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2306232
    iget-object v1, p0, LX/Fy3;->e:LX/Fy4;

    iget-wide v2, p0, LX/Fy3;->a:J

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v4, v5}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306233
    new-instance v1, LX/Fy2;

    invoke-direct {v1, p0}, LX/Fy2;-><init>(LX/Fy3;)V

    iget-object v2, p0, LX/Fy3;->e:LX/Fy4;

    iget-object v2, v2, LX/Fy4;->i:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2306234
    return-void
.end method
