.class public LX/GHF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;
.implements LX/GGk;


# instance fields
.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/GDm;

.field private final k:LX/GCB;

.field public l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;


# direct methods
.method public constructor <init>(LX/GDm;LX/GCB;LX/GLH;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334921
    iput-object p1, p0, LX/GHF;->j:LX/GDm;

    .line 2334922
    iput-object p2, p0, LX/GHF;->k:LX/GCB;

    .line 2334923
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/GEZ;

    const v2, 0x7f030082

    sget-object v3, LX/GHF;->a:LX/GGX;

    sget-object v4, LX/8wK;->DURATION:LX/8wK;

    invoke-direct {v1, v2, p3, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GHF;->i:LX/0Px;

    .line 2334924
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334925
    iget-object v0, p0, LX/GHF;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 1

    .prologue
    .line 2334926
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object v0, p0, LX/GHF;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334927
    iget-object v0, p0, LX/GHF;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-interface {p2, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334928
    return-void
.end method

.method public final b()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 2334929
    iget-object v0, p0, LX/GHF;->k:LX/GCB;

    invoke-virtual {v0}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/GGh;
    .locals 1

    .prologue
    .line 2334930
    new-instance v0, LX/GHE;

    invoke-direct {v0, p0}, LX/GHE;-><init>(LX/GHF;)V

    return-object v0
.end method
