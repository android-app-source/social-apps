.class public LX/FRM;
.super LX/6sU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sU",
        "<",
        "Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;",
        "Lcom/facebook/payments/history/model/PaymentTransactions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240274
    const-class v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-direct {p0, p1, v0}, LX/6sU;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2240275
    return-void
.end method

.method public static b(LX/0QB;)LX/FRM;
    .locals 2

    .prologue
    .line 2240276
    new-instance v1, LX/FRM;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/FRM;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2240277
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2240278
    check-cast p1, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;

    .line 2240279
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2240280
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer() {  pay_account {    pay_transactions%s {      page_info {        has_next_page      },      nodes {        id,        creation_time,        updated_time,        uri,        sender_pay_profile {          id,          name,          profile_picture {            uri          }        },        receiver_pay_profile {          id,          name,          profile_picture {            uri          }        },        pay_transaction_status,        transaction_amount {          currency,          amount_in_hundredths        }      }    }  }}"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2240281
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2240282
    if-eqz p1, :cond_1

    .line 2240283
    iget-object v7, p1, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->a:Ljava/lang/Long;

    if-eqz v7, :cond_0

    .line 2240284
    new-instance v7, Ljava/lang/StringBuilder;

    const-string p0, ".before_time("

    invoke-direct {v7, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, p1, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->a:Ljava/lang/Long;

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string p0, ")"

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2240285
    :cond_0
    iget-object v7, p1, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->b:Ljava/lang/Integer;

    if-eqz v7, :cond_1

    .line 2240286
    new-instance v7, Ljava/lang/StringBuilder;

    const-string p0, ".first("

    invoke-direct {v7, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, p1, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->b:Ljava/lang/Integer;

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string p0, ")"

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2240287
    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v6, v6

    .line 2240288
    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2240289
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "get_payment_history"

    .line 2240290
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2240291
    move-object v1, v1

    .line 2240292
    const-string v2, "GET"

    .line 2240293
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2240294
    move-object v1, v1

    .line 2240295
    const-string v2, "graphql"

    .line 2240296
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2240297
    move-object v1, v1

    .line 2240298
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2240299
    move-object v0, v1

    .line 2240300
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2240301
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2240302
    move-object v0, v0

    .line 2240303
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2240304
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/FRP;->a(LX/0lF;)Lcom/facebook/payments/history/model/PaymentTransactions;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2240305
    const-string v0, "get_payment_history"

    return-object v0
.end method
