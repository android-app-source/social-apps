.class public LX/G9N;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[LX/G92;


# instance fields
.field private final b:LX/G9b;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2322373
    const/4 v0, 0x0

    new-array v0, v0, [LX/G92;

    sput-object v0, LX/G9N;->a:[LX/G92;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2322493
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322494
    new-instance v0, LX/G9b;

    invoke-direct {v0}, LX/G9b;-><init>()V

    iput-object v0, p0, LX/G9N;->b:LX/G9b;

    return-void
.end method

.method private static a([ILX/G96;)F
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2322495
    iget v0, p1, LX/G96;->b:I

    move v7, v0

    .line 2322496
    iget v0, p1, LX/G96;->a:I

    move v8, v0

    .line 2322497
    aget v3, p0, v2

    .line 2322498
    aget v0, p0, v1

    move v4, v1

    move v5, v0

    move v6, v3

    move v0, v2

    .line 2322499
    :goto_0
    if-ge v6, v8, :cond_1

    if-ge v5, v7, :cond_1

    .line 2322500
    invoke-virtual {p1, v6, v5}, LX/G96;->a(II)Z

    move-result v3

    if-eq v4, v3, :cond_4

    .line 2322501
    add-int/lit8 v3, v0, 0x1

    const/4 v0, 0x5

    if-eq v3, v0, :cond_1

    .line 2322502
    if-nez v4, :cond_0

    move v0, v1

    :goto_1
    move v9, v3

    move v3, v0

    move v0, v9

    .line 2322503
    :goto_2
    add-int/lit8 v6, v6, 0x1

    .line 2322504
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_0

    :cond_0
    move v0, v2

    .line 2322505
    goto :goto_1

    .line 2322506
    :cond_1
    if-eq v6, v8, :cond_2

    if-ne v5, v7, :cond_3

    .line 2322507
    :cond_2
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2322508
    throw v0

    .line 2322509
    :cond_3
    aget v0, p0, v2

    sub-int v0, v6, v0

    int-to-float v0, v0

    const/high16 v1, 0x40e00000    # 7.0f

    div-float/2addr v0, v1

    return v0

    :cond_4
    move v3, v4

    goto :goto_2
.end method

.method private static a(LX/G96;)LX/G96;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2322420
    const/4 v1, 0x0

    .line 2322421
    move v0, v1

    .line 2322422
    :goto_0
    iget-object v3, p0, LX/G96;->d:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, LX/G96;->d:[I

    aget v3, v3, v0

    if-nez v3, :cond_0

    .line 2322423
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2322424
    :cond_0
    iget-object v3, p0, LX/G96;->d:[I

    array-length v3, v3

    if-ne v0, v3, :cond_11

    .line 2322425
    const/4 v0, 0x0

    .line 2322426
    :goto_1
    move-object v0, v0

    .line 2322427
    iget-object v1, p0, LX/G96;->d:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    .line 2322428
    :goto_2
    if-ltz v1, :cond_1

    iget-object v3, p0, LX/G96;->d:[I

    aget v3, v3, v1

    if-nez v3, :cond_1

    .line 2322429
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 2322430
    :cond_1
    if-gez v1, :cond_13

    .line 2322431
    const/4 v1, 0x0

    .line 2322432
    :goto_3
    move-object v1, v1

    .line 2322433
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    .line 2322434
    :cond_2
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2322435
    throw v0

    .line 2322436
    :cond_3
    invoke-static {v0, p0}, LX/G9N;->a([ILX/G96;)F

    move-result v5

    .line 2322437
    aget v3, v0, v4

    .line 2322438
    aget v6, v1, v4

    .line 2322439
    aget v4, v0, v2

    .line 2322440
    aget v0, v1, v2

    .line 2322441
    if-ge v4, v0, :cond_4

    if-lt v3, v6, :cond_5

    .line 2322442
    :cond_4
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2322443
    throw v0

    .line 2322444
    :cond_5
    sub-int v1, v6, v3

    sub-int v7, v0, v4

    if-eq v1, v7, :cond_6

    .line 2322445
    sub-int v0, v6, v3

    add-int/2addr v0, v4

    .line 2322446
    :cond_6
    sub-int v1, v0, v4

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 2322447
    sub-int v1, v6, v3

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 2322448
    if-lez v7, :cond_7

    if-gtz v8, :cond_8

    .line 2322449
    :cond_7
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2322450
    throw v0

    .line 2322451
    :cond_8
    if-eq v8, v7, :cond_9

    .line 2322452
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2322453
    throw v0

    .line 2322454
    :cond_9
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v5, v1

    float-to-int v9, v1

    .line 2322455
    add-int v1, v3, v9

    .line 2322456
    add-int v3, v4, v9

    .line 2322457
    add-int/lit8 v4, v7, -0x1

    int-to-float v4, v4

    mul-float/2addr v4, v5

    float-to-int v4, v4

    add-int/2addr v4, v3

    sub-int v0, v4, v0

    .line 2322458
    if-lez v0, :cond_10

    .line 2322459
    if-le v0, v9, :cond_a

    .line 2322460
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2322461
    throw v0

    .line 2322462
    :cond_a
    sub-int v0, v3, v0

    move v4, v0

    .line 2322463
    :goto_4
    add-int/lit8 v0, v8, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, v5

    float-to-int v0, v0

    add-int/2addr v0, v1

    sub-int/2addr v0, v6

    .line 2322464
    if-lez v0, :cond_f

    .line 2322465
    if-le v0, v9, :cond_b

    .line 2322466
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2322467
    throw v0

    .line 2322468
    :cond_b
    sub-int v0, v1, v0

    .line 2322469
    :goto_5
    new-instance v6, LX/G96;

    invoke-direct {v6, v7, v8}, LX/G96;-><init>(II)V

    move v3, v2

    .line 2322470
    :goto_6
    if-ge v3, v8, :cond_e

    .line 2322471
    int-to-float v1, v3

    mul-float/2addr v1, v5

    float-to-int v1, v1

    add-int v9, v0, v1

    move v1, v2

    .line 2322472
    :goto_7
    if-ge v1, v7, :cond_d

    .line 2322473
    int-to-float v10, v1

    mul-float/2addr v10, v5

    float-to-int v10, v10

    add-int/2addr v10, v4

    invoke-virtual {p0, v10, v9}, LX/G96;->a(II)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 2322474
    invoke-virtual {v6, v1, v3}, LX/G96;->b(II)V

    .line 2322475
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 2322476
    :cond_d
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6

    .line 2322477
    :cond_e
    return-object v6

    :cond_f
    move v0, v1

    goto :goto_5

    :cond_10
    move v4, v3

    goto :goto_4

    .line 2322478
    :cond_11
    iget v3, p0, LX/G96;->c:I

    div-int v3, v0, v3

    .line 2322479
    iget v5, p0, LX/G96;->c:I

    rem-int v5, v0, v5

    mul-int/lit8 v5, v5, 0x20

    .line 2322480
    iget-object v6, p0, LX/G96;->d:[I

    aget v6, v6, v0

    move v0, v1

    .line 2322481
    :goto_8
    rsub-int/lit8 v7, v0, 0x1f

    shl-int v7, v6, v7

    if-nez v7, :cond_12

    .line 2322482
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 2322483
    :cond_12
    add-int/2addr v5, v0

    .line 2322484
    const/4 v0, 0x2

    new-array v0, v0, [I

    aput v5, v0, v1

    const/4 v1, 0x1

    aput v3, v0, v1

    goto/16 :goto_1

    .line 2322485
    :cond_13
    iget v3, p0, LX/G96;->c:I

    div-int v3, v1, v3

    .line 2322486
    iget v5, p0, LX/G96;->c:I

    rem-int v5, v1, v5

    mul-int/lit8 v5, v5, 0x20

    .line 2322487
    iget-object v6, p0, LX/G96;->d:[I

    aget v6, v6, v1

    .line 2322488
    const/16 v1, 0x1f

    .line 2322489
    :goto_9
    ushr-int v7, v6, v1

    if-nez v7, :cond_14

    .line 2322490
    add-int/lit8 v1, v1, -0x1

    goto :goto_9

    .line 2322491
    :cond_14
    add-int/2addr v5, v1

    .line 2322492
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v6, 0x0

    aput v5, v1, v6

    const/4 v5, 0x1

    aput v3, v1, v5

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(LX/G8q;Ljava/util/Map;)LX/G90;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G8q;",
            "Ljava/util/Map",
            "<",
            "LX/G8t;",
            "*>;)",
            "LX/G90;"
        }
    .end annotation

    .prologue
    .line 2322374
    if-eqz p2, :cond_4

    sget-object v0, LX/G8t;->PURE_BARCODE:LX/G8t;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2322375
    invoke-virtual {p1}, LX/G8q;->a()LX/G96;

    move-result-object v0

    invoke-static {v0}, LX/G9N;->a(LX/G96;)LX/G96;

    move-result-object v0

    .line 2322376
    iget-object v1, p0, LX/G9N;->b:LX/G9b;

    invoke-virtual {v1, v0, p2}, LX/G9b;->a(LX/G96;Ljava/util/Map;)LX/G99;

    move-result-object v1

    .line 2322377
    sget-object v0, LX/G9N;->a:[LX/G92;

    move-object v2, v1

    move-object v1, v0

    .line 2322378
    :goto_0
    iget-object v0, v2, LX/G99;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 2322379
    instance-of v0, v0, LX/G9f;

    if-eqz v0, :cond_0

    .line 2322380
    iget-object v0, v2, LX/G99;->e:Ljava/lang/Object;

    move-object v0, v0

    .line 2322381
    check-cast v0, LX/G9f;

    const/4 p0, 0x2

    const/4 v5, 0x0

    .line 2322382
    iget-boolean v3, v0, LX/G9f;->a:Z

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    array-length v3, v1

    const/4 v4, 0x3

    if-ge v3, v4, :cond_5

    .line 2322383
    :cond_0
    :goto_1
    new-instance v0, LX/G90;

    .line 2322384
    iget-object v3, v2, LX/G99;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2322385
    iget-object v4, v2, LX/G99;->a:[B

    move-object v4, v4

    .line 2322386
    sget-object v5, LX/G8o;->QR_CODE:LX/G8o;

    invoke-direct {v0, v3, v4, v1, v5}, LX/G90;-><init>(Ljava/lang/String;[B[LX/G92;LX/G8o;)V

    .line 2322387
    iget-object v1, v2, LX/G99;->c:Ljava/util/List;

    move-object v1, v1

    .line 2322388
    if-eqz v1, :cond_1

    .line 2322389
    sget-object v3, LX/G91;->BYTE_SEGMENTS:LX/G91;

    invoke-virtual {v0, v3, v1}, LX/G90;->a(LX/G91;Ljava/lang/Object;)V

    .line 2322390
    :cond_1
    iget-object v1, v2, LX/G99;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2322391
    if-eqz v1, :cond_2

    .line 2322392
    sget-object v3, LX/G91;->ERROR_CORRECTION_LEVEL:LX/G91;

    invoke-virtual {v0, v3, v1}, LX/G90;->a(LX/G91;Ljava/lang/Object;)V

    .line 2322393
    :cond_2
    iget v1, v2, LX/G99;->f:I

    if-ltz v1, :cond_6

    iget v1, v2, LX/G99;->g:I

    if-ltz v1, :cond_6

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2322394
    if-eqz v1, :cond_3

    .line 2322395
    sget-object v1, LX/G91;->STRUCTURED_APPEND_SEQUENCE:LX/G91;

    .line 2322396
    iget v3, v2, LX/G99;->g:I

    move v3, v3

    .line 2322397
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2322398
    invoke-virtual {v0, v1, v3}, LX/G90;->a(LX/G91;Ljava/lang/Object;)V

    .line 2322399
    sget-object v1, LX/G91;->STRUCTURED_APPEND_PARITY:LX/G91;

    .line 2322400
    iget v3, v2, LX/G99;->f:I

    move v2, v3

    .line 2322401
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2322402
    invoke-virtual {v0, v1, v2}, LX/G90;->a(LX/G91;Ljava/lang/Object;)V

    .line 2322403
    :cond_3
    return-object v0

    .line 2322404
    :cond_4
    new-instance v0, LX/G9l;

    invoke-virtual {p1}, LX/G8q;->a()LX/G96;

    move-result-object v1

    invoke-direct {v0, v1}, LX/G9l;-><init>(LX/G96;)V

    .line 2322405
    if-nez p2, :cond_7

    const/4 v1, 0x0

    .line 2322406
    :goto_3
    iput-object v1, v0, LX/G9l;->b:LX/G93;

    .line 2322407
    new-instance v1, LX/G9p;

    iget-object v2, v0, LX/G9l;->a:LX/G96;

    iget-object v3, v0, LX/G9l;->b:LX/G93;

    invoke-direct {v1, v2, v3}, LX/G9p;-><init>(LX/G96;LX/G93;)V

    .line 2322408
    invoke-virtual {v1, p2}, LX/G9p;->a(Ljava/util/Map;)LX/G9q;

    move-result-object v1

    .line 2322409
    invoke-static {v0, v1}, LX/G9l;->a(LX/G9l;LX/G9q;)LX/G9C;

    move-result-object v1

    move-object v0, v1

    .line 2322410
    iget-object v1, p0, LX/G9N;->b:LX/G9b;

    .line 2322411
    iget-object v2, v0, LX/G9C;->a:LX/G96;

    move-object v2, v2

    .line 2322412
    invoke-virtual {v1, v2, p2}, LX/G9b;->a(LX/G96;Ljava/util/Map;)LX/G99;

    move-result-object v1

    .line 2322413
    iget-object v2, v0, LX/G9C;->b:[LX/G92;

    move-object v0, v2

    .line 2322414
    move-object v2, v1

    move-object v1, v0

    goto/16 :goto_0

    .line 2322415
    :cond_5
    aget-object v3, v1, v5

    .line 2322416
    aget-object v4, v1, p0

    aput-object v4, v1, v5

    .line 2322417
    aput-object v3, v1, p0

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    .line 2322418
    :cond_7
    sget-object v1, LX/G8t;->NEED_RESULT_POINT_CALLBACK:LX/G8t;

    .line 2322419
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G93;

    goto :goto_3
.end method
