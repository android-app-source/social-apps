.class public LX/Fjz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:I

.field public final k:Ljava/lang/String;

.field public final l:J

.field public final m:Ljava/lang/String;

.field public final n:J

.field public final o:Ljava/lang/String;

.field public final p:J


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 2278033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278034
    const-string v0, "update_check_minutes"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/Fjz;->b:J

    .line 2278035
    const-string v0, "release_package"

    invoke-static {p2, v0}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->c:Ljava/lang/String;

    .line 2278036
    const-string v0, "release_number"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/Fjz;->d:I

    .line 2278037
    const-string v0, "download_url"

    invoke-static {p2, v0}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->f:Ljava/lang/String;

    .line 2278038
    const-string v0, "client_action"

    sget-object v1, LX/Fjx;->DEFAULT:LX/Fjx;

    iget v1, v1, LX/Fjx;->value:I

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/Fjz;->e:I

    .line 2278039
    const-string v0, "release_notes"

    const-string v1, ""

    invoke-static {p2, v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->g:Ljava/lang/String;

    .line 2278040
    const-string v0, "application_name"

    const-string v1, ""

    invoke-static {p2, v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->h:Ljava/lang/String;

    .line 2278041
    const-string v0, "application_version"

    const-string v1, ""

    invoke-static {p2, v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->i:Ljava/lang/String;

    .line 2278042
    const-string v0, "allowed_networks"

    sget-object v1, LX/Fjy;->DEFAULT:LX/Fjy;

    iget v1, v1, LX/Fjy;->value:I

    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/Fjz;->j:I

    .line 2278043
    const-string v0, "megaphone"

    const-string v1, "no_megaphone"

    invoke-static {p2, v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->a:Ljava/lang/String;

    .line 2278044
    const-string v0, "file_mime_type"

    const-string v1, "application/vnd.android.package-archive"

    invoke-static {p2, v0, v1}, LX/Ef2;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->k:Ljava/lang/String;

    .line 2278045
    const-string v0, "file_size"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/Fjz;->l:J

    .line 2278046
    const-string v0, "bsdiff_download_uri"

    invoke-static {p2, v0}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->m:Ljava/lang/String;

    .line 2278047
    const-string v0, "bsdiff_size"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/Fjz;->n:J

    .line 2278048
    const-string v0, "zipdiff_download_uri"

    invoke-static {p2, v0}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Fjz;->o:Ljava/lang/String;

    .line 2278049
    const-string v0, "zipdiff_size"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/Fjz;->p:J

    .line 2278050
    return-void
.end method


# virtual methods
.method public final a(J)J
    .locals 5

    .prologue
    .line 2278032
    iget-wide v0, p0, LX/Fjz;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, LX/Fjz;->b:J

    const-wide/16 v2, 0x3c

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long p1, v0, v2

    :cond_0
    return-wide p1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2278031
    iget-object v0, p0, LX/Fjz;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2278030
    iget v0, p0, LX/Fjz;->e:I

    sget-object v1, LX/Fjx;->FORCE_UPDATE_NOTIFY_USER:LX/Fjx;

    iget v1, v1, LX/Fjx;->value:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
