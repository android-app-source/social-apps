.class public final LX/GUr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2358247
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2358248
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2358249
    :goto_0
    return v1

    .line 2358250
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2358251
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2358252
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2358253
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 2358254
    const-string v6, "count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2358255
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2358256
    :cond_1
    const-string v6, "nodes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2358257
    invoke-static {p0, p1}, LX/GVR;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2358258
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2358259
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2358260
    if-eqz v0, :cond_4

    .line 2358261
    invoke-virtual {p1, v1, v4, v1}, LX/186;->a(III)V

    .line 2358262
    :cond_4
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 2358263
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2358264
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2358265
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 2358266
    if-eqz v0, :cond_0

    .line 2358267
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358268
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2358269
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2358270
    if-eqz v0, :cond_1

    .line 2358271
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358272
    invoke-static {p0, v0, p2, p3}, LX/GVR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2358273
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2358274
    return-void
.end method
