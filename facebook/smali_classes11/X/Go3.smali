.class public final LX/Go3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:D

.field public e:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2394023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static i(LX/Go3;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2394011
    iget-wide v0, p0, LX/Go3;->d:D

    cmpl-double v0, v0, v6

    if-eqz v0, :cond_0

    .line 2394012
    iget-wide v0, p0, LX/Go3;->e:D

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    long-to-double v2, v2

    iget-wide v4, p0, LX/Go3;->d:D

    sub-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, LX/Go3;->e:D

    .line 2394013
    iput-wide v6, p0, LX/Go3;->d:D

    .line 2394014
    :cond_0
    return-void
.end method

.method public static j(LX/Go3;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2394015
    iput-boolean v0, p0, LX/Go3;->a:Z

    .line 2394016
    iput-boolean v0, p0, LX/Go3;->c:Z

    .line 2394017
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2394018
    invoke-static {p0}, LX/Go3;->j(LX/Go3;)V

    .line 2394019
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Go3;->a:Z

    .line 2394020
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Go3;->b:Z

    .line 2394021
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    long-to-double v0, v0

    iput-wide v0, p0, LX/Go3;->d:D

    .line 2394022
    return-void
.end method
