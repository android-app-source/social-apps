.class public final LX/Fdd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;)V
    .locals 0

    .prologue
    .line 2263823
    iput-object p1, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x33529119

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263817
    iget-object v1, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->n:LX/FdZ;

    if-eqz v1, :cond_0

    .line 2263818
    iget-object v1, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    invoke-virtual {v1}, LX/FdT;->a()LX/5uu;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2263819
    iget-object v1, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->n:LX/FdZ;

    iget-object v2, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    iget-object v2, v2, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    invoke-virtual {v2}, LX/FdT;->a()LX/5uu;

    move-result-object v2

    iget-object v3, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    iget-object v3, v3, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    invoke-virtual {v3}, LX/FdT;->b()LX/0Px;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/FdZ;->a(LX/5uu;LX/0Px;)V

    .line 2263820
    :cond_0
    :goto_0
    iget-object v1, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2263821
    const v1, 0xcabd2bf

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2263822
    :cond_1
    iget-object v1, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->n:LX/FdZ;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Fdd;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;

    iget-object v3, v3, Lcom/facebook/search/results/filters/ui/SearchResultPageGeneralFilterFragment;->o:LX/FdT;

    invoke-virtual {v3}, LX/FdT;->b()LX/0Px;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/FdZ;->a(LX/5uu;LX/0Px;)V

    goto :goto_0
.end method
