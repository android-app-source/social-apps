.class public LX/FID;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2221703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/photos/base/media/VideoItem;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2221704
    sget-object v0, LX/2MK;->VIDEO:LX/2MK;

    invoke-virtual {v0}, LX/2MK;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2221705
    const/4 v0, 0x0

    .line 2221706
    :goto_0
    return-object v0

    .line 2221707
    :cond_0
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    .line 2221708
    new-instance v4, LX/4gP;

    invoke-direct {v4}, LX/4gP;-><init>()V

    iget-wide v6, p0, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v4

    sget-object v5, LX/4gQ;->Video:LX/4gQ;

    invoke-virtual {v4, v5}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v4, v5}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-static {v5}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v4

    iget v5, p0, Lcom/facebook/ui/media/attachments/MediaResource;->k:I

    .line 2221709
    iput v5, v4, LX/4gP;->f:I

    .line 2221710
    move-object v4, v4

    .line 2221711
    iget v5, p0, Lcom/facebook/ui/media/attachments/MediaResource;->l:I

    .line 2221712
    iput v5, v4, LX/4gP;->g:I

    .line 2221713
    move-object v4, v4

    .line 2221714
    invoke-virtual {v4}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 2221715
    new-instance v5, LX/4gN;

    invoke-direct {v5}, LX/4gN;-><init>()V

    invoke-virtual {v5, v4}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v4

    iget-wide v6, p0, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    .line 2221716
    iput-wide v6, v4, LX/4gN;->b:J

    .line 2221717
    move-object v4, v4

    .line 2221718
    iget-wide v6, p0, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    .line 2221719
    iput-wide v6, v4, LX/4gN;->d:J

    .line 2221720
    move-object v4, v4

    .line 2221721
    invoke-virtual {v4}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v4

    move-object v1, v4

    .line 2221722
    iput-object v1, v0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2221723
    move-object v0, v0

    .line 2221724
    invoke-virtual {p0}, Lcom/facebook/ui/media/attachments/MediaResource;->c()I

    move-result v1

    int-to-long v2, v1

    .line 2221725
    iput-wide v2, v0, LX/74m;->d:J

    .line 2221726
    move-object v0, v0

    .line 2221727
    iget-wide v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    .line 2221728
    iput-wide v2, v0, LX/74m;->d:J

    .line 2221729
    move-object v0, v0

    .line 2221730
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 2221731
    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->o:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2221732
    iput-object v1, v0, LX/74m;->g:Ljava/lang/String;

    .line 2221733
    :cond_1
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    goto :goto_0
.end method
