.class public LX/FXs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/support/annotation/UiThread;
.end annotation


# instance fields
.field public final a:LX/FXN;

.field public final b:LX/FXr;

.field public final c:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/01J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/01J",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z


# direct methods
.method public constructor <init>(LX/FXN;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/16 v2, 0xc

    .line 2255540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2255541
    new-instance v0, LX/FXr;

    invoke-direct {v0, p0}, LX/FXr;-><init>(LX/FXs;)V

    iput-object v0, p0, LX/FXs;->b:LX/FXr;

    .line 2255542
    new-instance v0, LX/01J;

    invoke-direct {v0, v2}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/FXs;->c:LX/01J;

    .line 2255543
    new-instance v0, LX/01J;

    invoke-direct {v0, v2}, LX/01J;-><init>(I)V

    iput-object v0, p0, LX/FXs;->d:LX/01J;

    .line 2255544
    iput-object p1, p0, LX/FXs;->a:LX/FXN;

    .line 2255545
    return-void
.end method

.method public static b(LX/0QB;)LX/FXs;
    .locals 2

    .prologue
    .line 2255546
    new-instance v1, LX/FXs;

    invoke-static {p0}, LX/FXN;->a(LX/0QB;)LX/FXN;

    move-result-object v0

    check-cast v0, LX/FXN;

    invoke-direct {v1, v0}, LX/FXs;-><init>(LX/FXN;)V

    .line 2255547
    return-object v1
.end method

.method public static d(LX/FXs;Ljava/lang/String;LX/FXS;)V
    .locals 3

    .prologue
    .line 2255548
    iget-object v0, p0, LX/FXs;->a:LX/FXN;

    const/4 v1, 0x0

    iget-object v2, p0, LX/FXs;->b:LX/FXr;

    invoke-virtual {v2, p2}, LX/FXr;->a(LX/FXS;)LX/FXq;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, LX/FXN;->a(Ljava/lang/String;Ljava/lang/String;LX/FXq;)V

    .line 2255549
    return-void
.end method
