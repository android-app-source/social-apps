.class public LX/FM9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/FM3;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile i:LX/FM9;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMo;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6jJ;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3MG;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ud;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Lx;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2228419
    const/4 v0, 0x0

    sput-object v0, LX/FM9;->a:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2228420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2228421
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228422
    iput-object v0, p0, LX/FM9;->b:LX/0Ot;

    .line 2228423
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228424
    iput-object v0, p0, LX/FM9;->c:LX/0Ot;

    .line 2228425
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228426
    iput-object v0, p0, LX/FM9;->d:LX/0Ot;

    .line 2228427
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228428
    iput-object v0, p0, LX/FM9;->e:LX/0Ot;

    .line 2228429
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228430
    iput-object v0, p0, LX/FM9;->f:LX/0Ot;

    .line 2228431
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228432
    iput-object v0, p0, LX/FM9;->g:LX/0Ot;

    .line 2228433
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228434
    iput-object v0, p0, LX/FM9;->h:LX/0Ot;

    .line 2228435
    return-void
.end method

.method public static a(LX/0QB;)LX/FM9;
    .locals 10

    .prologue
    .line 2228436
    sget-object v0, LX/FM9;->i:LX/FM9;

    if-nez v0, :cond_1

    .line 2228437
    const-class v1, LX/FM9;

    monitor-enter v1

    .line 2228438
    :try_start_0
    sget-object v0, LX/FM9;->i:LX/FM9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2228439
    if-eqz v2, :cond_0

    .line 2228440
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2228441
    new-instance v3, LX/FM9;

    invoke-direct {v3}, LX/FM9;-><init>()V

    .line 2228442
    const/16 v4, 0x298f

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2985

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xdad

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xcfc

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x123d

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xda0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p0, 0x2982

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2228443
    iput-object v4, v3, LX/FM9;->b:LX/0Ot;

    iput-object v5, v3, LX/FM9;->c:LX/0Ot;

    iput-object v6, v3, LX/FM9;->d:LX/0Ot;

    iput-object v7, v3, LX/FM9;->e:LX/0Ot;

    iput-object v8, v3, LX/FM9;->f:LX/0Ot;

    iput-object v9, v3, LX/FM9;->g:LX/0Ot;

    iput-object p0, v3, LX/FM9;->h:LX/0Ot;

    .line 2228444
    move-object v0, v3

    .line 2228445
    sput-object v0, LX/FM9;->i:LX/FM9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2228446
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2228447
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2228448
    :cond_1
    sget-object v0, LX/FM9;->i:LX/FM9;

    return-object v0

    .line 2228449
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2228450
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/FM9;JLjava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2228451
    sget-object v0, LX/FM9;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 2228452
    if-eqz p4, :cond_1

    .line 2228453
    sget-object v0, LX/FM9;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2228454
    sget-object v0, LX/FM9;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FM3;

    .line 2228455
    iput-wide p1, v0, LX/FM3;->c:J

    .line 2228456
    :goto_0
    return-void

    .line 2228457
    :cond_0
    iget-object v0, p0, LX/FM9;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MG;

    invoke-virtual {v0, p3}, LX/3MG;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2228458
    new-instance v1, LX/FM3;

    invoke-direct {v1, v0, p3, p1, p2}, LX/FM3;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 2228459
    sget-object v0, LX/FM9;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2228460
    :cond_1
    sget-object v0, LX/FM9;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2228461
    :cond_2
    invoke-static {p0}, LX/FM9;->b(LX/FM9;)V

    goto :goto_0
.end method

.method public static b(LX/FM9;)V
    .locals 4

    .prologue
    .line 2228462
    invoke-direct {p0}, LX/FM9;->d()Ljava/util/List;

    move-result-object v0

    .line 2228463
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, LX/FM9;->a:Ljava/util/HashMap;

    .line 2228464
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FM3;

    .line 2228465
    sget-object v2, LX/FM9;->a:Ljava/util/HashMap;

    .line 2228466
    iget-object v3, v0, LX/FM3;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2228467
    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2228468
    :cond_0
    return-void
.end method

.method private d()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/FM3;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2228469
    :try_start_0
    iget-object v0, p0, LX/FM9;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jJ;

    const/4 v9, 0x0

    .line 2228470
    iget-object v7, v0, LX/6jJ;->a:LX/3MK;

    invoke-virtual {v7}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, "block_table"

    move-object v10, v9

    move-object v11, v9

    move-object v12, v9

    move-object v13, v9

    move-object v14, v9

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    move-object v2, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2228471
    if-nez v2, :cond_1

    .line 2228472
    if-eqz v2, :cond_0

    .line 2228473
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    .line 2228474
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2228475
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2228476
    sget-object v0, LX/3MN;->a:LX/0U1;

    .line 2228477
    iget-object v3, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v3

    .line 2228478
    invoke-static {v2, v0}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2228479
    iget-object v0, p0, LX/FM9;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3MG;

    invoke-virtual {v0, v3}, LX/3MG;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2228480
    sget-object v4, LX/3MN;->b:LX/0U1;

    .line 2228481
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2228482
    invoke-static {v2, v4}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2228483
    new-instance v6, LX/FM3;

    invoke-direct {v6, v0, v3, v4, v5}, LX/FM3;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2228484
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    if-eqz v1, :cond_2

    .line 2228485
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2228486
    :cond_3
    if-eqz v2, :cond_4

    .line 2228487
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v0, v1

    goto :goto_0

    .line 2228488
    :catchall_1
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2228489
    sget-object v0, LX/FM9;->a:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 2228490
    invoke-static {p0}, LX/FM9;->b(LX/FM9;)V

    .line 2228491
    :cond_0
    sget-object v0, LX/FM9;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 2228492
    iget-object v0, p0, LX/FM9;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMI;

    invoke-virtual {v0, v1}, LX/FMI;->a(Ljava/util/Set;)LX/0Rf;

    move-result-object v0

    move-object v0, v0

    .line 2228493
    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/FMK;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2228494
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2228495
    invoke-static {p1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2228496
    iget-object v0, p0, LX/FM9;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lx;

    invoke-virtual {v0, p1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2228497
    :cond_0
    iget-object v0, p0, LX/FM9;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ud;

    invoke-virtual {v0}, LX/2Ud;->a()J

    move-result-wide v2

    .line 2228498
    iget-object v0, p0, LX/FM9;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jJ;

    invoke-virtual {v0, p1, v2, v3}, LX/6jJ;->a(Ljava/lang/String;J)V

    .line 2228499
    invoke-static {p0, v2, v3, p1, v1}, LX/FM9;->a(LX/FM9;JLjava/lang/String;Z)V

    .line 2228500
    iget-object v0, p0, LX/FM9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMo;

    .line 2228501
    sget-object v2, LX/6el;->SPAM:LX/6el;

    invoke-static {v0, v2}, LX/FMo;->a(LX/FMo;LX/6el;)V

    .line 2228502
    sget-object v2, LX/6el;->BUSINESS:LX/6el;

    invoke-static {v0, v2}, LX/FMo;->a(LX/FMo;LX/6el;)V

    .line 2228503
    sget-object v2, LX/6el;->SMS:LX/6el;

    invoke-static {v0, v2}, LX/FMo;->a(LX/FMo;LX/6el;)V

    .line 2228504
    iget-object v0, p0, LX/FM9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMo;

    invoke-virtual {v0}, LX/FMo;->a()V

    .line 2228505
    iget-object v0, p0, LX/FM9;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30m;

    invoke-virtual {v0, p2, v1}, LX/30m;->a(LX/FMK;Z)V

    .line 2228506
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;LX/FMK;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2228507
    invoke-static {p1}, LX/2UG;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2228508
    iget-object v0, p0, LX/FM9;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lx;

    invoke-virtual {v0, p1}, LX/3Lx;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2228509
    :cond_0
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2228510
    iget-object v0, p0, LX/FM9;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jJ;

    invoke-virtual {v0, p1}, LX/6jJ;->a(Ljava/lang/String;)V

    .line 2228511
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1, p1, v2}, LX/FM9;->a(LX/FM9;JLjava/lang/String;Z)V

    .line 2228512
    iget-object v0, p0, LX/FM9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMo;

    invoke-virtual {v0}, LX/FMo;->a()V

    .line 2228513
    iget-object v0, p0, LX/FM9;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30m;

    invoke-virtual {v0, p2, v2}, LX/30m;->a(LX/FMK;Z)V

    .line 2228514
    :cond_1
    return-void
.end method
