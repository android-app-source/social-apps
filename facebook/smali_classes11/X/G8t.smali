.class public final enum LX/G8t;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G8t;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G8t;

.field public static final enum ALLOWED_EAN_EXTENSIONS:LX/G8t;

.field public static final enum ALLOWED_LENGTHS:LX/G8t;

.field public static final enum ASSUME_CODE_39_CHECK_DIGIT:LX/G8t;

.field public static final enum ASSUME_GS1:LX/G8t;

.field public static final enum CHARACTER_SET:LX/G8t;

.field public static final enum NEED_RESULT_POINT_CALLBACK:LX/G8t;

.field public static final enum OTHER:LX/G8t;

.field public static final enum POSSIBLE_FORMATS:LX/G8t;

.field public static final enum PURE_BARCODE:LX/G8t;

.field public static final enum RETURN_CODABAR_START_END:LX/G8t;

.field public static final enum TRY_HARDER:LX/G8t;


# instance fields
.field private final valueType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2321277
    new-instance v0, LX/G8t;

    const-string v1, "OTHER"

    const-class v2, Ljava/lang/Object;

    invoke-direct {v0, v1, v4, v2}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->OTHER:LX/G8t;

    .line 2321278
    new-instance v0, LX/G8t;

    const-string v1, "PURE_BARCODE"

    const-class v2, Ljava/lang/Void;

    invoke-direct {v0, v1, v5, v2}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->PURE_BARCODE:LX/G8t;

    .line 2321279
    new-instance v0, LX/G8t;

    const-string v1, "POSSIBLE_FORMATS"

    const-class v2, Ljava/util/List;

    invoke-direct {v0, v1, v6, v2}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->POSSIBLE_FORMATS:LX/G8t;

    .line 2321280
    new-instance v0, LX/G8t;

    const-string v1, "TRY_HARDER"

    const-class v2, Ljava/lang/Void;

    invoke-direct {v0, v1, v7, v2}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->TRY_HARDER:LX/G8t;

    .line 2321281
    new-instance v0, LX/G8t;

    const-string v1, "CHARACTER_SET"

    const-class v2, Ljava/lang/String;

    invoke-direct {v0, v1, v8, v2}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->CHARACTER_SET:LX/G8t;

    .line 2321282
    new-instance v0, LX/G8t;

    const-string v1, "ALLOWED_LENGTHS"

    const/4 v2, 0x5

    const-class v3, [I

    invoke-direct {v0, v1, v2, v3}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->ALLOWED_LENGTHS:LX/G8t;

    .line 2321283
    new-instance v0, LX/G8t;

    const-string v1, "ASSUME_CODE_39_CHECK_DIGIT"

    const/4 v2, 0x6

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->ASSUME_CODE_39_CHECK_DIGIT:LX/G8t;

    .line 2321284
    new-instance v0, LX/G8t;

    const-string v1, "ASSUME_GS1"

    const/4 v2, 0x7

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->ASSUME_GS1:LX/G8t;

    .line 2321285
    new-instance v0, LX/G8t;

    const-string v1, "RETURN_CODABAR_START_END"

    const/16 v2, 0x8

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->RETURN_CODABAR_START_END:LX/G8t;

    .line 2321286
    new-instance v0, LX/G8t;

    const-string v1, "NEED_RESULT_POINT_CALLBACK"

    const/16 v2, 0x9

    const-class v3, LX/G93;

    invoke-direct {v0, v1, v2, v3}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->NEED_RESULT_POINT_CALLBACK:LX/G8t;

    .line 2321287
    new-instance v0, LX/G8t;

    const-string v1, "ALLOWED_EAN_EXTENSIONS"

    const/16 v2, 0xa

    const-class v3, [I

    invoke-direct {v0, v1, v2, v3}, LX/G8t;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, LX/G8t;->ALLOWED_EAN_EXTENSIONS:LX/G8t;

    .line 2321288
    const/16 v0, 0xb

    new-array v0, v0, [LX/G8t;

    sget-object v1, LX/G8t;->OTHER:LX/G8t;

    aput-object v1, v0, v4

    sget-object v1, LX/G8t;->PURE_BARCODE:LX/G8t;

    aput-object v1, v0, v5

    sget-object v1, LX/G8t;->POSSIBLE_FORMATS:LX/G8t;

    aput-object v1, v0, v6

    sget-object v1, LX/G8t;->TRY_HARDER:LX/G8t;

    aput-object v1, v0, v7

    sget-object v1, LX/G8t;->CHARACTER_SET:LX/G8t;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/G8t;->ALLOWED_LENGTHS:LX/G8t;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/G8t;->ASSUME_CODE_39_CHECK_DIGIT:LX/G8t;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/G8t;->ASSUME_GS1:LX/G8t;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/G8t;->RETURN_CODABAR_START_END:LX/G8t;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/G8t;->NEED_RESULT_POINT_CALLBACK:LX/G8t;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/G8t;->ALLOWED_EAN_EXTENSIONS:LX/G8t;

    aput-object v2, v0, v1

    sput-object v0, LX/G8t;->$VALUES:[LX/G8t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2321271
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2321272
    iput-object p3, p0, LX/G8t;->valueType:Ljava/lang/Class;

    .line 2321273
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G8t;
    .locals 1

    .prologue
    .line 2321276
    const-class v0, LX/G8t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G8t;

    return-object v0
.end method

.method public static values()[LX/G8t;
    .locals 1

    .prologue
    .line 2321275
    sget-object v0, LX/G8t;->$VALUES:[LX/G8t;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G8t;

    return-object v0
.end method


# virtual methods
.method public final getValueType()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2321274
    iget-object v0, p0, LX/G8t;->valueType:Ljava/lang/Class;

    return-object v0
.end method
