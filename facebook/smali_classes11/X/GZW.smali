.class public LX/GZW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field public a:LX/GZj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2366664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2366665
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 2366666
    iget-object v0, p0, LX/GZW;->a:LX/GZj;

    invoke-virtual {v0}, LX/GZj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GZW;->b:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_1

    .line 2366667
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2366668
    const-string v0, "collectionID"

    const-string v2, "collection_id"

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2366669
    const-string v0, "refID"

    const-string v2, "product_ref_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366670
    const-string v0, "refType"

    const-string v2, "product_ref_type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366671
    const-string v0, "title"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2366672
    if-eqz v0, :cond_0

    .line 2366673
    :try_start_0
    const-string v2, "UTF-8"

    invoke-static {v0, v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2366674
    :cond_0
    const-string v2, "title"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366675
    const-string v0, "hidePageHeader"

    const-string v2, "hide_page_header"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366676
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v2, "/shops_collection"

    .line 2366677
    iput-object v2, v0, LX/98r;->a:Ljava/lang/String;

    .line 2366678
    move-object v0, v0

    .line 2366679
    const-string v2, "ShopsCollectionRoute"

    .line 2366680
    iput-object v2, v0, LX/98r;->b:Ljava/lang/String;

    .line 2366681
    move-object v0, v0

    .line 2366682
    iput-object v1, v0, LX/98r;->f:Landroid/os/Bundle;

    .line 2366683
    move-object v0, v0

    .line 2366684
    const v1, 0x7f0814cb

    .line 2366685
    iput v1, v0, LX/98r;->d:I

    .line 2366686
    move-object v0, v0

    .line 2366687
    const/4 v1, 0x1

    .line 2366688
    iput v1, v0, LX/98r;->h:I

    .line 2366689
    move-object v0, v0

    .line 2366690
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 2366691
    invoke-static {v0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->b(Landroid/os/Bundle;)Lcom/facebook/fbreact/fragment/FbReactFragment;

    move-result-object v0

    .line 2366692
    :goto_0
    return-object v0

    .line 2366693
    :catch_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "UTF-8 not supported"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 2366694
    :cond_1
    const-string v0, "collection_id"

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2366695
    const-string v1, "merchant_page_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2366696
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v0, 0x0

    .line 2366697
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2366698
    const-string v5, "collection_id"

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2366699
    const-string v5, "is_adunit"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2366700
    const-string v5, "merchant_page_id"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2366701
    new-instance v5, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;

    invoke-direct {v5}, Lcom/facebook/commerce/storefront/fragments/CollectionViewFragment;-><init>()V

    .line 2366702
    invoke-virtual {v5, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2366703
    move-object v0, v5

    .line 2366704
    goto :goto_0
.end method
