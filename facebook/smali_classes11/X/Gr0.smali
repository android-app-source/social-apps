.class public final LX/Gr0;
.super LX/Cqf;
.source ""


# instance fields
.field public final synthetic g:LX/Gr1;


# direct methods
.method public constructor <init>(LX/Gr1;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V
    .locals 7

    .prologue
    .line 2397126
    iput-object p1, p0, LX/Gr0;->g:LX/Gr1;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    return-void
.end method


# virtual methods
.method public final g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2397127
    invoke-virtual {p0}, LX/Cqf;->k()F

    move-result v0

    .line 2397128
    invoke-virtual {p0}, LX/Cqf;->l()F

    move-result v1

    .line 2397129
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v2

    if-eqz v2, :cond_0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    .line 2397130
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    .line 2397131
    int-to-float v2, v0

    div-float v1, v2, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 2397132
    new-instance v2, Landroid/graphics/Rect;

    add-int/lit8 v0, v0, 0x0

    add-int/lit8 v1, v1, 0x0

    invoke-direct {v2, v3, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2397133
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/CrW;

    invoke-direct {v1, v2}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 2397134
    :goto_0
    return-void

    .line 2397135
    :cond_0
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/CrW;

    .line 2397136
    iget-object v2, p0, LX/Cqf;->o:Landroid/graphics/Rect;

    move-object v2, v2

    .line 2397137
    invoke-direct {v1, v2}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v1}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    goto :goto_0
.end method
