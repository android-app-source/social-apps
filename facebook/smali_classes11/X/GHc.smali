.class public LX/GHc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2335095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2335096
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GHc;->c:Z

    .line 2335097
    iput-object p1, p0, LX/GHc;->a:Landroid/content/Context;

    .line 2335098
    iput-object p2, p0, LX/GHc;->b:LX/0Or;

    .line 2335099
    return-void
.end method

.method public static a(LX/0QB;)LX/GHc;
    .locals 5

    .prologue
    .line 2335100
    const-class v1, LX/GHc;

    monitor-enter v1

    .line 2335101
    :try_start_0
    sget-object v0, LX/GHc;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2335102
    sput-object v2, LX/GHc;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2335103
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2335104
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2335105
    new-instance v4, LX/GHc;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 p0, 0x122d

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/GHc;-><init>(Landroid/content/Context;LX/0Or;)V

    .line 2335106
    move-object v0, v4

    .line 2335107
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2335108
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GHc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2335109
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2335110
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/8wL;)Z
    .locals 1

    .prologue
    .line 2335094
    sget-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
