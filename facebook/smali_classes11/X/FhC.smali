.class public LX/FhC;
.super LX/1Cd;
.source ""


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2272419
    invoke-direct {p0}, LX/1Cd;-><init>()V

    .line 2272420
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, LX/FhC;->a:Ljava/util/Set;

    .line 2272421
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2272422
    const/4 v1, 0x0

    .line 2272423
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/1Rk;

    if-nez v0, :cond_3

    :cond_0
    move-object v0, v1

    .line 2272424
    :goto_0
    move-object v0, v0

    .line 2272425
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;

    if-eqz v1, :cond_2

    .line 2272426
    :cond_1
    :goto_1
    return-void

    .line 2272427
    :cond_2
    iget-object v1, p0, LX/FhC;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2272428
    :cond_3
    check-cast p1, LX/1Rk;

    .line 2272429
    iget-object v0, p1, LX/1Rk;->a:LX/1RA;

    move-object v0, v0

    .line 2272430
    iget v2, p1, LX/1Rk;->b:I

    .line 2272431
    invoke-static {v0}, LX/1RA;->e(LX/1RA;)V

    .line 2272432
    iget-object p1, v0, LX/1RA;->k:LX/0Px;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/1RZ;

    .line 2272433
    iget-object v0, p1, LX/1RZ;->d:Ljava/lang/Object;

    move-object p1, v0

    .line 2272434
    move-object v0, p1

    .line 2272435
    if-eqz v0, :cond_4

    instance-of v2, v0, Lcom/facebook/search/model/TypeaheadUnit;

    if-nez v2, :cond_5

    :cond_4
    move-object v0, v1

    .line 2272436
    goto :goto_0

    .line 2272437
    :cond_5
    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    goto :goto_0
.end method
