.class public LX/GQh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/HcO;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HcO",
            "<",
            "LX/GQZ;",
            "LX/GQf;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2Dg;


# direct methods
.method public constructor <init>(LX/2Dg;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2350274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2350275
    iput-object p1, p0, LX/GQh;->b:LX/2Dg;

    .line 2350276
    new-instance v0, LX/HcO;

    .line 2350277
    iget-object v1, p0, LX/GQh;->b:LX/2Dg;

    invoke-virtual {v1}, LX/2Dg;->d()Lcom/facebook/aldrin/status/AldrinUserStatus;

    move-result-object v1

    .line 2350278
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2350279
    sget-object v2, LX/GQg;->a:[I

    iget-object v3, v1, Lcom/facebook/aldrin/status/AldrinUserStatus;->tosTransitionType:Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLTosTransitionTypeEnum;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2350280
    :cond_0
    sget-object v1, LX/GQZ;->VIEW_ALDRIN_TOS:LX/GQZ;

    :goto_0
    move-object v1, v1

    .line 2350281
    invoke-direct {v0, v1}, LX/HcO;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/GQh;->a:LX/HcO;

    .line 2350282
    iget-object v0, p0, LX/GQh;->a:LX/HcO;

    sget-object v1, LX/GQf;->AGREED_TO_ALDRIN_TOS:LX/GQf;

    sget-object v2, LX/GQZ;->VIEW_ALDRIN_TOS:LX/GQZ;

    sget-object v3, LX/GQZ;->AGREED_TO_ALDRIN_TOS:LX/GQZ;

    invoke-virtual {v0, v1, v2, v3}, LX/HcO;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2350283
    iget-object v0, p0, LX/GQh;->a:LX/HcO;

    sget-object v1, LX/GQf;->DENIED_ALDRIN_TOS:LX/GQf;

    sget-object v2, LX/GQZ;->VIEW_ALDRIN_TOS:LX/GQZ;

    sget-object v3, LX/GQZ;->VIEW_BLOCK_INTERSTITIAL:LX/GQZ;

    invoke-virtual {v0, v1, v2, v3}, LX/HcO;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2350284
    iget-object v0, p0, LX/GQh;->a:LX/HcO;

    sget-object v1, LX/GQf;->GO_BACK:LX/GQf;

    sget-object v2, LX/GQZ;->VIEW_BLOCK_INTERSTITIAL:LX/GQZ;

    sget-object v3, LX/GQZ;->VIEW_ALDRIN_TOS:LX/GQZ;

    invoke-virtual {v0, v1, v2, v3}, LX/HcO;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2350285
    iget-object v0, p0, LX/GQh;->a:LX/HcO;

    sget-object v1, LX/GQf;->AGREED_TO_GENERAL_TOS:LX/GQf;

    sget-object v2, LX/GQZ;->VIEW_GENERAL_TOS:LX/GQZ;

    sget-object v3, LX/GQZ;->AGREED_TO_GENERAL_TOS:LX/GQZ;

    invoke-virtual {v0, v1, v2, v3}, LX/HcO;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2350286
    return-void

    .line 2350287
    :pswitch_0
    iget-object v2, v1, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-ne v2, v3, :cond_1

    iget-object v2, v1, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-ne v2, v3, :cond_1

    .line 2350288
    sget-object v1, LX/GQZ;->VIEW_ALDRIN_TOS:LX/GQZ;

    goto :goto_0

    .line 2350289
    :cond_1
    iget-object v2, v1, Lcom/facebook/aldrin/status/AldrinUserStatus;->effectiveRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->ALDRIN:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-ne v2, v3, :cond_0

    iget-object v1, v1, Lcom/facebook/aldrin/status/AldrinUserStatus;->currentRegion:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;->GENERAL:Lcom/facebook/graphql/enums/GraphQLTosRegionCodeEnum;

    if-ne v1, v2, :cond_0

    .line 2350290
    sget-object v1, LX/GQZ;->VIEW_GENERAL_TOS:LX/GQZ;

    goto :goto_0

    .line 2350291
    :pswitch_1
    sget-object v1, LX/GQZ;->VIEW_BLOCK_INTERSTITIAL:LX/GQZ;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/GQh;
    .locals 4

    .prologue
    .line 2350292
    const-class v1, LX/GQh;

    monitor-enter v1

    .line 2350293
    :try_start_0
    sget-object v0, LX/GQh;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2350294
    sput-object v2, LX/GQh;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2350295
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2350296
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2350297
    new-instance p0, LX/GQh;

    invoke-static {v0}, LX/2Dg;->a(LX/0QB;)LX/2Dg;

    move-result-object v3

    check-cast v3, LX/2Dg;

    invoke-direct {p0, v3}, LX/GQh;-><init>(LX/2Dg;)V

    .line 2350298
    move-object v0, p0

    .line 2350299
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2350300
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GQh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2350301
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2350302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/GQf;)V
    .locals 3

    .prologue
    .line 2350303
    iget-object v0, p0, LX/GQh;->a:LX/HcO;

    .line 2350304
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2350305
    iget-object v1, v0, LX/HcO;->a:Ljava/util/Map;

    iget-object v2, v0, LX/HcO;->c:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 2350306
    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2350307
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HcN;

    .line 2350308
    iget-object v2, v1, LX/HcN;->a:Ljava/lang/Object;

    move-object v1, v2

    .line 2350309
    iget-object v2, v0, LX/HcO;->c:Ljava/lang/Object;

    iput-object v2, v0, LX/HcO;->d:Ljava/lang/Object;

    .line 2350310
    iput-object v1, v0, LX/HcO;->c:Ljava/lang/Object;

    .line 2350311
    iget-object v1, v0, LX/HcO;->e:LX/GQX;

    if-eqz v1, :cond_0

    .line 2350312
    iget-object v1, v0, LX/HcO;->e:LX/GQX;

    iget-object v2, v0, LX/HcO;->d:Ljava/lang/Object;

    iget-object p0, v0, LX/HcO;->c:Ljava/lang/Object;

    invoke-virtual {v1, p1, v2, p0}, LX/GQX;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2350313
    :cond_0
    return-void
.end method
