.class public LX/FvV;
.super LX/Fv4;
.source ""


# instance fields
.field public final e:LX/Fur;

.field public final f:LX/Fuy;

.field private final g:Z

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fqi;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BP0;LX/BQ1;ZLX/0ad;LX/Fur;LX/Fuy;LX/0Ot;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BP0;",
            "LX/BQ1;",
            "Z",
            "LX/0ad;",
            "LX/Fur;",
            "LX/Fuy;",
            "LX/0Ot",
            "<",
            "LX/Fqi;",
            ">;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2301628
    invoke-direct {p0, p1, p5, p2, p3}, LX/Fv4;-><init>(Landroid/content/Context;LX/0ad;LX/BP0;LX/BQ1;)V

    .line 2301629
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FvV;->j:Z

    .line 2301630
    iput-object p6, p0, LX/FvV;->e:LX/Fur;

    .line 2301631
    iput-object p7, p0, LX/FvV;->f:LX/Fuy;

    .line 2301632
    iput-boolean p4, p0, LX/FvV;->g:Z

    .line 2301633
    iput-object p8, p0, LX/FvV;->h:LX/0Ot;

    .line 2301634
    invoke-direct {p0, p9}, LX/FvV;->a(Landroid/os/Handler;)V

    .line 2301635
    return-void
.end method

.method private a(Landroid/os/Handler;)V
    .locals 6

    .prologue
    .line 2301626
    new-instance v0, Lcom/facebook/timeline/header/TimelineHeaderTopAdapter$1;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/header/TimelineHeaderTopAdapter$1;-><init>(LX/FvV;)V

    iget-object v1, p0, LX/Fv4;->b:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget v4, LX/0wf;->ad:I

    const/16 v5, 0x1388

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v1

    int-to-long v2, v1

    const v1, -0x7d8158aa

    invoke-static {p1, v0, v2, v3, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2301627
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2301625
    invoke-static {}, LX/FvU;->cachedValues()[LX/FvU;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2301548
    invoke-static {}, LX/FvU;->cachedValues()[LX/FvU;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2301549
    sget-object v1, LX/FvT;->a:[I

    invoke-virtual {v0}, LX/FvU;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2301550
    invoke-static {p1}, LX/Fv4;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2301551
    :pswitch_0
    iget-object v0, p0, LX/Fv4;->b:LX/0ad;

    sget-short v1, LX/0wf;->W:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2301552
    new-instance v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, LX/Fv4;->a:Landroid/content/Context;

    const v3, 0x7f0e05b0

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/FvV;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    .line 2301553
    :goto_1
    iget-object v0, p0, LX/FvV;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-boolean v1, p0, LX/FvV;->g:Z

    .line 2301554
    iput-boolean v1, v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->I:Z

    .line 2301555
    iget-object v0, p0, LX/FvV;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    goto :goto_0

    .line 2301556
    :cond_0
    new-instance v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v1, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/FvV;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    goto :goto_1

    .line 2301557
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0314cb

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2301558
    :pswitch_2
    new-instance v0, LX/Fv3;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Fv3;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2301559
    :pswitch_3
    iget-object v0, p0, LX/Fv4;->b:LX/0ad;

    sget-short v1, LX/0wf;->av:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f031563

    .line 2301560
    :goto_2
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2301561
    :cond_1
    const v0, 0x7f031562

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2301624
    invoke-static {}, LX/FvU;->cachedValues()[LX/FvU;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a([Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2301609
    sget-object v0, LX/FvU;->COVER_PHOTO:LX/FvU;

    invoke-virtual {v0}, LX/FvU;->ordinal()I

    move-result v0

    aput-boolean v1, p1, v0

    .line 2301610
    sget-object v0, LX/FvU;->HEADER_LOADING_VIEW:LX/FvU;

    invoke-virtual {v0}, LX/FvU;->ordinal()I

    move-result v3

    iget-boolean v0, p0, LX/FvV;->g:Z

    if-nez v0, :cond_3

    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301611
    iget v4, v0, LX/BPy;->g:I

    move v0, v4

    .line 2301612
    const/4 v4, 0x2

    if-ne v0, v4, :cond_3

    :cond_0
    iget-boolean v0, p0, LX/FvV;->j:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    aput-boolean v0, p1, v3

    .line 2301613
    sget-object v0, LX/FvU;->FRIENDING_BUTTON:LX/FvU;

    invoke-virtual {v0}, LX/FvU;->ordinal()I

    move-result v0

    iget-object v3, p0, LX/Fv4;->b:LX/0ad;

    sget-short v4, LX/0wf;->L:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v3, v4, :cond_1

    iget-object v3, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v4, :cond_4

    :cond_1
    :goto_1
    aput-boolean v1, p1, v0

    .line 2301614
    sget-object v0, LX/FvU;->ACTION_BAR:LX/FvU;

    invoke-virtual {v0}, LX/FvU;->ordinal()I

    move-result v0

    const/4 v1, 0x1

    .line 2301615
    iget-object v2, p0, LX/Fv4;->c:LX/BP0;

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301616
    iget-object v3, v2, LX/BQ1;->d:LX/5wQ;

    move-object v2, v3

    .line 2301617
    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301618
    iget v3, v2, LX/BPy;->d:I

    move v2, v3

    .line 2301619
    if-eq v2, v1, :cond_5

    :cond_2
    :goto_2
    move v1, v1

    .line 2301620
    aput-boolean v1, p1, v0

    .line 2301621
    return-void

    :cond_3
    move v0, v2

    .line 2301622
    goto :goto_0

    :cond_4
    move v1, v2

    .line 2301623
    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 12

    .prologue
    const/4 v1, 0x1

    .line 2301564
    invoke-static {}, LX/FvU;->cachedValues()[LX/FvU;

    move-result-object v0

    aget-object v0, v0, p2

    .line 2301565
    sget-object v2, LX/FvU;->COVER_PHOTO:LX/FvU;

    if-ne v0, v2, :cond_0

    instance-of v2, p1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    if-eqz v2, :cond_0

    .line 2301566
    check-cast p1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, p0, LX/Fv4;->c:LX/BP0;

    iget-object v1, p0, LX/Fv4;->d:LX/BQ1;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->a(LX/BP0;LX/BQ1;)Z

    move-result v0

    .line 2301567
    :goto_0
    return v0

    .line 2301568
    :cond_0
    sget-object v2, LX/FvU;->HEADER_LOADING_VIEW:LX/FvU;

    if-ne v0, v2, :cond_2

    instance-of v2, p1, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;

    if-eqz v2, :cond_2

    .line 2301569
    check-cast p1, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;

    .line 2301570
    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301571
    iget v2, v0, LX/BPy;->g:I

    move v0, v2

    .line 2301572
    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    .line 2301573
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->a()V

    :goto_1
    move v0, v1

    .line 2301574
    goto :goto_0

    .line 2301575
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/header/TimelineHeaderLoadingIndicatorView;->b()V

    goto :goto_1

    .line 2301576
    :cond_2
    sget-object v2, LX/FvU;->FRIENDING_BUTTON:LX/FvU;

    if-ne v0, v2, :cond_4

    instance-of v2, p1, LX/Fv3;

    if-eqz v2, :cond_4

    .line 2301577
    check-cast p1, LX/Fv3;

    .line 2301578
    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    iget-object v2, p0, LX/Fv4;->c:LX/BP0;

    .line 2301579
    iget-wide v4, v2, LX/5SB;->b:J

    move-wide v2, v4

    .line 2301580
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2301581
    invoke-virtual {v0}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v4, v5, :cond_8

    .line 2301582
    const v5, 0x7f080f7b

    const v6, 0x7f0a00d5

    const v7, 0x7f020b08

    const v8, 0x7f020b77

    move-object v4, p1

    invoke-static/range {v4 .. v9}, LX/Fv3;->a(LX/Fv3;IIIIZ)V

    .line 2301583
    new-instance v4, LX/Fv1;

    invoke-direct {v4, p1, v2, v3, v0}, LX/Fv1;-><init>(LX/Fv3;JLX/BQ1;)V

    invoke-virtual {p1, v4}, LX/Fv3;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2301584
    :cond_3
    :goto_2
    move v0, v1

    .line 2301585
    goto :goto_0

    .line 2301586
    :cond_4
    sget-object v2, LX/FvU;->ACTION_BAR:LX/FvU;

    if-ne v0, v2, :cond_7

    instance-of v0, p1, Lcom/facebook/timeline/actionbar/TimelineActionBar;

    if-nez v0, :cond_5

    instance-of v0, p1, Lcom/facebook/fig/actionbar/FigActionBar;

    if-eqz v0, :cond_7

    .line 2301587
    :cond_5
    iget-object v0, p0, LX/FvV;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fqi;

    iget-object v2, p0, LX/Fv4;->d:LX/BQ1;

    iget-object v3, p0, LX/Fv4;->c:LX/BP0;

    invoke-virtual {v0, p1, v2, v3}, LX/Fqi;->a(Landroid/view/View;LX/BQ1;LX/5SB;)Z

    .line 2301588
    check-cast p1, Landroid/view/ViewGroup;

    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301589
    invoke-virtual {v0}, LX/BQ1;->W()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2301590
    iget-object v2, p0, LX/FvV;->e:LX/Fur;

    .line 2301591
    :try_start_0
    iget-object v3, v2, LX/Fur;->a:LX/0iA;

    const-string v4, "3226"

    const-class p0, LX/3kS;

    invoke-virtual {v3, v4, p0}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/3kS;

    .line 2301592
    new-instance v4, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;

    invoke-direct {v4, v2, p1, v3, v0}, Lcom/facebook/timeline/header/ManageInterstitialHelper$1;-><init>(LX/Fur;Landroid/view/ViewGroup;LX/3kS;LX/BQ1;)V

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2301593
    :cond_6
    :goto_3
    move v0, v1

    .line 2301594
    goto/16 :goto_0

    .line 2301595
    :cond_7
    invoke-static {p2}, LX/Fv4;->c(I)Z

    move-result v0

    goto/16 :goto_0

    .line 2301596
    :cond_8
    invoke-virtual {v0}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v4, v5, :cond_3

    .line 2301597
    iget-object v4, p1, LX/Fv3;->d:LX/0ad;

    sget-short v5, LX/0wf;->K:S

    invoke-interface {v4, v5, v10}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2301598
    const v5, 0x7f080f85

    const v6, 0x7f0a00a7

    const v7, 0x7f0a00d5

    const v8, 0x7f0207da

    move-object v4, p1

    move v9, v10

    invoke-static/range {v4 .. v9}, LX/Fv3;->a(LX/Fv3;IIIIZ)V

    goto :goto_2

    .line 2301599
    :cond_9
    const v5, 0x7f080f7d

    const v6, 0x7f0a00a3

    const v7, 0x7f020b0b

    const v8, 0x7f02089f

    move-object v4, p1

    invoke-static/range {v4 .. v9}, LX/Fv3;->a(LX/Fv3;IIIIZ)V

    .line 2301600
    new-instance v4, LX/Fv2;

    invoke-direct {v4, p1, v2, v3}, LX/Fv2;-><init>(LX/Fv3;J)V

    invoke-virtual {p1, v4}, LX/Fv3;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 2301601
    :cond_a
    invoke-virtual {v0}, LX/BPy;->j()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, LX/BQ1;->s()Z

    move-result v2

    if-nez v2, :cond_6

    .line 2301602
    iget-object v2, p0, LX/FvV;->f:LX/Fuy;

    .line 2301603
    iget-boolean v3, v0, LX/BQ1;->k:Z

    move v3, v3

    .line 2301604
    if-eqz p1, :cond_b

    iget-boolean v4, v2, LX/Fuy;->c:Z

    if-eqz v4, :cond_c

    .line 2301605
    :cond_b
    :goto_4
    goto :goto_3

    :catch_0
    goto :goto_3

    .line 2301606
    :cond_c
    iget-object v4, v2, LX/Fuy;->a:LX/0iA;

    new-instance p0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v0, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {p0, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v0, LX/8DE;

    invoke-virtual {v4, p0, v0}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v4

    check-cast v4, LX/8DE;

    .line 2301607
    if-eqz v4, :cond_b

    if-eqz v3, :cond_b

    .line 2301608
    new-instance p0, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;

    invoke-direct {p0, v2, p1, v4}, Lcom/facebook/timeline/header/SeeFirstInterstitialHelper$1;-><init>(LX/Fuy;Landroid/view/ViewGroup;LX/8DE;)V

    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    goto :goto_4
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2301563
    invoke-virtual {p0, p1}, LX/Fv4;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FvU;

    invoke-virtual {v0}, LX/FvU;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2301562
    invoke-static {}, LX/FvU;->cachedValues()[LX/FvU;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
