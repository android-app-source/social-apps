.class public final LX/GV6;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V
    .locals 0

    .prologue
    .line 2358589
    iput-object p1, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2358593
    iget-object v0, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->z:LX/GTt;

    invoke-virtual {v0}, LX/GTt;->a()V

    .line 2358594
    if-eqz p1, :cond_0

    .line 2358595
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2358596
    if-eqz v0, :cond_0

    .line 2358597
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2358598
    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel$LocationSharingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2358599
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2358600
    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel$LocationSharingModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2358601
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2358602
    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel$LocationSharingModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2358603
    :cond_0
    iget-object v0, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->w:LX/03V;

    const-string v1, "background_location_resurrection_upsell_data_abnormal"

    const-string v2, "upsell data is empty"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2358604
    iget-object v0, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->o(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    .line 2358605
    :goto_0
    return-void

    .line 2358606
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2358607
    check-cast v0, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel$LocationSharingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellQueryModel$LocationSharingModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    .line 2358608
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    .line 2358609
    iget-object v0, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->o(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    goto :goto_0

    .line 2358610
    :cond_2
    iget-object v1, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->E:Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->b(LX/0Px;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/backgroundlocation/upsell/PoppingProfileImagesView;->setProfileImages(LX/0Px;)V

    .line 2358611
    iget-object v1, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->F:Landroid/widget/TextView;

    iget-object v2, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v2, v2, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->p:LX/GVF;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->b()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/GVF;->a(ILX/0Px;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2358612
    iget-object v0, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->o(Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2358591
    iget-object v0, p0, LX/GV6;->a:Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/upsell/BackgroundLocationResurrectionActivity;->w:LX/03V;

    const-string v1, "background_location_resurrection_data_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2358592
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2358590
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/GV6;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
