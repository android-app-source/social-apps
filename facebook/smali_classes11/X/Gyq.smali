.class public LX/Gyq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2410626
    iput-object p1, p0, LX/Gyq;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2410627
    return-void
.end method

.method public static a(LX/0QB;)LX/Gyq;
    .locals 2

    .prologue
    .line 2410616
    new-instance v1, LX/Gyq;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {v1, v0}, LX/Gyq;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 2410617
    move-object v0, v1

    .line 2410618
    return-object v0
.end method


# virtual methods
.method public final a(LX/Gyp;)V
    .locals 3

    .prologue
    .line 2410619
    iget-object v0, p0, LX/Gyq;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/Gyp;->markerId:I

    iget-object v2, p1, LX/Gyp;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2410620
    return-void
.end method

.method public final b(LX/Gyp;)V
    .locals 3

    .prologue
    .line 2410621
    iget-object v0, p0, LX/Gyq;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/Gyp;->markerId:I

    iget-object v2, p1, LX/Gyp;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2410622
    return-void
.end method

.method public final c(LX/Gyp;)V
    .locals 3

    .prologue
    .line 2410623
    iget-object v0, p0, LX/Gyq;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/Gyp;->markerId:I

    iget-object v2, p1, LX/Gyp;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2410624
    return-void
.end method
