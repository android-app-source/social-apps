.class public final LX/FVc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fu;


# instance fields
.field public final synthetic a:LX/FVn;


# direct methods
.method public constructor <init>(LX/FVn;)V
    .locals 0

    .prologue
    .line 2250956
    iput-object p1, p0, LX/FVc;->a:LX/FVn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final ko_()Z
    .locals 7

    .prologue
    .line 2250957
    iget-object v0, p0, LX/FVc;->a:LX/FVn;

    iget-object v0, v0, LX/FVn;->j:LX/FVS;

    iget-object v1, p0, LX/FVc;->a:LX/FVn;

    iget-object v1, v1, LX/FVn;->r:LX/FWC;

    const/4 v2, 0x0

    const/4 p0, 0x2

    const/4 v3, 0x1

    .line 2250958
    iget-object v4, v1, LX/FWC;->e:LX/FWA;

    move-object v4, v4

    .line 2250959
    invoke-virtual {v1}, LX/FWC;->getCount()I

    move-result v5

    .line 2250960
    iget-object v6, v0, LX/FVS;->g:LX/FWh;

    invoke-virtual {v6, v5, v4}, LX/FWh;->a(ILX/FWA;)V

    .line 2250961
    iget v6, v0, LX/FVS;->d:I

    if-ne v6, v3, :cond_0

    iget v6, v0, LX/FVS;->c:I

    if-ne v6, p0, :cond_0

    .line 2250962
    iget-object v3, v0, LX/FVS;->g:LX/FWh;

    invoke-virtual {v3, v5}, LX/FWh;->a(I)V

    .line 2250963
    :goto_0
    move v0, v2

    .line 2250964
    return v0

    .line 2250965
    :cond_0
    iget v6, v0, LX/FVS;->d:I

    if-ne v6, p0, :cond_1

    iget v6, v0, LX/FVS;->c:I

    if-ne v6, v3, :cond_1

    .line 2250966
    iget-object v3, v0, LX/FVS;->g:LX/FWh;

    invoke-virtual {v3, v5}, LX/FWh;->b(I)V

    goto :goto_0

    .line 2250967
    :cond_1
    iget v6, v0, LX/FVS;->d:I

    if-ne v6, p0, :cond_4

    iget v6, v0, LX/FVS;->c:I

    if-ne v6, p0, :cond_4

    .line 2250968
    sget-object v2, LX/FWA;->FROM_SERVER:LX/FWA;

    if-ne v4, v2, :cond_2

    .line 2250969
    iget-object v2, v0, LX/FVS;->g:LX/FWh;

    invoke-virtual {v2, v5}, LX/FWh;->b(I)V

    .line 2250970
    :goto_1
    iget-object v2, v0, LX/FVS;->g:LX/FWh;

    invoke-virtual {v2, v3}, LX/FWh;->a(Z)V

    move v2, v3

    .line 2250971
    goto :goto_0

    .line 2250972
    :cond_2
    sget-object v2, LX/FWA;->FROM_CACHE:LX/FWA;

    if-ne v4, v2, :cond_3

    .line 2250973
    iget-object v2, v0, LX/FVS;->g:LX/FWh;

    invoke-virtual {v2, v5}, LX/FWh;->a(I)V

    goto :goto_1

    .line 2250974
    :cond_3
    iget-object v2, v0, LX/FVS;->h:LX/03V;

    sget-object v4, LX/FVS;->a:Ljava/lang/String;

    const-string v5, "Invalid ItemsFreshness from adapter"

    invoke-virtual {v2, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2250975
    :cond_4
    iget-object v3, v0, LX/FVS;->h:LX/03V;

    sget-object v4, LX/FVS;->a:Ljava/lang/String;

    const-string v5, "Invalid state with onListDrawn"

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
