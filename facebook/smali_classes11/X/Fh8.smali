.class public final LX/Fh8;
.super LX/Cwg;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/suggestions/SuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2271573
    iput-object p1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-direct {p0}, LX/Cwg;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/model/EntityTypeaheadUnit;)V
    .locals 9

    .prologue
    .line 2271513
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2271514
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v2, :cond_0

    .line 2271515
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    iget-object v3, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v3, v3, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v3}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v3

    .line 2271516
    iget-object v4, v2, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v4, v4, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    invoke-virtual {v4, v3, p1}, LX/FZe;->a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/model/EntityTypeaheadUnit;)V

    .line 2271517
    :cond_0
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const-string v3, "entity"

    invoke-static {v2, v3, p1, v0, v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V

    .line 2271518
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271519
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->F:LX/EQ9;

    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EQ9;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SZ;

    move-result-object v0

    invoke-interface {v0, p1}, LX/2SZ;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)V

    .line 2271520
    :try_start_0
    iget-boolean v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v0, v0

    .line 2271521
    if-nez v0, :cond_1

    .line 2271522
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bf;

    .line 2271523
    new-instance v4, LX/7Bz;

    invoke-direct {v4}, LX/7Bz;-><init>()V

    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    .line 2271524
    iput-object v5, v4, LX/7Bz;->a:Ljava/lang/String;

    .line 2271525
    move-object v4, v4

    .line 2271526
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    .line 2271527
    iput-object v5, v4, LX/7Bz;->b:Ljava/lang/String;

    .line 2271528
    move-object v4, v4

    .line 2271529
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->c:Ljava/lang/String;

    .line 2271530
    iput-object v5, v4, LX/7Bz;->c:Ljava/lang/String;

    .line 2271531
    move-object v4, v4

    .line 2271532
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->g:Ljava/lang/String;

    .line 2271533
    iput-object v5, v4, LX/7Bz;->g:Ljava/lang/String;

    .line 2271534
    move-object v4, v4

    .line 2271535
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    .line 2271536
    iput-object v5, v4, LX/7Bz;->f:Ljava/lang/String;

    .line 2271537
    move-object v4, v4

    .line 2271538
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    .line 2271539
    iput-object v5, v4, LX/7Bz;->e:Landroid/net/Uri;

    .line 2271540
    move-object v4, v4

    .line 2271541
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2271542
    iput-object v5, v4, LX/7Bz;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2271543
    move-object v4, v4

    .line 2271544
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2271545
    iput-object v5, v4, LX/7Bz;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2271546
    move-object v4, v4

    .line 2271547
    iget-boolean v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->k:Z

    .line 2271548
    iput-boolean v5, v4, LX/7Bz;->k:Z

    .line 2271549
    move-object v4, v4

    .line 2271550
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2271551
    iput-object v5, v4, LX/7Bz;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2271552
    move-object v4, v4

    .line 2271553
    iget-boolean v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->h:Z

    .line 2271554
    iput-boolean v5, v4, LX/7Bz;->h:Z

    .line 2271555
    move-object v4, v4

    .line 2271556
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2271557
    iput-object v5, v4, LX/7Bz;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2271558
    move-object v4, v4

    .line 2271559
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->o:LX/0Px;

    invoke-virtual {v4, v5}, LX/7Bz;->a(LX/0Px;)LX/7Bz;

    move-result-object v4

    iget-wide v6, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->n:D

    .line 2271560
    iput-wide v6, v4, LX/7Bz;->n:D

    .line 2271561
    move-object v4, v4

    .line 2271562
    iget-object v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->u:Ljava/lang/String;

    .line 2271563
    iput-object v5, v4, LX/7Bz;->o:Ljava/lang/String;

    .line 2271564
    move-object v4, v4

    .line 2271565
    iget-boolean v5, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->w:Z

    .line 2271566
    iput-boolean v5, v4, LX/7Bz;->p:Z

    .line 2271567
    move-object v4, v4

    .line 2271568
    invoke-virtual {v4}, LX/7Bz;->q()LX/7C0;

    move-result-object v4

    move-object v1, v4

    .line 2271569
    iget-object v2, v0, LX/8bf;->f:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2271570
    :cond_1
    :goto_0
    return-void

    .line 2271571
    :catch_0
    move-exception v0

    .line 2271572
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->w:LX/2Sc;

    invoke-virtual {v1, v0}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2271360
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2271361
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v2, :cond_0

    .line 2271362
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    invoke-virtual {v2, p1}, LX/FZb;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V

    .line 2271363
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v2

    sget-object v3, LX/CwF;->escape_pps_style:LX/CwF;

    if-ne v2, v3, :cond_1

    .line 2271364
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v2}, Lcom/facebook/search/suggestions/SuggestionsFragment;->w(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0Px;

    move-result-object v2

    .line 2271365
    iget-object v3, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v3, v3

    .line 2271366
    invoke-virtual {v3}, LX/CwI;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v4}, Lcom/facebook/search/suggestions/SuggestionsFragment;->J(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v4

    iget-object v5, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v5, v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v5}, LX/Fgb;->e()LX/7HZ;

    move-result-object v5

    iget-object v6, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v6}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/Cvp;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZLX/7HZ;LX/CwU;)V

    .line 2271367
    :goto_0
    return-void

    .line 2271368
    :cond_1
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const-string v3, "keyword"

    invoke-static {v2, v3, p1, v0, v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V

    .line 2271369
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271370
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2271371
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v1

    .line 2271372
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2271373
    sget-object v0, LX/103;->USER:LX/103;

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->k()LX/103;

    move-result-object v2

    if-ne v0, v2, :cond_4

    .line 2271374
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const v2, 0x7f082341

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2271375
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/facebook/ui/search/SearchEditText;->a(Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 2271376
    :goto_2
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->M:LX/0Uh;

    sget v1, LX/2SU;->y:I

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->i()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->k()LX/103;

    move-result-object v0

    sget-object v1, LX/103;->VIDEO:LX/103;

    if-ne v0, v1, :cond_3

    .line 2271377
    :cond_2
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->F:LX/EQ9;

    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EQ9;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SZ;

    move-result-object v0

    invoke-interface {v0, p1}, LX/2SZ;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V

    .line 2271378
    :cond_3
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271379
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->s:LX/Cvy;

    invoke-virtual {v0}, LX/Cvy;->f()V

    goto :goto_0

    .line 2271380
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2271381
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)V
    .locals 6

    .prologue
    .line 2271454
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2271455
    iget-object v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v0, v0

    .line 2271456
    sget-object v1, LX/3bj;->ns_trending:LX/3bj;

    invoke-virtual {v0, v1}, LX/3bj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2271457
    iget-object v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v0, v0

    .line 2271458
    sget-object v1, LX/3bj;->ns_interest:LX/3bj;

    invoke-virtual {v0, v1}, LX/3bj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const-string v0, "news_v2"

    .line 2271459
    :goto_0
    const-string v1, "news_v2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->o()Ljava/lang/String;

    move-result-object v1

    .line 2271460
    :goto_1
    new-instance v4, LX/CwH;

    invoke-direct {v4}, LX/CwH;-><init>()V

    .line 2271461
    iget-object v5, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->k:Ljava/lang/String;

    move-object v5, v5

    .line 2271462
    iput-object v5, v4, LX/CwH;->b:Ljava/lang/String;

    .line 2271463
    move-object v4, v4

    .line 2271464
    iget-object v5, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->k:Ljava/lang/String;

    move-object v5, v5

    .line 2271465
    iput-object v5, v4, LX/CwH;->d:Ljava/lang/String;

    .line 2271466
    move-object v4, v4

    .line 2271467
    iget-object v5, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->j:Ljava/lang/String;

    move-object v5, v5

    .line 2271468
    iput-object v5, v4, LX/CwH;->c:Ljava/lang/String;

    .line 2271469
    move-object v4, v4

    .line 2271470
    iput-object v0, v4, LX/CwH;->e:Ljava/lang/String;

    .line 2271471
    move-object v0, v4

    .line 2271472
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2271473
    iput-object v4, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2271474
    move-object v0, v0

    .line 2271475
    sget-object v4, LX/CwF;->keyword:LX/CwF;

    .line 2271476
    iput-object v4, v0, LX/CwH;->g:LX/CwF;

    .line 2271477
    move-object v0, v0

    .line 2271478
    sget-object v4, LX/CwI;->NULL_STATE_MODULE:LX/CwI;

    .line 2271479
    iput-object v4, v0, LX/CwH;->l:LX/CwI;

    .line 2271480
    move-object v0, v0

    .line 2271481
    iput-object v1, v0, LX/CwH;->j:Ljava/lang/String;

    .line 2271482
    move-object v0, v0

    .line 2271483
    iget-object v1, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->l:Ljava/lang/String;

    move-object v1, v1

    .line 2271484
    iput-object v1, v0, LX/CwH;->z:Ljava/lang/String;

    .line 2271485
    move-object v0, v0

    .line 2271486
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    .line 2271487
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v1, :cond_1

    .line 2271488
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    invoke-virtual {v1, v0}, LX/FZb;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V

    .line 2271489
    :cond_1
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2271490
    iget-object v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v0, v0

    .line 2271491
    sget-object v4, LX/3bj;->ns_trending:LX/3bj;

    invoke-virtual {v0, v4}, LX/3bj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "trending"

    :goto_2
    invoke-static {v1, v0, p1, v2, v3}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V

    .line 2271492
    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->z()V

    .line 2271493
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271494
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2271495
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v1

    .line 2271496
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271497
    iget-object v2, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v2

    .line 2271498
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271499
    iget-object v2, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v2

    .line 2271500
    iget-object v2, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v2

    .line 2271501
    if-eqz v0, :cond_2

    .line 2271502
    iget-object v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->i:Ljava/lang/String;

    move-object v0, v0

    .line 2271503
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2271504
    iget-object v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->i:Ljava/lang/String;

    move-object v0, v0

    .line 2271505
    :goto_3
    invoke-virtual {v1, v0}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2271506
    :cond_2
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271507
    return-void

    .line 2271508
    :cond_3
    const-string v0, "content"

    goto/16 :goto_0

    .line 2271509
    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 2271510
    :cond_5
    const-string v0, "keyword"

    goto :goto_2

    .line 2271511
    :cond_6
    iget-object v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2271512
    goto :goto_3
.end method

.method public final a(Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)V
    .locals 9

    .prologue
    .line 2271433
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v2

    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2271434
    iget-object v3, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->aa:LX/Cwd;

    move-object v3, v3

    .line 2271435
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->X:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271436
    iget-object v4, v1, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v4, v4

    .line 2271437
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->r(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/7B5;

    move-result-object v5

    move-object v1, p1

    .line 2271438
    const-string v6, "null_state_see_more"

    invoke-static {v0, v6}, LX/Cvp;->c(LX/Cvp;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "null_state_group_type"

    invoke-virtual {v1}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v8

    invoke-virtual {v8}, LX/Cwb;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "last_state"

    invoke-virtual {v6, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2271439
    invoke-static {v6, v5}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/7B5;)V

    .line 2271440
    invoke-static {v6, v3, v4}, LX/Cvp;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;LX/Cwd;LX/103;)V

    .line 2271441
    iget-object v7, v0, LX/Cvp;->b:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2271442
    const/4 v7, 0x3

    invoke-static {v7}, LX/01m;->b(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2271443
    invoke-virtual {v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->s()Ljava/lang/String;

    .line 2271444
    :cond_0
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->h()LX/2SP;

    move-result-object v0

    .line 2271445
    if-nez v0, :cond_1

    .line 2271446
    :goto_0
    return-void

    .line 2271447
    :cond_1
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->u(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271448
    invoke-virtual {p1}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2SP;->a(LX/Cwb;)V

    .line 2271449
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->l()LX/Fid;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2271450
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->l()LX/Fid;

    move-result-object v1

    invoke-virtual {v0}, LX/2SP;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v1, v0}, LX/Fid;->a(Ljava/util/List;)V

    .line 2271451
    :cond_2
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->d()LX/1Qq;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2271452
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v0}, LX/Fgb;->d()LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2271453
    :cond_3
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V
    .locals 8

    .prologue
    .line 2271574
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2271575
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v2, :cond_0

    .line 2271576
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    iget-object v3, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v3, v3, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v3}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v3

    .line 2271577
    iget-object v4, v2, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v4, v4, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    const/4 v6, 0x0

    .line 2271578
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v5, v5

    .line 2271579
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    .line 2271580
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->l:Ljava/lang/String;

    move-object v7, v7

    .line 2271581
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 2271582
    iget-object v5, v4, LX/FZe;->b:LX/17W;

    iget-object v6, v4, LX/FZe;->a:Landroid/content/Context;

    .line 2271583
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->l:Ljava/lang/String;

    move-object v7, v7

    .line 2271584
    invoke-virtual {v5, v6, v7}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2271585
    :cond_0
    :goto_0
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const-string v3, "recent_search"

    invoke-static {v2, v3, p1, v0, v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V

    .line 2271586
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271587
    invoke-virtual {p1}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2271588
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271589
    iget-object v1, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, v1

    .line 2271590
    iget-object v1, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2271591
    iget-object v1, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2271592
    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2271593
    :cond_1
    iget-boolean v0, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->d:Z

    move v0, v0

    .line 2271594
    if-nez v0, :cond_2

    .line 2271595
    :goto_1
    return-void

    .line 2271596
    :cond_2
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v1, Lcom/facebook/search/suggestions/SuggestionsFragment$DispatchTypeaheadSuggestionClickVisitor$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment$DispatchTypeaheadSuggestionClickVisitor$1;-><init>(LX/Fh8;Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    goto :goto_1

    .line 2271597
    :cond_3
    const v7, 0x361ab677

    if-ne v5, v7, :cond_6

    .line 2271598
    sget-object v5, LX/CwI;->RECENT_SEARCHES:LX/CwI;

    .line 2271599
    sget-object v6, LX/CwF;->keyword:LX/CwF;

    .line 2271600
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v7, v7

    .line 2271601
    sget-object v2, LX/CwF;->local:LX/CwF;

    if-eq v7, v2, :cond_4

    .line 2271602
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v7, v7

    .line 2271603
    sget-object v2, LX/CwF;->local_category:LX/CwF;

    if-ne v7, v2, :cond_5

    .line 2271604
    :cond_4
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v6, v6

    .line 2271605
    :cond_5
    new-instance v2, LX/CwH;

    invoke-direct {v2}, LX/CwH;-><init>()V

    .line 2271606
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->m:Ljava/lang/String;

    move-object v7, v7

    .line 2271607
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2271608
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v7, v7

    .line 2271609
    :goto_2
    iput-object v7, v2, LX/CwH;->c:Ljava/lang/String;

    .line 2271610
    move-object v7, v2

    .line 2271611
    iget-object v2, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2271612
    iput-object v2, v7, LX/CwH;->b:Ljava/lang/String;

    .line 2271613
    move-object v7, v7

    .line 2271614
    iget-object v2, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2271615
    iput-object v2, v7, LX/CwH;->d:Ljava/lang/String;

    .line 2271616
    move-object v7, v7

    .line 2271617
    const-string v2, "content"

    .line 2271618
    iput-object v2, v7, LX/CwH;->e:Ljava/lang/String;

    .line 2271619
    move-object v7, v7

    .line 2271620
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2271621
    iput-object v2, v7, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2271622
    move-object v7, v7

    .line 2271623
    iput-object v6, v7, LX/CwH;->g:LX/CwF;

    .line 2271624
    move-object v6, v7

    .line 2271625
    iput-object v5, v6, LX/CwH;->l:LX/CwI;

    .line 2271626
    move-object v6, v6

    .line 2271627
    move-object v5, v6

    .line 2271628
    invoke-static {v3}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v6

    .line 2271629
    iput-object v6, v5, LX/CwH;->x:LX/0Px;

    .line 2271630
    move-object v5, v5

    .line 2271631
    invoke-virtual {v5}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/FZe;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V

    goto/16 :goto_0

    .line 2271632
    :cond_6
    const v7, -0x1bce060e

    if-ne v5, v7, :cond_7

    .line 2271633
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2271634
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->m:Ljava/lang/String;

    move-object v6, v6

    .line 2271635
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v7, v7

    .line 2271636
    sget-object v2, LX/CwI;->RECENT_SEARCHES:LX/CwI;

    invoke-static {v5, v6, v7, v2}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/CwF;LX/CwI;)LX/CwH;

    move-result-object v5

    invoke-static {v3}, LX/8ht;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/0Px;

    move-result-object v6

    .line 2271637
    iput-object v6, v5, LX/CwH;->x:LX/0Px;

    .line 2271638
    move-object v5, v5

    .line 2271639
    invoke-virtual {v5}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/FZe;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V

    goto/16 :goto_0

    .line 2271640
    :cond_7
    const v7, 0x30654a2e

    if-ne v5, v7, :cond_8

    .line 2271641
    new-instance v5, LX/CwZ;

    invoke-direct {v5}, LX/CwZ;-><init>()V

    .line 2271642
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2271643
    iput-object v6, v5, LX/CwZ;->a:Ljava/lang/String;

    .line 2271644
    move-object v5, v5

    .line 2271645
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2271646
    iput-object v6, v5, LX/CwZ;->b:Ljava/lang/String;

    .line 2271647
    move-object v5, v5

    .line 2271648
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v6, v6

    .line 2271649
    iput-object v6, v5, LX/CwZ;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2271650
    move-object v5, v5

    .line 2271651
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v6, v6

    .line 2271652
    iput-object v6, v5, LX/CwZ;->d:Landroid/net/Uri;

    .line 2271653
    move-object v5, v5

    .line 2271654
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->f:Landroid/net/Uri;

    move-object v6, v6

    .line 2271655
    iput-object v6, v5, LX/CwZ;->g:Landroid/net/Uri;

    .line 2271656
    move-object v5, v5

    .line 2271657
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->g:Landroid/net/Uri;

    move-object v6, v6

    .line 2271658
    iput-object v6, v5, LX/CwZ;->h:Landroid/net/Uri;

    .line 2271659
    move-object v5, v5

    .line 2271660
    invoke-virtual {v5}, LX/CwZ;->i()Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    move-result-object v5

    move-object v5, v5

    .line 2271661
    invoke-virtual {v4, v5}, LX/FZe;->a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)V

    goto/16 :goto_0

    .line 2271662
    :cond_8
    invoke-static {v3}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2271663
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v7, v5

    .line 2271664
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v5

    .line 2271665
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v5, v5

    .line 2271666
    if-eqz v5, :cond_9

    .line 2271667
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v5, v5

    .line 2271668
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_3
    invoke-static {v4, v7, v2, v6, v5}, LX/FZe;->a(LX/FZe;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    move-object v5, v6

    goto :goto_3

    .line 2271669
    :cond_a
    const v6, 0x25d6af

    if-ne v5, v6, :cond_b

    .line 2271670
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2271671
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2271672
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v7, v7

    .line 2271673
    sget-object v2, LX/89z;->RECENT_SEARCHS:LX/89z;

    invoke-static {v4, v5, v6, v7, v2}, LX/FZe;->a(LX/FZe;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/89z;)V

    goto/16 :goto_0

    .line 2271674
    :cond_b
    const v6, 0x403827a

    if-ne v5, v6, :cond_c

    .line 2271675
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2271676
    invoke-static {v4, v5}, LX/FZe;->b(LX/FZe;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2271677
    :cond_c
    iget-object v5, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v5, v5

    .line 2271678
    iget-object v6, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2271679
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v7, v7

    .line 2271680
    iget-object v2, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2271681
    invoke-static {v4, v5, v6, v7, v2}, LX/FZe;->b(LX/FZe;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2271682
    :cond_d
    iget-object v7, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->m:Ljava/lang/String;

    move-object v7, v7

    .line 2271683
    goto/16 :goto_2
.end method

.method public final a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)V
    .locals 7

    .prologue
    .line 2271424
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2271425
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v0, :cond_0

    .line 2271426
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    invoke-virtual {v0, p1}, LX/FZb;->a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)V

    .line 2271427
    :cond_0
    iget-object v0, p1, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->a:Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-object v1, v0

    .line 2271428
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2271429
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cw;

    sget-object v4, LX/3FC;->SEARCH_NULL_STATE_CLICK:LX/3FC;

    invoke-virtual {v1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->l()LX/2cx;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->d()Z

    move-result v1

    invoke-virtual {v0, v4, v5, v6, v1}, LX/2cw;->a(LX/3FC;LX/2cx;Ljava/lang/String;Z)V

    .line 2271430
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const-string v1, "place_tip"

    invoke-static {v0, v1, p1, v2, v3}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V

    .line 2271431
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271432
    return-void
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V
    .locals 4

    .prologue
    .line 2271417
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2271418
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v2, :cond_0

    .line 2271419
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    invoke-virtual {v2, p1}, LX/FZb;->a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V

    .line 2271420
    :cond_0
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const-string v3, "entity"

    invoke-static {v2, v3, p1, v0, v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V

    .line 2271421
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271422
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->F:LX/EQ9;

    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EQ9;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SZ;

    move-result-object v0

    invoke-interface {v0, p1}, LX/2SZ;->a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V

    .line 2271423
    return-void
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreTypeaheadUnit;)V
    .locals 9

    .prologue
    .line 2271382
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->K(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2271383
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v1

    .line 2271384
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v0, :cond_2

    .line 2271385
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    .line 2271386
    iget-object v2, v0, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v2, v2, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    .line 2271387
    iget-object v3, v2, LX/FZe;->d:LX/FZW;

    .line 2271388
    sget-object v4, LX/FZc;->RESULTS_SEE_MORE:LX/FZc;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, LX/FZW;->a(LX/FZW;LX/FZc;Landroid/os/Bundle;)LX/FZU;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    move-object v3, v4

    .line 2271389
    iget-object v4, v2, LX/FZe;->d:LX/FZW;

    invoke-virtual {v4}, LX/FZW;->c()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v4

    const/4 v7, 0x0

    .line 2271390
    iget-object v5, v3, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->j:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, v3, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2271391
    iget-object v5, v3, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->a:LX/FfG;

    .line 2271392
    iget-object v6, v5, LX/FfG;->d:LX/FfJ;

    .line 2271393
    invoke-virtual {v6}, LX/FfJ;->b()V

    .line 2271394
    iget-object v8, v6, LX/FfJ;->c:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->clear()V

    .line 2271395
    iget-object v8, v6, LX/FfJ;->d:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->clear()V

    .line 2271396
    iget-object v8, v6, LX/FfJ;->b:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->clear()V

    .line 2271397
    iget-object v8, v6, LX/FfJ;->e:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->clear()V

    .line 2271398
    iget-object v8, v6, LX/FfJ;->f:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->clear()V

    .line 2271399
    iget-object v8, v6, LX/FfJ;->g:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->clear()V

    .line 2271400
    iget-object v8, v6, LX/FfJ;->h:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->clear()V

    .line 2271401
    iget-object v6, v5, LX/FfG;->e:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Cw8;

    .line 2271402
    iget-object v8, v5, LX/FfG;->e:Ljava/util/Map;

    invoke-interface {v8, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Ff8;

    invoke-static {v5, v6, v8}, LX/FfG;->a$redex0(LX/FfG;LX/Cw8;LX/Ff8;)V

    goto :goto_0

    .line 2271403
    :cond_0
    iget-object v6, v5, LX/FfG;->d:LX/FfJ;

    invoke-static {v5}, LX/FfG;->i(LX/FfG;)Ljava/lang/String;

    move-result-object v8

    iget-object v0, v5, LX/FfG;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-static {v0}, LX/Cw8;->getCoreFilterTypeAt(I)LX/Cw8;

    move-result-object v0

    invoke-virtual {v6, v8, v0}, LX/FfJ;->a(Ljava/lang/String;LX/Cw8;)V

    .line 2271404
    :cond_1
    invoke-static {v3}, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->j:Ljava/lang/String;

    .line 2271405
    iget-object v5, v3, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5, v7, v7}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2271406
    iget-object v5, v3, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v5, v7, v7}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->scrollTo(II)V

    .line 2271407
    iget-object v5, v3, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->g:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    const/4 v6, 0x0

    invoke-virtual {v5, v7, v6, v7}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->a(IFI)V

    .line 2271408
    iget-object v5, v3, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    .line 2271409
    iput-object v4, v5, LX/Cvm;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 2271410
    invoke-static {v5}, LX/Cvm;->c(LX/Cvm;)V

    .line 2271411
    iget-object v4, v2, LX/FZe;->e:LX/FZX;

    invoke-virtual {v4, v3}, LX/FZX;->b(Landroid/support/v4/app/Fragment;)V

    .line 2271412
    :cond_2
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvp;

    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v2}, Lcom/facebook/search/suggestions/SuggestionsFragment;->w(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/0Px;

    move-result-object v2

    .line 2271413
    iget-object v3, p1, Lcom/facebook/search/model/SeeMoreTypeaheadUnit;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2271414
    iget-object v4, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v4}, Lcom/facebook/search/suggestions/SuggestionsFragment;->J(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v4

    iget-object v5, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v5, v5, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v5}, LX/Fgb;->e()LX/7HZ;

    move-result-object v5

    iget-object v6, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v6}, Lcom/facebook/search/suggestions/SuggestionsFragment;->p(Lcom/facebook/search/suggestions/SuggestionsFragment;)LX/CwU;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/Cvp;->a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZLX/7HZ;LX/CwU;)V

    .line 2271415
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271416
    return-void
.end method

.method public final a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)V
    .locals 4

    .prologue
    .line 2271352
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2271353
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v2, :cond_0

    .line 2271354
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    .line 2271355
    iget-object v3, v2, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v3, v3, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    invoke-virtual {v3, p1}, LX/FZe;->a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)V

    .line 2271356
    :cond_0
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const-string v3, "shortcut"

    invoke-static {v2, v3, p1, v0, v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V

    .line 2271357
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271358
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->F:LX/EQ9;

    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->Z:LX/Fgb;

    invoke-interface {v1}, LX/Fgb;->i()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EQ9;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SZ;

    move-result-object v0

    invoke-interface {v0, p1}, LX/2SZ;->a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)V

    .line 2271359
    return-void
.end method

.method public final a(Lcom/facebook/search/model/TrendingTypeaheadUnit;)V
    .locals 7

    .prologue
    .line 2271307
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v0, v0, Lcom/facebook/search/suggestions/SuggestionsFragment;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2271308
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    if-eqz v2, :cond_0

    .line 2271309
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->W:LX/FZb;

    .line 2271310
    iget-object v3, v2, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v3, v3, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    .line 2271311
    invoke-virtual {p1}, Lcom/facebook/search/model/TrendingTypeaheadUnit;->u()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2271312
    sget-object v4, LX/0ax;->fr:Ljava/lang/String;

    .line 2271313
    iget-object v5, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->j:Ljava/lang/String;

    move-object v5, v5

    .line 2271314
    const-string v6, "ANDROID_TRENDING_PLACE_TIPS"

    invoke-static {v4, v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2271315
    iget-object v5, v3, LX/FZe;->b:LX/17W;

    iget-object v6, v3, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {v5, v6, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2271316
    :cond_0
    :goto_0
    iget-object v2, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    const-string v3, "trending"

    invoke-static {v2, v3, p1, v0, v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;Lcom/facebook/search/model/TypeaheadUnit;J)V

    .line 2271317
    iget-object v0, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->i:Ljava/lang/String;

    move-object v0, v0

    .line 2271318
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2271319
    :goto_1
    iget-object v1, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->i:LX/FgS;

    .line 2271320
    iget-object v2, v1, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v1, v2

    .line 2271321
    iget-object v2, v1, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v2

    .line 2271322
    invoke-virtual {v1, v0}, Lcom/facebook/ui/search/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2271323
    iget-object v0, p0, LX/Fh8;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2271324
    return-void

    .line 2271325
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/model/TrendingTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2271326
    :cond_2
    new-instance v4, LX/CwH;

    invoke-direct {v4}, LX/CwH;-><init>()V

    iget-object v5, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->c:Ljava/lang/String;

    .line 2271327
    iput-object v5, v4, LX/CwH;->b:Ljava/lang/String;

    .line 2271328
    move-object v4, v4

    .line 2271329
    iget-object v5, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->d:Ljava/lang/String;

    .line 2271330
    iput-object v5, v4, LX/CwH;->c:Ljava/lang/String;

    .line 2271331
    move-object v4, v4

    .line 2271332
    iget-object v5, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->a:Ljava/lang/String;

    .line 2271333
    iput-object v5, v4, LX/CwH;->d:Ljava/lang/String;

    .line 2271334
    move-object v4, v4

    .line 2271335
    const-string v5, "news_v2"

    .line 2271336
    iput-object v5, v4, LX/CwH;->e:Ljava/lang/String;

    .line 2271337
    move-object v4, v4

    .line 2271338
    invoke-virtual {p1}, Lcom/facebook/search/model/TrendingTypeaheadUnit;->jC_()LX/0Px;

    move-result-object v5

    .line 2271339
    iput-object v5, v4, LX/CwH;->x:LX/0Px;

    .line 2271340
    move-object v4, v4

    .line 2271341
    sget-object v5, LX/CwI;->TRENDING_ENTITY:LX/CwI;

    .line 2271342
    iput-object v5, v4, LX/CwH;->l:LX/CwI;

    .line 2271343
    move-object v4, v4

    .line 2271344
    iget-object v5, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->g:Ljava/lang/String;

    .line 2271345
    iput-object v5, v4, LX/CwH;->j:Ljava/lang/String;

    .line 2271346
    move-object v4, v4

    .line 2271347
    iget-object v5, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->l:Ljava/lang/String;

    iget-object v6, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->m:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->n:LX/103;

    invoke-virtual {v4, v5, v6, v2}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/103;)LX/CwH;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/search/model/TrendingTypeaheadUnit;->o:Ljava/lang/String;

    .line 2271348
    iput-object v5, v4, LX/CwH;->z:Ljava/lang/String;

    .line 2271349
    move-object v4, v4

    .line 2271350
    invoke-virtual {v4}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v4

    move-object v4, v4

    .line 2271351
    invoke-virtual {v3, v4}, LX/FZe;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V

    goto :goto_0
.end method
