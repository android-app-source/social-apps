.class public final LX/GLD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/GLE;


# direct methods
.method public constructor <init>(LX/GLE;J)V
    .locals 0

    .prologue
    .line 2342425
    iput-object p1, p0, LX/GLD;->b:LX/GLE;

    iput-wide p2, p0, LX/GLD;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 6

    .prologue
    .line 2342426
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2342427
    iget-wide v2, p0, LX/GLD;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2342428
    invoke-virtual {v0, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 2342429
    invoke-virtual {p1}, Landroid/widget/DatePicker;->getMinDate()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Landroid/widget/DatePicker;->getMaxDate()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 2342430
    :cond_0
    iget-object v0, p0, LX/GLD;->b:LX/GLE;

    invoke-virtual {v0, p1}, LX/GLE;->onClick(Landroid/view/View;)V

    .line 2342431
    :goto_0
    return-void

    .line 2342432
    :cond_1
    iget-object v1, p0, LX/GLD;->b:LX/GLE;

    iget-object v1, v1, LX/GLE;->a:LX/GLH;

    iget-object v1, v1, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->setDate(Ljava/lang/Long;)V

    .line 2342433
    iget-object v0, p0, LX/GLD;->b:LX/GLE;

    iget-object v0, v0, LX/GLE;->a:LX/GLH;

    iget-object v1, p0, LX/GLD;->b:LX/GLE;

    iget-object v1, v1, LX/GLE;->a:LX/GLH;

    iget-object v1, v1, LX/GLH;->i:LX/GG6;

    iget-object v2, p0, LX/GLD;->b:LX/GLE;

    iget-object v2, v2, LX/GLE;->a:LX/GLH;

    iget-object v2, v2, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    .line 2342434
    iget-object v3, v2, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->c:Ljava/lang/Long;

    move-object v2, v3

    .line 2342435
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/GG6;->a(J)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2342436
    iput-object v1, v0, LX/GLH;->d:Ljava/lang/Integer;

    .line 2342437
    iget-object v0, p0, LX/GLD;->b:LX/GLE;

    iget-object v0, v0, LX/GLE;->a:LX/GLH;

    iget-object v0, v0, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GLD;->b:LX/GLE;

    iget-object v1, v1, LX/GLE;->a:LX/GLH;

    iget-object v1, v1, LX/GLH;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2342438
    iget-object v0, p0, LX/GLD;->b:LX/GLE;

    iget-object v0, v0, LX/GLE;->a:LX/GLH;

    .line 2342439
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2342440
    new-instance v1, LX/GFM;

    iget-object v2, p0, LX/GLD;->b:LX/GLE;

    iget-object v2, v2, LX/GLE;->a:LX/GLH;

    iget-object v2, v2, LX/GLH;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, LX/GFM;-><init>(I)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2342441
    iget-object v0, p0, LX/GLD;->b:LX/GLE;

    iget-object v0, v0, LX/GLE;->a:LX/GLH;

    iget-object v0, v0, LX/GLH;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesScheduleView;->a()V

    .line 2342442
    iget-object v0, p0, LX/GLD;->b:LX/GLE;

    iget-object v0, v0, LX/GLE;->a:LX/GLH;

    invoke-static {v0}, LX/GLH;->e(LX/GLH;)V

    .line 2342443
    iget-object v0, p0, LX/GLD;->b:LX/GLE;

    iget-object v0, v0, LX/GLE;->a:LX/GLH;

    .line 2342444
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2342445
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2342446
    iget-object v1, p0, LX/GLD;->b:LX/GLE;

    iget-object v1, v1, LX/GLE;->a:LX/GLH;

    iget-object v1, v1, LX/GLH;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1}, LX/GG3;->r(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2342447
    iget-object v0, p0, LX/GLD;->b:LX/GLE;

    iget-object v0, v0, LX/GLE;->a:LX/GLH;

    invoke-static {v0}, LX/GLH;->c(LX/GLH;)V

    goto/16 :goto_0
.end method
