.class public final LX/GIB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bc9;


# instance fields
.field public final synthetic a:LX/GID;


# direct methods
.method public constructor <init>(LX/GID;)V
    .locals 0

    .prologue
    .line 2335909
    iput-object p1, p0, LX/GIB;->a:LX/GID;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;I)V
    .locals 3

    .prologue
    .line 2335910
    invoke-virtual {p1, p2}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2335911
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2335912
    iget-object v1, p0, LX/GIB;->a:LX/GID;

    const/16 p2, 0x8

    const/4 p1, 0x0

    .line 2335913
    const-string v2, "no_button_tag"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2335914
    iget-object v2, v1, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335915
    iget-object p0, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v2, p0

    .line 2335916
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2335917
    iput-object p0, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2335918
    iget-object v2, v1, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2335919
    iput-object p0, v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2335920
    iget-object v2, v1, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v2, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setRadioGroupVisibility(I)V

    .line 2335921
    :cond_0
    :goto_0
    invoke-static {v1}, LX/GID;->d(LX/GID;)V

    .line 2335922
    :goto_1
    return-void

    .line 2335923
    :cond_1
    const-string v2, "instant_workflow_tag"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2335924
    iget-object v2, v1, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    if-eqz v2, :cond_2

    .line 2335925
    iget-object v2, v1, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object p0, v1, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;->j()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object p0

    .line 2335926
    iput-object p0, v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2335927
    iget-object v2, v1, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335928
    iget-object p0, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v2, p0

    .line 2335929
    iget-object p0, v1, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;->j()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object p0

    .line 2335930
    iput-object p0, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2335931
    iget-object v2, v1, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335932
    iget-object p0, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v2, p0

    .line 2335933
    iget-object p0, v1, LX/GID;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentActionButtonModel;->k()Ljava/lang/String;

    move-result-object p0

    .line 2335934
    iput-object p0, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    .line 2335935
    iget-object v2, v1, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335936
    iget-object p0, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v2, p0

    .line 2335937
    const/4 p0, 0x1

    .line 2335938
    iput-boolean p0, v2, Lcom/facebook/adinterfaces/model/CreativeAdModel;->p:Z

    .line 2335939
    :cond_2
    iget-object v2, v1, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v2, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setRadioGroupVisibility(I)V

    goto :goto_0

    .line 2335940
    :cond_3
    const-string v2, "other_button_tag"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2335941
    iget-object v2, v1, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getCheckedCallToActionType()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    .line 2335942
    iget-object p0, v1, LX/GID;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2335943
    iget-object p2, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object p0, p2

    .line 2335944
    iput-boolean p1, p0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->p:Z

    .line 2335945
    iget-object p0, v1, LX/GID;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setRadioGroupVisibility(I)V

    .line 2335946
    invoke-static {v1, v2}, LX/GID;->a$redex0(LX/GID;Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    goto :goto_1
.end method
