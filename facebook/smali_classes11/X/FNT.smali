.class public final LX/FNT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;",
        ">;",
        "LX/0Px",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FNW;


# direct methods
.method public constructor <init>(LX/FNW;)V
    .locals 0

    .prologue
    .line 2232641
    iput-object p1, p0, LX/FNT;->a:LX/FNW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2232642
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2232643
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2232644
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2232645
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2232646
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2232647
    check-cast v0, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel;->a()Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/sms/spam/protocol/SmsSpamStatusModels$FetchUserReportedNumbersModel$BlockedNumbersModel;->a()LX/0Px;

    move-result-object v0

    new-instance v1, LX/FNS;

    invoke-direct {v1, p0}, LX/FNS;-><init>(LX/FNT;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
