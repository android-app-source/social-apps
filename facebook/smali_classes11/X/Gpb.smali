.class public LX/Gpb;
.super LX/GpR;
.source ""


# instance fields
.field private final d:LX/CHi;


# direct methods
.method public constructor <init>(LX/Gpv;LX/CHi;)V
    .locals 0

    .prologue
    .line 2395247
    invoke-direct {p0, p1}, LX/GpR;-><init>(LX/Gpv;)V

    .line 2395248
    iput-object p2, p0, LX/Gpb;->d:LX/CHi;

    .line 2395249
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2395246
    invoke-virtual {p0}, LX/GpR;->b()V

    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2395230
    iget-object v0, p0, LX/Gpb;->d:LX/CHi;

    invoke-interface {v0}, LX/CHg;->p()LX/1Fb;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2395231
    :goto_0
    return-void

    .line 2395232
    :cond_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395233
    check-cast v0, LX/Gpv;

    invoke-interface {v0}, LX/Gpv;->a()V

    .line 2395234
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395235
    check-cast v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;

    iget-object v1, p0, LX/Gpb;->d:LX/CHi;

    invoke-interface {v1}, LX/CHg;->p()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 2395236
    iget-object v2, v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->f()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2395237
    new-instance v1, LX/GoE;

    iget-object v0, p0, LX/Gpb;->d:LX/CHi;

    invoke-interface {v0}, LX/CHg;->jt_()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/Gpb;->d:LX/CHi;

    invoke-interface {v2}, LX/CHg;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395238
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395239
    check-cast v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;

    .line 2395240
    iput-object v1, v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->e:LX/GoE;

    .line 2395241
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395242
    check-cast v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;

    .line 2395243
    iget-object v2, v0, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;->c:LX/Go0;

    invoke-virtual {v2, v1}, LX/Go0;->a(LX/GoE;)V

    .line 2395244
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395245
    check-cast v0, LX/Gpv;

    iget-object v1, p0, LX/Gpb;->d:LX/CHi;

    invoke-interface {v1}, LX/CHi;->j()LX/CHX;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Gpv;->a(LX/CHX;)V

    goto :goto_0
.end method
