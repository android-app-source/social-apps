.class public LX/Fu1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0ad;

.field private final c:LX/0hB;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;LX/0hB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2299471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2299472
    iput-object p1, p0, LX/Fu1;->a:Landroid/content/res/Resources;

    .line 2299473
    iput-object p2, p0, LX/Fu1;->b:LX/0ad;

    .line 2299474
    iput-object p3, p0, LX/Fu1;->c:LX/0hB;

    .line 2299475
    return-void
.end method

.method public static a(LX/0QB;)LX/Fu1;
    .locals 6

    .prologue
    .line 2299476
    const-class v1, LX/Fu1;

    monitor-enter v1

    .line 2299477
    :try_start_0
    sget-object v0, LX/Fu1;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2299478
    sput-object v2, LX/Fu1;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2299479
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299480
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2299481
    new-instance p0, LX/Fu1;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v5

    check-cast v5, LX/0hB;

    invoke-direct {p0, v3, v4, v5}, LX/Fu1;-><init>(Landroid/content/res/Resources;LX/0ad;LX/0hB;)V

    .line 2299482
    move-object v0, p0

    .line 2299483
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2299484
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fu1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2299485
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2299486
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 2299487
    iget-object v0, p0, LX/Fu1;->b:LX/0ad;

    sget-short v1, LX/0wf;->F:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2299488
    iget-object v0, p0, LX/Fu1;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b228e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 2299489
    iget-object v1, p0, LX/Fu1;->c:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x3

    .line 2299490
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Fu1;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b2292

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method
