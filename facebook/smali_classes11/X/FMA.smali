.class public final LX/FMA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/messaging/model/messages/Message;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FMB;


# direct methods
.method public constructor <init>(LX/FMB;)V
    .locals 0

    .prologue
    .line 2228515
    iput-object p1, p0, LX/FMA;->a:LX/FMB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 2228516
    check-cast p1, Lcom/facebook/messaging/model/messages/Message;

    check-cast p2, Lcom/facebook/messaging/model/messages/Message;

    .line 2228517
    iget-wide v0, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    iget-wide v2, p2, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2228518
    const/4 v0, 0x0

    .line 2228519
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    iget-wide v2, p2, Lcom/facebook/messaging/model/messages/Message;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
