.class public LX/Gw8;
.super LX/Gvy;
.source ""


# static fields
.field private static final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final l:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private final m:LX/1nC;

.field public final n:LX/8PB;

.field private final o:LX/47E;

.field public final p:LX/74n;

.field private final q:LX/0i4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2406679
    const-class v0, LX/Gw8;

    sput-object v0, LX/Gw8;->k:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;LX/8PB;LX/0aG;LX/0Zb;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/1nC;Ljava/util/concurrent/Executor;LX/47E;LX/74n;LX/0TD;LX/BM1;LX/0i4;LX/BKe;Lcom/facebook/content/SecureContextHelper;)V
    .locals 13
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/8PB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p10    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2406671
    const/16 v6, 0x73

    invoke-virtual {p2}, LX/4hh;->a()Lcom/facebook/platform/common/action/PlatformAppCall;

    move-result-object v7

    invoke-virtual {p2}, LX/8PB;->k()Z

    move-result v8

    move-object v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p7

    move-object v5, p1

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p13

    move-object/from16 v12, p14

    invoke-direct/range {v1 .. v12}, LX/Gvy;-><init>(LX/0aG;LX/0Zb;Ljava/util/concurrent/Executor;Landroid/app/Activity;ILcom/facebook/platform/common/action/PlatformAppCall;ZLX/0TD;LX/BM1;LX/BKe;Lcom/facebook/content/SecureContextHelper;)V

    .line 2406672
    move-object/from16 v0, p5

    iput-object v0, p0, LX/Gw8;->l:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2406673
    move-object/from16 v0, p6

    iput-object v0, p0, LX/Gw8;->m:LX/1nC;

    .line 2406674
    iput-object p2, p0, LX/Gw8;->n:LX/8PB;

    .line 2406675
    move-object/from16 v0, p8

    iput-object v0, p0, LX/Gw8;->o:LX/47E;

    .line 2406676
    move-object/from16 v0, p9

    iput-object v0, p0, LX/Gw8;->p:LX/74n;

    .line 2406677
    move-object/from16 v0, p12

    iput-object v0, p0, LX/Gw8;->q:LX/0i4;

    .line 2406678
    return-void
.end method

.method public static a(LX/Gw8;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406656
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2406657
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2406658
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2406659
    const-string v4, "uri"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2406660
    const-string v5, "type"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/4hX;->valueOf(Ljava/lang/String;)LX/4hX;

    move-result-object v0

    .line 2406661
    sget-object v5, LX/Gw7;->a:[I

    invoke-virtual {v0}, LX/4hX;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2406662
    :pswitch_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2406663
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid media type specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406664
    :goto_1
    return-object v0

    .line 2406665
    :pswitch_1
    :try_start_1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2406666
    :cond_0
    const-string v0, ".jpeg"

    const-string v3, "Failed to copy image."

    invoke-direct {p0, v1, v0, v3}, LX/Gw8;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406667
    const-string v1, ".mp4"

    const-string v3, "Failed to copy video."

    invoke-direct {p0, v2, v1, v3}, LX/Gw8;->a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2406668
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    invoke-static {v2}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406669
    new-instance v1, LX/Gw5;

    invoke-direct {v1, p0, p1}, LX/Gw5;-><init>(LX/Gw8;Ljava/util/List;)V

    .line 2406670
    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406680
    new-instance v0, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;

    iget-object v1, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2406681
    iget-object v2, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2406682
    invoke-direct {v0, v1, p1, p2}, Lcom/facebook/katana/platform/handler/CopyPlatformAppContentToTempFileOperation$Params;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 2406683
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2406684
    const-string v2, "platform_copy_platform_app_content_params"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2406685
    iget-object v0, p0, LX/Gvy;->b:LX/0aG;

    const-string v2, "platform_copy_platform_app_content"

    const v3, 0x73535ba

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    move-object v0, v0

    .line 2406686
    new-instance v1, LX/Gw6;

    invoke-direct {v1, p0, p3}, LX/Gw6;-><init>(LX/Gw8;Ljava/lang/String;)V

    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2406687
    return-object v0
.end method

.method public static f(LX/Gw8;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406648
    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v1, v0

    .line 2406649
    iget-object v0, p0, LX/Gw8;->q:LX/0i4;

    iget-object v2, p0, LX/Gvy;->e:Landroid/app/Activity;

    invoke-virtual {v0, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v2

    .line 2406650
    invoke-virtual {v2, v1}, LX/0i5;->a([Ljava/lang/String;)Z

    .line 2406651
    invoke-virtual {v2, v1}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2406652
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406653
    :goto_0
    return-object v0

    .line 2406654
    :cond_0
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2406655
    iget-object v3, p0, LX/Gvy;->e:Landroid/app/Activity;

    const v4, 0x7f083610

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Gvy;->e:Landroid/app/Activity;

    const v5, 0x7f083611

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Gw2;

    invoke-direct {v5, p0, v0}, LX/Gw2;-><init>(LX/Gw8;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v2, v1, v3, v4, v5}, LX/0i5;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Zx;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2406640
    const-string v0, "attachment_uris"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2406641
    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2406642
    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    .line 2406643
    new-instance v1, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;

    iget-object v2, p0, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    invoke-direct {v1, v2, v0}, Lcom/facebook/katana/platform/handler/AddPendingMediaUploadAppCallOperation$Params;-><init>(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;)V

    .line 2406644
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2406645
    const-string v2, "platform_add_pending_media_upload_params"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2406646
    iget-object v1, p0, LX/Gvy;->b:LX/0aG;

    const-string v2, "platform_add_pending_media_upload"

    const v3, -0x37727633

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2406647
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Gw8;->l:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/8Or;
    .locals 2

    .prologue
    .line 2406622
    invoke-super {p0, p1}, LX/Gvy;->b(Ljava/lang/String;)LX/8Or;

    move-result-object v0

    const-string v1, "android_feed_dialog"

    .line 2406623
    iput-object v1, v0, LX/8Or;->h:Ljava/lang/String;

    .line 2406624
    move-object v0, v0

    .line 2406625
    iget-object v1, p0, LX/Gw8;->n:LX/8PB;

    .line 2406626
    invoke-virtual {v1}, LX/8PB;->q()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, v1, LX/8PB;->i:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    :goto_0
    move v1, p1

    .line 2406627
    iput v1, v0, LX/8Or;->i:I

    .line 2406628
    move-object v0, v0

    .line 2406629
    iget-object v1, p0, LX/Gw8;->n:LX/8PB;

    invoke-virtual {v1}, LX/8PB;->r()Z

    move-result v1

    .line 2406630
    iput-boolean v1, v0, LX/8Or;->j:Z

    .line 2406631
    move-object v0, v0

    .line 2406632
    iget-object v1, p0, LX/Gw8;->n:LX/8PB;

    .line 2406633
    iget-boolean p0, v1, LX/8PB;->o:Z

    move v1, p0

    .line 2406634
    iput-boolean v1, v0, LX/8Or;->m:Z

    .line 2406635
    move-object v0, v0

    .line 2406636
    const-string v1, "share"

    .line 2406637
    iput-object v1, v0, LX/8Or;->g:Ljava/lang/String;

    .line 2406638
    move-object v0, v0

    .line 2406639
    return-object v0

    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2406543
    iget-object v0, p0, LX/Gw8;->n:LX/8PB;

    .line 2406544
    iget-object v3, v0, LX/8PB;->c:Ljava/lang/String;

    move-object v0, v3

    .line 2406545
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2406546
    :goto_0
    iget-object v3, p0, LX/Gw8;->n:LX/8PB;

    invoke-virtual {v3}, LX/8PB;->q()Z

    move-result v3

    .line 2406547
    iget-object v4, p0, LX/Gw8;->n:LX/8PB;

    invoke-virtual {v4}, LX/8PB;->r()Z

    move-result v4

    .line 2406548
    iget-object v5, p0, LX/Gw8;->n:LX/8PB;

    .line 2406549
    iget-object v6, v5, LX/8PB;->m:Ljava/util/ArrayList;

    invoke-static {v6}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v6

    move v5, v6

    .line 2406550
    if-eqz v0, :cond_9

    .line 2406551
    :goto_1
    if-eqz v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    .line 2406552
    :cond_0
    if-eqz v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    .line 2406553
    :cond_1
    if-eqz v5, :cond_8

    add-int/lit8 v1, v1, 0x1

    move v2, v1

    .line 2406554
    :goto_2
    const/4 v1, 0x0

    .line 2406555
    packed-switch v2, :pswitch_data_0

    .line 2406556
    const-string v0, "Only one of link, photos, and video should be specified."

    invoke-virtual {p0, v0}, LX/Gvy;->a(Ljava/lang/String;)V

    .line 2406557
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Only one of link, photos, and video should be specified."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406558
    :goto_3
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2406559
    new-instance v1, LX/Gw0;

    invoke-direct {v1, p0}, LX/Gw0;-><init>(LX/Gw8;)V

    .line 2406560
    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    .line 2406561
    goto :goto_0

    .line 2406562
    :pswitch_0
    if-eqz v3, :cond_3

    .line 2406563
    invoke-static {p0}, LX/Gw8;->f(LX/Gw8;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406564
    new-instance v1, LX/Gw1;

    invoke-direct {v1, p0}, LX/Gw1;-><init>(LX/Gw8;)V

    .line 2406565
    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2406566
    goto :goto_3

    .line 2406567
    :cond_3
    if-eqz v4, :cond_4

    .line 2406568
    invoke-static {p0}, LX/Gw8;->f(LX/Gw8;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406569
    new-instance v1, LX/Gw3;

    invoke-direct {v1, p0}, LX/Gw3;-><init>(LX/Gw8;)V

    .line 2406570
    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2406571
    goto :goto_3

    .line 2406572
    :cond_4
    if-eqz v0, :cond_6

    .line 2406573
    const/4 v8, 0x1

    .line 2406574
    iget-object v0, p0, LX/Gw8;->n:LX/8PB;

    .line 2406575
    iget-object v1, v0, LX/8PB;->c:Ljava/lang/String;

    move-object v3, v1

    .line 2406576
    iget-object v0, p0, LX/Gw8;->n:LX/8PB;

    .line 2406577
    iget-object v1, v0, LX/8PB;->d:Ljava/lang/String;

    move-object v4, v1

    .line 2406578
    iget-object v0, p0, LX/Gw8;->n:LX/8PB;

    .line 2406579
    iget-object v1, v0, LX/8PB;->e:Ljava/lang/String;

    move-object v5, v1

    .line 2406580
    iget-object v0, p0, LX/Gw8;->n:LX/8PB;

    .line 2406581
    iget-object v1, v0, LX/8PB;->f:Ljava/lang/String;

    move-object v2, v1

    .line 2406582
    iget-object v0, p0, LX/Gw8;->n:LX/8PB;

    .line 2406583
    iget-object v1, v0, LX/8PB;->g:Ljava/lang/String;

    move-object v1, v1

    .line 2406584
    iget-object v0, p0, LX/Gw8;->n:LX/8PB;

    .line 2406585
    iget-object v6, v0, LX/8PB;->k:Ljava/lang/String;

    move-object v6, v6

    .line 2406586
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    move-object v0, v1

    .line 2406587
    :goto_4
    new-instance v7, LX/89O;

    invoke-direct {v7}, LX/89O;-><init>()V

    .line 2406588
    iput-object v5, v7, LX/89O;->a:Ljava/lang/String;

    .line 2406589
    move-object v7, v7

    .line 2406590
    iput-object v0, v7, LX/89O;->b:Ljava/lang/String;

    .line 2406591
    move-object v0, v7

    .line 2406592
    iput-object v4, v0, LX/89O;->d:Ljava/lang/String;

    .line 2406593
    move-object v0, v0

    .line 2406594
    iput-boolean v8, v0, LX/89O;->e:Z

    .line 2406595
    move-object v0, v0

    .line 2406596
    invoke-virtual {v0}, LX/89O;->a()Lcom/facebook/ipc/composer/intent/SharePreview;

    move-result-object v0

    .line 2406597
    invoke-static {v3}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v3

    .line 2406598
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2406599
    iput-object v6, v3, LX/89G;->e:Ljava/lang/String;

    .line 2406600
    :cond_5
    invoke-virtual {v3}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    .line 2406601
    sget-object v6, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    const-string v7, "feedDialogActionExecutorForLinkShare"

    invoke-static {v6, v7}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    sget-object v7, LX/2rt;->SHARE:LX/2rt;

    invoke-virtual {v6, v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setComposerType(LX/2rt;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableAttachToAlbum(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    const-string v6, "ANDROID_PLATFORM_COMPOSER"

    invoke-virtual {v3, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    new-instance v6, LX/2ro;

    invoke-direct {v6}, LX/2ro;-><init>()V

    .line 2406602
    iput-object v5, v6, LX/2ro;->f:Ljava/lang/String;

    .line 2406603
    move-object v5, v6

    .line 2406604
    iput-object v2, v5, LX/2ro;->g:Ljava/lang/String;

    .line 2406605
    move-object v2, v5

    .line 2406606
    iput-object v1, v2, LX/2ro;->i:Ljava/lang/String;

    .line 2406607
    move-object v1, v2

    .line 2406608
    iput-object v4, v1, LX/2ro;->h:Ljava/lang/String;

    .line 2406609
    move-object v1, v1

    .line 2406610
    iput-object v0, v1, LX/2ro;->j:Lcom/facebook/ipc/composer/intent/SharePreview;

    .line 2406611
    move-object v0, v1

    .line 2406612
    invoke-virtual {v0}, LX/2ro;->a()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPlatformConfiguration(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    move-object v0, v0

    .line 2406613
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_3

    .line 2406614
    :cond_6
    if-eqz v5, :cond_7

    .line 2406615
    invoke-static {p0}, LX/Gw8;->f(LX/Gw8;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406616
    new-instance v1, LX/Gw4;

    invoke-direct {v1, p0}, LX/Gw4;-><init>(LX/Gw8;)V

    .line 2406617
    iget-object v2, p0, LX/Gvy;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2406618
    goto/16 :goto_3

    .line 2406619
    :pswitch_1
    sget-object v0, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    const-string v1, "feedDialogActionExecutorStatus"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableAttachToAlbum(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const-string v1, "ANDROID_PLATFORM_COMPOSER"

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    move-object v0, v0

    .line 2406620
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_3

    :cond_7
    move-object v0, v1

    goto/16 :goto_3

    :cond_8
    move v2, v1

    goto/16 :goto_2

    :cond_9
    move v1, v2

    goto/16 :goto_1

    :cond_a
    move-object v0, v2

    .line 2406621
    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
