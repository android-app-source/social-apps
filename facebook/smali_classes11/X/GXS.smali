.class public LX/GXS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GXR;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1OM;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/GXR;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2364133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2364134
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GXS;->a:Ljava/util/List;

    .line 2364135
    if-eqz p1, :cond_0

    .line 2364136
    iget-object v0, p0, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2364137
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2364138
    iget-object v0, p0, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(Z)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2364139
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2364140
    iget-object v0, p0, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GXR;

    .line 2364141
    iget-object v3, v0, LX/GXR;->b:LX/GXQ;

    move-object v3, v3

    .line 2364142
    sget-object v4, LX/GXQ;->PRODUCT_IMAGE:LX/GXQ;

    if-ne v3, v4, :cond_1

    .line 2364143
    iget-object v3, v0, LX/GXR;->a:Ljava/lang/Object;

    move-object v0, v3

    .line 2364144
    check-cast v0, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2364145
    :cond_1
    if-eqz p1, :cond_0

    .line 2364146
    const-string v0, "pending_media_item_upload"

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2364147
    :cond_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2364148
    iget-object v0, p0, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GXR;

    invoke-virtual {v0}, LX/GXR;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2364149
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2364150
    iget-object v0, p0, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GXR;

    .line 2364151
    iget-object v3, v0, LX/GXR;->b:LX/GXQ;

    move-object v3, v3

    .line 2364152
    sget-object v4, LX/GXQ;->MEDIA_ITEM:LX/GXQ;

    if-ne v3, v4, :cond_0

    .line 2364153
    iget-object v3, v0, LX/GXR;->a:Ljava/lang/Object;

    move-object v0, v3

    .line 2364154
    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2364155
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
