.class public LX/FbN;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2260083
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2260084
    return-void
.end method

.method public static a(LX/0QB;)LX/FbN;
    .locals 3

    .prologue
    .line 2260085
    const-class v1, LX/FbN;

    monitor-enter v1

    .line 2260086
    :try_start_0
    sget-object v0, LX/FbN;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2260087
    sput-object v2, LX/FbN;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2260088
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2260089
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2260090
    new-instance v0, LX/FbN;

    invoke-direct {v0}, LX/FbN;-><init>()V

    .line 2260091
    move-object v0, v0

    .line 2260092
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2260093
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FbN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260094
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2260095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260096
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2260097
    if-nez v0, :cond_0

    .line 2260098
    const/4 v0, 0x0

    .line 2260099
    :goto_0
    return-object v0

    .line 2260100
    :cond_0
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 2260101
    iput-object v2, v1, LX/8dX;->L:Ljava/lang/String;

    .line 2260102
    move-object v1, v1

    .line 2260103
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, -0x2238d5d

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2260104
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2260105
    move-object v1, v1

    .line 2260106
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lx()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 2260107
    if-nez v2, :cond_1

    .line 2260108
    const/4 v3, 0x0

    .line 2260109
    :goto_1
    move-object v2, v3

    .line 2260110
    iput-object v2, v1, LX/8dX;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 2260111
    move-object v1, v1

    .line 2260112
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2260113
    if-nez v2, :cond_2

    .line 2260114
    const/4 v3, 0x0

    .line 2260115
    :goto_2
    move-object v2, v3

    .line 2260116
    iput-object v2, v1, LX/8dX;->M:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    .line 2260117
    move-object v1, v1

    .line 2260118
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2260119
    if-nez v2, :cond_3

    .line 2260120
    const/4 v3, 0x0

    .line 2260121
    :goto_3
    move-object v2, v3

    .line 2260122
    iput-object v2, v1, LX/8dX;->N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2260123
    move-object v1, v1

    .line 2260124
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    .line 2260125
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2260126
    move-object v1, v1

    .line 2260127
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->l()Ljava/lang/String;

    move-result-object v0

    .line 2260128
    iput-object v0, v1, LX/8dX;->c:Ljava/lang/String;

    .line 2260129
    move-object v0, v1

    .line 2260130
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2260131
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    .line 2260132
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2260133
    move-object v0, v1

    .line 2260134
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v1

    .line 2260135
    if-nez v1, :cond_4

    .line 2260136
    const/4 v2, 0x0

    .line 2260137
    :goto_4
    move-object v1, v2

    .line 2260138
    iput-object v1, v0, LX/8dV;->d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    .line 2260139
    move-object v0, v0

    .line 2260140
    goto :goto_0

    :cond_1
    new-instance v3, LX/8hA;

    invoke-direct {v3}, LX/8hA;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p0

    .line 2260141
    iput-object p0, v3, LX/8hA;->b:Ljava/lang/String;

    .line 2260142
    move-object v3, v3

    .line 2260143
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p0

    .line 2260144
    iput-object p0, v3, LX/8hA;->c:Ljava/lang/String;

    .line 2260145
    move-object v3, v3

    .line 2260146
    invoke-virtual {v3}, LX/8hA;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v3

    goto :goto_1

    :cond_2
    new-instance v3, LX/8db;

    invoke-direct {v3}, LX/8db;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p0

    .line 2260147
    iput p0, v3, LX/8db;->c:I

    .line 2260148
    move-object v3, v3

    .line 2260149
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p0

    .line 2260150
    iput p0, v3, LX/8db;->a:I

    .line 2260151
    move-object v3, v3

    .line 2260152
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    .line 2260153
    iput-object p0, v3, LX/8db;->b:Ljava/lang/String;

    .line 2260154
    move-object v3, v3

    .line 2260155
    invoke-virtual {v3}, LX/8db;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v3

    goto :goto_2

    :cond_3
    invoke-static {v2}, LX/9JZ;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1Fb;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    goto :goto_3

    :cond_4
    new-instance v2, LX/8do;

    invoke-direct {v2}, LX/8do;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2260156
    iput-object v3, v2, LX/8do;->a:Ljava/lang/String;

    .line 2260157
    move-object v2, v2

    .line 2260158
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2260159
    iput-object v3, v2, LX/8do;->b:Ljava/lang/String;

    .line 2260160
    move-object v2, v2

    .line 2260161
    invoke-virtual {v2}, LX/8do;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v2

    goto :goto_4
.end method
