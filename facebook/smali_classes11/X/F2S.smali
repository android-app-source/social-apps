.class public final LX/F2S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/F2U;


# direct methods
.method public constructor <init>(LX/F2U;)V
    .locals 0

    .prologue
    .line 2193093
    iput-object p1, p0, LX/F2S;->a:LX/F2U;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x59950fd7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    move-object v0, p1

    .line 2193094
    check-cast v0, LX/Daz;

    .line 2193095
    iget-object v2, v0, LX/Daz;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2193096
    iget-object v3, p0, LX/F2S;->a:LX/F2U;

    iget-object v3, v3, LX/F2U;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2193097
    new-instance v3, LX/F2R;

    invoke-direct {v3, p0, v2}, LX/F2R;-><init>(LX/F2S;Ljava/lang/String;)V

    .line 2193098
    invoke-virtual {v0}, LX/Daz;->getTitleText()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const/4 v0, 0x0

    .line 2193099
    :goto_0
    iget-object v2, p0, LX/F2S;->a:LX/F2U;

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2193100
    new-instance v7, LX/0ju;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v7, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2193101
    iget-object v4, v2, LX/F2U;->d:Landroid/content/res/Resources;

    const v8, 0x7f0831bd

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2193102
    const v4, 0x7f080019

    invoke-virtual {v7, v4, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2193103
    const v4, 0x7f080017

    new-instance v8, LX/F2T;

    invoke-direct {v8, v2}, LX/F2T;-><init>(LX/F2U;)V

    invoke-virtual {v7, v4, v8}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2193104
    iget-object v4, v2, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v4}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->o()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-nez v4, :cond_3

    move v4, v5

    :goto_1
    if-nez v4, :cond_0

    iget-object v4, v2, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-static {v4}, LX/F4L;->b(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2193105
    :cond_0
    iget-object v4, v2, LX/F2U;->d:Landroid/content/res/Resources;

    const v8, 0x7f0831cb

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v4, v8, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2193106
    :goto_2
    invoke-virtual {v7, v4}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2193107
    invoke-virtual {v7}, LX/0ju;->a()LX/2EJ;

    move-result-object v4

    invoke-virtual {v4}, LX/2EJ;->show()V

    .line 2193108
    :cond_1
    const v0, 0x543915a1

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2193109
    :cond_2
    invoke-virtual {v0}, LX/Daz;->getTitleText()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/F2S;->a:LX/F2U;

    iget-object v2, v2, LX/F2U;->e:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2193110
    :cond_3
    iget-object v4, v2, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v4}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->o()LX/1vs;

    move-result-object v4

    iget-object v8, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2193111
    invoke-virtual {v8, v4, v6}, LX/15i;->j(II)I

    move-result v4

    iget-object v8, v2, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v8}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->z()I

    move-result v8

    if-ge v4, v8, :cond_4

    move v4, v5

    goto :goto_1

    :cond_4
    move v4, v6

    goto :goto_1

    .line 2193112
    :cond_5
    iget-object v4, v2, LX/F2U;->f:LX/F4L;

    iget-object v8, v2, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v4, v8}, LX/F4L;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2193113
    iget-object v4, v2, LX/F2U;->d:Landroid/content/res/Resources;

    const v8, 0x7f0831cc

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v4, v8, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 2193114
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, v2, LX/F2U;->d:Landroid/content/res/Resources;

    const p0, 0x7f0831cd

    invoke-virtual {v8, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v8, v2, LX/F2U;->d:Landroid/content/res/Resources;

    const p0, 0x7f0831cb

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v8, p0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2
.end method
