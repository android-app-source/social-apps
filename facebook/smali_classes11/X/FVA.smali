.class public LX/FVA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FV3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FV3",
        "<",
        "LX/FVI;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/1Kf;

.field private final c:LX/1nC;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1Kf;LX/1nC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250424
    iput-object p1, p0, LX/FVA;->a:Landroid/content/res/Resources;

    .line 2250425
    iput-object p2, p0, LX/FVA;->b:LX/1Kf;

    .line 2250426
    iput-object p3, p0, LX/FVA;->c:LX/1nC;

    .line 2250427
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/FVI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2250428
    const-class v0, LX/FVI;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2250429
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Z
    .locals 6

    .prologue
    .line 2250430
    check-cast p1, LX/FVI;

    const/4 v5, 0x1

    .line 2250431
    iget-object v0, p0, LX/FVA;->b:LX/1Kf;

    const/4 v1, 0x0

    sget-object v2, LX/21D;->SAVED_STORIES_DASHBOARD:LX/21D;

    const-string v3, "shareFromSavedDashboard"

    invoke-interface {p1}, LX/FVI;->i()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    invoke-static {v4}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v4

    invoke-virtual {v4}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    sget-object v3, LX/79w;->SHARE_ITEM:LX/79w;

    invoke-virtual {v3}, LX/79w;->ordinal()I

    move-result v3

    invoke-interface {v0, v1, v2, v3, p2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2250432
    return v5
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2250433
    iget-object v0, p0, LX/FVA;->a:Landroid/content/res/Resources;

    const v1, 0x7f081acb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2250434
    check-cast p1, LX/FVI;

    .line 2250435
    invoke-interface {p1}, LX/FVI;->i()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2250436
    const-string v0, "share_button"

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2250437
    const v0, 0x7f0217a3

    return v0
.end method
