.class public final LX/H1M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Landroid/view/ViewGroup;

.field public final synthetic d:LX/H0w;


# direct methods
.method public constructor <init>(LX/H0w;Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2415090
    iput-object p1, p0, LX/H1M;->d:LX/H0w;

    iput-object p2, p0, LX/H1M;->a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    iput-object p3, p0, LX/H1M;->b:Landroid/content/Context;

    iput-object p4, p0, LX/H1M;->c:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x1a75cc49

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2415091
    iget-object v1, p0, LX/H1M;->a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    iget-object v2, p0, LX/H1M;->d:LX/H0w;

    invoke-virtual {v1, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H0w;)V

    .line 2415092
    iget-object v1, p0, LX/H1M;->a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    const-string v2, "Override removed! Restart the app for changes to take effect."

    invoke-virtual {v1, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->b(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v1

    invoke-virtual {v1}, LX/3oW;->b()V

    .line 2415093
    iget-object v1, p0, LX/H1M;->d:LX/H0w;

    iget-object v2, p0, LX/H1M;->b:Landroid/content/Context;

    iget-object v3, p0, LX/H1M;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v3}, LX/H0w;->c(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 2415094
    const v1, 0x63356662

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
