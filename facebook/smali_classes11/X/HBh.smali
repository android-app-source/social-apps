.class public LX/HBh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/HNE;

.field public final b:LX/HN9;


# direct methods
.method public constructor <init>(LX/HNF;LX/HN9;Landroid/os/ParcelUuid;)V
    .locals 1
    .param p3    # Landroid/os/ParcelUuid;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2437791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2437792
    invoke-virtual {p1, p3}, LX/HNF;->a(Landroid/os/ParcelUuid;)LX/HNE;

    move-result-object v0

    iput-object v0, p0, LX/HBh;->a:LX/HNE;

    .line 2437793
    iput-object p2, p0, LX/HBh;->b:LX/HN9;

    .line 2437794
    return-void
.end method

.method public static a(LX/34b;)Z
    .locals 1

    .prologue
    .line 2437795
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;Landroid/net/Uri;ZLcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)LX/5fp;
    .locals 9

    .prologue
    const/4 v0, 0x1

    .line 2437796
    iget-object v1, p0, LX/HBh;->a:LX/HNE;

    invoke-virtual {v1, p1, p2, p6, v0}, LX/HNE;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Z)LX/34b;

    move-result-object v4

    .line 2437797
    new-instance v7, LX/5fp;

    if-nez p5, :cond_0

    invoke-static {v4}, LX/HBh;->a(LX/34b;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v6, v0

    :goto_0
    const v8, 0x7f020722

    new-instance v0, LX/HBg;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/HBg;-><init>(LX/HBh;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Lcom/facebook/graphql/enums/GraphQLPageActionType;LX/34b;Landroid/content/Context;)V

    move-object v1, v7

    move-object v2, p3

    move-object v3, p4

    move v4, v6

    move v5, v8

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, LX/5fp;-><init>(Ljava/lang/String;Landroid/net/Uri;ZILandroid/view/View$OnClickListener;)V

    return-object v7

    :cond_0
    const/4 v0, 0x0

    move v6, v0

    goto :goto_0
.end method
