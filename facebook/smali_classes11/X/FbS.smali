.class public LX/FbS;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260260
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2260261
    return-void
.end method

.method public static a(LX/0QB;)LX/FbS;
    .locals 3

    .prologue
    .line 2260262
    const-class v1, LX/FbS;

    monitor-enter v1

    .line 2260263
    :try_start_0
    sget-object v0, LX/FbS;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2260264
    sput-object v2, LX/FbS;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2260265
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2260266
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2260267
    new-instance v0, LX/FbS;

    invoke-direct {v0}, LX/FbS;-><init>()V

    .line 2260268
    move-object v0, v0

    .line 2260269
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2260270
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FbS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260271
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2260272
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 14
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260273
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2260274
    if-nez v0, :cond_0

    .line 2260275
    const/4 v0, 0x0

    .line 2260276
    :goto_0
    return-object v0

    .line 2260277
    :cond_0
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 2260278
    iput-object v2, v1, LX/8dX;->L:Ljava/lang/String;

    .line 2260279
    move-object v1, v1

    .line 2260280
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, 0x3c4e1b9e

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2260281
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2260282
    move-object v1, v1

    .line 2260283
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lp()Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;

    move-result-object v0

    .line 2260284
    if-nez v0, :cond_1

    .line 2260285
    const/4 v4, 0x0

    .line 2260286
    :goto_1
    move-object v0, v4

    .line 2260287
    iput-object v0, v1, LX/8dX;->aB:Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    .line 2260288
    move-object v0, v1

    .line 2260289
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2260290
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    .line 2260291
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2260292
    move-object v0, v1

    .line 2260293
    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    .line 2260294
    if-nez v0, :cond_3

    .line 2260295
    :cond_2
    :goto_2
    move-object v4, v7

    .line 2260296
    invoke-static {v4}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a(Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;)Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v4

    goto :goto_1

    .line 2260297
    :cond_3
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 2260298
    const/4 v13, 0x1

    const/4 v9, 0x0

    .line 2260299
    if-nez v0, :cond_5

    .line 2260300
    :goto_3
    move v6, v9

    .line 2260301
    if-eqz v6, :cond_2

    .line 2260302
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 2260303
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 2260304
    const/4 v5, 0x0

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2260305
    new-instance v5, LX/15i;

    const/4 v9, 0x1

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2260306
    instance-of v6, v0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v6, :cond_4

    .line 2260307
    const-string v6, "SearchModelConversionHelper.getSearchResultsElection"

    invoke-virtual {v5, v6, v0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2260308
    :cond_4
    new-instance v7, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    invoke-direct {v7, v5}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;-><init>(LX/15i;)V

    goto :goto_2

    .line 2260309
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->a()LX/0Px;

    move-result-object v10

    .line 2260310
    if-eqz v10, :cond_9

    .line 2260311
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v6

    new-array v11, v6, [I

    move v8, v9

    .line 2260312
    :goto_4
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v6

    if-ge v8, v6, :cond_6

    .line 2260313
    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLSearchElectionPartyInfo;

    invoke-static {v5, v6}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSearchElectionPartyInfo;)I

    move-result v6

    aput v6, v11, v8

    .line 2260314
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_4

    .line 2260315
    :cond_6
    invoke-virtual {v5, v11, v13}, LX/186;->a([IZ)I

    move-result v6

    move v8, v6

    .line 2260316
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->j()LX/0Px;

    move-result-object v11

    .line 2260317
    if-eqz v11, :cond_8

    .line 2260318
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v6

    new-array v12, v6, [I

    move v10, v9

    .line 2260319
    :goto_6
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v6

    if-ge v10, v6, :cond_7

    .line 2260320
    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLSearchElectionDate;

    invoke-static {v5, v6}, LX/A0T;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSearchElectionDate;)I

    move-result v6

    aput v6, v12, v10

    .line 2260321
    add-int/lit8 v6, v10, 0x1

    move v10, v6

    goto :goto_6

    .line 2260322
    :cond_7
    invoke-virtual {v5, v12, v13}, LX/186;->a([IZ)I

    move-result v6

    .line 2260323
    :goto_7
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->n()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2260324
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2260325
    const/4 v12, 0x6

    invoke-virtual {v5, v12}, LX/186;->c(I)V

    .line 2260326
    invoke-virtual {v5, v9, v8}, LX/186;->b(II)V

    .line 2260327
    invoke-virtual {v5, v13, v6}, LX/186;->b(II)V

    .line 2260328
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v10}, LX/186;->b(II)V

    .line 2260329
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v11}, LX/186;->b(II)V

    .line 2260330
    const/4 v6, 0x4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->l()I

    move-result v8

    invoke-virtual {v5, v6, v8, v9}, LX/186;->a(III)V

    .line 2260331
    const/4 v6, 0x5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSearchElectionAllData;->m()I

    move-result v8

    invoke-virtual {v5, v6, v8, v9}, LX/186;->a(III)V

    .line 2260332
    invoke-virtual {v5}, LX/186;->d()I

    move-result v9

    .line 2260333
    invoke-virtual {v5, v9}, LX/186;->d(I)V

    goto/16 :goto_3

    :cond_8
    move v6, v9

    goto :goto_7

    :cond_9
    move v8, v9

    goto :goto_5
.end method
