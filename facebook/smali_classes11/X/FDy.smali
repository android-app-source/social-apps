.class public LX/FDy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Pt;


# static fields
.field public static final a:LX/6Pr;

.field public static final b:LX/6Pr;

.field public static final c:LX/6Pr;

.field public static final d:LX/6Pr;

.field public static final e:LX/6Pr;

.field public static final f:LX/6Pr;

.field public static final g:LX/6Pr;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xff

    const/4 v4, 0x0

    .line 2215951
    new-instance v0, LX/6Pr;

    const-string v1, "Threads Fetch Web"

    const-string v2, "FQL/GQL fetches related to threads/messages"

    const/16 v3, 0xa5

    invoke-static {v5, v5, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/FDy;->a:LX/6Pr;

    .line 2215952
    new-instance v0, LX/6Pr;

    const-string v1, "Threads Fetch DB"

    const-string v2, "Thread operations at DB level"

    const v3, -0xff01

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/FDy;->b:LX/6Pr;

    .line 2215953
    new-instance v0, LX/6Pr;

    const-string v1, "Threads Fetch Cache"

    const-string v2, "Thread operations Cache level"

    const v3, -0x777778

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/FDy;->c:LX/6Pr;

    .line 2215954
    new-instance v0, LX/6Pr;

    const-string v1, "Threads Fetch Caller"

    const-string v2, "Thread operations\' callers"

    const/high16 v3, -0x1000000

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/FDy;->d:LX/6Pr;

    .line 2215955
    new-instance v0, LX/6Pr;

    const-string v1, "Delta"

    const-string v2, "Deltas from the sync protocol"

    const/16 v3, 0xc8

    invoke-static {v5, v4, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/FDy;->e:LX/6Pr;

    .line 2215956
    new-instance v0, LX/6Pr;

    const-string v1, "Sync Exception"

    const-string v2, "Uncaught exceptions from the sync protocol"

    const/high16 v3, -0x10000

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/FDy;->f:LX/6Pr;

    .line 2215957
    new-instance v0, LX/6Pr;

    const-string v1, "Sync network"

    const-string v2, "Sync connection events (i.e. get_diffs)"

    const v3, -0xffff01

    invoke-direct {v0, v1, v2, v3}, LX/6Pr;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, LX/FDy;->g:LX/6Pr;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2215958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2215959
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/6Pr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2215960
    sget-object v0, LX/FDy;->a:LX/6Pr;

    sget-object v1, LX/FDy;->b:LX/6Pr;

    sget-object v2, LX/FDy;->c:LX/6Pr;

    sget-object v3, LX/FDy;->d:LX/6Pr;

    sget-object v4, LX/FDy;->e:LX/6Pr;

    sget-object v5, LX/FDy;->f:LX/6Pr;

    const/4 v6, 0x1

    new-array v6, v6, [LX/6Pr;

    const/4 v7, 0x0

    sget-object v8, LX/FDy;->g:LX/6Pr;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
