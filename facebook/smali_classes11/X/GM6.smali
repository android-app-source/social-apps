.class public final LX/GM6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/GJH;


# direct methods
.method public constructor <init>(LX/GJH;)V
    .locals 0

    .prologue
    .line 2344046
    iput-object p1, p0, LX/GM6;->a:LX/GJH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 2344038
    if-eqz p2, :cond_1

    .line 2344039
    iget-object v0, p0, LX/GM6;->a:LX/GJH;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2344040
    iget-object v0, p0, LX/GM6;->a:LX/GJH;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->toggle()V

    .line 2344041
    :cond_0
    iget-object v0, p0, LX/GM6;->a:LX/GJH;

    invoke-virtual {v0}, LX/GJH;->z()V

    .line 2344042
    iget-object v0, p0, LX/GM6;->a:LX/GJH;

    invoke-virtual {v0, p1}, LX/GJH;->a(Landroid/view/View;)V

    .line 2344043
    :goto_0
    return-void

    .line 2344044
    :cond_1
    iget-object v0, p0, LX/GM6;->a:LX/GJH;

    invoke-virtual {v0}, LX/GJH;->z()V

    .line 2344045
    iget-object v0, p0, LX/GM6;->a:LX/GJH;

    invoke-virtual {v0}, LX/GJH;->y()V

    goto :goto_0
.end method
