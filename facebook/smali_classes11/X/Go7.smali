.class public LX/Go7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0if;

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2394094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2394095
    iput-object p1, p0, LX/Go7;->a:LX/0if;

    .line 2394096
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Go7;->b:Ljava/util/Map;

    .line 2394097
    return-void
.end method

.method public static a(LX/0QB;)LX/Go7;
    .locals 4

    .prologue
    .line 2394098
    const-class v1, LX/Go7;

    monitor-enter v1

    .line 2394099
    :try_start_0
    sget-object v0, LX/Go7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2394100
    sput-object v2, LX/Go7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2394101
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2394102
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2394103
    new-instance p0, LX/Go7;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/Go7;-><init>(LX/0if;)V

    .line 2394104
    move-object v0, p0

    .line 2394105
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2394106
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Go7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2394107
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2394108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)LX/GoE;
    .locals 1

    .prologue
    .line 2394090
    new-instance v0, LX/GoE;

    invoke-direct {v0, p0, p1}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2394091
    iget-object v0, p0, LX/Go7;->a:LX/0if;

    if-eqz v0, :cond_0

    .line 2394092
    iget-object v0, p0, LX/Go7;->a:LX/0if;

    sget-object v1, LX/0ig;->w:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2394093
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2394083
    iget-object v0, p0, LX/Go7;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2394084
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2394085
    iget-object v0, p0, LX/Go7;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2394086
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2394087
    iget-object v0, p0, LX/Go7;->a:LX/0if;

    if-eqz v0, :cond_0

    .line 2394088
    iget-object v0, p0, LX/Go7;->a:LX/0if;

    sget-object v1, LX/0ig;->w:LX/0ih;

    invoke-virtual {v0, v1, p1, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2394089
    :cond_0
    return-void
.end method
