.class public LX/GgA;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GgA",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378609
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2378610
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/GgA;->b:LX/0Zi;

    .line 2378611
    iput-object p1, p0, LX/GgA;->a:LX/0Ot;

    .line 2378612
    return-void
.end method

.method public static a(LX/0QB;)LX/GgA;
    .locals 4

    .prologue
    .line 2378641
    const-class v1, LX/GgA;

    monitor-enter v1

    .line 2378642
    :try_start_0
    sget-object v0, LX/GgA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378643
    sput-object v2, LX/GgA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378644
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378645
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378646
    new-instance v3, LX/GgA;

    const/16 p0, 0x212c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GgA;-><init>(LX/0Ot;)V

    .line 2378647
    move-object v0, v3

    .line 2378648
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378649
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GgA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378650
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2378640
    const v0, -0x3392c77f    # -6.2185988E7f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2378623
    check-cast p2, LX/Gg9;

    .line 2378624
    iget-object v0, p0, LX/GgA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;

    iget-object v1, p2, LX/Gg9;->a:LX/GfY;

    iget-object v2, p2, LX/Gg9;->b:LX/1Pp;

    const/4 p0, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v4, 0x0

    const/4 v9, 0x2

    .line 2378625
    iget-object v3, v1, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    .line 2378626
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2378627
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->p()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;

    move-result-object v5

    .line 2378628
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;->a()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 2378629
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnitItemContentConnection;->a()LX/0Px;

    move-result-object v8

    .line 2378630
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p2

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, p2, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2378631
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2378632
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2378633
    :cond_0
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 2378634
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v6, v5

    .line 2378635
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    .line 2378636
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x6

    invoke-interface {v3, v5, v9}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v10, v9}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v8

    if-nez v7, :cond_2

    move-object v5, v4

    :goto_1
    if-ge v7, v9, :cond_3

    move-object v3, v4

    :goto_2
    invoke-static {v0, p1, v5, v3, v2}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->a(Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;LX/1De;Landroid/net/Uri;Landroid/net/Uri;LX/1Pp;)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b22fd

    invoke-interface {v3, v11, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v8, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    if-ge v7, v11, :cond_4

    move-object v5, v4

    :goto_3
    const/4 v3, 0x4

    if-ge v7, v3, :cond_5

    :goto_4
    invoke-static {v0, p1, v5, v4, v2}, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->a(Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;LX/1De;Landroid/net/Uri;Landroid/net/Uri;LX/1Pp;)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b22fd

    invoke-interface {v3, v10, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v8, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2378637
    const v4, -0x3392c77f    # -6.2185988E7f

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2378638
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2378639
    return-object v0

    :cond_2
    invoke-virtual {v6, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    move-object v5, v3

    goto :goto_1

    :cond_3
    invoke-virtual {v6, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    goto :goto_2

    :cond_4
    invoke-virtual {v6, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    move-object v5, v3

    goto :goto_3

    :cond_5
    invoke-virtual {v6, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    move-object v4, v3

    goto :goto_4
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2378613
    invoke-static {}, LX/1dS;->b()V

    .line 2378614
    iget v0, p1, LX/1dQ;->b:I

    .line 2378615
    packed-switch v0, :pswitch_data_0

    .line 2378616
    :goto_0
    return-object v2

    .line 2378617
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2378618
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2378619
    check-cast v1, LX/Gg9;

    .line 2378620
    iget-object p1, p0, LX/GgA;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;

    iget-object p2, v1, LX/Gg9;->a:LX/GfY;

    .line 2378621
    iget-object p0, p1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlPhotoComponentSpec;->c:LX/Gfl;

    iget-object v1, p2, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Gfl;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPage;)V

    .line 2378622
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3392c77f
        :pswitch_0
    .end packed-switch
.end method
