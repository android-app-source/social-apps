.class public final LX/GBV;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 0

    .prologue
    .line 2327001
    iput-object p1, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2327002
    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->G:LX/GBg;

    sget-object v1, LX/GBg;->SMS:LX/GBg;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->j:LX/2Dt;

    invoke-virtual {v0}, LX/2Dt;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-boolean v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    if-eqz v0, :cond_1

    .line 2327003
    :cond_0
    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->m(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327004
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2327005
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v2, 0x0

    .line 2327006
    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    .line 2327007
    iput-boolean v2, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->N:Z

    .line 2327008
    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    if-eqz v0, :cond_0

    .line 2327009
    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    invoke-virtual {v0}, LX/GaP;->d()V

    .line 2327010
    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    const/4 v1, 0x0

    .line 2327011
    iput-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->Q:LX/GaP;

    .line 2327012
    :cond_0
    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->z(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327013
    iget-object v0, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0, v2}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Z)V

    .line 2327014
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodResult;

    .line 2327015
    iget-object v1, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->m:LX/2Ng;

    iget-object v2, p0, LX/GBV;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v2, v2, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/account/recovery/common/protocol/OpenIDConnectAccountRecoveryMethodResult;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/2Ng;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2327016
    return-void
.end method
