.class public LX/FkC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile u:LX/FkC;


# instance fields
.field private final b:LX/EmQ;

.field private final c:LX/EmX;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field private final f:LX/0V8;

.field private final g:LX/0en;

.field private final h:LX/Fjf;

.field private final i:LX/0aG;

.field public final j:LX/FkD;

.field public final k:LX/Fk4;

.field private final l:LX/Fk3;

.field private final m:LX/Fk5;

.field private final n:LX/Fk7;

.field private final o:LX/Fk8;

.field private final p:LX/Fk9;

.field private final q:LX/FkA;

.field private final r:LX/FkB;

.field public s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/FBP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2278224
    const-class v0, LX/FkC;

    sput-object v0, LX/FkC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/EmQ;LX/EmX;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;LX/0V8;LX/0en;LX/Fjf;LX/0aG;LX/FkD;LX/Fk4;LX/Fk3;LX/Fk5;)V
    .locals 2
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2278268
    new-instance v0, LX/Fk7;

    invoke-direct {v0, p0}, LX/Fk7;-><init>(LX/FkC;)V

    iput-object v0, p0, LX/FkC;->n:LX/Fk7;

    .line 2278269
    new-instance v0, LX/Fk8;

    invoke-direct {v0, p0}, LX/Fk8;-><init>(LX/FkC;)V

    iput-object v0, p0, LX/FkC;->o:LX/Fk8;

    .line 2278270
    new-instance v0, LX/Fk9;

    invoke-direct {v0, p0}, LX/Fk9;-><init>(LX/FkC;)V

    iput-object v0, p0, LX/FkC;->p:LX/Fk9;

    .line 2278271
    new-instance v0, LX/FkA;

    invoke-direct {v0, p0}, LX/FkA;-><init>(LX/FkC;)V

    iput-object v0, p0, LX/FkC;->q:LX/FkA;

    .line 2278272
    new-instance v0, LX/FkB;

    invoke-direct {v0, p0}, LX/FkB;-><init>(LX/FkC;)V

    iput-object v0, p0, LX/FkC;->r:LX/FkB;

    .line 2278273
    iput-object p2, p0, LX/FkC;->c:LX/EmX;

    .line 2278274
    iput-object p1, p0, LX/FkC;->b:LX/EmQ;

    .line 2278275
    iput-object p3, p0, LX/FkC;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2278276
    iput-object p4, p0, LX/FkC;->e:Ljava/util/concurrent/ExecutorService;

    .line 2278277
    iput-object p5, p0, LX/FkC;->f:LX/0V8;

    .line 2278278
    iput-object p6, p0, LX/FkC;->g:LX/0en;

    .line 2278279
    iput-object p7, p0, LX/FkC;->h:LX/Fjf;

    .line 2278280
    iput-object p8, p0, LX/FkC;->i:LX/0aG;

    .line 2278281
    iput-object p9, p0, LX/FkC;->j:LX/FkD;

    .line 2278282
    iput-object p10, p0, LX/FkC;->k:LX/Fk4;

    .line 2278283
    iput-object p11, p0, LX/FkC;->l:LX/Fk3;

    .line 2278284
    iput-object p12, p0, LX/FkC;->m:LX/Fk5;

    .line 2278285
    iget-object v0, p0, LX/FkC;->c:LX/EmX;

    iget-object v1, p0, LX/FkC;->n:LX/Fk7;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2278286
    iget-object v0, p0, LX/FkC;->c:LX/EmX;

    iget-object v1, p0, LX/FkC;->o:LX/Fk8;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2278287
    iget-object v0, p0, LX/FkC;->c:LX/EmX;

    iget-object v1, p0, LX/FkC;->p:LX/Fk9;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2278288
    iget-object v0, p0, LX/FkC;->c:LX/EmX;

    iget-object v1, p0, LX/FkC;->q:LX/FkA;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2278289
    iget-object v0, p0, LX/FkC;->c:LX/EmX;

    iget-object v1, p0, LX/FkC;->r:LX/FkB;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2278290
    return-void
.end method

.method public static a(LX/0QB;)LX/FkC;
    .locals 3

    .prologue
    .line 2278257
    sget-object v0, LX/FkC;->u:LX/FkC;

    if-nez v0, :cond_1

    .line 2278258
    const-class v1, LX/FkC;

    monitor-enter v1

    .line 2278259
    :try_start_0
    sget-object v0, LX/FkC;->u:LX/FkC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2278260
    if-eqz v2, :cond_0

    .line 2278261
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/FkC;->b(LX/0QB;)LX/FkC;

    move-result-object v0

    sput-object v0, LX/FkC;->u:LX/FkC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2278262
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2278263
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2278264
    :cond_1
    sget-object v0, LX/FkC;->u:LX/FkC;

    return-object v0

    .line 2278265
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2278266
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/FkC;J)V
    .locals 3

    .prologue
    .line 2278291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FkC;->b:LX/EmQ;

    sget-object v1, LX/EmM;->APP_UPDATE:LX/EmM;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p2, v1, v2}, LX/EmQ;->a(JLX/EmM;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2278292
    monitor-exit p0

    return-void

    .line 2278293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/FkC;Ljava/lang/String;J)V
    .locals 10

    .prologue
    .line 2278229
    const/4 v6, 0x1

    .line 2278230
    iget-object v0, p0, LX/FkC;->m:LX/Fk5;

    const-string v1, "sideloading_on_download_success"

    invoke-virtual {v0, v1}, LX/Fk5;->a(Ljava/lang/String;)V

    .line 2278231
    :try_start_0
    iget-object v0, p0, LX/FkC;->b:LX/EmQ;

    sget-object v4, LX/EmM;->APP_UPDATE:LX/EmM;

    const/4 v5, 0x0

    move-object v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v5}, LX/EmQ;->a(Ljava/lang/String;JLX/EmM;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 2278232
    if-eqz v0, :cond_6

    .line 2278233
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 2278234
    iget-object v2, p0, LX/FkC;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    iget-object v3, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-static {v3}, LX/FkE;->d(Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    invoke-interface {v2, v3, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    iget-object v3, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-static {v3}, LX/FkE;->g(Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2278235
    :try_start_1
    invoke-static {v0}, LX/0en;->b(Ljava/io/File;)Ljava/util/jar/JarFile;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2278236
    :try_start_2
    iget-object v2, p0, LX/FkC;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-static {v3}, LX/FkE;->f(Ljava/lang/String;)LX/0Tn;

    move-result-object v3

    const-string v4, "application/vnd.android.package-archive"

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2278237
    iget-object v3, p0, LX/FkC;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v4, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-static {v4}, LX/FkE;->e(Ljava/lang/String;)LX/0Tn;

    move-result-object v4

    const-wide/32 v8, 0x1e00000

    invoke-interface {v3, v4, v8, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 2278238
    iget-object v3, p0, LX/FkC;->f:LX/0V8;

    sget-object v7, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v3, v7}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v8

    .line 2278239
    iget-object v3, p0, LX/FkC;->h:LX/Fjf;

    invoke-virtual {v3, v0, v2}, LX/Fjf;->a(Ljava/util/jar/JarFile;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    cmp-long v3, v8, v4

    if-ltz v3, :cond_5

    .line 2278240
    const-string v3, "application/java-archive"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2278241
    iget-object v2, p0, LX/FkC;->t:LX/FBP;

    if-eqz v2, :cond_0

    .line 2278242
    iget-object v2, p0, LX/FkC;->t:LX/FBP;

    .line 2278243
    iget-object v3, v2, LX/FBP;->a:Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 2278244
    if-eqz v3, :cond_0

    .line 2278245
    new-instance v4, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment$DownloadingButtonListenImplentation$1;

    invoke-direct {v4, v2}, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment$DownloadingButtonListenImplentation$1;-><init>(LX/FBP;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2278246
    :cond_0
    iget-object v2, p0, LX/FkC;->j:LX/FkD;

    iget-object v3, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, LX/FkD;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2278247
    :cond_1
    const/4 v6, 0x0

    move v1, v6

    .line 2278248
    :goto_0
    if-eqz v0, :cond_2

    .line 2278249
    :try_start_3
    invoke-virtual {v0}, Ljava/util/jar/JarFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2278250
    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    .line 2278251
    iget-object v0, p0, LX/FkC;->k:LX/Fk4;

    iget-object v1, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Fk4;->a(Ljava/lang/String;)V

    .line 2278252
    :cond_3
    :goto_2
    return-void

    .line 2278253
    :catch_0
    iget-object v0, p0, LX/FkC;->k:LX/Fk4;

    iget-object v1, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Fk4;->a(Ljava/lang/String;)V

    goto :goto_2

    :catch_1
    iget-object v0, p0, LX/FkC;->k:LX/Fk4;

    iget-object v1, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Fk4;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 2278254
    :catchall_0
    move-exception v0

    move v1, v6

    :goto_3
    if-eqz v1, :cond_4

    .line 2278255
    iget-object v1, p0, LX/FkC;->k:LX/Fk4;

    iget-object v2, p0, LX/FkC;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/Fk4;->a(Ljava/lang/String;)V

    :cond_4
    throw v0

    :catch_2
    goto :goto_1

    .line 2278256
    :catchall_1
    move-exception v0

    goto :goto_3

    :cond_5
    move v1, v6

    goto :goto_0

    :cond_6
    move v1, v6

    goto :goto_1
.end method

.method private static b(LX/0QB;)LX/FkC;
    .locals 13

    .prologue
    .line 2278227
    new-instance v0, LX/FkC;

    invoke-static {p0}, LX/EmQ;->a(LX/0QB;)LX/EmQ;

    move-result-object v1

    check-cast v1, LX/EmQ;

    invoke-static {p0}, LX/EmX;->a(LX/0QB;)LX/EmX;

    move-result-object v2

    check-cast v2, LX/EmX;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v5

    check-cast v5, LX/0V8;

    invoke-static {p0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v6

    check-cast v6, LX/0en;

    invoke-static {p0}, LX/Fjf;->b(LX/0QB;)LX/Fjf;

    move-result-object v7

    check-cast v7, LX/Fjf;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v8

    check-cast v8, LX/0aG;

    invoke-static {p0}, LX/FkD;->a(LX/0QB;)LX/FkD;

    move-result-object v9

    check-cast v9, LX/FkD;

    invoke-static {p0}, LX/Fk4;->b(LX/0QB;)LX/Fk4;

    move-result-object v10

    check-cast v10, LX/Fk4;

    invoke-static {p0}, LX/Fk3;->b(LX/0QB;)LX/Fk3;

    move-result-object v11

    check-cast v11, LX/Fk3;

    invoke-static {p0}, LX/Fk5;->b(LX/0QB;)LX/Fk5;

    move-result-object v12

    check-cast v12, LX/Fk5;

    invoke-direct/range {v0 .. v12}, LX/FkC;-><init>(LX/EmQ;LX/EmX;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;LX/0V8;LX/0en;LX/Fjf;LX/0aG;LX/FkD;LX/Fk4;LX/Fk3;LX/Fk5;)V

    .line 2278228
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2278225
    const/4 v0, 0x0

    iput-object v0, p0, LX/FkC;->t:LX/FBP;

    .line 2278226
    return-void
.end method
