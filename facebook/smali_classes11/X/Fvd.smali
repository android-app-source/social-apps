.class public final LX/Fvd;
.super Landroid/view/View;
.source ""


# static fields
.field private static final b:LX/Fwd;


# instance fields
.field public a:I

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2301691
    new-instance v0, LX/Fwd;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1, v2}, LX/Fwd;-><init>(ZZZZ)V

    sput-object v0, LX/Fvd;->b:LX/Fwd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Ot;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/FyY;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2301687
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2301688
    iput-object p2, p0, LX/Fvd;->c:LX/0Ot;

    .line 2301689
    iput p3, p0, LX/Fvd;->a:I

    .line 2301690
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2301681
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2301682
    iget-boolean v0, p0, LX/Fvd;->d:Z

    if-nez v0, :cond_0

    .line 2301683
    :goto_0
    return-void

    .line 2301684
    :cond_0
    iget-object v0, p0, LX/Fvd;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FyY;

    sget-object v1, LX/Fvd;->b:LX/Fwd;

    invoke-virtual {v0, p0, p1, v1}, LX/FyY;->a(Landroid/view/View;Landroid/graphics/Canvas;LX/Fwd;)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 2301685
    iget v0, p0, LX/Fvd;->a:I

    invoke-static {v0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0, p1, v0}, LX/Fvd;->setMeasuredDimension(II)V

    .line 2301686
    return-void
.end method
