.class public final LX/GAF;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/GAE;


# instance fields
.field private final b:LX/GAD;

.field public final c:I

.field public final d:I

.field private final e:I

.field public final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Lorg/json/JSONObject;

.field private final l:Lorg/json/JSONObject;

.field private final m:Ljava/lang/Object;

.field private final n:Ljava/net/HttpURLConnection;

.field public final o:LX/GAA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2324875
    new-instance v0, LX/GAE;

    const/16 v1, 0xc8

    const/16 v2, 0x12b

    invoke-direct {v0, v1, v2}, LX/GAE;-><init>(II)V

    sput-object v0, LX/GAF;->a:LX/GAE;

    return-void
.end method

.method private constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;LX/GAA;)V
    .locals 2

    .prologue
    .line 2324876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2324877
    iput p1, p0, LX/GAF;->c:I

    .line 2324878
    iput p2, p0, LX/GAF;->d:I

    .line 2324879
    iput p3, p0, LX/GAF;->e:I

    .line 2324880
    iput-object p4, p0, LX/GAF;->f:Ljava/lang/String;

    .line 2324881
    iput-object p5, p0, LX/GAF;->g:Ljava/lang/String;

    .line 2324882
    iput-object p9, p0, LX/GAF;->l:Lorg/json/JSONObject;

    .line 2324883
    iput-object p10, p0, LX/GAF;->k:Lorg/json/JSONObject;

    .line 2324884
    iput-object p11, p0, LX/GAF;->m:Ljava/lang/Object;

    .line 2324885
    iput-object p12, p0, LX/GAF;->n:Ljava/net/HttpURLConnection;

    .line 2324886
    iput-object p6, p0, LX/GAF;->h:Ljava/lang/String;

    .line 2324887
    iput-object p7, p0, LX/GAF;->i:Ljava/lang/String;

    .line 2324888
    const/4 v0, 0x0

    .line 2324889
    if-eqz p13, :cond_0

    .line 2324890
    iput-object p13, p0, LX/GAF;->o:LX/GAA;

    .line 2324891
    const/4 v0, 0x1

    .line 2324892
    :goto_0
    invoke-static {}, LX/GAF;->f()LX/GsH;

    move-result-object v1

    .line 2324893
    if-eqz v0, :cond_1

    sget-object v0, LX/GAD;->OTHER:LX/GAD;

    :goto_1
    iput-object v0, p0, LX/GAF;->b:LX/GAD;

    .line 2324894
    iget-object v0, p0, LX/GAF;->b:LX/GAD;

    .line 2324895
    sget-object p1, LX/GsG;->a:[I

    invoke-virtual {v0}, LX/GAD;->ordinal()I

    move-result p2

    aget p1, p1, p2

    packed-switch p1, :pswitch_data_0

    .line 2324896
    const/4 p1, 0x0

    :goto_2
    move-object v0, p1

    .line 2324897
    iput-object v0, p0, LX/GAF;->j:Ljava/lang/String;

    .line 2324898
    return-void

    .line 2324899
    :cond_0
    new-instance v1, LX/GAM;

    invoke-direct {v1, p0, p5}, LX/GAM;-><init>(LX/GAF;Ljava/lang/String;)V

    iput-object v1, p0, LX/GAF;->o:LX/GAA;

    goto :goto_0

    .line 2324900
    :cond_1
    if-eqz p8, :cond_2

    .line 2324901
    sget-object v0, LX/GAD;->TRANSIENT:LX/GAD;

    .line 2324902
    :goto_3
    move-object v0, v0

    .line 2324903
    goto :goto_1

    .line 2324904
    :pswitch_0
    iget-object p1, v1, LX/GsH;->d:Ljava/lang/String;

    goto :goto_2

    .line 2324905
    :pswitch_1
    iget-object p1, v1, LX/GsH;->f:Ljava/lang/String;

    goto :goto_2

    .line 2324906
    :pswitch_2
    iget-object p1, v1, LX/GsH;->e:Ljava/lang/String;

    goto :goto_2

    .line 2324907
    :cond_2
    iget-object v0, v1, LX/GsH;->a:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, v1, LX/GsH;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2324908
    iget-object v0, v1, LX/GsH;->a:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 2324909
    if-eqz v0, :cond_3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2324910
    :cond_3
    sget-object v0, LX/GAD;->OTHER:LX/GAD;

    goto :goto_3

    .line 2324911
    :cond_4
    iget-object v0, v1, LX/GsH;->c:Ljava/util/Map;

    if-eqz v0, :cond_6

    iget-object v0, v1, LX/GsH;->c:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2324912
    iget-object v0, v1, LX/GsH;->c:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 2324913
    if-eqz v0, :cond_5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2324914
    :cond_5
    sget-object v0, LX/GAD;->LOGIN_RECOVERABLE:LX/GAD;

    goto :goto_3

    .line 2324915
    :cond_6
    iget-object v0, v1, LX/GsH;->b:Ljava/util/Map;

    if-eqz v0, :cond_8

    iget-object v0, v1, LX/GsH;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2324916
    iget-object v0, v1, LX/GsH;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 2324917
    if-eqz v0, :cond_7

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2324918
    :cond_7
    sget-object v0, LX/GAD;->TRANSIENT:LX/GAD;

    goto/16 :goto_3

    .line 2324919
    :cond_8
    sget-object v0, LX/GAD;->OTHER:LX/GAD;

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 14

    .prologue
    .line 2324870
    const/4 v1, -0x1

    const/4 v3, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move v2, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v0 .. v13}, LX/GAF;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;LX/GAA;)V

    .line 2324871
    return-void
.end method

.method public constructor <init>(Ljava/net/HttpURLConnection;Ljava/lang/Exception;)V
    .locals 15

    .prologue
    .line 2324872
    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p2

    instance-of v1, v0, LX/GAA;

    if-eqz v1, :cond_0

    check-cast p2, LX/GAA;

    move-object/from16 v14, p2

    :goto_0
    move-object v1, p0

    move-object/from16 v13, p1

    invoke-direct/range {v1 .. v14}, LX/GAF;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;LX/GAA;)V

    .line 2324873
    return-void

    .line 2324874
    :cond_0
    new-instance v14, LX/GAA;

    move-object/from16 v0, p2

    invoke-direct {v14, v0}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;)LX/GAF;
    .locals 14

    .prologue
    .line 2324834
    :try_start_0
    const-string v0, "code"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2324835
    const-string v0, "code"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2324836
    const-string v0, "body"

    const-string v2, "FACEBOOK_NON_JSON_RESULT"

    invoke-static {p0, v0, v2}, LX/Gsc;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    .line 2324837
    if-eqz v9, :cond_3

    instance-of v0, v9, Lorg/json/JSONObject;

    if-eqz v0, :cond_3

    .line 2324838
    check-cast v9, Lorg/json/JSONObject;

    .line 2324839
    const/4 v4, 0x0

    .line 2324840
    const/4 v5, 0x0

    .line 2324841
    const/4 v7, 0x0

    .line 2324842
    const/4 v6, 0x0

    .line 2324843
    const/4 v8, 0x0

    .line 2324844
    const/4 v2, 0x0

    .line 2324845
    const/4 v3, 0x0

    .line 2324846
    const/4 v0, 0x0

    .line 2324847
    const-string v10, "error"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2324848
    const-string v0, "error"

    const/4 v2, 0x0

    invoke-static {v9, v0, v2}, LX/Gsc;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 2324849
    const-string v2, "type"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2324850
    const-string v2, "message"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2324851
    const-string v2, "code"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 2324852
    const-string v3, "error_subcode"

    const/4 v6, -0x1

    invoke-virtual {v0, v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    .line 2324853
    const-string v6, "error_user_msg"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2324854
    const-string v6, "error_user_title"

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2324855
    const-string v8, "is_transient"

    const/4 v10, 0x0

    invoke-virtual {v0, v8, v10}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 2324856
    const/4 v0, 0x1

    .line 2324857
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 2324858
    new-instance v0, LX/GAF;

    const/4 v13, 0x0

    move-object v10, p0

    move-object v11, p1

    move-object/from16 v12, p2

    invoke-direct/range {v0 .. v13}, LX/GAF;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;LX/GAA;)V

    .line 2324859
    :goto_1
    return-object v0

    .line 2324860
    :cond_1
    const-string v10, "error_code"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "error_msg"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "error_reason"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2324861
    :cond_2
    const-string v0, "error_reason"

    const/4 v2, 0x0

    invoke-virtual {v9, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2324862
    const-string v0, "error_msg"

    const/4 v2, 0x0

    invoke-virtual {v9, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2324863
    const-string v0, "error_code"

    const/4 v2, -0x1

    invoke-virtual {v9, v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 2324864
    const-string v0, "error_subcode"

    const/4 v3, -0x1

    invoke-virtual {v9, v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    .line 2324865
    const/4 v0, 0x1

    goto :goto_0

    .line 2324866
    :cond_3
    sget-object v0, LX/GAF;->a:LX/GAE;

    invoke-virtual {v0, v1}, LX/GAE;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2324867
    new-instance v0, LX/GAF;

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "body"

    invoke-virtual {p0, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    const-string v9, "body"

    const-string v10, "FACEBOOK_NON_JSON_RESULT"

    invoke-static {p0, v9, v10}, LX/Gsc;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/json/JSONObject;

    :goto_2
    const/4 v13, 0x0

    move-object v10, p0

    move-object v11, p1

    move-object/from16 v12, p2

    invoke-direct/range {v0 .. v13}, LX/GAF;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLorg/json/JSONObject;Lorg/json/JSONObject;Ljava/lang/Object;Ljava/net/HttpURLConnection;LX/GAA;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2324868
    :catch_0
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2324869
    :cond_5
    const/4 v9, 0x0

    goto :goto_2
.end method

.method private static declared-synchronized f()LX/GsH;
    .locals 3

    .prologue
    .line 2324826
    const-class v1, LX/GAF;

    monitor-enter v1

    :try_start_0
    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v0

    .line 2324827
    if-eqz v0, :cond_1

    sget-object v2, LX/Gsc;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gsa;

    :goto_0
    move-object v0, v2

    .line 2324828
    if-nez v0, :cond_0

    .line 2324829
    invoke-static {}, LX/GsH;->a()LX/GsH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2324830
    :goto_1
    monitor-exit v1

    return-object v0

    .line 2324831
    :cond_0
    :try_start_1
    iget-object v2, v0, LX/Gsa;->e:LX/GsH;

    move-object v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2324832
    goto :goto_1

    .line 2324833
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2324823
    iget-object v0, p0, LX/GAF;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2324824
    iget-object v0, p0, LX/GAF;->g:Ljava/lang/String;

    .line 2324825
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GAF;->o:LX/GAA;

    invoke-virtual {v0}, LX/GAA;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2324822
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{HttpStatus: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/GAF;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/GAF;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAF;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", errorMessage: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LX/GAF;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
