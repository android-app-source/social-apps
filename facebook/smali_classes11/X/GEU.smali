.class public LX/GEU;
.super LX/GDY;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GDY",
        "<",
        "LX/AAf;",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final b:LX/GG6;

.field private final c:LX/GF4;

.field private final d:LX/2U3;

.field public final e:LX/0ad;

.field public f:LX/GNL;

.field public g:LX/GDm;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/2U3;LX/GNL;LX/GG3;LX/GDm;LX/0ad;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331790
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p7

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/GDY;-><init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/2U3;)V

    .line 2331791
    iput-object p4, p0, LX/GEU;->b:LX/GG6;

    .line 2331792
    iput-object p3, p0, LX/GEU;->c:LX/GF4;

    .line 2331793
    iput-object p5, p0, LX/GEU;->d:LX/2U3;

    .line 2331794
    iput-object p6, p0, LX/GEU;->f:LX/GNL;

    .line 2331795
    iput-object p8, p0, LX/GEU;->g:LX/GDm;

    .line 2331796
    iput-object p9, p0, LX/GEU;->e:LX/0ad;

    .line 2331797
    return-void
.end method

.method public static a(LX/0QB;)LX/GEU;
    .locals 13

    .prologue
    .line 2331779
    const-class v1, LX/GEU;

    monitor-enter v1

    .line 2331780
    :try_start_0
    sget-object v0, LX/GEU;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2331781
    sput-object v2, LX/GEU;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2331782
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2331783
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2331784
    new-instance v3, LX/GEU;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v6

    check-cast v6, LX/GF4;

    invoke-static {v0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v7

    check-cast v7, LX/GG6;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v8

    check-cast v8, LX/2U3;

    invoke-static {v0}, LX/GNL;->b(LX/0QB;)LX/GNL;

    move-result-object v9

    check-cast v9, LX/GNL;

    invoke-static {v0}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v10

    check-cast v10, LX/GG3;

    invoke-static {v0}, LX/GDm;->a(LX/0QB;)LX/GDm;

    move-result-object v11

    check-cast v11, LX/GDm;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-direct/range {v3 .. v12}, LX/GEU;-><init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/2U3;LX/GNL;LX/GG3;LX/GDm;LX/0ad;)V

    .line 2331785
    move-object v0, v3

    .line 2331786
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2331787
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GEU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2331788
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2331789
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)LX/0zP;
    .locals 8

    .prologue
    .line 2331708
    invoke-static {}, LX/AAg;->a()LX/AAf;

    move-result-object v0

    const-string v1, "input"

    .line 2331709
    new-instance v3, LX/4DC;

    invoke-direct {v3}, LX/4DC;-><init>()V

    .line 2331710
    new-instance v4, LX/4DB;

    invoke-direct {v4}, LX/4DB;-><init>()V

    .line 2331711
    iget-object v5, p0, LX/GEU;->f:LX/GNL;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/GNL;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)LX/4Ch;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(LX/4Ch;)LX/4DB;

    .line 2331712
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    move-object v5, v5

    .line 2331713
    if-eqz v5, :cond_5

    .line 2331714
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    move-object v5, v5

    .line 2331715
    invoke-virtual {v4, v5}, LX/4DB;->b(Ljava/lang/String;)LX/4DB;

    .line 2331716
    :cond_0
    :goto_0
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v5

    .line 2331717
    if-eqz v5, :cond_6

    .line 2331718
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v5

    .line 2331719
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(Ljava/lang/String;)LX/4DB;

    .line 2331720
    :goto_1
    invoke-virtual {v3, v4}, LX/4DC;->a(LX/4DB;)LX/4DC;

    .line 2331721
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v4

    const/4 v5, 0x1

    .line 2331722
    sget-object v6, LX/GET;->a:[I

    invoke-virtual {v4}, LX/8wL;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 2331723
    const/4 v5, 0x0

    :goto_2
    :pswitch_0
    move v4, v5

    .line 2331724
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2331725
    const-string v5, "save_settings"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2331726
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->c(Ljava/lang/String;)LX/4DC;

    .line 2331727
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v4

    .line 2331728
    invoke-virtual {v4}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 2331729
    invoke-virtual {v4}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v5

    .line 2331730
    :goto_3
    move-object v4, v5

    .line 2331731
    invoke-virtual {v3, v4}, LX/4DC;->d(Ljava/lang/String;)LX/4DC;

    .line 2331732
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->a(Ljava/lang/String;)LX/4DC;

    .line 2331733
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2331734
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->b(Ljava/lang/Integer;)LX/4DC;

    .line 2331735
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2331736
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->b(Ljava/lang/String;)LX/4DC;

    .line 2331737
    :cond_2
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v4, v4

    .line 2331738
    if-eqz v4, :cond_3

    .line 2331739
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v4, v4

    .line 2331740
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/CreativeAdModel;->n()LX/4Cf;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->a(LX/4Cf;)LX/4DC;

    .line 2331741
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v4

    if-ltz v4, :cond_4

    .line 2331742
    iget-object v4, p0, LX/GEU;->b:LX/GG6;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v5

    invoke-virtual {v4, v5}, LX/GG6;->c(I)J

    move-result-wide v5

    long-to-int v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DC;->a(Ljava/lang/Integer;)LX/4DC;

    .line 2331743
    :cond_4
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2331744
    const-string v5, "validate_only"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2331745
    move-object v2, v3

    .line 2331746
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v1, "validateOnly"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/AAf;

    return-object v0

    .line 2331747
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2331748
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v6

    .line 2331749
    if-eqz v5, :cond_0

    .line 2331750
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2331751
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v6

    .line 2331752
    invoke-virtual {v4, v5}, LX/4DB;->b(Ljava/lang/String;)LX/4DB;

    goto/16 :goto_0

    .line 2331753
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2331754
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v6

    .line 2331755
    if-eqz v5, :cond_7

    .line 2331756
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2331757
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v6

    .line 2331758
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(Ljava/lang/String;)LX/4DB;

    goto/16 :goto_1

    .line 2331759
    :cond_7
    const-string v5, "NCPP"

    invoke-virtual {v4, v5}, LX/4DB;->a(Ljava/lang/String;)LX/4DB;

    goto/16 :goto_1

    .line 2331760
    :pswitch_1
    iget-object v5, p0, LX/GEU;->e:LX/0ad;

    sget-short v6, LX/GDK;->z:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v5, v5

    .line 2331761
    goto/16 :goto_2

    .line 2331762
    :cond_8
    sget-object v5, LX/GET;->a:[I

    invoke-virtual {v4}, LX/8wL;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 2331763
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 2331764
    :pswitch_2
    const-string v5, "BOOSTED_PAGELIKE_MOBILE"

    goto/16 :goto_3

    .line 2331765
    :pswitch_3
    const-string v5, "BOOSTED_WEBSITE_MOBILE"

    goto/16 :goto_3

    .line 2331766
    :pswitch_4
    const-string v5, "BOOSTED_CCTA_MOBILE"

    goto/16 :goto_3

    .line 2331767
    :pswitch_5
    const-string v5, "BOOSTED_LOCAL_AWARENESS_MOBILE"

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;IZ)V
    .locals 4

    .prologue
    .line 2331798
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PAUSED:LX/GGB;

    if-ne v0, v1, :cond_1

    .line 2331799
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v0, v1, :cond_0

    const v0, 0x7f080a41

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2331800
    :goto_0
    new-instance v1, LX/5Pu;

    invoke-direct {v1}, LX/5Pu;-><init>()V

    .line 2331801
    iput-object v0, v1, LX/5Pu;->b:Ljava/lang/String;

    .line 2331802
    move-object v0, v1

    .line 2331803
    invoke-virtual {v0}, LX/5Pu;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    .line 2331804
    new-instance v1, LX/A97;

    invoke-direct {v1}, LX/A97;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->WARNING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    .line 2331805
    iput-object v2, v1, LX/A97;->b:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    .line 2331806
    move-object v1, v1

    .line 2331807
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->GENERIC:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 2331808
    iput-object v2, v1, LX/A97;->c:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 2331809
    move-object v1, v1

    .line 2331810
    iput-object v0, v1, LX/A97;->d:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2331811
    move-object v0, v1

    .line 2331812
    invoke-virtual {v0}, LX/A97;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v0

    .line 2331813
    new-instance v1, LX/GES;

    invoke-direct {v1, p0, p1, p2}, LX/GES;-><init>(LX/GEU;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    .line 2331814
    iget-object v2, p0, LX/GEU;->c:LX/GF4;

    new-instance v3, LX/GFn;

    invoke-direct {v3, v0, v1}, LX/GFn;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2331815
    :goto_1
    return-void

    .line 2331816
    :cond_0
    const v0, 0x7f080a42

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2331817
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, LX/GDY;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;IZ)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2331768
    if-eqz p1, :cond_0

    .line 2331769
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331770
    if-nez v0, :cond_1

    .line 2331771
    :cond_0
    iget-object v0, p0, LX/GEU;->d:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "ValidateCreateBoostedComponentMethod got null as result"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2331772
    :goto_0
    return-void

    .line 2331773
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331774
    check-cast v0, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;

    .line 2331775
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentCreateMutationModels$BoostedComponentCreateMutationModel;->a()LX/0Px;

    move-result-object v0

    .line 2331776
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2331777
    iget-object v1, p0, LX/GEU;->c:LX/GF4;

    new-instance v2, LX/GFn;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {v2, v0}, LX/GFn;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 2331778
    :cond_2
    iget-object v0, p0, LX/GEU;->c:LX/GF4;

    new-instance v1, LX/GFn;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/GFn;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2331706
    iget-object v0, p0, LX/GEU;->d:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Failed to validate boosted component"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2331707
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2331705
    const/4 v0, 0x0

    return v0
.end method
