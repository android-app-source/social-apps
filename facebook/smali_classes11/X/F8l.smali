.class public LX/F8l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13F;
.implements LX/13G;
.implements LX/6Xx;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/F8l;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/F8w;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/NuxStep;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/F8w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2204511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204512
    iput-object p1, p0, LX/F8l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2204513
    iput-object p2, p0, LX/F8l;->b:LX/F8w;

    .line 2204514
    return-void
.end method

.method public static a(LX/0QB;)LX/F8l;
    .locals 5

    .prologue
    .line 2204479
    sget-object v0, LX/F8l;->e:LX/F8l;

    if-nez v0, :cond_1

    .line 2204480
    const-class v1, LX/F8l;

    monitor-enter v1

    .line 2204481
    :try_start_0
    sget-object v0, LX/F8l;->e:LX/F8l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2204482
    if-eqz v2, :cond_0

    .line 2204483
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2204484
    new-instance p0, LX/F8l;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/F8w;->a(LX/0QB;)LX/F8w;

    move-result-object v4

    check-cast v4, LX/F8w;

    invoke-direct {p0, v3, v4}, LX/F8l;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/F8w;)V

    .line 2204485
    move-object v0, p0

    .line 2204486
    sput-object v0, LX/F8l;->e:LX/F8l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2204487
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2204488
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2204489
    :cond_1
    sget-object v0, LX/F8l;->e:LX/F8l;

    return-object v0

    .line 2204490
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2204491
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2204510
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204509
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 3

    .prologue
    .line 2204501
    iget-object v0, p0, LX/F8l;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1nR;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2204502
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 2204503
    :goto_0
    return-object v0

    .line 2204504
    :cond_0
    iget-object v0, p0, LX/F8l;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/F8l;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2204505
    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0

    .line 2204506
    :cond_2
    iget-object v1, p0, LX/F8l;->c:Ljava/util/List;

    invoke-static {v1}, LX/F8w;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2204507
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0

    .line 2204508
    :cond_3
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2204500
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/growth/nux/UserAccountNUXActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 2204515
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2204495
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;

    if-nez v0, :cond_1

    .line 2204496
    :cond_0
    :goto_0
    return-void

    .line 2204497
    :cond_1
    check-cast p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;

    .line 2204498
    iget-object v0, p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->steps:Ljava/util/List;

    iput-object v0, p0, LX/F8l;->c:Ljava/util/List;

    .line 2204499
    iget-object v0, p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->extraData:Ljava/util/Map;

    iput-object v0, p0, LX/F8l;->d:Ljava/util/Map;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2204494
    const-string v0, "1630"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204493
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204492
    const-class v0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/NuxStep;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204478
    iget-object v0, p0, LX/F8l;->c:Ljava/util/List;

    return-object v0
.end method
