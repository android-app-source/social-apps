.class public final LX/Gho;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ghp;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Ghp;


# direct methods
.method public constructor <init>(LX/Ghp;)V
    .locals 1

    .prologue
    .line 2384845
    iput-object p1, p0, LX/Gho;->b:LX/Ghp;

    .line 2384846
    move-object v0, p1

    .line 2384847
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2384848
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2384849
    const-string v0, "GametimeMatchStatusComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2384850
    if-ne p0, p1, :cond_1

    .line 2384851
    :cond_0
    :goto_0
    return v0

    .line 2384852
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2384853
    goto :goto_0

    .line 2384854
    :cond_3
    check-cast p1, LX/Gho;

    .line 2384855
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2384856
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2384857
    if-eq v2, v3, :cond_0

    .line 2384858
    iget-object v2, p0, LX/Gho;->a:LX/0Px;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Gho;->a:LX/0Px;

    iget-object v3, p1, LX/Gho;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2384859
    goto :goto_0

    .line 2384860
    :cond_4
    iget-object v2, p1, LX/Gho;->a:LX/0Px;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
