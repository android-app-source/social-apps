.class public final LX/2IX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 391853
    new-instance v0, LX/0U1;

    const-string v1, "identifier"

    const-string v2, "TEXT NOT NULL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2IX;->a:LX/0U1;

    .line 391854
    new-instance v0, LX/0U1;

    const-string v1, "namespace"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2IX;->b:LX/0U1;

    .line 391855
    new-instance v0, LX/0U1;

    const-string v1, "configuration"

    const-string v2, "BLOB"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2IX;->c:LX/0U1;

    .line 391856
    new-instance v0, LX/0U1;

    const-string v1, "priority"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2IX;->d:LX/0U1;

    .line 391857
    new-instance v0, LX/0U1;

    const-string v1, "connection"

    const-string v2, "STRING"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2IX;->e:LX/0U1;

    .line 391858
    new-instance v0, LX/0U1;

    const-string v1, "available_since"

    const-string v2, "INTEGER DEFAULT -1"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2IX;->f:LX/0U1;

    .line 391859
    new-instance v0, LX/0U1;

    const-string v1, "last_failure"

    const-string v2, "INTEGER DEFAULT -1"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/2IX;->g:LX/0U1;

    return-void
.end method
