.class public final LX/3Ms;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/3Mm;


# direct methods
.method public constructor <init>(LX/3Mm;)V
    .locals 0

    .prologue
    .line 556408
    iput-object p1, p0, LX/3Ms;->a:LX/3Mm;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 556409
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, LX/3Of;

    .line 556410
    iget-object v1, p0, LX/3Ms;->a:LX/3Mm;

    iget-object v2, v0, LX/3Of;->a:Ljava/lang/CharSequence;

    iget-object v3, v0, LX/3Of;->c:LX/39y;

    invoke-virtual {v1, v2, v3}, LX/3Mm;->a(Ljava/lang/CharSequence;LX/39y;)V

    .line 556411
    iget-object v1, v0, LX/3Of;->b:LX/3NY;

    if-eqz v1, :cond_0

    .line 556412
    iget-object v1, v0, LX/3Of;->c:LX/39y;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/3Of;->c:LX/39y;

    iget v1, v1, LX/39y;->b:I

    .line 556413
    :goto_0
    iget-object v2, v0, LX/3Of;->b:LX/3NY;

    invoke-interface {v2, v1}, LX/3NY;->a(I)V

    .line 556414
    :cond_0
    iget v1, v0, LX/3Of;->d:I

    iget-object v2, p0, LX/3Ms;->a:LX/3Mm;

    iget v2, v2, LX/3Mm;->f:I

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    .line 556415
    :goto_1
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/3Ms;->a:LX/3Mm;

    iget-object v1, v1, LX/3Mm;->g:LX/3Mr;

    sget-object v2, LX/3Mr;->FINISHED:LX/3Mr;

    if-eq v1, v2, :cond_1

    .line 556416
    iget-object v1, p0, LX/3Ms;->a:LX/3Mm;

    sget-object v2, LX/3Mr;->FINISHED:LX/3Mr;

    .line 556417
    iput-object v2, v1, LX/3Mm;->g:LX/3Mr;

    .line 556418
    iget-object v1, v0, LX/3Of;->b:LX/3NY;

    if-eqz v1, :cond_1

    .line 556419
    iget-object v0, v0, LX/3Of;->b:LX/3NY;

    iget-object v1, p0, LX/3Ms;->a:LX/3Mm;

    iget-object v1, v1, LX/3Mm;->g:LX/3Mr;

    invoke-interface {v0, v1}, LX/3NY;->a(LX/3Mr;)V

    .line 556420
    :cond_1
    return-void

    .line 556421
    :cond_2
    const/4 v1, -0x1

    goto :goto_0

    .line 556422
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
