.class public LX/275;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/ComponentName;

.field private final c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 372724
    const-class v0, LX/275;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/275;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ComponentName;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/katana/login/LoginActivityComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 372725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372726
    iput-object p1, p0, LX/275;->b:Landroid/content/ComponentName;

    .line 372727
    iput-object p2, p0, LX/275;->c:Lcom/facebook/content/SecureContextHelper;

    .line 372728
    return-void
.end method

.method public static b(LX/0QB;)LX/275;
    .locals 3

    .prologue
    .line 372729
    new-instance v2, LX/275;

    invoke-static {p0}, LX/27F;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/275;-><init>(Landroid/content/ComponentName;Lcom/facebook/content/SecureContextHelper;)V

    .line 372730
    return-object v2
.end method

.method public static c(LX/275;Landroid/app/Activity;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 372731
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, LX/275;->b:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 372732
    instance-of v1, p1, Lcom/facebook/registration/activity/AccountRegistrationActivity;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/facebook/registration/activity/RegistrationLoginActivity;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/facebook/languages/switcher/activity/LanguageSwitcherActivity;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/facebook/growth/nux/UserAccountNUXActivity;

    if-nez v1, :cond_0

    .line 372733
    const-string v1, "calling_intent"

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 372734
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "start"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 372735
    const-string v1, "start"

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "start"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 372736
    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ref"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 372737
    const-string v1, "ref"

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "ref"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372738
    :cond_2
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 372739
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 372740
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 372741
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, LX/275;->b:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 372742
    instance-of v0, p1, LX/2A2;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, LX/2A2;

    invoke-interface {v0}, LX/2A2;->b()LX/0kX;

    move-result-object v0

    invoke-virtual {v0}, LX/0kX;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 372743
    :cond_0
    instance-of v0, p1, LX/274;

    if-nez v0, :cond_1

    .line 372744
    invoke-static {p0, p1}, LX/275;->c(LX/275;Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 372745
    :goto_0
    move-object v0, v0

    .line 372746
    if-nez p2, :cond_3

    .line 372747
    :goto_1
    iget-object v1, p0, LX/275;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 372748
    const v0, 0x7f04003b

    const v1, 0x7f040041

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 372749
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 372750
    return-void

    .line 372751
    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "otp"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "username"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 372752
    const-string v0, "otp"

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "otp"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372753
    const-string v0, "username"

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "username"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 372754
    :cond_3
    instance-of v1, p1, Lcom/facebook/registration/activity/AccountRegistrationActivity;

    if-nez v1, :cond_4

    instance-of v1, p1, Lcom/facebook/registration/activity/RegistrationLoginActivity;

    if-eqz v1, :cond_6

    .line 372755
    :cond_4
    const-string v1, "extra_uid"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 372756
    const-string v1, "extra_uid"

    const-string v2, "extra_uid"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372757
    :cond_5
    const-string v1, "extra_pwd"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 372758
    const-string v1, "extra_pwd"

    const-string v2, "extra_pwd"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372759
    :cond_6
    const-string v1, "logout_snackbar_enabled"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 372760
    const-string v1, "logout_snackbar_enabled"

    const-string v2, "logout_snackbar_enabled"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 372761
    :cond_7
    const-string v1, "uid"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 372762
    const-string v1, "uid"

    const-string v2, "uid"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372763
    :cond_8
    const-string v1, "ndid"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 372764
    const-string v1, "ndid"

    const-string v2, "ndid"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372765
    :cond_9
    const-string v1, "landing_experience"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 372766
    const-string v1, "landing_experience"

    const-string v2, "landing_experience"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372767
    :cond_a
    const-string v1, "logged_in_user_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 372768
    const-string v1, "logged_in_user_id"

    const-string v2, "logged_in_user_id"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372769
    :cond_b
    const-string v1, "after_login_redirect_to_notifications_extra"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 372770
    const-string v1, "after_login_redirect_to_notifications_extra"

    const-string v2, "after_login_redirect_to_notifications_extra"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 372771
    :cond_c
    const-string v1, "user_id_to_log_in"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 372772
    const-string v1, "user_id_to_log_in"

    const-string v2, "user_id_to_log_in"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372773
    :cond_d
    goto/16 :goto_1
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 372774
    invoke-static {p0, p1}, LX/275;->c(LX/275;Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 372775
    const-string v1, "login_redirect"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 372776
    iget-object v1, p0, LX/275;->c:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x8a2

    invoke-interface {v1, v0, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 372777
    return-void
.end method
