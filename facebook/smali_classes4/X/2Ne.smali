.class public LX/2Ne;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Cy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/2Cy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 399939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399940
    iput-object p1, p0, LX/2Ne;->a:LX/0Zb;

    .line 399941
    iput-object p2, p0, LX/2Ne;->b:LX/0Ot;

    .line 399942
    return-void
.end method

.method public static a(LX/0QB;)LX/2Ne;
    .locals 5

    .prologue
    .line 399943
    const-class v1, LX/2Ne;

    monitor-enter v1

    .line 399944
    :try_start_0
    sget-object v0, LX/2Ne;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 399945
    sput-object v2, LX/2Ne;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 399946
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399947
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 399948
    new-instance v4, LX/2Ne;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const/16 p0, 0xb3b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/2Ne;-><init>(LX/0Zb;LX/0Ot;)V

    .line 399949
    move-object v0, v4

    .line 399950
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 399951
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/2Ne;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 399952
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 399953
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 399954
    iget-object v0, p0, LX/2Ne;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/GB4;->FORGOT_PASSWORD_CLICK:LX/GB4;

    invoke-virtual {v2}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "account_recovery"

    .line 399955
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 399956
    move-object v1, v1

    .line 399957
    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 399958
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 399959
    iget-object v0, p0, LX/2Ne;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Cy;

    sget-object v1, LX/GB4;->CAPTCHA_REQUIRED:LX/GB4;

    invoke-virtual {v1}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/2Cy;->a(Ljava/lang/String;Z)V

    .line 399960
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 399961
    iget-object v0, p0, LX/2Ne;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/GB4;->SEARCH_VIEWED:LX/GB4;

    invoke-virtual {v2}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "account_recovery"

    .line 399962
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 399963
    move-object v1, v1

    .line 399964
    const-string v2, "search_type"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 399965
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 399966
    iget-object v0, p0, LX/2Ne;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/GB4;->FB4A_ACCOUNT_RECOVERY:LX/GB4;

    invoke-virtual {v2}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "account_recovery"

    .line 399967
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 399968
    move-object v1, v1

    .line 399969
    invoke-interface {v0, v1, v3}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 399970
    iget-object v0, p0, LX/2Ne;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Cy;

    sget-object v1, LX/GB4;->FB4A_ACCOUNT_RECOVERY:LX/GB4;

    invoke-virtual {v1}, LX/GB4;->getEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, LX/2Cy;->a(Ljava/lang/String;Z)V

    .line 399971
    return-void
.end method
