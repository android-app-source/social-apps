.class public final LX/3I4;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3Hz;


# direct methods
.method public constructor <init>(LX/3Hz;)V
    .locals 0

    .prologue
    .line 545489
    iput-object p1, p0, LX/3I4;->a:LX/3Hz;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545490
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 545491
    check-cast p1, LX/2ou;

    .line 545492
    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->z:LX/D6v;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-eq v0, v1, :cond_1

    .line 545493
    :cond_0
    :goto_0
    return-void

    .line 545494
    :cond_1
    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->z:LX/D6v;

    iget v0, v0, LX/D6v;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 545495
    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->t:LX/3I5;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/3I5;->sendEmptyMessage(I)Z

    .line 545496
    :cond_2
    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->z:LX/D6v;

    iget-boolean v0, v0, LX/D6v;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 545497
    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->o:LX/3H4;

    iget-object v1, p0, LX/3I4;->a:LX/3Hz;

    iget-object v1, v1, LX/3Hz;->x:Ljava/lang/String;

    iget-object v2, p0, LX/3I4;->a:LX/3Hz;

    iget-object v2, v2, LX/3Hz;->u:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    iget-object v3, p0, LX/3I4;->a:LX/3Hz;

    iget-boolean v3, v3, LX/3Hz;->B:Z

    iget-object v4, p0, LX/3I4;->a:LX/3Hz;

    iget-object v4, v4, LX/2oy;->j:LX/2pb;

    invoke-virtual {v4}, LX/2pb;->s()LX/04D;

    move-result-object v4

    .line 545498
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "commercial_break_eligible_vod_start"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "commercial_break"

    .line 545499
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 545500
    move-object v5, v5

    .line 545501
    const-string v6, "host_video_id"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "instream_video_ad_type"

    sget-object p1, LX/3H0;->VOD:LX/3H0;

    invoke-virtual {v5, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "number_of_breaks"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "is_sponsored"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "player_origin"

    iget-object p1, v4, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v5, v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 545502
    iget-object v6, v0, LX/3H4;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 545503
    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_3

    .line 545504
    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->B()V

    .line 545505
    :cond_3
    iget-object v0, p0, LX/3I4;->a:LX/3Hz;

    iget-object v0, v0, LX/3Hz;->z:LX/D6v;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/D6v;->e:Z

    goto/16 :goto_0
.end method
