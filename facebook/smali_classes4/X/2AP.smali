.class public final LX/2AP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:LX/2CH;


# direct methods
.method public constructor <init>(LX/2CH;)V
    .locals 0

    .prologue
    .line 377300
    iput-object p1, p0, LX/2AP;->a:LX/2CH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x76e43819

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 377301
    iget-object v1, p0, LX/2AP;->a:LX/2CH;

    const/4 v6, -0x1

    .line 377302
    const-string v4, "extra_user_key"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/user/model/UserKey;

    .line 377303
    const-string v5, "extra_new_state"

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 377304
    if-ne v5, v6, :cond_0

    .line 377305
    :goto_0
    const/16 v1, 0x27

    const v2, -0x55d3ae86

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 377306
    :cond_0
    invoke-static {v1, v4}, LX/2CH;->g(LX/2CH;Lcom/facebook/user/model/UserKey;)LX/3hU;

    move-result-object v6

    .line 377307
    sget-object v7, LX/9lB;->ACTIVE:LX/9lB;

    iget v7, v7, LX/9lB;->value:I

    if-ne v5, v7, :cond_3

    const/4 v5, 0x1

    :goto_1
    iput-boolean v5, v6, LX/3hU;->a:Z

    .line 377308
    iget-boolean v5, v6, LX/3hU;->a:Z

    if-eqz v5, :cond_2

    .line 377309
    iget-object v5, v1, LX/2CH;->k:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v8

    .line 377310
    iget-object v5, v1, LX/2CH;->n:LX/13Q;

    const-string v7, "presence_typing"

    invoke-virtual {v5, v7}, LX/13Q;->a(Ljava/lang/String;)V

    .line 377311
    iget-object v5, v1, LX/2CH;->o:LX/1BA;

    const v7, 0x59000b

    invoke-virtual {v5, v7}, LX/1BA;->a(I)V

    .line 377312
    iget-boolean v5, v6, LX/3hU;->d:Z

    if-nez v5, :cond_1

    .line 377313
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4}, LX/2CH;->e(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/LastActive;

    move-result-object v7

    invoke-static {v1, v8, v9, v5, v7}, LX/2CH;->a(LX/2CH;JLjava/lang/String;Lcom/facebook/user/model/LastActive;)V

    .line 377314
    :cond_1
    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    iput-wide v8, v6, LX/3hU;->e:J

    .line 377315
    :cond_2
    invoke-static {v1, v4}, LX/2CH;->h(LX/2CH;Lcom/facebook/user/model/UserKey;)V

    goto :goto_0

    .line 377316
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method
