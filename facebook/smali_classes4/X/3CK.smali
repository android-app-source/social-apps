.class public LX/3CK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/lang/reflect/Field;

.field public static b:Ljava/lang/reflect/Field;

.field public static c:Ljava/lang/reflect/Field;

.field public static d:Ljava/lang/reflect/Field;

.field public static e:Ljava/lang/reflect/Field;

.field public static f:Ljava/lang/reflect/Field;

.field public static g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 530067
    sput-boolean v4, LX/3CK;->g:Z

    .line 530068
    :try_start_0
    const-string v0, "android.graphics.drawable.LayerDrawable$LayerState"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 530069
    const-string v1, "android.graphics.drawable.LayerDrawable$ChildDrawable"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 530070
    const-class v2, Landroid/graphics/drawable/LayerDrawable;

    const-string v3, "mLayerState"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 530071
    sput-object v2, LX/3CK;->a:Ljava/lang/reflect/Field;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 530072
    const-string v2, "mChildren"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 530073
    sput-object v0, LX/3CK;->b:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 530074
    const-string v0, "mInsetL"

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 530075
    sput-object v0, LX/3CK;->c:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 530076
    const-string v0, "mInsetT"

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 530077
    sput-object v0, LX/3CK;->d:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 530078
    const-string v0, "mInsetR"

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 530079
    sput-object v0, LX/3CK;->e:Ljava/lang/reflect/Field;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 530080
    const-string v0, "mInsetB"

    invoke-virtual {v1, v0}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 530081
    sput-object v0, LX/3CK;->f:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 530082
    const/4 v0, 0x1

    sput-boolean v0, LX/3CK;->g:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 530083
    :goto_0
    return-void

    .line 530084
    :catch_0
    sput-boolean v4, LX/3CK;->g:Z

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 530144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 530085
    if-ne p0, p1, :cond_1

    .line 530086
    :cond_0
    :goto_0
    return v0

    .line 530087
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_0

    .line 530088
    :cond_2
    if-eqz p0, :cond_3

    if-nez p1, :cond_4

    :cond_3
    move v0, v1

    .line 530089
    goto :goto_0

    .line 530090
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 530091
    goto :goto_0

    .line 530092
    :cond_5
    instance-of v2, p0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v2, :cond_7

    .line 530093
    check-cast p0, Landroid/graphics/drawable/ColorDrawable;

    .line 530094
    check-cast p1, Landroid/graphics/drawable/ColorDrawable;

    .line 530095
    const/4 v0, 0x0

    .line 530096
    invoke-virtual {p0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v2

    if-eq v1, v2, :cond_9

    .line 530097
    :cond_6
    :goto_1
    move v0, v0

    .line 530098
    goto :goto_0

    .line 530099
    :cond_7
    instance-of v2, p0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v2, :cond_8

    .line 530100
    check-cast p0, Landroid/graphics/drawable/LayerDrawable;

    .line 530101
    check-cast p1, Landroid/graphics/drawable/LayerDrawable;

    .line 530102
    const/4 v2, 0x0

    .line 530103
    sget-boolean v0, LX/3CK;->g:Z

    if-nez v0, :cond_a

    move v0, v2

    .line 530104
    :goto_2
    move v0, v0

    .line 530105
    goto :goto_0

    .line 530106
    :cond_8
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 530107
    :cond_9
    invoke-virtual {p0}, Landroid/graphics/drawable/ColorDrawable;->getOpacity()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/drawable/ColorDrawable;->getOpacity()I

    move-result v2

    if-ne v1, v2, :cond_6

    .line 530108
    invoke-virtual {p0}, Landroid/graphics/drawable/ColorDrawable;->getAlpha()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/drawable/ColorDrawable;->getAlpha()I

    move-result v2

    if-ne v1, v2, :cond_6

    .line 530109
    const/4 v0, 0x1

    goto :goto_1

    .line 530110
    :cond_a
    :try_start_0
    sget-object v0, LX/3CK;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 530111
    sget-object v1, LX/3CK;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 530112
    sget-object v1, LX/3CK;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 530113
    sget-object v3, LX/3CK;->b:Ljava/lang/reflect/Field;

    invoke-virtual {v3, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 530114
    if-eqz v0, :cond_b

    if-nez v1, :cond_c

    :cond_b
    move v0, v2

    .line 530115
    goto :goto_2

    .line 530116
    :catch_0
    move v0, v2

    goto :goto_2

    .line 530117
    :cond_c
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v4

    if-eq v3, v4, :cond_d

    move v0, v2

    .line 530118
    goto :goto_2

    .line 530119
    :cond_d
    invoke-virtual {p0}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v4

    move v3, v2

    :goto_3
    if-ge v3, v4, :cond_11

    .line 530120
    invoke-virtual {p0, v3}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 530121
    invoke-virtual {p1, v3}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    .line 530122
    invoke-static {v5, v6}, LX/3CK;->a(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z

    move-result v5

    if-nez v5, :cond_e

    move v0, v2

    .line 530123
    goto :goto_2

    .line 530124
    :cond_e
    :try_start_1
    aget-object v5, v0, v3

    aget-object v6, v1, v3

    const/4 v7, 0x0

    .line 530125
    sget-object v8, LX/3CK;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v8, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 530126
    sget-object v9, LX/3CK;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v9, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v9

    .line 530127
    if-eq v8, v9, :cond_12

    .line 530128
    :cond_f
    :goto_4
    move v5, v7
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    .line 530129
    if-nez v5, :cond_10

    move v0, v2

    .line 530130
    goto/16 :goto_2

    .line 530131
    :catch_1
    move v0, v2

    goto/16 :goto_2

    .line 530132
    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 530133
    :cond_11
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 530134
    :cond_12
    sget-object v8, LX/3CK;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v8, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 530135
    sget-object v9, LX/3CK;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v9, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v9

    .line 530136
    if-ne v8, v9, :cond_f

    .line 530137
    sget-object v8, LX/3CK;->e:Ljava/lang/reflect/Field;

    invoke-virtual {v8, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 530138
    sget-object v9, LX/3CK;->e:Ljava/lang/reflect/Field;

    invoke-virtual {v9, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v9

    .line 530139
    if-ne v8, v9, :cond_f

    .line 530140
    sget-object v8, LX/3CK;->f:Ljava/lang/reflect/Field;

    invoke-virtual {v8, v5}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v8

    .line 530141
    sget-object v9, LX/3CK;->f:Ljava/lang/reflect/Field;

    invoke-virtual {v9, v6}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v9

    .line 530142
    if-ne v8, v9, :cond_f

    .line 530143
    const/4 v7, 0x1

    goto :goto_4
.end method
