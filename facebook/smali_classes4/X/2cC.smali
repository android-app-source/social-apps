.class public final LX/2cC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mStoredProcedureId:I

.field public final synthetic this$0:LX/2Pk;


# direct methods
.method public constructor <init>(LX/2Pk;I)V
    .locals 0

    .prologue
    .line 440406
    iput-object p1, p0, LX/2cC;->this$0:LX/2Pk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440407
    iput p2, p0, LX/2cC;->mStoredProcedureId:I

    .line 440408
    return-void
.end method


# virtual methods
.method public callStoredProcedure([B)V
    .locals 6

    .prologue
    .line 440409
    iget-object v0, p0, LX/2cC;->this$0:LX/2Pk;

    iget-object v1, v0, LX/2Pk;->mOmnistoreMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 440410
    :try_start_0
    iget-object v0, p0, LX/2cC;->this$0:LX/2Pk;

    iget-object v0, v0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    if-nez v0, :cond_0

    .line 440411
    const-string v0, "OmnistoreComponentManager"

    const-string v2, "Calling invalid stored procedure sender for storedProcedureId=%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, LX/2cC;->mStoredProcedureId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440412
    monitor-exit v1

    .line 440413
    :goto_0
    return-void

    .line 440414
    :cond_0
    iget-object v0, p0, LX/2cC;->this$0:LX/2Pk;

    iget-object v0, v0, LX/2Pk;->mOmnistore:Lcom/facebook/omnistore/Omnistore;

    iget v2, p0, LX/2cC;->mStoredProcedureId:I

    invoke-virtual {v0, v2, p1}, Lcom/facebook/omnistore/Omnistore;->applyStoredProcedure(I[B)V

    .line 440415
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
