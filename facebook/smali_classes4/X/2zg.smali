.class public LX/2zg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Fp;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/pm/PackageManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 483710
    const-class v0, LX/2zg;

    sput-object v0, LX/2zg;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 483708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 483709
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 483701
    sget-wide v0, LX/0X5;->U:J

    return-wide v0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 483705
    new-instance v1, Landroid/content/ComponentName;

    iget-object v0, p0, LX/2zg;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/backgroundlocation/reporting/monitors/PhotographyMonitorReceiver;

    invoke-direct {v1, v0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 483706
    iget-object v0, p0, LX/2zg;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 483707
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 483702
    new-instance v1, Landroid/content/ComponentName;

    iget-object v0, p0, LX/2zg;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/backgroundlocation/reporting/monitors/PhotographyMonitorReceiver;

    invoke-direct {v1, v0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 483703
    iget-object v0, p0, LX/2zg;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 483704
    return-void
.end method
