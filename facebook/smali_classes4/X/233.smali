.class public LX/233;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0So;

.field public final b:LX/0Zb;

.field private final c:LX/0kb;

.field public final d:Landroid/util/SparseIntArray;

.field public e:J


# direct methods
.method public constructor <init>(LX/0So;LX/0Zb;LX/0kb;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 361759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 361760
    new-instance v0, Landroid/util/SparseIntArray;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, LX/233;->d:Landroid/util/SparseIntArray;

    .line 361761
    iput-object p1, p0, LX/233;->a:LX/0So;

    .line 361762
    iput-object p2, p0, LX/233;->b:LX/0Zb;

    .line 361763
    iput-object p3, p0, LX/233;->c:LX/0kb;

    .line 361764
    return-void
.end method

.method public static a(LX/233;IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 361744
    move v0, v1

    :goto_0
    iget-object v2, p0, LX/233;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 361745
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x3f747ae147ae147bL    # 0.005

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 361746
    iget-object v2, p0, LX/233;->b:LX/0Zb;

    const-string v3, "android_feed_streaming_timings"

    invoke-interface {v2, v3, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 361747
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 361748
    const-string v4, "stop_reason"

    if-eqz p2, :cond_1

    const-string v2, "success"

    :goto_1
    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361749
    const-string v2, "network_type"

    iget-object v4, p0, LX/233;->c:LX/0kb;

    invoke-virtual {v4}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361750
    const-string v2, "network_subtype"

    iget-object v4, p0, LX/233;->c:LX/0kb;

    invoke-virtual {v4}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361751
    const-string v2, "story_index"

    iget-object v4, p0, LX/233;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v3, v2, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 361752
    const-string v2, "story_time"

    iget-object v4, p0, LX/233;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v4

    invoke-virtual {v3, v2, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 361753
    const-string v2, "total_time"

    invoke-virtual {v3, v2, p1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 361754
    const-string v2, "total_stories_count"

    iget-object v4, p0, LX/233;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    invoke-virtual {v3, v2, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 361755
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 361756
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 361757
    :cond_1
    const-string v2, "failure"

    goto :goto_1

    .line 361758
    :cond_2
    return-void
.end method

.method public static a(LX/233;Ljava/lang/String;Lcom/facebook/api/feed/FetchFeedParams;IZ)V
    .locals 3

    .prologue
    .line 361734
    iget-object v0, p0, LX/233;->b:LX/0Zb;

    const-string v1, "android_feed_streaming_reliability"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 361735
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 361736
    const-string v1, "event"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361737
    const-string v1, "fetch_type"

    .line 361738
    iget-object v2, p2, Lcom/facebook/api/feed/FetchFeedParams;->s:LX/0rU;

    move-object v2, v2

    .line 361739
    invoke-virtual {v2}, LX/0rU;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 361740
    const-string v1, "total"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 361741
    const-string v1, "has_next"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 361742
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 361743
    :cond_0
    return-void
.end method

.method public static b(LX/233;)I
    .locals 4

    .prologue
    .line 361731
    iget-wide v0, p0, LX/233;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03g;->b(Z)V

    .line 361732
    iget-object v0, p0, LX/233;->a:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v2, p0, LX/233;->e:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0

    .line 361733
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
