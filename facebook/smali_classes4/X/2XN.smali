.class public LX/2XN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;",
        "Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0SG;

.field private b:LX/00H;


# direct methods
.method public constructor <init>(LX/0SG;LX/00H;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 419834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419835
    iput-object p1, p0, LX/2XN;->a:LX/0SG;

    .line 419836
    iput-object p2, p0, LX/2XN;->b:LX/00H;

    .line 419837
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 419838
    check-cast p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;

    .line 419839
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "push_url"

    .line 419840
    iget-object v2, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 419841
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "token"

    .line 419842
    iget-object v3, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 419843
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "access_token"

    .line 419844
    iget-object v4, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->c:Ljava/lang/String;

    move-object v4, v4

    .line 419845
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "locale"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "device_id"

    .line 419846
    iget-object v6, p1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenNoUserParams;->d:Ljava/lang/String;

    move-object v6, v6

    .line 419847
    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 419848
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/2XN;->b:LX/00H;

    invoke-virtual {v2}, LX/00H;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/nonuserpushtokens"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 419849
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "registerPushNoUser"

    .line 419850
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 419851
    move-object v2, v2

    .line 419852
    const-string v3, "POST"

    .line 419853
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 419854
    move-object v2, v2

    .line 419855
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 419856
    move-object v1, v2

    .line 419857
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 419858
    move-object v0, v1

    .line 419859
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 419860
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 419861
    move-object v0, v0

    .line 419862
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 419863
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 419864
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 419865
    new-instance v1, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;

    const-string v2, "success"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    const/4 v2, 0x0

    iget-object v3, p0, LX/2XN;->a:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct {v1, v0, v2, v4, v5}, Lcom/facebook/push/fbpushtoken/RegisterPushTokenResult;-><init>(ZZJ)V

    return-object v1
.end method
