.class public LX/23s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/1Qt;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/23s;


# instance fields
.field private final a:LX/0sV;

.field public final b:Z

.field private final c:LX/19m;

.field private final d:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 365509
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/1Qt;->FEED:LX/1Qt;

    const-string v2, "NEWSFEED"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->GOOD_FRIENDS_FEED:LX/1Qt;

    const-string v2, "NEWSFEED"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->PAGE_TIMELINE:LX/1Qt;

    const-string v2, "PAGE"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->MY_TIMELINE:LX/1Qt;

    const-string v2, "PROFILE"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->OTHER_PERSON_TIMELINE:LX/1Qt;

    const-string v2, "PROFILE"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->MY_TIMELINE_VIDEO:LX/1Qt;

    const-string v2, "PROFILE"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->OTHER_PERSON_TIMELINE_VIDEO:LX/1Qt;

    const-string v2, "PROFILE"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->GROUPS:LX/1Qt;

    const-string v2, "GROUP"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->PERMALINK:LX/1Qt;

    const-string v2, "NEWSFEED"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->VIDEO_HOME:LX/1Qt;

    const-string v2, "VIDEO_HOME"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->SEARCH_RESULTS:LX/1Qt;

    const-string v2, "SEARCH"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    const-string v2, "SEARCH"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->SEARCH_DENSE_FEED_WITHOUT_UFI:LX/1Qt;

    const-string v2, "SEARCH"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/1Qt;->ELECTION_HUB:LX/1Qt;

    const-string v2, "ELECTION_HUB"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/23s;->e:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(LX/0sV;LX/19m;LX/0ad;LX/0Uh;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 365490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365491
    iput-object p1, p0, LX/23s;->a:LX/0sV;

    .line 365492
    iput-object p2, p0, LX/23s;->c:LX/19m;

    .line 365493
    iput-object p3, p0, LX/23s;->d:LX/0ad;

    .line 365494
    const/16 v0, 0x2a6

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/23s;->b:Z

    .line 365495
    return-void
.end method

.method public static a(LX/0QB;)LX/23s;
    .locals 7

    .prologue
    .line 365496
    sget-object v0, LX/23s;->f:LX/23s;

    if-nez v0, :cond_1

    .line 365497
    const-class v1, LX/23s;

    monitor-enter v1

    .line 365498
    :try_start_0
    sget-object v0, LX/23s;->f:LX/23s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 365499
    if-eqz v2, :cond_0

    .line 365500
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 365501
    new-instance p0, LX/23s;

    invoke-static {v0}, LX/0sV;->a(LX/0QB;)LX/0sV;

    move-result-object v3

    check-cast v3, LX/0sV;

    invoke-static {v0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v4

    check-cast v4, LX/19m;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {p0, v3, v4, v5, v6}, LX/23s;-><init>(LX/0sV;LX/19m;LX/0ad;LX/0Uh;)V

    .line 365502
    move-object v0, p0

    .line 365503
    sput-object v0, LX/23s;->f:LX/23s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365504
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 365505
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 365506
    :cond_1
    sget-object v0, LX/23s;->f:LX/23s;

    return-object v0

    .line 365507
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 365508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/1Qt;)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/VideoChannelEntryPoint;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 365461
    sget-object v0, LX/23s;->e:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1Qt;",
            ")",
            "LX/04H;"
        }
    .end annotation

    .prologue
    .line 365462
    iget-object v0, p0, LX/23s;->a:LX/0sV;

    iget-boolean v0, v0, LX/0sV;->a:Z

    if-nez v0, :cond_0

    .line 365463
    sget-object v0, LX/04H;->CHANNEL_DISABLED:LX/04H;

    .line 365464
    :goto_0
    return-object v0

    .line 365465
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 365466
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 365467
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 365468
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4ed245b

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/1Qt;->VIDEO_HOME:LX/1Qt;

    if-ne p2, v1, :cond_2

    .line 365469
    :cond_1
    iget-boolean v1, p0, LX/23s;->b:Z

    if-eqz v1, :cond_a

    invoke-static {v0}, LX/393;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 365470
    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/23s;->c:LX/19m;

    .line 365471
    iget-boolean v2, v1, LX/19m;->g:Z

    move v1, v2

    .line 365472
    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 365473
    :cond_2
    sget-object v0, LX/04H;->WRONG_STORY_TYPE:LX/04H;

    goto :goto_0

    .line 365474
    :cond_3
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 365475
    if-eqz v1, :cond_4

    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 365476
    sget-object v0, LX/04H;->SPONSORED_VIDEO:LX/04H;

    goto :goto_0

    .line 365477
    :cond_4
    sget-object v1, LX/23s;->e:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 365478
    sget-object v0, LX/04H;->UNSUPPORTED_LOCATION:LX/04H;

    goto :goto_0

    .line 365479
    :cond_5
    iget-object v1, p0, LX/23s;->d:LX/0ad;

    .line 365480
    sget-short v2, LX/100;->bo:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v2

    move v1, v2

    .line 365481
    if-eqz v1, :cond_7

    .line 365482
    sget-object v1, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    if-eq p2, v1, :cond_6

    sget-object v1, LX/1Qt;->SEARCH_DENSE_FEED_WITHOUT_UFI:LX/1Qt;

    if-eq p2, v1, :cond_6

    sget-object v1, LX/1Qt;->SEARCH_RESULTS:LX/1Qt;

    if-ne p2, v1, :cond_b

    :cond_6
    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 365483
    if-eqz v1, :cond_7

    .line 365484
    sget-object v0, LX/04H;->UNSUPPORTED_LOCATION:LX/04H;

    goto :goto_0

    .line 365485
    :cond_7
    iget-object v1, p0, LX/23s;->a:LX/0sV;

    iget-boolean v1, v1, LX/0sV;->j:Z

    if-eqz v1, :cond_9

    .line 365486
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bw()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v0

    .line 365487
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 365488
    :cond_8
    sget-object v0, LX/04H;->NO_RELATED:LX/04H;

    goto/16 :goto_0

    .line 365489
    :cond_9
    sget-object v0, LX/04H;->ELIGIBLE:LX/04H;

    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x0

    goto :goto_1

    :cond_b
    const/4 v1, 0x0

    goto :goto_2
.end method
